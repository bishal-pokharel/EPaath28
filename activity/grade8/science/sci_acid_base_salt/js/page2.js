var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

//array of image
var timeToThinkImage = [
	"images/timetothink/timetothink1.png",
	"images/timetothink/timetothink2.png",
	"images/timetothink/timetothink3.png",
	"images/timetothink/timetothink4.png"
];

var randomImageNumeral = ole.getRandom(1,3,0);
var currentTimetoThinkImage = timeToThinkImage[randomImageNumeral];

var imgpath = $ref+"/images/page2/";

var content=[
	{	
		typeoflayout : "timetothinklayout",
		timetothink  : [
			{
				timeToThinkImageSource     : currentTimetoThinkImage,
				timeToThinkTextData        : data.string.p1_1_time,
				timeToThinkInstructionData : data.string.p2_1,
			}
		],			
	},
	{
		typeoflayout:"svglayout",
		uppertextblock:[
			{
				textclass:"sideheadingstyle",
				textdata: data.string.p2_title1
			},
			{
				textclass:"middleheading",
				textdata: data.string.p2_2
			},
			{
				textclass:"middleheading2",
				textdata: data.string.p2_2_1,
			},
			{
				textclass:"middleheading3",
				textdata: data.string.p2_2_2,
			},
			{
				textclass: "middleheading4",
				textdata: data.string.p2_2_5
			}
		],

		svgblock:[
			{
				middleblock:[
					{
						imagestoshow:[
							{
								imgclass:"arrow1",
								imgsrc: imgpath+"pointing-arrow.png"
							},
							{
								imgclass:"arrow2",
								imgsrc: imgpath+"pointing-arrow2.png"
							},
							{
								imgclass : "clickmeadd",
								imgsrc: "images/clickme_icon.png",
							},
							{
								imgclass : "clickmeacid",
								imgsrc: "images/clickme_icon.png",
							},
							{
								imgclass : "clickmebase",
								imgsrc: "images/clickme_icon.png",
							}
						]
					}
				]
			}
		]

	},
	{
		typeoflayout:"svglayout",
		uppertextblock:[
			{
				textclass:"sideheadingstyle",
				textdata: data.string.p2_2_4
			},
			{
				textclass:"acidsvglable",
				textdata: data.string.p1_2
			},
			{
				textclass:"plus",
				textdata: data.string.p2_3,
			},
			{
				textclass:"basesvglable",
				textdata: data.string.p1_3,
			},
			{
				datahighlightflag:true,
				textclass: "arrowtext",
				textdata: data.string.p2_3_1
			},
			{
				textclass:"arrowchar",
				textdata: data.string.p2_3_2
			},
			{
				textclass: "lasttext",
				textdata: data.string.p2_3_3
			}
		],
		svgblock:[
			{
				middleblock:[
					{
						imagestoshow:[
							{
								imgclass : "clickonadd",
								imgsrc: "images/clickme_icon.png",
							},
							
						]
					}
				]
			}
		]
	},
	{
		typeoflayout:"svglayout",
		uppertextblock:[
			{
				textclass:"sideheadingstyle3",
				textdata: data.string.p2_4
			},
			{
				textclass:"acidsvglable",
				textdata: data.string.p2_4_1
			},
			{
				textclass:"plus",
				textdata: data.string.p2_3,
			},
			{
				textclass:"basesvglable",
				textdata: data.string.p2_4_2,
			},
			{
				datahighlightflag:true,
				textclass: "arrowtext",
				textdata: data.string.p2_3_1
			},
			{
				textclass:"arrowchar",
				textdata: data.string.p2_3_2
			},
			{
				textclass: "lasttext",
				textdata: data.string.p2_5
			},
			{
				textclass:"lasttext2",
				textdata: data.string.p2_6
			}
			
		],
		svgblock:[
			{
				middleblock:[
					{
						imagestoshow:[
							{
								imgclass:"arrow2",
								imgsrc: imgpath+"pointing-arrow2.png"
							},
							{
								imgclass : "clickonadd",
								imgsrc: "images/clickme_icon.png",
							}
						]
					}
				]
		
			}
		]
	},
	{
		uppertextblock:[
			{
				textclass:"polybgtextstyle",
				textdata: data.string.p2_6_1
			}
		]	
	},
	{
		uppertextblock:[
			{
				textclass:"sideheadingstyle2",
				textdata: data.string.p1_4
			},
			{
				textclass: "arrow_box divslidedown",
				textdata: data.string.p2_6_2
			}
		],
	},
	{
		uppertextblock:[
			{
				textclass:"sideheadingstyle2",
				textdata: data.string.p1_4
			}
		],

		imageblock:[
			{
				imagestoshow: [
					{
						imgclass:"saltinspoon",
						imgsrc:imgpath+"salt.png"
					}
				]
			}
		],
		lowertextblock:[
			{
				textclass: "bottomparatextstyle",
				textdata: data.string.p2_6_3,
			}
		]
	},

	{
		uppertextblock:[
			{
				textclass:"sideheadingstyle2",
				textdata: data.string.p1_4
			}
		],

		imageblock:[
			{
				imagestoshow: [
					{
						imgclass:"saltinocean",
						imgsrc:imgpath+"salt05.png"
					}
				]
			}
		],
		lowertextblock:[
			{
				textclass: "bottomparatextstyle",
				textdata: data.string.p2_6_4,
			}
		]
	},
	{
		uppertextblock:[
			{
				textclass:"sideheadingstyle2",
				textdata: data.string.p1_4
			}
		],

		imageblock:[
			{
				imagestoshow: [
					{
						imgclass:"threeimgtogether",
						imgsrc:imgpath+"salt02.png"
					},
					{
						imgclass: "threeimgtogether",
						imgsrc:imgpath+"salt03.png"
					},
					{
						imgclass: "threeimgtogether",
						imgsrc:imgpath+"salt04.png"
					}
				]
			}
		],
		lowertextblock:[
			{
				textclass: "bottomparatextstyle",
				textdata: data.string.p2_6_5,
			}
		]
	},


	
];

$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/	
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("cardcontent", $("#cardcontent-partial").html());
	 Handlebars.registerPartial("usageslowrevealcontent", $("#usageslowrevealcontent-partial").html());
	 Handlebars.registerPartial("timetothinkcontent", $("#timetothinkcontent-partial").html());
	 Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());
     Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	 /*===============================================
		=            data highlight function            =
		===============================================*/
		/**

			What it does:
			- send an element where the function has to see
			for data to highlight
			- this function searches for all text nodes whose
			data-highlight element is set to true 
			-searches for # character and gives a start tag
			;span tag here, also for @ character and replaces with
			end tag of the respective
			
			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var replaceinstring;
			var texthighlightstarttag = "<span class='parsedstring'>";
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					// console.log(val);
					replaceinstring = $(this).html();
					replaceinstring = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		=            user notification function            =
		===============================================*/
		/**		
			How to:
			- First set any html element with "data-usernotification='notifyuser'" attribute
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser 
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

		/*==========  navigation controller function  ==========*/	 
	 function navigationcontroller(){
	 	if(countNext == 0){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }
	
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	
	/*----------  curvedmirrorusage template  ----------*/	
	function curvedmirrorusage(){
		var source = $("#curvedmirrorusage-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		//data highlighter
		// texthighlight($board);

		// user notification
		// notifyuser($board);


		countNext < 1 ? navigationcontroller() : null;

		// if display has usageslowrevealblock
		var $usageslowrevealblock = $board.find('div.usageslowrevealblock');

		if($usageslowrevealblock.length > 0){
			var $nextusageBtn      = $usageslowrevealblock.find("div.nextusageBtn");
			var $cards             = $usageslowrevealblock.find("div[class^='card_block']");
			var totalcardstoreveal = $cards.length - 1;
			var totalcardsrevealed = 0;

			// next use button navigation control
			$nextusageBtn.on('click', function() {
				$(this).css('display', 'none');
				++totalcardsrevealed;
				if(totalcardsrevealed < totalcardstoreveal){
					$cards.eq(totalcardsrevealed).css('opacity', '1');
					$nextusageBtn.show(0);
				}
				else if(totalcardsrevealed == totalcardstoreveal){
					$cards.eq(totalcardsrevealed).css('opacity', '1');
					navigationcontroller();
				}
			});
		}
	}
	
	function svgtemplate(){
		var source   = $("#curvedmirrorusage-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper  = $board.children('div.svglayout').children('div.svgwrapper');
		var $svgcontainer  = $svgwrapper.children('div.svgcontainer'); 	
	
		// if there is a svg content
		if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");
			Snap.load(imgpath+"mixingsalt01.svg", function (f) {    
    		s.append(f);

    	  		// snap objects
    		var basebeaker = s.select("#basebeaker");
    		var acidbeaker = s.select("#acidbeaker");
    		var base_tilt = s.select("#base_tilt");
    		var acid_tilt = s.select("#acid_tilt");
    		var acid_drop = s.select("#acid_drop");
    		var base_drop = s.select("#base_drop");
    		var mixing_action = s.select("#mixing_action");
    		var firstlayer =s.select("#firstlayer");
  			var lastlayer = s.select("#lastlayer");
    		var cloudslayer = s.select("#cloudslayer");
    		var green_plus = s.select("#green_plus");
    		

    		// jquery objects and js variables
    		var $svg      = $svgcontainer.find('svg');
    		var $basebeaker = $svg.find('#basebeaker');
    		var $acidbeaker = $svg.find('#acidbeaker');
    		var $cloudslayer = $svg.find("#cloudslayer");
    		var $green_plus = $svg.find('#green_plus');
    		var $green_arrow = $svg.find('#green_arrow');

    		var $base_tilt = $svg.find('#base_tilt');
    		var $acid_tilt = $svg.find('#acid_tilt');

    		var $acid_drop = $svg.find('#acid_drop');
    		var $base_drop = $svg.find('#base_drop');

    		var $mixing_beaker = $svg.find('#mixing_beaker');
    		var $firstlayer = $svg.find('#firstlayer');
    		var $last_beaker = $svg.find('#last_beaker');

    		basebeaker.addClass("baseshow");			
    		acidbeaker.addClass("acidshow");	
    		
    		$basebeaker.on(animationend, function(){ 
    			green_plus.addClass("arrowmove");
    			$(".clickmeadd").addClass("cssfadein1");
    			$(".middleheading2").delay(2000).fadeIn(100);
    		});	
  			
  			$(".clickmeadd").on("click",function() { 
  				$(this).hide(0);
  	  			$(".middleheading, .middleheading2, .arrow1").addClass("cssfadeout");
    			$acidbeaker.delay(1000).hide(80);
    			$basebeaker.delay(1000).hide(80);
    			$green_plus.delay(1000).hide(80);
    			acid_tilt.addClass("cssfadein");
    			base_tilt.addClass("cssfadein");
    			mixing_action.addClass("beakermoveup"); 
    			$(".middleheading4").addClass("peekahboo");
    			$(".clickmeacid, .clickmebase").addClass("cssfadein1");  			
  			});

  			$(".clickmeacid").on("click", function() {  
  				$(this).hide(0);
  				acid_drop.addClass("dropityo");
    			firstlayer.addClass("cssfadein1");
  			});

  			$(".clickmebase").on("click", function() {
  				$(this).hide(0);  
  				base_drop.addClass("dropityo");  
    			firstlayer.addClass("cssfadein1");  								
  			});

  			$base_drop.on(animationend, function() { 
  				$base_tilt.delay(1000).hide(0);
  			});
  			$acid_drop.on(animationend, function() { 
  				$acid_tilt.delay(1000).hide(0);

  			});

  			$firstlayer.on(animationend, function() {
  				$(".middleheading4").removeClass("peekahboo"); 
  				cloudslayer.addClass("upabovethebeaker");
  				$(".middleheading3").addClass("cssfadein2");

  			});
 			

  			$cloudslayer.on(animationend, function(){ 
  				// $(".middleheading3").hide(0);
  				$(".middleheading4").text(data.string.p2_2_3);
  				$(".middleheading4").addClass("peekahboo");
  				$firstlayer.hide(0);
  				$cloudslayer.hide(0);
  				lastlayer.addClass("cssfadein");
  				$nextBtn.delay(2500).show(0);
  			});

 		});
 	}
}

function svgtemplate1(){
		var source   = $("#curvedmirrorusage-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper  = $board.children('div.svglayout').children('div.svgwrapper');
		var $svgcontainer  = $svgwrapper.children('div.svgcontainer'); 	
		
		// if there is a svg content
		if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");
			Snap.load(imgpath+"mixingsalt01.svg", function (f) {    
    		s.append(f);

    	  		// snap objects
    		var basebeaker = s.select("#basebeaker");
    		var acidbeaker = s.select("#acidbeaker");
    		var last_beaker = s.select("#last_beaker");
    		var green_arrow = s.select("#green_arrow");
    		var green_plus = s.select("#green_plus");
    		


    		// jquery objects and js variables
    		var $svg      = $svgcontainer.find('svg');
    		var $basebeaker = $svg.find('#basebeaker');
    		var $acidbeaker = $svg.find('#acidbeaker');
    		var $last_beaker = $svg.find("#last_beaker");
    		var $green_plus = $svg.find('#green_plus');
    		var $green_arrow = $svg.find('#green_arrow');
    		var $mixing_action = $svg.find('#mixing_action');
    		var $mixing_beaker = $svg.find('#mixing_beaker');

			$mixing_action.css("opacity","1");
			$mixing_beaker.css("opacity","0");

    		
    		$(".acidsvglable").addClass("acidbeakerslide");			
    		$(".basesvglable").addClass("basebeakerslide");			
    		acidbeaker.addClass("acidbeakerslide");
    		basebeaker.addClass("basebeakerslide");

    		
    		$basebeaker.on(animationend, function(){ 
    			green_plus.addClass("plusslide");
    			$(".plus").addClass("cssfadein");
    			$(".clickonadd").addClass("cssfadein1");
    		});	
  			
  			$(".clickonadd").on("click",function() { 
  				$(this).hide(0);
  				green_arrow.addClass("acidbeakerslide");
  				$(".arrowchar").addClass("cssfadein");
  				$(".arrowtext").addClass("cssfadein");
  				last_beaker.addClass("cssfadein1");
  				$(".lasttext").addClass("cssfadein1");
  				$nextBtn.delay(3500).fadeIn(1000);
  				$(".lasttext2, .arrow2").addClass("cssfadein3");
  			});
 		});
 	}
}
/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */
	
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// if footerNotificationHandler pageEndSetNotification was called then
		// countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
		
		// call navigation controller
		navigationcontroller();
		
		// call the template
		switch(countNext){
			case 0 : curvedmirrorusage(); break;
			case 1 : svgtemplate(); break;
			case 2 : svgtemplate1(); break;
			case 3 : svgtemplate1(); break;
			case 4 : curvedmirrorusage(); break;
			case 5 : curvedmirrorusage(); break;
			case 6 : curvedmirrorusage(); break;
			case 7 : curvedmirrorusage(); break;
			case 8 : curvedmirrorusage(); break;
			case 9 : curvedmirrorusage(); break;
			case 10 : svgtemplate1(); break;
			case 11 : generaltemplate(); break;
			case 12 : svgtemplate2(); break;
			case 13 : generaltemplate(); break;
			default : break;
		}

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);	
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();
	});

/*=====  End of Templates Controller Block  ======*/
});

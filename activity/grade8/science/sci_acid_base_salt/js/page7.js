var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
var imgpath = $ref+"/images/page7/";

var content=[
    {
        typeoflayout   : "svglayout",

        uppertextblock:[
            {
                textclass:"polybgtextstyle",
                textdata: data.string.p7_title1
            }
        ],
    },
    {
        uppertextblock:[
            {
                textclass: "middlepara move",
                textdata: data.string.p7_definition,
            }
        ],
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"weak_acid_large",
                        imgsrc: imgpath+"weak-acid.png"
                    },
                    {
                        imgclass:"strong_acid_large",
                        imgsrc: imgpath+"strong-acid.png"
                    }
                ]
            }
        ],
    },

    {
        headerblockwithimg:[
            {
                textclass: "sidepara",
                textdata: data.string.p7_title2,
            },
            {
                imagestoshow:[
                    {
                        imgclass:"acid_small",
                        imgsrc: imgpath+"weak-acid.png"
                    },
                ]
            }

        ],

        uppertextblock:[
            {
                datahighlightflag:false,
                textclass: "middledef move",
                textdata: data.string.p7_t2_para1,
            },
            {
                datahighlightflag:true,
                textclass: "middledef2 move2",
                textdata: data.string.p7_t2_para2,
            }
        ]

    },

    {
        typeoflayout   : "propertylayout",

        headerblockwithimg:[
            {
                textclass: "sidepara",
                textdata: data.string.p7_title2,
            },
            {
                imagestoshow:[
                    {
                        imgclass:"acid_small",
                        imgsrc: imgpath+"weak-acid.png"
                    },
                ]
            }
        ],

        propertyblock:[
        {
            propertycontainerheadingdata: data.string.p7_title2,
            propertyexplainimgsrc: imgpath+"lemon.png",
            mainpropertytext:data.string.p7_title7,
            propertyexplaindata: data.string.p7_im1,
                propertycontainerlist:[
                    {
                        propertylistdata: data.string.p7_li1,
                    },
                ]
            }
        ]
    },

    {
        typeoflayout   : "propertylayout",

        headerblockwithimg:[
            {
                textclass: "sidepara",
                textdata: data.string.p7_title2,
            },
            {
                imagestoshow:[
                    {
                        imgclass:"acid_small",
                        imgsrc: imgpath+"weak-acid.png"
                    },
                ]
            }
        ],

        propertyblock:[
        {
            propertycontainerheadingdata: data.string.p7_title2,
            propertyexplainimgsrc: imgpath+"dairy-product.png",
            mainpropertytext:data.string.p7_title7,
            propertyexplaindata: data.string.p7_im2,
                propertycontainerlist:[
                    {
                        propertylistdata: data.string.p7_li1,
                    },
                    {
                        propertylistdata: data.string.p7_li2,

                    }
                ]
            }
        ]
    },

    {
        typeoflayout   : "propertylayout",

        headerblockwithimg:[
            {
                textclass: "sidepara",
                textdata: data.string.p7_title2,
            },
            {
                imagestoshow:[
                    {
                        imgclass:"acid_small",
                        imgsrc: imgpath+"weak-acid.png"
                    },
                ]
            }
        ],

        propertyblock:[
        {
            propertycontainerheadingdata: data.string.p7_title2,
            propertyexplainimgsrc: imgpath+"green-apple.png",
            mainpropertytext:data.string.p7_title7,
            propertyexplaindata: data.string.p7_im3,
                propertycontainerlist:[
                    {
                        propertylistdata: data.string.p7_li1,
                    },
                    {
                        propertylistdata: data.string.p7_li2,
                    },
                    {
                        propertylistdata: data.string.p7_li3,
                    }
                ]
            }
        ]
    },

    {
        typeoflayout   : "propertylayout",

        headerblockwithimg:[
            {
                textclass: "sidepara",
                textdata: data.string.p7_title2,
            },
            {
                imagestoshow:[
                    {
                        imgclass:"acid_small",
                        imgsrc: imgpath+"weak-acid.png"
                    },
                ]
            }
        ],

        propertyblock:[
        {
            propertycontainerheadingdata: data.string.p7_title2,
            propertyexplainimgsrc: imgpath+"grapes.png",
            mainpropertytext:data.string.p7_title7,
            propertyexplaindata: data.string.p7_im4,
                propertycontainerlist:[
                    {
                        propertylistdata: data.string.p7_li1,
                    },
                    {
                        propertylistdata: data.string.p7_li2,
                    },
                    {
                        propertylistdata: data.string.p7_li3,
                    },
                    {
                        propertylistdata: data.string.p7_li4,
                    }
                ]
            }
        ]
    },

    {
        typeoflayout   : "propertylayout",

        headerblockwithimg:[
            {
                textclass: "sidepara",
                textdata: data.string.p7_title2,
            },
            {
                imagestoshow:[
                    {
                        imgclass:"acid_small",
                        imgsrc: imgpath+"weak-acid.png"
                    },
                ]
            }
        ],

        propertyblock:[
        {
            propertycontainerheadingdata: data.string.p7_title2,
            propertyexplainimgsrc: imgpath+"vinegar.png",
            mainpropertytext:data.string.p7_title7,
            propertyexplaindata: data.string.p7_im5,
                propertycontainerlist:[
                    {
                        propertylistdata: data.string.p7_li1,
                    },
                    {
                        propertylistdata: data.string.p7_li2,
                    },
                    {
                        propertylistdata: data.string.p7_li3,
                    },
                    {
                        propertylistdata: data.string.p7_li4,
                    },
                    {
                        propertylistdata: data.string.p7_li5,
                    }
                ]
            }
        ]
    },

    {
        typeoflayout   : "propertylayout",

        headerblockwithimg:[
            {
                textclass: "sidepara",
                textdata: data.string.p7_title3,
            },
            {
                imagestoshow:[
                    {
                        imgclass:"acid_small",
                        imgsrc: imgpath+"strong-acid.png"
                    },
                ]
            }
        ],

        uppertextblock:[
            {
                datahighlightflag:false,
                textclass: "middledef move",
                textdata: data.string.p7_t3_para1,
            },
            {
                datahighlightflag:false,
                textclass: "middledef2 move2",
                textdata: data.string.p7_t3_para2,
            }
        ]
    },

    {
        typeoflayout   : "svglayout",

        headerblockwithimg:[
            {
                textclass: "sidepara",
                textdata: data.string.p7_title3,
            },
            {
                imagestoshow:[
                    {
                        imgclass:"acid_small",
                        imgsrc: imgpath+"strong-acid.png"
                    },
                ]
            }
        ],

        uppertextblock:[
            {
                textclass:"intruction",
                textdata: data.string.p7_t3_para3_1
            }
        ],

        svgblock:[
            {

                svgheading:[
                    {
                        textclass: "svgfirstpara",
                        textdata: data.string.p7_t3_para3
                    }
                ],
                imagecharacterstics:[
                    {
                        naturedetail: data.string.p7_t3_para3
                    }
                ]
            }
        ]
    },
    {
     typeoflayout   : "svglayout",

        uppertextblock:[
            {
                textclass: "middledef1",
                textdata: data.string.p7_title8,
            }
        ],
        middletextblock:[
            {
                textclass: "acidtitle showfromleft",
                textdata: data.string.p7_t8_li1
            }
        ],

        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"strong_acid",
                        imgsrc:imgpath+"strong-acid.png"

                    }
                ]
            }
        ],
        lowertextblock:[
            {
                textclass:"usesinfo1 showfromright",
                textdata: data.string.p7_t8_li4
            },
            {
                textclass:"usesinfo2 showfromright2",
                textdata: data.string.p7_t8_li4_1
            }
        ]
    },

    {
     typeoflayout   : "svglayout",

        uppertextblock:[
            {
                textclass: "middledef1",
                textdata: data.string.p7_title8,
            }
        ],
        middletextblock:[
            {
                textclass: "acidtitle showfromleft",
                textdata: data.string.p7_t8_li2
            }
        ],

        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"strong_acid",
                        imgsrc:imgpath+"strong-acid.png"

                    }
                ]
            }
        ],
        lowertextblock:[
            {
                textclass:"usesinfo1 showfromright",
                textdata: data.string.p7_t8_li5
            },
            {
                textclass:"usesinfo2 showfromright2",
                textdata: data.string.p7_t8_li5_1
            }
        ]
    },

    {
     typeoflayout   : "svglayout",

        uppertextblock:[
            {
                textclass: "middledef1",
                textdata: data.string.p7_title8,
            }
        ],
        middletextblock:[
            {
                textclass: "acidtitle showfromleft",
                textdata: data.string.p7_t8_li3
            }
        ],

        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"strong_acid",
                        imgsrc:imgpath+"strong-acid.png"

                    }
                ]
            }
        ],
        lowertextblock:[
            {
                textclass:"usesinfo1 showfromright",
                textdata: data.string.p7_t8_li6
            }
        ]
    },

    {
     typeoflayout   : "svglayout",

        usernotificationblock:[
            {
            	datahighlightflag: "true",
                textclass: "headingstylewidanim",
                textdata: data.string.p7_t8_heading5
            }
        ],

        middletextblock:[
            {
                textclass: "colorbox1",
                textdata: data.string.p7_t8_h4_1
            },
            {
                textclass: "colorbox2",
                textdata: data.string.p7_t8_h4_2
            },
            {
                textclass: "colorbox3",
                textdata: data.string.p7_t8_h4_3
            },
            {
                textclass: "colorbox4",
                textdata: data.string.p7_t8_h4_4
            },
            {
                textclass: "colorbox5",
                textdata: data.string.p7_t8_h4_5
            }
        ]
    },

    {
     typeoflayout   : "svglayout",

        uppertextblock:[
            {
                textclass: "headingstyle",
                textdata: data.string.p7_t8_heading6
            }
        ],

        imagetextblock:[
            {
                textclass:"acidch",
                textdata: data.string.p7_ch1
            },
            {
                textclass:"acidch1",
                textdata: data.string.p7_ch1_1
            },
            {
                textclass:"acidch2",
                textdata: data.string.p7_ch1_2
            },
            {
                textclass:"acidch3",
                textdata: data.string.p7_ch1_3
            },
            {
                textclass:"plusch",
                textdata: data.string.p7_ch1_plus
            },
            {
                textclass:"blankch",
                textdata: data.string.p7_ch2
            },
            {
                textclass:"blankch1",
                textdata: data.string.p7_ch2_1
            },
            {
                textclass:"blankch2",
                textdata: data.string.p7_ch2_2
            },
            {
                textclass:"blankch3",
                textdata: data.string.p7_ch2_3
            },
            {
                textclass:"arrowch",
                textdata: data.string.p7_ch2_arrow
            },
            {
                textclass:"reaction1",
                textdata: data.string.p7_ch3
            },
            {
                textclass:"reaction1_1",
                textdata: data.string.p7_ch3_1
            },
            {
                textclass:"reaction1_2",
                textdata: data.string.p7_ch3_2
            },
            {
                textclass:"reaction1_3",
                textdata: data.string.p7_ch3_3
            },
            {
                textclass:"reactionplus",
                textdata: data.string.p7_ch1_plus
            },
            {
                textclass:"reaction2",
                textdata: data.string.p7_ch4
            },
            {
                textclass:"reaction2_1",
                textdata: data.string.p7_ch4_1
            },
            {
                textclass:"reaction2_2",
                textdata: data.string.p7_ch_1
            },
            {
                textclass:"reaction2_carbon",
                textdata: data.string.p7_ch4_2
            },
            {
                textclass:"reaction2_plussign",
                textdata: data.string.p7_ch1_plus
            },
            {
                textclass:"reaction2_3",
                textdata: data.string.p7_ch4_3
            },


        ],

        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"acidbottle",
                        imgsrc:imgpath+"acid.png"
                    },
                    {
                        imgclass: "plus",
                        imgsrc:imgpath+"plus.png"
                    },
                    {
                        imgclass: "blank",
                        imgsrc:imgpath+"space.png"
                    },
                    {
                        imgclass: "arrow",
                        imgsrc:imgpath+"arrow.png"
                    },
                    {
                        imgclass: "question",
                        imgsrc:imgpath+"question-mark.png"
                    },
                    {
                        imgclass: "hydrogengas",
                        imgsrc: imgpath+"beaker.png"
                    },
                    {
                        imgclass: "plus2",
                        imgsrc:imgpath+"plus.png"
                    },
                    {
                        imgclass: "waterinbeaker",
                        imgsrc: imgpath+"water.png"
                    },
                    {
                        imgclass: "waterwithbubble",
                        imgsrc: imgpath+"animation.gif"
                    },
                    {
                        imgclass: "NaCl",
                        imgsrc: imgpath+"acid_beaker.png"
                    },
                    {
                        imgclass: "MgCl",
                        imgsrc: imgpath+"beakergrey.png"
                    },
                    {
                        imgclass: "NaNO",
                        imgsrc: imgpath+"beaker02.png"
                    },
                    {
                        imgclass:"salt",
                        imgsrc: imgpath+"salt.png"
                    }
                ],
            },
        ],

        imageblockwidclick:[
            {
                imagestoshow:[
                   {
                        imgclass: "metal1",
                        imgsrc:imgpath+"metal-new.png"
                    },
                    {
                        imgclass: "clickmemag",
                        imgsrc: "images/clickme_icon.png",
                    }
                ],
                imagelabels:[
                    {
                        imagelabelclass: "magnesium",
                        imagelabeldata: data.string.p7_metal1
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass: "metal2",
                        imgsrc:imgpath+"metal-carbonate.png"
                    },
                    {
                        imgclass: "clickmepot",
                        imgsrc: "images/clickme_icon.png",
                    }
                ],
                imagelabels:[
                    {
                        imagelabelclass: "potassiumhydro",
                        imagelabeldata: data.string.p7_metal2
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass: "metal3",
                        imgsrc:imgpath+"metal-oxide.png"
                    },
                    {
                        imgclass: "clickmebi",
                        imgsrc: "images/clickme_icon.png",
                    }
                ],
                imagelabels:[
                    {
                        imagelabelclass: "sodiumbi",
                        imagelabeldata: data.string.p7_metal3
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass: "metal4",
                        imgsrc:imgpath+"pure-metal.png"
                    },
                    {
                        imgclass: "clickmemet",
                        imgsrc: "images/clickme_icon.png",
                    }
                ],
                imagelabels:[
                    {
                        imagelabelclass: "sodiumoxide",
                        imagelabeldata: data.string.p7_metal4
                    }
                ]
            }
        ],

        lowertextblock:[
            {
                textclass:"metal1info",
                textdata: data.string.p7_lower1
            },
            {
                textclass:"metal2info",
                textdata: data.string.p7_lower2
            },
            {
                textclass:"metal3info",
                textdata: data.string.p7_lower3
            },
            {
                textclass:"metal4info",
                textdata: data.string.p7_lower4
            }
        ]
    },
    {
    typeoflayout: "vertical-center",
       uppertextblock:[
            {
                textclass: "headingstyle2",
                textdata: data.string.p2_12
            }
        ],
        middletextblock:[
            {
                textclass:"headingmetal1",
                textdata: data.string.p7_last1
            },
            {
                textclass:"headingmetal2",
                textdata: data.string.p7_last2
            },
            {
                textclass:"headingmetal3",
                textdata: data.string.p7_last3
            },
            {
                textclass:"headingmetal4",
                textdata: data.string.p7_last4
            },
        ],

         lowertextblock:[
            {
                textclass:"metal1info cssfadein",
                textdata: data.string.p7_lower1
            },
            {
                textclass:"metal2info cssfadein",
                textdata: data.string.p7_lower2
            },
            {
                textclass:"metal3info cssfadein",
                textdata: data.string.p7_lower3
            },
            {
                textclass:"metal4info cssfadein",
                textdata: data.string.p7_lower4
            }
        ]
    }

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);


/*==================================================
=            Handlers and helpers Block            =
==================================================*/

    // register the handlebar partials first
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("imageclickcontent", $("#imageclickcontent-partial").html());
    Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());
    Handlebars.registerPartial("propertycontent", $("#propertycontent-partial").html());

     /*===============================================
        =            data highlight function            =
        ===============================================*/
        /**

            What it does:
            - send an element where the function has to see
            for data to highlight
            - this function searches for all text nodes whose
            data-highlight element is set to true
            -searches for # character and gives a start tag
            ;span tag here, also for @ character and replaces with
            end tag of the respective

            E.g: caller : texthighlight($board);
         */
        function texthighlight($highlightinside){
            var $alltextpara = $highlightinside.find("*[data-highlight='true']");
            var replaceinstring;
            var texthighlightstarttag = "<span class='parsedstring'>";
            var texthighlightendtag   = "</span>";
            if($alltextpara.length > 0){
                $.each($alltextpara, function(index, val) {
                    // console.log(val);
                    replaceinstring = $(this).html();
                    replaceinstring = replaceinstring.replace(/#/g,texthighlightstarttag);
                    replaceinstring = replaceinstring.replace(/@/g,texthighlightendtag);
                    $(this).html(replaceinstring);
                });
            }
        }
        /*=====  End of data highlight function  ======*/

        /*===============================================
        =            user notification function            =
        ===============================================*/
        /**
            How to:
            - First set any html element with "data-usernotification='notifyuser'" attribute
            - Then call this function to give notification
         */

        /**
            What it does:
            - You send an element where the function has to see
            for data to notify user
            - this function searches for all text nodes whose
            data-usernotification attribute is set to notifyuser
            - applies event handler for each of the html element which
             removes the notification style.
         */
        function notifyuser($notifyinside){
            /*variable that will store the element(s) to remove notification from*/
            var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
             // if there are any notifications removal required add the event handler

            // if($allnotifications.length > 0){
            	// // $allnotifications.removeAttr('data-usernotification');
 				// $allnotifications.one('click', function() {
					// /* Act on the event */
					// $(this).attr('data-isclicked', 'clicked');
					// $(this).removeAttr('data-usernotification');
					// $(".headingstylewidanim").addClass("goupdiv");
					// $(".colorbox1").addClass("showfromleft2");
					// $(".colorbox2").addClass("showfromright2");
					// $(".colorbox3").addClass("showfromleft3");
					// $(".colorbox4").addClass("showfromright");
					// $(".colorbox5").addClass("metalanim");
        		 // });
            // }
        }
        /*=====  End of user notification function  ======*/

        /*==========  navigation controller function  ==========*/
     function navigationcontroller(){
        if(countNext == 0){
            $nextBtn.show(0);
            $prevBtn.css('display', 'none');
        }
        else if(countNext > 0 && countNext < $total_page-1){
            $nextBtn.show(0);
            $prevBtn.show(0);
        }
        else if(countNext == $total_page-1){
            $nextBtn.css('display', 'none');
            $prevBtn.show(0);
            ole.footerNotificationHandler.pageEndSetNotification();
        }
     }

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/

	/*=================================================
	=            general template function            =
	=================================================*/
	function generaltemplate(){
		var source   = $("#svg-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
        $nextBtn.hide(0);

        if (countNext==0) {
            $nextBtn.show(0);
        };

        $(".middlepara").on(animationend, function(){
            $nextBtn.show(0);
        });

        $(".middledef2").on(animationend, function(){
            $nextBtn.show(0);
        });

       if (countNext==3||countNext==4||countNext==5||countNext==6||countNext==7) {
        $nextBtn.show(0);
       };

       $(".usesinfo2").on(animationend, function(){
            $nextBtn.show(0);
       });

       if (countNext==12) {
        $nextBtn.delay(2000).show(100);
       };

       $(".colorbox5").on(animationend, function(){
            $nextBtn.show(0);
       });


		//notify user call
        notifyuser($board);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);
        $(".clickmemag").addClass("cssfadein");

        $(".clickmemag").on("click", function(){
            $(".clickmemag").hide(0);
            $(".blank, .question, .magnesium").addClass("cssfadeout");
            $(".metal1").addClass("upup");
        });

        $(".metal1").on(animationend, function(){
            $(".acidch, .plusch, .blankch, .arrowch, .reaction1, .reactionplus, .reaction2").addClass("cssfadein");
            $(".MgCl, .hydrogengas, .plus2").addClass("cssfadein");
        });

        $(".hydrogengas").on(animationend, function(){
            $(".metal1info").addClass("cssfadein");
            $(".clickmepot").addClass("cssfadein2");
        });

        $(".clickmepot").on("click",function(){
            $(".metal1").hide(0);
            $(".clickmepot").hide(0);
            $(".metal2").addClass("upup");
            $(".potassiumhydro").addClass("cssfadeout");
            $(".acidch, .plusch, .blankch, .arrowch, .reaction1, .reactionplus, .reaction2, .plus2, .MgCl, .hydrogengas, .metal1info").hide(0);
        });

        $(".metal2").on(animationend, function(){
            $(".salt, .waterinbeaker").addClass("cssfadein");
            $(".plusch, .arrowch, .reactionplus, .plus2").delay(900).show(0);
            $(".acidch1, .blankch1, .reaction1_1, .reaction2_1").addClass("cssfadein");
        });

        $(".reaction2_1").on(animationend, function(){
            $(".metal2info").addClass("cssfadein");
            $(".clickmebi").addClass("cssfadein2");
        });

        $(".clickmebi").on("click",function(){
            $(".metal2").hide(0);
            $(".clickmebi").hide(0);
            $(".metal3").addClass("upup2");
            $(".sodiumbi").addClass("cssfadeout");
            $(".acidch1, .plusch, .blankch1, .arrowch, .reaction1_1, .reactionplus, .reaction2_1, .plus2, .salt, .waterinbeaker, .metal2info").hide(0);
        });

        $(".metal3").on(animationend, function(){
            $(".salt").delay(900).show(0);
            $(".waterwithbubble").addClass("cssfadein");
            $(".plusch, .arrowch, .reactionplus, .plus2").delay(900).show(0);
            $(".acidch2, .blankch2, .reaction1_2, .reaction2_2, .reaction2_plussign, .reaction2_carbon").addClass("cssfadein");
        });

        $(".reaction2_2").on(animationend, function(){
            $(".metal3info").addClass("cssfadein");
            $(".clickmemet").addClass("cssfadein2");
        });

        $(".clickmemet").on("click",function(){
            $(".clickmemet").hide(0);
            $(".metal3").hide(0);
            $(".metal4").addClass("upup2");
            $(".sodiumoxide").addClass("cssfadeout");
            $(".acidch2, .plusch, .blankch2,.arrowch, .reaction1_2, .reactionplus, .reaction2_2, .plus2, .salt, .waterwithbubble, .metal3info, .reaction2_plussign, .reaction2_carbon").hide(0);
        });

        $(".metal4").on(animationend, function(){
            $(".NaNO").addClass("cssfadein");
            $(".waterinbeaker").delay(900).show(0);
            $(".plusch, .arrowch, .reactionplus, .plus2").delay(900).show(0);
            $(".acidch3,.blankch3, .reaction1_3, .reaction2_3").addClass("cssfadein");
        });

        $(".reaction2_3").on(animationend, function(){
            $(".metal4info").addClass("cssfadein");
            $nextBtn.delay(1000).show(0);
        });

         $(".parsedstring").one('click', function() {
                    $(this).removeAttr('data-usernotification');
                    $(".headingstylewidanim").addClass("goupdiv");
                    $(".colorbox1").addClass("showfromleft2");
                    $(".colorbox2").addClass("showfromright2");
                    $(".colorbox3").addClass("showfromleft3");
                    $(".colorbox4").addClass("showfromright");
                    $(".colorbox5").addClass("metalanim");
         }).attr("data-usernotification","notifyuser");

	}


    /*====================================
    =            svg template            =
    ====================================*/
    function svgtemplate(){
        var source   = $("#svg-template").html();
        var template = Handlebars.compile(source);
        var html     = template(content[countNext]);
        $board.html(html);
        $nextBtn.hide(0);

        //notify user call
        notifyuser($board);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);

        var $svgwrapper              = $board.children('div.svglayout').children('div.svgwrapper');
        var $svgcontainer            = $svgwrapper.children('div.svgcontainer');
        var $imagecharacteristicsbox = $svgwrapper.children('div.imagecharacteristicsbox');

        if($svgcontainer.length > 0){
            var s = Snap(".svgcontainer");
            Snap.load(imgpath+"handwithmasking.svg", function (f) {
            s.append(f);
                // snap objects
            var acidline        = s.select("#acidline");
            var aciddrop        = s.select("#aciddrop");
            var pouringbeaker   = s.select("#pouringbeaker");
            var acidbeaker      = s.select("#acidbeaker");

                // jquery objects and js variables
            var $svg         = $svgcontainer.find('svg');
            var $acidline    = $svg.find('#acidline');
            var $aciddrop    = $svg.find('#aciddrop');
            var $pouringbeaker = $svg.find('#pouringbeaker');
            var $acidbeaker = $svg.find('#acidbeaker');
                $imagecharacteristicsbox.css("top","90%");
                $nextBtn.delay(6000).show(0);

        });
        }
    }


    /*=====  End of svg template  ======*/


/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

    /*==================================================
    =            function to call templates            =
    ==================================================*/
    /**
        Motivation :
        - Make a single function call that handles all the
          template load easier

        How To:
        - Update the template caller with the required templates
        - Call template caller

        What it does:
        - According to value of the Global Variable countNext
            the slide templates are updated
     */

    function templateCaller(){
        /*always hide next and previous navigation button unless
        explicitly called from inside a template*/
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');

        navigationcontroller();

        // if footerNotificationHandler pageEndSetNotification was called then
        // countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;

        // On the basis of countNext value call templates
        switch(countNext){
            case 0: generaltemplate();
                     break;
            case 1: generaltemplate();
                     break;
            case 2: generaltemplate();
                     break;
            case 3: generaltemplate();
                     break;
            case 4: generaltemplate();
                     break;
            case 5: generaltemplate();
                     break;
            case 6: generaltemplate();
                     break;
            case 7: generaltemplate();
                     break;
            case 8: generaltemplate();
                     break;
            case 9: svgtemplate();
                     break;
            case 10: generaltemplate();
                     break;
            case 11: generaltemplate();
                     break;
            case 12: generaltemplate();
                     break;
            case 13: generaltemplate();
                     break;
            case 14: generaltemplate();
                     break;
            case 15: generaltemplate();
                     break
            default:break;
        }

        //call the slide indication bar handler for pink indicators
        loadTimelineProgress($total_page,countNext+1);
    }

    /*this countNext variable change here is solely for development phase and
    should be commented out for deployment*/
    // countNext+=1;

    // first call to template caller
    templateCaller();

    /* navigation buttons event handlers */

    $nextBtn.on('click',function () {
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
        countNext--;
        templateCaller();
    });

/*=====  End of Templates Controller Block  ======*/
});

var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
var imgpath = $ref+"/images/page5/";

var content=[
	{
		typeoflayout   : "svglayout",

		uppertextblock:[
			{
				textclass:"paratextstyle",
				textdata : data.string.p5_title
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"twoimgtogether slideanim",
						imgsrc: imgpath+"methyl-orange.png"
					},
					{
						imgclass:"twoimgtogether slideanim",
						imgsrc: imgpath+"phenolphthalein.png"
					},

				]
			}
		]
	},
	{
		typeoflayout   : "svglayout",

		imageblock:[
			{
				imagestoshow: [
					{
						imgclass : "clickmebase",
						imgsrc: "images/clickme_icon.png",
					},
					{
						imgclass : "clickmeacid",
						imgsrc: "images/clickme_icon.png",
					}
				],
			}
		],

		svgblock:[
			{
				svgheading:[
					{
						textclass:"headingstyle",
						textdata: data.string.p5_title2
					},
					{
						textclass: "acidcap",
						textdata: data.string.p5_caption1
					},
					{
						textclass: "basecap",
						textdata: data.string.p5_caption2
					},
					{
						datahighlightflag:true,
						textclass: "arrow_box",
						textdata: data.string.p5_bubble1
					},
					{
						datahighlightflag:true,
						textclass: "arrow_box2",
						textdata: data.string.p5_bubble2
					}
				]
			}
		],

	},
	{
		typeoflayout   : "svglayout",

		imageblock:[
			{
				imagestoshow: [
					{
						imgclass : "clickmebase",
						imgsrc: "images/clickme_icon.png",
					},
					{
						imgclass : "clickmeacid",
						imgsrc: "images/clickme_icon.png",
					}
				],
			}
		],

		svgblock:[
			{
				svgheading:[
					{
						textclass:"headingstyle",
						textdata: data.string.p5_title3
					},
					{
						textclass: "acidcap",
						textdata: data.string.p5_caption1
					},
					{
						textclass: "basecap",
						textdata: data.string.p5_caption2
					},
					{
						datahighlightflag:true,
						textclass: "arrow_box",
						textdata: data.string.p5_bubble3
					},
					{
						datahighlightflag:true,
						textclass: "arrow_box2",
						textdata: data.string.p5_bubble4
					}
				]
			}
		],

	},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/

	// register the handlebar partials first
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());

	 /*===============================================
		=            data highlight function            =
		===============================================*/
		/**

			What it does:
			- send an element where the function has to see
			for data to highlight
			- this function searches for all text nodes whose
			data-highlight element is set to true
			-searches for # character and gives a start tag
			;span tag here, also for @ character and replaces with
			end tag of the respective

			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var replaceinstring;
			var texthighlightstarttag = "<span class='parsedstring'>";
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					// console.log(val);
					replaceinstring = $(this).html();
					replaceinstring = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		=            user notification function            =
		===============================================*/
		/**
			How to:
			- First set any html element with "data-usernotification='notifyuser'" attribute
			- Then call this function to give notification
		 */

		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			/*variable that will store the element(s) to remove notification from*/
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

		/*==========  navigation controller function  ==========*/
	 function navigationcontroller(){
	 	if(countNext == 0){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }

/*=====  End of Handlers and helpers Block  ======*/


/*=======================================
=            Templates Block            =
=======================================*/
	/*=================================================
	=            svg template function            =
	=================================================*/
	function svgtemplate(){
		var source   = $("#svg-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);

		//notify user call
		notifyuser($board);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		var $svgwrapper              = $board.children('div.svglayout').children('div.svgwrapper');
		var $svgcontainer            = $svgwrapper.children('div.svgcontainer');
		var $imagecharacteristicsbox = $svgwrapper.children('div.imagecharacteristicsbox');


		if($svgcontainer.length > 0){
			$nextBtn.hide(0);
			var s = Snap(".svgcontainer");
			Snap.load(imgpath+"methyl_orange.svg", function (f) {
    		s.append(f);
    			// snap objects
    		var base_first_layer          = s.select("#base_first_layer");
    		var acid_first_layer        = s.select("#acid_first_layer");
    		var baseside_drop         = s.select("#baseside_drop");
    		var acidside_drop      = s.select("#acidside_drop");
    		var aciddropper      = s.select("#aciddropper");
    		var basedropper      = s.select("#basedropper");

    			// jquery objects and js variables
    		var $svg           = $svgcontainer.find('svg');
    		var $base_first_layer      = $svg.find('#base_first_layer');
    		var $acid_first_layer      = $svg.find('#acid_first_layer');
    		var $baseside_drop      = $svg.find('#baseside_drop');
    		var $acidside_drop      = $svg.find('#acidside_drop');
    		var $aciddropper      = $svg.find('#aciddropper');
    		var $basedropper      = $svg.find('#basedropper');

    		$(".clickmeacid").addClass("cssfadein2");
    		aciddropper.addClass("cssfadein");

    		$(".clickmeacid").on("click", function() {
    			$(this).hide(0);
    			acidside_drop.addClass("dropdown");
    			acid_first_layer.addClass("tadaa");
    		});

    		$(".clickmebase").on("click", function() {
    			$(this).hide(0);
    			baseside_drop.addClass("dropdown");
    			base_first_layer.addClass("tadaa");
    		});

    		$acid_first_layer.on(animationend, function() {
    			$(".arrow_box").addClass("show_arrr");
    			aciddropper.removeClass("cssfadein");
    			basedropper.addClass("cssfadein2");
    			$(".clickmebase").addClass("cssfadein3");
    			// $nextBtn.delay(3000).fadeIn(100);
    		});

    		$base_first_layer.on(animationend, function() {
    			$(".arrow_box2").addClass("show_arrr");
    			basedropper.removeClass("cssfadein2");
    			$nextBtn.delay(3000).fadeIn(100);
    		});
 		});
		}
	}

	function svgtemplate1(){
		var source   = $("#svg-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);

		//notify user call
		notifyuser($board);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		var $svgwrapper              = $board.children('div.svglayout').children('div.svgwrapper');
		var $svgcontainer            = $svgwrapper.children('div.svgcontainer');
		var $imagecharacteristicsbox = $svgwrapper.children('div.imagecharacteristicsbox');

		if($svgcontainer.length > 0){
			$nextBtn.hide(0);
			var s = Snap(".svgcontainer");
			Snap.load(imgpath+"phenolpthalein_colours.svg", function (f) {
    		s.append(f);
    			// snap objects
    		var base_first_layer          = s.select("#base_first_layer");
    		var acid_first_layer        = s.select("#acid_first_layer");
    		var baseside_drop         = s.select("#baseside_drop");
    		var acidside_drop      = s.select("#acidside_drop");
    		var aciddropper      = s.select("#aciddropper");
    		var basedropper      = s.select("#basedropper");

    			// jquery objects and js variables
    		var $svg           = $svgcontainer.find('svg');
    		var $base_first_layer      = $svg.find('#base_first_layer');
    		var $acid_first_layer      = $svg.find('#acid_first_layer');
    		var $baseside_drop      = $svg.find('#baseside_drop');
    		var $acidside_drop      = $svg.find('#acidside_drop');
    		var $aciddropper      = $svg.find('#aciddropper');
    		var $basedropper      = $svg.find('#basedropper');

    		$(".clickmeacid").addClass("cssfadein2");
    		aciddropper.addClass("cssfadein");

    		$(".clickmeacid").on("click", function() {
    			$(this).hide(0);
    			acidside_drop.addClass("dropdown");
    		});

    		$(".clickmebase").on("click", function() {
    			$(this).hide(0);
    			baseside_drop.addClass("dropdown");
    			base_first_layer.addClass("tadaa");
    		});

    		$acidside_drop.on(animationend, function() {
    			$(".arrow_box").addClass("show_arrr");
    			aciddropper.removeClass("cssfadein");
    			basedropper.addClass("cssfadein2");
    			$(".clickmebase").addClass("cssfadein3");
    		});

    		$base_first_layer.on(animationend, function() {
    			$(".arrow_box2").addClass("show_arrr");
				navigationcontroller();

    		});
 		});
		}
	}


/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		// if footerNotificationHandler pageEndSetNotification was called then
		// countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
		navigationcontroller();

		// On the basis of countNext value call templates
		switch(countNext){
			case 0: svgtemplate();
					 break;
			case 1: svgtemplate();
					 break;
			case 2: svgtemplate1();
					 break;
			default:break;
		}

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click',function () {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
	});

/*=====  End of Templates Controller Block  ======*/
});

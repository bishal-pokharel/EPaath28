var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
var imgpath = $ref + "/images/page7/";

var content = [{
    typeoflayout: "svglayout",

    uppertextblock: [{
      textclass: "polybgtextstyle",
      textdata: data.string.p9_heading1
    }],
  },
  {
    uppertextblock: [{
      textclass: "middlepara move",
      textdata: data.string.p7_definition2,
    }],

    imageblock: [{
      imagestoshow: [{
          imgclass: "weak_base_large",
          imgsrc: imgpath + "weak-base.png"
        },
        {
          imgclass: "strong_base_large",
          imgsrc: imgpath + "strong-base.png"
        }
      ]
    }],
  },

  {
    headerblockwithimg: [{
        textclass: "sidepara",
        textdata: data.string.p7_title4,
      },
      {
        imagestoshow: [{
          imgclass: "acid_small",
          imgsrc: imgpath + "weak-base.png"
        }, ]
      }
    ],

    uppertextblock: [{
        datahighlightflag: true,
        textclass: "middledef move",
        textdata: data.string.p9_def1,
      },
      {
        datahighlightflag: true,
        textclass: "middledef1 move3",
        textdata: data.string.p9_def2,
      },
      {
        datahighlightflag: true,
        textclass: "middledef2 move2",
        textdata: data.string.p9_def3,
      }
    ]
  },

  {
    typeoflayout: "svglayout",

    headerblockwithimg: [{
        textclass: "sidepara",
        textdata: data.string.p7_title5,
      },
      {
        imagestoshow: [{
          imgclass: "acid_small",
          imgsrc: imgpath + "strong-base.png"
        }, ]
      }
    ],

    uppertextblock: [{
      textclass: "middledef move",
      textdata: data.string.p9_def4
    }]

    // propertyblock:[
    // {
    //     propertycontainerheadingdata: data.string.p7_title2,
    //     propertyexplainimgsrc: imgpath+"lemon.png",
    //     mainpropertytext:data.string.p7_title7,
    //     propertyexplaindata: data.string.p7_im1,
    //         propertycontainerlist:[
    //             {
    //                 propertylistdata: data.string.p7_li1,
    //             },
    //         ]
    //     }
    // ]
  },

  {
    typeoflayout: "propertylayout",

    propertyblock: [{
      propertycontainerheadingdata: data.string.p5_caption2,
      propertyexplainimgsrc: imgpath + "cement01.png",
      mainpropertytext: data.string.p9_heading2,
      propertyexplaindata: data.string.p9_im1,
      propertycontainerlist: [{
        propertylistdata: data.string.p9_li1,
      }, ]
    }]
  },

  {
    typeoflayout: "propertylayout",

    propertyblock: [{
      propertycontainerheadingdata: data.string.p5_caption2,
      propertyexplainimgsrc: imgpath + "soap.png",
      mainpropertytext: data.string.p9_heading2,
      propertyexplaindata: data.string.p9_im2,
      propertycontainerlist: [{
          propertylistdata: data.string.p9_li1,
        },
        {
          propertylistdata: data.string.p9_li2,
        },
      ]
    }]
  },

  {
    typeoflayout: "propertylayout",

    propertyblock: [{
      propertycontainerheadingdata: data.string.p5_caption2,
      propertyexplainimgsrc: imgpath + "ash01.png",
      mainpropertytext: data.string.p9_heading2,
      propertyexplaindata: data.string.p9_im3,
      propertycontainerlist: [{
          propertylistdata: data.string.p9_li1,
        },
        {
          propertylistdata: data.string.p9_li2,
        },
        {
          propertylistdata: data.string.p9_li3,
        }
      ]
    }]
  },

  {
    typeoflayout: "propertylayout",

    propertyblock: [{
      propertycontainerheadingdata: data.string.p5_caption2,
      propertyexplainimgsrc: imgpath + "antacid.png",
      mainpropertytext: data.string.p9_heading2,
      propertyexplaindata: data.string.p9_im4,
      propertycontainerlist: [{
          propertylistdata: data.string.p9_li1,
        },
        {
          propertylistdata: data.string.p9_li2,
        },
        {
          propertylistdata: data.string.p9_li3,
        },
        {
          propertylistdata: data.string.p9_li4,
        }
      ]
    }]
  },
  {
    typeoflayout: "svglayout",

    usernotificationblock: [{
      datahighlightflag: true,
      textclass: "headingstylewidanim",
      textdata: data.string.p9_heading3
    }],

    middletextblock: [{
        textclass: "colorbox1",
        textdata: data.string.p9_h4_1
      },
      {
        textclass: "colorbox2",
        textdata: data.string.p9_h4_2
      },
      {
        textclass: "colorbox3",
        textdata: data.string.p9_h4_3
      },
      {
        textclass: "colorbox4",
        textdata: data.string.p9_h4_4
      },
      {
        textclass: "colorbox5",
        textdata: data.string.p9_h4_5
      }
    ]
  },

  {
    typeoflayout: "svglayout",

    uppertextblock: [{
      textclass: "headingstyle",
      textdata: data.string.p9_heading4
    }],

    imagetextblock: [{
        textclass: "acidch",
        textdata: data.string.p10_ch1
      },
      {
        textclass: "acidch1",
        textdata: data.string.p10_ch1_1
      },
      {
        textclass: "acidch2",
        textdata: data.string.p10_ch1_2
      },
      {
        textclass: "plusch",
        textdata: data.string.p7_ch1_plus
      },
      {
        textclass: "blankch",
        textdata: data.string.p10_ch2
      },
      {
        textclass: "blankch1",
        textdata: data.string.p10_ch2_1
      },
      {
        textclass: "blankch2",
        textdata: data.string.p10_ch2_2
      },
      {
        textclass: "arrowch",
        textdata: data.string.p7_ch2_arrow
      },
      {
        textclass: "reaction1",
        textdata: data.string.p10_ch3
      },
      {
        textclass: "reaction1_1",
        textdata: data.string.p10_ch3_1
      },
      {
        textclass: "reaction1_2",
        textdata: data.string.p10_ch3_2
      },
      {
        textclass: "reactionplus",
        textdata: data.string.p7_ch1_plus
      },
      {
        textclass: "reaction2",
        textdata: data.string.p7_ch4
      },
      {
        textclass: "reaction2_1",
        textdata: data.string.p10_ch_1
      },
      {
        textclass: "reaction2_plussign",
        textdata: data.string.p7_ch1_plus
      },
      {
        textclass: "reaction2_3",
        textdata: data.string.p10_ch4
      },
      {
        textclass: "reaction2_carbon",
        textdata: data.string.p10_ch4
      }
    ],

    imageblock: [{
      imagestoshow: [{
          imgclass: "basebeaker",
          imgsrc: imgpath + "beakergrey.png"
        },
        {
          imgclass: "plus",
          imgsrc: imgpath + "plus.png"
        },
        {
          imgclass: "blank",
          imgsrc: imgpath + "space.png"
        },
        {
          imgclass: "arrow",
          imgsrc: imgpath + "arrow.png"
        },
        {
          imgclass: "question",
          imgsrc: imgpath + "question-mark.png"
        },
        {
          imgclass: "hydrogengas",
          imgsrc: imgpath + "beaker.png"
        },
        {
          imgclass: "plus2",
          imgsrc: imgpath + "plus.png"
        },
        {
          imgclass: "waterinbeaker",
          imgsrc: imgpath + "water.png"
        },
        {
          imgclass: "waterwithbubble",
          imgsrc: imgpath + "animation.gif"
        },
        {
          imgclass: "NaCl",
          imgsrc: imgpath + "acid_beaker.png"
        },
        {
          imgclass: "MgCl",
          imgsrc: imgpath + "beakergrey.png"
        },
        {
          imgclass: "NaNO",
          imgsrc: imgpath + "beaker02.png"
        },
        {
          imgclass: "salt",
          imgsrc: imgpath + "salt.png"
        },
      ],
    }, ],

    imageblockwidclick: [{
        imagestoshow: [{
            imgclass: "metal1",
            imgsrc: imgpath + "acid.png"
          },
          {
            imgclass: "clickme1",
            imgsrc: "images/clickme_icon.png",
          }
        ],
        imagelabels: [{
          imagelabelclass: "name1",
          imagelabeldata: data.string.p10_name1
        }]
      },
      {
        imagestoshow: [{
            imgclass: "metal2",
            imgsrc: imgpath + "animation.gif"
          },
          {
            imgclass: "clickmepot",
            imgsrc: "images/clickme_icon.png",
          }
        ],
        imagelabels: [{
          imagelabelclass: "name2",
          imagelabeldata: data.string.p10_name2
        }]
      },
      {
        imagestoshow: [{
            imgclass: "metal3",
            imgsrc: imgpath + "salt2.png"
          },
          {
            imgclass: "clickmebi",
            imgsrc: "images/clickme_icon.png",
          }
        ],
        imagelabels: [{
          imagelabelclass: "name3",
          imagelabeldata: data.string.p10_name3
        }]
      },
    ],

    lowertextblock: [{
        textclass: "metal1info",
        textdata: data.string.p10_lower1
      },
      {
        textclass: "metal2info",
        textdata: data.string.p10_lower2
      },
      {
        textclass: "metal3info",
        textdata: data.string.p10_lower3
      },
    ]
  },

  {
    typeoflayout: "vertical-center",
    uppertextblock: [{
      textclass: "headingstyle2",
      textdata: data.string.p2_12
    }],
    middletextblock: [{
        textclass: "headingmetal1",
        textdata: data.string.p10_last1
      },
      {
        textclass: "headingmetal2",
        textdata: data.string.p10_last2
      },
      {
        textclass: "headingmetal3",
        textdata: data.string.p10_last3
      },
    ],

    lowertextblock: [{
        textclass: "metal_1 cssfadein",
        textdata: data.string.p10_lower1
      },
      {
        textclass: "metal_2 cssfadein",
        textdata: data.string.p10_lower2
      },
      {
        textclass: "metal_3 cssfadein",
        textdata: data.string.p10_lower3
      }
    ]
  }
];


$(function() {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page, countNext + 1);


  /*==================================================
  =            Handlers and helpers Block            =
  ==================================================*/

  // register the handlebar partials first
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());
  Handlebars.registerPartial("propertycontent", $("#propertycontent-partial").html());
  Handlebars.registerPartial("imageclickcontent", $("#imageclickcontent-partial").html());


  /*===============================================
     =            data highlight function            =
     ===============================================*/
  /**

      What it does:
      - send an element where the function has to see
      for data to highlight
      - this function searches for all text nodes whose
      data-highlight element is set to true
      -searches for # character and gives a start tag
      ;span tag here, also for @ character and replaces with
      end tag of the respective

      E.g: caller : texthighlight($board);
   */
  function texthighlight($highlightinside) {
    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var replaceinstring;
    var texthighlightstarttag = "<span class='parsedstring'>";
    var texthighlightendtag = "</span>";
    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        // console.log(val);
        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
        $(this).html(replaceinstring);
      });
    }
  }
  /*=====  End of data highlight function  ======*/

  /*===============================================
  =            user notification function            =
  ===============================================*/
  /**
      How to:
      - First set any html element with "data-usernotification='notifyuser'" attribute
      - Then call this function to give notification
   */

  /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
   */
  function notifyuser($notifyinside) {
    /*variable that will store the element(s) to remove notification from*/
    var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
    // if there are any notifications removal required add the event handler
    // if($allnotifications.length > 0){
    // $allnotifications.one('click', function() {
    // /* Act on the event */
    // $(this).attr('data-isclicked', 'clicked');
    // $(this).removeAttr('data-usernotification');
    // $(".headingstylewidanim").addClass("goupdiv");
    // $(".colorbox1").addClass("showfromleft2");
    // $(".colorbox2").addClass("showfromright2");
    // $(".colorbox3").addClass("showfromleft3");
    // $(".colorbox4").addClass("showfromright");
    // $(".colorbox5").addClass("metalanim");
    // });
    // }
  }
  /*=====  End of user notification function  ======*/

  /*==========  navigation controller function  ==========*/
  function navigationcontroller() {
    if (countNext == 0) {
      $nextBtn.show(0);
      $prevBtn.css('display', 'none');
    } else if (countNext > 0 && countNext < $total_page - 1) {
      $nextBtn.show(0);
      $prevBtn.show(0);
    } else if (countNext == $total_page - 1) {
      $nextBtn.css('display', 'none');
      $prevBtn.show(0);
      ole.footerNotificationHandler.pageEndSetNotification();
    }
  }

  /*=====  End of Handlers and helpers Block  ======*/

  /*=======================================
  =            Templates Block            =
  =======================================*/

  /*=================================================
  =            general template function            =
  =================================================*/
  function generaltemplate() {
    var source = $("#svg-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $nextBtn.hide(0);

    //notify user call
    notifyuser($board);

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);

    if (countNext == 0 || countNext == 4 || countNext == 5 || countNext == 6 || countNext == 7 || countNext == 8) {
      $nextBtn.show(0);
    };

    if (countNext == 3) {
      $nextBtn.delay(1000).show(0);
    };

    $(".middlepara").on(animationend, function() {
      $nextBtn.show(0);
    });

    $(".middledef2").on(animationend, function() {
      $nextBtn.show(0);
    });

    $(".colorbox5").on(animationend, function() {
      $nextBtn.show(0);
    });

    $(".clickme1").addClass("cssfadein");

    $(".clickme1").on("click", function() {
      $(".clickme1").hide(0);
      $(".blank, .question, .name1").addClass("cssfadeout");
      $(".metal1").addClass("upup");
    });

    $(".metal1").on(animationend, function() {
      $(".acidch, .plusch, .blankch, .arrowch, .reaction1, .reactionplus, .reaction2_1").addClass("cssfadein");
      $(".salt, .waterinbeaker, .plus2").addClass("cssfadein");
    });

    $(".waterinbeaker").on(animationend, function() {
      $(".metal1info").addClass("cssfadein");
      $(".clickmepot").addClass("cssfadein2");
    });

    $(".clickmepot").on("click", function() {
      $(".metal1").hide(0);
      $(".clickmepot").hide(0);
      $(".metal2").addClass("upup3");
      $(".name2").addClass("cssfadeout");
      $(".acidch, .plusch, .blankch, .arrowch, .reaction1, .reactionplus, .reaction2_1, .plus2, .salt, .waterinbeaker, .metal1info").hide(0);
    });

    $(".metal2").on(animationend, function() {
      $(".NaCl").addClass("cssfadein");
      $(".plusch, .arrowch").delay(900).show(0);
      $(".acidch1, .blankch1, .reaction1_1").addClass("cssfadein");
    });

    $(".reaction1_1").on(animationend, function() {
      $(".metal2info").addClass("cssfadein");
      $(".clickmebi").addClass("cssfadein2");
    });

    $(".clickmebi").on("click", function() {
      $(".metal2").hide(0);
      $(".clickmebi").hide(0);
      $(".metal3").addClass("upup2");
      $(".name3").addClass("cssfadeout");
      $(".acidch1, .plusch, .blankch1, .arrowch, .reaction1_1, .reactionplus, .reaction2_1, .NaCl, .metal2info").hide(0);
    });

    $(".metal3").on(animationend, function() {
      $(".salt").delay(900).show(0);
      $(".waterwithbubble").addClass("cssfadein");
      $(".plusch, .arrowch, .reactionplus, .plus2, .reaction2_1").delay(900).show(0);
      $(".acidch2, .blankch2, .reaction1_2, .reaction2_plussign, .reaction2_carbon").addClass("cssfadein");
    });

    $(".reaction2_carbon").on(animationend, function() {
      $(".metal3info").addClass("cssfadein");
      $nextBtn.delay(2000).show(0);
    });

    $(".parsedstring").one('click', function() {
      $(this).removeAttr('data-usernotification');
      $(".headingstylewidanim").addClass("goupdiv");
      $(".colorbox1").addClass("showfromleft2");
      $(".colorbox2").addClass("showfromright2");
      $(".colorbox3").addClass("showfromleft3");
      $(".colorbox4").addClass("showfromright");
      $(".colorbox5").addClass("metalanim");
    }).attr("data-usernotification", "notifyuser");


  }


  /*====================================
  =            svg template            =
  ====================================*/
  function svgtemplate() {
    var source = $("#svg-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $nextBtn.hide(0);

    //notify user call
    notifyuser($board);

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);

    var $svgwrapper = $board.children('div.svglayout').children('div.svgwrapper');
    var $svgcontainer = $svgwrapper.children('div.svgcontainer');
    var $imagecharacteristicsbox = $svgwrapper.children('div.imagecharacteristicsbox');

    if ($svgcontainer.length > 0) {
      var s = Snap(".svgcontainer");
      Snap.load(imgpath + "acidwithhand.svg", function(f) {
        s.append(f);
        // snap objects
        var acidline = s.select("#acidline");
        var aciddrop = s.select("#aciddrop");
        var pouringbeaker = s.select("#pouringbeaker");
        var acidbeaker = s.select("#acidbeaker");

        // jquery objects and js variables
        var $svg = $svgcontainer.find('svg');
        var $acidline = $svg.find('#acidline');
        var $aciddrop = $svg.find('#aciddrop');
        var $pouringbeaker = $svg.find('#pouringbeaker');
        var $acidbeaker = $svg.find('#acidbeaker');

        $(".clickmeacid").on('click', function() {
          $(this).hide(0);
          $acidbeaker.hide(0);
          pouringbeaker.addClass("showanim");
          aciddrop.addClass("godown");
          acidline.addClass("metalanim");
          $imagecharacteristicsbox.css("top", "90%");
          $nextBtn.delay(4000).show(0);
        });
      });
    }
  }


  /*=====  End of svg template  ======*/


  /*=====  End of Templates Block  ======*/

  /*==================================================
  =            Templates Controller Block            =
  ==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
      Motivation :
      - Make a single function call that handles all the
        template load easier

      How To:
      - Update the template caller with the required templates
      - Call template caller

      What it does:
      - According to value of the Global Variable countNext
          the slide templates are updated
   */

  function templateCaller() {
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    navigationcontroller();

    // if footerNotificationHandler pageEndSetNotification was called then
    // countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;

    // On the basis of countNext value call templates
    switch (countNext) {
      case 0:
        generaltemplate();
        break;
      case 1:
        generaltemplate();
        break;
      case 2:
        generaltemplate();
        break;
      case 3:
        generaltemplate();
        break;
      case 4:
        generaltemplate();
        break;
      case 5:
        generaltemplate();
        break;
      case 6:
        generaltemplate();
        break;
      case 7:
        generaltemplate();
        break;
      case 8:
        generaltemplate();
        break;
      case 9:
        generaltemplate();
        break;
      case 10:
        svgtemplate();
        break;
      case 11:
        generaltemplate();
        break;
      case 12:
        generaltemplate();
        break;
      case 13:
        generaltemplate();
        break;
      case 14:
        generaltemplate();
        break;
      case 15:
        generaltemplate();
        break;
      default:
        break;
    }

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page, countNext + 1);
  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/
  // countNext+=1;

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */

  $nextBtn.on('click', function() {
    countNext++;
    templateCaller();
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on('click', function() {
    countNext--;
    templateCaller();
  });

  /*=====  End of Templates Controller Block  ======*/
});

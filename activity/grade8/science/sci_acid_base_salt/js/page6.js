var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
var imgpath = $ref+"/images/page6/";

var content=[
	{
		typeoflayout   : "svglayout",

		uppertextblock: [
			{
				textclass: "headingstyle",
				textdata: data.string.p1_title3,
			}
		],
		svgblock : [
			{
				svgheading : [
					{
						datahighlightflag: true,
						textclass: "svgtitle2 slowthetxt1",
						textdata: data.string.testpara3,
					},
					{
						datahighlightflag: true,
						textclass: "svgtitle3 slowthetxt1",
						textdata: data.string.testpara3_1,
					},
					{
						datahighlightflag: true,
						textclass: "svgtitle4 slowthetxt3",
						textdata: data.string.testpara3_3,
					},
					
					{
						svgname:"svgcontainer1"
					}
				]		
			},
		]
	},
	{
		typeoflayout   : "svglayout",

		uppertextblock: [
			{
				textclass: "headingstyle",
				textdata: data.string.p1_title3,
			},
			{
				textclass:"lemonpara",
				textdata:data.string.testpara4_2 
			},
			{
				textclass:"shampoopara",
				textdata:data.string.testpara4_3 
			}
		],
		svgblock : [
			{
				svgheading : [
					{
						textclass: "svgtitle2",
						textdata: data.string.testpara4,
					},
					{
						svgname:"svgcontainer1"
					}
				],
						
				imagecharacterstics : [
					{
						imagecharacteristicheadingdata  : data.string.testpara4_1,
					},	
				]	
			},
		],
	},
	{
		typeoflayout   : "svglayout",
		uppertextblock:[
			{
				textclass: "svgtitle2 slowthetxt1",
				textdata: data.string.p6_title3
			},
			
		],

		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"phchart",
						imgsrc: imgpath+"Universal-indicator-and-the-pH-scale.png"
					}
				],
				imagelabels:[
					{
						imagelabelclass:"l1 lablestyle  imageanim",
						imagelabeldata: data.string.p6_lable1,
					},
					{
						imagelabelclass:"l2 lablestyle  imageanim",
						imagelabeldata: data.string.p6_lable2,
					},
					{
						imagelabelclass:"l3 lablestyle  imageanim",
						imagelabeldata: data.string.p6_lable3,
					},
					{
						imagelabelclass:"l4 lablestyle  imageanim",
						imagelabeldata: data.string.p6_lable4,
					},
					{
						imagelabelclass:"l5 lablestyle  imageanim",
						imagelabeldata: data.string.p6_lable5,
					},
					{
						imagelabelclass:"l6 lablestyle  imageanim",
						imagelabeldata: data.string.p6_lable6,
					},
					{
						imagelabelclass:"l7 lablestyle  imageanim",
						imagelabeldata: data.string.p6_lable7,
					},
					{
						imagelabelclass:"l8 lablestyle  imageanim",
						imagelabeldata: data.string.p6_lable8,
					},
					{
						imagelabelclass:"l9 lablestyle  imageanim",
						imagelabeldata: data.string.p6_lable9,
					},
					{
						imagelabelclass:"l10 lablestyle  imageanim",
						imagelabeldata: data.string.p6_lable10,
					},
					{
						imagelabelclass:"l11 lablestyle  imageanim",
						imagelabeldata: data.string.p6_lable11,
					},
					{
						imagelabelclass:"l12 lablestyle  imageanim",
						imagelabeldata: data.string.p6_lable12,
					},
					{
						imagelabelclass:"l13 lablestyle  imageanim",
						imagelabeldata: data.string.p6_lable13,
					},
					{
						imagelabelclass:"l14 lablestyle  imageanim",
						imagelabeldata: data.string.p6_lable14,
					},

				]
			}
		]
	}
];


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);


	/*==================================================
=            Handlers and helpers Block            =
==================================================*/
	
	// register the handlebar partials first
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());

	 /*===============================================
		=            data highlight function            =
		===============================================*/
		/**

			What it does:
			- send an element where the function has to see
			for data to highlight
			- this function searches for all text nodes whose
			data-highlight element is set to true 
			-searches for # character and gives a start tag
			;span tag here, also for @ character and replaces with
			end tag of the respective
			
			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var replaceinstring;
			var texthighlightstarttag = "<span class='parsedstring'>";
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					// console.log(val);
					replaceinstring = $(this).html();
					replaceinstring = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		=            user notification function            =
		===============================================*/
		/**		
			How to:
			- First set any html element with "data-usernotification='notifyuser'" attribute
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser 
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

		/*==========  navigation controller function  ==========*/	 
	 function navigationcontroller(){
	 	if(countNext == 0){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }
	
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/

		/*=================================================
		=            svg template function            =
		=================================================*/		
	function svgtemplate(){
		var source   = $("#svg-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		// alert(countNext);

		//notify user call
		notifyuser($board);
		$nextBtn.delay(5500).fadeIn(100);
		var $svgwrapper  = $board.children('div.svglayout').children('div.svgwrapper');
		var $svgcontainer1  = $svgwrapper.children('div.svgcontainer1');
		var length= $svgwrapper.length;
		
		// if there is a svg content
		if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer1");
			Snap.load(imgpath+"acid.svg", function (f) {    
    		s.append(f);

    			// snap objects
    		var lemon_acid       = s.select("#lemon_acid");
    		var water_neutral    = s.select("#water_neutral");
    		var shampo_base      = s.select("#shampo_base");
    		var numberzero     = s.select("#number0");
    			
    			// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');
    		var $lemon_acid     = $svg.children("[id$='lemon_acid']");
    		var $lemon_acid = $svg.children('#lemon_acid');
    		var $water_neutral     = $svg.children("[id$='water_neutral']");
    		var $water = $svg.children('#water_neutral');
    		var $shampo_base     = $svg.children("[id$='shampo_base']");
    		var $shampo_base = $svg.children('#shampo_base');


		});
	}

}

function svgtemplate2(){
		var source   = $("#svg-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		// alert(countNext);

		//notify user call
		notifyuser($board);

		var $svgwrapper              = $board.children('div.svglayout').children('div.svgwrapper');
		var $imagecharacteristicsbox = $svgwrapper.children('div.imagecharacteristicsbox');
		var $svgcontainer1            = $svgwrapper.children('div.svgcontainer1');
		var length= $svgwrapper.length;
		
		// if there is a svg content
		if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer1");
			Snap.load(imgpath+"acid01.svg", function (f) {    
    		s.append(f);

    			// snap objects
    		var lemon_acid       = s.select("#lemon_acid");
    		var water_neutral    = s.select("#water_neutral");
    		var shampo_base      = s.select("#shampo_base");
    		var numberzero     = s.select("#number0");
    			
    			// jquery objects and js variables
    		var $svg      = $svgcontainer1.find('svg');
    		// alert($svg.length);
    		var $lemon_acid     = $svg.find("[id$='lemon_acid']");
    		var $lemon_acid = $svg.find('#lemon_acid');
 
    		var $water_neutral = $svg.find('#water_neutral');
    		var $shampo_base     = $svg.find("[id$='shampo_base']");
    		var $shampo_base = $svg.find('#shampo_base');

    		water_neutral.addClass("wateranimate");
       		$water_neutral.on(animationend, function() {
    			$imagecharacteristicsbox.css('top', '80%');
    		});

    		$imagecharacteristicsbox.css("top","100%");

    		lemon_acid.addClass("lemonanimate");
    		$lemon_acid.on(animationend, function() { 
    			$(".lemonpara").addClass("cssfadein");
    		});

    		shampo_base.addClass("bottleanimate");
    		$shampo_base.on(animationend, function() { 
    			$(".shampoopara").addClass("cssfadein");
    			$nextBtn.delay(2000).fadeIn(0);
    		});
		});
	}

}
	
function svgtemplate3(){
		var source   = $("#svg-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		// alert(countNext);

		//notify user call
		notifyuser($board);
	
		var $svgwrapper  = $board.children('div.svglayout').children('div.svgwrapper');
		var $svgcontainer1  = $svgwrapper.children('div.svgcontainer1');
		var length= $svgwrapper.length;
		
		// if there is a svg content
		if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer1");
			Snap.load(imgpath+"acid.svg", function (f) {    
    		s.append(f);

    			// snap objects
    		var lemon_acid       = s.select("#lemon_acid");
    		var water_neutral    = s.select("#water_neutral");
    		var shampo_base      = s.select("#shampo_base");
    		var numberzero     = s.select("#number0");
    			
    			// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');
    		var $lemon_acid     = $svg.children("[id$='lemon_acid']");
    		var $lemon_acid = $svg.children('#lemon_acid');
    		var $water_neutral     = $svg.children("[id$='water_neutral']");
    		var $water = $svg.children('#water_neutral');
    		var $shampo_base     = $svg.children("[id$='shampo_base']");
    		var $shampo_base = $svg.children('#shampo_base');


		});
	}

}
/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */
	
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// if footerNotificationHandler pageEndSetNotification was called then
		// countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
		
		 navigationcontroller();
		// On the basis of countNext value call templates
		switch(countNext){
			case 0: svgtemplate(); 
					 break;
			case 1: svgtemplate2();
					 break;
			case 2: svgtemplate3();
					break;
			default:break;
		}

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);	
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();
	});

/*=====  End of Templates Controller Block  ======*/
});
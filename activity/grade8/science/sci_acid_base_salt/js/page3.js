var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
var imgpath = $ref+"/images/page3/";

var content=[
	{
		typeoflayout   : "generallayout",

		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"timetothink",
						imgsrc: "images/lokharke/2.png"
					}
				],
				imagelabels:[
					{
						imagelabelclass: "timeToThink_figdata",
						imagelabeldata: data.string.p1_5,
					}
				]
			}
		],
	},
	{
		typeoflayout   : "generallayout",
		uppertextblock: [
			{
				textclass: "headingstyle colorchange",
				textdata: data.string.p1_5,
			}
		],
		lowertextblock:[
			{
				textclass: "speechbubblediv",
				textdata: data.string.p3_1_1,
			},
			{
				textclass: "speechbubblediv2",
				textdata: data.string.p3_1,
			}
		],
	},
	{
		typeoflayout   : "generallayout",

		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "threeimgtogether",
						imgsrc: imgpath+"Litmus.png",
					},
				],
				imagelabels:[
					{
						imagelabelclass: "lable1",
						imagelabeldata: data.string.p3_lable1,
					}
				]
			},
			{
				imagestoshow:[
					{
						imgclass: "threeimgtogether",
						imgsrc: imgpath+"turmeric.png",
					},
				],
				imagelabels:[
					{
						imagelabelclass: "lable2",
						imagelabeldata: data.string.p3_lable2,

					}
				]
			},
			{
				imagestoshow:[
					{
						imgclass: "threeimgtogether",
						imgsrc: imgpath+"China-rose.png",
					}
				],
				imagelabels:[
					{
						imagelabelclass: "lable3",
						imagelabeldata: data.string.p3_lable3,

					}
				]
			}
		],
		lowertextblock:[
			{
				textclass: "centertext  godowndiv",
				textdata: data.string.p3_3,
			}
		]
	},
	{
		typeoflayout   : "generallayout",

		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "threeimgtogether ",
						imgsrc: imgpath+"Litmus.png",
					},
				],
				imagelabels:[
					{
						imagelabelclass: "lable1",
						imagelabeldata: data.string.p3_lable1,
					}
				]
			},
			{
				imagestoshow:[
					{
						imgclass: "threeimgtogether",
						imgsrc: imgpath+"turmeric.png",
					},
				],
				imagelabels:[
					{
						imagelabelclass: "lable2",
						imagelabeldata: data.string.p3_lable2,

					}
				]
			},
			{
				imagestoshow:[
					{
						imgclass: "threeimgtogether",
						imgsrc: imgpath+"China-rose.png",
					}
				],
				imagelabels:[
					{
						imagelabelclass: "lable3",
						imagelabeldata: data.string.p3_lable3,

					}
				]
			}
		],
		lowertextblock:[
			{
				textclass: "centertext2",
				textdata: data.string.p3_4,
			}
		],
		lowerimageblock:[
			{
				imagestoshow:[
					{
						imgclass: "twoimgtogether",
						imgsrc: imgpath+"methyl-orange.png",
					},
					{
						imgclass: "twoimgtogether",
						imgsrc: imgpath+"phenolphthalein.png",
					}
				]
			}
		]
	},
	{
		typeoflayout   : "generallayout",
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"simpleimgstyle",
						imgsrc: imgpath+"Litmus.png"
					}
				],
				imagelabels:[
					{
						imagelabelclass: "bottomsimpletextstyle",
						imagelabeldata: data.string.p3_lable4,
					}
				]
			}
		]

	},
	{
		typeoflayout   : "generallayout",

		uppertextblock: [
			{
				textclass: "headingstyle",
				textdata: data.string.p3_lable1,
			}
		],
		notifytextblock: [
			{
				datahighlightflag:true,
				textdata: data.string.p3_5,
			}
		],
		hiddenimageblock:[
			{
				imagestoshow:[
					{
						imgclass: "twolitmus",
						imgsrc: imgpath+"Natural-lichen01.png",
					},
					{
						imgclass: "twolitmus",
						imgsrc: imgpath+"Natural-lichen02.png",
					}
				]
			}
		],
		lowertextblock:[
			{
				textclass:"lowertext",
				textdata: data.string.p3_6,
			}
		]
	},
	{
		typeoflayout:"svglayout",

		uppertextblock:[
			{
				textclass: "svgheadingstyle",
				textdata: data.string.testheading,
			},
			{
				textclass: "svgquestion",
				textdata: data.string.testpara1,
			}
		],

		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "threeimgtogethersvg",
						imgsrc: imgpath+"beaker.png",
					},
				],
				imagelabels:[
					{
						imagelabelclass: "acid_beaker",
						imagelabeldata: data.string.p1_2,

					}
				]
			},
			{
				imagestoshow:[
					{
						imgclass: "threeimgtogethersvg",
						imgsrc: imgpath+"beakerblue.png"
					},
				],
				imagelabels:[
					{
						imagelabelclass: "neutral_beaker",
						imagelabeldata: data.string.p1_2_22,

					}
				]
			},
			{
				imagestoshow:[
					{
						imgclass: "threeimgtogethersvg",
						imgsrc: imgpath+"beakergrey.png"
					}
				],
				imagelabels:[
					{
						imagelabelclass: "base_beaker",
						imagelabeldata: data.string.p1_3,

					}
				]
			}
		]
	},

	{
		typeoflayout:"svglayout",

		svgblock : [
			{
				svgheading : [
					{
						datahighlightflag : true,
						textclass         : "svgheadingstyle",
						textdata          : data.string.testheading,
					},
					{
						textclass : "svgquestion",
						textdata: data.string.testpara2,
					},
					{
						textclass: "bubble",
						textdata: data.string.p2_9
					},
					{
						textclass: "popupdiv2",
						textdata: data.string.p2_9_1
					},
					{
						textclass: "a_beaker",
						textdata : data.string.p1_2,
					},
					{
						textclass: "n_beaker",
						textdata : data.string.p1_2_22
					},
					{
						textclass: "b_beaker",
						textdata : data.string.p1_3
					},
					{
						svgname: "svgacidbeaker"
					},
					{
						svgname: "svgneutralbeaker"
					},
					{
						svgname: "svgbasebeaker"
					}
				]
			},
		],

	},
	{
		typeoflayout:"svglayout",

		svgblock : [
			{
				svgheading : [
					{
						datahighlightflag : true,
						textclass         : "svgheadingstyle",
						textdata          : data.string.testheading,
					},
					{
						textclass : "svgquestion",
						textdata: data.string.testpara2_1,
					},
					{
						textclass: "bubble2",
						textdata: data.string.p2_8
					},
					{
						textclass: "popupdiv3",
						textdata: data.string.p2_9_2
					},
					{
						textclass: "a_beaker",
						textdata : data.string.p1_2,
					},
					{
						textclass: "n_beaker",
						textdata : data.string.p1_2_22
					},
					{
						textclass: "b_beaker",
						textdata : data.string.p1_3
					},
					{
						svgname: "svgacidbeaker"
					},
					{
						svgname: "svgneutralbeaker"
					},
					{
						svgname: "svgbasebeaker"
					}
				]
			},
		],

	},
	{
		typeoflayout:"svglayout",
		uppertextblock:[
			{
				textclass: "headingstyle",
				textdata: data.string.p2_12,
			},
		],
		contentnocenteradjust:true,
		middletextblock:[
			{
				datahighlightflag: true,
				textclass: "middlepara1",
				textdata : data.string.p2_7,
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sidesingleimg",
						imgsrc: imgpath+"10.png",
					}
				]
			}
		]
	},
	{
		typeoflayout:"svglayout",
		uppertextblock:[
			{
				textclass: "headingstyle",
				textdata: data.string.p2_12,
			},
		],
		contentnocenteradjust:true,
		middletextblock:[
			{
				datahighlightflag: true,
				textclass: "middlepara1",
				textdata : data.string.p2_7,
			},
			{
				datahighlightflag: true,
				textclass: "middlepara2",
				textdata : data.string.p2_7_1,
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sidesingleimg",
						imgsrc: imgpath+"09.png",
					},
				]
			}
		]
	},
	{
		typeoflayout:"svglayout",
		uppertextblock:[
			{
				textclass: "headingstyle",
				textdata: data.string.p2_12,
			},
		],
		contentnocenteradjust:true,
		middletextblock:[
			{
				datahighlightflag: true,
				textclass: "middlepara1",
				textdata : data.string.p2_7,
			},
			{
				datahighlightflag: true,
				textclass: "middlepara2",
				textdata : data.string.p2_7_1,
			},
			{
				datahighlightflag: true,
				textclass: "middlepara3",
				textdata : data.string.p2_7_2,
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sidesingleimg",
						imgsrc: imgpath+"beakerblue.png",
					},
					{
						imgclass:"litmusinthebeaker",
						imgsrc: imgpath+"paper.png"
					}
				]
			}
		]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/

	// register the handlebar partials first
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());

	 /*===============================================
		=            data highlight function            =
		===============================================*/
		/**

			What it does:
			- send an element where the function has to see
			for data to highlight
			- this function searches for all text nodes whose
			data-highlight element is set to true
			-searches for # character and gives a start tag
			;span tag here, also for @ character and replaces with
			end tag of the respective

			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var replaceinstring;
			var texthighlightstarttag = "<span class='parsedstring'>";
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					// console.log(val);
					replaceinstring = $(this).html();
					replaceinstring = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		=            user notification function            =
		===============================================*/
		/**
			How to:
			- First set any html element with "data-usernotification='notifyuser'" attribute
			- Then call this function to give notification
		 */

		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			/*variable that will store the element(s) to remove notification from*/
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

		/*==========  navigation controller function  ==========*/
	 function navigationcontroller(){
	 	if(countNext == 0){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/

	/*=================================================
	=            svg template function            =
	=================================================*/
	function svgtemplate(){
		var source   = $("#svg-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		// alert(countNext);

		//notify user call
		notifyuser($board);
		if (countNext==0) {
			$nextBtn.show(0);
		};

		if (countNext==1) {
			$(".speechbubblediv").addClass("divupanimation");
			$(".speechbubblediv2").addClass("divupanimation3");
			$nextBtn.delay(3000).fadeIn(100);
		};

		if (countNext==2) {
			$nextBtn.delay(2000).fadeIn(100);
		};

		if (countNext==3 || countNext==4 ||countNext==6) {
			$nextBtn.show(0);
		};

		$(".parsedstring").click (function(){
			$(".hiddenimageblock, .lowertext").show(0);
			$nextBtn.show(0);
			$(this).attr("data-usernotification","");
		}).css("cursor","pointer")
		.attr("data-usernotification","notifyuser");

		if (countNext==8|| countNext==9 || countNext==10) {
			$nextBtn.show(0);
		};
}

	function svgtemplate1(){
		var source   = $("#svg-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		// alert(countNext);
		$(".litmuspaperred1, .litmuspaperred2, .litmuspaperred3").hide(0);

		//notify user call
		notifyuser($board);

		var countlitmus=0;

		var $svgwrapper              = $board.children('div.svglayout').children('div.svgwrapper');
		var $svgcontainer            = $svgwrapper.children('div.svgcontainer');
		var $svgacidbeaker            = $svgwrapper.children('div.svgacidbeaker');
		var $svgneutralbeaker            = $svgwrapper.children('div.svgneutralbeaker');
		var $svgbasebeaker            = $svgwrapper.children('div.svgbasebeaker');
		var length= $svgwrapper.length;
		// alert(length);


		// if there is a svg content
		if($svgwrapper.length > 0){
			var s = Snap(".svgacidbeaker");
			var s2 = Snap(".svgneutralbeaker");
			var s3 = Snap(".svgbasebeaker");

			Snap.load(imgpath+"beakeracid.svg", function (f) {
    		s.append(f);
    			// snap objects
    		var bubbles = s.select("#bubbles");

    			// jquery objects and js variables
    		var $svg = $svgcontainer.children('svg');

    		$(".litmuspaperblue1").click (function() {
    			$(".litmuspaperblue1").addClass("litmusmoveanim");

    			$(".litmuspaperblue1").on(animationend, function(){
    				$(".litmuspaperblue1").addClass("colormoveup");
    			});
    			countlitmus++;
    			if (countlitmus ==3) {
    				$(".svgquestion").hide(0);
    				$(".bubble").addClass("divupanimation");
    				$(".popupdiv2").addClass("divupanimation2");
    				$nextBtn.delay(3000).fadeIn(100);

    			};
    		});
 		});

			Snap.load(imgpath+"beakerblue.svg", function (f) {
    		s2.append(f);
    			// snap objects
    		var bubbles = s2.select("#bubbles");

    			// jquery objects and js variables
    		var $svg = $svgcontainer.children('svg');


    		$(".litmuspaperblue2").click (function() {
    			countlitmus++;
    			if (countlitmus ==3) {
    				$(".svgquestion").hide(0);
    				$(".popupdiv2").addClass("divupanimation2");
    				$(".bubble").addClass("divupanimation");
    				$nextBtn.delay(3000).fadeIn(100);

    			};
    			$(this).css("margin-top", "30%");
    		});
 		});

			Snap.load(imgpath+"beakergrey.svg", function (f) {
    		s3.append(f);
    			// snap objects
    		var bubbles = s3.select("#bubbles");

    			// jquery objects and js variables
    		var $svg = $svgcontainer.children('svg');

    		$(".litmuspaperblue3").click (function() {
    			countlitmus++;
    			if (countlitmus ==3) {
    				$(".popupdiv2").addClass("divupanimation2");
    				$(".svgquestion").hide(0);
    				$(".bubble").addClass("divupanimation");
    				$nextBtn.delay(3000).fadeIn(100);

    			};
    			$(this).css("margin-top", "30%");

    		});
 		});
	}

}

function svgtemplate2(){
		var source   = $("#svg-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		// alert(countNext);

		$(".litmuspaperblue1, .litmuspaperblue2, .litmuspaperblue3").hide(0);
		$(".litmuspaperred1, .litmuspaperred2, .litmuspaperred3").show(0);
		//notify user call
		notifyuser($board);
		var countlitmus=0;

		var $svgwrapper              = $board.children('div.svglayout').children('div.svgwrapper');
		var $svgcontainer            = $svgwrapper.children('div.svgcontainer');
		var $svgacidbeaker            = $svgwrapper.children('div.svgacidbeaker');
		var $svgneutralbeaker            = $svgwrapper.children('div.svgneutralbeaker');
		var $svgbasebeaker            = $svgwrapper.children('div.svgbasebeaker');
		var length= $svgwrapper.length;
		// alert(length);


		// if there is a svg content
		if($svgwrapper.length > 0){
			var s = Snap(".svgacidbeaker");
			var s2 = Snap(".svgneutralbeaker");
			var s3 = Snap(".svgbasebeaker");

			Snap.load(imgpath+"beakeracid.svg", function (f) {
    		s.append(f);
    			// snap objects
    		var bubbles = s.select("#bubbles");

    			// jquery objects and js variables
    		var $svg = $svgcontainer.children('svg');

    		$(".litmuspaperred1").click (function() {
    			countlitmus++;
    			if (countlitmus ==3) {
    				$(".svgquestion").hide(0);
    				$(".bubble2").addClass("divupanimation");
    				$(".popupdiv3").addClass("divupanimation2");
    				$nextBtn.delay(3000).fadeIn(100);

    			};
    			$(this).css("margin-top", "30%");

    		});
 		});

			Snap.load(imgpath+"beakerblue.svg", function (f) {
    		s2.append(f);
    			// snap objects
    		var bubbles = s2.select("#bubbles");

    			// jquery objects and js variables
    		var $svg = $svgcontainer.children('svg');


    		$(".litmuspaperred2").click (function() {
    			countlitmus++;
    			if (countlitmus ==3) {
    				$(".svgquestion").hide(0);
    				$(".popupdiv3").addClass("divupanimation2");
    				$(".bubble2").addClass("divupanimation");
    				$nextBtn.delay(3000).fadeIn(100);

    			};
    			$(this).css("margin-top", "30%");

    		});
 		});

			Snap.load(imgpath+"beakergrey.svg", function (f) {
    		s3.append(f);
    			// snap objects
    		var bubbles = s3.select("#bubbles");

    			// jquery objects and js variables
    		var $svg = $svgcontainer.children('svg');

    		$(".litmuspaperred3").click (function() {

    			$(".litmuspaperred3").addClass("litmusmoveanim");

    			$(".litmuspaperred3").on(animationend, function(){
    				$(".litmuspaperred3").addClass("colormoveup2");
    			});
    			countlitmus++;
    			if (countlitmus ==3) {
    				$(".popupdiv3").addClass("divupanimation2");
    				$(".svgquestion").hide(0);
    				$(".bubble2").addClass("divupanimation");
    				$nextBtn.delay(3000).fadeIn(100);

    			};
    		});
 		});
	}

}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		// $prevBtn.css('display', 'none');
		// $nextBtn.css('display', 'none');

		// if footerNotificationHandler pageEndSetNotification was called then
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
		navigationcontroller();
		// On the basis of countNext value call templates
		switch(countNext){
			case 0: svgtemplate();
					 break;
			case 1: svgtemplate();
					 break;
			case 2: svgtemplate();
					 break;
			case 3: svgtemplate();
					 break;
			case 4: svgtemplate();
					 break;
			case 5: svgtemplate();
					 break;
			case 6: svgtemplate();
					 break;
			case 7: svgtemplate1();
					 break;
			case 8: svgtemplate2();
					 break;
			case 9: svgtemplate();
					 break;
			case 10: svgtemplate();
					 break;
			case 11: svgtemplate();
					 break;
			default:break;
		}

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click',function () {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
	});

/*=====  End of Templates Controller Block  ======*/

});

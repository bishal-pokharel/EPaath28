//array of image
var recapImage = [
	"images/recap/recap1.png",
	"images/recap/recap2.png",
	"images/recap/recap3.png",
	"images/recap/recap4.png"
];

var randomImageNumeral = ole.getRandom(1,3,0);
var currentrecapImage = recapImage[randomImageNumeral];

var imgpath = $ref+"/images/page11/";

var content=[
	{	
		typeoflayout: "recaplayout",
		recapblock : [
			{
				recapheadingdata : data.string.recapheadingtext,
				recaphostimgsrc : currentrecapImage,
				tableheading : [
					data.string.raydiagramtableheading1,
					data.string.raydiagramtableheading2,
					data.string.raydiagramtableheading3,
					data.string.raydiagramtableheading4,
				],
				tablecolumn1heading1 : data.string.raydiagramcol1heading1,
				tablerow : [
					{
						tablerowdata : [
							data.string.p11row1to5cell1,
							data.string.p11row1cell2,
							data.string.p11row1cell3,
							data.string.p11row1cell4,
						],
					},
					{
						tablerowdata : [
							data.string.p11row1to5cell1,
							data.string.p11row2cell2,
							data.string.p11row2cell3,
							data.string.p11row2cell4,
						],
					},
					{
						tablerowdata : [
							data.string.p11row1to5cell1,
							data.string.p11row3cell2,
							data.string.p11row3cell3,
							data.string.p11row3cell4,
						],
					},
					{
						tablerowdata : [
							data.string.p11row1to5cell1,
							data.string.p11row4cell2,
							data.string.p11row4cell3,
							data.string.p11row4cell4,
						],
					},
					{
						tablerowdata : [
							data.string.p11row1to5cell1,
							data.string.p11row5cell2,
							data.string.p11row5cell3,
							data.string.p11row5cell4,
						],
					},
					{
						tablerowdata : [
							data.string.p11row6cell1,
							data.string.p11row6cell2,
							data.string.p11row6cell3,
							data.string.p11row6cell4,
						],
					},
				],
			},
		],
	},
];

$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/	
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("recapcontent", $("#recapcontent-partial").html());

	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		/**

			What it does:
			- send an element where the function has to see
			for data to highlight
			- this function searches for all text nodes whose
			data-highlight element is set to true 
			-searches for # character and gives a start tag
			;span tag here, also for @ character and replaces with
			end tag of the respective
			
			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var replaceinstring;
			var texthighlightstarttag = "<span class='parsedstring'>";
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					// console.log(val);
					replaceinstring = $(this).html();
					replaceinstring = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		=            user notification function            =
		===============================================*/
		/**		
			How to:
			- First set any html element with "data-usernotification='notifyuser'" attribute
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser 
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

		/*==========  navigation controller function  ==========*/	 
	 function navigationcontroller(){
	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			ole.footerNotificationHandler.lessonEndSetNotification();
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }
	
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	
	/*----------  recap template  ----------*/
	function recap(){
		var source = $("#recap-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		//text highlighter
		texthighlight($board);
	}
	
/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */
	
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// if footerNotificationHandler pageEndSetNotification was called then
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
		
		// call the template
		recap();

		// navigationcontroller
		navigationcontroller();

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);	
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();
	});

/*=====  End of Templates Controller Block  ======*/
});
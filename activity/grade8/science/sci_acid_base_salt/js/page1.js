var imgpath = $ref+"/images/page1/";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var content=[
	{
		contentnocenteradjust:true,
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass         : "bigheadingtextstyle",
				textdata          : data.string.p1_title1,
			}
		],
		imageblockadditionalclass:"cover_bg",
		imageblock : [
			{
				imagestoshow: [
					{
						imgclass : "cover_image",
						imgsrc : imgpath+"coverpage.png",
					},

				],
				imagelabels:[
					{
						imagelabelclass : "cvr_text",
						imagelabeldata : data.lesson.chapter,
					}
				]
			},
		],

	},
	{
		headerblock : [
			{
				textclass : "headermove",
				textdata   : data.string.p1_1,
			}
		],
		imageblock : [
			{
				imagestoshow: [
					{
						imgclass : "fiveimgtogether",
						imgsrc : imgpath+"acid.png",
					},

				],
				imagelabels:[
					{
						imagelabelclass : "lable1",
						imagelabeldata : data.string.p1_2,
					}
				]
			},
			{
				imagestoshow: [
					{
						imgclass : "fiveimgtogether",
						imgsrc : imgpath+"base.png",
					},

				],
				imagelabels:[
					{
						imagelabelclass : "lable2",
						imagelabeldata : data.string.p1_3,
					}
				]
			},
			{
				imagestoshow: [
					{
						imgclass : "fiveimgtogether",
						imgsrc : imgpath+"salt.png",
					},

				],
				imagelabels:[
					{
						imagelabelclass : "lable3",
						imagelabeldata : data.string.p1_4,
					}
				]
			},
			{
				imagestoshow: [
					{
						imgclass : "fiveimgtogether indicator",
						imgsrc : imgpath+"indicator.png",
					},

				],
				imagelabels:[
					{
						imagelabelclass : "lable4",
						imagelabeldata : data.string.p1_5,
					}
				]
			},
			{
				imagestoshow: [
					{
						imgclass : "fiveimgtogether",
						imgsrc : imgpath+"litmus-paper.png",
					},

				],
				imagelabels:[
					{
						imagelabelclass : "lable5",
						imagelabeldata : data.string.p1_6_1,
					}
				]
			}
		],
	},
	{
		headerblock : [
			{
				textclass : "sidestyle",
				textdata   : data.string.p1_title2,
			},
			{
				datahighlightflag:true,
				textclass: "sidestylemove moveleft",
				textdata: data.string.p1_title2_1,
			}
		],

		imageblock : [

			{
				imagestoshow: [
					{
						imgclass : "sevenimgtogether ",
						imgsrc : imgpath+"food.png",
					},

				],
				imagelabels:[
					{
						imagelabelclass : "lablestyle1",
						imagelabeldata : data.string.p1_l1,
					}
				]
			},
			{
				imagestoshow: [
					{
						imgclass : "sevenimgtogether ",
						imgsrc : imgpath+"fertilizer.png",
					},

				],
				imagelabels:[
					{
						imagelabelclass : "lablestyle2",
						imagelabeldata : data.string.p1_l3,
					}
				]
			},
			{
				imagestoshow: [
					{
						imgclass : "sevenimgtogether ",
						imgsrc : imgpath+"plastic.png",
					},

				],
				imagelabels:[
					{
						imagelabelclass : "lablestyle3",
						imagelabeldata : data.string.p1_l5,
					}
				]
			},
			{
				imagestoshow: [
					{
						imgclass : "sevenimgtogether",
						imgsrc : imgpath+"explosives.png",
					},

				],
				imagelabels:[
					{
						imagelabelclass : "lablestyle4",
						imagelabeldata : data.string.p1_l4,
					}
				]
			},
			{
				imagestoshow: [
					{
						imgclass : "sevenimgtogether",
						imgsrc : imgpath+"paper.png",
					},

				],
				imagelabels:[
					{
						imagelabelclass : "lablestyle5",
						imagelabeldata : data.string.p1_l7,
					}
				]
			},
			{
				imagestoshow: [
					{
						imgclass : "sevenimgtogether",
						imgsrc : imgpath+"soap.png",
					},

				],
				imagelabels:[
					{
						imagelabelclass : "lablestyle6",
						imagelabeldata : data.string.p1_l2,
					}
				]
			},
			{
				imagestoshow: [
					{
						imgclass : "sevenimgtogether",
						imgsrc : imgpath+"paint.png",
					},

				],
				imagelabels:[
					{
						imagelabelclass : "lablestyle7",
						imagelabeldata : data.string.p1_l6,
					}
				]
			},
		],

	},
	{
		uppertextblock:[
			{
				datahighlightflag: true,
				textclass:"middlepara",
				textdata : data.string.p1_4_1,
			}
		]
	},
	{
		headerblock : [
			{
				textclass : "sideheadingstyle",
				textdata   : data.string.p1_2,
			}
		],
		contentnocenteradjust:true,
		headingdata: data.string.p1_2,
			svgblock :[
				{
					svgname:"svgcontainer",
				}
			],
		imageblock:[
			{
				imagestoshow: [
					{
						imgclass : "clickmeacid",
						imgsrc: "images/clickme_icon.png",

					}
				],
			}
		],

		lowertextblock:[
				{
					datahighlightflag: true,
					textclass: "acidtxt",
					textdata: data.string.p1_6,
				}
			],
	},
	{
		headerblock : [
			{
				textclass : "sideheadingstyle",
				textdata   : data.string.p1_2,
			}
		],
		contentnocenteradjust:true,

		imageblock:[
			{
				imagestoshow: [
					{
						imgclass: "threeimgtogether",
						imgsrc: imgpath+"lemon.png",
					},
				],
				imagelabels:[
					{
						imagelabelclass:"lemontext",
						imagelabeldata: data.string.p1_7_1
					}
				]
			},

			{
				imagestoshow:[
					{
						imgclass: "threeimgtogether",
						imgsrc: imgpath+"dairy-product.png",
					},
				],
				imagelabels:[
					{
						imagelabelclass: "milktext",
						imagelabeldata: data.string.p1_7_2
					}
				]
			},
			{
				imagestoshow:[
					{
						imgclass: "threeimgtogether",
						imgsrc: imgpath+"orange.png",
					}

				],
				imagelabels:[
					{
						imagelabelclass: "orangetext",
						imagelabeldata: data.string.p1_7_3
					}
				]
			}
		],

		lowertextblock:[
			{
				datahighlightflag: true,
				textclass:"threeimgpara",
				textdata: data.string.p1_7,
			}
		],

	},
	{
		headerblock : [
			{
				textclass : "sideheadingstyle",
				textdata   : data.string.p1_2,
			}
		],
		contentnocenteradjust:true,
		middletextblock:[
			{
				datahighlightflag: true,
				textclass: "middlepara1",
				textdata : data.string.p1_8,
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sidesingleimg cssfadein",
						imgsrc: imgpath+"dairy-product.png",
					}
				]
			}
		]
	},
	{
		headerblock : [
			{
				textclass : "sideheadingstyle",
				textdata   : data.string.p1_2,
			}
		],
		contentnocenteradjust:true,
		middletextblock:[
			{
				datahighlightflag: true,
				textclass: "middlepara1",
				textdata : data.string.p1_8,
			},
			{
				datahighlightflag: true,
				textclass: "middlepara2",
				textdata : data.string.p1_9,
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sidesingleimg cssfadein",
						imgsrc: imgpath+"lemon.png",
					}
				]
			}
		]
	},
	{
		headerblock : [
			{
				textclass : "sideheadingstyle",
				textdata   : data.string.p1_2,
			}
		],
		contentnocenteradjust:true,
		middletextblock:[
			{
				datahighlightflag: true,
				textclass: "middlepara1",
				textdata : data.string.p1_8,
			},
			{
				datahighlightflag: true,
				textclass: "middlepara2",
				textdata : data.string.p1_9,
			},
			{
				datahighlightflag: true,
				textclass: "middlepara3",
				textdata : data.string.p1_10,
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sidesingleimg cssfadein",
						imgsrc: imgpath+"guava-and-kiwi.png",
					}
				]
			}
		]
	},
	{
		uppertextblock:[
			{
				textclass:"middlepara",
				textdata: data.string.p1_middletitle,
			}
		]
	},
	{
		headerblock : [
			{
				textclass : "sideheadingstyle",
				textdata   : data.string.p1_3,
			}
		],
		contentnocenteradjust:true,
		// headingdata: data.string.p1_2,
			svgblock :[
				{
					svgname: "svgcontainer"
				}
			],
		imageblock:[
			{
				imagestoshow: [
					{
						imgclass : "clickmebase",
						imgsrc: "images/clickme_icon.png",

					}
				],
			}
		],
		lowertextblock:[
				{
					datahighlightflag: true,
					textclass: "acidtxt",
					textdata: data.string.p1_11,
				}
			],
	},
	{
		headerblock : [
			{
				datahighlightflag: true,
				textclass : "sideheadingstyle",
				textdata : data.string.p1_3,
			},
		],
		contentnocenteradjust:true,
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "bitter",
						imgsrc: imgpath+"testing.png",
					}
				]
			}
		],


		middletextblock:[
				{
					textclass:"bottomtxt",
					textdata: data.string.p1_14,

				}
			],
	},
	{
		headerblock : [
			{
				datahighlightflag: true,
				textclass : "sideheadingstyle",
				textdata : data.string.p1_3,
			},
		],

		contentnocenteradjust:true,
		headingdata: data.string.p1_2,
			svgblock :[
				{
					svgname: "soapsvg"
				}
			],
		middletextblock: [
			{
				datahighlightflag:true,
				textclass : "middlepara1",
				textdata: data.string.p1_13,
			},
			{
				datahighlightflag: true,
				textclass: "slidetext divslide",
				textdata: data.string.p1_13_1
			}

		],

	},
	{
		headerblock : [
			{
				datahighlightflag: true,
				textclass : "sideheadingstyle",
				textdata : data.string.p1_3,
			},
		],

		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sidesingleimg cssfadein",
						imgsrc: imgpath+"baking-soda.png",
					}
				]
			}
		],
		contentnocenteradjust:true,

		middletextblock:[
			{
				datahighlightflag:true,
				textclass : "middlepara1",
				textdata: data.string.p1_13,
			},
			{
				datahighlightflag: true,
				textclass: "middlepara2",
				textdata: data.string.p1_15
			}
		]
	},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("cardcontent", $("#cardcontent-partial").html());
	 Handlebars.registerPartial("usageslowrevealcontent", $("#usageslowrevealcontent-partial").html());
	 Handlebars.registerPartial("timetothinkcontent", $("#timetothinkcontent-partial").html());
	 Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());
	 Handlebars.registerPartial("textlistcontent", $("#textlistcontent-partial").html());
	 Handlebars.registerPartial("usagecontent", $("#usagecontent-partial").html());
	 Handlebars.registerPartial("propertycontent", $("#propertycontent-partial").html());
	 Handlebars.registerPartial("propertysummarycontent", $("#propertysummarycontent-partial").html());


		 /*===============================================
		 =            data highlight function            =
		 ===============================================*/
			/**

				What it does:
				- send an element where the function has to see
				for data to highlight
				- this function searches for all nodes whose
				data-highlight element is set to true
				-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
				- if provided with data-highlightcustomclass value for highlight it
				  applies the custom class or else uses parsedstring class

				E.g: caller : texthighlight($board);
			 */
			function texthighlight($highlightinside){
				//check if $highlightinside is provided
				typeof $highlightinside !== "object" ?
				alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
				null ;

				var $alltextpara = $highlightinside.find("*[data-highlight='true']");
				var stylerulename;
				var replaceinstring;
				var texthighlightstarttag;
				var texthighlightendtag   = "</span>";
				if($alltextpara.length > 0){
					$.each($alltextpara, function(index, val) {
						/*if there is a data-highlightcustomclass attribute defined for the text element
						use that or else use default 'parsedstring'*/
						$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
							(stylerulename = $(this).attr("data-highlightcustomclass")) :
							(stylerulename = "parsedstring") ;

						texthighlightstarttag = "<span class='"+stylerulename+"'>";
						replaceinstring       = $(this).html();
						replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
						replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
						$(this).html(replaceinstring);
					});
				}
			}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**
			How to:
			- First set any html element with
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification
		 */

		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	 /*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/
	 /**
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that
	 		footernotification is called for continue button to navigate to exercise
	  */

	/**
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is
	   */

	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	 /*=====  End of user navigation controller function  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	/*=================================================
	=            general template function            =
	=================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);


		if (countNext==0 || countNext==3 || countNext==9) {
			$nextBtn.show(0);
		};
		if (countNext==1) {
			$(".fiveimgtogether").addClass("poppop");
			$(".lable1, .lable2, .lable3, .lable4, .lable5").delay(2000).fadeIn(200);
			$nextBtn.delay(2500).fadeIn(200);
		};

		$(".sidestylemove").on(animationend, function(){
			$nextBtn.show(0);
		});

		if (countNext == 5) {
			$(".threeimgpara").addClass("cssfadein");
			$nextBtn.delay(1000).show(0);
		}

		if (countNext ==11) {
			$(".acidtxt").show(0);
			$nextBtn.show(0);
		};

		$(".sidesingleimg").on(animationend, function(){
			if (countNext==13) {
				$nextBtn.hide(0);
			}
			else{
				$nextBtn.show(0);
			}
		});

		// find if there is linehorizontal div in the slide
		// var $linehorizontal = $board.find("div.linehorizontal");
		// if($linehorizontal.length > 0)
		// {
		// 	$linehorizontal.attr('data-isdrawn', 'draw');
		// }
	}


	/*=================================================
	=            svg template function            =
	=================================================*/
	function svgtemplate(){
		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer');
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");
			Snap.load(imgpath+"aciddropping-01.svg", function (f) {
    		s.append(f);

    	  		// snap objects
    		var acidcontainer    = s.select("#acidcontainer");
    		var acid_drop        = s.select("#acid_drop");
    		var hydrogen_ion     = s.select("#hydrogen_ion");
    		var hydrogen_ion2     = s.select("#hydrogen_ion2");
    		var hydrogen_ion3     = s.select("#hydrogen_ion3");
    		var water_bubble     = s.select("#water_bubble");

    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');
    		var $acid_drop = $svg.children('#acid_drop');
    		var $acidcontainer = $svg.children('#acidcontainer');
    		var $hydrogen_ion = $svg.children('#hydrogen_ion');
    		var $hydrogen_ion2 = $svg.children('#hydrogen_ion2');
    		var $hydrogen_ion3 = $svg.children('#hydrogen_ion3');
    		var $hydrogen_ion4 = $svg.children('#hydrogen_ion4');
    		var $hydrogen_ion5 = $svg.children('#hydrogen_ion5');
    		var $water_bubble = $svg.children('#water_bubble');

    			$(".clickmeacid").click(function () {
    				$(this).hide(0);
    				// alert("hahahah");
    				acid_drop.addClass("down");
  					hydrogen_ion.addClass("ion_move");
  					hydrogen_ion2.addClass("ion_move");
  					hydrogen_ion3.addClass("ion_move");
  					// hydrogen_ion4.addClass("ion_move");
  					// hydrogen_ion5.addClass("ion_move");

  					water_bubble.addClass("H2O");
  					$(".acidtxt").delay(2000).fadeIn(1000);
  					$nextBtn.delay(2500).fadeIn(1000);
    			});
 		});
	// }
}

function svgtemplate1(){
		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer');

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");
			Snap.load(imgpath+"basedropping.svg", function (f) {
    		s.append(f);

    	  		// snap objects
    		var acidcontainer    = s.select("#acidcontainer");
    		var basedrop        = s.select("#basedrop");
    		var oh_ion1     = s.select("#oh_ion1");
    		var oh_ion2     = s.select("#oh_ion2");
    		var oh_ion3     = s.select("#oh_ion3");
    		var oh_ion4     = s.select("#oh_ion4");
    		var water_bubble     = s.select("#water_bubble");

    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');

    		var $drop     = $svg.children("[id$='drop']");
    		var $basedrop = $svg.children('#basedrop');

    		var $ion = $svg.children("[id$='ion']");
    		var $oh_ion1 = $svg.children('#oh_ion1');
    		var $oh_ion2 = $svg.children('#oh_ion2');
    		var $oh_ion3 = $svg.children('#oh_ion3');
    		var $oh_ion4 = $svg.children('#oh_ion4');
    		var $water = $svg.children("[id$='water']");
    		var $water_bubble = $svg.children('#water_bubble');

    			$(".clickmebase").click(function () {
    				$(this).hide(0);
    				basedrop.addClass("down");
  					oh_ion1.addClass("ion_move");
  					oh_ion2.addClass("ion_move");
  					oh_ion3.addClass("ion_move");
  					oh_ion4.addClass("ion_move");

  					water_bubble.addClass("H2O");
  					$(".acidtxt").delay(2000).fadeIn(1000);
  					$nextBtn.delay(2500).fadeIn(1000);
    			});
 		});
	// }
}


function svgtemplate2(){
		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $soapsvg = $board.children('div.soapsvg');

		$nextBtn.delay(1800).fadeIn(1000);
		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".soapsvg");
			Snap.load(imgpath+"soap.svg", function (f) {
    		s.append(f);

    	  		// snap objects
    		var soapbody = s.select("#soapbody");
    		var soap_bubbles = s.select("#soap_bubbles");

    		// jquery objects and js variables
    		var $svg      = $soapsvg.children('soapsvg');

    		var $soap     = $svg.children("[id$='soap']");
    		var $soapbody = $svg.children('#soapbody');

    		var $bubble = $svg.children("[id$='bubble']");
    		var $soap_bubbles = $svg.children('#soap_bubbles');
 		});
	// }
}


/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template


		/*OR, call templates like this if you have more than one template
		to call*/
		switch(countNext){
			case 0 : generaltemplate(); break;
			case 1 : generaltemplate(); break;
			case 2 : generaltemplate(); break;
			case 3 : generaltemplate(); break;
			case 4 : svgtemplate(); break;
			case 5 : generaltemplate(); break;
			case 6 : generaltemplate(); break;
			case 7 : generaltemplate(); break;
			case 8 : generaltemplate(); break;
			case 9 : generaltemplate(); break;
			case 10 : svgtemplate1(); break;
			case 11 : generaltemplate(); break;
			case 12 : svgtemplate2(); break;
			case 13 : generaltemplate(); break;
			case 14 : generaltemplate(); break;
			default : break;
		}

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose to see total slide vs current slide number
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click',function () {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

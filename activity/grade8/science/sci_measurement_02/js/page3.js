var imgpath = $ref+"/images/page3/";

var content=[
	{	
		uppertextblock : [
			{
				textclass : "textmovetop",
				para  : data.string.p3_1,
			},
			{
				textclass: "textmoveleft",
				textdata : data.string.p3_1_1,
			}
		],
	},
	{
		uppertextblock:[
			{
				textclass: "fundamentaldefinition",
				textdata : data.string.p2_1_def,
			}
		]
		
	},
	{
		uppertextblock : [
			{
				textclass: "bignumber",
				textdata: data.string.p3_3_1,
			}
		],
		middletextblock : [
			{
				datahighlightflag : "true",
				textclass : "bottomtext",
				textdata  : data.string.p3_3,
			}
		],	
	},

	{	
		uppertextblock : [
			{
				textclass : "bigheadingtextstyle",
				textdata  : data.string.p3_1,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "length",
						imgsrc   : imgpath+"length.png",
					}
				],
			},			
		],
		
		table : [
			{
				table_class: "highlight_first",
				table_data : data.string.p3_5_1,
			},
			{
				table_data : data.string.common1,
			},
			{
				table_class : "highlight_word",
				table_data : data.string.p3_5_3,				
			},
			{
				table_data : data.string.common2,
			},
			{
				table_class : "highlight_word",
				table_data : data.string.p3_5_2,
				
			}
		],
	},

	{	
		uppertextblock : [
			{
				textclass : "bigheadingtextstyle",
				textdata  : data.string.p3_1,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "mass",
						imgsrc   : imgpath+"mass.png",
					}
				],
			},			
		],
		table : [
			{
				table_class : "highlight_first",
				table_data : data.string.p3_6_1,
			},
			{
				table_data : data.string.common1,
			},
			{
				table_class : "highlight_word",
				table_data : data.string.p3_6_2,			
			},
			{
				table_data : data.string.common2,
			},
			{
				table_class : "highlight_word",
				table_data : data.string.p3_6_3,				
			}
		],
	},

	{	
		uppertextblock : [
			{
				textclass : "bigheadingtextstyle",
				textdata  : data.string.p3_1,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "clock",
						imgsrc   : imgpath+"Time.png",
					}
				],
			},			
		],
		table : [
			{
				table_class : "highlight_first",
				table_data : data.string.p3_7_1,
			},
			{
				table_data : data.string.common1,
			},
			{
				table_class : "highlight_word",
				table_data : data.string.p3_7_2,			
			},
			{
				table_data : data.string.common2,
			},
			{
				table_class : "highlight_word",
				table_data : data.string.p3_7_3,				
			}
		],
	},

	{	
		uppertextblock : [
			{
				textclass : "bigheadingtextstyle",
				textdata  : data.string.p3_1,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "temp",
						imgsrc   : imgpath+"temperature.png",
					}
				],
			},			
		],
		table : [
			{
				table_class : "highlight_first",
				table_data : data.string.p3_8_1,
			},
			{
				table_data : data.string.common1,
			},
			{
				table_class : "highlight_word",
				table_data : data.string.p3_8_2,			
			},
			{
				table_data : data.string.common2,
			},
			{
				table_class : "highlight_word",
				table_data : data.string.p3_8_3,				
			}
		],
	},

	{	
		uppertextblock : [
			{
				textclass : "bigheadingtextstyle",
				textdata  : data.string.p3_1,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "current",
						imgsrc   : imgpath+"electric-current.png",
					}
				]
			},			
		],
		table : [
			{
				table_class : "highlight_first",
				table_data : data.string.p3_9_1,
			},
			{
				table_data : data.string.common1,
			},
			{
				table_class : "highlight_word",
				table_data : data.string.p3_9_2,			
			},
			{
				table_data : data.string.common2,
			},
			{
				table_class : "highlight_word",
				table_data : data.string.p3_9_3,				
			}
		],
	},

	{	
		uppertextblock : [
			{
				textclass : "bigheadingtextstyle",
				textdata  : data.string.p3_1,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "sub",
						imgsrc   : imgpath+"Amount_of_Substance.png",
					}
				]
			},			
		],
		table : [
			{
				table_class : "highlight_first",
				table_data : data.string.p3_10_1,
			},
			{
				table_data : data.string.common1,
			},
			{
				table_class : "highlight_word",
				table_data : data.string.p3_10_2,			
			},
			{
				table_data : data.string.common2,
			},
			{
				table_class : "highlight_word",
				table_data : data.string.p3_10_3,				
			}
		],
	},
	
	{	
		uppertextblock : [
			{
				textclass : "bigheadingtextstyle",
				textdata  : data.string.p3_1,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "candle",
						imgsrc   : imgpath+"Candela.png",
					}
				],
			},			
		],
		table : [
			{
				table_class : "highlight_first",
				table_data : data.string.p3_11_1,
			},
			{
				table_data : data.string.common1,
			},
			{
				table_class : "highlight_word",
				table_data : data.string.p3_11_2,			
			},
			{
				table_data : data.string.common2,
			},
			{
				table_class : "highlight_word",
				table_data : data.string.p3_11_3,				
			}
		],
	},
	
];


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);


	// register the handlebar partials first
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("tablecontent", $("#tablecontent-partial").html());

	 /*===============================================
		=            data highlight function            =
		===============================================*/
		/**

			What it does:
			- send an element where the function has to see
			for data to highlight
			- this function searches for all text nodes whose
			data-highlight element is set to true 
			-searches for # character and gives a start tag
			;span tag here, also for @ character and replaces with
			end tag of the respective
			
			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside,highlightclassname){
			var highlightclassvar;
			highlightclassname === "undefined" ? highlightclassvar = "parsedstring" : highlightclassvar = highlightclassname;
			
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var replaceinstring;
			var texthighlightstarttag = "<span class='"+highlightclassvar+"'>";
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					// console.log(val);
					replaceinstring = $(this).html();
					replaceinstring = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		=            user notification function            =
		===============================================*/
		/**		
			How to:
			- First set any html element with "data-usernotification='notifyuser'" attribute
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser 
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

		/*==========  navigation controller function  ==========*/	 
	 function navigationcontroller(){
	 	if(countNext == 0){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }

		/*=================================================
		=            general template function            =
		=================================================*/		
	function general() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
				
		// call navigation controller
		navigationcontroller();

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);


		$(document).ready(function(){ 
			$(".textmovetop").css("left","30%").delay(5000, function() { 
				$(".textmoveleft").css("right","33%");
			});
			
		});
		
		// find if there is linehorizontal div in the slide
		var $linehorizontal = $board.find("div.linehorizontal");
		if($linehorizontal.length > 0)
		{
			$linehorizontal.attr('data-isdrawn', 'draw');
		}
	}

	/* this is for development and testing purpose only, please comment it out
	during deployment */
	// countNext+=15;
	general();

	$nextBtn.on('click',function () {
		$(this).css('display', 'none');
		countNext++;
		// alert(countNext);		
		general();
		loadTimelineProgress($total_page,countNext+1);
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css('display', 'none');
		countNext--;	
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;	
		general();
		loadTimelineProgress($total_page,countNext+1);
	});
});
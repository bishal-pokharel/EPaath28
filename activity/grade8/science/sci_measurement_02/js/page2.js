var imgpath = $ref+"/images/page2/";

var content=[
	{
		imgsrc : "./images/lokharke/definition2.png",
		cap :"cap1", 
		imgcap : data.string.p2_1,	

		arrowimg :[
			{arrname :"arr1", arrimg : $ref+"/images/page2/arrow.png"},
			{arrname :"arr2", arrimg : $ref+"/images/page2/arrow2.png"},
		],	

		imagecaption :[
			{cap :"cap2", imgcaption : data.string.p2_2},
			{cap :"cap3", imgcaption : data.string.p2_3},
		],
	},
			
		
]


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	 // navigation controller function
	 // function navigationcontroller(){
	 // 	if(countNext == 0){
		// 	$nextBtn.show(0);
		// 	$prevBtn.css('display', 'none');
		// }
		// else if(countNext > 0 && countNext < $total_page-1){
		// 	$nextBtn.show(0);
		// 	$prevBtn.show(0);
		// }
		// else if(countNext == $total_page-1){
		// 	$nextBtn.css('display', 'none');
		// 	$prevBtn.show(0);
		// 	ole.footerNotificationHandler.pageEndSetNotification();
		// }
	 // }

/*
* measureintro
*/
	function measureintro() {
		var source = $("#measureintro-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$("#arr1").delay(500).show(80);
		$("#arr2").delay(800).show(80);
		$("#cap2").delay(600).show(80);
		$("#cap3").delay(900).show(80);
		ole.footerNotificationHandler.pageEndSetNotification();
		// call navigation controller
		// navigationcontroller();
	}

	measureintro(0);

	// $nextBtn.on('click',function () {
	// 	$(this).css('display', 'none');
	// 	countNext++;		
	// 	measureintro();
	// 	loadTimelineProgress($total_page,countNext+1);
	// });

	// $refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
	// 	$(this).css('display', 'none');
	// 	countNext--;	
	// 	countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;	
	// 	measureintro();
	// 	loadTimelineProgress($total_page,countNext+1);
	// });
});
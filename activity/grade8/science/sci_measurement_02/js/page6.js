var imgpath = $ref+"/images/page6/";
var squirrelears = "images/lokharke/important_content_animated.svg";
var squirrelwhat = "images/lokharke/squirrel_what_animated.svg";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var content=[
	{	
		headerblock : [
			{
				textclass : "headertextstyle", /*specific for the header text*/
				textdata   : data.string.p7_heading,
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "squirrelears",
						imgsrc: squirrelwhat
					}
				]
			}
		]
	},
	{
   		contentnocenteradjust:true,

   		headerblock:[
   			{
   				textclass : "headertextstyle", /*specific for the header text*/
				textdata   : data.string.p7_para1,
   			},
   			{
   				datahighlightflag: true,
   				textclass:"secondline",
   				textdata: data.string.p7_para2
   			},
   			{
   				textclass: "thirdline",
   				textdata: data.string.p7_para3
   			},
   			
   		],


   		imageblock:[
   			{
   				imagestoshow:[
   					{
   						imgclass: "dietcoke",
   						imgsrc: imgpath+"dietcoke.png"
   					},
   					{
   						imgclass: "regucoke",
   						imgsrc: imgpath+"regular-coke.png"
   					},
   					{
   						imgclass:"clickmediet",
   						imgsrc: "images/clickme_icon.png",
   					},
   					{
   						imgclass:"clickmeregu",
   						imgsrc: "images/clickme_icon.png",
   					}
   				]
   			}
   		],

   		lowertextblock:[
   			{
   				textclass: "waterlevel cssfadein",
   			},
   			{
   				textclass: "waterwater cssfadein",   				
   			},
   			{
   				textclass: "waterlevel2 cssfadein",
   			},
   			{
   				textclass: "waterwaterwater cssfadein", 			

   			}
   		],
	},

	{
   		contentnocenteradjust:true,

   		headerblock:[
   			
   			{
   				datahighlightflag:true,
   				textclass: "fourthline cssfadein",
   				textdata: data.string.p7_para4
   			},
   			{
   				datahighlightflag:true,
   				textclass: "fifthline cssfadein3",
   				textdata: data.string.p7_para7
   			}
   		],
   		imageblock:[
   			{
   				imagestoshow:[
   					{
   						imgclass: "container1 cssfadein2",
   						imgsrc: imgpath+"dietcoke01.png"
   					},
   				],
				imagelabel:[
					{
						imagelabelclass:"img1text",
						imagelabeldata: data.string.p7_para5
					}
				]
   			}, 
   			{
   				imagestoshow:[
   					{
   						imgclass: "container2 cssfadein2",
   						imgsrc: imgpath+"regular-coke01.png"
   					}
   				],
   				imagelabel:[
					{
						imagelabelclass:"img2text",
						imagelabeldata: data.string.p7_para5
					}
				]
			}   				
   		]
	},
	{
		contentnocenteradjust:true,

   		headerblock:[
   			{
   				datahighlightflag:true,
   				textclass: "headertextstyle",
   				textdata: data.string.p7_heading2
   			}
   		],
   		textlistblock:[
   			{
   				listtype:[
   					{
   						listtypedata: data.string.p7_para8
   					},
   					{
   						listtypedata: data.string.p7_para9
   					},
   					{
   						listtypedata: data.string.p7_para10
   					}
   				]
   			}
   		]
	},
	{
		contentnocenteradjust:true,
		headerblock:[
			{
   				textclass: "headertextstyle",
   				textdata: data.string.p6_heading
			}
		],
		uppertextblock:[
			{
				textclass: "singlepara cssfadein",
				textdata: data.string.p7_para11
			},
            {
                textclass:"sidepara cssfadein2",
                textdata: data.string.p7_para13
            }
		],

		slidecontrolledanimationblock : [
			{
				animationimgsrc : imgpath+"box01.png",
				minimagecount : 1,
				maximagecount  : 4,
			}
		],

		lowertextblock:[
            {
                textclass:"low",
                textdata: data.string.p7_para14
            },
            {
                textclass:"high",
                textdata: data.string.p7_para15
            },
			{
				textclass:"lowerpara",
				textdata: data.string.p7_para12
			}
		]
	}
	
];


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("timetothinkcontent", $("#timetothinkcontent-partial").html());
	 Handlebars.registerPartial("textlistcontent", $("#textlistcontent-partial").html());
	 Handlebars.registerPartial("slidecontrolledanimationcontent", $("#slidecontrolledanimationcontent-partial").html());
	 
		 /*===============================================
		 =            data highlight function            =
		 ===============================================*/
			/**

				What it does:
				- send an element where the function has to see
				for data to highlight
				- this function searches for all nodes whose
				data-highlight element is set to true 
				-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
				- if provided with data-highlightcustomclass value for highlight it
				  applies the custom class or else uses parsedstring class
				
				E.g: caller : texthighlight($board);
			 */
			function texthighlight($highlightinside){
				//check if $highlightinside is provided
				typeof $highlightinside !== "object" ?
				alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
				null ;
					
				var $alltextpara = $highlightinside.find("*[data-highlight='true']");
				var stylerulename;
				var replaceinstring;
				var texthighlightstarttag;
				var texthighlightendtag   = "</span>";
				if($alltextpara.length > 0){
					$.each($alltextpara, function(index, val) {	
						/*if there is a data-highlightcustomclass attribute defined for the text element
						use that or else use default 'parsedstring'*/				
						$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
							(stylerulename = $(this).attr("data-highlightcustomclass")) :
							(stylerulename = "parsedstring") ;

						texthighlightstarttag = "<span class='"+stylerulename+"'>";
						replaceinstring       = $(this).html();
						replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
						replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
						$(this).html(replaceinstring);
					});
				}
			}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**		
			How to:
			- First set any html element with 
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	 /*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/	 	 
	 /**	 
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that 
	 		footernotification is called for continue button to navigate to exercise
	  */
	
	/**	  
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if 
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is 
	   */  
	 
	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			
			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;			
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	 /*=====  End of user navigation controller function  ======*/

	 /*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**		
			How to:
			- Just call instructionblockcontroller() from the template		
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);					
				});
			}
		}	
	/*=====  End of InstructionBlockController  ======*/
	
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	/*=================================================
	=            general template function            =
	=================================================*/		
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);

		if (countNext==0||countNext==3) {
			$nextBtn.show(0);
		};
				
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//call instruction block controller
		// instructionblockcontroller($board);

		//call notifyuser
		// notifyuser($anydiv);

		$(".waterwaterwater").on(animationend, function(){ 
			$(".headertextstyle").delay(1000).hide(0);
			$(".secondline").addClass("cssfadein");
		});

		$(".secondline").on(animationend, function(){ 
			$(".dietcoke, .regucoke, .clickmeregu, .clickmediet").addClass("cssfadein");
		});

		var count=0;
		$(".clickmediet").on("click",function(){ 
			$(this).hide(0);
			count++;
			$(".dietcoke").addClass("slidedown");
			$(".waterwater").addClass("waterslidedown");
			if (count==2) {
				$(".secondline").delay(1000).hide(0);
				$(".thirdline").addClass("cssfadein2");
                $nextBtn.delay(2000).show(0);

			};
		});

		$(".clickmeregu").on("click",function(){ 
			count++;
			$(this).hide(0);
			$(".regucoke").addClass("slideagaindown");
			$(".waterwaterwater").addClass("waterslideagaindown");
			if (count==2) {
				$(".secondline").delay(1000).hide(0);
				$(".thirdline").addClass("cssfadein2");
				$nextBtn.delay(2000).show(0);
			};
		});

		$(".fifthline").on(animationend, function(){ 
			$nextBtn.delay(1000).show(0);
		});
	}

	function slidecontrolledanimation() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		var $slidecontrolledanimationblock = $board.find('div.slidecontrolledanimationblock');
			// if there is a svg content
			if($slidecontrolledanimationblock.length > 0){
				var $animimage = $slidecontrolledanimationblock.find("img.slidecontrolledimgs");
				var $rangeslider = $slidecontrolledanimationblock.find("input[type='range']");
				var changeimgsrc;
				$rangeslider.on('input',  function() {
					changeimgsrc = imgpath+"box0"+$(this).val()+".png";
					$animimage.attr('src', changeimgsrc);
					$(".lowerpara").addClass("cssfadein2")
				});
			}
	}
/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */
	
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		// call navigation controller
		navigationcontroller();

		// call the template
		// generaltemplate();

		/*OR, call templates like this if you have more than one template
		to call*/
		switch(countNext){
			case 0 : generaltemplate(); break;
			case 1 : generaltemplate(); break;
			case 2 : generaltemplate(); break;
			case 3 : generaltemplate(); break;
			case 4 : slidecontrolledanimation(); break;
			default : break;
		}

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose to see total slide vs current slide number
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
			previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});
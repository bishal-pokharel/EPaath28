var imgpath = $ref + "/images/page4/";
var clickicon = "images/clickme_icon.png";

var content = [{
    typeoflayout: "layouthorizontal",
    uppertextblock: [{
        textclass: "bigheadingtextstyle",
        textdata: data.string.p4_1,
      },
      {
        highlighttext: true,
        textclass: "mediumheadingtextstyle",
        textdata: data.string.p4_2,
      }
    ],
  },
  {
    typeoflayout: "layoutlayersoil",
    uppertextblock: [{
        textclass: "topheadingstyle",
        textdata: data.string.p4_3,
      },

    ],

    middletextblock: [{
        textclass: "unit1",
        textdata: data.string.p4_4,
      },
      {
        textclass: "unit2",
        textdata: data.string.p4_5,
      },
      {
        textclass: "unit3",
        textdata: data.string.p4_6,
      },
      {
        textclass: "unit4",
        textdata: data.string.p4_7,
      },
      {
        textclass: "unit5",
        textdata: data.string.p4_8,
      },
      {
        textclass: "unit6",
        textdata: data.string.p4_9,
      },
      {
        textclass: "unit7",
        textdata: data.string.p4_10,
      }
    ],

    lowertextblock: [{
      textclass: "lowertext1",
      textdata: data.string.p4_11,
    }],
  },

  {
    typeoflayout: "layoutlayersoil",
    uppertextblock: [{
        textclass: "sideheadingstyle",
        textdata: data.string.p4_4,
      },
      {
        textclass: "sidepara1",
        textdata: data.string.p4_12,
      }
    ],

  },

  {
    typeoflayout: "layoutlayersoil",
    uppertextblock: [{
        textclass: "sideheadingstyle",
        textdata: data.string.p4_4,
      },

    ],
    middletextblock: [{
      datahighlightflag: true,
      textclass: "sidepara1",
      textdata: data.string.p4_13,
    }],

    lowerimageblock: [{
      imagecontentclass: "lowerimagediv",
      imagestoshow: [{
          imgclass: "candle_small",
          imgsrc: imgpath + "candela.png",
        },
        {
          imgclass: "sub_small",
          imgsrc: imgpath + "Amount_of_Substance.png",
        },
        {
          imgclass: "current_small",
          imgsrc: imgpath + "electric-current.png",
        },
        {
          imgclass: "temp_small",
          imgsrc: imgpath + "temperature.png",
        },
        {
          imgclass: "mass_small",
          imgsrc: imgpath + "mass.png",
        },
        {
          imgclass: "length_small",
          imgsrc: imgpath + "length.png",
        },
        {
          imgclass: "clock_small",
          imgsrc: imgpath + "time.png",
        },
      ],
    }],
  },

  {
    typeoflayout: "layoutlayersoil",
    uppertextblock: [{
      textclass: "sideheadingstyle",
      textdata: data.string.p4_4,
    }],

    middletextblock: [{
        datahighlightflag: true,
        textclass: "sidepara1",
        textdata: data.string.p4_13,
      },
      {
        datahighlightflag: true,
        textclass: "sidepara2",
        textdata: data.string.p4_13_1,
      },


    ],
    imageblock: [{
        imagecontentclass: "windowblock",
        imagestoshow: [{
          imgclass: "window",
          imgsrc: imgpath + "window01.png",
        }],
      },

    ],
    lowerimageblock: [{
      imagecontentclass: "lowerimagediv",
      imagestoshow: [{
          imgclass: "candle_small",
          imgsrc: imgpath + "candela.png",
        },
        {
          imgclass: "sub_small",
          imgsrc: imgpath + "Amount_of_Substance.png",
        },
        {
          imgclass: "current_small",
          imgsrc: imgpath + "electric-current.png",
        },
        {
          imgclass: "temp_small",
          imgsrc: imgpath + "temperature.png",
        },
        {
          imgclass: "mass_small",
          imgsrc: imgpath + "mass.png",
        },
        {
          imgclass: "length_small",
          imgsrc: imgpath + "length.png",
        },
        {
          imgclass: "clock_small",
          imgsrc: imgpath + "time.png",
        },
      ],
    }],

  },

  {
    typeoflayout: "layoutlayersoil",
    uppertextblock: [{
      textclass: "sideheadingstyle",
      textdata: data.string.p4_4,
    }],

    middletextblock: [{
        datahighlightflag: true,
        textclass: "sidepara1",
        textdata: data.string.p4_13,
      },
      {
        datahighlightflag: true,
        textclass: "sidepara2",
        textdata: data.string.p4_13_1,
      },
      {
        textclass: "sidepara3",
        textdata: data.string.p4_14,
      },

    ],
    imageblock: [{
      imagecontentclass: "windowblock",
      imagestoshow: [{
        imgclass: "window",
        imgsrc: imgpath + "window02.png",
      }],
    }, ],
    lowerimageblock: [{
      imagecontentclass: "lowerimagediv",
      imagestoshow: [{
          imgclass: "candle_small",
          imgsrc: imgpath + "candela.png",
        },
        {
          imgclass: "sub_small",
          imgsrc: imgpath + "Amount_of_Substance.png",
        },
        {
          imgclass: "current_small",
          imgsrc: imgpath + "electric-current.png",
        },
        {
          imgclass: "temp_small",
          imgsrc: imgpath + "temperature.png",
        },
        {
          imgclass: "mass_small",
          imgsrc: imgpath + "mass.png",
        },
        {
          imgclass: "length_small",
          imgsrc: imgpath + "length.png",
        },
        {
          imgclass: "clock_small",
          imgsrc: imgpath + "time.png",
        },
      ],
    }],

  },
  {
    typeoflayout: "layoutlayersoil",
    uppertextblock: [{
        textclass: "sideheadingstyle",
        textdata: data.string.p4_4,
      },

    ],
    middletextblock: [{
        datahighlightflag: true,
        textclass: "sidepara1",
        textdata: data.string.p4_13,
      },
      {
        datahighlightflag: true,
        textclass: "sidepara2",
        textdata: data.string.p4_13_1,
      },
      {
        textclass: "sidepara3",
        textdata: data.string.p4_14,
      },
      {
        textclass: "sidepara4",
        textdata: data.string.p4_15,
      },

    ],

    imageblock: [{
      imagecontentclass: "windowblock",
      imagestoshow: [{
        imgclass: "window",
        imgsrc: imgpath + "window04.png",
      }],
    }, ],
    lowerimageblock: [{
      imagecontentclass: "lowerimagediv",
      imagestoshow: [{
          imgclass: "candle_blur",
          imgsrc: imgpath + "candela.png",
        },
        {
          imgclass: "sub_blur",
          imgsrc: imgpath + "Amount_of_Substance.png",
        },
        {
          imgclass: "current_blur",
          imgsrc: imgpath + "electric-current.png",
        },
        {
          imgclass: "temp_blur",
          imgsrc: imgpath + "temperature.png",
        },
        {
          imgclass: "mass_blur",
          imgsrc: imgpath + "mass.png",
        },
        {
          imgclass: "length_highlight",
          imgsrc: imgpath + "length.png",
        },
        {
          imgclass: "clock_blur",
          imgsrc: imgpath + "time.png",
        },
      ],
    }],


  },
  {
    typeoflayout: "layoutlayersoil",
    uppertextblock: [{
      textclass: "sideheadingstyle",
      textdata: data.string.p4_4,
    }],
    middletextblock: [{
        datahighlightflag: true,
        textclass: "sidepara1",
        textdata: data.string.p4_13,
      },

      {
        textclass: "sidepara2",
        textdata: data.string.p4_13_1,
      },
      {
        textclass: "sidepara3",
        textdata: data.string.p4_14,
      },
      {
        datahighlightflag: true,
        textclass: "sidepara4",
        textdata: data.string.p4_16,
      }
    ],
    imageblock: [{
      imagecontentclass: "windowblock",
      imagestoshow: [{
        imgclass: "window",
        imgsrc: imgpath + "window03.png",
      }],
    }, ],
    lowerimageblock: [{
      imagecontentclass: "lowerimagediv",
      imagestoshow: [{
          imgclass: "candle_blur",
          imgsrc: imgpath + "candela.png",
        },
        {
          imgclass: "sub_blur",
          imgsrc: imgpath + "Amount_of_Substance.png",
        },
        {
          imgclass: "current_blur",
          imgsrc: imgpath + "electric-current.png",
        },
        {
          imgclass: "temp_blur",
          imgsrc: imgpath + "temperature.png",
        },
        {
          imgclass: "mass_blur",
          imgsrc: imgpath + "mass.png",
        },
        {
          imgclass: "length_highlight",
          imgsrc: imgpath + "length.png",
        },
        {
          imgclass: "clock_blur",
          imgsrc: imgpath + "time.png",
        },
      ],
    }],
  },
  {
    typeoflayout: "layoutlayersoil",
    uppertextblock: [{
      textclass: "sideheadingstyle",
      textdata: data.string.p4_4,
    }],
    middletextblock: [{
        datahighlightflag: true,
        textclass: "sidepara1",
        textdata: data.string.p4_13,
      },

      {
        textclass: "sidepara2",
        textdata: data.string.p4_13_1,
      },
      {
        textclass: "sidepara3",
        textdata: data.string.p4_14,
      },
      {
        datahighlightflag: true,
        textclass: "sidepara4",
        textdata: data.string.p4_16,
      },
      {
        datahighlightflag: true,
        textclass: "sidepara5",
        textdata: data.string.p4_17,
      }
    ],
    imageblock: [{
      imagecontentclass: "windowblock",
      imagestoshow: [{
        imgclass: "window",
        imgsrc: imgpath + "window03.png",
      }],
    }, ],
    lowerimageblock: [{
      imagecontentclass: "lowerimagediv",
      imagestoshow: [{
          imgclass: "candle_blur",
          imgsrc: imgpath + "candela.png",
        },
        {
          imgclass: "sub_blur",
          imgsrc: imgpath + "Amount_of_Substance.png",
        },
        {
          imgclass: "current_blur",
          imgsrc: imgpath + "electric-current.png",
        },
        {
          imgclass: "temp_blur",
          imgsrc: imgpath + "temperature.png",
        },
        {
          imgclass: "mass_blur",
          imgsrc: imgpath + "mass.png",
        },
        {
          imgclass: "length_highlight",
          imgsrc: imgpath + "length.png",
        },
        {
          imgclass: "clock_blur",
          imgsrc: imgpath + "time.png",
        },
      ],
    }],
  },
  {
    typeoflayout: "layoutlayersoil",
    uppertextblock: [{
      textclass: "sideheadingstyle",
      textdata: data.string.p4_4,
    }],
    middletextblock: [{
        datahighlightflag: true,
        textclass: "sidepara1",
        textdata: data.string.p4_13,
      },

      {
        textclass: "sidepara2",
        textdata: data.string.p4_13_1,
      },
      {
        textclass: "sidepara3",
        textdata: data.string.p4_14,
      },
      {
        datahighlightflag: true,
        textclass: "sidepara4",
        textdata: data.string.p4_16,
      },
      {
        datahighlightflag: true,
        textclass: "sidepara5",
        textdata: data.string.p4_17,
      },

    ],
    lowertextblock: [{
      datahighlightflag: true,
      textclass: "lowertextstyle",
      textdata: data.string.p4_18,
    }],
    imageblock: [{
      imagecontentclass: "windowblock",
      imagestoshow: [{
        imgclass: "window",
        imgsrc: imgpath + "length.png",
      }],
    }, ],
  },
  {
    typeoflayout: "layoutlayersoil",
    uppertextblock: [{
        textclass: "sideheadingstyle",
        textdata: data.string.p4_5,
      },
      {
        textclass: "sidepara1",
        textdata: data.string.p4_19,
      }
    ],

  },


  {
    typeoflayout: "layoutlayersoil",
    uppertextblock: [{
        textclass: "sideheadingstyle",
        textdata: data.string.p4_5,
      },

    ],
    middletextblock: [{
      datahighlightflag: true,
      textclass: "sidepara1",
      textdata: data.string.p4_20,
    }],

    imageblock: [{
      imagecontentclass: "windowblock",
      imagestoshow: [{
        imgclass: "box",
        imgsrc: imgpath + "box.png",
      }],
    }, ],
    lowerimageblock: [{
      imagecontentclass: "lowerimagediv",
      imagestoshow: [{
          imgclass: "candle_small",
          imgsrc: imgpath + "candela.png",
        },
        {
          imgclass: "sub_small",
          imgsrc: imgpath + "Amount_of_Substance.png",
        },
        {
          imgclass: "current_small",
          imgsrc: imgpath + "electric-current.png",
        },
        {
          imgclass: "temp_small",
          imgsrc: imgpath + "temperature.png",
        },
        {
          imgclass: "mass_small",
          imgsrc: imgpath + "mass.png",
        },
        {
          imgclass: "length_small",
          imgsrc: imgpath + "length.png",
        },
        {
          imgclass: "clock_small",
          imgsrc: imgpath + "time.png",
        },
      ],
    }],
  },

  {
    typeoflayout: "layoutlayersoil",
    uppertextblock: [{
        textclass: "sideheadingstyle",
        textdata: data.string.p4_5,
      },

    ],
    middletextblock: [{
        datahighlightflag: true,
        textclass: "sidepara1",
        textdata: data.string.p4_20,
      },
      {
        datahighlightflag: true,
        textclass: "sidepara2",
        textdata: data.string.p4_21,
      }
    ],

    imageblock: [{
      imagecontentclass: "windowblock",
      imagestoshow: [{
        imgclass: "box",
        imgsrc: imgpath + "box01.png",
      }],
    }, ],
    lowerimageblock: [{
      imagecontentclass: "lowerimagediv",
      imagestoshow: [{
          imgclass: "candle_small",
          imgsrc: imgpath + "candela.png",
        },
        {
          imgclass: "sub_small",
          imgsrc: imgpath + "Amount_of_Substance.png",
        },
        {
          imgclass: "current_small",
          imgsrc: imgpath + "electric-current.png",
        },
        {
          imgclass: "temp_small",
          imgsrc: imgpath + "temperature.png",
        },
        {
          imgclass: "mass_small",
          imgsrc: imgpath + "mass.png",
        },
        {
          imgclass: "length_small",
          imgsrc: imgpath + "length.png",
        },
        {
          imgclass: "clock_small",
          imgsrc: imgpath + "time.png",
        },
      ],
    }],
  },

  {
    typeoflayout: "layoutlayersoil",
    uppertextblock: [{
        textclass: "sideheadingstyle",
        textdata: data.string.p4_5,
      },

    ],
    middletextblock: [{
        datahighlightflag: true,
        textclass: "sidepara1",
        textdata: data.string.p4_20,
      },
      {
        datahighlightflag: true,
        textclass: "sidepara2",
        textdata: data.string.p4_21,
      },
      {
        datahighlightflag: true,
        textclass: "sidepara6",
        textdata: data.string.p4_22,
      },
    ],

    imageblock: [{
      imagecontentclass: "windowblock",
      imagestoshow: [{
        imgclass: "box",
        imgsrc: imgpath + "box02.png",
      }],
    }, ],
    lowerimageblock: [{
      imagecontentclass: "lowerimagediv",
      imagestoshow: [{
          imgclass: "candle_blur",
          imgsrc: imgpath + "candela.png",
        },
        {
          imgclass: "sub_blur",
          imgsrc: imgpath + "Amount_of_Substance.png",
        },
        {
          imgclass: "current_blur",
          imgsrc: imgpath + "electric-current.png",
        },
        {
          imgclass: "temp_blur",
          imgsrc: imgpath + "temperature.png",
        },
        {
          imgclass: "mass_blur",
          imgsrc: imgpath + "mass.png",
        },
        {
          imgclass: "length_highlight",
          imgsrc: imgpath + "length.png",
        },
        {
          imgclass: "clock_blur",
          imgsrc: imgpath + "time.png",
        },
      ],
    }],
  },

  {
    typeoflayout: "layoutlayersoil",
    uppertextblock: [{
        textclass: "sideheadingstyle",
        textdata: data.string.p4_5,
      },

    ],
    middletextblock: [{
        datahighlightflag: true,
        textclass: "sidepara1",
        textdata: data.string.p4_20,
      },
      {
        datahighlightflag: true,
        textclass: "sidepara2",
        textdata: data.string.p4_21,
      },
      {
        datahighlightflag: true,
        textclass: "sidepara6",
        textdata: data.string.p4_22_1,
      },
    ],

    imageblock: [{
      imagecontentclass: "windowblock",
      imagestoshow: [{
        imgclass: "box",
        imgsrc: imgpath + "box04.png",
      }],
    }, ],
    lowerimageblock: [{
      imagecontentclass: "lowerimagediv",
      imagestoshow: [{
          imgclass: "candle_blur",
          imgsrc: imgpath + "candela.png",
        },
        {
          imgclass: "sub_blur",
          imgsrc: imgpath + "Amount_of_Substance.png",
        },
        {
          imgclass: "current_blur",
          imgsrc: imgpath + "electric-current.png",
        },
        {
          imgclass: "temp_blur",
          imgsrc: imgpath + "temperature.png",
        },
        {
          imgclass: "mass_blur",
          imgsrc: imgpath + "mass.png",
        },
        {
          imgclass: "length_highlight",
          imgsrc: imgpath + "length.png",
        },
        {
          imgclass: "clock_blur",
          imgsrc: imgpath + "time.png",
        },
      ],
    }],
  },

  {
    typeoflayout: "layoutlayersoil",
    uppertextblock: [{
        textclass: "sideheadingstyle",
        textdata: data.string.p4_5,
      },

    ],
    middletextblock: [{
        datahighlightflag: true,
        textclass: "sidepara1",
        textdata: data.string.p4_20,
      },
      {
        datahighlightflag: true,
        textclass: "sidepara2",
        textdata: data.string.p4_21,
      },
      {
        datahighlightflag: true,
        textclass: "sidepara6",
        textdata: data.string.p4_22_2,
      },
    ],

    imageblock: [{
      imagecontentclass: "windowblock",
      imagestoshow: [{
        imgclass: "box",
        imgsrc: imgpath + "box03.png",
      }],
    }, ],
    lowerimageblock: [{
      imagecontentclass: "lowerimagediv",
      imagestoshow: [{
          imgclass: "candle_blur",
          imgsrc: imgpath + "candela.png",
        },
        {
          imgclass: "sub_blur",
          imgsrc: imgpath + "Amount_of_Substance.png",
        },
        {
          imgclass: "current_blur",
          imgsrc: imgpath + "electric-current.png",
        },
        {
          imgclass: "temp_blur",
          imgsrc: imgpath + "temperature.png",
        },
        {
          imgclass: "mass_blur",
          imgsrc: imgpath + "mass.png",
        },
        {
          imgclass: "length_highlight",
          imgsrc: imgpath + "length.png",
        },
        {
          imgclass: "clock_blur",
          imgsrc: imgpath + "time.png",
        },
      ],
    }],
  },

  {
    typeoflayout: "layoutlayersoil",
    uppertextblock: [{
        textclass: "sideheadingstyle",
        textdata: data.string.p4_5,
      },

    ],
    middletextblock: [{
        datahighlightflag: true,
        textclass: "sidepara1",
        textdata: data.string.p4_20,
      },
      {
        datahighlightflag: true,
        textclass: "sidepara2",
        textdata: data.string.p4_21,
      },
      {
        datahighlightflag: true,
        textclass: "sidepara6",
        textdata: data.string.p4_22_2,
      },
      {
        datahighlightflag: true,
        textclass: "sidepara4",
        textdata: data.string.p4_23,
      },
    ],

    imageblock: [{
      imagecontentclass: "windowblock",
      imagestoshow: [{
        imgclass: "box",
        imgsrc: imgpath + "box05.png",
      }],
    }, ],
    lowerimageblock: [{
      imagecontentclass: "lowerimagediv",
      imagestoshow: [{
          imgclass: "candle_blur",
          imgsrc: imgpath + "candela.png",
        },
        {
          imgclass: "sub_blur",
          imgsrc: imgpath + "Amount_of_Substance.png",
        },
        {
          imgclass: "current_blur",
          imgsrc: imgpath + "electric-current.png",
        },
        {
          imgclass: "temp_blur",
          imgsrc: imgpath + "temperature.png",
        },
        {
          imgclass: "mass_blur",
          imgsrc: imgpath + "mass.png",
        },
        {
          imgclass: "length_highlight",
          imgsrc: imgpath + "length.png",
        },
        {
          imgclass: "clock_blur",
          imgsrc: imgpath + "time.png",
        },
      ],
    }],
  },

  {
    typeoflayout: "layoutlayersoil",
    uppertextblock: [{
        textclass: "sideheadingstyle",
        textdata: data.string.p4_5,
      },

    ],
    middletextblock: [{
        datahighlightflag: true,
        textclass: "sidepara1",
        textdata: data.string.p4_20,
      },
      {
        datahighlightflag: true,
        textclass: "sidepara2",
        textdata: data.string.p4_21,
      },
      {
        datahighlightflag: true,
        textclass: "sidepara6",
        textdata: data.string.p4_22_2,
      },
      {
        datahighlightflag: true,
        textclass: "sidepara4",
        textdata: data.string.p4_23,
      },
    ],
    lowertextblock: [{
      datahighlightflag: true,
      textclass: "lowertextstyle",
      textdata: data.string.p4_24,
    }],
    imageblock: [{
      imagecontentclass: "windowblock",
      imagestoshow: [{
        imgclass: "window",
        imgsrc: imgpath + "length.png",
      }],
    }, ],
  },

  // Speed data starts
  {
    typeoflayout: "layoutlayersoil",
    uppertextblock: [{
        textclass: "sideheadingstyle",
        textdata: data.string.p4_6,
      },
      {
        textclass: "sidepara1",
        textdata: data.string.p4_25,
      }
    ],
  },

  {
    typeoflayout: "layoutlayersoil",
    uppertextblock: [{
        textclass: "sideheadingstyle",
        textdata: data.string.p4_6,
      },

    ],
    middletextblock: [{
        datahighlightflag: true,
        textclass: "sidepara1",
        textdata: data.string.p4_25,
      },
      {
        datahighlightflag: true,
        textclass: "sidepara2",
        textdata: data.string.p4_26,
      },

    ],

    imageblock: [{
      imagecontentclass: "carmoveblock",
      imagestoshow: [{
          imgclass: "box",
          imgsrc: imgpath + "road2.png",
        },
        {
          imgclass: "wheelfront",
          imgsrc: imgpath + "wheelbus.png",
        },
        {
          imgclass: "wheelback",
          imgsrc: imgpath + "wheelbus.png",
        },
        {
          imgclass: "bus",
          imgsrc: imgpath + "bus.png",
        }
      ],
    }, ],
    lowerimageblock: [{
      imagecontentclass: "lowerimagediv",
      imagestoshow: [{
          imgclass: "candle_small",
          imgsrc: imgpath + "candela.png",
        },
        {
          imgclass: "sub_small",
          imgsrc: imgpath + "Amount_of_Substance.png",
        },
        {
          imgclass: "current_small",
          imgsrc: imgpath + "electric-current.png",
        },
        {
          imgclass: "temp_small",
          imgsrc: imgpath + "temperature.png",
        },
        {
          imgclass: "mass_small",
          imgsrc: imgpath + "mass.png",
        },
        {
          imgclass: "length_small",
          imgsrc: imgpath + "length.png",
        },
        {
          imgclass: "clock_small",
          imgsrc: imgpath + "time.png",
        },
      ],
    }],
  },

  {
    typeoflayout: "layoutlayersoil",
    hrcustomclass: "fractiondivisorline",
    uppertextblock: [{
        textclass: "sideheadingstyle",
        textdata: data.string.p4_6,
      },
      {
        textclass: "timebox",
        textdata: data.string.p4_29_1,
      }

    ],
    middletextblock: [{
        datahighlightflag: true,
        textclass: "sidepara1",
        textdata: data.string.p4_25,
      },
      {
        datahighlightflag: true,
        textclass: "sidepara2",
        textdata: data.string.p4_26,
      },

      {
        datahighlightflag: true,
        textclass: "abovethedash",
        textdata: data.string.p4_27,
      },
      /*
      {
      				datahighlightflag: true,
      				textclass : "dashedline",
      				textdata : data.string.p4_28,
      			},*/

      {
        datahighlightflag: true,
        textclass: "bellowthedash",
        textdata: data.string.p4_29,
      },


    ],

    imageblock: [{
        imagecontentclass: "carblock",
        imagestoshow: [{
          imgclass: "box",
          imgsrc: imgpath + "road2.png",
        }, ]
      },
      {
        imagecontentclass: "carmoveblock moveright",
        imagestoshow: [{
            imgclass: "wheelfront wheelRotate",
            imgsrc: imgpath + "wheelbus.png",
          },
          {
            imgclass: "wheelback wheelRotate",
            imgsrc: imgpath + "wheelbus.png",
          },
          {
            imgclass: "bus",
            imgsrc: imgpath + "bus.png",
          },
        ],
      },
    ],


    lowerimageblock: [{
      imagecontentclass: "lowerimagediv",
      imagestoshow: [{
          imgclass: "candle_blur",
          imgsrc: imgpath + "candela.png",
        },
        {
          imgclass: "sub_blur",
          imgsrc: imgpath + "Amount_of_Substance.png",
        },
        {
          imgclass: "current_blur",
          imgsrc: imgpath + "electric-current.png",
        },
        {
          imgclass: "temp_blur",
          imgsrc: imgpath + "temperature.png",
        },
        {
          imgclass: "mass_blur",
          imgsrc: imgpath + "mass.png",
        },
        {
          imgclass: "length_highlight",
          imgsrc: imgpath + "length.png",
        },
        {
          imgclass: "clock_highlight",
          imgsrc: imgpath + "time.png",
        },
      ],
    }],
  },
  {
    typeoflayout: "layoutlayersoil",
    hrcustomclass: "fractiondivisorline",
    uppertextblock: [{
        textclass: "sideheadingstyle",
        textdata: data.string.p4_6,
      },

    ],
    middletextblock: [{
        datahighlightflag: true,
        textclass: "sidepara1",
        textdata: data.string.p4_25,
      },
      {
        datahighlightflag: true,
        textclass: "sidepara2",
        textdata: data.string.p4_26,
      },
      {
        datahighlightflag: true,
        textclass: "abovethedash",
        textdata: data.string.p4_27_1,
      },
      /*
      {
      				datahighlightflag: true,
      				textclass : "dashedline",
      				textdata : data.string.p4_28_1,
      			},*/

      {
        datahighlightflag: true,
        textclass: "bellowthedash",
        textdata: data.string.p4_29_1_3,
      },
    ],

    imageblock: [{
      imagecontentclass: "carmoveblock",
      imagestoshow: [{
          imgclass: "box",
          imgsrc: imgpath + "road2.png",
        },
        {
          imgclass: "wheelfrontright",
          imgsrc: imgpath + "wheelbus.png",
        },
        {
          imgclass: "wheelbackright",
          imgsrc: imgpath + "wheelbus.png",
        },
        {
          imgclass: "busright",
          imgsrc: imgpath + "bus.png",
        }
      ],
    }, ],
    lowerimageblock: [{
      imagecontentclass: "lowerimagediv",
      imagestoshow: [{
          imgclass: "candle_blur",
          imgsrc: imgpath + "candela.png",
        },
        {
          imgclass: "sub_blur",
          imgsrc: imgpath + "Amount_of_Substance.png",
        },
        {
          imgclass: "current_blur",
          imgsrc: imgpath + "electric-current.png",
        },
        {
          imgclass: "temp_blur",
          imgsrc: imgpath + "temperature.png",
        },
        {
          imgclass: "mass_blur",
          imgsrc: imgpath + "mass.png",
        },
        {
          imgclass: "length_highlight",
          imgsrc: imgpath + "length.png",
        },
        {
          imgclass: "clock_highlight",
          imgsrc: imgpath + "time.png",
        },
      ],
    }],
  },
  {
    typeoflayout: "layoutlayersoil",
    hrcustomclass: "fractiondivisorline",
    uppertextblock: [{
        textclass: "sideheadingstyle",
        textdata: data.string.p4_6,
      },

    ],
    middletextblock: [{
        datahighlightflag: true,
        textclass: "sidepara1",
        textdata: data.string.p4_25,
      },
      {
        datahighlightflag: true,
        textclass: "sidepara2",
        textdata: data.string.p4_26,
      },
      {
        datahighlightflag: true,
        textclass: "abovethedash",
        textdata: data.string.p4_27_1,
      },
      /*
      {
      				datahighlightflag: true,
      				textclass : "dashedline",
      				textdata : data.string.p4_28_1,
      			},*/

      {
        datahighlightflag: true,
        textclass: "bellowthedash",
        textdata: data.string.p4_29_1_3,
      },
      {
        datahighlightflag: true,
        textclass: "answerpart1",
        textdata: data.string.p4_30_1,
      },
    ],

    imageblock: [{
      imagecontentclass: "carmoveblock",
      imagestoshow: [{
          imgclass: "box",
          imgsrc: imgpath + "road2.png",
        },
        {
          imgclass: "wheelfrontright",
          imgsrc: imgpath + "wheelbus.png",
        },
        {
          imgclass: "wheelbackright",
          imgsrc: imgpath + "wheelbus.png",
        },
        {
          imgclass: "busright",
          imgsrc: imgpath + "bus.png",
        }
      ],
    }, ],
    lowerimageblock: [{
      imagecontentclass: "lowerimagediv",
      imagestoshow: [{
          imgclass: "candle_blur",
          imgsrc: imgpath + "candela.png",
        },
        {
          imgclass: "sub_blur",
          imgsrc: imgpath + "Amount_of_Substance.png",
        },
        {
          imgclass: "current_blur",
          imgsrc: imgpath + "electric-current.png",
        },
        {
          imgclass: "temp_blur",
          imgsrc: imgpath + "temperature.png",
        },
        {
          imgclass: "mass_blur",
          imgsrc: imgpath + "mass.png",
        },
        {
          imgclass: "length_highlight",
          imgsrc: imgpath + "length.png",
        },
        {
          imgclass: "clock_highlight",
          imgsrc: imgpath + "time.png",
        },
      ],
    }],
  },
  {
    typeoflayout: "layoutlayersoil",
    hrcustomclass: "fractiondivisorline",
    uppertextblock: [{
        textclass: "sideheadingstyle",
        textdata: data.string.p4_6,
      },

    ],
    middletextblock: [{
        datahighlightflag: true,
        textclass: "sidepara1",
        textdata: data.string.p4_25,
      },
      {
        datahighlightflag: true,
        textclass: "sidepara2",
        textdata: data.string.p4_26,
      },
      {
        datahighlightflag: true,
        textclass: "abovethedash",
        textdata: data.string.p4_27_1,
      },
      {
        datahighlightflag: true,
        textclass: "dashedline",
        textdata: data.string.p4_28_1,
      },
      {
        datahighlightflag: true,
        textclass: "bellowthedash",
        textdata: data.string.p4_29_1_3,
      },
      {
        datahighlightflag: true,
        textclass: "answerpart1",
        textdata: data.string.p4_30_1,
      },

      {
        datahighlightflag: true,
        textclass: "answerpart2",
        textdata: data.string.p4_30,
      },
    ],

    imageblock: [{
      imagecontentclass: "carmoveblock",
      imagestoshow: [{
          imgclass: "box",
          imgsrc: imgpath + "road2.png",
        },
        {
          imgclass: "wheelfrontright",
          imgsrc: imgpath + "wheelbus.png",
        },
        {
          imgclass: "wheelbackright",
          imgsrc: imgpath + "wheelbus.png",
        },
        {
          imgclass: "busright",
          imgsrc: imgpath + "bus.png",
        }
      ],
    }, ],
    lowerimageblock: [{
      imagecontentclass: "lowerimagediv",
      imagestoshow: [{
          imgclass: "candle_blur",
          imgsrc: imgpath + "candela.png",
        },
        {
          imgclass: "sub_blur",
          imgsrc: imgpath + "Amount_of_Substance.png",
        },
        {
          imgclass: "current_blur",
          imgsrc: imgpath + "electric-current.png",
        },
        {
          imgclass: "temp_blur",
          imgsrc: imgpath + "temperature.png",
        },
        {
          imgclass: "mass_blur",
          imgsrc: imgpath + "mass.png",
        },
        {
          imgclass: "length_highlight",
          imgsrc: imgpath + "length.png",
        },
        {
          imgclass: "clock_highlight",
          imgsrc: imgpath + "time.png",
        },
      ],
    }],
  },
  {
    typeoflayout: "layoutlayersoil",
    hrcustomclass: "fractiondivisorline",
    uppertextblock: [{
        textclass: "sideheadingstyle",
        textdata: data.string.p4_6,
      },

    ],
    middletextblock: [{
        datahighlightflag: true,
        textclass: "sidepara1",
        textdata: data.string.p4_25,
      },
      {
        datahighlightflag: true,
        textclass: "sidepara2",
        textdata: data.string.p4_26,
      },
      {
        datahighlightflag: true,
        textclass: "abovethedash",
        textdata: data.string.p4_27_1,
      },
      /*
      {
      				datahighlightflag: true,
      				textclass : "dashedline",
      				textdata : data.string.p4_28_1,
      			},*/

      {
        datahighlightflag: true,
        textclass: "bellowthedash",
        textdata: data.string.p4_29_1_3,
      },
      {
        datahighlightflag: true,
        textclass: "answerpart1",
        textdata: data.string.p4_30_1,
      },

      {
        datahighlightflag: true,
        textclass: "answerpart2",
        textdata: data.string.p4_30,
      },
    ],

    imageblock: [{
      imagecontentclass: "windowblock",
      imagestoshow: [{
          imgclass: "window2",
          imgsrc: imgpath + "length.png",
        },
        {
          imgclass: "window1",
          imgsrc: imgpath + "time.png",
        },

      ],
    }, ],

    lowertextblock: [{
      datahighlightflag: true,
      textclass: "lowertextstyle",
      textdata: data.string.p4_31,
    }],
  },
];



$(function() {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page, countNext + 1);

  /*==========  register the handlebar partials first  ==========*/
  // Handlebars.registerHelper("indexbaseone", function(value){	return value+1; });
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  // Handlebars.registerPartial("tablecontent", $("#tablecontent-partial").html());
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  // Handlebars.registerPartial("textlistcontent", $("#textlistcontent-partial").html());

  /*==========  navigation controller function  ==========*/
  function navigationcontroller() {
    if (countNext == 0) {
      $nextBtn.show(0);
      $prevBtn.css('display', 'none');
    } else if (countNext > 0 && countNext < $total_page - 1) {
      $nextBtn.show(0);
      $prevBtn.show(0);
    } else if (countNext == $total_page - 1) {
      $nextBtn.css('display', 'none');
      $prevBtn.show(0);
      ole.footerNotificationHandler.pageEndSetNotification();
    }
  }

  /*===============================================
		=            data highlight function            =
		===============================================*/
  /**

  	What it does:
  	- send an element where the function has to see
  	for data to highlight
  	- this function searches for all text nodes whose
  	data-highlight element is set to true
  	-searches for # character and gives a start tag
  	;span tag here, also for @ character and replaces with
  	end tag of the respective

   */
  function texthighlight($highlightinside) {
    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var replaceinstring;
    var texthighlightstarttag = "<span class='parsedstring'>";
    var texthighlightendtag = "</span>";
    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        // console.log(val);
        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
        $(this).html(replaceinstring);
      });
    }
  }
  /*=====  End of data highlight function  ======*/

  /*==========  general template caller  ==========*/
  function general() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    navigationcontroller();
    $(".unit1, .unit2, .unit3, .unit4, .unit5, .unit6, .unit7").addClass("pulsate");
    /* highlight the para with attribute data-highlight equals true */
    texthighlight($board);
    switch (countNext) {
      case 21:
      case 22:
      case 23:
      case 24:
        if ($lang === "en") {
          $(".bellowthedash").css({
            "text-indent": "29%"
          });
          $(".fractiondivisorline").css({
            "margin-left": "32.8%"
          });
        }
        break;
      default:

    }
    if (countNext == 3) {
      $(".sideheadingstyle").css("left", "0%");
    };

    if (countNext == 20) {
      $('.timebox').fadeOut(1000, function() {
        $(this).text('1sec').fadeIn(1500, function() {
          $(this).text('2sec').fadeOut(800, function() {
            $(this).text('3sec').fadeIn(1000);
          });
        });
      });

      $(".fractiondivisorline").css({
        "width": "18%",
        "margin-left": "28%"
      });
    }

    if (countNext >= 21 && countNext <= 23) {
      $(".carmoveblock").css('background-color', 'transparent');
    }

  }


  /* now call the first template to be shown when the page load */
  general();


  $nextBtn.on('click', function() {
    $(this).css('display', 'none');
    countNext++;
    // console.log(countNext);
    // alert(countNext);
    general();
    loadTimelineProgress($total_page, countNext + 1);
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on('click', function() {
    $(this).css('display', 'none');
    countNext--;
    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    general();
    loadTimelineProgress($total_page, countNext + 1);
  });
});

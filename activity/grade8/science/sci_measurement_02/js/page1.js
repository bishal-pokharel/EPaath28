var imgpath = $ref + "/images/page1/";

var content = [{
    imageblock: [{
      imagedivclass: "background",
      imagestoshow: [{
        imgclass: "background",
        imgsrc: imgpath + "coverpage.png",
        dialogturn: "cover_heading",
        dialog: data.lesson.chapter,
      }],

    }],
  },
  {
    imageblock: [{
      imagedivclass: "background",
      imagestoshow: [{
          imgclass: "background",
          imgsrc: imgpath + "Eiffel_Tower.png",
        },
        {
          imgclass: "character1",
          imgsrc: imgpath + "jon.png",
        },
        {
          imgclass: "character2",
          imgsrc: imgpath + "lee.png",
        },
      ],

    }],
    narration: [{
      textdata: data.string.p1_narr,
    }],
  },

  {

    imageblock: [{
      imagedivclass: "background",

      imagestoshow: [{
          imgclass: "background",
          imgsrc: imgpath + "Eiffel_Tower.png",
        },
        {
          imgclass: "character1",
          imgsrc: imgpath + "jon.png",
        },
        {
          imgclass: "character2",
          imgsrc: imgpath + "lee.png",
        },

        {
          imgclass: "bubble2",
          imgsrc: imgpath + "speech-bubble2.png",
          dialogturn: "lee",
          dialog: data.string.p1_lee1,
        },

      ],

    }],

  },
  {

    imageblock: [{
      imagestoshow: [{
          imgclass: "background",
          imgsrc: imgpath + "Eiffel_Tower.png",
        },
        {
          imgclass: "character1",
          imgsrc: imgpath + "jon.png",
        },
        {
          imgclass: "character2",
          imgsrc: imgpath + "lee.png",
        },

        {
          imgclass: "bubble1",
          imgsrc: imgpath + "speech-bubble.png",
          dialogturn: "jon",
          dialog: data.string.p1_jon1,
        },

      ],

    }],

  },
  {

    imageblock: [{
      imagestoshow: [{
          imgclass: "background",
          imgsrc: imgpath + "Eiffel_Tower.png",
        },
        {
          imgclass: "character1",
          imgsrc: imgpath + "jon.png",
        },
        {
          imgclass: "character2",
          imgsrc: imgpath + "lee.png",
        },

        {
          imgclass: "bubble2",
          imgsrc: imgpath + "speech-bubble2.png",
          dialogturn: "lee",
          dialog: data.string.p1_lee2,
        },

      ],

    }],

  },
  {

    imageblock: [{
      imagestoshow: [{
          imgclass: "background",
          imgsrc: imgpath + "Eiffel_Tower.png",
        },
        {
          imgclass: "character1",
          imgsrc: imgpath + "jon.png",
        },
        {
          imgclass: "character2",
          imgsrc: imgpath + "lee.png",
        },
        {
          imgclass: "character3",
          imgsrc: imgpath + "rose.png",
        },
        {
          imgclass: "bubble3",
          imgsrc: imgpath + "speech-bubble.png",
          dialogturn: "rose",
          dialog: data.string.p1_rose1,
        },


      ],

    }],
  },
  {

    imageblock: [{
      imagestoshow: [{
          imgclass: "background",
          imgsrc: imgpath + "Eiffel_Tower.png",
        },
        {
          imgclass: "character1",
          imgsrc: imgpath + "jon.png",
        },
        {
          imgclass: "character2",
          imgsrc: imgpath + "lee.png",
        },
        {
          imgclass: "character3",
          imgsrc: imgpath + "rose.png",
        },
        {
          imgclass: "bubble3",
          imgsrc: imgpath + "speech-bubble.png",
          dialogturn: "rose",
          dialog: data.string.p1_rose3,
        },


      ],

    }],
  },
  {

    imageblock: [{
      imagestoshow: [{
          imgclass: "background",
          imgsrc: imgpath + "Eiffel_Tower.png",
        },
        {
          imgclass: "character1",
          imgsrc: imgpath + "jon.png",
        },
        {
          imgclass: "character2",
          imgsrc: imgpath + "lee.png",
        },
        {
          imgclass: "character3",
          imgsrc: imgpath + "rose.png",
        },
        {
          imgclass: "bubble3",
          imgsrc: imgpath + "speech-bubble.png",
          dialogturn: "rose",
          dialog: data.string.p1_rose4,
        },
        {
          imgclass: "height",
          imgsrc: imgpath + "label.png",
          dialogturn: "height_eiffel",
          dialog: "324 m",
        },

      ],

    }],

  },


  {
    imageblock: [{
      imagestoshow: [{
          imgclass: "simpleimgstyle",
          imgsrc: imgpath + "globe02.png",
        }

      ],
    }],

    uppertextblock: [{
        textclass: "earthtitle",
        textdata: data.string.p1_1,
      },
      {
        textclass: "earthtitle1",
        textdata: data.string.p1_1_1,
      }
    ],

  },

  {
    imageblock: [{
      imagestoshow: [{
          imgclass: "simpleimgstyle",
          imgsrc: imgpath + "globe02.png",
        }

      ],
    }],

    uppertextblock: [{
        textclass: "earthtitle",
        textdata: data.string.p1_1,
      },
      {
        textclass: "earth1green",
        textdata: data.string.p1_1_1,
      }
    ],

    lowertextblock: [{
      textclass: "imagecaptionleft",
      textdata: data.string.p1_1_2,
    }, ],

  },
  {
    imageblock: [{
      imagestoshow: [{
          imgclass: "simpleimgstyle",
          imgsrc: imgpath + "globe02.png",
        }

      ],
    }],

    uppertextblock: [{
        textclass: "earthgreen",
        textdata: data.string.p1_1,
      },
      {
        textclass: "earth1green",
        textdata: data.string.p1_1_1,
      }
    ],
    lowertextblock: [{
      textclass: "imagecaption",
      textdata: data.string.isu,
    }],

  },
  {
    uppertextblock: [{
      textclass: "bigheadingtextstyle",
      textdata: data.string.p1_4,
    }],

    lowertextblock: [{
      textclass: "simpletextstyle",
      textdata: data.string.p1_5,
    }],
  },
  {
    uppertextblock: [{
      textclass: "bigheadingtextstyle",
      textdata: data.string.p1_4,
    }],

    lowertextblock: [{
        textclass: "simpletextstyle",
        textdata: data.string.p1_5,
      },
      {
        textclass: "simpletextstyle",
        textdata: data.string.p1_6,
      }
    ],

  },
  {
    uppertextblock: [{
      textclass: "bigheadingtextstyle",
      textdata: data.string.p1_4,
    }],

    lowertextblock: [{
        textclass: "simpletextstyle",
        textdata: data.string.p1_5,
      },
      {
        textclass: "simpletextstyle",
        textdata: data.string.p1_6,
      },
      {
        textclass: "simpletextstyle",
        textdata: data.string.p1_7,
      }
    ],

  },
]


$(function() {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page, countNext + 1);


  // register the handlebar partials first
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

  /*=====================================
		=            datahighlight            =
		=====================================*/
  function texthighlight($highlightinside) {
    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var replaceinstring;
    var texthighlightstarttag = "<span class='parsedstring'>";
    var texthighlightendtag = "</span>";
    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        // console.log(val);
        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
        $(this).html(replaceinstring);
      });
    }
  }



  /*=====  End of datahighlight  ======*/

  // navigation controller function
  function navigationcontroller() {
    if (countNext == 0) {
      $nextBtn.show(0);
      $prevBtn.css('display', 'none');
    } else if (countNext > 0 && countNext < $total_page - 1) {
      $nextBtn.show(0);
      $prevBtn.show(0);
    } else if (countNext == $total_page - 1) {
      $nextBtn.css('display', 'none');
      $prevBtn.show(0);
      ole.footerNotificationHandler.pageEndSetNotification();
    }
  }

  /*
   * mirrorintro
   */
  function mirrorintro() {
    var source = $("#mirrorintro-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    texthighlight($board);

    if (countNext == 5 || countNext == 6 || countNext == 7) {
      $(".character1").css("right", "31%");
      $(".character2").css("right", "41%");
    };

    if (countNext == 2) {
      $(".earthtitle, .earthtitle1").delay(100).fadeIn(1000);
    };
    // call navigation controller
    navigationcontroller();
  }

  mirrorintro();

  $nextBtn.on('click', function() {
    $(this).css('display', 'none');
    countNext++;
    // alert(countNext);
    mirrorintro();
    loadTimelineProgress($total_page, countNext + 1);
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on('click', function() {
    $(this).css('display', 'none');
    countNext--;
    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    mirrorintro();
    loadTimelineProgress($total_page, countNext + 1);
  });
});

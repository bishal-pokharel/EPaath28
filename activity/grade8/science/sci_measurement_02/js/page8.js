var imgpath = $ref+"/images/page8/";
var squirreldef = "images/lokharke/definition2.png"
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var content=[
  { 
    uppertextblock : [
      {
        textclass : "headingstyle",
        textdata   : data.string.p9_title,
      }
    ],
    imageblock:[
      {
        imagestoshow:[
          {
            imgclass: "squirreldef",
            imgsrc: squirreldef
          }
        ]
      }
    ]
  },
  {
    uppertextblock:[
      {
        datahighlightflag: true,
        datahighlightcustomclass : "multiplehighlightslide3",
        textclass:"paratextstyle",
        textdata: data.string.p9_para1
      }
    ]
  },
  {
    contentnocenteradjust:true,
    uppertextblock:[
      {
        textclass: "subtitle",
        textdata: data.string.p9_para2
      },
      {
        textclass:"middleline"
      },
      {
        textclass: "middleheadingstyle moveright",
        textdata: data.string.p9_para3
      },
      {
        textclass: "middleheadingstyle2",
        textdata: data.string.p9_para3_1
      },
      {
        textclass: "middleheadingstyle3",
        textdata: data.string.p9_para8
      },
      {
        textclass: "weightbox1",
        textdata: data.string.p9_para4
      },
      {
        textclass: "weightbox2",
        textdata: data.string.p9_para5
      },
      {
        textclass: "weightbox3",
        textdata: data.string.p9_para6
      },
      {
        textclass: "weightbox4",
        textdata: data.string.p9_para7
      }
    ],
    imageblock:[
      {
        imagestoshow:[
          {
            imgclass:"empty_beaker cssfadein",
            imgsrc: imgpath+"empty-beaker.png"
          },
          {
            imgclass:"full_beaker",
            imgsrc: imgpath+"fill-beaker.png"
          },
          {
            imgclass:"panbalance cssfadein",
            imgsrc: imgpath+"machine0.png"
          },
          {
            imgclass:"panbalance5",
            imgsrc: imgpath+"machine5.png"
          },
          {
            imgclass:"panbalance25",
            imgsrc: imgpath+"machine25.png"
          },
          {
            imgclass:"clickmebeaker cssfadein",
            imgsrc: "images/clickme_icon.png"
          },
          {
            imgclass:"clickmebeaker2",
            imgsrc: "images/clickme_icon.png"
          }
        ]
      }
    ]
  },
  {
    contentnocenteradjust:true,

    uppertextblock:[
      {
        textclass: "middleheadingstyle3 moveright",
        textdata: data.string.p9_para8
      }
    ],
    imageblock:[
      {
        imagestoshow:[
          {
            imgclass:"beaker1 moverightagain",
            imgsrc: imgpath+"fill-beaker.png"
          }
        ],

        imagelabels:[
          {
            imagelabelclass:"label1 moverightagain",
            imagelabeldata: data.string.p9_para7
          }
        ]
      },
      {
        imagestoshow:[
          {
            imgclass:"minus",
            imgsrc: imgpath+"minus.png"
          }
        ]
      },

      {
        imagestoshow:[
          {
            imgclass:"beaker2",
            imgsrc: imgpath+"empty-beaker.png"
          }
        ],

        imagelabels:[
          {
            imagelabelclass:"label2",
            imagelabeldata: data.string.p9_para5
          }
        ]
      },

      {
        imagestoshow:[
          {
            imgclass:"arrow",
            imgsrc: imgpath+"arrow.png"
          }
        ]
      },

      {
        imagestoshow:[
          {
            imgclass:"wateronly",
            imgsrc: imgpath+"water.png"
          }
        ],
        imagelabels:[
          {
            imagelabelclass:"label3",
            imagelabeldata: data.string.p9_para9
          }
        ]
      }          
    ],
    lowertextblock:[
      {
        textclass: "lasttext",
        textdata: data.string.p9_para10
      }
    ]
  },
  {
    contentnocenteradjust:true,

    uppertextblock:[
      {
        textclass: "middleheadingstyle3 moveright",
        textdata: data.string.p9_para11
      },
      {
        textclass: "lastpara1",
        textdata: data.string.p9_para12
      },
      {
        textclass: "lastpara2",
        textdata: data.string.p9_para13
      }
    ],
    imageblock:[
      {
        imagestoshow:[
          {
            imgclass: "beakervolume",
            imgsrc: imgpath+"fill-beaker.png"
          },
          {
            imgclass: "arrowmove moveup",
            imgsrc: imgpath+"arrow.png"
          }
        ]
      }
    ]
  },
  {
    contentnocenteradjust:true,

    uppertextblock:[
      {
        textclass: "middleheadingstyle3 moveright",
        textdata: data.string.p9_para14
      },
      {
        textclass: "densitybig cssfadein",
        textdata: data.string.p9_para15
      },
      {
        textclass: "massbig",
        textdata: data.string.p9_para16
      },
      {
        textclass: "underline"
      },
      {
        textclass: "volumebig",
        textdata: data.string.p9_para17
      },
      {
        textclass: "lastbig",
        textdata: data.string.p9_para18
      },
    ],
  
    imageblock:[
      {
        imagestoshow:[
          {
            imgclass: "masscalcu",
            imgsrc: imgpath+"compare.png"
          },
          {
            imgclass: "volumecalcu",
            imgsrc: imgpath+"water-fill.gif"
          }
        ]
      }
    ]
  },
  {
    contentnocenteradjust:true,

    uppertextblock:[
      {
        textclass: "middleheadingstyle3 moveright",
        textdata: data.string.p9_para19
      },
      {
        textclass: "densitybig cssfadein",
        textdata: data.string.p9_para15
      },
      {
        textclass: "massbig cssfadein2",
        textdata: data.string.p9_para16
      },
      {
        textclass: "underline cssfadein2"
      },
      {
        textclass: "volumebig2 cssfadein2",
        textdata: data.string.p9_para17
      },
      {
        textclass:"sidetext",
        textdata: data.string.p9_para20
      },
      {
        textclass: "densitybig2 cssfadein",
        textdata: data.string.p9_para20_1
      },
      {
        textclass: "massbig2 cssfadein2",
        textdata: data.string.p9_para20_2
      },
      {
        textclass: "underline2 cssfadein2"
      },
      {
        textclass: "volumebig3 cssfadein2",
        textdata: data.string.p9_para20_3
      }
    ],
    lowertextblock:[
      {
        textclass:"where",
        textdata: "where,"
      },
      {
        datahighlightflag:true,
        textclass:"inst1",
        textdata: data.string.p9_para22,
      },
      {
        datahighlightflag:true,
        textclass:"inst2",
        textdata: data.string.p9_para23,
      },
      {
        datahighlightflag:true,
        textclass:"inst3",
        textdata: data.string.p9_para24,
      },
    ],
  }

  
];


$(function () { 
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 6;

  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
   Handlebars.registerPartial("instructioncontent", $("#instructioncontent-partial").html());
   Handlebars.registerPartial("cardcontent", $("#cardcontent-partial").html());
   Handlebars.registerPartial("usageslowrevealcontent", $("#usageslowrevealcontent-partial").html());
   Handlebars.registerPartial("timetothinkcontent", $("#timetothinkcontent-partial").html());
   Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());
   Handlebars.registerPartial("textlistcontent", $("#textlistcontent-partial").html());
   Handlebars.registerPartial("usagecontent", $("#usagecontent-partial").html());
   Handlebars.registerPartial("propertycontent", $("#propertycontent-partial").html());
   Handlebars.registerPartial("propertysummarycontent", $("#propertysummarycontent-partial").html());
   Handlebars.registerPartial("diysplashcontent", $("#diysplashcontent-partial").html()); 
   Handlebars.registerPartial("diycontent", $("#diycontent-partial").html());
   Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
   
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true 
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class
        
        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;
          
        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) { 
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/       
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
    /*=====  End of data highlight function  ======*/

    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**   
      How to:
      - First set any html element with 
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification    
     */
    
    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which 
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/ 
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
      
      // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/    
   /**   
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that 
      footernotification is called for continue button to navigate to exercise
    */
  
  /**   
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templatecaller
        function
      - Can be put anywhere in the template function as per the need, if 
        so should be taken out from the templatecaller function
      - If the total page number is 
     */  
   
  function navigationcontroller(islastpageflag){
    // check if the parameter is defined and if a boolean,
    // update islastpageflag accordingly
    typeof islastpageflag === "undefined" ? 
    islastpageflag = false : 
      typeof islastpageflag != 'boolean'?
      alert("NavigationController : Hi Master, please provide a boolean parameter") :
      null;

    if(countNext == 0 && $total_page!=1){
      $nextBtn.show(0);
      $prevBtn.css('display', 'none');
    }
    else if($total_page == 1){
      $prevBtn.css('display', 'none');
      $nextBtn.css('display', 'none');
      
      // if lastpageflag is true 
      islastpageflag ? 
        ole.footerNotificationHandler.lessonEndSetNotification() :
          ole.footerNotificationHandler.pageEndSetNotification() ;      
    }
    else if(countNext > 0 && countNext < $total_page-1){
      $nextBtn.show(0);
      $prevBtn.show(0);
    }
    else if(countNext == $total_page-1){
      $nextBtn.css('display', 'none');
      $prevBtn.show(0);

      // if lastpageflag is true 
      islastpageflag ? 
        ole.footerNotificationHandler.lessonEndSetNotification() :
          ole.footerNotificationHandler.pageEndSetNotification() ;
    }
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**   
      How to:
      - Just call instructionblockcontroller() from the template    
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which 
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);          
        });
      }
    } 
  /*=====  End of InstructionBlockController  ======*/
  
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/   
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $nextBtn.hide(0);
        
    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);

    if (countNext==0|| countNext==1) {
      $nextBtn.show(0);
    };

    //call instruction block controller
    // instructionblockcontroller($board);

    //call notifyuser
    // notifyuser($anydiv);

    $(".clickmebeaker").on("click", function(){ 
        $(this).hide(0);
        $(".empty_beaker").addClass("cssfadeinslide");
        $(".weightbox1").addClass("cssfadein");
    });

    $(".weightbox1").on(animationend, function(){ 
      $(".panbalance").hide(0);
      $(".panbalance5").show(0);
      $(".weightbox2").addClass("cssfadein");
    });

    $(".weightbox2").on(animationend, function(){ 
      $(".empty_beaker").delay(100).hide(0);
      $(".panbalance5").delay(100).hide(0);
      $(".panbalance").delay(100).show(0);
      $(".middleheadingstyle").hide(0);
      $(".middleheadingstyle2").addClass("moveright");
      $(".full_beaker, .clickmebeaker2").addClass("cssfadein");
    });


    $(".clickmebeaker2").on("click", function(){ 
      $(this).hide(0);
      $(".full_beaker").addClass("cssfadeinslideleft");
      $(".weightbox3").addClass("cssfadein");
    });

    $(".weightbox3").on(animationend, function(){ 
      $(".panbalance5").hide(0);
      $(".panbalance25").show(0);
      $(".weightbox4").addClass("cssfadein");
    });

    $(".weightbox4").on(animationend, function(){ 
      $nextBtn.show(0);
    });

    $(".label1").on(animationend, function(){ 
      $(".minus").addClass("moverightagain");
    });

    $(".minus").on(animationend, function(){ 
      $(".beaker2, .label2").addClass("moverightagain");
    });

    $(".label2").on(animationend, function(){ 
      $(".arrow").addClass("moverightagain");
    });

    $(".arrow").on(animationend, function(){ 
      $(".wateronly, .label3").addClass("moverightagain");
      $(".lasttext").addClass("cssfadein2");
      $nextBtn.delay(2000).show(100);
    });

    $(".arrowmove").on(animationend, function(){ 
        $(".lastpara1, .lastpara2").addClass("cssfadein");
    });

    $(".lastpara2").on(animationend, function(){ 
      $nextBtn.show(0);
    });

    $(".densitybig").on(animationend, function() { 
      $(".massbig").addClass("cssfadein");
      $(".masscalcu").addClass("cssfadein2");
    });

    $(".masscalcu").on(animationend, function(){ 
      $(".massbig").text(data.string.p9_para9);
      $(".underline").addClass("cssfadein2");
    });

    $(".underline").on(animationend, function(){ 
      $(".masscalcu").hide(0);
      $(".volumebig").addClass("cssfadein");
    });

    $(".volumebig").on(animationend, function(){ 
      $(".volumecalcu").addClass("cssfadein");
    });

    $(".volumecalcu").on(animationend, function(){ 
      $(".volumebig").text(data.string.p9_para13);
      $(".lastbig").addClass("cssfadein");
      $nextBtn.delay(2000).show(0);
    });

    $(".volumebig2").on(animationend, function(){ 
      $(".sidetext").addClass("cssfadein");
    });

    $(".volumebig3").on(animationend, function() {
      $(".where").addClass("cssfadein");
    });
    $(".where").on(animationend, function() {
      $(".inst1").addClass("cssfadein");
      $(".inst2").addClass("cssfadein2");
      $(".inst3").addClass("cssfadein3");
    })
  }

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the 
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated 
   */
  
  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/ 
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');
    
    // call navigation controller
    navigationcontroller();

    // call the template
    generaltemplate();

    /*OR, call templates like this if you have more than one template
    to call*/
    // switch(countNext){
    //   case 0 : generaltemplate(); break;
    //   case 1 : generaltemplate(); break;
    //   case 2 : generaltemplate(); break;
    //   default : break;
    // }

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

    // just for development purpose to see total slide vs current slide number
    // $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/
  // countNext+=1;

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */
  
  $nextBtn.on('click',function () {
    countNext++;    
    templateCaller();   
  });

  $refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
    countNext--;      
    templateCaller();

    /* if footerNotificationHandler pageEndSetNotification was called then on click of 
      previous slide button hide the footernotification */    
    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
  });

/*=====  End of Templates Controller Block  ======*/
});
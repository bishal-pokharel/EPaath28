var SPACE = data.string.spacecharacter;
var defimg = "images/definitionpage/definition4.png";

var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
var imgpath = $ref+"/images/page8/";
var diyImg = "images/diy/diy1.png";

var content=[
	{
		uppertextblock:[
			{
				textclass:"bigcustomheading",
				textdata: data.string.lever+": "+data.string.Velocityratio,
			}
		]
	},
	{
    	definitionblock:[
    		{
    			definitionImageSource:defimg,
    			definitionTextData: data.string.Velocityratio,
    			definitionInstructionData: data.string.p8text1
    		}
    	]
    },
    {
    	hasheader: true,
    	headerblock:[
    		{
    			textclass:"headertextstyle",
    			textdata: data.string.p8text3
    		}
    	],
        contentblocknocenteradjust : true,
        uppertextblock:[
            {
                textclass:"bottompara1",
                textdata: data.string.p8text2
            }
        ],
    	imageblock:[
    		{
    			imagestoshow:[
    				{
    					imgclass: "image1",
    					imgsrc: imgpath+"page48.png"
    				}
    			],
                imagelabels:[
                    {
                        imagelabelclass:"ld",
                        imagelabeldata: data.string.ld
                    },
                    {
                        imagelabelclass:"ed",
                        imagelabeldata: data.string.ed
                    }
                ]
    		}
    	],
    },
    {
        contentblocknocenteradjust : true,
        uppertextblock:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "bigfont",
                textclass:"paragraph1 cssfadein",
                textdata: data.string.p8text26
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "bigfont",
                textclass:"paragraph2 cssfadein3",
                textdata: data.string.p8text27
            }
        ],
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass: "image2",
                        imgsrc: imgpath+"page47.png"
                    }
                ],
                imagelabels:[
                    {
                        imagelabelclass:"ld2 cssfadein2",
                        imagelabeldata: data.string.ld
                    },
                    {
                        imagelabelclass:"ed2 cssfadein4",
                        imagelabeldata: data.string.ed
                    }
                ]
            }
        ],
    },
    {
    	hasheader:true,
		headerblock:[
    		{
        		textclass:"headertextstyle",
				textdata: data.string.p8text4
        	}
    	],
        contentblocknocenteradjust : true,
        imageblock:[
        	{
        		imagestoshow:[
        			{
        				imgclass: "velocityratioimg",
        				imgsrc: imgpath+"page47.png"
        			}
        		],
                imagelabels:[
                    {
                        imagelabelclass:"ld3",
                        imagelabeldata: data.string.ld
                    },
                    {
                        imagelabelclass:"ed3",
                        imagelabeldata: data.string.ed
                    }
                ]
        	}
        ],
    	uppertextblock:[
    		{
    			textclass:"formula1 cssfadein",
    			textdata: data.string.p8velocityload
    		},
    		{
    			textclass:"equal cssfadein",
    			textdata: data.string.equalsign
    		},
    		{
    			textclass:"formula2 cssfadein2",
    			textdata: data.string.p8Ld
    		},
    		{
    			textclass:"formula3 cssfadein2",
    			textdata: data.string.p8T
    		},
    		{
    			textclass:"formula4 cssfadein3",
    			textdata: data.string.p8velocityeffort
    		},
    		{
    			textclass:"equal2 cssfadein3",
    			textdata: data.string.equalsign
    		},
    		{
    			textclass:"formula5 cssfadein4",
    			textdata: data.string.p8Ed
    		},
    		{
    			textclass:"formula6 cssfadein4",
    			textdata: data.string.p8T
    		}
    	],
    	middletextblock:[
    		{
    			textclass: "formulamain",
    			textdata: data.string.p8velocityratio
    		},
    		{
    			textclass: "equalformula",
    			textdata: data.string.equalsign
    		},
            {
                textclass: "formulamaintop",
                textdata: data.string.p8EdT
            },
            {
                textclass: "formulamainbottom",
                textdata: data.string.p8LdT
            },
            {
                textclass: "equalformulamain2",
                textdata: data.string.equalsign
            },
            {
                textclass: "formulamaintop2",
                textdata: data.string.p8Ed
            },
            {
                textclass: "formulamainbottom2",
                textdata: data.string.p8Ld
            },
            {
                textclass: "therefore",
                textdata: data.string.p7therefore
            },
            {
                textclass: "velocityratio",
                textdata: data.string.p8velocityratio
            },
            {
                textclass: "equalformulamain3",
                textdata: data.string.equalsign
            },
            {
                textclass: "distanceEffort",
                textdata: data.string.p8distanceEffort
            },
            {
                textclass: "distanceLoad",
                textdata: data.string.p8distanceLoad
            },
    	]
    },
    {
        hasheader: true,
        headerblock:[
            {
                textclass:"headertextstyle customcss",
                textdata: data.string.p8text28
            }
        ],
        contentblocknocenteradjust : true,
        uppertextblock:[
            {
                textclass: "seesolution",
                textdata: data.string.p7text22
            },
            {
                textclass:"solution",
                textdata: data.string.Solution
            },
            {
                textclass:"given",
                textdata: data.string.Given
            },
            {
                textclass:"given1 cssfadein",
                textdata: data.string.p8distancemovedE
            },
            {
                textclass:"given2 cssfadein",
                textdata: data.string.p8distancemovedL
            },
            {
                textclass:"given3 cssfadein",
                textdata: data.string.p8velocityratiowat
            },
            {
                textclass:"wehave",
                textdata: data.string.Wehave
            },
            {
                textclass:"MA1",
                textdata: data.string.p8velocityratio
            },
            {
                textclass:"equalMA",
                textdata: data.string.equalsign
            },
            {
                textclass:"topformula",
                textdata: data.string.p8distanceEffort
            },
            {
                textclass:"bottomformula",
                textdata: data.string.p8distanceLoad
            },
            {
                textclass:"MA2",
                textdata: data.string.p8VR
            },
            {
                textclass:"equalMA2",
                textdata: data.string.equalsign
            },
            {
                textclass:"topformula2",
                textdata: data.string.p815m
            },
            {
                textclass:"bottomformula2",
                textdata: data.string.p85m
            },
            {
                textclass:"therefore2",
                textdata: data.string.p7therefore
            },
            {
                textclass:"MA3",
                textdata: data.string.p8VR
            },
            {
                textclass:"equalMA3",
                textdata: data.string.equalsign
            },
            {
                textclass:"answer",
                textdata: data.string.p83
            },
            // {
            //     datahighlightflag:true,
            //     datahighlightcustomclass: "orangehighlight",
            //     textclass:"answertext",
            //     textdata: data.string.p7text30
            // }
        ]
    },
    {
        uppertextblock:[
            {
                textclass: "onlyparatextstyle bigbigfont",
                textdata: data.string.lever+": "+data.string.Efficiency
            }
        ]
    },
    {
        hasheader: true,
        headerblock:[
            {
                textclass: "headertextstyle",
                textdata: data.string.p8text5
            }
        ],
        contentblocknocenteradjust : true,
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"singleimgeff",
                        imgsrc: imgpath+"page47.png"
                    }
                ],
                imagelabels:[
                    {
                        imagelabelclass: "loadlabel1",
                        imagelabeldata: data.string.ld
                    },
                    {
                        imagelabelclass: "loadlabel2",
                        imagelabeldata: data.string.loadload
                    }
                ]
            }
        ],
        lowertextblock:[
            {
                datahighlightflag:true,
                datahighlightcustomclass: "orangehighlight",
                textclass: "effpara1",
                textdata: data.string.p8text6
            },
            {
                textclass: "effpara2",
                textdata: data.string.p8text7
            },
            {
                textclass: "effpara3",
                textdata: data.string.p8text8
            }
        ]
    },
    {
        hasheader: true,
        headerblock:[
            {
                textclass: "headertextstyle",
                textdata: data.string.p8text9
            }
        ],
        contentblocknocenteradjust : true,
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"singleimgeff",
                        imgsrc: imgpath+"page47.png"
                    }
                ],
                imagelabels:[
                    {
                        imagelabelclass: "effortlabel1",
                        imagelabeldata: data.string.ed
                    },
                    {
                        imagelabelclass: "effortlabel2",
                        imagelabeldata: data.string.efforteffort
                    }
                ]
            }
        ],
        lowertextblock:[
            {
                datahighlightflag:true,
                datahighlightcustomclass: "orangehighlight",
                textclass: "effpara1",
                textdata: data.string.p8text10
            },
            {
                textclass: "effpara2",
                textdata: data.string.p8text11
            },
            {
                textclass: "effpara3",
                textdata: data.string.p8text12
            }
        ]
    },
    {
        definitionblock:[
            {
                definitionImageSource:defimg,
                definitionTextData: data.string.Efficiency,
                definitionInstructionData: data.string.p8text13
            }
        ]
    },
    {
        hasheader:true,
        headerblock:[
            {
                datahighlightflag:true,
                datahighlightcustomclass: "orangehighlight",
                textclass: "headertextstyle",
                textdata: data.string.formula+":"+data.string.Efficiency
            }
        ],
        contentblocknocenteradjust : true,
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"singleimgeff3",
                        imgsrc: imgpath+"04.png"
                    }
                ],
                imagelabels:[
                    {
                        imagelabelclass:"label1",
                        imagelabeldata: data.string.p8text15
                    },
                    {
                        imagelabelclass:"label2",
                        imagelabeldata: data.string.p8text16
                    },
                    {
                        imagelabelclass:"edlabel1",
                        imagelabeldata: data.string.ed
                    },
                    {
                        imagelabelclass:"ldlabel1",
                        imagelabeldata: data.string.ld
                    }
                ]
            }
        ],
        lowertextblock:[
            {
                textclass: "derivpara1 cssfadein",
                textdata: data.string.p8text14
            },
            {
                textclass: "derivpara2 cssfadein2",
                textdata: data.string.p8text15
            },
            {
                textclass: "derivpara3 cssfadein3",
                textdata: data.string.p8text16
            },
            {
                textclass: "equalderiv1 cssfadein4",
                textdata: data.string.equalsign
            },
            {
                textclass: "derivpara4 cssfadein4",
                textdata: data.string.p8text17
            },
            {
                textclass: "derivpara5 cssfadein4",
                textdata: data.string.p8text18
            },
            {
                textclass: "derivpara6",
                textdata: data.string.p8text19
            },
            {
                textclass: "derivpara7",
                textdata: data.string.p8text20
            },
            {
                textclass: "derivpara8",
                textdata: data.string.p8text21
            },
            {
                textclass: "derivpara9",
                textdata: data.string.p8text22
            },
            {
                textclass: "derivpara10",
                textdata: data.string.p8text19
            }
        ]
    },
    {
        uppertextblock: [
            {
                textclass: "onlyparatextstyle",
                textdata: data.string.p8text25
            }
        ]
    },
    {
        hasheader: true,
        headerblock:[
            {
                textclass: "headertextstyle",
                textdata: data.string.p8text23
            }
        ],
        // contentblocknocenteradjust : true,
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass: "singleimgstyleanim",
                        imgsrc:imgpath+"machine.gif"
                    }
                ]
            }
        ],
        lowertextblock:[
            {
                textclass: "bottomparatextstyle",
                textdata: data.string.p8text24
            }
        ]
    }
];

$(function () {	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("diysplashcontent", $("#diysplashcontent-partial").html()); 
	 Handlebars.registerPartial("diycontent", $("#diycontent-partial").html());
	 Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	 
	 /*===============================================
	 =            data highlight function            =
	 ===============================================*//**
			What it does:
			- send an element where the function has to see
				for data to highlight
			- this function searches for all nodes whose
			    data-highlight element is set to true 
			-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
			- if provided with data-highlightcustomclass value for highlight it
				applies the custom class or else uses parsedstring class
			
			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;
				
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {	
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/				
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/	 	 
	 /**	 
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that 
	 		footernotification is called for continue button to navigate to exercise
	  */
	
	 /**	  
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if 
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is 
	   */  
	 
	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			
			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;			
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**		
			How to:
			- Just call instructionblockcontroller() from the template		
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);					
				});
			}
		}	
	/*=====  End of InstructionBlockController  ======*/
		
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	
	/*=================================================
	=            general template function            =
	=================================================*/		
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
				
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

        $(".formula6").on(animationend, function(){
            $(".middletextblock").addClass("csslineanim");
            $(".middletextblock").addClass("curl");
        });

        $(".middletextblock").on(animationend, function(){
            $(".formulamain, .equalformula").addClass("cssfadein");
            $(".formulamaintop, .formulamainbottom").addClass("cssfadein2");
            $(".equalformulamain2, .formulamaintop2, .formulamainbottom2").addClass("cssfadein3");
        });

        $(".formulamaintop").on(animationend, function(){ 
            // $(this).text(data.string.p8EdTstrike);
        });

        $(".formulamainbottom").on(animationend, function(){ 
            // $(this).text(data.string.p8LdTstrike);
        });

        $(".equalformulamain2").on(animationend, function(){
            $(".therefore, .velocityratio").addClass('cssfadein');
            $(".distanceEffort, .distanceLoad").addClass('cssfadein2');
        });

        if (countNext==5) {
            $nextBtn.hide(0);
        }

        $(".given3").on(animationend, function(){
            $(".seesolution").addClass('moveleftsolution');
        });

        $(".seesolution").on("click", function(){ 
            $(this).hide(0);
            $(".wehave").addClass("cssfadein");
            $(".MA1, .equalMA").addClass("cssfadein2");
            $(".topformula").addClass("cssfadein3");
            $(".bottomformula").addClass("cssfadein4");
        });

        $(".bottomformula").on(animationend, function(){
            $(".MA2, .equalMA2").addClass("cssfadein");
            $(".topformula2, .bottomformula2").addClass("cssfadein2");
        });

        $(".bottomformula2").on(animationend, function(){ 
            $(".therefore2, .MA3, .equalMA3").addClass("cssfadein");
            $(".answer").addClass("cssfadein2");
            $nextBtn.delay(2000).show(0);
        });

        $(".derivpara5").on(animationend, function(){ 
            $(".derivpara6").addClass("cssfadein");
            $(".derivpara7").addClass("cssfadein2");
            $(".derivpara8, .derivpara9").addClass("cssfadein3");
            $(".derivpara10").addClass("cssfadein4");
        });

		// for diy blocks only
		var $diyblock = $board.find('div.diyblock');

		if($diyblock.length > 0){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			ole.footerNotificationHandler.hideNotification();

			var $diyworkarea                 = $board.find("div.diyworkarea");
			var $machinecontainer            = $diyworkarea.find("div.machinecontainer");
			var $timelyhints                 = $machinecontainer.find("p.timelyhints");
			var $leverparaphernaliacontainer = $machinecontainer.find("div.leverparaphernaliacontainer");
			var $effortobject                = $leverparaphernaliacontainer.find("img.effortobject");
			var $loadobject                  = $leverparaphernaliacontainer.find("img.loadobject");
			var $leverattributes             = $machinecontainer.find("div.leverattributes");
			var $allattributes               = $leverattributes.children("div.individualattribute");

			var $rangeinputs = $allattributes.children("input[type=range]");
			var $currentattributeuservalue;
			var recentuserval;
			var $effortvalue = $leverattributes.find("div.attribeffort").children("input[type=range]");
			var effortmagnitude;
			var $effortarmvalue = $leverattributes.find("div.attribeffortarm").children("input[type=range]");
			var effortarmmagnitude;
			var $loadvalue = $leverattributes.find("div.attribload").children("input[type=range]");
			var loadmagnitude;
			var $loadarmvalue = $leverattributes.find("div.attribloadarm").children("input[type=range]");
			var loadarmmagnitude;
			var effortforce;
			var loadforce;
			var resultantforce;
			var changethisparaphernalia;
			
			var $recordkeep    = $leverattributes.find("button.recordkeep");
			var $flipcontainer = $diyworkarea.find("div.flipcontainer");
			var $datarecords   = $diyworkarea.find("table.datarecords > tbody");
			var $currentrow;
			var activaterecordrow = 1;
			var checkthisstringinrecord;
			var alreadyrecorded = [];
			
			/* whenever user changes value of range inputs */			
			$rangeinputs.on('change', function() {
				// update on what to change on leverparaphernalia
				changethisparaphernalia = $(this).attr("data-changethis");

				// update recent uservalue and display it
				recentuserval = $(this).val();
				$currentattributeuservalue = $(this).siblings('span.uservalue');
				$currentattributeuservalue.html(recentuserval);

				/*update effortvalue, effortarmvalue, loadvalue, loadarmvalue,
				effortforce, effortload, resultantforce*/
				effortmagnitude    = $effortvalue.val();
				effortarmmagnitude = $effortarmvalue.val();
				loadmagnitude      = $loadvalue.val();
				loadarmmagnitude   = $loadarmvalue.val();
				effortforce        = effortmagnitude * effortarmmagnitude;
				loadforce          = loadmagnitude * loadarmmagnitude;
				resultantforce     = loadforce - effortforce; /*change the variables to change direction of rotation*/
				
				// changethis leverparaphernalia
				switch(changethisparaphernalia){
					case "effort":$effortobject.attr('src', imgpath+'brick'+effortmagnitude+".png");
									break;
					case "load":$loadobject.attr('src', imgpath+'brick'+loadmagnitude+".png");
									break;
					case "effortarm":
						switch(effortarmmagnitude){
							case "5"  : $effortobject.css("right","14%"); break;
							case "10" : $effortobject.css("right","37%"); break;
							case "15" : $effortobject.css("right","60%"); break;
							case "20" : $effortobject.css("right","82%"); break;
							default : break;
						}; break;
					case "loadarm" : 
						switch(loadarmmagnitude){
							case "5"  : $loadobject.css("left","14%"); break;
							case "10" : $loadobject.css("left","37%"); break;
							case "15" : $loadobject.css("left","60%"); break;
							case "20" : $loadobject.css("left","82%"); break;
							default : break;
						}; break;
					default : break;
				}

				// rotate the plank with resultantforce times a decreament factor
				$leverparaphernaliacontainer.css({
					"-webkit-transform": "rotate("+resultantforce*0.05+"deg)",
					"transform": "rotate("+resultantforce*0.05+"deg)"
				});

				// if resultantforce is zero activate table row for record entry
				if( resultantforce == 0 ){
					checkthisstringinrecord = ""+effortmagnitude+effortarmmagnitude+loadmagnitude+loadarmmagnitude;
					
					if( $.inArray(checkthisstringinrecord, alreadyrecorded) < 0)
					{
						$rangeinputs.prop('disabled', true);
						$rangeinputs.css("cursor", "default");
						$timelyhints.html(data.string.p6instruction2);
						$recordkeep.css('opacity', '1');
					}

					else{
						$timelyhints.html(data.string.p6instruction3);
					}
				}
			});

			// event handler for recordkeep button
			$recordkeep.on('click', function() {
				$(this).css('opacity', '0');
				activaterecordrow++;
				
				$currentrow = $datarecords.children('tr:nth-of-type('+activaterecordrow+')');
				$currentrow.children('td.effortdata').text(effortmagnitude);
				$currentrow.children('td.effortarmdata').text(effortarmmagnitude);
				$currentrow.children('td.effortforcedata').text(effortforce);
				$currentrow.children('td.loaddata').text(loadmagnitude);
				$currentrow.children('td.loadarmdata').text(loadarmmagnitude);				
				$currentrow.children('td.loadforcedata').text(loadforce);
				$currentrow.children("td[class$=forcedata]").css('background-color', 'rgb(179, 216, 140)');

				alreadyrecorded.push(""+effortmagnitude+effortarmmagnitude+loadmagnitude+loadarmmagnitude);
				
				if(activaterecordrow < 4){
					$rangeinputs.prop('disabled',false);
					$rangeinputs.css("cursor", "col-resize");
					$timelyhints.html(data.string.p6instruction1);
				}

				else if(activaterecordrow >= 4){
					$timelyhints.html(data.string.p6instruction4);
					$flipcontainer.css('pointer-events', 'auto');
				}
			});

			// data for flipcontainer
			var frontpanevisible = true;

			$flipcontainer.on('click', function() {
				frontpanevisible == true ? 
					(
						$flipcontainer.attr('data-paneflip', 'showbackpane'),
						frontpanevisible = false
					) : 
					(
						$flipcontainer.attr('data-paneflip', 'showfrontpane'),
						frontpanevisible = true
					);
			});
		}
	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		// call navigation controller
		navigationcontroller();

		// call the template
		generaltemplate();

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developer : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
			previous slide button hide the footernotification */		
		// countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});
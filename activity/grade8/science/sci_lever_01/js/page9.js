var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
var imgpath = $ref+"/images/page9/";

var content=[
	{
		uppertextblock:[
			{
				textclass:"bigheadingtextstyle",
				textdata: data.string.lever+": "+data.string.numerical,
			}
		]
	},
	{
		hasheader: true,
		headerblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "multiplehighlightslide3",
				textclass:"headertextstyle2",
				textdata: data.string.p9question1
			}
		],
        contentblocknocenteradjust : true,
		uppertextblock:[
			{
				textclass:"solution",
				textdata: data.string.Solution
			},
			{
				textclass:"given",
				textdata: data.string.Given
			},
			{
				textclass:"given1",
				textdata: data.string.p9q1given1
			},
			{
				textclass:"given2",
				textdata: data.string.p9q1given2
			},
			{
				textclass:"given3",
				textdata: data.string.p9q1given3
			},
			{
				textclass:"wehave",
				textdata: data.string.Wehave
			},
			{
				textclass:"mechanicaladv",
				textdata: data.string.Mechanicaladvwidbracket
			},
			{
				textclass:"firstequal",
				textdata: data.string.equalsign
			},
			{
				textclass:"formulatop",
				textdata: data.string.loadwithbracket
			},
			{
				textclass:"formulabottom",
				textdata: data.string.effortapplied
			},
			{
				textclass:"mechanicaladv2",
				textdata: data.string.p7MA
			},
			{
				textclass:"secondequal",
				textdata: data.string.equalsign
			},
			{
				textclass:"answer",
				textdata: data.string.p9q1answer
			},
		],
		inputblockname: "inputblock",
		imageblock:[
			{
				// imagestoshow:[
					// {
						// imgclass:"clickmeload cssfadein",
						// imgsrc: "images/clickme_icon.png"
					// },
					// {
						// imgclass:"clickmeeffort",
						// imgsrc: "images/clickme_icon.png"
					// }
				// ]
			}
		],
		inputblock:[
			{
				input:[
					{
						inputdivname: "firstbox",
						inputname: "loadbox",
						unitname: "newton",
						unitvalue: "N"
					},
					{
						inputdivname: "secondbox",
						inputname: "effortbox",
						unitname: "newton",
						unitvalue: "N"
					}
				]
			},
			
		]
	},
	{
		hasheader: true,
		headerblock:[
			{
				datahighlightflag: true,
				// datahighlightcustomclass: "multiplehighlightslide3",
				textclass:"headertextstyle2",
				textdata: data.string.p9question2
			}
		],
        contentblocknocenteradjust : true,
		uppertextblock:[
			{
				textclass:"solution",
				textdata: data.string.Given
			},
			{
				textclass:"given1 newgivencss",
				textdata: data.string.p9q2given2
			},
			{
				textclass:"given2 newgivencss2",
				textdata: data.string.p9q2given1
			},
			{
				textclass:"given3 newgivencss3",
				textdata: data.string.p9q2given3
			},
			{
				textclass:"wehave newwehave",
				textdata: data.string.Wehave
			},
			{
				textclass:"mechanicaladv newpos",
				textdata: data.string.Mechanicaladvwidbracket
			},
			{
				textclass:"firstequal",
				textdata: data.string.equalsign
			},
			{
				textclass:"formulatop",
				textdata: data.string.loadwithbracket
			},
			{
				textclass:"formulabottom",
				textdata: data.string.effortapplied
			},
			{
				textclass:"formulabottom2",
				textdata: data.string.p6attribeffort
			},
			{
				textclass:"secondequal",
				textdata: data.string.equalsign
			},
			{
				textclass:"answer2",
				textdata: data.string.p9q2answer
			},
			{
				textclass:"answer3",
				textdata: data.string.p9q2answer2
			},
			{
				textclass:"answer4",
				textdata: data.string.p9q2answer3
			},
			{
				textclass:"answer5",
				textdata: data.string.p9q2answer4
			}
		],
		inputblockname: "inputblock",
		inputblock:[
			{
				input:[
					{
						inputdivname: "firstgiven1box",
						inputname: "givenbox1",
						unitname: "newton",
						unitvalue: "N"
					},
					{
						inputdivname: "secondgiven2box",
						inputname: "givenbox2",
					},
					{
						inputdivname: "firstbox2",
						inputname: "loadbox2",
						unitname: "newton",
						unitvalue: "N"
					},
					{
						inputdivname: "secondbox2",
						inputname: "effortbox2",
					}
				]
			},
			
		]
	},
	{
		hasheader: true,
		headerblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "multiplehighlightslide3",
				textclass:"headertextstyle2",
				textdata: data.string.p9question3
			}
		],
        contentblocknocenteradjust : true,
		uppertextblock:[
			{
				textclass:"solution",
				textdata: data.string.Given
			},
			{
				textclass:"given4",
				textdata: data.string.p9q3given4
			},
			{
				textclass:"given5",
				textdata: data.string.p9q3given5
			},
			{
				textclass:"given6",
				textdata: data.string.p9q3given6
			},
			{
				textclass:"given7",
				textdata: data.string.p9q1given3
			},
			{
				textclass:"given8",
				textdata: data.string.p9q3given7
			},
			{
				textclass:"given9",
				textdata: data.string.p9q2given1
			},
			{
				textclass:"given10",
				textdata: data.string.p9q2given1_2
			},
			{
				textclass:"wehave newwehave",
				textdata: data.string.Wehave
			},
			{
				textclass:"mechanicaladv newpos",
				textdata: data.string.Mechanicaladvwidbracket
			},
			{
				textclass:"mechanicaladv2 newpos",
				textdata: data.string.p7MA
			},
			{
				textclass:"firstequal",
				textdata: data.string.equalsign
			},
			{
				textclass:"formulatop",
				textdata: data.string.loadwithbracket
			},
			{
				textclass:"formulabottom",
				textdata: data.string.effortapplied
			},
			{
				textclass:"formulabottom2",
				textdata: data.string.p6attribeffort
			},
			{
				textclass:"secondequal2",
				textdata: data.string.equalsign
			},
			{
				textclass:"answer2_1",
				textdata: data.string.p9q3answer1
			},
			{
				textclass:"velocityratio newpos",
				textdata: data.string.p9q3given1
			},
			{
				textclass:"formulatopVR",
				textdata: data.string.p9q3formula1
			},
			{
				textclass:"formulabottomVR",
				textdata: data.string.p9q3formula2
			},
			{
				textclass:"velocityratio3",
				textdata: data.string.p9VR
			},
			{
				textclass:"secondequalVR",
				textdata: data.string.equalsign
			},
			{
				textclass:"answerVR",
				textdata: data.string.p9q3answer2
			},
			{
				textclass:"efficiency",
				textdata: data.string.p9q3given8
			},
			{
				textclass:"formulatopE",
				textdata: data.string.p9q3given8_1
			},
			{
				textclass:"formulabottomE",
				textdata: data.string.p9q3given8_2
			},
			{
				textclass:"times",
				textdata: data.string.p9q3timeshundred
			},
			{
				textclass:"efficiency2",
				textdata: data.string.p9q3eta
			},
			{
				textclass:"secondequalE",
				textdata: data.string.equalsign
			},
			{
				textclass:"timesagain",
				textdata: data.string.p9q3timeshundred
			},
			{
				textclass:"answerE",
				textdata: data.string.p9q3answer3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					// {
						// imgclass:"clickmeEA cssfadein",
						// imgsrc: imgpath+"clickme_icon.png"
					// },
					// {
						// imgclass:"clickmeLA",
						// imgsrc: imgpath+"clickme_icon.png"
					// },
					{
						imgclass:"clickmeMA",
						imgsrc: imgpath+"clickme_icon.png"
					},
					{
						imgclass:"clickmeVR",
						imgsrc: imgpath+"clickme_icon.png"
					},
					{
						imgclass:"clickmeE",
						imgsrc: imgpath+"clickme_icon.png"
					}
				]
			}
		],
		inputblockname: "inputblock2",
		inputblock:[
			{
				input:[
					{
						inputdivname: "loadgivenbox",
						inputname: "loadgiven",
						unitname: "newton",
						unitvalue: "N"
					},
					{
						inputdivname: "effortgivenbox",
						inputname: "effortgiven",
						unitname: "newton",
						unitvalue: "N"
					},
					{
						inputdivname: "Effbox1",
						inputname: "MAbox",
					},
					{
						inputdivname: "Effbox2",
						inputname: "VRbox",
					},
					{
						inputdivname: "VRbox3",
						inputname: "loadboxVR3",
						unitname: "newton",
						unitvalue: "m"
					},
					{
						inputdivname: "VRbox4",
						inputname: "effortboxVR3",
						unitname: "newton",
						unitvalue: "m"
					},
					{
						inputdivname: "firstbox3",
						inputname: "loadbox3",
						unitname: "newton",
						unitvalue: "N"
					},
					{
						inputdivname: "secondbox3",
						inputname: "effortbox3",
						unitname: "newton",
						unitvalue: "N"
					},
					
				]
			},
			
		]
	}
];

$(function () {	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("inputcontent", $("#inputcontent-partial").html());
	 
	 
	 /*===============================================
	 =            data highlight function            =
	 ===============================================*//**
			What it does:
			- send an element where the function has to see
				for data to highlight
			- this function searches for all nodes whose
			    data-highlight element is set to true 
			-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
			- if provided with data-highlightcustomclass value for highlight it
				applies the custom class or else uses parsedstring class
			
			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;
				
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {	
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/				
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**		
			How to:
			- First set any html element with 
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/	 	 
	 /**	 
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that 
	 		footernotification is called for continue button to navigate to exercise
	  */
	
	 /**	  
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if 
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is 
	   */  
	 
	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			
			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;			
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**		
			How to:
			- Just call instructionblockcontroller() from the template		
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);					
				});
			}
		}	
	/*=====  End of InstructionBlockController  ======*/
		
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	
	/*=================================================
	=            general template function            =
	=================================================*/		
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
				
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		if (countNext==0) {
			$nextBtn.show(0);
		};


		if(countNext == 1){
			img1 = $('<img class= "clickmeload" src= "images/clickme_icon.png"/>');
			img2 = $('<img class= "clickmeeffort" src= "images/clickme_icon.png"/>');
			$(".headertextstyle2>.multiplehighlightslide3:nth-child(1)").append(img1);
			$(".headertextstyle2>.multiplehighlightslide3:nth-child(2)").append(img2);
		
			//Ashish: this is done because putting this class in img1 internal html isnot behaving in the expected manner
			$(".clickmeload").toggleClass("cssfadein");
			
			$(".clickmeload").on("click", function() {
				$(this).hide(0);
				$(".multiplehighlightslide3:nth-of-type(1)").addClass("removebackground");
				$(".given1").addClass("cssfadein2");
				$(".clickmeeffort").addClass("cssfadein3");
			});

			$(".clickmeeffort").on("click", function() {
				$(this).hide(0);
				$(".multiplehighlightslide3:nth-of-type(2)").addClass("removebackground");
				$(".given2").addClass("cssfadein2");
				$(".given3").addClass("cssfadein3");
			}); 

		}else if(countNext == 3){
			img1 = $('<img class="clickmeEA cssfadein" src="images/clickme_icon.png">');
			img2 = $('<img class="clickmeLA" src="images/clickme_icon.png">'); 
			$(".headertextstyle2>.multiplehighlightslide3:nth-child(1)").append(img1);
			$(".headertextstyle2>.multiplehighlightslide3:nth-child(2)").append(img2);
			
			$(".clickmeEA").on("click", function() {
				$(this).hide(0);
				$(".multiplehighlightslide3:nth-of-type(1)").addClass("removebackground");
				$(".given4").addClass("cssfadein2");
				$(".clickmeLA").addClass("cssfadein3");
			}); 
			
			$(".clickmeLA").on("click", function() {
				$(this).hide(0);
				$(".multiplehighlightslide3:nth-of-type(2)").addClass("removebackground");
				$(".given5").addClass("cssfadein2");
				$(".given9, .given10, .loadgivenbox, .effortgivenbox").addClass("cssfadein3");
			}); 

			
		}
		// common animationend functions
		$(".given3").on(animationend, function(){ 
			$(".wehave").addClass("cssfadein");
		});

		$(".wehave").on(animationend, function(){ 
			$(".mechanicaladv, .firstequal, .formulatop, .formulabottom").addClass("cssfadein");
			$(".mechanicaladv2, .secondequal, .inputblock").addClass("cssfadein3");
			$(".inputblock > input").addClass("zindexfront");
		});

		// first numerical question part


		if (countNext==1) {
			var correctans1 = false;
			var correctans2 = false;
			$(".loadbox").keyup(function(){
				var inputvalue = $(this).val();
				$(this).val($(this).val().replace(/[^0-9.]/g,''));

				if (inputvalue ==200) {
					$(this).addClass("correct");
					$(this).removeClass("incorrect");
					correctans1 =true;
					if (correctans1 == true && correctans2 == true) {
						$(".answer").addClass("cssfadein");
						$nextBtn.delay(2000).show(0);
					};
				}
				else{
					$(this).addClass('incorrect');
				}
			});

			$(".effortbox").keyup(function(){
				$(this).val($(this).val().replace(/[^0-9.]/g,''));
				var inputvalue = $(this).val();
				if (inputvalue ==50) {
					$(this).removeClass("incorrect");
					$(this).addClass("correct");
					correctans2 =true;
					if (correctans1 == true && correctans2 == true) {
						$(".answer").addClass("cssfadein");
						$nextBtn.delay(2000).show(0);
					};
				}
				else{
					$(this).addClass('incorrect');
				}
			});
		};
		// numerical one ends


		// numerical two starts

		if (countNext==2) {
			$(".inputblock").addClass('showthings');
			$(".givenbox1").keyup(function(){ 
				$(this).val($(this).val().replace(/[^0-9.]/g,''));
				var givenvalue1 = $(this).val();
					if (givenvalue1 ==100) {
						$(this).addClass("correct");
						$(this).removeClass("incorrect");
						correctgiven1 =true;
						if (correctgiven1 == true && correctgiven2 == true) {
							$(".wehave").addClass('cssfadein2');
							$(".mechanicaladv, .firstequal, .formulatop, .formulabottom").addClass("cssfadein");
							$(".formulabottom2, .secondequal, .firstbox2, .secondbox2").addClass("cssfadein3");
						};
					}
					else{
						$(this).addClass('incorrect');
					}
			});

			$(".givenbox2").keyup(function(){ 
				$(this).val($(this).val().replace(/[^0-9.]/g,''));
				var givenvalue2 = $(this).val();
					if (givenvalue2 == 2) {
						$(this).addClass("correct");
						$(this).removeClass("incorrect");
						correctgiven2 =true;
						if (correctgiven1 == true && correctgiven2 == true) {
							$(".wehave").addClass('cssfadein2');
							$(".mechanicaladv, .firstequal, .formulatop, .formulabottom").addClass("cssfadein");
							$(".formulabottom2, .secondequal, .firstbox2, .secondbox2").addClass("cssfadein3");
						};
					}
					else{
						$(this).addClass('incorrect');
					}
			});

			var correctans2_1 = false;
			var correctans2_2 = false;
			$(".effortbox2").keyup(function(){ 
				$(this).val($(this).val().replace(/[^0-9.]/g,''));
				var ansvalue = $(this).val();
				if (ansvalue == 2) {
					$(this).addClass("correct");
					$(this).removeClass("incorrect");
						correctans2_1 =true;
						if (correctans2_1 == true && correctans2_2 == true) {
							$(".answer2, .answer3, .answer4, .answer5").addClass("cssfadein2");
							$nextBtn.show(0);
						};
				}
				else{
						$(this).addClass('incorrect');
					}
			});

			$(".loadbox2").keyup(function(){ 
				$(this).val($(this).val().replace(/[^0-9.]/g,''));
				var ansvalue2 = $(this).val();
				if (ansvalue2 == 100) {
					$(this).addClass("correct");
						$(this).removeClass("incorrect");
						correctans2_2 =true;
						if (correctans2_1 == true && correctans2_2 == true) {
							$(".answer2, .answer3, .answer4, .answer5").addClass("cssfadein2");
							$nextBtn.show(0);
						};
					}
				else{
						$(this).addClass('incorrect');
					}
			});
		};

		// numerical two ends

		// numerical three starts
		

		var correctgivenans1 = false;
		var correctgivenans2 = false;
		$(".loadgiven").keyup(function(){
			$(this).val($(this).val().replace(/[^0-9.]/g,''));
			var givenvalue = $(this).val();
			if (givenvalue == 200) {
				$(this).addClass("correct");
				$(this).removeClass("incorrect");
				correctgivenans1 =true;
				if (correctgivenans1 == true && correctgivenans2 == true) {
					$(".clickmeMA").addClass("cssfadein");
				};
			}
			else{
				$(this).addClass('incorrect');
			}
		});

		$(".effortgiven").keyup(function(){
			$(this).val($(this).val().replace(/[^0-9.]/g,''));
			var givenvalue = $(this).val();
			if (givenvalue == 50) {
				$(this).addClass("correct");
				$(this).removeClass("incorrect");
				correctgivenans2 =true;
				if (correctgivenans1 == true && correctgivenans2 == true) {
					$(".clickmeMA").addClass("cssfadein");
				};
			}
			else{
				$(this).addClass('incorrect');
			}
		});


		$(".mechanicaladv2").on(animationend, function(){ 
			$(".secondequal2, .firstbox3, .secondbox3").addClass("cssfadein");
		});

		var secondinput1 = false;
		var secondinput2 = false;
		$(".loadbox3").keyup(function(){ 
			$(this).val($(this).val().replace(/[^0-9.]/g,''));
			var loadvalue = $(this).val();
			if (loadvalue == 200) {
				$(this).addClass("correct");
				$(this).removeClass("incorrect");
				secondinput1 =true;
				if (secondinput1 == true && secondinput2 == true) {
					$(".answer2_1, .clickmeVR").addClass("cssfadein");
					$(".given7").text(data.string.p9q3answer1_1);
					$(".given7").addClass('removebacandbox');
				};
			}
			else{
				$(this).addClass('incorrect');
			}
		});

		$(".effortbox3").keyup(function(){ 
			$(this).val($(this).val().replace(/[^0-9.]/g,''));
			var loadvalue = $(this).val();
			if (loadvalue == 50) {
				$(this).addClass("correct");
				$(this).removeClass("incorrect");
				secondinput2 =true;
				if (secondinput1 == true && secondinput2 == true) {
					$(".answer2_1, .clickmeVR").addClass("cssfadein");
					$(".given7").text(data.string.p9q3answer1_1);
					$(".given7").addClass('removebacandbox');

				};
			}
			else{
				$(this).addClass('incorrect');
			}
		});

		$(".clickmeMA").on("click", function() {
			$(this).hide(0);
			$(".wehave").addClass("cssfadein");
		}); 
			

		$(".clickmeVR").on("click", function(){ 
			$(this).hide(0);
			$(".mechanicaladv, .mechanicaladv2, .firstequal, .formulatop, .formulabottom, .formulabottom2, .secondequal2, .answer2_1, .firstbox3, .secondbox3").hide(0);
			$(".velocityratio, .formulatopVR, .formulabottomVR").addClass('cssfadein');
			$(".velocityratio3, .secondequalVR, .VRbox3, .VRbox4").addClass("cssfadein3");
		});

		var VRcorrect1 = false;
		var VRcorrect2 = false;
		$(".loadboxVR3").keyup(function(){ 
			var vrvalue = $(this).val();
			if (vrvalue == 5) {
				$(this).addClass("correct");
				$(this).removeClass("incorrect");
				VRcorrect1 =true;
				if (VRcorrect1 == true && VRcorrect2 == true) {
					$(".answerVR, .clickmeE").addClass("cssfadein");
					$(".given6").text(data.string.p9q3answer2_1);
					$(".given6").addClass('removebacandbox');

				};
			}
			else{
				$(this).addClass('incorrect');
			}
		});
		$(".effortboxVR3").keyup(function(){ 
			var vrvalue = $(this).val();
			if (vrvalue == 1) {
				$(this).addClass("correct");
				$(this).removeClass("incorrect");
				VRcorrect2 =true;
				if (VRcorrect1 == true && VRcorrect2 == true) {
					$(".answerVR, .clickmeE").addClass("cssfadein");
					$(".given6").text(data.string.p9q3answer2_1);
					$(".given6").addClass('removebacandbox');
				};
			}
			else{
				$(this).addClass('incorrect');
			}
		});

		$(".clickmeE").on("click", function(){
			$(this).hide(0);
			$(".velocityratio, .formulatopVR, .formulabottomVR, .velocityratio3, .secondequalVR, .VRbox3, .VRbox4, .answerVR").hide(0);
			$(".efficiency, .formulatopE, .formulabottomE, .times").addClass('cssfadein');
			$(".efficiency2, .secondequalE, .timesagain, .Effbox1, .Effbox2").addClass("cssfadein3");
		});

		var effans1 = false;
		var effans2 = false;
		$(".MAbox").keyup(function(){ 
			var effvalue = $(this).val();
			if (effvalue == 4) {
				$(this).addClass("correct");
				$(this).removeClass("incorrect");
				effans1 =true;
				if (effans1 == true && effans2 == true) {
					$(".answerE").addClass("cssfadein");
					$(".given8").text(data.string.p9q3answer3_1);
					$(".given8").addClass('removebacandbox');
				};
			}
			else{
				$(this).addClass('incorrect');
			}
		});

		$(".VRbox").keyup(function(){ 
			var effvalue = $(this).val();
			if (effvalue == 5) {
				$(this).addClass("correct");
				$(this).removeClass("incorrect");
				effans2 =true;
				if (effans1 == true && effans2 == true) {
					$(".answerE").addClass("cssfadein");
					$(".given8").text(data.string.p9q3answer3_1);
					$(".given8").addClass('removebacandbox');
				};
			}
			else{
				$(this).addClass('incorrect');
			}
		});
	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		// call navigation controller
		navigationcontroller();

		// call the template
		generaltemplate();

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developer : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable keyup here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
			previous slide button hide the footernotification */		
		// countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});
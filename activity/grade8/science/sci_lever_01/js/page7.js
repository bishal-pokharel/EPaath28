var SPACE = data.string.spacecharacter;
var defimg = "images/definitionpage/definition4.png";

var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
var imgpath = $ref+"/images/page7/";
var diyImg = "images/diy/diy1.png";

var content=[
	{
		uppertextblock:[
			{
				textclass:"bigcustomheading customposition",
				textdata: data.string.lever+": "+data.string.Mechanicaladv,
			}
		]
	},
	{
		hasheader:true,
        headerblock:[
        	{
        		textclass:"headertextstyle",
				textdata: data.string.p7text1
        	}
        ],
        contentblocknocenteradjust : true,
        imageblock:[
        	{
        		imagestoshow:[
        			{
        				imgclass:"withoutlever",
        				imgsrc: imgpath+"lever2.gif"
        			}
        		]
        	}
        ]
    },
    {
		hasheader:true,
        headerblock:[
        	{
        		textclass:"headertextstyle",
				textdata: data.string.p7text2
        	}
        ],
        contentblocknocenteradjust : true,
        imageblock:[
        	{
        		imagestoshow:[
        			{
        				imgclass:"lever",
        				imgsrc: imgpath+"lever.gif"
        			}
        		]
        	}
        ]
    },
    {
    	uppertextblock:[
    		{
        		textclass:"paperstacktextstyle",
				textdata: data.string.p7text3
        	}
    	]
    },
    {
    	hasheader:true,
		headerblock:[
    		{
        		textclass:"headertextstyle",
				textdata: data.string.p7text4
        	}
    	],
        // contentblocknocenteradjust : true,
        uppertextblock:[
        	{
        		textclass:"onlyparatextstyle",
        		textdata: data.string.p7text5
        	}
        ],
    	imageblock:[
    		{
    			imagestoshow:[
    				{
    					imgclass:"fourimgtogether cssfadein",
    					imgsrc: imgpath+"books.png"
    				},
    				{
    					imgclass:"fourimgtogether cssfadein2",
    					imgsrc: imgpath+"springbalance.jpg"
    				},
    				{
    					imgclass:"fourimgtogether cssfadein3",
    					imgsrc: imgpath+"wood.png"
    				},
    				{
    					imgclass:"fourimgtogether cssfadein4",
    					imgsrc: imgpath+"rope.png"
    				}
    			]
    		}
    	]
    },
    {
        hasheader: true,
        headerblock:[
            {
                textclass: "headertextstyle",
                textdata: data.string.p7text6
            }
        ],
        contentblocknocenteradjust : true,
        uppertextblock:[
            {
                textclass: "arrowbox cssfadein",
                textdata: data.string.p7instruction1
            },
            {
                textclass: "arrowbox2 cssfadein2",
                textdata: data.string.p7instruction2
            }
        ],
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass: "showbooks",
                        imgsrc: imgpath+"books02.png"
                    },
                    {
                        imgclass: "showspringbalance",
                        imgsrc: imgpath+"springbalance.jpg"
                    },
                ]
            }
        ],
        lowertextblock:[
            {
                textclass: "bottomparagraph cssfadein3",
                textdata: data.string.p7instruction3
            }
        ]
    },
    {
        hasheader: true,
        headerblock:[
            {
                textclass: "headertextstyle",
                textdata: data.string.p7text6
            }
        ],
        contentblocknocenteradjust : true,
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass: "booksliftnormal",
                        imgsrc: imgpath+"machenical-adv01_Big.gif"
                    }
                ],
                imagelabels:[
                    {
                        imagelabelclass: "labelnewton cssfadein4",
                        imagelabeldata: data.string.twentyNewton
                    }
                ]
            }
        ],
        lowertextblock:[
            {
                textclass: "bottomparagraph cssfadein3",
                textdata: data.string.p7instruction4
            }
        ]
    },
    {
        hasheader: true,
        headerblock:[
            {
                textclass: "headertextstyle",
                textdata: data.string.p7text6
            }
        ],
        contentblocknocenteradjust : true,
        uppertextblock:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "bigfont",
                textclass: "paragraphtext",
                textdata: data.string.p7instruction5
            }
        ],
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass: "booksliftnormal",
                        imgsrc: imgpath+"machenical-adv02_Big.gif"
                    }
                ],
                imagelabels:[
                    {
                        imagelabelclass: "labelnewton2 cssfadein4",
                        imagelabeldata: data.string.sixNewton
                    }
                ]
            }
        ],
        lowertextblock:[
            {
                textclass: "bottomparagraph cssfadein3",
                textdata: data.string.p7instruction6
            }
        ]
    },
    {
        contentblocknocenteradjust : true,
        uppertextblock:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "bigfont",
                textclass: "paragraphtext",
                textdata: data.string.p7instruction7
            }
        ],
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass: "booksliftspring",
                        imgsrc: imgpath+"machenical-adv01_Small.gif"
                    },
                    {
                        imgclass: "booksliftlever",
                        imgsrc: imgpath+"machenical-adv02_Small.gif"
                    }
                ],
                imagelabels:[
                    {
                        imagelabelclass: "labelnewton3 cssfadein4",
                        imagelabeldata: data.string.twentyNewton
                    },
                    {
                        imagelabelclass: "labelnewton4 cssfadein4",
                        imagelabeldata: data.string.sixNewton
                    }
                ]
            }
        ],
    },
    {
    	definitionblock:[
    		{
    			definitionImageSource:defimg,
    			definitionTextData: data.string.Mechanicaladv,
    			definitionInstructionData: data.string.p7text7
    		}
    	]
    },
    {
        uppertextblock:[
            {
                textclass: "onlyparatextstyle",
                textdata: data.string.p7lasttext
            }
        ]
    },
    {
    	hasheader:true,
		headerblock:[
    		{
        		textclass:"headertextstyle",
				textdata: data.string.p7text8
        	}
    	],
        contentblocknocenteradjust : true,

    	uppertextblock:[
    		{
    			textclass:"formula1 cssfadein",
    			textdata: data.string.p7text9
    		},
    		{
    			textclass:"equal cssfadein2",
    			textdata: data.string.equalsign
    		},
    		{
    			textclass:"formula2 cssfadein3",
    			textdata: data.string.p7text10
    		},
    		{
    			textclass:"formula3 cssfadein4",
    			textdata: data.string.p7text11
    		},
			{
    			textclass:"therefore",
    			textdata: data.string.p7therefore
    		},
    		{
    			textclass:"formula4",
    			textdata: data.string.p7MA
    		},
    		{
    			textclass:"equal2",
    			textdata: data.string.equalsign
    		},
    		{
    			textclass:"formula5",
    			textdata: data.string.p7text13
    		},
    		{
    			textclass:"formula6",
    			textdata: data.string.p7text14
    		}
    	]
    },
    {
        hasheader:true,
        headerblock:[
            {
                textclass:"headertextstyle",
                textdata: data.string.p7text8
            }
        ],
        contentblocknocenteradjust : true,

        uppertextblock:[
            {
                textclass:"formula1 showformula",
                textdata: data.string.p7text9
            },
            {
                textclass:"equal showformula",
                textdata: data.string.equalsign
            },
            {
                textclass:"formula2 showformula",
                textdata: data.string.p7text10
            },
            {
                textclass:"formula3 showformula",
                textdata: data.string.p7text11
            },
            {
                textclass:"therefore showformula",
                textdata: data.string.p7therefore
            },
            {
                textclass:"formula4 showformulawidborder",
                textdata: data.string.p7MA
            },
            {
                textclass:"equal2 showformula",
                textdata: data.string.equalsign
            },
            {
                textclass:"formula5 showformula",
                textdata: data.string.p7text13
            },
            {
                textclass:"formula6 showformula",
                textdata: data.string.p7text14
            },
            {
                textclass:"arrowline cssfadein",
                textdata: data.string.p7text15
            },
            {
                textclass:"arrowline2",
                textdata: data.string.p7text16
            }
        ]
    },
    {
        hasheader:true,
        headerblock:[
            {
                textclass:"headertextstyle",
                textdata: data.string.p7text17
            }
        ],
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass: "pushinggif",
                        imgsrc: imgpath+"pushing.gif"
                    }
                ]
            }
        ],
        lowertextblock:[
            {
                textclass: "bottomparatextstyle",
                textdata: data.string.p7text18+ data.string.p7text19
            }
        ]
    },
    {
        uppertextblock:[
            {
                textclass: "paperstacktextstyle",
                textdata: data.string.p7text20
            }
        ]
    },
    {
        hasheader: true,
        headerblock:[
            {
                textclass:"headertextstyle customcss",
                textdata: data.string.p7text21
            }
        ],
        contentblocknocenteradjust : true,
        uppertextblock:[
            {
                textclass: "seesolution",
                textdata: data.string.p7text22
            },
            {
                textclass:"solution",
                textdata: data.string.Solution
            },
            {
                textclass:"given",
                textdata: data.string.Given
            },
            {
                textclass:"given1 cssfadein",
                textdata: data.string.p7text23
            },
            {
                textclass:"given2 cssfadein",
                textdata: data.string.p7text24
            },
            {
                textclass:"given3 cssfadein",
                textdata: data.string.p7text25
            },
            {
                textclass:"wehave",
                textdata: data.string.Wehave
            },
            {
                textclass:"MA1",
                textdata: data.string.p7text9
            },
            {
                textclass:"equalMA",
                textdata: data.string.equalsign
            },
            {
                textclass:"topformula",
                textdata: data.string.p7text10
            },
            {
                textclass:"bottomformula",
                textdata: data.string.p7text11
            },
            {
                textclass:"MA2",
                textdata: data.string.p7MA
            },
            {
                textclass:"equalMA2",
                textdata: data.string.equalsign
            },
            {
                textclass:"topformula2",
                textdata: data.string.p7text27
            },
            {
                textclass:"bottomformula2",
                textdata: data.string.p7text28
            },
            {
                textclass:"therefore2",
                textdata: data.string.p7therefore
            },
            {
                textclass:"MA3",
                textdata: data.string.p7MA
            },
            {
                textclass:"equalMA3",
                textdata: data.string.equalsign
            },
            {
                textclass:"answer",
                textdata: data.string.p7text29
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass: "orangehighlight",
                textclass:"answertext",
                textdata: data.string.p7text30
            }
        ]
    }
];

$(function () {	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("usageslowrevealcontent", $("#usageslowrevealcontent-partial").html());
	 Handlebars.registerPartial("diysplashcontent", $("#diysplashcontent-partial").html()); 
	 Handlebars.registerPartial("diycontent", $("#diycontent-partial").html());
	 Handlebars.registerPartial("instructioncontent", $("#instructioncontent-partial").html());
	 Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	 
	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		/**

			What it does:
			- send an element where the function has to see
				for data to highlight
			- this function searches for all nodes whose
			    data-highlight element is set to true 
			-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
			- if provided with data-highlightcustomclass value for highlight it
				applies the custom class or else uses parsedstring class
			
			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;
				
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {	
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/				
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**		
			How to:
			- First set any html element with 
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/	 	 
	 /**	 
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that 
	 		footernotification is called for continue button to navigate to exercise
	  */
	
	 /**	  
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if 
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is 
	   */  
	 
	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			
			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;			
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**		
			How to:
			- Just call instructionblockcontroller() from the template		
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);					
				});
			}
		}	
	/*=====  End of InstructionBlockController  ======*/
		
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	
	/*=================================================
	=            general template function            =
	=================================================*/		
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		instructionblockcontroller();
				
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

        $(".formula3").on(animationend, function(){
            $(".therefore, .formula4").addClass('cssfadein');
            $(".equal2").addClass('cssfadein2');
            $(".formula5").addClass('cssfadein3');
            $(".formula6").addClass('cssfadein4');
        });

		$(".headingstyle2").on(animationend, function(){
			$(".headingstyle2").addClass("curl");
		});

        $(".arrowline").on(animationend, function(){
            $(".arrowline2").addClass("cssfadein");
        });


        $(".given3").on(animationend, function(){
            $(".seesolution").addClass('moveleftsolution');
        });

        $(".seesolution").on("click", function(){ 
            $(this).hide(0);
            $(".wehave").addClass("cssfadein");
            $(".MA1, .equalMA").addClass("cssfadein2");
            $(".topformula").addClass("cssfadein3");
            $(".bottomformula").addClass("cssfadein4");
        });
        $(".bottomformula").on(animationend, function(){ 
            $(".MA2, .equalMA2").addClass("cssfadein");
            $(".topformula2").addClass("cssfadein2");
            $(".bottomformula2").addClass("cssfadein3");
        });

        $(".bottomformula2").on(animationend, function(){ 
            $(".therefore2, .MA3, .equalMA3").addClass("cssfadein");
            $(".answer").addClass("cssfadein2");
            $(".answertext").addClass("cssfadein3");
        });
	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		// call navigation controller
		navigationcontroller();

		// call the template
		generaltemplate();

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developer : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
			previous slide button hide the footernotification */		
		// countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});
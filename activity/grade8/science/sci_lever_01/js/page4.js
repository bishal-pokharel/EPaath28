var SPACE = data.string.spacecharacter;
var imgpath = $ref+"/images/page4/";
var squirrelwhat = "images/lokharke/squirrel_what_animated.svg";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var content=[	
	{
		uppertextblock : [
			{
				textclass : "onlyparatextstyle",
				textdata  : data.string.p4text1,
			}
		],
	},
	{
		hasheaderblock : true,
		headerblock : [
			{	
				textclass : "headertextstyle",
				textdata  : data.string.thirdClassLever,
			}
		],
		uppertextblock : [
			{
				datahighlightflag: true,
				datahighlightcustomclass: "orangehighlight",
				textclass : "paratextstyle",
				textdata  : data.string.p4text2,
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass : "levertypeimgstyle",
						imgsrc   : imgpath+"03.png"
					}
				]
			}
		],
	},
	{
		hasheader:true,
		headerblock:[
			{
				textclass : "headertextstyle",
				textdata  : data.string.p4text3,
			}
		],
		contentblocknocenteradjust : true,
		uppertextblock : [
			{
				datahighlightflag: true,
				datahighlightcustomclass: "greenhighlight",
				textclass : "quespara cssfadein",
				textdata  : data.string.p4question,
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass : "wheelbarrow",
						imgsrc   : imgpath+"fishing.gif",						
					},
					{
						imgclass : "wheelbarrow2",
						imgsrc   : imgpath+"rod.png",						
					}
				]
			}
		],
		selectblock:[
			{
				selecttoshow:[
					{
						selectname:"effortinput"
					},
					{
						selectname:"loadinput"
					},
					{
						selectname:"fulcruminput"
					}
				]
			}
		]
	},
	{
		hasheaderblock : true,
		headerblock : [
			{	
				textclass : "headertextstyle",
				textdata  : data.string.thirdClassLever+" : "+data.string.p2examples,
			}
		],
		contentblocknocenteradjust : true,
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "arrowload",
						imgsrc: imgpath+"arrow-red.png"
					},
					{
						imgclass: "arroweffort",
						imgsrc: imgpath+"arrow-green.png"
					}
				]
			}
		],
		hovertodescribeblock:[
			{
				mainimgsrc : imgpath + "third-class-lever.png",
				leverpartslabel : [
					{
						labelname : "labeleffort",
						labeltext : data.string.effort
					},
					{
						labelname : "labeleffortarm",
						labeltext : data.string.effortarm
					},
					{
						labelname : "labelload",
						labeltext : data.string.load
					},
					{
						labelname : "labelloadarm",
						labeltext : data.string.loadarm
					},
					{
						labelname : "labelfulcrum",
						labeltext : data.string.fulcrum
					}
				],
				descriptionheadingtext : data.string.p2descriptiontext,
				instructiontext : data.string.p2instructiontext,
				instructiondata : data.string.p3instruction,
			}
		],
	},
	{
		uppertextblock:[
			{
				textclass : "bottomparatextstyle",
				textdata  : data.string.p4text4,
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass : "threeimgtogether",
						imgsrc   : imgpath+"broom.png",
					},
					{
						imgclass : "threeimgtogether",
						imgsrc   : imgpath+"tweezers.png",
					},
					{
						imgclass : "threeimgtogether",
						imgsrc   : imgpath+"fork.png",
					}
				]
			}
		]
	}
];

$(function () {	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   	 Handlebars.registerPartial("listcontent", $("#listcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("timetothinkcontent", $("#timetothinkcontent-partial").html());
	 Handlebars.registerPartial("diysplashcontent", $("#diysplashcontent-partial").html()); 
	 Handlebars.registerPartial("diycontent", $("#diycontent-partial").html());
	 Handlebars.registerPartial("selectcontent", $("#selectcontent-partial").html());
	 Handlebars.registerPartial("hovertodescribecontent", $("#hovertodescribecontent-partial").html());
	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		/**

			What it does:
			- send an element where the function has to see
				for data to highlight
			- this function searches for all nodes whose
			    data-highlight element is set to true 
			-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
			- if provided with data-highlightcustomclass value for highlight it
				applies the custom class or else uses parsedstring class
			
			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;
				
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {	
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/				
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**		
			How to:
			- First set any html element with 
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/	 	 
	 /**	 
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that 
	 		footernotification is called for continue button to navigate to exercise
	  */
	
	 /**	  
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if 
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is 
	   */  
	 
	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			
			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;			
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**		
			How to:
			- Just call instructionblockcontroller() from the template		
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);					
				});
			}
		}	
	/*=====  End of InstructionBlockController  ======*/
		
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	
	/*=================================================
	=            general template function            =
	=================================================*/		
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
				
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		$(".quespara").on(animationend, function(){
			$(".effortinput, .loadinput, .fulcruminput").addClass("cssfadein");
		});

		if(countNext==2){
			$nextBtn.hide(0);
		}
		// for inputblock 
		var loadvalue;
		var fulcrumvalue;
		var effortvalue;
		var correctcount1=false;
		var correctcount2=false;
		var correctcount3=false;

		var disableselect = $(".effortinput").disabled;

		$(".effortinput").change(function () {
			effortvalue= $(this).val();
				if (effortvalue=="effort") {
					$(".effortinput").addClass('correct');
					$(".effortinput").removeClass('incorrect');
                    $(".effortinput").addClass('removearrow');
					correctcount1=true;
					if (correctcount1==true && correctcount2==true && correctcount3==true) {
						$nextBtn.show(0);
						$(".wheelbarrow").addClass("cssfadein");
						$(".wheelbarrow2").addClass("cssfadeout");
					};
				}
				else{
					$(".effortinput").addClass('incorrect');
				}
		});

		$(".fulcruminput").change(function () {
				fulcrumvalue= $(this).val();
				if (fulcrumvalue=="fulcrum") {
					$(".fulcruminput").addClass('correct');
					$(".fulcruminput").removeClass('incorrect');
                    $(".fulcruminput").addClass('removearrow');
					correctcount2=true;
					if (correctcount1==true && correctcount2==true && correctcount3==true) {
						$nextBtn.show(0);
						$(".wheelbarrow").addClass("cssfadein");
						$(".wheelbarrow2").addClass("cssfadeout");
					};
				}
				else{
					$(".fulcruminput").addClass('incorrect');
				}
		});

		$(".loadinput").change(function () {
				loadvalue= $(this).val();
				if (loadvalue=="load") {
					$(".loadinput").addClass('correct');
					$(".loadinput").removeClass('incorrect');
                    $(".loadinput").addClass('removearrow');
					correctcount3=true;
					if (correctcount1==true && correctcount2==true && correctcount3==true) {
						$nextBtn.show(0);
						$(".wheelbarrow").addClass("cssfadein");
						$(".wheelbarrow2").addClass("cssfadeout");
					};
				}
				else{
					$(".loadinput").addClass('incorrect');
				}
		});


		$(".side1").on(animationend, function(){
			$(".headertextstyle2, .side2").addClass("cssfadein");
		});
		var $hovertodescribeblock = $board.find("div.hovertodescribeblock");
		var check1 = false;
		var check2 = false;
		var check3 = false;
		var check4 = false;
		var check5 = false;

		// if hoverdescribe block is present
		if($hovertodescribeblock){
			/*var descriptioncontent = [
				effort : [data.string.effort]
			]*/
			var $hovercontents = $hovertodescribeblock.find("div.hovercontents");
			var $labels = $hovercontents.find("label");

			var $discriptioncontents = $hovertodescribeblock.find("div.discriptioncontents");
			var $leverpartheading = $discriptioncontents.find("p.leverpartheading");
			var $leverpartdescription = $discriptioncontents.find("p.leverpartdescription");
			var $leverpartimageeffortarm = $hovercontents.find("div.effortarm");
            var $leverpartimageloadarm = $hovercontents.find("div.loadarm");
            var $leverpartimagefulcrum = $hovercontents.find("div.fulcrum");

			$labels.hover(function() {
				/* Stuff to do when the mouse enters the element */
				$(this).css('color', 'red');

				switch($(this).attr('class')){
					case "labeleffort"    : 
											// $leverpartheading.html(data.string.effort+":");
											// $leverpartdescription.html(data.string.p3effortdescription);
											$(".arroweffort").addClass("opacityfull");
											check1 = true;
											if (check1 == true && check2 == true && check3 == true && check4 == true && check5 == true) {
									        	$nextBtn.show(0);
									        	$(".leverpartheading").text(data.string.p4instruction);
									    	};
									    	break;
					case "labeleffortarm" : 
											// $leverpartheading.html(data.string.effortarm+":");
											// $leverpartdescription.html(data.string.p3effortarmdescription);
                                            $leverpartimageeffortarm.addClass("opacityfull");
                                            check2 = true;
											if (check1 == true && check2 == true && check3 == true && check4 == true && check5 == true) {
									        	$nextBtn.show(0);
									        	$(".leverpartheading").text(data.string.p4instruction);
									    	};
									    	break;
					case "labelload"      : 
											// $leverpartheading.html(data.string.load+":");
											// $leverpartdescription.html(data.string.p3loaddescription);
											$(".arrowload").addClass("opacityfull");
											check3 = true;
											if (check1 == true && check2 == true && check3 == true && check4 == true && check5 == true) {
									        	$nextBtn.show(0);
									        	$(".leverpartheading").text(data.string.p4instruction);
									    	};
									    	break;
					case "labelloadarm"   : 
											// $leverpartheading.html(data.string.loadarm+":");
                                            $leverpartimageloadarm.addClass("opacityfull");
											// $leverpartdescription.html(data.string.p3loadarmdescription);
											check4 = true;
											if (check1 == true && check2 == true && check3 == true && check4 == true && check5 == true) {
									        	$nextBtn.show(0);
									        	$(".leverpartheading").text(data.string.p4instruction);
									    	};
											break;
					case "labelfulcrum"   : 
											// $leverpartheading.html(data.string.fulcrum+":");
                                            $leverpartimagefulcrum.addClass('opacityfull');
											// $leverpartdescription.html(data.string.p3fulcrumdescription);
											check5 = true;
											if (check1 == true && check2 == true && check3 == true && check4 == true && check5 == true) {
									        	$nextBtn.show(0);
									        	$(".leverpartheading").text(data.string.p4instruction);
									    	};
									    	break;
					default : break;
				}

			}, function() {
				/* Stuff to do when the mouse leaves the element */
				$(this).css('color', 'black');
                $leverpartimageeffortarm.removeClass("opacityfull");                                            
                $leverpartimageloadarm.removeClass("opacityfull");
                $leverpartimagefulcrum.removeClass('opacityfull');
				$(".arrowload").removeClass("opacityfull");
				$(".arroweffort").removeClass("opacityfull");

			});
		}
	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		// call navigation controller
		navigationcontroller();

		// call the template
		generaltemplate();

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developer : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
			previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});
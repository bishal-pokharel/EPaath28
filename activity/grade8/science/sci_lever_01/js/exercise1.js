// prototype for array shuffle
Array.prototype.shufflearray = function(){
  var i = this.length-1, j, temp;
	    while(--i > 0){
	        j = Math.floor(Math.random() * (i+1));
	        temp = this[j];
	        this[j] = this[i];
	        this[i] = temp;
	    }
	    return this;
};

var imgpath = $ref+"/images/exercise1/";
var correctimg = "images/correct.png";
var incorrectimg = "images/wrong.png";
var congratulationimgarray = [
	"images/quizcongratulation/gradea.png",
	"images/quizcongratulation/gradeb.png",
	"images/quizcongratulation/gradec.png",
];
var tempcontent=[
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofimgoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e1question1,
		optionsdata : [
				{ datafornotcorrect: "incorrect1", pidname: "imgoptionp1", optionstext: data.string.e1ans1, 
					imgoption:[
						{
							imgoptionname : "optionimg1", 
							imgoptionsrc: imgpath+"q01.png" 
						},
					]
				},
				{ datafornotcorrect: "incorrect2", pidname: "imgoptionp2", optionstext: data.string.e1ans2,
					imgoption:[
							{
								imgoptionname : "optionimg2", 
								imgoptionsrc: imgpath+"q01a.png" 
							},
						]
				},
				{ pidname: "imgoptionp3", optionstext: data.string.e1ans3, 
					imgoption:[
							{
								imgoptionname : "optionimg3", 
								imgoptionsrc: imgpath+"q01b.png" 
							},
						], isdatacorrect : true, 
				},
			]
	},
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofimgoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e1question2,
		// questionimgsrc      : imgpath+"period5.svg",
		optionsdata : [
				{ 
					pidname: "imgoptionp1", 
					optionstext: data.string.e1ans4,
					imgoption:[
						{
							imgoptionname : "animimg1_1", 
							imgoptionsrc: imgpath+"q02d.png" 
						},
						{
							imgoptionname : "animimg1", 
							imgoptionsrc: imgpath+"q02a.png"
						}
					], isdatacorrect : true,
				},
				{ 
					datafornotcorrect: "incorrect1",
					pidname: "imgoptionp2", 
					optionstext: data.string.e1ans5,
					imgoption:[
						{
							imgoptionname : "animimg2_1", 
							imgoptionsrc: imgpath+"q02e.png" 
						},
						{
							imgoptionname : "animimg2", 
							imgoptionsrc: imgpath+"q02b.png"
						}
					]
				},
				{ 
					datafornotcorrect: "incorrect2",
					pidname: "imgoptionp3", 
					optionstext: data.string.e1ans6, 
					imgoption:[
						{
							imgoptionname : "animimg3_1", 
							imgoptionsrc: imgpath+"q02f.png" 
						},
						{
							imgoptionname : "animimg3", 
							imgoptionsrc: imgpath+"q02c.png"
						}
					]
				},
			]
	},
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofimgoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e1question3,
		middleimg:[
			{questionimgclass: "questionimage",questionimgsrc  : imgpath+"q03a.png"},
			{questionimgclass: "answerimage",questionimgsrc  : imgpath+"q03b.png"},
			{questionimgclass: "incorrectimgsingle1",questionimgsrc  : imgpath+"q03c.png"},
			{questionimgclass: "incorrectimgsingle2",questionimgsrc  : imgpath+"q03d.png"},

		],
		optionsdata : [
			{ datafornotcorrect: "incorrectsingleimg1", pidname: "optiontext1", optionstext : data.string.e1ans8, },
			{ pidname: "optiontext2", optionstext : data.string.e1ans7, isdatacorrect : true,},
			{ datafornotcorrect: "incorrectsingleimg2", pidname: "optiontext3", optionstext : data.string.e1ans9,}
		],
	},
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e1question4,
		middleimg:[
			{questionimgclass: "questionimage",questionimgsrc  : imgpath+"q04a.png"},
			{questionimgclass: "answerimage",questionimgsrc  : imgpath+"q04b.png"}
		],
		optionsdata : [
			{ 
				pidname: "imgoptionpSmall1", optionstext: data.string.e1ans1,
				imgoption:[
					{
						imgoptionname : "imageoptionsmall1", 
						imgoptionsrc: imgpath+"q04c.png" 
					}
				],isdatacorrect : true,
			},
			{ 
				datafornotcorrect: "incorrect1",
				pidname: "imgoptionpSmall2", optionstext: data.string.e1ans2,
				imgoption:[
					{
						imgoptionname : "imageoptionsmall2", 
						imgoptionsrc: imgpath+"q04d.png" 
					}
				]
			},
			{ 
				datafornotcorrect: "incorrect2",
				pidname: "imgoptionpSmall3", optionstext: data.string.e1ans3,
				imgoption:[
					{
						imgoptionname : "imageoptionsmall3", 
						imgoptionsrc: imgpath+"q04e.png" 
					}
				]
			}
		],
	},
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofimgoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e1question5,
		middleimg:[
			{questionimgclass: "questionimage",questionimgsrc  : imgpath+"q05a.png"},
			{questionimgclass: "answerimage",questionimgsrc  : imgpath+"q05c.png"},
			{questionimgclass: "incorrectimgsingle1",questionimgsrc  : imgpath+"q05b.png"},
			{questionimgclass: "incorrectimgsingle2",questionimgsrc  : imgpath+"q05e.png"},
		],
		optionsdata : [
			{ datafornotcorrect: "incorrectsingleimg1", pidname: "optiontext1", optionstext : data.string.e1ans10, },
			{ datafornotcorrect: "incorrectsingleimg2",pidname: "optiontext2", optionstext : data.string.e1ans12, },
			{ pidname: "optiontext3", optionstext : data.string.e1ans11, isdatacorrect : true,}
		],
	},
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e1question6,
		middleimg:[
			{questionimgclass: "questionimage",questionimgsrc  : imgpath+"q06a.png"},
			{questionimgclass: "answerimage",questionimgsrc  : imgpath+"q06b.png"}
		],
		optionsdata : [
			{ 
				pidname: "imgoptionpSmall1", optionstext: data.string.e1ans1,
				imgoption:[
					{
						imgoptionname : "imageoptionsmall1", 
						imgoptionsrc: imgpath+"q06c.png" 
					}
				],isdatacorrect : true,
			},
			{
				datafornotcorrect: "incorrect1", 
				pidname: "imgoptionpSmall2", optionstext: data.string.e1ans2,
				imgoption:[
					{
						imgoptionname : "imageoptionsmall2", 
						imgoptionsrc: imgpath+"q06d.png" 
					}
				]
			},
			{ 
				datafornotcorrect: "incorrect2",
				pidname: "imgoptionpSmall3", optionstext: data.string.e1ans3,
				imgoption:[
					{
						imgoptionname : "imageoptionsmall3", 
						imgoptionsrc: imgpath+"q06e.png" 
					}
				]
			}
		],
	},
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofimgoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e1question7,
		middleimg:[
			{questionimgclass: "questionimageStill",questionimgsrc  : imgpath+"q07.png"},
		],
		optionsdata : [
			{ datafornotcorrect: "incorrect1", pidname: "imgoptionplarge1", optionstext : data.string.e1ans19, },
			{ datafornotcorrect: "incorrect2", pidname: "imgoptionplarge2", optionstext : data.string.e1ans20, },
			{ pidname: "imgoptionplarge3", optionstext : data.string.e1ans21, isdatacorrect : true,},
		],
		descriptioncontent:[
			{
				descriptiontextblock:[
					{
						descriptionname: "answerdescription1",
						descriptioncontenttextdata: data.string.e1ans22
					},
					{
						descriptionname: "answerdescription2",
						descriptioncontenttextdata: data.string.e1ans22_2
					},
					{
						descriptionname: "secondequal",
						descriptioncontenttextdata: data.string.equalsign
					},
					{
						descriptionname: "answerdescription3",
						descriptioncontenttextdata: data.string.e1ans22_3
					},
					{
						descriptionname: "answerdescription4",
						descriptioncontenttextdata: data.string.e1ans22_4
					},
					{
						descriptionname: "answerdescription5",
						descriptioncontenttextdata: data.string.e1ans22_5
					},
					{
						descriptionname: "answerdescription6",
						descriptioncontenttextdata: data.string.e1ans22_6
					}
				]
				
			}
		]
	},
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e1question8,
		middleimg:[
			{questionimgclass: "questionimage",questionimgsrc  : imgpath+"q08a.png"},
			{questionimgclass: "answerimage",questionimgsrc  : imgpath+"q08b.png"}
		],
		optionsdata : [
			{ 
				datafornotcorrect: "incorrect1",
				pidname: "imgoptionpSmall1", optionstext: data.string.e1ans1,
				imgoption:[
					{
						imgoptionname : "imageoptionsmall1", 
						imgoptionsrc: imgpath+"q08d.png" 
					}
				]
			},
			{ 
				pidname: "imgoptionpSmall2", optionstext: data.string.e1ans2,
				imgoption:[
					{
						imgoptionname : "imageoptionsmall2", 
						imgoptionsrc: imgpath+"q08c.png" 
					}
				],isdatacorrect : true,
			},
			{ 
				datafornotcorrect: "incorrect1",
				pidname: "imgoptionpSmall3", optionstext: data.string.e1ans3,
				imgoption:[
					{
						imgoptionname : "imageoptionsmall3", 
						imgoptionsrc: imgpath+"q08e.png" 
					}
				]
			}
		],
	},
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofimgoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e1question9,
		middleimg:[
			{questionimgclass: "questionimage",questionimgsrc  : imgpath+"q09a.png"},
			{questionimgclass: "answerimage",questionimgsrc  : imgpath+"q09b.png"},
			{questionimgclass: "incorrectimgsingle1",questionimgsrc  : imgpath+"q09c.png"},
			{questionimgclass: "incorrectimgsingle2",questionimgsrc  : imgpath+"q09d.png"},
		],
		optionsdata : [
			{ datafornotcorrect: "incorrectsingleimg1", pidname: "optiontext1", optionstext : data.string.e1ans13, },
			{ pidname: "optiontext2",optionstext : data.string.e1ans14, isdatacorrect : true, },
			{ datafornotcorrect: "incorrectsingleimg2", pidname: "optiontext3",optionstext : data.string.e1ans15, },
		],
	},
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofimgoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e1question10,
		middleimg:[
			{questionimgclass: "questionimagestill",questionimgsrc  : imgpath+"q10.png"}
		],
		optionsdata : [
			{ pidname: "imgoptionplarge1",optionstext : data.string.e1ans17, isdatacorrect : true, },
			{ pidname: "imgoptionplarge2",optionstext : data.string.e1ans16,},
			{ pidname: "imgoptionplarge3",optionstext : data.string.e1ans18}
		],
		descriptioncontent:[
			{
				descriptiontextblock:[
					{
						descriptionname: "answerdescription2_1",
						descriptioncontenttextdata: data.string.e1ans23
					},
					{
						descriptionname: "answerdescription2_2",
						descriptioncontenttextdata: data.string.e1ans23_2
					},
					{
						descriptionname: "answerdescription2_5",
						descriptioncontenttextdata: data.string.e1ans23_5
					},
					{
						descriptionname: "answerdescription2_3",
						descriptioncontenttextdata: data.string.e1ans23_3
					},
					{
						descriptionname: "answerdescription2_4",
						descriptioncontenttextdata: data.string.e1ans23_4
					}
				]
				
			}
		]
	}
];

// now randomize the question content
var content = tempcontent.shufflearray();

var congratulationcontent = [
	{
		congratulationtextdata : data.string.e2congratulationtext,
		congratulationimgsrc : congratulationimgarray[1],
		congratulationcompletedtextdata : data.string.e2congratulationcompletedtext,
		congratulationyourscoretextdata : data.string.e2congratulationyourscoretext,
		congratulationreviewtextdata : data.string.e2congratulationreviewtext,
	}
];

$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;

	var $total_page = content.length+congratulationcontent.length+2;
	loadTimelineProgress($total_page,countNext+1);

	// assign variable with quizboard container and scoreboard elements
	var $quizboard = $board.children('div.quizboard'); 
	var $scoreboard = $board.children('div.scoreboard'); 
	var $scoretext = $scoreboard.children('.scoretext');
	var $scorecount = $scoreboard.children('.scorecount');
	var $userscore = $scorecount.children('.userscore');
	var $totalproblemstext = $scoreboard.children('.totalproblemstext');
	// all elements which contains data about total questions
	var $totalquestiondata = $scoreboard.find('.totalquestiondata');
	var $scoregraph = $scoreboard.children('.scoregraph');
	var $scoregraphchildren;
	var totalquestioncount = 0; /*initiate total question count as 0*/
	var userscorecount = 0;
	var userscorestorage;
	var scoregraphstorehtml;
	var $congratulationscoregraphchildren;

	/*==========  navigation controller function  ==========*/	 
	 function navigationcontroller(){
		if(countNext >= 0 && countNext < $total_page-1){
			$nextBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }

	 /*==========  scoreboard update caller  ==========*/
	 function updatescoreboard(){	 	
	 	// for only first call updates
	 	if(countNext == 0){
	 		// update the total question count
	 		$.each(content, function(index, val) {
	 			if(content[index].hasquiztemplate){
	 				totalquestioncount++;
	 			}
	 		});

	 		$scoretext.html(data.string.e2scoretextdata);
	 		$totalquestiondata.html(totalquestioncount);
	 		$totalproblemstext.html(data.string.e2totalproblemtextdata);

	 		// now populate scoregraph block with batch of tags
	 		var blocktag = "<span data-correct=''></span>";

	 		for(var i=1 ; i <= totalquestioncount ; i++){
	 			scoregraph = $scoregraph.html();
	 			$scoregraph.html(scoregraph+blocktag);
	 		}

	 		$scoregraphchildren = $scoregraph.children('span')
	 	}
	 }

	/*==========  quiz template caller  ==========*/

	function quiz(){
		var source = $("#quiz-template").html();
		var template = Handlebars.compile(source);
		// update question count in content before updating the quiz template
		content[countNext].questioncount = countNext+1;
		// update options count in content before updating the quiz template
		content[countNext].numberofoptions = content[countNext].optionsdata.length;
		// content[countNext].numberofimgoptions = content[countNext].imgoptiondata.length;
		// update if quiz board has description content before updating the quiz template	
		content[countNext].hasdescriptionclass = typeof content[countNext].descriptioncontent !== "undefined" ? "hasdescriptionclass" : null;

		var html = template(content[countNext]);
		$quizboard.html(html);
		var $options = $quizboard.children('.options').children('p');
		var $optionsimg = $quizboard.children('.options').children('img');
		var clickcount = 0;

		// on options click do following
		$options.on('click', function() {
			// if incorrect is choosen
			$(this).attr("data-isclicked","clicked");

			// on first click only
			if(++clickcount == 1){
				$scoregraphchildren.eq(countNext).attr({
					'data-correct' : $(this).attr("data-correct")
				});
			}
			
			if($(this).attr("data-correct") == "correct"){
				// update isclicked data attribute to clicked
				clickcount == 1 ? $userscore.html(++userscorecount) : null;
				$options.css('pointer-events', 'none');
				$(".animimg1").addClass("cssfadeout");
				$(".animimg1_1").addClass("cssfadein");
				$(".answerdescription1, .answerdescription2, .secondequal, .answerdescription3, .answerdescription4, .answerdescription5, .answerdescription6").addClass('cssfadein');
				$(".answerdescription2_1, .answerdescription2_2, .answerdescription2_3, .answerdescription2_4, .answerdescription2_5").addClass('cssfadein');

				$(".questionimage").addClass("cssfadeout");
				$(".incorrectimgsingle2, .incorrectimgsingle1").removeClass("cssfadein");
				$(".answerimage").addClass("cssfadein");
				/*store the scoregraph and userscore as it is needed on congratulations templates
				when all the questions are attempted*/
				if(countNext+1 == totalquestioncount){
					userscorestorage = $userscore.html();
					scoregraphstorehtml = $scoregraph.html();					
				}
				navigationcontroller();
			}

			if($(this).attr("data-correct") == "incorrect1"){
				$(".animimg2").addClass("cssfadeout");
				$(".animimg2_1").addClass("cssfadein");
			}

			if($(this).attr("data-correct") == "incorrect2"){
				$(".animimg3").addClass("cssfadeout");
				$(".animimg3_1").addClass("cssfadein");
			}

			if($(this).attr("data-correct") == "incorrectsingleimg1"){
				$(".questionimage").addClass("cssfadeout");
				$(".incorrectimgsingle1").addClass("cssfadein");
				$(".incorrectimgsingle2").removeClass("cssfadein");
			}

			if($(this).attr("data-correct") == "incorrectsingleimg2"){
				$(".questionimage").addClass("cssfadeout");
				$(".incorrectimgsingle2").addClass("cssfadein");
				$(".incorrectimgsingle1").removeClass("cssfadein");

			}
		});
	}

	/*==========  congratulations template caller  ==========*/
	
	function congratulation(){
		var source = $("#congratulation-template").html();
		var template = Handlebars.compile(source);
		// update question count in content before updating the congratulation template
		// content[countNext].questioncount = countNext+1;
		
		var html = template(congratulationcontent[0]);
		$board.html(html);

		var $congratulationcontainer = $board.children('.congratulationcontainer');
		var $congratulationreviewtext = $congratulationcontainer.children('.congratulationreviewtext');
		var $congratulationscoregraph = $congratulationcontainer.children('.congratulationscoregraph');
		var $congratulationyourscoretext = $congratulationcontainer.children('.congratulationyourscoretext');
		$congratulationscoregraph.html(scoregraphstorehtml);

		// update the congratulationyourscoretext sentence
		var rawstatement = $congratulationyourscoretext.html();
		rawstatement = rawstatement.replace("#userscore#",userscorestorage);
		rawstatement = rawstatement.replace("#totalscore#",totalquestioncount);
		$congratulationyourscoretext.html(rawstatement);

		$congratulationscoregraphchildren = $congratulationscoregraph.children('span');
		$nextBtn.show(0);
	}

	quiz();
	updatescoreboard();
	// congratulation();
	// summary();
	
	$nextBtn.on('click',function () {
		$(this).css("display","none");
		countNext++;
		// alert(countNext +" and "+content.length);
		if(countNext < content.length){
			quiz();
		}	
		else if(countNext == content.length){
			congratulation();
		}
		else if(countNext == content.length+1){
			ole.activityComplete.finishingcall();
		}

		loadTimelineProgress($total_page,countNext+1);
	});
});
//array of diy squirrel image
	var diyImgPath = [
	"images/diy/diy1.png",
	"images/diy/diy2.png",
	"images/diy/diy3.png",
	"images/diy/diy4.png"
	];

var randomImageNumeral = ole.getRandom(1,3,0);
var diyImg = diyImgPath[randomImageNumeral];

var SPACE = data.string.spacecharacter;
var imgpath = $ref+"/images/page1/";
var squirrelwhat = "images/lokharke/squirrel_what_animated.svg";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var content=[	
	{
		contentblocknocenteradjust : true,
		uppertextblockadditionalclass : "coverall",
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass : "blueandgoldleverstyle",
				textdata  : data.string.lever,
			}
		],
	},
	{
		hasheaderblock : true,
		headerblock : [
			{	
				textclass : "headertextstyle",
				textdata  : data.string.lever,
			}
		],
		uppertextblock : [
			{
				textclass : "simpletextstyle",
				textdata  : data.string.p1text1,
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass : "threeimgtogether",
						imgsrc   : imgpath+"lol.jpg"						
					},
					{
						imgclass : "threeimgtogether",
						imgsrc   : imgpath+"seesaw.png"						
					},
					{
						imgclass : "threeimgtogether",
						imgsrc   : imgpath+"girls.gif"						
					}
				]
			}
		]
	},
	{
		hasheaderblock : true,
		headerblock:[
			{
				textclass                : "bigheadingtextstyle customborder cssborderanim",
				textdata                 : data.string.p1text2,
			}
		],
		contentblocknocenteradjust : true,
		uppertextblock : [
			{
				datahighlightflag        : true,
				datahighlightcustomclass : "clickmetextpink",
				textclass                : "definitionstyle",
				textdata                 : data.string.p1text2_1,
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "plank",
						imgsrc: imgpath+"lever01.png"
					},
					{
						imgclass: "fulcrum",
						imgsrc: imgpath+"lever02.png"
					},
					{
						imgclass: "clickonfulcrum",
						imgsrc: "images/hand-icon.gif"
					},
					{
						imgclass: "clickonbar",
						imgsrc: "images/hand-icon.gif"
					}
				],
				imagelabels:[
					{
						imagelabelclass: "fulcrumtxt",
						imagelabeldata: data.string.fulcrum
					},
					{
						imagelabelclass: "bartxt",
						imagelabeldata: data.string.rigidbar
					}
				]
			}
		]
	},
	{
		hasheaderblock : true,
		headerblock : [
			{	
				textclass : "headertextstyle",
				textdata  : data.string.egLever,
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"singleimgstyle",
						imgsrc: imgpath+"kids.gif"
					}
				]
			}
		]
	},
	{
		hasheaderblock : true,
		headerblock : [
			{	
				textclass : "headertextstyle",
				textdata  : data.string.egLever,
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"singleimgstyle2",
						imgsrc: imgpath+"class1anim.gif"
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,

		uppertextblock : [
			{
				datahighlightflag        : true,
				datahighlightcustomclass : "clickmetextpink",
				textclass                : "definitionstyle2",
				textdata                 : data.string.p1text3,
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass : "clickload cssfadein",
						imgsrc   : "images/clickme_icon.png"
					},
					{
						imgclass : "clickeffort",
						imgsrc   : "images/clickme_icon.png"
					}
				]
			}
		],
		svgblock:[
			{

			}
		]
	},
	{
		uppertextblock : [
			{
				datahighlightflag        : true,
				datahighlightcustomclass : "bigfont",
				textclass                : "bottomparatextstyle",
				textdata                 : data.string.p1text4,
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass : "labeledimg",
						imgsrc   : imgpath+"loadarm.png"
					}
				],
				imagelabels:[
					{
						imagelabelclass:"label1",
						imagelabeldata: data.string.effortarm
					},
					{
						imagelabelclass:"label2",
						imagelabeldata: data.string.loadarm
					}
				]
			}
		],
	},
	{   
        diysplashblock : [
            {
                diysplashImageSource     : diyImg,
                diysplashTextData        : data.string.diytext,
                diysplashInstructionData : data.string.p1diysplashinstruction,
            },
        ]
    },
    {
        contentblocknocenteradjust : true,
        diyblock: [
            {   
				diyImageSource         : diyImg,
				diyTextData            : data.string.diytext,
				diyInstructionData     : data.string.p1diyquestion,
				questionimgsrc         : imgpath+"diy.svg",
				congratulationsmsgdata : data.string.p1congratulationsmsg,
				effortdata             : data.string.effort,
				effortarmdata          : data.string.effortarm,
				loaddata               : data.string.load,
				loadarmdata            : data.string.loadarm,
				fulcrumdata            : data.string.fulcrum,
            },
        ],
        
    },
    {
		hasheaderblock : true,
		headerblock : [
			{	
				textclass : "headertextstyle1",
				textdata  : data.string.p1text9,
			}
		],
		contentblocknocenteradjust : true,
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass : "side1",
						imgsrc   : imgpath+"suirrelandelephant.gif",
					}
				]
			}
		],
		lowertextblock:[
			{
				textclass: "bottomparagraph cssfadein",
				textdata: data.string.p1text6
			}
		]
	},
	{
		hasheaderblock : true,
		headerblock : [
			{	
				textclass : "headertextstyle1",
				textdata  : data.string.p1text9,
			}
		],
		contentblocknocenteradjust : true,
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass : "side1",
						imgsrc   : imgpath+"suirrelandpenguin.gif",
					}
				]
			}
		],
		lowertextblock:[
			{
				textclass: "bottomparagraph cssfadein",
				textdata: data.string.p1text7
			}
		]
	},
	{
		hasheaderblock : true,
		headerblock : [
			{	
				textclass : "headertextstyle",
				textdata  : data.string.p1text8,
			}
		],
		contentblocknocenteradjust : true,
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass : "side1 positionchangeanim",
						imgsrc   : imgpath+"suirrelandelephant.gif",
					},
					{
						imgclass : "side2",
						imgsrc   : imgpath+"suirrelandpenguin.gif",
					}
				]
			}
		],
	},
	{
		timetothinkblock:[
			{
				timetothinkcommonimg:[
					{
						imagestoshow:[
							{
								imgclass : "timetothinksquirrelstyle",
								imgsrc   : squirrelwhat						
							}
						],
					}
				],
				timetothinkquestion: [
					{
						textclass : "simpletextstyle",
						textdata  : data.string.p1text5
					}
				],
				timetothinkextraimg:[
					{
						imagestoshow:[
							{
								imgclass : "threeimgtogether",
								imgsrc   : imgpath+"01.svg"						
							},
							{
								imgclass : "threeimgtogether",
								imgsrc   : imgpath+"02.svg"						
							},
							{
								imgclass : "threeimgtogether",
								imgsrc   : imgpath+"03.svg"						
							},
						],
					}
				]
			}
		]
	},
	{
		hasheaderblock : true,
		headerblock : [
			{	
				textclass : "headertextstyle",
				textdata  : data.string.p1typesofleverheading,
			}
		],
		typesofblock : [
			{
				typesofpara1 : [
					{
						textclass : "simpletextstyle",
						textdata  : data.string.p1typesofleverpara1,
					}
				],
				typesoftypes:[
					{
						listclass : "typesofliststyle",
						listdata  : data.string.p1typesoflevertype1,
					},
					{
						listclass : "typesofliststyle",
						listdata  : data.string.p1typesoflevertype2,
					},
					{
						listclass : "typesofliststyle",
						listdata  : data.string.p1typesoflevertype3,
					},
				],
			}
		]
	},
];

$(function () {	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   	 Handlebars.registerPartial("listcontent", $("#listcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("timetothinkcontent", $("#timetothinkcontent-partial").html());
	 Handlebars.registerPartial("diysplashcontent", $("#diysplashcontent-partial").html()); 
	 Handlebars.registerPartial("diycontent", $("#diycontent-partial").html());
	 Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());
	 Handlebars.registerPartial("typesofcontent", $("#typesofcontent-partial").html());
	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		/**

			What it does:
			- send an element where the function has to see
				for data to highlight
			- this function searches for all nodes whose
			    data-highlight element is set to true 
			-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
			- if provided with data-highlightcustomclass value for highlight it
				applies the custom class or else uses parsedstring class
			
			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;
				
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {	
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/				
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**		
			How to:
			- First set any html element with 
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/	 	 
	 /**	 
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that 
	 		footernotification is called for continue button to navigate to exercise
	  */
	
	 /**	  
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if 
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is 
	   */  
	 
	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			
			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;			
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**		
			How to:
			- Just call instructionblockcontroller() from the template		
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);					
				});
			}
		}	
	/*=====  End of InstructionBlockController  ======*/
		
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	
	/*=================================================
	=            general template function            =
	=================================================*/		
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
				
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		if(countNext==2 || countNext==5){
			$nextBtn.hide(0);
		}

		$(".cssborderanim").on(animationend, function(){
			$(".definitionstyle").addClass('cssfadein');
		});

		$(".definitionstyle").on(animationend, function(){
			$(".fulcrum").addClass("cssfadein");
			$(".clickonfulcrum").addClass('cssfadein')
		});

		$(".clickonfulcrum").on("click", function(){ 
			$(this).hide(0);
			$(".fulcrumtxt").addClass("cssfadein");
			$(".clickonbar, .plank").addClass("cssfadein2");
		});

		$(".clickonbar").on("click", function(){ 
			$(this).hide(0);
			$(".bartxt").addClass("cssfadein");
		});

		$(".bartxt").on(animationend, function(){
			$nextBtn.show(0);
		});

		$(".side1").on(animationend, function(){
			$(".headertextstyle2, .side2").addClass("cssfadein");
			$(".bottomparagraph").addClass("cssfadein2");
		});

		//just for svgblock
		var $svgcontainer = $board.children('div.svgcontainer'); 	
		$svgwrapper = $board.find('div.svgwrapper');
		// if there is a svg content
		if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");
			Snap.load(imgpath+"seesaw02.svg", function (f) {    
	    		s.append(f);
	    			// snap objects
	    		var arrow = s.select("#arrow");
	    		var loadPlank = s.select("#loadPlank");
	    		var effortPlank = s.select("#effortPlank");
	    		var rockone = s.select("#rockone");
	    		var empty_Plank = s.select("#empty_Plank");
	    		var firstposition_Plank = s.select("#firstposition_Plank");
	    		var secondposition_Plank = s.select("#secondposition_Plank");
	    		var thirdposition_Plank = s.select("#thirdposition_Plank");


	    			// jquery objects and js variables
	    		var $svg = $svgcontainer.children('svg');
	    		var $arrow = $svg.find('#arrow');
    			var $loadPlank = $svg.find('#loadPlank');
    			var $rockone = $svg.find('#rockone');
	    		var $empty_Plank = $svg.find("#empty_Plank");
	    		var $firstposition_Plank = $svg.find("#firstposition_Plank");
    			var $secondposition_Plank = $svg.find('#secondposition_Plank');
    			var $thirdposition_Plank = $svg.find('#thirdposition_Plank');

    			$(".clickload").on("click", function(){
    				$(this).hide(0);
    				rockone.addClass("animaterock");
    				empty_Plank.addClass("cssfadeout");
    				thirdposition_Plank.addClass("cssfadein2");
    				$(".clickeffort").addClass("cssfadein2");
    			});

    			$(".clickeffort").on("click", function(){
    				$(this).hide(0);
    				arrow.addClass('animatearrow');
    				secondposition_Plank.addClass("cssfadeinout");

    				setTimeout(function() {
    					thirdposition_Plank.removeClass("cssfadein2");
    				firstposition_Plank.addClass("cssfadein");

    				}, 1000);
    				$nextBtn.delay(1000).show(0);
    			});
 			});
		}

		// for diy blocks only
		var $diyblock = $board.find('div.diyblock');

		if($diyblock.length > 0){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			ole.footerNotificationHandler.hideNotification();

			var $diyworkarea        = $board.find("div.diyworkarea");
			var $draggablecontainer = $diyworkarea.find("div.draggablecontainer");
			var $draggables         = $draggablecontainer.children('p');
			var $droppablecontainer = $diyworkarea.find("div.droppablecontainer");
			var $congratulationsmsg = $droppablecontainer.children('h4.congratulationsmsg');
			var $droppables         = $droppablecontainer.children('div');
			var dropcount           = 0;
			var totaldroprequired   = 5;

			//make draggables draggable - LOL
			$draggables.draggable({containment: $diyworkarea, revert:"invalid"});

			$droppables.droppable({ tolerance: "pointer", 
				accept : function(dropElem){
					//dropElem was the dropped element, return true or false to accept/refuse
					if($(this).attr('data-acceptdroppable') === dropElem.attr("id")){
						return true;
					}
				},
				drop : function (event, ui) {
					$(this).html($(ui.draggable).html());
					$(this).addClass('droppableafterdrop');
					$(ui.draggable).css('display', 'none');
					if( ++dropcount == totaldroprequired ){
						$congratulationsmsg.show(0);
						$droppablecontainer.css('left', '12.5%');
						$prevBtn.show(0);
						$nextBtn.show(0);
					}
				}
			});
		}
	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		// call navigation controller
		navigationcontroller();

		// call the template
		generaltemplate();

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developer : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
			previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});
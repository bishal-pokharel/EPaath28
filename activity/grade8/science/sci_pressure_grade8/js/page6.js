var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var SPACE = data.string.spacecharacter;
var imgpath = $ref+"/images/page6/";

var content=[
	{
		contentblockadditionalclass : "contentblockoverridestyle",
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass         : "textpinkbandstyle",
				textdata          : data.string.p6heading1,
			}
		],
	},
	{
		headerblock : [
			{
				datahighlightflag : true,
				textclass         : "headertextstyle",
				textdata          : data.string.p6heading1,
			}
		],
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass         : "onlyparatextstyle",
				textdata          : data.string.p6text1,
			}
		]
	},
	{
		headerblock : [
			{
				datahighlightflag : true,
				textclass         : "headertextstyle",
				textdata          : data.string.p6heading1,
			}
		],
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass         : "instructiontextstyle",
				textdata          : data.string.p6instruction1,
			}
		],
		filltapwaterblock : [
			{
				tapimgsrc : imgpath + "tapclosed.png",
			}
		]
	},	
	{
		headerblock : [
			{
				datahighlightflag : true,
				textclass         : "headertextstyle",
				textdata          : data.string.p6heading1,
			}
		],
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass         : "instructiontextstyle",
				textdata          : data.string.p6instruction2,
			}
		],
		waterpropblock : [
			{
				waterprops : [
					{
						waterpropname : data.string.p6waterpropvolume,
						relatedimage : "volume"
					},
					{
						waterpropname : data.string.p6waterproparea,
						relatedimage : "area"
					},
					{
						waterpropname : data.string.p6waterpropheight,
						relatedimage : "height"
					},
					{
						waterpropname : data.string.p6waterpropdensity,
						relatedimage : "density"
					},
					{
						waterpropname : data.string.p6waterpropweight,
						relatedimage : "weight"
					}
				],
				waterpropimages : [					
					{
						imgsrc : imgpath + "volume_density.png",
						imgcontainerlabel : "density",
						imglabel : data.string.p6labelpropdensity,
					},
					{
						imgsrc : imgpath + "weight.png",
						imgcontainerlabel : "weight",
						imglabel : data.string.p6labelpropweight,
					},
					{
						imgsrc : imgpath + "height.png",
						imgcontainerlabel : "height",
						imglabel : data.string.p6labelpropheight,
					},
					{
						imgsrc : imgpath + "area.png",
						imgcontainerlabel : "area",
						imglabel : data.string.p6labelproparea,
					},
					{
						imgsrc : imgpath + "volume_density.png",
						imgcontainerlabel : "volume",
						imglabel : data.string.p6labelpropvolume,
					},
					{
						imgsrc : imgpath + "watervessel.png",
						imgcontainerlabel : "watervessel",
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		headerblock : [
			{
				datahighlightflag : true,
				textclass         : "headertextstyle",
				textdata          : data.string.p6heading1,
			}
		],
		formulablockadditionalclass : "pressureformulaonleft",
		formulablock : [
			{				
				formulaprefixdata : data.string.p6formula1prefix,
				fractionnumeratordata : data.string.p6formula1fxnnumeratordata,
				fractiondenominatordata : data.string.p6formula1fxndenominatordata,
			}
		],
		imageblockadditionalclass : "pressureimageonright",
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass : "pressureformulaimgstyle1",
						imgsrc   : imgpath+"pressurevessel.png"
					},
					{
						imgclass : "pressureformulaimgstyle1",
						imgsrc   : imgpath+"arrow.png"
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		headerblock : [
			{
				datahighlightflag : true,
				textclass         : "headertextstyle",
				textdata          : data.string.p6heading1,
			}
		],
		formulablockadditionalclass : "pressureformulaonleft",
		formulablock : [
			{
				formulaprefixdata       : data.string.p6formula2prefix,
				fractionnumeratordata   : data.string.p6formula2fxnnumeratordata,
				fractiondenominatordata : data.string.p6formula2fxndenominatordata,
			}
		],
		imageblockadditionalclass : "pressureimageonright",
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "pressureformulaimgstyle1",
						imgsrc   : imgpath+"pressurevessel.png"
					},
					{
						imgclass : "pressureformulaimgstyle1",
						imgsrc   : imgpath+"arrow.png"
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		headerblock : [
			{
				datahighlightflag : true,
				textclass         : "headertextstyle",
				textdata          : data.string.p6heading1,
			}
		],
		formulablockadditionalclass : "pressureformulaonleft bigtext",
		formulablock : [
			{
				formulaprefixdata       : data.string.p6formula2prefix,
				fractionnumeratordata   : data.string.p6formula2fxnnumeratordata,
				fractiondenominatordata : data.string.p6formula2fxndenominatordata,
			}
		],
		imageblockadditionalclass : "pressureimageonright",
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "pressureformulaimgstyle1",
						imgsrc   : imgpath+"pressurevessel.png"
					},
					{
						imgclass : "pressureformulaimgstyle1",
						imgsrc   : imgpath+"arrow.png"
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		headerblock : [
			{
				datahighlightflag : true,
				textclass         : "headertextstyle",
				textdata          : data.string.p6heading1,
			}
		],
		uppertextblockadditionalclass : "pressuretextblockonleft",
		uppertextblock : [
			{
				textdata : data.string.weknowthat,
			},
			{
				datahighlightflag : true,
				textdata : data.string.p6text2,
			},
			{
				datahighlightflag : true,
				textdata : data.string.p6text3,
			}
		],
		imageblockadditionalclass : "pressureimageonright",
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "pressureformulaimgstyle1",
						imgsrc   : imgpath+"pressurevessel.png"
					},
					{
						imgclass : "pressureformulaimgstyle1",
						imgsrc   : imgpath+"arrow.png"
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		headerblock : [
			{
				datahighlightflag : true,
				textclass         : "headertextstyle",
				textdata          : data.string.p6heading1,
			}
		],
		uppertextblockadditionalclass : "pressuretextblockonleft",
		uppertextblock : [
			{
				textdata : data.string.weknowthat,
			},
			{
				datahighlightflag : true,
				textdata : data.string.p6text2,
			},
			{
				datahighlightflag : true,
				textdata : data.string.p6text3,
			},
			{
				textdata : data.string.p6text4,
			}
		],
		imageblockadditionalclass : "pressureimageonright",
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "pressureformulaimgstyle1",
						imgsrc   : imgpath+"pressurevessel.png"
					},
					{
						imgclass : "pressureformulaimgstyle1",
						imgsrc   : imgpath+"arrow.png"
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		headerblock : [
			{
				datahighlightflag : true,
				textclass         : "headertextstyle",
				textdata          : data.string.p6heading1,
			}
		],
		derivationblocktype1 : [
			{			
				formulaprefixdata          : data.string.p6formula2prefix,
				numerator : [
					{
						fractionnumeratordata : data.string.p6formula2fxnnumeratordata,
						notifyuserdata : "notifyuser", /*this activates notifyuser attribute for pulse notification*/
					}
				],				
				fractiondenominatordata    : data.string.p6formula2fxndenominatordata,
				replacethisinformuladata   : data.string.p6text5,
			}
		],		
		imageblockadditionalclass : "pressureimageonright",
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "pressureformulaimgstyle1",
						imgsrc   : imgpath+"pressurevessel.png"
					},
					{
						imgclass : "pressureformulaimgstyle1",
						imgsrc   : imgpath+"arrow.png"
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		headerblock : [
			{
				datahighlightflag : true,
				textclass         : "headertextstyle",
				textdata          : data.string.p6heading1,
			}
		],
		uppertextblockadditionalclass : "pressuretextblockonleft",
		uppertextblock : [
			{
				textdata : data.string.weknowthat,
			},
			{
				datahighlightflag : true,
				textdata : data.string.p6massformula,
			},
			{
				datahighlightflag : true,
				textdata : data.string.p6abbreviationmassformulahilighted,
			}
		],
		imageblockadditionalclass : "pressureimageonright",
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "pressureformulaimgstyle1",
						imgsrc   : imgpath+"pressurevessel.png"
					},
					{
						imgclass : "pressureformulaimgstyle1",
						imgsrc   : imgpath+"arrow.png"
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		headerblock : [
			{
				datahighlightflag : true,
				textclass         : "headertextstyle",
				textdata          : data.string.p6heading1,
			}
		],
		uppertextblockadditionalclass : "pressuretextblockonleft",
		uppertextblock : [
			{
				textdata : data.string.weknowthat,
			},
			{
				datahighlightflag : true,
				textdata : data.string.p6massformula,
			},
			{
				datahighlightflag : true,
				textdata : data.string.p6abbreviationmassformulahilighted,
			},
			{
				textdata : data.string.p6text4,
			}
		],
		imageblockadditionalclass : "pressureimageonright",
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "pressureformulaimgstyle1",
						imgsrc   : imgpath+"pressurevessel.png"
					},
					{
						imgclass : "pressureformulaimgstyle1",
						imgsrc   : imgpath+"arrow.png"
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		headerblock : [
			{
				datahighlightflag : true,
				textclass         : "headertextstyle",
				textdata          : data.string.p6heading1,
			}
		],
		derivationblocktype1 : [
			{			
				formulaprefixdata : data.string.p6formula2prefix,
				numerator : [
					{
						fractionnumeratordata : data.string.p6massformulafractionnumeratordata1,
						notifyuserdata        : "notifyuser", /*this activates notifyuser attribute for pulse notification*/
					},
					{
						fractionnumeratordata : data.string.p6massformulafractionnumeratordata2,
					}
				],
				fractiondenominatordata    : data.string.p6formula2fxndenominatordata,
				replacethisinformuladata   : data.string.p6abbreviationmassformula,
			}
		],		
		imageblockadditionalclass : "pressureimageonright",
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "pressureformulaimgstyle1",
						imgsrc   : imgpath+"pressurevessel.png"
					},
					{
						imgclass : "pressureformulaimgstyle1",
						imgsrc   : imgpath+"arrow.png"
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		headerblock : [
			{
				datahighlightflag : true,
				textclass         : "headertextstyle",
				textdata          : data.string.p6heading1,
			}
		],
		uppertextblockadditionalclass : "pressuretextblockonleft",
		uppertextblock : [
			{
				textdata : data.string.weknowthat,
			},
			{
				datahighlightflag : true,
				textdata : data.string.p6volumeformula,
			},
			{
				datahighlightflag : true,
				textdata : data.string.p6abbreviationvolumeformulahilighted,
			}
		],
		imageblockadditionalclass : "pressureimageonright",
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "pressureformulaimgstyle1",
						imgsrc   : imgpath+"pressurevessel.png"
					},
					{
						imgclass : "pressureformulaimgstyle1",
						imgsrc   : imgpath+"arrow.png"
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		headerblock : [
			{
				datahighlightflag : true,
				textclass         : "headertextstyle",
				textdata          : data.string.p6heading1,
			}
		],
		uppertextblockadditionalclass : "pressuretextblockonleft",
		uppertextblock : [
			{
				textdata : data.string.weknowthat,
			},
			{
				datahighlightflag : true,
				textdata : data.string.p6volumeformula,
			},
			{
				datahighlightflag : true,
				textdata : data.string.p6abbreviationvolumeformulahilighted,
			},
			{
				textdata : data.string.p6text4,
			}
		],
		imageblockadditionalclass : "pressureimageonright",
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "pressureformulaimgstyle1",
						imgsrc   : imgpath+"pressurevessel.png"
					},
					{
						imgclass : "pressureformulaimgstyle1",
						imgsrc   : imgpath+"arrow.png"
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		headerblock : [
			{
				datahighlightflag : true,
				textclass         : "headertextstyle",
				textdata          : data.string.p6heading1,
			}
		],
		derivationblocktype1 : [
			{			
				formulaprefixdata          : data.string.p6formula2prefix,
				numerator : [
					{
						fractionnumeratordata : data.string.p6volumeformulafractionnumeratordata1,
						
					},
					{
						fractionnumeratordata : data.string.p6volumeformulafractionnumeratordata2,
						notifyuserdata        : "notifyuser", /*this activates notifyuser attribute for pulse notification*/
					},
					{
						fractionnumeratordata : data.string.p6volumeformulafractionnumeratordata3,
					}
				],
				fractiondenominatordata    : data.string.p6formula2fxndenominatordata,
				replacethisinformuladata   : data.string.p6abbreviationvolumeformula,
			}
		],		
		imageblockadditionalclass : "pressureimageonright",
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "pressureformulaimgstyle1",
						imgsrc   : imgpath+"pressurevessel.png"
					},
					{
						imgclass : "pressureformulaimgstyle1",
						imgsrc   : imgpath+"arrow.png"
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		headerblock : [
			{
				datahighlightflag : true,
				textclass         : "headertextstyle",
				textdata          : data.string.p6heading1,
			}
		],
		uppertextblockadditionalclass : "pressuretextblockonleftandtop",
		uppertextblock : [
			{
				datahighlightflag : true,
				textdata          : data.string.p6removecommonterm,
			},
		],
		formulablockadditionalclass : "pressureformulaonleftandbottom",
		formulablock : [
			{				
				formulaprefixdata : data.string.p6pressureformulawithareaprefix,
				numeratordatahighlightflag : true,
				numeratordatahighlightcustomclass : "strikeitout",
				denominatordatahighlightflag : true,
				denominatordatahighlightcustomclass : "strikeitout",
				fractionnumeratordata : data.string.p6pressureformulawithareanumerator,
				fractiondenominatordata : data.string.p6pressureformulawithareadenominator,
			}
		],
		imageblockadditionalclass : "pressureimageonright",
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "pressureformulaimgstyle1",
						imgsrc   : imgpath+"pressurevessel.png"
					},
					{
						imgclass : "pressureformulaimgstyle1",
						imgsrc   : imgpath+"arrow.png"
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		headerblock : [
			{
				datahighlightflag : true,
				textclass         : "headertextstyle",
				textdata          : data.string.p6heading1,
			}
		],
		uppertextblockadditionalclass : "pressuretextblockonleft conclusionstyle",
		uppertextblock : [
			{
				textdata : data.string.p6pressurederivationconclusion,
			},
			{
				textdata : data.string.p6pressureformula,
			},
			{
				datahighlightflag : true,
				textdata : data.string.p6pressureformulasymbolmeaningpart1+"<br>"+
							data.string.p6pressureformulasymbolmeaningpart2+"<br>"+
							data.string.p6pressureformulasymbolmeaningpart3+"<br>"+
							data.string.p6pressureformulasymbolmeaningpart4+"<br>"+
							data.string.p6pressureformulasymbolmeaningpart5,
			}
		],
		imageblockadditionalclass : "pressureimageonright",
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "pressureformulaimgstyle1",
						imgsrc   : imgpath+"pressurevessel.png"
					},
					{
						imgclass : "pressureformulaimgstyle1",
						imgsrc   : imgpath+"arrow.png"
					}
				]
			}
		]
	},
];

$(function () {	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   	 Handlebars.registerPartial("listcontent", $("#listcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("formulacontent", $("#formulacontent-partial").html());
	 Handlebars.registerPartial("filltapwatercontent", $("#filltapwatercontent-partial").html());
	 Handlebars.registerPartial("waterpropcontent", $("#waterpropcontent-partial").html());
	 Handlebars.registerPartial("derivationblocktype1content", $("#derivationblocktype1content-partial").html());
	 
	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		/**

			What it does:
			- send an element where the function has to see
				for data to highlight
			- this function searches for all nodes whose
			    data-highlight element is set to true 
			-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
			- if provided with data-highlightcustomclass value for highlight it
				applies the custom class or else uses parsedstring class
			
			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;
				
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {	
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/				
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**		
			How to:
			- First set any html element with 
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/	 	 
	 /**	 
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that 
	 		footernotification is called for continue button to navigate to exercise
	  */
	
	 /**	  
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if 
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is 
	   */  
	 
	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			
			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;			
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**		
			How to:
			- Just call instructionblockcontroller() from the template		
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);					
				});
			}
		}	
	/*=====  End of InstructionBlockController  ======*/
		
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	
	/*=================================================
	=            general template function            =
	=================================================*/		
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
				
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
	}

	function filltapwatertemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
				
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		var $filltapwaterblock = $board.find('div.filltapwaterblock');
		
		notifyuser($filltapwaterblock);

		// hide nextBtn before acitvity is complete
		$nextBtn.css('display', 'none');

		var $tapcontainer = $filltapwaterblock.children('div.tapcontainer');
		var $tap = $filltapwaterblock.children('div.tapcontainer').children('img.tap');
		var $vesselcontainer = $filltapwaterblock.children('div.vesselcontainer');
		var $tapwater = $vesselcontainer.children('div.tapwater');
		var $waterfill = $vesselcontainer.children('div.waterfill');

		$tapcontainer.one('click',  function(event) {
			event.preventDefault();
			/* Act on the event */
			$tapwater.attr('data-istapopen', 'tapopen');
			$waterfill.attr('data-istapopen', 'tapopen');
			$tap.attr("src",imgpath+"tapopen.png")
		});

		$waterfill.one(transitionend, function(event) {
			/* Act on the event */
			$tap.attr("src",imgpath+"tapclosed.png")
			$tapwater.css('display', 'none');
			$nextBtn.show(0);
		});
	}

	function waterproptemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
				
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		$nextBtn.css('display', 'none');

		var $waterpropblock = $board.find('div.waterpropblock');

		var $waterprops = $waterpropblock.find("div.waterprops");
		var $waterpropnames = $waterprops.children('p');
		var $waterpropimagecontainer = $waterpropblock.find("div.waterpropimagecontainer");
		var $singleimgcontainers = $waterpropimagecontainer.children('div.singleimgcontainer');
		var $mainimgcontainer = $waterpropimagecontainer.children('div.singleimgcontainer.watervessel');
		var hovercount = 0;

		$waterpropnames.hover(function() {
			/* Stuff to do when the mouse enters the element */
			var showthispropimg = $(this).attr("data-relatedimg");
			var $showthis = $waterpropimagecontainer.children('div.singleimgcontainer.'+showthispropimg);
			$singleimgcontainers.css('opacity', '0');
			$showthis.css('opacity', '1');

			hovercount++ < 5 ? null : $nextBtn.show(0);

			$(this).css('color', 'grey');

		}, function() {
			/* Stuff to do when the mouse leaves the element */
			$singleimgcontainers.css('opacity', '0');
			$mainimgcontainer.css('opacity', '1');
			$waterpropnames.css('color', 'black');
		});
	}

	function derivationblocktype1template() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
				
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		$nextBtn.css('display', 'none');

		var $derivationblocktype1 = $board.find('div.derivationblocktype1');

		notifyuser($derivationblocktype1);

		var $replaceinto = $derivationblocktype1.find("#replaceinto");
		var $replacethis = $derivationblocktype1.find(".replacethis");

		var datatoreplace;
		switch(countNext){
			case 9 : datatoreplace = data.string.p6replacemg; break;
			case 12 : datatoreplace = data.string.p6replacedv; break;
			case 15 : datatoreplace = data.string.p6replaceah; break;
			default : break;
		}

		$replaceinto.one('click', function() {
			/* Act on the event */
			$replaceinto.attr("data-replace","true");
			$replaceinto.html(datatoreplace);
			$replacethis.css('display', 'none');
			$nextBtn.show(0);
		});
	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		// call navigation controller
		navigationcontroller();

		// call the template
		switch(countNext){
			case 2  : filltapwatertemplate(); break;
			case 3  : waterproptemplate(); break;
			case 9  : 
			case 12 : 
			case 15 : derivationblocktype1template(); break;
			default : generaltemplate(); break;
		}
		
		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developer : "+countNext+" / "+($total_page-1)+"</span>");
	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
			previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});
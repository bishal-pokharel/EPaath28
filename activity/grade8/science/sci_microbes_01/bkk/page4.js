
$(function ($) {
	var $board = $('.board'),
		$nextbtn = $("#activity-page-next-btn-enabled"),
		$prevbtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 1,
		$total_page = 10,
		$refImg = $ref+"/images/page4/";

	var setNewLoad = false;

	$nextbtn.html(getSubpageMoveButton($lang,"next"));
	$prevbtn.html(getSubpageMoveButton($lang,"prev"));

	$(".mytext").html(data.string.p3_1);

	

	var content=[
		{	
			listTitle:data.string.p4_1,
			num : 1,
			title:data.string.p4_5_1,
			propertiesImg:$refImg+"1.png"
		},
		{	
			listTitle:data.string.p4_1,
			num : 2,
			list:[data.string.p4_5_1],
			title:data.string.p4_5_2,
			propertiesImg:$refImg+"1.png"
		},

		{	
			listTitle:data.string.p4_1,
			num : 3,
			list:[data.string.p4_5_1,data.string.p4_5_2],
			title:data.string.p4_5_3,
			propertiesImg:$refImg+"1.png"		
		},
		{	
			listTitle:data.string.p4_1,
			num : 4,
			list:[data.string.p4_5_1,data.string.p4_5_2,data.string.p4_5_3],
			title:data.string.p4_5_4,
			propertiesImg:$refImg+"1.png"		
		},
		{	
			listTitle:data.string.p4_1,
			num : 4,
			list:[data.string.p4_5_1,data.string.p4_5_2,data.string.p4_5_3,data.string.p4_5_4],
			title:data.string.p4_5_5,
			propertiesImg:$refImg+"1.png"		
		},
		{
			listTitle:data.string.p4_1,
			list:[data.string.p4_5_1,data.string.p4_5_2,data.string.p4_5_3,data.string.p4_5_4,data.string.p4_5_5]
		}
		
	];


	getHtml();

	$nextbtn.click(function(){
		$(this).hide(0);
		countNext++;
		getHtml();
		

	});

	$prevbtn.click(function(){
		$(this).hide(0);
		countNext--;
		getHtml();

	});	


	function getHtml()
	{
		loadTimelineProgress($total_page,countNext);

		var sourceHtml=getSource();
		
		//var sourceHtml=$("#first-template").html();
		
		var template = Handlebars.compile(sourceHtml);
		var getDatas= content[countNext-1];

		var html = template(getDatas);
		$board.fadeOut(10,function(){
			$(this).html(html)
		}).delay(10).fadeIn(10,function(){
			if(countNext>1)
			{
				$prevbtn.show(0);
			}
			if(countNext<$total_page)
				$nextbtn.show(0);
			else
				ole.footerNotificationHandler.pageEndSetNotification();
		});
	};

	function getSource()
	{
		var sourceHtml;
		switch (countNext)
		{
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
				sourceHtml = $("#first-template").html();
				break;
			
			case 6:			
				sourceHtml = $("#summary-template").html(); 
				break;
		}
		return sourceHtml;
	}
	
	
	Handlebars.registerHelper("inc", function(value, options)
	{
		return parseInt(value) + 1;
	});

})(jQuery);
var content=[
	{	
		text_1:data.string.p2_1,
		text_2:	data.string.p2_5,
		imgarr:[
				{imgbxcls:"imgBox1",imgsrcc:$ref+"/images/page2/Bacteria.png",caption:data.string.p2_2},
				{imgbxcls:"imgBox2",imgsrcc:$ref+"/images/page2/Virus.png",caption:data.string.p2_3},
				{imgbxcls:"imgBox3",imgsrcc:$ref+"/images/page2/Fungi.png",caption:data.string.p2_4}
			]
	}
];

$(function ($) {
	var $value = 1;
		$length = 0,
		$area = 0,
		countNext=1,
		$board = $('.board');

	getHtml(countNext);
	function getHtml()
	{
		loadTimelineProgress(1,1);


		 var sourceHtml = $("#first-template-1").html();
		
		var template = Handlebars.compile(sourceHtml);
		var getDatas= content[countNext-1];

		var html = template(getDatas);
		$board.fadeOut(10,function(){
			$(this).html(html)
		}).delay(50).fadeIn(800,function(){
			$("#activity-page-next-btn-enabled").show(0);
		});

		$("#activity-page-next-btn-enabled").click (function(){ 
			$(this).hide(0);
			$(".text_2").hide(0);
			$(".text_1").show(0);
			$(".imageBoxme").show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		});

	};
});
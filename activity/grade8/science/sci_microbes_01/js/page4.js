var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var content = [
	{
    heading: data.string.p4_title,
    title1: data.string.p4_1,
    title2: data.string.p4_2,
    title3: data.string.p4_3,
    title4: data.string.p4_4,
    cap1: data.string.c1,
    cap2: data.string.c2,
    cap3: data.string.c3,
    cap4: data.string.c4,
    cap5: data.string.c5,
    background: $ref + "/images/page4/microbes_tabs.png",
  },
	
  {
    title1: data.string.p4_1,
    title2: data.string.p4_2,
    title3: data.string.p4_3,
    title4: data.string.p4_4,

    def1: data.string.p4_6,
    def2: data.string.p4_7,
    microscope1: $ref + "/images/page4/microscope/front.png",
    microscope2: $ref + "/images/page4/microscope/back.png",
    microscope3: $ref + "/images/page4/microscope/slide.png",
    clickme: "images/hand-icon.gif",
    clickme2: $ref + "/images/page4/clickme_icon2.png",

    popup2: $ref + "/images/page4/micros-growth_final.gif",
    popuppara: data.string.p4_8_1,

    first_para: data.string.p4_9,
    s_data: data.string.p4_10,
    r_data: data.string.p4_11,
    sp_data: data.string.p4_12,


    spheres1: $ref + "/images/page4/Becteria04.png",
    rods1: $ref + "/images/page4/Becteria05.png",
    spirals1: $ref + "/images/page4/Becteria06.png",

    img_text: data.string.p4_8,

    val1: data.string.p4_13,
    val2: data.string.p4_14,
    greenImageval: $ref + "/images/page4/banawot/11.png",
    capsule1: $ref + "/images/page4/banawot/02.png",
    wall1: $ref + "/images/page4/banawot/08.png",
    flagella1: $ref + "/images/page4/banawot/09.png",
    pili1: $ref + "/images/page4/banawot/10.png",

    label1: data.string.p4_1_1,
    label2: data.string.p4_1_2,
    label3: data.string.p4_1_3,
    label4: data.string.p4_1_4,

    diyImageSource: "images/diy/diy1.png",
    diyTextData: data.string.d_1,
    text_1_1: data.string.d_3,

    arrBack: [{
        txt: 1,
        whatarrs: data.string.d_4_1,
        opt1: data.string.d_4_1_op1,
        opt2: data.string.d_4_1_op2,
        opt3: data.string.d_4_1_op3,
        corr1: 1,
        corr2: 0,
        corr3: 0,
        remain: data.string.d_4_1_1
      },
      {
        txt: 2,
        whatarrs: data.string.d_4_2,
        opt1: data.string.d_4_2_op1,
        opt2: data.string.d_4_2_op2,
        opt3: data.string.d_4_2_op3,
        corr1: 0,
        corr2: 0,
        corr3: 1
      },
      {
        txt: 3,
        whatarrs: data.string.d_4_3,
        opt1: data.string.d_4_3_op1,
        opt2: data.string.d_4_3_op2,
        opt3: data.string.d_4_3_op3,
        corr1: 1,
        corr2: 0,
        corr3: 0,
        remain: data.string.d_4_3_1
      },
      {
        txt: 4,
        whatarrs: data.string.d_4_4,
        opt1: data.string.d_4_4_op1,
        opt2: data.string.d_4_4_op2,
        opt3: data.string.d_4_4_op3,
        corr1: 0,
        corr2: 1,
        corr3: 0
      },
      {
        txt: 5,
        whatarrs: data.string.d_4_5,
        opt1: data.string.d_4_5_op1,
        opt2: data.string.d_4_5_op2,
        opt3: data.string.d_4_5_op3,
        corr1: 1,
        corr2: 0,
        corr3: 0
      },
      {
        txt: 6,
        whatarrs: data.string.d_4_6,
        opt1: data.string.d_4_6_op1,
        opt2: data.string.d_4_6_op2,
        opt3: data.string.d_4_6_op3,
        corr1: 1,
        corr2: 0,
        corr3: 0,
        remain: data.string.d_4_6_1
      },
    ],
    whatImgvalme: $ref + "/images/diy1/1.png",
    capsule: data.string.d_6,
    cellwall: data.string.d_5,
    flagella: data.string.d_7,
    bacteria_anim: $ref + "/images/diy1/7.gif",

    definitionTextData: data.string.next_3,
    definitionFirstWord: data.string.p2_3,
    definitionData: [data.string.p5_1,
      data.string.p5_2,
      data.string.p5_3,
      data.string.p5_4,
      data.string.p5_5
    ]
  },

  {
    title1: data.string.p4_1,
    title2: data.string.p4_2,
    title3: data.string.p4_3,
    title4: data.string.p4_4,
    para1: data.string.p4_24,
    para2: data.string.p4_25,
    para3: data.string.p4_26,
    para4: data.string.p4_27,

    bacteria_img: $ref + "/images/page4/Microbs-war04.gif",
    text_bubble: $ref + "/images/page4/speech-bubble-hi.png",

    img1: $ref + "/images/page4/human-body.png",
    img2: $ref + "/images/page4/dog.png",
    img3: $ref + "/images/page4/bird.png",
    img4: $ref + "/images/page4/plant01.png",

    img5: $ref + "/images/page4/soil.png",
    img6: $ref + "/images/page4/rock.png",
    img7: $ref + "/images/page4/waste-materials.png",
    img8: $ref + "/images/page4/Air.png",

    img9: $ref + "/images/page4/Ocean.png",
    img10: $ref + "/images/page4/Lava.png",
    img11: $ref + "/images/page4/ice.png",
    img12: $ref + "/images/page4/desert.png",

    humanbeings: data.string.p4_humanbeings,
    animals: data.string.p4_animals,
    birds: data.string.p4_birds,
    plants: data.string.p4_plants,
    soil: data.string.p4_soil,
    rocks: data.string.p4_rocks,
    garbage: data.string.p4_garbage,
    wind: data.string.p4_wind,
    ocean: data.string.p4_ocean,
    volcano: data.string.p4_volcano,
    ice: data.string.p4_ice,
    desert: data.string.p4_desert,

    title: data.string.p4_5_6_4,
    coverimg1: $ref + "/images/page4/plant01.png",
    coverimg2: $ref + "/images/page4/Ocean.png",
    coverimg3: $ref + "/images/page4/rabit.png",
    coverimg4: $ref + "/images/page4/waste-materials.png",
    coverimg5: $ref + "/images/page4/Lava.png",
  },

  {
    title1: data.string.p4_1,
    title2: data.string.p4_2,
    title3: data.string.p4_3,
    title4: data.string.p4_4,

    def1: data.string.p4_5_8,
    def2: data.string.p4_5_9,
    play: "./images/playbtn.png",
    repeatType: [{
        reproduction_img: $ref + "/images/page4/reproduction/01.png"
      },
      {
        reproduction_img: $ref + "/images/page4/reproduction/02.png"
      },
      {
        reproduction_img: $ref + "/images/page4/reproduction/03.png"
      },
      {
        reproduction_img: $ref + "/images/page4/reproduction/04.png"
      },
      {
        reproduction_img: $ref + "/images/page4/reproduction/05.png"
      },
      {
        reproduction_img: $ref + "/images/page4/reproduction/06.png"
      },
      {
        reproduction_img: $ref + "/images/page4/reproduction/07.png"
      },
      {
        reproduction_img: $ref + "/images/page4/reproduction/08.png"
      },
      {
        reproduction_img: $ref + "/images/page4/reproduction/09.png"
      },
      {
        reproduction_img: $ref + "/images/page4/reproduction/10.png"
      },
      {
        reproduction_img: $ref + "/images/page4/reproduction/11.png"
      },
      {
        reproduction_img: $ref + "/images/page4/reproduction/12.png"
      },
      {
        reproduction_img: $ref + "/images/page4/reproduction/13.png"
      },
      {
        reproduction_img: $ref + "/images/page4/reproduction/14.png"
      },
      {
        reproduction_img: $ref + "/images/page4/reproduction/15.png"
      },
      {
        reproduction_img: $ref + "/images/page4/reproduction/16.png"
      },
      {
        reproduction_img: $ref + "/images/page4/reproduction/17.png"
      },
      {
        reproduction_img: $ref + "/images/page4/reproduction/18.png"
      },
      {
        reproduction_img: $ref + "/images/page4/reproduction/19.png"
      },
      {
        reproduction_img: $ref + "/images/page4/reproduction/20.png"
      },
      {
        reproduction_img: $ref + "/images/page4/reproduction/21.png"
      },
      {
        reproduction_img: $ref + "/images/page4/reproduction/22.png"
      },
      {
        reproduction_img: $ref + "/images/page4/reproduction/23.png"
      },
    ],
  },

  {
    title1: data.string.p4_1,
    title2: data.string.p4_2,
    title3: data.string.p4_3,
    title4: data.string.p4_4,
    para4: data.string.p4_6_1,
    para5: data.string.p4_6_2,

    cutie1: $ref + "/images/exercise1/badMicrobes/bad3.png",
    cutie2: $ref + "/images/exercise1/badMicrobes/bad4.png",
    cutie3: $ref + "/images/exercise1/badMicrobes/bad6.png",
    cutie4: $ref + "/images/exercise1/badMicrobes/bad5.png",
    cutie5: $ref + "/images/exercise1/goodMicrobes/good7.png",
    cutie6: $ref + "/images/exercise1/goodMicrobes/good6.png",
    cutie7: $ref + "/images/exercise1/goodMicrobes/good5.png",
    cutie8: $ref + "/images/exercise1/goodMicrobes/good3.png",

    info1: data.string.p4_6_3,
    info2: data.string.p4_6_7,
    info3: data.string.p4_6_4,
    info4: data.string.p4_6_8,
    info5: data.string.p4_6_5,
    info6: data.string.p4_6_9,
    info7: data.string.p4_6_6,
    info8: data.string.p4_6_10,
  },
];

$(function() {

  var $board = $('.board');
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $tabBtn = $(".tab_nextBtn").html(data.string.next);
  var $tabPBtn = $(".tab_prevBtn").html(data.string.prev);
  var countNext = 0;
  var count = 0;
  var c_count = 0;
  loadTimelineProgress(5, 1);

  /*=====================================
  =            datahighlight            =
  =====================================*/
  function texthighlight($highlightinside) {
    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var replaceinstring;
    var texthighlightstarttag = "<span class='parsedstring'>";
    var texthighlightendtag = "</span>";
    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        // console.log(val);
        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
        $(this).html(replaceinstring);
      });
    }
  }



  /*=====  End of datahighlight  ======*/

  /*
   * first
   */
  function first() {
    var source = $("#first-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $prevBtn.hide(0);
    // alert(countNext);
    $nextBtn.show(0);
    $("#tabContainer").hide(0);
  }
  /*
   *
   * second
   */
  function second() {
    var source = $("#second-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $prevBtn.hide(0);
    $nextBtn.hide(0);
    // $tabBtn.hide(0);
    texthighlight($board);
    $("#tabContainer").show(0);
    $("#tab1").addClass("highlightborder");

    $(".def_part").delay(500).show(80);
    $tabBtn.delay(1000).show(100);
    var click_count = 0;
    $tabBtn.click(function() {
      click_count++;
      // alert(click_count);
      switch (click_count) {
        case 1:
          $tabBtn.hide(0);
          $(".image_part").show(0);
          $("#caption_pop").show(0);
          $("#click_me").click(function() {
            $(this).hide(0);
            $("#middle").css("right", "47%");
            $("#click_me2").delay(1000).fadeIn(800);
          });

          $("#click_me2").click(function() {
            $(".popup_img").delay(100).fadeIn(800);
            $("#p2").delay(100).fadeIn(800);
            $tabBtn.delay(1000).show(80);
          });
          break;

        case 2:
          $tabBtn.hide(0);
          $("#f_p1").delay(500).show(80);
          $("#spheres").delay(800).fadeIn(800);
          $("#rods").delay(1200).fadeIn(800);
          $("#spirals").delay(1600).fadeIn(800);
          $tabBtn.delay(2000).show(80);
          $tabPBtn.delay(2000).show(80);

          $(".image_part").hide(0);
          $("#caption_pop").hide(0);
          $(".def_part").hide(0);
          $(".popup_img").hide(0);
          break;

        case 3:
          $("#f_p1, #spheres, #rods, #spirals").hide(0);
          $(".greenImageval").show(0);
          $tabBtn.hide(0);
          break;

        case 4:
          $(".halfBox").show(0);
          $(".backtg").show(0);
          $(".greenImageval").hide(0);
          $tabBtn.hide(0);
          // $nextBtn.hide(0);
          break;

        default:
          break;
      }

    });


    var all_label = $board.children(".greenImageval").children(".image_area").children("div");
    var l1click = false;
    var l2click = false;
    var l3click = false;
    var l4click = false;

    all_label.click(function() {
      var clicked_label = $(this).attr("id");
      $("#capsule1").addClass("neutral");
      $("#wall1").addClass("neutral");
      $("#flagella1").addClass("neutral");
      $("#pili1").addClass("neutral");

      if (clicked_label == "label1") {
        $("#label1").addClass("clickedlabel");
        $("#divVal2").hide(0);
        $("#click_p").show(0);
        $("#banawat").fadeTo('slow', 0.1);
        $("#capsule1").removeClass("neutral");
        $("#click_p").text(data.string.p4_19);
        l1click = true;

        if (l1click == true && l2click == true && l3click == true && l4click == true) {
          $tabBtn.show(0);
        };
      };

      if (clicked_label == "label2") {
        $("#label2").addClass("clickedlabel");
        $("#divVal2").hide(0);
        $("#banawat").fadeTo('slow', 0.1);
        $("#click_p").show(0);
        $("#wall1").removeClass("neutral");
        $("#click_p").text(data.string.p4_17);
        l2click = true;

        if (l1click == true && l2click == true && l3click == true && l4click == true) {
          $tabBtn.show(0);
        };
      };

      if (clicked_label == "label3") {
        $("#label3").addClass("clickedlabel");
        $("#divVal2").hide(0);
        $("#banawat").fadeTo('slow', 0.1);
        $("#click_p").show(0);
        $("#flagella1").removeClass("neutral");
        $("#click_p").text(data.string.p4_16);
        l3click = true;

        if (l1click == true && l2click == true && l3click == true && l4click == true) {
          $tabBtn.show(0);
        };
      };

      if (clicked_label == "label4") {
        $("#label4").addClass("clickedlabel");
        $("#divVal2").hide(0);
        $("#banawat").fadeTo('slow', 0.1);
        $("#pili1").removeClass("neutral");
        $("#click_p").text(data.string.p4_15);
        $("#click_p").show(0);
        l4click = true;

        if (l1click == true && l2click == true && l3click == true && l4click == true) {
          $tabBtn.show(0);
        };
      };

    });

    var $value = 1;
    $(".nonactive").on("click", function() {
      var $whatIdvalstoo = $("#whatId_" + $value);
      // alert ($whatIdvalstoo);
      var datvals = $(this).attr('dataval');
      var valHtml = $(this).html();
      var clickedme = $(this);

      if (datvals == 1) {
        // alert("inside if loop");
        $whatIdvalstoo.find(".whatactive[dataval='1']").addClass('actives');
        $whatIdvalstoo.find(".whatactive").removeClass('nonactive');

        $whatIdvalstoo.find(".bothvals").delay(500).fadeOut(500, function() {
          $whatIdvalstoo.find(".dashedLine").html(valHtml).addClass('dsLine');

          if ($value == 1)
            // alert("inside if loop");

            $(".whatImgvalme").delay(200).fadeIn(500);
          else {
            $(".whatImgvalme img:nth-of-type(1)").attr("src", $ref + "/images/diy1/" + $value + ".png");
          }
          $value++;
          // alert($value);
          if ($value <= 6) {
            $("#whatId_" + $value).delay(500).fadeIn(500);
          } else {
            setTimeout(function() {
              $("#bac_anim_2").show(0);
              $("#bac_anim_2").css("right", "400%");
              $(".whatImgvalme img:nth-of-type(1)").hide(0);
              // $nextBtn.show(0);
              // ole.footerNotificationHandler.pageEndSetNotification();
            }, 800);
            $nextBtn.delay(2000).show(0);
          }

        });
      } else {
        $whatIdvalstoo.find(clickedme).addClass('wronged');
      }
    });

    // $board.on("click",".cutie", function(){
    // 	$(".cutie").css("top","-15%");
    // });
  }


  /*
   *
   * third
   */
  function third() {
    var source = $("#third-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $nextBtn.hide(0);
    $prevBtn.hide(0);

    $("#tab2").addClass("highlightborder");

    $("#habi1, #habi3").addClass("moveleft");
    $("#habi2, #habi4").addClass("moveright");
    $tabBtn.delay(4000).show(0);

    var Count = 0;
    $tabBtn.click(function() {
      Count++;
      switch (Count) {
        case 1:
          $("#bac_habi_def").hide(0);
          $("#wrapper").show(0);
          $("#bac").show(80);
          $("#dialog").text(data.string.p4_5_6_4);
          $("#bubble").show(80);
          $tabBtn.show(80);
          break;

        case 2:
          $("#top1, #right1, #left1, #bottom1").delay(500).show(800);
          $("#dialog").text(data.string.p4_5_6_1);
          $tabBtn.show(0);
          break;

        case 3:
          $("#first_box").addClass('hidediv');
          $("#dialog").text(data.string.p4_5_6_2);
          $("#second_box").show(0);
          $("#top2, #right2, #left2, #bottom2").delay(500).show(800);
          $tabBtn.show(80);
          break;

        case 4:
          $("#second_box").addClass('hidediv');
          $("#third_box").show(0);
          $("#top3, #right3, #left3, #bottom3").delay(500).show(800);
          $("#dialog").text(data.string.p4_5_6_3);
          $("#dialog").css("font-size", "0.7em");
          $tabBtn.show(80);
          break;

        case 5:
          $("#third_box").addClass('hidediv');
          $("#dialog").text(data.string.p4_5_6_4);
          $("#fourth_box").show(0);
          $tabBtn.hide(0);
          $prevBtn.delay(2000).show(0);
          $nextBtn.delay(2000).show(0);
          break;
        default:
          break;
      }
    });
  }

  /*
   *
   * fourth
   */
  function fourth() {
    var source = $("#fourth-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $nextBtn.hide(0);
    $prevBtn.hide(0);
    $tabBtn.hide(0);
    // $prevBtn.show(0);

    $("#tab3").addClass("highlightborder");

    $("#playbutton").show(0);
    $("#playbutton").click(function() {
      $(this).hide(0);
      $nextBtn.delay(5000).show(0);
      $prevBtn.delay(5000).show(0);

      jQuery(function() { // DOM ready shorthand
        var $gal = $("#repro_img"),
          $img = $gal.find(">*"),
          n = $img.length, // number of images
          c = 0, // counter
          itv; // loop interval

        function anim() {
          $img.hide(0).eq(++c % n).stop().show(0);
          if (c == n) {
            stop();
            $("#playbutton").show(0);
          };
          // alert(c);
        }

        function loop() {
          itv = setInterval(anim, 200);
        }

        function stop() {
          clearInterval(itv);
        }

        $img.hide(0).eq(c).show(0); // Begin with `c` indexed image
        loop();
        // stop();
      });
    });

  }

  /*
   *
   * fifth
   */
  function fifth() {
    var source = $("#fifth-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $prevBtn.hide(0);
    $nextBtn.hide(0);
    // $prevBtn.show(0);
    // $tabBtn.show(0);
    // alert(countNext);
    $("#tab4").addClass("highlightborder");

    // $(".expanded").addClass("buldging1");
    $("#repro_def").css({
      "top": "45%"
    });
    $("#repro_def > p:nth-of-type(1)").css({
      "color": "#FC0047",
      "font-size": "1.8em"
    });

    $("#repro_def > p:nth-of-type(1)").text(data.string.p4_6_1);
    $("#repro_def > p:nth-of-type(2)").text(data.string.p4_6_2);

    $tabBtn.delay(500).show(0);
    // $("#bac_box1").addClass("cssfadein");
    // $("#bac_box2").addClass("cssfadein2");
    // $("#bac_box2").on(animationend, function(){
    // $tabBtn.show(0);
    // });

    var tabBtn_click = 0;

    $tabBtn.click(function() {

      tabBtn_click++;
      switch (tabBtn_click) {
        case 1:
          // alert(tabBtn_click);
          $tabBtn.hide(0);
          $("#repro_def").css({
            "top": "12%"
          });
          $("#repro_def > p:nth-of-type(1)").css({
            "color": "#FC0047",
            "font-size": "1.2em"
          });
          $("#repro_def > p:nth-of-type(2)").text(data.string.p4_6_2);

          $("#bac_box1").addClass("cssfadein");
          $("#bac_box2").addClass("cssfadein2");
          $("#bac_box2").on(animationend, function() {
            $tabBtn.show(0);
          });
          break;
        case 2:
          // alert(tabBtn_click);
          $tabBtn.hide(0);
          $("#bac_box1").removeClass("cssfadein");
          $("#bac_box2").removeClass("cssfadein2");
          $("#bac_box3").addClass("cssfadein");
          $("#bac_box4").addClass("cssfadein2");
          $tabBtn.delay(3000).show(0);
          // $("#bac_box1").removeClass("cssfadein");
          // $("#bac_box2").removeClass("cssfadein2");
          // $("#bac_box3").addClass("cssfadein");
          // $("#bac_box4").addClass("cssfadein2");
          // break;
          // case 2:
          // // alert(tabBtn_click);
          //
          // $(".bac_box_wrapper1").hide(0);
          // $(".bac_box_w2").show(0);
          // $("#bac_box5").addClass("cssfadein");
          // $("#bac_box6").addClass("cssfadein2");
          // $tabBtn.delay(700).show(0);
          //
          // $("#repro_def > p:nth-of-type(1)").text(data.string.p4_6_11);
          // $("#repro_def > p:nth-of-type(2)").hide(0);
          //
          // $("#bac_box5 >p:nth-of-type(1)").text(data.string.p4_6_12);
          // $("#bac_box5 >p:nth-of-type(2)").text(data.string.p4_6_13);
          //
          // $("#bac_box6 >p:nth-of-type(1)").text(data.string.p4_6_14);
          // $("#bac_box6 >p:nth-of-type(2)").text(data.string.p4_6_15);
          break;

        case 3:
          // alert(tabBtn_click);/
          $tabBtn.hide(0);
          $(".bac_box_wrapper1").hide(0);
          $("#repro_def").css({
            "top": "45%"
          });
          $("#repro_def > p:nth-of-type(1)").css({
            "color": "#4B7BEA",
            "font-size": "2.5em"
          });
          // $("#repro_def > p:nth-of-type(1)").css({"color": "#4B7BEA"});
          $("#repro_def > p:nth-of-type(1)").text(data.string.p4_6_11);

          $("#repro_def > p:nth-of-type(2)").hide(0);

          $tabBtn.delay(500).show(0);

          break;
          // $(".bac_box_w2").show(0);
          // $("#bac_box5").removeClass("cssfadein");
          // $("#bac_box6").removeClass("cssfadein2");
          // $("#bac_box7").addClass("cssfadein");
          // $("#bac_box8").addClass("cssfadein2");
          //
          // $("#bac_box7 >p:nth-of-type(1)").text(data.string.p4_6_24);
          // $("#bac_box7 >p:nth-of-type(2)").text(data.string.p4_6_25);
          //
          // $("#bac_box8 >p:nth-of-type(1)").text(data.string.p4_6_26);
          // $("#bac_box8 >p:nth-of-type(2)").text(data.string.p4_6_27);
          // break;

        case 4:
          $tabBtn.hide(0);

          $("#repro_def").css({
            "top": "12%"
          });
          $("#repro_def > p:nth-of-type(1)").css({
            "color": "#4B7BEA",
            "font-size": "1.5em"
          });
          $(".bac_box_wrapper1").hide(0);
          $(".bac_box_w2").show(0);
          $("#bac_box5").addClass("cssfadein");
          $("#bac_box6").addClass("cssfadein2");

          $("#repro_def").removeClass("expanded");

          $("#repro_def > p:nth-of-type(1)").text(data.string.p4_6_11);

          $("#repro_def > p:nth-of-type(2)").hide(0);
          $("#bac_box5 >p:nth-of-type(1)").text(data.string.p4_6_12);
          $("#bac_box5 >p:nth-of-type(2)").text(data.string.p4_6_13);
          $("#bac_box6 >p:nth-of-type(1)").text(data.string.p4_6_14);
          $("#bac_box6 >p:nth-of-type(2)").text(data.string.p4_6_15);
          $tabBtn.delay(3000).show(0);
          break;
          // $(".bac_box_w2").show(0);
          // $("#bac_box7").removeClass("cssfadein");
          // $("#bac_box8").removeClass("cssfadein2");
          // $("#bac_box9").addClass("cssfadein");
          // $("#bac_box10").addClass("cssfadein2");
          //
          // $("#bac_box9 >p:nth-of-type(1)").text(data.string.p4_6_16);
          // $("#bac_box9 >p:nth-of-type(2)").text(data.string.p4_6_17);
          //
          // $("#bac_box10 >p:nth-of-type(1)").text(data.string.p4_6_18);
          // $("#bac_box10 >p:nth-of-type(2)").text(data.string.p4_6_19);
          // break;

        case 5:
          // alert(tabBtn_click);/
          $tabBtn.hide(0);
          $(".bac_box_w2").show(0);
          $("#bac_box5").removeClass("cssfadein");
          $("#bac_box6").removeClass("cssfadein2");
          $("#bac_box7").addClass("cssfadein");
          $("#bac_box8").addClass("cssfadein2");
          $("#bac_box7 >p:nth-of-type(1)").text(data.string.p4_6_24);
          $("#bac_box7 >p:nth-of-type(2)").text(data.string.p4_6_25);
          $("#bac_box8 >p:nth-of-type(1)").text(data.string.p4_6_26);
          $("#bac_box8 >p:nth-of-type(2)").text(data.string.p4_6_27);
          $tabBtn.delay(3000).show(0);
          break;
          // $(".bac_box_w2").show(0);
          // $("#bac_box9").removeClass("cssfadein");
          // $("#bac_box10").removeClass("cssfadein2");
          // $("#bac_box11").addClass("cssfadein");
          // $("#bac_box12").addClass("cssfadein2");
          //
          // $("#bac_box11 >p:nth-of-type(1)").text(data.string.p4_6_20);
          // $("#bac_box11 >p:nth-of-type(2)").text(data.string.p4_6_21);
          //
          // $("#bac_box12 >p:nth-of-type(1)").text(data.string.p4_6_22);
          // $("#bac_box12 >p:nth-of-type(2)").text(data.string.p4_6_23);
          // $tabBtn.hide(0);
          // $prevBtn.show(0);
          // $(".footerNotification").show(0);
          // ole.footerNotificationHandler.setNotificationMsgShowNextPagebutton(data.string.next_2);
          // break;

        case 6:
          $tabBtn.hide(0);
          $(".bac_box_w2").show(0);
          $("#bac_box7").removeClass("cssfadein");
          $("#bac_box8").removeClass("cssfadein2");
          $("#bac_box9").addClass("cssfadein");
          $("#bac_box10").addClass("cssfadein2");

          $("#bac_box9 >p:nth-of-type(1)").text(data.string.p4_6_16);
          $("#bac_box9 >p:nth-of-type(2)").text(data.string.p4_6_17);

          $("#bac_box10 >p:nth-of-type(1)").text(data.string.p4_6_18);
          $("#bac_box10 >p:nth-of-type(2)").text(data.string.p4_6_19);
          $tabBtn.delay(3000).show(0);
          break;

        case 7:
          $(".bac_box_w2").show(0);
          $("#bac_box9").removeClass("cssfadein");
          $("#bac_box10").removeClass("cssfadein2");
          $("#bac_box11").addClass("cssfadein");
          $("#bac_box12").addClass("cssfadein2");

          $("#bac_box11 >p:nth-of-type(1)").text(data.string.p4_6_20);
          $("#bac_box11 >p:nth-of-type(2)").text(data.string.p4_6_21);

          $("#bac_box12 >p:nth-of-type(1)").text(data.string.p4_6_22);
          $("#bac_box12 >p:nth-of-type(2)").text(data.string.p4_6_23);
          $tabBtn.hide(0);
          $prevBtn.show(0);
          $("#bac_box12").on(animationend, function() {
            $(".footerNotification").show(0);
            ole.footerNotificationHandler.pageEndSetNotification();
          });

          break;

        default:
          break;

      }

    });
  }

  first(0);

  $nextBtn.on('click', function() {
    loadTimelineProgress(5, countNext + 2);
    countNext++;
    // alert(countNext);
    switch (countNext) {
      case 1:
        second();
        break;
      case 2:
        third();
        break;
      case 3:
        fourth();
        break;
      case 4:
        fifth(4);
        break;

      case 5:
        data(5);
        break;

      default:
        break;
    }
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on('click', function() {
    countNext--;
    // alert(countNext);
    switch (countNext) {
      case 3:
        fourth();
        $(".footerNotification").hide(0);

        break;
      case 2:
        third();
        break;
      case 1:
        second();
        break;
      case 0:
        first();
        $whatIdvalstoo.find(".whatactive[dataval='1']").removeClass('actives');
        $whatIdvalstoo.find(".whatactive").addClass('nonactive');

        break;

      default:
        break;
    }
  });
});

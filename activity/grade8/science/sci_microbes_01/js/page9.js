var content=[
    {
       heading : data.string.s_1,
       title1 : data.string.s_2,
       title2 : data.string.s_3,
       title3 : data.string.s_4,

       table_data:[
            {t_data1: data.string.s_data_1,t_data2: data.string.s_data_2,t_data3: data.string.s_data_3},
            {t_data1: data.string.s_data_4,t_data2: data.string.s_data_5,t_data3: data.string.s_data_6},
            {t_data1: data.string.s_data_7,t_data2: data.string.s_data_8,t_data3: data.string.s_data_9},
            {t_data1: data.string.s_data_10,t_data2: data.string.s_data_11,t_data3: data.string.s_data_12},
            {t_data1: data.string.s_data_13,t_data2: data.string.s_data_14,t_data3: data.string.s_data_15},
            {t_data1: data.string.s_data_16,t_data2: data.string.s_data_17,t_data3: data.string.s_data_18},
       ]
    },
];

$(function () {
    var $board = $('.board');
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var countNext =0;

    var $total_page = 2;
    loadTimelineProgress(1,countNext+1);

    function first(){
        var source = $("#first-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        // $nextBtn.show(0);
        ole.footerNotificationHandler.lessonEndSetNotification();
    }

    first();

});
var content = [{
	text_1_1 : data.string.p1_0,
	imgpath : $ref + "/images/page1/bg.jpg"
}, {
	text1_ : data.string.p1_,
	text_1_1 : data.string.p1_1,
	text_1_2 : data.string.p1_2,
	backImg : $ref + "/images/page1/animals.png",
	imgbackImg : "imgbackImg"
}, {
	text_1_1 : data.string.p1_3,
	text_1_2 : data.string.p1_4_2,
	backImg : $ref + "/images/page1/boy02.png",
	imgbackImg : "imgbackImg2"
}, {
	text_1_1 : data.string.p1_1_4,
	backImg : $ref + "/images/page1/boy02.png",
	imgbackImg : "imgbackImg2"
}, {
	text_1_1 : data.string.p1_5,
	text_1_2 : data.string.p1_5_2,
	backImg : $ref + "/images/page1/boy02.png",
	imgbackImg : "imgbackImg2"
}, {
	text_1_1 : data.string.p1_6,
	backImg2 : $ref + "/images/page1/boy01.png",
	imgbackImg : "imgbackImg21",
	skintxt : data.string.p1_skin,
	lungstxt : data.string.p1_lungs,
	mouthtxt : data.string.p1_mou,
	inttxt : data.string.p1_int,
}];

var source = ["#first-template-1", "#first-template-2", "#first-template-2", "#first-template-2", "#first-template-2", "#first-template-2", "#first-template-2", "#first-template-3"];

var bodypartInfoshowing = false;

$(function () {
var $value = 1;
$clickcount = 0,
$board = $('.board'),
$nextbtn = $("#activity-page-next-btn-enabled"),
$prevbtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
countNext = 1,
$total_page = 6;

var bodypartinfo = false;

$nextbtn.html(getSubpageMoveButton($lang,"next"));
$prevbtn.html(getSubpageMoveButton($lang,"prev"));

getHtml(countNext);

$nextbtn.click(function(){
$(this).hide(0);
if (!bodypartInfoshowing) {
countNext++;
getHtml();
}else{
$clickcount++;
bodypartinfo = false;
$("#zoomParts").removeAttr('style class').html('');

if ($clickcount == 1) {
$("#lungstxt").hide(0);
$("#boysStomach").addClass('whatClickMe');
$("#boysNose").removeClass('whatClickMe');
} else if ($clickcount == 2) {
$("#inttxt").hide(0);
$("#boysMouth").addClass('whatClickMe');

$("#boysStomach").removeClass('whatClickMe');
} else if ($clickcount == 3) {
$("#mouthtxt").hide(0);
$("#boysSkin").addClass('whatClickMe');
$("#boysMouth").removeClass('whatClickMe');
} else if ($clickcount == 4) {
$("#boysNose").addClass('whatClickMe');
$("#boysSkin").removeClass('whatClickMe');
$("#skintxt").hide(0);

$clickcount = 0;
ole.footerNotificationHandler.pageEndSetNotification();
}

}
// alert(countNext);

});

$prevbtn.click(function(){
$(this).hide(0);
if(bodypartInfoshowing){
bodypartInfoshowing = false;
clickcount = 0;
}
countNext--;
getHtml();

});

if (countNext==6) {
$nextbtn.hide(0);

};
$(".board").on("click",".whatClickMe",function(){
if(!bodypartinfo){
bodypartinfo = true;

var id=$(this).attr('id');

var clsVal="";
var imgsrcVal="";
var selectedtext = "";

if(id=="boysNose")
{
clsVal="enlargeNose";
imgsrcVal=$ref+'/images/page1/Bacteria-Nose.png';
selectedtext = "lungstxt";//$("#lungstxt").show(0);
}
else if(id=="boysSkin")
{
clsVal="enlargeSkin";
imgsrcVal=$ref+'/images/page1/Bacteria-Skin.png';
selectedtext = "skintxt";//$("#skintxt").show(0);
}
else if(id=="boysMouth")
{
clsVal="enlargeMouth";
imgsrcVal=$ref+'/images/page1/Bacteria-Mouth.png';
selectedtext = "mouthtxt";//$("#mouthtxt").show(0);
}
else if(id=="boysStomach")
{
clsVal="enlargeStomach";
imgsrcVal=$ref+'/images/page1/Bacteria-Intestinal.png';
selectedtext = "inttxt";//$("#inttxt").show(0);
}

setTimeout(function(){
$("#"+selectedtext).show(0);
}, 1000);

//$clickcount++;

$("#zoomParts").addClass(clsVal).html('<img src="'+imgsrcVal+'" />').animate({'width':'30%', "top":"10%",'left': '60%'},1000,function(){

// setTimeout(function(){
// bodypartinfo = false;
// $("#zoomParts").removeAttr('style class').html('');

$nextbtn.show(0);
// },4000);
});
}

});

function getHtml()
{
loadTimelineProgress($total_page,countNext);

var sourceHtml = $(source[countNext-1]).html();

var template = Handlebars.compile(sourceHtml);
var getDatas= content[countNext-1];

var html = template(getDatas);
$board.fadeOut(0,function(){
$(this).html(html);
}).delay(10).fadeIn(10,function(){

if(countNext>1 && countNext != 6)
{
$prevbtn.delay(100).fadeIn(1000);
$nextbtn.delay(100).fadeIn(1000);
$(".text1_").delay(100).fadeIn(1000);
$(".text_1_1").delay(100).fadeIn(1000);
$(".text_1_2").delay(100).fadeIn(2000);

}
if(countNext<($total_page-1))
{
$nextbtn.delay(100).fadeIn(1000);
}
else if(countNext<$total_page)
{
$nextbtn.delay(100).fadeIn(1000);
}
if(countNext==6)
{
bodypartInfoshowing= true;
$nextbtn.hide(0);
}

});

}

function getAnimate()
{
//ole.footerNotificationHandler.pageEndSetNotification();
}
})(jQuery);


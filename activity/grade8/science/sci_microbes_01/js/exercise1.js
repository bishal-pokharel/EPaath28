var animationtransitionEnd = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var transitionEnd = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";

var ex1imgPath = $ref+"/images/exercise1/";
var goodmicrobesPath = ex1imgPath+"realgoodmicrobes/";
var badmicrobesPath = ex1imgPath+"realbadmicrobes/";
var quizPath = ex1imgPath+"quiz/";
var quizgoodMicrobesPath = quizPath+"good/";
var quizbadMicrobesPath = quizPath+"bad/";


var content=[
	{
		wrapperclass : "firstslidewrapper", /*insert this if there is a wrapper class required*/
		fieldsetLegendText:data.string.e1slide1heading,
		insideFieldsetTag: [
					{
						insideFieldsetTagtextclass : "",
						insideFieldsetTagtext : data.string.e1slide1text,
					}
				],
	},
	{
		paragraphs : [{
			showorderclass : "showorder2",
			paratext : data.string.e1slide2text,
		}],
		additionalclass : "blackbackground",
		imageFeature : [
						{
						 showorderclass : "showorder1", 
						 styleFeat : "foodchain1",
						 sourceFeat : ex1imgPath+"foodchain1.png",
					   	}
					   ],
	},
	{
		paragraphs : [{
			showorderclass : "showorder2",
			paratext : data.string.e1slide3text,
		}],
		additionalclass : "blackbackground",
		imageFeature : [
						{
						 showorderclass : "showorder1", 
						 styleFeat : "foodchain2",
						 sourceFeat : ex1imgPath+"foodchain2.png",
					   	}
					   ],
	},	
	{
		paragraphs : [{
			showorderclass : "showorder2",
			paratext : data.string.e1slide4text,
		}],
		additionalclass : "blackbackground",
		imageFeature : [
						{ 
 						 showorderclass : "showorder1",
						 styleFeat : "foodchain3",
						 sourceFeat : ex1imgPath+"foodchain3.png",
					   	}
					   ],
	},
	{
		paragraphs : [{
			showorderclass : "showorder2",
			paratext : data.string.e1slide5text,
		}],
		additionalclass : "blackbackground",
		imageFeature : [
						{
						 showorderclass : "showorder1", 
						 styleFeat : "foodchain4",
						 sourceFeat : ex1imgPath+"foodchain4.png",
					   	}
					   ],
	},
	{
		paragraphs : [{
			showorderclass : "showorder1",
			paratext : data.string.e1slide6text,
		}],
		additionalclass : "blackbackground",
		imageFeature : [
						{
						 showorderclass : "showorder2", 
						 styleFeat : "foodchain",
						 sourceFeat : ex1imgPath+"foodchain.png",
					   	}
					   ],
	},
	{
		paragraphs : [{
			showorderclass : "showorder1",
			paratext : data.string.e1slide7text,
		}],
		additionalclass : "blackbackground",
		imageFeature : [
						{
						 showorderclass : "showorder2", 
						 styleFeat : "pollutionImageStyle",
						 sourceFeat : ex1imgPath+"industrypollution.png",
					   	}
					   ],
	},
	{
		sickvillageIntroData : data.string.e1slide8text,
		paratext : data.string.e1slide9text,
		sickvillageImgSrc : ex1imgPath+"sickvillage.png",
	},
	{
		sickvillageImgSrc : ex1imgPath+"sickvillage.png",
		hoverinstructionText : data.string.e1slide10text,
		sickanimal : [
			{
				diseasedata : data.string.e1hendisease,
				animalclassname : "hen",
				lensImgSrc : ex1imgPath+"virushen.png",
			},
			{
				diseasedata : data.string.e1pigdisease,
				animalclassname : "pig",
				lensImgSrc : ex1imgPath+"viruspig.png",
			},
			{
				diseasedata : data.string.e1dogdisease,
				animalclassname : "dog",
				lensImgSrc : ex1imgPath+"virusdog.png",
			},
			{
				diseasedata : data.string.e1catdisease,
				animalclassname : "cat",
				lensImgSrc : ex1imgPath+"viruscat.png",
			},
			{
				diseasedata : data.string.e1cowbrowndisease,
				animalclassname : "cowbrown",
				lensImgSrc : ex1imgPath+"virusbrowncow.png",
			},
			{
				diseasedata : data.string.e1cowgreydisease,
				animalclassname : "cowgrey",
				lensImgSrc : ex1imgPath+"virusgreycow.png",
			},
		],
	},
	{
		paragraphs : [{
			showorderclass : "showorder2",
			paratext : data.string.e1slide11text,
		}],
		additionalclass : "whitebackground",
		imageFeature : [
						{
						 showorderclass : "showorder1 microbesgroupsqueezeeffect", 
						 styleFeat : "groupmicrobes",
						 sourceFeat : ex1imgPath+"microbesgroup.png",
					   	}
					   ],
	},
	{
		paragraphs : [{
			showorderclass : "showorder2",
			paratext : data.string.e1slide12text,
		}],
		additionalclass : "whitebackground",
		imageFeature : [
						{
						 showorderclass : "showorder1", 
						 styleFeat : "groupmicrobes2",
						 sourceFeat : ex1imgPath+"microbesgroup.png",
					   	},
					   	{
						 showorderclass : "bacaraexpandeffect", 
						 styleFeat : "bacaraonly",
						 sourceFeat : ex1imgPath+"banyanbacara.png",
					   	}
					   ],
	},
	{
		paragraphs : [
			data.string.e1makingplantext1,data.string.e1makingplantext2,
			data.string.e1makingplantext3,data.string.e1makingplantext4,
		]
	},
	{	
		ballonsImgs : [
						{
							baloonclass : "bigbaloon animationdelay1s",
							sourceimg : ex1imgPath+"bigbaloon1.png",
					  	},					  	
					  	{
							baloonclass : "smallbaloon animationdelay2s",
							sourceimg : ex1imgPath+"smallbaloon1.png",
					  	},
					  	{
							baloonclass : "bunchbaloon animationdelay3s",
							sourceimg : ex1imgPath+"baloonbunch.png",
					  	},
					  ],
		bannerText : data.string.bannertextdata,
	},
	{	
		sendInviteInstruction : data.string.invitationInstructionText,
		microbetoinvite : 
		[
			{
				microbeClass : "goodmicrobe1",
				notificationPosition : "left",
				ismicrobegood : "good",
				sourceimgmicrobereal : goodmicrobesPath+"Lactobacillus-acidophilus.png",
				diseaseName : data.string.goodDiseaseName1,
				diseaseText : data.string.goodDiseaseText1,
				sendInviteText : data.string.sendInvitationData,
				cancelInviteText : data.string.cancelInvitationData,
			},
			{
				microbeClass : "goodmicrobe2",
				notificationPosition : "middle",
				ismicrobegood : "good",
				sourceimgmicrobereal : goodmicrobesPath+"Escherichia-coli.png",
				diseaseName : data.string.goodDiseaseName2,
				diseaseText : data.string.goodDiseaseText2,
				sendInviteText : data.string.sendInvitationData,
				cancelInviteText : data.string.cancelInvitationData,
			},
			{
				microbeClass : "goodmicrobe3",
				notificationPosition : "middle",
				ismicrobegood : "good",
				sourceimgmicrobereal : goodmicrobesPath+"Penicillium.png",
				diseaseName : data.string.goodDiseaseName3,
				diseaseText : data.string.goodDiseaseText3,
				sendInviteText : data.string.sendInvitationData,
				cancelInviteText : data.string.cancelInvitationData,
			},
			{
				microbeClass : "goodmicrobe4",
				notificationPosition : "right",
				ismicrobegood : "good",
				sourceimgmicrobereal : goodmicrobesPath+"Cyanobacteria.png",
				diseaseName : data.string.goodDiseaseName4,
				diseaseText : data.string.goodDiseaseText4,
				sendInviteText : data.string.sendInvitationData,
				cancelInviteText : data.string.cancelInvitationData,
			},
			{
				microbeClass : "goodmicrobe5",
				notificationPosition : "right",
				ismicrobegood : "good",
				sourceimgmicrobereal : goodmicrobesPath+"Yeast.png",
				diseaseName : data.string.goodDiseaseName5,
				diseaseText : data.string.goodDiseaseText5,
				sendInviteText : data.string.sendInvitationData,
				cancelInviteText : data.string.cancelInvitationData,
			},
			{
				microbeClass : "goodmicrobe6",
				notificationPosition : "left",
				ismicrobegood : "good",
				sourceimgmicrobereal : goodmicrobesPath+"Bacillus-thuringiensis.png",
				diseaseName : data.string.goodDiseaseName6,
				diseaseText : data.string.goodDiseaseText6,
				sendInviteText : data.string.sendInvitationData,
				cancelInviteText : data.string.cancelInvitationData,
			},
			{
				microbeClass : "goodmicrobe7",
				notificationPosition : "middle",
				ismicrobegood : "good",
				sourceimgmicrobereal : goodmicrobesPath+"Pseudomonas-putida.png",
				diseaseName : data.string.goodDiseaseName7,
				diseaseText : data.string.goodDiseaseText7,
				sendInviteText : data.string.sendInvitationData,
				cancelInviteText : data.string.cancelInvitationData,
			},
			{
				microbeClass : "badmicrobe1",
				notificationPosition : "middle",
				ismicrobegood : "bad",
				sourceimgmicrobereal : badmicrobesPath+"Salmonella-typhi.png",
				diseaseName : data.string.badDiseaseName3,
				diseaseText : data.string.badDiseaseText3,
				sendInviteText : data.string.sendInvitationData,
				cancelInviteText : data.string.cancelInvitationData,
			},
			{
				microbeClass : "badmicrobe2",
				notificationPosition : "right",
				ismicrobegood : "bad",
				sourceimgmicrobereal : badmicrobesPath+"Variola-Virus.png",
				diseaseName : data.string.badDiseaseName2,
				diseaseText : data.string.badDiseaseText2,
				sendInviteText : data.string.sendInvitationData,
				cancelInviteText : data.string.cancelInvitationData,
			},
			{
				microbeClass : "badmicrobe3",
				notificationPosition : "right",
				ismicrobegood : "bad",
				sourceimgmicrobereal : badmicrobesPath+"Vibrio-cholerae.png",
				diseaseName : data.string.badDiseaseName4,
				diseaseText : data.string.badDiseaseText4,
				sendInviteText : data.string.sendInvitationData,
				cancelInviteText : data.string.cancelInvitationData,
			},
			{
				microbeClass : "badmicrobe4",
				notificationPosition : "left-bottom",
				ismicrobegood : "bad",
				sourceimgmicrobereal : badmicrobesPath+"Mycobacterium-tuberculosis.png",
				diseaseName : data.string.badDiseaseName5,
				diseaseText : data.string.badDiseaseText5,
				sendInviteText : data.string.sendInvitationData,
				cancelInviteText : data.string.cancelInvitationData,
			},
			{
				microbeClass : "badmicrobe5",
				notificationPosition : "left-bottom",
				ismicrobegood : "bad",
				sourceimgmicrobereal : badmicrobesPath+"Ebola.png",
				diseaseName : data.string.badDiseaseName1,
				diseaseText : data.string.badDiseaseText1,
				sendInviteText : data.string.sendInvitationData,
				cancelInviteText : data.string.cancelInvitationData,
			},
			{
				microbeClass : "badmicrobe6",
				notificationPosition : "middle-bottom",
				ismicrobegood : "bad",
				sourceimgmicrobereal : badmicrobesPath+"Staphylococcus-aureus.png",
				diseaseName : data.string.badDiseaseName6,
				diseaseText : data.string.badDiseaseText6,
				sendInviteText : data.string.sendInvitationData,
				cancelInviteText : data.string.cancelInvitationData,
			},
			{
				microbeClass : "badmicrobe7",
				notificationPosition : "right-bottom",
				ismicrobegood : "bad",
				sourceimgmicrobereal : badmicrobesPath+"Mycobacterium-leprae.png",
				diseaseName : data.string.badDiseaseName7,
				diseaseText : data.string.badDiseaseText7,
				sendInviteText : data.string.sendInvitationData,
				cancelInviteText : data.string.cancelInvitationData,
			},
			{
				microbeClass : "badmicrobe8",
				notificationPosition : "right-bottom",
				ismicrobegood : "bad",
				sourceimgmicrobereal : badmicrobesPath+"Bacillus-Cereus.png",
				diseaseName : data.string.badDiseaseName8,
				diseaseText : data.string.badDiseaseText8,
				sendInviteText : data.string.sendInvitationData,
				cancelInviteText : data.string.cancelInvitationData,
			},
		]
	},
	{
		banyanbgsrc : ex1imgPath+"banyan.png",
	},
	{
		trainingbgsrc : ex1imgPath+"traininghall.png",
		books : [
			{
				bookaboutclass : "fungi",
				bookimgsrc : ex1imgPath+"trainingbook.png",
				bookname : data.string.trainingbookaboutfungi,
			},
			{
				bookaboutclass : "bacteria",
				bookimgsrc : ex1imgPath+"trainingbook.png",
				bookname : data.string.trainingbookaboutbacteria,
			},
			{
				bookaboutclass : "virus",
				bookimgsrc : ex1imgPath+"trainingbook.png",
				bookname : data.string.trainingbookaboutvirus,
			}
		]
	},
	{
		wrapperclass : "slide16wrapper",
		fieldsetLegendText:data.string.e1slide16heading,
		insideFieldsetTag: [
					{
						insideFieldsetTagtextclass : "slide16text1",
						insideFieldsetTagtext : data.string.e16text1,
					},
					{
						insideFieldsetTagtextclass : "slide16text2",
						insideFieldsetTagtext : data.string.e16text2,
					}
				],

	},
	{
		goodmicrobes : [
					{ goodmicrobename : "rowbackmicrobe1",},										
					{ goodmicrobename : "rowbackmicrobe2",},										
					{ goodmicrobename : "rowbackmicrobe3",},										
					{ goodmicrobename : "rowbackmicrobe4",},	
					{ goodmicrobename : "rowfrontmicrobe1",},										
					{ goodmicrobename : "rowfrontmicrobe2",},										
					{ goodmicrobename : "rowfrontmicrobe3",},										
					{ goodmicrobename : "rowfrontmicrobe4",},

				],

		badmicrobes : [
					{ badmicrobename : "rowbackmicrobe1",},										
					{ badmicrobename : "rowbackmicrobe2",},										
					{ badmicrobename : "rowbackmicrobe3",},										
					{ badmicrobename : "rowbackmicrobe4",},	
					{ badmicrobename : "rowfrontmicrobe1",},										
					{ badmicrobename : "rowfrontmicrobe2",},										
					{ badmicrobename : "rowfrontmicrobe3",},										
					{ badmicrobename : "rowfrontmicrobe4",},

				],
		goodHurrayScreamTextData : data.string.goodmicrobeapplaudtext,
		badHurrayScreamTextData : data.string.badmicrobeapplaudtext,
		quizcompleteinfodata : data.string.endslidetext1+"<br>"+data.string.endslidetext2,
	},
	{
		wrapperclass : "theEndwrapperclass",
		insideFieldsetTag: [
					{
						insideFieldsetTagtextclass : "endwrapperbottomtext",
						insideFieldsetTagtext : data.string.endslidetext3,
					},
				],

	},	
];

var banyancontent = [
	{
		containsboard : [data.string.banyanslidetext1],
	},
	{
		containsimage : {
			masterimgsrc : ex1imgPath+"banyanbacara.png",
			discipleimgsrc : ex1imgPath+"disciples.png",
		},
	},
	{
		containsdialogimage : {
				imagesrc : ex1imgPath+"banyanbacara.png",
				dilogdata : data.string.banyanslidedialog1,
				discipleimgsrc : ex1imgPath+"disciples.png",
			},
	},
	{
		containsdialogimage : {
				imagesrc : ex1imgPath+"banyanbacara.png",
				dilogdata : data.string.banyanslidedialog2,
				discipleimgsrc : ex1imgPath+"disciples.png",
			},
	},
	{
		containsdialogimage : {
				imagesrc : ex1imgPath+"banyanbacara.png",
				dilogdata : data.string.banyanslidedialog3,
				discipleimgsrc : ex1imgPath+"disciples.png",
			},
	},
];


var trainingcontent = [
	{
		trainingboardclass : "vertical-horizontal-center",
		boardtext : data.string.trainingslidetext1,
	},
	{
		trainingboardclass : "hidetrainingboard",
	},
	{
		trainingboardclass : "vertical-horizontal-center-trainingslide",
		boardtext : data.string.trainingslidetext2,
	},
	{
		trainingboardclass : "vertical-horizontal-center-trainingslide",
		boardtext : data.string.trainingslidetext3,
	}
];

var trainingbookcontent = [
	{
		okaytext : data.string.okaybuttontext,
		bookheading : data.string.trainingbookaboutfungi,
		description : [data.string.trainingbookfungilist1,
						data.string.trainingbookfungilist2,
						data.string.trainingbookfungilist3,
						data.string.trainingbookfungilist4,],
	},
	{
		okaytext : data.string.okaybuttontext,
		bookheading : data.string.trainingbookaboutbacteria,
		description : [data.string.trainingbookbacterialist1,
						data.string.trainingbookbacterialist2,
						data.string.trainingbookbacterialist3,
						data.string.trainingbookbacterialist4,
						data.string.trainingbookbacterialist5,],
	},
	{
		okaytext : data.string.okaybuttontext,
		bookheading : data.string.trainingbookaboutvirus,
		description : [data.string.trainingbookviruslist1,
						data.string.trainingbookviruslist2,
						data.string.trainingbookviruslist3,
						data.string.trainingbookviruslist4,
						data.string.trainingbookviruslist5,],
	},
	
];

var quizContent = [
			{
				questionText : data.string.question1,
				optionsarray : [
				{
					iscorrect : "incorrect",
					optionText : data.string.q1opt1,
				},
				{
					iscorrect : "incorrect",
					optionText : data.string.q1opt2,
				},
				{
					iscorrect : "correct",
					optionText : data.string.q1opt3,
				},
				
				],
				// afteranswerdescribedata : data.string.q1afteranswerdescribe,	
			},
			{
				questionText : data.string.question2,
				optionsarray : [
				{
					iscorrect : "correct",
					optionText : data.string.q2opt1,
				},
				{
					iscorrect : "incorrect",
					optionText : data.string.q2opt2,
				},
				{
					iscorrect : "incorrect",
					optionText : data.string.q2opt3,
				},
				
				],
				// afteranswerdescribedata : data.string.q2afteranswerdescribe,	
			},
			{
				questionText : data.string.question3,
				optionsarray : [
				{
					iscorrect : "incorrect",
					optionText : data.string.q3opt1,
				},
				{
					iscorrect : "correct",
					optionText : data.string.q3opt2,
				},
				{
					iscorrect : "incorrect",
					optionText : data.string.q3opt3,
				},
				
				],
				// afteranswerdescribedata : data.string.q3afteranswerdescribe,	
			},
			{
				questionText : data.string.question4,
				optionsarray : [
				{
					iscorrect : "incorrect",
					optionText : data.string.q4opt1,
				},
				{
					iscorrect : "incorrect",
					optionText : data.string.q4opt2,
				},
				{
					iscorrect : "correct",
					optionText : data.string.q4opt3,
				},
				
				],
				// afteranswerdescribedata : data.string.q4afteranswerdescribe,	
			},
			{
				questionText : data.string.question5,
				optionsarray : [
				{
					iscorrect : "incorrect",
					optionText : data.string.q5opt1,
				},
				{
					iscorrect : "correct",
					optionText : data.string.q5opt2,
				},
				{
					iscorrect : "incorrect",
					optionText : data.string.q5opt3,
				},
				
				],
				// afteranswerdescribedata : data.string.q5afteranswerdescribe,	
			},
			{
				questionText : data.string.question6,
				optionsarray : [
				{
					iscorrect : "incorrect",
					optionText : data.string.q6opt1,
				},
				{
					iscorrect : "incorrect",
					optionText : data.string.q6opt2,
				},
				{
					iscorrect : "correct",
					optionText : data.string.q6opt3,
				},
				
				],
				// afteranswerdescribedata : data.string.q6afteranswerdescribe,	
			},
			{
				questionText : data.string.question7,
				optionsarray : [
				{
					iscorrect : "incorrect",
					optionText : data.string.q7opt1,
				},
				{
					iscorrect : "correct",
					optionText : data.string.q7opt2,
				},
				{
					iscorrect : "incorrect",
					optionText : data.string.q7opt3,
				},
				
				],
				// afteranswerdescribedata : data.string.q7afteranswerdescribe,	
			},
			{
				questionText : data.string.question8,
				optionsarray : [
				{
					iscorrect : "correct",
					optionText : data.string.q8opt1,
				},
				{
					iscorrect : "incorrect",
					optionText : data.string.q8opt2,
				},
				{
					iscorrect : "incorrect",
					optionText : data.string.q8opt3,
				},
				
				],
				// afteranswerdescribedata : data.string.q8afteranswerdescribe,	
			},
			{
				questionText : data.string.question9,
				optionsarray : [
				{
					iscorrect : "correct",
					optionText : data.string.q9opt1,
				},
				{
					iscorrect : "incorrect",
					optionText : data.string.q9opt2,
				},
				{
					iscorrect : "incorrect",
					optionText : data.string.q9opt3,
				},
				
				],
				// afteranswerdescribedata : data.string.q9afteranswerdescribe,	
			},
			{
				questionText : data.string.question10,
				optionsarray : [
				{
					iscorrect : "incorrect",
					optionText : data.string.q10opt1,
				},
				{
					iscorrect : "incorrect",
					optionText : data.string.q10opt2,
				},
				{
					iscorrect : "correct",
					optionText : data.string.q10opt3,
				},

				],
				// afteranswerdescribedata : data.string.q10afteranswerdescribe,	
			}

];

$(function($) {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length+1;
	loadTimelineProgress($total_page,countNext+1);

/*
* textOnlySlide template
*/
function textOnlySlide() {
		var source = $("#textOnlySlide-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		countNext == 0 
		?
		 ($nextBtn.show(0)) 
		: 
		 ($nextBtn.show(0),$prevBtn.show(0));		 	
	}

/*
* animatedTextSlide template
*/
	function animatedTextSlide() {
		var source = $("#animatedTextSlide-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		
		var $paralines = $board.children('div.animatedTextSlide').children('p');
		var numberoflines = $paralines.length;
		
		var mysettimeout = setTimeout(function(){
			$paralines.eq(0).css("opacity",1);

			/*following .each chunk of code adds the transitionend event handler 
			to each para lines. the last line always shows the navigation next and 
			prev button*/
			$.each($paralines, function(index, val){				
			 	$paralines.eq(index).one(animationtransitionEnd, function() {
			 		if(index < numberoflines-1){					
						$paralines.eq(index+1).css('opacity', '1');
					}
					else if(index == numberoflines-1){
						$nextBtn.show(0);
						$prevBtn.show(0);
					}
				});
			});
		},0);	
	}

/*
* textNimageSlide template
*/
function textNimageSlide() {
		var source = $("#textNimageSlide-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		// $nextBtn.hide(0);
		// $prevBtn.hide(0);
		
		/*Not sure though, but with out giving a settimeout opacity is not working 
		this gives a solution | possible explanation : the applying of css to element
		goes before the template is loaded | other solution was just putting $nextBtn.hide(0);
		before this comment - wierd right? */
		var mysettimeout = setTimeout(function(){
			$textNimageSlide = $board.children('div.textNimageSlide');
			$firstorderedelement = $textNimageSlide.children('.showorder1');
			$secondorderedelement = $textNimageSlide.children('.showorder2');

			$firstorderedelement.css("opacity",1);

			$firstorderedelement.on(animationtransitionEnd, function() {
				$secondorderedelement.css("opacity",1);
			});

			$secondorderedelement.on(animationtransitionEnd, function() {
				$nextBtn.show(0);
				$prevBtn.show(0);
				clearTimeout(mysettimeout);
			});
		}, 0);		
	}

/*
* beforeSickVillage template
*/
function beforeSickVillage(){
		var source = $("#beforeSickVillage-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		$prevBtn.show(0);
		
		var $beforeSickVillage = $board.children('div.beforeSickVillage');
		var $whitedissolve = $beforeSickVillage.children('div.whitedissolve');
		var $beforeSickVillageHeading = $beforeSickVillage.children('p.beforeSickVillageHeading');
		var $slideNextBtn = $beforeSickVillage.children('div.slideNextBtn').html(data.string.next);
		var beforeSickVillageSlideCount = 0;
		$slideNextBtn.show(0);

		$slideNextBtn.on('click', function() {
			$(this).css('display', 'none');
			$prevBtn.hide(0); 
			$whitedissolve.css('opacity', '0');
		});

		$whitedissolve.on(transitionEnd, function(){			
			$beforeSickVillageHeading.css({
				'opacity' : '1',
				'top' : '5%'
			});
		});

		$beforeSickVillageHeading.on(transitionEnd, function() {
			$prevBtn.show(0);
			$nextBtn.show(0);
		});		
	}

/*
* sickVillage template
*/
function sickVillage() {
		var source = $("#sickVillage-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		var countsickhover = 0;

		var $sickvillagediv = $board.children('div.sickvillage');
		var $instruction = $sickvillagediv.children('p:nth-of-type(1)')
		var $hoverareasickVillage = $sickvillagediv.children('div');

		$instruction.css('opacity', '1');
		$hoverareasickVillage.one('mouseenter', function() {
			countsickhover++;
			$(this).attr('data-hovered','animalhovered');
			$instruction.css("opacity","0");
			if(countsickhover > 5){
				$instruction.css({
					"background-color" : "rgb(255, 89, 91)",
					"color" : "white",
					"margin-top" : "3%"
				});
				$instruction.addClass("allhoveredandfade");
				$instruction.html(data.string.e1slide10text2);				
				$nextBtn.show(0);
				$prevBtn.show(0);
			}
		});
	}

/*
* animation template
*/
function animationtemplate() {
		var source = $("#animation-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.show(0);
		$prevBtn.show(0);
	}

/*
* sendinvitation template
*/
function sendinvitation() {
		var source = $("#sendinvitation-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		
		var invitecount = 0;
		var $selectedGood , $selectedBad, $totalbadmicrobeinvited;
		var topicktotal = 7;
		var goodpickrequired = topicktotal;

		var $sendinvitation = $board.children('div.sendinvitation');
		var $instructionInvite = $sendinvitation.children('p#instructioninvite');
		var $instructionInviteclone;
		var $invitationFilterScreen = $sendinvitation.children('div.invitationFilterScreen');
		var $invitationFilter = $invitationFilterScreen.children('.invitationFilter');
		var $microbeselection = $sendinvitation.children('div.microbeselection');
		var $microbes = $microbeselection.children('div.microbeCollection');
		var $microbenotification = $microbes.children('div.microbenotification-wrap');
		var $invitationInteraction = $microbenotification.children('div.invitationInteraction');
		var $sendInvite = $invitationInteraction.children('button.inviteSend');
		var $cancelInvite = $invitationInteraction.children('button.inviteCancel');		

		$microbes.on('click', function() {			
			$(this).children('div.microbenotification-wrap').show(0);
			$microbenotification.not($(this).children('div.microbenotification-wrap')).css('display', 'none');
		});

		$cancelInvite.on('click', function(event){	
			event.stopPropagation(); /*use this to stop click to itself's parent*/

			var $thisnotification = $(this).parent("div").parent("div");
			$thisnotification.hide(0);

			var $thismicrobe = $(this).parent("div").parent("div").parent("div");

			if($thismicrobe.attr("data-goodness") === "bad" && $thismicrobe.attr("data-invitationstatus") === "invitationsent"){
				$thismicrobe.css({
					'pointer-events' : 'none',
					'opacity' : '0.7',
				});

				$thismicrobe.attr('data-invitationstatus', 'invitationpending');
			
				$totalbadmicrobeinvited = $microbes.filter(function(index) {
		        	if($(this).attr("data-goodness") === "bad" && $(this).attr("data-invitationstatus") === "invitationsent"){
		        		return $(this);
		        	}	        	
		        });	

				if($totalbadmicrobeinvited.length == 0){
					 
   					/*Look carefully at the code below! (you might find this repeated 
   					in the scope of sendInvitation function) Carefully view the first four lines of code :
   					code that clones instructionInvite element and removes and replaces with the same(clone)
   					just because regular remove and add class does not play the css animation everytime
   					For detailed demo visit this site, it has fine e.g : 
   					https://paulund.co.uk/playground/demo/restart-css-animation/
   					alternatively this site but the above one is better :
   					https://css-tricks.com/restart-css-animation/
   					*/
   					$instructionInviteclone = $instructionInvite.clone().removeClass();
   					$instructionInvite.remove();
   					$sendinvitation.append($instructionInviteclone);
   					$instructionInvite = $sendinvitation.children('p#instructioninvite');

   					$instructionInvite.addClass('startinstruction');
       				$instructionInvite.html(data.string.invitationdeselectedponderText);
					$invitationFilterScreen.show(0);		
				}
			}
		});

		$sendInvite.on('click', function(event){
			// this prevents the click event on children to propagate through and upto parent div
			event.stopPropagation();

			/* Act on the event */
			// hide notification div
			var $thismicrobe = $(this).parent("div").parent("div");
			$thismicrobe.hide(0);
			
			// disable interactivity on selected microbes
			var $currentmicrobe = $(this).parent("div.invitationInteraction").parent("div.microbenotification-wrap").parent("div.microbeCollection");
			$currentmicrobe.attr('data-invitationstatus', 'invitationsent');
			$currentmicrobe.addClass('disabledMicrobe');			
			
			if(++invitecount >= goodpickrequired){

				// this is similar clone and replace lines of code explained above 
   				$instructionInviteclone = $instructionInvite.clone().removeClass();
				$instructionInvite.remove();
				$sendinvitation.append($instructionInviteclone);
				$instructionInvite = $sendinvitation.children('p#instructioninvite');

   				$instructionInvite.addClass('startinstruction');
       			$instructionInvite.html(data.string.invitationselectedponderText);
				$invitationFilterScreen.show(0);					
			}			
		});

        $invitationFilter.on('click', function(event) {          
        /* Act on the event */ 
        	// separate good microbes from the set of selected microbes
	        $selectedGood = $microbes.filter(function(index) {
	        	if($(this).attr("data-goodness") === "good" && $(this).attr("data-invitationstatus") === "invitationsent"){
	        		return $(this);
	        	}	        	
	        });

        	// separate bad microbes from the set of selected microbes
	        $selectedBad = $microbes.filter(function(index) {
	        	if($(this).attr("data-goodness") === "bad" && $(this).attr("data-invitationstatus") === "invitationsent"){
	        		return $(this);
	        	}	        	
	        });

	        $totalbadmicrobeinvited = $microbes.filter(function(index) {
	        	if($(this).attr("data-goodness") === "bad" && $(this).attr("data-invitationstatus") === "invitationsent"){
	        		return $(this);
	        	}	        	
	        });	

	        // $selectedGood.css({"border":"2px dotted green","border-radius":"5px"});
	        $selectedBad.css({"border-color":"rgb(255, 87, 90)"});

	        $selectedBad.css({"pointer-events":"auto", "opacity":"1"});	        

	        // alert("goodselected = "+$selectedGood.length + " badselected = "+$totalbadmicrobeinvited.length + " to pick total=" + topicktotal);
	       	
	       	// update the style of instruction before displaying
	       	// this is similar clone and replace lines of code explained above 
       		$instructionInviteclone = $instructionInvite.clone().removeClass();
			$instructionInvite.remove();
			$sendinvitation.append($instructionInviteclone);
			$instructionInvite = $sendinvitation.children('p#instructioninvite');

       		$instructionInvite.addClass('runtimeinstruction');

	        // update instruction according to the selection of microbes
	        if ($selectedGood.length < topicktotal){
        		var pickcount = topicktotal - $selectedGood.length ;
        		goodpickrequired = pickcount;
        		invitecount = 0;
	        	var updateInstructionraw = data.string.youNeedinstruction;
	        	var badmicrobescancelinstruction = data.string.cancelbadinvitaioninstruction;
	        	var updateInstructionmodified = updateInstructionraw.replace("#microbesneeded#",pickcount);
	        	if($selectedBad.length > 0){
	        		updateInstructionmodified = updateInstructionmodified + badmicrobescancelinstruction.replace("#badselectedcount#",$selectedBad.length);
	        	}
	        	else if($selectedBad.length < 1){
	        		badmicrobescancelinstruction = data.string.cancelnotrequiredinstruction;
	        		updateInstructionmodified = updateInstructionmodified + badmicrobescancelinstruction;
	        	}
	        	$instructionInvite.html(updateInstructionmodified);
	        	// update goodpickrequired and total invites
	        	
	        	$invitationFilterScreen.css('display', 'none');
	        }

	        else if ($totalbadmicrobeinvited.length > 0){	        	
	        	var badmicrobescancelinstruction = data.string.cancelbadinvitaioninstruction;	        	
	        	var updateInstructionmodified = badmicrobescancelinstruction.replace("#badselectedcount#",$selectedBad.length);
	        	$instructionInvite.html(updateInstructionmodified);
	        	// update goodpickrequired and total invites
	        	
	        	$invitationFilterScreen.css('display', 'none');
	        }

	        else if ($selectedGood.length == topicktotal && $totalbadmicrobeinvited.length > 0){	        	
	        	var badmicrobescancelinstruction = data.string.cancelbadinvitaioninstruction;	        	
	        	var updateInstructionmodified = badmicrobescancelinstruction.replace("#badselectedcount#",$selectedBad.length);
	        	$instructionInvite.html(updateInstructionmodified);
	        	// update goodpickrequired and total invites
	        	
	        	$invitationFilterScreen.css('display', 'none');
	        }

	        else if ($selectedGood.length == topicktotal && $totalbadmicrobeinvited.length == 0){
	        	// this is similar clone and replace lines of code explained above 
       			$instructionInviteclone = $instructionInvite.clone().removeClass();
				$instructionInvite.remove();
				$sendinvitation.append($instructionInviteclone);
				$instructionInvite = $sendinvitation.children('p#instructioninvite');

       			$instructionInvite.addClass('youmayproceed');
	       		$instructionInvite.html(data.string.youCanProceedinstruction);
	        	$invitationFilter.css('display', 'none');
	        	$nextBtn.show(0);	        	
	        }
    	});     
    }

/*
* banyantree template
*/
	function banyantree(){
		var source = $("#banyantree-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);

		var $banyantree = $board.children('div.banyantree');
		var $banyantreecontent = $banyantree.children('div.banyantreecontent');
		var $slideNextBtn = $banyantree.children('.slideNextBtn').html(data.string.next);
		var $slidePrevBtn = $banyantree.children('.slidePrevBtn').html(data.string.prev);
		var contentindex = 0;
		
		function banyantreesubtemplate(){
			var source = $("#banyantreesubtemplate-template").html();
			var template = Handlebars.compile(source);
			var html = template(banyancontent[contentindex]);
			$banyantreecontent.html(html);

			var slidelength = banyancontent.length;
			
			if(contentindex > 0 && contentindex < slidelength-1){				
				$prevBtn.hide(0);
				$slideNextBtn.show(0);
				$slidePrevBtn.show(0);
			}
			else if(contentindex == 0){
				$prevBtn.show(0);
				$slideNextBtn.show(0);
			}
			else if(contentindex == slidelength-1){
				$slidePrevBtn.show(0);
				$nextBtn.show(0);
			}
		}	

		$slideNextBtn.on('click', function() {
			/* Act on the event */
			$(this).css('display', 'none');
			$slidePrevBtn.css('display', 'none');
			contentindex++;
			banyantreesubtemplate();
		});

		$slidePrevBtn.on('click', function() {
			/* Act on the event */
			$(this).css('display', 'none');
			$slideNextBtn.css('display', 'none');
			contentindex--;
			banyantreesubtemplate();
		});

		banyantreesubtemplate();
	}

/*
* traininghall template
*/
	function traininghall(){
		var source = $("#traininghall-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.css('display', 'none');

		var $traininghall = $board.children('div.traininghall');
		var $bookcontainer = $traininghall.children('.bookcontainer');
		var $books = $bookcontainer.children('figure.bookimage');
		var $traininghallcontent = $traininghall.children('div.traininghallcontent');
		var $bookcontent = $traininghall.children('div.bookcontent');
		var $slideNextBtn = $traininghall.children('.slideNextBtn').html(data.string.next);
		var contentindex = 0 , $trainingboard;
		
				
		function traininghallsubtemplate(){
			var source = $("#traininghallsubtemplate-template").html();
			var template = Handlebars.compile(source);
			var html = template(trainingcontent[contentindex]);
			$traininghallcontent.html(html);

			$trainingboard = $traininghallcontent.children('div.trainingboard');

			var slidelength = trainingcontent.length;
			
			if(contentindex > 0 && contentindex < slidelength-1){				
				$prevBtn.hide(0);
				$slideNextBtn.show(0);				
			}
			else if(contentindex == 0){
				$prevBtn.show(0);
				$slideNextBtn.show(0);
			}
			else if(contentindex == slidelength-1){
				$prevBtn.show(0);
				/*$nextBtn.show(0);*/
			}
		}

		// flags that store the state of books read/unread
		var fungiread = false;
		var bacteriaread = false;
		var virusread = false;

		function bookcontentsubtemplate(bookcontentindex){
			var source = $("#bookcontentsubtemplate-template").html();
			var template = Handlebars.compile(source);
			var html = template(trainingbookcontent[bookcontentindex]);
			$bookcontent.html(html);

			var $closebutton = $bookcontent.children('div.bookcontentscreen').children('div.closebutton');

			$closebutton.on('click', function(){			
				$bookcontent.css('display', 'none');

				/*when close button is close check if all books are read
				flag are updated on books clicks*/
				if(fungiread && bacteriaread && virusread){
					$nextBtn.show(0);
				}
			});
		}	

		
		$slideNextBtn.on('click', function() {
			/* Act on the event */
			$(this).css('display', 'none');
			contentindex++;
			switch(contentindex){
				case 1: $bookcontainer.css('opacity', '1');break;
				case 2: $bookcontainer.css('bottom', '40%');
						$bookcontainer.one(transitionEnd, function() {			
							$bookcontainer.addClass('levitate');
						});
						break;
				case 3: $books.css('pointer-events', 'auto');
						break;
				default:break;
			}
			traininghallsubtemplate();
		});

		$books.on('click', function(){
			$trainingboard.is(':visible') ? $trainingboard.css("display","none") : null;
			switch($(this).attr("data-bookabout")){
				case "bookaboutfungi" : bookcontentsubtemplate(0); fungiread ? null : fungiread = true; break;
				case "bookaboutbacteria" : bookcontentsubtemplate(1); bacteriaread ? null : bacteriaread = true; break;
				case "bookaboutvirus" : bookcontentsubtemplate(2); virusread ? null : virusread = true; break;
				default:break;
			}
			$bookcontent.show(0);
		});

		traininghallsubtemplate();
	}

/*
* quiz template
*/
function quiztemplate(){
		var source = $("#quiz-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		var $quiz = $board.children('div.quiz');
		var $goodMicrobeScoreContainer = $quiz.children('b.goodMicrobeScore');
		var $badMicrobeScoreContainer = $quiz.children('b.badMicrobeScore');
		var $quizBoard = $quiz.children('div.quizBoard');
		var $nextQn = $quiz.children('.nextQn').html(data.string.next);
		var $goodMicrobeSeat = $quiz.children('div.goodMicrobeSeat');
		var $badMicrobeSeat = $quiz.children('div.badMicrobeSeat');
		var $goodStadiumMicrobe = $goodMicrobeSeat.children('div[class^=stadium_good]');
		var $badStadiumMicrobe = $badMicrobeSeat.children('div[class^=stadium_bad]');
		var $goodHurrayText = $goodMicrobeSeat.children('b.goodHurrayScreamText');
		var $badHurrayText = $badMicrobeSeat.children('b.badHurrayScreamText');
		var $quizcompleteinfo = $quiz.children('p.quizcompleteinfo');
		var questionnumber = 0;
		var goodmicrobescore = 0, badmicrobescore = 0;

		function quizsubtemplate(){
			var source = $("#quizboard-template").html();
			var template = Handlebars.compile(source);
			var html = template(quizContent[questionnumber]);
			$quizBoard.html(html);

			var $question = $quizBoard.children('p.question');
			var $optionsBoard = $quizBoard.children('.optionsBoard');
			var $optionIndividual = $optionsBoard.children('b.optionIndividual');
			var $afteranswerdescribe = $quizBoard.children('p.afteranswerdescribe');

			$optionIndividual.on('click', function() {	
				$(this).removeClass('optionIndividualNeutral');
				$goodStadiumMicrobe.removeClass('microbeneutral');
				$badStadiumMicrobe.removeClass('microbeneutral');

				if ($(this).data("correct") == "correct") {		
					$(this).addClass('optionIndividualCorrect');					
					$goodStadiumMicrobe.addClass('microbewin');
					$badStadiumMicrobe.addClass('microbelose');
					$goodMicrobeScoreContainer.html(++goodmicrobescore);
					$afteranswerdescribe.text(data.string.youansweredcorrect+" "+$afteranswerdescribe.text());
					$afteranswerdescribe.css('opacity', '1');
					$goodHurrayText.addClass('flyYahoo');
				}

				else if($(this).data("correct") == "incorrect"){
					$(this).addClass('optionIndividualInCorrect');
					$goodStadiumMicrobe.addClass('microbelose');
					$badStadiumMicrobe.addClass('microbewin');
					$badMicrobeScoreContainer.html(++badmicrobescore);
					$afteranswerdescribe.text(data.string.youansweredwrong+" "+$afteranswerdescribe.text());
					$afteranswerdescribe.css('opacity', '1');
					$badHurrayText.addClass('flyYahoo');
				}
				$optionIndividual.css('cursor','default');
				$optionIndividual.off("click");
				$nextQn.show(0);				
			});		
		}

		$nextQn.on('click', function() {
			/* Act on the event */
			$(this).css('display', 'none');
			questionnumber+=1;
			if(questionnumber < quizContent.length){
				quizsubtemplate();
				$goodStadiumMicrobe.removeClass('microbewin microbelose').addClass('microbeneutral');
				$badStadiumMicrobe.removeClass('microbewin microbelose').addClass('microbeneutral');
				$goodHurrayText.removeClass('flyYahoo');
				$badHurrayText.removeClass('flyYahoo');
			}
			else if(questionnumber == quizContent.length){
				$quizBoard.remove();
				$quizcompleteinfo.css('opacity', '1');
				$nextBtn.show(0);
			}
			
		});
		quizsubtemplate();
}

textOnlySlide(countNext+=0);
// textNimageSlide(countNext+=10);
// animatedTextSlide(countNext+=12);
// beforeSickVillage(countNext+=7);
// sickVillage(countNext+=8);
// sendinvitation(countNext+=13);
// animationtemplate(countNext+=12);
// textOnlySlide(countNext+=16);
// quiztemplate(countNext+=17);
// banyantree(countNext+=14);
// traininghall(countNext+=15);
// textOnlySlide(countNext+=18);

$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css("display","none");
		countNext++;		
		switch (countNext){
			case 1:				
				textNimageSlide();
				break;
			case 2:
				textNimageSlide();
				break;
			case 3:
				textNimageSlide();
				break;
			case 4:
				textNimageSlide();
				break;
			case 5:
				textNimageSlide();
				break;
			case 6:
				textNimageSlide();
				break;
			case 7:
				beforeSickVillage();
				break;
			case 8:
				sickVillage();
				break;
			case 9:
				textNimageSlide();
				break;
			case 10:
				textNimageSlide();
				break;
			case 11:
				animatedTextSlide();
				break;
			case 12:
				animationtemplate();
				break;
			case 13:
				sendinvitation();
				break;
			case 14:
				banyantree();
				break;
			case 15:
				traininghall();
				break;
			case 16:
				textOnlySlide();
				break;
			case 17:
				quiztemplate();
				break;
			case 18:
				textOnlySlide();
				break;
			case 19:
				ole.activityComplete.finishingcall();
				break;
			default:break;
		}

		loadTimelineProgress($total_page,countNext+1);
	});

$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css("display","none");
		countNext--;		
		switch (countNext) {
			case 0:				
				textOnlySlide();
				break;
			case 1:				
				textNimageSlide();
				break;
			case 2:
				textNimageSlide();
				break;
			case 3:
				textNimageSlide();
				break;
			case 4:
				textNimageSlide();
				break;
			case 5:
				textNimageSlide();
				break;
			case 6:
				textNimageSlide();
				break;
			case 7:
				beforeSickVillage();
				break;
			case 8:
				sickVillage();
				break;
			case 9:
				textNimageSlide();
				break;
			case 10:
				textNimageSlide();
				break;
			case 11:
				animatedTextSlide();
				break;
			case 12:
				animationtemplate();
				break;
			case 13:
				sendinvitation();
				break;
			case 14:
				banyantree();
				break;
			case 15:
				traininghall();
				break;
			case 16:
				textOnlySlide();
				break;
			case 17:
				quiztemplate();
				break;
			default:break;
		}

		loadTimelineProgress($total_page,countNext+1);
	});

})(jQuery);




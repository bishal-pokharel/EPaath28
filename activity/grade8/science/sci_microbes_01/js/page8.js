var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";


var content = [{
    heading: data.string.p2_4,
    title1: data.string.p4_1,
    title2: data.string.p4_2,
    title3: data.string.p4_3,
    title4: data.string.p4_4,
    cap1: data.string.c1_2,
    cap2: data.string.c2,
    cap3: data.string.c3,
    cap4: data.string.c4,
    cap5: data.string.c5,
    background: $ref + "/images/page8/fungi_tabs.png",
  },
  {
    title1: data.string.p4_1,
    title2: data.string.p4_2,
    title3: data.string.p4_3,
    title4: data.string.p4_4,
    bg: $ref + "/images/page8/bg.jpg",
    deff: [

      {
        def_class: "list1",
        def1: data.string.p8_1
      },
      {
        def_class: "list2",
        def1: data.string.p8_1_1
      },
      {
        def_class: "list3",
        def1: data.string.p8_2
      },
      {
        def_class: "list4",
        def1: data.string.p8_3
      },
      {
        def_class: "list5",
        def1: data.string.p8_4
      },
    ]
  },

  {
    title1: data.string.p4_1,
    title2: data.string.p4_2,
    title3: data.string.p4_3,
    title4: data.string.p4_4,
    para1: data.string.p4_5_6,
    bacteria_img: $ref + "/images/page4/Microbs-war04.gif",
    text_bubble: $ref + "/images/page4/speech-bubble-hi.png",

    sec_p: [{
        sec_class: "sec_para1",
        st1: data.string.p8_5
      },
      {
        sec_class: "sec_para2",
        st1: data.string.p8_6
      },
      {
        sec_class: "sec_para3",
        st1: data.string.p8_6_1
      },
      {
        sec_class: "sec_para4",
        st1: data.string.p8_6_2
      },
    ],

    img_array: [{
        img_name: "onwater",
        popup1: $ref + "/images/page8/fungionwater.png"
      },
      {
        img_name: "onplant",
        popup1: $ref + "/images/page8/fungionplant.jpg"
      },
      {
        img_name: "onearth",
        popup1: $ref + "/images/page8/fungy.png"
      },
      {
        img_name: "onbread",
        popup1: $ref + "/images/page8/fruits.png"
      },
    ],

  },

  {
    title1: data.string.p4_1,
    title2: data.string.p4_2,
    title3: data.string.p4_3,
    title4: data.string.p4_4,

    def1: data.string.p8_7,
    fungi: $ref + "/images/page8/mushroom.gif",
    img_info: data.string.p8_9_2,
    img_info2: data.string.p8_9_1,

    def2: data.string.p8_7_1,
    fungi2: $ref + "/images/page8/mushroom-root.gif",
    img_info3: data.string.p8_9_4,
    img_info4: data.string.p8_9_3,

  },

  {
    title1: data.string.p4_1,
    title2: data.string.p4_2,
    title3: data.string.p4_3,
    title4: data.string.p4_4,
    para4: data.string.p4_6_1,
    para5: data.string.p4_6_2,
    edible: $ref + "/images/page8/Edible_Fungis.png",
    poisonous: $ref + "/images/page8/Poisonous-Fungis-01.png",
    info1: data.string.edible,
    info2: data.string.poison,

    textblock: [{
        divname: "sidebox cssfadein",
        divdata: data.string.p8_91_1
      },
      {
        divname: "sidebox2 cssfadein2",
        divdata: data.string.p8_91_2
      },
      {
        divname: "sidebox3 cssfadein3",
        divdata: data.string.p8_91_3
      }
    ],
    textblock3: [{
        divname: "sidebox cssfadein",
        divdata: data.string.p8_91_4
      },
      {
        divname: "sidebox3 cssfadein2",
        divdata: data.string.p8_91_5
      },
    ],
    imageblock: [{
        imgname: "med1",
        imgsrc: $ref + "/images/page8/medicines.jpg"
      },
      {
        imgname: "med3",
        imgsrc: $ref + "/images/page8/bread.jpg"
      },
    ],
    imageblock3: [{
        imgname: "med1",
        imgsrc: $ref + "/images/page8/strawberry.jpeg"
      },
      {
        imgname: "med4",
        imgsrc: $ref + "/images/page8/leaf.jpeg"
      },
    ],
  },
  {
    title1: data.string.p4_1,
    title2: data.string.p4_2,
    title3: data.string.p4_3,
    title4: data.string.p4_4,
    repro_data: data.string.p8_999_1,
    cutie1: $ref + "/images/page8/icon01a.png",
    cutie2: $ref + "/images/page8/icon01b.png",
    cutie3: $ref + "/images/page8/icon01c.png",
    cutie4: $ref + "/images/page8/icon01d.png",

    head1: data.string.p8_11,
    des1: data.string.p8_12,
    head2: data.string.p8_13,
    des2: data.string.p8_14,
    head3: data.string.p8_15,
    des3: data.string.p8_16,
    head4: data.string.p8_17,
    des4: data.string.p8_18,
  }
];

function notifyuser($notifyinside) {
  //check if $notifyinside is provided
  typeof $notifyinside !== "object" ?
    alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
    null;

  /*variable that will store the element(s) to remove notification from*/
  var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
  // if there are any notifications removal required add the event handler
  if ($allnotifications.length > 0) {
    $allnotifications.one('click', function() {
      /* Act on the event */
      $(this).attr('data-isclicked', 'clicked');
      $(this).removeAttr('data-usernotification');
    });
  }
}

$(function() {
  var $value = 1;
  var $board = $('.board');
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $tabBtn = $(".tab_nextBtn").html(data.string.next);
  var $tabPBtn = $(".tab_prevBtn");
  var countNext = 0;
  var count = 0;
  var click_count = 0;
  var tabBtn_click = 0;
  loadTimelineProgress(6, countNext + 1);

  /*=====================================
  =            datahighlight            =
  =====================================*/
  function texthighlight($highlightinside) {
    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var replaceinstring;
    var texthighlightstarttag = "<span class='parsedstring'>";
    var texthighlightendtag = "</span>";
    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        // console.log(val);
        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
        $(this).html(replaceinstring);
      });
    }
  }



  /*=====  End of datahighlight  ======*/

  /*
   * first
   */
  function first() {
    var source = $("#first-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $prevBtn.hide(0);
    $nextBtn.show(0);
    $("#tabContainer").hide(0);
  }
  /*
   *
   * second
   */
  function second() {
    var source = $("#second-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $prevBtn.hide(0);
    $nextBtn.hide(0);
    $tabBtn.show(0);
    texthighlight($board);
    $("#tabContainer").show(0);
    $("#tab1").addClass('highlightborder');
    $(".def_part").show(0);
    $(".list1").delay(10).show(80);
    var Count = 0;
    $tabBtn.click(function() {
      Count++;
      switch (Count) {
        case 1:
          $(".list2").delay(10).show(80);

          break;

        case 2:
          $(".list3").delay(10).show(80);
          break;

        case 3:
          $(".list4").delay(10).show(80);
          $tabBtn.hide(0);
          $nextBtn.show(0);
          $prevBtn.show(0);
          break;
        default:
          break;
      }
    });
  }

  /*
   *
   * third
   */
  function third() {
    var source = $("#third-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $prevBtn.hide(0);
    $nextBtn.hide(0);
    $tabBtn.hide(0);
    texthighlight($board);


    $("#tab2").addClass('highlightborder');
    $("#bac_def").delay(500).fadeIn(500);

    $(".sec_para1").addClass("cssfadein");

    $(".sec_para1").on(animationend, function() {
      $("#onwater").addClass("cssfadein");
    });
    $("#onwater").on(animationend, function() {
      $(".sec_para2").addClass("cssfadein");
    });

    $(".sec_para2").on(animationend, function() {
      $("#onplant").addClass("cssfadein");
      $tabBtn.delay(1000).show(0);
    });

    $tabBtn.on("click", function() {
      $(this).hide(0);
      $(".sec_para1, .sec_para2, #onwater, #onplant").removeClass("cssfadein");
      $(".sec_para3").addClass("cssfadein");
    });

    $(".sec_para3").on(animationend, function() {
      $("#onbread").addClass("cssfadein");
    });

    $("#onbread").on(animationend, function() {
      $(".sec_para4").addClass("cssfadein");
    });

    $(".sec_para4").on(animationend, function() {
      $("#onearth").addClass("cssfadein");
      $tabBtn.hide(0);
      $nextBtn.delay(1000).show(0);
      $prevBtn.delay(1000).show(0);
    });

  }

  /*
   *
   * fourth
   */
  function fourth() {
    var source = $("#fourth-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $prevBtn.hide(0);
    $nextBtn.hide(0);
    $tabBtn.hide(0);

    $tabBtn.delay(3000).show(80);
    // $tabBtn.show(0);
    $("#tab3").addClass('highlightborder');
    $("#fungi_repro").delay(1000).show(80);
    $(".label1").delay(1000).show(80);

    $tabBtn.on("click", function() {
      $(".def_wrapper2").show(0);
      $(".def_wrapper1").hide(0);
      $(this).hide(0);
      $nextBtn.delay(1200).show(80);
      $prevBtn.delay(1200).show(80);
      $("#l1").delay(1000).show(80);
    });
  }

  /*
   *
   * fifth
   */
  function fifth() {
    var source = $("#fifth-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $nextBtn.hide(0);
    $tabBtn.hide(0);
    $prevBtn.hide(0);

    $("#tab4").addClass('highlightborder');

    $("#i1").delay(500).show(0);
    $(".edible").delay(1000).fadeIn(80);

    $("#i2").delay(1500).show(0);
    $(".poisonous").delay(2000).fadeIn(80);
    $tabBtn.delay(2500).show(0);

    $("#repro_def > p:nth-of-type(1)").text(data.string.p8_9);
    $("#repro_def > p:nth-of-type(2)").text(data.string.p8_91);

    var Count_click = 0;
    $tabBtn.click(function() {
      Count_click++;
      console.log(Count_click);
      switch (Count_click) {
        case 1:
          $(".bac_box_wrapper1").hide(0);
          $(".bac_box_wrapper3").show(0);
          $("#repro_def > p:nth-of-type(1)").text(data.string.p8_10);
          $("#repro_def > p:nth-of-type(2)").hide(0);
          $nextBtn.hide(0);
          $prevBtn.hide(0);
          $tabBtn.show(0);
          break;

        case 2:
          $(".medicines_box").css("display", "none");
          $(".fruit_box").show(0);
          $prevBtn.hide(0);
          $nextBtn.show(0);
          $tabBtn.hide(0);
          break;
        default:
          break;
      }
    });
  }

  /*
   *
   * sixth
   */
  function sixth() {
    var source = $("#sixth-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $nextBtn.hide(0);
    $prevBtn.show(0);
    $tabBtn.hide(0);

    $("#tab4").addClass('highlightborder');
    var chapend1 = false;
    var chapend2 = false;
    var chapend3 = false;
    var chapend4 = false;

    var all_p = $board.children(".bac_box_w2").children("div");
    all_p.click(function() {
      var id_p = $(this).attr("id");
      if (id_p == "bac_box5") {
        chapend1 = true;
        $("#" + id_p + " > .textblock").hide(0);
        $("#desc1").delay(100).show(900);
        $("#bac_box5").css("top", "4%");
        $(this).children(".handicon").hide(0);
      } else if (id_p == "bac_box6") {
        chapend2 = true;
        $("#" + id_p + " > .textblock").hide(0);
        $("#desc2").delay(100).show(900);
        $("#bac_box6").css("top", "4%");
        $(this).children(".handicon").hide(0);
      } else if (id_p == "bac_box7") {
        chapend3 = true;
        $("#" + id_p + " > .textblock").hide(0);
        $("#desc3").delay(100).show(900);
        $(this).children(".handicon").hide(0);
      } else if (id_p == "bac_box8") {
        chapend4 = true;
        $("#" + id_p + " > .textblock").hide(0);
        $("#desc4").delay(100).show(900);
        $(this).children(".handicon").hide(0);
      }
      if (chapend1 == true && chapend2 == true && chapend3 == true && chapend4 == true) {
        ole.footerNotificationHandler.pageEndSetNotification();
        $(".footerNotification").show(0);
      };
    });
  }

  first();

  $nextBtn.on('click', function() {
    loadTimelineProgress(6, countNext + 2);

    countNext++;
    switch (countNext) {
      case 1:
        second();
        break;
      case 2:
        third();
        break;
      case 3:
        fourth();
        break;
      case 4:
        fifth();
        break;
      case 5:
        sixth();
        break;

      default:
        break;
    }
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on('click', function() {
    countNext--;
    // alert(countNext);
    switch (countNext) {
      case 4:
        fifth();
        $(".footerNotification").hide(0);
        break;
      case 3:
        fourth();
        break;
      case 2:
        third();
        break;
      case 1:
        second();
        break;
      case 0:
        first();
        break;

      default:
        break;
    }
  });
});

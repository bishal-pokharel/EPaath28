var content=[
	{
		lokharke :"images/lokharke/definition2.png",
		heading : data.string.p3_1,
		deff :[
			{def:data.string.p3_2},
			{def:data.string.p3_2_1},
			{def:data.string.p3_2_2},
		],
		bacteria : $ref+"/images/page3/1.png",
		virus : $ref+"/images/page3/2.png",
		fungi : $ref+"/images/page3/3.png",
		info1 : data.string.p3_3,
		info2 : data.string.p3_4,
		info3 : data.string.p3_5,
		figcaption1 : data.string.p3_clickcaption,
   	figcaption2 : data.string.p3_clickcaption,
   	figcaption3 : data.string.p3_clickcaption,
	},
];


$(function () {
		var $board = $('.board');
		var countClick = 0;
		// $length = 0,
		// $area = 0,
		var countNext = 0;
		// var $total_page = 3;
		// loadTimelineProgress($total_page,countNext+1);

/*
* first
*/
	function first () {
		var source = $("#first-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		loadTimelineProgress(1,1);

		$(".definition >p").delay(500).fadeIn(800);
		$("#img1").delay(100).fadeIn(900);
		$("#img2").delay(200).fadeIn(900);
		$("#img3").delay(300).fadeIn(900);

		/*=====================================
		=            datahighlight            =
		=====================================*/
		function texthighlight($highlightinside){
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var replaceinstring;
			var texthighlightstarttag = "<span class='parsedstring'>";
			var texthighlightendtag = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					// console.log(val);
					replaceinstring = $(this).html();
					replaceinstring = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}

		texthighlight($board);

		/*=====  End of datahighlight  ======*/


		$("#img1").click (function (){
			$("#img3").toggleClass('flipped');
			$(this).css("cursor", "default");
			$(".back1").show(0);
			countClick++;
			if (countClick==3) {
                ole.footerNotificationHandler.pageEndSetNotification();
            }
		});

		$("#img2").click (function (){
			$("#img3").toggleClass('flipped');
			$(".back2").show(0);
			$(this).css("cursor", "default");
			countClick++;
			if (countClick==3) {
				ole.footerNotificationHandler.pageEndSetNotification();
			};
		});

		$("#img3").click (function (){
			$("#img3").toggleClass('flipped');
			$(this).css("cursor", "default");
			$(".back3").show(0);
			countClick++;
			if (countClick==3) {
				ole.footerNotificationHandler.pageEndSetNotification();
			};
		});

	}

	first(countNext);


});

var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var content=[
	{
		heading : data.string.p2_3,
		title1 : data.string.p4_1,
		title2 : data.string.p4_2,
		title3 : data.string.p4_3,
		title4 : data.string.p4_4,
		cap1 : data.string.c1_1,
		cap2 : data.string.c2,
		cap3 : data.string.c3,
		cap4 : data.string.c4,
		cap5 : data.string.c5,
		background : $ref+"/images/page6/virus_tabs.png",
	},
	{
		title1 : data.string.p4_1,
		title2 : data.string.p4_2,
		title3 : data.string.p4_3,
		title4 : data.string.p4_4,

		def : data.string.p6_1,
		arrPoints:[
			{point_class:"p1", point:data.string.p6_1_1},
			{point_class:"p3", point:data.string.p6_3},
			{point_class:"p4", point:data.string.p6_4},
			{point_class:"p5", point:data.string.p6_5},
			{point_class:"p6", point:data.string.p6_6},
		],

		def2 : data.string.p6_6_1,
		subhead : data.string.p6_6_2,
		sideimg: $ref+"/images/page6/virus-for-label.png",
		capsid_text:data.string.capsid_text_xml,
		capsid_image: $ref+"/images/page6/capsuds.png",
		capsid_virustext1:data.string.virus_type_1,
		capsid_virustext2:data.string.virus_type_2,
		capsid_virustext3:data.string.virus_type_3,
		point:data.string.p6_6_2_1,
		point1:data.string.p6_6_2_2,
		point2:data.string.p6_6_2_3,
		point3:data.string.p6_6_6_1,
		point4:data.string.p6_6_6_2,
		point5:data.string.p6_6_6_3,
		point6:data.string.p6_6_6_4,

		text_title_top:data.string.head_virus,
		image_head_virus : $ref+"/images/page6/head.png",
		image : $ref+"/images/page6/virus.png"

	},
	{
		title1 : data.string.p4_1,
		title2 : data.string.p4_2,
		title3 : data.string.p4_3,
		title4 : data.string.p4_4,

		diyImageSource : "images/diy/diy2.png",
		diyTextData : data.string.d_1,
		text_1_1:data.string.d_3,

		arrBack:[
			{txt:1,whatarrs:data.string.d_7_1,opt1:data.string.d_7_1_op1,opt2:data.string.d_7_1_op2,opt3:data.string.d_7_1_op3,corr1:0,corr2:1,corr3:0},
			{txt:2,whatarrs:data.string.d_7_2,opt1:data.string.d_7_2_op1,opt2:data.string.d_7_2_op2,opt3:data.string.d_7_2_op3,corr1:0,corr2:0,corr3:1},
			{txt:3,whatarrs:data.string.d_7_3,opt1:data.string.d_7_3_op1,opt2:data.string.d_7_3_op2,opt3:data.string.d_7_3_op3,corr1:0,corr2:1,corr3:0},
			{txt:4,opt1:data.string.d_7_4_op1,opt2:data.string.d_7_4_op2,opt3:data.string.d_7_4_op3,corr1:1,corr2:0,corr3:0,remain:data.string.d_7_4},
		],
		whatImgvalme:$ref+"/images/diy2/1.png",
		virus_anim : $ref+"/images/diy2/5.gif",

		definitionTextData : data.string.next_3,
        definitionFirstWord:data.string.p2_3,
        definitionData : [	data.string.p5_1,
        					data.string.p5_2,
        					data.string.p5_3,
        					data.string.p5_4,
        					data.string.p5_5
        ]
	},
	{
		title1 : data.string.p4_1,
		title2 : data.string.p4_2,
		title3 : data.string.p4_3,
		title4 : data.string.p4_4,

		para1 : data.string.p6_8,
		para2 : data.string.p6_9,
		para3 : data.string.p6_10,
		para4 : data.string.p6_11,
		para5 : data.string.p6_12,

		middle_text: data.string.p6_13,
		bulletpoint: data.string.p6_14,
		cutie1 : $ref+"/images/page6/icon02d.png",
		cutie2 : $ref+"/images/page6/icon02b.png",
		cutie3 : $ref+"/images/page6/icon02c.png",
		cutie4 : $ref+"/images/page6/icon02b.png",

		image_array:[
			{img_name:"hand", pic:$ref+"/images/page6/shake-hand.png", text:data.string.p6_15},
			{img_name:"food", pic:$ref+"/images/page6/food.png", text:data.string.p6_16},
			{img_name:"girl", pic:$ref+"/images/page6/girl.png", text:data.string.p6_17},
			{img_name:"bee", pic:$ref+"/images/page6/bee.png", text:data.string.p6_18},
		],

	},
	{
		title1 : data.string.p4_1,
		title2 : data.string.p4_2,
		title3 : data.string.p4_3,
		title4 : data.string.p4_4,

		def : data.string.p6_19,
		def3: data.string.p6_19_1,
		def2 : data.string.p7_2_6,
		def2 : data.string.p7_2_7,
		repro_img_array:[
			{pic_name:"cycle1", virus_repro : $ref+"/images/page6/virus1.png", name:"virus_info1",virus_info: data.string.p6_20},
			{pic_name:"cycle2", virus_repro : $ref+"/images/page6/virus2.png", name:"virus_info2",virus_info: data.string.p6_21},
			{pic_name:"cycle3", virus_repro : $ref+"/images/page6/virus3.png", name:"virus_info3",virus_info: data.string.p6_21_1},
			{pic_name:"cycle4", virus_repro : $ref+"/images/page6/virus4.png", name:"virus_info4",virus_info: data.string.p6_21_2},
			{pic_name:"cycle5", virus_repro : $ref+"/images/page6/virus_animation.gif", name:"virus_info5",virus_info: data.string.p6_21_3},
		],
	},
	{
		title1 : data.string.p4_1,
		title2 : data.string.p4_2,
		title3 : data.string.p4_3,
		title4 : data.string.p4_4,
		def_array:[
			{deff: data.string.p6_22},
			{deff: data.string.p6_23},
			{deff: data.string.p6_24},

		],

		cutie1 : $ref+"/images/page6/icon02a.png",
		cutie2 : $ref+"/images/page6/varialo.png",
		cutie3 : $ref+"/images/page6/rhino.png",
		cutie4 : $ref+"/images/page6/polio.png",
		cutie5 : $ref+"/images/page6/icon02e.png",
		cutie6 : $ref+"/images/page6/corona.png",
		info1 : data.string.p7_2_8,
		info2 : data.string.p7_2_9,
		info3 : data.string.p7_2_10,
		info4 : data.string.p7_2_11,
		info5 : data.string.p7_2_12,
		info6 : data.string.p7_2_13,
		info7 : data.string.p7_2_14,
		info8 : data.string.p7_2_15,
		info9 : data.string.p7_2_16,
		infonew1 : data.string.p7_2_22,
		infonew2 : data.string.p7_2_23,
		info10 : data.string.p7_2_17,
		info11 : data.string.p7_2_18,
		info12 : data.string.p7_2_19,
		info13 : data.string.p7_2_20,
		info14 : data.string.p7_2_21,
		title_wash: data.string.washtitle,
		captionwash1:data.string.wash1,
		captionwash2:data.string.wash2,
		captionwash3:data.string.wash3,
		imgwash1: $ref+"/images/page6/ahnd.png",
		imgwash3: $ref+"/images/page6/two-girl.png",
		imgwash2: $ref+"/images/page6/tree_girl.png",
		captionwash1_a:data.string.wash1_a,
		captionwash2_a:data.string.wash2_a,
		captionwash3_a:data.string.wash3_a,
		handwashing:data.string.wash_hands,
		soapkills:data.string.soap_text,
		title_soap_text:data.string.saop_des_title,
		soaptext_1:data.string.soap_text_1,
		soaptext_2:data.string.soap_text_2,
		soaptext_3:data.string.soap_text_3,
		soaptext_4:data.string.soap_text_4,
		soaptext_5:data.string.soap_text_5,
		soaptext_6:data.string.soap_text_6,
		soaptext_7:data.string.soap_text_7,
		soaptext_8:data.string.soap_text_8,
		soaptext_9:data.string.soap_text_9,
		soaptext_10:data.string.soap_text_10,
		soapdemolish_text:data.string.soap_text,
		viruss:$ref+"/images/page6/soapmolish.png",
		imgwash_circle1:$ref+"/images/page6/hand_washing.png",
		imgwash_circle2:$ref+"/images/page6/soap.gif",
		imgwash_circle2:$ref+"/images/page6/soap.gif",
		mol_1:data.string.soap_mol_1,
		mol_2:data.string.soap_mol_2,
		mol_3:data.string.soap_mol_3,
		imgwash1_a: $ref+"/images/page6/keep_distance.png",
		imgwash3_a: $ref+"/images/page6/exercise.png",
		imgwash2_a: $ref+"/images/page6/animals.png",
		captionwash1_b:data.string.wash1_b,
		captionwash2_b:data.string.wash2_b,
		imgwash1_b: $ref+"/images/page6/healthyfood.png",
		imgwash2_b: $ref+"/images/page6/clean.png",
		title_cure:data.string.title_cure,
		ul_text_1:data.string.cure_text_1,
		ul_text_2:data.string.cure_text_2,
		ul_text_3:data.string.cure_text_3,
		wash : $ref+"/images/page6/wash.png",
		family : $ref+"/images/page6/family.png",
		cover : $ref+"/images/page6/cover.png",
	},

];

$(function () {
		var $board = $('.board');
		var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
		var $nextBtn = $("#activity-page-next-btn-enabled");
		var $tabBtn = $(".tab_nextBtn").html(data.string.next);

		var countNext = 0;
		var $total_page = 6;
        var TabCounter=false;

		loadTimelineProgress($total_page,countNext+1);


        /*=====================================
        =            datahighlight            =
        =====================================*/
        function texthighlight($highlightinside){
            var $alltextpara = $highlightinside.find("*[data-highlight='true']");
            var replaceinstring;
            var texthighlightstarttag = "<span class='parsedstring'>";
            var texthighlightendtag = "</span>";
            if($alltextpara.length > 0){
                $.each($alltextpara, function(index, val) {
                    // console.log(val);
                    replaceinstring = $(this).html();
                    replaceinstring = replaceinstring.replace(/#/g,texthighlightstarttag);
                    replaceinstring = replaceinstring.replace(/@/g,texthighlightendtag);
                    $(this).html(replaceinstring);
                });
            }
        }


        /*=====  End of datahighlight  ======*/


/*
* first
*/
	function first () {
		var source = $("#first-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.show(0);
	}


/*
*
* second
*/
	function second () {
		var source = $("#second-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		$tabBtn.show(0);
        texthighlight($board);

		// $nextBtn.hide(0);
		$("#tab1").addClass("highlightborder");
		var count=0;
		$tabBtn.click (function(){
			count++;
			switch(count){
				case 1:
				$(".p1").delay(100).fadeIn(800);
				$nextBtn.hide(0);
				break;

				case 2:
				$(".p3").delay(100).fadeIn(800);
				$nextBtn.hide(0);
				break;

				case 3:
				$(".p4").delay(100).fadeIn(800);
				$nextBtn.hide(0);
				break;

				case 4:
				$(".p5").delay(100).fadeIn(800);
				$nextBtn.hide(0);
				break;

				case 5:
				$(".p6").delay(100).fadeIn(800);
				$nextBtn.hide(0);
				break;

				case 6:
				$(".definition_text").hide(0);
				$(".div_Points, #img").hide(0);
				$("#capsid").show(100);
				break;

				case 7:
				$("#capsid").hide(10);
				$(".struct_wrapper").show(0);
				$(".ending_text").fadeIn(80);
				$nextBtn.hide(0);
				$tabBtn.hide(0);
				var labelclick = $board.children(".struct_wrapper").children(".para_img").children(".imglable").children("div");
 				var l1click= false;
				var l2click= false;
				var l3click= false;
				var l4click= false;
				var l5click= false;
				var l6click= false;
				var l7click= false;

				labelclick.on("click", function() {
					var classname = $(this).attr('class');

					if (classname=="one") {
						$(".img_definition").addClass("cssfadein");
						$(".img_definition").text(data.string.p6_6_3);
						$(".one").addClass("clickedlabel");
 						l1click= true;

 						if (l1click==true && l2click==true && l3click==true && l4click==true && l5click==true && l6click==true && l7click==true) {
							$nextBtn.show(0);
						};
					}

					if (classname=="two") {
						// alert(classname);
						$(".img_definition").addClass("cssfadein");
						$(".img_definition").text(data.string.p6_6_4);
						$(".two").addClass("clickedlabel");
 						l2click= true;

 						if (l1click==true && l2click==true && l3click==true && l4click==true && l5click==true && l6click==true && l7click==true) {
							$nextBtn.show(0);
						};
					}

					if (classname=="three") {
						// alert(classname);
						$(".img_definition").addClass("cssfadein");
						$(".img_definition").text(data.string.p6_6_5);
						$(".three").addClass("clickedlabel");
 						l3click= true;

 						if (l1click==true && l2click==true && l3click==true && l4click==true && l5click==true && l6click==true && l7click==true) {
							$nextBtn.show(0);
						};
					}

					if (classname=="four") {
						// alert(classname);
						$(".img_definition").addClass("cssfadein");
						$(".img_definition").text(data.string.p6_6_7);
						$(".four").addClass("clickedlabel");
 						l4click= true;

 						if (l1click==true && l2click==true && l3click==true && l4click==true && l5click==true && l6click==true && l7click==true) {
							$nextBtn.show(0);
						};
					}

					if (classname=="five") {
						// alert(classname);
						$(".img_definition").addClass("cssfadein");
						$(".img_definition").text(data.string.p6_6_8);
						$(".five").addClass("clickedlabel");
 						l5click= true;

 						if (l1click==true && l2click==true && l3click==true && l4click==true && l5click==true && l6click==true && l7click==true) {
							$nextBtn.show(0);
						};
					}

					if (classname=="six") {
						// alert(classname);
						$(".img_definition").addClass("cssfadein");
						$(".img_definition").text(data.string.p6_6_9);
						$(".six").addClass("clickedlabel");
 						l6click= true;

 						if (l1click==true && l2click==true && l3click==true && l4click==true && l5click==true && l6click==true && l7click==true) {
							$nextBtn.show(0);
						};
					}

					if (classname=="seven") {
						// alert(classname);
						$(".img_definition").addClass("cssfadein");
						$(".img_definition").text(data.string.p6_6_10);
						$(".seven").addClass("clickedlabel");
 						l7click= true;

 						if (l1click==true && l2click==true && l3click==true && l4click==true && l5click==true && l6click==true && l7click==true) {
							$nextBtn.show(0);
						};
					}
				});
				break;
				default:break;
			}
		});
	}

/*
*
* diy
*/
	function diy () {
		var source = $("#diy").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$prevBtn.hide(0);
		$tabBtn.hide(0);
		$nextBtn.hide(0);
		$("#tab1").addClass("highlightborder");

		var $value = 1;

		$(".board").on("click",".nonactive",function(){

		var $whatIdvalstoo=$("#whatId_"+$value);
		var datvals=$(this).attr('dataval');
		// alert(datvals);
		var valHtml=$(this).html();
		var clickedme=$(this);

		if(datvals==1)
		{

			$whatIdvalstoo.find(".whatactive[dataval='1']").addClass('actives');
			$whatIdvalstoo.find(".whatactive").removeClass('nonactive');

			$whatIdvalstoo.find(".bothvals").delay(500).fadeOut(500,function(){
			$whatIdvalstoo.find(".dashedLine").html(valHtml).addClass('dsLine');

				if($value==1)
					$(".whatImgvalme").delay(200).fadeIn(500);
				else
				{
					$(".whatImgvalme > img:nth-of-type(1)").attr("src",$ref+"/images/diy2/"+$value+".png");

				}
				$value++;
				if($value<=4)
				{
					$("#whatId_"+$value).delay(500).fadeIn(500);
				}
				else
				{
					setTimeout(function(){
						$("#bac_anim_2").show(0);
						$("#bac_anim_2").css("right","400%");
						$(".whatImgvalme img:nth-of-type(1)").hide(0);
						$nextBtn.show(0);
						$prevBtn.show(0);
					},1500);
				}

			});
		}
		else
		{
			$whatIdvalstoo.find(clickedme).addClass('wronged');
		}

	});
}

/*
*
* third
*/
	function third () {
		var source = $("#third-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		$tabBtn.hide(0);
        texthighlight($board);

		$("#tab2").addClass("highlightborder");

        $("#habi1").addClass("cssfadein");
        $("#habi1").on(animationend, function() {
        	$("#habi2").addClass("cssfadein");
        });
        $("#habi2").on(animationend, function() {
        	$("#habi3").addClass("cssfadein");
        });
        $("#habi3").on(animationend, function() {
        	$("#habi4").addClass("cssfadein");
        });
        $("#habi4").on(animationend, function() {
        	$("#habi5").addClass("cssfadein");
        	$tabBtn.delay(1000).show(0);
        });

		$tabBtn.click (function(){
			$(this).hide(0);
			$(".definition_text").hide(0);
			$("#bullet_Points > p").text(data.string.p6_14);
			$(".middle_text").show(0);
			$("#cuties1").addClass('virusshowanim');
			$("#cuties2").addClass('virusshowanim2');
			$("#girl, .girl").addClass("cssfadein");
			$("#food, .food").addClass("cssfadein2");
		});
		$(".food").on(animationend, function() {
			$("#cuties3").addClass('virusshowanim');
			$("#cuties4").addClass('virusshowanim2');
			$("#hand, .hand").addClass("cssfadein");
			$("#bee, .bee").addClass("cssfadein2");
			$nextBtn.delay(5000).show(0);
		});



}

/*
*
* fourth
*/
	function fourth () {
		var source = $("#fourth-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		$prevBtn.hide(0);
        texthighlight($board);

		$("#tab3").addClass("highlightborder");

		$("#cycle1").delay(400).fadeIn(100);
		$(".virus_info1").show(100);
		$tabBtn.delay(1000).fadeIn(100);

		var fourthCounter = 0;
		$tabBtn.on("click", function(){
			fourthCounter++;
			switch(fourthCounter){
				case 1:
				$prevBtn.hide(0);
				$(".definition_text2").hide(0);
				$(".definition_text").show(0);
				$("#cycle1,.virus_info1").delay(100).hide(100);
				$("#cycle2").delay(800).fadeIn(100);
				$(".virus_info2").show(100);
				$tabBtn.delay(1000).fadeIn(100);
				break;

				case 2:
				$(".definition_text").show(0);
				$("#cycle2,.virus_info2").delay(100).hide(100);
				$("#cycle3").delay(800).fadeIn(100);
				$(".virus_info3").show(100);
				$tabBtn.delay(1000).fadeIn(100);
				break;

				case 3:
				$(".definition_text").show(0);
				$("#cycle3,.virus_info3").delay(100).hide(100);
				$("#cycle4").delay(800).fadeIn(100);
				$(".virus_info4").show(100);
				$tabBtn.delay(1000).fadeIn(100);
				break;

				case 4:
				$(".definition_text").show(0);
				$("#cycle4 , .virus_info4").hide(0);
				$("#cycle5").addClass("repro_cycle1");
				$("#cycle5").show(0);
				$(".virus_info5").show(100);
				$nextBtn.delay(1200).fadeIn(100);
				$prevBtn.delay(1200).fadeIn(100);
				break;
				default:break;
			}
		});
	}
/*
*
* fifth
*/
	function fifth () {
		var source = $("#fifth-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		$tabBtn.show(0);
		$("#tab4").addClass("highlightborder");
		for (let index = 1; index < 11; index++) {
			$("#text_soap_"+index).html($("#text_soap_"+index).text());
		}
		$("#bac_box_w8").hide(0);
		var Countfifth = 0;
		$tabBtn.click (function(){
			Countfifth++;
			switch(Countfifth){
				case 1:
				$(".bac_box_wrapper1").show(0);
				$(".def").hide(0);
				$("#bac_box1").show(100);
				$tabBtn.show(100);
				break;

				case 2:
				$("#bac_box2").show(100);
				$tabBtn.show(100);
				break;

				case 3:
				$("#bac_box1, #bac_box2").delay(100).fadeOut(100);
				$("#bac_box3").delay(300).show(100);
				$tabBtn.delay(300).show(100);
				break;

				case 4:
				$("#bac_box4").show(100);
				$tabBtn.show(100);
				break;

				case 5:
				$("#bac_box3, #bac_box4").delay(100).fadeOut(100);
				$("#bac_box5").delay(300).show(100);
				$tabBtn.delay(300).show(100);
				$nextBtn.hide(0);

				break;	

				case 6:
				$("#bac_box3, #bac_box4").delay(100).fadeOut(100);
				$("#bac_box6").delay(300).show(100);
				$tabBtn.delay(300).show(100);
				$nextBtn.hide(0);
				break;


				case 7:
					$(".bac_box_wrapper1").hide(0);
					$('#bac_box_w3').fadeIn(100);
					$('#bac_box_w3 .div_handwash').hide(0);
					$('#bac_box_w3 .div_handwash:nth-child(2)').fadeIn(1000);
					$('#bac_box_w3 .div_handwash:nth-child(3)').delay(1000).fadeIn(500);
					$('#bac_box_w3 .div_handwash:nth-child(4)').delay(2000).fadeIn(500);
					$tabBtn.delay(3000).show(100);
					$prevBtn.hide(0);
					$nextBtn.hide(0);
					break;

			case 8:
				$(".bac_box_wrapper1").hide(0);
				$("#bac_box_w3").fadeOut(100);
				$("#bac_box_w4").fadeIn(300);
				$tabBtn.delay(300).show(100);
				$prevBtn.hide(0);
				$nextBtn.hide(0);
				break;

				
				
				case 9:
					$(".bac_box_wrapper1").hide(0);
					$("#bac_box_w4").fadeOut(100);
					$("#bac_box_w8").fadeIn(300);
					$tabBtn.delay(300).show(100);
					$prevBtn.hide(0);
					$nextBtn.hide(0);
					break;
				case 10:
					$(".bac_box_wrapper1").hide(0);
					$("#bac_box_w8").fadeOut(100);
					$("#bac_box_w10").fadeIn(300);
					$('p#left_mol_1').fadeIn(200);
					$('p#left_mol_2').delay(7000).fadeIn(500);
					$('p#left_mol_3').delay(8000).fadeIn(500);
					$tabBtn.delay(12000).show(100);
					$prevBtn.hide(0);
					$nextBtn.hide(0);
					break;
				case 11:
					$(".bac_box_wrapper1").hide(0);
					$("#bac_box_w10").fadeOut(100);
					$("#bac_box_w5").fadeIn(300);
					$('#bac_box_w5 .div_handwash').hide(0);
					$('#bac_box_w5 .div_handwash:nth-child(2)').fadeIn(1000);
					$('#bac_box_w5 .div_handwash:nth-child(3)').delay(1000).fadeIn(500);
					$('#bac_box_w5 .div_handwash:nth-child(4)').delay(2000).fadeIn(500);
					$tabBtn.delay(3000).show(100);
					$prevBtn.hide(0);
					$nextBtn.hide(0);
					break;

				case 12:
					$(".bac_box_wrapper1").hide(0);
					$("#bac_box_w5").fadeOut(100);
					$("#bac_box_w6").fadeIn(300);
					$('#bac_box_w6 .div_handwash_b').hide(0);
					$('#bac_box_w6 .div_handwash_b:nth-child(2)').fadeIn(1000);
					$('#bac_box_w6 .div_handwash_b:nth-child(3)').delay(1000).fadeIn(500);
					$tabBtn.delay(2000).show(100);
					$prevBtn.hide(0);
					$nextBtn.hide(0);
					break;
				case 13:
					$(".bac_box_wrapper1").hide(0);
					$("#bac_box_w6").fadeOut(100);
					$("#bac_box_w7").fadeIn(300);
					$tabBtn.hide(0);
					$prevBtn.show(0);
					$(".footerNotification").show(0);
					ole.footerNotificationHandler.pageEndSetNotification();
					break;

				default:break;

			}

		});
	}

	first(0);
$nextBtn.on('click',function () {
	loadTimelineProgress($total_page,countNext+2);
		countNext++;
		// alert(countNext);
		switch (countNext) {
			case 1:
				second();
				break;
			case 2:
				diy();
				break;
			case 3:
				third();
				break;
			case 4:
				fourth();
				break;
			case 5:
				fifth();
				break;
			default:break;
		}
		loadTimelineProgress($total_page,countNext+1);
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
            countNext--;
                // alert(countNext);
            switch (countNext) {
                case 4:
                    fourth();
                $(".footerNotification").hide(0);
                    break;
                case 3:
                    third();
                    break;
                case 2:
                    diy();
                    break;
                case 1:
                    second();
                    break;
                default:break;
            }
    });
});

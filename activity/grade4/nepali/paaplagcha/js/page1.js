var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/new/page1/";

var content=[
		// slide1
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: "thebg1",
			singletext:[
			{
				textclass: "title",
				textdata: data.string.p1s0
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "coverpage",
					imgid : 'coverpage',
					imgsrc: ""
				},
				{
					imgclass: "fairytara",
					imgid : 'fairytaragif',
					imgsrc: ""
				},
				{
					imgclass: "starmagic1",
					imgid : 'starmagic',
					imgsrc: ""
				},
			]
		}]
	},
	// slide1

	{
		contentblockadditionalclass: "",
		poemtext:[
		{
			// textclass: "covertext",
			poembox:"content0",
			textdata1: data.string.p1s1,
			textdata2: data.string.p1s2
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "coverpage",
					imgid : 'bg',
					imgsrc: ""
				},
				{
					imgclass: "flowerwaving",
					imgid : 'flowerwaving',
					imgsrc: ""
				},
				{
					imgclass: "niti01",
					imgid : 'niti01',
					imgsrc: ""
				},
				{
					imgclass: "niti02",
					imgid : 'niti02',
					imgsrc: ""
				},
				{
					imgclass: "fairytara1",
					imgid : 'fairytaragif',
					imgsrc: ""
				},
				{
					imgclass: "starmagic2",
					imgid : 'starmagic',
					imgsrc: ""
				},
			]
		}]
	},

	// slide3
	{
		contentblockadditionalclass: "",
		poemtext:[
		{
			// textclass: "covertext",
			poembox:"content0",
			textdata1: data.string.p1s3,
			textdata2: data.string.p1s5
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "coverpage",
					imgid : 'bg1',
					imgsrc: ""
				},
				{
					imgclass: "flowerwaving1",
					imgid : 'flowertugging',
					imgsrc: ""
				},
				{
					imgclass: "floweruntugging",
					imgid : 'floweruntugging',
					imgsrc: ""
				},

				{
					imgclass: "fairytara1",
					imgid : 'fairytaragif',
					imgsrc: ""
				},
				{
					imgclass: "starmagic2",
					imgid : 'starmagic',
					imgsrc: ""
				},
			]
		}]
	},

	// slide4
	{
		contentblockadditionalclass: "",
		poemtext:[
		{
			// textclass: "covertext",
			poembox:"content0",
			textdata1: data.string.p1s6,
			textdata2: data.string.p1s7
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "coverpage",
					imgid : 'bg2',
					imgsrc: ""
				},
				{
					imgclass: "huntingbird",
					imgid : 'huntingbird',
					imgsrc: ""
				},
				{
					imgclass: "unhuntingbird",
					imgid : 'unhuntingbird',
					imgsrc: ""
				},
				{
					imgclass: "fairytara1",
					imgid : 'fairytaragif',
					imgsrc: ""
				},
				{
					imgclass: "starmagic2",
					imgid : 'starmagic',
					imgsrc: ""
				},
			]
		}]
	},

	// slide5
	{
		contentblockadditionalclass: "",
		poemtext:[
		{
			// textclass: "covertext",
			poembox:"content0",
			textdata1: data.string.p1s8,
			textdata2: data.string.p1s9
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "coverpage",
					imgid : 'bg3',
					imgsrc: ""
				},
				{
					imgclass: "hittingdog",
					imgid : 'hittingdog',
					imgsrc: ""
				},
				{
					imgclass: "unhittingdog",
					imgid : 'unhittingdog',
					imgsrc: ""
				},
				{
					imgclass: "fairytara1",
					imgid : 'fairytaragif',
					imgsrc: ""
				},
				{
					imgclass: "starmagic2",
					imgid : 'starmagic',
					imgsrc: ""
				},
			]
		}]
	},

	// slide6
	{
		contentblockadditionalclass: "",
		poemtext:[
		{
			// textclass: "covertext",
			poembox:"content0",
			textdata1: data.string.p1s10,
			textdata2: data.string.p1s11
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "coverpage",
					imgid : 'bg4',
					imgsrc: ""
				},
				{
					imgclass: "fightingboy",
					imgid : 'fightingboy',
					imgsrc: ""
				},
				{
					imgclass: "notfightingboy",
					imgid : 'notfightingboy',
					imgsrc: ""
				},
				{
					imgclass: "fairytara1",
					imgid : 'fairytaragif',
					imgsrc: ""
				},
				{
					imgclass: "starmagic2",
					imgid : 'starmagic',
					imgsrc: ""
				},
			]
		}]
	},

	// slide7
	{
		contentblockadditionalclass: "",
		poemtext:[
		{
			// textclass: "covertext",
			poembox:"content0",
			textdata1: data.string.p1s12,
			textdata2: data.string.p1s13
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "coverpage",
					imgid : 'bg5',
					imgsrc: ""
				},
				{
					imgclass: "playingdhulo",
					imgid : 'playingdhulo',
					imgsrc: ""
				},
				{
					imgclass: "notplayingdhulo",
					imgid : 'notplayingdhulo',
					imgsrc: ""
				},
				{
					imgclass: "fairytara1",
					imgid : 'fairytaragif',
					imgsrc: ""
				},
				{
					imgclass: "starmagic2",
					imgid : 'starmagic',
					imgsrc: ""
				},
			]
		}]
	},

	// slide8
	{
		contentblockadditionalclass: "",
		poemtext:[
		{
			// textclass: "covertext",
			poembox:"content0",
			textdata1: data.string.p1s14,
			textdata2: data.string.p1s15
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "coverpage",
					imgid : 'bg6',
					imgsrc: ""
				},
				{
					imgclass: "playinghilo",
					imgid : 'playinghilo',
					imgsrc: ""
				},
				{
					imgclass: "notplayinghilo",
					imgid : 'notplayinghilo',
					imgsrc: ""
				},
				{
					imgclass: "fairytara1",
					imgid : 'fairytaragif',
					imgsrc: ""
				},
				{
					imgclass: "starmagic2",
					imgid : 'starmagic',
					imgsrc: ""
				},
			]
		}]
	},

	// slide9
	{
		contentblockadditionalclass: "",
		poemtext:[
		{
			// textclass: "covertext",
			poembox:"content0",
			textdata1: data.string.p1s16,
			textdata2: data.string.p1s17
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "coverpage",
					imgid : 'bg7',
					imgsrc: ""
				},
				{
					imgclass: "scolding",
					imgid : 'scolding',
					imgsrc: ""
				},
				{
					imgclass: "notscolding",
					imgid : 'notscolding',
					imgsrc: ""
				},
				{
					imgclass: "fairytara1",
					imgid : 'fairytaragif',
					imgsrc: ""
				},
				{
					imgclass: "starmagic2",
					imgid : 'starmagic',
					imgsrc: ""
				},
			]
		}]
	},

	// slide10
	{
		contentblockadditionalclass: "",
		poemtext:[
		{
			// textclass: "covertext",
			poembox:"content0",
			textdata1: data.string.p1s18,
			textdata2: data.string.p1s19
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "coverpage",
					imgid : 'bg8',
					imgsrc: ""
				},
				{
					imgclass: "bush",
					imgid : 'bush',
					imgsrc: ""
				},
				{
					imgclass: "grandpa",
					imgid : 'grandpa',
					imgsrc: ""
				},
				{
					imgclass: "fairytara1",
					imgid : 'fairytaragif',
					imgsrc: ""
				},
				{
					imgclass: "starmagic2",
					imgid : 'starmagic',
					imgsrc: ""
				},
			]
		}]
	},

	// slide11
	{
		contentblockadditionalclass: "contentwithbg",
		poemtext:[
		{
			// textclass: "covertext",
			poembox:"content0",
			textdata1: data.string.p1s20,
			textdata2: data.string.p1s21
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "nitidoctor1",
					imgid : 'nitidoctor1',
					imgsrc: ""
				},
				{
					imgclass: "nitidoctor2",
					imgid : 'nitidoctor2',
					imgsrc: ""
				},
				{
					imgclass: "cloud1",
					imgid : 'cloud1',
					imgsrc: ""
				},
				{
					imgclass: "sagar1",
					imgid : 'sagar1',
					imgsrc: ""
				},
				{
					imgclass: "sagar2",
					imgid : 'sagar2',
					imgsrc: ""
				},
				{
					imgclass: "cloud2",
					imgid : 'cloud2',
					imgsrc: ""
				},
				{
					imgclass: "fairytara1",
					imgid : 'fairytaragif',
					imgsrc: ""
				},
				{
					imgclass: "starmagic2",
					imgid : 'starmagic',
					imgsrc: ""
				},
			]
		}]
	},

	// slide12
	{
		contentblockadditionalclass: "",
		poemtext:[
		{
			// textclass: "covertext",
			poembox:"content0",
			textdata1: data.string.p1s22,
			textdata2: data.string.p1s23
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "coverpage",
					imgid : 'bg9',
					imgsrc: ""
				},
				{
					imgclass: "man1",
					imgid : 'man1',
					imgsrc: ""
				},
				{
					imgclass: "man2",
					imgid : 'man2',
					imgsrc: ""
				},
				{
					imgclass: "man3",
					imgid : 'man3',
					imgsrc: ""
				},
				{
					imgclass: "man4",
					imgid : 'man4',
					imgsrc: ""
				},
				{
					imgclass: "fairytara1",
					imgid : 'fairytaragif',
					imgsrc: ""
				},
				{
					imgclass: "starmagic2",
					imgid : 'starmagic',
					imgsrc: ""
				},
			]
		}]
	},

	// slide13
	{
		contentblockadditionalclass: "",
		poemtext:[
		{
			// textclass: "covertext",
			poembox:"content0",
			textdata1: data.string.p1s24,
			textdata2: data.string.p1s25
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "coverpage",
					imgid : 'bg10',
					imgsrc: ""
				},
				{
					imgclass: "hiding",
					imgid : 'hiding',
					imgsrc: ""
				},
				{
					imgclass: "hiding1",
					imgid : 'hiding1',
					imgsrc: ""
				},
				{
					imgclass: "fairytara1",
					imgid : 'fairytaragif',
					imgsrc: ""
				},
				{
					imgclass: "starmagic2",
					imgid : 'starmagic',
					imgsrc: ""
				},
			]
		}]
	},

];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var isFirefox = typeof InstallTrigger !== 'undefined';
	var count = 0;
	var textanimatecomplete = false;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "coverpage", src: imgpath+"coverpage.png", type: createjs.AbstractLoader.IMAGE},
			{id: "starmagic", src: imgpath+"stars.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fairytara", src: imgpath+"fairy-tara.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fairytaragif", src: imgpath+"flying-chibi-fairy-animation.gif", type: createjs.AbstractLoader.IMAGE},


			{id: "bg", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg1", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg2", src: imgpath+"bg-bird.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg3", src: imgpath+"bg_dog.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg4", src: imgpath+"bg_boy_fight.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg5", src: imgpath+"bg_dhulo.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg6", src: imgpath+"bg_hilo.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg7", src: imgpath+"bg_aasu.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg8", src: imgpath+"with_grand_pa-01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg9", src: imgpath+"temples.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg10", src: imgpath+"bg_hiding.png", type: createjs.AbstractLoader.IMAGE},

			{id: "niti01", src: imgpath+"niti01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "niti02", src: imgpath+"niti02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "flowerwaving", src: imgpath+"flower_waving.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "flowertugging", src: imgpath+"flower_tugging.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "floweruntugging", src: imgpath+"flower_tugging_boy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "huntingbird", src: imgpath+"hunting_bird.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "unhuntingbird", src: imgpath+"hunting_bird01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hittingdog", src: imgpath+"hitting_dog.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "unhittingdog", src: imgpath+"hitting_dog.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fightingboy", src: imgpath+"fight_with_boy.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "notfightingboy", src: imgpath+"fight_with_boy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "playingdhulo", src: imgpath+"play_with_dhulo.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "notplayingdhulo", src: imgpath+"play_with_dhulo.png", type: createjs.AbstractLoader.IMAGE},
			{id: "playinghilo", src: imgpath+"boy_with_hilo.gif", type: createjs.AbstractLoader.IMAGE},
		//	{id: "playinghilo", src: imgpath+"boy_with_hilo_one_time.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "notplayinghilo", src: imgpath+"boy_with_hilo01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "scolding", src: imgpath+"girl_for_aasu.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "notscolding", src: imgpath+"girl_for_aasu01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bush", src: imgpath+"bush.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "grandpa", src: imgpath+"walking_with_grand_paa.png", type: createjs.AbstractLoader.IMAGE},
			{id: "nitidoctor1", src: imgpath+"niti_doctor.png", type: createjs.AbstractLoader.IMAGE},
			{id: "nitidoctor2", src: imgpath+"niti_doctor01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sagar1", src: imgpath+"sagar_footballer01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sagar2", src: imgpath+"sagar_footballer02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud1", src: imgpath+"clouds01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud2", src: imgpath+"clouds02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "man1", src: imgpath+"man01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "man2", src: imgpath+"man02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "man3", src: imgpath+"man03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "man4", src: imgpath+"man04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hiding", src: imgpath+"hiding.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "hiding1", src: imgpath+"hiding.png", type: createjs.AbstractLoader.IMAGE},

			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
			{id: "s1_p2_1", src: soundAsset+"s1_p2_1.ogg"},
			{id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
			{id: "s1_p3_1", src: soundAsset+"s1_p3_1.ogg"},
			{id: "s1_p4", src: soundAsset+"s1_p4.ogg"},
			{id: "s1_p4_1", src: soundAsset+"s1_p4_1.ogg"},
			{id: "s1_p5", src: soundAsset+"s1_p5.ogg"},
			{id: "s1_p5_1", src: soundAsset+"s1_p5_1.ogg"},
			{id: "s1_p6", src: soundAsset+"s1_p6.ogg"},
			{id: "s1_p6_1", src: soundAsset+"s1_p6_1.ogg"},
			{id: "s1_p7", src: soundAsset+"s1_p7.ogg"},
			{id: "s1_p7_1", src: soundAsset+"s1_p7_1.ogg"},
			{id: "s1_p8", src: soundAsset+"s1_p8.ogg"},
			{id: "s1_p8_1", src: soundAsset+"s1_p8_1.ogg"},
			{id: "s1_p9", src: soundAsset+"s1_p9.ogg"},
			{id: "s1_p9_1", src: soundAsset+"s1_p9_1.ogg"},
			{id: "s1_p10", src: soundAsset+"s1_p10.ogg"},
			{id: "s1_p10_1", src: soundAsset+"s1_p10_1.ogg"},
			{id: "s1_p11", src: soundAsset+"s1_p11.ogg"},
			{id: "s1_p11_1", src: soundAsset+"s1_p11_1.ogg"},
			{id: "s1_p12", src: soundAsset+"s1_p12.ogg"},
			{id: "s1_p12_1", src: soundAsset+"s1_p12_1.ogg"},
			{id: "s1_p13", src: soundAsset+"s1_p13.ogg"},
			{id: "s1_p13_1", src: soundAsset+"s1_p13_1.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}

	function handleFileLoad(event) {
		// console.log(event.item);
	}

	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}

	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		var pbox1, pbox2, ptext1, ptext2;


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
	//	vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		pbox1 = "poembox1";
		pbox2 = "poembox2";
		switch(countNext){
			case 0:
				sound_player_nav("s1_p1");
				break;
			case 1:

				$('.niti02,.starmagic2,.fairytara1,.content0').hide();
				$('.niti02').delay(2500).fadeIn(1000);
				$('.niti01').delay(2500).fadeOut(1000);
				$('.fairytara1,.starmagic2').delay(1000).fadeIn(2000);
				$('.content0').delay(1500).fadeIn(1500,function(){
					for_all_slides(pbox1, ptext1, false);
				});

				ptext1 = "s1_p2";
				ptext2 = "s1_p2_1";
			  break;
			case 2:

				$('.floweruntugging,.starmagic2,.fairytara1,.content0').hide();

				$('.floweruntugging').delay(2500).fadeIn(1000);
				$('.flowerwaving1').delay(2500).fadeOut(1000);
				$('.fairytara1,.starmagic2').delay(1000).fadeIn(2000);
				$('.content0').delay(1500).fadeIn(1500,function(){
					for_all_slides(pbox1, ptext1, false);
				});

				setTimeout(function(){
					$('.content0').show();
					ptext1 = "s1_p3";
					ptext2 = "s1_p3_1";
					third_line_flag = false;
				},1500);
				break;
			case 3:
				$('.unhuntingbird,.starmagic2,.fairytara1,.content0').hide();

				$('.unhuntingbird').delay(2500).fadeIn(1000);
				$('.huntingbird').delay(2500).fadeOut(1000);
				$('.fairytara1,.starmagic2').delay(1000).fadeIn(2000);
				$('.content0').delay(1500).fadeIn(1500,function(){
					for_all_slides(pbox1, ptext1, false);
				});
				setTimeout(function(){
					$('.content0').show();
					ptext1 = "s1_p4";
					ptext2 = "s1_p4_1";
					third_line_flag = false;
				},1500);
			break;
			case 4:
				$('.unhittingdog,.starmagic2,.fairytara1,.content0').hide();

				$('.unhittingdog').delay(2500).fadeIn(1000);
				$('.hittingdog').delay(2500).fadeOut(1000);
				$('.fairytara1,.starmagic2').delay(1000).fadeIn(2000);
				$('.content0').delay(1500).fadeIn(1500,function(){
					for_all_slides(pbox1, ptext1, false);
				});
				setTimeout(function(){
					$('.content0').show();
					ptext1 = "s1_p5";
					ptext2 = "s1_p5_1";
					third_line_flag = false;
				},1500);;

				break;
			break;
			case 5:
				$('.notfightingboy,.starmagic2,.fairytara1,.content0').hide();

				$('.notfightingboy').delay(2500).fadeIn(1000);
				$('.fightingboy').delay(2500).fadeOut(1000);
				$('.fairytara1,.starmagic2').delay(1000).fadeIn(2000);
				$('.content0').delay(1500).fadeIn(1500,function(){
					for_all_slides(pbox1, ptext1, false);
				});
				setTimeout(function(){
					$('.content0').show();
					ptext1 = "s1_p6";
					ptext2 = "s1_p6_1";
					third_line_flag = false;
				},1500);

			break;
			case 6:
				$('.notplayingdhulo,.starmagic2,.fairytara1,.content0').hide();

				$('.notplayingdhulo').delay(2500).fadeIn(1000);
				$('.playingdhulo').delay(2500).fadeOut(1000);
				$('.fairytara1,.starmagic2').delay(1000).fadeIn(2000);
				$('.content0').delay(1500).fadeIn(1500,function(){
					for_all_slides(pbox1, ptext1, false);
				});
				setTimeout(function(){
					$('.content0').show();
					ptext1 = "s1_p7";
					ptext2 = "s1_p7_1";
					third_line_flag = false;
				},1500);
			break;
			case 7:
				$('.notplayinghilo,.starmagic2,.fairytara1,.content0').hide();

				$('.notplayinghilo').delay(2500).fadeIn(1000);
				$('.playinghilo').delay(2500).fadeOut(1000);
				$('.fairytara1,.starmagic2').delay(1000).fadeIn(2000);
				$('.content0').delay(1500).fadeIn(1500,function(){
					for_all_slides(pbox1, ptext1, false);
				});
				setTimeout(function(){
					$('.content0').show();
					ptext1 = "s1_p8";
					ptext2 = "s1_p8_1";
					third_line_flag = false;
				},1500);
			break;
			case 8:
				$('.notscolding,.starmagic2,.fairytara1,.content0').hide();

				$('.notscolding').delay(2500).fadeIn(1000);
				$('.scolding').delay(2500).fadeOut(1000);
				$('.fairytara1,.starmagic2').delay(1000).fadeIn(2000);
				$('.content0').delay(1500).fadeIn(1500,function(){
					for_all_slides(pbox1, ptext1, false);
				});
				setTimeout(function(){
					$('.content0').show();
					ptext1 = "s1_p9";
					ptext2 = "s1_p9_1";
					third_line_flag = false;
				},1500);
			break;

			case 9:
				$('.notscolding,.starmagic2,.fairytara1,.content0').hide();

				$('.notscolding').delay(1000).fadeIn(1000);
				$('.scolding').delay(1000).fadeOut(1000);
				$('.fairytara1,.starmagic2').delay(1000).fadeIn(2000);
				$('.content0').delay(1500).fadeIn(1500,function(){
					for_all_slides(pbox1, ptext1, false);
				});
				setTimeout(function(){
					$('.content0').show();
					ptext1 = "s1_p10";
					ptext2 = "s1_p10_1";
					third_line_flag = false;
				},1500);
			break;

			case 10:
				$('.sagar1,.sagar2,.cloud1,.cloud2,.nitidoctor1,.nitidoctor2,.starmagic2,.fairytara1,.content0').hide();

				$('.sagar1,.cloud1,.sagar2').delay(1000).fadeIn(1000);
				$('.nitidoctor1,.nitidoctor2,.cloud2').delay(1000).fadeIn(1000);
				$('.fairytara1,.starmagic2').delay(1000).fadeIn(2000);
				$('.content0').delay(1500).fadeIn(1800,function(){
					for_all_slides(pbox1, ptext1, false);
				});
				setTimeout(function(){
					$('.content0').show();
					ptext1 = "s1_p11";
					ptext2 = "s1_p11_1";
					third_line_flag = false;
				},1500);
			break;

			case 11:
				$('.man1,.man2,.man3,.man4,.starmagic2,.fairytara1,.content0').hide();

				$('.man1').delay(1000).fadeIn(1000);
				$('.man2').delay(1700).fadeIn(1000);
				$('.man3').delay(2400).fadeIn(1000);
				$('.man4').delay(3100).fadeIn(1000);
				$('.fairytara1,.starmagic2').delay(3300).fadeIn(2000);
				$('.content0').delay(3800).fadeIn(3500,function(){
					for_all_slides(pbox1, ptext1, false);
				});
				setTimeout(function(){
					$('.content0').show();
					ptext1 = "s1_p12";
					ptext2 = "s1_p12_1";
					third_line_flag = false;
				},3500);
			break;

			case 12:
				$('.hiding1,.starmagic2,.fairytara1,.content0').hide();

				$('.hiding1').delay(3000).fadeIn(1000);
				$('.hiding').delay(3000).fadeOut(1000);
				$('.fairytara1,.starmagic2').delay(2000).fadeIn(2000);
				$('.content0').delay(3000).fadeIn(3000,function(){
					for_all_slides(pbox1, ptext1, false);
				});
				setTimeout(function(){
					$('.content0').show();
					ptext1 = "s1_p13";
					ptext2 = "s1_p13_1";
					third_line_flag = false;
				},3500);
			break;

		}


		function firstline_fin(){
			textanimatecomplete = false;
			$("#span_speec_text").append("<br>");
			for_all_slides(pbox2, ptext2, true);
			// switch(countNext){
			//   case 1:
			// 	case 2:
			// 	case 3:
			// 	case 4:
			// 	case 5:
			// 	case 6:
			// 	case 7:
			// 	case 8:
			// 	case 9:
			// 	case 10:
			// 	break;
			//
			// }
		}

		function sound_player_nav(sound_id){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			console.log("sound id is >>>>>>>>>>>"+current_sound);
			current_sound.play();
			current_sound.on("complete", function(){
				nav_button_controls(1000);
			});
		}

		function nav_button_controls(delay_ms){
			timeoutvar = setTimeout(function(){
				if(countNext==0){
					$nextBtn.show(0);
				} else if( countNext>0 && countNext == $total_page-1){
					$prevBtn.show(0);
					ole.footerNotificationHandler.lessonEndSetNotification();
				} else{
					$prevBtn.show(0);
					$nextBtn.show(0);
				}
			},delay_ms);
		}

	function for_all_slides(text_class, my_sound_data, last_page_flag){
		var $textblack = $("."+text_class);
		var current_text = $textblack.html();
		// current_text.replace(/<.*>/, '');
		play_text($textblack, current_text);
			current_sound = createjs.Sound.play(my_sound_data);
			current_sound.play();
			current_sound.on('complete', function(){
				textanimatecomplete = false;
				if(!last_page_flag)
						firstline_fin();
				else{
					textanimatecomplete = true;
					vocabcontroller.findwords(countNext);
					navigationcontroller();
				}
			});
		}

		function play_text($this, text){
			original_text =  text;
			if(isFirefox){
				$this.html("<span id='span_speec_text'></span>"+text);
				$prevBtn.hide(0);
				var $span_speec_text = $("#span_speec_text");
				// $this.css("background-color", "#faf");
				show_text($this, $span_speec_text,text, 110);	// 65 ms is the interval found out by hit and trial
			} else {
				$this.html("<span id='span_speec_text'>"+original_text+"</span>");
			}
		}

		function show_text($this,  $span_speec_text, message, interval) {
			if (0 < message.length && !textanimatecomplete) {
				var nextText = message.substring(0, 1);
				var additionalinterval = 0;
				if (nextText == "<") {
					additionalinterval = 800;
					message = message.substring(4, message.length);
				} else {
					$span_speec_text.append(nextText);
					message = message.substring(1, message.length);
				}
				$this.html($span_speec_text);
				$this.append(message);
				setTimeout(function() {
					show_text($this, $span_speec_text, message, interval);
				}, (interval + additionalinterval));
			}else{
				count = count + 1;
		 		//	textanimatecomplete = true;
				// if(count==2){
				// 	refresh();
				//   console.log('completedtext'+count);
			  // }
			//	refresh();
		 	}
		}
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
	}

	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
    textanimatecomplete = false;
		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		console.log('textanimatecomplete'+ textanimatecomplete);
		if (textanimatecomplete == true){

		 	templateCaller();
		}
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"coverpagetext",
        uppertextblock: [
            {
                datahighlightflag: true,
                datahighlightcustomclass: "subtextclr",
                textclass: "content centertext",
                textdata: data.string.p2text1
            }

        ],
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "arrow arrow1",
                    imgclass: "relativecls  arrow1img",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow arrow2",
                    imgclass: "relativecls  arrow2img",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow arrow3",
                    imgclass: "relativecls  arrow3img",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow arrow4",
                    imgclass: "relativecls  arrow4img",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock: [
            {   textdiv:"male",
                textclass: "content centertext",
                textdata: data.string.male
            },
            {
                textdiv:"female",
                textclass: "content centertext",
                textdata: data.string.female
            },
            {
                textdiv:"box1 ghoda b1",
                textclass: "content centertext",
                textdata: data.string.ghoda
            },
            {
                textdiv:"box2 ghodi b2",
                textclass: "content centertext",
                textdata: data.string.ghodi
            },
            {
                textdiv:"box1 pado b3",
                textclass: "content centertext",
                textdata: data.string.pado
            },
            {
                textdiv:"box2 padi b4",
                textclass: "content centertext",
                textdata: data.string.padi
            },
            {
                textdiv:"box1 bagh b5",
                textclass: "content centertext",
                textdata: data.string.bagh
            },
            {
                textdiv:"box2 baghini b6",
                textclass: "content centertext",
                textdata: data.string.baghini
            },
            {
                textdiv:"box1 boko b7",
                textclass: "content centertext",
                textdata: data.string.boko
            },
            {
                textdiv:"box2 bhakhro b8",
                textclass: "content centertext",
                textdata: data.string.bhakhro
            }
        ],
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "arrow arrow1",
                    imgclass: "relativecls  arrow1img",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow arrow2",
                    imgclass: "relativecls  arrow2img",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow arrow3",
                    imgclass: "relativecls  arrow3img",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow arrow4",
                    imgclass: "relativecls  arrow4img",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock: [
            {   textdiv:"male",
                textclass: "content centertext",
                textdata: data.string.male
            },
            {
                textdiv:"female",
                textclass: "content centertext",
                textdata: data.string.female
            },
            {
                textdiv:"box1 singha b1",
                textclass: "content centertext",
                textdata: data.string.singha
            },
            {
                textdiv:"box2 singhini b2",
                textclass: "content centertext",
                textdata: data.string.singhini
            },
            {
                textdiv:"box1 ragho b3",
                textclass: "content centertext",
                textdata: data.string.ragho
            },
            {
                textdiv:"box2 bhaisi b4",
                textclass: "content centertext",
                textdata: data.string.bhaisi
            },
            {
                textdiv:"box1 patho b5",
                textclass: "content centertext",
                textdata: data.string.patho
            },
            {
                textdiv:"box2 pathi b6",
                textclass: "content centertext",
                textdata: data.string.pathi
            },
            {
                textdiv:"box1 hatti b7",
                textclass: "content centertext",
                textdata: data.string.hatti
            },
            {
                textdiv:"box2 dhoi b8",
                textclass: "content centertext",
                textdata: data.string.dhoi
            }
        ],
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"topic",
        uppertextblock: [
            {
                textclass: "content centertext ",
                textdata: data.string.p2text2,
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "img1",
                    imgclass: "relativecls  img1img",
                    imgid: 'cowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "img2",
                    imgclass: "relativecls  img2img",
                    imgid: 'oxImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "curvearrow1 arrowanim",
                    imgclass: "relativecls  curvearrow1img",
                    imgid: 'curvearrow1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "curvearrow2 arrowanim",
                    imgclass: "relativecls  curvearrow2img",
                    imgid: 'curvearrow2Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock: [
            {   textdiv:"malediv",
                textclass: "subtopic centertext",
                textdata: data.string.male
            },
            {
                textdiv:"femalediv",
                textclass: "subtopic centertext",
                textdata: data.string.female
            },
            {
                textdiv:"imgtext",
                textclass: "content centertext",
                textdata: data.string.cow,
                ans:data.string.ox
            },
            {
                textdiv:"option option1",
                textclass: "content centertext",
                textdata: data.string.ox
            },
            {
                textdiv:"option option2",
                textclass: "content centertext",
                textdata: data.string.pado
            },
        ],
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"topic",
        uppertextblock: [
            {
                textclass: "content centertext ",
                textdata: data.string.p2text2,
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "img1",
                    imgclass: "relativecls  img1img",
                    imgid: 'henImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "img2",
                    imgclass: "relativecls  img2img",
                    imgid: 'roosterImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "curvearrow1 arrowanim",
                    imgclass: "relativecls  curvearrow1img",
                    imgid: 'curvearrow1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "curvearrow2 arrowanim",
                    imgclass: "relativecls  curvearrow2img",
                    imgid: 'curvearrow2Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock: [
            {   textdiv:"malediv",
                textclass: "subtopic centertext",
                textdata: data.string.male
            },
            {
                textdiv:"femalediv",
                textclass: "subtopic centertext",
                textdata: data.string.female
            },
            {
                textdiv:"imgtext",
                textclass: "content centertext",
                textdata: data.string.hen,
                ans:data.string.rooster
            },
            {
                textdiv:"option option1",
                textclass: "content centertext",
                textdata: data.string.rooster
            },
            {
                textdiv:"option option2",
                textclass: "content centertext",
                textdata: data.string.pathi
            },
        ],
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"topic",
        uppertextblock: [
            {
                textclass: "content centertext ",
                textdata: data.string.p2text2,
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "img1",
                    imgclass: "relativecls  img1img",
                    imgid: 'pigImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "img2 img3",
                    imgclass: "relativecls  img2img",
                    imgid: 'hepigImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "curvearrow1 arrowanim",
                    imgclass: "relativecls  curvearrow1img",
                    imgid: 'curvearrow1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "curvearrow2 arrowanim",
                    imgclass: "relativecls  curvearrow2img",
                    imgid: 'curvearrow2Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock: [
            {   textdiv:"malediv",
                textclass: "subtopic centertext",
                textdata: data.string.male
            },
            {
                textdiv:"femalediv",
                textclass: "subtopic centertext",
                textdata: data.string.female
            },
            {
                textdiv:"imgtext",
                textclass: "content centertext",
                textdata: data.string.pig,
                ans:data.string.hepig
            },
            {
                textdiv:"option option1",
                textclass: "content centertext",
                textdata: data.string.hepig
            },
            {
                textdiv:"option option2",
                textclass: "content centertext",
                textdata: data.string.boko
            },
        ],
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "arrowImg", src: imgpath+"arrow02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "cowImg", src: imgpath+"cow.png", type: createjs.AbstractLoader.IMAGE},
            {id: "oxImg", src: imgpath+"ox.png", type: createjs.AbstractLoader.IMAGE},
            {id: "curvearrow1Img", src: imgpath+"curve_arrow01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "curvearrow2Img", src: imgpath+"curve_arrow02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "henImg", src: imgpath+"hen.png", type: createjs.AbstractLoader.IMAGE},
            {id: "roosterImg", src: imgpath+"rooster.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pigImg", src: imgpath+"pig.png", type: createjs.AbstractLoader.IMAGE},
            {id: "hepigImg", src: imgpath+"pig02.png", type: createjs.AbstractLoader.IMAGE},
            // sounds
            {id: "sound_0", src: soundAsset+"p2_s0.ogg"},
            {id: "sound_1_1", src: soundAsset+"p2_s1_1.ogg"},
            {id: "sound_1_2", src: soundAsset+"p2_s1_2.ogg"},
            {id: "sound_1_3", src: soundAsset+"p2_s1_3.ogg"},
            {id: "sound_1_4", src: soundAsset+"p2_s1_4.ogg"},
            {id: "sound_1_5", src: soundAsset+"p2_s1_5.ogg"},
            {id: "sound_2_1", src: soundAsset+"p2_s2_1.ogg"},
            {id: "sound_2_2", src: soundAsset+"p2_s2_2.ogg"},
            {id: "sound_2_3", src: soundAsset+"p2_s2_3.ogg"},
            {id: "sound_2_4", src: soundAsset+"p2_s2.ogg"},
            {id: "sound_3", src: soundAsset+"p2_Strilinga_anusarko_sabda.ogg"},

        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        switch(countNext){
            case 0:
                sound_player("sound_0",true);
                break;
            case 1:
                sound_player("sound_1_1",false);
                showturnwise();
                playsound("sound_1_2","sound_1_3","sound_1_4","sound_1_5");
                break;
            case 2:
                showturnwise();
                playsound("sound_2_1","sound_2_2","sound_2_3","sound_2_4");
                break;
            case 3:
                sound_player("sound_3",false);
                shufflehint();
                checkans();
                break;
            case 4:
                shufflehint();
                checkans();
                break;
            case 5:
                shufflehint();
                checkans();
                break;
            default:
                navigationcontroller(countNext,$total_page);
                break;
        }
    }

    function showturnwise(){
        $(".b1").delay(2500).animate({"opacity":"1"},800);
        $(".arrow1").delay(3000).animate({"opacity":"1"},800);
        $(".b2").delay(3200).animate({"opacity":"1"},800);
        $(".b3").delay(4500).animate({"opacity":"1"},800);
        $(".arrow2").delay(5000).animate({"opacity":"1"},800);
        $(".b4").delay(5200).animate({"opacity":"1"},800);
        $(".b5").delay(6500).animate({"opacity":"1"},800);
        $(".arrow3").delay(7000).animate({"opacity":"1"},800);
        $(".b6").delay(7200).animate({"opacity":"1"},800);
        $(".b7").delay(9000).animate({"opacity":"1"},800);
        $(".arrow4").delay(9500).animate({"opacity":"1"},800);
        $(".b8").delay(9700).animate({"opacity":"1"},800);
    }
   function playsound(s1,s2,s3,s4){
        setTimeout(function(){
            sound_player(s1,false);
            setTimeout(function(){
                sound_player(s2,false);
                setTimeout(function(){
                    sound_player(s3,false);
                    setTimeout(function(){
                        sound_player(s4,true);
                    },2200);
                },2200);
            },2200);
        },2300)
    }

    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller(countNext,$total_page):"";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }
    function shufflehint() {
        var optiondiv = $(".coverboardfull");
        for (var i = 2; i >= 0; i--) {
            optiondiv.append(optiondiv.find(".option").eq(Math.random() * i | 0));
        }
        optiondiv.find(".option").removeClass().addClass("current");
        var a = ["option option1","option option2"]
        optiondiv.find(".current").each(function (index) {
            var $this = $(this)
            $this.addClass(a[index]);
        });
        optiondiv.find(".option").removeClass("current");


    }

    function checkans(){
        $(".option ").on("click",function () {
            createjs.Sound.stop();
            if($(this).text().trim()==$(".imgtext").attr("data-answer").toString().trim()) {
                $(this).addClass("correctans");
                $(".option").addClass("avoid-clicks");
                play_correct_incorrect_sound(1);
                $(this).prepend("<img class='correctWrongImg' src='images/right.png'/>");
                $(".option").not(".correctans").remove();
                animateImg();
            }
            else{
                $(this).addClass("wrongans");
                $(this).addClass("avoid-clicks");
                $(this).prepend("<img class='correctWrongImg' src='images/wrong.png'/>")
                play_correct_incorrect_sound(0);
            }
        });
    }
    function animateImg(){
        $(".correctWrongImg").delay(500).animate({"opacity":"0"})
        $(".img2").addClass("slideT");
        $(".correctans").addClass("slideD");
        $(".femalediv").delay(1500).animate({"opacity":"1"},1000);
        $(".curvearrow1").delay(1900).animate({"opacity":"1"},1000);
        $(".malediv").delay(2100).animate({"opacity":"1"},1000);
        $(".curvearrow2").delay(2700).animate({"opacity":"1"},1000);
        setTimeout(function(){
            navigationcontroller(countNext,$total_page,countNext==7?true:false);
        },2900);
    }
});

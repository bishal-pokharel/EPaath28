var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"coverpagetext",
        uppertextblock: [
            {
                textclass: "chapter centertext",
                textdata: data.lesson.chapter
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "covepage",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'coverpageImg',
                    imgsrc: ""
                },
            ]
        }]
    },
    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        uppertextblockadditionalclass:"topic slideR",
        uppertextblock: [
            {
                textclass: "content centertext ",
                textdata: data.string.p1text1,
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "male draggable dragimg1",
                    imgclass: "relativecls  mimg1img",
                    imgid: 'mimg1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "female draggable dragimg2",
                    imgclass: "relativecls  mimg2img",
                    imgid: 'fimg1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "female draggable dragimg3",
                    imgclass: "relativecls  mimg3img",
                    imgid: 'fimg2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "male draggable dragimg4",
                    imgclass: "relativecls  mimg4img",
                    imgid: 'mimg2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "drop mimg1",
                    imgclass: "relativecls  mimg1img",
                    imgid: 'mimg1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "drop mimg2",
                    imgclass: "relativecls  mimg2img",
                    imgid: 'fimg1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "drop fimg1",
                    imgclass: "relativecls  fimg1img",
                    imgid: 'fimg2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "drop fimg2",
                    imgclass: "relativecls  fimg2img",
                    imgid: 'mimg2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "stars zoomInEffect1",
                    imgclass: "relativecls  starsimg",
                    imgid: 'starsImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"male boydiv droppable",
                textclass:"content centertext",
                textdata:data.string.male
            },
            {
                textdiv:"female girldiv droppable",
                textclass:"content centertext",
                textdata:data.string.female
            },
        ]
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "m1",
                    imgclass: "relativecls  mimg1img",
                    imgid: 'mimg1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "m2",
                    imgclass: "relativecls  mimg2img",
                    imgid: 'fimg1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "m3",
                    imgclass: "relativecls  fimg1img",
                    imgid: 'fimg2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "m4",
                    imgclass: "relativecls  fimg2img",
                    imgid: 'mimg2Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"boydiv1",
                textclass:"content centertext",
                textdata:data.string.male
            },
            {
                textdiv:"girldiv1",
                textclass:"content centertext",
                textdata:data.string.female
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "subtextclr",
                textdiv:"p1",
                textclass: "subtopic centertext fadeInEffect",
                textdata: data.string.p1text2,
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "subtextclr",
                textdiv:"p2",
                textclass: "subtopic centertext fadeInEffect",
                textdata: data.string.p1text3,
            },
        ]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "coverpageImg", src: imgpath+"coverpage.png", type: createjs.AbstractLoader.IMAGE},
            {id: "boyImg", src: imgpath+"boy.png", type: createjs.AbstractLoader.IMAGE},
            {id: "girlImg", src: imgpath+"girl.png", type: createjs.AbstractLoader.IMAGE},
            {id: "mimg1Img", src: imgpath+"boy02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "mimg2Img", src: imgpath+"telephone.png", type: createjs.AbstractLoader.IMAGE},
            {id: "fimg1Img", src: imgpath+"girl01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "fimg2Img", src: imgpath+"grandmum.png", type: createjs.AbstractLoader.IMAGE},
            {id: "starsImg", src: imgpath+"stars.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset+"p1_s0.ogg"},
            {id: "sound_1_1", src: soundAsset+"p1_s1_1.ogg"},
            {id: "sound_2_1", src: soundAsset+"p1_s2_1.ogg"},
            {id: "sound_2_2", src: soundAsset+"p1_s2_2.ogg"},
            {id: "sound_2_3", src: soundAsset+"p1_s2_3.ogg"},
            {id: "sound_3_1", src: soundAsset+"p1_s3_1.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        switch(countNext){
            case 0:
                sound_player("sound_0",true);
                break;
            case 1:
                sound_player("sound_1_1",false);
                dragdrop();
                break;
            case 2:
                sound_player("sound_2_1",false);
                setTimeout(function () {
                    sound_player("sound_2_2",true)
                },13000);
                break;
            default:
                navigationcontroller(countNext,$total_page);
                break;
        }
    }

    function dragdrop(){
        $(".drop,.dragimg2,.dragimg3,.dragimg4,.stars").hide();
        var imgtoShow = ["mimg1","mimg2","fimg1","fimg2"]
        var imgcount = 1;
        $(".draggable").draggable({
            containment: "body",
            revert: true,
            appendTo: "body",
            zindex: 10,
        });
        $('.droppable').droppable({
            accept : ".draggable",
            hoverClass: "hovered",
            drop: function(event, ui) {
                var dragfirstClass = ui.draggable.attr('class').split(' ')[0].toString().trim();
                var dropfirstClass = $(this).attr('class').split(' ')[0].toString().trim();
                if(dragfirstClass == dropfirstClass) {
                    play_correct_incorrect_sound(1);
                    $(".dragimg"+imgcount).hide();
                    var prevcount = imgcount-1;
                    $("."+imgtoShow[prevcount]).show();
                    imgcount++;
                    imgcount<5?$(".dragimg"+imgcount).show():'';
                    if(imgcount==5){
                        navigationcontroller(countNext,$total_page);
                        $(".boydiv,.girldiv").animate({"width": "47%","height":"70%"},1000);
                        $(".boydiv p,.girldiv p").animate({"left":"37%"},1000);
                        $(".topic").find("p").text(data.string.congrats);
                        $(".topic").animate({"height":"15%"},1000);
                        $(".stars").show();
                    }
                }
                else {
                    play_correct_incorrect_sound(0);
                }
            }
        });
    }

    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller(countNext,$total_page):"";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }
});

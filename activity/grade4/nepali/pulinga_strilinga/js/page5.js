var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    //slide 0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgdiy",
        uppertextblockadditionalclass:"titletext",
        uppertextblock: [
            {
                textclass: "subtopic centertext",
                textdata: data.string.p5text1
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv",
                    imgclass: "relativecls img1",
                    imgid: 'boygrbImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "option",
                textdiv:"popupdiv",
                textclass:"content centertext",
                textdata:data.string.p5text2,
                ans:data.string.ans1
            },
        ]
    },
    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgdiy",
        uppertextblockadditionalclass:"titletext",
        uppertextblock: [
            {
                textclass: "subtopic centertext",
                textdata: data.string.p5text1
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls img1",
                    imgid: 'roosterImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "option",
                textdiv:"popupdiv",
                textclass:"content centertext",
                textdata:data.string.p5text3,
                ans:data.string.ans2
            },
        ]
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgdiy",
        uppertextblockadditionalclass:"titletext",
        uppertextblock: [
            {
                textclass: "subtopic centertext",
                textdata: data.string.p5text1
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv2",
                    imgclass: "relativecls img1",
                    imgid: 'anjanaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "imgdiv3",
                    imgclass: "relativecls img2",
                    imgid: 'chibiImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "option",
                textdiv:"popupdiv",
                textclass:"content centertext",
                textdata:data.string.p5text4,
                ans:data.string.ans3
            },
        ]
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgdiy",
        uppertextblockadditionalclass:"titletext",
        uppertextblock: [
            {
                textclass: "subtopic centertext",
                textdata: data.string.p5text1
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls img1",
                    imgid: 'lionImg',
                    imgsrc: ""
                }
            ]
        }],
        textblock:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "option",
                textdiv:"popupdiv",
                textclass:"content centertext",
                textdata:data.string.p5text5,
                ans:data.string.ans4
            },
        ]
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgdiy",
        uppertextblockadditionalclass:"titletext",
        uppertextblock: [
            {
                textclass: "subtopic centertext",
                textdata: data.string.p5text1
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv4",
                    imgclass: "relativecls img1",
                    imgid: 'kakiImg',
                    imgsrc: ""
                }
            ]
        }],
        textblock:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "option",
                textdiv:"popupdiv",
                textclass:"content centertext",
                textdata:data.string.p5text6,
                ans:data.string.ans5
            },
        ]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "boygrbImg", src: imgpath+"boygrb.png", type: createjs.AbstractLoader.IMAGE},
            {id: "roosterImg", src: imgpath+"rooster.png", type: createjs.AbstractLoader.IMAGE},
            {id: "anjanaImg", src: imgpath+"anjana.png", type: createjs.AbstractLoader.IMAGE},
            {id: "chibiImg", src: imgpath+"flying-chibi.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "lionImg", src: imgpath+"lion.png", type: createjs.AbstractLoader.IMAGE},
            {id: "kakiImg", src: imgpath+"cleaning-floor.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset+"p5_s0.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        switch(countNext){
            case 0:
                sound_player("sound_0",false);
                checkans();
                break;
            default:
                checkans();
                break;
        }
    }



    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller(countNext,$total_page):"";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }
    function checkans(){
        $(".option").eq(1).removeClass("option").addClass("slash");
        $(".option ").on("click",function () {
            createjs.Sound.stop();
            if($(this).text().trim()==$(".popupdiv").attr("data-answer").toString().trim()) {
                $(this).addClass("correctans");
                $(".option").addClass("avoid-clicks");
                play_correct_incorrect_sound(1);
                $(this).prepend("<img class='correctWrongImg' src='images/right.png'/>")
                navigationcontroller(countNext,$total_page);
                $(".slash").remove();
                $(".option").not($(this)).remove();
            }
            else{
                $(this).addClass("wrongans");
                $(this).addClass("avoid-clicks");
                $(this).prepend("<img class='correctWrongImg' src='images/wrong.png'/>")
                play_correct_incorrect_sound(0);
            }
        });
    }

});

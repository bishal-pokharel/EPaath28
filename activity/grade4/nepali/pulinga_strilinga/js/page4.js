var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"topic",
        uppertextblock: [
            {
                datahighlightflag: true,
                datahighlightcustomclass: "subtextclr",
                textclass: "content centertext ",
                textdata: data.string.p4text1,
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "subtextclr",
                textclass: "subtopic centertext fadeInEffect",
                textdata: data.string.p4text2,
            }
        ]
    },
    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"maintitle",
        uppertextblock: [
            {
                textclass: "subtopic centertext ",
                textdata: data.string.p4text3,
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "div1 zoomInEffect",
                    imgclass: "relativecls  boycyclimg",
                    imgid: 'boycyclImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "div2 zoomInEffect",
                    imgclass: "relativecls  girlcyclimg",
                    imgid: 'girlcyclImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "subtextclr",
                textdiv:"boydiv1",
                textclass:"subtopic centertext",
                textdata:data.string.p4text4
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "subtextclr",
                textdiv:"girldiv1",
                textclass:"subtopic centertext",
                textdata:data.string.p4text5
            },
        ],
        emptydiv:"emptydiv"
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        uppertextblockadditionalclass:"maintitle",
        uppertextblock: [
            {
                textclass: "subtopic centertext ",
                textdata: data.string.p4text3,
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "div1 boysingdiv slideL",
                    imgclass: "relativecls  boysingimg",
                    imgid: 'boysingImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "div2 boysingdiv slideR",
                    imgclass: "relativecls  girlsingimg",
                    imgid: 'girlsingImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "div3 slideL",
                    imgclass: "relativecls  sing1img",
                    imgid: 'sing1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "div4 slideR",
                    imgclass: "relativecls  sing2img",
                    imgid: 'sing2Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "subtextclr",
                textdiv:"boydiv1",
                textclass:"subtopic centertext",
                textdata:data.string.p4text6
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "subtextclr",
                textdiv:"girldiv1",
                textclass:"subtopic centertext",
                textdata:data.string.p4text7
            },
        ],
        emptydiv:"emptydiv"
    },
    //slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        uppertextblockadditionalclass:"maintitle",
        uppertextblock: [
            {
                textclass: "subtopic centertext ",
                textdata: data.string.p4text3,
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "div1 boyscl slideL",
                    imgclass: "relativecls  boysclimg",
                    imgid: 'boysclImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "div2 girlscl slideR",
                    imgclass: "relativecls  girlsclimg",
                    imgid: 'girlsclImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "subtextclr",
                textdiv:"boydiv1",
                textclass:"subtopic centertext",
                textdata:data.string.p4text8
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "subtextclr",
                textdiv:"girldiv1",
                textclass:"subtopic centertext",
                textdata:data.string.p4text9
            },
        ],
        emptydiv:"emptydiv"
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg3",
        uppertextblockadditionalclass:"maintitle1",
        uppertextblock: [
            {
                textclass: "subtopic text1",
                textdata: data.string.p4text10,
            },
            {
                textclass: "subtopic fadeInEffect",
                textdata: data.string.p4text11,
            }
        ]
    },
    //slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "granddiv1 slideL",
                    imgclass: "relativecls grandfatherimg",
                    imgid: 'grandfatherImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "granddiv2 slideR",
                    imgclass: "relativecls  grandmotherimg",
                    imgid: 'grandmotherImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "subtextclr",
                textdiv:"boydiv1",
                textclass:"subtopic centertext",
                textdata:data.string.p4text12
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "subtextclr",
                textdiv:"girldiv1",
                textclass:"subtopic centertext",
                textdata:data.string.p4text13
            },
        ],
        emptydiv:"emptydiv1"
    },
    //slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg4",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "teacher1 slideL",
                    imgclass: "relativecls gurububaimg",
                    imgid: 'gurububaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "teacher2 slideR",
                    imgclass: "relativecls  gurumaimg",
                    imgid: 'gurumaImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "subtextclr",
                textdiv:"boydiv1",
                textclass:"subtopic centertext",
                textdata:data.string.p4text14
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "subtextclr",
                textdiv:"girldiv1",
                textclass:"subtopic centertext",
                textdata:data.string.p4text15
            },
        ],
        emptydiv:"emptydiv1"
    },
    //slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg5",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "mamadiv slideL",
                    imgclass: "relativecls mamaimg",
                    imgid: 'mamaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "maijudiv slideR",
                    imgclass: "relativecls  maijuimg",
                    imgid: 'maijuImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "subtextclr",
                textdiv:"boydiv1",
                textclass:"subtopic centertext",
                textdata:data.string.p4text16
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "subtextclr",
                textdiv:"girldiv1",
                textclass:"subtopic centertext",
                textdata:data.string.p4text17
            },
        ],
        emptydiv:"emptydiv1"
    },
    //slide 9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg3",
        uppertextblockadditionalclass:"maintitle1 hghtcss",
        uppertextblock: [
            {
                textclass: "subtopic text1",
                textdata: data.string.p4text18,
            },
            {
                textclass: "subtopic fadeInEffect",
                textdata: data.string.p4text11,
            }
        ]
    },
    //slide10
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"maintitle",
        uppertextblock: [
            {
                textclass: "subtopic centertext",
                textdata: data.string.p4text19,
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "oxdiv slideL",
                    imgclass: "relativecls  oximg",
                    imgid: 'oxImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "cowdiv slideR",
                    imgclass: "relativecls  cowimg",
                    imgid: 'cowImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "subtextclr",
                textdiv:"boydiv1",
                textclass:"subtopic centertext",
                textdata:data.string.p4text20
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "subtextclr",
                textdiv:"girldiv1",
                textclass:"subtopic centertext",
                textdata:data.string.p4text21
            },
        ],
        emptydiv:"emptydiv"
    },
    //slide11
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"maintitle",
        uppertextblock: [
            {
                textclass: "subtopic centertext ",
                textdata: data.string.p4text19,
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "oxdiv slideL",
                    imgclass: "relativecls  buffimg",
                    imgid: 'buffImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "cowdiv slideR",
                    imgclass: "relativecls  buff1img",
                    imgid: 'buff1Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "subtextclr",
                textdiv:"boydiv1",
                textclass:"subtopic centertext",
                textdata:data.string.p4text22
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "subtextclr",
                textdiv:"girldiv1",
                textclass:"subtopic centertext",
                textdata:data.string.p4text23
            },
        ],
        emptydiv:"emptydiv"
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "boycyclImg", src: imgpath+"boy-cycling.png", type: createjs.AbstractLoader.IMAGE},
            {id: "girlcyclImg", src: imgpath+"girl-cycling.png", type: createjs.AbstractLoader.IMAGE},
            {id: "boysingImg", src: imgpath+"boysing.png", type: createjs.AbstractLoader.IMAGE},
            {id: "girlsingImg", src: imgpath+"girl-sing.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sing1Img", src: imgpath+"musical-node.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sing2Img", src: imgpath+"musical-node01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "boysclImg", src: imgpath+"boy-school.png", type: createjs.AbstractLoader.IMAGE},
            {id: "girlsclImg", src: imgpath+"girl-school.png", type: createjs.AbstractLoader.IMAGE},
            {id: "grandfatherImg", src: imgpath+"grandfather.png", type: createjs.AbstractLoader.IMAGE},
            {id: "grandmotherImg", src: imgpath+"grandmother.png", type: createjs.AbstractLoader.IMAGE},
            {id: "gurububaImg", src: imgpath+"gurubaa.png", type: createjs.AbstractLoader.IMAGE},
            {id: "gurumaImg", src: imgpath+"guruaama.png", type: createjs.AbstractLoader.IMAGE},
            {id: "mamaImg", src: imgpath+"mama.png", type: createjs.AbstractLoader.IMAGE},
            {id: "maijuImg", src: imgpath+"maiju.png", type: createjs.AbstractLoader.IMAGE},
            {id: "oxImg", src: imgpath+"ox.png", type: createjs.AbstractLoader.IMAGE},
            {id: "cowImg", src: imgpath+"cow.png", type: createjs.AbstractLoader.IMAGE},
            {id: "buffImg", src: imgpath+"buffalo01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "buff1Img", src: imgpath+"buffalo.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset+"p4_s0.ogg"},
            {id: "sound_1_1", src: soundAsset+"p4_s1_1.ogg"},
            {id: "sound_1_2", src: soundAsset+"p4_s1_2.ogg"},
            {id: "sound_2", src: soundAsset+"p4_s2.ogg"},
            {id: "sound_3", src: soundAsset+"p4_s3.ogg"},
            {id: "sound_4", src: soundAsset+"p4_s4.ogg"},
            {id: "sound_5", src: soundAsset+"p4_s5.ogg"},
            {id: "sound_6", src: soundAsset+"p4_s6.ogg"},
            {id: "sound_7", src: soundAsset+"p4_s7.ogg"},
            {id: "sound_8", src: soundAsset+"p4_s8.ogg"},
            {id: "sound_9_1", src: soundAsset+"p4_s9_1.ogg"},
            {id: "sound_9_2", src: soundAsset+"p4_s9_2.ogg"},
            {id: "sound_10", src: soundAsset+"p4_s10.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        switch(countNext){
            case 0:
                sound_player("sound_0",true);
                $(".examples").hide();
                $(".examples").eq(0).delay(9000).fadeIn(100);
                $(".examples").eq(1).delay(13000).fadeIn(100);
                $(".examples").eq(2).delay(17000).fadeIn(100);

                break;
            case 1:
                sound_player("sound_1_1",false);
                setTimeout(function(){
                    sound_player("sound_1_2",true);
                },11000);
                $(".boydiv1 p,.girldiv1 p").hide();
                $(".boydiv1 p").delay(11000).fadeIn(100);
                $(".girldiv1 p").delay(15000).fadeIn(100);
                break;
            case 2:
                sound_player("sound_2",true);
                $(".boydiv1 p,.girldiv1 p").hide();
                $(".boydiv1 p").delay(500).fadeIn(100);
                $(".girldiv1 p").delay(5000).fadeIn(100);
                break;
            case 3:
                sound_player("sound_3",true);
                $(".boydiv1 p,.girldiv1 p").hide();
                $(".boydiv1 p").delay(500).fadeIn(100);
                $(".girldiv1 p").delay(3000).fadeIn(100);
                break;
            case 4:
                sound_player("sound_4",true);
                break;
            case 5:
                sound_player("sound_5",true);
                $(".boydiv1 p,.girldiv1 p").hide();
                $(".boydiv1 p").delay(500).fadeIn(100);
                $(".girldiv1 p").delay(3000).fadeIn(100);
                break;
            case 6:
                sound_player("sound_6",true);
                $(".boydiv1 p,.girldiv1 p").hide();
                $(".boydiv1 p").delay(500).fadeIn(100);
                $(".girldiv1 p").delay(3000).fadeIn(100);
                break;
            case 7:
                sound_player("sound_7",true);
                $(".boydiv1 p,.girldiv1 p").hide();
                $(".boydiv1 p").delay(500).fadeIn(100);
                $(".girldiv1 p").delay(3000).fadeIn(100);
                break;
            case 8:
                sound_player("sound_8",true);
                break;
            case 9:
                sound_player("sound_9_1",false);
                setTimeout(function(){
                    sound_player("sound_9_2",true);
                },13000);
                $(".boydiv1 p,.girldiv1 p").hide();
                $(".boydiv1 p").delay(13000).fadeIn(100);
                $(".girldiv1 p").delay(16000).fadeIn(100);
                break;
            case 10:
                sound_player("sound_10",true);
                $(".boydiv1 p,.girldiv1 p").hide();
                $(".boydiv1 p").delay(500).fadeIn(100);
                $(".girldiv1 p").delay(3000).fadeIn(100);
                break;
            default:
                navigationcontroller(countNext,$total_page);
                break;
        }
    }

    function dragdrop(){
        $(".drop,.dragimg2,.dragimg3,.dragimg4,.stars").hide();
        var imgtoShow = ["mimg1","mimg2","fimg1","fimg2"]
        var imgcount = 1;
        $(".draggable").draggable({
            containment: "body",
            revert: true,
            appendTo: "body",
            zindex: 10,
        });
        $('.droppable').droppable({
            accept : ".draggable",
            hoverClass: "hovered",
            drop: function(event, ui) {
                var dragfirstClass = ui.draggable.attr('class').split(' ')[0].toString().trim();
                var dropfirstClass = $(this).attr('class').split(' ')[0].toString().trim();
                if(dragfirstClass == dropfirstClass) {
                    play_correct_incorrect_sound(1);
                    $(".dragimg"+imgcount).hide();
                    var prevcount = imgcount-1;
                    $("."+imgtoShow[prevcount]).show();
                    imgcount++;
                    imgcount<5?$(".dragimg"+imgcount).show():'';
                    if(imgcount==5){
                        navigationcontroller(countNext,$total_page);
                        $(".boydiv,.girldiv").animate({"width": "47%","height":"70%"},1000);
                        $(".boydiv p,.girldiv p").animate({"left":"37%"},1000);
                        $(".topic").find("p").text(data.string.congrats);
                        $(".topic").animate({"height":"15%"},1000);
                        $(".stars").show();
                    }
                }
                else {
                    play_correct_incorrect_sound(0);
                }
            }
        });
    }

    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller(countNext,$total_page):"";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }
});

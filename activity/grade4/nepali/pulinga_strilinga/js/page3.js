var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"coverpagetext",
        uppertextblock: [
            {
                textclass: "chapter centertext",
                textdata: data.string.diy
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "diydiv1",
                    imgclass: "relativecls img1",
                    imgid: 'diyImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        uppertextblockadditionalclass:"topic",
        uppertextblock: [
            {
                textclass: "content centertext ",
                textdata: data.string.p3text1,
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "stars zoomInEffect1",
                    imgclass: "relativecls  starsimg",
                    imgid: 'starsImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[

            {
                textdiv:"m male droppable",
                textcontent:[
                    {
                        textclass:"subtopic ",
                        textdata:data.string.maletitle
                    },
                    {
                        textclass:"subtopic drop",
                        textdata:data.string.mopt1
                    },
                    {
                        textclass:"subtopic drop",
                        textdata:data.string.mopt2
                    },
                    {
                        textclass:"subtopic drop",
                        textdata:data.string.mopt3
                    },
                    {
                        textclass:"subtopic drop",
                        textdata:data.string.mopt4
                    }
                ]

            },
            {
                textdiv:"fe female droppable ",
                textcontent:[
                    {
                        textclass:"subtopic",
                        textdata:data.string.femaletitle
                    },
                    {
                        textclass:"subtopic drop",
                        textdata:data.string.feopt1
                    },
                    {
                        textclass:"subtopic drop",
                        textdata:data.string.feopt2
                    },
                    {
                        textclass:"subtopic drop",
                        textdata:data.string.feopt3
                    },
                    {
                        textclass:"subtopic drop",
                        textdata:data.string.feopt4
                    }
                ]

            },
            {
                textdiv:"n nondiv droppable ",
                textcontent:[
                    {
                        textclass:"subtopic",
                        textdata:data.string.nontitle
                    },
                    {
                        textclass:"subtopic drop",
                        textdata:data.string.neopt1
                    },
                    {
                        textclass:"subtopic drop",
                        textdata:data.string.neopt2
                    },
                    {
                        textclass:"subtopic drop",
                        textdata:data.string.neopt3
                    },
                    {
                        textclass:"subtopic drop",
                        textdata:data.string.neopt4
                    }
                ]

            },
            {
                textdiv:"draggable dragopt",
                textcontent:[
                    {
                        textclass:"fe subtopic centertext opt0",
                        textdata:data.string.feopt1
                    },
                    {
                        textclass:"m subtopic centertext drag opt1",
                        textdata:data.string.mopt1
                    },
                    {
                        textclass:"m subtopic centertext drag opt2",
                        textdata:data.string.mopt2
                    },
                    {
                        textclass:"fe subtopic centertext drag opt3",
                        textdata:data.string.feopt4
                    },
                    {
                        textclass:"n subtopic centertext drag opt4",
                        textdata:data.string.neopt1
                    },
                    {
                        textclass:"n subtopic centertext drag opt5",
                        textdata:data.string.neopt2
                    },
                    {
                        textclass:"fe subtopic centertext drag opt6",
                        textdata:data.string.feopt3
                    },
                    {
                        textclass:"fe subtopic centertext drag opt7",
                        textdata:data.string.feopt2
                    },
                    {
                        textclass:"n subtopic centertext drag opt8",
                        textdata:data.string.neopt3
                    },
                    {
                        textclass:"m subtopic centertext drag opt9",
                        textdata:data.string.mopt4
                    },
                    {
                        textclass:"n subtopic centertext drag opt10",
                        textdata:data.string.neopt4
                    },
                    {
                        textclass:"m subtopic centertext drag opt11",
                        textdata:data.string.mopt3
                    },
                    {
                        textclass:"subtopic centertext drag opt12",
                        textdata:data.string.congrats1
                    }
                ]

            },
        ]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "starsImg", src: imgpath+"stars.png", type: createjs.AbstractLoader.IMAGE},
            {id: "diyImg", src: imgpath+"bg_diy01.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset+"DIY.ogg"},
            {id: "sound_1", src: soundAsset+"p3_s1.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        switch(countNext){
            case 0:
                sound_player("sound_0",true);
                break;
            case 1:
                sound_player("sound_1",false);
                dragdrop();
                break;
            default:
                navigationcontroller(countNext,$total_page);
                break;
        }
    }

    function dragdrop(){
        $(".drop,.stars,.drag").hide();
        var imgtoShow = ["mimg1","mimg2","fimg1","fimg2"]
        var optcount = 0;
        $(".draggable p").draggable({
            containment: "body",
            revert: true,
            appendTo: "body",
            zindex: 10,
        });
        $('.droppable').droppable({
            accept : ".draggable p",
            hoverClass: "hovered",
            drop: function(event, ui) {
                var dragfirstClass = ui.draggable.attr('class').split(' ')[0].toString().trim();
                var dropfirstClass = $(this).attr('class').split(' ')[0].toString().trim();
                if(dragfirstClass == dropfirstClass) {
                    play_correct_incorrect_sound(1);
                    var textval = ui.draggable.text();
                    $('p:contains("'+textval+'")').show();
                    $(".opt"+optcount).remove();
                    optcount++;
                    $(".opt"+optcount).show();
                    if(optcount>11){
                        $(".draggable").addClass("avoid-clicks");
                        $(".stars").show();
                        navigationcontroller(countNext,$total_page)
                    }
                }
                else {
                    play_correct_incorrect_sound(0);
                }
            }
        });
    }

    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller(countNext,$total_page):"";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }
});

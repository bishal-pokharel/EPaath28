var imgpath = $ref + "/images/images_for_exercise/";
var soundAsset = $ref+"/sounds/";

Array.prototype.shufflearray = function() {
  var i = this.length,
  j, temp;
  while (--i > 0) {
    j = Math.floor(Math.random() * (i + 1));
    temp = this[j];
    this[j] = this[i];
    this[i] = temp;
  }
  return this;
};

var content=[
  //slide 0
  {
    contentnocenteradjust: true,
    uppertextblock: [
      {
        textclass: "top_text",
        textdata: data.string.ex1text1,
      },
      {
        textclass: "left_option correct",
        textdata: data.string.ex1text5,
      },
      {
        textclass: "right_option",
        textdata: data.string.ex1text2,
      }
    ],
    imageblock:[{
      imagestoshow:[{
        imgclass:'left_char',
        imgid:'left_char_1'
      },{
        imgclass:'right_char',
        imgid:'right_char_1'
      },{
        imgclass:'bg_full',
        imgid:'bg_exercise'
      },{
        imgclass:'monkey_expression',
        imgid:'happymonkey'
      }]
    }],
    speechbox:[{
      textclass:'insidetext',
      textdata:data.string.ex1text2,
      speechbox:'speech_1',
      imgid:'textbox',
    },{
      textclass:'insidetext',
      textdata:'',
      speechbox:'speech_2 speech_blank',
      imgid:'textbox',
      imgclass:"flipx"
    },{
      textclass:'insidetext',
      textdata:data.string.ex1text3,
      speechbox:'speech_3',
      imgid:'textbox',
    },{
      textclass:'insidetext',
      textdata:data.string.ex1text4,
      speechbox:'speech_4',
      imgid:'textbox',
      imgclass:"flipx"
    }]
  },

  //slide 1
  {
    contentnocenteradjust: true,
    uppertextblock: [
      {
        textclass: "top_text",
        textdata: data.string.ex1text1,
      },
      {
        textclass: "left_option ",
        textdata: data.string.ex1text10,
      },
      {
        textclass: "right_option correct",
        textdata: data.string.ex1text6,
      }
    ],
    imageblock:[{
      imagestoshow:[{
        imgclass:'left_char',
        imgid:'left_char_2'
      },{
        imgclass:'right_char',
        imgid:'right_char_2'
      },{
        imgclass:'bg_full',
        imgid:'bg_exercise'
      },{
        imgclass:'monkey_expression',
        imgid:'happymonkey'
      }]
    }],
    speechbox:[{
      textclass:'insidetext',
      textdata:'',
      speechbox:'speech_1 speech_blank',
      imgid:'textbox',
    },{
      textclass:'insidetext',
      textdata:data.string.ex1text7,
      speechbox:'speech_2',
      imgid:'textbox',
      imgclass:"flipx"
    },{
      textclass:'insidetext',
      textdata:data.string.ex1text8,
      speechbox:'speech_3',
      imgid:'textbox',
    },{
      textclass:'insidetext',
      textdata:data.string.ex1text9,
      speechbox:'speech_4',
      imgid:'textbox',
      imgclass:"flipx"
    }]
  },

  //slide 2
  {
    contentnocenteradjust: true,
    uppertextblock: [
      {
        textclass: "top_text",
        textdata: data.string.ex1text1,
      },
      {
        textclass: "left_option ",
        textdata: data.string.ex1text15,
      },
      {
        textclass: "right_option correct",
        textdata: data.string.ex1text11,
      }
    ],
    imageblock:[{
      imagestoshow:[{
        imgclass:'left_char',
        imgid:'left_char_3'
      },{
        imgclass:'right_char',
        imgid:'right_char_3'
      },{
        imgclass:'bg_full',
        imgid:'bg_exercise'
      },{
        imgclass:'monkey_expression',
        imgid:'happymonkey'
      }]
    }],
    speechbox:[{
      textclass:'insidetext',
      textdata:'',
      speechbox:'speech_1 speech_blank',
      imgid:'textbox',
    },{
      textclass:'insidetext',
      textdata:data.string.ex1text12,
      speechbox:'speech_2',
      imgid:'textbox',
      imgclass:"flipx"
    },{
      textclass:'insidetext',
      textdata:data.string.ex1text13,
      speechbox:'speech_3',
      imgid:'textbox',
    },{
      textclass:'insidetext',
      textdata:data.string.ex1text14,
      speechbox:'speech_4',
      imgid:'textbox',
      imgclass:"flipx"
    }]
  },

  //slide 3
  {
    contentnocenteradjust: true,
    uppertextblock: [
      {
        textclass: "top_text",
        textdata: data.string.ex1text1,
      },
      {
        textclass: "left_option correct",
        textdata: data.string.ex1text18,
      },
      {
        textclass: "right_option ",
        textdata: data.string.ex1text20,
      }
    ],
    imageblock:[{
      imagestoshow:[{
        imgclass:'left_char flipx',
        imgid:'left_char_4'
      },{
        imgclass:'right_char',
        imgid:'right_char_4'
      },{
        imgclass:'bg_full',
        imgid:'bg_exercise'
      },{
        imgclass:'monkey_expression',
        imgid:'happymonkey'
      }]
    }],
    speechbox:[{
      textclass:'insidetext',
      textdata:data.string.ex1text16,
      speechbox:'speech_1 ',
      imgid:'textbox',
    },{
      textclass:'insidetext',
      textdata:data.string.ex1text17,
      speechbox:'speech_2',
      imgid:'textbox',
      imgclass:"flipx"
    },{
      textclass:'insidetext',
      textdata:'',
      speechbox:'speech_3 speech_blank',
      imgid:'textbox',
    },{
      textclass:'insidetext',
      textdata:data.string.ex1text19,
      speechbox:'speech_4',
      imgid:'textbox',
      imgclass:"flipx"
    }]
  },

  //slide 4
  {
    contentnocenteradjust: true,
    uppertextblock: [
      {
        textclass: "top_text",
        textdata: data.string.ex1text1,
      },
      {
        textclass: "left_option correct",
        textdata: data.string.ex1text21,
      },
      {
        textclass: "right_option ",
        textdata: data.string.ex1text25,
      }
    ],
    imageblock:[{
      imagestoshow:[{
        imgclass:'left_char',
        imgid:'left_char_5'
      },{
        imgclass:'right_char',
        imgid:'right_char_5'
      },{
        imgclass:'bg_full',
        imgid:'bg_exercise'
      },{
        imgclass:'monkey_expression',
        imgid:'happymonkey'
      }]
    }],
    speechbox:[{
      textclass:'insidetext',
      textdata:'',
      speechbox:'speech_1 speech_blank',
      imgid:'textbox',
    },{
      textclass:'insidetext',
      textdata:data.string.ex1text22,
      speechbox:'speech_2',
      imgid:'textbox',
      imgclass:"flipx"
    },{
      textclass:'insidetext',
      textdata:data.string.ex1text23,
      speechbox:'speech_3 ',
      imgid:'textbox',
    },{
      textclass:'insidetext',
      textdata:data.string.ex1text24,
      speechbox:'speech_4',
      imgid:'textbox',
      imgclass:"flipx"
    }]
  },

  //slide 5
  {
    contentnocenteradjust: true,
    uppertextblock: [
      {
        textclass: "top_text",
        textdata: data.string.ex1text1,
      },
      {
        textclass: "left_option ",
        textdata: data.string.ex1text30,
      },
      {
        textclass: "right_option correct",
        textdata: data.string.ex1text27,
      }
    ],
    imageblock:[{
      imagestoshow:[{
        imgclass:'flipx left_char',
        imgid:'right_char_6'
      },{
        imgclass:'right_char',
        imgid:'left_char_6'
      },{
        imgclass:'bg_full',
        imgid:'bg_exercise'
      },{
        imgclass:'monkey_expression',
        imgid:'happymonkey'
      }]
    }],
    speechbox:[{
      textclass:'insidetext',
      textdata:data.string.ex1text26,
      speechbox:'speech_1 ',
      imgid:'textbox',
    },{
      textclass:'insidetext',
      textdata:'',
      speechbox:'speech_2 speech_blank',
      imgid:'textbox',
      imgclass:"flipx"
    },{
      textclass:'insidetext',
      textdata:data.string.ex1text28,
      speechbox:'speech_3 ',
      imgid:'textbox',
    },{
      textclass:'insidetext',
      textdata:data.string.ex1text29,
      speechbox:'speech_4',
      imgid:'textbox',
      imgclass:"flipx"
    }]
  },

  //slide 6
  {
    contentnocenteradjust: true,
    uppertextblock: [
      {
        textclass: "top_text",
        textdata: data.string.ex1text1,
      },
      {
        textclass: "left_option ",
        textdata: data.string.ex1text35,
      },
      {
        textclass: "right_option correct",
        textdata: data.string.ex1text32,
      }
    ],
    imageblock:[{
      imagestoshow:[{
        imgclass:'left_char flipx',
        imgid:'left_char_7'
      },{
        imgclass:'right_char',
        imgid:'right_char_7'
      },{
        imgclass:'bg_full',
        imgid:'bg_exercise'
      },{
        imgclass:'monkey_expression',
        imgid:'happymonkey'
      }]
    }],
    speechbox:[{
      textclass:'insidetext',
      textdata:data.string.ex1text31,
      speechbox:'speech_1 ',
      imgid:'textbox',
    },{
      textclass:'insidetext',
      textdata:'',
      speechbox:'speech_2 speech_blank',
      imgid:'textbox',
      imgclass:"flipx"
    },{
      textclass:'insidetext',
      textdata:data.string.ex1text33,
      speechbox:'speech_3 ',
      imgid:'textbox',
    },{
      textclass:'insidetext',
      textdata:data.string.ex1text34,
      speechbox:'speech_4',
      imgid:'textbox',
      imgclass:"flipx"
    }]
  },

  //slide 7
  {
    contentnocenteradjust: true,
    uppertextblock: [
      {
        textclass: "top_text",
        textdata: data.string.ex1text1,
      },
      {
        textclass: "left_option correct",
        textdata: data.string.ex1text37,
      },
      {
        textclass: "right_option ",
        textdata: data.string.ex1text40,
      }
    ],
    imageblock:[{
      imagestoshow:[{
        imgclass:'left_char',
        imgid:'left_char_8'
      },{
        imgclass:'right_char',
        imgid:'right_char_8'
      },{
        imgclass:'bg_full',
        imgid:'bg_exercise'
      },{
        imgclass:'monkey_expression',
        imgid:'happymonkey'
      }]
    }],
    speechbox:[{
      textclass:'insidetext',
      textdata:data.string.ex1text36,
      speechbox:'speech_1 ',
      imgid:'textbox',
    },{
      textclass:'insidetext',
      textdata:'',
      speechbox:'speech_2 speech_blank',
      imgid:'textbox',
      imgclass:"flipx"
    },{
      textclass:'insidetext',
      textdata:data.string.ex1text38,
      speechbox:'speech_3 ',
      imgid:'textbox',
    },{
      textclass:'insidetext',
      textdata:data.string.ex1text39,
      speechbox:'speech_4',
      imgid:'textbox',
      imgclass:"flipx"
    }]
  },

  //slide 8
  {
    contentnocenteradjust: true,
    uppertextblock: [
      {
        textclass: "top_text",
        textdata: data.string.ex1text1,
      },
      {
        textclass: "left_option correct",
        textdata: data.string.ex1text42,
      },
      {
        textclass: "right_option ",
        textdata: data.string.ex1text45,
      }
    ],
    imageblock:[{
      imagestoshow:[{
        imgclass:'left_char',
        imgid:'left_char_9'
      },{
        imgclass:'right_char',
        imgid:'right_char_9'
      },{
        imgclass:'bg_full',
        imgid:'bg_exercise'
      },{
        imgclass:'monkey_expression',
        imgid:'happymonkey'
      }]
    }],
    speechbox:[{
      textclass:'insidetext',
      textdata:data.string.ex1text41,
      speechbox:'speech_1 ',
      imgid:'textbox',
    },{
      textclass:'insidetext',
      textdata:'',
      speechbox:'speech_2 speech_blank',
      imgid:'textbox',
      imgclass:"flipx"
    },{
      textclass:'insidetext',
      textdata:data.string.ex1text43,
      speechbox:'speech_3 ',
      imgid:'textbox',
    },{
      textclass:'insidetext',
      textdata:data.string.ex1text44,
      speechbox:'speech_4',
      imgid:'textbox',
      imgclass:"flipx"
    }]
  },

  //slide 9
  {
    contentnocenteradjust: true,
    uppertextblock: [
      {
        textclass: "top_text",
        textdata: data.string.ex1text1,
      },
      {
        textclass: "left_option ",
        textdata: data.string.ex1text50,
      },
      {
        textclass: "right_option correct",
        textdata: data.string.ex1text47,
      }
    ],
    imageblock:[{
      imagestoshow:[{
        imgclass:'left_char',
        imgid:'left_char_10'
      },{
        imgclass:'right_char',
        imgid:'right_char_10'
      },{
        imgclass:'bg_full',
        imgid:'bg_exercise'
      },{
        imgclass:'monkey_expression',
        imgid:'happymonkey'
      }]
    }],
    speechbox:[{
      textclass:'insidetext',
      textdata:data.string.ex1text46,
      speechbox:'speech_1 ',
      imgid:'textbox',
    },{
      textclass:'insidetext',
      textdata:'',
      speechbox:'speech_2 speech_blank',
      imgid:'textbox',
      imgclass:"flipx"
    },{
      textclass:'insidetext',
      textdata:data.string.ex1text48,
      speechbox:'speech_3 ',
      imgid:'textbox',
    },{
      textclass:'insidetext',
      textdata:data.string.ex1text49,
      speechbox:'speech_4',
      imgid:'textbox',
      imgclass:"flipx"
    }]
  },




];
// content.shufflearray();
content.shufflearray();

$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;
  var count=0;
  var $total_page = content.length;

  var $playBtn = $('.playBtn');
  var $pauseBtn = $('.pauseBtn');
  var $replayBtn = $('.replayBtn');

  var preload;
  var timeoutvar = null;
  var current_sound;

  var testin = new NumberTemplate();

  testin.init(10);
  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
    {id: "left_char_2", src: imgpath+"q2a.png", type: createjs.AbstractLoader.IMAGE},
    {id: "right_char_2", src: imgpath+"q2b.png", type: createjs.AbstractLoader.IMAGE},
    {id: "left_char_1", src: imgpath+"q1a.png", type: createjs.AbstractLoader.IMAGE},
    {id: "right_char_1", src: imgpath+"q1b.png", type: createjs.AbstractLoader.IMAGE},
    {id: "bg_exercise", src: imgpath+"bg_exercise.png", type: createjs.AbstractLoader.IMAGE},
    {id: "sadmonkey", src: imgpath+"sadmonkey.png", type: createjs.AbstractLoader.IMAGE},
    {id: "happymonkey", src: imgpath+"happymonkey.png", type: createjs.AbstractLoader.IMAGE},
    {id: "left_char_2", src: imgpath+"q2a.png", type: createjs.AbstractLoader.IMAGE},
    {id: "right_char_2", src: imgpath+"q2b.png", type: createjs.AbstractLoader.IMAGE},
    {id: "left_char_3", src: imgpath+"q3a.png", type: createjs.AbstractLoader.IMAGE},
    {id: "right_char_3", src: imgpath+"q3b.png", type: createjs.AbstractLoader.IMAGE},
    {id: "left_char_4", src: imgpath+"q4a.png", type: createjs.AbstractLoader.IMAGE},
    {id: "right_char_4", src: imgpath+"q4b.png", type: createjs.AbstractLoader.IMAGE},
    {id: "left_char_5", src: imgpath+"q5a.png", type: createjs.AbstractLoader.IMAGE},
    {id: "right_char_5", src: imgpath+"q5b.png", type: createjs.AbstractLoader.IMAGE},
    {id: "left_char_6", src: imgpath+"q6a.png", type: createjs.AbstractLoader.IMAGE},
    {id: "right_char_6", src: imgpath+"q6b.png", type: createjs.AbstractLoader.IMAGE},
    {id: "left_char_7", src: imgpath+"q7a.png", type: createjs.AbstractLoader.IMAGE},
    {id: "right_char_7", src: imgpath+"q7b.png", type: createjs.AbstractLoader.IMAGE},
    {id: "left_char_8", src: imgpath+"q8a.png", type: createjs.AbstractLoader.IMAGE},
    {id: "right_char_8", src: imgpath+"q8b.png", type: createjs.AbstractLoader.IMAGE},
    {id: "left_char_9", src: imgpath+"q9a.png", type: createjs.AbstractLoader.IMAGE},
    {id: "right_char_9", src: imgpath+"q9b.png", type: createjs.AbstractLoader.IMAGE},
    {id: "left_char_10", src: imgpath+"q10a.png", type: createjs.AbstractLoader.IMAGE},
    {id: "right_char_10", src: imgpath+"q10b.png", type: createjs.AbstractLoader.IMAGE},

    {id: "textbox", src: "images/textbox/blue/text_box03.png", type: createjs.AbstractLoader.IMAGE},

    // sounds
    {id: "sound_1", src: soundAsset+"ex1_1.ogg"},
  ];
  preload = new createjs.LoadQueue(false);
  preload.installPlugin(createjs.Sound);//for registering sounds
  preload.installPlugin(createjs.Sound);//for registering sounds
  preload.on("progress", handleProgress);
  preload.on("complete", handleComplete);
  preload.on("fileload", handleFileLoad);
  preload.loadManifest(manifest, true);
}

function handleFileLoad(event) {
  // console.log(event.item);
}

function handleProgress(event) {
  $('#loading-text').html(parseInt(event.loaded * 100) + '%');
}

function handleComplete(event) {
  $('#loading-wrapper').hide(0);
  //initialize varibales
  current_sound = createjs.Sound.play('sound_1');
  current_sound.stop();
  // call main function
  var $total_page = content.length;
  templateCaller();
}

//initialize
init();

Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


/*=================================================
=            general template function            =
=================================================*/
function generaltemplate() {
  var source = $("#general-template").html();
  var template = Handlebars.compile(source);
  var html = template(content[countNext]);
  $board.html(html);
  texthighlight($board);
  if(countNext<10){
    put_image(content, countNext,preload);
    put_speechbox_image(content, countNext, preload);
  }
  if(countNext==0){
    createjs.Sound.stop();
    current_sound = createjs.Sound.play('sound_1');
    current_sound.play();
  }
  /*generate question no at the beginning of question*/
  testin.numberOfQuestions();
  $('.monkey_expression').hide(0);
  var wrngClicked = 0;
  var draggables = $(".left_option,.right_option").draggable({
    containment : "body",
    cursor : "grabbing",
    revert : "invalid",
    appendTo : "body",
    zindex : 10000,
  });


  $('.speech_blank').droppable({
    accept : ".left_option,.right_option",
    drop : handleCardDrop1,
    hoverClass : 'hovered'
  });
  function handleCardDrop1(event, ui){
    var dropped = ui.draggable;
    var droppedOn = $(this);

    //dropped at right place
    if(ui.draggable.hasClass("correct")){
      $('.monkey_expression').attr('src',preload.getResult('happymonkey').src).show(0);
      console.log(wrngClicked);
      if(wrngClicked == 0){
        testin.update(true);
      }
      $('.speech_blank > p').html($(ui.draggable).html()).css({'color':'6aa84f'});
      $(ui.draggable).hide(0);
      play_correct_incorrect_sound(true);
      draggables.draggable('disable');
      navigationcontroller(countNext,$total_page);
      $(".drag-item").css("pointer-events","none");
    }

    //dropped at wrong place
    else {
      $('.monkey_expression').attr('src',preload.getResult('sadmonkey').src).show(0);
      wrngClicked++;
      testin.update(false);
      dropped.draggable('option', 'revert', true);
      $(ui.draggable).css({'background-color':'#cc0000','border-color':'#cc0000','color':'white'});
      play_correct_incorrect_sound(false);
    }

  }

}

function navigationcontroller(islastpageflag) {
  if (countNext == 0 && $total_page != 1) {
    $nextBtn.show(0);
  }
  else if ($total_page == 1) {
    $nextBtn.css('display', 'none');

    ole.footerNotificationHandler.lessonEndSetNotification();
  }
  else if (countNext > 0 && countNext < $total_page) {

    $nextBtn.show(0);
  }
  else if (countNext == $total_page - 2) {

    $nextBtn.css('display', 'none');
    // if lastpageflag is true
  }

}

function sound_player(sound_id,navigate) {
  createjs.Sound.stop();
  current_sound = createjs.Sound.play(sound_id);
  current_sound.play();
  current_sound.on('complete', function () {
    navigate?navigationcontroller(countNext,$total_page):"";
  });
}


function templateCaller() {
  $prevBtn.css('display', 'none');
  $nextBtn.css('display', 'none');
  generaltemplate();
}

$nextBtn.on("click", function(){
  countNext++;
  templateCaller();
  testin.gotoNext();
});


$refreshBtn.click(function(){
  templateCaller();
});

$prevBtn.on('click', function () {
  createjs.Sound.stop();
  clearTimeout(timeoutvar);
  countNext--;
  templateCaller();
  /* if footerNotificationHandler pageEndSetNotification was called then on click of
  previous slide button hide the footernotification */
  countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
});

function texthighlight($highlightinside){
  //check if $highlightinside is provided
  typeof $highlightinside !== "object" ?
  alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
  null ;

  var $alltextpara = $highlightinside.find("*[data-highlight='true']");
  var stylerulename;
  var replaceinstring;
  var texthighlightstarttag;
  var texthighlightendtag   = "</span>";


  if($alltextpara.length > 0){
    $.each($alltextpara, function(index, val) {
      /*if there is a data-highlightcustomclass attribute defined for the text element
      use that or else use default 'parsedstring'*/
      $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
      (stylerulename = $(this).attr("data-highlightcustomclass")) :
      (stylerulename = "parsedstring") ;

      texthighlightstarttag = "<span class='"+stylerulename+"'>";


      replaceinstring       = $(this).html();
      replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
      replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


      $(this).html(replaceinstring);
    });
  }
}
function shufflehint() {
  var optiondiv = $(".bg");
  for (var i = 2; i >= 0; i--) {
    optiondiv.append(optiondiv.find(".commonbtn").eq(Math.random() * i | 0));
  }
  optiondiv.find(".commonbtn").removeClass().addClass("current");
  var a = ["commonbtn option1","commonbtn option2"]
  optiondiv.find(".current").each(function (index) {
    var $this = $(this)
    $this.addClass(a[index]);
  });
  optiondiv.find(".commonbtn").removeClass("current");


}

});

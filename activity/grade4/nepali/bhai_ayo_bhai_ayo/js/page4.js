var imgpath = $ref + "/images/images_diy/";
var soundAsset = $ref + "/sounds/";

var content = [
  // slide0
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "green_bg",
    uppertextblock: [
      {
        textclass: "mid_text",
        textdata: data.string.diy
      }
    ]
  },

  // slide1
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "yellow_bg",
    uppertextblock: [
      {
        textclass: "top_instruction",
        textdata: data.string.p4text1
      },
      {
        textclass: "draggable_1 correct",
        textdata: data.string.p4text3
      },
      {
        textclass: "draggable_2",
        textdata: data.string.p4text4
      }
    ],
    lowertextblock: [
      {
        textclass: "inside_text",
        textdata: data.string.p4text2
      },
      {
        textclass: "dropping_area"
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "right_image",
            imgid: "bg01",
            imgsrc: ""
          },
          {
            imgclass: "cross_icon_right cross_icon",
            imgid: "cross_icon",
            imgsrc: ""
          },
          {
            imgclass: "correct_icon",
            imgid: "correct_icon",
            imgsrc: ""
          }
        ]
      }
    ]
  },

  // slide2
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "yellow_bg",
    uppertextblock: [
      {
        textclass: "top_instruction",
        textdata: data.string.p4text1
      },
      {
        textclass: "draggable_1 ",
        textdata: data.string.p4text3
      },
      {
        textclass: "draggable_2 correct",
        textdata: data.string.p4text5
      }
    ],
    lowertextblock: [
      {
        textclass: "inside_text",
        textdata: data.string.p4text6
      },
      {
        textclass: "dropping_area"
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "right_image",
            imgid: "is_sita_sing01",
            imgsrc: ""
          },
          {
            imgclass: "cross_icon_left cross_icon",
            imgid: "cross_icon",
            imgsrc: ""
          },
          {
            imgclass: "correct_icon",
            imgid: "correct_icon",
            imgsrc: ""
          }
        ]
      }
    ]
  },

  // slide3
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "yellow_bg",
    uppertextblock: [
      {
        textclass: "top_instruction",
        textdata: data.string.p4text1
      },
      {
        textclass: "draggable_1 correct",
        textdata: data.string.p4text3
      },
      {
        textclass: "draggable_2 ",
        textdata: data.string.p4text5
      }
    ],
    lowertextblock: [
      {
        textclass: "inside_text",
        textdata: data.string.p4text7
      },
      {
        textclass: "dropping_area"
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "right_image",
            imgid: "sister01",
            imgsrc: ""
          },
          {
            imgclass: "cross_icon_right cross_icon",
            imgid: "cross_icon",
            imgsrc: ""
          },
          {
            imgclass: "correct_icon",
            imgid: "correct_icon",
            imgsrc: ""
          }
        ]
      }
    ]
  },

  // slide4
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "yellow_bg",
    uppertextblock: [
      {
        textclass: "top_instruction",
        textdata: data.string.p4text1
      },
      {
        textclass: "draggable_1 ",
        textdata: data.string.p4text3
      },
      {
        textclass: "draggable_2 correct",
        textdata: data.string.p4text4
      }
    ],
    lowertextblock: [
      {
        textclass: "inside_text",
        textdata: data.string.p4text8
      },
      {
        textclass: "dropping_area"
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "right_image",
            imgid: "moving_shoulders",
            imgsrc: ""
          },
          {
            imgclass: "cross_icon_left cross_icon",
            imgid: "cross_icon",
            imgsrc: ""
          },
          {
            imgclass: "correct_icon",
            imgid: "correct_icon",
            imgsrc: ""
          }
        ]
      }
    ]
  },

  // slide5
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "yellow_bg",
    uppertextblock: [
      {
        textclass: "top_instruction",
        textdata: data.string.p4text1
      },
      {
        textclass: "draggable_1 correct",
        textdata: data.string.p4text3
      },
      {
        textclass: "draggable_2 ",
        textdata: data.string.p4text4
      }
    ],
    lowertextblock: [
      {
        textclass: "inside_text",
        textdata: data.string.p4text9
      },
      {
        textclass: "dropping_area"
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "right_image",
            imgid: "are_u_eat",
            imgsrc: ""
          },
          {
            imgclass: "cross_icon_right cross_icon",
            imgid: "cross_icon",
            imgsrc: ""
          },
          {
            imgclass: "correct_icon",
            imgid: "correct_icon",
            imgsrc: ""
          }
        ]
      }
    ]
  }
];

$(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;
  var count = 0;
  var $total_page = content.length;
  loadTimelineProgress($total_page, countNext + 1);

  var preload;
  var timeoutvar = null;
  var current_sound;
  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();
  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      {
        id: "bg01",
        src: imgpath + "bg02.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "volume_icon",
        src: imgpath + "volume_icon.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "cross_icon",
        src: "images/wrong.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "correct_icon",
        src: "images/correct.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "is_sita_sing01",
        src: imgpath + "is_sita_sing01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "sister01",
        src: imgpath + "sister01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "moving_shoulders",
        src: imgpath + "moving_shoulders.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "are_u_eat",
        src: imgpath + "are_u_eat.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "change1",
        src: imgpath + "bg01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "change2",
        src: imgpath + "is_sita_sing02.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "change3",
        src: imgpath + "sister02.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "change4",
        src: imgpath + "moving_shoulders.gif",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "change5",
        src: imgpath + "are_u_eat01.png",
        type: createjs.AbstractLoader.IMAGE
      },

      {
        id: "textbox",
        src: "images/textbox/white/tl-1.png",
        type: createjs.AbstractLoader.IMAGE
      },

      // sounds
      { id: "sound_2_1", src: soundAsset + "s4_p2.ogg" },
      { id: "sound_2", src: soundAsset + "s4_p2_1.ogg" },
      { id: "sound_3", src: soundAsset + "s4_p3.ogg" },
      { id: "sound_4", src: soundAsset + "s4_p4.ogg" },
      { id: "sound_5", src: soundAsset + "s4_p5.ogg" },
      { id: "sound_6", src: soundAsset + "s4_p6.ogg" }
    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound); //for registering sounds
    preload.installPlugin(createjs.Sound); //for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }

  function handleFileLoad(event) {
    // console.log(event.item);
  }

  function handleProgress(event) {
    $("#loading-text").html(parseInt(event.loaded * 100) + "%");
  }

  function handleComplete(event) {
    $("#loading-wrapper").hide(0);
    //initialize varibales
    current_sound = createjs.Sound.play("sound_1");
    current_sound.stop();
    // call main function
    templateCaller();
  }

  //initialize
  init();

  /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

  /*=================================================
     =            general template function            =
     =================================================*/
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    texthighlight($board);
    vocabcontroller.findwords(countNext);
    put_image(content, countNext, preload);
    put_image1(content, countNext, preload);
    put_speechbox_image(content, countNext, preload);
    // sound_player("sound_" + (countNext + 1));
    $(".audio_icon").attr("src", preload.getResult("volume_icon").src);
    $(".cross_icon,.correct_icon").hide(0);
    var draggables = $(".draggable_1,.draggable_2").draggable({
      containment: "body",
      cursor: "grabbing",
      revert: "invalid",
      appendTo: "body",
      zindex: 10000
    });

    $(".dropping_area").droppable({
      accept: ".draggable_1,.draggable_2",
      hoverClass: "hovered",
      drop: handleCardDrop1
    });

    function handleCardDrop1(event, ui) {
      var dropped = ui.draggable;
      var droppedOn = $(this);
      //dropped correct option
      if (ui.draggable.hasClass("correct")) {
        createjs.Sound.stop();
        $(".correct_icon").show(0);
        $(".dropping_area")
          .html($(ui.draggable).html())
          .css({
            "background-color": "#98c02e",
            color: "white",
            "border-color": "#deef3c"
          });
        $(ui.draggable).hide(0);
        play_correct_incorrect_sound(true);
        draggables.draggable("disable");
        navigationcontroller(
          countNext,
          $total_page,
          countNext == 5 ? true : false
        );
        $(".right_image").attr(
          "src",
          preload.getResult("change" + countNext).src
        );
      }

      //dropped wrong option
      else {
        $(".cross_icon").show(0);
        dropped.draggable("option", "revert", true);
        $(ui.draggable).css({
          "background-color": "#cc0000",
          "border-color": "#cc0000"
        });
        play_correct_incorrect_sound(false);
      }
    }

    switch (countNext) {
      case 0:
        play_diy_audio();
        navigationcontroller(countNext, $total_page);
        break;
      case 1:
        sound_player("sound_2_1");
        current_sound.on("complete", function() {
          sound_player("sound_2");
        });
        audio_click("sound_" + (countNext + 1));
        break;
      default:
        setTimeout(function() {
          sound_player("sound_" + (countNext + 1));
        }, 1000);
        audio_click("sound_" + (countNext + 1));
        break;
    }

    function audio_click(sound_id) {
      $(".audio_icon").click(function() {
        sound_player(sound_id);
      });
    }
  }

  function sound_player(sound_id) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    // current_sound.on('complete', function () {
    //     navigate?navigationcontroller(countNext,$total_page):"";
    // });
  }

  function templateCaller() {
    $prevBtn.css("display", "none");
    $nextBtn.css("display", "none");
    generaltemplate();
    loadTimelineProgress($total_page, countNext + 1);
  }

  $nextBtn.on("click", function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    switch (countNext) {
      default:
        countNext++;
        templateCaller();
        break;
    }
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    countNext--;
    templateCaller();
    /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
    countNext < $total_page
      ? ole.footerNotificationHandler.hideNotification()
      : null;
  });

  function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object"
      ? alert(
          "Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted"
        )
      : null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";

    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
        $(this).attr(
          "data-highlightcustomclass"
        ) /*if there is data-highlightcustomclass defined it is true else it is not*/
          ? (stylerulename = $(this).attr("data-highlightcustomclass"))
          : (stylerulename = "parsedstring");

        texthighlightstarttag = "<span class='" + stylerulename + "'>";

        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

        $(this).html(replaceinstring);
      });
    }
  }
});

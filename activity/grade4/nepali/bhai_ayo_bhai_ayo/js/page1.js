var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        uppertextblockadditionalclass:"coverpagetext",
        uppertextblock: [
            {
                textclass: "chapter centertext",
                textdata: data.lesson.chapter
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "bg_full",
                    imgid: 'bg_cover',
                    imgsrc: ""
                },
            ]
        }]
    },

    //slide1
    {
      contentnocenteradjust: true,
      imageblock: [{
          imagestoshow: [
            {
                imgclass: "bg_full",
                imgid: 'coverpage',
                imgsrc: ""
            },
          ]
      }],
      speechbox:[{
        textclass:'insidetext',
        textdata:data.string.p1text1,
        speechbox:'speech_left',
        imgid:'textbox'
      }]
    },


        //slide2
        {
          contentnocenteradjust: true,
          imageblock: [{
              imagestoshow: [
                  {
                      imgclass: "bg_full",
                      imgid: 'bg_2',
                  }
              ]
          }],
          speechbox:[{
            textclass:'insidetext',
            textdata:data.string.p1text2,
            speechbox:'speech_top_middle',
            imgid:'textbox',
            imgclass:'flipx'
          }]
        },

        //slide3
        {
          contentnocenteradjust: true,
          imageblock: [{
              imagestoshow: [
                  {
                      imgclass: "bg_full",
                      imgid: 'bg_3',
                  }
              ]
          }],
          speechbox:[{
            textclass:'insidetext',
            textdata:data.string.p1text3,
            speechbox:'speech_right',
            imgid:'textbox',
            imgclass:'flipx'
          }]
        },

        //slide4
        {
          contentnocenteradjust: true,
          contentblockadditionalclass:'blue_bg',
          uppertextblock:[{
            textclass:'bottom_text',
            textdata: data.string.p1text4
          }],
          speechbox:[{
            textclass:'insidetext_cloud',
            textdata:data.string.p1text1,
            speechbox:'click-1',
            imgid:'cloud',
            imgclass:'flipx'
          },{
            textclass:'insidetext_cloud',
            textdata:data.string.p1text2,
            speechbox:'click-2',
            imgid:'cloud',
            imgclass:'flipx'
          },{
            textclass:'insidetext_cloud',
            textdata:data.string.p1text3,
            speechbox:'click-3',
            imgid:'cloud',
            imgclass:'flipx'
          }]
        },


];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "coverpage", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bg_2", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bg_3", src: imgpath+"bg03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "cloud", src: imgpath+"cloud.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bg_cover", src: imgpath+"bg_cover.png", type: createjs.AbstractLoader.IMAGE},

            {id: "textbox", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},


            // sounds
            {id: "sound_1", src: soundAsset+"s1_p1.ogg"},
            {id: "sound_2", src: soundAsset+"s1_p2.ogg"},
            {id: "sound_3", src: soundAsset+"s1_p3.ogg"},
            {id: "sound_4", src: soundAsset+"s1_p4.ogg"},
            {id: "sound_5", src: soundAsset+"s1_p5.ogg"},
            {id: "sound_5_1", src: soundAsset+"s1_p5_1.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        put_image1(content, countNext,preload);
        put_speechbox_image(content, countNext, preload);
        switch(countNext){
            case 0:
            case 1:
            case 2:
            case 3:
            sound_player_nav('sound_'+(countNext+1));
            break;
            case 4:
            $('.click-1,.click-2,.click-3,.bottom_text').hide(0);
              $('.click-1').show(500,function(){
                $('.click-2').show(500,function(){
                  $('.click-3').show(500,function(){
                      $('.bottom_text').fadeIn(500);
                      sound_player('sound_5_1');
                  });
                });
              });
            var count_click=0;
            sound_player_onclick(2);
            sound_player_onclick(3);
            sound_player_onclick(4);
            function sound_player_onclick(a){
              $('.click-'+(a-1)).click(function(){
                sound_player('sound_'+a);
                count_click++;
                if(count_click==3){
                  navigationcontroller(countNext,$total_page);
                }
              });
            }
            break;
            default:
                navigationcontroller(countNext,$total_page);
                break;
        }
    }


    function sound_player(sound_id) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        // current_sound.on('complete', function () {
        //     navigate?navigationcontroller(countNext,$total_page):"";
        // });
    }
    function sound_player_nav(sound_id) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigationcontroller(countNext,$total_page);
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }
});

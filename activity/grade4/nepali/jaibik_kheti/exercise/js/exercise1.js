var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	//slide 0
	{
		contentblockadditionalclass: 'bg_cream',
		extratextblock : [
		{
			textdata : data.string.extext4,
			textclass : 'top_instruction',
		},
		{
			textdata : data.string.extext1,
			textclass : 'question',
			spanclass:'question_number'
		},
		{
			textdata : data.string.extext2,
			textclass : 'class-1 options corans',
		},
		{
			textdata : data.string.extext3,
			textclass : 'class-2 options',
		}
		]
	},
	//slide 1
	{
		contentblockadditionalclass: 'bg_cream',
		extratextblock : [
		{
			textdata : data.string.extext4,
			textclass : 'top_instruction',
		},
		{
			textdata : data.string.extext5,
			textclass : 'question',
			spanclass:'question_number'
		},
		{
			textdata : data.string.extext6,
			textclass : 'class-1 options corans',
		},
		{
			textdata : data.string.extext7,
			textclass : 'class-2 options',
		}
		]
	},
	//slide 2
	{
		contentblockadditionalclass: 'bg_cream',
		extratextblock : [
		{
			textdata : data.string.extext4,
			textclass : 'top_instruction',
		},
		{
			textdata : data.string.extext8,
			textclass : 'question',
			spanclass:'question_number'
		},
		{
			textdata : data.string.extext10,
			textclass : 'class-1 options ',
		},
		{
			textdata : data.string.extext9,
			textclass : 'class-2 options corans',
		}
		]
	},

	//slide 3
	{
		contentblockadditionalclass: 'bg_cream',
		extratextblock : [
		{
			textdata : data.string.extext4,
			textclass : 'top_instruction',
		},
		{
			textdata : data.string.extext11,
			textclass : 'question',
			spanclass:'question_number'
		},
		{
			textdata : data.string.extext12,
			textclass : 'class-1 options corans ',
		},
		{
			textdata : data.string.extext13,
			textclass : 'class-2 options ',
		}
		]
	},

	//slide 4
	{
		contentblockadditionalclass: 'bg_cream',
		extratextblock : [
		{
			textdata : data.string.extext4,
			textclass : 'top_instruction',
		},
		{
			textdata : data.string.extext14,
			textclass : 'question',
			spanclass:'question_number'
		},
		{
			textdata : data.string.extext15,
			textclass : 'class-1 options  ',
		},
		{
			textdata : data.string.extext16,
			textclass : 'class-2 options corans',
		}
		]
	},

	//slide 5
	{
		contentblockadditionalclass: 'bg_cream',
		extratextblock : [
		{
			textdata : data.string.extext4,
			textclass : 'top_instruction',
		},
		{
			textdata : data.string.extext17,
			textclass : 'question',
			spanclass:'question_number'
		},
		{
			textdata : data.string.extext19,
			textclass : 'class-1 options  ',
		},
		{
			textdata : data.string.extext18,
			textclass : 'class-2 options corans',
		}
		]
	},

	//slide 6
	{
		contentblockadditionalclass: 'bg_cream',
		extratextblock : [
		{
			textdata : data.string.extext4,
			textclass : 'top_instruction',
		},
		{
			textdata : data.string.extext20,
			textclass : 'question',
			spanclass:'question_number'
		},
		{
			textdata : data.string.extext21,
			textclass : 'class-1 options corans',
		},
		{
			textdata : data.string.extext22,
			textclass : 'class-2 options ',
		}
		]
	},

	//slide 7
	{
		contentblockadditionalclass: 'bg_cream',
		extratextblock : [
		{
			textdata : data.string.extext4,
			textclass : 'top_instruction',
		},
		{
			textdata : data.string.extext30,
			textclass : 'question',
			spanclass:'question_number'
		},
		{
			textdata : data.string.extext32,
			textclass : 'class-1 options ',
		},
		{
			textdata : data.string.extext31,
			textclass : 'class-2 options corans',
		}
		]
	},
	//slide 8
	{
		contentblockadditionalclass: 'bg_cream',
		extratextblock : [
		{
			textdata : data.string.extext4,
			textclass : 'top_instruction',
		},
		{
			textdata : data.string.extext27,
			textclass : 'question',
			spanclass:'question_number'
		},
		{
			textdata : data.string.extext29,
			textclass : 'class-1 options ',
		},
		{
			textdata : data.string.extext28,
			textclass : 'class-2 options corans',
		}
		]
	},

	//slide 9
	{
		contentblockadditionalclass: 'bg_cream',
		extratextblock : [
		{
			textdata : data.string.extext4,
			textclass : 'top_instruction',
		},
		{
			textdata : data.string.extext24,
			textclass : 'class-1 options top_70 ',
		},
		{
			textdata : data.string.extext23,
			textclass : 'class-2 options corans top_70',
		}
	],
	hintimageblock:[
		{
			imgclass:'mid_mg',
			imgsrc: imgpath + "two_friends.png"
		}
	]
	},
];

// content.shufflearray();


$(function ()
{
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	var score = 0;

	  var preload;
	  var timeoutvar = null;
	  var current_sound;
	  var vocabcontroller =  new Vocabulary();
	  vocabcontroller.init();
	  function init() {
	    //specify type otherwise it will load assests as XHR
	    manifest = [
			//	img
	        // sounds
	      {id: "exer", src: soundAsset+"ex.ogg"},

	    ];
	    preload = new createjs.LoadQueue(false);
	    preload.installPlugin(createjs.Sound);//for registering sounds
	    preload.installPlugin(createjs.Sound);//for registering sounds
	    preload.on("progress", handleProgress);
	    preload.on("complete", handleComplete);
	    preload.on("fileload", handleFileLoad);
	    preload.loadManifest(manifest, true);
	  }

	  function handleFileLoad(event) {
	    // console.log(event.item);
	  }

	  function handleProgress(event) {
	    $('#loading-text').html(parseInt(event.loaded * 100) + '%');
	  }

	  function handleComplete(event) {
	    $('#loading-wrapper').hide(0);
	    //initialize varibales
	    current_sound = createjs.Sound.play('sound_1');
	    current_sound.stop();
	    // call main function
	    templateCaller();
	  }

	  //initialize
	  init();
	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

	var rhino = new NumberTemplate();

	rhino.init($total_page);



	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);

		//randomize options
		var positions = [1,2];
		positions.shufflearray();
		for(var i=1; i<=2; i++){
			$('.class-'+i).addClass('pos-'+positions[i-1]);
		}
		var nepali_number =english_to_nepali_converter(countNext+1);
		$('.question_number').html(nepali_number+'. ');
		// function converting nepali numbers to english
		function english_to_nepali_converter(english){
		  var nepali_number = ['१','२','३','४' ,'५' ,'६' ,'७' ,'८' ,'९' ,'१०', '११', '१२','१३', '१४' ,'१५' ,'१६' ,'१७' ,'१८' ,'१९','२०','२१','२२','२३','२४','२५' ,'२६' ,'२७' ,'२८' ,'२९','३०'];
		  return nepali_number[english-1];
		}
		//for sound
		if(countNext==0){
			sound_player("exer");
		}
		var wrong_clicked = false;
		$('.options').click(function(){
      if($(this).hasClass('corans')){
				if(!wrong_clicked){
					rhino.update(true);
				}
				$('.replace').html($(this).text()).css('color','#92af3b');
        var $this = $(this);
        var position = $this.position();
        var width = $this.width();
        var height = $this.height();
        var centerX = ((position.left + width / 2)*100)/$('.contentblock ').width()+'%';
        var centerY = (((position.top + height)*100)/$('.contentblock ').height())+'%';
        $(this).css({'border-color':'#D4EF34','background':'#92AF3B','color':'white'});
        $('.options').css('pointer-events','none');
        $('<img style="left:'+centerX+';top:'+centerY+';position:absolute;height:5%;transform:translate(626%,-73%);z-index:2;" src="'+imgpath +'correct.png" />').insertAfter(this);
        play_correct_incorrect_sound(1);
				if(countNext != $total_page)
				{
					$nextBtn.show(0);
				}
      }
      else{
				if(!wrong_clicked){
					rhino.update(false);
				}
				wrong_clicked = true;
        var $this = $(this);
        var position = $this.position();
        var width = $this.width();
        var height = $this.height();
        var centerX = ((position.left + width / 2)*100)/$('.contentblock ').width()+'%';
        var centerY = (((position.top + height)*100)/$('.contentblock ').height())+'%';
        $(this).css({'border-color':'#980000','background':'#FF0000'});
        $(this).css('pointer-events','none');
        $('<img style="left:'+centerX+';top:'+centerY+';position:absolute;height:5%;transform:translate(626%,-73%);z-index:2;" src="'+imgpath +'incorrect.png" />').insertAfter(this);
        play_correct_incorrect_sound(0);
      }
    });


	}
	function sound_player(sound_id,navigate) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
  }
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		rhino.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

  var content = [
    // slide0
    {
      contentnocenteradjust: true,
      uppertextblock: [
        {
          textclass: "centertext",
          textdata: data.lesson.chapter
        }
      ],
      imageblock: [{
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: 'coverpage',
            imgsrc: ""
          },
        ]
      }]
    },

    // slide1
    {
      contentnocenteradjust: true,
      imageblock: [{
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: 'bg_home01',
            imgsrc: ""
          },
        ]
      }],
      uppertextblock:[{
        textdata:data.string.p1text1,
        textclass:'left_top_text fadein'
      }]
    },
    // slide2
    {
      contentnocenteradjust: true,
      imageblock: [{
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: 'bg_home01',
            imgsrc: ""
          },
        ]
      }],
      uppertextblock:[{
        textdata:data.string.p1text2,
        textclass:'left_top_text fadein'
      }]
    },
    // slide3
    {
      contentnocenteradjust: true,
      imageblock: [{
        imagestoshow: [
          {
            imgclass: "bg_full decreasebrightness",
            imgid: 'bg_home01',
            imgsrc: ""
          },
        ]
      }],
      speechbox:[{
        textclass:'insidetext',
        textdata:data.string.p1text3,
        speechbox:'wobblebox animated wobble',
        imgid:'text_box_big',
        imgclass:'speechbg1'
      }]
    },
    // slide4
    {
      contentnocenteradjust: true,
      imageblock: [{
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: 'bg_home01',
            imgsrc: ""
          },
          {
            imgclass: "grandmom",
            imgid: 'grand_mum02',
            imgsrc: ""
          },
        ]
      }],
      speechbox:[{
        textclass:'insidetext_speech',
        textdata:data.string.p1text4,
        speechbox:'speech1',
        imgid:'textbox',
        imgclass:'speechbg'
      }]
    },

    // slide5
    {
      contentnocenteradjust: true,
      imageblock: [{
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: 'bg_home01',
            imgsrc: ""
          },
          {
            imgclass: "grandmom",
            imgid: 'grand_mum02',
            imgsrc: ""
          },
        ]
      }],
      speechbox:[{
        textclass:'insidetext_speech1',
        textdata:data.string.p1text5,
        speechbox:'speech2',
        imgid:'textbox1',
        imgclass:'speechbg'
      }]
    },

    // slide6
    {
      contentnocenteradjust: true,
      imageblock: [{
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: 'bg_home01',
            imgsrc: ""
          },
          {
            imgclass: "grandmom",
            imgid: 'grand_mum02',
            imgsrc: ""
          },
          {
            imgclass: "granddad",
            imgid: 'grand_dad02',
            imgsrc: ""
          },
        ]
      }],
      uppertextblock:[{
        textdata:data.string.p1text6,
        textclass:'left_top_text'
      }]
    },

    // slide7
    {
      contentnocenteradjust: true,
      imageblock: [{
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: 'bg_home01',
            imgsrc: ""
          },
          {
            imgclass: "grandmom",
            imgid: 'grand_mum02',
            imgsrc: ""
          },
          {
            imgclass: "granddad",
            imgid: 'grand_dad02',
            imgsrc: ""
          },
        ]
      }],
      speechbox:[{
        textclass:'insidetext_speech1',
        textdata:data.string.p1text7,
        speechbox:'speech3 fadein',
        imgid:'textbox2',
        imgclass:'speechbg'
      }]
    },
    // // slide8
    // {
    //   contentnocenteradjust: true,
    //   imageblock: [{
    //     imagestoshow: [
    //       {
    //         imgclass: "bg_full",
    //         imgid: 'bg_going_hospital',
    //         imgsrc: ""
    //       },
    //       {
    //         imgclass: "left_bot_image",
    //         imgid: 'going_hospital',
    //         imgsrc: ""
    //       }
    //     ]
    //   }],
    //   uppertextblock:[{
    //     textdata:data.string.p1text8,
    //     textclass:'top_text'
    //   }]
    // },
  ];

  $(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
      //specify type otherwise it will load assests as XHR
      manifest = [
        {id: "bg_home01", src: imgpath+"bg_home01.png", type: createjs.AbstractLoader.IMAGE},
        {id: "text_box_big", src: imgpath+"text_box_big.png", type: createjs.AbstractLoader.IMAGE},
        {id: "coverpage", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},
        {id: "grand_mum02", src: imgpath+"grand_mum02.png", type: createjs.AbstractLoader.IMAGE},
        {id: "grand_dad02", src: imgpath+"grand_dad02.png", type: createjs.AbstractLoader.IMAGE},
        {id: "bg_going_hospital", src: imgpath+"bg_going_hospital.png", type: createjs.AbstractLoader.IMAGE},
        {id: "going_hospital", src: imgpath+"going_hospital.png", type: createjs.AbstractLoader.IMAGE},
        {id: "textbox", src: "images/textbox/white/lb-1.png", type: createjs.AbstractLoader.IMAGE},
        {id: "textbox1", src: "images/textbox/white/tl-1.png", type: createjs.AbstractLoader.IMAGE},
        {id: "textbox2", src: "images/textbox/white/tr-2.png", type: createjs.AbstractLoader.IMAGE},


        // sounds
        {id: "sound_0", src: soundAsset+"s1_p1.ogg"},
        {id: "sound_1", src: soundAsset+"s1_p2.ogg"},
        {id: "sound_2", src: soundAsset+"s1_p3.ogg"},
        {id: "sound_3", src: soundAsset+"s1_p4.ogg"},
        {id: "sound_4", src: soundAsset+"s1_p5.ogg"},
        {id: "sound_5", src: soundAsset+"s1_p6.ogg"},
        {id: "sound_6", src: soundAsset+"s1_p7.ogg"},
        {id: "sound_7", src: soundAsset+"s1_p8.ogg"},

      ];
      preload = new createjs.LoadQueue(false);
      preload.installPlugin(createjs.Sound);//for registering sounds
      preload.installPlugin(createjs.Sound);//for registering sounds
      preload.on("progress", handleProgress);
      preload.on("complete", handleComplete);
      preload.on("fileload", handleFileLoad);
      preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
      // console.log(event.item);
    }

    function handleProgress(event) {
      $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
      $('#loading-wrapper').hide(0);
      //initialize varibales
      // call main function
      templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
    =            general template function            =
    =================================================*/
    function generaltemplate() {
      var source = $("#general-template").html();
      var template = Handlebars.compile(source);
      var html = template(content[countNext]);
      $board.html(html);
      texthighlight($board);
      vocabcontroller.findwords(countNext);
      put_image(content, countNext,preload);
      put_image1(content, countNext,preload);
      put_speechbox_image(content, countNext, preload);
      switch(countNext){
        case 4:
        $('.speechbox.speech1,.grandmom').hide(0);
        $('.grandmom').fadeIn(500,function(){
          $('.speechbox.speech1').fadeIn(500,function(){
            sound_player("sound_"+countNext);
          });
        });
        break;
        case 6:
        $('.granddad,.left_top_text').hide(0);
        $('.granddad').fadeIn(500,function(){
          $('.left_top_text').fadeIn(500,function(){
            sound_player("sound_"+countNext);
          });
        });
        break;
        case 8:
        $('.bg_full').css('cursor','pointer');
        sound_player("sound_"+countNext);
        break;
        default:
        $('.speechbox').hide(0).fadeIn(500);
        sound_player("sound_"+countNext);
        break;
      }
    }


    function sound_player(sound_id,navigate) {
      createjs.Sound.stop();
      current_sound = createjs.Sound.play(sound_id);
      current_sound.play();
      current_sound.on('complete', function () {
        navigationcontroller(countNext,$total_page);
      });
    }


    function templateCaller() {
      $prevBtn.css('display', 'none');
      $nextBtn.css('display', 'none');
      generaltemplate();
      loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
      createjs.Sound.stop();
      clearTimeout(timeoutvar);
      switch (countNext) {
        default:
        countNext++;
        templateCaller();
        break;
      }
    });

    $refreshBtn.click(function(){
      templateCaller();
    });

    $prevBtn.on('click', function () {
      createjs.Sound.stop();
      clearTimeout(timeoutvar);
      countNext--;
      templateCaller();
      /* if footerNotificationHandler pageEndSetNotification was called then on click of
      previous slide button hide the footernotification */
      countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });


    function texthighlight($highlightinside){
      //check if $highlightinside is provided
      typeof $highlightinside !== "object" ?
      alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
      null ;

      var $alltextpara = $highlightinside.find("*[data-highlight='true']");
      var stylerulename;
      var replaceinstring;
      var texthighlightstarttag;
      var texthighlightendtag   = "</span>";


      if($alltextpara.length > 0){
        $.each($alltextpara, function(index, val) {
          /*if there is a data-highlightcustomclass attribute defined for the text element
          use that or else use default 'parsedstring'*/
          $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
          (stylerulename = $(this).attr("data-highlightcustomclass")) :
          (stylerulename = "parsedstring") ;

          texthighlightstarttag = "<span class='"+stylerulename+"'>";


          replaceinstring       = $(this).html();
          replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
          replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


          $(this).html(replaceinstring);
        });
      }
    }
  });

var imgpath = $ref + "/images/";
var imgpath_hospital = $ref + "/images/hospital/";
var soundAsset = $ref+"/sounds/";

var content = [
  // slide0
  {
    contentnocenteradjust: true,
    imageblock:[{
      imagestoshow:[{
        imgclass:'bg_full',
        imgid:'room'
      },
      {
        imgclass:'lakhan',
        imgid:'lakhan03'
      },
      {
        imgclass:'dad',
        imgid:'grand_dad02'
      },
      {
        imgclass:'doctor',
        imgid:'doctor-gif'
      }]
    }],
    speechbox:[{
      textclass:'insidetext',
      textdata:data.string.p2text1,
      speechbox:'speech1',
      imgid:'textbox1',
      imgclass:'speechbg'
    }]

  },

  // slide1
  {
    contentnocenteradjust: true,
    imageblock:[{
      imagestoshow:[{
        imgclass:'bg_full',
        imgid:'room'
      },
      {
        imgclass:'lakhan',
        imgid:'lakhan03-gif'
      },
      {
        imgclass:'dad',
        imgid:'grand_dad02'
      },
      {
        imgclass:'doctor',
        imgid:'doctor-png'
      }]
    }],
    speechbox:[{
      textclass:'insidetext',
      textdata:data.string.p2text2,
      speechbox:'speech2',
      imgid:'textbox1',
      imgclass:'speechbg flipx'
    }]

  },
  // slide2
  {
    contentnocenteradjust: true,
    imageblock:[{
      imagestoshow:[{
        imgclass:'bg_full',
        imgid:'room'
      },
      {
        imgclass:'lakhan',
        imgid:'lakhan03'
      },
      {
        imgclass:'dad',
        imgid:'grand_dad02'
      },
      {
        imgclass:'doctor',
        imgid:'doctor-gif'
      }]
    }],
    speechbox:[{
      textclass:'insidetext',
      textdata:data.string.p2text3,
      speechbox:'speech1',
      imgid:'textbox1',
      imgclass:'speechbg'
    }]

  },
  // slide3
  {
    contentnocenteradjust: true,
    imageblock:[{
      imagestoshow:[{
        imgclass:'bg_full',
        imgid:'checking_rajan'
      },
      {
        imgclass:'dad',
        imgid:'grand_dad02'
      }]
    }]

  },

  // slide4
  {
    contentnocenteradjust: true,
    imageblock:[{
      imagestoshow:[{
        imgclass:'bg_full',
        imgid:'checking_rajan'
      },
      {
        imgclass:'dad',
        imgid:'grand_paa_gif'
      }]
    }],
    speechbox:[{
      textclass:'insidetext',
      textdata:data.string.p2text4,
      speechbox:'speech3',
      imgid:'textbox1',
      imgclass:'speechbg flipx'
    }]

  },

  // slide5
  {
    contentnocenteradjust: true,
    imageblock:[{
      imagestoshow:[{
        imgclass:'bg_full',
        imgid:'room01'
      },
      {
        imgclass:'lakhan',
        imgid:'lakhan04'
      },
      {
        imgclass:'dad',
        imgid:'grand_dad02'
      },
      {
        imgclass:'doctor2',
        imgid:'doctor-stand-gif'
      }]
    }],
    speechbox:[{
      textclass:'insidetext2',
      textdata:data.string.p2text5,
      speechbox:'speech4',
      imgid:'textbox',
      imgclass:'speechbg'
    }]

  },

  //slide6
  {
    contentnocenteradjust: true,
    imageblock:[{
      imagestoshow:[{
        imgclass:'bg_full',
        imgid:'room01'
      },
      {
        imgclass:'lakhan',
        imgid:'lakhan04'
      },
      {
        imgclass:'dad',
        imgid:'grand_dad02'
      },
      {
        imgclass:'doctor2',
        imgid:'doctor-stand-png'
      },
      {
        imgclass:'food',
        imgid:'food'
      }]
    }]

  },

  // slide7
  {
    contentnocenteradjust: true,
    imageblock:[{
      imagestoshow:[{
        imgclass:'bg_full',
        imgid:'room01'
      },
      {
        imgclass:'lakhan',
        imgid:'lakhan04'
      },
      {
        imgclass:'dad',
        imgid:'grand_paa_gif'
      },
      {
        imgclass:'doctor2',
        imgid:'doctor-stand-png'
      }]
    }],
    speechbox:[{
      textclass:'insidetext',
      textdata:data.string.p2text6,
      speechbox:'speech5',
      imgid:'textbox1',
      imgclass:'speechbg flipx'
    }]

  },


    // slide8
    {
      contentnocenteradjust: true,
      imageblock:[{
        imagestoshow:[{
          imgclass:'bg_full',
          imgid:'room01'
        },
        {
          imgclass:'lakhan',
          imgid:'lakhan04'
        },
        {
          imgclass:'dad',
          imgid:'grand_dad02'
        },
        {
          imgclass:'doctor2',
          imgid:'doctor-stand-gif'
        }]
      }],
      speechbox:[{
        textclass:'insidetext2',
        textdata:data.string.p2text21,
        speechbox:'speech4',
        imgid:'textbox',
        imgclass:'speechbg'
      }]

    },
  // slide9
  {
    contentblockadditionalclass:'bg_blue',
    imageblock:[{
      imagestoshow:[
        {
          imgclass:'img-1',
          imgid:'planting'
        },
        {
          imgclass:'img-2',
          imgid:'carrot'
        },
        {
          imgclass:'img-3',
          imgid:'brinjal'
        }]
      }],
      uppertextblock:[{
        textclass:'top_text white',
        textdata:data.string.p2text7
      }]
    },

    // slide10
    {
      contentnocenteradjust: true,
      imageblock:[{
        imagestoshow:[{
          imgclass:'bg_full',
          imgid:'room01'
        },
        {
          imgclass:'lakhan flipx',
          imgid:'lakhan02'
        },
        {
          imgclass:'dad',
          imgid:'grand_paa_gif'
        },
        {
          imgclass:'doctor2',
          imgid:'doctor-stand-png'
        }]
      }],
      speechbox:[{
        textclass:'insidetext',
        textdata:data.string.p2text8,
        speechbox:'speech5',
        imgid:'textbox1',
        imgclass:'speechbg flipx'
      }]

    },


    // slide11
    {
      contentnocenteradjust: true,
      imageblock:[{
        imagestoshow:[{
          imgclass:'bg_full',
          imgid:'room01'
        },
        {
          imgclass:'lakhan',
          imgid:'lakhan02'
        },
        {
          imgclass:'dad',
          imgid:'grand_dad02'
        },
        {
          imgclass:'doctor2',
          imgid:'doctor-stand-gif'
        }]
      }],
      speechbox:[{
        textclass:'insidetext2',
        textdata:data.string.p2text9,
        speechbox:'speech4',
        imgid:'textbox',
        imgclass:'speechbg'
      }]

    },

    // slide12
    {
      contentnocenteradjust: true,
      imageblock:[{
        imagestoshow:[{
          imgclass:'bg_full',
          imgid:'room01'
        },
        {
          imgclass:'lakhan flipx',
          imgid:'lakhan01_gif'
        },
        {
          imgclass:'dad',
          imgid:'grand_dad02'
        },
        {
          imgclass:'doctor2',
          imgid:'doctor-stand-png'
        }]
      }],
      speechbox:[{
        textclass:'insidetext3',
        textdata:data.string.p2text10,
        speechbox:'speech6',
        imgid:'textbox2',
        imgclass:'speechbg'
      }]

    },


    // slide13
    {
      contentnocenteradjust: true,
      imageblock:[{
        imagestoshow:[{
          imgclass:'bg_full',
          imgid:'room01'
        },
        {
          imgclass:'lakhan',
          imgid:'lakhan02'
        },
        {
          imgclass:'dad',
          imgid:'grand_dad02'
        },
        {
          imgclass:'doctor2',
          imgid:'doctor-stand-gif'
        }]
      }],
      speechbox:[{
        textclass:'insidetext2',
        textdata:data.string.p2text11,
        speechbox:'speech4',
        imgid:'textbox',
        imgclass:'speechbg'
      }]

    },

    // slide14
    {
      contentblockadditionalclass:'bg_blue',
      imageblock:[{
        imagestoshow:[
          {
            imgclass:'img-1',
            imgid:'compost_moal'
          },
          {
            imgclass:'img-2',
            imgid:'fertilizers'
          },
          {
            imgclass:'img-3',
            imgid:'kit_nasak'
          }]
        }],
        uppertextblock:[{
          textclass:'top_text white',
          textdata:data.string.p2text12
        }]
      },

      // slide15
      {
        contentnocenteradjust: true,
        imageblock:[{
          imagestoshow:[{
            imgclass:'bg_full',
            imgid:'room01'
          },
          {
            imgclass:'lakhan',
            imgid:'lakhan01_gif'
          },
          {
            imgclass:'dad',
            imgid:'grand_dad02'
          },
          {
            imgclass:'doctor2',
            imgid:'doctor-stand-png'
          }]
        }],
        speechbox:[{
          textclass:'insidetext3',
          textdata:data.string.p2text13,
          speechbox:'speech6',
          imgid:'textbox2',
          imgclass:'speechbg'
        }]

      },


      // slide16
      {
        contentnocenteradjust: true,
        imageblock:[{
          imagestoshow:[{
            imgclass:'bg_full',
            imgid:'room01'
          },
          {
            imgclass:'lakhan',
            imgid:'lakhan04'
          },
          {
            imgclass:'dad',
            imgid:'grand_dad02'
          },
          {
            imgclass:'doctor2',
            imgid:'doctor-stand-gif'
          }]
        }],
        speechbox:[{
          textclass:'insidetext2',
          textdata:data.string.p2text14,
          speechbox:'speech4',
          imgid:'textbox',
          imgclass:'speechbg'
        }]

      },

      // slide17
      {
        contentnocenteradjust: true,
        imageblock:[{
          imagestoshow:[{
            imgclass:'bg_full',
            imgid:'room01'
          },
          {
            imgclass:'lakhan',
            imgid:'lakhan04'
          },
          {
            imgclass:'dad',
            imgid:'grand_dad02'
          },
          {
            imgclass:'doctor2',
            imgid:'doctor-stand-gif'
          }]
        }],
        speechbox:[{
          textclass:'insidetext2',
          textdata:data.string.p2text15,
          speechbox:'speech4',
          imgid:'textbox',
          imgclass:'speechbg'
        }]

      },

      // slide18
      {
        contentnocenteradjust: true,
        imageblock:[{
          imagestoshow:[{
            imgclass:'bg_full',
            imgid:'room01'
          },
          {
            imgclass:'flipx lakhan ',
            imgid:'lakhan01_gif'
          },
          {
            imgclass:'dad',
            imgid:'grand_dad02'
          },
          {
            imgclass:'doctor2',
            imgid:'doctor-stand-png'
          }]
        }],
        speechbox:[{
          textclass:'insidetext3',
          textdata:data.string.p2text16,
          speechbox:'speech7',
          imgid:'textbox2',
          imgclass:'speechbg flipx'
        }]

      },

      // slide19
      {
        contentnocenteradjust: true,
        imageblock:[{
          imagestoshow:[{
            imgclass:'bg_full',
            imgid:'room01'
          },
          {
            imgclass:'flipx lakhan',
            imgid:'lakhan01_gif'
          },
          {
            imgclass:'dad',
            imgid:'grand_dad02'
          },
          {
            imgclass:'doctor2',
            imgid:'doctor-stand-png'
          }]
        }],
        speechbox:[{
          textclass:'insidetext3',
          textdata:data.string.p2text17,
          speechbox:'speech7',
          imgid:'textbox2',
          imgclass:'speechbg flipx'
        }]

      },


      // slide20
      {
        contentnocenteradjust: true,
        imageblock:[{
          imagestoshow:[{
            imgclass:'bg_full',
            imgid:'bg_going_hospital'
          },
          {
            imgclass:'dad_return',
            imgid:'coming_home'
          }]
        }],
        uppertextblock:[{
          textclass:'top_text',
          textdata:data.string.p2text18
        }]

      },
      // slide21
      {
        contentnocenteradjust: true,
        imageblock:[{
          imagestoshow:[{
            imgclass:'bg_full',
            imgid:'bg_home01'
          }]
        }],
        uppertextblock:[{
          textclass:'top_text_bg',
          textdata:data.string.p2text19
        }]

      },

      // slide22
      {
        contentnocenteradjust: true,
        imageblock:[{
          imagestoshow:[{
            imgclass:'left_bg',
            imgid:'boy_sleeping'
          },
          {
            imgclass:'right_bg',
            imgid:'compost_maal'
          }]
        }],
        uppertextblock:[{
          textclass:'left_top_text1',
          textdata:data.string.p2text19
        },{
          textclass:'right_top_text',
          textdata:data.string.p2text20
        }]

      },
    ];

    $(function () {
      var $board = $('.board');
      var $nextBtn = $("#activity-page-next-btn-enabled");
      var $prevBtn = $("#activity-page-prev-btn-enabled");
      var $refreshBtn= $("#activity-page-refresh-btn");
      var countNext = 0;
      var count=0;

      var $total_page = content.length;
      loadTimelineProgress($total_page, countNext + 1);

      var preload;
      var timeoutvar = null;
      var current_sound;
      var vocabcontroller =  new Vocabulary();
      vocabcontroller.init();
      function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
          {id: "doctor-png", src: imgpath_hospital+"doctor_sitting01.png", type: createjs.AbstractLoader.IMAGE},
          {id: "doctor-gif", src: imgpath_hospital+"doctor_sitting01.gif", type: createjs.AbstractLoader.IMAGE},
          {id: "doctor-stand-gif", src: imgpath_hospital+"doctor_talking02.gif", type: createjs.AbstractLoader.IMAGE},
          {id: "doctor-stand-png", src: imgpath_hospital+"doctor_talking02.png", type: createjs.AbstractLoader.IMAGE},
          {id: "lakhan02", src: imgpath+"lakhan06.png", type: createjs.AbstractLoader.IMAGE},
          {id: "lakhan03", src: imgpath_hospital+"lakhan03.png", type: createjs.AbstractLoader.IMAGE},
          {id: "lakhan03-gif", src: imgpath_hospital+"lakhan03.gif", type: createjs.AbstractLoader.IMAGE},
          {id: "room", src: imgpath_hospital+"room.png", type: createjs.AbstractLoader.IMAGE},
          {id: "room01", src: imgpath_hospital+"room01.png", type: createjs.AbstractLoader.IMAGE},
          {id: "checking_rajan", src: imgpath_hospital+"checking_rajan.png", type: createjs.AbstractLoader.IMAGE},
          {id: "lakhan04", src: imgpath+"lakhan04.png", type: createjs.AbstractLoader.IMAGE},
          {id: "grand_dad03", src: imgpath+"grand_dad03.png", type: createjs.AbstractLoader.IMAGE},
          {id: "brinjal", src: imgpath+"brinjal.png", type: createjs.AbstractLoader.IMAGE},
          {id: "carrot", src: imgpath+"carrot.png", type: createjs.AbstractLoader.IMAGE},
          {id: "planting", src: imgpath+"planting.png", type: createjs.AbstractLoader.IMAGE},
          {id: "food", src: imgpath+"food.gif", type: createjs.AbstractLoader.IMAGE},
          {id: "compost_moal", src: imgpath+"compost_moal.png", type: createjs.AbstractLoader.IMAGE},
          {id: "fertilizers", src: imgpath+"fertilizers.png", type: createjs.AbstractLoader.IMAGE},
          {id: "kit_nasak", src: imgpath+"kit_nasak.png", type: createjs.AbstractLoader.IMAGE},
          {id: "bg_going_hospital", src: imgpath+"bg_going_hospital.png", type: createjs.AbstractLoader.IMAGE},
          {id: "coming_home", src: imgpath+"coming_home.png", type: createjs.AbstractLoader.IMAGE},
          {id: "bg_home01", src: imgpath+"bg_home01.png", type: createjs.AbstractLoader.IMAGE},
          {id: "compost_maal", src: imgpath+"compost_maal.png", type: createjs.AbstractLoader.IMAGE},
          {id: "boy_sleeping", src: imgpath+"boy_sleeping.png", type: createjs.AbstractLoader.IMAGE},
          {id: "grand_dad02", src: imgpath_hospital+"grand_paa.png", type: createjs.AbstractLoader.IMAGE},
          {id: "grand_paa_gif", src: imgpath_hospital+"grand_paa.gif", type: createjs.AbstractLoader.IMAGE},
          {id: "lakhan01", src: imgpath_hospital+"lakhan01.png", type: createjs.AbstractLoader.IMAGE},
          {id: "lakhan01_gif", src: imgpath_hospital+"lakhan01.gif", type: createjs.AbstractLoader.IMAGE},

          //textbox
          {id: "textbox", src: "images/textbox/white/lb-1.png", type: createjs.AbstractLoader.IMAGE},
          {id: "textbox1", src: "images/textbox/white/tl-1.png", type: createjs.AbstractLoader.IMAGE},
          {id: "textbox2", src: "images/textbox/white/tr-2.png", type: createjs.AbstractLoader.IMAGE},

          // sounds
          {id: "sound_0", src: soundAsset+"s2_p1.ogg"},
          {id: "sound_1", src: soundAsset+"s2_p2.ogg"},
          {id: "sound_2", src: soundAsset+"s2_p3.ogg"},
          {id: "sound_4", src: soundAsset+"s2_p5.ogg"},
          {id: "sound_5", src: soundAsset+"s2_p6.ogg"},
          {id: "sound_7", src: soundAsset+"s2_p8.ogg"},
          {id: "sound_8", src: soundAsset+"s2_p9.ogg"},
          {id: "sound_9", src: soundAsset+"s2_p10.ogg"},
          {id: "sound_10", src: soundAsset+"s2_p11.ogg"},
          {id: "sound_11", src: soundAsset+"s2_p12.ogg"},
          {id: "sound_12", src: soundAsset+"s2_p13.ogg"},
          {id: "sound_13", src: soundAsset+"s2_p14.ogg"},
          {id: "sound_14", src: soundAsset+"s2_p15.ogg"},
          {id: "sound_15", src: soundAsset+"s2_p16.ogg"},
          {id: "sound_16", src: soundAsset+"s2_p17.ogg"},
          {id: "sound_17", src: soundAsset+"s2_p18.ogg"},
          {id: "sound_18", src: soundAsset+"s2_p19.ogg"},
          {id: "sound_19", src: soundAsset+"s2_p20.ogg"},
          {id: "sound_20", src: soundAsset+"s2_p21.ogg"},
          {id: "sound_21", src: soundAsset+"s2_p22.ogg"},
          {id: "sound_22", src: soundAsset+"s2_p23.ogg"},

        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
      }

      function handleFileLoad(event) {
        // console.log(event.item);
      }

      function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
      }

      function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
      }

      //initialize
      init();

      /*==================================================
      =            Handlers and helpers Block            =
      ==================================================*/
      /*==========  register the handlebar partials first  ==========*/
      Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
      Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


      /*=================================================
      =            general template function            =
      =================================================*/
      function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        put_speechbox_image(content, countNext, preload);
        $('.speechbox').hide(0).fadeIn(500);
        switch(countNext){
          case 3:
          case 6:
          navigationcontroller(countNext,$total_page);
          break;
          case 9:
          case 14:
          sound_player("sound_"+countNext);
          $('.img-1, .img-2, .img-3').hide(0);
          $('.img-1').show(500,function(){
            $('.img-2').show(500,function(){
              $('.img-3').show(500);
            });
          });
          $('.img-1').click(function(){
            $('.img-1,.img-2,.img-3').css({'pointer-events':'none'});
            $('.img-2,.img-3').css({'filter':"grayscale(100%)",'pointer-events':'none'});
            play_correct_incorrect_sound(1);
            navigationcontroller(countNext,$total_page);
          });
          $('.img-2,.img-3').click(function(){
            $(this).css({'filter':"grayscale(100%)",'pointer-events':'none'});
            play_correct_incorrect_sound(0);
          });
          break;
          case 20:
          case 21:
          sound_player("sound_"+countNext);
          break;
          case 22:
          $('.left_bg,.right_bg,.left_top_text1,.right_top_text').hide(0);
          $('.left_top_text1,.left_bg').fadeIn(500,function(){
          $('.right_top_text,.right_bg').delay(500).fadeIn(500);
        });
        setTimeout(function(){
          sound_player("sound_"+countNext);
        },1200);
          break;

          default:
          sound_nav("sound_"+(countNext));
          break;
        }
      }


      function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
          if(countNext==9 || countNext==14){
            $nextBtn.hide(0);
            $prevBtn.hide(0);
          }else{
              navigationcontroller(countNext,$total_page);
          }
        });
      }
      function sound_nav(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
          if(countNext==0 || countNext==2 || countNext==16 || countNext==17){
            $('.doctor').attr('src',preload.getResult('doctor-png').src);
            navigationcontroller(countNext,$total_page);
          }else if(countNext==1){
            $('.lakhan').attr('src',preload.getResult('lakhan03').src);
            navigationcontroller(countNext,$total_page);
          }else if(countNext==4 || countNext==7 || countNext==10){
            $('.dad').attr('src',preload.getResult('grand_dad02').src);
            navigationcontroller(countNext,$total_page);
          }else if(countNext==12 || countNext==15 || countNext==18 || countNext==19){
            $('.lakhan').attr('src',preload.getResult('lakhan01').src);
            navigationcontroller(countNext,$total_page);
          }else if(countNext==5 || countNext==11 || countNext==13 || countNext==8){
            $('.doctor2').attr('src',preload.getResult('doctor-stand-png').src);
            navigationcontroller(countNext,$total_page);
          }

        });
      }

      function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
      }

      $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
          default:
          countNext++;
          templateCaller();
          break;
        }
      });

      $refreshBtn.click(function(){
        templateCaller();
      });

      $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
        previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
      });

      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
            (stylerulename = $(this).attr("data-highlightcustomclass")) :
            (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";


            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            $(this).html(replaceinstring);
          });
        }
      }
    });

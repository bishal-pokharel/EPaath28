var imgpath = $ref + "/images/diy_image/";
var soundAsset = $ref+"/sounds/";

var content = [
  //slide0
  {
    contentnocenteradjust: true,
    uppertextblock:[{
      textclass:'center_text',
      textdata:data.string.diy
    },],
    imageblock:[{
      imagestoshow:[{
        imgclass:"bg_full",
        imgid:"diy_bg_2"
      }]
    }]
  },
  // slide1
  {
    contentnocenteradjust: true,
    contentblockadditionalclass:'bg_blue',
    uppertextblock:[{
      textclass:'top_text',
      textdata:data.string.p3text1
    },
    {
      textclass:'question',
      textdata:data.string.p3text2
    },
    {
      textclass:'option_1 ',
      textdata:data.string.p3text3
    },
    {
      textclass:'option_2 correct',
      textdata:data.string.p3text4
    },
  ],
    imageblock:[{
      imagestoshow:[{
        imgclass:"right_img",
        imgid:"planting"
      }]
    }]
  },

  // slide2
  {
    contentnocenteradjust: true,
    contentblockadditionalclass:'bg_blue',
    uppertextblock:[{
      textclass:'top_text',
      textdata:data.string.p3text1
    },
    {
      textclass:'question',
      textdata:data.string.p3text5
    },
    {
      textclass:'option_1 correct',
      textdata:data.string.p3text6
    },
    {
      textclass:'option_2 ',
      textdata:data.string.p3text7
    },
  ],
    imageblock:[{
      imagestoshow:[{
        imgclass:"right_img",
        imgid:"jaibik_khati"
      }]
    }]
  },

  // slide3
  {
    contentnocenteradjust: true,
    contentblockadditionalclass:'bg_blue',
    uppertextblock:[{
      textclass:'top_text',
      textdata:data.string.p3text1
    },
    {
      textclass:'question',
      textdata:data.string.p3text8
    },
    {
      textclass:'option_1 ',
      textdata:data.string.p3text10
    },
    {
      textclass:'option_2 correct',
      textdata:data.string.p3text9
    },
  ],
    imageblock:[{
      imagestoshow:[{
        imgclass:"right_img",
        imgid:"sugercane_field"
      }]
    }]
  },

  // slide4
  {
    contentnocenteradjust: true,
    contentblockadditionalclass:'bg_blue',
    uppertextblock:[{
      textclass:'top_text',
      textdata:data.string.p3text1
    },
    {
      textclass:'question',
      textdata:data.string.p3text11
    },
    {
      textclass:'option_1 ',
      textdata:data.string.p3text13
    },
    {
      textclass:'option_2 correct',
      textdata:data.string.p3text12
    },
  ],
    imageblock:[{
      imagestoshow:[{
        imgclass:"right_img",
        imgid:"maal"
      }]
    }]
  },

  // slide5
  {
    contentnocenteradjust: true,
    contentblockadditionalclass:'bg_blue',
    uppertextblock:[{
      textclass:'top_text',
      textdata:data.string.p3text1
    },
    {
      textclass:'question',
      textdata:data.string.p3text14
    },
    {
      textclass:'option_1 correct ',
      textdata:data.string.p3text15
    },
    {
      textclass:'option_2 ',
      textdata:data.string.p3text16
    },
  ],
    imageblock:[{
      imagestoshow:[{
        imgclass:"right_img",
        imgid:"thinking_niti"
      }]
    }]
  },


];

$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page, countNext + 1);

  var preload;
  var timeoutvar = null;
  var current_sound;
  var vocabcontroller =  new Vocabulary();
  vocabcontroller.init();
  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      {id: "jaibik_khati", src: imgpath+"jaibik_khati.png", type: createjs.AbstractLoader.IMAGE},
      {id: "planting", src: imgpath+"planting.png", type: createjs.AbstractLoader.IMAGE},
      {id: "sugercane_field", src: imgpath+"sugercane_field.png", type: createjs.AbstractLoader.IMAGE},
      {id: "sugercane", src: imgpath+"sugercane.png", type: createjs.AbstractLoader.IMAGE},
      {id: "thinking_niti_after", src: imgpath+"sick_niti.png", type: createjs.AbstractLoader.IMAGE},
      {id: "maal", src: imgpath+"maal.png", type: createjs.AbstractLoader.IMAGE},
      {id: "bisaadi", src: imgpath+"bisaadi.png", type: createjs.AbstractLoader.IMAGE},
      {id: "thinking_niti", src: imgpath+"thinking_niti.png", type: createjs.AbstractLoader.IMAGE},
      {id: "diy_bg_2", src: imgpath+"diy_bg_2.jpg", type: createjs.AbstractLoader.IMAGE},
      // sounds
      {id: "sound_0", src: soundAsset+"s3_p1.ogg"},

    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound);//for registering sounds
    preload.installPlugin(createjs.Sound);//for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }

  function handleFileLoad(event) {
    // console.log(event.item);
  }

  function handleProgress(event) {
    $('#loading-text').html(parseInt(event.loaded * 100) + '%');
  }

  function handleComplete(event) {
    $('#loading-wrapper').hide(0);
    //initialize varibales
    current_sound = createjs.Sound.play('sound_1');
    current_sound.stop();
    // call main function
    templateCaller();
  }

  //initialize
  init();

  /*==================================================
  =            Handlers and helpers Block            =
  ==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


  /*=================================================
  =            general template function            =
  =================================================*/
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    texthighlight($board);
    vocabcontroller.findwords(countNext);
    put_image(content, countNext,preload);
    put_speechbox_image(content, countNext, preload);
    $('.speechbox').hide(0).fadeIn(500);

    $('.option_1,.option_2').click(function(){
      createjs.Sound.stop();
      if($(this).hasClass('correct')){
        switch (countNext) {
          case 5:
            $('.right_img').attr('src',preload.getResult('thinking_niti_after').src);
            break;
          default:

        }
        var $this = $(this);
        var position = $this.position();
        var width = $this.width();
        var height = $this.height();
        var centerX = ((position.left + width / 2)*100)/$('.coverboardfull ').width()+'%';
        var centerY = (((position.top + height)*100)/$('.coverboardfull ').height())+'%';
        $(this).css({'border-color':'#D4EF34','background':'#92AF3B','color':'white'});
        $('.option_1,.option_2').css('pointer-events','none');
        $('<img style="left:'+centerX+';top:'+centerY+';position:absolute;height:5%;transform:translate(626%,-73%);z-index:2;" src="'+imgpath +'correct.png" />').insertAfter(this);
        play_correct_incorrect_sound(1);
        navigationcontroller(countNext,$total_page);
      }
      else{
        var $this = $(this);
        var position = $this.position();
        var width = $this.width();
        var height = $this.height();
        var centerX = ((position.left + width / 2)*100)/$('.coverboardfull ').width()+'%';
        var centerY = (((position.top + height)*100)/$('.coverboardfull ').height())+'%';
        $(this).css({'border-color':'#980000','background':'#FF0000','color':'white'});
        $(this).css('pointer-events','none');
        $('<img style="left:'+centerX+';top:'+centerY+';position:absolute;height:5%;transform:translate(626%,-73%);z-index:2;" src="'+imgpath +'incorrect.png" />').insertAfter(this);
        play_correct_incorrect_sound(0);
      }
    });
    switch (countNext) {
      case 0:
      play_diy_audio();
      $nextBtn.delay(2000).show(0);
      break;
      case 1:
        sound_player("sound_0");
      break;
    }


  }


  function sound_player(sound_id,navigate) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
  }


  function templateCaller() {
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');
    generaltemplate();
    loadTimelineProgress($total_page, countNext + 1);
  }

  $nextBtn.on('click', function () {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    countNext++;
    templateCaller();
});

$refreshBtn.click(function(){
  templateCaller();
});

$prevBtn.on('click', function () {
  createjs.Sound.stop();
  clearTimeout(timeoutvar);
  countNext--;
  templateCaller();
  /* if footerNotificationHandler pageEndSetNotification was called then on click of
  previous slide button hide the footernotification */
  countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
});

function texthighlight($highlightinside){
  //check if $highlightinside is provided
  typeof $highlightinside !== "object" ?
  alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
  null ;

  var $alltextpara = $highlightinside.find("*[data-highlight='true']");
  var stylerulename;
  var replaceinstring;
  var texthighlightstarttag;
  var texthighlightendtag   = "</span>";


  if($alltextpara.length > 0){
    $.each($alltextpara, function(index, val) {
      /*if there is a data-highlightcustomclass attribute defined for the text element
      use that or else use default 'parsedstring'*/
      $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
      (stylerulename = $(this).attr("data-highlightcustomclass")) :
      (stylerulename = "parsedstring") ;

      texthighlightstarttag = "<span class='"+stylerulename+"'>";


      replaceinstring       = $(this).html();
      replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
      replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


      $(this).html(replaceinstring);
    });
  }
}
});

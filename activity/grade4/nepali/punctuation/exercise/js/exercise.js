var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "contenttitle1 title1",
                textdata: data.string.extitle
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "img1div imagediv",
                    imgclass: "relativecls img1img",
                    imgid: 'img1Img',
                    imgsrc: ""
                }
            ]
        }],
        optionblock: [
            {
                outerdiv: "outerdiv",
                textblock: [
                    {
                        divclass: "draggable opt1",
                        textclass: "opt11img",
                        textdata: data.string.alpabiramsign
                    },
                    {
                        divclass: "draggable opt2 correct1",
                        textclass: "opt2img",
                        textdata: data.string.purnabiramsign
                    },
                    {
                        divclass: "draggable opt3",
                        textclass: "opt3img",
                        textdata: data.string.bismayasign
                    },
                    {
                        divclass: "draggable opt4",
                        textclass: "opt4img",
                        textdata: data.string.udaranopn
                    },
                    {
                        divclass: "draggable opt5",
                        textclass: "opt5img",
                        textdata: data.string.udarancls
                    },
                    {
                        divclass: "draggable opt6",
                        textclass: "opt6img",
                        textdata: data.string.prasnasign
                    }
                ]
            }
        ],
        lowertext:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "droppable dropbox drop1",
                textclass: "contenttitle1 question",
                textdata: data.string.exq1
            }
        ]
    },
    //slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "contenttitle1 title1",
                textdata: data.string.extitle
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "img2div imagediv",
                    imgclass: "relativecls img2img",
                    imgid: 'img2Img',
                    imgsrc: ""
                }
            ]
        }],
        optionblock: [
            {
                outerdiv: "outerdiv",
                textblock: [
                    {
                        divclass: "draggable opt1",
                        textclass: "opt11img",
                        textdata: data.string.alpabiramsign
                    },
                    {
                        divclass: "draggable opt2 correct1",
                        textclass: "opt2img",
                        textdata: data.string.purnabiramsign
                    },
                    {
                        divclass: "draggable opt3",
                        textclass: "opt3img",
                        textdata: data.string.bismayasign
                    },
                    {
                        divclass: "draggable opt4",
                        textclass: "opt4img",
                        textdata: data.string.udaranopn
                    },
                    {
                        divclass: "draggable opt5",
                        textclass: "opt5img",
                        textdata: data.string.udarancls
                    },
                    {
                        divclass: "draggable opt6",
                        textclass: "opt6img",
                        textdata: data.string.prasnasign
                    }
                ]
            }
        ],
        lowertext:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "droppable dropbox drop1",
                textclass: "contenttitle1 question q1",
                textdata: data.string.exq2
            }
        ]
    },
    //slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "contenttitle1 title1",
                textdata: data.string.extitle
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "img3div imagediv",
                    imgclass: "relativecls img3img",
                    imgid: 'img3Img',
                    imgsrc: ""
                }
            ]
        }],
        optionblock: [
            {
                outerdiv: "outerdiv",
                textblock: [
                    {
                        divclass: "draggable opt1",
                        textclass: "opt11img",
                        textdata: data.string.alpabiramsign
                    },
                    {
                        divclass: "draggable opt2 correct1",
                        textclass: "opt2img",
                        textdata: data.string.purnabiramsign
                    },
                    {
                        divclass: "draggable opt3",
                        textclass: "opt3img",
                        textdata: data.string.bismayasign
                    },
                    {
                        divclass: "draggable opt4",
                        textclass: "opt4img",
                        textdata: data.string.udaranopn
                    },
                    {
                        divclass: "draggable opt5",
                        textclass: "opt5img",
                        textdata: data.string.udarancls
                    },
                    {
                        divclass: "draggable opt6",
                        textclass: "opt6img",
                        textdata: data.string.prasnasign
                    }
                ]
            }
        ],
        lowertext:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "droppable dropbox drop1",
                textclass: "contenttitle1 question",
                textdata: data.string.exq3
            }
        ]
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "contenttitle1 title1",
                textdata: data.string.extitle
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "img4div imagediv",
                    imgclass: "relativecls img4img",
                    imgid: 'img4Img',
                    imgsrc: ""
                }
            ]
        }],
        optionblock: [
            {
                outerdiv: "outerdiv",
                textblock: [
                    {
                        divclass: "draggable opt1",
                        textclass: "opt11img",
                        textdata: data.string.alpabiramsign
                    },
                    {
                        divclass: "draggable opt2 correct1",
                        textclass: "opt2img",
                        textdata: data.string.purnabiramsign
                    },
                    {
                        divclass: "draggable opt3",
                        textclass: "opt3img",
                        textdata: data.string.bismayasign
                    },
                    {
                        divclass: "draggable opt4",
                        textclass: "opt4img",
                        textdata: data.string.udaranopn
                    },
                    {
                        divclass: "draggable opt5",
                        textclass: "opt5img",
                        textdata: data.string.udarancls
                    },
                    {
                        divclass: "draggable opt6",
                        textclass: "opt6img",
                        textdata: data.string.prasnasign
                    }
                ]
            }
        ],
        lowertext:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "droppable dropbox drop1",
                textclass: "contenttitle1 question q1",
                textdata: data.string.exq4
            }
        ]
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "contenttitle1 title1",
                textdata: data.string.extitle
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "img5div imagediv",
                    imgclass: "relativecls img5img",
                    imgid: 'img5Img',
                    imgsrc: ""
                }
            ]
        }],
        optionblock: [
            {
                outerdiv: "outerdiv",
                textblock: [
                    {
                        divclass: "draggable opt1",
                        textclass: "opt11img",
                        textdata: data.string.alpabiramsign
                    },
                    {
                        divclass: "draggable opt2",
                        textclass: "opt2img",
                        textdata: data.string.purnabiramsign
                    },
                    {
                        divclass: "draggable opt3",
                        textclass: "opt3img",
                        textdata: data.string.bismayasign
                    },
                    {
                        divclass: "draggable opt4 correct1",
                        textclass: "opt4img",
                        textdata: data.string.udaranopn
                    },
                    {
                        divclass: "draggable opt5 correct2",
                        textclass: "opt5img",
                        textdata: data.string.udarancls
                    },
                    {
                        divclass: "draggable opt6",
                        textclass: "opt6img",
                        textdata: data.string.prasnasign
                    }
                ]
            }
        ],
        lowertext:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "droppable dropbox drop1",
                textclass: "contenttitle1 question q4",
                textdata: data.string.exq5
            }
        ]
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "contenttitle1 title1",
                textdata: data.string.extitle
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "img6div imagediv",
                    imgclass: "relativecls img6img",
                    imgid: 'img6Img',
                    imgsrc: ""
                }
            ]
        }],
        optionblock: [
            {
                outerdiv: "outerdiv",
                textblock: [
                    {
                        divclass: "draggable opt1",
                        textclass: "opt11img",
                        textdata: data.string.alpabiramsign
                    },
                    {
                        divclass: "draggable opt2",
                        textclass: "opt2img",
                        textdata: data.string.purnabiramsign
                    },
                    {
                        divclass: "draggable opt3",
                        textclass: "opt3img",
                        textdata: data.string.bismayasign
                    },
                    {
                        divclass: "draggable opt4 correct1",
                        textclass: "opt4img",
                        textdata: data.string.udaranopn
                    },
                    {
                        divclass: "draggable opt5 correct2",
                        textclass: "opt5img",
                        textdata: data.string.udarancls
                    },
                    {
                        divclass: "draggable opt6",
                        textclass: "opt6img",
                        textdata: data.string.prasnasign
                    }
                ]
            }
        ],
        lowertext:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "droppable dropbox drop1",
                textclass: "contenttitle1 question q4",
                textdata: data.string.exq6
            }
        ]
    },
    //slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "contenttitle1 title1",
                textdata: data.string.extitle
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "img7div imagediv",
                    imgclass: "relativecls img7img",
                    imgid: 'img7Img',
                    imgsrc: ""
                }
            ]
        }],
        optionblock: [
            {
                outerdiv: "outerdiv",
                textblock: [
                    {
                        divclass: "draggable opt1",
                        textclass: "opt11img",
                        textdata: data.string.alpabiramsign
                    },
                    {
                        divclass: "draggable opt2",
                        textclass: "opt2img",
                        textdata: data.string.purnabiramsign
                    },
                    {
                        divclass: "draggable opt3",
                        textclass: "opt3img",
                        textdata: data.string.bismayasign
                    },
                    {
                        divclass: "draggable opt4",
                        textclass: "opt4img",
                        textdata: data.string.udaranopn
                    },
                    {
                        divclass: "draggable opt5",
                        textclass: "opt5img",
                        textdata: data.string.udarancls
                    },
                    {
                        divclass: "draggable opt6 correct1",
                        textclass: "opt6img",
                        textdata: data.string.prasnasign
                    }
                ]
            }
        ],
        lowertext:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "droppable dropbox drop1",
                textclass: "contenttitle1 question",
                textdata: data.string.exq7
            }
        ]
    },
    //slide7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "contenttitle1 title1",
                textdata: data.string.extitle
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "img8div imagediv",
                    imgclass: "relativecls img8img",
                    imgid: 'img8Img',
                    imgsrc: ""
                }
            ]
        }],
        optionblock: [
            {
                outerdiv: "outerdiv",
                textblock: [
                    {
                        divclass: "draggable opt1",
                        textclass: "opt11img",
                        textdata: data.string.alpabiramsign
                    },
                    {
                        divclass: "draggable opt2",
                        textclass: "opt2img",
                        textdata: data.string.purnabiramsign
                    },
                    {
                        divclass: "draggable opt3",
                        textclass: "opt3img",
                        textdata: data.string.bismayasign
                    },
                    {
                        divclass: "draggable opt4 correct1",
                        textclass: "opt4img",
                        textdata: data.string.udaranopn
                    },
                    {
                        divclass: "draggable opt5 correct2",
                        textclass: "opt5img",
                        textdata: data.string.udarancls
                    },
                    {
                        divclass: "draggable opt7",
                        textclass: "opt6img",
                        textdata: data.string.prasnasign
                    }
                ]
            }
        ],
        lowertext:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "droppable dropbox drop1",
                textclass: "contenttitle1 question q4",
                textdata: data.string.exq8
            }
        ]
    },
    //slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "contenttitle1 title1",
                textdata: data.string.extitle
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "img9div imagediv",
                    imgclass: "relativecls img9img",
                    imgid: 'img9Img',
                    imgsrc: ""
                }
            ]
        }],
        optionblock: [
            {
                outerdiv: "outerdiv",
                textblock: [
                    {
                        divclass: "draggable opt1",
                        textclass: "opt11img",
                        textdata: data.string.alpabiramsign
                    },
                    {
                        divclass: "draggable opt2",
                        textclass: "opt2img",
                        textdata: data.string.purnabiramsign
                    },
                    {
                        divclass: "draggable opt3",
                        textclass: "opt3img",
                        textdata: data.string.bismayasign
                    },
                    {
                        divclass: "draggable opt4",
                        textclass: "opt4img",
                        textdata: data.string.udaranopn
                    },
                    {
                        divclass: "draggable opt5",
                        textclass: "opt5img",
                        textdata: data.string.udarancls
                    },
                    {
                        divclass: "draggable opt6 correct1",
                        textclass: "opt6img",
                        textdata: data.string.prasnasign
                    }
                ]
            }
        ],
        lowertext:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "droppable dropbox drop1",
                textclass: "contenttitle1 question",
                textdata: data.string.exq9
            }
        ]
    },
    //slide 9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "contenttitle1 title1",
                textdata: data.string.extitle
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "img10div imagediv",
                    imgclass: "relativecls img10img",
                    imgid: 'img10Img',
                    imgsrc: ""
                }
            ]
        }],
        optionblock: [
            {
                outerdiv: "outerdiv",
                textblock: [
                    {
                        divclass: "draggable opt1",
                        textclass: "opt11img",
                        textdata: data.string.alpabiramsign
                    },
                    {
                        divclass: "draggable opt2 correct1",
                        textclass: "opt2img",
                        textdata: data.string.purnabiramsign
                    },
                    {
                        divclass: "draggable opt3",
                        textclass: "opt3img",
                        textdata: data.string.bismayasign
                    },
                    {
                        divclass: "draggable opt4",
                        textclass: "opt4img",
                        textdata: data.string.udaranopn
                    },
                    {
                        divclass: "draggable opt5",
                        textclass: "opt5img",
                        textdata: data.string.udarancls
                    },
                    {
                        divclass: "draggable opt6",
                        textclass: "opt6img",
                        textdata: data.string.prasnasign
                    }
                ]
            }
        ],
        lowertext:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "droppable dropbox drop1",
                textclass: "contenttitle1 question",
                textdata: data.string.exq10
            }
        ]
    },
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var countNext = 0;
    var wrongclicked = false;
    var alreadyupdated = false;

    var $total_page = content.length;
    // loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var count=0;

    var numbertemplate = new NumberTemplate();
    numbertemplate.init($total_page-1);


    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "ex", src: $ref + "css/exercisetemplate.css", type: createjs.AbstractLoader.CSS},
            {id: "ex1", src: $ref + "css/exercise1.css", type: createjs.AbstractLoader.CSS},

            {id: "img1Img", src: imgpath+"b10.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img2Img", src: imgpath+"b04.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img3Img", src: imgpath+"b05.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img4Img", src: imgpath+"b01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img5Img", src: imgpath+"b02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img6Img", src: imgpath+"b08.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img7Img", src: imgpath+"b09.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img8Img", src: imgpath+"b03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img9Img", src: imgpath+"b06.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img10Img", src: imgpath+"b07.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sound_1", src: soundAsset+"ex.ogg"},


        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());




    /*=====  End of user navigation controller function  ======*/


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
         wrongclicked = false;
         alreadyupdated = false;
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);

        put_image(content, countNext);
        switch (countNext){
            case 0:
                sound_player("sound_1");
                wrongclicked=false;
                shufflehint(data.string.purnabiramsign);
                dragdrop1(data.string.purnabiramsign);
                break;
            case 1:
                wrongclicked=false;
                shufflehint(data.string.alpabiramsign);
                dragdrop1(data.string.alpabiramsign);
                break;
            case 2:
                wrongclicked=false;
                shufflehint(data.string.prasnasign);
                dragdrop1(data.string.prasnasign);
                break;
            case 3:
                wrongclicked=false;
                shufflehint(data.string.bismayasign);
                dragdrop1(data.string.bismayasign);
                break;
            case 4:
                count=0;
                wrongclicked=false;
                shufflehint(data.string.udaranopn,data.string.udarancls);
                dragdrop3(data.string.udaranopn,data.string.udarancls);
                break;
            case 5:
                wrongclicked=false;
                count=0;
                shufflehint(data.string.udaranopn,data.string.udarancls);
                dragdrop3(data.string.udaranopn,data.string.udarancls);
                break;
            case 6:
                wrongclicked=false;
                shufflehint(data.string.prasnasign);
                dragdrop1(data.string.prasnasign);
                break;
            case 7:
                wrongclicked=false;
                count=0;
                shufflehint(data.string.udaranopn,data.string.udarancls);
                dragdrop3(data.string.udaranopn,data.string.udarancls);
                break;
            case 8:
                wrongclicked=false;
                shufflehint(data.string.prasnasign);
                dragdrop1(data.string.prasnasign);
                break;
            case 9:
                wrongclicked=false;
                shufflehint(data.string.purnabiramsign);
                dragdrop1(data.string.purnabiramsign);
                break;
        }

    }

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }
    function sound_player(sound_id) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigationcontroller();
        });
    }


    function put_image(content, count) {
        if (content[count].hasOwnProperty('imageblock')) {
            var imageblock = content[count].imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }



    function templateCaller() {
        $nextBtn.css('display', 'none');
        generaltemplate();
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                numbertemplate.gotoNext();
                templateCaller();
                break;
        }
    });


    function shufflehint(correcttxt,correcttxt2) {
        var optdiv = $(".outerdiv");

        for (var i = optdiv.children().length; i >= 0; i--) {
            optdiv.append(optdiv.children().eq(Math.random() * i | 0));
        }
        optdiv.children().removeClass();
        var optionclass = ["draggable opt1","draggable opt2","draggable opt3","draggable opt4","draggable opt5","draggable opt6"]
        optdiv.children().each(function (index) {
            $(this).addClass(optionclass[index]);
            if($(this).text().trim()==correcttxt){
                $(this).addClass("correct1");
            }
            else if($(this).text().trim()==correcttxt2){
                $(this).addClass("correct2");
            }
        });

    }

    function dragdrop1(correctans1,correctans2){
        $(".draggable").draggable({
            containment: "body",
            revert: true,
            appendTo: "body",
            zindex: 1000,
            stop:function () {
                if (!$(this).hasClass("correct1")) {
                    $(this).addClass("incorrect-ans avoid-clicks");
                    $(this).prepend("<img class='correctImg1' src='images/wrong.png'/>");
                    $(this).find("p").addClass("wrongImgptag");
                }
            }
        });
        $('.droppable').droppable({
            accept : ".draggable",
            hoverClass: "hovered",
            drop: function(event, ui) {
                if(ui.draggable.hasClass("correct1")) {
                    ui.draggable.hide(0);
                    $(".dropbox").text(correctans1);
                    $(".dropbox").addClass("correctcss");
                    $(".question p").append("<img class='correctImg' src='images/right.png'/>");
                    $(".draggable").addClass("avoid-clicks");
                    current_sound.stop();
                    play_correct_incorrect_sound(1);
                    navigationcontroller(countNext, $total_page);
                    wrongclicked == false ? numbertemplate.update(true) : "";
                   }
                else{
                    current_sound.stop();
                    play_correct_incorrect_sound(0);
                    wrongclicked = true;
                }
            }
        });
    }
    function dragdrop3(correctans1,correctans2){
        $(".draggable").draggable({
            containment: "body",
            revert: true,
            appendTo: "body",
            zindex: 1000,
        });
        $('.droppable').droppable({
            accept : ".draggable",
            hoverClass: "hovered",
            drop: function(event, ui) {
                if (ui.draggable.hasClass("correct1")) {
                    count++;
                    play_correct_incorrect_sound(1);
                    ui.draggable.hide(0);
                    $(".dropbox").eq(0).text(correctans1);
                    $(".dropbox").eq(0).addClass("correctcss");
                    play_correct_incorrect_sound(1);
                    if (count == 2) {
                        wrongclicked == false ? numbertemplate.update(true) : "";
                        navigationcontroller(countNext, $total_page);
                        $(".question p").append("<img class='correctImg' src='images/right.png'/>");
                    }
                }
                else if (ui.draggable.hasClass("correct2")) {
                    count++;
                    ui.draggable.hide(0);
                    $(".dropbox").eq(1).text(correctans2);
                    $(".dropbox").eq(1).addClass("correctcss");
                    play_correct_incorrect_sound(1);
                    if (count == 2) {
                        wrongclicked == false ? numbertemplate.update(true) : "";
                        navigationcontroller(countNext, $total_page);
                        $(".question p").append("<img class='correctImg' src='images/right.png'/>");
                    }
                }
                else {
                    play_correct_incorrect_sound(0);
                    wrongclicked = true;
                }
            }
        });
        // $('.droppable1').droppable({
        //     accept : ".draggable",
        //     hoverClass: "hovered",
        //     drop: function(event, ui) {
        //         if(ui.draggable.hasClass("correct2")) {
        //             var answeredfirst = ($('.dropbox1 > p').is(':empty'));
        //             var answeredfirst2 = ($('.dropbox2 > p').is(':empty'));
        //             ui.draggable.hide(0);
        //             $(".dropbox2 p").text(correctans2).addClass("correctImgptag");
        //             $(".dropbox2").addClass("correctcss");
        //             $(".dropbox2").prepend("<img class='correctImg' src='images/right.png'/>");
        //             (!(answeredfirst && answeredfirst2)) ? $(".draggable").addClass("avoid-clicks") : "";
        //             play_correct_incorrect_sound(1);
        //             if (!(answeredfirst && answeredfirst2))
        //                 navigationcontroller(countNext, $total_page);
        //
        //         }
        //         else{
        //             play_correct_incorrect_sound(0);
        //             wrongclicked = true;
        //         }
        //
        //     },
        //     deactivate:function(event,ui){
        //         var answeredfirst = ($('.dropbox1 > p').is(':empty'));
        //         var answeredfirst2 = ($('.dropbox2 > p').is(':empty'));
        //         if(!wrongclicked && !alreadyupdated && !answeredfirst2 && !answeredfirst){
        //             numbertemplate.update(true);
        //             alreadyupdated = true;
        //         }
        //         else{
        //             numbertemplate.update(false);
        //         }
        //     }
        // });
    }

});
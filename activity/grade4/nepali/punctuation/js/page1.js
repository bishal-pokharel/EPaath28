var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "chapter lessontitle",
                textdata: data.string.lesson_title
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images buble1",
                    imgclass: "relativecls bubble1img",
                    imgid: 'bubbleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images buble2",
                    imgclass: "relativecls bubble1img",
                    imgid: 'bubbleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "images buble3",
                    imgclass: "relativecls bubble1img",
                    imgid: 'bubbleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "images buble4",
                    imgclass: "relativecls bubble1img",
                    imgid: 'bubbleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "images buble5",
                    imgclass: "relativecls bubble1img",
                    imgid: 'bubbleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "images buble1",
                    imgclass: "relativecls img1img",
                    imgid: 'img1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "images buble2",
                    imgclass: "relativecls img2img",
                    imgid: 'img2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "images buble3",
                    imgclass: "relativecls img3img",
                    imgid: 'img3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "images buble4",
                    imgclass: "relativecls img4img",
                    imgid: 'img4Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "images buble5",
                    imgclass: "relativecls img5img",
                    imgid: 'img5Img',
                    imgsrc: ""
                }

            ]
        }]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgmain",
        uppertextblock: [
            {
                textclass: "contenttitle title1",
                textdata: data.string.p1text1
            },
            {
                textclass: "contenttitle title2",
                textdata: data.string.p1text2
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "jaldevidiv",
                    imgclass: "relativecls jaldeviimg",
                    imgid: 'jaldeviImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "woodcutter",
                    imgclass: "relativecls woodcutterimg",
                    imgid: 'woodcutterImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialogbox",
                    imgclass: "relativecls dialogboximg",
                    imgid: 'dialogboxImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialogbox1",
                    imgclass: "relativecls dialogbox1img",
                    imgid: 'dialogbox1Img',
                    imgsrc: ""
                }
            ]
        }]
    },
    //slide 2

    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgmain",
        uppertextblock: [
            {
                datahighlightflag: true,
                datahighlightcustomclass: "punctuationmark",
                textclass: "contenttitle title1",
                textdata: data.string.p1text3
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "punctuationmark",
                textclass: "contenttitle title2",
                textdata: data.string.p1text5
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "jaldevidiv",
                    imgclass: "relativecls jaldeviimg",
                    imgid: 'jaldeviImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "woodcutter",
                    imgclass: "relativecls woodcutterimg",
                    imgid: 'woodcutterImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialogbox",
                    imgclass: "relativecls dialogboximg",
                    imgid: 'dialogboxImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialogbox1",
                    imgclass: "relativecls dialogbox1img",
                    imgid: 'dialogbox1Img',
                    imgsrc: ""
                }
            ]
        }],
        lowertext:[
            {
                textclass: "contenttitle lowertext title10",
                textdata: data.string.p1text4
            }
        ]
    },
    // slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgmain",
        uppertextblock: [
            {
                datahighlightflag: true,
                datahighlightcustomclass: "colorchange",
                textclass: "contenttitle title1",
                textdata: data.string.p1text3_1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "colorchange",
                textclass: "contenttitle title2",
                textdata: data.string.p1text5_1
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "jaldevidiv",
                    imgclass: "relativecls jaldeviimg",
                    imgid: 'jaldeviImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "woodcutter",
                    imgclass: "relativecls woodcutterimg",
                    imgid: 'woodcutterImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialogbox",
                    imgclass: "relativecls dialogboximg",
                    imgid: 'dialogboxImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dialogbox1",
                    imgclass: "relativecls dialogbox1img",
                    imgid: 'dialogbox1Img',
                    imgsrc: ""
                }
            ]
        }],
        lowertext:[
            {
                textclass: "contenttitle lowertext title11",
                textdata: data.string.p1text8
            }
        ]
    },
    //slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "chapter lessontitle",
                textdata: data.string.lesson_title
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images buble1div",
                    imgclass: "relativecls bubble1img",
                    imgid: 'bubbleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images buble2div",
                    imgclass: "relativecls bubble1img",
                    imgid: 'bubbleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "images buble3div",
                    imgclass: "relativecls bubble1img",
                    imgid: 'bubbleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "images buble4div",
                    imgclass: "relativecls bubble1img",
                    imgid: 'bubbleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "images buble5div",
                    imgclass: "relativecls bubble1img",
                    imgid: 'bubbleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "images img1div",
                    imgclass: "relativecls img1img",
                    imgid: 'img1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "images img2div",
                    imgclass: "relativecls img2img",
                    imgid: 'img2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "images img3div",
                    imgclass: "relativecls img3img",
                    imgid: 'img3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "images img4div",
                    imgclass: "relativecls img4img",
                    imgid: 'img4Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "images img5div",
                    imgclass: "relativecls img5img",
                    imgid: 'img5Img',
                    imgsrc: ""
                }

            ]
        }]
    },
    //slide5
    {   contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        uppertextblockadditionalclass:"punctuationtitle",
        uppertextblock: [
            {
                textclass: "punctuationtext",
                textdata: data.string.purnabiram
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images punctuationsymbol",
                    imgclass: "relativecls bubble1img",
                    imgid: 'bubbleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "images punctuationsymbol",
                    imgclass: "relativecls img1img",
                    imgid: 'img1Img',
                    imgsrc: ""
                }
             ]
        }]

    },
    //slide6
    {   contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        uppertextblockadditionalclass:"punctuationtitle",
        uppertextblock: [
            {
                textclass: "punctuationtext",
                textdata: data.string.purnabiram
            }

        ],
        extradiv:[
            {
                divclass:"road"
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images punctuationsymbol",
                    imgclass: "relativecls bubble1img",
                    imgid: 'bubbleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "images punctuationsymbol",
                    imgclass: "relativecls img1img",
                    imgid: 'img1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "images  cyclinggirl ",
                    imgclass: "relativecls cyclinggirlimg",
                    imgid: 'cyclinggirlImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "images  cyclinggirl1 ",
                    imgclass: "relativecls cyclinggirlimg",
                    imgid: 'cyclinggirlImg1',
                    imgsrc: ""
                }
            ]
        }],
        lowertext:[
            {
                textclass: "contentopic punctutationtopic",
                textdata: data.string.p1text9
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "movingpunctuation",
                textclass: "contenttitle punctuationinfo",
                textdata: data.string.p1text10
            }
        ]

    },
    //slide7
    {   contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        uppertextblockadditionalclass:"punctuationtitle1",
        uppertextblock: [
            {
                textclass: "punctuationtext",
                textdata: data.string.purnabiram
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images punctuationsymbol1",
                    imgclass: "relativecls bubble1img",
                    imgid: 'bubbleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "images punctuationsymbol1",
                    imgclass: "relativecls img1img",
                    imgid: 'img1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "images  eatinggrass",
                    imgclass: "relativecls eatinggrassimg",
                    imgid: 'eatinggrassImg',
                    imgsrc: ""
                }
            ]
        }],
        lowertext:[
            {
                textclass: "contentopic punctutationtopic",
                textdata: data.string.p1text9
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "movingpunctuation",
                textclass: "contenttitle punctuationinfo",
                textdata: data.string.p1text11
            }
        ]

    },
    //slid8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgdiy",
        uppertextblockadditionalclass: "diy",
        uppertextblock: [
            {
                textclass: "diy",
                textdata: data.string.diy
            }

        ]
    },
    //slide9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgquery",
        uppertextblockadditionalclass:"questiontitle",
        uppertextblock: [
            {
                textclass: "contentopic query",
                textdata: data.string.p1query
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    extradiv:"extradiv",
                    imgdiv: " images diyimgopt",
                    imgclass: "diyimgoptimg im1",
                    imgid: 'diyimgoptImg',
                    imgsrc: ""
                },
            ]
        }],
        optionblock: [
            {
                outerdiv: "outerdiv",
                textblock: [
                    {
                        divclass: "draggable opt1",
                        textclass: "opt11img",
                        textdata: data.string.alpabiramsign
                    },
                    {
                        divclass: "draggable opt2 correct1",
                        textclass: "opt2img",
                        textdata: data.string.purnabiramsign
                    },
                    {
                        divclass: "draggable opt3",
                        textclass: "opt3img",
                        textdata: data.string.bismayasign
                    },
                    {
                        divclass: "draggable opt4",
                        textclass: "opt4img",
                        textdata: data.string.udaranopn
                    },
                    {
                        divclass: "draggable opt5",
                        textclass: "opt5img",
                        textdata: data.string.udarancls
                    },
                    {
                        divclass: "draggable opt6",
                        textclass: "opt6img",
                        textdata: data.string.prasnasign
                    }
                ]
            }
            ],
        lowertext:[
            {
                textclass: "contentopic subquery",
                textdata: data.string.p1query1
            },
            {
                textclass: "droppable dropbox dropbox1",
                textdata: ""
            }

        ]
    },
    //slide10
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgquery",
        uppertextblockadditionalclass:"questiontitle",
        uppertextblock: [
            {
                textclass: "contentopic query",
                textdata: data.string.p1query
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    extradiv:"extradiv",
                    imgdiv: "images diyimgopt",
                    imgclass: "diyimgoptimg im2",
                    imgid: 'diyimgoptImg1',
                    imgsrc: ""
                },
            ]
        }],
        optionblock: [
            {
                outerdiv: "outerdiv",
                textblock: [
                    {
                        divclass: "draggable opt1",
                        textclass: "opt11img",
                        textdata: data.string.alpabiramsign
                    },
                    {
                        divclass: "draggable opt2 correct1",
                        textclass: "opt2img",
                        textdata: data.string.purnabiramsign
                    },
                    {
                        divclass: "draggable opt3",
                        textclass: "opt3img",
                        textdata: data.string.bismayasign
                    },
                    {
                        divclass: "draggable opt4",
                        textclass: "opt4img",
                        textdata: data.string.udaranopn
                    },
                    {
                        divclass: "draggable opt5",
                        textclass: "opt5img",
                        textdata: data.string.udarancls
                    },
                    {
                        divclass: "draggable opt6",
                        textclass: "opt6img",
                        textdata: data.string.prasnasign
                    }
                ]
            }
        ],
        lowertext:[
            {
                textclass: "contentopic subquery",
                textdata: data.string.p1query2
            },
            {
                textclass: "droppable dropbox dropbox1",
                textdata: ""
            }

        ]
    },
    //slide11
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgquery",
        uppertextblockadditionalclass:"questiontitle",
        uppertextblock: [
            {
                textclass: "contentopic query",
                textdata: data.string.p1query
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    extradiv:"extradiv",
                    imgdiv: "images diyimgopt",
                    imgclass: "diyimgoptimg im3",
                    imgid: 'diyimgoptImg2',
                    imgsrc: ""
                },
            ]
        }],
        optionblock: [
            {
                outerdiv: "outerdiv",
                textblock: [
                    {
                        divclass: "draggable opt1",
                        textclass: "opt11img",
                        textdata: data.string.alpabiramsign
                    },
                    {
                        divclass: "draggable opt2 correct1",
                        textclass: "opt2img",
                        textdata: data.string.purnabiramsign
                    },
                    {
                        divclass: "draggable opt3",
                        textclass: "opt3img",
                        textdata: data.string.bismayasign
                    },
                    {
                        divclass: "draggable opt4",
                        textclass: "opt4img",
                        textdata: data.string.udaranopn
                    },
                    {
                        divclass: "draggable opt5",
                        textclass: "opt5img",
                        textdata: data.string.udarancls
                    },
                    {
                        divclass: "draggable opt6",
                        textclass: "opt6img",
                        textdata: data.string.prasnasign
                    }
                ]
            }
        ],
        lowertext:[
            {
                textclass: "contentopic subquery",
                textdata: data.string.p1query3
            },
            {
                textclass: "droppable dropbox dropbox1",
                textdata: ""
            }

        ]
    },
    //slide12
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgquery",
        uppertextblockadditionalclass:"questiontitle1",
        uppertextblock: [
            {
                textclass: "contentopic query1",
                textdata: data.string.p1query4
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    extradiv:"extradiv1",
                    imgdiv: "images diyimgopt",
                    imgclass: "diyimgoptimg1 im1",
                    imgid: 'diyimgoptImg',
                    imgsrc: ""
                },
                {
                    extradiv:"extradiv2",
                    imgdiv: "images diyimgopt",
                    imgclass: "diyimgoptimg2 im2",
                    imgid: 'diyimgoptImg1',
                    imgsrc: ""
                },
                {
                    extradiv:"extradiv3",
                    imgdiv: "images diyimgopt",
                    imgclass: "diyimgoptimg3 im3",
                    imgid: 'diyimgoptImg2',
                    imgsrc: ""
                }
            ]
        }],
        lowertext:[
            {
                textclass: "contentopic subquery1",
                textdata: data.string.p1query5
            },
            {
                textclass: "contentopic subquery2",
                textdata: data.string.p1query6
            },
            {
                textclass: "contentopic subquery3",
                textdata: data.string.p1query7
            }
        ]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "common-css", src: $ref + "css/common.css", type: createjs.AbstractLoader.CSS},
            {id: "page1-css", src: $ref + "css/page1.css", type: createjs.AbstractLoader.CSS},

            {id: "jaldeviImg", src: imgpath+"jal-devi.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialogboxImg", src: imgpath+"text_box01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialogbox1Img", src: imgpath+"text_box02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "woodcutterImg", src: imgpath+"woodcutter.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bubbleImg", src: imgpath+"circle01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img1Img", src: imgpath+"symbols/01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img2Img", src: imgpath+"symbols/02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img3Img", src: imgpath+"symbols/03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img4Img", src: imgpath+"symbols/04.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img5Img", src: imgpath+"symbols/05.png", type: createjs.AbstractLoader.IMAGE},
            {id: "cyclinggirlImg", src: imgpath+"girl_cycling.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "cyclinggirlImg1", src: imgpath+"cycling01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "eatinggrassImg", src: imgpath+"eating-grass.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "diyimgoptImg", src: imgpath+"boy.png", type: createjs.AbstractLoader.IMAGE},
            {id: "diyimgoptImg1", src: imgpath+"sun.png", type: createjs.AbstractLoader.IMAGE},
            {id: "diyimgoptImg2", src: imgpath+"dog.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_1", src: soundAsset+"p1_s0.ogg"},
            {id: "sound_1_0", src: soundAsset+"p1_s1_0.ogg"},
            {id: "sound_1_1", src: soundAsset+"p1_s1_1.ogg"},
            {id: "sound_2", src: soundAsset+"p1_s2.ogg"},
            {id: "sound_3", src: soundAsset+"p1_s3.ogg"},
            {id: "sound_5", src: soundAsset+"p1_s5.ogg"},
            {id: "sound_6_0", src: soundAsset+"p1_s6_0.ogg"},
            {id: "sound_6_1", src: soundAsset+"p1_s6_1.ogg"},
            {id: "sound_7", src: soundAsset+"p1_s7.ogg"},
            {id: "sound_8", src: soundAsset+"p1_s8.ogg"},
            {id: "sound_9", src: soundAsset+"p1_s9.ogg"},
            {id: "sound_12", src: soundAsset+"p1_s12.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        switch(countNext){
            case 0:
                sound_player("sound_1",true);
                break;
            case 1:
                    sound_player("sound_1_0",false);
                    setTimeout(function(){
                        sound_player("sound_1_1",true);
                    },2200)
                break;
            case 2:
                sound_player("sound_2",true);
                break;
            case 3:
                $(".p1").addClass("scaleup");
                $(".p1").css("color","#A61C00");
                sound_player("sound_3",true);
                break;
            case 4:
                bubleup($(".images").not('.img1div').not(".buble1div"),countNext,$total_page);
                break;
            case 5:
                sound_player("sound_5",true);
                break;
            case 6:
                animatepurna();
                break;
            case 7:
                animatepurna1();
                break;
            case 8:
                sound_player("sound_8",true);
                break;
            case 9:
                checkans(data.string.purnabiramsign,countNext,$total_page);
                setTimeout(function(){
                 sound_player("sound_9",false)
                },2000);
                break;
            case 10:
                $(".subquery").css({"font-size":"1.6vw","text-align":"center","width":"25%","right":"70%"});
                $(".dropbox1").css({"left":"30%"});
                checkans(data.string.purnabiramsign,countNext,$total_page);
                break;
            case 11:
                $(".subquery").css({"font-size":"1.8vw","text-align":"center","width":"25%","right":"70%"});
                $(".dropbox1").css({"left":"30%"});
                checkans(data.string.purnabiramsign,countNext,$total_page);
                break;
            case 12:
                sound_player("sound_12",true);
                break;
            default:
                navigationcontroller(countNext,$total_page);
                break;
        }
    }



    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller(countNext,$total_page):"";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function animatepurna(){
        $(".punctuationsymbol").addClass("translatepunctuation");
        $(".punctuationtitle").addClass("translatepunctuationtext");
        $(".cyclinggirl,.road,.punctuationinfo,.pushsymbolright,.cyclinggirl1").hide(0);
        sound_player("sound_6_0",false);
        setTimeout(function(){
            $(".cyclinggirl,.road,.punctuationinfo").show(0).addClass("fadeInEffect");
            setTimeout(function(){
                $(".cyclinggirl").hide(0);
                $(".cyclinggirl1").show(0);
                sound_player("sound_6_1",false);
                setTimeout(function (){
                    $(".pushsymbolright").show(0);
                    setTimeout(function(){
                        navigationcontroller(countNext,$total_page);
                    },1000);
                },1800);
            },3000);
        },1000);
    }
    function animatepurna1(){
        $(".eatinggrass,.punctuationinfo,.pushsymbolright1").hide(0);
        setTimeout(function(){
            $(".eatinggrass,.punctuationinfo").show(0).addClass("zoomInEffect");
            setTimeout(function (){
                sound_player("sound_7",false);
                $(".pushsymbolright1").show(0);
                setTimeout(function(){
                    navigationcontroller(countNext,$total_page);
                },1000);
            },3000);
        },1000);
    }
    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }
});


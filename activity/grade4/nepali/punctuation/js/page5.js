var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "chapter lessontitle",
                textdata: data.string.lesson_title
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images buble1div",
                    imgclass: "relativecls bubble1img",
                    imgid: 'bubbleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: " images buble2div",
                    imgclass: "relativecls bubble1img",
                    imgid: 'bubbleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "images buble3div",
                    imgclass: "relativecls bubble1img",
                    imgid: 'bubbleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "images buble4div",
                    imgclass: "relativecls bubble1img",
                    imgid: 'bubbleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "images buble5div",
                    imgclass: "relativecls bubble1img",
                    imgid: 'bubbleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "images img1div",
                    imgclass: "relativecls img1img",
                    imgid: 'img1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "images img2div",
                    imgclass: "relativecls img2img",
                    imgid: 'img2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "images img3div",
                    imgclass: "relativecls img3img",
                    imgid: 'img3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "images img4div",
                    imgclass: "relativecls img4img",
                    imgid: 'img4Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "images img5div",
                    imgclass: "relativecls img5img",
                    imgid: 'img5Img',
                    imgsrc: ""
                }

            ]
        }]
    },
    //slide1
    {   contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        uppertextblockadditionalclass:"punctuationtitle",
        uppertextblock: [
            {
                textclass: "punctuationtext",
                textdata: data.string.udaran
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images punctuationsymbol",
                    imgclass: "relativecls bubble1img",
                    imgid: 'bubbleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "images punctuationsymbol",
                    imgclass: "relativecls img4img",
                    imgid: 'img4Img',
                    imgsrc: ""
                }
            ]
        }]

    },
    //slide2
    {   contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        uppertextblockadditionalclass:"punctuationtitle",
        uppertextblock: [
            {
                textclass: "punctuationtext",
                textdata: data.string.udaran
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images punctuationsymbol",
                    imgclass: "relativecls bubble1img",
                    imgid: 'bubbleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "images punctuationsymbol",
                    imgclass: "relativecls img4img",
                    imgid: 'img4Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "images  mainimage",
                    imgclass: "relativecls im3image clothesimg",
                    imgid: 'clothesImg',
                    imgsrc: ""
                }
            ]
        }],
        topicblock:[
            {
                textclass: "contentopic punctutationtopic",
                textdata: data.string.p5text1
            }
        ],
        belowtext:[

            {
                datahighlightflag: true,
                datahighlightcustomclass: "commaanim",
                textclass: "relativecls contenttitle text1",
                textdata: data.string.p5text2
            },

        ]

    },
    //slide3
    {   contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        uppertextblockadditionalclass:"punctuationtitle1",
        uppertextblock: [
            {
                textclass: "punctuationtext",
                textdata: data.string.udaran
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: " images punctuationsymbol1",
                    imgclass: "relativecls bubble1img",
                    imgid: 'bubbleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "images punctuationsymbol1",
                    imgclass: "relativecls img4img",
                    imgid: 'img4Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "images  mainimage1",
                    imgclass: "relativecls jaldeviimg",
                    imgid: 'jaldeviImg',
                    imgsrc: ""
                }
            ]
        }],
        topicblock:[
            {
                textclass: "contentopic punctutationtopic",
                textdata: data.string.p5text1
            }
        ],
        belowtext:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "commaanim1",
                textclass: "contenttitle text3",
                textdata: data.string.p5text3
            }
        ]

    },
    //slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgdiy",
        uppertextblockadditionalclass: "diy",
        uppertextblock: [
            {
                textclass: "diy",
                textdata: data.string.diy
            }

        ]
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgquery",
        uppertextblockadditionalclass:"questiontitle",
        uppertextblock: [
            {
                textclass: "contentopic query",
                textdata: data.string.p5query
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    extradiv:"extradiv",
                    imgdiv: " images diyimgopt",
                    imgclass: "diyimgoptimg im1",
                    imgid: 'diyimgoptImg',
                    imgsrc: ""
                },
            ]
        }],
        optionblock: [
            {
                outerdiv: "outerdiv",
                textblock: [
                    {
                        divclass: "draggable opt1",
                        textclass: "opt11img",
                        textdata: data.string.alpabiramsign
                    },
                    {
                        divclass: "draggable opt2",
                        textclass: "opt2img",
                        textdata: data.string.purnabiramsign
                    },
                    {
                        divclass: "draggable opt3",
                        textclass: "opt3img",
                        textdata: data.string.bismayasign
                    },
                    {
                        divclass: "draggable opt4 correct1",
                        textclass: "opt4img",
                        textdata: data.string.udaranopn
                    },
                    {
                        divclass: "draggable opt5 correct2",
                        textclass: "opt5img",
                        textdata: data.string.udarancls
                    },
                    {
                        divclass: "draggable opt6",
                        textclass: "opt6img",
                        textdata: data.string.prasnasign
                    }
                ]
            }
        ],
        lowertext:[
            {
                textclass: "contentopic subquery query1",
                textdata: data.string.p5query1
            },
            {
                textclass: "contentopic subquery query2",
                textdata: data.string.p5query2
            },
            {
                textclass: "contentopic subquery query3",
                textdata: data.string.alpabiramsign
            },
            {
                textclass: "droppable dropbox dropbox1",
                textdata: ""
            },
            {
                textclass: "droppable1 dropbox dropbox2",
                textdata: ""
            }

        ]
    },
    //slide6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgquery",
        uppertextblockadditionalclass:"questiontitle",
        uppertextblock: [
            {
                textclass: "contentopic query",
                textdata: data.string.p5query
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    extradiv:"extradiv",
                    imgdiv: " images diyimgopt",
                    imgclass: "diyimgoptimg im2",
                    imgid: 'diyimgoptImg1',
                    imgsrc: ""
                },
            ]
        }],
        optionblock: [
            {
                outerdiv: "outerdiv",
                textblock: [
                    {
                        divclass: "draggable opt1",
                        textclass: "opt11img",
                        textdata: data.string.alpabiramsign
                    },
                    {
                        divclass: "draggable opt2",
                        textclass: "opt2img",
                        textdata: data.string.purnabiramsign
                    },
                    {
                        divclass: "draggable opt3",
                        textclass: "opt3img",
                        textdata: data.string.bismayasign
                    },
                    {
                        divclass: "draggable opt4 correct1",
                        textclass: "opt4img",
                        textdata: data.string.udaranopn
                    },
                    {
                        divclass: "draggable opt5 correct2",
                        textclass: "opt5img",
                        textdata: data.string.udarancls
                    },
                    {
                        divclass: "draggable opt6",
                        textclass: "opt6img",
                        textdata: data.string.prasnasign
                    }
                ]
            }
        ],
        lowertext:[
            {
                textclass: "contentopic subquery query5",
                textdata: data.string.p5query4
            },
            {
                textclass: "contentopic subquery query6",
                textdata: data.string.p5query5
            },
            {
                textclass: "contentopic subquery query7",
                textdata: data.string.alpabiramsign
            },
            {
                textclass: "droppable dropbox dropbox1 drop1",
                textdata: ""
            },
            {
                textclass: "droppable1 dropbox dropbox2 drop2",
                textdata: ""
            }

        ]
    },
    //slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgquery",
        uppertextblockadditionalclass:"questiontitle",
        uppertextblock: [
            {
                textclass: "contentopic query",
                textdata: data.string.p5query
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    extradiv:"extradiv",
                    imgdiv: " images diyimgopt",
                    imgclass: "diyimgoptimg im3",
                    imgid: 'diyimgoptImg2',
                    imgsrc: ""
                },
            ]
        }],
        optionblock: [
            {
                outerdiv: "outerdiv",
                textblock: [
                    {
                        divclass: "draggable opt1",
                        textclass: "opt11img",
                        textdata: data.string.alpabiramsign
                    },
                    {
                        divclass: "draggable opt2",
                        textclass: "opt2img",
                        textdata: data.string.purnabiramsign
                    },
                    {
                        divclass: "draggable opt3",
                        textclass: "opt3img",
                        textdata: data.string.bismayasign
                    },
                    {
                        divclass: "draggable opt4 correct1",
                        textclass: "opt4img",
                        textdata: data.string.udaranopn
                    },
                    {
                        divclass: "draggable opt5 correct2",
                        textclass: "opt5img",
                        textdata: data.string.udarancls
                    },
                    {
                        divclass: "draggable opt6",
                        textclass: "opt6img",
                        textdata: data.string.prasnasign
                    }
                ]
            }
        ],
        lowertext:[
            {
                textclass: "contentopic subquery query9",
                textdata: data.string.p5query7
            },
            {
                textclass: "contentopic subquery query10",
                textdata: data.string.p5query8
            },
            {
                textclass: "contentopic subquery query11",
                textdata: data.string.alpabiramsign
            },
            {
                textclass: "droppable dropbox dropbox1 drop3",
                textdata: ""
            },
            {
                textclass: "droppable1 dropbox dropbox2 drop4",
                textdata: ""
            }

        ]
    },
    //slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgquery",
        uppertextblockadditionalclass:"questiontitle1",
        uppertextblock: [
            {
                textclass: "contentopic",
                textdata: data.string.p5query9
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    extradiv:"extradiv1",
                    imgdiv: "images diyimgopt",
                    imgclass: "diyimgoptimg1 im1",
                    imgid: 'diyimgoptImg',
                    imgsrc: ""
                },
                {
                    extradiv:"extradiv2",
                    imgdiv: "images diyimgopt",
                    imgclass: "diyimgoptimg2 im2",
                    imgid: 'diyimgoptImg1',
                    imgsrc: ""
                },
                {
                    extradiv:"extradiv3",
                    imgdiv: "images diyimgopt",
                    imgclass: "diyimgoptimg3 im3",
                    imgid: 'diyimgoptImg2',
                    imgsrc: ""
                }
            ]
        }],
        lowertext:[
            {
                textclass: "congrats subquery1 topcss",
                textdata: data.string.p5query10
            },
            {
                textclass: "congrats subquery2",
                textdata: data.string.p5query11
            },
            {
                textclass: "congrats subquery3",
                textdata: data.string.p5query12
            }
        ]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "common-css", src: $ref + "css/common.css", type: createjs.AbstractLoader.CSS},
            {id: "page1-css", src: $ref + "css/page1.css", type: createjs.AbstractLoader.CSS},

            {id: "bubbleImg", src: imgpath+"circle01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img1Img", src: imgpath+"symbols/01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img2Img", src: imgpath+"symbols/02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img3Img", src: imgpath+"symbols/03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img4Img", src: imgpath+"symbols/04.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img5Img", src: imgpath+"symbols/05.png", type: createjs.AbstractLoader.IMAGE},
            {id: "clothesImg", src: imgpath+"drying-clothers.png", type: createjs.AbstractLoader.IMAGE},
            {id: "jaldeviImg", src: imgpath+"jal-devi.png", type: createjs.AbstractLoader.IMAGE},
            {id: "diyimgoptImg", src: imgpath+"running.png", type: createjs.AbstractLoader.IMAGE},
            {id: "diyimgoptImg1", src: imgpath+"wakeuplate.png", type: createjs.AbstractLoader.IMAGE},
            {id: "diyimgoptImg2", src: imgpath+"clicking-picture.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_1", src: soundAsset+"p5_s0.ogg"},
            {id: "sound_2_0", src: soundAsset+"p5_s1_0.ogg"},
            {id: "sound_2_1", src: soundAsset+"p5_s1_1.ogg"},
            {id: "sound_2", src: soundAsset+"p5_s2.ogg"},
            {id: "sound_3", src: soundAsset+"p5_s3.ogg"},
            {id: "sound_4", src: soundAsset+"p5_s4.ogg"},
            {id: "sound_5", src: soundAsset+"p5_s5.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        switch(countNext){
            case 0:
                bubleup($(".images").not('.img4div').not(".buble4div"),countNext,$total_page);
                break;
            case 1:
                sound_player("sound_1",true);
                break;
            case 2:
                animateclothes();
                break;
            case 3:
                animateclothes1();
                break;
            case 4:
                sound_player("sound_3",true);
                break;
            case 5:
                checkans3(data.string.udaranopn,data.string.udarancls,countNext,$total_page);
                setTimeout(function(){
                    sound_player("sound_4",false);
                },2000);
                break;
            case 6:
                checkans3(data.string.udaranopn,data.string.udarancls,countNext,$total_page);
                break;
            case 7:
                checkans3(data.string.udaranopn,data.string.udarancls,countNext,$total_page);
                break;
            case 8:
                sound_player("sound_5",true);
                break;
            default:
                navigationcontroller(countNext,$total_page);
                break;
        }
    }



    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller(countNext,$total_page):"";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });
    function animateclothes(){
        $(".punctuationsymbol").addClass("translatepunctuation");
        $(".punctuationtitle").addClass("translatepunctuationtext");
        $(".mainimage,.text1").hide(0);
        setTimeout(function(){
            sound_player("sound_2_0",false);
            $(".mainimage,.text1").show(0).addClass("fadeInEffect");
            setTimeout(function (){
                sound_player("sound_2_1",true);
            },8500);
        },1000);
    }
    function animateclothes1(){
        $(".mainimage1,.punctuationinfo1,.text3").hide(0);
        setTimeout(function(){
            $(".mainimage1,.text3").show(0).addClass("fadeInEffect");
            setTimeout(function (){
                sound_player("sound_2",true);
            },3000);
        },1000);
    }
    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }

});



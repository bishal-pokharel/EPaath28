var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/";

var content = [
  //slide0
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg",
    uppertextblock: [
      {
        textclass: "chapter lessontitle",
        textdata: data.string.lesson_title
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgdiv: " images buble1div",
            imgclass: "relativecls bubble1img",
            imgid: "bubbleImg",
            imgsrc: ""
          },
          {
            imgdiv: " images buble2div",
            imgclass: "relativecls bubble1img",
            imgid: "bubbleImg",
            imgsrc: ""
          },
          {
            imgdiv: "images buble3div",
            imgclass: "relativecls bubble1img",
            imgid: "bubbleImg",
            imgsrc: ""
          },
          {
            imgdiv: "images buble4div",
            imgclass: "relativecls bubble1img",
            imgid: "bubbleImg",
            imgsrc: ""
          },
          {
            imgdiv: "images buble5div",
            imgclass: "relativecls bubble1img",
            imgid: "bubbleImg",
            imgsrc: ""
          },
          {
            imgdiv: "images img1div",
            imgclass: "relativecls img1img",
            imgid: "img1Img",
            imgsrc: ""
          },
          {
            imgdiv: "images img2div",
            imgclass: "relativecls img2img",
            imgid: "img2Img",
            imgsrc: ""
          },
          {
            imgdiv: "images img3div",
            imgclass: "relativecls img3img",
            imgid: "img3Img",
            imgsrc: ""
          },
          {
            imgdiv: "images img4div",
            imgclass: "relativecls img4img",
            imgid: "img4Img",
            imgsrc: ""
          },
          {
            imgdiv: "images img5div",
            imgclass: "relativecls img5img",
            imgid: "img5Img",
            imgsrc: ""
          }
        ]
      }
    ]
  },
  //slide1
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg1",
    uppertextblockadditionalclass: "punctuationtitle",
    uppertextblock: [
      {
        textclass: "punctuationtext",
        textdata: data.string.prasna
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgdiv: " images punctuationsymbol",
            imgclass: "relativecls bubble1img",
            imgid: "bubbleImg",
            imgsrc: ""
          },
          {
            imgdiv: "images punctuationsymbol",
            imgclass: "relativecls img1img",
            imgid: "img5Img",
            imgsrc: ""
          }
        ]
      }
    ]
  },
  //slide2
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg1",
    uppertextblockadditionalclass: "punctuationtitle",
    uppertextblock: [
      {
        textclass: "punctuationtext",
        textdata: data.string.prasna
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgdiv: " images punctuationsymbol",
            imgclass: "relativecls bubble1img",
            imgid: "bubbleImg",
            imgsrc: ""
          },
          {
            imgdiv: "images punctuationsymbol",
            imgclass: "relativecls img5img",
            imgid: "img5Img",
            imgsrc: ""
          },
          {
            imgdiv: "images  mainimage",
            imgclass: "relativecls khaja",
            imgid: "khajasetImg",
            imgsrc: ""
          }
        ]
      }
    ],
    lowertext: [
      {
        textclass: "contentopic punctutationtopic",
        textdata: data.string.p3text1
      },
      {
        textclass: "contenttitle punctuationinfo",
        textdata: data.string.p3text2
      },
      {
        textclass: "contenttitle pushsymbolright",
        textdata: data.string.prasnasign
      }
    ]
  },
  //slide3
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg1",
    uppertextblockadditionalclass: "punctuationtitle1",
    uppertextblock: [
      {
        textclass: "punctuationtext",
        textdata: data.string.prasna
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgdiv: " images punctuationsymbol1",
            imgclass: "relativecls bubble1img",
            imgid: "bubbleImg",
            imgsrc: ""
          },
          {
            imgdiv: "images punctuationsymbol1",
            imgclass: "relativecls img5img",
            imgid: "img5Img",
            imgsrc: ""
          },
          {
            imgdiv: "images  mainimage1",
            imgclass: "relativecls earth",
            imgid: "earthImg",
            imgsrc: ""
          }
        ]
      }
    ],
    lowertext: [
      {
        textclass: "contentopic punctutationtopic",
        textdata: data.string.p3text1
      },
      {
        textclass: "contenttitle punctuationinfo",
        textdata: data.string.p3text3
      },
      {
        textclass: "contenttitle pushsymbolright1",
        textdata: data.string.prasnasign
      }
    ]
  },
  //slide4
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bgdiy",
    uppertextblockadditionalclass: "diy",
    uppertextblock: [
      {
        textclass: "diy",
        textdata: data.string.diy
      }
    ]
  },
  //slide5
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bgquery",
    uppertextblockadditionalclass: "questiontitle",
    uppertextblock: [
      {
        textclass: "contentopic query",
        textdata: data.string.p3query
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            extradiv: "extradiv",
            imgdiv: " images diyimgopt",
            imgclass: "diyimgoptimg im1",
            imgid: "diyimgoptImg",
            imgsrc: ""
          }
        ]
      }
    ],
    optionblock: [
      {
        outerdiv: "outerdiv",
        textblock: [
          {
            divclass: "draggable opt1",
            textclass: "opt11img",
            textdata: data.string.alpabiramsign
          },
          {
            divclass: "draggable opt2",
            textclass: "opt2img",
            textdata: data.string.purnabiramsign
          },
          {
            divclass: "draggable opt3",
            textclass: "opt3img",
            textdata: data.string.bismayasign
          },
          {
            divclass: "draggable opt4",
            textclass: "opt4img",
            textdata: data.string.udaranopn
          },
          {
            divclass: "draggable opt5",
            textclass: "opt5img",
            textdata: data.string.udarancls
          },
          {
            divclass: "draggable opt6 correct1",
            textclass: "opt6img",
            textdata: data.string.prasnasign
          }
        ]
      }
    ],
    lowertext: [
      {
        textclass: "contentopic subquery",
        textdata: data.string.p3query1
      },
      {
        textclass: "droppable dropbox dropbox1",
        textdata: ""
      }
    ]
  },
  //slide6
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bgquery",
    uppertextblockadditionalclass: "questiontitle",
    uppertextblock: [
      {
        textclass: "contentopic query",
        textdata: data.string.p3query
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            extradiv: "extradiv",
            imgdiv: "images diyimgopt",
            imgclass: "diyimgoptimg im2",
            imgid: "diyimgoptImg1",
            imgsrc: ""
          }
        ]
      }
    ],
    optionblock: [
      {
        outerdiv: "outerdiv",
        textblock: [
          {
            divclass: "draggable opt1",
            textclass: "opt11img",
            textdata: data.string.alpabiramsign
          },
          {
            divclass: "draggable opt2",
            textclass: "opt2img",
            textdata: data.string.purnabiramsign
          },
          {
            divclass: "draggable opt3",
            textclass: "opt3img",
            textdata: data.string.bismayasign
          },
          {
            divclass: "draggable opt4",
            textclass: "opt4img",
            textdata: data.string.udaranopn
          },
          {
            divclass: "draggable opt5",
            textclass: "opt5img",
            textdata: data.string.udarancls
          },
          {
            divclass: "draggable opt6 correct1",
            textclass: "opt6img",
            textdata: data.string.prasnasign
          }
        ]
      }
    ],
    lowertext: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "movingpunctuation",
        textclass: "contentopic subquery font",
        textdata: data.string.p3query2
      },
      {
        textclass: "droppable dropbox dropbox1",
        textdata: ""
      }
    ]
  },
  //slide 7
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bgquery",
    uppertextblockadditionalclass: "questiontitle",
    uppertextblock: [
      {
        textclass: "contentopic query",
        textdata: data.string.p3query
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            extradiv: "extradiv",
            imgdiv: "images diyimgopt",
            imgclass: "diyimgoptimg im2",
            imgid: "diyimgoptImg2",
            imgsrc: ""
          }
        ]
      }
    ],
    optionblock: [
      {
        outerdiv: "outerdiv",
        textblock: [
          {
            divclass: "draggable opt1",
            textclass: "opt11img",
            textdata: data.string.alpabiramsign
          },
          {
            divclass: "draggable opt2",
            textclass: "opt2img",
            textdata: data.string.purnabiramsign
          },
          {
            divclass: "draggable opt3",
            textclass: "opt3img",
            textdata: data.string.bismayasign
          },
          {
            divclass: "draggable opt4",
            textclass: "opt4img",
            textdata: data.string.udaranopn
          },
          {
            divclass: "draggable opt5",
            textclass: "opt5img",
            textdata: data.string.udarancls
          },
          {
            divclass: "draggable opt6 correct1",
            textclass: "opt6img",
            textdata: data.string.prasnasign
          }
        ]
      }
    ],
    lowertext: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "movingpunctuation",
        textclass: "contentopic subquery font",
        textdata: data.string.p3query3
      },
      {
        textclass: "droppable dropbox dropbox1",
        textdata: ""
      }
    ]
  },
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bgquery",
    uppertextblockadditionalclass: "questiontitle1",
    uppertextblock: [
      {
        textclass: "contentopic query1",
        textdata: data.string.p3query4
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            extradiv: "extradiv1",
            imgdiv: "images diyimgopt",
            imgclass: "diyimgoptimg1 im1",
            imgid: "diyimgoptImg",
            imgsrc: ""
          },
          {
            extradiv: "extradiv2",
            imgdiv: "images diyimgopt",
            imgclass: "diyimgoptimg2 im2",
            imgid: "diyimgoptImg1",
            imgsrc: ""
          },
          {
            extradiv: "extradiv3",
            imgdiv: "images diyimgopt",
            imgclass: "diyimgoptimg3 im3",
            imgid: "diyimgoptImg2",
            imgsrc: ""
          }
        ]
      }
    ],
    lowertext: [
      {
        textclass: "contentopic subquery1",
        textdata: data.string.p3query5
      },
      {
        textclass: "contentopic subquery2",
        textdata: data.string.p3query6
      },
      {
        textclass: "contentopic subquery3",
        textdata: data.string.p3query7
      }
    ]
  }
];

$(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;
  var count = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page, countNext + 1);

  var preload;
  var timeoutvar = null;
  var current_sound;
  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();
  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      {
        id: "common-css",
        src: $ref + "css/common.css",
        type: createjs.AbstractLoader.CSS
      },
      {
        id: "page1-css",
        src: $ref + "css/page1.css",
        type: createjs.AbstractLoader.CSS
      },

      {
        id: "bubbleImg",
        src: imgpath + "circle01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "img1Img",
        src: imgpath + "symbols/01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "img2Img",
        src: imgpath + "symbols/02.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "img3Img",
        src: imgpath + "symbols/03.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "img4Img",
        src: imgpath + "symbols/04.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "img5Img",
        src: imgpath + "symbols/05.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "khajasetImg",
        src: imgpath + "khajaset01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "earthImg",
        src: imgpath + "earth_rotation.gif",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "diyimgoptImg",
        src: imgpath + "cat.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "diyimgoptImg1",
        src: imgpath + "dog.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "diyimgoptImg2",
        src: imgpath + "toy.png",
        type: createjs.AbstractLoader.IMAGE
      },

      // sounds
      { id: "sound_1", src: soundAsset + "p2_s1.ogg" },
      { id: "sound_2_0", src: soundAsset + "p2_s2_0.ogg" },
      { id: "sound_2_1", src: soundAsset + "p2_s2_1.ogg" },
      { id: "sound_3", src: soundAsset + "p2_s3.ogg" },
      { id: "sound_4", src: soundAsset + "p2_s4.ogg" },
      { id: "sound_5", src: soundAsset + "p2_s5.ogg" },
      { id: "sound_8", src: soundAsset + "p2_s8.ogg" }
    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound); //for registering sounds
    preload.installPlugin(createjs.Sound); //for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }

  function handleFileLoad(event) {
    // console.log(event.item);
  }

  function handleProgress(event) {
    $("#loading-text").html(parseInt(event.loaded * 100) + "%");
  }

  function handleComplete(event) {
    $("#loading-wrapper").hide(0);
    //initialize varibales
    current_sound = createjs.Sound.play("sound_1");
    current_sound.stop();
    // call main function
    templateCaller();
  }

  //initialize
  init();

  /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

  /*=================================================
     =            general template function            =
     =================================================*/
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    vocabcontroller.findwords(countNext);
    put_image(content, countNext, preload);
    switch (countNext) {
      case 0:
        bubleup(
          $(".images")
            .not(".img5div")
            .not(".buble5div"),
          countNext,
          $total_page
        );
        break;
      case 1:
        sound_player("sound_1", true);
        break;
      case 2:
        animateprasna();
        break;
      case 3:
        animateprasna1();
        break;
      case 4:
        sound_player("sound_4", true);
        break;
      case 5:
        checkans(data.string.prasnasign, countNext, $total_page);
        setTimeout(function() {
          sound_player("sound_5", false);
        }, 2000);
        break;
      case 6:
        checkans(data.string.prasnasign, countNext, $total_page);
        break;
      case 7:
        checkans(data.string.prasnasign, countNext, $total_page);
        break;
      case 8:
        sound_player("sound_8", true);
        break;
      default:
        navigationcontroller(countNext, $total_page);
        break;
    }
  }

  function sound_player(sound_id, navigate) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on("complete", function() {
      navigate ? navigationcontroller(countNext, $total_page) : "";
    });
  }

  function templateCaller() {
    $prevBtn.css("display", "none");
    $nextBtn.css("display", "none");
    generaltemplate();
    loadTimelineProgress($total_page, countNext + 1);
  }

  $nextBtn.on("click", function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    switch (countNext) {
      default:
        countNext++;
        templateCaller();
        break;
    }
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    countNext--;
    templateCaller();
    /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
    countNext < $total_page
      ? ole.footerNotificationHandler.hideNotification()
      : null;
  });

  function animateprasna() {
    $(".punctuationsymbol").addClass("translatepunctuation");
    $(".punctuationtitle").addClass("translatepunctuationtext");
    $(".mainimage,.punctuationinfo,.pushsymbolright").hide(0);
    setTimeout(function() {
      $(".mainimage,.punctuationinfo")
        .show(0)
        .addClass("zoomInEffect");
      sound_player("sound_2_0", false);
      setTimeout(function() {
        sound_player("sound_2_1", true);
        $(".pushsymbolright").show(0);
      }, 5000);
    }, 1000);
  }
  function animateprasna1() {
    $(".mainimage,.punctuationinfo,.pushsymbolright1").hide(0);
    setTimeout(function() {
      $(".mainimage,.punctuationinfo")
        .show(0)
        .addClass("zoomInEffect");
      setTimeout(function() {
        sound_player("sound_3", false);
        $(".pushsymbolright1").show(0);
        setTimeout(function() {
          navigationcontroller(countNext, $total_page);
        }, 2000);
      }, 1800);
    }, 1000);
  }
});

/*======================================================
    =            Navigation Controller Function            =
    ======================================================*/
var $nextBtn = $("#activity-page-next-btn-enabled");
var $prevBtn = $("#activity-page-prev-btn-enabled");
var $refreshBtn = $("#activity-page-refresh-btn");
function navigationcontroller(countNext, $total_page, endLesson) {
  if (countNext == 0 && $total_page != 1) {
    console.log("Hello this is");
    $nextBtn.show(0);
    $prevBtn.css("display", "none");
  } else if ($total_page == 1) {
    $prevBtn.css("display", "none");
    $nextBtn.css("display", "none");

    ole.footerNotificationHandler.lessonEndSetNotification();
  } else if (countNext > 0 && countNext < $total_page - 1) {
    $nextBtn.show(0);
    $prevBtn.show(0);
  } else if (countNext == $total_page - 1) {
    $nextBtn.css("display", "none");
    $prevBtn.show(0);

    // if lastpageflag is true
    ole.footerNotificationHandler.pageEndSetNotification();
    endLesson ? ole.footerNotificationHandler.lessonEndSetNotification() : "";
  }
}

function put_image(content, count, preload) {
  var contentCount = content[count];
  var imageblockcontent = contentCount.hasOwnProperty("imageblock");
  dynamicimageload(imageblockcontent, contentCount, preload);
}

function dynamicimageload(imageblockcontent, contentCount, preload) {
  if (imageblockcontent) {
    var imageblock = contentCount.imageblock[0];
    if (imageblock.hasOwnProperty("imagestoshow")) {
      var imageClass = imageblock.imagestoshow;
      for (var i = 0; i < imageClass.length; i++) {
        var image_src = preload.getResult(imageClass[i].imgid).src;
        //get list of classes
        var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
        var selector = "." + classes_list[classes_list.length - 1];
        $(selector).attr("src", image_src);
      }
    }
  }
}

function bubleup(grayout, countNext, total_page) {
  $(".images").hide(0);
  $(".buble1div,.img1div")
    .show(0)
    .addClass("buble bub1anim");
  setTimeout(function() {
    $(".buble2div,.img2div")
      .show(0)
      .addClass("buble bub2anim");
    setTimeout(function() {
      $(".buble3div,.img3div")
        .show(0)
        .addClass("buble bub3anim");
      setTimeout(function() {
        $(".buble4div,.img4div")
          .show(0)
          .addClass("buble bub4anim");
        setTimeout(function() {
          $(".buble5div,.img5div")
            .show(0)
            .addClass("buble bub5anim");
          setTimeout(function() {
            grayout.addClass("grayout");
            navigationcontroller(countNext, total_page);
          }, 2000);
        }, 1000);
      }, 1000);
    }, 1000);
  }, 1000);
}
function shufflehint(correcttxt, correcttxt2) {
  var optdiv = $(".outerdiv");

  for (var i = optdiv.children().length; i >= 0; i--) {
    optdiv.append(optdiv.children().eq((Math.random() * i) | 0));
  }
  optdiv.children().removeClass();
  var optionclass = [
    "draggable opt1",
    "draggable opt2",
    "draggable opt3",
    "draggable opt4",
    "draggable opt5",
    "draggable opt6"
  ];
  optdiv.children().each(function(index) {
    $(this).addClass(optionclass[index]);
    if (
      $(this)
        .text()
        .trim() == correcttxt
    ) {
      $(this).addClass("correct1");
    } else if (
      $(this)
        .text()
        .trim() == correcttxt2
    ) {
      $(this).addClass("correct2");
    }
  });
}

function checkans(correctans1, countNext, totalpage) {
  $(".subquery,.dropbox").hide(0);
  var flipimgsrc = $(".extradiv")
    .find("img")
    .attr("src");
  $(".extradiv")
    .find("img")
    .attr("src", "");
  setTimeout(function() {
    $(".extradiv").addClass("flipdiv");
    setTimeout(function() {
      $(".diyimgoptimg").attr("src", flipimgsrc);
      $(".subquery,.dropbox").show(0);
    }, 700);
  }, 500);
  shufflehint(correctans1);
  dragdrop1(correctans1, countNext, totalpage);
}
function checkans1(correctans1, countNext, totalpage) {
  $(".subquery,.dropbox,.dropbox1").hide(0);
  var flipimgsrc = $(".extradiv")
    .find("img")
    .attr("src");
  $(".extradiv")
    .find("img")
    .attr("src", "");
  setTimeout(function() {
    $(".extradiv").addClass("flipdiv");
    setTimeout(function() {
      $(".diyimgoptimg").attr("src", flipimgsrc);
      $(".subquery,.dropbox,.dropbox1").show(0);
    }, 700);
  }, 500);
  shufflehint(correctans1);
  dragdrop2(correctans1, countNext, totalpage);
}
function checkans3(correctans1, correctans2, countNext, totalpage) {
  $(".subquery,.dropbox1,.dropbox2").hide(0);
  var flipimgsrc = $(".extradiv")
    .find("img")
    .attr("src");
  $(".extradiv")
    .find("img")
    .attr("src", "");
  setTimeout(function() {
    $(".extradiv").addClass("flipdiv");
    setTimeout(function() {
      $(".diyimgoptimg").attr("src", flipimgsrc);
      $(".subquery,.dropbox1,.dropbox2").show(0);
    }, 700);
  }, 500);
  shufflehint(correctans1, correctans2);
  dragdrop3(correctans1, correctans2, countNext, totalpage);
}
function dragdrop1(correctans1, countNext, totalpage) {
  $(".draggable").draggable({
    containment: "body",
    revert: true,
    appendTo: "body",
    zindex: 1000
  });
  $(".droppable").droppable({
    accept: ".draggable",
    hoverClass: "hovered",
    drop: function(event, ui) {
      if (ui.draggable.hasClass("correct1")) {
        play_correct_incorrect_sound(1);
        ui.draggable.hide(0);
        $(".dropbox p")
          .text(correctans1)
          .addClass("correctImgptag");
        $(".dropbox").addClass("correctcss");
        $(".dropbox").prepend(
          "<img class='correctImg' src='images/right.png'/>"
        );
        $(".draggable").addClass("avoid-clicks");
        navigationcontroller(countNext, totalpage);
      } else {
        play_correct_incorrect_sound(0);
      }
    }
  });
}
function dragdrop2(correctans1, countNext, totalpage) {
  var classname = $(".correct1").attr("class");
  $(".draggable").draggable({
    containment: "body",
    revert: true,
    appendTo: "body",
    zindex: 1000
  });
  $(".droppable").droppable({
    accept: ".draggable",
    hoverClass: "hovered",
    drop: function(event, ui) {
      if (ui.draggable.hasClass("correct1")) {
        play_correct_incorrect_sound(1);
        var answeredfirst = $(".dropbox1 > p").is(":empty");
        answeredfirst
          ? ui.draggable
              .removeClass()
              .removeAttr("style")
              .addClass(classname)
          : ui.draggable.hide(0);
        $(".dropbox p")
          .text(correctans1)
          .addClass("correctImgptag");
        $(".dropbox").addClass("correctcss");
        $(".dropbox").prepend(
          "<img class='correctImg' src='images/right.png'/>"
        );
        !answeredfirst ? $(".draggable").addClass("avoid-clicks") : "";
        answeredfirst = $(".dropbox1 > p").is(":empty");
        var answeredfirst2 = $(".dropbox > p").is(":empty");
        console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        console.log(answeredfirst);
        console.log(answeredfirst2);
        if (!answeredfirst && !answeredfirst2)
          navigationcontroller(countNext, totalpage);
      } else {
        play_correct_incorrect_sound(0);
      }
    }
  });
  $(".droppable1").droppable({
    accept: ".draggable",
    hoverClass: "hovered",
    drop: function(event, ui) {
      if (ui.draggable.hasClass("correct1")) {
        play_correct_incorrect_sound(1);
        var answeredfirst = $(".dropbox > p").is(":empty");
        answeredfirst
          ? ui.draggable
              .removeClass()
              .removeAttr("style")
              .addClass(classname)
          : ui.draggable.hide(0);
        $(".dropbox1 p")
          .text(correctans1)
          .addClass("correctImgptag");
        $(".dropbox1").addClass("correctcss");
        $(".dropbox1").prepend(
          "<img class='correctImg' src='images/right.png'/>"
        );
        !answeredfirst ? $(".draggable").addClass("avoid-clicks") : "";
        answeredfirst = $(".dropbox > p").is(":empty");
        var answeredfirst2 = $(".dropbox1 > p").is(":empty");
        console.log("++++++++++++++++++++++++++");
        console.log(answeredfirst);
        console.log(answeredfirst2);
        if (!answeredfirst && !answeredfirst2)
          navigationcontroller(countNext, totalpage);
      } else {
        play_correct_incorrect_sound(0);
      }
    }
  });
}
function dragdrop3(correctans1, correctans2, countNext, totalpage) {
  $(".draggable").draggable({
    containment: "body",
    revert: true,
    appendTo: "body",
    zindex: 1000
  });
  $(".droppable").droppable({
    accept: ".draggable",
    hoverClass: "hovered",
    drop: function(event, ui) {
      if (ui.draggable.hasClass("correct1")) {
        play_correct_incorrect_sound(1);
        var answeredfirst = $(".dropbox1 > p").is(":empty");
        ui.draggable.hide(0);
        $(".dropbox1 p")
          .text(correctans1)
          .addClass("correctImgptag");
        $(".dropbox1").addClass("correctcss");
        $(".dropbox1").prepend(
          "<img class='correctImg' src='images/right.png'/>"
        );
        !answeredfirst ? $(".draggable").addClass("avoid-clicks") : "";
        answeredfirst = $(".dropbox1 > p").is(":empty");
        var answeredfirst2 = $(".dropbox2 > p").is(":empty");
        if (!answeredfirst && !answeredfirst2)
          navigationcontroller(countNext, totalpage);
      } else {
        play_correct_incorrect_sound(0);
      }
    }
  });
  $(".droppable1").droppable({
    accept: ".draggable",
    hoverClass: "hovered",
    drop: function(event, ui) {
      if (ui.draggable.hasClass("correct2")) {
        play_correct_incorrect_sound(1);
        var answeredfirst = $(".dropbox2 > p").is(":empty");
        ui.draggable.hide(0);
        $(".dropbox2 p")
          .text(correctans2)
          .addClass("correctImgptag");
        $(".dropbox2").addClass("correctcss");
        $(".dropbox2").prepend(
          "<img class='correctImg' src='images/right.png'/>"
        );
        !answeredfirst ? $(".draggable").addClass("avoid-clicks") : "";
        answeredfirst = $(".dropbox2 > p").is(":empty");
        var answeredfirst2 = $(".dropbox1 > p").is(":empty");
        if (!answeredfirst && !answeredfirst2)
          navigationcontroller(countNext, totalpage);
      } else {
        play_correct_incorrect_sound(0);
      }
    }
  });
}

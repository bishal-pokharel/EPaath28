var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide1
	{
		contentblockadditionalclass:"grayBg",
		uppertextblock:[{
			textclass:"topTxt",
			textdata:data.string.p2s1toptxt
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:"two_arow",
				imgclass:"twinArw",
				imgsrc:''
			},
			{
				imgid:"two_arow",
				imgclass:"twinArw twnsec",
				imgsrc:''
			}]
		}],
		twoboxcontainer:[
			{
				boxclass:"bigBox lbox",
				boxcontents:[
					{
						boxtextcontainerclass:"txtContainer top",
						textclass:"boxTop",
						textdata:data.string.ikar
					},
					{
						boxtextcontainerclass:"imgContainer hrswik",
					},
					{
						boxtextcontainerclass:"imgContainer drghik",
					},
					{
						boxtextcontainerclass:"txtContainer btm",
						textclass:"midtxt mtleft mtb1",
						textdata:data.string.hrsw_txt
					},
					{
						boxtextcontainerclass:"txtContainer btm",
						textclass:"midtxt mtright mtb2",
						textdata:data.string.drgha_txt
					}
				]
			},
			{
				boxclass:"bigBox rbox",
				boxcontents:[
					{
						boxtextcontainerclass:"txtContainer top",
						textclass:"boxTop",
						textdata:data.string.ukar
					},
					{
						boxtextcontainerclass:"imgContainer hrswuk",
					},
					{
						boxtextcontainerclass:"imgContainer drghuk",
					},
					{
						boxtextcontainerclass:"txtContainer btm",
						textclass:"midtxt mtleft mtb1",
						textdata:data.string.hrsw_txt
					},
					{
						boxtextcontainerclass:"txtContainer btm",
						textclass:"midtxt mtright mtb2",
						textdata:data.string.drgha_txt
					}
				]
			}
		]
	},
	// slide2
	{
		contentblockadditionalclass:"grayBg",
		uppertextblock:[{
			textclass:"topTxt",
			textdata:data.string.p6s2toptxt
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:"dwn_arw",
				imgclass:"dwn_arw dwnFst",
				imgsrc:''
			},
			{
			imgid:"dwn_arw",
			imgclass:"dwn_arw dwnSec",
			imgsrc:''
		}]
		}],
		twoboxcontainer:[
			{
				boxclass:"bigBox lbox",
				boxcontents:[
					{
						boxtextcontainerclass:"txtContainer top",
						textclass:"boxTop",
						textdata:data.string.ikar
					},
					{
						boxtextcontainerclass:"imgContainer drghik",
					},
					{
						boxtextcontainerclass:"txtContainer btm",
						textclass:"midtxt mtright mtb2",
						textdata:data.string.drgha_txt
					}
				]
			},
			{
				boxclass:"bigBox rbox",
				boxcontents:[
					{
						boxtextcontainerclass:"txtContainer top",
						textclass:"boxTop",
						textdata:data.string.ukar
					},
					{
						boxtextcontainerclass:"imgContainer drghuk",
					},
					{
						boxtextcontainerclass:"txtContainer btm",
						textclass:"midtxt mtright mtb2",
						textdata:data.string.drgha_txt
					}
				]
			}
		]
	},
	// slide3
	{
		dragdiv:true,
		contentblockadditionalclass:"grayBg",
		extratext:[
		{
			textclass:"topBoxtxt",
			textdata:data.string.p3_s4_top
		}],
		dropdivblock:[
			{
				imageblock:[
					{
						dgfullblockclass:'block blk1',
						imagestoshow:[{
							imgclass:'blkimg bi1',
							imgid:"didi",
							imgsrc:""
						}],
						droppableclass:'droppable',
						textclass:"corAns",
						dataanswer:'sis',
					},
					{
						dgfullblockclass:'block blk2',
						imagestoshow:[{
							imgclass:'blkimg bi2',
							imgid:"buffalo",
							imgsrc:""
						}],
						droppableclass:'droppable',
						textclass:"corAns",
						dataanswer:'buf',
					},
					{
						dgfullblockclass:'block blk3',
						imagestoshow:[{
							imgclass:'blkimg bi3',
							imgid:"cow",
							imgsrc:""
						}],
						droppableclass:'droppable',
						textclass:"corAns",
						dataanswer:'cow',
					}
				],
			},
		],
		dragdiv:[{
			draggable:[
				{
					textclass:"dragable dragTxt1",
					textdata:data.string.p3_q1_op1,
					dataanswer:'sis'
				},
				{
					textclass:"dragable dragTxt2",
					textdata:data.string.p3_q1_op2,
					dataanswer:'buf'
				},
				{
					textclass:"dragable dragTxt3",
					textdata:data.string.p3_q1_op3,
					dataanswer:'cow'
				}
			]
		}]
	},
	// slide4
	{
		dragdiv:true,
		contentblockadditionalclass:"grayBg",
		dropdivblock:[
			{
				imageblock:[
					{
						dgfullblockclass:'block blk1',
						imagestoshow:[{
							imgclass:'blkimg bi1',
							imgid:"didi",
							imgsrc:""
						}],
						droppableclass:'imgName',
						textdata:data.string.p3_q1_op1
					},
					{
						dgfullblockclass:'block blk2',
						imagestoshow:[{
							imgclass:'blkimg bi2',
							imgid:"buffalo",
							imgsrc:""
						}],
						droppableclass:'imgName',
						textdata:data.string.p3_q1_op2
					},
					{
						dgfullblockclass:'block blk3',
						imagestoshow:[{
							imgclass:'blkimg bi3',
							imgid:"cow",
							imgsrc:""
						}],
						droppableclass:'imgName',
						textdata:data.string.p3_q1_op3
					}]
			}],
		extratext:[
			{
				textclass:"btmBoxtxt",
				textdata:data.string.p3_s4_btm
			}]
	},
	// slide5
	{
		contentblockadditionalclass:'grayBg',
		bigbox:[{
			textbox:[
				{
					datahighlightflag:'true',
					datahighlightcustomclass:'serialtxt',
					textboxclass:"midbox",
					textclass:'boxtext',
					textdata:data.string.p3_s5
				}
			]
		}]
	},
	// slide6
	{
		contentblockadditionalclass:'grayBg',
		bigbox:[{
			textbox:[
				{
					datahighlightflag:'true',
					datahighlightcustomclass:"blueHighlightTXt",
					textboxclass:"midbox",
					textclass:'boxtext twoVw',
					textdata:data.string.p3_s6
				},
				{
					textboxclass:"sideOrgBox",
					textclass:'boxtext',
					textdata:data.string.p3s8
			}]
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass:'arrow arup',
					imgid:"arw_up",
					imgsrc:""
				},{
					imgclass:'arrow arst',
					imgid:"arw_st",
					imgsrc:""
				},{
					imgclass:'arrow ardn',
					imgid:"arw_dn",
					imgsrc:""
				}
			]
		}]
	},
	// slide7
	{
		contentblockadditionalclass:'grayBg',
		bigbox:[{
			textbox:[
				{
					datahighlightflag:'true',
					datahighlightcustomclass:"hlgtTxt",
					textboxclass:"midbox",
					textclass:'boxtext twoVw',
					textdata:data.string.p3_s6
				},
				{
					textboxclass:"sideOrgBox",
					textclass:'boxtext',
					textdata:data.string.p3s8
				},
				{
					textboxclass:"btmTxtBox",
					textclass:'boxtext',
					textdata:data.string.p6s7_btm_txt
				}
			]
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass:'arrow arup',
					imgid:"arw_up",
					imgsrc:""
				},{
					imgclass:'arrow arst',
					imgid:"arw_st",
					imgsrc:""
				},{
					imgclass:'arrow ardn',
					imgid:"arw_dn",
					imgsrc:""
				}
			]
		}]
	},
	// slide8
	{
		contentblockadditionalclass:'grayBg',
		exerciseblock:[
			{
				optionsdivclass:"optionsdiv topFifty",
				ques_class:"topqn",
				textdata:data.string.p3q2,
				exeoptions:[
					{
						ans:"यी"
					},
					{
						optaddclass:'transparent',
						optdata:data.string.ki_txt
					},
					{
					},
					{
						optaddclass:'transparent',
						optdata:data.string.qn_mark
					},
				]

			}]
	},
	// slide9
	{
		contentblockadditionalclass:'grayBg',
		exerciseblock:[
			{
				optionsdivclass:"optionsdiv topFifty",
				ques_class:"topqn",
				textdata:data.string.p3q2,
				exeoptions:[
					{
						ans:"आफू"
					},
					{
						optaddclass:'transparent',
						optdata:data.string.ki_txt
					},
					{
					},
					{
						optaddclass:'transparent',
						optdata:data.string.qn_mark
					},
				]

			}]
	},
	// slide10
	{
		contentblockadditionalclass:'grayBg',
		exerciseblock:[
			{
				optionsdivclass:"optionsdiv topFifty",
				ques_class:"topqn",
				textdata:data.string.p3q2,
				exeoptions:[
					{
						ans:"तपाईँ"
					},
					{
						optaddclass:'transparent',
						optdata:data.string.ki_txt
					},
					{
					},
					{
						optaddclass:'transparent',
						optdata:data.string.qn_mark
					},
				]

			}]
	},
	// slide11
	{
		dragdiv:true,
		dropdivblock:[{
			imageblock:[
			{
				dgfullblockclass:"lastImgTxt lit1",
				imagestoshow:[
					{
						imgclass:'blkimg bi1',
						imgid:"poojari",
						imgsrc:""
					}],
				droppableclass:'imgName',
				textdata:data.string.pujari
			},
			{
				dgfullblockclass:"lastImgTxt lit2",
				imagestoshow:[
					{
						imgclass:'blkimg bi2',
						imgid:"dakarmi",
						imgsrc:""
					}],
				droppableclass:'imgName',
				textdata:data.string.dakarmi
			}]
		}],
		extratext:[
			{
			textclass:"topBoxtxt boxtext smallFnt",
			textdata:data.string.p3s15_txt
			},
			{
			textclass:"btmBoxtxt smallFnt",
			textdata:data.string.p3s16_txt
		}]
	},
	// slide12
	{
		contentblockadditionalclass:'grayBg',
		uppertextblock:[{
			textclass:"topTxt",
			textdata:data.string.s3_last_top_txt
		}],
		threebox:[
			{
				boxclass:"box fst",
				textbox:[
					{
						textclass:'boxTopTxt',
						textdata:data.string.s3_fst_box_1
					},
					{
						textclass:'undLine',
					},
					{
						textclass:'boxbtmTxt',
						textdata:data.string.s3_fst_box_2
				}]
			},
			{
				boxclass:"box sec",
				textbox:[
					{
						textclass:'boxTopTxt',
						textdata:data.string.s3_sec_box_1
					},
					{
						textclass:'undLine',
					},
					{
						textclass:'boxbtmTxt',
						textdata:data.string.s3_sec_box_2
				}]
			},
			{
				boxclass:"box third",
				textbox:[
					{
						textclass:'boxTopTxt',
						textdata:data.string.s3_third_box_1
					},
					{
						textclass:'undLine',
					},
					{
						textclass:'boxbtmTxt',
						textdata:data.string.s3_third_box_2
				}]
			}
		]
	},
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	var cor_count = 0;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "coverpage",    src: imgpath+"1.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "buffalo",    src: imgpath+"buffalo.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cow",    src: imgpath+"cow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dakarmi",    src: imgpath+"dakarmi.png", type: createjs.AbstractLoader.IMAGE},
			{id: "didi",    src: imgpath+"didi.png", type: createjs.AbstractLoader.IMAGE},
			{id: "poojari",    src: imgpath+"poojari.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arw_st",    src: imgpath+"arrow04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arw_st",    src: imgpath+"arrow04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arw_up",    src: imgpath+"arrow03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arw_dn",    src: imgpath+"arrow05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "poojari",    src: imgpath+"poojari.png", type: createjs.AbstractLoader.IMAGE},
			{id: "two_arow",    src: imgpath+"twodownarrow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dwn_arw",    src: imgpath+"onedownarrow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dakarmi",    src: imgpath+"dakarmi.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hrsw",    src: imgpath+"e.png", type: createjs.AbstractLoader.IMAGE},
			{id: "drgha",    src: imgpath+"ee.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hrs_uk",    src: imgpath+"ue.png", type: createjs.AbstractLoader.IMAGE},
			{id: "drgha_uk",    src: imgpath+"uee.png", type: createjs.AbstractLoader.IMAGE},

			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s6_p1", src: soundAsset+"s6_p1.ogg"},
			{id: "s6_p2", src: soundAsset+"s6_p2.ogg"},
			{id: "s6_p3", src: soundAsset+"s6_p3.ogg"},
			{id: "s6_p4", src: soundAsset+"s6_p4.ogg"},
			{id: "s6_p6", src: soundAsset+"s6_p6.ogg"},
			{id: "s6_p7", src: soundAsset+"s6_p7.ogg"},
			{id: "s6_p8", src: soundAsset+"s6_p8.ogg"},
			{id: "s6_p11", src: soundAsset+"s6_p11.ogg"},
			{id: "s6_p12", src: soundAsset+"s6_p12.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	// $nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		// put_speechbox_image(content, countNext);
		content[countNext].dragdiv?put_image_sec(content, countNext):"";
		$(".transparent").removeClass("forhover");
		var num1 = 0;
		var num = Math.floor(Math.random() * (2 - 0 +1)) + 0;
		num == 1?num=2:num;
		num == 0?num1=2:num1;
		switch(countNext){
			case 0:
			sound_player("s6_p"+(countNext+1),1);
			$(".hrswik").append("<img class='sign' src='"+preload.getResult("hrsw").src+"'/>");
			$(".drghik").append("<img class='sign' src='"+preload.getResult("drgha").src+"'/>");
			$(".hrswuk").append("<img class='sign' src='"+preload.getResult("hrs_uk").src+"'/>");
			$(".drghuk").append("<img class='sign' src='"+preload.getResult("drgha_uk").src+"'/>");
			break;
			case 1:
			$(".lbox, .rbox, .dwnFst, .dwnSec").hide();
			$(".drghik").append("<img class='sign' src='"+preload.getResult("drgha").src+"'/>");
			$(".drghuk").append("<img class='sign' src='"+preload.getResult("drgha_uk").src+"'/>");
				sound_player("s6_p"+(countNext+1),1);
				$(".lbox, .dwnFst").fadeIn(500);
				$(".rbox, .dwnSec").delay(500).fadeIn(500);
			break;
			case 2:
				sound_player("s6_p"+(countNext+1),0);
				randomize(".draggablecontainerclass");
			break;
			case 3:
				sound_player("s6_p"+(countNext+1),1);
			$(".block, .btmBoxtxt").hide();
			$(".blk1").fadeIn();
			$(".blk2").fadeIn();
			$(".blk3").fadeIn();
			$(".btmBoxtxt").fadeIn();
			break;
			case 4:
				sound_player("s6_p"+(countNext+1),1);
				$(".serialtxt:eq(0)").fadeIn(0);
				$(".serialtxt:eq(1)").fadeIn(0);
				$(".serialtxt:eq(2)").fadeIn(0);
				nav_button_controls(100);
			break;
			case 6:
				$(".btmTxtBox, .sideOrgBox, .arrow").hide(0);
				$(".hlgtTxt:eq(0)").addClass("blueHighlightTXt");
				setTimeout(function(){$(".hlgtTxt:eq(1)").delay(1000).addClass("blueHighlightTXt");},1000);
				setTimeout(function(){$(".hlgtTxt:eq(2)").delay(1000).addClass("blueHighlightTXt");},2000);
					sound_player("s6_p"+(countNext+1),1);
				$(".btmTxtBox").fadeIn(100);
				$(".sideOrgBox, .arrow").fadeIn(100);
				$(".btmTxtBox").fadeIn(100);
			break;
			case 7:
				sound_player("s6_p"+(countNext+1),0);
				var ans = $(".buttonsel").parent().find("p").attr("data-answer").toString();
				qnGenerator(".optionscontainer:eq("+num+")", ".optionscontainer:eq("+num1+")", "यी","यि");
				btnslClkSec(ans);
			break;
			case 8:
				sound_player("s6_p"+(countNext+1),0);
				var ans = $(".buttonsel").parent().find("p").attr("data-answer").toString();
				qnGenerator(".optionscontainer:eq("+num+")", ".optionscontainer:eq("+num1+")", "आफू","आफु");
				btnslClkSec(ans);
			break;
			case 9:
				sound_player("s6_p"+(countNext+1),0);
				var ans = $(".buttonsel").parent().find("p").attr("data-answer").toString();
				qnGenerator(".optionscontainer:eq("+num+")", ".optionscontainer:eq("+num1+")", "तपाईँ","तपाइ");
				btnslClkSec(ans);
			break;

			case 10:
				sound_player("s6_p"+(countNext+1),1);
				$(".lastImgTxt, .btmBoxtxt").hide(0);
				$(".lit1").delay(7000).fadeIn();
				$(".lit2").delay(8000).fadeIn();
				$(".btmBoxtxt").delay(9000).fadeIn();
			break;
			case 11:
				$(".box").hide(0);
					sound_player("s6_p"+(countNext+1),1);
					$(".fst").fadeIn();
					$(".sec").fadeIn();
					$(".third").fadeIn();
					// createjs.Sound.stop();
					// current_sound = createjs.Sound.play("sound_0");
					// current_sound.play();
					// current_sound.on('complete', function(){
					// 		createjs.Sound.stop();
					// 		current_sound_1 = createjs.Sound.play("sound_0");
					// 		current_sound_1.play();
					// 		current_sound_1.on('complete', function(){
					// 				createjs.Sound.stop();
					// 				current_sound_2 = createjs.Sound.play("sound_0");
					// 				current_sound_2.play();
					// 				current_sound_2.on('complete', function(){
					// 					nav_button_controls(0);
					// 				});
					// 		});
					// });
			break;
			default:
				sound_player("s6_p"+(countNext+1),1);
		}
		$(".dragable").draggable({
			cursor:"all-scroll",
			revert:true,
		});
		$(".droppable").droppable({
			accept:".dragable",
			hoverClass:"hovered",
			drop: function(event, ui){
				$this = $(this);
				dropfunc(event, ui, $(this), ".dragable");
			}
		});
	}

	function randomize(parent){
		var parent = $(parent);
		var divs = parent.children();
		while (divs.length) {
 			parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
	}

	function qnGenerator(divClass1,divClass2, txt1, txt2){
		var num1 = 0;
		var num = Math.floor(Math.random() * (2 - 0 +1)) + 0;
		num == 1?num=2:num;
		num == 0?num1=2:num1;
		$(divClass1).find("p").html(txt1);
		$(divClass2).find("p").html(txt2);
	}
	function btnslClkSec(ans){
		$(".buttonsel").click(function(){
			clkAns = $(this).text();
			if(ans == clkAns){
					$(this).css({
						"background":"#98C02E",
						"border":"4px solid #DEEF3C",
						"color":"#fff",
						"pointer-events":"none"
					});
					$(this).children(".corctopt").css("display","block");
					$(".buttonsel").css("pointer-events","none");
					$(".dsp").hide(0);
					$(".nodsp").show();
					play_correct_incorrect_sound(1);
					nav_button_controls(0);
						createjs.Sound.stop();
			}
			else{
					$(this).css({
						"background":"#FF0000",
						"border":"4px solid #980000",
						"color":"#000",
						"pointer-events":"none"
					});
					$(this).children(".wrngopt").css("display","block");
					play_correct_incorrect_sound(0);
						createjs.Sound.stop();
			}
		});
	}
  function appeartextonebyone(className) {
      var delaytime = 1000;
      setTimeout(function(){
          vocabcontroller.findwords(countNext);
					navigationcontroller(true);
      },(className.length+2)*1000);
      for (var i = 0; i < className.length; i++) {
          delaytime = delaytime+1000;
          $(className).eq(i).delay(delaytime).animate({"opacity":"1"},1000);

      }
  }
	function dropfunc(event, ui, dropClass, dragClass){
		var dragAns = ui.draggable.attr("data-answer").toString().trim();
		var dropAns = $(dropClass).children().attr("data-answer").toString().trim();
		// console.log("drag--"+dragAns+"\n"+" drop--"+dropAns);
		if(dragAns == dropAns){
			cor_count+=1;
			var text = ui.draggable.text();
			ui.draggable.detach();
			$(dropClass).children().append(text);
			$(dropClass).children(".wrngopt").hide(0);
			$(dropClass).children(".corctopt").show(0);
			$(dropClass).css({
				"background":"#98C02E",
				"border":"4px solid #DEEF3C"
			});
			// alert(cor_count);
			if((cor_count%3) == 0){
				nav_button_controls(0);
			}
			play_correct_incorrect_sound(1);
		}
		else{
			play_correct_incorrect_sound(0);
			$(dropClass).children(".wrngopt").show(0);
			$(dropClass).css({
				"background":"#FF0000",
				"border":"4px solid #980000"
			});
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls():'';
		});
	}

	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2);
		});

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image_sec(content, count){
		if(content[count].dropdivblock[0].hasOwnProperty('imageblock')){
			var imagblockVal = content[count].dropdivblock[0];
			for(var j=0;j<imagblockVal.imageblock.length; j++){
							var imageblock = imagblockVal.imageblock[j];
							if(imageblock.hasOwnProperty('imagestoshow')){
								var imageClass = imageblock.imagestoshow;
								for(var i=0; i<imageClass.length; i++){
									var image_src = preload.getResult(imageClass[i].imgid).src;
									//get list of classes
									// alert(image_src);
									var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
									var selector = ('.'+classes_list[classes_list.length-1]);
									$(selector).attr('src', image_src);
								}
							}
  		}

		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide1
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt top20",
		storytxt:[
			{
				textclass: "story",
				textdata: data.string.sp3s1_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'rats_on_floor_when',
					imgsrc: ""
				}
			]
		}]
	},
	// slide2
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt top20",
		storytxt:[
			{
				textclass: "story",
				textdata: data.string.sp3s1_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background leftNegHund",
					imgid : 'rats_on_floor_when',
					imgsrc: ""
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-1 sld5_sp',
			textdata : data.string.sp3s2_box_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'speechbox_fst',
			imgsrc: '',
		}]
	},
	// slide3
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt top35",
		storytxt:[
			{
				textclass: "story",
				textdata: data.string.sp3s3_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'two_rats',
					imgsrc: ""
				}
			]
		}],
		speechbox:[{
			speechbox: 'sld3_sp',
			textdata : data.string.sp3s3_box_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'speechbox_fst',
			imgsrc: '',
		}]
	},
	// slide4
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt top35",
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background zoom_in",
					imgid : 'two_rats',
					imgsrc: ""
				}
			]
		}],
		speechbox:[
		{
			speechbox: 'sld4_sp s4FstBx',
			textdata : data.string.sp3s4_box_txt_1,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'speechbox_fst',
			imgsrc: '',
		},
		{
			speechbox: 'sld4_sp s4SecBx',
			textdata : data.string.sp3s4_box_txt_2,
			textclass : 'txt1',
			imgclass: 'box rotate',
			imgid : 'speechbox_fst',
			imgsrc: '',
		}]
	},
	// slide5
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt t15",
		storytxt:[
			{
				textclass: "story",
				textdata: data.string.sp3s5_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'rats_on_floor',
					imgsrc: ""
				}
			]
		}],
	},
	// slide6
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt t15",
		storytxt:[
			{
				textclass: "story",
				textdata: data.string.sp3s6_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'flour_Knead',
					imgsrc: ""
				}
			]
		}],
		speechbox:[
		{
			speechbox: 'sld4_sp s4SpBx',
			textdata : data.string.sp3s6_box_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'speechbox_fst',
			imgsrc: '',
		}
		]
  },
	// slide7
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt t15",
		storytxt:[
			{
				textclass: "story",
				textdata: data.string.sp3s7_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'flour_Knead',
					imgsrc: ""
				}
			]
		}],
		speechbox:[
		{
			speechbox: 'sld7_sp s7FstBx',
			textdata : data.string.sp3s7_box_txt_1,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'speechbox_fst',
			imgsrc: '',
		},
		{
			speechbox: 'sld7_sp s7SecBx',
			textdata : data.string.sp3s7_box_txt_2,
			textclass : 'txt1',
			imgclass: 'box rotatex',
			imgid : 'speechbox_fst',
			imgsrc: '',
		}
	]
	},
	// slide8
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt t15",
		storytxt:[
			{
				textclass: "story",
				textdata: data.string.sp3s8_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'flour_Knead',
					imgsrc: ""
				}
			]
		}],
	},
	// slide9
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt t15",
		storytxt:[
			{
				textclass: "story",
				textdata: data.string.sp3s9_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'cook_with_fire',
					imgsrc: ""
				}
			]
		}],
	},
	// slide10
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt t15",
		storytxt:[
			{
				textclass: "story",
				textdata: data.string.sp3s10_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'cook_with_fire',
					imgsrc: ""
				}
			]
		}],
	},
	// slide11
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt t15",
		storytxt:[
			{
				textclass: "story",
				textdata: data.string.sp3s11_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background scaledBgS11",
					imgid : 'cook_with_fire',
					imgsrc: ""
				}
			]
		}],
	},
	// slide12
	{
		contentblockadditionalclass: "thebg1",
		uppertextblockadditionalclass: 'end-dialouge  end-anim wobble',
		uppertextblock:[{
			textdata: data.string.p3endtxt,
			textclass: "",
		}],
		extratextblock:[{
			textdata: '',
			textclass: "overlay-endpage",
		}],
		storytxtclass:"stryXtx fstTxt t15",
		storytxt:[
			{
				textclass: "story",
				textdata: data.string.sp3s11_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'cook_with_fire',
					imgsrc: ""
				}
			]
		}],
	},
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "first", src: imgpath+"1.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "hen_hold_maize", src: imgpath+"5.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "speechbox_fst", src: imgpath+"bubble-1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rats_on_floor", src: imgpath+"11.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "rats_on_floor_when", src: imgpath+"3.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "two_rats", src: imgpath+"10.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "flour_Knead", src: imgpath+"12.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "cook", src: imgpath+"13.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "cook_with_fire", src: imgpath+"14.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "thinkbox", src: imgpath+"thinkbox.png", type: createjs.AbstractLoader.IMAGE},
			{id: "together", src: imgpath+"together.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s3_p1", src: soundAsset+"s3_p1.ogg"},
			{id: "s3_p2_1", src: soundAsset+"s3_p2_1.ogg"},
			{id: "s3_p3", src: soundAsset+"s3_p3.ogg"},
			{id: "s3_p3_1", src: soundAsset+"s3_p3_1.ogg"},
			{id: "s3_p4", src: soundAsset+"s3_p4.ogg"},
			{id: "s3_p4_1", src: soundAsset+"s3_p4_1.ogg"},
			{id: "s3_p5", src: soundAsset+"s3_p5.ogg"},
			{id: "s3_p6", src: soundAsset+"s3_p6.ogg"},
			{id: "s3_p6_1", src: soundAsset+"s3_p6_1.ogg"},
			{id: "s3_p7", src: soundAsset+"s3_p7.ogg"},
			{id: "s3_p7_1", src: soundAsset+"s3_p7_1.ogg"},
			{id: "s3_p7_2", src: soundAsset+"s3_p7_2.ogg"},
			{id: "s3_p8", src: soundAsset+"s3_p8.ogg"},
			{id: "s3_p9", src: soundAsset+"s3_p9.ogg"},
			{id: "s3_p10", src: soundAsset+"s3_p10.ogg"},
			{id: "s3_p11", src: soundAsset+"s3_p11.ogg"},
			{id: "s3_p12", src: soundAsset+"s3_p12.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	// $nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	// islastpageflag ?
 	// ole.footerNotificationHandler.lessonEndSetNotification() :
 	// ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);

		function spchBoxAfterTxt(soundId, boxSoundId, spBoxClass){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play(soundId);
				current_sound.play();
				current_sound.on('complete', function(){
					$(spBoxClass).addClass("fadeInFast");
							createjs.Sound.stop();
							current_sound_1 = createjs.Sound.play(boxSoundId);
							current_sound_1.play();
							current_sound_1.on('complete', function(){
								navigationcontroller();
							});
				});
		}
		switch(countNext){
			case 0:
			// sound_player("s3_p"+(countNext+1),1);
				sound_player("s3_p1",1);
			break;
			case 1:
				$(".story").css("opacity","0");
				$(".leftNegHund").animate({
					left : "0%"
				},3000,function(){
					$(".story,.sp-1").addClass("fadeInFast");
					// spchBoxAfterTxt(" ", "s3_p2_1", ".sp-1");
                    sound_player("s3_p2_1",1);

                });
			break;
			case 2:
				$(".sld3_sp").css("opacity", "1");
				$(".story").css("opacity","0");
				spchBoxAfterTxt("s3_p3", "s3_p3_1", ".story");
			break
			case 3:
				setTimeout(function(){
					$(".s4FstBx").addClass("fadeInFast");createjs.Sound.stop();
					spchBoxAfterTxt("s3_p4", "s3_p4_1", ".s4SecBx");
				},2000);
			break;
			case 4:
			sound_player("s3_p5",1);
			break;
			case 5:
			spchBoxAfterTxt("s3_p6", "s3_p6_1", ".sld4_sp");
			break;
			case 6:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("s3_p7");
			current_sound.play();
			current_sound.on('complete', function(){
				$(".background").addClass("bgAnim");
				setTimeout(function(){
					$(".s7FstBx").addClass("fadeInFast");
					createjs.Sound.stop();
					current_sound_1 = createjs.Sound.play("s3_p7_1");
					current_sound_1.play();
					current_sound_1.on('complete', function(){
						$(".background").addClass("zoomOutz");
							// setTimeout(function(){
							$(".s7SecBx").addClass("fadeInFast");
								createjs.Sound.stop();
								current_sound_2 = createjs.Sound.play("s3_p7_2");
								current_sound_2.play();
								current_sound_2.on('complete', function(){
									nav_button_controls(100);
								});
							// },2000);
					});
				},2000);
			});
			// nav_button_controls(0);
			break;
			case 7:
				sound_player("s3_p"+(countNext+1),1);
			break;
			case 8:
				sound_player("s3_p"+(countNext+1),1);
			break;
			case 9:
				$('.story').css("opacity","0");
				$(".background").addClass("bgAnim");
				setTimeout(function(){
					$('.story').addClass("fadeInFast")
					sound_player("s3_p10",1);
				},2000);
			break;
			case 10:
			$(".background").addClass("zoomOutz");
				$('.story').css("opacity","0");
				setTimeout(function(){
					$('.story').addClass("fadeInFast")
					sound_player("s3_p11",1);
				},2000);
			break;
			case 11:
				sound_player("s3_p"+(countNext+1),1);
				$prevBtn.show(0);
				$('.end-dialouge').css({
					'background-image': 'url("'+ preload.getResult('thinkbox').src +'")',
					'background-size': '100% 100%',
				});
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		// alert(sound_id);
		current_sound.on('complete', function(){
			next?nav_button_controls():'';
		});
	}

	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2);
		});

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

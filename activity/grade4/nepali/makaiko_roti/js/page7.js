var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide1
	{
		contentblockadditionalclass:"grayBg",
		uppertextblock:[{
			textclass:"topTxt",
			textdata:data.string.p4_qn
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"arrow fp",
				imgid:'arrow',
				imgsrc:""
			}]
		}],
		dragdiv:[{
			draggable:[
				{
					textclass:"dragable",
					textdata:data.string.p4_qn1_op_1,
					dataanswer:"raswo"
				},
				{
					textclass:"dragable",
					textdata:data.string.p4_qn1_op_2,
					dataanswer:"dirgha"
			}]
		}],
		dropbox:[{
			dropableadnalclass:"qn1DrpBox",
			textclass:"corAns",
			dataanswer:"dirgha"
		}],
		extratextblock:[{
			datahighlightflag:'true',
			datahighlightcustomclass:"grenTxt",
			textclass:"question",
			textdata:data.string.p4_qn1
		}]
	},
	// slide2
	{
		contentblockadditionalclass:"grayBg",
		uppertextblock:[{
			textclass:"topTxt",
			textdata:data.string.p4_qn
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"arrow sp",
				imgid:'arrow',
				imgsrc:""
			}]
		}],
		dragdiv:[{
			draggable:[
				{
					textclass:"dragable",
					textdata:data.string.p4_qn1_op_1,
					dataanswer:"raswo"
				},
				{
					textclass:"dragable",
					textdata:data.string.p4_qn1_op_2,
					dataanswer:"dirgha"
			}]
		}],
		dropbox:[{
			dropableadnalclass:"qn2DrpBox",
			textclass:"corAns",
			dataanswer:"dirgha"
		}],
		extratextblock:[{
			datahighlightflag:'true',
			datahighlightcustomclass:"grenTxt",
			textclass:"question",
			textdata:data.string.p4_qn2
		}]
	},
	// slide3
	{
		contentblockadditionalclass:"grayBg",
		uppertextblock:[{
			textclass:"topTxt",
			textdata:data.string.p4_qn
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"arrow tp",
				imgid:'arrow',
				imgsrc:""
			}]
		}],
		dragdiv:[{
			draggable:[
				{
					textclass:"dragable",
					textdata:data.string.p4_qn1_op_1,
					dataanswer:"raswo"
				},
				{
					textclass:"dragable",
					textdata:data.string.p4_qn1_op_2,
					dataanswer:"dirgha"
			}]
		}],
		dropbox:[{
			dropableadnalclass:"qn3DrpBox",
			textclass:"corAns",
			dataanswer:"raswo"
		}],
		extratextblock:[{
			datahighlightflag:'true',
			datahighlightcustomclass:"grenTxt",
			textclass:"question",
			textdata:data.string.p4_qn3
		}]
	},
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "dummy", src: imgpath+"1.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "ship", src: imgpath+"2.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "onboat", src: imgpath+"3.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "dive", src: imgpath+"4.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "school", src: imgpath+"5.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "yellow", src: imgpath+"6.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "lion", src: imgpath+"7.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "complete", src: imgpath+"8.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "bg",    src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow",    src: imgpath+"arrow04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "birds",    src: imgpath+"flying-birds.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "eagle",    src: imgpath+"flying-eagle-gif", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud",    src: imgpath+"cloud03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bubbles",    src: imgpath+"water-bubbles.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "txtbox",    src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ring",    src: imgpath+"Circle-PNG-Image.png", type: createjs.AbstractLoader.IMAGE},
			{id: "coverpage",    src: imgpath+"coverpage.jpg", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s7_p1", src: soundAsset+"s7_p1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	// $nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		// put_speechbox_image(content, countNext);
		$(".transparent").removeClass("forhover");
		switch(countNext){
			case 0:
				sound_player("s7_p1",0);
			break;
			default:
			// nav_button_controls(0);
		}
		$(".dragable").draggable({
			cursor:"all-scroll",
			revert:true,
		});
		$(".droppable").droppable({
			accept:".dragable",
			hoverClass:"hovered",
			drop: function(event, ui){
				$this = $(this);
				dropfunc(event, ui, $(this), ".dragable");
			}
		});

	}
	function dropfunc(event, ui, dropClass, dragClass){
		var dragAns = ui.draggable.attr("data-answer").toString().trim();
		var dropAns = $(dropClass).children().attr("data-answer").toString().trim();
		// console.log("drag--"+dragAns+"\n"+" drop--"+dropAns);
		if(dragAns == dropAns){
			var text = ui.draggable.text();
			ui.draggable.detach();
			$(dropClass).children().append(text);
			$(dropClass).children(".corctopt").show(0);
			$(dropClass).css({
				"background":"#98C02E",
				"border":"4px solid #DEEF3C"
			});
			play_correct_incorrect_sound(1);
			nav_button_controls(0);
		}
		else{
			play_correct_incorrect_sound(0);
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?navigationcontroller():'';
		});
	}

	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2);
		});

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

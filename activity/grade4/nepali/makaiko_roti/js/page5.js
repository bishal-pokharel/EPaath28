var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide1
	{
		contentblockadditionalclass:"grayBg",
		uppertextblock:[{
			textclass:"topTxt",
			textdata:data.string.p2s1toptxt
		}],
		twoboxcontainer:[
			{
				boxclass:"bigBox lbox",
				boxcontents:[
					{
						boxtextcontainerclass:"txtContainer top",
						textclass:"boxTop",
						textdata:data.string.ikar
					},
					{
						boxtextcontainerclass:"imgContainer hrswik",
					},
					{
						boxtextcontainerclass:"imgContainer drghik",
					},
					{
						boxtextcontainerclass:"txtContainer btm",
						textclass:"midtxt mtleft mtb1",
						textdata:data.string.hrsw_txt
					},
					{
						boxtextcontainerclass:"txtContainer btm",
						textclass:"midtxt mtright mtb2",
						textdata:data.string.drgha_txt
					}
				]
			},
			{
				boxclass:"bigBox rbox",
				boxcontents:[
					{
						boxtextcontainerclass:"txtContainer top",
						textclass:"boxTop",
						textdata:data.string.ukar
					},
					{
						boxtextcontainerclass:"imgContainer hrswuk",
					},
					{
						boxtextcontainerclass:"imgContainer drghuk",
					},
					{
						boxtextcontainerclass:"txtContainer btm",
						textclass:"midtxt mtleft mtb1",
						textdata:data.string.hrsw_txt
					},
					{
						boxtextcontainerclass:"txtContainer btm",
						textclass:"midtxt mtright mtb2",
						textdata:data.string.drgha_txt
					}
				]
			}
		],
		imageblock:[{
			imagestoshow:[{
				imgid:"two_arow",
				imgclass:"twinArw",
				imgsrc:''
			},
			{
				imgid:"two_arow",
				imgclass:"twinArw twnsec",
				imgsrc:''
			}]
		}]
	},
	// slide2
	{
		contentblockadditionalclass:"grayBg",
		uppertextblock:[{
			textclass:"topTxt",
			textdata:data.string.p2s2toptxt
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:"two_arow",
				imgclass:"twinArw twnfst",
				imgsrc:''
			},
			{
				imgid:"two_arow",
				imgclass:"twinArw twnsec",
				imgsrc:''
			},{
				imgid:"dwn_arw",
				imgclass:"dwn_arw dwnfst",
				imgsrc:''
			},
			{
			imgid:"dwn_arw",
			imgclass:"dwn_arw dwnsec",
			imgsrc:''
		}]
		}],
		twoboxcontainer:[
			{
				boxclass:"bigBox lbox",
				boxcontents:[
					{
						boxtextcontainerclass:"txtContainer top",
						textclass:"boxTop",
						textdata:data.string.ikar
					},
					{
						boxtextcontainerclass:"imgContainer hrswik",
					},
					{
						boxtextcontainerclass:"imgContainer drghik",
					},
					{
						boxtextcontainerclass:"txtContainer btm",
						textclass:"midtxt mtleft mtb1",
						textdata:data.string.hrsw_txt
					},
					{
						boxtextcontainerclass:"txtContainer btm",
						textclass:"midtxt mtright mtb2",
						textdata:data.string.drgha_txt
					}
				]
			},
			{
				boxclass:"bigBox rbox",
				boxcontents:[
					{
						boxtextcontainerclass:"txtContainer top",
						textclass:"boxTop",
						textdata:data.string.ukar
					},
					{
						boxtextcontainerclass:"imgContainer hrswuk",
					},
					{
						boxtextcontainerclass:"imgContainer drghuk",
					},
					{
						boxtextcontainerclass:"txtContainer btm",
						textclass:"midtxt mtleft mtb1",
						textdata:data.string.hrsw_txt
					},
					{
						boxtextcontainerclass:"txtContainer btm",
						textclass:"midtxt mtright mtb2",
						textdata:data.string.drgha_txt
					}
				]
			},
			{
				boxclass:"bigBox lbox boxToshowlft",
				boxcontents:[
					{
						boxtextcontainerclass:"txtContainer top",
						textclass:"boxTop",
						textdata:data.string.ikar
					},
					,
					{
						boxtextcontainerclass:"imgContainer hrswik",
					},
					{
						boxtextcontainerclass:"txtContainer btm",
						textclass:"midtxt mtleft mtb1",
						textdata:data.string.hrsw_txt
					}
				]
			},
			{
				boxclass:"bigBox rbox boxToshowrght",
				boxcontents:[
					{
						boxtextcontainerclass:"txtContainer top",
						textclass:"boxTop",
						textdata:data.string.ukar
					},
					{
						boxtextcontainerclass:"imgContainer hrswuk",
					},
					{
						boxtextcontainerclass:"txtContainer btm",
						textclass:"midtxt mtleft mtb1",
						textdata:data.string.hrsw_txt
					},
				]
			}
		]
	},
	// slide3
	{
		contentblockadditionalclass:'grayBg',
		bigbox:[{
			textbox:[
				{
					datahighlightflag:'true',
					datahighlightcustomclass:'serialtxt',
					textboxclass:"midbox",
					textclass:'boxtext',
					textdata:data.string.p2s4txt
				}
			]
		}]
	},
	// slide4
	{
		contentblockadditionalclass:'grayBg',
		bigbox:[{
			textbox:[
				{
					datahighlightflag:'true',
					datahighlightcustomclass:"blueHighlightTXt",
					textboxclass:"midbox",
					textclass:'boxtext',
					textdata:data.string.p2s5txt
				},
				{
					textboxclass:"btmTxtBox",
					textclass:'boxtext',
					textdata:data.string.p2s6_btm_txt
				}
			]
		}]
	},
	// slide5 ---> arrows left
	{
		contentblockadditionalclass:'grayBg',
		bigbox:[{
			textbox:[
				{
					datahighlightflag:'true',
					datahighlightcustomclass:"hlgtTxt",
					textboxclass:"midbox",
					textclass:'boxtext',
					textdata:data.string.p2s5txt
				},
				{
					textboxclass:"sideOrgBox",
					textclass:'boxtext',
					textdata:data.string.p2s6_org_txt
				},
				{
					textboxclass:"btmTxtBox",
					textclass:'boxtext',
					textdata:data.string.p2s6_btm_txt
				}
			]
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass:'arrow arup',
					imgid:"arw_up",
					imgsrc:""
				},{
					imgclass:'arrow arst',
					imgid:"arw_st",
					imgsrc:""
				},{
					imgclass:'arrow ardn',
					imgid:"arw_dn",
					imgsrc:""
				}
			]
		}]
	},
	// slide6
	{
		contentblockadditionalclass:'grayBg',
		extratextblock:[{
			datahighlightflag:'true',
			datahighlightcustomclass:"pinkTXt",
			textclass:"ans nodsp",
			textdata:data.string.p2_mid_q1_2
		}],
		exerciseblock:[
			{
				optionsdivclass:"optionsdiv",
				ques_class:"topqn",
				textdata:data.string.p2q1,
				sent_class:'dsp',
				sentdata:data.string.p2_mid_q1,
				exeoptions:[
					{
						optaddclass:'correct',
						optdata:data.string.p2_q1_op1
					},
					{
						optdata:data.string.p2_q1_op2
					},
				]

			}]
	},
	// slide7
	{
		contentblockadditionalclass:'grayBg',
		exerciseblock:[
			{
				optionsdivclass:"optionsdiv",
				ques_class:"topqn blueHighlightTXt",
				textdata:data.string.p2q3,
				datahighlightflag:'true',
				datahighlightcustomclass:'blueHighlightTXt',
				sent_class:'',
				sentdata:data.string.p2_mid_q3,
				exeoptions:[
					{
						optaddclass:'correct',
						optdata:data.string.p2_q3_op1
					},
					{
						optdata:data.string.p2_q3_op2
					},
				]

			}]
	},
	// slide8--infoslide
	{
		contentblockadditionalclass:'grayBg',
		bigbox:[{
			textbox:[
				{
					textboxclass:"midLongBox",
					textclass:'boxtext',
					textdata:data.string.p2_s10_info
				}
			]
		}]
	},
	// slide9
	{
		contentblockadditionalclass:'grayBg',
		exerciseblock:[
			{
				optionsdivclass:"optionsdiv topFifty",
				ques_class:"topqn blueHighlightTXt",
				textdata:data.string.p2q4,
				exeoptions:[
					{
						ans:"भाइ",
					},
					{
						optaddclass:'transparent',
						optdata:data.string.ki_txt
					},
					{
					},
					{
						optaddclass:'transparent',
						optdata:data.string.qn_mark
					},
				]

			}]
	},
	// slide10
	{
		contentblockadditionalclass:'grayBg',
		exerciseblock:[
			{
				optionsdivclass:"optionsdiv topFifty",
				ques_class:"topqn blueHighlightTXt",
				textdata:data.string.p2q5,
				exeoptions:[
					{
						ans:"फुपाजु",
					},
					{
						optaddclass:'transparent',
						optdata:data.string.ki_txt
					},
					{
					},
					{
						optaddclass:'transparent',
						optdata:data.string.qn_mark
					},
				]

			}]
	},
	// slide11
	{
		contentblockadditionalclass:'grayBg',
		exerciseblock:[
			{
				optionsdivclass:"optionsdiv topFifty",
				ques_class:"topqn blueHighlightTXt",
				textdata:data.string.p2q5,
				exeoptions:[
					{
						ans:"भिनाजु"
					},
					{
						optaddclass:'transparent',
						optdata:data.string.ki_txt
					},
					{
						optdata:data.string.p2_q6_op2
					},
					{
						optaddclass:'transparent',
						optdata:data.string.qn_mark
					},
				]
			}]
	},
	// slide12--infoslide
	{
		contentblockadditionalclass:'grayBg',
		bigbox:[{
			textbox:[
				{
					textboxclass:"midLongBox",
					textclass:'boxtext',
					textdata:data.string.p2_s14_info
				}
			]
		}]
	},
	// slide13
	{
		contentblockadditionalclass:'grayBg',
		extratextblock:[{
			datahighlightflag:'true',
			datahighlightcustomclass:"pinkTXt",
			textclass:"ans nodsp",
			textdata:data.string.p2_mid_q7_1
		}],
		exerciseblock:[
			{
				optionsdivclass:"optionsdiv",
				ques_class:"topqn",
				textdata:data.string.p2q7,
				sent_class:'dsp',
				sentdata:data.string.p2_mid_q7,
				exeoptions:[
					{
						optaddclass:'correct',
						optdata:data.string.p2_q7_op1
					},
					{
						optdata:data.string.p2_q7_op2
					},
				]

			}]
	},
	// slide14
	{
		contentblockadditionalclass:'grayBg',
		extratextblock:[{
			datahighlightflag:'true',
			datahighlightcustomclass:"pinkTXt",
			textclass:"ans nodsp",
			textdata:data.string.p2_mid_q8_1
		}],
		exerciseblock:[
			{
				optionsdivclass:"optionsdiv",
				ques_class:"topqn",
				textdata:data.string.p2q8,
				sent_class:'dsp',
				sentdata:data.string.p2_mid_q8,
				exeoptions:[
					{
						optaddclass:'correct',
						optdata:data.string.p2_q8_op1
					},
					{
						optdata:data.string.p2_q8_op2
					},
				]

			}]
	},
	// slide15
	{
		contentblockadditionalclass:'grayBg',
		extratextblock:[{
			datahighlightflag:'true',
			datahighlightcustomclass:"pinkTXt",
			textclass:"ans nodsp",
			textdata:data.string.p2_mid_q9_1
		}],
		exerciseblock:[
			{
				optionsdivclass:"optionsdiv",
				ques_class:"topqn",
				textdata:data.string.p2q9,
				sent_class:'dsp',
				sentdata:data.string.p2_mid_q9,
				exeoptions:[
					{
						optaddclass:'correct',
						optdata:data.string.p2_q9_op1
					},
					{
						optdata:data.string.p2_q9_op2
					},
				]

			}]
	},
	// slide18
	{
		contentblockadditionalclass:'grayBg',
		uppertextblock:[{
			textclass:"topTxt",
			textdata:data.string.last_top_txt
		}],
		threebox:[
			{
				boxclass:"box fst",
				textbox:[
					{
						textclass:'boxTopTxt',
						textdata:data.string.fst_box_1
					},
					{
						textclass:'undLine',
					},
					{
						textclass:'boxbtmTxt',
						textdata:data.string.fst_box_2
				}]
			},
			{
				boxclass:"box sec",
				textbox:[
					{
						textclass:'boxTopTxt',
						textdata:data.string.sec_box_1
					},
					{
						textclass:'undLine',
					},
					{
						textclass:'boxbtmTxt',
						textdata:data.string.sec_box_2
				}]
			},
			{
				boxclass:"box third",
				textbox:[
					{
						textclass:'boxTopTxt',
						textdata:data.string.third_box_1
					},
					{
						textclass:'undLine',
					},
					{
						textclass:'boxbtmTxt',
						textdata:data.string.third_box_2
				}]
			}
		]
	},
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "dummy", src: imgpath+"1.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "coverpage",    src: imgpath+"coverpage.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "arw_st",    src: imgpath+"arrow04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arw_up",    src: imgpath+"arrow03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arw_dn",    src: imgpath+"arrow05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "two_arow",    src: imgpath+"twodownarrow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dwn_arw",    src: imgpath+"onedownarrow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hrsw",    src: imgpath+"e.png", type: createjs.AbstractLoader.IMAGE},
			{id: "drgha",    src: imgpath+"ee.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hrs_uk",    src: imgpath+"ue.png", type: createjs.AbstractLoader.IMAGE},
			{id: "drgha_uk",    src: imgpath+"uee.png", type: createjs.AbstractLoader.IMAGE},

			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s5_p1", src: soundAsset+"s5_p1.ogg"},
			{id: "s5_p2", src: soundAsset+"s5_p2.ogg"},
			{id: "s5_p3", src: soundAsset+"s5_p3.ogg"},
			{id: "s5_p4", src: soundAsset+"s5_p4.ogg"},
			{id: "s5_p5", src: soundAsset+"s5_p5.ogg"},
			{id: "s5_p6", src: soundAsset+"s5_p6.ogg"},
			{id: "s5_p7", src: soundAsset+"s5_p7.ogg"},
			{id: "s5_p8", src: soundAsset+"s5_p8.ogg"},
			{id: "s5_p9", src: soundAsset+"s5_p9.ogg"},
			{id: "s5_p12", src: soundAsset+"s5_p12.ogg"},
			{id: "s5_p13", src: soundAsset+"s5_p13.ogg"},
			{id: "s5_p16", src: soundAsset+"s5_p16.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	// $nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);
		$(".transparent").removeClass("forhover");
		$(".transparent").removeClass("buttonsel");
		$(".nodsp").hide(0);
		var num1 = 0;
		var num = Math.floor(Math.random() * (2 - 0 +1)) + 0;
		num == 1?num=2:num;
		num == 0?num1=2:num1;
		switch(countNext){
			case 0:
			sound_player("s5_p"+(countNext+1),1);
			$(".hrswik").append("<img class='sign' src='"+preload.getResult("hrsw").src+"'/>");
			$(".drghik").append("<img class='sign' src='"+preload.getResult("drgha").src+"'/>");
			$(".hrswuk").append("<img class='sign' src='"+preload.getResult("hrs_uk").src+"'/>");
			$(".drghuk").append("<img class='sign' src='"+preload.getResult("drgha_uk").src+"'/>");
			break;
			case 1:
			// sound_player("s5_p"+(countNext+1),1);
			$(".hrswik").append("<img class='sign' src='"+preload.getResult("hrsw").src+"'/>");
			$(".drghik").append("<img class='sign' src='"+preload.getResult("drgha").src+"'/>");
			$(".hrswuk").append("<img class='sign' src='"+preload.getResult("hrs_uk").src+"'/>");
			$(".drghuk").append("<img class='sign' src='"+preload.getResult("drgha_uk").src+"'/>");
			$(".boxToshow, .dwn_arw").hide(0);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s5_p2");
				current_sound.play();
				current_sound.on("complete", function(){
					$(".lbox, .twnfst").fadeOut(1000);
					$(".boxToshowlft").fadeIn(2000);
					$(".dwnfst").fadeIn(5200);
					setTimeout(function(){
						$(".rbox, .twnsec").fadeOut(1000);
						$(".boxToshowrght").fadeIn(2000);
						$(".dwnsec").fadeIn(5200);
						nav_button_controls(2000);
					},2100);
				});
			break;
			case 2:
				sound_player("s5_p"+(countNext+1),1);
				$(".serialtxt:eq(0)").fadeIn(0);
				$(".serialtxt:eq(1)").delay(3000).fadeIn(0);
				$(".serialtxt:eq(2)").delay(4700).fadeIn(0);
			break;
			case 4:
				sound_player("s5_p"+(countNext+1),1);
			break;
			case 15:
			sound_player("s5_p"+(countNext+1),1);
				$(".box").hide(0);
				$(".fst").fadeIn();
				$(".sec").fadeIn();
				$(".third").fadeIn();
					// createjs.Sound.stop();
					// current_sound = createjs.Sound.play("sound_0");
					// current_sound.play();
					// current_sound.on('complete', function(){
					// 		createjs.Sound.stop();
					// 		current_sound_1 = createjs.Sound.play("sound_0");
					// 		current_sound_1.play();
					// 		current_sound_1.on('complete', function(){
					// 				createjs.Sound.stop();
					// 				current_sound_2 = createjs.Sound.play("sound_0");
					// 				current_sound_2.play();
					// 				current_sound_2.on('complete', function(){
					// 					nav_button_controls(0);
					// 				});
					// 		});
					// });
			break;
			case 4:
			sound_player("s5_p"+(countNext+1),1);
				$(".btmTxtBox, .sideOrgBox, .arrow").hide(0);
				$(".hlgtTxt:eq(0)").addClass("blueHighlightTXt");
				setTimeout(function(){$(".hlgtTxt:eq(1)").addClass("blueHighlightTXt");},1000);
				setTimeout(function(){$(".hlgtTxt:eq(2)").addClass("blueHighlightTXt");},2000);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_0");
				current_sound.play();
				current_sound.on('complete', function(){
					$(".btmTxtBox").fadeIn(100);
					createjs.Sound.stop();
					current_sound_1 = createjs.Sound.play("sound_0");
					current_sound_1.play();
					current_sound_1.on('complete', function(){
						$(".sideOrgBox, .arrow").fadeIn(100);
							$(".btmTxtBox").fadeIn(100);
							createjs.Sound.stop();
							createjs.Sound.play("sound_0");
							nav_button_controls(0);
					});
				});
			break;
			case 5:
			case 6:
			case 12:
			case 13:
			case 14:
			sound_player("s5_p"+(countNext+1),0);
				btnslClk();
				randomize(".optionsdiv");
			break;
			case 8:
			sound_player("s5_p"+(countNext+1),1);
				var ans = $(".buttonsel").parent().find("p").attr("data-answer").toString();
				qnGenerator(".optionscontainer:eq("+num+")", ".optionscontainer:eq("+num1+")", "भाइ","भाई");
				btnslClkSec(ans);
			break;
			case 9:
			sound_player("s5_p"+(countNext+1),1);
				var ans = $(".buttonsel").parent().find("p").attr("data-answer").toString();
				qnGenerator(".optionscontainer:eq("+num+")", ".optionscontainer:eq("+num1+")", "फुपाजु","फुपाजू");
				btnslClkSec(ans);
			break;
			case 10:
			sound_player("s5_p"+(countNext+1),1);
				var ans = $(".buttonsel").parent().find("p").attr("data-answer").toString();
				qnGenerator(".optionscontainer:eq("+num+")", ".optionscontainer:eq("+num1+")", "भिनाजु","भिनाजू");
				btnslClkSec(ans);
			break;
			case 11:
			sound_player("s5_p"+(countNext+1),1);
			break;
			default:
			sound_player("s5_p"+(countNext+1),1);
		}
	}
	function btnslClk(){
		$(".buttonsel").on('click',function(){
			if($(this).hasClass("correct")){
				$(this).css({
					"background":"#98C02E",
					"border":"4px solid #DEEF3C",
					"color":"#fff",
					"pointer-events":"none"
				});
				$(this).siblings(".corctopt").css("display","block");
				$(".buttonsel").css("pointer-events","none");
				$(".dsp").hide(0);
				$(".nodsp").show();
				play_correct_incorrect_sound(1);
					createjs.Sound.stop();
				nav_button_controls(0);
			}
			else{
				$(this).css({
					"background":"#FF0000",
					"border":"4px solid #980000",
					"color":"#000",
					"pointer-events":"none"
				});
				$(this).siblings(".wrngopt").css("display","block");
					createjs.Sound.stop();
				play_correct_incorrect_sound(0);
			}
		});
	}
	function qnGenerator(divClass1,divClass2, txt1, txt2){
		var num1 = 0;
		var num = Math.floor(Math.random() * (2 - 0 +1)) + 0;
		num == 1?num=2:num;
		num == 0?num1=2:num1;
		$(divClass1).find("p").html(txt1);
		$(divClass2).find("p").html(txt2);

	}
	function btnslClkSec(ans){
		$(".buttonsel").click(function(){
			clkAns = $(this).text();
			if(ans == clkAns){
					$(this).css({
						"background":"#98C02E",
						"border":"4px solid #DEEF3C",
						"color":"#fff",
						"pointer-events":"none"
					});
					$(this).children(".corctopt").css("display","block");
					$(".buttonsel").css("pointer-events","none");
					$(".dsp").hide(0);
					$(".nodsp").show();
					play_correct_incorrect_sound(1);
						createjs.Sound.stop();
					nav_button_controls(0);
			}
			else{
					$(this).css({
						"background":"#FF0000",
						"border":"4px solid #980000",
						"color":"#000",
						"pointer-events":"none"
					});
					$(this).children(".wrngopt").css("display","block");
					play_correct_incorrect_sound(0);
						createjs.Sound.stop();
			}
		});
	}

  function appeartextonebyone(className) {
      var delaytime = 1000;
      setTimeout(function(){
          vocabcontroller.findwords(countNext);
					navigationcontroller(true);
      },(className.length+2)*1000);
      for (var i = 0; i < className.length; i++) {
          delaytime = delaytime+1000;
          $(className).eq(i).delay(delaytime).animate({"opacity":"1"},1000);
      }
  }
	/*for randomizing the options*/
	function randomize(parent){
		var parent = $(parent);
		var divs = parent.children();
		while (divs.length) {
			parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
	}


	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls():'';
		});
	}

	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2);
		});

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

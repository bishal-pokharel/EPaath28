var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide1
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt t15",
		storytxt:[
			{
				textclass: "story",
				textdata: data.string.sp4s1_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'hen_cooking',
					imgsrc: ""
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.sp4s1_box_txt,
			textclass : 'txt1',
			imgclass: 'box rotate',
			imgid : 'speechbox_fst',
			imgsrc: '',
		}]
	},
	// slide2
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt t15",
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background scaled",
					imgid : 'hen_cooking',
					imgsrc: ""
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-1 sp_s2',
			textdata : data.string.sp4s2_box_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'speechbox_fst',
			imgsrc: '',
		}]
	},
	// slide3
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt t15",
		storytxt:[
			{
				textclass: "story",
				textdata: data.string.sp4s3_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'hen_cooking',
					imgsrc: ""
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-1 sp_s3',
			textdata : data.string.sp4s3_box_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'speechbox_sixth',
			imgsrc: '',
		}]
	},
	// slide4
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt t15",
		storytxt:[
			{
				textclass: "story",
				textdata: data.string.sp4s4_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'hen_cooking',
					imgsrc: ""
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-1 sp_s4',
			textdata : data.string.sp4s4_box_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'speechbox_fst',
			imgsrc: '',
		}]
	},
	// slide5
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt t15",
		storytxt:[
			{
				textclass: "story",
				textdata: data.string.sp4s5_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'rats_asking',
					imgsrc: ""
				}
			]
		}],
		speechbox:[{
			speechbox: 'sld5_sp',
			textdata : data.string.sp4s5_box_txt,
			textclass : 'txt1',
			imgclass: 'box rotate',
			imgid : 'speechbox_sec',
			imgsrc: '',
		}]
	},
	// slide6
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt t15",
		storytxt:[
			{
				textclass: "story",
				textdata: data.string.sp4s6_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'hen_cooking',
					imgsrc: ""
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-1 sp_s6',
			textdata : data.string.sp4s6_box_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'speechbox_fst',
			imgsrc: '',
		}]
	},
	// slide7
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt t15",
		storytxt:[
			{
				textclass: "story",
				textdata: data.string.sp4s7_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "bgSec bgAnim",
					imgid : 'rats_asking',
					imgsrc: ""
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-1 sp_s7',
			textdata : data.string.sp4s7_box_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'speechbox_fst',
			imgsrc: '',
		}]
	},
	// slide8
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt t15",
		storytxt:[
			{
				textclass: "story",
				textdata: data.string.sp4s8_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background scaleBgS8",
					imgid : 'rats_talking',
					imgsrc: ""
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-1 sp_s8',
			textdata : data.string.sp4s8_box_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'together',
			imgsrc: '',
		}]
	},
	// slide9
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt t15",
		storytxt:[
			{
				textclass: "story",
				textdata: data.string.sp4s9_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background scaleBgS8",
					imgid : 'rats_talking',
					imgsrc: ""
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-1 sp_s9',
			textdata : data.string.sp4s9_box_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'speechbox_sec',
			imgsrc: '',
		}]
	},
	// slide10
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt t15",
		storytxt:[
			{
				textclass: "story",
				textdata: data.string.sp4s10_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background scaleBgS9",
					imgid : 'rats_talking',
					imgsrc: ""
				},
				{
					imgclass: "remember hide",
					imgid : 'remember',
					imgsrc: ""
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-1 sp_s10',
			textdata : data.string.sp4s10_box_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'together',
			imgsrc: '',
		}]
	},
	// slide11
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt t15",
		storytxt:[
			{
				textclass: "story",
				textdata: data.string.sp4s11_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background scaleBgS8",
					imgid : 'rats_talking',
					imgsrc: ""
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-1 sp_s11',
			textdata : data.string.sp4s11_box_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'speechbox_sec',
			imgsrc: '',
		}]
	},
	// slide12
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt t15",
		storytxt:[
			{
				textclass: "story",
				textdata: data.string.sp4s10_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background scaleBgS9",
					imgid : 'rats_talking',
					imgsrc: ""
				},
				{
					imgclass: "remember hide",
					imgid : 'remember_2',
					imgsrc: ""
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-1 sp_s10',
			textdata : data.string.sp4s12_box_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'together',
			imgsrc: '',
		}]
	},
	// slide13
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt t15",
		storytxt:[
			{
				textclass: "story",
				textdata: data.string.sp4s13_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'rats_asking',
					imgsrc: ""
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-1 sp_s13',
			textdata : data.string.sp4s13_box_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'speechbox_fst',
			imgsrc: '',
		}]
	},
	// slide14
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt t15",
		storytxt:[
			{
				textclass: "story",
				textdata: data.string.sp4s14_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'rats_asking',
					imgsrc: ""
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-1 sp_s14',
			textdata : data.string.sp4s14_box_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'speechbox_third',
			imgsrc: '',
		}]
	},
	// slide15
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt t15",
		storytxt:[
			{
				textclass: "story",
				textdata: data.string.sp4s15_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background scaleBgS9",
					imgid : 'rats_talking',
					imgsrc: ""
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-1 sp_s15',
			textdata : data.string.sp4s15_box_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'speechbox_sec',
			imgsrc: '',
		}]
	},
	// slide16
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt t15",
		storytxt:[
			{
				textclass: "story",
				textdata: data.string.sp4s16_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'rats_crying',
					imgsrc: ""
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-1 sp_s16',
			textdata : data.string.sp4s16_box_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'speechbox_fifth',
			imgsrc: '',
		}]
	},
	// slide17
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt t15",
		storytxt:[
			{
				textclass: "story whBg",
				textdata: data.string.sp4s17_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'rats_crying',
					imgsrc: ""
				}
			]
		}],
	},
	// slide18
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt t15",
		storytxt:[
			{
				textclass: "story",
				textdata: data.string.sp4s18_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "scaleBgS18",
					imgid : 'rats_crying',
					imgsrc: ""
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-1 sp_s18',
			textdata : data.string.sp4s18_box_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'speechbox_sec',
			imgsrc: '',
		}]
	},
	// slide19
	{
		contentblockadditionalclass: "thebg1",
		storytxtclass:"stryXtx fstTxt t15",
		storytxt:[
			{
				textclass: "story",
				textdata: data.string.sp4s19_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'rats_get_roti',
					imgsrc: ""
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-1 sp_s19',
			textdata : data.string.sp4s19_box_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'speechbox_fourth',
			imgsrc: '',
		}]
	},
	// slide20
	{
		contentblockadditionalclass: "thebg1",
		uppertextblockadditionalclass: 'end-dialouge  end-anim wobble',
		uppertextblock:[{
			textdata: data.string.p4endtxt,
			textclass: "",
		}],
		extratextblock:[{
			textdata: '',
			textclass: "overlay-endpage",
		}],
		storytxtclass:"stryXtx fstTxt t15",
		storytxt:[
			{
				textclass: "story",
				textdata: data.string.sp4s19_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'rats_get_roti',
					imgsrc: ""
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-1 sp_s19 show',
			textdata : data.string.sp4s19_box_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'speechbox_fourth',
			imgsrc: '',
		}]
	},
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "first", src: imgpath+"1.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "hen_hold_maize", src: imgpath+"5.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "rats_on_floor", src: imgpath+"11.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "rats_on_floor_when", src: imgpath+"3.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "hen_cooking", src: imgpath+"14.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "rats_asking", src: imgpath+"15.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "rats_talking", src: imgpath+"17.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "rats_crying", src: imgpath+"18.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "rats_get_roti", src: imgpath+"19.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "together", src: imgpath+"together.png", type: createjs.AbstractLoader.IMAGE},
			{id: "speechbox_fst", src: imgpath+"bubble-1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "speechbox_sec", src: imgpath+"bubble-1-10.png", type: createjs.AbstractLoader.IMAGE},
			{id: "speechbox_third", src: imgpath+"dialogue-boxes1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "speechbox_fourth", src: imgpath+"dialogue-boxes3.png", type: createjs.AbstractLoader.IMAGE},
			{id: "speechbox_fifth", src: imgpath+"dialogue-boxes2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "speechbox_sixth", src: imgpath+"text_box03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "remember", src: imgpath+"remember-frame.png", type: createjs.AbstractLoader.IMAGE},
			{id: "thinkbox", src: imgpath+"thinkbox.png", type: createjs.AbstractLoader.IMAGE},
			{id: "remember_2", src: imgpath+"remember-2-frame-2.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s4_p1", src: soundAsset+"s4_p1.ogg"},
			{id: "s4_p1_1", src: soundAsset+"s4_p1_1.ogg"},
			{id: "s4_p2", src: soundAsset+"s4_p2.ogg"},
			{id: "s4_p3", src: soundAsset+"s4_p3.ogg"},
			{id: "s4_p3_1", src: soundAsset+"s4_p3_1.ogg"},
			{id: "s4_p4", src: soundAsset+"s4_p4.ogg"},
			{id: "s4_p4_1", src: soundAsset+"s4_p4_1.ogg"},
			{id: "s4_p5", src: soundAsset+"s4_p5.ogg"},
			{id: "s4_p5_1", src: soundAsset+"s4_p5_1.ogg"},
			{id: "s4_p6", src: soundAsset+"s4_p6.ogg"},
			{id: "s4_p6_1", src: soundAsset+"s4_p6_1.ogg"},
			{id: "s4_p7", src: soundAsset+"s4_p7.ogg"},
			{id: "s4_p7_1", src: soundAsset+"s4_p7_1.ogg"},
			{id: "s4_p8", src: soundAsset+"s4_p8.ogg"},
			{id: "s4_p8_1", src: soundAsset+"s4_p8_1.ogg"},
			{id: "s4_p9", src: soundAsset+"s4_p9.ogg"},
			{id: "s4_p9_1", src: soundAsset+"s4_p9_1.ogg"},
			{id: "s4_p10", src: soundAsset+"s4_p10.ogg"},
			{id: "s4_p10_1", src: soundAsset+"s4_p10_1.ogg"},
			{id: "s4_p11", src: soundAsset+"s4_p11.ogg"},
			{id: "s4_p11_1", src: soundAsset+"s4_p11_1.ogg"},
			{id: "s4_p12", src: soundAsset+"s4_p12.ogg"},
			{id: "s4_p12_1", src: soundAsset+"s4_p12_1.ogg"},
			{id: "s4_p13", src: soundAsset+"s4_p13.ogg"},
			{id: "s4_p13_1", src: soundAsset+"s4_p13_1.ogg"},
			{id: "s4_p14", src: soundAsset+"s4_p14.ogg"},
			{id: "s4_p14_1", src: soundAsset+"s4_p14_1.ogg"},
			{id: "s4_p15", src: soundAsset+"s4_p15.ogg"},
			{id: "s4_p15_1", src: soundAsset+"s4_p15_1.ogg"},
			{id: "s4_p16", src: soundAsset+"s4_p16.ogg"},
			{id: "s4_p16_1", src: soundAsset+"s4_p16_1.ogg"},
			{id: "s4_p17", src: soundAsset+"s4_p17.ogg"},
			{id: "s4_p18", src: soundAsset+"s4_p18.ogg"},
			{id: "s4_p18_1", src: soundAsset+"s4_p18_1.ogg"},
			{id: "s4_p19", src: soundAsset+"s4_p19.ogg"},
			{id: "s4_p19_1", src: soundAsset+"s4_p19_1.ogg"},
			{id: "s4_p20", src: soundAsset+"s4_p20.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	// $nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.hide(0);
 	$prevBtn.hide(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	// islastpageflag ?
 	// ole.footerNotificationHandler.lessonEndSetNotification() :
 	// ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);

		function spchBoxAfterTxt(soundId,soundIdSec, spBoxClass){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(soundId);
			current_sound.play();
			current_sound.on('complete', function(){
				$(spBoxClass).addClass("fadeInFast");
					createjs.Sound.stop();
					current_sound_1 = createjs.Sound.play(soundIdSec);
					current_sound_1.play();
					current_sound_1.on('complete', function(){
						nav_button_controls(100);
					});
			});
		}
		switch(countNext){
			case 0:
				$(".story").css("opacity", "0");
				$(".background").addClass("zoom_in_1");
				setTimeout(function(){
					$(".story").addClass("fadeInFast");
						spchBoxAfterTxt("s4_p1","s4_p1_1", ".sp-1");
				},2000);
			break;
			case 1:
				$(".scaled").addClass("zoom_out_1");
				setTimeout(function(){
					sound_player("s4_p2",1);
					$(".sp-1").addClass("fadeInFast");
					// spchBoxAfterTxt("s4_p2","s4_p2_1", ".sp-1");
				},2000);
			break;
			case 2:
				spchBoxAfterTxt("s4_p3","s4_p3_1", ".sp-1");
			break
			case 3:
				spchBoxAfterTxt("s4_p4","s4_p4_1", ".sp-1");
			break;
			case 4:
				spchBoxAfterTxt("s4_p5","s4_p5_1", ".sld5_sp");
			break;
			case 5:
				spchBoxAfterTxt("s4_p6","s4_p6_1", ".sp-1");
			break;
			case 6:
				$(".story").css("opacity", "0");
				setTimeout(function(){
				$(".story").css("opacity", "1");
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s4_p7");
				current_sound.play();
				current_sound.on('complete', function(){
					$(".bgSec").addClass("zoomOutz");
					setTimeout(function(){
						$(".sp-1").addClass("fadeInFast");
						sound_player("s4_p7_1",1);
					},2000);
				});
				},2000);
			break;
			case 7:
				spchBoxAfterTxt("s4_p8","s4_p8_1", ".sp-1");
			break;
			case 8:
				$(".background").addClass("zoomoutS9");
				$(".story").css("opacity", "0");
				setTimeout(function(){
					$(".story").css("opacity", "1");
						spchBoxAfterTxt("s4_p9","s4_p9_1", ".sp-1");
				},2000);
			break;
			case 9:
				$(".background").addClass("zoominS9");
				$(".story").css("opacity", "0");
				setTimeout(function(){
					$(".story").css("opacity", "1");
					$(".remember").addClass("show");
						spchBoxAfterTxt("s4_p10","s4_p10_1", ".sp-1");
				},2000);
			break;
			case 10:
				$(".background").addClass("zoomoutS9");
				$(".story").css("opacity", "0");
				setTimeout(function(){
					$(".story").css("opacity", "1");
					$(".remember").addClass("show");
						spchBoxAfterTxt("s4_p11","s4_p11_1", ".sp-1");
				},2000);
			break;
			case 11:
				$(".background").addClass("zoominS9");
				$(".story").css("opacity", "0");
				setTimeout(function(){
					$(".story").css("opacity", "1");
					$(".remember").addClass("show");
						// sound_player("s4_p12",1);
						spchBoxAfterTxt("s4_p12","s4_p12_1", ".sp-1");
				},2000);
			break;
			case 12:
				spchBoxAfterTxt("s4_p13","s4_p13_1", ".sp-1");
			break;
			case 13:
				spchBoxAfterTxt("s4_p14","s4_p14_1", ".sp-1");
			break;
			case 14:
				$(".background").addClass("zoomoutS15");
				$(".story").css("opacity", "0");
				setTimeout(function(){
					$(".story").css("opacity", "1");
						spchBoxAfterTxt("s4_p15","s4_p15_1", ".sp-1");
				},2000);
			break;
			case 15:
				$(".background").addClass("bgAnim");
				$(".story").css("opacity", "0");
				setTimeout(function(){
					$(".story").css("opacity", "1");
						spchBoxAfterTxt("s4_p16","s4_p16_1", ".sp-1");
				},2000);
			break;
			case 16:
				$(".background").addClass("scaleBgS17");
				sound_player("s4_p17",1);
				$(".story").css("color","#120101");
				// $(".story").css("opacity", "0");
				// setTimeout(function(){
				// 	$(".story").css("opacity", "1");
				// 	spchBoxAfterTxt("sound_0", ".sp-1");
				// },2000);
				// navigationcontroller();
			break;
			case 17:
			$(".scaleBgS18").addClass("zoomoutS18");
			$(".story").css("opacity", "0");
			setTimeout(function(){
				$(".story").css("opacity", "1");
					spchBoxAfterTxt("s4_p18","s4_p18_1", ".sp-1");
			},2000);
			break;
			case 18:
				spchBoxAfterTxt("s4_p19","s4_p19_1", ".sp-1");
			break;
			case 19:
				$prevBtn.show(0);
				sound_player("s4_p20",1);
				$('.end-dialouge').css({
					'background-image': 'url("'+ preload.getResult('thinkbox').src +'")',
					'background-size': '100% 100%',
				});
				// nav_button_controls(1000);
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls(100):'';
		});
	}

	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2);
		});

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref+"/exercise/images/";

var content=[
	//slide 1
	{
		questionblock:[{
			instructiondata: data.string.qnTxt,
		}],
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				option: [
					{
						option_class: "class1",
						textdata: data.string.qn1_op1,
					},
					{
						option_class: "class2",
						textdata: data.string.qn1_op2,
					}],
			}
		]
	},
	//slide 2
	{
		questionblock:[{
			instructiondata: data.string.qnTxt,
		}],
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext1,
				option: [
					{
						option_class: "class1",
						textdata: data.string.qn2_op1,
					},
					{
						option_class: "class2",
						textdata: data.string.qn2_op2,
					}],
			}
		]
	},
	//slide 3
	{
		questionblock:[{
			instructiondata: data.string.qnTxt,
		}],
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext1,
				option: [
					{
						option_class: "class1",
						textdata: data.string.qn3_op1,
					},
					{
						option_class: "class2",
						textdata: data.string.qn3_op2,
					}],
			}
		]
	},
	//slide 4
	{
		questionblock:[{
			instructiondata: data.string.qnTxt,
		}],
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext1,
				option: [
					{
						option_class: "class1",
						textdata: data.string.qn4_op1,
					},
					{
						option_class: "class2",
						textdata: data.string.qn4_op2,
					}],
			}
		]
	},
	//slide 5
	{
		questionblock:[{
			instructiondata: data.string.qnTxt,
		}],
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext1,
				option: [
					{
						option_class: "class1",
						textdata: data.string.qn5_op1,
					},
					{
						option_class: "class2",
						textdata: data.string.qn5_op2,
					}],
			}
		]
	},
	//slide 6
	{
		questionblock:[{
			instructiondata: data.string.qnTxt,
		}],
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext1,
				option: [
					{
						option_class: "class1",
						textdata: data.string.qn6_op1,
					},
					{
						option_class: "class2",
						textdata: data.string.qn6_op2,
					}],
			}
		]
	},
	//slide 7
	{
		questionblock:[{
			instructiondata: data.string.qnTxt,
		}],
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext1,
				option: [
					{
						option_class: "class1",
						textdata: data.string.qn7_op1,
					},
					{
						option_class: "class2",
						textdata: data.string.qn7_op2,
					}],
			}
		]
	},
	//slide 8
	{
		questionblock:[{
			instructiondata: data.string.qnTxt,
		}],
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext1,
				option: [
					{
						option_class: "class1",
						textdata: data.string.qn8_op1,
					},
					{
						option_class: "class2",
						textdata: data.string.qn8_op2,
					}],
			}
		]
	},
	//slide 9
	{
		questionblock:[{
			instructiondata: data.string.qnTxt,
		}],
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext1,
				option: [
					{
						option_class: "class1",
						textdata: data.string.qn9_op1,
					},
					{
						option_class: "class2",
						textdata: data.string.qn9_op2,
					}],
			}
		]
	},
	//slide 10
	{
		questionblock:[{
			instructiondata: data.string.qnTxt,
		}],
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext1,
				option: [
					{
						option_class: "class1",
						textdata: data.string.qn10_op1,
					},
					{
						option_class: "class2",
						textdata: data.string.qn10_op2,
					}],
			}
		]
	},


];
content.shufflearray();

/*remove this for non random questions*/
$(function () {
	var testin = new NumberTemplate();

	var exercise = new template_exercise_mcq_monkey(content, testin, true);
	exercise.create_exercise();
});

function template_exercise_mcq_monkey(content, scoring){
	var $board			= $('.board');
	var $nextBtn		= $("#activity-page-next-btn-enabled");
	var $prevBtn		= $("#activity-page-prev-btn-enabled");
	var countNext		= 0;
	var testin			= new EggTemplate();

	var wrong_clicked 	= false;

	var total_page = content.length;

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);

		// testin.numberOfQuestions();

		/*for randomizing the options*/
		var option_position = [3,4];
		option_position.shufflearray();
		for(var op=0; op<4; op++){
			$('.main-container').eq(op).addClass('option-pos-'+option_position[op]);
		}
		// //top-left
		// $('.option-pos-1').hover(function(){
		// 	$('.center-sundar').attr('src', 'images/sundar/top-right.png');
		// 	$('.center-sundar').css('transform','scaleX(-1)');
		// }, function(){
		//
		// });
		//bottom-left
		$('.option-pos-3').hover(function(){
			$('.center-sundar').attr('src', 'images/sundar/bottom-right.png');
			$('.center-sundar').css('transform','scaleX(-1)');
		}, function(){

		});
		// //top-right
		// $('.option-pos-2').hover(function(){
		// 	$('.center-sundar').attr('src', 'images/sundar/top-right.png');
		// 	$('.center-sundar').css('transform','none');
		// }, function(){
		//
		// });
		//bottom-right
		$('.option-pos-4').hover(function(){
			$('.center-sundar').attr('src', 'images/sundar/bottom-right.png');
			$('.center-sundar').css('transform','none');
		}, function(){

		});


		var wrong_clicked = 0;
		var correct_images = ['correct-1.png', 'correct-2.png', 'correct-3.png'];
		$(".option-container").click(function(){
			if($(this).hasClass("class1")){
				if(wrong_clicked<1){
					testin.update(true);
				}
				var rand_img = Math.floor(Math.random()*correct_images.length);
				$('.option-pos-1, .option-pos-2, .option-pos-3, .option-pos-4').off('mouseenter mouseleave');
				$('.center-sundar').attr('src', 'images/sundar/'+correct_images[rand_img]);
				$(".option-container").css('pointer-events','none');
	 			play_correct_incorrect_sound(1);
				$(this).addClass('correct-ans');
				$(this).parent().children('.correct-icon').show(0);
				wrong_clicked = 0;
				if(countNext != total_page)
					$nextBtn.show(0);
			}
			else{
				var classname_monkey = $(this).parent().attr('class').replace(/main-container/, '');
				classname_monkey = classname_monkey.replace(/ /g, '');
				$('.'+classname_monkey).off('mouseenter mouseleave');
				if(wrong_clicked==0){
					$('.center-sundar').attr('src', 'images/sundar/incorrect-1.png');
				} else if(wrong_clicked == 1){
					$('.center-sundar').attr('src', 'images/sundar/incorrect-2.png');
				} else {
					$('.center-sundar').attr('src', 'images/sundar/incorrect-3.png');
				}
				testin.update(false);
	 			play_correct_incorrect_sound(0);
				$(this).addClass('incorrect-ans');
				$(this).parent().children('.incorrect-icon').show(0);
				wrong_clicked++;
			}
		});
	};


	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	};

	this.create_exercise = function(){
		if(typeof scoring != 'undefined'){
			testin = scoring;
		}
	 	testin.init(total_page);

		templateCaller();
		$nextBtn.on("click", function(){
			countNext++;
			testin.gotoNext();
			templateCaller();
		});
	// 	$refreshBtn.click(function(){
	// 	templateCaller();
	// });

	$prevBtn.on('click',function () {
			countNext--;
			templateCaller();
		});
	};
}

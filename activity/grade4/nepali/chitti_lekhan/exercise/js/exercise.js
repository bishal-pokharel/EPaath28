var content=[
    //slide 0
    {
        contentblockadditionalclass:'',
        exerciseblock: [
            {
                textclass : 'instruction my_font_big',
                questiondata: data.string.pq1,
                option: [
                    {
                        option_class: "class1",
                        optiondata: data.string.a1,
                    },
                    {
                        option_class: "class2",
                        optiondata: data.string.a2,
                    },
                    {
                        option_class: "class3",
                        optiondata: data.string.a3,
                    },
                    {
                        option_class: "class4",
                        optiondata: data.string.a4,
                    }],
            }
        ]
    },
    //slide 1
    {
        contentblockadditionalclass:'',
        exerciseblock: [
            {
                textclass : 'instruction my_font_big',
                questiondata: data.string.pq2,
                option: [
                    {
                        option_class: "class1",
                        optiondata: data.string.a1,
                    },
                    {
                        option_class: "class2",
                        optiondata: data.string.a2,
                    },
                    {
                        option_class: "class3",
                        optiondata: data.string.a3,
                    },
                    {
                        option_class: "class4",
                        optiondata: data.string.a5,
                    }],
            }
        ]
    },
    //slide 2
    {
        contentblockadditionalclass:'',
        exerciseblock: [
            {
                textclass : 'instruction my_font_big',
                questiondata: data.string.pq3,
                option: [
                    {
                        option_class: "class1",
                        optiondata: data.string.a3,
                    },
                    {
                        option_class: "class2",
                        optiondata: data.string.a5,
                    },
                    {
                        option_class: "class3",
                        optiondata: data.string.a1,
                    },
                    {
                        option_class: "class4",
                        optiondata: data.string.a4,
                    }],
            }
        ]
    },
    //slide 3
    {
        contentblockadditionalclass:'',
        exerciseblock: [
            {
                textclass : 'instruction my_font_big',
                questiondata: data.string.pq4,
                option: [
                    {
                        option_class: "class1",
                        optiondata: data.string.a2,
                    },
                    {
                        option_class: "class2",
                        optiondata: data.string.a6,
                    },
                    {
                        option_class: "class3",
                        optiondata: data.string.a4,
                    },
                    {
                        option_class: "class4",
                        optiondata: data.string.a5,
                    }],
            }
        ]
    },
    //slide 4
    {
        contentblockadditionalclass:'',
        exerciseblock: [
            {
                textclass : 'instruction my_font_big',
                questiondata: data.string.pq5,
                option: [
                    {
                        option_class: "class1",
                        optiondata: data.string.a7,
                    },
                    {
                        option_class: "class2",
                        optiondata: data.string.a8,
                    },
                    {
                        option_class: "class3",
                        optiondata: data.string.a9,
                    },
                    {
                        option_class: "class4",
                        optiondata: data.string.a10,
                    }],
            }
        ]
    },
    //slide 5
    {
        contentblockadditionalclass:'',
        exerciseblock: [
            {
                textclass : 'instruction my_font_big',
                questiondata: data.string.pq6,
                option: [
                    {
                        option_class: "class1",
                        optiondata: data.string.a11,
                    },
                    {
                        option_class: "class2",
                        optiondata: data.string.a12,
                    },
                    {
                        option_class: "class3",
                        optiondata: data.string.a13,
                    },
                    {
                        option_class: "class4",
                        optiondata: data.string.a14,
                    }],
            }
        ]
    },
    //slide 6
    {
        contentblockadditionalclass:'',
        exerciseblock: [
            {
                textclass : 'instruction my_font_big',
                questiondata: data.string.pq7,
                option: [
                    {
                        option_class: "class1",
                        optiondata: data.string.a15,
                    },
                    {
                        option_class: "class2",
                        optiondata: data.string.a16,
                    },
                    {
                        option_class: "class3",
                        optiondata: data.string.a17,
                    },
                    {
                        option_class: "class4",
                        optiondata: data.string.a18,
                    }],
            }
        ]
    },
    //slide 7
    {
        contentblockadditionalclass:'',
        exerciseblock: [
            {
                textclass : 'instruction my_font_big',
                questiondata: data.string.pq8,
                option: [
                    {
                        option_class: "class1",
                        optiondata: data.string.a19,
                    },
                    {
                        option_class: "class2",
                        optiondata: data.string.a20,
                    },
                    {
                        option_class: "class3",
                        optiondata: data.string.a21,
                    },
                    {
                        option_class: "class4",
                        optiondata: data.string.a22,
                    }],
            }
        ]
    },
    //slide 8
    {
        contentblockadditionalclass:'',
        exerciseblock: [
            {
                textclass : 'instruction my_font_big',
                questiondata: data.string.pq9,
                option: [
                    {
                        option_class: "class1",
                        optiondata: data.string.a3,
                    },
                    {
                        option_class: "class2",
                        optiondata: data.string.a6,
                    },
                    {
                        option_class: "class3",
                        optiondata: data.string.a2,
                    },
                    {
                        option_class: "class4",
                        optiondata: data.string.a1,
                    }],
            }
        ]
    },
    //slide 10
    {
        contentblockadditionalclass:'',
        exerciseblock: [
            {
                textclass : 'instruction my_font_big',
                questiondata: data.string.pq10,
                option: [
                    {
                        option_class: "class1",
                        optiondata: data.string.a23,
                    },
                    {
                        option_class: "class2",
                        optiondata: data.string.a24,
                    },
                    {
                        option_class: "class3",
                        optiondata: data.string.a25,
                    },
                    {
                        option_class: "class4",
                        optiondata: data.string.a26,
                    }],
            }
        ]
    },
];

/*remove this for non random questions*/
$(function () {
    var numbertemp = new NumberTemplate();

    var exercise = new template_exercise_mcq_monkey(content, numbertemp, true);
    exercise.create_exercise();
});

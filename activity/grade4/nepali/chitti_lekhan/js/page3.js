var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "dialtitle",
                textdata: data.string.p3text1
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "dialogbox",
                    imgclass: "relativecls dialogboximg",
                    imgid: 'dialogboxImg',
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide 1

    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "title",
                textdata: data.string.p3text2
            }
        ],

        contents:[{
            div:[
                {
                    divcls:"commondiv  bodydiv",
                    textdata: [
                        {
                            data: data.string.ltrbody,
                            datacls:"bodyp centertext"

                        }
                    ]

                },

                {
                    divcls:"commondiv headerdiv letterdown ",
                    textdata:[
                        {
                            data:data.string.ltrheading,
                            datacls:"headerp centertext"
                        },
                        {
                            data:data.string.ltrsalutation,
                            datacls:"salutationp centertext"

                        }
                    ]

                },

                {
                    divcls:"commondiv  ending  letterup",
                    textdata: [
                        {
                            data: data.string.ltrending,
                            datacls:"endingp centertext"

                        }
                    ]
                }

            ]
        }],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "girl",
                    imgclass: "relativecls girlimg",
                    imgid: 'girlImg',
                    imgsrc: "",
                }
            ]
        }]

    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "title",
                textdata: data.string.p3text3
            }
        ],

        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "commonenv envelop flipenv",
                    imgclass: "relativecls envelopimg",
                    imgid: 'envelopImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "commonenv envelopclose",
                    imgclass: "relativecls envelopcloseimg",
                    imgid: 'envelopcloseImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "commonenv envelopfront",
                    imgclass: "relativecls envelopfrontimg",
                    imgid: 'envelopfrontImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "commonenv enveloptop",
                    imgclass: "relativecls enveloptopimg",
                    imgid: 'enveloptopImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "commonenv envelopback",
                    imgdiv: "commonenv envelopback",
                    imgclass: "relativecls envelopbackimg",
                    imgid: 'envelopbackImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "commonenv envelopopn",
                    imgclass: "relativecls envelopopnimg",
                    imgid: 'envelopopnImg',
                    imgsrc: "",
                }
            ]
        }],
        extradiv:[
            {
                divclass:"commonenv insertletter"
            }
        ]

    },
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "title",
                textdata: data.string.p3text9
            },
            {
                textclass: "leftdiv1",
                textdata: data.string.p3text5
            },
            {
                textclass: "leftdiv2",
                textdata: data.string.p3text6
            },
            {
                textclass: "rightdiv1",
                textdata: data.string.p3text7
            },
            {
                textclass: "rightdiv2",
                textdata: data.string.p3text8
            }
        ],

        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "commonenv envelop",
                    imgclass: "relativecls envelopimg",
                    imgid: 'envelopImg',
                    imgsrc: "",
                }
            ]
        }],
        extradiv:[
            {
                divclass:"line"
            }
        ]

    },
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "title",
                textdata: data.string.p3text11
            },
        ],

        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "letterbg",
                    imgclass: "relativecls letterbgimg",
                    imgid: 'letterbgImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "letterpost",
                    imgclass: "relativecls letterpostimg",
                    imgid: 'letterpostImg',
                    imgsrc: "",
                }
            ]
        }]

    },
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "title",
                textdata: data.string.p3text12
            },
        ],

        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "letterman",
                    imgclass: "relativecls lettermanimg",
                    imgid: 'lettermanImg',
                    imgsrc: "",
                }
            ]
        }]

    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "common-css", src: $ref + "css/common.css", type: createjs.AbstractLoader.CSS},
            {id: "page1-css", src: $ref + "css/page1.css", type: createjs.AbstractLoader.CSS},

            {id: "girlImg", src: imgpath+"GirlTalking/girltalking01.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "dialogboxImg", src: imgpath+"text_box03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "girl2Img", src: imgpath+"girl05.png", type: createjs.AbstractLoader.IMAGE},
            {id: "envelopImg", src: imgpath+"envelopes/env01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "envelopcloseImg", src: imgpath+"envelopes/env_close.png", type: createjs.AbstractLoader.IMAGE},
            {id: "envelopfrontImg", src: imgpath+"envelopes/env_front.png", type: createjs.AbstractLoader.IMAGE},
            {id: "enveloptopImg", src: imgpath+"envelopes/env_top.png", type: createjs.AbstractLoader.IMAGE},
            {id: "envelopopnImg", src: imgpath+"envelopes/env_open.png", type: createjs.AbstractLoader.IMAGE},
            {id: "letterImg", src: imgpath+"envelopes/letter.png", type: createjs.AbstractLoader.IMAGE},
            {id: "envshadedImg", src: imgpath+"envelopes/env_shaded.png", type: createjs.AbstractLoader.IMAGE},
            {id: "envelopbackImg", src: imgpath+"envelopes/back-top-top.png", type: createjs.AbstractLoader.IMAGE},
            {id: "letterpostImg", src: imgpath+"postingletter.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "lettermanImg", src: imgpath+"bgaa.png", type: createjs.AbstractLoader.IMAGE},
            {id: "letterbgImg", src: imgpath+"bg_for_letter_box.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_1", src: soundAsset+"p3_s0.ogg"},
            {id: "sound_2", src: soundAsset+"p3_s1.ogg"},
            {id: "sound_3", src: soundAsset+"p3_s2.ogg"},
            {id: "sound_4", src: soundAsset+"p3_s3.ogg"},
            {id: "sound_5", src: soundAsset+"p3_s4.ogg"},
            {id: "sound_6", src: soundAsset+"p3_s5.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

    /*===============================================
    =            data highlight function            =
    ===============================================*/
    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function (index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

    /*=====  End of data highlight function  ======*/





    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);


        // highlight any text inside board div with datahighlightflag set true
        // texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        put_image2(content, countNext,preload);
        switch(countNext){
            case 0:
                sound_player("sound_1",true);
                break;
            case 1:
                sound_player("sound_2",false,'girlimg',imgpath+'GirlTalking/girltalking01.png',false);
                 animateLetter();
                break;
            case 2:
                sound_player("sound_3",false);
                animateEnvelop();
                break;
            case 3:
                sound_player("sound_4",true);
                animateText();
                break;
            case 4:
                sound_player("sound_5",true);
                break;
            case 5:
                sound_player("sound_6",true);
                break;
            default:
                navigationcontroller(countNext,$total_page);
                break;
        }
    }



    function sound_player(sound_id,navigate,imgId,changeImg) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller(countNext,$total_page):"";
            $("."+imgId).attr("src",changeImg)
        });
    }





    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function put_image2(content, count,preload) {
        if (content[count].hasOwnProperty("contents")) {
            var contentCount=content[count].contents[0];
            var imageblockcontent=contentCount.hasOwnProperty('imageblock');
            dynamicimageload(imageblockcontent,contentCount,preload);
        }
    }
   function animateLetter() {
       setTimeout(function(){
           $(".letterup").empty();

       },2000);
       setTimeout(function(){
           $(".letterdown").empty();
           $(".bodydiv").css({"top": "100%","height": "0%"}).empty();
           navigationcontroller(countNext,$total_page);
       },4200);
   }
   function animateEnvelop(){
       $(".commonenv").css("opacity", "0");
       $(".envelop").css("opacity", "1");
       setTimeout(function () {
           $(".envelop").css("opacity", "0");
           $(".envelopclose").css("opacity", "1");
           setTimeout(function () {
               $(".envelopclose").css("opacity", "0");
               $(".envelopfront,.enveloptop").css("opacity", "1").removeClass("commonenv").addClass("slidedwenv fixedposition");
               setTimeout(function () {
                   $(".enveloptop,.envelopfront").css("opacity", "0");
                   $(".envelopopn").css("opacity", "1").removeClass("commonenv").addClass("fixedposition");
                   setTimeout(function () {
                   $(".insertletter").css("opacity","1").removeClass("commonenv").addClass("slideletter");
                   setTimeout(function () {
                       $(".insertletter,.envelopopn").css("opacity","0");
                       $(".envelopclose").css("opacity", "1").removeClass("slidedwenv").removeClass("letter").css("top","7%");
                               navigationcontroller(countNext,$total_page,false);
                       },1500);
                   }, 2000);
               }, 2000);
           }, 1000);
       }, 1000);
   }


   function animateText(){
       $(".leftdiv1,.leftdiv2,.rightdiv1,.rightdiv2,.line").hide(0);
       setTimeout(function(){
           $(".leftdiv1,.leftdiv2").show(0).addClass("zoomInEffect");
           setTimeout(function(){
               $(".line").show(0).addClass("zoomInEffect");
               setTimeout(function(){
                   $(".rightdiv1,.rightdiv2").show(0).addClass("zoomInEffect");
               },1000);
           },500);
       },2000)
   }
});
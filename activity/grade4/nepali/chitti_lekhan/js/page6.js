var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        uppertextblock: [
            {
                textclass: "diy",
                textdata: data.string.p6text1
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "diyimg",
                    imgclass: "relativecls diyimg",
                    imgid: 'diyImg',
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "titletop2",
                textdata: data.string.p6text2
            },
            {
                textclass: "titletop1",
                textdata: data.string.p6text3
            }
        ],
        messageblock:[
            {
                msgdiv:"msgdiv",
                msgtext:[
                    {
                        textcls:" commontxt header1",
                        textdata:data.string.p6header1
                    },
                    {
                        textcls:"commontxt  header2",
                        textdata:data.string.p6header2
                    },
                    {
                        textcls:"commontxt  salut1",
                        textdata:data.string.p6salut1
                    },
                    {
                        textcls:"commontxt  salut2",
                        textdata:data.string.p6salut2,
                    },
                    {
                        textcls:"commonbtn body1",
                        textdata:data.string.p6body1,
                        opendiv:true

                    },
                    {
                        textcls:"commontxt  body2",
                        textdata:data.string.p6body2
                    },
                    {
                        textcls:" commonbtn body3",
                        textdata:data.string.p6body3
                    },
                    {
                        textcls:"body12",
                        textdata:data.string.p6body2
                    },
                    {
                        textcls:"commonbtn body4",
                        textdata:data.string.p6body4
                    },
                    {
                        textcls:"commonbtn body5",
                        textdata:data.string.p6body5
                    },
                    {
                        textcls:"commonbtn body6",
                        textdata:data.string.p6body6
                    },
                    {
                        textcls:"commonbtn body7",
                        textdata:data.string.p6body7
                    },
                    {
                        textcls:"commontxt  body8",
                        textdata:data.string.p6body8
                    },
                    {
                        textcls:"commonbtn body9",
                        textdata:data.string.p6body9
                    },
                    {
                        textcls:"commontxt  body10",
                        textdata:data.string.p6body10
                    },
                    {
                        textcls:"commonbtn body11",
                        textdata:data.string.p6body11,
                        closediv:true
                    },
                    {
                        textcls:"commontxt  ending1",
                        textdata:data.string.p6endin1
                    },
                    {
                        textcls:"commontxt  ending2",
                        textdata:data.string.p6endin2
                    }
                ]

            }
        ]
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "titletop2",
                textdata: data.string.p6text2
            },
            {
                textclass: "titletop1",
                textdata: data.string.p6text4
            }
        ],
        messageblock:[
            {
                msgdiv:"msgdiv",
                msgtext:[
                    {
                        textcls:" commontxt header1",
                        textdata:data.string.p6header1
                    },
                    {
                        textcls:"commontxt  header2",
                        textdata:data.string.p6header2
                    },
                    {
                        textcls:"commontxt  salut1",
                        textdata:data.string.p6salut1
                    },
                    {
                        textcls:"commontxt  salut2",
                        textdata:data.string.p6salut2,
                    },
                    {
                        textcls:"body1",
                        textdata:data.string.p6body1,
                        opendiv:true

                    },
                    {
                        textcls:"commontxt  body2",
                        textdata:data.string.p6body2
                    },
                    {
                        textcls:" body3",
                        textdata:data.string.p6body3
                    },
                    {
                        textcls:" body12",
                        textdata:data.string.p6body2
                    },
                    {
                        textcls:" body4",
                        textdata:data.string.p6body4
                    },
                    {
                        textcls:" body5",
                        textdata:data.string.p6body5
                    },
                    {
                        textcls:" body6",
                        textdata:data.string.p6body6
                    },
                    {
                        textcls:" body7",
                        textdata:data.string.p6body7
                    },
                    {
                        textcls:"commontxt  body8",
                        textdata:data.string.p6body8
                    },
                    {
                        textcls:" body9",
                        textdata:data.string.p6body9
                    },
                    {
                        textcls:"commontxt  body10",
                        textdata:data.string.p6body10
                    },
                    {
                        textcls:" body11",
                        textdata:data.string.p6body11,
                        closediv:true
                    },
                    {
                        textcls:"commontxt  ending1",
                        textdata:data.string.p6endin1
                    },
                    {
                        textcls:"commontxt  ending2",
                        textdata:data.string.p6endin2
                    }
                ]

            }
        ]
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "titletop2",
                textdata: data.string.p6text2
            },
            {
                textclass: "titletop1",
                textdata: data.string.p6text5
            }
        ],
        messageblock:[
            {
                msgdiv:"msgdiv",
                msgtext:[
                    {
                        textcls:" commontxt header1",
                        textdata:data.string.p6header1
                    },
                    {
                        textcls:"commontxt  header2",
                        textdata:data.string.p6header2
                    },
                    {
                        textcls:"commontxt  salut1",
                        textdata:data.string.p6salut1
                    },
                    {
                        textcls:"commontxt  salut2",
                        textdata:data.string.p6salut2,
                    },
                    {
                        textcls:"body1",
                        textdata:data.string.p6body1,
                        opendiv:true

                    },
                    {
                        textcls:"commontxt  body2",
                        textdata:data.string.p6body2
                    },
                    {
                        textcls:" body3",
                        textdata:data.string.p6body3
                    },
                    {
                        textcls:"body12",
                        textdata:data.string.p6body2
                    },
                    {
                        textcls:" body4",
                        textdata:data.string.p6body4
                    },
                    {
                        textcls:" body5",
                        textdata:data.string.p6body5
                    },
                    {
                        textcls:" body6",
                        textdata:data.string.p6body6
                    },
                    {
                        textcls:" body7",
                        textdata:data.string.p6body7
                    },
                    {
                        textcls:"commontxt  body8",
                        textdata:data.string.p6body8
                    },
                    {
                        textcls:" body9",
                        textdata:data.string.p6body9
                    },
                    {
                        textcls:"commontxt  body10",
                        textdata:data.string.p6body10
                    },
                    {
                        textcls:" body11",
                        textdata:data.string.p6body11,
                        closediv:true
                    },
                    {
                        textcls:"commontxt  ending1",
                        textdata:data.string.p6endin1
                    },
                    {
                        textcls:"commontxt  ending2",
                        textdata:data.string.p6endin2
                    }
                ]

            }
        ]
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "titletop2",
                textdata: data.string.p6text2
            },
            {
                textclass: "titletop1",
                textdata: data.string.p6text6
            }
        ],
        messageblock:[
            {
                msgdiv:"msgdiv",
                msgtext:[
                    {
                        textcls:" commontxt header1",
                        textdata:data.string.p6header1
                    },
                    {
                        textcls:"commontxt  header2",
                        textdata:data.string.p6header2
                    },
                    {
                        textcls:"commontxt  salut1",
                        textdata:data.string.p6salut1
                    },
                    {
                        textcls:"commontxt  salut2",
                        textdata:data.string.p6salut2,
                    },
                    {
                        textcls:"body1",
                        textdata:data.string.p6body1,
                        opendiv:true

                    },
                    {
                        textcls:"commontxt  body2",
                        textdata:data.string.p6body2
                    },
                    {
                        textcls:" body3",
                        textdata:data.string.p6body3
                    },
                    {
                        textcls:"body12",
                        textdata:data.string.p6body2
                    },
                    {
                        textcls:" body4",
                        textdata:data.string.p6body4
                    },
                    {
                        textcls:" body5",
                        textdata:data.string.p6body5
                    },
                    {
                        textcls:" body6",
                        textdata:data.string.p6body6
                    },
                    {
                        textcls:" body7",
                        textdata:data.string.p6body7
                    },
                    {
                        textcls:"commontxt  body8",
                        textdata:data.string.p6body8
                    },
                    {
                        textcls:" body9",
                        textdata:data.string.p6body9
                    },
                    {
                        textcls:"commontxt  body10",
                        textdata:data.string.p6body10
                    },
                    {
                        textcls:" body11",
                        textdata:data.string.p6body11,
                        closediv:true
                    },
                    {
                        textcls:"commontxt  ending1",
                        textdata:data.string.p6endin1
                    },
                    {
                        textcls:"commontxt  ending2",
                        textdata:data.string.p6endin2
                    }
                ]

            }
        ]
    },

    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "titletop2 opacitycss",
                textdata: data.string.p6text2
            },
            {
                textclass: "titletop1 opacitycss",
                textdata: data.string.p6text6
            },
        ],
        textblock:[
            {
                textdiv: "endmsg ",
                textclass: "centertext abstract ",
                textdata: data.string.p6text7
            }
        ],
        messageblock:[
            {
                msgdiv:"msgdiv",
                msgtext:[
                    {
                        textcls:" commontxt header1 opacitycss",
                        textdata:data.string.p6header1
                    },
                    {
                        textcls:"commontxt  header2 opacitycss",
                        textdata:data.string.p6header2
                    },
                    {
                        textcls:"commontxt  salut1 opacitycss",
                        textdata:data.string.p6salut1
                    },
                    {
                        textcls:"commontxt  salut2 opacitycss",
                        textdata:data.string.p6salut2,
                    },
                    {
                        textcls:"body1 opacitycss",
                        textdata:data.string.p6body1,
                        opendiv:true

                    },
                    {
                        textcls:"commontxt  body2 opacitycss",
                        textdata:data.string.p6body2
                    },
                    {
                        textcls:" body3 opacitycss",
                        textdata:data.string.p6body3
                    },
                    {
                        textcls:"body12",
                        textdata:data.string.p6body2
                    },
                    {
                        textcls:" body4 opacitycss",
                        textdata:data.string.p6body4
                    },
                    {
                        textcls:" body5 opacitycss",
                        textdata:data.string.p6body5
                    },
                    {
                        textcls:" body6 opacitycss",
                        textdata:data.string.p6body6
                    },
                    {
                        textcls:" body7 opacitycss",
                        textdata:data.string.p6body7
                    },
                    {
                        textcls:"commontxt  body8 opacitycss",
                        textdata:data.string.p6body8
                    },
                    {
                        textcls:" body9 opacitycss",
                        textdata:data.string.p6body9
                    },
                    {
                        textcls:"commontxt  body10 opacitycss",
                        textdata:data.string.p6body10
                    },
                    {
                        textcls:" body11 opacitycss",
                        textdata:data.string.p6body11,
                        closediv:true
                    },
                    {
                        textcls:"commontxt  ending1 opacitycss",
                        textdata:data.string.p6endin1
                    },
                ]

            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "girl1",
                    imgclass: "relativecls girl1img",
                    imgid: 'girl1Img',
                    imgsrc: "",
                }
            ]
        }]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "common-css", src: $ref + "css/common.css", type: createjs.AbstractLoader.CSS},
            {id: "page1-css", src: $ref + "css/page1.css", type: createjs.AbstractLoader.CSS},

            {id: "girl1Img", src: imgpath+"GirlTalking/girltalking01.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "dialogboxImg", src: imgpath+"text_box03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "geetaImg", src: imgpath+"geeta.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dadImg", src: imgpath+"dad.png", type: createjs.AbstractLoader.IMAGE},
            {id: "premImg", src: imgpath+"prem.png", type: createjs.AbstractLoader.IMAGE},
            {id: "didiImg", src: imgpath+"didi.png", type: createjs.AbstractLoader.IMAGE},
            {id: "diyImg", src: imgpath+"pencils-and-book.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_1", src: soundAsset+"p6_s0.ogg"},
            {id: "sound_2", src: soundAsset+"p6_s1.ogg"},
            {id: "sound_3", src: soundAsset+"p6_s5.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

    /*===============================================
    =            data highlight function            =
    ===============================================*/
    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function (index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

    /*=====  End of data highlight function  ======*/



    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        checkans(countNext);
        switch (countNext){
            case 0:
                sound_player("sound_1",true);
                break;
            case 1:
                sound_player("sound_2",false);
                break;
            case 5:
                $(".coverboardfull div").not(".girl1,.endmsg").css({"opacity":"0.3"});
                // $(".coverboardfull div").not(".endmsg").css({"opacity":"0.3"});
                $(".bg").css({"background":"#747571"});
                sound_player("sound_3",true,"girl1Img",imgpath+"GirlTalking/girltalking01.png");
                break;

        }
    }



    function sound_player(sound_id,navigate,imgId,changeImg) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller(countNext,$total_page):"";
            $("."+imgId).attr("src",changeImg)
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });
   function checkans(countNext){
       switch (countNext){
           case 1:
               $(".commontxt").click(function(){
                   if($(this).text()==data.string.p6header1) {
                       $(this).addClass("correctans");
                       play_correct_incorrect_sound(1);
                       navigationcontroller(countNext,$total_page);
                       $(".commontxt").addClass("avoid-clicks");
                   }
                   else {
                       play_correct_incorrect_sound(0);
                       $(this).addClass("wrongans");
                   }
               });
               break;
           case 2:
               $(".commontxt").click(function(){
                   if($(this).text()==data.string.p6salut1) {
                       $(this).addClass("correctans");
                       play_correct_incorrect_sound(1);
                       navigationcontroller(countNext,$total_page);
                       $(".commontxt").addClass("avoid-clicks");
                   }
                   else {
                       play_correct_incorrect_sound(0);
                       $(this).addClass("wrongans");
                   }
               });
               break;
           case 3:
               $(".commontxt").click(function(){
                   if($(this).text()==data.string.p6body2) {
                       $(this).addClass("correctans");
                       play_correct_incorrect_sound(1);
                       navigationcontroller(countNext,$total_page);
                       $(".commontxt").addClass("avoid-clicks");
                   }
                   else {
                       play_correct_incorrect_sound(0);
                       $(this).addClass("wrongans");
                   }
               });
               break;
           case 4:
               $(".commontxt").click(function(){
                   if($(this).text()==data.string.p6endin2) {
                       $(this).addClass("correctans");
                       play_correct_incorrect_sound(1);
                       navigationcontroller(countNext,$total_page);
                       $(".commontxt").addClass("avoid-clicks");
                   }
                   else {
                       play_correct_incorrect_sound(0);
                       $(this).addClass("wrongans");
                   }
               });
               break;
       }
   }
});
var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "backgr",
        uppertextblock: [
            {
                textclass: "topiccss",
                textdata: data.string.p2text1
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "writingletter",
                    imgclass: "writingletterimg",
                    imgid: 'writingletterImg',
                    imgsrc: "",
                },
            ]
        }]
    },
    //slide 1

    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "topic",
                textdata: data.string.p2text2
            }
        ],

        contents:[{
            div:[
                {

                    divcls:"commondiv heading1",
                    textclass: "commontext",
                    textdata: data.string.ltrheading
                },
                {
                    divcls:"salutation1",
                    textclass: "commontext",
                    textdata: data.string.ltrsalutation
                },
                {
                    divcls:"bodydiv1",
                    textclass: "commontext body1",
                    textdata: data.string.ltrbody
                },
                {
                    divcls:"ending1",
                    textclass: "commontext endingtext",
                    textdata: data.string.ltrending
                }
            ],
            buttons:[
                {
                   btncls:"commonbtn headingbtn",
                   btntxt:data.string.heading
                },
                {
                    btncls:"commonbtn salutationbtn",
                    btntxt:data.string.salutation
                },
                {
                    btncls:"commonbtn bodybtn",
                    btntxt:data.string.body
                },
                {
                    btncls:"commonbtn endingbtn",
                    btntxt:data.string.ending
                }

            ],
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "commonarrw arrowbtn1",
                        imgclass: "relativecls arrow",
                        imgid: 'arrowImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "commonarrw arrowbtn2",
                        imgclass: "relativecls arrow",
                        imgid: 'arrowImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "commonarrw arrowbtn3",
                        imgclass: "relativecls arrow",
                        imgid: 'arrowImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "commonarrw arrowbtn4",
                        imgclass: "relativecls arrow",
                        imgid: 'arrowImg',
                        imgsrc: "",
                    }
                ]
            }]
        }]

    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "topictitle title",
                textdata: data.string.p2text2_1
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "girlImgdiv",
                    imgclass: "relativecls girl1img",
                    imgid: 'girl1Img',
                    imgsrc: "",
                },
                {
                    imgdiv: "speechbox",
                    imgclass: "relativecls speechboximg",
                    imgid: 'speechbox1Img',
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "topic",
                textdata: data.string.p2text2
            }
        ],

        contents:[{
            div:[
                {

                    divcls:"commondiv heading1",
                    textclass: "commontext",
                    textdata: data.string.ltrheading
                },
                {
                    divcls:"salutation1",
                    textclass: "commontext",
                    textdata: data.string.ltrsalutation
                },
                {
                    divcls:"bodydiv1",
                    textclass: "commontext body1",
                    textdata: data.string.ltrbody
                },
                {
                    divcls:"ending1",
                    textclass: "commontext endingtext",
                    textdata: data.string.ltrending
                }
            ],
            buttons:[
                {
                    btncls:"commonbtn btnltr fadeInEffect headingbtn hbtn",
                    btntxt:data.string.heading
                },
                {
                    btncls:"commonbtn btnltr fadeInEffect salutationbtn sbtn",
                    btntxt:data.string.salutation
                },
                {
                    btncls:"commonbtn btnltr fadeInEffect bodybtn bbtn",
                    btntxt:data.string.body
                },
                {
                    btncls:"commonbtn btnltr fadeInEffect endingbtn ebtn",
                    btntxt:data.string.ending
                }

            ],
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "commonarrw arrowbtn1",
                        imgclass: "relativecls arrow",
                        imgid: 'arrowImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "commonarrw arrowbtn2",
                        imgclass: "relativecls arrow",
                        imgid: 'arrowImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "commonarrw arrowbtn3",
                        imgclass: "relativecls arrow",
                        imgid: 'arrowImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "commonarrw arrowbtn4",
                        imgclass: "relativecls arrow",
                        imgid: 'arrowImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "handicondiv handicon1 hbtn",
                        imgclass: "relativecls handiconimg",
                        imgid: 'handicon',
                        imgsrc: "",
                    }
                ]
            }]
        }]

    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textclass: "title2",
        textblock: [
            {
                textdata: data.string.p2text3
            }
        ],
        contents:[{
            div:[
                {

                    divcls:"commondiv heading1",
                    textclass: "commontext",
                    textdata: data.string.ltrheading
                }
                ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "girlheadingdiv",
                    imgclass: "relativecls girl2img",
                    imgid: 'girl2Img',
                    imgsrc: "",
                },
                {
                    imgdiv: "speechheadingbox",
                    imgclass: "relativecls speechbox2img",
                    imgid: 'speechbox2Img',
                    imgsrc: "",
                }
            ]
          }]
        }]
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textclass: "title3",
        textblock: [
            {
                textdata: data.string.p2text4
            }
        ],
        contents:[{
            div:[
                {
                    divcls:"salutation1",
                    textclass: "commontext",
                    textdata: data.string.ltrsalutation
                }
            ],
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "girlsalutationdiv",
                        imgclass: "relativecls girl2img",
                        imgid: 'girl2Img',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "speechsalutationbox",
                        imgclass: "relativecls speechbox2img",
                        imgid: 'speechbox2Img',
                        imgsrc: "",
                    }
                ]
            }]
        }]
    },
    //slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textclass: "title4",
        textblock: [
            {
                textdata: data.string.p2text5
            }
        ],
        contents:[{
            div:[
                {
                    divcls:"bodydiv2",
                    textclass: "body1",
                    textdata: data.string.ltrbody
                }
            ],
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "girlbodydiv",
                        imgclass: "relativecls girl2img",
                        imgid: 'girl2Img',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "speechbodybox",
                        imgclass: "relativecls speechbox2img",
                        imgid: 'speechbox2Img',
                        imgsrc: "",
                    }
                ]
            }]
        }]
    },
    //slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textclass: "title5",
        textblock: [
            {
                textdata: data.string.p2text6
            }
        ],
        contents:[{
            div:[
                {
                    divcls:"ending1",
                    textclass: "endingtext",
                    textdata: data.string.ltrending
                }
            ],
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "girlendingdiv",
                        imgclass: "relativecls girl2img",
                        imgid: 'girl2Img',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "speechendingbox",
                        imgclass: "relativecls speechbox2img",
                        imgid: 'speechbox2Img',
                        imgsrc: "",
                    }
                ]
            }]
        }]
    },
    //slide8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        contentblockcls:"popup1",
        textclass:"spchtxt",
        textblock: [
            {
                textclass: "spchtxt",
                textdata: data.string.p2text11
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "girl",
                    imgclass: "relativecls girl1img",
                    imgid: 'girl2Img',
                    imgsrc: "",
                },
                {
                    imgdiv: "speech",
                    imgclass: "relativecls speechboximg",
                    imgid: 'speechbox2Img',
                    imgsrc: "",
                },
                {
                    imgdiv: "audio audioicon1",
                    imgclass: "relativecls audioiconimg",
                    imgid: 'audioicon',
                    imgsrc: "",
                },
                {
                    imgdiv: "audio audioicon2",
                    imgclass: "relativecls audioiconimg",
                    imgid: 'audioicon',
                    imgsrc: "",
                },
                {
                    imgdiv: "audio audioicon3",
                    imgclass: "relativecls audioiconimg",
                    imgid: 'audioicon',
                    imgsrc: "",
                },
                {
                    imgdiv: "audio audioicon4",
                    imgclass: "relativecls audioiconimg",
                    imgid: 'audioicon',
                    imgsrc: "",
                }
            ]
        }],
        contents:[{
            div: [
                {
                    divcls: "emptydiv",
                },
                {
                    divcls: "head",
                    textclass: "",
                    textdata: data.string.heading
                },
                {
                    divcls: "headexp",
                    textclass: "",
                    textdata: data.string.p2text7
                },
                {
                    divcls: "saluat",
                    textclass: "",
                    textdata: data.string.salutation
                },
                {
                    divcls: "saluatexp",
                    textclass: "",
                    textdata: data.string.p2text8
                },
                {
                    divcls: "bdy",
                    textclass: "",
                    textdata: data.string.body
                },
                {
                    divcls: "bdyexp",
                    textclass: "",
                    textdata: data.string.p2text9
                },
                {
                    divcls: "end",
                    textclass: "",
                    textdata: data.string.ending
                },
                {
                    divcls: "endexp",
                    textclass: "",
                    textdata: data.string.p2text10
                }
            ]
        }]
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "common-css", src: $ref + "css/common.css", type: createjs.AbstractLoader.CSS},
            {id: "page1-css", src: $ref + "css/page1.css", type: createjs.AbstractLoader.CSS},

            {id: "girl1Img", src: imgpath+"girl_talking.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "handicon", src: imgpath+"hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "speechbox1Img", src: imgpath+"text_box01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "speechbox2Img", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
            {id: "girl2Img", src: imgpath+"GirlTalking/girltalking01.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "audioicon", src: "images/speaker.png", type: createjs.AbstractLoader.IMAGE},
            {id: "arrowImg", src: imgpath+"arrow.png", type: createjs.AbstractLoader.IMAGE},
            {id: "writingletterImg", src: imgpath+"writing_letter.gif", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_1", src: soundAsset+"p2_s0.ogg"},
            {id: "sound_2_1_0", src: soundAsset+"p2_s1_0.ogg"},
            {id: "sound_2_1_1", src: soundAsset+"p2_s1_1.ogg"},
            {id: "sound_2_1_2", src: soundAsset+"p2_s1_2.ogg"},
            {id: "sound_2_1_3", src: soundAsset+"p2_s1_3.ogg"},
            {id: "sound_2", src: soundAsset+"p2_s2.ogg"},
            {id: "sound_2_3_0", src: soundAsset+"p2_s3_0.ogg"},
            {id: "sound_2_3_1", src: soundAsset+"p2_s3_1.ogg"},
            {id: "sound_2_3_2", src: soundAsset+"p2_s3_2.ogg"},
            {id: "sound_2_3_3", src: soundAsset+"p2_s3_3.ogg"},
            {id: "sound_2_3_4", src: soundAsset+"p2_s3_4.ogg"},
            {id: "sound_2_3_5", src: soundAsset+"p2_s3_5.ogg"},
            {id: "sound_2_3_6", src: soundAsset+"p2_s3_6.ogg"},
            {id: "sound_2_3_7", src: soundAsset+"p2_s3_7.ogg"},
            {id: "sound_8", src: soundAsset+"p2_s8.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

    /*===============================================
    =            data highlight function            =
    ===============================================*/
    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function (index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

    /*=====  End of data highlight function  ======*/





    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);


        // highlight any text inside board div with datahighlightflag set true
        // texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        put_image2(content, countNext,preload);
        switch(countNext){
            case 0:
                sound_player("sound_1",true);
                break;
            case 1:
                showbtnoneafteranother();
                break;
            case 2:
                sound_player("sound_2",true,"girl1Img",imgpath+"girl_talking.png");
                break;
            case 3:
                animateBtn(count);
                break;
            case 4:
                break;
            case 5:
                break;
            case 6:
                break;
            case 7:
                break;
            case 8:
                sound_player("sound_8",true,'girl1img',imgpath+'GirlTalking/girltalking01.png',false);
                soundiconclick();
                break;
            default:
                navigationcontroller(countNext,$total_page);
                break;
        }
    }



    function sound_player(sound_id,navigate,imgId,changeImg,nextbtn) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller(countNext,$total_page):"";
            imgId!=''?$("."+imgId).attr("src",changeImg):'';
            if(nextbtn){
                $nextBtn.show(0);
                $prevBtn.hide(0)
            }
        });
    }





    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function put_image2(content, count,preload) {
        if (content[count].hasOwnProperty("contents")) {
            var contentCount=content[count].contents[0];
            var imageblockcontent=contentCount.hasOwnProperty('imageblock');
            dynamicimageload(imageblockcontent,contentCount,preload);
        }
    }
    function showbtnoneafteranother(){
        $(".commonBtn,.commonarrw").hide(0);
        setTimeout(function(){
            $(".headingbtn,.arrowbtn1").addClass("zoomInEffect").show(0);
            sound_player("sound_2_1_0",false);
            setTimeout(function(){
                $(".salutationbtn,.arrowbtn2").addClass("zoomInEffect").show(0);
                sound_player("sound_2_1_1",false);
                setTimeout(function(){
                    $(".bodybtn,.arrowbtn3").addClass("zoomInEffect").show(0);
                    sound_player("sound_2_1_2",false);
                    setTimeout(function(){
                        $(".endingbtn,.arrowbtn4").addClass("zoomInEffect").show(0);
                        sound_player("sound_2_1_3",false);
                        navigationcontroller(countNext,$total_page,false);
                    },2000);
                },2500);
            },3000);
        },2200);

    }
    function animateBtn(count){
        $(".commonBtn,.commonarrw").hide(0);
        count==0?$(".headingbtn,.arrowbtn1").addClass("zoomInEffect").show(0):'';
        sound_player("sound_2_3_0",false);
        if(count==1){
            $(".headingbtn,.arrowbtn1").addClass("grayout").removeClass("fadeInEffect").removeClass("btnltr").show(0);
            $(".salutationbtn,.arrowbtn2").addClass("zoomInEffect").show(0);
            $(".handicondiv").removeClass().addClass("handicondiv handicon2 sbtn");
            sound_player("sound_2_3_2",false);
        }
        if(count==2){
            $(".headingbtn,.arrowbtn1,.salutationbtn,.arrowbtn2").addClass("grayout").removeClass("fadeInEffect").removeClass("btnltr").show(0);
            $(".bodybtn,.arrowbtn3").addClass("zoomInEffect").show(0);
            $(".handicondiv").removeClass().addClass(" handicondiv handicon3 bbtn");
            sound_player("sound_2_3_4",false);
        }
        if(count==3){
            $(".headingbtn,.arrowbtn1,.salutationbtn,.arrowbtn2,.bodybtn,.arrowbtn3").addClass("grayout").removeClass("fadeInEffect").removeClass("btnltr").show(0);
            $(".endingbtn,.arrowbtn4").addClass("zoomInEffect").show(0);
            $(".handicondiv").removeClass().addClass("handicondiv handicon4 ebtn");
            sound_player("sound_2_3_6",false);
        }
        gottoExplanation();


    }

    function gottoExplanation(){
        $(".commonBtn,.handicondiv").click(function(){
            if($(this).hasClass("hbtn"))
            {
                countNext = 4;
                count=1;
                generaltemplate();
                sound_player("sound_2_3_1",false,'girl2img',imgpath+'GirlTalking/girltalking01.png',true);
                countNext=2;
            }
            else if($(this).hasClass("sbtn")){
                countNext = 5;
                count=2;
                generaltemplate();
                sound_player("sound_2_3_3",false,'girl2img',imgpath+'GirlTalking/girltalking01.png',true);
                countNext=2;
            }

            else if($(this).hasClass("bbtn")){
                countNext = 6;
                count=3;
                generaltemplate();
                sound_player("sound_2_3_5",false,'girl2img',imgpath+'GirlTalking/girltalking01.png',true);
                countNext=2;
            }
            else if($(this).hasClass("ebtn")){
                countNext = 7;
                generaltemplate();
                sound_player("sound_2_3_7",false,'girl2img',imgpath+'GirlTalking/girltalking01.png',true);
            }
            // $nextBtn.show(0);
        });
    }
    function soundiconclick(){
        $(".audio").click(function(){
            if($(this).hasClass("audioicon1")){
                sound_player("sound_2_3_1",false);
            }
            else if($(this).hasClass("audioicon2")){
                sound_player("sound_2_3_3",false);
            }
            else if($(this).hasClass("audioicon3")){
                sound_player("sound_2_3_5",false);
            }
            else if($(this).hasClass("audioicon4")){
                sound_player("sound_2_3_7",false);
            }
        });
    }
});
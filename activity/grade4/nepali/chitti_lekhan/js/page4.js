var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "dialtitle",
                textdata: data.string.p4text1
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "dialogbox",
                    imgclass: "relativecls dialogboximg",
                    imgid: 'dialogboxImg',
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide 1

    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "topic1",
                textdata: data.string.p4text2
            },
            {
                textclass: "dialtitle1",
                textdata: data.string.p4text3
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "dialogbox1",
                    imgclass: "relativecls dialogboximg",
                    imgid: 'dialogboxImg',
                    imgsrc: "",
                },
                {
                    imgdiv:" rounddiv div1",
                    imgclass: " image1 geetaimg",
                    imgid: 'geetaImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "rounddiv div2",
                    imgclass: "image2 dadimg",
                    imgid: 'dadImg',
                    imgsrc: "",
                }
            ]
        }]
    },
    // slide2
    {   contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        contents:[{
            div:[
                {
                    divcls:"headerdiv fadeInEffect",
                    textclass: "commontext heading1",
                    textdata: data.string.p4text4
                },
                {
                    divcls:"salutdiv fadeInEffect",
                    textclass: "commontext salutation1",
                    textdata: data.string.p4text5
                },
                {
                    divcls:"bdydiv fadeInEffect",
                    textclass: "commontext body1",
                    textdata: data.string.p4text6
                },
                {
                    divcls:"enddiv fadeInEffect",
                    textclass: "commontext ending1",
                    textdata: data.string.p4text7
                }
            ]
        }]
    },
    //slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "topic1",
                textdata: data.string.p4text8
            },
            {
                textclass: "dialtitle1",
                textdata: data.string.p4text9
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "dialogbox1",
                    imgclass: "relativecls dialogboximg",
                    imgid: 'dialogboxImg',
                    imgsrc: "",
                },
                {
                    imgdiv:" rounddiv div1",
                    imgclass: " image3 premimg",
                    imgid: 'premImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "rounddiv div2",
                    imgclass: "image4 didiimg",
                    imgid: 'didiImg',
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide4
    {   contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        contents:[{
            div:[
                {
                    divcls:"headerdiv1 fadeInEffect",
                    textclass: "commontext heading1",
                    textdata: data.string.p4text10
                },
                {
                    divcls:"salutdiv fadeInEffect",
                    textclass: "commontext salutation1",
                    textdata: data.string.p4text11
                },
                {
                    divcls:"bdydiv fadeInEffect",
                    textclass: "commontext body1",
                    textdata: data.string.p4text12
                },
                {
                    divcls:"enddiv1 fadeInEffect",
                    textclass: "commontext ending1",
                    textdata: data.string.p4text13
                }
            ]
        }]
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "common-css", src: $ref + "css/common.css", type: createjs.AbstractLoader.CSS},
            {id: "page1-css", src: $ref + "css/page1.css", type: createjs.AbstractLoader.CSS},

            {id: "girlImg", src: imgpath+"girl05.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialogboxImg", src: imgpath+"text_box03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "geetaImg", src: imgpath+"geeta.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dadImg", src: imgpath+"dad.png", type: createjs.AbstractLoader.IMAGE},
            {id: "premImg", src: imgpath+"prem.png", type: createjs.AbstractLoader.IMAGE},
            {id: "didiImg", src: imgpath+"didi.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_1", src: soundAsset+"p4_s0.ogg"},
            {id: "sound_2", src: soundAsset+"p4_s1.ogg"},
            {id: "sound_3", src: soundAsset+"p4_s3.ogg"},
            // {id: "sound_4", src: soundAsset+"p1_s3.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

    /*===============================================
    =            data highlight function            =
    ===============================================*/
    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function (index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

    /*=====  End of data highlight function  ======*/



    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);


        // highlight any text inside board div with datahighlightflag set true
        // texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        switch(countNext){
            case 0:
                sound_player("sound_1",true);
                break;
            case 1:
                sound_player("sound_2",true);
                break;
            case 3:
                sound_player("sound_3",true);
                break;
            default:
                navigationcontroller(countNext,$total_page);
                break;
        }
    }



    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller(countNext,$total_page):"";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

});
var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "dialtitle",
                textdata: data.string.p5text1
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "dialogbox",
                    imgclass: "relativecls dialogboximg",
                    imgid: 'dialogboxImg',
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "titletop",
                textdata: data.string.p5text2
            }
        ],
        btnblock:[
            {
                btndiv:"commonbutton btn1",
                btncls:"father",
                btntext:data.string.father
            },
            {
                btndiv:"commonbutton btn2",
                btncls:"mother",
                btntext:data.string.mother
            },
            {
                btndiv:"commonbutton btn3",
                btncls:"brother",
                btntext:data.string.brother
            },
            {
                btndiv:"commonbutton btn4",
                btncls:"sister",
                btntext:data.string.sister
            }
        ]
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "titletop",
                textdata: data.string.p5text3
            }
        ],
        btnblock:[
              {
                  btndiv:"showoption slideL namediv",
                  btncls:"name",
                  btntext:data.string.p5text4,
                  option:[
                      {
                          divcls:"commonnm name1",
                          divtext:data.string.male
                      },
                      {
                          divcls:"commonnm name2",
                          divtext:data.string.female
                      }
                  ]
              },
              {
                  btndiv:"showoption slideL countrydiv",
                  btncls:"country",
                  btntext:data.string.p5text5,
                  option:[
                      {
                          divcls:"country1",
                          divtext:data.string.country,
                      }
                  ]
              },
              {
                  btndiv:"showoption slideL districtdiv",
                  btncls:"district",
                  btntext:data.string.p5text6,
                  option:[
                      {
                          divcls:"district1",
                          divtype:"dropdown"
                      }
                  ]
              },
              {
                  btndiv:"showoption slideL warddiv",
                  btncls:"ward",
                  btntext:data.string.p5text7,
                  option:[
                      {
                          divcls:"ward1",
                          divtype:"dropdown"
                      }
                  ]
              }
        ]
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "titletop1",
                textdata: data.string.p5text8
            }
        ],
        messageblock:[
            {
                msgdiv:"msgdiv",
                msgtext:[
                    {
                        textcls:"text1",
                        textdata:data.string.p5text9
                    },
                    {
                        textcls:"option1 optclr",
                        textdata:data.string.option1,
                        option:true
                    },
                    {
                        textcls:"option1 optclr",
                        textdata:data.string.option2,
                    },
                    {
                        textcls:"text2",
                        textdata:data.string.p5text10,
                    },
                    {
                        textcls:"option2 optclr",
                        textdata:data.string.option3,
                        option:true
                    },
                    {
                        textcls:"option2 optclr",
                        textdata:data.string.option4
                    },
                    {
                        textcls:"text3",
                        textdata:data.string.p5text11,
                    },
                    {
                        textcls:"option3 optclr",
                        textdata:data.string.option5,
                        option:true
                    },
                    {
                        textcls:"option3 optclr",
                        textdata:data.string.option6
                    },
                    {
                        textcls:"text4",
                        textdata:data.string.p5text12,
                    },
                    {
                        textcls:"option4 optclr",
                        textdata:data.string.option7,
                        option:true
                    },
                    {
                        textcls:"option4 optclr",
                        textdata:data.string.option8
                    },
                    {
                        textcls:"text5",
                        textdata:data.string.p5text13
                    },
                    {
                        textcls:"option5 optclr",
                        textdata:data.string.option9,
                        option:true
                    },
                    {
                        textcls:"option5 optclr",
                        textdata:data.string.option10
                    },
                    {
                        textcls:"text6",
                        textdata:data.string.p5text14
                    },
                    {
                        textcls:"option6 optclr",
                        textdata:data.string.option11,
                        option:true
                    },
                    {
                        textcls:"option6 optclr",
                        textdata:data.string.option12
                    },
                    {
                        textcls:"text7",
                        textdata:data.string.p5text15
                    }
                ]

            }
        ]
    },
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "titletop",
                textdata: data.string.p5text16
            },
            {
                textclass: "spchtxt header1",
                textdata: "head"
            },
            {
                textclass: "spchtxt header2",
                textdata: data.string.date1
            },
            {
                textclass: "spchtxt salut1",
                textdata: "salut"
            },
            {
                textclass: "spchtxt end1",
                textdata: "end1"
            },
            {
                textclass: "spchtxt end2",
                textdata: "end2"
            }
        ],
        messageblock:[
            {
                msgdiv:"msgdiv1",
                msgtext:[
                    {
                        textcls:"spchtxt text1",
                        textdata:data.string.p5text9
                    },
                    {
                        textcls:"spchtxt option1 optclr",
                        ans:true
                    },
                    {
                        textcls:"spchtxt text2",
                        textdata:data.string.p5text10,
                    },
                    {
                        textcls:"spchtxt option2 optclr",
                        ans:true
                    },
                    {
                        textcls:"spchtxt text3",
                        textdata:data.string.p5text11,
                    },
                    {
                        textcls:"spchtxt option3 optclr",
                        ans:true
                    },
                    {
                        textcls:"spchtxt text4",
                        textdata:data.string.p5text12,
                    },
                    {
                        textcls:"spchtxt option4 optclr",
                        ans:true
                    },
                    {
                        textcls:"spchtxt text5",
                        textdata:data.string.p5text13
                    },
                    {
                        textcls:"spchtxt option5 optclr",
                        ans:true
                    },
                    {
                        textcls:"spchtxt text6",
                        textdata:data.string.p5text14
                    },
                    {
                        textcls:"spchtxt option6 optclr",
                        ans:true
                    },
                    {
                        textcls:"spchtxt text7",
                        textdata:data.string.p5text15
                    }
                ]

            }
        ]
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var header='';
    var salutation='';
    var district='';
    var ward = '';
    var body='';
    var ending='';
    var gender='';
    var opt1='';
    var opt2='';
    var opt3='';
    var opt4='';
    var opt5='';
    var opt6='';
    var letterto=''
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "common-css", src: $ref + "css/common.css", type: createjs.AbstractLoader.CSS},
            {id: "page1-css", src: $ref + "css/page1.css", type: createjs.AbstractLoader.CSS},

            {id: "girlImg", src: imgpath+"girl05.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialogboxImg", src: imgpath+"text_box03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "geetaImg", src: imgpath+"geeta.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dadImg", src: imgpath+"dad.png", type: createjs.AbstractLoader.IMAGE},
            {id: "premImg", src: imgpath+"prem.png", type: createjs.AbstractLoader.IMAGE},
            {id: "didiImg", src: imgpath+"didi.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_1", src: soundAsset+"p5_s0.ogg"},
            {id: "sound_2", src: soundAsset+"p5_s1.ogg"},
            {id: "sound_3", src: soundAsset+"p5_s2.ogg"},
            {id: "sound_4", src: soundAsset+"p5_s3.ogg"},
            {id: "sound_5", src: soundAsset+"p5_s4.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

    /*===============================================
    =            data highlight function            =
    ===============================================*/
    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function (index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

    /*=====  End of data highlight function  ======*/



    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        switch(countNext){
            case 0:
                sound_player("sound_1",true);
                break;
            case 1:
                sound_player("sound_2",false);
                btnclick();
                break;
            case 2:
                sound_player("sound_3",false);
                selectans();
                break;
            case 3:
                sound_player("sound_4",false);
                bodyans();
                break;
            case 4:
                sound_player("sound_5",true);
                letterans();
                break;
            default:
                navigationcontroller(countNext,$total_page);
                break;
        }
    }



    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller(countNext,$total_page):"";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });
    function btnclick() {
        $(".commonbutton").click(function () {
            $(".commonbutton").addClass("avoid-clicks");
            if($(this).hasClass("btn1"))
            {
                salutation = data.string.salutfth;
                letterto = "father";
                $(this).css("background-color","#FFD966");
                $(this).css("color","#43434B");
            }
            else if ($(this).hasClass("btn2")){
                salutation = data.string.salutmth;
                letterto = "mother";
                $(this).css("background-color","#FFD966");
                $(this).css("color","#43434B");

            }
            else if ($(this).hasClass("btn3")){
                salutation = data.string.salutbth;
                letterto = "brother";
                $(this).css("background-color","#FFD966");
                $(this).css("color","#43434B");

            }
            else if ($(this).hasClass("btn4")){
                salutation = data.string.salutsth;
                letterto = "sister";
                $(this).css("background-color","#FFD966");
                $(this).css("color","#43434B");
            }
            navigationcontroller(countNext,$total_page);
        });
    }
    function selectans(){
        $('.district1').append('<option hidden selected>'+data.string.choose+'</option>');
        var i=1;
        while(i<7) {
            var $ival = i.toString();
            $('.district1').append($('<option>', {
                value: data.string[("d" + $ival)],
                text: data.string[("d" + $ival)]
            }));
            i++;
        }
        var wards = [
            {value: "1", text: "१"},
            {value: "2", text: "२"},
            {value: "3", text: "३"},
            {value: "4", text: "४"},
            {value: "5", text: "५"},
            {value: "6", text: "६"},
            {value: "7", text: "७"},
            {value: "8", text: "८"},
            {value: "9", text: "९"},
            {value: "10", text: "१०"},
            {value: "11", text: "११"},
            {value: "12", text: "१२"},
            {value: "13", text: "१३"},
            {value: "14", text: "१४"},
            {value: "15", text: "१५"},
            {value: "16", text: "१६"},
            {value: "17", text: "१७"},
            {value: "18", text: "१८"},
            {value: "19", text: "१९"},
            {value: "20", text: "२०"},
            {value: "21", text: "२१"},
            {value: "22", text: "२२"},
            {value: "23", text: "२३"},
            {value: "24", text: "२४"},
            {value: "25", text: "२५"},
            {value: "26", text: "२६"},
            {value: "27", text: "२७"},
            {value: "28", text: "२८"},
            {value: "29", text: "२९"},
            {value: "30", text: "३०"}
        ];
        $('.ward1').append('<option hidden selected>'+data.string.choose+'</option>');
        $.each(wards, function (i, item) {
                $('.ward1').append($('<option>', {
                    value: item.value,
                    text: item.text
                }));
        });
        $(".showoption").hide(0);
        showoption();
    }
    function showoption(){
        $(".namediv").show(0);
        $(".commonnm").click(function () {
            ending=$(this).text();
            if($(this).hasClass("name1")) {
                gender = "male";
                $(".name1").css({"background-color":"#FFD966","color":"black"});
                $(".name2").css({"background-color":"#377DCB","color":"white"});
            }
            else {
                gender = "female";
                $(".name2").css({"background-color":"#FFD966","color":"black"});
                $(".name1").css({"background-color":"#377DCB","color":"white"});
            }
            $(".countrydiv").show(0);
            setTimeout(function(){
                $(".districtdiv").show(0);
            },2000);
        });
        $(".district1").change(function() {
            header = $(this).find(":selected").text();
        //     $(".warddiv").show(0);
        // });
        // $(".ward1").change(function() {
        //     ward = $(this).find(":selected").text();
        //     header = district +" - "+ward + ", " +data.string.country;
            navigationcontroller(countNext,$total_page,false);
        });
    }
    function bodyans(){
        $(".option1").click(function(){
            $(".option1").css("background-color","white");
             opt1 = $(this).text().toString().replace("(","").replace(")","");
             $(this).css("background-color","#FFD966");
             navigate();
        });
        $(".option2").click(function(){
            $(".option2").css("background-color","white");
            opt2 = $(this).text().toString().replace("(","").replace(")","");
            $(this).css("background-color","#FFD966");
            navigate();
        });
        $(".option3").click(function(){
            $(".option3").css("background-color","white");
            opt3 = $(this).text().toString().replace("(","").replace(")","");
            $(this).css("background-color","#FFD966");
            navigate();
        });
        $(".option4").click(function(){
            $(".option4").css("background-color","white");
            opt4 = $(this).text().toString().replace("(","").replace(")","");
            $(this).css("background-color","#FFD966");
            navigate();
        });
        $(".option5").click(function(){
            $(".option5").css("background-color","white");
            opt5 = $(this).text().toString().replace("(","").replace(")","");
            $(this).css("background-color","#FFD966");
            navigate();
        });
        $(".option6").click(function(){
            $(".option6").css("background-color","white");
            opt6 = $(this).text().toString().replace("(","").replace(")","");
            $(this).css("background-color","#FFD966");
            navigate();
        });

    }
    function navigate(){
        if(opt1!='' && opt2!='' && opt3!='' && opt4!='' && opt5!='' && opt6!=''){
            navigationcontroller(countNext,$total_page)
        }
    }
    function letterans(){
        $(".header1").text(header);
        $(".salut1").text(salutation);
        if(gender=="male") {
            $(".end1").text(salutation);
            if(letterto=='father' || letterto=='mother')
                $(".end1").text(data.string.fthmale);
            else if(letterto=='brother')
                $(".end1").text(data.string.bthmale);
            else if(letterto=='sister')
                $(".end1").text(data.string.sismale);
        }
        else {
            $(".end1").text(salutation);
            if(letterto=='father' || letterto=='mother')
                $(".end1").text(data.string.fthfemale);
            else if(letterto=='brother')
                $(".end1").text(data.string.bthfemale);
            else if(letterto=='sister')
                $(".end1").text(data.string.sisfemale);
        }
        $(".end2").text(ending);
        $(".option1").text(opt1).css("color","black");
        $(".option2").text(opt2).css("color","black");
        $(".option3").text(opt3).css("color","black");
        $(".option4").text(opt4).css("color","black");
        $(".option5").text(opt5).css("color","black");
        $(".option6").text(opt6).css("color","black");

    }
});

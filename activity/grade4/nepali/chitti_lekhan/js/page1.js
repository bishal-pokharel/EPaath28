var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "chapter",
                textdata: data.string.lesson_title
            }
        ]
    },
     //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        uppertextblockadditionalclass:"topictitle",
        uppertextblock: [
            {
                textclass: "text1",
                textdata: data.string.p1text1
            }
        ],
        imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "girl1",
                        imgclass: "relativecls girl1img",
                        imgid: 'girl1Img',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "speechbox1",
                        imgclass: "relativecls speechbox1img",
                        imgid: 'speechbox1Img',
                        imgsrc: "",
                    }
                    ]
              }]
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        uppertextblockadditionalclass:"topictitle",
        uppertextblock: [
            {
                textclass: "text1",
                textdata: data.string.p1text2
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "girl2",
                    imgclass: "relativecls girl2img",
                    imgid: 'girl2Img',
                    imgsrc: "",
                },
                {
                    imgdiv: "speechbox1",
                    imgclass: "relativecls speechbox1img",
                    imgid: 'speechbox1Img',
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        uppertextblockadditionalclass:"topictitle",
        uppertextblock: [
            {
                textclass: "text1",
                textdata: data.string.p1text3
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "girl3",
                    imgclass: "relativecls girl3img",
                    imgid: 'girl3Img',
                    imgsrc: "",
                },
                {
                    imgdiv: "speechbox1",
                    imgclass: "relativecls speechbox1img",
                    imgid: 'speechbox1Img',
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        uppertextblockadditionalclass:"topictitle slideOutwardLeft",
        uppertextblock: [
            {
                textclass: "text1",
                textdata: data.string.p1text3
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "girl3 slideOutwardLeft",
                    imgclass: "relativecls girl3img",
                    imgid: 'girl4Img',
                    imgsrc: "",
                },
                {
                    imgdiv: "speechbox1 slideOutwardLeft",
                    imgclass: "relativecls speechbox1img",
                    imgid: 'speechbox1Img',
                    imgsrc: "",
                },

            ]
        }],
        content:[{
           div:[

               {
                   divcls:"hideDiv title1",
                   textclass: "text1",
                   textdata: data.string.title
               },
               {

                   divcls:"hideDiv commondiv popupdiv",
                   textclass: "header commontext",
                   textdata: data.string.ltrheading
               },
               {
                   divcls:"hideDiv  salutation",
                   textclass: "commontext",
                   textdata: data.string.ltrsalutation
               },
               {
                   divcls:"hideDiv  body",
                   textclass: "body1 commontext",
                   textdata: data.string.ltrbody
               },
               {
                   divcls:"hideDiv  ending",
                   textclass: "endingtext commontext",
                   textdata: data.string.ltrending
               },
               {
                   divcls:"slideOutwardRight hideDiv coverdiv",

               }
           ]
        }]
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "common-css", src: $ref + "css/common.css", type: createjs.AbstractLoader.CSS},
            {id: "page1-css", src: $ref + "css/page1.css", type: createjs.AbstractLoader.CSS},

            {id: "girl1Img", src: imgpath+"GirlTalking/girl_talking.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "speechbox1Img", src: imgpath+"text_box01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "girl2Img", src: imgpath+"GirlTalking/girltalking02.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "girl3Img", src: imgpath+"GirlTalking/girltalking03.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "girl4Img", src: imgpath+"GirlTalking/girltalking03.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_1", src: soundAsset+"p1_s0.ogg"},
            {id: "sound_2", src: soundAsset+"p1_s1.ogg"},
            {id: "sound_3", src: soundAsset+"p1_s2.ogg"},
            {id: "sound_4", src: soundAsset+"p1_s3.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

    /*===============================================
    =            data highlight function            =
    ===============================================*/
    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function (index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

    /*=====  End of data highlight function  ======*/





    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);


        // highlight any text inside board div with datahighlightflag set true
        // texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        switch(countNext){
            case 0:
                sound_player("sound_1",true);
                break;
            case 1:
                sound_player("sound_2",true,"girl1Img",imgpath+"GirlTalking/girl_talking.png");
                break;
            case 2:
                sound_player("sound_3",true,"girl2Img",imgpath+"girl01.png");
                break;
            case 3:
                sound_player("sound_4",true,"girl3Img",imgpath+"GirlTalking/girltalking03.png");
                break;
            case 4:
                animateDiv();
                break;
            default:
                navigationcontroller(countNext,$total_page);
                break;
        }
    }



    function sound_player(sound_id,navigate,imgId,changeImg) {
        console.log();
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller(countNext,$total_page):"";
            $("."+imgId).attr("src",changeImg);
        });
    }





    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });


    function animateDiv(){
        $(".hideDiv").addClass("hide");
        setTimeout(function(){
            $(".hideDiv").addClass("show");
            setTimeout(function () {
                navigationcontroller(countNext,$total_page,false)
            },4000);
        },1000);

    }
});
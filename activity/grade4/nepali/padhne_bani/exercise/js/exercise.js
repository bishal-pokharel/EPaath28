var imgpath = $ref + "/exercise/images/";
var soundAsset = $ref+"/sounds/";


var content = [
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "contentwithbg",
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'bgmain',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg1",
                    imgclass: "speechimg1",
                    imgid: 'bgImgsp1',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg2",
                    imgclass: "speechimg2",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg3" ,
                    imgclass: "speechimg3",
                    imgid: 'bgImgsp1',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg4" ,
                    imgclass: "speechimg4",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv: "people1" ,
                    imgclass: "people",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv:"people2" ,
                    imgclass: "people_sec",
                    imgid: 'bgImg0',
                    imgsrc: ""
                },
                {
                    imgdiv:"sundarDiv" ,
                    imgclass: "sundar",
                    imgid: 'sundar',
                    imgsrc: ""
                }

            ]
        }],
        textblock:[
            {
                textdiv:"text1",
                textclass:"centertext chapter",
                textdata:data.string.e1s0
            },

            {
                textdiv:"violet draggable",
                textclass:"centertext chapter",
                textdata:data.string.e1s4,
                ans:data.string.e1s4
            },
            {
                textdiv:"indigo draggable",
                textclass:"centertext chapter",
                textdata:data.string.e1s5,
                ans:data.string.e1s5
            },
            {
                textdiv:"speechtxt1",
                textclass:"centertext chapter",
                textdata: data.string.e1s1,
                ans: ''
            },
            {
                textdiv:"speechtxt2",
                textclass:"centertext chapter",
                textdata: data.string.e1s2,
                ans: ''
            },
            {
                textdiv:"speechtxt3",
                textclass:"centertext chapter",
                textdata: data.string.e1s3,
                ans: ''
            },
            {
                textdiv:"speechtxt4 droppable",
                textclass:"centertext chapter",
                textdata:data.string.e1s4,
                ans:data.string.e1s4
            },

        ],
    },
    //slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "contentwithbg",
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'bgmain',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg1",
                    imgclass: "speechimg1",
                    imgid: 'bgImgsp1',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg2",
                    imgclass: "speechimg2",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg3" ,
                    imgclass: "speechimg3",
                    imgid: 'bgImgsp1',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg4" ,
                    imgclass: "speechimg4",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv: "people1" ,
                    imgclass: "people",
                    imgid: 'bgImg1',
                    imgsrc: ""
                },
                {
                    imgdiv:"people2" ,
                    imgclass: "people_sec",
                    imgid: 'bgImg2',
                    imgsrc: ""
                },
                {
                    imgdiv:"sundarDiv" ,
                    imgclass: "sundar",
                    imgid: 'sundar',
                    imgsrc: ""
                }

            ]
        }],
        textblock:[
            {
                textdiv:"text1",
                textclass:"centertext chapter",
                textdata:data.string.e1s0
            },

            {
                textdiv:"violet draggable",
                textclass:"centertext chapter",
                textdata:data.string.e1s10,
                ans:data.string.e1s10
            },
            {
                textdiv:"indigo draggable",
                textclass:"centertext chapter",
                textdata:data.string.e1s9,
                ans:data.string.e1s9
            },
            {
                textdiv:"speechtxt1",
                textclass:"centertext chapter",
                textdata: data.string.e1s6,
                ans: ''
            },
            {
                textdiv:"speechtxt2",
                textclass:"centertext chapter",
                textdata: data.string.e1s7,
                ans: ''
            },
            {
                textdiv:"speechtxt3 droppable",
                textclass:"centertext chapter",
                textdata:data.string.e1s10,
                ans:data.string.e1s10
            },
            {
                textdiv:"speechtxt4",
                textclass:"centertext chapter",
                textdata: data.string.e1s8,
                ans: ''
            },
          ],
    },
    //slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "contentwithbg",
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'bgmain',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg1",
                    imgclass: "speechimg1",
                    imgid: 'bgImgsp1',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg2",
                    imgclass: "speechimg2",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg3" ,
                    imgclass: "speechimg3",
                    imgid: 'bgImgsp1',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg4" ,
                    imgclass: "speechimg4",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv: "people1" ,
                    imgclass: "people",
                    imgid: 'bgImg3',
                    imgsrc: ""
                },
                {
                    imgdiv:"people2" ,
                    imgclass: "people_sec width96",
                    imgid: 'bgImg4',
                    imgsrc: ""
                },
                {
                    imgdiv:"sundarDiv" ,
                    imgclass: "sundar",
                    imgid: 'sundar',
                    imgsrc: ""
                }

            ]
        }],
        textblock:[
            {
                textdiv:"text1",
                textclass:"centertext chapter",
                textdata:data.string.e1s0
            },

            {
                textdiv:"violet draggable",
                textclass:"centertext chapter",
                textdata:data.string.e1s15,
                ans:data.string.e1s15
            },
            {
                textdiv:"indigo draggable",
                textclass:"centertext chapter",
                textdata:data.string.e1s14,
                ans:data.string.e1s9
            },
            {
                textdiv:"speechtxt1",
                textclass:"centertext chapter",
                textdata: data.string.e1s11,
                ans: ''
            },
            {
                textdiv:"speechtxt2 droppable",
                textclass:"centertext chapter",
                textdata: data.string.e1s15,
                ans: data.string.e1s15
            },
            {
                textdiv:"speechtxt3",
                textclass:"centertext chapter",
                textdata:data.string.e1s12,
                ans:data.string.e1s12
            },
            {
                textdiv:"speechtxt4",
                textclass:"centertext chapter",
                textdata: data.string.e1s13,
                ans: data.string.e1s13
            },
          ],
    },
    //slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "contentwithbg",
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'bgmain',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg1",
                    imgclass: "speechimg1",
                    imgid: 'bgImgsp1',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg2",
                    imgclass: "speechimg2",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg3" ,
                    imgclass: "speechimg3",
                    imgid: 'bgImgsp1',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg4" ,
                    imgclass: "speechimg4",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv: "people1" ,
                    imgclass: "people width96",
                    imgid: 'bgImg5',
                    imgsrc: ""
                },
                {
                    imgdiv:"people2" ,
                    imgclass: "people_sec",
                    imgid: 'bgImg6',
                    imgsrc: ""
                },
                {
                    imgdiv:"sundarDiv" ,
                    imgclass: "sundar",
                    imgid: 'sundar',
                    imgsrc: ""
                }

            ]
        }],
        textblock:[
            {
                textdiv:"text1",
                textclass:"centertext chapter",
                textdata:data.string.e1s0
            },

            {
                textdiv:"violet draggable",
                textclass:"centertext chapter",
                textdata:data.string.e1s20,
                ans:data.string.e1s20
            },
            {
                textdiv:"indigo draggable",
                textclass:"centertext chapter",
                textdata:data.string.e1s19,
                ans:data.string.e1s19
            },
            {
                textdiv:"speechtxt1",
                textclass:"centertext chapter",
                textdata: data.string.e1s16,
                ans: ''
            },
            {
                textdiv:"speechtxt2",
                textclass:"centertext chapter",
                textdata: data.string.e1s17,
                ans: ''
            },
            {
                textdiv:"speechtxt3",
                textclass:"centertext chapter",
                textdata:data.string.e1s18,
                ans:''
            },
            {
                textdiv:"speechtxt4 droppable",
                textclass:"centertext chapter",
                textdata: data.string.e1s20,
                ans: data.string.e1s20
            },
          ],
    },
    //slide5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "contentwithbg",
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'bgmain',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg1",
                    imgclass: "speechimg1",
                    imgid: 'bgImgsp1',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg2",
                    imgclass: "speechimg2",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg3" ,
                    imgclass: "speechimg3",
                    imgid: 'bgImgsp1',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg4" ,
                    imgclass: "speechimg4",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv: "people1" ,
                    imgclass: "people",
                    imgid: 'bgImg7',
                    imgsrc: ""
                },
                {
                    imgdiv:"people2" ,
                    imgclass: "people_sec width96",
                    imgid: 'bgImg8',
                    imgsrc: ""
                },
                {
                    imgdiv:"sundarDiv" ,
                    imgclass: "sundar",
                    imgid: 'sundar',
                    imgsrc: ""
                }

            ]
        }],
        textblock:[
            {
                textdiv:"text1",
                textclass:"centertext chapter",
                textdata:data.string.e1s0
            },

            {
                textdiv:"violet draggable",
                textclass:"centertext chapter",
                textdata:data.string.e1s25,
                ans:data.string.e1s25
            },
            {
                textdiv:"indigo draggable",
                textclass:"centertext chapter",
                textdata:data.string.e1s24,
                ans:data.string.e1s24
            },
            {
                textdiv:"speechtxt1",
                textclass:"centertext chapter",
                textdata: data.string.e1s21,
                ans: ''
            },
            {
                textdiv:"speechtxt2",
                textclass:"centertext chapter",
                textdata: data.string.e1s22,
                ans: ''
            },
            {
                textdiv:"speechtxt3 droppable",
                textclass:"centertext chapter",
                textdata:data.string.e1s25,
                ans: data.string.e1s25
            },
            {
                textdiv:"speechtxt4",
                textclass:"centertext chapter",
                textdata: data.string.e1s23,
                ans:''
            }
          ],
    },
    //slide6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "contentwithbg",
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'bgmain',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg1",
                    imgclass: "speechimg1",
                    imgid: 'bgImgsp1',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg2",
                    imgclass: "speechimg2",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg3" ,
                    imgclass: "speechimg3",
                    imgid: 'bgImgsp1',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg4" ,
                    imgclass: "speechimg4",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv: "people1" ,
                    imgclass: "people",
                    imgid: 'bgImg9',
                    imgsrc: ""
                },
                {
                    imgdiv:"people2" ,
                    imgclass: "people_sec width55",
                    imgid: 'bgImg10',
                    imgsrc: ""
                },
                {
                    imgdiv:"sundarDiv" ,
                    imgclass: "sundar",
                    imgid: 'sundar',
                    imgsrc: ""
                }

            ]
        }],
        textblock:[
            {
                textdiv:"text1",
                textclass:"centertext chapter",
                textdata:data.string.e1s0
            },

            {
                textdiv:"violet draggable",
                textclass:"centertext chapter",
                textdata:data.string.e1s30,
                ans:data.string.e1s30
            },
            {
                textdiv:"indigo draggable",
                textclass:"centertext chapter",
                textdata:data.string.e1s29,
                ans:data.string.e1s29
            },
            {
                textdiv:"speechtxt1",
                textclass:"centertext chapter",
                textdata: data.string.e1s26,
                ans: ''
            },
            {
                textdiv:"speechtxt2",
                textclass:"centertext chapter",
                textdata: data.string.e1s27,
                ans: ''
            },
            {
                textdiv:"speechtxt3 droppable",
                textclass:"centertext chapter",
                textdata:data.string.e1s30,
                ans:data.string.e1s30
            },
            {
                textdiv:"speechtxt4",
                textclass:"centertext chapter",
                textdata: data.string.e1s28,
                ans: ''
            },
          ],
    },
    //slide7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "contentwithbg",
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'bgmain',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg1",
                    imgclass: "speechimg1",
                    imgid: 'bgImgsp1',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg2",
                    imgclass: "speechimg2",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg3" ,
                    imgclass: "speechimg3",
                    imgid: 'bgImgsp1',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg4" ,
                    imgclass: "speechimg4",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv: "people1" ,
                    imgclass: "people",
                    imgid: 'bgImg11',
                    imgsrc: ""
                },
                {
                    imgdiv:"people2" ,
                    imgclass: "people_sec",
                    imgid: 'bgImg12',
                    imgsrc: ""
                },
                {
                    imgdiv:"sundarDiv" ,
                    imgclass: "sundar",
                    imgid: 'sundar',
                    imgsrc: ""
                }

            ]
        }],
        textblock:[
            {
                textdiv:"text1",
                textclass:"centertext chapter",
                textdata:data.string.e1s0
            },

            {
                textdiv:"violet draggable",
                textclass:"centertext chapter",
                textdata:data.string.e1s35,
                ans:data.string.e1s35
            },
            {
                textdiv:"indigo draggable",
                textclass:"centertext chapter",
                textdata:data.string.e1s34,
                ans:data.string.e1s34
            },
            {
                textdiv:"speechtxt1",
                textclass:"centertext chapter",
                textdata: data.string.e1s31,
                ans: ''
            },
            {
                textdiv:"speechtxt2 droppable",
                textclass:"centertext chapter",
                textdata: data.string.e1s35,
                ans: data.string.e1s35
            },
            {
                textdiv:"speechtxt3",
                textclass:"centertext chapter",
                textdata:data.string.e1s32,
                ans:''
            },
            {
                textdiv:"speechtxt4",
                textclass:"centertext chapter",
                textdata: data.string.e1s33,
                ans: ''
            },
          ],
    },
    //slide8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "contentwithbg",
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'bgmain',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg1",
                    imgclass: "speechimg1",
                    imgid: 'bgImgsp1',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg2",
                    imgclass: "speechimg2",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg3" ,
                    imgclass: "speechimg3",
                    imgid: 'bgImgsp1',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg4" ,
                    imgclass: "speechimg4",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv: "people1" ,
                    imgclass: "people",
                    imgid: 'bgImg13',
                    imgsrc: ""
                },
                {
                    imgdiv:"people2" ,
                    imgclass: "people_sec",
                    imgid: 'bgImg14',
                    imgsrc: ""
                },
                {
                    imgdiv:"sundarDiv" ,
                    imgclass: "sundar",
                    imgid: 'sundar',
                    imgsrc: ""
                }

            ]
        }],
        textblock:[
            {
                textdiv:"text1",
                textclass:"centertext chapter",
                textdata:data.string.e1s0
            },

            {
                textdiv:"violet draggable",
                textclass:"centertext chapter",
                textdata:data.string.e1s40,
                ans:data.string.e1s40
            },
            {
                textdiv:"indigo draggable",
                textclass:"centertext chapter",
                textdata:data.string.e1s39,
                ans:data.string.e1s39
            },
            {
                textdiv:"speechtxt1",
                textclass:"centertext chapter",
                textdata: data.string.e1s36,
                ans: ''
            },
            {
                textdiv:"speechtxt2 droppable",
                textclass:"centertext chapter",
                textdata: data.string.e1s40,
                ans: data.string.e1s40
            },
            {
                textdiv:"speechtxt3",
                textclass:"centertext chapter",
                textdata:data.string.e1s37,
                ans:''
            },
            {
                textdiv:"speechtxt4",
                textclass:"centertext chapter",
                textdata: data.string.e1s38,
                ans: ''
            },
            {
                imgdiv:"sundarDiv" ,
                imgclass: "sundar",
                imgid: 'sundar',
                imgsrc: ""
            }
          ],
    },
    //slide 9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "contentwithbg",
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'bgmain',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg1",
                    imgclass: "speechimg1",
                    imgid: 'bgImgsp1',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg2",
                    imgclass: "speechimg2",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg3" ,
                    imgclass: "speechimg3",
                    imgid: 'bgImgsp1',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg4" ,
                    imgclass: "speechimg4",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv: "people1" ,
                    imgclass: "people",
                    imgid: 'bgImg15',
                    imgsrc: ""
                },
                {
                    imgdiv:"people2" ,
                    imgclass: "people_sec",
                    imgid: 'bgImg16',
                    imgsrc: ""
                },
                {
                    imgdiv:"sundarDiv" ,
                    imgclass: "sundar",
                    imgid: 'sundar',
                    imgsrc: ""
                }

            ]
        }],
        textblock:[
            {
                textdiv:"text1",
                textclass:"centertext chapter",
                textdata:data.string.e1s0
            },

            {
                textdiv:"violet draggable",
                textclass:"centertext chapter",
                textdata:data.string.e1s45,
                ans:data.string.e1s45
            },
            {
                textdiv:"indigo draggable",
                textclass:"centertext chapter",
                textdata:data.string.e1s44,
                ans:data.string.e1s44
            },
            {
                textdiv:"speechtxt1",
                textclass:"centertext chapter",
                textdata: data.string.e1s41,
                ans: ''
            },
            {
                textdiv:"speechtxt2",
                textclass:"centertext chapter",
                textdata: data.string.e1s42,
                ans: ''
            },
            {
                textdiv:"speechtxt3",
                textclass:"centertext chapter",
                textdata:data.string.e1s43,
                ans: ''
            },
            {
                textdiv:"speechtxt4 droppable",
                textclass:"centertext chapter",
                textdata: data.string.e1s45,
                ans: data.string.e1s45
            },
          ],
    },
    //slide10
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "contentwithbg",
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'bgmain',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg1",
                    imgclass: "speechimg1",
                    imgid: 'bgImgsp1',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg2",
                    imgclass: "speechimg2",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg3" ,
                    imgclass: "speechimg3",
                    imgid: 'bgImgsp1',
                    imgsrc: ""
                },
                {
                    imgdiv:"speechimg4" ,
                    imgclass: "speechimg4",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv: "people1" ,
                    imgclass: "people width96",
                    imgid: 'bgImg17',
                    imgsrc: ""
                },
                {
                    imgdiv:"people2" ,
                    imgclass: "people_sec",
                    imgid: 'bgImg18',
                    imgsrc: ""
                },
                {
                    imgdiv:"sundarDiv" ,
                    imgclass: "sundar",
                    imgid: 'sundar',
                    imgsrc: ""
                }

            ]
        }],
        textblock:[
            {
                textdiv:"text1",
                textclass:"centertext chapter",
                textdata:data.string.e1s0
            },

            {
                textdiv:"violet draggable",
                textclass:"centertext chapter",
                textdata:data.string.e1s50,
                ans:data.string.e1s50
            },
            {
                textdiv:"indigo draggable",
                textclass:"centertext chapter",
                textdata:data.string.e1s49,
                ans:data.string.e1s49
            },
            {
                textdiv:"speechtxt1",
                textclass:"centertext chapter",
                textdata: data.string.e1s46,
                ans: ''
            },
            {
                textdiv:"speechtxt2 droppable",
                textclass:"centertext chapter",
                textdata: data.string.e1s50,
                ans: data.string.e1s50
            },
            {
                textdiv:"speechtxt3",
                textclass:"centertext chapter",
                textdata:data.string.e1s47,
                ans: ''
            },
            {
                textdiv:"speechtxt4",
                textclass:"centertext chapter",
                textdata: data.string.e1s48,
                ans: ''
            },
          ],
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "bgmain", src:imgpath+"bg_exercise.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg", src:imgpath+"q01a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg0", src:imgpath+"q01b.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg1", src:imgpath+"q02a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg2", src:imgpath+"q2b.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg3", src:imgpath+"q03a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg4", src:imgpath+"q03b.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg5", src:imgpath+"q04a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg6", src:imgpath+"q04b.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg7", src:imgpath+"q05a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg8", src:imgpath+"q05b.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg9", src:imgpath+"q06a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg10", src:imgpath+"q06b.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg11", src:imgpath+"q07a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg12", src:imgpath+"q07b.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg13", src:imgpath+"q08a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg14", src:imgpath+"q08b.png", type: createjs.AbstractLoader.IMAGE},

            {id: "bgImg15", src:imgpath+"q09a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg16", src:imgpath+"q09b.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg17", src:imgpath+"q10a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg18", src:imgpath+"q10b.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImgsp1", src:imgpath+"dialog1.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImgsp2", src:imgpath+"dialog2.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sundar", src:"images/sundar/correct-1.png", type: createjs.AbstractLoader.IMAGE},
            {id: "exe_ins", src: soundAsset + "ex_s1_p1.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();
    var rhino = new NumberTemplate();

    rhino.init(10);

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        if(countNext==0){
          sound_player("exe_ins",1);
        }
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
             // case 11:
             //  navigationcontroller(countNext,$total_page);
             //  break;
            default:
              var classoption = ["draggable violet", "draggable indigo"];
              shufflehint(classoption, "draggable");
              dragdrop();
              break;
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }



    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
      		createjs.Sound.stop();
      		clearTimeout(timeoutvar);
      			countNext++;
      			rhino.gotoNext();
      			if(countNext < 10){
      				templateCaller();
      			} else {
      				$(".scoreboard").hide(0);
      				$nextBtn.hide(0);
      			}

    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });


    function shufflehint(optionclass,option) {
        var optdiv = $(".coverboardfull");

        for (var i = optdiv.find("."+option).length; i >= 0; i--) {
            optdiv.append(optdiv.find("."+option).eq(Math.random() * i | 0));
        }
        optdiv.find("."+option).removeClass().addClass("current");
        optdiv.find(".current").each(function (index) {
            $(this).addClass(optionclass[index]);
        });
    }
    function loadsvg(imgid){
        var s1 = Snap('#sevencoloursvg');
        var svg = Snap.load(preload.getResult(imgid).src, function ( loadedFragment ) {
            s1.append(loadedFragment);
            $("#sevencoloursvg").hide();
            $("#sevencoloursvg").delay(1490).fadeIn(20);
        });

    }
    function rotatethewheel(){
        $(".wheel,.wheel1").css("top","23%");
        $(".hide3").delay(300).animate({"opacity":"0"},500);
        $(".hide2").delay(2000).animate({"opacity":"0"},500);
        $(".hide1").delay(2500).animate({"opacity":"0"},500);
    }
    function dragdrop(){
        $(".droppable").attr("disabled","disabled");
        $(".draggable").draggable({
            containment: "body",
            revert: true,
            appendTo: "body",
            zindex: 10,
            start:function(event, ui){
                $(".wrongImg").remove();
                $(".draggable,.droppable").removeClass("wrongcss");
            },
        });
        var rhino_flag = true;
        $('.droppable').droppable({
            accept : ".draggable",
            hoverClass: "hovered",
            drop: function(event, ui) {
                var draggableans = ui.draggable.attr("data-answer");
                var droppableans = $(this).attr("data-answer");
                if(draggableans.toString().trim() == droppableans.toString().trim()) {
                  createjs.Sound.stop();
                    play_correct_incorrect_sound(1);
                    if(rhino_flag!=false){
                      rhino.update(true);}
                    ui.draggable.hide(0);
                    // navigationcontroller(countNext,$total_page,true);
                    $nextBtn.show(0);
                    $(".draggable").addClass("avoid-clicks");
                    $(this).addClass("correctans");
                    $(".sundarDiv").show(0);
                    ui.draggable.draggable('disable');
                }
                else {
                    ui.draggable.addClass("wrongans avoid-clicks");
                    createjs.Sound.stop();

                    play_correct_incorrect_sound(0);
                    rhino.update(false);
                    rhino_flag=false;
                }
            }
        });
    }
});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";
var content=[
	// slide1
	{
		contentblockadditionalclass: "contentwithbg",
		extratextblock : [
		{
			textclass : "lesson-title",
			textdata : data.lesson.chapter
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "coverpage",
					imgid : 'coverpage',
					imgsrc: ""
				},
			]
		}]

	},
	// slide2
	{
		contentblockadditionalclass: "contentwithbga zoomout",
		uppertextblock : [
  	{
  		textclass : "headerblock",
  		textdata : data.string.p1s1
  	}
		],
		extratextblock : [
			{
				textclass : "button",
	  		textdata : data.string.p1s3
			},
			{
				textclass : "button1",
	  		textdata : data.string.p1s2
			}
		],
			imageblock:[{
				imagestoshow:[
					{
						imgclass: "backgroundgif",
						imgid : 'firstgif',
						imgsrc: ""
					},
					{
						imgclass: "somitaclass",
						imgid : 'somita1',
						imgsrc: ""
					},
					{
						imgclass: "sumanclass",
						imgid : 'suman1',
						imgsrc: ""
					}
				]
			}]
	},
	// slide3
	{
		contentblockadditionalclass: "contentwithbga",
		uppertextblock : [
		{
			textclass : "headerblock",
			textdata : data.string.p1s5
		},
		{
			textclass : "headerblock2",
			textdata : data.string.p1s6
		}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "backgroundgif",
					imgid : 'firstgif',
					imgsrc: ""
				},
				{
					imgclass: "somitaclass",
					imgid : 'somita1',
					imgsrc: ""
				},
				{
					imgclass: "sumanclass",
					imgid : 'suman1',
					imgsrc: ""
				}
			]
		}]
	},
	// slide4
	{
		contentblockadditionalclass: "contentwithbga",
		uppertextblock : [
		{
			textclass : "headerblock",
			textdata : data.string.p1s7
		}
		],
		extratextblock : [
			{
				textclass : "vayo",
				textdata : data.string.vayo_txt
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "backgroundgif",
					imgid : 'firstgif',
					imgsrc: ""
				},
				{
					imgclass: "somitaclass",
					imgid : 'somita2',
					imgsrc: ""
				},
				{
					imgclass: "sumanclass",
					imgid : 'suman1',
					imgsrc: ""
				},
			]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:"spImage",
			imgid:'somitadialogue',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p1s8,
		}]
	},
	//slide 5--SumanSay
	{
		contentblockadditionalclass: "contentwithbga",
		uppertextblock : [
		{
			textclass : "headerblock",
			textdata : data.string.p1s7
		}
		],
		extratextblock : [
			{
				textclass : "vayo",
				textdata : data.string.vayo_txt
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "backgroundgif",
					imgid : 'firstgif',
					imgsrc: ""
				},
				{
					imgclass: "somitaclass",
					imgid : 'somita1',
					imgsrc: ""
				},
				{
					imgclass: "sumanclass",
					imgid : 'suman2',
					imgsrc: ""
				},
			]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:"spImage",
			imgid:'sumandialogue',
			imgsrc:'',
			textclass:"spTxt_2",
			textdata:data.string.p1s9,
		}]
	},
	//slide 6
	{
		contentblockadditionalclass: "contentwithbga",
		uppertextblock : [
		{
			textclass : "headerblock",
			textdata : data.string.p1s7
		}
		],
		extratextblock : [
			{
				textclass : "vayo",
				textdata : data.string.vayo_txt
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "backgroundgif",
					imgid : 'firstgif',
					imgsrc: ""
				},
				{
					imgclass: "somitaclass",
					imgid : 'somita2',
					imgsrc: ""
				},
				{
					imgclass: "sumanclass",
					imgid : 'suman1',
					imgsrc: ""
				},
			]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:"spImage",
			imgid:'somitadialogue',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p1s10,
		}]
	},
	//slide 7--zmIn
	{
		// contentblockadditionalclass: "contentwithbga",
		uppertextblock : [
		{
			textclass : "headerblock",
			textdata : data.string.p1s7
		}
		],
		extratextblock : [
			{
				textclass : "vayo",
				textdata : data.string.vayo_txt
			}
		],
		imageblock:[{
			// imgdivblockclass:"zoomed",
			imagestoshow:[
				{
					imgclass: "fullBg",
					imgid : 'bg01',
					imgsrc: ""
				},
				{
					imgclass: "backgroundgif",
					imgid : 'firstgif',
					imgsrc: ""
				},
				{
					imgclass: "somitaclass",
					imgid : 'somita1',
					imgsrc: ""
				},
				{
					imgclass: "sumanclass",
					imgid : 'suman2',
					imgsrc: ""
				},
				{
					imgclass: "sumanimg",
					imgid : 'imagination',
					imgsrc: ""
				},
				{
					imgclass: "sumanbook",
					imgid : 'unibook',
					imgsrc: ""
				}
			]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:"spImage",
			imgid:'sumandialogue',
			imgsrc:'',
			textclass:"spTxt_2",
			textdata:data.string.p1s11,
		}]
	},
	//slide 8--zmOut
	{
		contentblockadditionalclass: "contentwithbga",
		uppertextblock : [
		{
			textclass : "headerblock",
			textdata : data.string.p1s7
		}
		],
		extratextblock : [
			{
				textclass : "vayo",
				textdata : data.string.vayo_txt
			}
		],

		imageblock:[{
			imagestoshow:[
				{
					imgclass: "backgroundgif",
					imgid : 'firstgif',
					imgsrc: ""
				},
				{
					imgclass: "somitaclass",
					imgid : 'somita1',
					imgsrc: ""
				},
				{
					imgclass: "sumanclass",
					imgid : 'suman2',
					imgsrc: ""
				},
			]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:"spImage",
			imgid:'sumandialogue',
			imgsrc:'',
			textclass:"spTxt_2",
			textdata:data.string.p1s12,
		}]
	},
	//slide 9
	{
		contentblockadditionalclass: "contentwithbga",
		uppertextblock : [
		{
			textclass : "headerblock",
			textdata : data.string.p1s7
		}
		],
		extratextblock : [
			{
				textclass : "vayo",
				textdata : data.string.vayo_txt
			}
		],

		imageblock:[{
			imagestoshow:[
				{
					imgclass: "backgroundgif",
					imgid : 'firstgif',
					imgsrc: ""
				},
				{
					imgclass: "somitaclass",
					imgid : 'somita3',
					imgsrc: ""
				},
				{
					imgclass: "sumanclass",
					imgid : 'suman1',
					imgsrc: ""
				},
			]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:"spImage",
			imgid:'somitadialogue',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p1s13,
		}]
	},
	//slide 10
	{
		contentblockadditionalclass: "contentwithbga zoomin",
		uppertextblock : [
		{
			textclass : "headerblock",
			textdata : data.string.p1s7
		}
		],
		extratextblock : [
			{
				textclass : "bajesay",
				textdata : data.string.p1s14
			},{
				textclass : "vayo",
				textdata : data.string.vayo_txt
			}
		],

		imageblock:[{
			imagestoshow:[
				{
					imgclass: "backgroundgif",
					imgid : 'firstgif',
					imgsrc: ""
				},
				{
					imgclass: "somitaclass",
					imgid : 'somita1',
					imgsrc: ""
				},
				{
					imgclass: "sumanclass",
					imgid : 'suman1',
					imgsrc: ""
				},
				{
					imgclass: "bajeimg",
					imgid : 'baje',
					imgsrc: ""
				}
			]
		}]
	},
	//slide 11
	{
		contentblockadditionalclass: "contentwithbga",
		uppertextblock : [
		{
			textclass : "headerblock",
			textdata : data.string.p1s7
		}
		],
		extratextblock : [
			{
				textclass : "vayo",
				textdata : data.string.vayo_txt
			}
		],

		imageblock:[{
			imagestoshow:[
				{
					imgclass: "backgroundgif",
					imgid : 'firstgif',
					imgsrc: ""
				},
				{
					imgclass: "somitaclass",
					imgid : 'somita1',
					imgsrc: ""
				},
				{
					imgclass: "sumanclass",
					imgid : 'suman2',
					imgsrc: ""
				},
			]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:"spImage",
			imgid:'sumandialogue',
			imgsrc:'',
			textclass:"spTxt_2",
			textdata:data.string.p1s15,
		}]
	},
	//slide 12
	{
		contentblockadditionalclass: "contentwithbga",
		uppertextblock : [
		{
			textclass : "headerblock",
			textdata : data.string.p1s7
		}
		],
		extratextblock : [
			{
				textclass : "vayo",
				textdata : data.string.vayo_txt
			}
		],

		imageblock:[{
			imagestoshow:[
				{
					imgclass: "backgroundgif",
					imgid : 'firstgif',
					imgsrc: ""
				},
				{
					imgclass: "somitaclass",
					imgid : 'somita2',
					imgsrc: ""
				},
				{
					imgclass: "sumanclass",
					imgid : 'suman1',
					imgsrc: ""
				},
			]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:"spImage",
			imgid:'somitadialogue',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p1s16,
		}]
	},
	// 13
	{
		contentblockadditionalclass: "contentwithbga",
		uppertextblock : [
		{
			textclass : "headerblock",
			textdata : data.string.p1s7
		}
		],
		extratextblock : [
			{
				textclass : "vayo",
				textdata : data.string.vayo_txt
			}
		],

		imageblock:[{
			imagestoshow:[
				{
					imgclass: "backgroundgif",
					imgid : 'firstgif',
					imgsrc: ""
				},
				{
					imgclass: "somitaclass",
					imgid : 'somita1',
					imgsrc: ""
				},
				{
					imgclass: "sumanclass",
					imgid : 'suman2',
					imgsrc: ""
				},
			]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:"spImage",
			imgid:'sumandialogue',
			imgsrc:'',
			textclass:"spTxt_2",
			textdata:data.string.p1s17,
		}]
	},
	//14
	{
		contentblockadditionalclass: "contentwithbga zoomin_sec",
		uppertextblock : [
		{
			textclass : "headerblock",
			textdata : data.string.p1s7
		}
		],
		extratextblock : [
			{
				textclass : "bajesay1",
				textdata : data.string.p1s18
			},
			{
				textclass : "sumansaytext1",
				textdata : data.string.p1s19
			},{
				textclass : "vayo",
				textdata : data.string.vayo_txt
			}

		],

		imageblock:[{
			imagestoshow:[
				{
					imgclass: "backgroundgif",
					imgid : 'firstgif',
					imgsrc: ""
				},
				{
					imgclass: "somitaclass",
					imgid : 'somita1',
					imgsrc: ""
				},
				{
					imgclass: "sumanclass",
					imgid : 'sumaneye',
					imgsrc: ""
				},
				{
					imgclass: "bajeimg2",
					imgid : 'baje',
					imgsrc: ""
				},
				{
					imgclass: "sumansay2",
					imgid : 'sumandialogue1',
					imgsrc: ""
				}
			]
		}]
	},
	//15
	{
		contentblockadditionalclass: "contentwithbga",
		uppertextblock : [
		{
			textclass : "headerblock",
			textdata : data.string.p1s7
		}
		],
		extratextblock : [
			{
				textclass : "vayo",
				textdata : data.string.vayo_txt
			}
		],

		imageblock:[{
			imagestoshow:[
				{
					imgclass: "backgroundgif",
					imgid : 'firstgif',
					imgsrc: ""
				},
				{
					imgclass: "somitaclass",
					imgid : 'somita1',
					imgsrc: ""
				},
				{
					imgclass: "sumanclass",
					imgid : 'suman1',
					imgsrc: ""
				},
			]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:"spImage",
			imgid:'sumandialogue',
			imgsrc:'',
			textclass:"spTxt_2",
			textdata:data.string.p1s20,
		}]
	},
	//16--zmIn
	{
		contentblockadditionalclass: "contentwithbga",
		uppertextblock : [
		{
			textclass : "headerblock",
			textdata : data.string.p1s7
		}
		],
		extratextblock : [
			{
				textclass : "vayo",
				textdata : data.string.vayo_txt
			}
		],

		imageblock:[{
			imagestoshow:[
				{
					imgclass: "backgroundgif",
					imgid : 'firstgif',
					imgsrc: ""
				},
				{
					imgclass: "somitaclass",
					imgid : 'somita3',
					imgsrc: ""
				},
				{
					imgclass: "sumanclass",
					imgid : 'suman1',
					imgsrc: ""
				},
			]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:"spImage",
			imgid:'somitadialogue',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p1s21,
		}]
	},
	//17--zmOut
	{
		contentblockadditionalclass: "contentwithbga",
		uppertextblock : [
		{
			textclass : "headerblock",
			textdata : data.string.p1s7
		}
		],
		extratextblock : [
			{
				textclass : "vayo",
				textdata : data.string.vayo_txt
			}
		],

		imageblock:[{
			imagestoshow:[
				{
					imgclass: "backgroundgif",
					imgid : 'firstgif',
					imgsrc: ""
				},
				{
					imgclass: "somitaclass",
					imgid : 'somita1',
					imgsrc: ""
				},
				{
					imgclass: "sumanclass",
					imgid : 'suman2',
					imgsrc: ""
				},
			]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:"spImage",
			imgid:'sumandialogue',
			imgsrc:'',
			textclass:"spTxt_2",
			textdata:data.string.p1s22,
		}]
	},
	//18
	{
		contentblockadditionalclass: "contentwithbga",
		uppertextblock : [
		{
			textclass : "headerblock",
			textdata : data.string.p1s7
		}
		],
		extratextblock : [
			{
				textclass : "vayo",
				textdata : data.string.vayo_txt
			}
		],

		imageblock:[{
			imagestoshow:[
				{
					imgclass: "backgroundgif",
					imgid : 'firstgif',
					imgsrc: ""
				},
				{
					imgclass: "somitaclass",
					imgid : 'somita1',
					imgsrc: ""
				},
				{
					imgclass: "sumanclass",
					imgid : 'suman2',
					imgsrc: ""
				},
			]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:"spImage",
			imgid:'sumandialogue',
			imgsrc:'',
			textclass:"spTxt_2",
			textdata:data.string.p1s31,
		}]
	},
	//19
	{
		contentblockadditionalclass: "contentwithbga",
		uppertextblock : [
		{
			textclass : "headerblock",
			textdata : data.string.p1s7
		}
		],
		extratextblock : [
			{
				textclass : "vayo",
				textdata : data.string.vayo_txt
			}
		],

		imageblock:[{
			imagestoshow:[
				{
					imgclass: "backgroundgif",
					imgid : 'firstgif',
					imgsrc: ""
				},
				{
					imgclass: "somitaclass",
					imgid : 'somita2',
					imgsrc: ""
				},
				{
					imgclass: "sumanclass",
					imgid : 'suman1',
					imgsrc: ""
				},
			]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:"spImage",
			imgid:'somitadialogue',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p1s23,
		}]
	},
	//20--zmIN
	{
		contentblockadditionalclass: "contentwithbga",
		uppertextblock : [
		{
			textclass : "headerblock",
			textdata : data.string.p1s7
		}
		],
		extratextblock : [
			{
				textclass : "vayo",
				textdata : data.string.vayo_txt
			}
		],

		imageblock:[{
			imagestoshow:[
				{
					imgclass: "backgroundgif",
					imgid : 'firstgif',
					imgsrc: ""
				},
				{
					imgclass: "somitaclass",
					imgid : 'somita1',
					imgsrc: ""
				},
				{
					imgclass: "sumanclass",
					imgid : 'suman2',
					imgsrc: ""
				},
			]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:"spImage",
			imgid:'sumandialogue',
			imgsrc:'',
			textclass:"spTxt_2",
			textdata:data.string.p1s24,
		}]
	},
	//21--zmOut
	{
		contentblockadditionalclass: "contentwithbga",
		uppertextblock : [
		{
			textclass : "headerblock",
			textdata : data.string.p1s7
		}
		],
		extratextblock : [
			{
				textclass : "vayo",
				textdata : data.string.vayo_txt
			}
		],

		imageblock:[{
			imagestoshow:[
				{
					imgclass: "backgroundgif",
					imgid : 'firstgif',
					imgsrc: ""
				},
				{
					imgclass: "somitaclass",
					imgid : 'somita3',
					imgsrc: ""
				},
				{
					imgclass: "sumanclass",
					imgid : 'suman1',
					imgsrc: ""
				},
			]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:"spImage",
			imgid:'somitadialogue',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p1s25,
		}]
	},
	//22
	{
		contentblockadditionalclass: "contentwithbga",
		uppertextblock : [
		{
			textclass : "headerblock",
			textdata : data.string.p1s7
		}
		],
		extratextblock : [
			{
				textclass : "vayo",
				textdata : data.string.vayo_txt
			}
		],

		imageblock:[{
			imagestoshow:[
				{
					imgclass: "backgroundgif",
					imgid : 'firstgif',
					imgsrc: ""
				},
				{
					imgclass: "somitaclass",
					imgid : 'somita1',
					imgsrc: ""
				},
				{
					imgclass: "sumanclass",
					imgid : 'suman2',
					imgsrc: ""
				},
			]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:"spImage",
			imgid:'sumandialogue',
			imgsrc:'',
			textclass:"spTxt_2",
			textdata:data.string.p1s26,
		}]
	},
	//23
	{
		contentblockadditionalclass: "contentwithbga",
		uppertextblock : [
		{
			textclass : "headerblock",
			textdata : data.string.p1s7
		}
		],
		extratextblock : [
			{
				textclass : "vayo",
				textdata : data.string.vayo_txt
			}
		],

		imageblock:[{
			imagestoshow:[
				{
					imgclass: "backgroundgif",
					imgid : 'firstgif',
					imgsrc: ""
				},
				{
					imgclass: "somitaclass",
					imgid : 'somita1',
					imgsrc: ""
				},
				{
					imgclass: "sumanclass",
					imgid : 'suman2',
					imgsrc: ""
				},
			]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgclass:"spImage",
			imgid:'sumandialogue',
			imgsrc:'',
			textclass:"spTxt_2",
			textdata:data.string.p1s27,
		}]
	},
	//24
	{
		contentblockadditionalclass: "contentwithbga",
		uppertextblock : [
		{
			textclass : "headerblock",
			textdata : data.string.p1s7
		}
		],
		extratextblock : [
			{
				textclass : "vayo",
				textdata : data.string.vayo_txt
			}
		],

		imageblock:[{
			imagestoshow:[
				{
					imgclass: "backgroundgif",
					imgid : 'firstgif',
					imgsrc: ""
				},
				{
					imgclass: "somitaclass",
					imgid : 'somita2',
					imgsrc: ""
				},
				{
					imgclass: "sumanclass",
					imgid : 'suman1',
					imgsrc: ""
				},
			]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:"spImage",
			imgid:'somitadialogue',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p1s28,
		}]
	},
	//25
	{
		contentblockadditionalclass: "contentwithbga",
		uppertextblock : [
		{
			textclass : "headerblock",
			textdata : data.string.p1s7
		}
		],
		extratextblock : [
			{
				textclass : "sumansaytext",
				textdata : data.string.p1s29
			},{
				textclass : "vayo",
				textdata : data.string.vayo_txt
			}
		],

		imageblock:[{
			imagestoshow:[
				{
					imgclass: "backgroundgif",
					imgid : 'firstgif',
					imgsrc: ""
				},
				{
					imgclass: "somitaclass",
					imgid : 'somita1',
					imgsrc: ""
				},
				{
					imgclass: "sumanclass",
					imgid : 'suman2',
					imgsrc: ""
				},
				{
					imgclass: "sumansay",
					imgid : 'sumandialogue',
					imgsrc: ""
				}
			]
		}]
	},
	//26
	{
		contentblockadditionalclass: "contentwithbgfinal",

		extratextblock : [
			{
				textclass : "finaltext",
				textdata : data.string.p1s30
			}
		],

		imageblock:[{
			imagestoshow:[
				{
					imgclass: "backgroundgif",
					imgid : 'firstgif',
					imgsrc: ""
				},
				{
					imgclass: "somitaclassfinal",
					imgid : 'somitalast',
					imgsrc: ""
				},
				{
					imgclass: "sumanclassfinal",
					imgid : 'sumanlast',
					imgsrc: ""
				}
			]
		}]
	}
];

$(function () {

	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var flag = true;


	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "coverpage", src: imgpath+"coverpage.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "starmagic", src: imgpath+"stars.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "fairytara", src: imgpath+"fairy-tara.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fairytaragif", src: imgpath+"flying-chibi-fairy-animation.gif", type: createjs.AbstractLoader.IMAGE},


			{id: "firstgif", src: imgpath+"play_ground.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "suman1", src: imgpath+"pawan01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sumaneye", src: imgpath+"pawan03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "somita1", src: imgpath+"somita01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "somitagif", src: imgpath+"somita_talking02.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "sumangif", src: imgpath+"pawan_talking.png", type: createjs.AbstractLoader.IMAGE},
			{id: "somitadialogue", src: imgpath+"bubble-2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sumandialogue", src: imgpath+"bubble-1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sumandialogue1", src: imgpath+"bubble-3.png", type: createjs.AbstractLoader.IMAGE},
			{id: "imagination", src: imgpath+"cloud02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "baje", src: imgpath+"cloud03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "somitalast", src: imgpath+"somita04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sumanlast", src: imgpath+"pawan.png", type: createjs.AbstractLoader.IMAGE},
			{id: "somita2", src: imgpath+"somita02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "somita3", src: imgpath+"somita03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "suman2", src: imgpath+"pawan02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bookturn", src: imgpath+"bookturn.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "bg01", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},


			{id: "unibook", src: imgpath+"the-universe.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "yourturn", src: soundAsset+"s1_timro.ogg"},
      {id: "sound_0", src: soundAsset+"s1_p1.ogg"},
      {id: "sound_1", src: soundAsset+"s1_p2.ogg"},
      {id: "sound_1_som", src: soundAsset+"s1_p2_somita.ogg"},
      {id: "sound_1_sum", src: soundAsset+"s1_p2_suman.ogg"},
      {id: "sound_2_som", src: soundAsset+"s1_p3_somita.ogg"},
      {id: "sound_2_sum", src: soundAsset+"s1_p3_suman.ogg"},
      {id: "sound_3", src: soundAsset+"s1_p4.ogg"},
      {id: "sound_4", src: soundAsset+"s1_p5.ogg"},
      {id: "sound_5", src: soundAsset+"s1_p6.ogg"},
      {id: "sound_6", src: soundAsset+"s1_p7.ogg"},
      {id: "sound_7", src: soundAsset+"s1_p8.ogg"},
      {id: "sound_8", src: soundAsset+"s1_p9.ogg"},
      {id: "sound_9", src: soundAsset+"s1_p10.ogg"},
      {id: "sound_10", src: soundAsset+"s1_p11.ogg"},
      {id: "sound_11", src: soundAsset+"s1_p12.ogg"},
      {id: "sound_12", src: soundAsset+"s1_p13.ogg"},
			{id: "sound_13", src: soundAsset+"s1_p14.ogg"},
      {id: "sound_13a", src: soundAsset+"s1_p14_1.ogg"},
      {id: "sound_14", src: soundAsset+"s1_p15.ogg"},
      {id: "sound_15", src: soundAsset+"s1_p16.ogg"},
      {id: "sound_16", src: soundAsset+"s1_p17.ogg"},
      {id: "sound_17", src: soundAsset+"s1_p18.ogg"},
      {id: "sound_18", src: soundAsset+"s1_p19.ogg"},
      {id: "sound_19", src: soundAsset+"s1_p20.ogg"},
      {id: "sound_20", src: soundAsset+"s1_p21.ogg"},
      {id: "sound_21", src: soundAsset+"s1_p22.ogg"},
      {id: "sound_22", src: soundAsset+"s1_p23.ogg"},
      {id: "sound_23", src: soundAsset+"s1_p24.ogg"},
      {id: "sound_24", src: soundAsset+"s1_p25.ogg"},
      {id: "sound_25", src: soundAsset+"s1_p26.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
	 typeof islastpageflag === "undefined" ?
	 islastpageflag = false :
	 typeof islastpageflag != 'boolean'?
	 alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 null;

	 if(countNext == 0 && $total_page!=1){
	 	$nextBtn.show(0);
	 	$prevBtn.css('display', 'none');
	 }
	 else if($total_page == 1){
	 	$prevBtn.css('display', 'none');
	 	$nextBtn.css('display', 'none');

	 	// if lastpageflag is true
	 	islastpageflag ?
	 	ole.footerNotificationHandler.lessonEndSetNotification() :
	 	ole.footerNotificationHandler.lessonEndSetNotification() ;
	 }
	 else if(countNext > 0 && countNext < $total_page-1){
	 	$nextBtn.show(0);
	 	$prevBtn.show(0);
	 }
	 else if(countNext == $total_page-1){
	 	$nextBtn.css('display', 'none');
	 	$prevBtn.show(0);

	 	// if lastpageflag is true
	 	islastpageflag ?
	 	ole.footerNotificationHandler.lessonEndSetNotification() :
	 	ole.footerNotificationHandler.pageEndSetNotification() ;
	 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);

		$(".vayo").click(function(){
			if(countNext==6){
				$(".coverboardfull ").addClass("zom_in");
			}
			else{
				navigationcontroller();
			}
			$(".vayo").hide();
		});
		// to show vayo text and header according to selected person
		function vayoShow(next,playsound)
		{
			if(!flag){
				if(next){
					$(".vayo").delay(3000).show(0);
					$(".headerblock").css('display', 'block');
					sound_player("yourturn", 0);

				}
				else{
				$(".headerblock").css('display', 'none');

				sound_player(playsound, 1);
				}
			}
			else if (flag) {
				if(!next){
					$(".vayo").delay(3000).show(0);
					$(".headerblock").css('display', 'block');
					sound_player("yourturn", 0);

				}
				else{
				$(".headerblock").css('display', 'none');

				sound_player(playsound, 1);

				}
			}
		}
		// TODO: format the switch cases properly
		switch(countNext){
			case 0:
				sound_player("sound_0");
			break;
			case 1:
			setTimeout(()=>sound_player("sound_1"),3000);

				$(".button1").click(function(){
					navigationcontroller();
					flag = true;
					$(".button").css('display', 'none');
					$(".button1").css("background-color","#0b5394");
					sound_player("sound_1_sum", 1);
				});

				$(".button").click(function(){
					navigationcontroller();

					flag = false;
					$(".button1").css('display', 'none');
					$(".button").css("background-color","#0b5394");
					sound_player("sound_1_som", 1);
				});
				$(".button").mouseenter(function(){
					$(".somitaclass").addClass("hghlght");
				})
				$(".button").mouseout(function(){
					$(".somitaclass").removeClass("hghlght");
				});
				$(".button1").mouseenter(function(){
					$(".sumanclass").addClass("hghlght");
				})
				$(".button1").mouseout(function(){
					$(".sumanclass").removeClass("hghlght");
				});
			break;

			case 2:
				if(flag==true){
					$(".headerblock2").css('display', 'none');
					sound_player("sound_2_sum", 1);
				}
				else if (flag==false){
					$(".headerblock").css('display', 'none');
					sound_player("sound_2_som", 1);
				}
				// sound_player("sound_0", 0);
				break;

			case 3:
			vayoShow(1,'sound_3');
			break;
			case 5:
			vayoShow(1,'sound_5');
			break;
			case 8:
				// $(".sp-1").hide(0);
				// $(".sp-1").delay(3000).fadeIn(1000);
				vayoShow(1,'sound_8');
			break;
			case 11:
			vayoShow(1,'sound_11');
			break;
			case 15:
			vayoShow(1,'sound_15');
			break;
			case 18:
			vayoShow(1,'sound_18');
			break;
			case 20:
			vayoShow(1,'sound_20');
			break;
			case 23:
			vayoShow(1,'sound_23');
			break;

			case 4:
			vayoShow(0,'sound_4');
			break;
			case 6:
			$(".sumanbook, .sumanimg").hide(0);

			$(".sumanbook, .sumanimg").delay(8000).fadeIn(500,()=>{
				$(".sumanbook").click(()=>{
					$(".sumanbook").attr('src',preload.getResult('bookturn').src).css({"width":"14%","left":"74%"});
					navigationcontroller();
				});
			});

			vayoShow(0,'sound_6');
			break;
			case 7:
			vayoShow(0,'sound_7');
			break;
			case 9:
			vayoShow(0,'sound_9');
			break;
			case 10:
			vayoShow(0,'sound_10');
			break;
			case 12:
			console.log(countNext);
			vayoShow(0,'sound_12');
			break;
			case 13:
			console.log(countNext);

			// if(!flag) sound_player_duo('sound_13','sound_13a');
			vayoShow(0,'sound_13');
			if(!flag){
				setTimeout(()=>{
					$nextBtn.hide();
					sound_player_duo2("sound_13a");
				},13000);
			}
			break;
			case 14:
			console.log(countNext);

			vayoShow(0,'sound_14');
			break;
			case 16:
			vayoShow(0,'sound_16');
			break;
			case 17:
			vayoShow(0,'sound_17');
			break;
			case 19:
			vayoShow(0,'sound_19');
			break;
			case 21:
			vayoShow(0,'sound_21');
			break;
			case 22:
			vayoShow(0,'sound_22');
			break;
			case 24:
			vayoShow(0,'sound_24');
				break;
			case 25:
			setTimeout(()=>sound_player("sound_25",1),1000);
			break;
			default:
				nav_button_controls(2500);
		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player_duo(sound_id,sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			sound_player_duo2(sound_id_2);
		});
	}

	function sound_player_duo2(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			navigationcontroller();
		});
	}



	function sound_player(sound_id,next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(!flag){
				if(next){
					navigationcontroller();
				}else{

				}
			}
			else if (flag) {
				if(!next){
				}else{
					navigationcontroller();
				}
			}
			else{
				navigationcontroller();
			}
		});
	}


	function sound_player_vayo(sound_id, next){
		createjs.Sound.stop();
		if(flag){
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
			current_sound.on('complete', function(){
				if(next) navigationcontroller();
			});
		}

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
	//TODO: what does this function do?
	function animateDiv(){
			$(".hideDiv").addClass("hide");
			setTimeout(function(){
					$(".hideDiv").addClass("show");
					navigationcontroller(countNext,$total_page,false);
			},1000);

	}

});

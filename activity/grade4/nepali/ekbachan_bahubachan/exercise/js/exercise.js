var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

Array.prototype.shufflearray = function() {
    var i = this.length,
        j, temp;
    while (--i > 0) {
        j = Math.floor(Math.random() * (i + 1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
    return this;
};

var content=[
    //slide 0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "purplebg",
        uppertextblock: [
            {
                textclass: "top_text",
                textdata: data.string.ex1text1,
            },
            {
                textclass: "midtext",
                textdata: data.string.ex1text5,
            }
        ],
        draggableblockadditionalclass: 'drag-drop-container',
        draggableblock:[{
          textdata: data.string.ex1text2,
          divclass: "drag-item drag-1 option1 correct",
        },{
          textdata: data.string.ex1text3,
          divclass: "drag-item drag-2 option2",
        },{
          textdata: data.string.ex1text4,
          divclass: "drag-item drag-3 option3",
        }]
    },

    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "purplebg",
        uppertextblock: [
            {
                textclass: "top_text",
                textdata: data.string.ex1text1,
            },
            {
                textclass: "midtext",
                textdata: data.string.ex1text10,
            }

        ],
        draggableblockadditionalclass: 'drag-drop-container',
        draggableblock:[{
          textdata: data.string.ex1text8,
          divclass: "drag-item drag-1 option1 ",
        },{
          textdata: data.string.ex1text7,
          divclass: "drag-item drag-2 option2 correct",
        },{
          textdata: data.string.ex1text9,
          divclass: "drag-item drag-3 option3",
        }]
    },

    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "purplebg",
        uppertextblock: [
            {
                textclass: "top_text",
                textdata: data.string.ex1text1,
            },
            {
                textclass: "midtext",
                textdata: data.string.ex1text14,
            }

        ],
        draggableblockadditionalclass: 'drag-drop-container',
        draggableblock:[{
          textdata: data.string.ex1text11,
          divclass: "drag-item drag-1 option1 correct",
        },{
          textdata: data.string.ex1text13,
          divclass: "drag-item drag-2 option2 ",
        },{
          textdata: data.string.ex1text12,
          divclass: "drag-item drag-3 option3",
        }]
    },

    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "purplebg",
        uppertextblock: [
            {
                textclass: "top_text",
                textdata: data.string.ex1text1,
            },
            {
                textclass: "midtext",
                textdata: data.string.ex1text18,
            }

        ],
        draggableblockadditionalclass: 'drag-drop-container',
        draggableblock:[{
          textdata: data.string.ex1text16,
          divclass: "drag-item drag-1 option1",
        },{
          textdata: data.string.ex1text17,
          divclass: "drag-item drag-2 option2 ",
        },{
          textdata: data.string.ex1text15,
          divclass: "drag-item drag-3 option3 correct",
        }]
    },
    //slide 4
    {
      contentnocenteradjust: true,
      contentblockadditionalclass: "purplebg",
      uppertextblock: [
          {
              textclass: "top_text",
              textdata: data.string.ex1text1,
          },
          {
              textclass: "midtext",
              textdata: data.string.ex1text22,
          }

      ],
      draggableblockadditionalclass: 'drag-drop-container',
      draggableblock:[{
        textdata: data.string.ex1text21,
        divclass: "drag-item drag-1 option1 correct",
      },{
        textdata: data.string.ex1text20,
        divclass: "drag-item drag-2 option2 ",
      },{
        textdata: data.string.ex1text19,
        divclass: "drag-item drag-3 option3",
      }]
    },
    //slide 5
    {
      contentnocenteradjust: true,
      contentblockadditionalclass: "purplebg",
      uppertextblock: [
          {
              textclass: "top_text",
              textdata: data.string.ex1text1,
          },
          {
              textclass: "midtext",
              textdata: data.string.ex1text26,
          }

      ],
      draggableblockadditionalclass: 'drag-drop-container',
      draggableblock:[{
        textdata: data.string.ex1text24,
        divclass: "drag-item drag-1 option1 ",
      },{
        textdata: data.string.ex1text23,
        divclass: "drag-item drag-2 option2 ",
      },{
        textdata: data.string.ex1text25,
        divclass: "drag-item drag-3 option3 correct",
      }]
    },
    //slide 6
    {
      contentnocenteradjust: true,
      contentblockadditionalclass: "purplebg",
      uppertextblock: [
          {
              textclass: "top_text",
              textdata: data.string.ex1text1,
          },
          {
              textclass: "midtext",
              textdata: data.string.ex1text30,
          }

      ],
      draggableblockadditionalclass: 'drag-drop-container',
      draggableblock:[{
        textdata: data.string.ex1text27,
        divclass: "drag-item drag-1 option1 ",
      },{
        textdata: data.string.ex1text28,
        divclass: "drag-item drag-2 option2 correct",
      },{
        textdata: data.string.ex1text29,
        divclass: "drag-item drag-3 option3",
      }]
    },
    //slide 7
    {
      contentnocenteradjust: true,
      contentblockadditionalclass: "purplebg",
      uppertextblock: [
          {
              textclass: "top_text",
              textdata: data.string.ex1text1,
          },
          {
              textclass: "midtext",
              textdata: data.string.ex1text34,
          }

      ],
      draggableblockadditionalclass: 'drag-drop-container',
      draggableblock:[{
        textdata: data.string.ex1text32,
        divclass: "drag-item drag-1 option1 ",
      },{
        textdata: data.string.ex1text33,
        divclass: "drag-item drag-2 option2 correct",
      },{
        textdata: data.string.ex1text31,
        divclass: "drag-item drag-3 option3",
      }]
    },
    //slide 8
    {
      contentnocenteradjust: true,
      contentblockadditionalclass: "purplebg",
      uppertextblock: [
          {
              textclass: "top_text",
              textdata: data.string.ex1text1,
          },
          {
              textclass: "midtext",
              textdata: data.string.ex1text38,
          }

      ],
      draggableblockadditionalclass: 'drag-drop-container',
      draggableblock:[{
        textdata: data.string.ex1text35,
        divclass: "drag-item drag-1 option1 ",
      },{
        textdata: data.string.ex1text36,
        divclass: "drag-item drag-2 option2 correct",
      },{
        textdata: data.string.ex1text37,
        divclass: "drag-item drag-3 option3",
      }]
    },
    //slide 9
    {
      contentnocenteradjust: true,
      contentblockadditionalclass: "purplebg",
      uppertextblock: [
          {
              textclass: "top_text",
              textdata: data.string.ex1text1,
          },
          {
              textclass: "midtext",
              textdata: data.string.ex1text42,
          }

      ],
      draggableblockadditionalclass: 'drag-drop-container',
      draggableblock:[{
        textdata: data.string.ex1text40,
        divclass: "drag-item drag-1 option1 ",
      },{
        textdata: data.string.ex1text39,
        divclass: "drag-item drag-2 option2 correct",
      },{
        textdata: data.string.ex1text41,
        divclass: "drag-item drag-3 option3",
      }]
    }
];
// content.shufflearray();
content.shufflearray();

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;

    var $playBtn = $('.playBtn');
    var $pauseBtn = $('.pauseBtn');
    var $replayBtn = $('.replayBtn');

    var preload;
    var timeoutvar = null;
    var current_sound;

    var testin = new EggTemplate();

 	  testin.init(10);
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            // sounds
            {id: "exe_ins_1", src: soundAsset+"exe_ins_1.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        	var $total_page = content.length;
        templateCaller();
    }

    //initialize
    init();

    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);

        		/*generate question no at the beginning of question*/
        		testin.numberOfQuestions();
            countNext==0?sound_player("exe_ins_1",0):'';
        var wrngClicked = 0;
        var draggables = $(".option1,.option2,.option3").draggable({
          containment : "body",
          cursor : "grabbing",
          revert : "invalid",
          appendTo : "body",
          zindex : 10000,
        });


        $('.drop_area').droppable({
          accept : ".option1,.option2,.option3",
          drop : handleCardDrop1
        });

           var positions = [1,2,3];
   				positions.shufflearray();
   				for(var i=1; i<=3; i++){
   					$('.drag-'+i).addClass('drag-pos-'+positions[i-1]);
   				}
        function handleCardDrop1(event, ui){
            var dropped = ui.draggable;
            var droppedOn = $(this);

            //dropped at right place
            if(ui.draggable.hasClass("correct")){
              console.log(wrngClicked);
              if(wrngClicked == 0){
                	testin.update(true);
              }
              $('.drop_area').html($(ui.draggable).html()).css({'background-color':'#6aa84f','color':'white'});
            $(ui.draggable).hide(0);
               play_correct_incorrect_sound(true);
               draggables.draggable('disable');
                navigationcontroller(countNext,$total_page);
                $(".drag-item").css("pointer-events","none");
               }

                //dropped at wrong place
                else {
                  wrngClicked++;
                				testin.update(false);
                   dropped.draggable('option', 'revert', true);
                   $(ui.draggable).css({'background-color':'#cc0000','border-color':'#cc0000','color':'white'});
                   play_correct_incorrect_sound(false);
                }

            }
    }
    function put_image(content, count,preload) {
        var contentCount=content[count];
        var imageblockcontent=contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent,contentCount,preload)
    }

    function dynamicimageload(imageblockcontent,contentCount,preload){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }

    function navigationcontroller(islastpageflag) {
        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
        }
        else if ($total_page == 1) {
            $nextBtn.css('display', 'none');

            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if (countNext > 0 && countNext < $total_page) {

            $nextBtn.show(0);
        }
        else if (countNext == $total_page - 2) {

            $nextBtn.css('display', 'none');
            // if lastpageflag is true
        }

    }

    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller(countNext,$total_page):"";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
    }

    $nextBtn.on("click", function(){
      countNext++;
        templateCaller();
        testin.gotoNext();
    });


    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }
    function shufflehint() {
        var optiondiv = $(".bg");
        for (var i = 2; i >= 0; i--) {
            optiondiv.append(optiondiv.find(".commonbtn").eq(Math.random() * i | 0));
        }
        optiondiv.find(".commonbtn").removeClass().addClass("current");
        var a = ["commonbtn option1","commonbtn option2"]
        optiondiv.find(".current").each(function (index) {
            var $this = $(this)
            $this.addClass(a[index]);
        });
        optiondiv.find(".commonbtn").removeClass("current");


    }

});

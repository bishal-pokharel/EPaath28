var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";
var content=[
    //slide 0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "purplebg",
        uppertextblock: [
            {
                textclass: "top_text",
                textdata: data.string.ex2text1,
            },
            {
                textclass: "top_below_text",
                textdata: data.string.ex2text2,
            },
            {
                textclass: "question_text",
                textdata: data.string.ex2text3,
            },
            {
                textclass: "drop_area_p",
            }

        ],
        draggableblockadditionalclass: 'drag-drop-container',
        draggableblock:[{
          textdata: data.string.ex2text4,
          divclass: "drag-item drag-1 optiona1 correct",
        },{
          textdata: data.string.ex2text5,
          divclass: "drag-item drag-2 optiona2",
        },{
          textdata: data.string.ex2text6,
          divclass: "drag-item drag-3 optiona3",
        }]
    },

    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "purplebg",
        uppertextblock: [
            {
                textclass: "top_text",
                textdata: data.string.ex2text1,
            },
            {
                textclass: "top_below_text",
                textdata: data.string.ex2text7,
            },
            {
                textclass: "question_text",
                textdata: data.string.ex2text8,
            },
            {
                textclass: "drop_area_p",
            }

        ],
        draggableblockadditionalclass: 'drag-drop-container',
        draggableblock:[{
          textdata: data.string.ex2text9,
          divclass: "drag-item drag-1 optiona1 correct",
        },{
          textdata: data.string.ex2text10,
          divclass: "drag-item drag-2 optiona2",
        },{
          textdata: data.string.ex2text11,
          divclass: "drag-item drag-3 optiona3",
        }]
    },
        //slide 2
        {
            contentnocenteradjust: true,
            contentblockadditionalclass: "purplebg",
            uppertextblock: [
                {
                    textclass: "top_text",
                    textdata: data.string.ex2text1,
                },
                {
                    textclass: "top_below_text",
                    textdata: data.string.ex2text12,
                },
                {
                    textclass: "question_text",
                    textdata: data.string.ex2text13,
                },
                {
                    textclass: "drop_area_p",
                }

            ],
            draggableblockadditionalclass: 'drag-drop-container',
            draggableblock:[{
              textdata: data.string.ex2text14,
              divclass: "drag-item drag-1 optiona1 correct",
            },{
              textdata: data.string.ex2text15,
              divclass: "drag-item drag-2 optiona2",
            },{
              textdata: data.string.ex2text16,
              divclass: "drag-item drag-3 optiona3",
            }]
        },

        //slide 3
        {
            contentnocenteradjust: true,
            contentblockadditionalclass: "purplebg",
            uppertextblock: [
                {
                    textclass: "top_text",
                    textdata: data.string.ex2text1,
                },
                {
                    textclass: "top_below_text",
                    textdata: data.string.ex2text17,
                },
                {
                    textclass: "question_text",
                    textdata: data.string.ex2text18,
                },
                {
                    textclass: "drop_area_p",
                }

            ],
            draggableblockadditionalclass: 'drag-drop-container',
            draggableblock:[{
              textdata: data.string.ex2text19,
              divclass: "drag-item drag-1 optiona1 correct",
            },{
              textdata: data.string.ex2text20,
              divclass: "drag-item drag-2 optiona2",
            },{
              textdata: data.string.ex2text21,
              divclass: "drag-item drag-3 optiona3",
            }]
        },


                //slide 4
                {
                    contentnocenteradjust: true,
                    contentblockadditionalclass: "purplebg",
                    uppertextblock: [
                        {
                            textclass: "top_text",
                            textdata: data.string.ex2text1,
                        },
                        {
                            textclass: "top_below_text",
                            textdata: data.string.ex2text22,
                        },
                        {
                            textclass: "question_text",
                            textdata: data.string.ex2text23,
                        },
                        {
                            textclass: "drop_area_p",
                        }

                    ],
                    draggableblockadditionalclass: 'drag-drop-container',
                    draggableblock:[{
                      textdata: data.string.ex2text24,
                      divclass: "drag-item drag-1 optiona1 correct",
                    },{
                      textdata: data.string.ex2text25,
                      divclass: "drag-item drag-2 optiona2",
                    },{
                      textdata: data.string.ex2text26,
                      divclass: "drag-item drag-3 optiona3",
                    }]
                },
        //slide 5
        {
          contentnocenteradjust: true,
          contentblockadditionalclass: "purplebg",
          uppertextblock: [
              {
                  textclass: "top_text",
                  textdata: data.string.ex2text1,
              },
              {
                  textclass: "top_below_text",
                  textdata: data.string.ex2text27,
              },
              {
                  textclass: "question_text",
                  textdata: data.string.ex2text28,
              },
              {
                  textclass: "drop_area_p",
              }

          ],
          draggableblockadditionalclass: 'drag-drop-container',
          draggableblock:[{
            textdata: data.string.ex2text29,
            divclass: "drag-item drag-1 optiona1 correct",
          },{
            textdata: data.string.ex2text30,
            divclass: "drag-item drag-2 optiona2",
          },{
            textdata: data.string.ex2text31,
            divclass: "drag-item drag-3 optiona3",
          }]
        },
        //slide 6
        {
          contentnocenteradjust: true,
          contentblockadditionalclass: "purplebg",
          uppertextblock: [
              {
                  textclass: "top_text",
                  textdata: data.string.ex2text1,
              },
              {
                  textclass: "top_below_text",
                  textdata: data.string.ex2text32,
              },
              {
                  textclass: "question_text",
                  textdata: data.string.ex2text33,
              },
              {
                  textclass: "drop_area_p",
              }

          ],
          draggableblockadditionalclass: 'drag-drop-container',
          draggableblock:[{
            textdata: data.string.ex2text34,
            divclass: "drag-item drag-1 optiona1 correct",
          },{
            textdata: data.string.ex2text35,
            divclass: "drag-item drag-2 optiona2",
          },{
            textdata: data.string.ex2text36,
            divclass: "drag-item drag-3 optiona3",
          }]
        },
        //slide 7
        {
          contentnocenteradjust: true,
          contentblockadditionalclass: "purplebg",
          uppertextblock: [
              {
                  textclass: "top_text",
                  textdata: data.string.ex2text1,
              },
              {
                  textclass: "top_below_text",
                  textdata: data.string.ex2text37,
              },
              {
                  textclass: "question_text",
                  textdata: data.string.ex2text38,
              },
              {
                  textclass: "drop_area_p",
              }

          ],
          draggableblockadditionalclass: 'drag-drop-container',
          draggableblock:[{
            textdata: data.string.ex2text39,
            divclass: "drag-item drag-1 optiona1 correct",
          },{
            textdata: data.string.ex2text40,
            divclass: "drag-item drag-2 optiona2",
          },{
            textdata: data.string.ex2text41,
            divclass: "drag-item drag-3 optiona3",
          }]
        },
        //slide 8
        {
          contentnocenteradjust: true,
          contentblockadditionalclass: "purplebg",
          uppertextblock: [
              {
                  textclass: "top_text",
                  textdata: data.string.ex2text1,
              },
              {
                  textclass: "top_below_text",
                  textdata: data.string.ex2text42,
              },
              {
                  textclass: "question_text",
                  textdata: data.string.ex2text43,
              },
              {
                  textclass: "drop_area_p",
              }

          ],
          draggableblockadditionalclass: 'drag-drop-container',
          draggableblock:[{
            textdata: data.string.ex2text45,
            divclass: "drag-item drag-1 optiona1 correct",
          },{
            textdata: data.string.ex2text44,
            divclass: "drag-item drag-2 optiona2",
          },{
            textdata: data.string.ex2text46,
            divclass: "drag-item drag-3 optiona3",
          }]
        },
        //slide 9
        {
          contentnocenteradjust: true,
          contentblockadditionalclass: "purplebg",
          uppertextblock: [
              {
                  textclass: "top_text",
                  textdata: data.string.ex2text1,
              },
              {
                  textclass: "top_below_text",
                  textdata: data.string.ex2text47,
              },
              {
                  textclass: "question_text",
                  textdata: data.string.ex2text48,
              },
              {
                  textclass: "drop_area_p",
              }

          ],
          draggableblockadditionalclass: 'drag-drop-container',
          draggableblock:[{
            textdata: data.string.ex2text49,
            divclass: "drag-item drag-1 optiona1 correct",
          },{
            textdata: data.string.ex2text50,
            divclass: "drag-item drag-2 optiona2",
          },{
            textdata: data.string.ex2text51,
            divclass: "drag-item drag-3 optiona3",
          }]
        }


];
content.shufflearray();
$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;

    var $playBtn = $('.playBtn');
    var $pauseBtn = $('.pauseBtn');
    var $replayBtn = $('.replayBtn');

    var preload;
    var timeoutvar = null;
    var current_sound;

    var testin = new EggTemplate();

     testin.init(10);

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            // sounds
            {id: "exe_ins_2", src: soundAsset+"exe_ins_2.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        	var $total_page = content.length;
        templateCaller();
    }

    //initialize
    init();



    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
          		/*generate question no at the beginning of question*/
          		testin.numberOfQuestions();
        var wrngClicked = 0;
        $('.ex-number-template-score').css('top','13.5%');
        countNext==0?sound_player("exe_ins_2"):'';
        var draggables = $(".optiona1,.optiona2,.optiona3").draggable({
          containment : "body",
          cursor : "grabbing",
          revert : "invalid",
          appendTo : "body",
          zindex : 10000,
        });


        $('.drop_area_p').droppable({
          accept : ".optiona1,.optiona2,.optiona3",
          hoverClass : "hovered",
          drop : handleCardDrop1
        });

        var positions = [1,2,3];
       positions.shufflearray();
       for(var i=1; i<=3; i++){
         $('.drag-'+i).addClass('drag-pos-'+positions[i-1]);
       }

        function handleCardDrop1(event, ui){
            var dropped = ui.draggable;
            var droppedOn = $(this);

            //dropped at right place
            if(ui.draggable.hasClass("correct")){
              console.log(wrngClicked);
              if(wrngClicked == 0){
	               testin.update(true);
              }
              $('.drop_area_p').html($(ui.draggable).html()).css({'background-color':'#6aa84f','color':'white'});
              $(ui.draggable).hide(0);
               play_correct_incorrect_sound(true);
               draggables.draggable('disable');
               $(".drag-item").css("pointer-events","none");
                navigationcontroller(countNext,$total_page);
               }

                //dropped at wrong place
                else {
                  wrngClicked++;
        	         testin.update(false);
                   dropped.draggable('option', 'revert', true);
                   $(ui.draggable).css({'background-color':'#cc0000','border-color':'#cc0000','color':'white'});
                   play_correct_incorrect_sound(false);
                }

            }
    }
    function put_image(content, count,preload) {
        var contentCount=content[count];
        var imageblockcontent=contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent,contentCount,preload)
    }

    function dynamicimageload(imageblockcontent,contentCount,preload){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }

    function navigationcontroller(islastpageflag) {
        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
        }
        else if ($total_page == 1) {
            $nextBtn.css('display', 'none');

            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if (countNext > 0 && countNext < $total_page) {

            $nextBtn.show(0);
        }
        else if (countNext == $total_page - 2) {

            $nextBtn.css('display', 'none');
            // if lastpageflag is true
        }

    }

    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller(countNext,$total_page):"";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
    }

    $nextBtn.on("click", function(){
      countNext++;
        templateCaller();
        testin.gotoNext();
    });


    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }
    function shufflehint() {
        var optiondiv = $(".bg");
        for (var i = 2; i >= 0; i--) {
            optiondiv.append(optiondiv.find(".commonbtn").eq(Math.random() * i | 0));
        }
        optiondiv.find(".commonbtn").removeClass().addClass("current");
        var a = ["commonbtn option1","commonbtn option2"]
        optiondiv.find(".current").each(function (index) {
            var $this = $(this)
            $this.addClass(a[index]);
        });
        optiondiv.find(".commonbtn").removeClass("current");


    }

});

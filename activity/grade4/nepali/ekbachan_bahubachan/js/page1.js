var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        uppertextblockadditionalclass:"coverpagetext",
        uppertextblock: [
            {
                textclass: "chapter centertext",
                textdata: data.lesson.chapter
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "bg_full",
                    imgid: 'coverpage',
                    imgsrc: ""
                },
            ]
        }]
    },

    //slide1
    {
      contentnocenteradjust: true,
      uppertextblock: [
          {
              textclass: "top_text",
              textdata: data.string.p1text1
          }

      ],
      div_and_image: [{
          imagestoshow: [
              {
                  imgclass: "insideimage",
                  imgid: 'gita',
                  imgdivclass: "left_div"
              },
              {
                  imgdivclass: "right_div",
                  imgid: 'rita',
                  imgclass: "insideimage yeah",
              },
          ]
      }]
    },


        //slide2
        {
          contentnocenteradjust: true,
          uppertextblock: [
              {
                  textclass: "left_text",
                  textdata: data.string.p1text2,
                  datahighlightflag:true,
                  datahighlightcustomclass:'col'
              },
              {
                  textclass: "right_text",
                  textdata: data.string.p1text3,
                  datahighlightflag:true,
                  datahighlightcustomclass:'col'
              }

          ],
          imageblock: [{
              imagestoshow: [
                  {
                      imgclass: "left_img",
                      imgid: 'sabina-eating',
                  },
                  {
                      imgid: 'pabitra-eating',
                      imgclass: "right_img",
                  },
              ]
          }]
        },

        //slide3
        {
          contentnocenteradjust: true,
          uppertextblock: [
              {
                  textclass: "top_text text_slider_animation_class",
                  textdata: data.string.p1text4,
                  datahighlightflag:true,
                  datahighlightcustomclass:'dummy'
              }

          ],
          imageblock: [{
              imagestoshow: [
                  {
                      imgclass: "bg_full",
                      imgid: 'rita-and-her-brother',
                      imgsrc: ""
                  },
              ]
          }]
        },

        //slide4
        {
          contentnocenteradjust: true,
          contentblockadditionalclass:"lightbg",
          uppertextblock: [
              {
                  textclass: "top_text text_slider_animation_class",
                  textdata: data.string.p1text5,
                  datahighlightflag:true,
                  datahighlightcustomclass:'bold'
              },
              {
                  textclass: "middletext_1",
                  textdata: data.string.p1text6,
                  datahighlightflag:true,
                  datahighlightcustomclass:'pink'
              },
              {
                  textclass: "middletext_2",
                  textdata: data.string.p1text7,
                  datahighlightflag:true,
                  datahighlightcustomclass:'pink'
              },

              {
                  textclass: "middletext_3",
                  textdata: data.string.p1text9,
                  datahighlightflag:true,
                  datahighlightcustomclass:'pink'
              },
              {
                  textclass: "ekbachan_text",
                  textdata: data.string.p1text8,
              },
              {
                  textclass: "bahubachan_text",
                  textdata: data.string.p1text10,
              }

          ],
          imageblock: [{
              imagestoshow: [
                  {
                      imgclass: "arrow1",
                      imgid: 'arrow',
                  },
                  {
                      imgclass: "arrow2",
                      imgid: 'arrow',
                  },
                  {
                      imgclass: "arrow3",
                      imgid: 'arrow',
                  },
              ]
          }]
        },

        //slide5
        {
          contentnocenteradjust: true,
          contentblockadditionalclass:"lightbg",
          uppertextblock: [
              {
                  textclass: "center_text",
                  textdata: data.string.p1text11
              }

          ]
        },

        //slide6
        {
          contentnocenteradjust: true,
          uppertextblock: [
              {
                  textclass: "left_text",
                  textdata: data.string.p1text8,
              },
              {
                  textclass: "right_text",
                  textdata: data.string.p1text10,
              }

          ],
          imageblock: [{
              imagestoshow: [
                  {
                      imgclass: "left_img",
                      imgid: 'doing_homework_alone',
                  },
                  {
                      imgclass: 'right_img',
                      imgid: "doing_homework_with_brother",
                  },
              ]
          }],
          speechbox:[{
            textclass:'insidetext',
            textdata:data.string.p1text12,
            speechbox:'speech_left',
            imgid:'textbox'
          },{
            textclass:'insidetext',
            textdata:data.string.p1text13,
            speechbox:'speech_right',
            imgid:'textbox'
          }]
        },

        //slide7
        {
          contentnocenteradjust: true,
          uppertextblock: [
              {
                  textclass: "left_text",
                  textdata: data.string.p1text8,
              },
              {
                  textclass: "right_text",
                  textdata: data.string.p1text10,
              }

          ],
          imageblock: [{
              imagestoshow: [
                  {
                      imgclass: "left_img",
                      imgid: 'playing_alone',
                  },
                  {
                      imgclass: 'right_img',
                      imgid: "playing_with_ram",
                  },
              ]
          }],
          speechbox:[{
            textclass:'insidetext',
            textdata:data.string.p1text14,
            speechbox:'speech_left',
            imgid:'textbox'
          },{
            textclass:'insidetext',
            textdata:data.string.p1text15,
            speechbox:'speech_right',
            imgid:'textbox'
          }]
        },

];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "coverpage", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "gita", src: imgpath+"gita.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rita", src: imgpath+"rita.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sabina-eating", src: imgpath+"sabina-eating.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rita-and-her-brother", src: imgpath+"rita-and-her-brother.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pabitra-eating", src: imgpath+"pabitra-eating.png", type: createjs.AbstractLoader.IMAGE},
            {id: "arrow", src: imgpath+"arrow.png", type: createjs.AbstractLoader.IMAGE},
            {id: "doing_homework_alone", src: imgpath+"doing_homework_alone.png", type: createjs.AbstractLoader.IMAGE},
            {id: "doing_homework_with_brother", src: imgpath+"doing_homework_with_brother.png", type: createjs.AbstractLoader.IMAGE},
            {id: "playing_alone", src: imgpath+"playing_alone.png", type: createjs.AbstractLoader.IMAGE},
            {id: "playing_with_ram", src: imgpath+"playing_with_ram.png", type: createjs.AbstractLoader.IMAGE},

            {id: "textbox", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},


            // sounds
            {id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
            {id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
            {id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
            {id: "s1_p3_1", src: soundAsset+"s1_p3_1.ogg"},
            {id: "s1_p4", src: soundAsset+"s1_p4.ogg"},
            {id: "s1_p5", src: soundAsset+"s1_p5.ogg"},
            {id: "s1_p5_1", src: soundAsset+"s1_p5_1.ogg"},
            {id: "s1_p6", src: soundAsset+"s1_p6.ogg"},
            {id: "s1_p7", src: soundAsset+"s1_p7(heading).ogg"},
            {id: "s1_p7_1", src: soundAsset+"s1_p7_1.ogg"},
            {id: "s1_p7_2", src: soundAsset+"s1_p7_2.ogg"},
            {id: "s1_p8", src: soundAsset+"s1_p8.ogg"},
            {id: "s1_p8_1", src: soundAsset+"s1_p8_1.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        put_image1(content, countNext,preload);
        put_speechbox_image(content, countNext, preload);
        switch(countNext){

            case 1:
              sound_player("s1_p"+(countNext+1),1);
            var topval=($('.top_text').height()/$('.coverboardfull ').height())*100+3;
            $('.left_div').css({'height':'0%',
                                            'top':topval+'%'});
            $('.right_div').css({'height':'0%',
                                            'bottom':'0%'});
            $('.left_div,.right_div').animate({
              'height':100-topval+'%'
            },1500,function(){
            });
            break;
            case 2:
              $('.right_text,.left_text').hide(0);
              $('.left_img').css({'height':'100%','top':'-100%'});
              $('.right_img').css({'height':'100%','bottom':'-100%'});
              $('.left_img').animate({
              'top':'0%'
              },1000);
              $('.left_text').delay(1000).fadeIn(1000);
              createjs.Sound.stop();
              current_sound = createjs.Sound.play("s1_p3");
              current_sound.play();
              current_sound.on('complete', function(){
              $('.right_img').animate({
              'top':'0%'
              },1000);
              $('.right_text').delay(1000).fadeIn(1000);
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("s1_p3_1");
                current_sound.play();
                current_sound.on('complete', function(){
                  nav_button_controls();
                });
              });
            break;
            case 4:
              $('.middletext_1,.middletext_2,.middletext_3,.arrow1,.arrow2,.arrow3,.ekbachan_text,.bahubachan_text').hide(0);
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("s1_p"+(countNext+1));
                current_sound.play();
                current_sound.on('complete', function () {
                      sound_player("s1_p5_1",1);
                      $('.middletext_1').fadeIn(1000,function(){
                        $('.middletext_2').delay(1500).fadeIn(1000,function(){
                          $('.arrow1,.arrow2').delay(1500).fadeIn(1000,function(){
                            $('.ekbachan_text').delay(500).fadeIn(1000,function(){
                              $('.middletext_3').delay(500).fadeIn(1000,function(){
                                $('.arrow3').delay(1500).fadeIn(1000,function(){
                                  $('.bahubachan_text').fadeIn(1000,function(){
                                    // navigationcontroller(countNext,$total_page);
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                });

            break;

            case 6:
              $('.speech_left,.speech_right').hide(0);
              createjs.Sound.stop();
              current_sound = createjs.Sound.play("s1_p7");
              current_sound.play();
              current_sound.on('complete', function(){
                $('.speech_left').fadeIn(1000);
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("s1_p7_1");
                current_sound.play();
                current_sound.on('complete', function(){
                  $('.speech_right').fadeIn(1000);
                  createjs.Sound.stop();
                  current_sound = createjs.Sound.play("s1_p7_2");
                  current_sound.play();
                  current_sound.on('complete', function(){
                    nav_button_controls();
                  });
                });
              });
            break;
            case 7:
              $('.speech_left,.speech_right').hide(0);
              $('.speech_left').fadeIn(1000);
              createjs.Sound.stop();
              current_sound = createjs.Sound.play("s1_p8");
              current_sound.play();
              current_sound.on('complete', function(){
                 $('.speech_right').fadeIn(1000);
                  createjs.Sound.stop();
                  current_sound = createjs.Sound.play("s1_p8_1");
                  current_sound.play();
                  current_sound.on('complete', function(){
                    nav_button_controls();
                  });
              });
            break;
            default:
              sound_player("s1_p"+(countNext+1),1);
            break;
        }
    }


  function nav_button_controls(delay_ms){
    timeoutvar = setTimeout(function(){
      if(countNext==0){
        $nextBtn.show(0);
      } else if( countNext>0 && countNext == $total_page-1){
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else{
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    },delay_ms);
  }
    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?nav_button_controls():"";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }
});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "pinkbg",
        uppertextblock: [
            {
                textclass: "left_text",
                textdata: data.string.p2text1,
                datahighlightflag:true,
                datahighlightcustomclass:'focus1'
            },
            {
                textclass: "right_text",
                textdata: data.string.p2text2,
                datahighlightflag:true,
                datahighlightcustomclass:'focus2'
            }
        ],
        imageblock:[{
          imagestoshow:[{
            imgid:'one_cow',
            imgclass:'left_img'
          },{
            imgid:'three_cow',
            imgclass:'right_img'
          }]
        }]
    },


    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "pinkbg",
        uppertextblock: [
            {
                textclass: "left_text",
                textdata: data.string.p2text3,
                datahighlightflag:true,
                datahighlightcustomclass:'focus1'
            },
            {
                textclass: "right_text",
                textdata: data.string.p2text4,
                datahighlightflag:true,
                datahighlightcustomclass:'focus2'
            }
        ],
        imageblock:[{
          imagestoshow:[{
            imgid:'one_flower',
            imgclass:'left_img'
          },{
            imgid:'three_flower',
            imgclass:'right_img'
          }]
        }]
    },


    // slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "pinkbg",
        uppertextblock: [
            {
                textclass: "left_text",
                textdata: data.string.p2text5,
                datahighlightflag:true,
                datahighlightcustomclass:'focus1'
            },
            {
                textclass: "right_text",
                textdata: data.string.p2text6,
                datahighlightflag:true,
                datahighlightcustomclass:'focus2'
            }
        ],
        imageblock:[{
          imagestoshow:[{
            imgid:'one_fox',
            imgclass:'left_img'
          },{
            imgid:'two_fox',
            imgclass:'right_img'
          }]
        }]
    },

    // slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "pinkbg",
        uppertextblock: [
            {
                textclass: "left_text",
                textdata: data.string.p2text7,
                datahighlightflag:true,
                datahighlightcustomclass:'focus1'
            },
            {
                textclass: "right_text",
                textdata: data.string.p2text8,
                datahighlightflag:true,
                datahighlightcustomclass:'focus2'
            }
        ],
        imageblock:[{
          imagestoshow:[{
            imgid:'one_bird',
            imgclass:'left_img'
          },{
            imgid:'two_bird',
            imgclass:'right_img'
          }]
        }]
    },

    //slide4
    {
      contentnocenteradjust: true,
      uppertextblock: [
          {
              textclass: "left_text1",
              textdata: data.string.p2text3,
              datahighlightflag:true,
              datahighlightcustomclass:'focus1'
          },
          {
              textclass: "left_text2",
              textdata: data.string.p2text5,
              datahighlightflag:true,
              datahighlightcustomclass:'focus3'
          },
          {
              textclass: "left_text3",
              textdata: data.string.p2text7,
              datahighlightflag:true,
              datahighlightcustomclass:'focus5'
          },
          {
              textclass: "right_text1",
              textdata: data.string.p2text4,
              datahighlightflag:true,
              datahighlightcustomclass:'focus2'
          },
          {
              textclass: "right_text2",
              textdata: data.string.p2text6,
              datahighlightflag:true,
              datahighlightcustomclass:'focus4'
          },
          {
              textclass: "right_text3",
              textdata: data.string.p2text8,
              datahighlightflag:true,
              datahighlightcustomclass:'focus6'
          },
          {
              textclass: "top_left_text",
              textdata: data.string.p1text8,
          },
          {
              textclass: "top_right_text",
              textdata: data.string.p1text10,
          },
          {
              textclass: "left_bg",
          },
          {
              textclass: "right_bg",
          }
      ],
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "one_cow", src: imgpath+"one_cow.png", type: createjs.AbstractLoader.IMAGE},
            {id: "three_cow", src: imgpath+"three_cow.png", type: createjs.AbstractLoader.IMAGE},
            {id: "one_flower", src: imgpath+"one_flower.png", type: createjs.AbstractLoader.IMAGE},
            {id: "three_flower", src: imgpath+"three_flower.png", type: createjs.AbstractLoader.IMAGE},
            {id: "one_fox", src: imgpath+"one_fox.png", type: createjs.AbstractLoader.IMAGE},
            {id: "two_fox", src: imgpath+"two_fox.png", type: createjs.AbstractLoader.IMAGE},
            {id: "one_bird", src: imgpath+"one_bird.png", type: createjs.AbstractLoader.IMAGE},
            {id: "two_bird", src: imgpath+"two_bird.png", type: createjs.AbstractLoader.IMAGE},
            // sounds
            {id: "s2_p1", src: soundAsset+"s2_p1.ogg"},
            {id: "s2_p1_1", src: soundAsset+"s2_p1_1.ogg"},
            {id: "s2_p2", src: soundAsset+"s2_p2.ogg"},
            {id: "s2_p2_1", src: soundAsset+"s2_p2_1.ogg"},
            {id: "s2_p3", src: soundAsset+"s2_p3.ogg"},
            {id: "s2_p3_1", src: soundAsset+"s2_p3_1.ogg"},
            {id: "s2_p4", src: soundAsset+"s2_p4.ogg"},
            {id: "s2_p4_1", src: soundAsset+"s2_p4_1.ogg"},
            {id: "s2_p5", src: soundAsset+"s2_p5(heading).ogg"},

        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        switch(countNext){
            case 0:
            case 1:
            case 2:
            case 3: 
                $('.left_text,.left_img,.right_text,.right_img').hide(0);             
              $('.left_text,.left_img').fadeIn(400);             
              createjs.Sound.stop();
              current_sound = createjs.Sound.play("s2_p"+(countNext+1));
              current_sound.play();
              current_sound.on('complete', function () {
                $('.focus1').addClass('animateVerb');
                $('.right_text,.right_img').fadeIn(400); 
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("s2_p"+(countNext+1)+"_1");
                current_sound.play();
                current_sound.on('complete', function () {
                  $('.focus2').addClass('animateVerb');
                  nav_button_controls();                  
                });
              });
                break;


            case 4:
              $('.left_text1,.left_text2,.left_text3,.right_text1,.right_text2,.right_text3').hide(0);
              sound_player("s2_p5",0);
              createjs.Sound.stop();
              current_sound = createjs.Sound.play("s2_p5");
              current_sound.play();
              current_sound.on("complete", function(){
                $('.left_text1').fadeIn(1000);
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("s2_p2");
                current_sound.play();
                current_sound.on("complete", function(){
                  $('.focus1').animate({"color": "#f7166d","font-size": "5vmin"},500);
                  $('.right_text1').fadeIn(1000);
                  createjs.Sound.stop();
                  current_sound = createjs.Sound.play("s2_p2_1");
                  current_sound.play();
                  current_sound.on("complete", function(){
                    $('.focus2').animate({"color": "#f7166d","font-size": "5vmin"},500);
                    $('.left_text2').fadeIn(1000);
                    createjs.Sound.stop();
                    current_sound = createjs.Sound.play("s2_p3");
                    current_sound.play();
                    current_sound.on("complete", function(){
                      $('.focus3').animate({"color": "#f7166d","font-size": "5vmin"},500);
                      $('.right_text2').fadeIn(1000);
                      createjs.Sound.stop();
                      current_sound = createjs.Sound.play("s2_p3_1");
                      current_sound.play();
                      current_sound.on("complete", function(){
                        $('.focus4').animate({"color": "#f7166d","font-size": "5vmin"},500);
                        $('.left_text3').fadeIn(1000);
                        createjs.Sound.stop();
                        current_sound = createjs.Sound.play("s2_p4");
                        current_sound.play();
                        current_sound.on("complete", function(){
                          $('.focus5').animate({"color": "#f7166d","font-size": "5vmin"},500);
                          $('.right_text3').fadeIn(1000);
                          createjs.Sound.stop();
                          current_sound = createjs.Sound.play("s2_p4_1");
                          current_sound.play();
                          current_sound.on("complete", function(){
                            $('.focus6').animate({"color": "#f7166d","font-size": "5vmin"},500);
                            nav_button_controls();                          
                          });                           
                        });                         
                      });                      
                    });                    
                  });
                });
              });
            break;

            default:
                navigationcontroller(countNext,$total_page);
                break;
        }
    }

  function nav_button_controls(delay_ms){
    timeoutvar = setTimeout(function(){
      if(countNext==0){
        $nextBtn.show(0);
      } else if( countNext>0 && countNext == $total_page-1){
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else{
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    },delay_ms);
  }

    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?nav_button_controls():"";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }
});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    //slide 0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg_blue",
        uppertextblock: [
            {
                textclass: "top_text",
                textdata: data.string.p5text1
            },
            {
                textclass: "question_top",
                textdata: data.string.p5text2
            },
            {
                textclass: "question_below",
                textdata: data.string.p5text3
            },
            {
                textclass: "option1 correct",
                textdata: data.string.p5text4
            },
            {
                textclass: "option2",
                textdata: data.string.p5text5
            },
            {
                textclass: "middle_area_bg",
            },
            {
                textclass: "drop_area",
            },
        ],
    },
    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg_blue",
        uppertextblock: [
            {
                textclass: "top_text",
                textdata: data.string.p5text1
            },
            {
                textclass: "question_top",
                textdata: data.string.p5text10
            },
            {
                textclass: "question_below",
                textdata: data.string.p5text11
            },
            {
                textclass: "option1 ",
                textdata: data.string.p5text13
            },
            {
                textclass: "option2 correct",
                textdata: data.string.p5text12
            },
            {
                textclass: "middle_area_bg",
            },
            {
                textclass: "drop_area",
            },
        ],
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg_blue",
        uppertextblock: [
            {
                textclass: "top_text",
                textdata: data.string.p5text1
            },
            {
                textclass: "question_top",
                textdata: data.string.p5text14
            },
            {
                textclass: "question_below",
                textdata: data.string.p5text15
            },
            {
                textclass: "option1 correct",
                textdata: data.string.p5text16
            },
            {
                textclass: "option2",
                textdata: data.string.p5text17
            },
            {
                textclass: "middle_area_bg",
            },
            {
                textclass: "drop_area",
            },
        ],
    },

];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "boygrbImg", src: imgpath+"boygrb.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "s5_p1", src: soundAsset+"s5_p1(instruction).ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        countNext==0?sound_player("s5_p1"):'';
        var draggables = $(".option1,.option2").draggable({
          containment : "body",
          cursor : "grabbing",
          revert : "invalid",
          appendTo : "body",
          zindex : 10000,
        });


        $('.drop_area').droppable({
          accept : ".option1,.option2",
          hoverClass : "hovered",
          drop : handleCardDrop1
        });


        function handleCardDrop1(event, ui){
            var dropped = ui.draggable;
            var droppedOn = $(this);

            //dropped at right place
            if(ui.draggable.hasClass("correct")){
              $('.drop_area').html($(ui.draggable).html()).css({'background-color':'#6aa84f','color':'white'});
              $(ui.draggable).hide(0);
               play_correct_incorrect_sound(true);
               draggables.draggable('disable');
                navigationcontroller(countNext,$total_page);
               }

                //dropped at wrong place
                else {
                   dropped.draggable('option', 'revert', true);
                   $(ui.draggable).css({'background-color':'#cc0000','border-color':'#cc0000'});
                   play_correct_incorrect_sound(false);
                }

            }
    }



    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller(countNext,$total_page):"";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }

});

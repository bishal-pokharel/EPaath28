var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bluebg",
        uppertextblock: [
            {
                textclass: "left_text",
                textdata: data.string.p4text2,
                datahighlightflag:true,
                datahighlightcustomclass:'focus1'
            },
            {
                textclass: "right_text",
                textdata: data.string.p4text1,
                datahighlightflag:true,
                datahighlightcustomclass:'focus2'
            }
        ],
        imageblock:[{
          imagestoshow:[{
            imgid:'rajan_singing',
            imgclass:'left_img'
          },{
            imgid:'ramita_singing',
            imgclass:'right_img'
          }]
        }]
    },


    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bluebg",
        uppertextblock: [
            {
                textclass: "left_text",
                textdata: data.string.p4text2,
                datahighlightflag:true,
                datahighlightcustomclass:'focus1'
            },
            {
                textclass: "right_text",
                textdata: data.string.p4text1,
                datahighlightflag:true,
                datahighlightcustomclass:'focus2'
            },
            {
                textclass: "center_text",
                textdata: data.string.p4text3,
                datahighlightflag:true,
                datahighlightcustomclass:'animateVerb'
            }
        ],
        imageblock:[{
          imagestoshow:[{
            imgid:'rajan_singing',
            imgclass:'left_img'
          },{
            imgid:'ramita_singing',
            imgclass:'right_img'
          },{
            imgid:'rajan-and-ramita-singing',
            imgclass:'center_img'
          }]
        }]
    },
    // slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bluebg",
        uppertextblock: [
            {
                textclass: "left_text",
                textdata: data.string.p4text4,
                datahighlightflag:true,
                datahighlightcustomclass:'focus1'
            },
            {
                textclass: "right_text",
                textdata: data.string.p4text5,
                datahighlightflag:true,
                datahighlightcustomclass:'focus2'
            }
        ],
        imageblock:[{
          imagestoshow:[{
            imgid:'dog_chase_nouse',
            imgclass:'left_img'
          },{
            imgid:'cat_chase_nouse',
            imgclass:'right_img'
          }]
        }]
    },


    // slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bluebg",
        uppertextblock: [
            {
                textclass: "left_text",
                textdata: data.string.p4text4,
                datahighlightflag:true,
                datahighlightcustomclass:'focus1'
            },
            {
                textclass: "right_text",
                textdata: data.string.p4text5,
                datahighlightflag:true,
                datahighlightcustomclass:'focus2'
            },
            {
                textclass: "center_text",
                textdata: data.string.p4text6,
                datahighlightflag:true,
                datahighlightcustomclass:'animateVerb'
            }
        ],
        imageblock:[{
          imagestoshow:[{
            imgid:'dog_chase_nouse',
            imgclass:'left_img'
          },{
            imgid:'cat_chase_nouse',
            imgclass:'right_img'
          },{
            imgid:'cat_and_dog_chase_mouse',
            imgclass:'center_img'
          }]
        }]
    },

    // slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bluebg",
        uppertextblock: [
            {
                textclass: "left_text",
                textdata: data.string.p4text8,
                datahighlightflag:true,
                datahighlightcustomclass:'focus1'
            },
            {
                textclass: "right_text",
                textdata: data.string.p4text7,
                datahighlightflag:true,
                datahighlightcustomclass:'focus2'
            }
        ],
        imageblock:[{
          imagestoshow:[{
            imgid:'hari_cooking',
            imgclass:'left_img'
          },{
            imgid:'anusha_cooking',
            imgclass:'right_img'
          }]
        }]
    },


    // slide5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bluebg",
        uppertextblock: [
            {
                textclass: "left_text",
                textdata: data.string.p4text8,
                datahighlightflag:true,
                datahighlightcustomclass:'focus1'
            },
            {
                textclass: "right_text",
                textdata: data.string.p4text7,
                datahighlightflag:true,
                datahighlightcustomclass:'focus2'
            },
            {
                textclass: "center_text",
                textdata: data.string.p4text9,
                datahighlightflag:true,
                datahighlightcustomclass:'animateVerb'
            }
        ],
        imageblock:[{
          imagestoshow:[{
            imgid:'hari_cooking',
            imgclass:'left_img'
          },{
            imgid:'anusha_cooking',
            imgclass:'right_img'
          },{
            imgid:'hari_anusha_cooking',
            imgclass:'center_img'
          }]
        }]
    },

];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "rajan_singing", src: imgpath+"rajan_singing.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ramita_singing", src: imgpath+"ramita_singing.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dog_chase_nouse", src: imgpath+"dog_chase_nouse.png", type: createjs.AbstractLoader.IMAGE},
            {id: "cat_chase_nouse", src: imgpath+"cat_chase_nouse.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rajan-and-ramita-singing", src: imgpath+"rajan-and-ramita-singing.png", type: createjs.AbstractLoader.IMAGE},
            {id: "cat_and_dog_chase_mouse", src: imgpath+"cat_and_dog_chase_mouse.png", type: createjs.AbstractLoader.IMAGE},
            {id: "anusha_cooking", src: imgpath+"anusha_cooking.png", type: createjs.AbstractLoader.IMAGE},
            {id: "hari_cooking", src: imgpath+"hari_cooking.png", type: createjs.AbstractLoader.IMAGE},
            {id: "hari_anusha_cooking", src: imgpath+"hari_anusha_cooking.png", type: createjs.AbstractLoader.IMAGE},
            // sounds
            {id: "s4_p1", src: soundAsset+"s4_p1.ogg"},
            {id: "s4_p1_1", src: soundAsset+"s4_p1_1.ogg"},
            {id: "s4_p2", src: soundAsset+"s4_p2.ogg"},
            {id: "s4_p3", src: soundAsset+"s4_p3.ogg"},
            {id: "s4_p3_1", src: soundAsset+"s4_p3_1.ogg"},
            {id: "s4_p4", src: soundAsset+"s4_p4.ogg"},
            {id: "s4_p5", src: soundAsset+"s4_p5.ogg"},
            {id: "s4_p5_1", src: soundAsset+"s4_p5_1.ogg"},
            {id: "s4_p6", src: soundAsset+"s4_p6.ogg"},

        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        switch(countNext){
            case 0:
            case 2:
            case 4:
                $('.left_text,.left_img,.right_text,.right_img').hide(0);
                $('.left_text,.left_img').show(0);
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("s4_p"+(countNext+1));
                current_sound.play();
                current_sound.on('complete', function () {
                    $('.focus1').addClass('animateVerb');
                    $('.right_text,.right_img').show(0);
                    createjs.Sound.stop();
                    current_sound = createjs.Sound.play("s4_p"+(countNext+1)+"_1");
                    current_sound.play();
                    current_sound.on('complete', function () {
                        $('.focus2').addClass('animateVerb')
                        nav_button_controls();
                    });
                });

                break;
            case 1:
            case 3:
            case 5:
              $('.center_text,.center_img').hide(0);
              setTimeout(function(){sound_player("s4_p"+(countNext+1),1);},2000);
              $('.left_text,.right_text').fadeOut(1000,function(){
                      $('.left_img').animate({'left':'14%','border-top-right-radius':'0vmin','border-bottom-right-radius':'0vmin'},1000);
                      $('.right_img').animate({'left':'50%','border-top-left-radius':'0vmin','border-bottom-left-radius':'0vmin'},1000,function(){
                        $('.left_img,.right_img').fadeOut(500);
                        $('.center_img').fadeIn(500,function(){
                        $('.center_text').fadeIn(500);
                        });
                  });
              });
          break;
        }
    }
      function nav_button_controls(delay_ms){
        timeoutvar = setTimeout(function(){
          if(countNext==0){
            $nextBtn.show(0);
          } else if( countNext>0 && countNext == $total_page-1){
            $prevBtn.show(0);
            ole.footerNotificationHandler.pageEndSetNotification();
          } else{
            $prevBtn.show(0);
            $nextBtn.show(0);
          }
        },delay_ms);
      }

    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?nav_button_controls():"";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }
});

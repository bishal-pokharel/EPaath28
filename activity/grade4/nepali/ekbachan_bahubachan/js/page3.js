var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
  // slide0
  {
      contentnocenteradjust: true,
      contentblockadditionalclass: "bg_purple",
      uppertextblock: [
          {
              textclass: "centertext",
              textdata: data.string.diy
          }

      ],
      imageblock: [{
          imagestoshow: [
              {
                  imgclass: "bg_full",
                  imgid: 'rainbow',
              },
          ]
      }],
  },


    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg_purple",
        uppertextblock: [
            {
                textclass: "top_text",
                textdata: data.string.p3text1
            },
            {
                textclass: "question_text",
                textdata: data.string.p3text2
            }
        ],
        exetype1:[{
          optionsdivclass:"optionsdiv",
          exeoptions:[{
            optaddclass:"opti opti1 correct",
            optdata:data.string.p3text3
          },{
            optaddclass:"opti opti2",
            optdata:data.string.p3text4
          },{
            optaddclass:"opti opti3",
            optdata:data.string.p3text5
          }]
        }],
        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "left_bot_img",
                    imgid: 'lion',
                },
            ]
        }],
    },

    // slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg_purple",
        uppertextblock: [
            {
                textclass: "top_text",
                textdata: data.string.p3text1
            },
            {
                textclass: "question_text",
                textdata: data.string.p3text6
            }
        ],
        exetype1:[{
          optionsdivclass:"optionsdiv",
          exeoptions:[{
            optaddclass:"opti opti1",
            optdata:data.string.p3text8
          },{
            optaddclass:"opti opti2 correct",
            optdata:data.string.p3text7
          },{
            optaddclass:"opti opti3",
            optdata:data.string.p3text9
          }]
        }],
        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "left_bot_img",
                    imgid: 'frog',
                },
            ]
        }],
    },

    // slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg_purple",
        uppertextblock: [
            {
                textclass: "top_text",
                textdata: data.string.p3text1
            },
            {
                textclass: "question_text",
                textdata: data.string.p3text10
            }
        ],
        exetype1:[{
          optionsdivclass:"optionsdiv",
          exeoptions:[{
            optaddclass:"opti opti1 correct",
            optdata:data.string.p3text11
          },{
            optaddclass:"opti opti2",
            optdata:data.string.p3text12
          },{
            optaddclass:"opti opti3 ",
            optdata:data.string.p3text13
          }]
        }],
        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "left_bot_img",
                    imgid: 'horse',
                },
            ]
        }],
    },

    // slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg_purple",
        uppertextblock: [
            {
                textclass: "top_text",
                textdata: data.string.p3text1
            },
            {
                textclass: "question_text",
                textdata: data.string.p3text14
            }
        ],
        exetype1:[{
          optionsdivclass:"optionsdiv",
          exeoptions:[{
            optaddclass:"opti opti1",
            optdata:data.string.p3text17
          },{
            optaddclass:"opti opti2",
            optdata:data.string.p3text16
          },{
            optaddclass:"opti opti3 correct",
            optdata:data.string.p3text15
          }]
        }],
        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "left_bot_img",
                    imgid: 'cutting_grass',
                },
            ]
        }],
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "rainbow", src: imgpath+"rainbow.png", type: createjs.AbstractLoader.IMAGE},
            {id: "lion", src: imgpath+"lion.png", type: createjs.AbstractLoader.IMAGE},
            {id: "frog", src: imgpath+"frog.png", type: createjs.AbstractLoader.IMAGE},
            {id: "horse", src: imgpath+"horse.png", type: createjs.AbstractLoader.IMAGE},
            {id: "cutting_grass", src: imgpath+"cutting_grass.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "s3_p1", src: soundAsset+"s3_p1(instruction).ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);

        // random number generator
    			function rand_generator(limit){
    				var randNum = Math.floor(Math.random() * (limit - 1 +1)) + 1;
    				return randNum;
    			}

    			/*for randomizing the options*/
    			function randomize(parent){
    				var parent = $(parent);
    				var divs = parent.children();
    				while (divs.length) {
    				parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
    				}
    			}

        switch(countNext){
            case 0:
              play_diy_audio();
                setTimeout(function(){
                  navigationcontroller(countNext,$total_page);
                },2000);
                break;
                case 1:
                case 2:
                case 3:
                case 4:
                  countNext==1?sound_player("s3_p1"):'';
                randomize(".optionsdiv");
                    break;
        }
        $(".optionscontainer").click(function(){
          if($(this).hasClass("correct")){
                  var $this = $(this);
                  $(" .optionscontainer").css("pointer-events","none");
                  $('.correct_answer').html($(this).html()).css('color','#9900ff');
                  var position = $this.position();
                  var width = $this.width();
                  var height = $this.height();
                  var centerX = ((position.left + width / 2)*100)/$('.coverboardfull  ').width()+'%';
                  var centerY = ((position.top + height)*100)/$('.coverboardfull  ').height()+'%';
                  // $('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:10%;transform:translate(-1%,-199%);z-index:10" src="'+imgpath +'correct.png" />').insertAfter(this);
                  // $('.option1,.option2,.option3 ').css({"pointer-events":"none"});
                  $(this).css({"background":"rgb(190,214,47)","color":"white","border-color":"#e0ff59"});
                  play_correct_incorrect_sound(1);
                 navigationcontroller(countNext,$total_page);
          }
          else {
          $(this).css("pointer-events","none");
           var $this = $(this);
          var position = $this.position();
          var width = $this.width();
           var height = $this.height();
           var centerX = ((position.left + width / 2)*100)/$('.coverboardfull  ').width()+'%';
           var centerY = ((position.top + height)*100)/$('.coverboardfull  ').height()+'%';
            // $('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-1%,-199%);z-index:10" src="'+imgpath +'incorrect.png" />').insertAfter(this);
           play_correct_incorrect_sound(0);
           $(this).css({"background":"rgb(168, 33, 36)","color":"white","pointer-events":"none","border-color":"#980000"});
        }
        });

    //         $('.option1,.option2,.option3 ').click(function(){
    //         if($(this).hasClass('correct'))
    //     {
    //      var $this = $(this);
    //        $('.correct_answer').html($(this).html()).css('color','#9900ff');
    //        var position = $this.position();
    //        var width = $this.width();
    //      var height = $this.height();
    //         var centerX = ((position.left + width / 2)*100)/$('.coverboardfull  ').width()+'%';
    //       var centerY = ((position.top + height)*100)/$('.coverboardfull  ').height()+'%';
    //       $('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-1%,-199%);z-index:10" src="'+imgpath +'correct.png" />').insertAfter(this);
    //           $('.option1,.option2,.option3 ').css({"pointer-events":"none"});
    //     $(this).css({"background":"rgb(190,214,47)","color":"white","border-color":"#e0ff59"});
    //    play_correct_incorrect_sound(1);
    //           navigationcontroller(countNext,$total_page);
    // }
     //       else {
     //        var $this = $(this);
     //         var position = $this.position();
     //          var width = $this.width();
     //        var height = $this.height();
     //      var centerX = ((position.left + width / 2)*100)/$('.coverboardfull  ').width()+'%';
     // var centerY = ((position.top + height)*100)/$('.coverboardfull  ').height()+'%';
     //       $('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-1%,-199%);z-index:10" src="'+imgpath +'incorrect.png" />').insertAfter(this);
     //      play_correct_incorrect_sound(0);
     //   $(this).css({"background":"rgb(168, 33, 36)","color":"white","pointer-events":"none","border-color":"#980000"});
     //   }
    //   });
    //


    }

    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }
});

var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/orgSnds/";

var content = [
  // coverpage
  {
    contentblockadditionalclass: "thebg1",
    extratextblock: [
      {
        textclass: "cvChpter",
        textdata: data.lesson.chapter
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "background",
            imgid: "cover_page_pg0",
            imgsrc: ""
          }
        ]
      }
    ]
  },
  // slide 2
  {
    contentblockadditionalclass: "thebg1",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "background",
            imgid: "cover_page",
            imgsrc: ""
          },
          {
            imgclass: "ashaHome inverted",
            imgid: "asha03",
            imgsrc: ""
          },
          {
            imgclass: "inverted nitiHome",
            imgid: "niti04a",
            imgsrc: ""
          },
          {
            imgclass: "inverted sahilHome",
            imgid: "saahil_01",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        speechbox: "sp-1-txtSz4-sps1",
        imgclass: "",
        imgid: "textbox_1",
        imgsrc: "",
        textclass: "textInSp",
        textdata: data.string.p1s2_txt
      }
    ]
  },
  // slide 3
  {
    contentblockadditionalclass: "thebg1",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "background",
            imgid: "cover_page",
            imgsrc: ""
          },
          {
            imgclass: "ashaHome inverted",
            imgid: "asha03",
            imgsrc: ""
          },
          {
            imgclass: "inverted nitiHome",
            imgid: "niti04a",
            imgsrc: ""
          },
          {
            imgclass: "inverted sahilHome",
            imgid: "saahil_01",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        speechbox: "sp-1-txtSz3-sps1-l49",
        imgclass: "",
        imgid: "textbox_1",
        imgsrc: "",
        textclass: "textInSp",
        textdata: data.string.p1s3_txt
      }
    ]
  },
  // slide 4
  {
    contentblockadditionalclass: "thebg1",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "background",
            imgid: "cover_page",
            imgsrc: ""
          },
          {
            imgclass: "ashaHome inverted",
            imgid: "asha03",
            imgsrc: ""
          },
          {
            imgclass: "inverted nitiHome",
            imgid: "niti04a",
            imgsrc: ""
          },
          {
            imgclass: "inverted sahilHome",
            imgid: "saahil_01",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        speechbox: "sp-1-txtSz3-sps1-l49",
        imgclass: "",
        imgid: "textbox_1",
        imgsrc: "",
        textclass: "textInSp",
        textdata: data.string.p1s4_txt
      }
    ]
  },
  // slide 5
  {
    contentblockadditionalclass: "thebg1",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "background",
            imgid: "cover_page",
            imgsrc: ""
          },
          {
            imgclass: "ashaHome inverted",
            imgid: "asha03",
            imgsrc: ""
          },
          {
            imgclass: "inverted nitiHome",
            imgid: "niti04a",
            imgsrc: ""
          },
          {
            imgclass: "inverted sahilHome",
            imgid: "saahil_01",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        speechbox: "sp-1-txtSz3-sps1",
        imgclass: "",
        imgid: "textbox_1",
        imgsrc: "",
        textclass: "textInSp",
        textdata: data.string.p1s5_txt
      }
    ]
  },
  // slide 6
  {
    contentblockadditionalclass: "thebg1",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "background",
            imgid: "gaijatra",
            imgsrc: ""
          },
          {
            imgclass: "asha-inverted",
            imgid: "asha01",
            imgsrc: ""
          },
          {
            imgclass: "niti",
            imgid: "niti01a",
            imgsrc: ""
          },
          {
            imgclass: "sahil",
            imgid: "saahil_01",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        speechbox: "sp-1-sps6-txtSz3",
        imgclass: "",
        imgid: "textbox_1",
        imgsrc: "",
        textclass: "textInSp",
        textdata: data.string.p1s6_txt
      }
    ]
  },
  // slide 7
  {
    contentblockadditionalclass: "thebg1",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "background",
            imgid: "gaijatra",
            imgsrc: ""
          },
          {
            imgclass: "asha-inverted",
            imgid: "asha01",
            imgsrc: ""
          },
          {
            imgclass: "niti",
            imgid: "niti01a",
            imgsrc: ""
          },
          {
            imgclass: "sahil-shls7 ",
            imgid: "man-talking1",
            imgsrc: ""
          },
          {
            imgclass: "sahil-shls7 manhide",
            imgid: "man-talking",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        speechbox: "sp-1-sps7-txtSz3",
        imgclass: "inverted",
        imgid: "textbox_1",
        imgsrc: "",
        textclass: "textInSp",
        textdata: data.string.p1s7_txt
      }
    ]
  },
  // slide 8
  {
    contentblockadditionalclass: "thebg1",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "background",
            imgid: "gaijatra",
            imgsrc: ""
          },
          {
            imgclass: "asha-inverted-wdth9",
            imgid: "asha03",
            imgsrc: ""
          },
          {
            imgclass: "niti-wdth5",
            imgid: "niti02a",
            imgsrc: ""
          },
          {
            imgclass: "sahil",
            imgid: "saahil_01",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        speechbox: "sp-1-sps8-txtSz3hlf",
        imgclass: "inverted",
        imgid: "textbox_1",
        imgsrc: "",
        textclass: "textInSp",
        textdata: data.string.p1s8_txt
      }
    ]
  },
  // slide 9
  {
    contentblockadditionalclass: "thebg1",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "background",
            imgid: "gaijatra",
            imgsrc: ""
          },
          {
            imgclass: "asha-inverted",
            imgid: "asha01",
            imgsrc: ""
          },
          {
            imgclass: "niti-wdth5",
            imgid: "niti02a",
            imgsrc: ""
          },
          {
            imgclass: "sahil-shls7",
            imgid: "man-talking1",
            imgsrc: ""
          },
          {
            imgclass: "sahil-shls7 manhide",
            imgid: "man-talking",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        speechbox: "sp-1-sps9-txtSz3hlf",
        imgclass: "",
        imgid: "textbox_3",
        imgsrc: "",
        textclass: "textInSp",
        textdata: data.string.p1s9_txt
      }
    ]
  },
  // slide 10
  {
    contentblockadditionalclass: "thebg1",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "background",
            imgid: "gaijatra",
            imgsrc: ""
          },
          {
            imgclass: "asha-inverted",
            imgid: "asha01",
            imgsrc: ""
          },
          {
            imgclass: "niti-wdth5",
            imgid: "niti02a",
            imgsrc: ""
          },
          {
            imgclass: "sahil-shls7",
            imgid: "man-talking1",
            imgsrc: ""
          },
          {
            imgclass: "sahil-shls7 manhide",
            imgid: "man-talking",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        speechbox: "sp-1-sps9-txtSz3hlf",
        imgclass: "",
        imgid: "textbox_3",
        imgsrc: "",
        textclass: "textInSp",
        textdata: data.string.p1s10_txt
      }
    ]
  },
  // slide 11
  {
    contentblockadditionalclass: "thebg1",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "background",
            imgid: "gaijatra",
            imgsrc: ""
          },
          {
            imgclass: "asha-inverted",
            imgid: "asha01",
            imgsrc: ""
          },
          {
            imgclass: "niti",
            imgid: "niti01a",
            imgsrc: ""
          },
          {
            imgclass: "sahil",
            imgid: "saahil_01",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        speechbox: "sp-1-sps6-txtSz3",
        imgclass: "",
        imgid: "textbox_1",
        imgsrc: "",
        textclass: "textInSp",
        textdata: data.string.p1s11_txt
      }
    ]
  }
];

$(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page, countNext + 1);

  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();

  var preload;
  var timeoutvar = null;
  var current_sound;

  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      // {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
      // {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
      //   ,
      //images
      {
        id: "cover_page_pg0",
        src: imgpath + "cover_page.jpg",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "cover_page",
        src: imgpath + "bg_village01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "foreshadowing",
        src: imgpath + "foreshadowing.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "funny_gaijatra",
        src: imgpath + "funny_gaijatra.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "gaijatra",
        src: imgpath + "gaijatra.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "upaya_alking",
        src: imgpath + "upaya_alking.gif",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "man-talking",
        src: imgpath + "man-talking.gif",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "man-talking1",
        src: imgpath + "man_talking1.png",
        type: createjs.AbstractLoader.IMAGE
      },

      {
        id: "asha01",
        src: imgpath + "asha01a.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "asha02",
        src: imgpath + "asha02a.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "asha03",
        src: imgpath + "asha03a.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "niti01a",
        src: imgpath + "niti01a.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "niti02a",
        src: imgpath + "niti02a.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "niti03a",
        src: imgpath + "niti03a.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "niti04a",
        src: imgpath + "niti04a.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "saahil_01",
        src: imgpath + "saahil_01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      //textboxes
      {
        id: "textbox_1",
        src: imgpath + "text_box01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "textbox_2",
        src: imgpath + "dialogue-box-2.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "textbox_3",
        src: imgpath + "dialogue-box-3.png",
        type: createjs.AbstractLoader.IMAGE
      },
      // sounds
      { id: "s1_p1", src: soundAsset + "s1_p1.ogg" },
      { id: "s1_p2", src: soundAsset + "s1_p2.ogg" },
      { id: "s1_p3", src: soundAsset + "s1_p3.ogg" },
      { id: "s1_p4", src: soundAsset + "s1_p4.ogg" },
      { id: "s1_p5", src: soundAsset + "s1_p5.ogg" },
      { id: "s1_p6", src: soundAsset + "s1_p6.ogg" },
      { id: "s1_p7", src: soundAsset + "s1_p7.ogg" },
      { id: "s1_p8", src: soundAsset + "s1_p8.ogg" },
      { id: "s1_p9", src: soundAsset + "s1_p9.ogg" },
      { id: "s1_p10", src: soundAsset + "s1_p10.ogg" },
      { id: "s1_p11", src: soundAsset + "s1_p11.ogg" }
    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound); //for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }
  function handleFileLoad(event) {
    // console.log(event.item);
  }
  function handleProgress(event) {
    $("#loading-text").html(parseInt(event.loaded * 100) + "%");
  }
  function handleComplete(event) {
    $("#loading-wrapper").hide(0);
    //initialize varibales
    current_sound = createjs.Sound.play("sound_1");
    current_sound.stop();
    // call main function
    templateCaller();
  }
  //initialize
  init();

  /*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  /*===============================================
	=            data highlight function            =
	===============================================*/
  function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object"
      ? alert(
          "Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted"
        )
      : null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";
    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
        $(this).attr(
          "data-highlightcustomclass"
        ) /*if there is data-highlightcustomclass defined it is true else it is not*/
          ? (stylerulename = $(this).attr("data-highlightcustomclass"))
          : (stylerulename = "parsedstring");

        texthighlightstarttag = "<span class='" + stylerulename + "'>";
        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
        $(this).html(replaceinstring);
      });
    }
  }
  /*=====  End of data highlight function  ======*/

  /*===============================================
	 =            user notification function        =
	 ===============================================*/
  function notifyuser($notifyinside) {
    //check if $notifyinside is provided
    typeof $notifyinside !== "object"
      ? alert(
          "Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style."
        )
      : null;

    /*variable that will store the element(s) to remove notification from*/
    var $allnotifications = $notifyinside.find(
      "*[data-usernotification='notifyuser']"
    );
    // if there are any notifications removal required add the event handler
    if ($allnotifications.length > 0) {
      $allnotifications.one("click", function() {
        /* Act on the event */
        $(this).attr("data-isclicked", "clicked");
        $(this).removeAttr("data-usernotification");
      });
    }
  }

  /*=====  End of user notification function  ======*/

  /*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

  function navigationcontroller(islastpageflag) {
    typeof islastpageflag === "undefined"
      ? (islastpageflag = false)
      : typeof islastpageflag != "boolean"
      ? alert(
          "NavigationController : Hi Master, please provide a boolean parameter"
        )
      : null;

    if (countNext == 0 && $total_page != 1) {
      // $nextBtn.show(0);
      $prevBtn.css("display", "none");
    } else if ($total_page == 1) {
      $prevBtn.css("display", "none");
      $nextBtn.css("display", "none");

      // if lastpageflag is true
      islastpageflag
        ? ole.footerNotificationHandler.lessonEndSetNotification()
        : ole.footerNotificationHandler.lessonEndSetNotification();
    } else if (countNext > 0 && countNext < $total_page - 1) {
      $nextBtn.show(0);
      $prevBtn.show(0);
    } else if (countNext == $total_page - 1) {
      $nextBtn.css("display", "none");
      $prevBtn.show(0);

      // if lastpageflag is true
      islastpageflag
        ? ole.footerNotificationHandler.lessonEndSetNotification()
        : ole.footerNotificationHandler.pageEndSetNotification();
    }
  }

  /*=====  End of user navigation controller function  ======*/

  /*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

  function instructionblockcontroller() {
    var $instructionblock = $board.find("div.instructionblock");
    if ($instructionblock.length > 0) {
      var $contentblock = $board.find("div.contentblock");
      var $toggleinstructionblockbutton = $instructionblock.find(
        "div.toggleinstructionblock"
      );
      var instructionblockisvisibleflag;

      $contentblock.css("pointer-events", "none");

      $toggleinstructionblockbutton.on("click", function() {
        instructionblockisvisibleflag = $instructionblock.attr(
          "data-instructionblockshow"
        );
        if (instructionblockisvisibleflag == "true") {
          instructionblockisvisibleflag = "false";
          $contentblock.css("pointer-events", "auto");
        } else if (instructionblockisvisibleflag == "false") {
          instructionblockisvisibleflag = "true";
          $contentblock.css("pointer-events", "none");
        }

        $instructionblock.attr(
          "data-instructionblockshow",
          instructionblockisvisibleflag
        );
      });
    }
  }

  /*=====  End of InstructionBlockController  ======*/

  /*=================================================
	 =            general template function            =
	 =================================================*/
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    vocabcontroller.findwords(countNext);
    put_image(content, countNext);
    put_speechbox_image(content, countNext);
    function spchBoxAfterTxt(soundId, spBoxClass) {
      createjs.Sound.stop();
      current_sound = createjs.Sound.play(soundId);
      current_sound.play();
      current_sound.on("complete", function() {
        $(spBoxClass).addClass("fadeInFast");
        navigationcontroller();
      });
    }
    switch (countNext) {
      case 0:
        sound_player("s1_p" + (countNext + 1), 1);
        break;
      default:
        sound_player("s1_p" + (countNext + 1), 1);
    }
  }

  function nav_button_controls(delay_ms) {
    timeoutvar = setTimeout(function() {
      if (countNext == 0) {
        $nextBtn.show(0);
      } else if (countNext > 0 && countNext == $total_page - 1) {
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else {
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    }, delay_ms);
  }
  function sound_player(sound_id, next) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on("complete", function() {
      next ? nav_button_controls(100) : "";
      $(".manhide").hide();
    });
  }

  function sound_player_duo(sound_id, sound_id_2) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    //current_sound_2 = createjs.Sound.play(sound_id_2);
    current_sound.play();
    current_sound.on("complete", function() {
      $(".dotext").show(0);
      sound_player(sound_id_2);
    });
  }

  function put_image(content, count) {
    if (content[count].hasOwnProperty("imageblock")) {
      var imageblock = content[count].imageblock[0];
      if (imageblock.hasOwnProperty("imagestoshow")) {
        var imageClass = imageblock.imagestoshow;
        for (var i = 0; i < imageClass.length; i++) {
          var image_src = preload.getResult(imageClass[i].imgid).src;
          //get list of classes
          var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
          var selector = "." + classes_list[classes_list.length - 1];
          $(selector).attr("src", image_src);
        }
      }
    }
  }

  function put_speechbox_image(content, count) {
    if (content[count].hasOwnProperty("speechbox")) {
      var speechbox = content[count].speechbox;
      for (var i = 0; i < speechbox.length; i++) {
        var image_src = preload.getResult(speechbox[i].imgid).src;
        //get list of classes
        var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
        var selector =
          "." + classes_list[classes_list.length - 1] + ">.speechbg";
        // console.log(selector);
        $(selector).attr("src", image_src);
      }
    }
  }
  function templateCaller() {
    $prevBtn.css("display", "none");
    $nextBtn.css("display", "none");

    if (countNext == 0) navigationcontroller();

    generaltemplate();
    loadTimelineProgress($total_page, countNext + 1);
  }

  $nextBtn.on("click", function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    switch (countNext) {
      default:
        countNext++;
        templateCaller();
        break;
    }
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    countNext--;
    templateCaller();
    /* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
    countNext < $total_page
      ? ole.footerNotificationHandler.hideNotification()
      : null;
  });
});

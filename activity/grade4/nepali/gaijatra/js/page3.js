var imgpath = $ref+"/images/diyimg/";
var soundAsset = $ref+"/sounds/orgSnds/";

var content=[
	// slide 1
	{
		contentblockadditionalclass : "grnBg",
		exetype1:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"qn_top",
			qndata:data.string.p3s2_txt
		}],
		imageblock:[{
			imgindiv:true,
			imgindivclass:"twoImgsCont",
			imagestoshow:[{
				imgtoshowindiv:true,
				imgclass:"flexImages flxImg1",
				imgid:"q1op2",
				imgsrc:'',
				imgopn:true,
				exeoptions:[{
					optaddclass:"class1",
					optdata:data.string.q1_op1
				}]
			},{
				imgtoshowindiv:true,
				imgclass:"flexImages flxImg2",
				imgid:"q1op1",
				imgsrc:'',
				imgopn:true,
				exeoptions:[{
					optdata:data.string.q1_op2
				}]
			}]
		}]
},
	// slide 2
	{
		contentblockadditionalclass : "grnBg",
		exetype1:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"qn_top",
			qndata:data.string.p3s2_txt
		}],
		imageblock:[{
			imgindiv:true,
			imgindivclass:"twoImgsCont",
			imagestoshow:[{
				imgtoshowindiv:true,
				imgclass:"flexImages flxImg1",
				imgid:"kite",
				imgsrc:'',
				imgopn:true,
				exeoptions:[{
					optaddclass:"class1",
					optdata:data.string.q2_op1
				}]
			},{
				imgtoshowindiv:true,
				imgclass:"flexImages flxImg2",
				imgid:"cry",
				imgsrc:'',
				imgopn:true,
				exeoptions:[{
					optdata:data.string.q2_op2
				}]
			}]
		}]
	},
	// slide 3
	{
		contentblockadditionalclass : "grnBg",
		exetype1:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"qn_top",
			qndata:data.string.p3s2_txt
		}],
		imageblock:[{
			imgindiv:true,
			imgindivclass:"twoImgsCont",
			imagestoshow:[{
				imgtoshowindiv:true,
				imgclass:"flexImages flxImg1",
				imgid:"play_kite",
				imgsrc:'',
				imgopn:true,
				exeoptions:[{
					optaddclass:"class1",
					optdata:data.string.q3_op1
				}]
			},{
				imgtoshowindiv:true,
				imgclass:"flexImages flxImg2",
				imgid:"jungle",
				imgsrc:'',
				imgopn:true,
				exeoptions:[{
					optdata:data.string.q3_op2
				}]
			}]
		}]
	},
	// slide 4
	{
		contentblockadditionalclass : "blueBg",
		extratextblock:[{
			textclass:"topStatement",
			textdata:data.string.p3s4_txt
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"background",
				imgid:"dashain_tika",
				imgsrc:'',
			}]
		}]
	},
	// slide 5
	{
		contentblockadditionalclass : "blueBg",
		extratextblock:[{
			textclass:"topStatement",
			textdata:data.string.p3s5_txt
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"background",
				imgid:"eat-apple",
				imgsrc:'',
			}]
		}]
	},
	// slide 6
	{
		contentblockadditionalclass : "blueBg",
		extratextblock:[{
			textclass:"bottomStatement",
			textdata:data.string.p3s6_txt
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"background",
				imgid:"play_ground",
				imgsrc:'',
			},{
				imgclass:"dilak",
				imgid:"dilak",
				imgsrc:'',
			},{
				imgclass:"playing_dog",
				imgid:"playing_dog",
				imgsrc:'',
			},{
				imgclass:"girlInCycle",
				imgid:"girl-in-cycle",
				imgsrc:'',
			},{
				imgclass:"grlSkipping",
				imgid:"girl_skipping",
				imgsrc:'',
			},{
				imgclass:"grlSwing",
				imgid:"girls_swinging",
				imgsrc:'',
			}]
		}]
	},
	// slide 7
	{
		contentblockadditionalclass : "blueBg",
		extratextblock:[{
			textclass:"topStatement",
			textdata:data.string.p3s7_txt
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"background",
				imgid:"mero_ghar_aagan",
				imgsrc:'',
			},{
				imgclass:"wastage",
				imgid:"wastage",
				imgsrc:'',
			},{
				imgtoshowindiv:true,
				imgstoshowdivclass:"spriteCont",
				imgclass:"sweeping",
				imgid:"sweeping",
				imgsrc:'',
			}]
		}]
	},
];

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var item_txt ='';
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "q1", src: imgpath+"water_pot.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q1op1", src: imgpath+"reading.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q1op2", src: imgpath+"tika.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cry", src: imgpath+"cry.png", type: createjs.AbstractLoader.IMAGE},
			{id: "kite", src: imgpath+"kite.png", type: createjs.AbstractLoader.IMAGE},
			{id: "jungle", src: imgpath+"jungle.png", type: createjs.AbstractLoader.IMAGE},
			{id: "play_kite", src: imgpath+"play_kite.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dashain_tika", src: imgpath+"dashain_tika.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eat-apple", src: imgpath+"eat-apple.png", type: createjs.AbstractLoader.IMAGE},
			{id: "play_ground", src: imgpath+"play_ground.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mero_ghar_aagan", src: imgpath+"mero_ghar_aagan.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sweeping", src: imgpath+"sweeping.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wastage", src: imgpath+"wastage.png", type: createjs.AbstractLoader.IMAGE},

			{id: "dilak", src: imgpath+"dilak.png", type: createjs.AbstractLoader.IMAGE},
			{id: "playing_dog", src: imgpath+"playing_dog.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-in-cycle", src: imgpath+"girl-in-cycle.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "girl_skipping", src: imgpath+"girl_skipping.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "girls_swinging", src: imgpath+"girls_swinging.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "q1op3", src: imgpath+"bucket.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spchbx", src: imgpath+"speechbubble-02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "watering_plant_gif", src: imgpath+"watering_plant.gif", type: createjs.AbstractLoader.IMAGE},
			// {id: "bg_village", src: imgpath1+"bg_village.png", type: createjs.AbstractLoader.IMAGE},


			// sounds
			{id: "s3_p1", src: soundAsset+"s3_p1.ogg"},
			{id: "s3_p4", src: soundAsset+"s3_p4.ogg"},
			{id: "s3_p5", src: soundAsset+"s3_p5.ogg"},
			{id: "s3_p6", src: soundAsset+"s3_p6.ogg"},
			{id: "s3_p7", src: soundAsset+"s3_p7.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		//scoring.init(10);
		templateCaller();
	}
	//initialize
	init();

	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
 	}

	 /*values in this array is same as the name of images of eggs in image folder*/

	 //create eggs
	//  var testin = new NumberTemplate();

	 	//eggTemplate.eggMove(countNext);
 	// 	testin.init(10);


	/*for randomizing the options*/
	function randomize(parent){
		var parent = $(parent);
		var divs = parent.children();
		while (divs.length) {
		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
	}
	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		content[countNext].imgload?put_image_sec(content, countNext):"";
	 	var corcount = 0;

	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);

		// 	testin.numberOfQuestions();
		//
		// 	var ansClicked = false;
		// 	var wrngClicked = false;

	  function rand_generator(limit){
	    var randNum = Math.floor(Math.random() * (limit - 1 +1)) + 1;
	    return randNum;
	  }
		// $(".qn_top").prepend(countNext+1+". ");
		switch (countNext) {
			case 0:
                sound_player("s3_p1",0);
                 break;
			case 6:
			sound_player("s3_p"+(countNext+1),0);
				$(".sweeping").click(function(){
					$(".sweeping").addClass("swepAnim");
					setTimeout(function(){
						$(".wastage").hide(0);
					},1500);
					nav_button_controls(2200);
				});
			break;
			default:
			sound_player("s3_p"+(countNext+1),1);

		}
		 	randomize(".twoImgsCont");

		$(".buttonsel").click(function(){
			createjs.Sound.stop();
			if($(this).hasClass("class1")){
				play_correct_incorrect_sound(1);
				$(this).css({
					"pointer-events":"none",
					"background":"#8eba2d",
					"border":"5px solid #ff0"
				});
				$(".buttonsel").css("pointer-events",'none');
				$(this).siblings(".corctopt").show();
				// !wrngClicked?testin.update(true):testin.update(false);
				nav_button_controls(100);
			}else{
				play_correct_incorrect_sound(0);
				$(this).css({
					"pointer-events":"none",
					"background":"#f00",
					"border":"5px solid #b81021"
				});
				$(this).siblings(".wrngopt").show();
				// wrngClicked = true;
			}
		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls(300):'';
		});
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					// console.log(selector);
					$(selector).attr('src',image_src);
				}
			}
		}
	}

	function put_image_sec(content, count){
		if(content[count].hasOwnProperty('imgexc')){
			for(var j=0;j<content[count].imgexc.length;j++){
				var imageblock = content[count].imgexc[j];
				if(imageblock.hasOwnProperty('imgexcoptions')){
					var imageClass = imageblock.imgexcoptions;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						// console.log(selector);
						$(selector).attr('src',image_src);
					}
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	 	/*======= SCOREBOARD SECTION ==============*/
	//  }

	 function templateCaller(){
		/*always hide next and previousloadimage navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
		loadTimelineProgress($total_page, countNext + 1);
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators


	}

	// first call to template caller
	// templateCaller(); loadimage($(".myimg3").find("img"),$(".myimg3").find("p"),preload.getResult('ex1typ3qn'+quesNo).src,eval('data.string.ex2typ1op'+quesNo));


	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		createjs.Sound.stop();
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

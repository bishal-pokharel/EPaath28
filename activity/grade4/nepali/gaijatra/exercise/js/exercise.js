var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/orgSnds/";

var content=[
	// slide 1--> 2opn type
	{
		contentblockadditionalclass : "blueBg",
		exetype1:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_q1_toptxt
		},{
			qntextdivclass:"topInstrn-tpInstrnSec",
			qntextclass:"qn_top",
			qndata:data.string.exc_q1_toptxt_sec
		}],
		imageblock:[{
			imgindiv:true,
			imgindivclass:"twoImgsCont",
			imagestoshow:[{
				imgtoshowindiv:true,
				imgclass:"flexImages flxImg1",
				imgid:"q1op2",
				imgsrc:'',
				imgopn:true,
				exeoptions:[{
					optaddclass:"class1",
					optdata:data.string.exc_q1_op1
				}]
			},{
				imgtoshowindiv:true,
				imgclass:"flexImages flxImg2",
				imgid:"q1op1",
				imgsrc:'',
				imgopn:true,
				exeoptions:[{
					optdata:data.string.exc_q1_op2
				}]
			}]
		}]
	},
	// slide 2--> singleopn type
	{
		contentblockadditionalclass : "blueBg",
		exetype1:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_q1_toptxt
		},{
			qntextdivclass:"secTyp_SecInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_q2
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv optionsdivSecType",
			exeoptions:[{
				optcontainerextra:"opnContLong",
				optdata:data.string.exc_q2_op1
			},{
				optaddclass:"class1",
				optcontainerextra:"opnContLong",
				optdata:data.string.exc_q2_op2
			}]
		}],
		imageblock:[{
			imgindiv:true,
			imgindivclass:"singleImgCont",
			imagestoshow:[{
				imgclass:"singleImg",
				imgid:"dance",
				imgsrc:'',
			}]
		}]
	},
	// slide 3--> 2opn type
	{
		contentblockadditionalclass : "blueBg",
		exetype1:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_q1_toptxt
		},{
			qntextdivclass:"topInstrn-tpInstrnSec",
			qntextclass:"qn_top",
			qndata:data.string.exc_q3
		}],
		imageblock:[{
			imgindiv:true,
			imgindivclass:"twoImgsCont",
			imagestoshow:[{
				imgtoshowindiv:true,
				imgclass:"flexImages flxImg1",
				imgid:"lakha_dance",
				imgsrc:'',
				imgopn:true,
				exeoptions:[{
					optaddclass:"class1",
					optdata:data.string.exc_q3_op1
				}]
			},{
				imgtoshowindiv:true,
				imgclass:"flexImages flxImg2",
				imgid:"taking_veg",
				imgsrc:'',
				imgopn:true,
				exeoptions:[{
					optdata:data.string.exc_q3_op2
				}]
			}]
		}]
	},
	// slide 4--> singleopn type
	{
		contentblockadditionalclass : "blueBg",
		exetype1:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_q1_toptxt
		},{
			qntextdivclass:"secTyp_SecInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_q4
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv optionsdivSecType",
			exeoptions:[{
				optcontainerextra:"opnContLong",
				optaddclass:"class1",
				optdata:data.string.exc_q4_op1
			},{
				optcontainerextra:"opnContLong",
				optdata:data.string.exc_q4_op2
			}]
		}],
		imageblock:[{
			imgindiv:true,
			imgindivclass:"singleImgCont",
			imagestoshow:[{
				imgclass:"singleImg",
				imgid:"eat-apple",
				imgsrc:'',
			}]
		}]
	},
	// slide 5--> 2opn type---left to do
	{
		contentblockadditionalclass : "blueBg",
		exetype1:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_q1_toptxt
		},{
			qntextdivclass:"topInstrn-tpInstrnSec",
			qntextclass:"qn_top",
			qndata:data.string.exc_q5
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv t30",
			exeoptions:[{
				// optcontainerextra:"opnContLong",
				optaddclass:"class1",
				optdata:data.string.exc_q5_op1
			},{
				// optcontainerextra:"opnContLong",
				optdata:data.string.exc_q5_op2
			}]
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"background",
				imgid:"zebra_crossing",
				imgsrc:'',
			}]
		}]
	},
	// slide 6--> 2opn type
	{
		contentblockadditionalclass : "blueBg",
		exetype1:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_q1_toptxt
		},{
			qntextdivclass:"topInstrn-tpInstrnSec",
			qntextclass:"qn_top",
			qndata:data.string.exc_q6
		}],
		imageblock:[{
			imgindiv:true,
			imgindivclass:"twoImgsCont",
			imagestoshow:[{
				imgtoshowindiv:true,
				imgclass:"flexImages flxImg1",
				imgid:"singing",
				imgsrc:'',
				imgopn:true,
				exeoptions:[{
					optaddclass:"class1",
					optdata:data.string.exc_q6_op1
				}]
			},{
				imgtoshowindiv:true,
				imgclass:"flexImages flxImg2",
				imgid:"eid",
				imgsrc:'',
				imgopn:true,
				exeoptions:[{
					optdata:data.string.exc_q6_op2
				}]
			}]
		}]
	},
	// slide 7--> 2opn type
	{
		contentblockadditionalclass : "blueBg",
		exetype1:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_q1_toptxt
		},{
			qntextdivclass:"topInstrn-tpInstrnSec",
			qntextclass:"qn_top",
			qndata:data.string.exc_q7
		}],
		imageblock:[{
			imgindiv:true,
			imgindivclass:"twoImgsCont",
			imagestoshow:[{
				imgtoshowindiv:true,
				imgclass:"flexImages flxImg1",
				imgid:"hospital",
				imgsrc:'',
				imgopn:true,
				exeoptions:[{
					optaddclass:"class1",
					optdata:data.string.exc_q7_op1
				}]
			},{
				imgtoshowindiv:true,
				imgclass:"flexImages flxImg2",
				imgid:"eating",
				imgsrc:'',
				imgopn:true,
				exeoptions:[{
					optdata:data.string.exc_q7_op2
				}]
			}]
		}]
	},
	// slide 8--> singleopn type
	{
		contentblockadditionalclass : "blueBg",
		exetype1:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_q1_toptxt
		},{
			qntextdivclass:"secTyp_SecInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_q8
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv optionsdivSecType",
			exeoptions:[{
				optcontainerextra:"opnContLong",
				optaddclass:"class1",
				optdata:data.string.exc_q8_op2
			},{
				optcontainerextra:"opnContLong",
				optdata:data.string.exc_q8_op1
			}]
		}],
		imageblock:[{
			imgindiv:true,
			imgindivclass:"singleImgCont",
			imagestoshow:[{
				imgclass:"singleImg",
				imgid:"house",
				imgsrc:'',
			}]
		}]
	},
	// slide 9--> singleopn type
	{
		contentblockadditionalclass : "blueBg",
		exetype1:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_q1_toptxt
		},{
			qntextdivclass:"secTyp_SecInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_q9
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv optionsdivSecType",
			exeoptions:[{
				optcontainerextra:"opnContLong",
				optaddclass:"class1",
				optdata:data.string.exc_q9_op1
			},{
				optcontainerextra:"opnContLong",
				optdata:data.string.exc_q9_op2
			}]
		}],
		imageblock:[{
			imgindiv:true,
			imgindivclass:"singleImgCont",
			imagestoshow:[{
				imgclass:"singleImg",
				imgid:"selling_fruits",
				imgsrc:'',
			}]
		}]
	},
	// slide 10--> singleopn type
	{
		contentblockadditionalclass : "blueBg",
		exetype1:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_q1_toptxt
		},{
			qntextdivclass:"secTyp_SecInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_q10
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv optionsdivSecType",
			exeoptions:[{
				optcontainerextra:"opnContLong",
				optaddclass:"class1",
				optdata:data.string.exc_q10_op2
			},{
				optcontainerextra:"opnContLong",
				optdata:data.string.exc_q10_op1
			}]
		}],
		imageblock:[{
			imgindiv:true,
			imgindivclass:"singleImgCont",
			imagestoshow:[{
				imgclass:"singleImg",
				imgid:"nest",
				imgsrc:'',
			}]
		}]
	},
];

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var item_txt ='';
	var countNext = 0;

	/*for limiting the questions to 10*/
	var $total_page = 10;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "q1op1", src: imgpath+"dashain_fest.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q1op2", src: imgpath+"picnic.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q1op3", src: imgpath+"bucket.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dance", src: imgpath+"dance.png", type: createjs.AbstractLoader.IMAGE},
			{id: "lakha_dance", src: imgpath+"lakha_dance.png", type: createjs.AbstractLoader.IMAGE},
			{id: "taking_veg", src: imgpath+"taking_veg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eat-apple", src: imgpath+"eat-apple.png", type: createjs.AbstractLoader.IMAGE},
			{id: "singing", src: imgpath+"singing.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eid", src: imgpath+"eid.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hospital", src: imgpath+"hospital.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eating", src: imgpath+"eating.png", type: createjs.AbstractLoader.IMAGE},
			{id: "house", src: imgpath+"house.png", type: createjs.AbstractLoader.IMAGE},
			{id: "selling_fruits", src: imgpath+"selling_fruits.png", type: createjs.AbstractLoader.IMAGE},
			{id: "nest", src: imgpath+"nest.png", type: createjs.AbstractLoader.IMAGE},
			{id: "zebra_crossing", src: imgpath+"zebra_crossing.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "ex_instr", src: soundAsset+"ex_instr.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		//scoring.init(10);
		templateCaller();
	}
	//initialize
	init();

	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
 	}

	 /*values in this array is same as the name of images of eggs in image folder*/

	 //create eggs
	 var testin = new NumberTemplate();

	 	//eggTemplate.eggMove(countNext);
 		testin.init(10);

	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		content[countNext].imgload?put_image_sec(content, countNext):"";
	 	var corcount = 0;

	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);

	 	testin.numberOfQuestions();

	 	var ansClicked = false;
	 	var wrngClicked = false;

	  function rand_generator(limit){
	    var randNum = Math.floor(Math.random() * (limit - 1 +1)) + 1;
	    return randNum;
	  }
		// $(".qn_top").prepend(countNext+1+". ");

	 	/*for randomizing the options*/
		function randomize(parent){
			var parent = $(parent);
			var divs = parent.children();
			while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}
		}

		countNext==0?sound_player("ex_instr"):'';
	 	randomize(".optionsdiv");
	 	randomize(".twoImgsCont");

		$(".buttonsel").click(function(){
			if($(this).hasClass("class1")){
				play_correct_incorrect_sound(1);
				$(this).css({
					"pointer-events":"none",
					"background":"#8eba2d",
					"border":"5px solid #ff0"
				});
				$(".buttonsel").css("pointer-events",'none');
				$(this).siblings(".corctopt").show();
				!wrngClicked?testin.update(true):testin.update(false);
				nav_button_controls(100);
			}else{
				play_correct_incorrect_sound(0);
				$(this).css({
					"pointer-events":"none",
					"background":"#f00",
					"border":"5px solid #b81021"
				});
				$(this).siblings(".wrngopt").show();
				wrngClicked = true;
			}
		});
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls(300):'';
		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				$nextBtn.show(0);
				// ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					// console.log(selector);
					$(selector).attr('src',image_src);
				}
			}
		}
	}

	function put_image_sec(content, count){
		if(content[count].hasOwnProperty('imgexc')){
			for(var j=0;j<content[count].imgexc.length;j++){
				var imageblock = content[count].imgexc[j];
				if(imageblock.hasOwnProperty('imgexcoptions')){
					var imageClass = imageblock.imgexcoptions;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						// console.log(selector);
						$(selector).attr('src',image_src);
					}
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	 	/*======= SCOREBOARD SECTION ==============*/
	//  }

	 function templateCaller(){
		/*always hide next and previousloadimage navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators


	}

	// first call to template caller
	// templateCaller(); loadimage($(".myimg3").find("img"),$(".myimg3").find("p"),preload.getResult('ex1typ3qn'+quesNo).src,eval('data.string.ex2typ1op'+quesNo));


	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

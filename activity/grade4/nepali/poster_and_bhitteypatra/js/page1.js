var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

  var content = [
    // slide0
    {
      contentnocenteradjust: true,
      uppertextblock: [
        {
          textclass: "centertext",
          textdata: data.lesson.chapter
        }
      ],
      imageblock: [{
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: 'page01',
            imgsrc: ""
          },
        ]
      }]
    },

    // slide1
    {
      contentnocenteradjust: true,
      imageblock: [{
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: 'page01',
            imgsrc: ""
          },
        ]
      }],
      speechbox:[{
        textclass:'insidetext',
        textdata:data.string.p1text1,
        speechbox:'speech_1',
        imgid:'textbox'
      }]

    },


    // slide2
    {
      contentnocenteradjust: true,
      imageblock: [{
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: 'page02',
            imgsrc: ""
          },
        ]
      }],
      speechbox:[{
        textclass:'insidetext_type2',
        textdata:data.string.p1text2,
        speechbox:'speech_2',
        imgid:'textbox_two'
      }]

    },

    // slide3
    {
      contentnocenteradjust: true,
      imageblock: [{
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: 'page03',
            imgsrc: ""
          },
        ]
      }],
      speechbox:[{
        textclass:'insidetext_type2',
        textdata:data.string.p1text3,
        speechbox:'speech_3',
        imgid:'textbox_three'
      }]

    },

    // slide4
    {
      contentnocenteradjust: true,
      imageblock: [{
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: 'page04',
            imgsrc: ""
          },
          {
            imgclass: "patro_right",
            imgid: 'bhatro_patro',
            imgsrc: ""
          },
        ]
      }],
      speechbox:[{
        textclass:'insidetext',
        textdata:data.string.p1text4,
        speechbox:'speech_4',
        imgid:'textbox'
      }]

    },

    // slide5
    {
      contentnocenteradjust: true,
      imageblock: [{
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: 'page05',
            imgsrc: ""
          }
        ]
      }],
      speechbox:[{
        textclass:'insidetext_type2',
        textdata:data.string.p1text5,
        speechbox:'speech_5',
        imgid:'textbox_two'
      }]

    },


    // slide6
    {
      contentnocenteradjust: true,
      imageblock: [{
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: 'page01',
            imgsrc: ""
          },
          {
            imgclass: "patro_right_2",
            imgid: 'bhatro_patro',
            imgsrc: ""
          },
        ]
      }],
      speechbox:[{
        textclass:'insidetext',
        textdata:data.string.p1text6,
        speechbox:'speech_1',
        imgid:'textbox'
      }]

    },


    // slide7
    {
      contentnocenteradjust: true,
      imageblock: [{
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: 'page06',
            imgsrc: ""
          }
        ]
      }],
      speechbox:[{
        textclass:'insidetext_type2',
        textdata:data.string.p1text7,
        speechbox:'speech_6',
        imgid:'textbox_two'
      }]

    },
    // slide8
    {
      contentnocenteradjust: true,
      imageblock: [{
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: 'page01',
            imgsrc: ""
          },
          {
            imgclass: "patro_right_2",
            imgid: 'bhatro_patro',
            imgsrc: ""
          },
        ]
      }],
      speechbox:[{
        textclass:'insidetext',
        textdata:data.string.p1text8,
        speechbox:'speech_7',
        imgid:'textbox'
      }]

    },

    // slide9
    {
      contentnocenteradjust: true,
      imageblock: [{
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: 'page08',
            imgsrc: ""
          },
          {
            imgclass: "patro_right_3",
            imgid: 'bhatro_patro',
            imgsrc: ""
          },
          {
            imgclass: "sagar",
            imgid: 'sagar',
            imgsrc: ""
          },
        ]
      }],
      speechbox:[{
        textclass:'insidetext_type2',
        textdata:data.string.p1text9,
        speechbox:'speech_8',
        imgid:'textbox_two'
      }]

    },

    // slide10
    {
      contentnocenteradjust: true,
      imageblock: [{
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: 'page07',
            imgsrc: ""
          },
          {
            imgclass: "patro_right_4",
            imgid: 'bhatro_patro',
            imgsrc: ""
          }
        ]
      }],
      speechbox:[{
        textclass:'insidetext',
        textdata:data.string.p1text10,
        speechbox:'speech_9',
        imgid:'textbox'
      }]

    },



        // slide11
        {
          contentnocenteradjust: true,
          contentblockadditionalclass: 'yellowish_bg',
          imageblock: [{
            imagestoshow: [
              {
                imgclass: "patro_center",
                imgid: 'bhatro_patro',
                imgsrc: ""
              }
            ]
          }]

        },
  ];

  $(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
      //specify type otherwise it will load assests as XHR
      manifest = [
        {id: "page01", src: imgpath+"page01.png", type: createjs.AbstractLoader.IMAGE},
        {id: "page02", src: imgpath+"page02.png", type: createjs.AbstractLoader.IMAGE},
        {id: "page03", src: imgpath+"page03.png", type: createjs.AbstractLoader.IMAGE},
        {id: "page04", src: imgpath+"page04.png", type: createjs.AbstractLoader.IMAGE},
        {id: "page05", src: imgpath+"page05.png", type: createjs.AbstractLoader.IMAGE},
        {id: "page06", src: imgpath+"page06.png", type: createjs.AbstractLoader.IMAGE},
        {id: "page07", src: imgpath+"page07.png", type: createjs.AbstractLoader.IMAGE},
        {id: "page08", src: imgpath+"page08.png", type: createjs.AbstractLoader.IMAGE},
        {id: "bhatro_patro", src: imgpath+"bhatro_patro.png", type: createjs.AbstractLoader.IMAGE},
        {id: "sagar", src: imgpath+"sagar.png", type: createjs.AbstractLoader.IMAGE},

        {id: "textbox", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
        {id: "textbox_two", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
        {id: "textbox_three", src: 'images/textbox/white/tl-2.png', type: createjs.AbstractLoader.IMAGE},


        // sounds
        {id: "sound_1", src: soundAsset+"s1_p1.ogg"},
        {id: "sound_2", src: soundAsset+"s1_p2.ogg"},
        {id: "sound_3", src: soundAsset+"s1_p3.ogg"},
        {id: "sound_4", src: soundAsset+"s1_p4.ogg"},
        {id: "sound_5", src: soundAsset+"s1_p5.ogg"},
        {id: "sound_6", src: soundAsset+"s1_p6.ogg"},
        {id: "sound_7", src: soundAsset+"s1_p7.ogg"},
        {id: "sound_8", src: soundAsset+"s1_p8.ogg"},
        {id: "sound_9", src: soundAsset+"s1_p9.ogg"},
        {id: "sound_10", src: soundAsset+"s1_p10.ogg"},
        {id: "sound_11", src: soundAsset+"s1_p11.ogg"},
      ];
      preload = new createjs.LoadQueue(false);
      preload.installPlugin(createjs.Sound);//for registering sounds
      preload.installPlugin(createjs.Sound);//for registering sounds
      preload.on("progress", handleProgress);
      preload.on("complete", handleComplete);
      preload.on("fileload", handleFileLoad);
      preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
      // console.log(event.item);
    }

    function handleProgress(event) {
      $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
      $('#loading-wrapper').hide(0);
      //initialize varibales
      // call main function
      templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
    =            general template function            =
    =================================================*/
    function generaltemplate() {
      var source = $("#general-template").html();
      var template = Handlebars.compile(source);
      var html = template(content[countNext]);
      $board.html(html);
      texthighlight($board);
      vocabcontroller.findwords(countNext);
      put_image(content, countNext,preload);
      put_image1(content, countNext,preload);
      put_speechbox_image(content, countNext, preload);
      $('.speechbox').hide(0).fadeIn(500);
      switch(countNext){
        case 11:
        navigationcontroller(countNext,$total_page);
        break;
        default:
        sound_player('sound_'+(countNext+1));
        break;
      }
    }


    function sound_player(sound_id,navigate) {
      createjs.Sound.stop();
      current_sound = createjs.Sound.play(sound_id);
      current_sound.play();
      current_sound.on('complete', function () {
        navigationcontroller(countNext,$total_page);
      });
    }


    function templateCaller() {
      $prevBtn.css('display', 'none');
      $nextBtn.css('display', 'none');
      generaltemplate();
      loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
      createjs.Sound.stop();
      clearTimeout(timeoutvar);
      switch (countNext) {
        default:
        countNext++;
        templateCaller();
        break;
      }
    });

    $refreshBtn.click(function(){
      templateCaller();
    });

    $prevBtn.on('click', function () {
      createjs.Sound.stop();
      clearTimeout(timeoutvar);
      countNext--;
      templateCaller();
      /* if footerNotificationHandler pageEndSetNotification was called then on click of
      previous slide button hide the footernotification */
      countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });


    function texthighlight($highlightinside){
      //check if $highlightinside is provided
      typeof $highlightinside !== "object" ?
      alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
      null ;

      var $alltextpara = $highlightinside.find("*[data-highlight='true']");
      var stylerulename;
      var replaceinstring;
      var texthighlightstarttag;
      var texthighlightendtag   = "</span>";


      if($alltextpara.length > 0){
        $.each($alltextpara, function(index, val) {
          /*if there is a data-highlightcustomclass attribute defined for the text element
          use that or else use default 'parsedstring'*/
          $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
          (stylerulename = $(this).attr("data-highlightcustomclass")) :
          (stylerulename = "parsedstring") ;

          texthighlightstarttag = "<span class='"+stylerulename+"'>";


          replaceinstring       = $(this).html();
          replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
          replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


          $(this).html(replaceinstring);
        });
      }
    }
  });

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    //slide 0
    {
      contentblockadditionalclass:'background_blue',
      uppertextblock:[{
        textclass:'center_text',
        textdata:data.string.p5text1
      }]
    },

    //slide 1
    {
      contentblockadditionalclass:'background_blue',
      uppertextblock:[{
        textclass:'top_text',
        textdata:data.string.p5text2
      },
      {
        textclass:'top_line',
      }],
      imageblock:[{
        imagestoshow:[{
          imgid:'poster_1',
          imgclass:'mid_img'
        }]
      }]
    },

    //slide 2
    {
      contentblockadditionalclass:'background_blue',
      uppertextblock:[{
        textclass:'top_text',
        textdata:data.string.p5text3
      },
      {
        textclass:'top_line',
      },
      {
        textclass:'text_poster',
        textdata:data.string.p4text19,
      },],
      imageblock:[{
        imagestoshow:[{
          imgid:'poster_2',
          imgclass:'mid_img'
        }]
      }]
    },

    //slide 3
    {
      contentblockadditionalclass:'background_blue',
      uppertextblock:[{
        textclass:'top_text',
        textdata:data.string.p5text4
      },
      {
        textclass:'poster_text_1',
        textdata:data.string.p5text5
      },
      {
        textclass:'poster_text_2',
        textdata:data.string.p5text6
      },
      {
        textclass:'poster_text_3',
        textdata:data.string.p5text7
      },
      {
        textclass:'top_line',
      }],
      imageblock:[{
        imagestoshow:[{
          imgid:'poster_3',
          imgclass:'mid_img'
        }]
      }]
    }

];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
          {id: "poster_1", src: imgpath+"hand_written_poster.jpg", type: createjs.AbstractLoader.IMAGE},
          {id: "poster_2", src: imgpath+"world_env_poster_without_tex.png", type: createjs.AbstractLoader.IMAGE},
          {id: "poster_3", src: imgpath+"making_own_poster03.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_1", src: soundAsset+"s5_p1.ogg"},
            {id: "sound_2", src: soundAsset+"s5_p2.ogg"},
            {id: "sound_3", src: soundAsset+"s5_p3.ogg"},
            {id: "sound_4", src: soundAsset+"s5_p4.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        sound_player('sound_'+(countNext+1));
    }



    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigationcontroller(countNext,$total_page);
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }

});

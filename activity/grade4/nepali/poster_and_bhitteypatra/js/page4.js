var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

  var content = [

    // slide1
    {
      contentnocenteradjust: true,
      imageblock: [{
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: 'page01',
            imgsrc: ""
          },
          {
            imgclass: "patro_right",
            imgid: 'bhatro_patro',
            imgsrc: ""
          },
        ]
      }],
      speechbox:[{
        textclass:'insidetext',
        textdata:data.string.p4text1,
        speechbox:'speech_1',
        imgid:'textbox'
      }]

    },


    // slide2
    {
      contentnocenteradjust: true,
      imageblock: [{
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: 'page02',
            imgsrc: ""
          },
          {
            imgclass: "patro_right",
            imgid: 'bhatro_patro',
            imgsrc: ""
          },
        ]
      }],
      speechbox:[{
        textclass:'insidetext_type2',
        textdata:data.string.p4text2,
        speechbox:'speech_2',
        imgid:'textbox_two'
      }]

    },

    // slide3
    {
      contentnocenteradjust: true,
      imageblock: [{
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: 'page03',
            imgsrc: ""
          },
        ]
      }],
      speechbox:[{
        textclass:'insidetext_type2',
        textdata:data.string.p4text3,
        speechbox:'speech_3',
        imgid:'textbox_three'
      }]

    },

    // slide4
    {
      contentnocenteradjust: true,
      imageblock: [{
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: 'page04',
            imgsrc: ""
          },
          {
            imgclass: "patro_right",
            imgid: 'bhatro_patro',
            imgsrc: ""
          },
        ]
      }],
      speechbox:[{
        textclass:'insidetext',
        textdata:data.string.p4text4,
        speechbox:'speech_4',
        imgid:'textbox'
      }]

    },


    // slide5
    {
      contentnocenteradjust: true,
      imageblock: [{
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: 'mala_holding_poster',
            imgsrc: ""
          },
          {
            imgclass: "patro_right",
            imgid: 'bhatro_patro',
            imgsrc: ""
          },
        ]
      }],
      speechbox:[{
        textclass:'insidetext',
        textdata:data.string.p4text5,
        speechbox:'speech_4',
        imgid:'textbox'
      }],

    },

    // slide6
    {
      contentnocenteradjust: true,
      contentblockadditionalclass:'cream_bg',
      imageblock: [{
        imagestoshow: [
          {
            imgclass: "top_image",
            imgid: 'mala-06',
            imgsrc: ""
          },
          {
            imgclass: "mid_image",
            imgid: 'world_env_poster_without_tex',
            imgsrc: ""
          },
        ]
      }],
      uppertextblock:[{
        textclass:'top_text',
        textdata:data.string.p4text6,
      }]

    },
    // slide7
    {
      contentnocenteradjust: true,
      contentblockadditionalclass:'cream_bg',
      imageblock: [{
        imagestoshow: [
          {
            imgclass: "top_image",
            imgid: 'mala-06',
            imgsrc: ""
          },
          {
            imgclass: "left_image",
            imgid: 'world_env_poster',
            imgsrc: ""
          },
          {
            imgclass: "right_image",
            imgid: 'tourism_day',
            imgsrc: ""
          },
        ]
      }],
      uppertextblock:[{
        textclass:'top_text',
        textdata:data.string.p4text7,
      }]

    },

    // slide8
    {
      contentnocenteradjust: true,
      contentblockadditionalclass:'cream_bg',
      imageblock: [{
        imagestoshow: [
          {
            imgclass: "top_image",
            imgid: 'mala-06',
            imgsrc: ""
          },
          {
            imgclass: "mid_image",
            imgid: 'free_education_with_text',
            imgsrc: ""
          },
        ]
      }],
      uppertextblock:[{
        textclass:'top_text',
        textdata:data.string.p4text8,
      }]

    },
    // slide9
    {
      contentnocenteradjust: true,
      contentblockadditionalclass:'cream_bg',
      imageblock: [{
        imagestoshow: [
          {
            imgclass: "top_image",
            imgid: 'mala-06',
            imgsrc: ""
          },
          {
            imgclass: "mid_image2",
            imgid: 'art_competition',
            imgsrc: ""
          },
        ]
      }],
      uppertextblock:[{
        textclass:'top_text',
        textdata:data.string.p4text9,
      },{
        textclass:'text_click_1 clickable',
        textdata:data.string.p4text10,
          ans:"s9_1"
      },{
        textclass:'text_click_2 clickable',
        textdata:data.string.p4text11,
          ans:"s9_2"
      },{
        textclass:'text_click_3 clickable',
        textdata:data.string.p4text12,
          ans:"s9_3"
      },{
        textclass:'text_click_6 clickable',
        textdata:data.string.p4text15,
          ans:"s9_4"
      },{
        textclass:'text_click_7 clickable',
        textdata:data.string.p4text16,
          ans:"s9_5"
      },{
        textclass:'text_click_8 clickable',
        textdata:data.string.p4text17,
          ans:"s9_6"
      },{
        textclass:'text_click_9 clickable',
        textdata:data.string.p4text18,
          ans:"s9_7"
      }]

    },

  ];

  $(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
      //specify type otherwise it will load assests as XHR
      manifest = [
        {id: "page01", src: imgpath+"page01.png", type: createjs.AbstractLoader.IMAGE},
        {id: "page02", src: imgpath+"page02.png", type: createjs.AbstractLoader.IMAGE},
        {id: "page03", src: imgpath+"page03.png", type: createjs.AbstractLoader.IMAGE},
        {id: "page04", src: imgpath+"page04.png", type: createjs.AbstractLoader.IMAGE},
        {id: "page05", src: imgpath+"page05.png", type: createjs.AbstractLoader.IMAGE},
        {id: "page06", src: imgpath+"page06.png", type: createjs.AbstractLoader.IMAGE},
        {id: "page07", src: imgpath+"page07.png", type: createjs.AbstractLoader.IMAGE},
        {id: "page08", src: imgpath+"page08.png", type: createjs.AbstractLoader.IMAGE},
        {id: "bhatro_patro", src: imgpath+"bhatro_patro.png", type: createjs.AbstractLoader.IMAGE},
        {id: "sagar", src: imgpath+"sagar.png", type: createjs.AbstractLoader.IMAGE},
        {id: "mala_holding_poster", src: imgpath+"mala_holding_poster.png", type: createjs.AbstractLoader.IMAGE},
        {id: "world_env_poster_without_tex", src: imgpath+"world_env_poster_without_tex.png", type: createjs.AbstractLoader.IMAGE},
        {id: "world_env_poster", src: imgpath+"world_env_poster.png", type: createjs.AbstractLoader.IMAGE},
        {id: "tourism_day", src: imgpath+"tourism_day.png", type: createjs.AbstractLoader.IMAGE},
        {id: "free_education_with_text", src: imgpath+"free_education_with_text.jpg", type: createjs.AbstractLoader.IMAGE},
        {id: "art_competition", src: imgpath+"art_competition_without_text.png", type: createjs.AbstractLoader.IMAGE},
        {id: "mala-06", src: imgpath+"mala-06.png", type: createjs.AbstractLoader.IMAGE},

        {id: "textbox", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
        {id: "textbox_two", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
        {id: "textbox_three", src: 'images/textbox/white/tl-2.png', type: createjs.AbstractLoader.IMAGE},


        // sounds
        {id: "sound_1", src: soundAsset+"s4_p1.ogg"},
        {id: "sound_2", src: soundAsset+"s4_p2.ogg"},
        {id: "sound_3", src: soundAsset+"s4_p3.ogg"},
        {id: "sound_4", src: soundAsset+"s4_p4.ogg"},
        {id: "sound_5", src: soundAsset+"s4_p5.ogg"},
        {id: "sound_6", src: soundAsset+"s4_p6.ogg"},
        {id: "sound_7", src: soundAsset+"s4_p7.ogg"},
        {id: "sound_8", src: soundAsset+"s4_p8.ogg"},
        {id: "sound_9", src: soundAsset+"s4_p9.ogg"},
        {id: "s9_1", src: soundAsset+"s4_p9_1.ogg"},
        {id: "s9_2", src: soundAsset+"s4_p9_2.ogg"},
        {id: "s9_3", src: soundAsset+"s4_p9_3.ogg"},
        {id: "s9_4", src: soundAsset+"s4_p9_4.ogg"},
        {id: "s9_5", src: soundAsset+"s4_p9_5.ogg"},
        {id: "s9_6", src: soundAsset+"s4_p9_6.ogg"},
        {id: "s9_7", src: soundAsset+"s4_p9_7.ogg"},
      ];
      preload = new createjs.LoadQueue(false);
      preload.installPlugin(createjs.Sound);//for registering sounds
      preload.installPlugin(createjs.Sound);//for registering sounds
      preload.on("progress", handleProgress);
      preload.on("complete", handleComplete);
      preload.on("fileload", handleFileLoad);
      preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
      // console.log(event.item);
    }

    function handleProgress(event) {
      $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
      $('#loading-wrapper').hide(0);
      //initialize varibales
      // call main function
      templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
    =            general template function            =
    =================================================*/
    function generaltemplate() {
      var source = $("#general-template").html();
      var template = Handlebars.compile(source);
      var html = template(content[countNext]);
      $board.html(html);
      texthighlight($board);
      vocabcontroller.findwords(countNext);
      put_image(content, countNext,preload);
      put_image1(content, countNext,preload);
      put_speechbox_image(content, countNext, preload);
      $('.speechbox').hide(0).fadeIn(500);
      var top_text_height = (($('.top_text').outerHeight())/($('.coverboardfull').outerHeight()))*100;
      $('.top_image').css("height",(top_text_height)+'%');
      countNext!=8?sound_player('sound_'+(countNext+1),true):"";
      if(countNext == 8) {
          sound_player('sound_'+(countNext+1),false)
          current_sound.on('complete', function () {
            var counter = 0;
              clickableDialogue(counter);
          });

          function clickableDialogue(counter){
              $(".clickable").eq(counter).addClass("blinkeffect");
              $(".clickable").click(function () {
                  $(".clickable").removeClass("blinkeffect");
                  console.log("countwer "+counter);
                  sound_player($(this).attr("data-answer"),counter>=6?true:false);
                  current_sound.on('complete', function () {
                      counter++;
                      counter!=8?clickableDialogue(counter):"";
                  });

              })
          }
      }

    }


    function sound_player(sound_id,navigate) {
      createjs.Sound.stop();
      current_sound = createjs.Sound.play(sound_id);
      current_sound.play();
      current_sound.on('complete', function () {
        navigate?navigationcontroller(countNext,$total_page):"";
      });
    }


    function templateCaller() {
      $prevBtn.css('display', 'none');
      $nextBtn.css('display', 'none');
      generaltemplate();
      loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
      createjs.Sound.stop();
      clearTimeout(timeoutvar);
      switch (countNext) {
        default:
        countNext++;
        templateCaller();
        break;
      }
    });

    $refreshBtn.click(function(){
      templateCaller();
    });

    $prevBtn.on('click', function () {
      createjs.Sound.stop();
      clearTimeout(timeoutvar);
      countNext--;
      templateCaller();
      /* if footerNotificationHandler pageEndSetNotification was called then on click of
      previous slide button hide the footernotification */
      countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });


    function texthighlight($highlightinside){
      //check if $highlightinside is provided
      typeof $highlightinside !== "object" ?
      alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
      null ;

      var $alltextpara = $highlightinside.find("*[data-highlight='true']");
      var stylerulename;
      var replaceinstring;
      var texthighlightstarttag;
      var texthighlightendtag   = "</span>";


      if($alltextpara.length > 0){
        $.each($alltextpara, function(index, val) {
          /*if there is a data-highlightcustomclass attribute defined for the text element
          use that or else use default 'parsedstring'*/
          $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
          (stylerulename = $(this).attr("data-highlightcustomclass")) :
          (stylerulename = "parsedstring") ;

          texthighlightstarttag = "<span class='"+stylerulename+"'>";


          replaceinstring       = $(this).html();
          replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
          replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


          $(this).html(replaceinstring);
        });
      }
    }
  });

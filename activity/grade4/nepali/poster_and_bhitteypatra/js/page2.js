var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/";

var content = [
  // slide0
  {
    contentnocenteradjust: true,
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "patro_1",
            imgid: "bhatro_patro"
          },
          {
            imgclass: "arrow_1",
            imgid: "arrow"
          }
        ]
      }
    ],
    uppertextblock: [
      {
        textclass: "dialogue_text1",
        textdata: data.string.p2text1
      },
      {
        textclass: "highlight_1"
      }
    ]
  },

  // slide1
  {
    contentnocenteradjust: true,
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "patro_1",
            imgid: "bhatro_patro"
          },
          {
            imgclass: "arrow_2",
            imgid: "arrow"
          }
        ]
      }
    ],
    uppertextblock: [
      {
        textclass: "dialogue_text1",
        textdata: data.string.p2text2
      },
      {
        textclass: "highlight_2"
      }
    ]
  },

  // slide2
  {
    contentnocenteradjust: true,
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "patro_1",
            imgid: "bhatro_patro"
          },
          {
            imgclass: "arrow_3",
            imgid: "arrow"
          }
        ]
      }
    ],
    uppertextblock: [
      {
        textclass: "dialogue_text1",
        textdata: data.string.p2text3
      },
      {
        textclass: "highlight_3"
      }
    ]
  },

  // slide3
  {
    contentnocenteradjust: true,
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "patro_1",
            imgid: "bhatro_patro"
          },
          {
            imgclass: "arrow_3",
            imgid: "arrow"
          }
        ]
      }
    ],
    uppertextblock: [
      {
        textclass: "dialogue_text1",
        textdata: data.string.p2text4
      },
      {
        textclass: "highlight_4"
      }
    ]
  },

  // slide4
  {
    contentnocenteradjust: true,
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "patro_1",
            imgid: "bhatro_patro"
          },
          {
            imgclass: "arrow_4",
            imgid: "arrow"
          }
        ]
      }
    ],
    uppertextblock: [
      {
        textclass: "dialogue_text1",
        textdata: data.string.p2text5
      },
      {
        textclass: "highlight_5"
      }
    ]
  },

  // slide5
  {
    contentnocenteradjust: true,
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "patro_1",
            imgid: "bhatro_patro"
          },
          {
            imgclass: "arrow_5",
            imgid: "arrow"
          }
        ]
      }
    ],
    uppertextblock: [
      {
        textclass: "dialogue_text1",
        textdata: data.string.p2text6
      },
      {
        textclass: "highlight_6"
      }
    ]
  },

  // slide6
  {
    contentnocenteradjust: true,
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: "page07",
            imgsrc: ""
          },
          {
            imgclass: "patro_right_4",
            imgid: "patro_july",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        textclass: "insidetext",
        textdata: data.string.p2text7,
        speechbox: "speech_1",
        imgid: "textbox"
      }
    ]
  },

  // slide7
  {
    contentnocenteradjust: true,
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: "page07",
            imgsrc: ""
          },
          {
            imgclass: "patro_right_4",
            imgid: "patro_july",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        textclass: "insidetext",
        textdata: data.string.p2text8,
        speechbox: "speech_1",
        imgid: "textbox"
      }
    ]
  },

  // slide8
  {
    contentnocenteradjust: true,
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: "page03",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        textclass: "insidetext_type2",
        textdata: data.string.p2text9,
        speechbox: "speech_2",
        imgid: "textbox_three"
      }
    ]
  },

  // slide9
  {
    contentnocenteradjust: true,
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: "page07",
            imgsrc: ""
          },
          {
            imgclass: "patro_right_4",
            imgid: "patro_july",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        textclass: "insidetext",
        textdata: data.string.p2text10,
        speechbox: "speech_1",
        imgid: "textbox"
      }
    ]
  },

  // slide10
  {
    contentnocenteradjust: true,
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: "page07",
            imgsrc: ""
          },
          {
            imgclass: "patro_right_4",
            imgid: "patro_july",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        textclass: "insidetext",
        textdata: data.string.p2text11,
        speechbox: "speech_1",
        imgid: "textbox"
      }
    ]
  },

  // slide11
  {
    contentnocenteradjust: true,
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: "page02",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        textclass: "insidetext_type2",
        textdata: data.string.p2text12,
        speechbox: "speech_3",
        imgid: "textbox_two"
      }
    ]
  },

  // slide12
  {
    contentnocenteradjust: true,
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: "page07",
            imgsrc: ""
          },
          {
            imgclass: "patro_right_4",
            imgid: "patro_july",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        textclass: "insidetext",
        textdata: data.string.p2text13,
        speechbox: "speech_1",
        imgid: "textbox"
      }
    ]
  },

  // slide13
  {
    contentnocenteradjust: true,
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: "page05",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        textclass: "insidetext_type2",
        textdata: data.string.p2text14,
        speechbox: "speech_5",
        imgid: "textbox_two"
      }
    ]
  },

  // slide14
  {
    contentnocenteradjust: true,
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: "page07",
            imgsrc: ""
          },
          {
            imgclass: "patro_right_4",
            imgid: "patro_july",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        textclass: "insidetext",
        textdata: data.string.p2text15,
        speechbox: "speech_14",
        imgid: "textbox"
      }
    ]
  },

  // slide15
  {
    contentnocenteradjust: true,
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: "page02",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        textclass: "insidetext_type2",
        textdata: data.string.p2text16,
        speechbox: "speech_3",
        imgid: "textbox_two"
      }
    ]
  }
];

$(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;
  var count = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page, countNext + 1);

  var preload;
  var timeoutvar = null;
  var current_sound;
  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();
  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      {
        id: "bhatro_patro",
        src: imgpath + "bhatro_patro.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "patro_july",
        src: imgpath + "patro_july.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "page02",
        src: imgpath + "page02.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "page07",
        src: imgpath + "page07.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "page03",
        src: imgpath + "page03.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "page05",
        src: imgpath + "page05.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "textbox_three",
        src: "images/textbox/white/tl-2.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "textbox_two",
        src: "images/textbox/white/tr-2.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "textbox",
        src: "images/textbox/white/lb-1.png",
        type: createjs.AbstractLoader.IMAGE
      },

      {
        id: "arrow",
        src: imgpath + "arrow.png",
        type: createjs.AbstractLoader.IMAGE
      },
      // sounds
      { id: "sound_1", src: soundAsset + "s2_p1.ogg" },
      { id: "sound_2", src: soundAsset + "s2_p2.ogg" },
      { id: "sound_3", src: soundAsset + "s2_p3.ogg" },
      { id: "sound_4", src: soundAsset + "s2_p4.ogg" },
      { id: "sound_5", src: soundAsset + "s2_p5.ogg" },
      { id: "sound_6", src: soundAsset + "s2_p6.ogg" },
      { id: "sound_7", src: soundAsset + "s2_p7.ogg" },
      { id: "sound_8", src: soundAsset + "s2_p8.ogg" },
      { id: "sound_9", src: soundAsset + "s2_p9.ogg" },
      { id: "sound_10", src: soundAsset + "s2_p10.ogg" },
      { id: "sound_11", src: soundAsset + "s2_p11.ogg" },
      { id: "sound_12", src: soundAsset + "s2_p12.ogg" },
      { id: "sound_13", src: soundAsset + "s2_p13.ogg" },
      { id: "sound_14", src: soundAsset + "s2_p14.ogg" },
      { id: "sound_15", src: soundAsset + "s2_p15.ogg" },
      { id: "sound_16", src: soundAsset + "s2_p16.ogg" }
    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound); //for registering sounds
    preload.installPlugin(createjs.Sound); //for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }

  function handleFileLoad(event) {
    // console.log(event.item);
  }

  function handleProgress(event) {
    $("#loading-text").html(parseInt(event.loaded * 100) + "%");
  }

  function handleComplete(event) {
    $("#loading-wrapper").hide(0);
    //initialize varibales
    current_sound = createjs.Sound.play("sound_1");
    current_sound.stop();
    // call main function
    templateCaller();
  }

  //initialize
  init();

  /*==================================================
  =            Handlers and helpers Block            =
  ==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

  /*=================================================
  =            general template function            =
  =================================================*/
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    texthighlight($board);
    vocabcontroller.findwords(countNext);
    put_image(content, countNext, preload);
    put_speechbox_image(content, countNext, preload);
    $(".speechbox")
      .hide(0)
      .fadeIn(500);
    switch (countNext) {
      case 0:
        $(".dialogue_text1,.arrow_1,.highlight_1").hide(0);
        $(".patro_1")
          .hide(0)
          .fadeIn(500, function() {
            $(".dialogue_text1").fadeIn(500, function() {
              createjs.Sound.stop();
              current_sound = createjs.Sound.play("sound_1");
              current_sound.play();
              $(".arrow_1,.highlight_1").fadeIn(500, function() {
                current_sound.on("complete", function() {
                  $(".dialogue_text1,.arrow_1").hide(200);
                  $(".highlight_1").animate(
                    { width: "23%", left: "9%", top: "17%", height: "8%" },
                    1000
                  );
                  $(".patro_1").animate(
                    { width: "90%", left: "5%", top: "108%" },
                    1000,
                    function() {
                      navigationcontroller(countNext, $total_page);
                    }
                  );
                });
              });
            });
          });
        break;

      case 3:
        sound_player("sound_" + (countNext + 1));
        $(".highlight_4").animate({ left: "61.3%" }, 1000, function() {
          $(".arrow_3").fadeOut(300);
          $(".highlight_4").animate({ left: "66.8%" }, 1000, function() {
            $(".highlight_4").animate({ left: "72%" }, 1000, function() {
              $(".highlight_4").animate({ left: "77.4%" }, 1000, function() {
                $(".highlight_4").animate({ left: "82.7%" }, 1000, function() {
                  $(".highlight_4").animate(
                    { left: "87.8%" },
                    1000,
                    function() {
                      navigationcontroller(countNext, $total_page);
                    }
                  );
                });
              });
            });
          });
        });
        break;

      default:
        sound_player("sound_" + (countNext + 1));
        break;
    }
    navigationcontroller;
  }

  function sound_player(sound_id, navigate) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on("complete", function() {
      navigationcontroller(countNext, $total_page);
    });
  }

  function templateCaller() {
    $prevBtn.css("display", "none");
    $nextBtn.css("display", "none");
    generaltemplate();
    loadTimelineProgress($total_page, countNext + 1);
  }

  $nextBtn.on("click", function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    switch (countNext) {
      default:
        countNext++;
        templateCaller();
        break;
    }
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    countNext--;
    templateCaller();
    /* if footerNotificationHandler pageEndSetNotification was called then on click of
    previous slide button hide the footernotification */
    countNext < $total_page
      ? ole.footerNotificationHandler.hideNotification()
      : null;
  });

  function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object"
      ? alert(
          "Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted"
        )
      : null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";

    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
        use that or else use default 'parsedstring'*/
        $(this).attr(
          "data-highlightcustomclass"
        ) /*if there is data-highlightcustomclass defined it is true else it is not*/
          ? (stylerulename = $(this).attr("data-highlightcustomclass"))
          : (stylerulename = "parsedstring");

        texthighlightstarttag = "<span class='" + stylerulename + "'>";

        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

        $(this).html(replaceinstring);
      });
    }
  }
});

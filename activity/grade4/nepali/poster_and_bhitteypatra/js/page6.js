var imgpath = $ref + "/images/";
var imgpath_poster = $ref + "/images/posters/";
var soundAsset = $ref+"/sounds/";
var clicked_poster_number;
var text_1_container;
var clicked_slide4_text_number;
var slide4_text_array = [
  [data.string.p6text6,data.string.p6text7],
  [data.string.p6text8,data.string.p6text9],
  [data.string.p6text10,data.string.p6text11]
];

var text_2_container;
var text_3_container;
var clicked_slide5_text_number;
var slide5_text_array = [
  [data.string.p6text12,data.string.p6text13],
  [data.string.p6text14,data.string.p6text15],
  [data.string.p6text16,data.string.p6text17]
];

var content = [
  // slide0
  {
    contentnocenteradjust: true,
    uppertextblock:[{
      textclass:'centertext',
      textdata:data.string.p3text1
    }],
    imageblock:[{
      imagestoshow:[{
        imgclass:"bg_full",
        imgid:"diy_bg_2"
      }]
    }]
  },

  // slide1
  {
    contentnocenteradjust: true,
    contentblockadditionalclass:'background_blue',
    uppertextblock:[{
      textclass:'top_text',
      textdata:data.string.p6text1
    }],
    imageblock:[{
      imagestoshow:[{
        imgclass:"poster_1",
        imgid:"post_1"
      },
      {
        imgclass:"poster_2",
        imgid:"post_2"
      },
      {
        imgclass:"poster_3",
        imgid:"post_3"
      }]
    }]
  },

  // slide2
  {
    contentnocenteradjust: true,
    contentblockadditionalclass:'background_blue',
    uppertextblock:[{
      textclass:'top_text',
      textdata:data.string.p6text2
    },
    {
      textclass:'left_bg',
    },
    {
      textclass:'the_drop_box'
    },
    {
      textclass:'text_1',
      textdata:data.string.p6text3
    },
    {
      textclass:'text_2',
      textdata:data.string.p6text4
    },
    {
      textclass:'text_3',
      textdata:data.string.p6text5
    }],
    only_image:[{
      imgclass:"selected_poster",
    }]
  },

  // slide3
  {
    contentnocenteradjust: true,
    contentblockadditionalclass:'background_blue',
    uppertextblock:[{
      textclass:'top_text',
      textdata:data.string.p6text2
    },
    {
      textclass:'left_bg',
    },
    {
      textclass:'title_text'
    },
    {
      textclass:'the_seconddrop_box'
    },
    {
      textclass:'secondtext_1',
    },
    {
      textclass:'secondtext_2',
    }],
    only_image:[{
      imgclass:"selected_poster",
    }]
  },


  // slide4
  {
    contentnocenteradjust: true,
    contentblockadditionalclass:'background_blue',
    uppertextblock:[{
      textclass:'top_text',
      textdata:data.string.p6text2
    },
    {
      textclass:'left_bg',
    },
    {
      textclass:'title_text'
    },
    {
      textclass:'mid_text'
    },
    {
      textclass:'the_thirddropbox'
    },
    {
      textclass:'thirdtext_1',
    },
    {
      textclass:'thirdtext_2',
    }],
    only_image:[{
      imgclass:"selected_poster",
    }]
  },

  // slide5
  {
    contentnocenteradjust: true,
    contentblockadditionalclass:'background_blue',
    uppertextblock:[{
      textclass:'top_text',
      textdata:data.string.p6text18
    },
    {
      textclass:'final_text_1',
    },
    {
      textclass:'final_text_2'
    },
    {
      textclass:'final_text_3'
    }],
    only_image:[{
      imgclass:"final_poster",
    }]
  },

];

$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;
  var count=0;

  var $total_page = content.length;
  loadTimelineProgress($total_page, countNext + 1);

  var preload;
  var timeoutvar = null;
  var current_sound;
  var vocabcontroller =  new Vocabulary();
  vocabcontroller.init();
  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      {id: "diy_bg_2", src: imgpath+"diy_bg_2.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "post_1", src: imgpath_poster+"making_own_poster02.png", type: createjs.AbstractLoader.IMAGE},
      {id: "post_3", src: imgpath_poster+"making_own_poster03.png", type: createjs.AbstractLoader.IMAGE},
      {id: "post_2", src: imgpath_poster+"making_own_poster04.png", type: createjs.AbstractLoader.IMAGE},

      // sounds
      {id: "sound_1", src: soundAsset+"s6_p2.ogg"},
      {id: "sound_2", src: soundAsset+"s6_p3.ogg"},
      {id: "sound_3", src: soundAsset+"s6_p6.ogg"},
    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound);//for registering sounds
    preload.installPlugin(createjs.Sound);//for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }

  function handleFileLoad(event) {
    // console.log(event.item);
  }

  function handleProgress(event) {
    $('#loading-text').html(parseInt(event.loaded * 100) + '%');
  }

  function handleComplete(event) {
    $('#loading-wrapper').hide(0);
    //initialize varibales
    current_sound = createjs.Sound.play('sound_1');
    current_sound.stop();
    // call main function
    templateCaller();
  }

  //initialize
  init();

  /*==================================================
  =            Handlers and helpers Block            =
  ==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


  /*=================================================
  =            general template function            =
  =================================================*/
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    texthighlight($board);
    vocabcontroller.findwords(countNext);
    put_image(content, countNext,preload);
    switch (countNext) {
      case 0:
      play_diy_audio();
      navigationcontroller(countNext,$total_page);
      break;
      case 1:
      sound_player('sound_1');
      $('.poster_1,.poster_2,.poster_3').click(function(){
        $(this).css('border','.8vmin solid #d9fc7b');
        var splitted_string_array = $(this).attr('class').split('_');
        clicked_poster_number = splitted_string_array[1];
        $('.poster_1,.poster_2,.poster_3').css('pointer-events','none');
        navigationcontroller(countNext,$total_page);
      });
      break;
      case 2:
      sound_player('sound_2');
      $('.selected_poster').attr('src',preload.getResult('post_'+clicked_poster_number).src);
      $('.the_drop_box').addClass('the_drop_box_'+clicked_poster_number);

      var draggables = $(".text_1,.text_2,.text_3").draggable({
        containment : "body",
        cursor : "grabbing",
        revert : "invalid",
        appendTo : "body",
        zindex : 10000,
      });


      $('.the_drop_box').droppable({
        accept : ".text_1,.text_2,.text_3",
        hoverClass : "hovered",
        drop : handleCardDrop1
      });


      function handleCardDrop1(event, ui){
        var dropped = ui.draggable;
        var droppedOn = $(this);
        droppedOn.css({'height':'auto','padding':'1%'});
        $(ui.draggable).hide(0);
        $(".text_1,.text_2,.text_3").css('pointer-events','none');
        clicked_slide4_text_number = dropped.attr('class').split('_')[1].split(' ')[0];
        console.log(clicked_slide4_text_number);
        text_1_container = $(ui.draggable).html();
        $('.the_drop_box').html(text_1_container);
        draggables.draggable('disable');
        navigationcontroller(countNext,$total_page);
      }

      break;

      case 3:
      $('.selected_poster').attr('src',preload.getResult('post_'+clicked_poster_number).src);
      $('.title_text').addClass('the_drop_box_'+clicked_poster_number).css({'height':'auto','padding':'1%','background':'none','border':'0','font-size':'1.8vw'}).html(text_1_container);
      $('.secondtext_1').html(slide4_text_array[clicked_slide4_text_number-1][0]);
      $('.secondtext_2').html(slide4_text_array[clicked_slide4_text_number-1][1]);
      $('.the_seconddrop_box').addClass('the_seconddrop_box'+clicked_poster_number);

      var draggables1 = $(".secondtext_1,.secondtext_2").draggable({
        containment : "body",
        cursor : "grabbing",
        revert : "invalid",
        appendTo : "body",
        zindex : 10000
      });


      $('.the_seconddrop_box').droppable({
        accept : ".secondtext_1,.secondtext_2",
        hoverClass : "hovered",
        drop : handleCardDrop2
      });


      function handleCardDrop2(event, ui){
        var dropped = ui.draggable;
        var droppedOn = $(this);
        $(".secondtext_1,.secondtext_2").css('pointer-events','none');
        droppedOn.css({'height':'auto','padding':'1%'});
        $(ui.draggable).hide(0);
        text_2_container = $(ui.draggable).html();
        $('.the_seconddrop_box').html(text_2_container);
        draggables1.draggable('disable');
        navigationcontroller(countNext,$total_page);
      }
      break;


      case 4:
      $('.selected_poster').attr('src',preload.getResult('post_'+clicked_poster_number).src);
      $('.title_text').addClass('the_drop_box_'+clicked_poster_number).css({'height':'auto','padding':'1%','background':'none','border':'0','font-size':'1.8vw'}).html(text_1_container);
      $('.mid_text').addClass('the_seconddrop_box'+clicked_poster_number).css({'height':'auto','padding':'1%','background':'none','border':'0','font-size':'1.2vw'}).html(text_2_container);
      $('.thirdtext_1').html(slide5_text_array[clicked_slide4_text_number-1][0]);
      $('.thirdtext_2').html(slide5_text_array[clicked_slide4_text_number-1][1]);
      $('.the_thirddropbox').addClass('the_thirddropbox'+clicked_poster_number);

      var draggables1 = $(".thirdtext_1,.thirdtext_2").draggable({
        containment : "body",
        cursor : "grabbing",
        revert : "invalid",
        appendTo : "body",
        zindex : 10000
      });


      $('.the_thirddropbox').droppable({
        accept : ".thirdtext_1,.thirdtext_2",
        hoverClass : "hovered",
        drop : handleCardDrop3
      });


      function handleCardDrop3(event, ui){
        var dropped = ui.draggable;
        var droppedOn = $(this);
        $(".thirdtext_1,.thirdtext_2").css('pointer-events','none');
        droppedOn.css({'height':'auto','padding':'1%'});
        $(ui.draggable).hide(0);
        text_3_container = $(ui.draggable).html();
        $('.the_thirddropbox').html(text_3_container);
        draggables1.draggable('disable');
        navigationcontroller(countNext,$total_page);
      }
      break;

      case 5:
      sound_player('sound_3','1');
      $('.final_poster').attr('src',preload.getResult('post_'+clicked_poster_number).src);
      $('.final_text_1').html(text_1_container).addClass('final_text_1_'+clicked_poster_number);
      $('.final_text_2').html(text_2_container).addClass('final_text_2_'+clicked_poster_number);
      $('.final_text_3').html(text_3_container).addClass('final_text_3_'+clicked_poster_number);
      break;

      default:
      navigationcontroller(countNext,$total_page);
      break;

    }

  }



  function sound_player(sound_id,navigate) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on('complete', function () {
      navigate?navigationcontroller(countNext,$total_page):"";
    });
  }


  function templateCaller() {
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');
    generaltemplate();
    loadTimelineProgress($total_page, countNext + 1);
  }

  $nextBtn.on('click', function () {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    switch (countNext) {
      default:
      countNext++;
      templateCaller();
      break;
    }
  });

  $refreshBtn.click(function(){
    templateCaller();
  });

  $prevBtn.on('click', function () {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    countNext--;
    templateCaller();
    /* if footerNotificationHandler pageEndSetNotification was called then on click of
    previous slide button hide the footernotification */
    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
  });

  function texthighlight($highlightinside){
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
    alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
    null ;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag   = "</span>";


    if($alltextpara.length > 0){
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
        use that or else use default 'parsedstring'*/
        $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
        (stylerulename = $(this).attr("data-highlightcustomclass")) :
        (stylerulename = "parsedstring") ;

        texthighlightstarttag = "<span class='"+stylerulename+"'>";


        replaceinstring       = $(this).html();
        replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
        replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


        $(this).html(replaceinstring);
      });
    }
  }

});

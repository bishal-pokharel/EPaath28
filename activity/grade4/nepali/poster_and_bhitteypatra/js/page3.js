var imgpath = $ref + "/images/";
var imgpath_calendar = $ref + "/images/calendar/";
var soundAsset = $ref+"/sounds/";
var current_calendar = 0;
var month_array = [data.string.p3text10,
  data.string.p3text11,
  data.string.p3text12,
  data.string.p3text13,
  data.string.p3text14,
  data.string.p3text15,
  data.string.p3text16,
  data.string.p3text17,
  data.string.p3text18,
  data.string.p3text19,
  data.string.p3text20,
  data.string.p3text21
];
var correct_options_0 = [
  [data.string.p3text7,data.string.p3text6,data.string.p3text8,data.string.p3text5],
  [data.string.p3text7,data.string.p3text6,data.string.p3text8,data.string.p3text5],
  [data.string.p3text8,data.string.p3text7,data.string.p3text6,data.string.p3text5],
  [data.string.p3text7,data.string.p3text6,data.string.p3text8,data.string.p3text5],
  [data.string.p3text7,data.string.p3text6,data.string.p3text8,data.string.p3text5],
  [data.string.p3text7,data.string.p3text6,data.string.p3text8,data.string.p3text5],
  [data.string.p3text6,data.string.p3text7,data.string.p3text8,data.string.p3text5],
  [data.string.p3text5,data.string.p3text7,data.string.p3text8,data.string.p3text6],
  [data.string.p3text6,data.string.p3text7,data.string.p3text8,data.string.p3text5],
  [data.string.p3text5,data.string.p3text7,data.string.p3text8,data.string.p3text6],
  [data.string.p3text6,data.string.p3text7,data.string.p3text8,data.string.p3text5],
  [data.string.p3text6,data.string.p3text7,data.string.p3text8,data.string.p3text5]
];
var correct_options_1 = [
  [data.string.p3text31,data.string.p3text32,data.string.p3text33,data.string.p3text34],
  [data.string.p3text34,data.string.p3text32,data.string.p3text33,data.string.p3text31],
  [data.string.p3text30,data.string.p3text32,data.string.p3text33,data.string.p3text34],
  [data.string.p3text34,data.string.p3text32,data.string.p3text33,data.string.p3text31],
  [data.string.p3text30,data.string.p3text32,data.string.p3text33,data.string.p3text34],
  [data.string.p3text33,data.string.p3text32,data.string.p3text31,data.string.p3text34],
  [data.string.p3text29,data.string.p3text32,data.string.p3text33,data.string.p3text34],
  [data.string.p3text31,data.string.p3text32,data.string.p3text33,data.string.p3text34],
  [data.string.p3text32,data.string.p3text31,data.string.p3text33,data.string.p3text34],
  [data.string.p3text34,data.string.p3text32,data.string.p3text33,data.string.p3text31],
  [data.string.p3text28,data.string.p3text32,data.string.p3text33,data.string.p3text34],
  [data.string.p3text30,data.string.p3text32,data.string.p3text33,data.string.p3text34]
];
var correct_options_2 = [
  [data.string.p3text36,data.string.p3text38,data.string.p3text35,data.string.p3text37],
  [data.string.p3text35,data.string.p3text38,data.string.p3text36,data.string.p3text37],
  [data.string.p3text36,data.string.p3text35,data.string.p3text38,data.string.p3text37],
  [data.string.p3text35,data.string.p3text38,data.string.p3text36,data.string.p3text37],
  [data.string.p3text36,data.string.p3text38,data.string.p3text35,data.string.p3text37],
  [data.string.p3text35,data.string.p3text38,data.string.p3text36,data.string.p3text37],
  [data.string.p3text35,data.string.p3text39,data.string.p3text36,data.string.p3text37],
  [data.string.p3text36,data.string.p3text38,data.string.p3text35,data.string.p3text37],
  [data.string.p3text35,data.string.p3text38,data.string.p3text36,data.string.p3text37],
  [data.string.p3text35,data.string.p3text36,data.string.p3text38,data.string.p3text37],
  [data.string.p3text35,data.string.p3text37,data.string.p3text38,data.string.p3text36],
  [data.string.p3text36,data.string.p3text35,data.string.p3text37,data.string.p3text38]
];
var correct_options_3 = [
  [data.string.p3text11,data.string.p3text12,data.string.p3text13,data.string.p3text14],
  [data.string.p3text12,data.string.p3text13,data.string.p3text14,data.string.p3text15],
  [data.string.p3text13,data.string.p3text14,data.string.p3text15,data.string.p3text16],
  [data.string.p3text14,data.string.p3text15,data.string.p3text16,data.string.p3text17],
  [data.string.p3text15,data.string.p3text16,data.string.p3text17,data.string.p3text18],
  [data.string.p3text16,data.string.p3text17,data.string.p3text18,data.string.p3text19],
  [data.string.p3text17,data.string.p3text18,data.string.p3text19,data.string.p3text20],
  [data.string.p3text18,data.string.p3text19,data.string.p3text20,data.string.p3text21],
  [data.string.p3text19,data.string.p3text20,data.string.p3text21,data.string.p3text10],
  [data.string.p3text20,data.string.p3text21,data.string.p3text10,data.string.p3text11],
  [data.string.p3text21,data.string.p3text29,data.string.p3text11,data.string.p3text12],
  [data.string.p3text10,data.string.p3text23,data.string.p3text12,data.string.p3text13]
];
var correct_options_4 = [
  [data.string.p3text21,data.string.p3text20,data.string.p3text11,data.string.p3text12],
  [data.string.p3text10,data.string.p3text21,data.string.p3text12,data.string.p3text13],
  [data.string.p3text11,data.string.p3text12,data.string.p3text13,data.string.p3text14],
  [data.string.p3text12,data.string.p3text13,data.string.p3text14,data.string.p3text15],
  [data.string.p3text13,data.string.p3text14,data.string.p3text15,data.string.p3text16],
  [data.string.p3text14,data.string.p3text15,data.string.p3text16,data.string.p3text17],
  [data.string.p3text15,data.string.p3text16,data.string.p3text17,data.string.p3text18],
  [data.string.p3text16,data.string.p3text17,data.string.p3text18,data.string.p3text19],
  [data.string.p3text17,data.string.p3text18,data.string.p3text19,data.string.p3text20],
  [data.string.p3text18,data.string.p3text19,data.string.p3text20,data.string.p3text21],
  [data.string.p3text19,data.string.p3text20,data.string.p3text21,data.string.p3text10],
  [data.string.p3text20,data.string.p3text21,data.string.p3text10,data.string.p3text11]
];
var correct_options_5 = [
  [data.string.p3text41,data.string.p3text42,data.string.p3text43,data.string.p3text44],
  [data.string.p3text45,data.string.p3text42,data.string.p3text43,data.string.p3text44],
  [data.string.p3text42,data.string.p3text41,data.string.p3text43,data.string.p3text44],
  [data.string.p3text45,data.string.p3text42,data.string.p3text43,data.string.p3text44],
  [data.string.p3text42,data.string.p3text42,data.string.p3text43,data.string.p3text44],
  [data.string.p3text46,data.string.p3text42,data.string.p3text43,data.string.p3text44],
  [data.string.p3text43,data.string.p3text42,data.string.p3text41,data.string.p3text44],
  [data.string.p3text41,data.string.p3text42,data.string.p3text43,data.string.p3text44],
  [data.string.p3text40,data.string.p3text42,data.string.p3text43,data.string.p3text44],
  [data.string.p3text47,data.string.p3text42,data.string.p3text43,data.string.p3text44],
  [data.string.p3text44,data.string.p3text42,data.string.p3text43,data.string.p3text41],
  [data.string.p3text42,data.string.p3text41,data.string.p3text43,data.string.p3text44],
];
var correct_options_6 = [
  [data.string.p3text38,data.string.p3text35,data.string.p3text36,data.string.p3text37],
  [data.string.p3text35,data.string.p3text38,data.string.p3text36,data.string.p3text37],
  [data.string.p3text36,data.string.p3text35,data.string.p3text38,data.string.p3text37],
  [data.string.p3text35,data.string.p3text38,data.string.p3text36,data.string.p3text37],
  [data.string.p3text36,data.string.p3text38,data.string.p3text35,data.string.p3text37],
  [data.string.p3text38,data.string.p3text35,data.string.p3text36,data.string.p3text37],
  [data.string.p3text39,data.string.p3text35,data.string.p3text36,data.string.p3text37],
  [data.string.p3text36,data.string.p3text38,data.string.p3text35,data.string.p3text37],
  [data.string.p3text35,data.string.p3text38,data.string.p3text36,data.string.p3text37],
  [data.string.p3text35,data.string.p3text36,data.string.p3text38,data.string.p3text37],
  [data.string.p3text37,data.string.p3text35,data.string.p3text38,data.string.p3text36],
  [data.string.p3text37,data.string.p3text35,data.string.p3text36,data.string.p3text38]
];
var content = [
  // slide0
  {
    contentnocenteradjust: true,
    uppertextblock:[{
      textclass:'centertext',
      textdata:data.string.p3text1
    }],
    imageblock:[{
      imagestoshow:[{
        imgclass:"bg_full",
        imgid:"diy_bg_1"
      }]
    }]
  },

  // slide1
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: 'cream_bg',
    uppertextblock:[{
      textclass:'top_text',
      textdata:data.string.p3text2
    }],
    calendar_selector:[{
    }],
    imageblock:[{
      imagestoshow:[{
        imgclass:"calendar",
        imgid:"calendar_1"
      }]
    }]
  },

  // slide2
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: 'cream_bg',
    uppertextblock:[{
      textclass:'top_text',
      textdata:data.string.p3text49
    },{
      textclass:'left_half',
    },{
      textclass:'right_half',
    },{
      textclass:'question',
      textdata:data.string.p3text48,
      datahighlightflag:true,
      datahighlightcustomclass:'question_word'
    },{
      textclass:'option_1'
    },{
      textclass:'option_2'
    },{
      textclass:'option_3'
    },{
      textclass:'option_4'
    }],
    extraimage:[{
      imgclass:"calendar_right",
    }]
  },


  // slide3
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: 'cream_bg',
    uppertextblock:[{
      textclass:'top_text',
      textdata:data.string.p3text49
    },{
      textclass:'left_half',
    },{
      textclass:'right_half',
    },{
      textclass:'question',
      textdata:data.string.p3text22,
      datahighlightflag:true,
      datahighlightcustomclass:'question_word'
    },{
      textclass:'option_1'
    },{
      textclass:'option_2'
    },{
      textclass:'option_3'
    },{
      textclass:'option_4'
    }],
    extraimage:[{
      imgclass:"calendar_right",
    }]
  },

  // slide4
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: 'cream_bg',
    uppertextblock:[{
      textclass:'top_text',
      textdata:data.string.p3text49
    },{
      textclass:'left_half',
    },{
      textclass:'right_half',
    },{
      textclass:'question',
      textdata:data.string.p3text23,
      datahighlightflag:true,
      datahighlightcustomclass:'question_word'
    },{
      textclass:'option_1'
    },{
      textclass:'option_2'
    },{
      textclass:'option_3'
    },{
      textclass:'option_4'
    }],
    extraimage:[{
      imgclass:"calendar_right",
    }]
  },

  // slide5
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: 'cream_bg',
    uppertextblock:[{
      textclass:'top_text',
      textdata:data.string.p3text49
    },{
      textclass:'left_half',
    },{
      textclass:'right_half',
    },{
      textclass:'question',
      textdata:data.string.p3text24,
      datahighlightflag:true,
      datahighlightcustomclass:'question_word'
    },{
      textclass:'option_1'
    },{
      textclass:'option_2'
    },{
      textclass:'option_3'
    },{
      textclass:'option_4'
    }],
    extraimage:[{
      imgclass:"calendar_right",
    }]
  },

  // slide6
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: 'cream_bg',
    uppertextblock:[{
      textclass:'top_text',
      textdata:data.string.p3text49
    },{
      textclass:'left_half',
    },{
      textclass:'right_half',
    },{
      textclass:'question',
      textdata:data.string.p3text25,
      datahighlightflag:true,
      datahighlightcustomclass:'question_word'
    },{
      textclass:'option_1'
    },{
      textclass:'option_2'
    },{
      textclass:'option_3'
    },{
      textclass:'option_4'
    }],
    extraimage:[{
      imgclass:"calendar_right",
    }]
  },

  // slide7
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: 'cream_bg',
    uppertextblock:[{
      textclass:'top_text',
      textdata:data.string.p3text49
    },{
      textclass:'left_half',
    },{
      textclass:'right_half',
    },{
      textclass:'question',
      textdata:data.string.p3text26,
      datahighlightflag:true,
      datahighlightcustomclass:'question_word'
    },{
      textclass:'option_1'
    },{
      textclass:'option_2'
    },{
      textclass:'option_3'
    },{
      textclass:'option_4'
    }],
    extraimage:[{
      imgclass:"calendar_right",
    }]
  },

  // slide8
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: 'cream_bg',
    uppertextblock:[{
      textclass:'top_text',
      textdata:data.string.p3text49
    },{
      textclass:'left_half',
    },{
      textclass:'right_half',
    },{
      textclass:'question',
      textdata:data.string.p3text27,
      datahighlightflag:true,
      datahighlightcustomclass:'question_word'
    },{
      textclass:'option_1'
    },{
      textclass:'option_2'
    },{
      textclass:'option_3'
    },{
      textclass:'option_4'
    }],
    extraimage:[{
      imgclass:"calendar_right",
    }]
  },
];

$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;
  var count=0;

  var $total_page = content.length;
  loadTimelineProgress($total_page, countNext + 1);

  var preload;
  var timeoutvar = null;
  var current_sound;
  var vocabcontroller =  new Vocabulary();
  vocabcontroller.init();
  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      {id: "calendar_1", src: imgpath_calendar+"calendar_1.png", type: createjs.AbstractLoader.IMAGE},
      {id: "calendar_2", src: imgpath_calendar+"calendar_2.png", type: createjs.AbstractLoader.IMAGE},
      {id: "calendar_3", src: imgpath_calendar+"calendar_3.png", type: createjs.AbstractLoader.IMAGE},
      {id: "calendar_4", src: imgpath_calendar+"calendar_4.png", type: createjs.AbstractLoader.IMAGE},
      {id: "calendar_5", src: imgpath_calendar+"calendar_5.png", type: createjs.AbstractLoader.IMAGE},
      {id: "calendar_6", src: imgpath_calendar+"calendar_6.png", type: createjs.AbstractLoader.IMAGE},
      {id: "calendar_7", src: imgpath_calendar+"calendar_7.png", type: createjs.AbstractLoader.IMAGE},
      {id: "calendar_8", src: imgpath_calendar+"calendar_8.png", type: createjs.AbstractLoader.IMAGE},
      {id: "calendar_9", src: imgpath_calendar+"calendar_9.png", type: createjs.AbstractLoader.IMAGE},
      {id: "calendar_10", src: imgpath_calendar+"calendar_10.png", type: createjs.AbstractLoader.IMAGE},
      {id: "calendar_11", src: imgpath_calendar+"calendar_11.png", type: createjs.AbstractLoader.IMAGE},
      {id: "calendar_12", src: imgpath_calendar+"calendar_12.png", type: createjs.AbstractLoader.IMAGE},
      {id: "btn01", src: imgpath+"arrow01.png", type: createjs.AbstractLoader.IMAGE},
      {id: "diy_bg_1", src: imgpath+"diy_bg_1.jpg", type: createjs.AbstractLoader.IMAGE},
      // sounds
      {id: "sound_2", src: soundAsset+"s3_p2.ogg"},
      {id: "sound_3", src: soundAsset+"s3_p3.ogg"},

    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound);//for registering sounds
    preload.installPlugin(createjs.Sound);//for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }

  function handleFileLoad(event) {
    // console.log(event.item);
  }

  function handleProgress(event) {
    $('#loading-text').html(parseInt(event.loaded * 100) + '%');
  }

  function handleComplete(event) {
    $('#loading-wrapper').hide(0);
    //initialize varibales
    current_sound = createjs.Sound.play('sound_1');
    current_sound.stop();
    // call main function
    templateCaller();
  }

  //initialize
  init();

  /*==================================================
  =            Handlers and helpers Block            =
  ==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


  /*=================================================
  =            general template function            =
  =================================================*/
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    texthighlight($board);
    vocabcontroller.findwords(countNext);
    put_image(content, countNext,preload);
    put_speechbox_image(content, countNext, preload);
    $('.speechbox').hide(0).fadeIn(500);

    $('.option_1,.option_2,.option_3,.option_4').click(function(){
      createjs.Sound.stop();
      if($(this).hasClass('correct')){
        var $this = $(this);
        var position = $this.position();
        var width = $this.width();
        var height = $this.height();
        var centerX = ((position.left + width / 2)*100)/$('.coverboardfull ').width()+'%';
        var centerY = (((position.top + height)*100)/$('.coverboardfull ').height())+'%';
        $(this).css({'border-color':'#D4EF34','background':'#92AF3B'});
        $('.option_1,.option_2,.option_3,.option_4').css('pointer-events','none');
        $('<img style="left:'+centerX+';top:'+centerY+';position:absolute;height:5%;transform:translate(-4%,-220%);z-index:2;" src="'+imgpath +'correct.png" />').insertAfter(this);
        play_correct_incorrect_sound(1);
        navigationcontroller(countNext,$total_page);
      }
      else{
        var $this = $(this);
        var position = $this.position();
        var width = $this.width();
        var height = $this.height();
        var centerX = ((position.left + width / 2)*100)/$('.coverboardfull ').width()+'%';
        var centerY = (((position.top + height)*100)/$('.coverboardfull ').height())+'%';
        $(this).css({'border-color':'#980000','background':'#FF0000'});
        $(this).css('pointer-events','none');
        $('<img style="left:'+centerX+';top:'+centerY+';position:absolute;height:5%;transform:translate(-4%,-220%);z-index:2;" src="'+imgpath +'incorrect.png" />').insertAfter(this);
        play_correct_incorrect_sound(0);
      }
    });


    switch(countNext){

      case 0:
      play_diy_audio();
      navigationcontroller(countNext,$total_page);
      break;
      case 1:
      sound_player('sound_2');
      navigationcontroller(countNext,$total_page);
      $('.calendar_text').html(month_array[current_calendar]);
      $('.left_slider,.right_slider').attr('src',preload.getResult('btn01').src);

      $('.right_slider').click(function(){
        current_calendar++;
        $('.calendar').attr('src',preload.getResult('calendar_'+(current_calendar+1)).src);
        $('.calendar_text').html(month_array[current_calendar]);
        navigationcontroller(countNext,$total_page);

        if(current_calendar == 11){
          $('.right_slider').css('pointer-events','none');
        }
        else if (current_calendar < 11) {
          $('.right_slider').css('pointer-events','auto');
        }
        if (current_calendar > 0 ) {
          $('.left_slider').css('pointer-events','auto');
        }
      });

      $('.left_slider').click(function(){
        current_calendar--;
        $('.calendar').attr('src',preload.getResult('calendar_'+(current_calendar+1)).src);
        $('.calendar_text').html(month_array[current_calendar]);

        if(current_calendar == 0){
          $('.left_slider').css('pointer-events','none');
        }
        else if (current_calendar > 0 ) {
          $('.left_slider').css('pointer-events','auto');
        }
        if (current_calendar < 11) {
          $('.right_slider').css('pointer-events','auto');
        }
      });

      break;

      case 2:
      sound_player('sound_3');
      the_data_extractor(correct_options_0);
      break;
      case 3:
      the_data_extractor(correct_options_1);
      break;
      case 4:
      the_data_extractor(correct_options_2);
      break;
      case 5:
      the_data_extractor(correct_options_3);
      break;
      case 6:
      the_data_extractor(correct_options_4);
      break;
      case 7:
      the_data_extractor(correct_options_5);
      break;
      case 8:
      the_data_extractor(correct_options_6);
      break;


      default:
      navigationcontroller(countNext,$total_page);
      break;
    }
  }

  //extracts the data according to the selection from arrays
  function the_data_extractor(array_answers){
    console.log(array_answers[current_calendar][0]);
    var class_array=['option_css_1','option_css_2','option_css_3','option_css_4'];
    class_array.shufflearray();
    $('.calendar_right').attr('src',preload.getResult('calendar_'+(current_calendar+1)).src);
    $('.option_1').html(array_answers[current_calendar][0]).addClass(class_array[0]+' correct');
    $('.option_2').html(array_answers[current_calendar][1]).addClass(class_array[1]);
    $('.option_3').html(array_answers[current_calendar][2]).addClass(class_array[2]);
    $('.option_4').html(array_answers[current_calendar][3]).addClass(class_array[3]);
    texthighlight($board);
    $('.question_word').html(month_array[current_calendar]);
  }


  function sound_player(sound_id,navigate) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on('complete', function () {
      navigate?navigationcontroller(countNext,$total_page):"";
    });
  }


  function templateCaller() {
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');
    generaltemplate();
    loadTimelineProgress($total_page, countNext + 1);
  }

  $nextBtn.on('click', function () {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    switch (countNext) {
      default:
      switch (countNext) {
        case 4:
        if(current_calendar==11)
        {
          countNext=countNext+2;
        }
        else{
          countNext++;
        }
        break;
        case 5:
        if(current_calendar==0)
        {
          countNext=countNext+2;
        }
        else{
          countNext++;
        }
        break;
        default:
        countNext++;

      }
      templateCaller();
      break;
    }
  });

  function sound_player(sound_id) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
  }

  $refreshBtn.click(function(){
    templateCaller();
  });

  $prevBtn.on('click', function () {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    countNext--;
    templateCaller();
    /* if footerNotificationHandler pageEndSetNotification was called then on click of
    previous slide button hide the footernotification */
    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
  });

  function texthighlight($highlightinside){
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
    alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
    null ;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag   = "</span>";


    if($alltextpara.length > 0){
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
        use that or else use default 'parsedstring'*/
        $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
        (stylerulename = $(this).attr("data-highlightcustomclass")) :
        (stylerulename = "parsedstring") ;

        texthighlightstarttag = "<span class='"+stylerulename+"'>";


        replaceinstring       = $(this).html();
        replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
        replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


        $(this).html(replaceinstring);
      });
    }
  }
});

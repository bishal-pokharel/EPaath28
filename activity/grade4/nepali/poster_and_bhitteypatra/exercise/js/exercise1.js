Array.prototype.shufflearray = function(){
  var i = this.length, j, temp;
	    while(--i > 0){
	        j = Math.floor(Math.random() * (i+1));
	        temp = this[j];
	        this[j] = this[i];
	        this[i] = temp;
	    }
	    return this;
}

var main_imgpath = $ref+"/images/";
var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref + "/sounds/";

var sound1 = new buzz.sound(soundAsset + "thik_uttarma_click_gara.ogg");
var content=[

	//ex1
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.ex1text1,
        sentdata: data.string.ex1text51,
				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.ex1text5,
					},
					{
						forshuffle: "class2",
						optdata: data.string.ex1text2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.ex1text4,
					},
					{
						forshuffle: "class4",
						optdata: data.string.ex1text3,
					}]

			}
		],
    imageblock:[{
      imgclass:'right_image',
      imgsrc:main_imgpath+'bhatro_patro.png'
    }]
	},
	//ex2
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.ex1text6,
        sentdata: data.string.ex1text51,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.ex1text9,
					},
					{
						forshuffle: "class2",
						optdata: data.string.ex1text7,
					},
					{
						forshuffle: "class3",
						optdata: data.string.ex1text8,
					},
					{
						forshuffle: "class4",
						optdata: data.string.ex1text10,
					}]

			}
		]
	},
	//ex3
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.ex1text11,
        sentdata: data.string.ex1text51,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.ex1text12,
					},
					{
						forshuffle: "class2",
						optdata: data.string.ex1text13,
					},
					{
						forshuffle: "class3",
						optdata: data.string.ex1text14,
					},
					{
						forshuffle: "class4",
						optdata: data.string.ex1text15,
					}]

			}
		]
	},
	//ex4
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
        sentdata: data.string.ex1text51,
				textdata: data.string.ex1text16,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.ex1text17,
					},
					{
						forshuffle: "class2",
						optdata: data.string.ex1text18,
					},
					{
						forshuffle: "class3",
						optdata: data.string.ex1text19,
					},
					{
						forshuffle: "class4",
						optdata: data.string.ex1text20,
					}]

			}
		],
    imageblock:[{
      imgclass:'right_image',
      imgsrc:main_imgpath+'/calendar/calendar_1.png'
    }]
	},
	//ex5
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.ex1text21,
        sentdata: data.string.ex1text51,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.ex1text22,
					},
					{
						forshuffle: "class2",
						optdata: data.string.ex1text23,
					},
					{
						forshuffle: "class3",
						optdata: data.string.ex1text24,
					},
					{
						forshuffle: "class4",
						optdata: data.string.ex1text25,
					}]

			}
		]
	},
	//ex6
  {
    exerciseblock: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
        textdata: data.string.ex1text26,
        sentdata: data.string.ex1text51,

        exeoptions: [
          {
            forshuffle: "class1",
            optdata: data.string.ex1text27,
          },
          {
            forshuffle: "class2",
            optdata: data.string.ex1text28,
          },
          {
            forshuffle: "class3",
            optdata: data.string.ex1text29,
          },
          {
            forshuffle: "class4",
            optdata: data.string.ex1text30,
          }]

      }
    ],
    imageblock:[{
      imgclass:'right_image',
      imgsrc:main_imgpath+'/calendar/chori_la_pani_school.png'
    }]
  },
  //ex7
  {
    exerciseblock: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
        textdata: data.string.ex1text31,
        sentdata: data.string.ex1text51,

        exeoptions: [
          {
            forshuffle: "class1",
            optdata: data.string.ex1text32,
          },
          {
            forshuffle: "class2",
            optdata: data.string.ex1text33,
          },
          {
            forshuffle: "class3",
            optdata: data.string.ex1text34,
          },
          {
            forshuffle: "class4",
            optdata: data.string.ex1text35,
          }]

      }
    ]
  },
  //ex8
  {
    exerciseblock: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
        textdata: data.string.ex1text36,
        sentdata: data.string.ex1text51,

        exeoptions: [
          {
            forshuffle: "class1",
            optdata: data.string.ex1text37,
          },
          {
            forshuffle: "class2",
            optdata: data.string.ex1text38,
          },
          {
            forshuffle: "class3",
            optdata: data.string.ex1text39,
          },
          {
            forshuffle: "class4",
            optdata: data.string.ex1text40,
          }]

      }
    ]
  },
  //ex9
  {
    exerciseblock: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
        textdata: data.string.ex1text41,
        sentdata: data.string.ex1text51,

        exeoptions: [
          {
            forshuffle: "class1",
            optdata: data.string.ex1text42,
          },
          {
            forshuffle: "class2",
            optdata: data.string.ex1text43,
          },
          {
            forshuffle: "class3",
            optdata: data.string.ex1text44,
          },
          {
            forshuffle: "class4",
            optdata: data.string.ex1text45,
          }]

      }
    ]
  },
  //ex10
  {
    exerciseblock: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
        textdata: data.string.ex1text46,
        sentdata: data.string.ex1text51,

        exeoptions: [
          {
            forshuffle: "class1",
            optdata: data.string.ex1text47,
          },
          {
            forshuffle: "class2",
            optdata: data.string.ex1text48,
          },
          {
            forshuffle: "class3",
            optdata: data.string.ex1text49,
          },
          {
            forshuffle: "class4",
            optdata: data.string.ex1text50,
          }]

      }
    ],
    imageblock:[{
      imgclass:'right_image',
      imgsrc:main_imgpath+'/calendar/kabita-partiyogita.png'
    }]
  },
];

/*remove this for non random questions*/
//content.shufflearray();


$(function () {
	var $board    = $('.board');
	var $nextBtn  = $("#activity-page-next-btn-enabled");
	var $prevBtn  = $("#activity-page-prev-btn-enabled");
	var countNext = 0;

	/*for limiting the questions to 10*/
	var $total_page = 10;

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	var score = 0;
	var testin = new LampTemplate();

 	testin.init(10);
	/*values in this array is same as the name of images of eggs in image folder*/
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
    texthighlight($board);

		$nextBtn.hide(0);
		$prevBtn.hide(0);

		/*generate question no at the beginning of question*/
		testin.numberOfQuestions();

    if(countNext==0){
      sound1.play();
    }
		/*for randomizing the options*/
		var parent = $(".optionsdiv");
		var divs = parent.children();
			 while (divs.length) {
			        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			  }

    var alphs = ["क) ", "ख) ", "ग) ", "घ) "];
    var alcount = 0;
    $(".optionsdiv").children().children(".forhover").each(function(){
      $(this).prepend(alphs[alcount]);
      alcount++;
    });

		var ansClicked = false;
		var wrngClicked = false;

		$(".buttonsel").click(function(){
			$(this).removeClass('forhover');
				if(ansClicked == false){

					/*class 1 is always for the right answer. updates scoreboard and disables other click if
					right answer is clicked*/
					if($(this).hasClass("class1")){

						if(wrngClicked == false){
							testin.update(true);
						}
						play_correct_incorrect_sound(1);
						$(this).css("background","#bed62fff");
						$(this).css("border","5px solid #deef3c");
            $(this).css("color","white");
						$(this).siblings(".corctopt").show(0);
						//$('.hint_image').show(0);
						$('.buttonsel').removeClass('forhover forhoverimg');
						ansClicked = true;

						if(countNext != $total_page)
						$nextBtn.show(0);
					}
					else{
						testin.update(false);
						play_correct_incorrect_sound(0);
						$(this).css("background","#FF0000");
						$(this).css("border","5px solid #980000");
						$(this).css("color","white");
						$(this).siblings(".wrngopt").show(0);
						wrngClicked = true;
					}
				}
			});

		/*======= SCOREBOARD SECTION ==============*/
	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();

	});


	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

/*===============================================
  =            data highlight function            =
  ===============================================*/
  function texthighlight($highlightinside){
     //check if $highlightinside is provided
     typeof $highlightinside !== "object" ?
     alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
     null ;

     var $alltextpara = $highlightinside.find("*[data-highlight='true']");
     var stylerulename;
     var replaceinstring;
     var texthighlightstarttag;
     var texthighlightendtag   = "</span>";


     if($alltextpara.length > 0){
       $.each($alltextpara, function(index, val) {
         /*if there is a data-highlightcustomclass attribute defined for the text element
         use that or else use default 'parsedstring'*/
         $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
         (stylerulename = $(this).attr("data-highlightcustomclass")) :
         (stylerulename = "parsedstring") ;

         texthighlightstarttag = "<span class='"+stylerulename+"'>";
         replaceinstring       = $(this).html();
         replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
         replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


         $(this).html(replaceinstring);
       });
     }
   }
   /*=====  End of data highlight function  ======*/

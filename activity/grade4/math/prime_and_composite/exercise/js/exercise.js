var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
//exercise 1
	{
		contentblockadditionalclass:"creambg",
		extratxtdiv:[{
			textclass:"question",
			textdata:data.string.excq1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"background",
				imgid:'bg',
				imgsrc:"",
			}]
		},{
			imgindiv:true,
			qnclddivclass:"qncldImg",
			imagestoshow:[{
				imgclass:"background b1",
				imgid:'cloud2',
				imgsrc:"",
			}]
		}],
		imgcontainer:[{
			img_containerclass:"prmCmp primeContainer",
			textclass:"pmcmName",
			textdata:data.string.prime_num,
			box:[{
				boxclass:"pnCn pn"
			}]
		},{
			img_containerclass:"prmCmp compositeContainer",
			textclass:"pmcmName",
			textdata:data.string.comp_num,
			box:[{
				boxclass:"pnCn cn"
			}]
		}]
	},
//exercise 2
	{
		contentblockadditionalclass:"creambg",
		extratxtdiv:[{
			textclass:"question",
			textdata:data.string.excq1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"background",
				imgid:'bg',
				imgsrc:"",
			}]
		},{
			imgindiv:true,
			qnclddivclass:"qncldImg",
			imagestoshow:[{
				imgclass:"background b1",
				imgid:'cloud2',
				imgsrc:"",
			}]
		}],
		imgcontainer:[{
			img_containerclass:"prmCmp primeContainer",
			textclass:"pmcmName",
			textdata:data.string.prime_num,
			box:[{
				boxclass:"pnCn pn"
			}]
		},{
			img_containerclass:"prmCmp compositeContainer",
			textclass:"pmcmName",
			textdata:data.string.comp_num,
			box:[{
				boxclass:"pnCn cn"
			}]
		}]
	},
//exercise 3
	{
		contentblockadditionalclass:"creambg",
		extratxtdiv:[{
			textclass:"question",
			textdata:data.string.excq1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"background",
				imgid:'bg',
				imgsrc:"",
			}]
		},{
			imgindiv:true,
			qnclddivclass:"qncldImg",
			imagestoshow:[{
				imgclass:"background b1",
				imgid:'cloud2',
				imgsrc:"",
			}]
		}],
		imgcontainer:[{
			img_containerclass:"prmCmp primeContainer",
			textclass:"pmcmName",
			textdata:data.string.prime_num,
			box:[{
				boxclass:"pnCn pn"
			}]
		},{
			img_containerclass:"prmCmp compositeContainer",
			textclass:"pmcmName",
			textdata:data.string.comp_num,
			box:[{
				boxclass:"pnCn cn"
			}]
		}]
	},
//exercise 4
	{
		contentblockadditionalclass:"creambg",
		extratxtdiv:[{
			textclass:"question",
			textdata:data.string.excq1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"background",
				imgid:'bg',
				imgsrc:"",
			}]
		},{
			imgindiv:true,
			qnclddivclass:"qncldImg",
			imagestoshow:[{
				imgclass:"background b1",
				imgid:'cloud2',
				imgsrc:"",
			}]
		}],
		imgcontainer:[{
			img_containerclass:"prmCmp primeContainer",
			textclass:"pmcmName",
			textdata:data.string.prime_num,
			box:[{
				boxclass:"pnCn pn"
			}]
		},{
			img_containerclass:"prmCmp compositeContainer",
			textclass:"pmcmName",
			textdata:data.string.comp_num,
			box:[{
				boxclass:"pnCn cn"
			}]
		}]
	},
//exercise 5
	{
		contentblockadditionalclass:"creambg",
		extratxtdiv:[{
			textclass:"question",
			textdata:data.string.excq1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"background",
				imgid:'bg',
				imgsrc:"",
			}]
		},{
			imgindiv:true,
			qnclddivclass:"qncldImg",
			imagestoshow:[{
				imgclass:"background b1",
				imgid:'cloud2',
				imgsrc:"",
			}]
		}],
		imgcontainer:[{
			img_containerclass:"prmCmp primeContainer",
			textclass:"pmcmName",
			textdata:data.string.prime_num,
			box:[{
				boxclass:"pnCn pn"
			}]
		},{
			img_containerclass:"prmCmp compositeContainer",
			textclass:"pmcmName",
			textdata:data.string.comp_num,
			box:[{
				boxclass:"pnCn cn"
			}]
		}]
	},
//exercise 6
	{
		contentblockadditionalclass:"creambg",
		extratxtdiv:[{
			textclass:"question",
			textdata:data.string.excq1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"background",
				imgid:'bg',
				imgsrc:"",
			}]
		},{
			imgindiv:true,
			qnclddivclass:"qncldImg",
			imagestoshow:[{
				imgclass:"background b1",
				imgid:'cloud2',
				imgsrc:"",
			}]
		}],
		imgcontainer:[{
			img_containerclass:"prmCmp primeContainer",
			textclass:"pmcmName",
			textdata:data.string.prime_num,
			box:[{
				boxclass:"pnCn pn"
			}]
		},{
			img_containerclass:"prmCmp compositeContainer",
			textclass:"pmcmName",
			textdata:data.string.comp_num,
			box:[{
				boxclass:"pnCn cn"
			}]
		}]
	},
//exercise 7
	{
		contentblockadditionalclass:"creambg",
		extratxtdiv:[{
			textclass:"question",
			textdata:data.string.excq1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"background",
				imgid:'bg',
				imgsrc:"",
			}]
		},{
			imgindiv:true,
			qnclddivclass:"qncldImg",
			imagestoshow:[{
				imgclass:"background b1",
				imgid:'cloud2',
				imgsrc:"",
			}]
		}],
		imgcontainer:[{
			img_containerclass:"prmCmp primeContainer",
			textclass:"pmcmName",
			textdata:data.string.prime_num,
			box:[{
				boxclass:"pnCn pn"
			}]
		},{
			img_containerclass:"prmCmp compositeContainer",
			textclass:"pmcmName",
			textdata:data.string.comp_num,
			box:[{
				boxclass:"pnCn cn"
			}]
		}]
	},
//exercise 8
	{
		contentblockadditionalclass:"creambg",
		extratxtdiv:[{
			textclass:"question",
			textdata:data.string.excq1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"background",
				imgid:'bg',
				imgsrc:"",
			}]
		},{
			imgindiv:true,
			qnclddivclass:"qncldImg",
			imagestoshow:[{
				imgclass:"background b1",
				imgid:'cloud2',
				imgsrc:"",
			}]
		}],
		imgcontainer:[{
			img_containerclass:"prmCmp primeContainer",
			textclass:"pmcmName",
			textdata:data.string.prime_num,
			box:[{
				boxclass:"pnCn pn"
			}]
		},{
			img_containerclass:"prmCmp compositeContainer",
			textclass:"pmcmName",
			textdata:data.string.comp_num,
			box:[{
				boxclass:"pnCn cn"
			}]
		}]
	},
//exercise 9
	{
		contentblockadditionalclass:"creambg",
		extratxtdiv:[{
			textclass:"question",
			textdata:data.string.excq1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"background",
				imgid:'bg',
				imgsrc:"",
			}]
		},{
			imgindiv:true,
			qnclddivclass:"qncldImg",
			imagestoshow:[{
				imgclass:"background b1",
				imgid:'cloud2',
				imgsrc:"",
			}]
		}],
		imgcontainer:[{
			img_containerclass:"prmCmp primeContainer",
			textclass:"pmcmName",
			textdata:data.string.prime_num,
			box:[{
				boxclass:"pnCn pn"
			}]
		},{
			img_containerclass:"prmCmp compositeContainer",
			textclass:"pmcmName",
			textdata:data.string.comp_num,
			box:[{
				boxclass:"pnCn cn"
			}]
		}]
	},
//exercise 10
	{
		contentblockadditionalclass:"creambg",
		extratxtdiv:[{
			textclass:"question",
			textdata:data.string.excq1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"background",
				imgid:'bg',
				imgsrc:"",
			}]
		},{
			imgindiv:true,
			qnclddivclass:"qncldImg",
			imagestoshow:[{
				imgclass:"background b1",
				imgid:'cloud2',
				imgsrc:"",
			}]
		}],
		imgcontainer:[{
			img_containerclass:"prmCmp primeContainer",
			textclass:"pmcmName",
			textdata:data.string.prime_num,
			box:[{
				boxclass:"pnCn pn"
			}]
		},{
			img_containerclass:"prmCmp compositeContainer",
			textclass:"pmcmName",
			textdata:data.string.comp_num,
			box:[{
				boxclass:"pnCn cn"
			}]
		}]
	},
];

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var item_txt ='';
	var countNext = 0;
	var test = false;
	var qnArray=[],nwQnArr=[], prmAnsArr=[], cmpAnsArr=[], cldPrmCnt = 0, cldCmpCnt = 0, nwqnAsgnd = false, ofc = false;
	/*for limiting the questions to 10*/
	var $total_page = 10;
	var current_sound;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images		],
			{id: "basket", src: imgpath+"basket.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg", src: imgpath+"bg.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud2", src: imgpath+"cloud2.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
            {id: "sound_1", src: soundAsset + "ex_p1_title.ogg"},

        ];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		//scoring.init(10);
		templateCaller();
	}
	//initialize
	init();

	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller() : "";
        });
    }
	/*=====  End of data highlight function  ======*/
	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
 	}

	 /*values in this array is same as the name of images of eggs in image folder*/

	 //create eggs
	 var testin = new EggTemplate();

	 	//eggTemplate.eggMove(countNext);
 		testin.init(10);




	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);
		texthighlight($board);
		put_image(content, countNext);
		var updateScore = 0;
		// put_image_sec(content, countNext);
	 	var testcount = 0;

	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);

	 	/*generate question no at the beginning of question*/
	 	testin.numberOfQuestions();

	 	/*for randomizing the options*/
		function randomize(parent){
			var parent = $(parent);
			var divs = parent.children();
			while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}
}
	 	/*======= SCOREBOARD SECTION ==============*/
	 	/*random scoreboard eggs*/
	 	//var i = Math.floor(Math.random() * imageArray.length);
	 	//var randImg = imageArray[i];
	 	var ansClicked = false;
	 	var wrngClicked = false;
		var updateScore = 0;
		var appendCount = 0;
		$(".question").prepend(countNext+1+". ");

		$(".primeContainer, .compositeContainer").append("<img class='bskts' src='"+preload.getResult("basket").src+"'/>");
		for(var i=0; i<=4; i++){
			$(".pn").append("<div class='cldDiv cld_prm_"+i+"'></div>");
			$(".cld_prm_"+i).append("<img class='background' src='"+preload.getResult("cloud2").src+"'>");
		}
		for(var i=0; i<=4; i++){
			$(".cn").append("<div class='cldDiv cld_cmp_"+i+"'></div>");
			$(".cld_cmp_"+i).append("<img class='background' src='"+preload.getResult("cloud2").src+"'>");
		}

       countNext == 0 ?sound_player("sound_1",false):"";
	 	switch(countNext){
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
	 		case 9:
				!ofc?onceCallFn():"";
				ofc = true;
				console.log(qnArray);
				console.log(nwQnArr);


			var countPrm = 0;
			var  countCmp = 0;
			for(var i=0;i<=nwQnArr.length;i++){
				var isPrm = isPrimeNumber(qnArray[i]);
				if(isPrm){
					$(".cld_prm_"+countPrm).append("<p class='num'>"+qnArray[i]+"</p>");
					countPrm+=1;
				}else
				{
					$(".cld_cmp_"+countCmp).append("<p class='num'>"+qnArray[i]+"</p>");
					countCmp+=1;
				}
			}

			$(".qncldImg").append("<p class='num'>"+qnArray[countNext]+"</p>");
			var isPrime = isPrimeNumber(qnArray[countNext]);

			if(isPrime){
				$(".primeContainer").addClass("correct");
			}else{
				$(".compositeContainer").addClass("correct");
			}

			$(".cldDiv").hide(0);

			$(".prmCmp").click(function(){

				if($(this).hasClass("correct")){
					current_sound.stop();
					play_correct_incorrect_sound(1);
					$(".prmCmp").css("pointer-events",'none');
					if(isPrime){
						$(".qncldImg").animate({
							top:"85%",
							left:"21%"
						},1000,function(){
							$(".qncldImg").hide(0);
							$(".cld_prm_"+cldPrmCnt).show(0);
							prmAnsArr.push(cldPrmCnt);
							cldPrmCnt +=1;
							$nextBtn.show();
						});
					}else{
						$(".qncldImg").animate({
							top:"85%",
							left:"79%"
						},1000,function(){
							$(".qncldImg").hide(0);
							$(".cld_cmp_"+cldCmpCnt).show(0);
							cmpAnsArr.push(cldCmpCnt);
							cldCmpCnt +=1;
							$nextBtn.show();
						});
					}
					!wrngClicked?testin.update(true):testin.update(false);
                    $(this).append("<img class='wrongright' src='images/right.png'/>");


                }else{
                    $(this).append("<img class='wrongright' src='images/wrong.png'/>");
                    $(this).css("pointer-events",'none');
					wrngClicked  = true;
					testin.update(false);
                    current_sound.stop();
                    play_correct_incorrect_sound(0);
				}
			});
			if(countNext>0){
				for(var i= 0;i<prmAnsArr.length; i++){
					// $(".cld_prm_"+prmAnsArr[i]).show(0);
					$(".cld_prm_"+prmAnsArr[i]).show(0);

				}
				for(var i= 0;i<cmpAnsArr.length; i++){
					// $(".cld_cmp_"+cmpAnsArr[i]).show(0);
					$(".cld_cmp_"+cmpAnsArr[i]).show(0);

				}
			}
			// qnArray.splice(0,1);

		break;
	 	}


		function onceCallFn(){
			// for obtaining 10 prime and composiye numbers and adding them to an array
			// which is then shuffled to give have 10 random prime and composite numbers
			// alert("here");
			var prmArray = getPrimes(50);newarray1
			var cmpArray = getComposite(50);
			var newarray1 = [];
			var newarray2 = [];
			for(var j=0; j<=4;j++){
				newarray1.push(prmArray[j]);
			}
			for(var j=0; j<=4;j++){
				newarray1.push(cmpArray[j]);
			}
			newarray1.shufflearray();
			qnArray = newarray1;
			nwQnArr = newarray1;
			// 10 prime and composite numbers obtained
		}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			// alert(content[count].imageblock.length);
			for(var j=0; j<content[count].imageblock.length; j++){
				var imageblock = content[count].imageblock[j];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						// alert(image_src);
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						// console.log(selector);
						$(selector).attr('src',image_src);
					}
				}
			}
		}
	}

	function put_image_sec(content, count){
		if(content[count].arrow_nav[0].hasOwnProperty('imageblock')){
			var imagblockVal = content[count].arrow_nav[0];
			for(var j=0;j<imagblockVal.imageblock.length; j++){
							var imageblock = imagblockVal.imageblock[j];
							if(imageblock.hasOwnProperty('imagestoshow')){
								var imageClass = imageblock.imagestoshow;
								for(var i=0; i<imageClass.length; i++){
									var image_src = preload.getResult(imageClass[i].imgid).src;
									//get list of classes
									var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
									var selector = ('.'+classes_list[classes_list.length-1]);
									$(selector).attr('src', image_src);
								}
							}
    }

		}
	}
	 	/*======= SCOREBOARD SECTION ==============*/
	 }


	 function getPrimes(max) {
		 var sieve = [], i, j, primes = [];
		 for (i = 2; i <= max; ++i) {
				 if (!sieve[i]) {
						 // i has not been marked -- it is prime
						 primes.push(i);
						 for (j = i << 1; j <= max; j += i) {
								 sieve[j] = true;
						 }
				 }
		 }
		 return primes;
	 }
	 function getComposite(max) {
		 var sieve = [], i, j, comps = [];
		 for (i = 2; i <= max; ++i) {
				 if (!sieve[i]) {
						 // i has not been marked -- it is prime
						 for (j = i << 1; j <= max; j += i) {
								 sieve[j] = true;
						 }
				 }else{
					 comps.push(i);
				 }
		 }
		 return comps;
	 }
	 function isPrimeNumber(n) {
		 for (var i = 2; i < n; i++) { // i will always be less than the parameter so the condition below will never allow parameter to be divisible by itself ex. (7 % 7 = 0) which would return true
			 if(n % i === 0) return false; // when parameter is divisible by i, it's not a prime number so return false
		 }
		 return n > 1; // otherwise it's a prime number so return true (it also must be greater than 1, reason for the n > 1 instead of true)
	 }

	 function templateCaller(){
		/*always hide next and previousloadimage navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();


		//call the slide indication bar handler for pink indicators


	}

	// first call to template caller
	// templateCaller(); loadimage($(".myimg3").find("img"),$(".myimg3").find("p"),preload.getResult('ex1typ3qn'+quesNo).src,eval('data.string.ex2typ1op'+quesNo));


	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
 	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	function loadimage(imgrep,imgtext,imgsrc,quesNo){
		imgrep.attr("src",imgsrc);
		imgtext.text(quesNo);
	}

		/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
		 * event.key reurns the value of key pressed by user and it is converted to integer
		 * event.target gets the element where event is occuring (usually a div)
		 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
		 * input_class and button_classes should be something like '.class_name'
		 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
		 */
		function input_box(input_class, max_number, button_class) {
			$(input_class).keydown(function(event) {
				var charCode = (event.which) ? event.which : event.keyCode;
				/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
				if (charCode === 13 && button_class != null) {
					$(button_class).trigger("click");
				}
				var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
				//check if user inputs del, backspace or arrow keys
				if (!condition) {
					return true;
				}
				//check if user inputs more than one '.'
				if ((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
					return false;
				}
				//check . and 0-9 separately after checking arrow and other keys
				if ((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110) {
					return false;
				}
				//check max no of allowed digits
				if (String(event.target.value).length >= max_number) {
					return false;
				}
				return true;
			});
		}
/*=====  End of Templates Controller Block  ======*/
});

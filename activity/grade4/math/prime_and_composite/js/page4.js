var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	{
		//slide 0
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description2",
			textdata: data.string.p4_s101
		}],
		imageblock:[
		  {
		   imagestoshow:[
		       {
		         imgclass:"sundar",
		         imgsrc: imgpath+"teaching02.png"
		      }
		    ]
		  }
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",	
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s1					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s11
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s21
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s31
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s41
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s51				
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s61
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s71
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s81
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s91
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s2					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s12
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s22
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s32
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s42
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s52					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s62
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s72
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s82
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s92
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s3					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s13
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s23
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s33
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s43
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s53					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s63
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s73
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s83
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s93
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s4					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s14
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s24
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s34
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s44
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s54					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s64
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s74
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s84
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s94
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s5					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s15
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s25
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s35
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s45
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s55					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s65
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s75
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s85
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s95
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s6					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s16
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s26
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s36
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s46
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s56					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s66
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s76
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s86
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s96
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s7					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s17
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s27
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s37
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s47
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s57					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s67
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s77
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s87
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s97
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s8					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s18
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s28
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s38
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s48
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s58					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s68
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s78
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s88
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s98
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s9					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s19
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s29
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s39
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s49
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s59					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s69
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s79
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s89
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s99
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s10					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s20
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s30
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s40
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s50
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s60					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s70
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s80
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s90
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s100
						}
						]
					}
					
				]
			}
		]
	},{
		//slide 1
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description2",
			textdata: data.string.p4_s101
		}],
		imageblock:[
		  {
		   imagestoshow:[
		       {
		         imgclass:"sundar",
		         imgsrc: imgpath+"teaching01.png"
		      },
		      {
		      	imgclass:"worm",
		         imgsrc: imgpath+"mr.-know-it-all-transparent.gif"
		      }
		    ]
		  }
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",	
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal _1_notprime",
							textdata: data.string.p4_s1					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s11
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s21
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s31
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s41
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s51				
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s61
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s71
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s81
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s91
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s2					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s12
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s22
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s32
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s42
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s52					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s62
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s72
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s82
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s92
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s3					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s13
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s23
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s33
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s43
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s53					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s63
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s73
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s83
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s93
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s4					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s14
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s24
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s34
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s44
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s54					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s64
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s74
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s84
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s94
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s5					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s15
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s25
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s35
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s45
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s55					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s65
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s75
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s85
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s95
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s6					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s16
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s26
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s36
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s46
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s56					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s66
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s76
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s86
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s96
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s7					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s17
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s27
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s37
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s47
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s57					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s67
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s77
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s87
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s97
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s8					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s18
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s28
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s38
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s48
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s58					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s68
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s78
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s88
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s98
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s9					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s19
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s29
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s39
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s49
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s59					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s69
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s79
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s89
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s99
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s10					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s20
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s30
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s40
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s50
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s60					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s70
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s80
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s90
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s100
						}
						]
					}
					
				]
			}
		],
		lowertextblockadditionalclass :"yellowlowertextblock",
		lowertextblock:[{
			textclass: "description_center",
			datahighlightflag: true,
			datahighlightcustomclass: "customhighlightclass",
			textdata: data.string.p4_s102
		}]
	},{
		//slide 2
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description2",
			textdata: data.string.p4_s101
		}],
		imageblock:[
		  {
		   imagestoshow:[
		       {
		         imgclass:"sundar",
		         imgsrc: imgpath+"teaching01.png"
		      },
		      {
		      	imgclass:"worm",
		         imgsrc: imgpath+"mr.-know-it-all-transparent.gif"
		      }
		    ]
		  }
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",	
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal _1_notprime",
							textdata: data.string.p4_s1					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s11
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s21
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s31
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s41
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s51				
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s61
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s71
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s81
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s91
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s2					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s12
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s22
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s32
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s42
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s52					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s62
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s72
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s82
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s92
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s3					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s13
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s23
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s33
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s43
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s53					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s63
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s73
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s83
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s93
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s4					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s14
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s24
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s34
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s44
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s54					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s64
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s74
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s84
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s94
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s5					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s15
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s25
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s35
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s45
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s55					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s65
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s75
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s85
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s95
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s6					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s16
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s26
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s36
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s46
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s56					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s66
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s76
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s86
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s96
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s7					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s17
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s27
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s37
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s47
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s57					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s67
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s77
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s87
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s97
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s8					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s18
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s28
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s38
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s48
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s58					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s68
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s78
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s88
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s98
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s9					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s19
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s29
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s39
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s49
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s59					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s69
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s79
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s89
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s99
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s10					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s20
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s30
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s40
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s50
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s60					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s70
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s80
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s90
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s100
						}
						]
					}
					
				]
			}
		],
		lowertextblockadditionalclass :"yellowlowertextblock",
		lowertextblock:[{
			textclass: "description_center",
			datahighlightflag: true,
			datahighlightcustomclass: "customgreenhighlight",
			textdata: data.string.p4_s103
		}]
	},{
		//slide 3
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description2",
			textdata: data.string.p4_s101
		}],
		imageblock:[
		  {
		   imagestoshow:[
		       {
		         imgclass:"sundar",
		         imgsrc: imgpath+"teaching01.png"
		      },
		      {
		      	imgclass:"worm",
		         imgsrc: imgpath+"mr.-know-it-all-transparent.gif"
		      }
		    ]
		  }
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",	
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal _1_notprime",
							textdata: data.string.p4_s1					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s11
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s21
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s31
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s41
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s51				
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s61
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s71
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s81
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s91
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s2					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s12
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s22
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s32
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s42
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s52					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s62
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s72
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s82
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s92
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s3					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s13
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s23
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s33
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s43
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s53					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s63
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s73
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s83
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s93
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s4					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s14
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s24
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s34
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s44
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s54					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s64
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s74
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s84
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s94
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s5					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s15
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s25
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s35
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s45
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s55					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s65
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s75
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s85
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s95
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s6					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s16
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s26
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s36
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s46
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s56					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s66
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s76
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s86
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s96
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s7					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s17
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s27
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s37
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s47
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s57					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s67
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s77
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s87
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s97
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s8					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s18
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s28
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s38
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s48
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s58					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s68
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s78
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s88
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s98
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s9					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s19
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s29
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s39
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s49
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s59					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s69
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s79
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s89
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s99
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s10					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s20
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s30
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s40
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s50
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s60					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s70
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s80
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s90
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s100
						}
						]
					}
					
				]
			}
		],
		lowertextblockadditionalclass :"yellowlowertextblock",
		lowertextblock:[{
			textclass: "description_center",
			datahighlightflag: true,
			datahighlightcustomclass: "customgreenhighlight",
			textdata: data.string.p4_s104
		}]
	},{
		//slide 4
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description2",
			textdata: data.string.p4_s101
		}],
		imageblock:[
		  {
		   imagestoshow:[
		       {
		         imgclass:"sundar",
		         imgsrc: imgpath+"teaching01.png"
		      },
		      {
		      	imgclass:"worm",
		         imgsrc: imgpath+"mr.-know-it-all-transparent.gif"
		      }
		    ]
		  }
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",	
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal _1_notprime",
							textdata: data.string.p4_s1					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s11
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s21
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s31
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s41
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s51				
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s61
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s71
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s81
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s91
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s2					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s12
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s22
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s32
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s42
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s52					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s62
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s72
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s82
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s92
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s3					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s13
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s23
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s33
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s43
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s53					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s63
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s73
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s83
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s93
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s4					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s14
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s24
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s34
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s44
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s54					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s64
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s74
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s84
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s94
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s5					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s15
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s25
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s35
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s45
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s55					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s65
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s75
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s85
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s95
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s6					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s16
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s26
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s36
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s46
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s56					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s66
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s76
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s86
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s96
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s7					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s17
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s27
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s37
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s47
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s57					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s67
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s77
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s87
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s97
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s8					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s18
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s28
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s38
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s48
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s58					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s68
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s78
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s88
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s98
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s9					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s19
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s29
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s39
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s49
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s59					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s69
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s79
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s89
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s99
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s10					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s20
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s30
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s40
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s50
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s60					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s70
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s80
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s90
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s100
						}
						]
					}
					
				]
			}
		],
		lowertextblockadditionalclass :"yellowlowertextblock",
		lowertextblock:[{
			textclass: "description_center",
			datahighlightflag: true,
			datahighlightcustomclass: "customgreenhighlight",
			textdata: data.string.p4_s105
		}]
	},{
		//slide 5
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description2",
			textdata: data.string.p4_s101
		}],
		imageblock:[
		  {
		   imagestoshow:[
		       {
		         imgclass:"sundar",
		         imgsrc: imgpath+"teaching01.png"
		      },
		      {
		      	imgclass:"worm",
		         imgsrc: imgpath+"mr.-know-it-all-transparent.gif"
		      }
		    ]
		  }
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",	
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal _1_notprime",
							textdata: data.string.p4_s1					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s11
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s21
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s31
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s41
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s51				
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s61
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s71
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s81
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s91
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s2					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s12
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s22
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s32
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s42
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s52					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s62
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s72
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s82
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s92
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s3					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s13
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s23
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s33
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s43
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s53					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s63
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s73
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s83
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s93
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s4					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s14
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s24
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s34
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s44
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s54					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s64
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s74
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s84
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s94
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s5					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s15
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s25
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s35
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s45
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s55					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s65
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s75
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s85
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s95
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s6					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s16
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s26
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s36
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s46
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s56					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s66
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s76
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s86
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s96
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s7					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s17
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s27
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s37
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s47
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s57					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s67
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s77
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s87
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s97
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s8					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s18
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s28
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s38
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s48
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s58					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s68
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s78
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s88
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s98
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s9					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s19
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s29
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s39
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s49
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s59					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s69
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s79
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s89
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s99
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s10					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s20
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s30
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s40
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s50
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s60					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s70
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s80
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s90
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s100
						}
						]
					}
					
				]
			}
		],
		lowertextblockadditionalclass :"yellowlowertextblock",
		lowertextblock:[{
			textclass: "description_center",
			datahighlightflag: true,
			datahighlightcustomclass: "customgreenhighlight",
			textdata: data.string.p4_s106
		}]
	},{
		//slide 6
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description2",
			textdata: data.string.p4_s111
		}],
		imageblock:[
		  {
		   imagestoshow:[
		       {
		         imgclass:"sundar",
		         imgsrc: imgpath+"teaching01.png"
		      },
		      {
		      	imgclass:"worm",
		         imgsrc: imgpath+"mr.-know-it-all-transparent.gif"
		      }
		    ]
		  }
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",	
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal _1_notprime",
							textdata: data.string.p4_s1					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s11
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s21
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s31
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s41
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s51				
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s61
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s71
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s81
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s91
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s2					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s12
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s22
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s32
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s42
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s52					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s62
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s72
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s82
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s92
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s3					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s13
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s23
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s33
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s43
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s53					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s63
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s73
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s83
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s93
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s4					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s14
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s24
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s34
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s44
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s54					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s64
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s74
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s84
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s94
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s5					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s15
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s25
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s35
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s45
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s55					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s65
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s75
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s85
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s95
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s6					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s16
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s26
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s36
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s46
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s56					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s66
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s76
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s86
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s96
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s7					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s17
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s27
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s37
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s47
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s57					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s67
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s77
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s87
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s97
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s8					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s18
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s28
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s38
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s48
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s58					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s68
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s78
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s88
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s98
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s9					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s19
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s29
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s39
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s49
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s59					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s69
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s79
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s89
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s99
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s10					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s20
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s30
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s40
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s50
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s60					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s70
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s80
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s90
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s100
						}
						]
					}
					
				]
			}
		],
		lowertextblockadditionalclass :"yellowlowertextblock",
		lowertextblock:[{
			textclass: "description_center",
			datahighlightflag: true,
			datahighlightcustomclass: "customgreenhighlight",
			textdata: data.string.p4_s107
		}]
	},{
		//slide 7
		contentnocenteradjust: true,
		imageblock:[
		  {
		   imagestoshow:[
		      {
		      	imgclass:"worm",
		         imgsrc: imgpath+"mr.-know-it-all-transparent.gif"
		      }
		      ]
		  }
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",	
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal _1_notprime",
							textdata: data.string.p4_s1					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s11
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s21
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s31
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s41
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s51				
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s61
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s71
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s81
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s91
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s2					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s12
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s22
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s32
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s42
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s52					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s62
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s72
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s82
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s92
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s3					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s13
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s23
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s33
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s43
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s53					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s63
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s73
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s83
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s93
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s4					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s14
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s24
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s34
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s44
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s54					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s64
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s74
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s84
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s94
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s5					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s15
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s25
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s35
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s45
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s55					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s65
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s75
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s85
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s95
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s6					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s16
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s26
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s36
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s46
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s56					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s66
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s76
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s86
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s96
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s7					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s17
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s27
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s37
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s47
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s57					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s67
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s77
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s87
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s97
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s8					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s18
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s28
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s38
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s48
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s58					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s68
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s78
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s88
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s98
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s9					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s19
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s29
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s39
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s49
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s59					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s69
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s79
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s89
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s99
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s10					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s20
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s30
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s40
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s50
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s60					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s70
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s80
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s90
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s100
						}
						]
					}
					
				]
			}
		],
		lowertextblockadditionalclass :"yellowlowertextblock1",
		lowertextblock:[{
			textclass: "description_center",
			datahighlightflag: true,
			datahighlightcustomclass: "customhighlightclass",
			textdata: data.string.p4_s108
		}],
		
		inputtypes:true,
		primevalue: data.string.p4_s109,
		compositevalue: data.string.p4_s110
	},{
		//slide 8
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description2",
			textdata: data.string.p4_s111
		}], imagestoshow:[
		      {
		      	imgclass:"worm",
		         imgsrc: imgpath+"mr.-know-it-all-transparent.gif"
		      }
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",	
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal _1_notprime",
							textdata: data.string.p4_s1					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s11
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s21
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s31
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s41
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s51				
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s61
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s71
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s81
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s91
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s2					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s12
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s22
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s32
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s42
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s52					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s62
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s72
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s82
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s92
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s3					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s13
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s23
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s33
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s43
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s53					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s63
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s73
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s83
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s93
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s4					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s14
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s24
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s34
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s44
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s54					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s64
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s74
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s84
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s94
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s5					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s15
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s25
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s35
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s45
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s55					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s65
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s75
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s85
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s95
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s6					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s16
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s26
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s36
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s46
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s56					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s66
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s76
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s86
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s96
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s7					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s17
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s27
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s37
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s47
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s57					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s67
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s77
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s87
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s97
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s8					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s18
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s28
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s38
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s48
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s58					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s68
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s78
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s88
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s98
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s9					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s19
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s29
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s39
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s49
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s59					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s69
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s79
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s89
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s99
						}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s10					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s20
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s30
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s40
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s50
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s60					
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s70
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s80
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s90
						},{
							flexboxrowclass :"rownormal",
							textdata: data.string.p4_s100
						}
						]
					}
					
				]
			},{
				flexblockadditionalclass: 'primeDroppable',
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rowheaderdropable",
							textdata: data.string.p4_s113					
						},{
							flexboxrowclass :"rownormaldropable",
							textdata: ""
						}
						]
					}]
			},{
				flexblockadditionalclass: 'compositeDroppable',
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rowheaderdropable",
							textdata: data.string.p4_s114					
						},{
							flexboxrowclass :"rownormaldropable",
							textdata: ""
						}
						]
					}]
			}
		],
		lowertextblockadditionalclass :"yellowlowertextblock",
		lowertextblock:[{
			textclass: "description_center",
			datahighlightflag: true,
			datahighlightcustomclass: "customhighlightclass",
			textdata: data.string.p4_s112
		}],
		
		inputtypes:true,
		playagainvalue: data.string.p4_s115
	},{
		//slide 9
		uppertextblockadditionalclass: "blackboard3",
		duster : true,
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description",
			datahighlightflag: true,
			datahighlightcustomclass: "custompinkhighlight",
			textdata: data.string.p4_s116
		},{
			textclass: "description2",
			textdata: data.string.p4_s118
		}],
		
		lowertextblockadditionalclass: "blackboard4",
		duster : true,
		lowertextblock:[{
			textclass: "description",
			datahighlightflag: true,
			datahighlightcustomclass: "customgreenhighlight",
			textdata: data.string.p4_s117
		},{
			textclass: "description2",
			textdata: data.string.p4_s118
		}],
		
		inputtypes:true,
		playagainvalue: data.string.p4_s115,
		additionalplayagainclass: "animateplayagain"
	}
];


$(function () { 
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;
 
  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);
	
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
    var current_sound;
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            // sounds
            {id: "sound_1", src: soundAsset + "s4_p1.ogg"},
            {id: "sound_2", src: soundAsset + "s4_p2.ogg"},
            {id: "sound_3", src: soundAsset + "s4_p3.ogg"},
            {id: "sound_4", src: soundAsset + "s4_p4.ogg"},
            {id: "sound_5", src: soundAsset + "s4_p5.ogg"},
            {id: "sound_6", src: soundAsset + "s4_p6.ogg"},
            {id: "sound_7", src: soundAsset + "s4_p7.ogg"},
            {id: "sound_8", src: soundAsset + "s4_p8.ogg"},
            {id: "sound_9", src: soundAsset + "s4_p9.ogg"},
            {id: "sound_10", src: soundAsset + "s4_p10.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();
    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller() : "";
        });
    }
/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true 
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class
        
        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;
          
        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) { 
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/       
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
    /*=====  End of data highlight function  ======*/

	 /*===== This function splits the string in data into convential fraction used in mathematics =====*/
    function splitintofractions($splitinside){
   		typeof $splitinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;
        
        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
       	if($splitintofractions.length > 0){
       		$.each($splitintofractions, function(index, value){
	        	$this = $(this);
	        	var tobesplitfraction = $this.html();
	        	if($this.hasClass('fraction')){
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
	        	}else{
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
	        	}
				
				
				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');	        	
	        	$this.html(tobesplitfraction);
	        });	
       	}
   	}
	/*===== split into fractions end =====*/

    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**   
      How to:
      - First set any html element with 
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification    
     */
    
    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which 
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/ 
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/    
   /**   
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that 
      footernotification is called for continue button to navigate to exercise
    */
  
  /**   
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if 
        so should be taken out from the templateCaller function
      - If the total page number is 
     */  
   
  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			 ole.footerNotificationHandler.pageEndSetNotification();
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**   
      How to:
      - Just call instructionblockcontroller() from the template    
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which 
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);          
        });
      }
    } 
  /*=====  End of InstructionBlockController  ======*/
  
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/   
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);
	        
	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
		splitintofractions($board);
		vocabcontroller.findwords(countNext);
      countNext!=8?sound_player("sound_"+(countNext+1),true):sound_player("sound_"+(countNext+1),false);
      switch(countNext){
	   		case 2:
	   			highlightspecificgroups(2, "purple_primefocus");
	   			break;
   			case 3:
	   			highlightspecificgroups(2, "purple_prime");
	   			highlightspecificgroups(3, "purple_primefocus");
	   			break;
	   		case 4:
	   			highlightspecificgroups(2, "purple_prime");
	   			highlightspecificgroups(3, "purple_prime");
	   			highlightspecificgroups(5, "purple_primefocus");
	   			break;
	   		case 5:
	   			$nextBtn.show(0);
	   			highlightspecificgroups(2, "purple_prime");
	   			highlightspecificgroups(3, "purple_prime");
	   			highlightspecificgroups(5, "purple_prime");
	   			highlightspecificgroups(7, "purple_primefocus");
	   			break;
	   		case 6:
	   			highlightspecificgroups(2, "purple_prime2");
	   			highlightspecificgroups(3, "purple_prime2");
	   			highlightspecificgroups(5, "purple_prime2");
	   			highlightspecificgroups(7, "purple_prime2");
	   			break;
	   		case 7:
	   			$(".flexcontainerblock100").css("top","19%");
	   			highlightspecificgroups(2, "purple_primefocus");
	   			highlightspecificgroups(3, "purple_primefocus");
	   			highlightspecificgroups(5, "purple_primefocus");
	   			highlightspecificgroups(7, "purple_primefocus");
	   			var primeshowing = false;
	   			
   				$('#composite').hover(function(){
   					$(this).css({
   						"background-color": "#FF007D",
   						"border": "0.1em solid #E8B22C"
   					});
	   				if(primeshowing){
	   					primeshowing = false;
	   					var $rownormal = $('.rownormal');
						$.each($rownormal, function(index, val) {
							$(this).removeClass('green_primefocus');
						});
						highlightspecificgroups(2, "purple_primefocus");
		   				highlightspecificgroups(3, "purple_primefocus");
		   				highlightspecificgroups(5, "purple_primefocus");
		   				highlightspecificgroups(7, "purple_primefocus");	   					
	   				}
	   			}, function(){
	   				$(this).css({
   						"background-color": "#FC0070",
   						"border": "initial"
   					});
	   			});
	   			
	   			$('#prime').hover(function(){
	   				$(this).css({
   						"background-color": "#00B7FF",
   						"border": "0.1em solid #E8B22C"
   					});
	   				if(!primeshowing){
	   					var $rownormal = $('.rownormal');
						$.each($rownormal, function(index, val) {
							$(this).removeClass('purple_primefocus');
						});
	   					highlightprimenumbers();
	   					primeshowing = true;
	   					$nextBtn.delay(500).show(0);
	   				}
	   			}, function(){
	   				$(this).css({
   						"background-color": "#0097DE",
   						"border": "initial"
   					});
	   			});
	   			break;
	   		case 8:
	   			generatforurandomprimenumbers();
	   			generatforurandomcompositenumbers();
	   			generateprimecompositset();
	   			setDroppables();
	   			break;
	   		case 9:
	   			var $blackboard = $(".blackboard").hide(0);
	   			var $blackboard2 = $(".blackboard2").hide(0);
	   			$('#playagain').show(0).click(function(){
					countNext = 8;
					ole.footerNotificationHandler.hideNotification();
					templateCaller();
				});
				
				setTimeout(function() {
					$blackboard.show(0);
					$blackboard2.show(0);
	
					var $primeexample = $(".blackboard3 > .description2");
					var $compositeexample = $(".blackboard4 > .description2");
					var numberinindex = 0;
					var tens = 0;
					var ones = 0;
					for (var i = 0; i < primecompositeindexset.length; i++) {
						numberinindex = primecompositeindexset[i].numberindex;
						ones = Math.floor(numberinindex / 10);
						tens = numberinindex % 10;
						if (primecompositeindexset[i].type == 'prime') {
							$primeexample.append(" " + ((ones + 1)+ 10 * tens) + ",");
						} else {
							$compositeexample.append(" " + ((ones + 1)+ 10 * tens) + ",");
						}
					}
	
					var htmlcontent = $primeexample.html();
					var index = htmlcontent.lastIndexOf(',');
					$primeexample.html(htmlcontent.substr(0, index) + ".");
					htmlcontent = $compositeexample.html();
					index = htmlcontent.lastIndexOf(',');
					$compositeexample.html(htmlcontent.substr(0, index) + ".");
	
					ole.footerNotificationHandler.pageEndSetNotification();
				}, 1000); 

	   		    break;
	   		
	   		default:
	   			break;
	    }
  }

/*=====  End of Templates Block  ======*/

	function highlightspecificgroups(primenumber, highlightclass){
		var $rownormal = $('.rownormal');
		var $rownormalp = $('.rownormal> p');
		var value;
		var count = 0;
		$.each($rownormalp, function(index, val) {
			value = parseInt(val.innerHTML);
			
			if((value%primenumber)==0 && value != primenumber){
				count++;
				$($rownormal[index]).addClass(highlightclass);
				console.log(value, primenumber);	
			}
			
		});
		
		console.log("cpunt for prime number "+primenumber, count);
		
	}
	
	var indexof4randomprime = [];
	function generatforurandomprimenumbers(){
		var randomnogenerate;
		var dataalreadycontained = false;
		var count = 0;
		indexof4randomprime = [];
		while(indexof4randomprime.length < 4 && count < 50){
			count++;
			randomnogenerate = arrayofindexofprimenumbers[Math.floor(Math.random()* (arrayofindexofprimenumbers.length-1))];
			if(randomnogenerate == undefined){
				console.log("undef");
			}

			if(indexof4randomprime.indexOf(randomnogenerate) > -1) continue;
			indexof4randomprime[indexof4randomprime.length] = randomnogenerate;
		}
		
		var arrayofprime = [ 1, 25, 68, 83];
		if(indexof4randomprime.length < 4){
			index = 0;
			while(indexof4randomprime.length < 4){
				if(indexof4randomprime.indexOf(arrayofprime[index]) > -1) continue;
				indexof4randomprime[indexof4randomprime.length] = arrayofprime[index];
			}
		}
		
	}
	
	var indexof4randomcomposite = [];
	function generatforurandomcompositenumbers(){
		var randomnogenerate;
		var dataalreadycontained = false;
		indexof4randomcomposite = [];
		while(indexof4randomcomposite.length < 4 ){
			randomnogenerate = arrayofindexofcompositenumbers[Math.floor(Math.random()* (arrayofindexofcompositenumbers.length-1))];
			if(randomnogenerate == undefined){
				console.log("undef");
			}
			if(indexof4randomcomposite.indexOf(randomnogenerate) > -1) continue;
			indexof4randomcomposite[indexof4randomcomposite.length] = randomnogenerate;
		}
		
		var arrayofprime = [99, 17, 38, 58];
		if(indexof4randomprime.length < 4){
			index = 0;
			while(indexof4randomprime.length < 4){
				if(indexof4randomprime.indexOf(arrayofprime[index]) > -1) continue;
				indexof4randomprime[indexof4randomprime.length] = arrayofprime[index];
			}
		}
		
	}
	var primecompositeindexset = [];
	function generateprimecompositset(){
		var primecompositesetemp = [];
		primecompositeindexset = [];
		for(var i = 0; i < 8; i++){
			if(i<4){
				primecompositesetemp[i] = {
					numberindex: indexof4randomprime[i],
					type: 'prime'
				};
			}else{
				primecompositesetemp[i] = {
					numberindex: indexof4randomcomposite[i%4],
					type: 'composite'
				};
			}
		}
		
		while(primecompositeindexset.length < 8){
			randomnogenerate = Math.floor(Math.random()* (primecompositesetemp.length-1));
			primecompositeindexset[primecompositeindexset.length] = primecompositesetemp[randomnogenerate];
			primecompositesetemp.splice(randomnogenerate, 1);
		}
		
	}
	
	var arrayofindexofprimenumbers = [];
	var arrayofindexofcompositenumbers = [];
	
	function highlightprimenumbers() {
		var $rownormal = $('.rownormal');
		if (arrayofindexofprimenumbers.length == 0) {
			var $rownormalp = $('.rownormal> p');
			var value;
			var count = 0;
			var arrayofindexofprimenumberstemp = [];
			for (var j = 0; j < 100; j++) {
				arrayofindexofprimenumberstemp[j] = true;
			}
			arrayofindexofprimenumberstemp[0] = false;
			for (var i = 2; i < 11; i++) {
				$.each($rownormalp, function(index, val) {
					value = parseInt(val.innerHTML);

					if ((value % i) == 0 && value != i) {
						arrayofindexofprimenumberstemp[index] = false;
						console.log("value changed to false : "+value, i);
					}
				});
			}
			
			for (var k =0; k < arrayofindexofprimenumberstemp.length; k++){
				if(arrayofindexofprimenumberstemp[k]){
					console.log("value with true : "+k, true);
					arrayofindexofprimenumbers[arrayofindexofprimenumbers.length] = k;
					$($rownormal[k]).addClass('green_primefocus');
				}else if(k != 0){
					arrayofindexofcompositenumbers[arrayofindexofcompositenumbers.length] = k;
				}
			}
		}else{
			for(var h = 0; h < arrayofindexofprimenumbers.length ; h++){
				$($rownormal[arrayofindexofprimenumbers[h]]).addClass('green_primefocus');
			}
		}
	}
	
	function setDroppables(){
		var $rownormal = $('.rownormal');
		//array.splice(0, 1);
		var $primedroppable = $('.primeDroppable .rownormaldropable');
		var $compositedroppable = $('.compositeDroppable .rownormaldropable');
		
		$primedroppable.droppable({
			accept : ".prime",
			hoverClass : "hovered",
			drop : function upondrop(event, ui){
				handleCardDrop(event, ui, "prime");
			}
		});
		
		$compositedroppable.droppable({
			accept : ".composite",
			hoverClass : "hovered",
			drop : function upondrop(event, ui){
				handleCardDrop(event, ui, "composite");
			}
		});
		
		var draggablecount = 0;
		
		var type = primecompositeindexset[draggablecount].type;
		
		if(type == 'prime'){
			$($rownormal[primecompositeindexset[draggablecount].numberindex]).addClass("prime bluebackground")
				.draggable({
						containment : "body",
						cursor : "crosshair",
						revert : "invalid",
						appendTo : "body",
						helper : "clone",
						zindex : 1000,
						start: function(event, ui){
							
						},
						stop: function(event, ui){
						}
				});
		}else{
			$($rownormal[primecompositeindexset[draggablecount].numberindex]).addClass("composite bluebackground")
				.draggable({
						containment : "body",
						cursor : "crosshair",
						revert : "invalid",
						appendTo : "body",
						helper : "clone",
						zindex : 1000,
						start: function(event, ui){
							
						},
						stop: function(event, ui){
						}
				});
		}
		
		
		
		function handleCardDrop(event, ui, classname){
			ui.draggable.draggable('disable');
			var dropped = ui.draggable;
			var count = 0;
			var top = 0;
			
			$(dropped).addClass('graydisable');
			
			
			
				
			if(type == 'prime'){
				$(dropped).removeClass('prime');
				$(dropped).clone().removeClass('rownormal graydisable').addClass("rownormalafterdrop").appendTo($primedroppable);
			}else{
				$(dropped).removeClass('composite');
				$(dropped).clone().removeClass('rownormal graydisable').addClass("rownormalafterdrop").appendTo($compositedroppable);
			}
			
			draggablecount++;
			if(draggablecount == 8){
				$('.yellowlowertextblock').hide(0);
				$('#playagain').show(0).click(function(){
					// countNext = 8;
					templateCaller();
				});
				$nextBtn.show(0);
				return true;
			}
				
			
			
			type = primecompositeindexset[draggablecount].type;
			if(type == 'prime'){
				$($rownormal[primecompositeindexset[draggablecount].numberindex]).addClass("prime bluebackground")
					.draggable({
							containment : "body",
							cursor : "crosshair",
							revert : "invalid",
							appendTo : "body",
							helper : "clone",
							zindex : 1000,
							start: function(event, ui){
								
							},
							stop: function(event, ui){
							}
					});
			}else{
				$($rownormal[primecompositeindexset[draggablecount].numberindex]).addClass("composite bluebackground")
					.draggable({
							containment : "body",
							cursor : "crosshair",
							revert : "invalid",
							appendTo : "body",
							helper : "clone",
							zindex : 1000,
							start: function(event, ui){
								
							},
							stop: function(event, ui){
							}
					});
			}
		}
	}

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the 
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated 
   */
  
  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/ 
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');
    

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

    // just for development purpose to see total slide vs current slide number
    // $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/
  // countNext+=1;

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */
  
	$nextBtn.on('click', function() {
			countNext++;	
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});
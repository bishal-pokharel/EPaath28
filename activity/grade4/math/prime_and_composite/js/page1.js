var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";


var content=[
	{
		// slide0
 		contentnocenteradjust: true,
		contentblockadditionalclass:"firstpagebackground",
		imageblock:[
		  {
		   imagestoshow:[
		       {
		         imgclass:"elephant",
		         imgsrc: imgpath+"elephant.gif"
		      }]
		  }
		],
		uppertextblock:[{
			textclass: "chaptertext",
			textdata: data.string.p1_header
		}]
	},
	{
 		// slide1
 		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description",
			datahighlightflag: true,
			datahighlightcustomclass: "customhighlightclass",
			textdata: data.string.p1_s1
		}],
		imageblock:[
		  {
		   imageblockclass: "imageblock50 center",
		   imagestoshow:[
		       {
		         imgclass:"marble1",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble2",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble3",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble4",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble5",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble6",
		         imgsrc: imgpath+"marble1.png"
		      }
		    ],
		     imagelabels:[
			   {
			   		imagelabelclass: "imagelabel_header",
			   		imagelabeldata: data.string.p1_s11
			   },{
			   		imagelabelclass: "imagelabel_footer",
			   		imagelabeldata: data.string.p1_s2
			   }]
		  }
		]
	},
	{
		// slide2
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description",
			datahighlightflag: true,
			datahighlightcustomclass: "customhighlightclass",
			textdata: data.string.p1_s1
		}],
		imageblock:[
		  {
		   imageblockclass: "imageblock50 center",
		   imagestoshow:[
		      {
		         imgclass:"marble1 animate_6_3_2_left",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble2",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble3 animate_6_3_2_right",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble4 animate_6_3_2_left",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble5",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble6 animate_6_3_2_right",
		         imgsrc: imgpath+"marble1.png"
		      }
		    ],
		     imagelabels:[
			   {
			   		imagelabelclass: "imagelabel_header",
			   		imagelabeldata: data.string.p1_s13
			   },{
			   		imagelabelclass: "imagelabel_footer",
			   		imagelabeldata: data.string.p1_s3
			   }]
		  }
		]
	},
	{
		// slide3
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description",
			datahighlightflag: true,
			datahighlightcustomclass: "customhighlightclass",
			textdata: data.string.p1_s1
		}],
		imageblock:[
		  {
		   imageblockclass: "imageblock50 center",
		   imagestoshow:[
		      {
		         imgclass:"marble1 animate_6_3_2_top",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble2 animate_6_3_2_top",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble3 animate_6_3_2_top",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble4 animate_6_3_2_bottom",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble5 animate_6_3_2_bottom",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble6 animate_6_3_2_bottom",
		         imgsrc: imgpath+"marble1.png"
		      }
		    ],
		     imagelabels:[
			   {
			   		imagelabelclass: "imagelabel_header",
			   		imagelabeldata: data.string.p1_s14
			   },{
			   		imagelabelclass: "imagelabel_footer",
			   		imagelabeldata: data.string.p1_s4
			   }]
		  }
		]
	},
	{
		// slide4
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description",
			datahighlightflag: true,
			datahighlightcustomclass: "customhighlightclass",
			textdata: data.string.p1_s1
		}],
		imageblock:[
		  {
		   imageblockclass: "imageblock50 center",
		   imagestoshow:[
		       {
		         imgclass:"marble1 animate_6_3_2_left_top",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble2 animate_6_3_2_center_top",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble3 animate_6_3_2_right_top",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble4 animate_6_3_2_left_bottom",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble5 animate_6_3_2_center_bottom",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble6 animate_6_3_2_right_bottom",
		         imgsrc: imgpath+"marble1.png"
		      }
		    ],
		     imagelabels:[
			   {
			   		imagelabelclass: "imagelabel_header",
			   		imagelabeldata: data.string.p1_s11
			   },{
			   		imagelabelclass: "imagelabel_footer",
			   		datahighlightflag: true,
			   		imagelabeldata: data.string.p1_s5
			   }]
		  }
		]
	},
	{
		// slide5
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description",
			datahighlightflag: true,
			datahighlightcustomclass: "customhighlightclass",
			textdata: data.string.p1_s1
		}],
		imageblock:[
		  {
		   imageblockclass: "imageblock50 center slideleft",
		   imagestoshow:[
		       {
		         imgclass:"marble1",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble2",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble3",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble4",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble5",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble6",
		         imgsrc: imgpath+"marble1.png"
		      }
		    ],
		     imagelabels:[
			   {
			   		imagelabelclass: "imagelabel_header",
			   		imagelabeldata: data.string.p1_s11
			   }]
		  },{
		   imageblockclass: "imageblock50 right",
		   imagestoshow:[
		        {
		         imgclass:"marble7_1",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble7_2",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble7_3",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble7_4",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble7_5",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble7_6",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble7_7",
		         imgsrc: imgpath+"marble1.png"
		      }
		    ],
		     imagelabels:[
			   {
			   		imagelabelclass: "imagelabel_header",
			   		imagelabeldata: data.string.p1_s12
			   },{
			   		imagelabelclass: "imagelabel_footer footerleft",
			   		datahighlightflag: true,
			   		imagelabeldata: data.string.p1_s6
			   }]
		  }
		]
		
	},
	{
		// slide6
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description",
			datahighlightflag: true,
			datahighlightcustomclass: "customhighlightclass",
			textdata: data.string.p1_s1
		}],
		imageblock:[
		  {
		   imageblockclass: "imageblock50 left",
		   imagestoshow:[
		       {
		         imgclass:"marble1",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble2",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble3",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble4",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble5",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble6",
		         imgsrc: imgpath+"marble1.png"
		      }
		    ],
		     imagelabels:[
			   {
			   		imagelabelclass: "imagelabel_header",
			   		imagelabeldata: data.string.p1_s11
			   }]
		  },{
		   imageblockclass: "imageblock50 right",
		   imagestoshow:[
		       {
		         imgclass:"marble7_1 animate7_2_1",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble7_2 animate7_2_2",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble7_3 animate7_2_3",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble7_4 animate7_2_4",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble7_5 animate7_2_5",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble7_6 animate7_2_6",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble7_7 animate7_2_7",
		         imgsrc: imgpath+"marble1.png"
		      }
		    ],
		     imagelabels:[
			   {
			   		imagelabelclass: "imagelabel_header",
			   		datahighlightflag: true,
			   		datahighlightcustomclass: "scalehighlightclass",
			   		imagelabeldata: data.string.p1_s15
			   },{
			   		imagelabelclass: "imagelabel_footer footerleft",
			   		datahighlightflag: true,
			   		imagelabeldata: data.string.p1_s17
			   }]
		  }
		]
	},
	{
		// slide7
	  	contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description",
			datahighlightflag: true,
			datahighlightcustomclass: "customhighlightclass",
			textdata: data.string.p1_s1
		}],
		imageblock:[
		  {
		   imageblockclass: "imageblock50 left",
		   imagestoshow:[
		       {
		         imgclass:"marble1",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble2",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble3",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble4",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble5",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble6",
		         imgsrc: imgpath+"marble1.png"
		      }
		    ],
		     imagelabels:[
			   {
			   		imagelabelclass: "imagelabel_header",
			   		imagelabeldata: data.string.p1_s11
			   }]
		  },{
		   imageblockclass: "imageblock50 right",
		   imagestoshow:[
		       {
		         imgclass:"marble7_1 animate7_3_1",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble7_2 animate7_3_2",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble7_3 animate7_3_3",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble7_4 animate7_3_4",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble7_5 animate7_3_5",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble7_6 animate7_3_6",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble7_7 animate7_2_7",
		         imgsrc: imgpath+"marble1.png"
		      }
		    ],
		     imagelabels:[
			   {
			   		imagelabelclass: "imagelabel_header",
			   		datahighlightflag: true,
			   		datahighlightcustomclass: "scalehighlightclass",
			   		imagelabeldata: data.string.p1_s16
			   },{
			   		imagelabelclass: "imagelabel_footer footerleft",
			   		datahighlightflag: true,
			   		imagelabeldata: data.string.p1_s18
			   }]
		  }
		]
  },{
  	// slide8
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description",
			datahighlightflag: true,
			datahighlightcustomclass: "customhighlightclass",
			textdata: data.string.p1_s1
		}],
		imageblock:[
		  {
		   imageblockclass: "imageblock50 left",
		   imagestoshow:[
		       {
		         imgclass:"marble1",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble2",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble3",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble4",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble5",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble6",
		         imgsrc: imgpath+"marble1.png"
		      }
		    ],
		     imagelabels:[
			   {
			   		imagelabelclass: "imagelabel_header",
			   		imagelabeldata: data.string.p1_s11
			   }]
		  },{
		   imageblockclass: "imageblock50 right",
		   imagestoshow:[
		          {
			         imgclass:"marble7_1 animate7_23_1",
			         imgsrc: imgpath+"marble1.png"
			      },{
			         imgclass:"marble7_2 animate7_23_2",
			         imgsrc: imgpath+"marble1.png"
			      },{
			         imgclass:"marble7_3 animate7_23_3",
			         imgsrc: imgpath+"marble1.png"
			      },{
			         imgclass:"marble7_4 animate7_23_4",
			         imgsrc: imgpath+"marble1.png"
			      },{
			         imgclass:"marble7_5 animate7_23_5",
			         imgsrc: imgpath+"marble1.png"
			      },{
			         imgclass:"marble7_6 animate7_23_6",
			         imgsrc: imgpath+"marble1.png"
			      },{
			         imgclass:"marble7_7",
			         imgsrc: imgpath+"marble1.png"
			      }
		    ],
		     imagelabels:[
			   {
			   		imagelabelclass: "imagelabel_header",
			   		imagelabeldata: data.string.p1_s12
			   },{
			   		imagelabelclass: "imagelabel_footer footerleft",
			   		datahighlightflag: true,
			   		imagelabeldata: data.string.p1_s8
			   }]
		  }
		]
		
  },{
  	//slide 9
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description",
			datahighlightflag: true,
			datahighlightcustomclass: "customhighlightclass",
			textdata: data.string.p1_s1
		}],
		imageblock:[
		  {
		   imageblockclass: "imageblock50 left",
		   imagestoshow:[
		       {
		         imgclass:"marble1 animate_6_3_2_left_top",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble2 animate_6_3_2_center_top",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble3 animate_6_3_2_right_top",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble4 animate_6_3_2_left_bottom",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble5 animate_6_3_2_center_bottom",
		         imgsrc: imgpath+"marble1.png"
		      },{
		         imgclass:"marble6 animate_6_3_2_right_bottom",
		         imgsrc: imgpath+"marble1.png"
		      }
		    ],
		     imagelabels:[
			   {
			   		imagelabelclass: "imagelabel_header",
			   		imagelabeldata: data.string.p1_s11
			   },{
			   		imagelabelclass: "lastfooter",
			   		datahighlightflag: true,
			   		imagelabeldata: data.string.p1_s9
			   }]
		  },{
		   imageblockclass: "imageblock50 right",
		   imagestoshow:[
			      {
			         imgclass:"marble7_1 animate7_23_1",
			         imgsrc: imgpath+"marble1.png"
			      },{
			         imgclass:"marble7_2 animate7_23_2",
			         imgsrc: imgpath+"marble1.png"
			      },{
			         imgclass:"marble7_3 animate7_23_3",
			         imgsrc: imgpath+"marble1.png"
			      },{
			         imgclass:"marble7_4 animate7_23_4",
			         imgsrc: imgpath+"marble1.png"
			      },{
			         imgclass:"marble7_5 animate7_23_5",
			         imgsrc: imgpath+"marble1.png"
			      },{
			         imgclass:"marble7_6 animate7_23_6",
			         imgsrc: imgpath+"marble1.png"
			      },{
			         imgclass:"marble7_7",
			         imgsrc: imgpath+"marble1.png"
			      }
			    ],
		     imagelabels:[
			   {
			   		imagelabelclass: "imagelabel_header",
			   		imagelabeldata: data.string.p1_s12
			   },{
			   		imagelabelclass: "lastfooter",
			   		datahighlightflag: true,
			   		imagelabeldata: data.string.p1_s10
			   }]
		  }
		]
		
  }
];


$(function () { 
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;
  
  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);
	
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var current_sound;
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            // sounds
            {id: "sound_1", src: soundAsset + "s1_p1.ogg"},
            {id: "sound_2", src: soundAsset + "s1_p2.ogg"},
            {id: "sound_3", src: soundAsset + "s1_p3.ogg"},
            {id: "sound_4", src: soundAsset + "s1_p4.ogg"},
            {id: "sound_5", src: soundAsset + "s1_p5.ogg"},
            {id: "sound_6", src: soundAsset + "s1_p6.ogg"},
            {id: "sound_7", src: soundAsset + "s1_p7.ogg"},
            {id: "sound_8", src: soundAsset + "s1_p8.ogg"},
            {id: "sound_9", src: soundAsset + "s1_p9.ogg"},
            {id: "sound_10", src: soundAsset + "s1_p10.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller() : "";
        });
    }

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true 
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class
        
        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;
          
        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) { 
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/       
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
  
 	/*=====  End of data highlight function  ======*/
    
    /*===== This function splits the string in data into convential fraction used in mathematics =====*/
    function splitintofractions($splitinside){
   		typeof $splitinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;
        
        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
       	if($splitintofractions.length > 0){
       		$.each($splitintofractions, function(index, value){
	        	$this = $(this);
	        	var tobesplitfraction = $this.html();
	        	if($this.hasClass('fraction')){
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
	        	}else{
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
	        	}
				
				
				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');	        	
	        	$this.html(tobesplitfraction);
	        });	
       	}
   	}
   	/*===== split into fractions end =====*/

    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**   
      How to:
      - First set any html element with 
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification    
     */
    
    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which 
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/ 
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/    
   /**   
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that 
      footernotification is called for continue button to navigate to exercise
    */
  
  /**   
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if 
        so should be taken out from the templateCaller function
      - If the total page number is 
     */  
   
  function navigationcontroller(islastpageflag){

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			 ole.footerNotificationHandler.pageEndSetNotification();
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**   
      How to:
      - Just call instructionblockcontroller() from the template    
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which 
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);          
        });
      }
    } 
  /*=====  End of InstructionBlockController  ======*/
  
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/   
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);
	        
	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    splitintofractions($board);
		vocabcontroller.findwords(countNext);
		// splitintofractions($(".fractionblock"));
	  sound_player("sound_"+(countNext+1),true);
	    switch(countNext){
	   		case 2:
	   			setTimeout(function(){
					$(".animate_6_3_2_left").css('filter','drop-shadow(-4px 0px 5px #0f0)');
	   				$(".animate_6_3_2_right").css('filter','drop-shadow(4px 0px 5px #00f)');
	   				$(".marble2, .marble5").css('filter','drop-shadow(0px 0px 4px #dcf442)');
	   			}, 1000);
	   			break;
	   		case 3:
	   			setTimeout(function(){
					$(".animate_6_3_2_top").css('filter','drop-shadow(0px -4px 5px #0f0)');
	   				$(".animate_6_3_2_bottom").css('filter','drop-shadow(0px 4px 5px #00f)');
	   			}, 1000);
	   			break;
	   		case 5:
	   			var $right = $(".right");
	   			$right.hide(0);
	   			setTimeout(function(){
	   				$right.show(0);	
	   			},1000);
	   			break;
   			case 8:
   				setTimeout(function(){
   					$('.marble7_7').css('filter','drop-shadow(0px 0px 5px #f00)');
   				}, 400);
	   			break;
   			case 9:
	   			setTimeout(function(){
   					$('.marble7_7').css('filter','drop-shadow(0px 0px 5px #f00)');
   				}, 400);
	   		default:
	   			break;
	    }
  }

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the 
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated 
   */
  
  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/ 
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

    // just for development purpose to see total slide vs current slide number
    // $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/
  // countNext+=1;

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */
  
	$nextBtn.on('click', function() {
			countNext++;	
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});
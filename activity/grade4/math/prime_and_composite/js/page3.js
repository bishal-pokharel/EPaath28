var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	{
		//slide 0
		uppertextblockadditionalclass: "blackboard",
		duster : true,
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description",
			datahighlightflag: true,
			datahighlightcustomclass: "customgreenhighlight",
			textdata: data.string.p3_s1
		}],
		// imageblock:[
		  // {
		   // imagestoshow:[
		       // {
		         // imgclass:"sundar",
		         // imgsrc: imgpath+"teaching02.png"
		      // }
		    // ]
		  // }
		// ],
		
		flexblock:[
			{
				flexboxcolumnclass: "column1",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s5					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s45
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s46
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s48
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s50
				}
				]
			},
			{
				flexboxcolumnclass: "column2",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s6					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s47
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s49
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s51
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s52
				}
				]
			}
		]
	},{
		//slide 1
		uppertextblockadditionalclass: "blackboard",
		duster : true,
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description",
			datahighlightflag: true,
			datahighlightcustomclass: "customgreenhighlight",
			textdata: data.string.p3_s1
		},{
			textclass: "description2 scaleanimation",
			textdata: data.string.p3_s2
		}],
		
			flexblock:[
			{
				flexboxcolumnclass: "column1",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s5					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s45
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s46
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s48
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s50
				}
				]
			},
			{
				flexboxcolumnclass: "column2",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s6					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s47
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s49
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s51
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s52
				}
				]
			}
		]
	},{
		//slide 2
		uppertextblockadditionalclass: "blackboard increaseblackboardheight",
		duster : true,
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description",
			datahighlightflag: true,
			datahighlightcustomclass: "customgreenhighlight",
			textdata: data.string.p3_s3
		}],
		
		flexblock:[
			{
				flexboxcolumnclass: "column1",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s5					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s45
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s46
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s48
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s50
				}
				]
			},
			{
				flexboxcolumnclass: "column2",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s6					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s47
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s49
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s51
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s52
				}
				]
			}
		]
	},{
		//slide 3
		uppertextblockadditionalclass: "blackboard2",
		duster : true,
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description_a",
			textdata: data.string.p3_s31
		},{
			textclass: "description_a",
			textdata: data.string.p3_s5
		},{
			textclass: "description_a",
			textdata: data.string.p3_s6
		},{
			textclass: "description_a",
			textdata: data.string.p3_s7
		},{
			textclass: "description_a",
			textdata: data.string.p3_s8
		},{
			textclass: "description_a",
			textdata: data.string.p3_s9
		},{
			textclass: "description_a",
			textdata: data.string.p3_s32
		}],
		
		flexblock:[
			{
				flexboxcolumnclass: "column1",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s5					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s45
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s46
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s48
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s50
				}
				]
			},
			{
				flexboxcolumnclass: "column2",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s6					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s47
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s49
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s51
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s52
				}
				]
			}
		]
	},{
		//slide 4
		uppertextblockadditionalclass: "blackboard2",
		duster : true,
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description_a",
			textdata: data.string.p3_s4
		},{
			textclass: "description_a",
			textdata: data.string.p3_s5
		},{
			textclass: "description_a",
			textdata: data.string.p3_s6
		},{
			textclass: "description_a",
			textdata: data.string.p3_s7
		},{
			textclass: "description_a",
			textdata: data.string.p3_s8
		},{
			textclass: "description_a",
			textdata: data.string.p3_s9
		},{
			textclass: "description_a",
			textdata: data.string.p3_s10
		},{
			textclass: "description",
			textdata: data.string.p3_s11
		}],
		
		flexblock:[
			{
				flexboxcolumnclass: "column1",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s5					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s45
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s46
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s48
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s50
				}
				]
			},
			{
				flexboxcolumnclass: "column2",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s6					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s47
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s49
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s51
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s52
				}
				]
			}
		]
	},{
		//slide 5
		uppertextblockadditionalclass: "blackboard2",
		duster : true,
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description_a",
			textdata: data.string.p3_s4
		},{
			textclass: "description_a",
			textdata: data.string.p3_s5
		},{
			textclass: "description_a",
			textdata: data.string.p3_s6
		},{
			textclass: "description_a",
			textdata: data.string.p3_s7
		},{
			textclass: "description_a",
			textdata: data.string.p3_s8
		},{
			textclass: "description_a",
			textdata: data.string.p3_s9
		},{
			textclass: "description_a",
			textdata: data.string.p3_s10
		},{
			textclass: "description",
			textdata: data.string.p3_s12
		}],
		
		flexblock:[
			{
				flexboxcolumnclass: "column1",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s5					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s45
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s46
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s48
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s50
				}
				]
			},
			{
				flexboxcolumnclass: "column2",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s6					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s47
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s49
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s51
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s52
				}
				]
			}
		]
	},{
		//slide 6
		uppertextblockadditionalclass: "blackboard2",
		duster : true,
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description_a",
			textdata: data.string.p3_s4
		},{
			textclass: "description_a",
			textdata: data.string.p3_s5
		},{
			textclass: "description_a",
			textdata: data.string.p3_s6
		},{
			textclass: "description_a",
			textdata: data.string.p3_s7
		},{
			textclass: "description_a",
			textdata: data.string.p3_s8
		},{
			textclass: "description_a",
			textdata: data.string.p3_s9
		},{
			textclass: "description_a",
			textdata: data.string.p3_s10
		},{
			textclass: "description",
			datahighlightflag: true,
			datahighlightcustomclass: "customgreenhighlight",
			textdata: data.string.p3_s13
		}],
		
		flexblock:[
			{
				flexboxcolumnclass: "column1",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s5					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s45
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s46
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s48
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s50
				}
				]
			},
			{
				flexboxcolumnclass: "column2",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s6					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s47
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s49
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s51
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s52
				}
				]
			}
		]
	},{
		//slide 7
		uppertextblockadditionalclass: "blackboard",
		duster : true,
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description",
			datahighlightflag: true,
			datahighlightcustomclass: "custompinkhighlight",
			textdata: data.string.p3_s14
		}],
		
		flexblock:[
			{
				flexboxcolumnclass: "column1",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s5					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s45
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s46
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s48
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s50
				}
				]
			},
			{
				flexboxcolumnclass: "column2",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s6					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s47
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s49
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s51
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s52
				}
				]
			}
		]
	},{
		//slide 8
		uppertextblockadditionalclass: "blackboard",
		duster : true,
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description",
			datahighlightflag: true,
			datahighlightcustomclass: "custompinkhighlight",
			textdata: data.string.p3_s14
		},{
			textclass: "description2 scaleanimation",
			textdata: data.string.p3_s15
		}],
		
		flexblock:[
			{
				flexboxcolumnclass: "column1",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s5					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s45
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s46
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s48
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s50
				}
				]
			},
			{
				flexboxcolumnclass: "column2",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s6					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s47
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s49
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s51
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s52
				}
				]
			}
		]
	},{
		//slide 9
		uppertextblockadditionalclass: "blackboard increaseblackboardheight",
		duster : true,
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description",
			datahighlightflag: true,
			datahighlightcustomclass: "custompinkhighlight",
			textdata: data.string.p3_s16
		}],
		
		flexblock:[
			{
				flexboxcolumnclass: "column1",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s5					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s45
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s46
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s48
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s50
				}
				]
			},
			{
				flexboxcolumnclass: "column2",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s6					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s47
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s49
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s51
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s52
				}
				]
			}
		]
	},{
		//slide 10
		uppertextblockadditionalclass: "blackboard2",
		duster : true,
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description_a",
			textdata: data.string.p3_s33
		},{
			textclass: "description_a",
			textdata: data.string.p3_s34
		},{
			textclass: "description_a",
			textdata: data.string.p3_s19
		},{
			textclass: "description_a",
			textdata: data.string.p3_s35
		},{
			textclass: "description_a",
			textdata: data.string.p3_s21
		},{
			textclass: "description_a",
			textdata: data.string.p3_s22
		},{
			textclass: "description_a",
			textdata: data.string.p3_s23
		},{
			textclass: "description_a",
			textdata: data.string.p3_s36
		}],
		
		flexblock:[
			{
				flexboxcolumnclass: "column1",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s5					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s45
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s46
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s48
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s50
				}
				]
			},
			{
				flexboxcolumnclass: "column2",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s6					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s47
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s49
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s51
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s52
				}
				]
			}
		]
	},{
		//slide 11
		uppertextblockadditionalclass: "blackboard2",
		duster : true,
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description_a",
			textdata: data.string.p3_s17
		},{
			textclass: "description_a",
			textdata: data.string.p3_s18
		},{
			textclass: "description_a",
			textdata: data.string.p3_s19
		},{
			textclass: "description_a",
			textdata: data.string.p3_s20
		},{
			textclass: "description_a",
			textdata: data.string.p3_s21
		},{
			textclass: "description_a",
			textdata: data.string.p3_s22
		},{
			textclass: "description_a",
			textdata: data.string.p3_s23
		},{
			textclass: "description_a",
			textdata: data.string.p3_s24
		},{
			textclass: "description",
			datahighlightflag: true,
			datahighlightcustomclass: "customgreenhighlight",
			textdata: data.string.p3_s25
		}],
		
		flexblock:[
			{
				flexboxcolumnclass: "column1",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s5					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s45
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s46
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s48
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s50
				}
				]
			},
			{
				flexboxcolumnclass: "column2",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s6					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s47
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s49
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s51
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s52
				}
				]
			}
		]
	},{
		//slide 12
		uppertextblockadditionalclass: "blackboard2",
		duster : true,
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description_a",
			textdata: data.string.p3_s17
		},{
			textclass: "description_a",
			textdata: data.string.p3_s18
		},{
			textclass: "description_a",
			textdata: data.string.p3_s19
		},{
			textclass: "description_a",
			textdata: data.string.p3_s20
		},{
			textclass: "description_a",
			textdata: data.string.p3_s21
		},{
			textclass: "description_a",
			textdata: data.string.p3_s22
		},{
			textclass: "description_a",
			textdata: data.string.p3_s23
		},{
			textclass: "description_a",
			textdata: data.string.p3_s24
		},{
			textclass: "description",
			datahighlightflag: true,
			datahighlightcustomclass: "custompinkhighlight",
			textdata: data.string.p3_s26
		}],
		
		flexblock:[
			{
				flexboxcolumnclass: "column1",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s5					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s45
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s46
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s48
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s50
				}
				]
			},
			{
				flexboxcolumnclass: "column2",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s6					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s47
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s49
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s51
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s52
				}
				]
			}
		]
	},{
		//slide 13
		uppertextblockadditionalclass: "blackboard",
		duster : true,
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p3_s27
		}],
		
		flexblock:[
			{
				flexboxcolumnclass: "column1",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s5					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s45
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s46
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s48
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s50
				}
				]
			},
			{
				flexboxcolumnclass: "column2",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s6					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s47
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s49
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s51
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s52
				}
				]
			}
		]
	},{
		//slide 14
		uppertextblockadditionalclass: "blackboard",
		duster : true,
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p3_s27
		},{
			textclass: "description",
			textdata: data.string.p3_s28
		},{
			textclass: "description3",
			textdata: data.string.p3_s37
		},{
			textclass: "description_x",
			datahighlightflag: true,
			datahighlightcustomclass: "customgreenhighlight",
			textdata: data.string.p3_s29
		}],
		
		flexblock:[
			{
				flexboxcolumnclass: "column1",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s5					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s45
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s46
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s48
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s50
				}
				]
			},
			{
				flexboxcolumnclass: "column2",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s6					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s47
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s49
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s51
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s52
				}
				]
			}
		]/*
		,lowertextblockadditionalclass: "yellowlowertextblock",
				lowertextblock:[
					{
						textclass: "lowerinfotext",
						datahighlightflag: true,
						datahighlightcustomclass: "customgreenhighlight",
						textdata: data.string.p3_s29
					}
				]*/
		
	},{
		//slide 15
		uppertextblockadditionalclass: "blackboard",
		duster : true,
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p3_s27
		},{
			textclass: "description",
			textdata: data.string.p3_s28
		},{
			textclass: "description2",
			textdata: data.string.p3_s30
		}],
		
		flexblock:[
			{
				flexboxcolumnclass: "column1",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s5					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s45
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s46
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s48
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s50
				}
				]
			},
			{
				flexboxcolumnclass: "column2",
				flexblockcolumn:[{
					flexboxrowclass :"rowheader",
					textdata: data.string.p2_s6					
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s47
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s49
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s51
				},{
					flexboxrowclass :"rownormal",
					textdata: data.string.p2_s52
				}
				]
			}
		]
	}
];


$(function () { 
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);
	
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var current_sound;
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            // sounds
            {id: "sound_1", src: soundAsset + "s3_p1.ogg"},
            {id: "sound_2", src: soundAsset + "s3_p2.ogg"},
            {id: "sound_3", src: soundAsset + "s3_p3.ogg"},
            // {id: "sound_4", src: soundAsset + "s3_p4.ogg"},
            {id: "sound_5", src: soundAsset + "s3_p5.ogg"},
            {id: "sound_6", src: soundAsset + "s3_p6.ogg"},
            {id: "sound_7", src: soundAsset + "s3_p7.ogg"},
            {id: "sound_8", src: soundAsset + "s3_p8.ogg"},
            {id: "sound_9", src: soundAsset + "s3_p9.ogg"},
            {id: "sound_10", src: soundAsset + "s3_p10.ogg"},
            {id: "sound_12", src: soundAsset + "s3_p12.ogg"},
            {id: "sound_13", src: soundAsset + "s3_p13.ogg"},
            {id: "sound_14", src: soundAsset + "s3_p14.ogg"},
            {id: "sound_15", src: soundAsset + "s3_p15.ogg"},
            {id: "sound_16", src: soundAsset + "s3_p16.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller() : "";
        });
    }

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true 
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class
        
        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;
          
        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) { 
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/       
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
    /*=====  End of data highlight function  ======*/

	 /*===== This function splits the string in data into convential fraction used in mathematics =====*/
    function splitintofractions($splitinside){
   		typeof $splitinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;
        
        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
       	if($splitintofractions.length > 0){
       		$.each($splitintofractions, function(index, value){
	        	$this = $(this);
	        	var tobesplitfraction = $this.html();
	        	if($this.hasClass('fraction')){
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
	        	}else{
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
	        	}
				
				
				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');	        	
	        	$this.html(tobesplitfraction);
	        });	
       	}
   	}
	/*===== split into fractions end =====*/

    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**   
      How to:
      - First set any html element with 
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification    
     */
    
    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which 
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/ 
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/    
   /**   
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that 
      footernotification is called for continue button to navigate to exercise
    */
  
  /**   
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if 
        so should be taken out from the templateCaller function
      - If the total page number is 
     */  
   
  function navigationcontroller(islastpageflag){

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			 ole.footerNotificationHandler.pageEndSetNotification();
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**   
      How to:
      - Just call instructionblockcontroller() from the template    
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which 
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);          
        });
      }
    } 
  /*=====  End of InstructionBlockController  ======*/
  
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/   
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);
	        
	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
		splitintofractions($board);
		vocabcontroller.findwords(countNext);
      $nextBtn.hide(0);

      countNext==14?sound_player("sound_"+(countNext+1),false):sound_player("sound_"+(countNext+1),true);
      switch(countNext){
		  case 3:
		  	navigationcontroller();
		  	break;
	   		case 10:
	   			$nextBtn.show(0);
	   			break;
	   		case 1:
	   			$('.column1 > .rownormal > p').addClass('scaleanimation');
	   			break;
	   		case 5:
	   			var $description_a = $('.description_a');
	   			for(var i = 1; i < 6 ; i++){
	   				$($description_a[i]).addClass('blinktextclass');
	   			}
	   			break;
	   		case 8:
	   			$('.column2 > .rownormal > p').addClass('scaleanimation');
	   			break;
	   		case 14:
	   			var $description3 = $('.description3').hide(0);
                $('.primeclick,.compositeclick').click(function() {
                    current_sound.stop();
                    play_correct_incorrect_sound(0)
                });
	   			$('.noneclick').click(function(){
	   				current_sound.stop();
	   				play_correct_incorrect_sound(1)
	   				$(".blackboard> .description_x").hide(0);
	   				$description3.show(0);
	   				$nextBtn.delay(1000).show(0);
	   			});
	   			break;
	   		default:
	   			break;
	    }
  }

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the 
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated 
   */
  
  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/ 
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');
    
    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

    // just for development purpose to see total slide vs current slide number
    // $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/
  // countNext+=1;


  /* navigation buttons event handlers */
  
	$nextBtn.on('click', function() {
			countNext++;	
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});
var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

Array.prototype.shufflearray = function() {
    var i = this.length,
        j, temp;
    while (--i > 0) {
        j = Math.floor(Math.random() * (i + 1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
    return this;
};

var content=[
	//slide 0
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{
			textdata : data.string.ex_instrution,
			textclass : 'instruction'
		},
		{
			textdata : data.string.ex_frac1,
			textclass : 'fraction fractions_q q_1',
			splitintofractionsflag: true,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_red'
		},
		{
			textdata : ' ',
			textclass : 'fractions_q empty_sign',
		},
		{
			textdata : '=',
			textclass : 'fractions_q empty_sign hidden_sign',
		},
		{
			textdata : data.string.ex_frac1,
			textclass : 'fraction fractions_q q_2',
			splitintofractionsflag: true,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_red'
		}],
		lowertextblockadditionalclass: 'hint_div',
		lowertextblock : [{
			textdata : data.string.ex_hint_1,
			textclass : 'hint_header'
		},
		{
			textdata : data.string.ex_hint_1_a,
			textclass : 'hints hint_top',
			splitintofractionsflag: true,
			datahighlightflag : true,
			datahighlightcustomclass : 'hint_a'
		},
		{
			textdata : data.string.ex_hint_1_b,
			textclass : 'hints hint_bottom',
			splitintofractionsflag: true,
			datahighlightflag : true,
			datahighlightcustomclass : 'hint_b'
		}],

		optionsblockadditionalclass: '',
		optionsblock : [{
			textdata : data.string.eq_sign,
			textclass : 'options_sign sign_eq'
		},
		{
			textdata : data.string.lt_sign,
			textclass : 'options_sign sign_lt'
		},
		{
			textdata : data.string.gt_sign,
			textclass : 'options_sign sign_gt'
		}],
	},
];

// for question
var possible_question = ['1/10', '1/9', '1/8', '1/7', '1/6', '1/5', '2/9', '1/4', '2/7', '3/10', '1/3', '3/8', '2/5', '3/7', '4/9', '1/2', '5/9', '4/7', '3/5', '5/8', '2/3', '7/10', '5/7', '3/4', '7/9', '4/5', '5/6', '6/7', '7/8', '8/9', '9/10'];
// 4 -1 for less than questions
// 2 0 for equalto questions
// 4 1 for greater than questions
var possible_answer = [-1, -1, -1, -1, 0, 0, 1, 1, 1, 1];

possible_answer.shufflearray();


$(function ()
{
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 10;
	var score = 0;
  var preload;
	var timeoutvar = null;
	var current_sound;


	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
      {id: "ex", src: soundAsset+"ex.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
    /*=====  End of data highlight function  ======*/

	function splitintofractions($splitinside){
   		typeof $splitinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
       	if($splitintofractions.length > 0){
       		$.each($splitintofractions, function(index, value){
	        	$this = $(this);
	        	var tobesplitfraction = $this.html();
	        	if($this.hasClass('fraction')){
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
	        	}else{
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
	        	}

				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
	        	$this.html(tobesplitfraction);
	        });
       	}
   	}



	//add bg to rhino
	// add_bg(['bg_1.png','bg_2.png','bg_3.png']);
	// add_bg(['bg01.png','bg02.png','bg03.png']);
	// add_bg(['city_1.png','city_2.png','city_3.png']);
	var rhino = new RhinoTemplate();

	rhino.init(10, ['bg01.png','bg02.png','bg03.png']);



	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[0]);
		$board.html(html);

		texthighlight($board);
		splitintofractions($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		$('.congratulation').hide(0);
		$('.exefin').hide(0);
    if(countNext==0){
      sound_player("ex");
    }
		switch(countNext) {
			default:
				$nextBtn.hide(0);
				var wrong_clicked = 0;
				var frac_1_n = 0;
				var frac_1_d = 0;
				var frac_2_n = 0;
				var frac_2_d = 0;
				var new_question1 = 0;
				var new_question2 = 0;
				var rand_multiplier_1 = 1;
				var rand_multiplier_2 = 1;
				var rand_idx_1 = 0;
				var rand_idx_2 = 0;

				switch(possible_answer[countNext])	{
					case 0:
						rand_idx_1 = Math.floor(Math.random()*possible_question.length);
						new_question1 = possible_question[rand_idx_1];
						while(parseInt(new_question1.split("/")[1])>5){
							rand_idx_1 = Math.floor(Math.random()*possible_question.length);
							new_question1 = possible_question[rand_idx_1];
						}
						new_question2=new_question1;
						set_variables(new_question1, new_question2);
						$('.sign_gt, .sign_lt').click(function(){
							incorrect_btn(this);
						});
						$('.sign_eq').click(function(){
							correct_btn(this);
						});
						break;
					case 1:
						rand_idx_1 = Math.floor(Math.random()*possible_question.length);
						while( rand_idx_1 < 6){
							rand_idx_1 = Math.floor(Math.random()*possible_question.length);
						}
						rand_idx_2 = Math.floor(Math.random()*(rand_idx_1-1));
						new_question1 = possible_question[rand_idx_1];
						new_question2 = possible_question[rand_idx_2];
						set_variables(new_question1, new_question2);
						$('.sign_eq, .sign_lt').click(function(){
							incorrect_btn(this);
						});
						$('.sign_gt').click(function(){
							correct_btn(this);
						});
						break;
					case -1:
						rand_idx_1 = Math.floor(Math.random()*possible_question.length);
						while( rand_idx_1 > 25){
							rand_idx_1 = Math.floor(Math.random()*possible_question.length);
						}
						rand_idx_2 = rand_idx_1 + 1 + Math.floor(Math.random()*(possible_question.length-(rand_idx_1+2)));
						new_question1 = possible_question[rand_idx_1];
						new_question2 = possible_question[rand_idx_2];
						set_variables(new_question1, new_question2);
						$('.sign_eq, .sign_gt').click(function(){
							incorrect_btn(this);
						});
						$('.sign_lt').click(function(){
							correct_btn(this);
						});
						break;
				}
				function set_variables(ques_1, ques_2){
					var current_max_multiplier = 9;
					if(parseFloat(ques_1)==parseFloat(ques_2)){
						current_max_multiplier = 4;
					}
					rand_multiplier_1 = 1+Math.round(Math.random()*current_max_multiplier);
					rand_multiplier_2 = 1+Math.round(Math.random()*current_max_multiplier);

					if(parseFloat(ques_1)==parseFloat(ques_2)){
						while(rand_multiplier_1==rand_multiplier_2){
							rand_multiplier_2 = 1;
							rand_multiplier_1 = 2;
						}
					}

					frac_1_n = parseInt(ques_1.split("/")[0]);
					frac_1_d = parseInt(ques_1.split("/")[1]);
					frac_2_n = parseInt(ques_2.split("/")[0]);
					frac_2_d = parseInt(ques_2.split("/")[1]);

					while(rand_multiplier_1*frac_1_d>20){
						rand_multiplier_1 = 1+Math.round(Math.random()*current_max_multiplier);
						current_max_multiplier = 4;
					}
					current_max_multiplier = 9;
					while(rand_multiplier_2*frac_2_d>20){
						rand_multiplier_2 = 1+Math.round(Math.random()*9);
						current_max_multiplier = 4;
					}

					frac_1_n = rand_multiplier_1*parseInt(ques_1.split("/")[0]);
					frac_1_d = rand_multiplier_1*parseInt(ques_1.split("/")[1]);
					frac_2_n = rand_multiplier_2*parseInt(ques_2.split("/")[0]);
					frac_2_d = rand_multiplier_2*parseInt(ques_2.split("/")[1]);

					$('.q_1>.top').html(frac_1_n);
					$('.q_1>.bottom').html(frac_1_d);
					$('.hint_top>.fraction2>.top').html(frac_1_n);
					$('.hint_top>.fraction2>.bottom').html(frac_1_d);
					$('.q_1>.bottom').css('border-top', '0.1em solid white');
					$('.q_2>.top').html(frac_2_n);
					$('.q_2>.bottom').html(frac_2_d);
					$('.hint_bottom>.fraction2>.top').html(frac_2_n);
					$('.hint_bottom>.fraction2>.bottom').html(frac_2_d);
					$('.q_2>.bottom').css('border-top', '0.1em solid white');
				}
				function correct_btn(current_btn){
					$('.options_sign').addClass('disabled');
					$(current_btn).addClass('option_true');
					$('.hidden_sign').html($(current_btn).html());
					$('.hidden_sign').addClass('fade_in');
					if(countNext < 10){
						$nextBtn.show(0);
					} else {
						$nextBtn.hide(0);
					}
					if(!wrong_clicked){
						rhino.update(true,['options_sign']);
					}
              		play_correct_incorrect_sound(1);
				}
				function incorrect_btn(current_btn){
					$(current_btn).addClass('disabled');
					$(current_btn).addClass('option_false');
					if(!wrong_clicked){
						rhino.update(false,['options_sign']);
					}
					wrong_clicked++;
					if(wrong_clicked==1){
						show_hint_1();
					} else if(wrong_clicked==2){
						show_hint_2();
					}
              		play_correct_incorrect_sound(0);
				}
				function show_hint_1(){
					$('.hint_div').show(0);
					$('.hint_top>.hint_a>.fraction2>.top').html(frac_1_n/rand_multiplier_1);
					$('.hint_top>.hint_a>.fraction2>.bottom').html(frac_1_d/rand_multiplier_1);
					$('.hint_bottom>.hint_b>.fraction2>.top').html(frac_2_n/rand_multiplier_2);
					$('.hint_bottom>.hint_b>.fraction2>.bottom').html(frac_2_d/rand_multiplier_2);
				}
				function show_hint_2(){
					var lcm = lcm_two_numbers(frac_1_d,frac_2_d);
					var top_1= frac_1_n*lcm/frac_1_d;
					var top_2 = frac_2_n*lcm/frac_2_d;
					$('.hint_header').html(data.string.ex_hint_2);
					$('.hint_top>.hint_a>.fraction2>.top').html(top_1);
					$('.hint_top>.hint_a>.fraction2>.bottom').html(lcm);
					$('.hint_bottom>.hint_b>.fraction2>.top').html(top_2);
					$('.hint_bottom>.hint_b>.fraction2>.bottom').html(lcm);
				}
				break;
		}
	}

	function lcm_two_numbers(x1, y1) {
		x = Math.abs(x1);
		y = Math.abs(y1);
		while(y) {
			var t = y;
			y = x % y;
			x = t;
		}
		return (!x1 || !y1) ? 0 : Math.abs((x1 * y1) / x);
	}

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		if(countNext < 11){
			templateCaller();
			rhino.gotoNext();
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref+"/images/";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
  //slide 1
  {
    contentblockadditionalclass : "bluishbg",
    uppertextblock: [
      {
        datahighlightflag: true,
        textclass: "textclass textclass1 cssfadein",
        textdata: data.string.p5text1,
      }
    ]
  },
  //slide 2
  {
    contentblockadditionalclass : "bluishbg",
    uppertextblock: [
      {
        datahighlightflag: true,
        textclass: "textclass textclass1 opa",
        textdata: data.string.p5text1,
      },
      {
        datahighlightflag: true,
        textclass: "textclass textclass2 cssfadein",
        textdata: data.string.p5text2,
      },
      {
        textclass: "greaterthan cssfadein2",
        textdata: ">"
      },
      {
        textclass: "smallerthan cssfadein3",
        textdata: "<"
      },
      {
        textclass: "equalto cssfadein4",
        textdata: "="
      }
    ],
    fractionblock:[
      {
        fractionClass: "bluefraction frac-1_q cssfadein2",
        splitintofractionsflag: true,
        spandata: data.string.p5frac1
      },
      {
        fractionClass: "bluefraction frac-2_q cssfadein2",
        splitintofractionsflag: true,
        spandata: data.string.p5frac2
      },
      {
        fractionClass: "bluefraction frac-3_q cssfadein3",
        splitintofractionsflag: true,
        spandata: data.string.p5frac3
      },
      {
        fractionClass: "bluefraction frac-4_q cssfadein3",
        splitintofractionsflag: true,
        spandata: data.string.p5frac4
      },
      {
        fractionClass: "bluefraction frac-5_q cssfadein4",
        splitintofractionsflag: true,
        spandata: data.string.p5frac5
      },
      {
        fractionClass: "bluefraction frac-6_q cssfadein4",
        splitintofractionsflag: true,
        spandata: data.string.p5frac5
      }
    ]
  },
//slide 3
  {
    contentblockadditionalclass : "bluishbg",
    uppertextblock: [
      {
        datahighlightflag: true,
        textclass: "textclass textclass1 opa",
        textdata: data.string.p5text1,
      },
      {
        datahighlightflag: true,
        textclass: "textclass textclass2 opa",
        textdata: data.string.p5text2,
      },
      {
        textclass: "greaterthan opa",
        textdata: ">"
      },
      {
        textclass: "smallerthan opa",
        textdata: "<"
      },
      {
        textclass: "equalto opa",
        textdata: "="
      },
      {
        datahighlightflag: true,
        textclass: "textclass middlehighlight cssfadein",
        textdata: data.string.p5text3_1,
      }
    ],
    fractionblock:[
      {
        fractionClass: "bluefraction frac-1_q opa",
        splitintofractionsflag: true,
        spandata: data.string.p5frac1
      },
      {
        fractionClass: "bluefraction frac-2_q opa",
        splitintofractionsflag: true,
        spandata: data.string.p5frac2
      },
      {
        fractionClass: "bluefraction frac-3_q opa",
        splitintofractionsflag: true,
        spandata: data.string.p5frac3
      },
      {
        fractionClass: "bluefraction frac-4_q opa",
        splitintofractionsflag: true,
        spandata: data.string.p5frac4
      },
      {
        fractionClass: "bluefraction frac-5_q opa",
        splitintofractionsflag: true,
        spandata: data.string.p5frac5
      },
      {
        fractionClass: "bluefraction frac-6_q opa",
        splitintofractionsflag: true,
        spandata: data.string.p5frac5
      }
    ],
  },
//slide 4
  {
    contentblockadditionalclass : "bluishbg",
    imageblock:[
      {
        imagestoshow:[
            {
              imgclass:"plate1",
              imgsrc: imgpath+"empty-plate.png"
            },
            {
              imgclass:"firsthalf",
              imgsrc: imgpath+"half01.png"
            },
            {
              imgclass:"secondhalf",
              imgsrc: imgpath+"half02.png"
            },
            {
              imgclass:"plate2",
              imgsrc: imgpath+"empty-plate.png"
            },
            {
              imgclass:"firstthird",
              imgsrc: imgpath+"one_third01.png"
            },
            {
              imgclass:"secondthird",
              imgsrc: imgpath+"one_third02.png"
            },
            {
              imgclass:"thirdthird",
              imgsrc: imgpath+"one_third03.png"
            },

          ],

        },
      ],
      fractionblock:[
        {
          fractionClass: "normalfraction halftext1",
          splitintofractionsflag: true,
          spandata: data.string.p5frac6
        },
        {
          fractionClass: "normalfraction halftext2",
          splitintofractionsflag: true,
          spandata: data.string.p5frac6
        },
        {
          fractionClass: "normalfraction thirdtext1",
          splitintofractionsflag: true,
          spandata: data.string.p5frac7
        },
        {
          fractionClass: "normalfraction thirdtext2",
          splitintofractionsflag: true,
          spandata: data.string.p5frac7
        },
        {
          fractionClass: "normalfraction thirdtext3",
          splitintofractionsflag: true,
          spandata: data.string.p5frac7
        }
    ],

    uppertextblock: [
      {
        textclass: "textclass textclass3 cssfadein",
        textdata: data.string.p5text3,
        splitintofractionsflag: true,

      },


    ],
  },

  //slide 5
  {
    contentblockadditionalclass : "bluishbg",
    imageblock:[
      {
        imagestoshow:[



            {
              imgclass:"plate1",
              imgsrc: imgpath+"empty-plate.png"
            },
            {
              imgclass:"firsthalf cssfadeout",
              imgsrc: imgpath+"half01.png"
            },
            {
              imgclass:"secondhalf cssfadeout",
              imgsrc: imgpath+"half02.png"
            },
            {
              imgclass:"firstonesixth cssfadein2",
              imgsrc: imgpath+"one_sixth01.png"
            },
            {
              imgclass:"secondonesixth cssfadein2",
              imgsrc: imgpath+"one_sixth02.png"
            },
            {
              imgclass:"thirdonesixth cssfadein2",
              imgsrc: imgpath+"one_sixth03.png"
            },
            {
              imgclass:"forthonesixth cssfadein2",
              imgsrc: imgpath+"one_sixth04.png"
            },
            {
              imgclass:"fifthonesixth cssfadein2",
              imgsrc: imgpath+"one_sixth05.png"
            },
            {
              imgclass:"sixthonesixth cssfadein2",
              imgsrc: imgpath+"one_sixth06.png"
            },



            {
              imgclass:"plate2",
              imgsrc: imgpath+"empty-plate.png"
            },
            {
              imgclass:"firstthird",
              imgsrc: imgpath+"one_third01.png"
            },
            {
              imgclass:"secondthird",
              imgsrc: imgpath+"one_third02.png"
            },
            {
              imgclass:"thirdthird",
              imgsrc: imgpath+"one_third03.png"
            },

          ],

        },
      ],
      fractionblock:[
        {
          fractionClass: "normalfraction halftext1 cssfadeout",
          splitintofractionsflag: true,
          spandata: data.string.p5frac6
        },
        {
          fractionClass: "normalfraction halftext2 cssfadeout",
          splitintofractionsflag: true,
          spandata: data.string.p5frac6
        },


        {
          fractionClass: "normalfractionsmall firstfracsix cssfadein2",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall secondfracsix cssfadein2",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall thirdfracsix cssfadein2",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall forthfracsix cssfadein2",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall fifthfracsix cssfadein2",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall sixthfracsix cssfadein2",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },

        {
          fractionClass: "normalfraction thirdtext1",
          splitintofractionsflag: true,
          spandata: data.string.p5frac7
        },
        {
          fractionClass: "normalfraction thirdtext2",
          splitintofractionsflag: true,
          spandata: data.string.p5frac7
        },
        {
          fractionClass: "normalfraction thirdtext3",
          splitintofractionsflag: true,
          spandata: data.string.p5frac7
        }
    ],

    uppertextblock: [

      {
        textclass: "textclass textclass3 opa",
        textdata: data.string.p5text3,
        splitintofractionsflag: true,

      },
      {
        datahighlightflag: true,
        textclass: "textclass textclass4 cssfadein",
        textdata: data.string.p5text4,
      },


    ],
  },

  //slide 6
  {
    contentblockadditionalclass : "bluishbg",
    imageblock:[
      {
        imagestoshow:[
            {
              imgclass:"plate1",
              imgsrc: imgpath+"empty-plate.png"
            },
            {
              imgclass:"firstonesixth opa",
              imgsrc: imgpath+"one_sixth01.png"
            },
            {
              imgclass:"secondonesixth opa",
              imgsrc: imgpath+"one_sixth02.png"
            },
            {
              imgclass:"thirdonesixth opa",
              imgsrc: imgpath+"one_sixth03.png"
            },
            {
              imgclass:"forthonesixth opa",
              imgsrc: imgpath+"one_sixth04.png"
            },
            {
              imgclass:"fifthonesixth opa",
              imgsrc: imgpath+"one_sixth05.png"
            },
            {
              imgclass:"sixthonesixth opa",
              imgsrc: imgpath+"one_sixth06.png"
            },



            {
              imgclass:"plate2",
              imgsrc: imgpath+"empty-plate.png"
            },
            {
              imgclass:"firstthird",
              imgsrc: imgpath+"one_third01.png"
            },
            {
              imgclass:"secondthird",
              imgsrc: imgpath+"one_third02.png"
            },
            {
              imgclass:"thirdthird",
              imgsrc: imgpath+"one_third03.png"
            },

          ],

        },
      ],
      fractionblock:[
        {
          fractionClass: "normalfractionsmall firstfracsix opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall secondfracsix opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall thirdfracsix opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall forthfracsix opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall fifthfracsix opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall sixthfracsix opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },

        {
          fractionClass: "normalfraction thirdtext1",
          splitintofractionsflag: true,
          spandata: data.string.p5frac7
        },
        {
          fractionClass: "normalfraction thirdtext2",
          splitintofractionsflag: true,
          spandata: data.string.p5frac7
        },
        {
          fractionClass: "normalfraction thirdtext3",
          splitintofractionsflag: true,
          spandata: data.string.p5frac7
        }
    ],

    uppertextblock: [

      {
        textclass: "textclass textclass3 opa",
        textdata: data.string.p5text3,
        splitintofractionsflag: true,

      },
      {
        textclass: "textclass textclass5 opa",
        textdata: data.string.p5text5,
        splitintofractionsflag: true,

      },
      {
        datahighlightflag: true,
        textclass: "textclass textclass4 opa",
        textdata: data.string.p5text4,
      },


    ],
  },

  //slide 7
  {
    contentblockadditionalclass : "bluishbg",
    imageblock:[
      {
        imagestoshow:[
            {
              imgclass:"plate1",
              imgsrc: imgpath+"empty-plate.png"
            },
            {
              imgclass:"firstonesixth opa",
              imgsrc: imgpath+"one_sixth01.png"
            },
            {
              imgclass:"secondonesixth opa",
              imgsrc: imgpath+"one_sixth02.png"
            },
            {
              imgclass:"thirdonesixth opa",
              imgsrc: imgpath+"one_sixth03.png"
            },
            {
              imgclass:"forthonesixth opa",
              imgsrc: imgpath+"one_sixth04.png"
            },
            {
              imgclass:"fifthonesixth opa",
              imgsrc: imgpath+"one_sixth05.png"
            },
            {
              imgclass:"sixthonesixth opa",
              imgsrc: imgpath+"one_sixth06.png"
            },



            {
              imgclass:"plate2",
              imgsrc: imgpath+"empty-plate.png"
            },
            {
              imgclass:"firstthird cssfadeout",
              imgsrc: imgpath+"one_third01.png"
            },
            {
              imgclass:"secondthird cssfadeout",
              imgsrc: imgpath+"one_third02.png"
            },
            {
              imgclass:"thirdthird cssfadeout",
              imgsrc: imgpath+"one_third03.png"
            },
            {
              imgclass:"firsttwosixth cssfadein2",
              imgsrc: imgpath+"one_sixth01.png"
            },
            {
              imgclass:"secondtwosixth cssfadein2",
              imgsrc: imgpath+"one_sixth02.png"
            },
            {
              imgclass:"thirdtwosixth cssfadein2",
              imgsrc: imgpath+"one_sixth03.png"
            },
            {
              imgclass:"forthtwosixth cssfadein2",
              imgsrc: imgpath+"one_sixth04.png"
            },
            {
              imgclass:"fifthtwosixth cssfadein2",
              imgsrc: imgpath+"one_sixth05.png"
            },
            {
              imgclass:"sixthtwosixth cssfadein2",
              imgsrc: imgpath+"one_sixth06.png"
            },

          ],

        },
      ],
      fractionblock:[
        {
          fractionClass: "normalfractionsmall firstfracsix opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall secondfracsix opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall thirdfracsix opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall forthfracsix opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall fifthfracsix opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall sixthfracsix opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },

         {
          fractionClass: "normalfraction thirdtext1 cssfadeout",
          splitintofractionsflag: true,
          spandata: data.string.p5frac7
        },
        {
          fractionClass: "normalfraction thirdtext2 cssfadeout",
          splitintofractionsflag: true,
          spandata: data.string.p5frac7
        },
        {
          fractionClass: "normalfraction thirdtext3 cssfadeout",
          splitintofractionsflag: true,
          spandata: data.string.p5frac7
        },

        {
          fractionClass: "normalfractionsmall firstfracsixtwo cssfadein2",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall secondfracsixtwo cssfadein2",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall thirdfracsixtwo cssfadein2",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall forthfracsixtwo cssfadein2",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall fifthfracsixtwo cssfadein2",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall sixthfracsixtwo cssfadein2",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },


    ],

    uppertextblock: [

      {
        textclass: "textclass textclass3 opa",
        textdata: data.string.p5text3,
        splitintofractionsflag: true,

      },
      {
        textclass: "textclass textclass5 opa",
        textdata: data.string.p5text5,
        splitintofractionsflag: true,

      },
      {
        datahighlightflag: true,
        textclass: "textclass textclass4 opa",
        textdata: data.string.p5text4,
      },
      {
        datahighlightflag: true,
        textclass: "textclass textclass6 cssfadein",
        textdata: data.string.p5text6,
      },

    ],
  },


    //slide 8
  {
    contentblockadditionalclass : "bluishbg",
    imageblock:[
      {
        imagestoshow:[
            {
              imgclass:"plate1",
              imgsrc: imgpath+"empty-plate.png"
            },
            {
              imgclass:"firstonesixth opa",
              imgsrc: imgpath+"one_sixth01.png"
            },
            {
              imgclass:"secondonesixth opa",
              imgsrc: imgpath+"one_sixth02.png"
            },
            {
              imgclass:"thirdonesixth opa",
              imgsrc: imgpath+"one_sixth03.png"
            },
            {
              imgclass:"forthonesixth opa",
              imgsrc: imgpath+"one_sixth04.png"
            },
            {
              imgclass:"fifthonesixth opa",
              imgsrc: imgpath+"one_sixth05.png"
            },
            {
              imgclass:"sixthonesixth opa",
              imgsrc: imgpath+"one_sixth06.png"
            },



            {
              imgclass:"plate2",
              imgsrc: imgpath+"empty-plate.png"
            },
            {
              imgclass:"firsttwosixth opa",
              imgsrc: imgpath+"one_sixth01.png"
            },
            {
              imgclass:"secondtwosixth opa",
              imgsrc: imgpath+"one_sixth02.png"
            },
            {
              imgclass:"thirdtwosixth opa",
              imgsrc: imgpath+"one_sixth03.png"
            },
            {
              imgclass:"forthtwosixth opa",
              imgsrc: imgpath+"one_sixth04.png"
            },
            {
              imgclass:"fifthtwosixth opa",
              imgsrc: imgpath+"one_sixth05.png"
            },
            {
              imgclass:"sixthtwosixth opa",
              imgsrc: imgpath+"one_sixth06.png"
            },

          ],

        },
      ],
      fractionblock:[
        {
          fractionClass: "normalfractionsmall firstfracsix opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall secondfracsix opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall thirdfracsix opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall forthfracsix opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall fifthfracsix opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall sixthfracsix opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },



        {
          fractionClass: "normalfractionsmall firstfracsixtwo opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall secondfracsixtwo opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall thirdfracsixtwo opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall forthfracsixtwo opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall fifthfracsixtwo opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },
        {
          fractionClass: "normalfractionsmall sixthfracsixtwo opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac8
        },


    ],

    uppertextblock: [

      {
        textclass: "textclass textclass3 opa",
        textdata: data.string.p5text3,
        splitintofractionsflag: true,

      },
      {
        textclass: "textclass textclass5 opa",
        textdata: data.string.p5text5,
        splitintofractionsflag: true,

      },
      {
        datahighlightflag: true,
        textclass: "textclass textclass4 opa",
        textdata: data.string.p5text4,
      },
      {
        datahighlightflag: true,
        textclass: "textclass textclass6 opa",
        textdata: data.string.p5text6,
      },
      {
        datahighlightflag: true,
        textclass: "textclass middlehighlight cssfadein",
        textdata: data.string.p5text7,
      }

    ],
  },


  //slide 9
  {
    contentblockadditionalclass : "bluishbg",
    imageblock:[
      {
        imagestoshow:[
            {
              imgclass:"plate1",
              imgsrc: imgpath+"empty-plate.png"
            },
            {
              imgclass:"firstonesixth opa",
              imgsrc: imgpath+"one_sixth01.png"
            },
            {
              imgclass:"secondonesixth opa",
              imgsrc: imgpath+"one_sixth02.png"
            },
            {
              imgclass:"thirdonesixth opa",
              imgsrc: imgpath+"one_sixth03.png"
            },


            {
              imgclass:"plate2",
              imgsrc: imgpath+"empty-plate.png"
            },
            {
              imgclass:"firsttwosixth opa",
              imgsrc: imgpath+"one_sixth01.png"
            },
            {
              imgclass:"secondtwosixth opa",
              imgsrc: imgpath+"one_sixth02.png"
            },
            {
              imgclass:"thirdtwosixth opa",
              imgsrc: imgpath+"one_sixth03.png"
            },
            {
              imgclass:"forthtwosixth opa",
              imgsrc: imgpath+"one_sixth04.png"
            },


            {
              imgclass:"sundarimg sundarright",
              imgsrc: imgpath+"sundarright.png"
            },

            {
              imgclass:"sundarimg sundarwrong",
              imgsrc: imgpath+"sundarwrong.png"
            },


          ],

        },
      ],


    uppertextblock: [
      {
        textclass: "textclass textclass3 cssfadein",
        textdata: data.string.p5text8,
      },
     /* {
        textclass: "textclass textclass5",
        textdata: data.string.p5text8,
      },*/
      {
        textclass: "checkbutton checkbutton1 opa",
        textdata: data.string.p5text9,
      },
      /*{
        textclass: "checkbutton checkbutton2",
        textdata: data.string.p5text9,
      },*/

    ],

    inputtext: [
        {
          inputclass : "textbox textbox1",
        },
       /* {
          inputclass : "textbox textbox2",
        },*/
    ]
  },


  //slide 10
  {
    contentblockadditionalclass : "bluishbg",
    imageblock:[
      {
        imagestoshow:[
            {
              imgclass:"plate1",
              imgsrc: imgpath+"empty-plate.png"
            },
            {
              imgclass:"firstonesixth opa",
              imgsrc: imgpath+"one_sixth01.png"
            },
            {
              imgclass:"secondonesixth opa",
              imgsrc: imgpath+"one_sixth02.png"
            },
            {
              imgclass:"thirdonesixth opa",
              imgsrc: imgpath+"one_sixth03.png"
            },


            {
              imgclass:"plate2",
              imgsrc: imgpath+"empty-plate.png"
            },
            {
              imgclass:"firsttwosixth opa",
              imgsrc: imgpath+"one_sixth01.png"
            },
            {
              imgclass:"secondtwosixth opa",
              imgsrc: imgpath+"one_sixth02.png"
            },
            {
              imgclass:"thirdtwosixth opa",
              imgsrc: imgpath+"one_sixth03.png"
            },
            {
              imgclass:"forthtwosixth opa",
              imgsrc: imgpath+"one_sixth04.png"
            },


            {
              imgclass:"sundarimg sundarright",
              imgsrc: imgpath+"sundarright.png"
            },

            {
              imgclass:"sundarimg sundarwrong",
              imgsrc: imgpath+"sundarwrong.png"
            },


          ],

        },
      ],


    uppertextblock: [
      {
        textclass: "textclass textclass3 opa",
        textdata: data.string.p5text8,
      },
      {
        textclass: "textclass textclass5 cssfadein",
        textdata: data.string.p5text8,
      },
      {
        textclass: "checkbutton checkbutton1 opa",
        textdata: data.string.p5text9,
      },
      {
        textclass: "checkbutton checkbutton2 cssfadein",
        textdata: data.string.p5text9,
      },

    ],

    inputtext: [
        {
          inputclass : "textbox textbox1",
        },
        {
          inputclass : "textbox textbox2 cssfadein",
        },
    ]
  },


  //slide 10
  {
    contentblockadditionalclass : "bluishbg",
    imageblock:[
      {
        imagestoshow:[
            {
              imgclass:"plate1",
              imgsrc: imgpath+"empty-plate.png"
            },
            {
              imgclass:"firstonesixth opa",
              imgsrc: imgpath+"one_sixth01.png"
            },
            {
              imgclass:"secondonesixth opa",
              imgsrc: imgpath+"one_sixth02.png"
            },
            {
              imgclass:"thirdonesixth opa",
              imgsrc: imgpath+"one_sixth03.png"
            },


            {
              imgclass:"plate2",
              imgsrc: imgpath+"empty-plate.png"
            },
            {
              imgclass:"firsttwosixth opa",
              imgsrc: imgpath+"one_sixth01.png"
            },
            {
              imgclass:"secondtwosixth opa",
              imgsrc: imgpath+"one_sixth02.png"
            },
            {
              imgclass:"thirdtwosixth opa",
              imgsrc: imgpath+"one_sixth03.png"
            },
            {
              imgclass:"forthtwosixth opa",
              imgsrc: imgpath+"one_sixth04.png"
            },

          ],

        },
      ],

    uppertextblock: [
      {
        textclass: "textclass textclass3 cssfadein",
        textdata: data.string.p5text10,
        splitintofractionsflag: true
      },
      {
        textclass: "textclass textclass5 cssfadein3",
        textdata: data.string.p5text11_sec,
        splitintofractionsflag: true

      },
      {
        textclass: "p11equalto1 cssfadein2",
        textdata: "="
      },
      {
        textclass: "p11equalto2 cssfadein4",
        textdata: "="
      }
    ],

    fractionblock:[
        {
          fractionClass: "normalfraction p11frac1 cssfadein2",
          splitintofractionsflag: true,
          spandata: data.string.p5frac6
        },
        {
          fractionClass: "normalfraction p11frac2 cssfadein2",
          splitintofractionsflag: true,
          spandata: data.string.p5frac9
        },
        {
          fractionClass: "normalfraction p11frac3 cssfadein4",
          splitintofractionsflag: true,
          spandata: data.string.p5frac10
        },
        {
          fractionClass: "normalfraction p11frac4 cssfadein4",
          splitintofractionsflag: true,
          spandata: data.string.p5frac11
        },

    ],
  },


  //slide 11
  {
    contentblockadditionalclass : "bluishbg",
    imageblock:[
      {
        imagestoshow:[
            {
              imgclass:"plate1",
              imgsrc: imgpath+"empty-plate.png"
            },
            {
              imgclass:"firstonesixth opa",
              imgsrc: imgpath+"one_sixth01.png"
            },
            {
              imgclass:"secondonesixth opa",
              imgsrc: imgpath+"one_sixth02.png"
            },
            {
              imgclass:"thirdonesixth opa",
              imgsrc: imgpath+"one_sixth03.png"
            },


            {
              imgclass:"plate2",
              imgsrc: imgpath+"empty-plate.png"
            },
            {
              imgclass:"firsttwosixth opa",
              imgsrc: imgpath+"one_sixth01.png"
            },
            {
              imgclass:"secondtwosixth opa",
              imgsrc: imgpath+"one_sixth02.png"
            },
            {
              imgclass:"thirdtwosixth opa",
              imgsrc: imgpath+"one_sixth03.png"
            },
            {
              imgclass:"forthtwosixth opa",
              imgsrc: imgpath+"one_sixth04.png"
            },

          ],

        },
      ],


    uppertextblock: [
      {
        textclass: "textclass textclass3 opa",
        textdata: data.string.p5text10,
        splitintofractionsflag: true

      },
      {
        textclass: "textclass textclass5 opa",
        textdata: data.string.p5text11_sec,
        splitintofractionsflag: true

      },
      {
        textclass: "p11equalto1 opa",
        textdata: "="
      },
      {
        textclass: "p11equalto2 opa",
        textdata: "="
      },
      {
        datahighlightflag: true,
        textclass: "textclass middlehighlight cssfadein nwmiddlehighlight",
        textdata: data.string.p5text12,
      }
    ],

    fractionblock:[
        {
          fractionClass: "normalfraction p11frac1 opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac6
        },
        {
          fractionClass: "normalfraction p11frac2 blinkfrac opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac9
        },
        {
          fractionClass: "normalfraction p11frac3 opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac10
        },
        {
          fractionClass: "normalfraction p11frac4 blinkfrac opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac11
        },

    ],
  },


  //slide 12
  {
    contentblockadditionalclass : "bluishbg",
    imageblock:[
      {
        imagestoshow:[
            {
              imgclass:"plate1",
              imgsrc: imgpath+"empty-plate.png"
            },
            {
              imgclass:"firstonesixth opa",
              imgsrc: imgpath+"one_sixth01.png"
            },
            {
              imgclass:"secondonesixth opa",
              imgsrc: imgpath+"one_sixth02.png"
            },
            {
              imgclass:"thirdonesixth opa",
              imgsrc: imgpath+"one_sixth03.png"
            },


            {
              imgclass:"plate2",
              imgsrc: imgpath+"empty-plate.png"
            },
            {
              imgclass:"firsttwosixth opa",
              imgsrc: imgpath+"one_sixth01.png"
            },
            {
              imgclass:"secondtwosixth opa",
              imgsrc: imgpath+"one_sixth02.png"
            },
            {
              imgclass:"thirdtwosixth opa",
              imgsrc: imgpath+"one_sixth03.png"
            },
            {
              imgclass:"forthtwosixth opa",
              imgsrc: imgpath+"one_sixth04.png"
            },

          ],

        },
      ],


    uppertextblock: [
      {
        textclass: "textclass textclass3 opa",
        textdata: data.string.p5text10,
        splitintofractionsflag: true

      },
      {
        textclass: "textclass textclass5 opa",
        textdata: data.string.p5text11,
        splitintofractionsflag: true

      },
      {
        textclass: "p11equalto1 opa",
        textdata: "="
      },
      {
        textclass: "p11equalto2 opa",
        textdata: "="
      },
      {
        datahighlightflag: true,
        textclass: "textclass middlehighlight cssfadein nwmiddlehighlight",
        textdata: data.string.p5text13,
      }
    ],

    fractionblock:[
        {
          fractionClass: "normalfraction p11frac1 opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac6
        },
        {
          fractionClass: "normalfraction p11frac2 blinkfrac opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac9
        },
        {
          fractionClass: "normalfraction p11frac3 opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac10
        },
        {
          fractionClass: "normalfraction p11frac4 blinkfrac opa",
          splitintofractionsflag: true,
          spandata: data.string.p5frac11
        },

    ],
  }
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext =0;

  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
  var preload;
	var timeoutvar = null;
	var current_sound;


	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
      {id: "s1_p1", src: soundAsset+"s5_p1.ogg"},
      {id: "s1_p2", src: soundAsset+"s5_p2.ogg"},
      {id: "s1_p3", src: soundAsset+"s5_p3.ogg"},
      {id: "s1_p4", src: soundAsset+"s5_p4.ogg"},
      {id: "s1_p5", src: soundAsset+"s5_p5.ogg"},
      {id: "s1_p6", src: soundAsset+"s5_p6.ogg"},
      {id: "s1_p7", src: soundAsset+"s5_p7.ogg"},
      {id: "s1_p8", src: soundAsset+"s5_p8.ogg"},
      {id: "s1_p9", src: soundAsset+"s5_p9.ogg"},
      {id: "s1_p10", src: soundAsset+"s5_p10.ogg"},
      {id: "s1_p11", src: soundAsset+"s5_p11.ogg"},
      {id: "s1_p12", src: soundAsset+"s5_p12.ogg"},
      {id: "s1_p13", src: soundAsset+"s5_p13.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
   Handlebars.registerPartial("fractioncontent", $("#fractioncontent-partial").html());

     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
    /*=====  End of data highlight function  ======*/

      function splitintofractions($splitinside){
      typeof $splitinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
        if($splitintofractions.length > 0){
          $.each($splitintofractions, function(index, value){
            $this = $(this);
            var tobesplitfraction = $this.html();
            if($this.hasClass('fraction')){
              tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
              tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
            }else{
              tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
              tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
            }


        tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
            $this.html(tobesplitfraction);
          });
        }
    }

    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag) {
    typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

  }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $('.sundarimg').hide(0);

    $(".textbox1").keypress(function (e) {
           if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {

              return false;
          }

        });

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    splitintofractions($(".fractionblock"));
    splitintofractions($board);
		vocabcontroller.findwords(countNext);

    //call instruction block controller
    // instructionblockcontroller($board);

    //call notifyuser
    // notifyuser($anydiv);
     switch(countNext){
        case 8:
          sound_player("s1_p9");
          $('.checkbutton1').click(function(){
            if($(".textbox1").val() == 3){
              $(".checkbutton1").css("background","#93C47D");
              $('.sundarwrong').hide(0);
              $('.sundarright').show(0);
              nav_button_controls(1000);
              play_correct_incorrect_sound(1);
            }
            else{
              $(".checkbutton1").css("background","#E06666");
              $('.sundarright').hide(0);
              $('.sundarwrong').show(0);
              play_correct_incorrect_sound(0);
            }
          });

          $('.checkbutton2').click(function(){
            if($(".textbox2").val() == 4){
              $(".checkbutton2").css("background","#93C47D");
            }
          });
        break;
        case 9:
          sound_player("s1_p9");
          $(".checkbutton1").css("background","#93C47D");
          $(".textbox1").val('3').prop('disabled', true);;

          $('.checkbutton2').click(function(){
            if($(".textbox2").val() == 4){
              $(".checkbutton2").css("background","#93C47D");
              $('.sundarwrong').hide(0);
              $('.sundarright').show(0);
              nav_button_controls(1000);
              play_correct_incorrect_sound(1);
            }
            else{
              $(".checkbutton2").css("background","#E06666");
              $('.sundarright').hide(0);
              $('.sundarwrong').show(0);
              play_correct_incorrect_sound(0);
            }
          });
        break;
        default:
        sound_player_nav("s1_p"+(countNext+1));
        break;
        }


    // find if there is linehorizontal div in the slide
    // var $linehorizontal = $board.find("div.linehorizontal");
    // if($linehorizontal.length > 0)
    // {
    //   $linehorizontal.attr('data-isdrawn', 'draw');
    // }
  }
  function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls();
		});
	}
/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


    /*OR, call templates like this if you have more than one template
    to call*/
    /*switch(countNext){
      case 0 : sometemplate()); break;
      .
      .
      .
      case 5 : someothertemplate();
    break;
      default : break;
    }*/

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

    // just for development purpose to see total slide vs current slide number
    // $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/
  // countNext+=1;

  $nextBtn.on("click", function(){
    countNext++;
    templateCaller();
  });

  $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
    countNext--;
    templateCaller();
    /* if footerNotificationHandler pageEndSetNotification was called then on click of
    previous slide button hide the footernotification */
    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
  });
  total_page = content.length;

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */

/*=====  End of Templates Controller Block  ======*/

});

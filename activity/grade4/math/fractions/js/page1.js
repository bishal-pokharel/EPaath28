var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	{
		// slide0
		contentblockadditionalclass : "firstpagebg",
		uppertextblock:[{
			textclass: "chaptertext",
			textdata: data.string.p1_s1
		}]
	},
	{
 		// slide1
 		contentblockadditionalclass : "bluishbg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "textblockleft60",
		uppertextblocknocenteradjust: true,
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p1_s2
		}],
		imageblockadditionalclass: "imageblockright40",
		imageblock:[
		  {
		   imagestoshow:[
		      {
		         imgclass:"plate",
		         imgsrc: imgpath+"empty-plate.png"
		      },{
		         imgclass:"roti",
		         imgsrc: imgpath+"rotione.png"
		      }
		    ]
		  }
		],
		fractionblock:[{
				fractionClass: "frac-1",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac1
			}]
	},
	{
		// slide2
		contentblockadditionalclass : "bluishbg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "textblockleft60",
		uppertextblocknocenteradjust: true,
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p1_s2
		},{
			textclass: "description",
			textdata: data.string.p1_s3
		}],
		imageblockadditionalclass: "imageblockright40",
		imageblock:[
		  {
		   imagestoshow:[
		      {
		         imgclass:"plate",
		         imgsrc: imgpath+"empty-plate.png"
		      },{
		         imgclass:"rotihalf1 rotihalf1animate",
		         imgsrc: imgpath+"half02.png"
		      },{
		         imgclass:"rotihalf2",
		         imgsrc: imgpath+"half01.png"
		      },{
		         imgclass:"arrow",
		         imgsrc: imgpath+"downarrow.png"
		      }
		    ]
		  }
		]
	},
	{
		// slide3
		contentblockadditionalclass : "bluishbg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "textblockleft60",
		uppertextblocknocenteradjust: true,
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p1_s2
		},{
			textclass: "description",
			textdata: data.string.p1_s3
		},{
			textclass: "description",
			datahighlightflag: true,
			splitintofractionsflag: true,
			textdata: data.string.p1_s4
		}],
		imageblockadditionalclass: "imageblockright40",
		imageblock:[
		  {
		   imagestoshow:[
		      {
		         imgclass:"plate",
		         imgsrc: imgpath+"empty-plate.png"
		      },{
		         imgclass:"rotihalf1",
		         imgsrc: imgpath+"half02.png"
		      },{
		         imgclass:"rotihalf2",
		         imgsrc: imgpath+"half01.png"
		      }
		    ]
		  }
		],
		fractionblock:[{
				fractionClass: "frac-2",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac2
			},{
				fractionClass: "frac-3",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac2
			}]
	},
	{
		// slide4
		contentblockadditionalclass : "bluishbg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "textblockleft60",
		uppertextblocknocenteradjust: true,
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p1_s2
		},{
			textclass: "description",
			textdata: data.string.p1_s3
		},{
			textclass: "description",
			datahighlightflag: true,
			splitintofractionsflag: true,
			textdata: data.string.p1_s4
		},{
			textclass: "description",
			textdata: data.string.p1_s5
		},{
			textclass: "description slowlyfadein fdin",
			textdata: data.string.p1_s6
		}],
		imageblockadditionalclass: "imageblockright40",
		imageblock:[
		  {
		   imagestoshow:[
		      {
		         imgclass:"plate",
		         imgsrc: imgpath+"empty-plate.png"
		      },{
		         imgclass:"rotisix6 fade_away3",
		         imgsrc: imgpath+"one_sixth06.png"
		      },{
		         imgclass:"rotisix5 fade_away2",
		         imgsrc: imgpath+"one_sixth05.png"
		      },{
		         imgclass:"rotisix4 fade_away1",
		         imgsrc: imgpath+"one_sixth04.png"
		      },{
		         imgclass:"rotihalf2",
		         imgsrc: imgpath+"half01.png"
		      }
		    ],
		    imagelabels:[{
		    	imagelabelclass: "platelabel slowlyfadein2",
		    	imagelabeldata: data.string.p1_s7
		    }]
		  }
		],
		fractionblock:[{
				fractionClass: "frac-2 fade_away4",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac2
			},{
				fractionClass: "frac-3",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac2
			}]
	},
	{
		// slide5
		contentblockadditionalclass : "bluishbg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "textblockleft60",
		uppertextblocknocenteradjust: true,
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p1_s8
		},{
			textclass: "description slowlyfadein2 fdin1",
			datahighlightflag: true,
			splitintofractionsflag: true,
			textdata: data.string.p1_s9
		}],
		imageblockadditionalclass: "imageblockright40",
		imageblock:[
		  {
		   imagestoshow:[
		      {
		         imgclass:"plate",
		         imgsrc: imgpath+"empty-plate.png"
		      },{
		         imgclass:"rotithird01 rotihalf1animate",
		         imgsrc: imgpath+"one_third01.png"
		      },{
		         imgclass:"rotithird02",
		         imgsrc: imgpath+"one_third02.png"
		      },{
		         imgclass:"rotithird03 rotithirdanimate",
		         imgsrc: imgpath+"one_third03.png"
		      }
		    ]
		  }
		],
		fractionblock:[{
				fractionClass: "frac-4",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac3
			},{
				fractionClass: "frac-5",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac3
			},{
				fractionClass: "frac-6",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac3
			}]

	},
	{
		// slide6
		contentblockadditionalclass : "bluishbg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "textblockleft60",
		uppertextblocknocenteradjust: true,
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p1_s8
		},{
			textclass: "description",
			datahighlightflag: true,
			splitintofractionsflag: true,
			textdata: data.string.p1_s9
		},{
			textclass: "description",
			textdata: data.string.p1_s10
		},{
			textclass: "description slowlyfadein",
			splitintofractionsflag: true,
			textdata: data.string.p1_s11
		}],
		imageblockadditionalclass: "imageblockright40",
		imageblock:[
		  {
		   imagestoshow:[
		      {
		         imgclass:"plate",
		         imgsrc: imgpath+"empty-plate.png"
		      },{
		         imgclass:"rotisix6 fade_away1",
		         imgsrc: imgpath+"one_sixth06.png"
		      },{
		         imgclass:"rotisix5 fade_away2",
		         imgsrc: imgpath+"one_sixth05.png"
		      },{
		         imgclass:"rotisix4 fade_away3",
		         imgsrc: imgpath+"one_sixth04.png"
		      },{
		         imgclass:"rotithird02",
		         imgsrc: imgpath+"one_third02.png"
		      },{
		         imgclass:"rotisix4 fade_away4",
		         imgsrc: imgpath+"one_sixth03.png"
		      }
		    ],
		    imagelabels:[{
		    	imagelabelclass: "platelabel slowlyfadein2",
		    	imagelabeldata: data.string.p1_s12
		    }]
		  }
		],
		fractionblock:[{
				fractionClass: "frac-4 fade_away4",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac3
			},{
				fractionClass: "frac-5 fade_away2",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac3
			},{
				fractionClass: "frac-6",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac3
			}]
	},
	{
		// slide7
		contentblockadditionalclass : "bluishbg",
	  	contentnocenteradjust: true,
	  	imageblockadditionalclass: "finalpageimageblock",
	    imageblock:[
	      {
	        imagestoshow:[
	          {
	            imgclass:"correctsundar",
	            imgsrc: imgpath+"sundarright.png"
	          },
	          {
	            imgclass:"wrongsundar",
	            imgsrc: imgpath+"sundarwrong.png"
	          },
	          {
	          	imgclass:"correct",
	            imgsrc: "images/correct.png"
	          },
	          {
	          	imgclass:"incorrect",
	            imgsrc: "images/wrong.png"
	          }
	        ],
	        imagelabels:[
	          {
	            imagelabelclass: "question",
	            imagelabeldata: data.string.p1_s13
	          }
	        ]
	      },{
	      	imageblockclass: "imagespecialclass1 forHover",
	      	imagestoshow:[
				{
					imgclass:"plate1",
					imgsrc:	imgpath + "empty-plate.png"
				}, {
					imgclass:"half",
					imgsrc:	imgpath + "half02.png"
				}
			],
	      	imagelabels:[
	          {
	            imagelabelclass: "imagelabel",
	            imagelabeldata: data.string.p1_s7
	          }
	        ]
	      },{
	      	imageblockclass: "imagespecialclass2 forHover",
	      	imagestoshow:[
				{
					imgclass:"plate1",
					imgsrc:	imgpath + "empty-plate.png"
				}, {
					imgclass:"onethird",
					imgsrc: imgpath + "one_third02.png"
				}
	      	],
	      	imagelabels:[
	          {
	            imagelabelclass: "imagelabel",
	            imagelabeldata: data.string.p1_s12
	          }
	        ]
	      }
   		 ]
  }
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;


	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
      {id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
      {id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
      {id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
      {id: "s1_p4", src: soundAsset+"s1_p4.ogg"},
			{id: "s1_p5", src: soundAsset+"s1_p5.ogg"},
			{id: "s1_p6", src: soundAsset+"s1_p6.ogg"},
			{id: "s1_p7", src: soundAsset+"s1_p7.ogg"},
			{id: "s1_p8", src: soundAsset+"s1_p8.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
   Handlebars.registerPartial("fractioncontent", $("#fractioncontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/

    /*===== This function splits the string in data into convential fraction used in mathematics =====*/
    function splitintofractions($splitinside){
   		typeof $splitinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
       	if($splitintofractions.length > 0){
       		$.each($splitintofractions, function(index, value){
	        	$this = $(this);
	        	var tobesplitfraction = $this.html();
	        	if($this.hasClass('fraction')){
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
	        	}else{
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
	        	}


				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
	        	$this.html(tobesplitfraction);
	        });
       	}
   	}
   	/*===== split into fractions end =====*/

    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    splitintofractions($board);
		vocabcontroller.findwords(countNext);
		// splitintofractions($(".fractionblock"));
	    switch(countNext){
	   		case 7:
				sound_player("s1_p8");
	   			var $correctsundar = $(".correctsundar");
	   			var $wrongsundar = $(".wrongsundar");
	   			var $correct = $(".correct");
	   			var $incorrect = $(".incorrect");
	   			var $imagespecialclass1 = $(".imagespecialclass1");
	   			var $imagespecialclass2 = $(".imagespecialclass2");
	   			var correctclicked = false;
	   			$imagespecialclass1.click(function(){
	   				if(correctclicked){
	   					return false;
	   				}
	   				$imagespecialclass2.css('border','');
	   				$(this).css({"border": "0.4em solid #87F100",
	   							"border-radius": "10%"});
	   				if($wrongsundar.is(':visible')){
	   					$wrongsundar.hide(0);
	   					$incorrect.hide(0);
	   				}
	   				$correctsundar.show(0);
	   				$correct.show(0);
	   			nav_button_controls(1000);
	   				correctclicked = true;
	   				$(this).removeClass('forHover');
					play_correct_incorrect_sound(1);
	   			});
	   			$imagespecialclass2.click(function(){
	   				if(correctclicked){
	   					return false;
	   				}
	   				$imagespecialclass1.css('border','');
	   				$(this).css({"border": "0.4em solid red",
	   							"border-radius": "10%"});
	   				if($correctsundar.is(':visible')){
	   					$correctsundar.hide(0);
	   					$correct.hide(0);
	   				}
	   				$(this).removeClass('forHover');
	   				$wrongsundar.show(0);
	   				$incorrect.show(0);
					play_correct_incorrect_sound(0);
	   			});
	   			break;
	   		default:
				sound_player_nav("s1_p"+(countNext+1));
	   			break;
	    }
  }
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls();
		});
	}
/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

    // just for development purpose to see total slide vs current slide number
    // $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/
  // countNext+=1;

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
	});
/*=====  End of Templates Controller Block  ======*/

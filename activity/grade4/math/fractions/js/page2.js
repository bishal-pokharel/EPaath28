var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	{
		// slide0
		contentblockadditionalclass : "bluebg",
		uppertextblock:[{
			textclass: "description1",
			textdata: data.string.p2_s1
		}]
	},
	{
 		// slide1
 		contentblockadditionalclass : "bluishbg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "textblockleft60",
		uppertextblocknocenteradjust: true,
		uppertextblock:[{
			textclass: "description",
			datahighlightflag: true,
			splitintofractionsflag: true,
			textdata: data.string.p2_s3
		},{
			textclass: "description fadein3",
			datahighlightflag: true,
			textdata: data.string.p2_s4
		}],
		imageblockadditionalclass: "imageblockright40",
		imageblock:[
		  {
		   imageblockclass: "incorrect_container animatetotop",
		   imagestoshow:[
		      {
		         imgclass:"plate1",
		         imgsrc: imgpath+"empty-plate.png"
		      },{
		         imgclass:"roti1",
		         imgsrc: imgpath+"rotione.png"
		      }, {
		         imgclass:"plate2",
		         imgsrc: imgpath+"empty-plate.png"
		      },{
		         imgclass:"roti2",
		         imgsrc: imgpath+"rotione.png"
		      },{
		      	 imgclass:"incorrect",
		         imgsrc: "images/wrong.png"
		      }
		    ]
		  },{
		   imageblockclass: "correct_container fadein3",
		   imagestoshow:[
		      {
		         imgclass:"plate0",
		         imgsrc: imgpath+"empty-plate.png"
		      },{
		         imgclass:"rotihalf1 rotihalf1animate",
		         imgsrc: imgpath+"half02.png"
		      }, {
		         imgclass:"rotihalf2",
		         imgsrc: imgpath+"half01.png"
		      },{
		      	 imgclass:"correct",
		         imgsrc: "images/correct.png"
		      }
		    ]
		  }
		]
	},
	{
		// slide2
		contentblockadditionalclass : "bluishbg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "textblockleft60",
		uppertextblocknocenteradjust: true,
		uppertextblock:[{
			textclass: "description",
			datahighlightflag: true,
			splitintofractionsflag: true,
			textdata: data.string.p2_s5
		},{
			textclass: "description fadein3",
			datahighlightflag: true,
			textdata: data.string.p2_s6
		}],
		imageblockadditionalclass: "imageblockright40",
		imageblock:[
		  {
		   imageblockclass: "incorrect_container animatetotop",
		   imagestoshow:[
		      {
		         imgclass:"plate1_3",
		         imgsrc: imgpath+"empty-plate.png"
		      },{
		         imgclass:"roti1_3",
		         imgsrc: imgpath+"rotione.png"
		      }, {
		         imgclass:"plate2_3",
		         imgsrc: imgpath+"empty-plate.png"
		      },{
		         imgclass:"roti2_3",
		         imgsrc: imgpath+"rotione.png"
		      },{
		         imgclass:"plate3_3",
		         imgsrc: imgpath+"empty-plate.png"
		      },{
		         imgclass:"roti3_3",
		         imgsrc: imgpath+"rotione.png"
		      },{
		      	 imgclass:"incorrect",
		         imgsrc: "images/wrong.png"
		      }
		    ]
		  },{
		   imageblockclass: "correct_container fadein3",
		   imagestoshow:[
		      {
		         imgclass:"plate0",
		         imgsrc: imgpath+"empty-plate.png"
		      },{
		         imgclass:"rotionethird1 rotihalf1animate",
		         imgsrc: imgpath+"one_third01.png"
		      },{
		         imgclass:"rotionethird2 rotithirdanimate",
		         imgsrc: imgpath+"one_third03.png"
		      },{
		         imgclass:"rotionethird3",
		         imgsrc: imgpath+"one_third02.png"
		      },{
		      	 imgclass:"correct",
		         imgsrc: "images/correct.png"
		      }
		    ]
		  }
		]
	},
	{

	// slide3
	contentblockadditionalclass : "bluishbg",
	contentnocenteradjust: true,
	imageblockadditionalclass: "additional_image_block",
	imageblock:[{
		imageblockclass : "upperblock50 fade1",
		imagestoshow : [{
			imgclass : "platep3_1",
			imgsrc : imgpath + "empty-plate.png"
		}, {
			imgclass : "rotip3_1",
			imgsrc : imgpath + "rotione.png"
		},{
			imgclass : "platep3_2",
			imgsrc : imgpath + "empty-plate.png"
		}, {
			imgclass : "rotip3_2",
			imgsrc : imgpath + "rotione.png"
		},{
			imgclass : "platep3_3",
			imgsrc : imgpath + "empty-plate.png"
		}, {
			imgclass : "rotip3_3",
			imgsrc : imgpath + "rotione.png"
		},{
			imgclass : "platep3_4",
			imgsrc : imgpath + "empty-plate.png"
		}, {
			imgclass : "rotip3_4",
			imgsrc : imgpath + "rotione.png"
		},{
			imgclass : "platep3_5",
			imgsrc : imgpath + "empty-plate.png"
		}, {
			imgclass : "rotip3_5",
			imgsrc : imgpath + "rotione.png"
		}],
		imagelabels:[{
			imagelabelclass: "label1",
			imagelabeldata: data.string.p2_s7
		},{
			imagelabelclass: "label2",
			imagelabeldata: data.string.p2_s9
		}]
	}, {
		imageblockclass : "lowerblock50",
		imagestoshow : [{
			imgclass : "platep3_6",
			imgsrc : imgpath + "empty-plate.png"
		}, {
			imgclass : "rotip3_6",
			imgsrc : imgpath + "half02.png"
		},{
			imgclass : "platep3_7",
			imgsrc : imgpath + "empty-plate.png"
		}, {
			imgclass : "rotip3_7",
			imgsrc : imgpath + "one_third01.png"
		}],
		imagelabels:[{
			imagelabelclass: "label1",
			datahighlightflag: true,
			splitintofractionsflag: true,
			imagelabeldata: data.string.p2_s8
		},{
			imagelabelclass: "label3",
			imagelabeldata: data.string.p2_s9
		}]
	}]

	},
	{
		// slide4
		contentblockadditionalclass : "bluishbg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "textblocktop50",
		uppertextblocknocenteradjust: true,
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p2_s11
		},{
			textclass: "description fadein1",
			textdata: data.string.p2_s12
		},{
			textclass: "description fadein2",
			datahighlightflag: true,
			textdata: data.string.p2_s13
		},{
			textclass: "description fadein3",
			textdata: data.string.p2_s14
		},{
			textclass: "description fadein4",
			textdata: data.string.p2_s15
		}],
		imageblockadditionalclass: "imageblockbottom50",
		imageblock:[
		  {
		   imageblockclass: "imagebox",
		   imagestoshow:[
		      {
		         imgclass:"imageboxplate",
		         imgsrc: imgpath+"empty-plate.png"
		      },
		       {
		         imgclass:"imageboxbread",
		         imgsrc: imgpath+"half02.png"
		      }
		    ],
		    imagelabels:[{
		    	imagelabelclass: "imageboxlabel fadein1",
		    	imagelabeldata: data.string.p2_s9
		    }]
		  },{
		   imageblockclass: "imagebox fadein1",
		   imagestoshow:[
		      {
		         imgclass:"imageboxplate",
		         imgsrc: imgpath+"empty-plate.png"
		      },
		       {
		         imgclass:"imageboxbread",
		         imgsrc: imgpath+"one_third01.png"
		      }
		    ],
		    imagelabels:[{
		    	imagelabelclass: "imageboxlabel fadein2",
		    	imagelabeldata: data.string.p2_s9
		    }]
		  },{
		   imageblockclass: "imagebox fadein2",
		   imagestoshow:[
		      {
		         imgclass:"imageboxplate",
		         imgsrc: imgpath+"empty-plate.png"
		      },
		       {
		         imgclass:"imageboxbread",
		         imgsrc: imgpath+"one_forth.png"
		      }
		    ],
		    imagelabels:[{
		    	imagelabelclass: "imageboxlabel fadein3",
		    	imagelabeldata: data.string.p2_s9
		    }]
		  },{
		   imageblockclass: "imagebox fadein3",
		   imagestoshow:[
		      {
		         imgclass:"imageboxplate",
		         imgsrc: imgpath+"empty-plate.png"
		      },
		       {
		         imgclass:"imageboxbread",
		         imgsrc: imgpath+"one_fifth.png"
		      }
		    ],
		    imagelabels:[{
		    	imagelabelclass: "imageboxlabel fadein4",
		    	imagelabeldata: data.string.p2_s9
		    }]
		  },{
		   imageblockclass: "imagebox fadein4",
		   imagestoshow:[
		      {
		         imgclass:"imageboxplate",
		         imgsrc: imgpath+"empty-plate.png"
		      },
		       {
		         imgclass:"imageboxbread",
		         imgsrc: imgpath+"one_sixth01.png"
		      }
		    ]
		  }
		],
		fractionblock:[{
				fractionClass: "frac-1",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac2
			},{
				fractionClass: "frac-2 fadein1",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac3
			},{
				fractionClass: "frac-3 fadein2",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac4
			},{
				fractionClass: "frac-4 fadein3",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac5
			},{
				fractionClass: "frac-5 fadein5",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac6
			}]
	},
	{
		// slide5
		contentblockadditionalclass : "bluishbg",
		contentnocenteradjust: true,

		imageblockadditionalclass: "imageblockbottom50",
		imageblock:[
		  {
		   imageblockclass: "imagebox",
		   imagestoshow:[
		      {
		         imgclass:"imageboxplate",
		         imgsrc: imgpath+"empty-plate.png"
		      },
		       {
		         imgclass:"imageboxbread",
		         imgsrc: imgpath+"half02.png"
		      }
		    ],
		    imagelabels:[{
		    	imagelabelclass: "imageboxlabel",
		    	imagelabeldata: data.string.p2_s9
		    },{
		    	imagelabelclass: "imageboxlabel2",
		    	imagelabeldata: data.string.p2_s16
		    }]
		  },{
		   imageblockclass: "imagebox",
		   imagestoshow:[
		      {
		         imgclass:"imageboxplate",
		         imgsrc: imgpath+"empty-plate.png"
		      },
		       {
		         imgclass:"imageboxbread",
		         imgsrc: imgpath+"one_third01.png"
		      }
		    ],
		    imagelabels:[{
		    	imagelabelclass: "imageboxlabel",
		    	imagelabeldata: data.string.p2_s9
		    },{
		    	imagelabelclass: "imageboxlabel2",
		    	imagelabeldata: data.string.p2_s17
		    }]
		  },{
		   imageblockclass: "imagebox",
		   imagestoshow:[
		      {
		         imgclass:"imageboxplate",
		         imgsrc: imgpath+"empty-plate.png"
		      },
		       {
		         imgclass:"imageboxbread",
		         imgsrc: imgpath+"one_forth.png"
		      }
		    ],
		    imagelabels:[{
		    	imagelabelclass: "imageboxlabel",
		    	imagelabeldata: data.string.p2_s9
		    },{
		    	imagelabelclass: "imageboxlabel2",
		    	imagelabeldata: data.string.p2_s18
		    }]
		  },{
		   imageblockclass: "imagebox",
		   imagestoshow:[
		      {
		         imgclass:"imageboxplate",
		         imgsrc: imgpath+"empty-plate.png"
		      },
		       {
		         imgclass:"imageboxbread",
		         imgsrc: imgpath+"one_fifth.png"
		      }
		    ],
		    imagelabels:[{
		    	imagelabelclass: "imageboxlabel",
		    	imagelabeldata: data.string.p2_s9
		    },{
		    	imagelabelclass: "imageboxlabel2",
		    	imagelabeldata: data.string.p2_s19
		    }]
		  },{
		   imageblockclass: "imagebox",
		   imagestoshow:[
		      {
		         imgclass:"imageboxplate",
		         imgsrc: imgpath+"empty-plate.png"
		      },
		       {
		         imgclass:"imageboxbread",
		         imgsrc: imgpath+"one_sixth01.png"
		      }
		    ],
		    imagelabels:[{
		    	imagelabelclass: "imageboxlabel2",
		    	imagelabeldata: data.string.p2_s20
		    }]
		  },{
		  	imagelabels:[{
		    	imagelabelclass: "imageboxlabel3",
		    	imagelabeldata: data.string.p2_s26
		    }]
		  }
		],

		imageblockadditionalclass2: "imageblocktop50",
		imageblock2:[
		  {
		   imageblockclass: "imagebox",
		   imagestoshow:[
		      {
		         imgclass:"imageboxplate",
		         imgsrc: imgpath+"empty-plate.png"
		      },
		       {
		         imgclass:"imageboxbread",
		         imgsrc: imgpath+"one_sixth01.png"
		      }
		    ],
		    imagelabels:[{
		    	imagelabelclass: "imageboxlabel fadein1",
		    	imagelabeldata: data.string.p2_s10
		    },{
		    	imagelabelclass: "imageboxlabel2",
		    	imagelabeldata: data.string.p2_s20
		    }]
		  },{
		   imageblockclass: "imagebox fadein1",
		   imagestoshow:[
		      {
		         imgclass:"imageboxplate",
		         imgsrc: imgpath+"empty-plate.png"
		      },
		       {
		         imgclass:"imageboxbread",
		         imgsrc: imgpath+"one_third02.png"
		      }
		    ],
		    imagelabels:[{
		    	imagelabelclass: "imageboxlabel fadein2",
		    	imagelabeldata: data.string.p2_s10
		    },{
		    	imagelabelclass: "imageboxlabel2",
		    	imagelabeldata: data.string.p2_s21
		    }]
		  },{
		   imageblockclass: "imagebox fadein2",
		   imagestoshow:[
		      {
		         imgclass:"imageboxplate",
		         imgsrc: imgpath+"empty-plate.png"
		      },
		       {
		         imgclass:"imageboxbread",
		         imgsrc: imgpath+"half01.png"
		      }
		    ],
		    imagelabels:[{
		    	imagelabelclass: "imageboxlabel fadein3",
		    	imagelabeldata: data.string.p2_s10
		    },{
		    	imagelabelclass: "imageboxlabel2",
		    	imagelabeldata: data.string.p2_s22
		    }]
		  },{
		   imageblockclass: "imagebox fadein3",
		   imagestoshow:[
		      {
		         imgclass:"imageboxplate",
		         imgsrc: imgpath+"empty-plate.png"
		      },
		       {
		         imgclass:"imageboxbread",
		         imgsrc: imgpath+"one_third02.png"
		      },{
		         imgclass:"imageboxbread",
		         imgsrc: imgpath+"one_third03.png"
		      }
		    ],
		    imagelabels:[{
		    	imagelabelclass: "imageboxlabel fadein4",
		    	imagelabeldata: data.string.p2_s10
		    },{
		    	imagelabelclass: "imageboxlabel2",
		    	imagelabeldata: data.string.p2_s23
		    }]
		  },{
		   imageblockclass: "imagebox fadein4",
		   imagestoshow:[
		      {
		         imgclass:"imageboxplate",
		         imgsrc: imgpath+"empty-plate.png"
		      },
		       {
		         imgclass:"imageboxbread",
		         imgsrc: imgpath+"half01.png"
		      },
		      {
		         imgclass:"imageboxbread",
		         imgsrc: imgpath+"one_sixth05.png"
		      },
		      {
		         imgclass:"imageboxbread",
		         imgsrc: imgpath+"one_third03.png"
		      }
		    ],
		    imagelabels:[{
		    	imagelabelclass: "imageboxlabel2",
		    	imagelabeldata: data.string.p2_s24
		    }]
		  },{
		  	imagelabels:[{
		    	imagelabelclass: "imageboxlabel3",
		    	imagelabeldata: data.string.p2_s25
		    }]
		  }
		],
		fractionblock:[{
				fractionClass: "frac-1",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac2
			},{
				fractionClass: "frac-2",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac3
			},{
				fractionClass: "frac-3",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac4
			},{
				fractionClass: "frac-4",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac5
			},{
				fractionClass: "frac-5",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac6
			},{
				fractionClass: "frac-1_top",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac6
			},{
				fractionClass: "frac-2_top fadein1",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac7
			},{
				fractionClass: "frac-3_top fadein2",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac8
			},{
				fractionClass: "frac-4_top fadein3",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac9
			},{
				fractionClass: "frac-5_top fadein4",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac10
			}]

	},{
		// slide6
		contentblockadditionalclass : "bluishbg",
	  	contentnocenteradjust: true,
	  	imageblockadditionalclass: "finalpageimageblock",
	    imageblock:[
	      {
	        imagestoshow:[
	          {
	            imgclass:"correctsundar",
	            imgsrc: imgpath+"sundarright.png"
	          },
	          {
	            imgclass:"wrongsundar",
	            imgsrc: imgpath+"sundarwrong.png"
	          },
	          {
	          	imgclass:"correct02",
	            imgsrc: "images/correct.png"
	          },
	          {
	          	imgclass:"incorrect02",
	            imgsrc: "images/wrong.png"
	          }
	        ],
	        imagelabels:[
	          {
	            imagelabelclass: "question",
	            imagelabeldata: data.string.p1_s13
	          }
	        ]
	      },{
	      	imageblockclass: "imagespecialclass1 forHover small",
	      	imagestoshow:[
				{
					imgclass:"plate1_q",
					imgsrc:	imgpath + "empty-plate.png"
				}, {
					imgclass:"onethird",
					imgsrc: imgpath + "one_third02.png"
				}
			],
	      	imagelabels:[
	          {
	            imagelabelclass: "imagelabel",
	            imagelabeldata: data.string.p2_s21
	          }
	        ]
	      },{
	      	imageblockclass: "imagespecialclass2 forHover big",
	      	imagestoshow:[
				{
					imgclass:"plate1_q",
					imgsrc:	imgpath + "empty-plate.png"
				}, {
					imgclass:"half",
					imgsrc:	imgpath + "half02.png"
				}, {
					imgclass:"half",
					imgsrc:	imgpath + "one_third02.png"
				}
	      	],
	      	imagelabels:[
	          {
	            imagelabelclass: "imagelabel",
	            imagelabeldata: data.string.p2_s24
	          }
	        ]
	      }
   		 ]
  },{
		// slide7
		contentblockadditionalclass : "bluishbg",
	  	contentnocenteradjust: true,
	  	imageblockadditionalclass: "finalpageimageblock",
	    imageblock:[
	      {
	        imagestoshow:[
	          {
	            imgclass:"correctsundar",
	            imgsrc: imgpath+"sundarright.png"
	          },
	          {
	            imgclass:"wrongsundar",
	            imgsrc: imgpath+"sundarwrong.png"
	          },
	          {
	          	imgclass:"correct02",
	            imgsrc: "images/correct.png"
	          },
	          {
	          	imgclass:"incorrect02",
	            imgsrc: "images/wrong.png"
	          }
	        ],
	        imagelabels:[
	          {
	            imagelabelclass: "question",
	            imagelabeldata: data.string.p1_s13
	          }
	        ]
	      },{
	      	imageblockclass: "imagespecialclass1 forHover big",
	      	imagestoshow:[
				{
					imgclass:"plate1_q",
					imgsrc:	imgpath + "empty-plate.png"
				}, {
					imgclass:"half",
					imgsrc:	imgpath + "one_third02.png"
				}
			],
	      	imagelabels:[
	          {
	            imagelabelclass: "imagelabel",
	            imagelabeldata: data.string.p2_s17
	          }
	        ]
	      },{
	      	imageblockclass: "imagespecialclass2 forHover small",
	      	imagestoshow:[
				{
					imgclass:"plate1_q",
					imgsrc:	imgpath + "empty-plate.png"
				}, {
					imgclass:"onethird",
					imgsrc:	imgpath + "one_forth.png"
				}
	      	],
	      	imagelabels:[
	          {
	            imagelabelclass: "imagelabel",
	            imagelabeldata: data.string.p2_s18
	          }
	        ]
	      }
   		 ]
  },{
		// slide8
		contentblockadditionalclass : "bluishbg",
	  	contentnocenteradjust: true,
	  	imageblockadditionalclass: "finalpageimageblock",
	    imageblock:[
	      {
	        imagestoshow:[
	          {
	            imgclass:"correctsundar",
	            imgsrc: imgpath+"sundarright.png"
	          },
	          {
	            imgclass:"wrongsundar",
	            imgsrc: imgpath+"sundarwrong.png"
	          },
	          {
	          	imgclass:"correct02",
	            imgsrc: "images/correct.png"
	          },
	          {
	          	imgclass:"incorrect02",
	            imgsrc: "images/wrong.png"
	          }
	        ],
	        imagelabels:[
	          {
	            imagelabelclass: "question",
	            imagelabeldata: data.string.p2_s27
	          }
	        ]
	      }
   		 ],
   		 fractionblock:[{
				fractionClass: "frac-1_q small",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac11
			},{
				fractionClass: "frac-2_q big",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac12
			}],
		lowertextblockadditionalclass: "textblockbottom30",
		lowertextblock:[
			{
				textclass : "leftlabel",
				textdata: data.string.p2_s28
			},
			{
				textclass : "rightlabel",
				textdata: data.string.p2_s32
			}
		]

  },{
		// slide9
		contentblockadditionalclass : "bluishbg",
	  	contentnocenteradjust: true,
	  	imageblockadditionalclass: "finalpageimageblock",
	    imageblock:[
	      {
	        imagestoshow:[
	          {
	            imgclass:"correctsundar",
	            imgsrc: imgpath+"sundarright.png"
	          },
	          {
	            imgclass:"wrongsundar",
	            imgsrc: imgpath+"sundarwrong.png"
	          },
	          {
	          	imgclass:"correct02",
	            imgsrc: "images/correct.png"
	          },
	          {
	          	imgclass:"incorrect02",
	            imgsrc: "images/wrong.png"
	          }
	        ],
	        imagelabels:[
	          {
	            imagelabelclass: "question",
	            imagelabeldata: data.string.p2_s27
	          }
	        ]
	      }
   		 ],
   		 fractionblock:[{
				fractionClass: "frac-1_q big",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac13
			},{
				fractionClass: "frac-2_q small",
				splitintofractionsflag: true,
				spandata: data.string.p1_s1_frac14
			}],
		lowertextblockadditionalclass: "textblockbottom30",
		lowertextblock:[
			{
				textclass : "leftlabel",
				textdata: data.string.p2_s30
			},
			{
				textclass : "rightlabel",
				textdata: data.string.p2_s31
			}
		]
  }
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;


	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
      {id: "s1_p1", src: soundAsset+"s2_p1.ogg"},
      {id: "s1_p2", src: soundAsset+"s2_p2.ogg"},
      {id: "s1_p3", src: soundAsset+"s2_p3.ogg"},
      {id: "s1_p4", src: soundAsset+"s2_p4.ogg"},
			{id: "s1_p5", src: soundAsset+"s2_p5.ogg"},
			{id: "s1_p6", src: soundAsset+"s2_p6.ogg"},
			{id: "s1_p7", src: soundAsset+"s2_p7.ogg"},
			{id: "s1_p9", src: soundAsset+"s2_p9.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
   Handlebars.registerPartial("fractioncontent", $("#fractioncontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
    /*=====  End of data highlight function  ======*/

	 /*===== This function splits the string in data into convential fraction used in mathematics =====*/
    function splitintofractions($splitinside){
   		typeof $splitinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
       	if($splitintofractions.length > 0){
       		$.each($splitintofractions, function(index, value){
	        	$this = $(this);
	        	var tobesplitfraction = $this.html();
	        	if($this.hasClass('fraction')){
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
	        	}else{
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
	        	}


				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
	        	$this.html(tobesplitfraction);
	        });
       	}
   	}
	/*===== split into fractions end =====*/

    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
		splitintofractions($board);
		vocabcontroller.findwords(countNext);
	    switch(countNext){
	   		case 6:
	   		case 7:
	   		case 8:
	   		case 9:
					sound_player("s1_p"+(countNext+1));
	   			var $correctsundar = $(".correctsundar");
	   			var $wrongsundar = $(".wrongsundar");
	   			var $correct = $(".correct02");
	   			var $incorrect = $(".incorrect02");
	   			var $big = $(".big");
	   			var $small = $(".small");
	   			var correctclicked = false;
	   			$big.click(function(){
	   				if(correctclicked){
	   					return false;
	   				}
	   				if(countNext > 7){
	   					$small.css('background-color','#6D9EEB');
	   					$(this).css('background-color','#93C47D');
	   				}else {
	   					$small.css('border','');
	   					$(this).css({"border": "0.4em solid #87F100",
	   							"border-radius": "10%"});
	   				}
	   				if($wrongsundar.is(':visible')){
	   					$wrongsundar.hide(0);
	   					$incorrect.hide(0);
	   				}
	   				$correctsundar.show(0);
	   				$correct.show(0);
	   				correctclicked = true;
	   			nav_button_controls(500);
					play_correct_incorrect_sound(1);
					$(this).removeClass('forHover');
	   			});
	   			$small.click(function(){
	   				if(correctclicked){
	   					return false;
	   				}
	   				if(countNext > 7){
	   					$big.css('background-color','#6D9EEB');
		   				$(this).css('background-color','#E06666');
					}else{
						$big.css('border','');
						$(this).css({"border": "0.4em solid #D0553E",
									"border-radius": "10%"});
					}
	   				if($correctsundar.is(':visible')){
	   					$correctsundar.hide(0);
	   					$correct.hide(0);
	   				}
	   				$(this).removeClass('forHover');
	   				$wrongsundar.show(0);
	   				$incorrect.show(0);
					play_correct_incorrect_sound(0);
	   			});
	   			break;
	   		default:
				sound_player_nav("s1_p"+(countNext+1));
	   		break;
	    }
  }
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls();
		});
	}
/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

    // just for development purpose to see total slide vs current slide number
    // $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/
  // countNext+=1;

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
	});
/*=====  End of Templates Controller Block  ======*/

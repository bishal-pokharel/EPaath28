var imgpath = $ref+"/images/";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
var soundAsset = $ref+"/sounds/"+$lang+"/";
/* any of the dropped div can accept any of the fractions in the first place and after one of the fractions
 * has been dropped on the box, other boxes should reject that fraction and accept only other fractions while
 * that box accepts only that fraction(like fraction = 1/3, 2/3.....) */

/* accept array is used to trigger that accepting event, all of the elements act as boolean flag, if value is 0, common
 * class among all fractions->fractions is removed and is left with like_fraction_class -> f_id_x
 */
var accept_array = [1,1,1,1];
/* dropped_items to count the number of items dropped in each of the dropboxes */
var dropped_items = [0,0,0,0];

Array.prototype.shufflearray = function() {
    var i = this.length,
        j, temp;
    while (--i > 0) {
        j = Math.floor(Math.random() * (i + 1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
    return this;
};


var content=[
  //page 0
  {
  	contentblockadditionalclass : "bluishbg",
  	contentnocenteradjust: true,
  	uppertextblockadditionalclass: "instruct_text",
  	uppertextblock: [{
  		textdata: data.string.p4text1,
  		textclass: 'description'
  	}]
  },
  //page 1
  /* 18 fractions are included in which 4 is only displayed at beginning */
  {
  		contentblockadditionalclass: "bluishbg",
  		contentnocenteradjust: true,
  		uppertextblockadditionalclass: "instruction_text_1",
	  	uppertextblock: [{
	  		textdata: data.string.p4text2,
	  		textclass: ''
	  	}],

	  	fractionblockadditionalclass: 'frac_ques',
	  	fractionblock:[{
			fractionClass: "fractions frac_0 draggableposition1 draggable",
			splitintofractionsflag: true,
			spandata: data.string.p4frac1
		},{
			fractionClass: "fractions frac_1 draggableposition2 draggable",
			splitintofractionsflag: true,
			spandata: data.string.p4frac2
		},{
			fractionClass: "fractions frac_2 draggableposition3 draggable",
			splitintofractionsflag: true,
			spandata: data.string.p4frac3
		},{
			fractionClass: "fractions frac_3 draggableposition4 draggable",
			splitintofractionsflag: true,
			spandata: data.string.p4frac4
		},{
			fractionClass: "fractions frac_4 its_hidden draggable",
			splitintofractionsflag: true,
			spandata: data.string.p4frac4
		},{
			fractionClass: "fractions frac_5 its_hidden draggable",
			splitintofractionsflag: true,
			spandata: data.string.p4frac4
		},{
			fractionClass: "fractions frac_6 its_hidden draggable",
			splitintofractionsflag: true,
			spandata: data.string.p4frac4
		},{
			fractionClass: "fractions frac_7 its_hidden draggable",
			splitintofractionsflag: true,
			spandata: data.string.p4frac4
		},{
			fractionClass: "fractions frac_8 its_hidden draggable",
			splitintofractionsflag: true,
			spandata: data.string.p4frac4
		},{
			fractionClass: "fractions frac_9 its_hidden draggable",
			splitintofractionsflag: true,
			spandata: data.string.p4frac4
		},{
			fractionClass: "fractions frac_10 its_hidden draggable",
			splitintofractionsflag: true,
			spandata: data.string.p4frac4
		},{
			fractionClass: "fractions frac_11 its_hidden draggable",
			splitintofractionsflag: true,
			spandata: data.string.p4frac4
		},{
			fractionClass: "fractions frac_12 its_hidden draggable",
			splitintofractionsflag: true,
			spandata: data.string.p4frac4
		},{
			fractionClass: "fractions frac_13 its_hidden draggable",
			splitintofractionsflag: true,
			spandata: data.string.p4frac4
		},{
			fractionClass: "fractions frac_14 its_hidden draggable",
			splitintofractionsflag: true,
			spandata: data.string.p4frac4
		},{
			fractionClass: "fractions frac_15 its_hidden draggable",
			splitintofractionsflag: true,
			spandata: data.string.p4frac4
		}],
		specialdropdiv:[{
	  		droplabelclass: "fraction_drop drop_0 emptybox",
	  		dropdiv: "itemdrop_0"
	  	},{
	  		droplabelclass: "fraction_drop drop_1 emptybox",
	  		dropdiv: "itemdrop_1"
	  	},{
	  		droplabelclass: "fraction_drop drop_2 emptybox",
	  		dropdiv: "itemdrop_2"
	  	},{
	  		droplabelclass: "fraction_drop drop_3 emptybox",
	  		dropdiv: "itemdrop_3"
	  	}]
  }
];


$(function() {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;

	loadTimelineProgress($total_page, countNext + 1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
  var preload;
	var timeoutvar = null;
	var current_sound;


	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
      {id: "s1_p1", src: soundAsset+"s4_p2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
	/*==================================================
	 =            Handlers and helpers Block            =
	 ==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("fractioncontent", $("#fractioncontent-partial").html());

	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	/*=====  End of data highlight function  ======*/


	function splitintofractions($splitinside){
   		typeof $splitinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
       	if($splitintofractions.length > 0){
       		$.each($splitintofractions, function(index, value){
	        	$this = $(this);
	        	var tobesplitfraction = $this.html();
	        	if($this.hasClass('fraction')){
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
	        	}else{
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
	        	}


				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
	        	$this.html(tobesplitfraction);
	        });
       	}
   	}


	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templateCaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templateCaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

	}

	/*=====  End of user navigation controller function  ======*/

	/*=====  End of Handlers and helpers Block  ======*/

	/*=======================================
	 =            Templates Block            =
	 =======================================*/
	/*=================================================
	 =            general template function            =
	 =================================================*/
	var source = $("#general-template").html();
	var template = Handlebars.compile(source);

	function generalTemplate() {
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
		splitintofractions($(".fractionblock"));
		vocabcontroller.findwords(countNext);

		//call notifyuser
		// notifyuser($anydiv);

		switch(countNext){
      case 0:
      play_diy_audio();
      nav_button_controls(2000);
      break;
			case 1:
      sound_player("s1_p1");
				var no_of_items = 16;
				/* if draggable pointer events are not disabled then revert position will be the position at the
				 * instance of mouse event, thus is animation is half complete position will be messed up */
				disable_pointer_events('.draggable', 1500);
				/* make array[1,2,3,4] and randomize the positon of elements for eg [4,2,1,3] and call it frac_repeat arr
				 * here 1 represents 3, 2 represents 4, 3 = 5 and 4 = 6, making total sum to be 18 and generates array like
				 * frac_array = [3,3,3,3,3,3,4,4,4,4,5,5,5,6,6,6,6,6]
				 */


				// var frac_repeat_arr = sandy_random_display(4);
				// var frac_array = [];
				// for(var j=0; j< 4; j++){
					// for(var i=0; i<(frac_repeat_arr[j]+2); i++){
						// frac_array.push(j+6);
					// }
				// }
				// // counter to count total no of fractions dropped
				var counter=0;
				// /* frac_array is randomized and pushed into frac_random */
				// var frac_random=[];
				// while(no_of_items)
				// {
					// var index = ole.getRandom(1, no_of_items-1, 0);
					// frac_random.push(frac_array[index]);
					// frac_array.splice((index),1);
					// no_of_items--;
				// }
				/* set nominator randomly such that nominator < denominator*/
				/* for 6 there is only 5 number so adding 7 to end by pushing to array*/
				// var arr_for_6 = sandy_random_display(5);
				// arr_for_6.push(7);


				var frac_random = [6,6,6,6,7,7,7,7,8,8,8,8,9,9,9,9];
				frac_random.shufflearray();

				var arr_for_5 = [1,2,3,4];
				var arr_for_7 = [1,2,3,4,5,6];
				var arr_for_8 = [1,3,5,7];
				var arr_for_9 = [1,2,4,5,7,8];
				arr_for_5.shufflearray();
				arr_for_7.shufflearray();
				arr_for_8.shufflearray();
				arr_for_9.shufflearray();
				var numi = [arr_for_5, arr_for_7, arr_for_8, arr_for_9];


				// var numi = [arr_for_6, sandy_random_display(6), sandy_random_display(7), sandy_random_display(8)];
				/* set denominator as element from frac_random */
				for(var m=0; m<frac_random.length; m++){
					var class_name = '.frac_'+(m)+'>.fraction>.bottom';
					var class_id = '.frac_'+(m);
					var class_nomi = '.frac_'+(m)+'>.fraction>.top';
					$(class_id).addClass('f_id_'+frac_random[m]);
					var deno_data = frac_random[m];
					if(deno_data==6){
						deno_data = 5;
					}
					$(class_name).html(deno_data);
					$(class_nomi).html(numi[frac_random[m]-6][0]);
					numi[frac_random[m]-6].splice(0,1);
				}

				$(".draggable").draggable({
					containment : "body",
					revert : "invalid",
					appendTo : "body",
					zindex : 1000,
					start: function(event, ui){
						$(ui.helper).addClass("disableanimation");
						$(ui.helper).css({
						});
					},
					stop: function(event, ui){
						$(ui.helper).removeClass("disableanimation");
						$(ui.helper).css({
						});
					}
				});

				$('.drop_0').droppable({
					hoverClass : "hovered",
					accept : '.fractions',
					drop : function upondrop(event, ui){
						handleCardDrop(event, ui, "f_id_0" , '.drop_0', '.itemdrop_0');
					}
				});

				$('.drop_1').droppable({
					hoverClass : "hovered",
					accept : '.fractions',
					drop : function upondrop(event, ui){
						handleCardDrop(event, ui, "f_id_1" , '.drop_1', '.itemdrop_1');
					}
				});

				$('.drop_2').droppable({
					hoverClass : "hovered",
					accept : '.fractions',
					drop : function upondrop(event, ui){
						handleCardDrop(event, ui, "f_id_2" , '.drop_2', '.itemdrop_2');
					}
				});

				$('.drop_3').droppable({
					accept : '.fractions',
					drop : function upondrop(event, ui){
						handleCardDrop(event, ui, "f_id_3" , '.drop_3', '.itemdrop_3');
					}
				});


				function handleCardDrop(event, ui, classname, dropbox, dropdiv) {
					ui.draggable.draggable('disable');
					var dropped = ui.draggable;
					var left_pos=10, top_pos=10;
					/* get the index of dropped div using substring method for eg itemdrop_4 gives 4*/
					var drop_index = dropdiv.substr(dropdiv.length-1);
					/* position for dropped item in the dropbox */
					left_pos = (dropped_items[drop_index]%2)?65:15;
					if(dropped_items[drop_index]>1){
						top_pos = 48;
					}
					if(dropped_items[drop_index]>3){
						top_pos = 65;
					}
					dropped_items[drop_index]++;
					$(ui.draggable).detach().css({
						'position': 'absolute',
						'left': left_pos+'%',
						'top':  top_pos + '%',
						'border': '0px solid white'
					}).appendTo(dropdiv);

					if(counter<(frac_random.length-1)){
						counter++;
					} else {
            $prevBtn.show(0);
						ole.footerNotificationHandler.pageEndSetNotification();
					}

					/* initially all droppables are empty and once a item is dropped, it accepts only that type of item
					 * and common class fraction is removed from fractions that belong to that dropbox */
					if($(dropbox).hasClass('emptybox')){
						$(dropbox).removeClass('emptybox');
						var id_class;
						for(var m=6; m<10;m++){
							if($(ui.draggable).hasClass('f_id_'+m)){
								id_class = ('.f_id_'+ m);
								accept_array[m-6]=0;
							}
						}
						/* check the accept flag and remove common class as one class of item is dropped into a dropbox
						 * for eg, if 1/6 is dropped, accept_array[3] is set to 0 then fractions class is removed from f_id_6 class */
						for(var i=6; i<10; i++){
							if(accept_array[i-6]==0){
								$('.f_id_'+i).removeClass('fractions');
							}
						}
							$(dropbox).droppable({
							accept : id_class,
						});
					}
					var $newEntry = $(".frac_ques> .its_hidden").eq(0);
					var $draggable3;
					var $draggable2;
					var $draggable1;
					if(dropped.hasClass("draggableposition4")){
						dropped.toggleClass("draggableposition4");
						$draggable3 = $(".draggableposition3");
						$draggable2 = $(".draggableposition2");
						$draggable1 = $(".draggableposition1");
					}else if(dropped.hasClass("draggableposition3")){
						dropped.toggleClass("draggableposition3");
						$draggable2 = $(".draggableposition2");
						$draggable1 = $(".draggableposition1");
					}else if(dropped.hasClass("draggableposition2")){
						dropped.toggleClass("draggableposition2");
						$draggable1 = $(".draggableposition1");
					}else if(dropped.hasClass("draggableposition1")){
						dropped.toggleClass("draggableposition1");
					}

					if($draggable3 != null){
						 $draggable3.removeClass("draggableposition3").addClass("draggableposition4");
					}
					if($draggable2 != null){
						 $draggable2.removeClass("draggableposition2").addClass("draggableposition3");
					}
					if($draggable1 != null){
						 $draggable1.removeClass("draggableposition1").addClass("draggableposition2");
					}
					if($newEntry != null){
						 $newEntry.removeClass("its_hidden").addClass("draggableposition1");
					}
					disable_pointer_events('.draggable', 1500);
				}
				break;
			default:
				break;
		}

	}
  function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	/*=====  End of Templates Block  ======*/

	/*==================================================
	=            Templates Controller Block            =
	==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
	 Motivation :
	 - Make a single function call that handles all the
	 template load easier

	 How To:
	 - Update the template caller with the required templates
	 - Call template caller

	 What it does:
	 - According to value of the Global Variable countNext
	 the slide templates are updated
	 */

	function templateCaller() {
			/*always hide next and previous navigation button unless
			 explicitly called from inside a template*/
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// call navigation controller
			navigationcontroller();

			// call the template
			generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


			//call the slide indication bar handler for pink indicators
			loadTimelineProgress($total_page, countNext + 1);

			// just for development purpose to see total slide vs current slide number
			// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/* function used to disable pointer events of class for time_in_ms period */
	function disable_pointer_events(classname, time_in_ms){
		$(classname).css('pointer-events', 'none');
		setTimeout(function(){
			$(classname).css('pointer-events', 'all');
		}, time_in_ms);
	}

	/* this function generates the random number array with range number of elements with min=1 and max=range */
	function sandy_random_display(range){
		var random_arr = [];
		var return_arr = [];
		var random_max = range;
		for(var i=0; i<range; i++){
			random_arr.push(i+1);
		}

		while(random_max)
		{
			var index = ole.getRandom(1, random_max-1, 0);
			return_arr.push(random_arr[index]);
			random_arr.splice((index),1);
			random_max--;
		}
		return return_arr;
	}
	/*=====  End of Templates Controller Block  ======*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("fractioncontent", $("#fractioncontent-partial").html());

	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		uppertextblock:[
		{
			textclass: "covertext",
			textdata: data.string.diy
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'cover',
				imgsrc: ""
			}
		]
	}]
},
//slide1
{
	singletext:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "drophere",
			textclass: "texttype-1",
			textdata: data.string.p3text1
		},
		{
			textclass: "texttype-2",
			textdata: data.string.p3text2
		},
		{
			textclass: "diydnd-1",
			textdata: "5"
		},
		{
			textclass: "diydnd-2",
			textdata: "1"
		},
		{
			textclass: "diydnd-3 corr",
			textdata: "3"
		}
	],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'cover',
			imgsrc: ""
		},
		{
			imgclass : "corr-ico",
			imgsrc : '',
			imgid : 'corrimg'
		}
	]
}]
},
//slide2
{
	singletext:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "drophere",
			textclass: "texttype-1",
			textdata: data.string.p3text3
		},
		{
			textclass: "texttype-2",
			textdata: data.string.p3text4
		},
		{
			textclass: "diydnd-1 corr",
			textdata: "9"
		},
		{
			textclass: "diydnd-2",
			textdata: "10"
		},
		{
			textclass: "diydnd-3",
			textdata: "6"
		}
	],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'cover',
			imgsrc: ""
		},
		{
			imgclass : "corr-ico",
			imgsrc : '',
			imgid : 'corrimg'
		}
	]
}]
},
//slide3
{
	singletext:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "drophere",
			textclass: "texttype-1",
			textdata: data.string.p3text5
		},
		{
			textclass: "texttype-2",
			textdata: data.string.p3text6
		},
		{
			textclass: "diydnd-1 corr",
			textdata: "9"
		},
		{
			textclass: "diydnd-2",
			textdata: "2"
		},
		{
			textclass: "diydnd-3 corr2",
			textdata: "7"
		}
	],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'cover',
			imgsrc: ""
		},
		{
			imgclass : "corr-ico",
			imgsrc : '',
			imgid : 'corrimg'
		}
	]
}]
},
//slide4
{
	singletext:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "drophere",
			textclass: "texttype-1",
			textdata: data.string.p3text7
		},
		{
			textclass: "texttype-2",
			textdata: data.string.p3text4
		},
		{
			textclass: "diydnd-1 corr",
			textdata: "2"
		},
		{
			textclass: "diydnd-2",
			textdata: "3"
		},
		{
			textclass: "diydnd-3",
			textdata: "8"
		}
	],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'cover',
			imgsrc: ""
		},
		{
			imgclass : "corr-ico",
			imgsrc : '',
			imgid : 'corrimg'
		}
	]
}]
},
//slide5
{
	singletext:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "drophere",
			textclass: "texttype-1",
			textdata: data.string.p3text8
		},
		{
			textclass: "texttype-2",
			textdata: data.string.p3text9
		},
		{
			textclass: "diydnd-1 corr",
			textdata: "5"
		},
		{
			textclass: "diydnd-2 corr2",
			textdata: "6"
		},
		{
			textclass: "diydnd-3",
			textdata: "1"
		}
	],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'cover',
			imgsrc: ""
		},
		{
			imgclass : "corr-ico",
			imgsrc : '',
			imgid : 'corrimg'
		}
	]
}]
},
//slide6
{
	singletext:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "drophere",
			textclass: "texttype-1",
			textdata: data.string.p3text10
		},
		{
			textclass: "texttype-2",
			textdata: data.string.p3text4
		},
		{
			textclass: "diydnd-1",
			textdata: "25"
		},
		{
			textclass: "diydnd-2 corr",
			textdata: "30"
		},
		{
			textclass: "diydnd-3",
			textdata: "15"
		}
	],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'cover',
			imgsrc: ""
		},
		{
			imgclass : "corr-ico",
			imgsrc : '',
			imgid : 'corrimg'
		}
	]
}]
},
//slide7
{
	singletext:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "drophere",
			textclass: "texttype-1",
			textdata: data.string.p3text11
		},
		{
			textclass: "texttype-2",
			textdata: data.string.p3text12
		},
		{
			textclass: "diydnd-1",
			textdata: "50"
		},
		{
			textclass: "diydnd-2 corr",
			textdata: "60"
		},
		{
			textclass: "diydnd-3 corr2",
			textdata: "10"
		}
	],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'cover',
			imgsrc: ""
		},
		{
			imgclass : "corr-ico",
			imgsrc : '',
			imgid : 'corrimg'
		}
	]
}]
},
//slide6
{
	singletext:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "drophere",
			textclass: "texttype-1",
			textdata: data.string.p3text13
		},
		{
			textclass: "texttype-2",
			textdata: data.string.p3text4
		},
		{
			textclass: "diydnd-1",
			textdata: "5"
		},
		{
			textclass: "diydnd-2 corr",
			textdata: "6"
		},
		{
			textclass: "diydnd-3",
			textdata: "12"
		}
	],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'cover',
			imgsrc: ""
		},
		{
			imgclass : "corr-ico",
			imgsrc : '',
			imgid : 'corrimg'
		}
	]
}]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "cover", src: imgpath+"bgnew.png", type: createjs.AbstractLoader.IMAGE},
			{id: "corrimg", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"s3_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s3_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s3_p4.ogg"},
			{id: "sound_4", src: soundAsset+"s3_p5.ogg"},
			{id: "sound_5", src: soundAsset+"s3_p6.ogg"},
			{id: "sound_6", src: soundAsset+"s3_p7.ogg"},
			{id: "sound_7", src: soundAsset+"s3_p8.ogg"},
			{id: "sound_8", src: soundAsset+"s3_p9.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	// $nextBtn.show(0);
 	// $prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		var dropCounter = 0;
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		// vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);

		// if(countNext!=0) sound_player(`sound_${countNext}`,0);
		switch (countNext) {
			case 0:
				play_diy_audio();
				nav_button_controls(2000);
				break;
			default:

		}

		$("[class*='diydnd']").draggable({
			containment : ".generalTemplateblock",
			revert : true,
			cursor : "move",
			zIndex: 100000,
			stop: function(ev,ui){
				var Stoppos = $(this).position();
				console.log("STOP: \nLeft: "+ Stoppos.left + "\nTop: " + Stoppos.top);
			}
		});

		$(".drophere:eq(0)").droppable({
			hoverClass: "hovered",
			drop: function (event, ui){
				$this = $(this);
				dropfunc(event, ui, $this, "corr");
			}
		});

		$(".drophere:eq(1)").droppable({
			hoverClass: "hovered",
			drop: function (event, ui){
				$this = $(this);
				dropfunc(event, ui, $this, "corr2");
			}
		});

		function dropfunc(event, ui, $droppedOn, classname){
			if(ui.draggable.hasClass(classname)){
				var left_addfactor = $(".texttype-1").position();
        var posIt = $droppedOn.position();
				//cor_sound.play();
				var centerX = posIt.left + left_addfactor.left + ($droppedOn.width()/2);
				var centerY = posIt.top + left_addfactor.top + $droppedOn.height();
				$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform: translateY(90%);" src="images/correct.png" />').insertAfter(".coverboardfull");

				var newVal = ui.draggable.text();
				$droppedOn.text(newVal);
				createjs.Sound.stop();
				play_correct_incorrect_sound(true);
				ui.draggable.draggable('option', 'revert', false);
				ui.draggable.draggable('disable').hide(0);
				$droppedOn.css({
					"background":"#98C02E",
					"border":"0.2em solid #DEEF3C",
					"color":"white"
				});

				if($(".drophere:eq(1)").length){
					dropCounter++;
					if(dropCounter == 2)
						navigationcontroller();
				}
				else {
					navigationcontroller();
				}
			}
			else{
				play_correct_incorrect_sound(false);
				// $(".wrong-ico").fadeIn();
			}
		}

		switch(countNext){
			case 0:
		  play_diy_audio();
			navigationcontroller();
			break;

		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function sound_player_seq(soundarray,navflag){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(soundarray[0]);
			soundarray.splice(0, 1);
			current_sound.on("complete", function(){
					if(soundarray.length > 0){
							sound_player_seq(soundarray, navflag);
					}else{
							if(navflag)
									navigationcontroller();
					}

			});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// if(countNext == 0)
		// navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

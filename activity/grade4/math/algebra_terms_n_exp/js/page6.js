var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentblockadditionalclass: "thebg1",
		singletext:[
		{
			textclass: "uptext2",
			textdata: data.string.p6text1
		}
	]
},
// slide1
{
	contentblockadditionalclass: "thebg1",
	singletext:[
	{
		textclass: "uptext2",
		textdata: data.string.p6text1
	},
	{
		textclass: "yelbgonly",
	},
	{
		textclass: "texttype-1",
		textdata: data.string.p6text2
	},
	{
		textclass: "texttype-2",
		textdata: "10m"
	},
	{
		textclass: "texttype-3",
		textdata: "2n"
	},
	{
		textclass: "texttype-4",
		textdata: "3p"
	}
],
imageblock:[{
imagestoshow:[
	{
		imgclass: "aniforarrow myarrow-1",
		imgid : 'arrow',
		imgsrc: ""
	},
	{
		imgclass : "aniforarrow myarrow-2",
		imgsrc : '',
		imgid : 'arrow'
	},
	{
		imgclass : "aniforarrow myarrow-3",
		imgsrc : '',
		imgid : 'arrow'
	}
]
}]
},
// slide2
{
	contentblockadditionalclass: "thebg1",
	singletext:[
	{
		textclass: "uptext2",
		textdata: data.string.p6text1
	},
	{
		textclass: "yelbgonly",
	},
	{
		textclass: "texttype-1",
		textdata: data.string.p6text2
	},
	{
		textclass: "texttype-2",
		textdata: "10m"
	},
	{
		textclass: "texttype-3",
		textdata: "2n"
	},
	{
		textclass: "texttype-4",
		textdata: "3p"
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "italiit",
		textclass: "texttype-5",
		textdata: data.string.p6text3
	}
],
imageblock:[{
imagestoshow:[
	{
		imgclass: "myarrow-1",
		imgid : 'arrow',
		imgsrc: ""
	},
	{
		imgclass : "myarrow-2",
		imgsrc : '',
		imgid : 'arrow'
	},
	{
		imgclass : "myarrow-3",
		imgsrc : '',
		imgid : 'arrow'
	}
]
}]
},
// slide3
{
	contentblockadditionalclass: "thebg1",
	singletext:[
	{
		textclass: "uptext2",
		textdata: data.string.p6text1
	},
	{
		textclass: "yelbgonly",
	},
	{
		textclass: "texttype-1",
		textdata: data.string.p6text2
	},
	{
		textclass: "texttype-2",
		textdata: "10m"
	},
	{
		textclass: "texttype-3",
		textdata: "2n"
	},
	{
		textclass: "texttype-4",
		textdata: "3p"
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "italiit",
		textclass: "texttype-5",
		textdata: data.string.p6text4
	}
],
imageblock:[{
imagestoshow:[
	{
		imgclass: "myarrow-1",
		imgid : 'arrow',
		imgsrc: ""
	},
	{
		imgclass : "myarrow-2",
		imgsrc : '',
		imgid : 'arrow'
	},
	{
		imgclass : "myarrow-3",
		imgsrc : '',
		imgid : 'arrow'
	}
]
}]
},
	// slide4
	{
		contentblockadditionalclass: "thebg1",
		singletext:[
		{
			textclass: "uptext2",
			textdata: data.string.p6text5
		}
	]
},
// slide5
{
	contentblockadditionalclass: "thebg1",
	singletext:[
	{
		textclass: "uptext2",
		textdata: data.string.p6text5
	},
	{
		textclass: "yelbgonly",
	},
	{
		textclass: "texttype-1",
		textdata: data.string.p6text2
	},
	{
		textclass: "texttype-2",
		textdata: "10m"
	},
	{
		textclass: "texttype-3",
		textdata: "2n"
	}
],
imageblock:[{
imagestoshow:[
	{
		imgclass: "aniforarrow myarrow-1",
		imgid : 'arrow',
		imgsrc: ""
	},
	{
		imgclass : "aniforarrow myarrow-2",
		imgsrc : '',
		imgid : 'arrow'
	}
]
}]
},
// slide6
{
	contentblockadditionalclass: "thebg1",
	singletext:[
	{
		textclass: "uptext2",
		textdata: data.string.p6text5
	},
	{
		textclass: "yelbgonly",
	},
	{
		textclass: "texttype-1",
		textdata: data.string.p6text2
	},
	{
		textclass: "texttype-2",
		textdata: "10m"
	},
	{
		textclass: "texttype-3",
		textdata: "2n"
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "italiit",
		textclass: "texttype-5",
		textdata: data.string.p6text6
	}
],
imageblock:[{
imagestoshow:[
	{
		imgclass: "myarrow-1",
		imgid : 'arrow',
		imgsrc: ""
	},
	{
		imgclass : "myarrow-2",
		imgsrc : '',
		imgid : 'arrow'
	}
]
}]
},
// slide7
{
	contentblockadditionalclass: "thebg1",
	singletext:[
	{
		textclass: "uptext2",
		textdata: data.string.p6text5
	},
	{
		textclass: "yelbgonly",
	},
	{
		textclass: "texttype-1",
		textdata: data.string.p6text2
	},
	{
		textclass: "texttype-2",
		textdata: "10m"
	},
	{
		textclass: "texttype-3",
		textdata: "2n"
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "italiit",
		textclass: "texttype-5",
		textdata: data.string.p6text7
	}
],
imageblock:[{
imagestoshow:[
	{
		imgclass: "myarrow-1",
		imgid : 'arrow',
		imgsrc: ""
	},
	{
		imgclass : "myarrow-2",
		imgsrc : '',
		imgid : 'arrow'
	}
]
}]
},
	// slide8
	{
		contentblockadditionalclass: "thebg1",
		singletext:[
		{
			textclass: "uptext2",
			textdata: data.string.p6text8
		}
	]
},
// slide9
{
	contentblockadditionalclass: "thebg1",
	singletext:[
	{
		textclass: "uptext2",
		textdata: data.string.p6text8
	},
	{
		textclass: "yelbgonly",
	},
	{
		textclass: "texttype-1",
		textdata: data.string.p6text2
	},
	{
		textclass: "texttype-3",
		textdata: "10m"
	}
],
imageblock:[{
imagestoshow:[
	{
		imgclass : "aniforarrow myarrow-2",
		imgsrc : '',
		imgid : 'arrow'
	}
]
}]
},
// slide10
{
	contentblockadditionalclass: "thebg1",
	singletext:[
	{
		textclass: "uptext2",
		textdata: data.string.p6text8
	},
	{
		textclass: "yelbgonly",
	},
	{
		textclass: "texttype-1",
		textdata: data.string.p6text2
	},
	{
		textclass: "texttype-3",
		textdata: "10m"
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "italiit",
		textclass: "texttype-5",
		textdata: data.string.p6text10
	}
],
imageblock:[{
imagestoshow:[
	{
		imgclass : "myarrow-2",
		imgsrc : '',
		imgid : 'arrow'
	}
]
}]
},
// slide11
{
	contentblockadditionalclass: "thebg1",
	singletext:[
	{
		textclass: "uptext2",
		textdata: data.string.p6text8
	},
	{
		textclass: "yelbgonly",
	},
	{
		textclass: "texttype-1",
		textdata: data.string.p6text2
	},
	{
		textclass: "texttype-3",
		textdata: "10m"
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "italiit",
		textclass: "texttype-5",
		textdata: data.string.p6text11
	}
],
imageblock:[{
imagestoshow:[
	{
		imgclass : "myarrow-2",
		imgsrc : '',
		imgid : 'arrow'
	}
]
}]
},
//slide12
{
	contentblockadditionalclass: "thebg1",
	headerblockadditionalclass: "custhead",
	headerblock:[
		{
			textdata: data.string.p6text12
		}
	],
	singletext:[
		{
			textclass: "uptext",
			textdata: data.string.p6text13
		},
		{
			textclass: "buttonsel forhover diydnd-1",
			textdata: "1"
		},
		{
			textclass: "buttonsel forhover diydnd-2",
			textdata: "5"
		},
		{
			textclass: "buttonsel correct forhover diydnd-3",
			textdata: "3"
		}
	]
},
// slide13
{
	contentblockadditionalclass: "thebg1",
	singletext:[
	{
		textclass: "uptext2",
		textdata: data.string.p6text15
	},
	{
		textclass: "bluebgonly",
	},
	{
		textclass: "texttype-6",
		textdata: data.string.p6text16
	},
	{
		textclass: "texttype-7",
		textdata: data.string.p6text17
	}
]
},
// slide14
{
	contentblockadditionalclass: "thebg1",
	singletext:[
	{
		textclass: "uptext2",
		textdata: data.string.p6text15
	},
	{
		textclass: "bluebgonly",
	},
	{
		textclass: "texttype-6",
		textdata: data.string.p6text18
	},
	{
		textclass: "texttype-7",
		textdata: data.string.p6text19
	}
]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "arrow", src: imgpath+"arrow02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "corrimg", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrimg", src: "images/wrongicon.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"s6_p1.ogg"},
			{id: "sound_3", src: soundAsset+"s6_p3.ogg"},
			{id: "sound_4", src: soundAsset+"s6_p4.ogg"},
			{id: "sound_5", src: soundAsset+"s6_p5.ogg"},
			{id: "sound_7", src: soundAsset+"s6_p7.ogg"},
			{id: "sound_8", src: soundAsset+"s6_p8.ogg"},
			{id: "sound_9", src: soundAsset+"s6_p9.ogg"},
			{id: "sound_11", src: soundAsset+"s6_p11.ogg"},
			{id: "sound_12", src: soundAsset+"s6_p12.ogg"},
			{id: "sound_13", src: soundAsset+"s6_p13.ogg"},
			{id: "sound_14", src: soundAsset+"s6_p14.ogg"},
			{id: "sound_15", src: soundAsset+"s6_p15.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		if(countNext!=12) sound_player(`sound_${countNext+1}`,1);
		if(countNext==12) sound_player(`sound_${countNext+1}`,0);

		switch(countNext){
			case 1: case 5: case 9:
				nav_button_controls(1000);
			break;
			case 12:
			$(".buttonsel").click(function(){
				if($(this).hasClass("forhover")){
					$(this).removeClass('forhover');
						if($(this).hasClass("correct")){
							createjs.Sound.stop();
							play_correct_incorrect_sound(1);
							$(this).css("background","#bed62f");
							$(this).css("border","5px solid #deef3c");
		          $(this).css("color","white");
							// $(this).siblings(".corctopt").show(0);
							//$('.hint_image').show(0);
							appender($(this),'corrimg');
							$('.buttonsel').removeClass('forhover forhoverimg');
							navigationcontroller();
						}
						else{
							play_correct_incorrect_sound(0);
							appender($(this),'incorrimg');
							$(this).css("background","#FF0000");
							$(this).css("border","5px solid #980000");
							$(this).css("color","white");
							// $(this).siblings(".wrngopt").show(0);
						}
				}

				function appender($this, icon){
					if($this.hasClass("diydnd-1"))
						$(".coverboardfull").append("<img class='myicon-one' src= '"+ preload.getResult(icon).src +"'>");
					else if($this.hasClass("diydnd-2"))
						$(".coverboardfull").append("<img class='myicon-two' src= '"+ preload.getResult(icon).src +"'>");
					else
					$(".coverboardfull").append("<img class='myicon-three' src= '"+ preload.getResult(icon).src +"'>");
					}
				});
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// if(countNext == 0)
		// navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

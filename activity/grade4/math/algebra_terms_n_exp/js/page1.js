var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		singletext:[
		{
			textclass: "covertext",
			textdata: data.lesson.chapter
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'cover',
				imgsrc: ""
			}
		]
	}]
},
// slide1
{
	singletext:[
	{
		textclass: "uptext",
		textdata: data.string.p1text1
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg1',
			imgsrc: ""
		}
	]
}]
},
// slide2
{
	singletext:[
	{
		textclass: "uptext",
		textdata: data.string.p1text2
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg2',
			imgsrc: ""
		}
	]
}]
},
// slide3
{
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg3',
			imgsrc: ""
		}
	]
}],
speechbox:[{
	speechbox: 'sp-1',
	textclass: "answer",
	textdata: data.string.p1text3,
	imgclass: '',
	imgid : 'tb-2',
	imgsrc: '',
}]
},
// slide4
{
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg2',
			imgsrc: ""
		}
	]
}],
speechbox:[{
	speechbox: 'sp-2',
	textclass: "answer",
	textdata: data.string.p1text4,
	imgclass: '',
	imgid : 'tb-2',
	imgsrc: '',
}]
},
// slide5
{
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg4',
			imgsrc: ""
		}
	]
}],
speechbox:[{
	speechbox: 'sp-1',
	textclass: "answer",
	textdata: data.string.p1text5,
	imgclass: '',
	imgid : 'tb-2',
	imgsrc: '',
}]
},
// slide6
{
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg5',
			imgsrc: ""
		}
	]
}],
speechbox:[{
	speechbox: 'sp-2',
	textclass: "answer",
	textdata: data.string.p1text6,
	imgclass: '',
	imgid : 'tb-2',
	imgsrc: '',
}]
},
// slide7
{
	singletext:[
	{
		textclass: "uptext2",
		textdata: data.string.p1text7
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'woodbg',
				imgsrc: ""
			},
			{
				imgclass : "hand",
				imgsrc : '',
				imgid : 'hand'
			},
			{
				imgclass : "pencil",
				imgsrc : '',
				imgid : 'marble'
			},
			{
				imgclass : "pencil_big",
				imgsrc : '',
				imgid : 'marble'
			}
		]
	}]
},
// slide8
{
	singletext:[
	{
		textclass: "uptext2",
		textdata: data.string.p1text8
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'woodbg',
				imgsrc: ""
			},
			{
				imgclass : "pencil_big",
				imgsrc : '',
				imgid : 'marble'
			}
		]
	}]
},
// slide9
{
	singletext:[
	{
		textclass: "uptext2",
		textdata: data.string.p1text9
	},
	{
		textclass: "texttype-1",
		textdata: data.string.p1text10
	},
	{
		textclass: "texttype-2",
		textdata: data.string.p1text11
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'woodbg',
				imgsrc: ""
			},
			{
				imgclass : "pencil_big",
				imgsrc : '',
				imgid : 'marble'
			}
		]
	}]
},
// slide10
{
	singletext:[
	{
		textclass: "uptext2",
		textdata: data.string.p1text12
	},
	{
		textclass: "texttype-3",
		textdata: data.string.p1text13
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "yellowtext",
		textclass: "texttype-4",
		textdata: data.string.p1text14
	},
	{
		textclass: "texttype-2",
		textdata: data.string.p1text15
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'woodbg',
				imgsrc: ""
			},
			{
				imgclass : "pencil_big",
				imgsrc : '',
				imgid : 'marble'
			}
		]
	}]
},
// slide11
{
	singletext:[
	{
		textclass: "texttype-2",
		textdata: data.string.p1text15
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "yellowtext",
		textclass: "texttype-5",
		textdata: data.string.p1text16
	},
	{
		textclass: "texttype-6",
		textdata: data.string.p1text17
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'woodbg',
				imgsrc: ""
			},
			{
				imgclass : "pencil_big",
				imgsrc : '',
				imgid : 'marble'
			}
		]
	}]
},
// slide12
{
	singletext:[
	{
		textclass: "texttype-2",
		textdata: data.string.p1text15
	},
	{
		textclass: "texttype-7",
		textdata: data.string.p1text18
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'woodbg',
				imgsrc: ""
			},
			{
				imgclass : "pencil_big",
				imgsrc : '',
				imgid : 'marble'
			}
		]
	}]
},
// slide13
{
	singletext:[
	{
		textclass: "texttype-2",
		textdata: data.string.p1text15
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "yellowtext",
		textclass: "texttype-8",
		textdata: data.string.p1text19
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'woodbg',
				imgsrc: ""
			},
			{
				imgclass : "pencil_big",
				imgsrc : '',
				imgid : 'marble'
			},
			{
				imgclass : "fairy",
				imgsrc : '',
				imgid : 'fairy'
			}
		]
	}],
	speechbox:[{
		speechbox: 'sp-3',
		textclass: "answer",
		textdata: data.string.p1text20,
		imgclass: '',
		imgid : 'tb-3',
		imgsrc: '',
	}]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "cover", src: imgpath+"cover_algebra.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg1", src: imgpath+"shop_inside01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg2", src: imgpath+"shop_inside02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg3", src: imgpath+"shop_inside03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg4", src: imgpath+"shop_inside04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg5", src: imgpath+"shop_inside05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "woodbg", src: imgpath+"table01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hand", src: imgpath+"hand.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble", src: imgpath+"ten_marble.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fairy", src: 'images/fairy/fairy_fly.gif', type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-3", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"s1_p1.ogg"},
			{id: "sound_2", src: soundAsset+"s1_p2.ogg"},
			{id: "sound_3", src: soundAsset+"s1_p3.ogg"},
			{id: "sound_4", src: soundAsset+"s1_p4.ogg"},
			{id: "sound_5", src: soundAsset+"s1_p5.ogg"},
			{id: "sound_6", src: soundAsset+"s1_p6.ogg"},
			{id: "sound_7", src: soundAsset+"s1_p7.ogg"},
			{id: "sound_8", src: soundAsset+"s1_p8.ogg"},
			{id: "sound_9", src: soundAsset+"s1_p9.ogg"},
			{id: "sound_10", src: soundAsset+"s1_p10.ogg"},
			{id: "sound_11", src: soundAsset+"s1_p11.ogg"},
			{id: "sound_12", src: soundAsset+"s1_p12.ogg"},
			{id: "sound_13", src: soundAsset+"s1_p13.ogg"},
			{id: "sound_14", src: soundAsset+"s1_p14.ogg"},
			{id: "sound_15", src: soundAsset+"s1_p14_1.ogg"},
		];

		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		if(countNext!=13) sound_player(`sound_${countNext+1}`,1);
		switch(countNext){
			case 0:
			break;
			case 7:
			$('.pencil_big,.pencil_tag1,.one_pencil').css('opacity','0');
			setTimeout(function () {
				$('.pencil_big,.pencil_tag1,.one_pencil').animate({'opacity':'1'},1000);
				$('.pencil,.pencil_tag,.hand').animate({'opacity':'0'},1000);
			}, 2000);
			break;
			case 13:
				sound_player_seq(['sound_14','sound_15'],1);
				break;

		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function sound_player_seq(soundarray,navflag){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(soundarray[0]);
			soundarray.splice( 0, 1);
			current_sound.on("complete", function(){
					if(soundarray.length > 0){
							sound_player_seq(soundarray, navflag);
					}else{
							if(navflag)
									navigationcontroller();
					}

			});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// if(countNext == 0)
		// navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

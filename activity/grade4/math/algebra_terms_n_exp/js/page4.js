var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "yellowbg",
		uppertextblock:[
		{
			textclass: "texttype-1",
			textdata: data.string.p4text1
		}
	],
	imageblock:[{
		imagestoshow:[
		{
			imgclass : "fairy",
			imgsrc : '',
			imgid : 'fairy'
		}
		]
	}],
	speechbox:[{
		speechbox: 'sp-3',
		textclass: "answer",
		textdata: data.string.p4text2,
		imgclass: '',
		imgid : 'tb-3',
		imgsrc: '',
	}]
},
// slide3
{
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg1',
			imgsrc: ""
		}
	]
}],
speechbox:[{
	speechbox: 'sp-1',
	textclass: "answer",
	textdata: data.string.p4text3,
	imgclass: '',
	imgid : 'tb-2',
	imgsrc: '',
}]
},
// slide4
{
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg2',
			imgsrc: ""
		}
	]
}],
speechbox:[{
	speechbox: 'sp-2',
	textclass: "answer",
	textdata: data.string.p4text4,
	imgclass: '',
	imgid : 'tb-2',
	imgsrc: '',
}]
},
// slide5
{
	singletext:[
	{
		textclass: "uptext2",
		textdata: data.string.p4text5
	},
	{
		textclass: "coltxt-1",
		textdata: data.string.p4text6
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'woodbg',
				imgsrc: ""
			},
			{
				imgclass : "colimg-1",
				imgsrc : '',
				imgid : 'marble'
			},
			{
				imgclass : "colimg-2",
				imgsrc : '',
				imgid : 'ball'
			},
			{
				imgclass : "colimg-3",
				imgsrc : '',
				imgid : 'rubber'
			}
		]
	}]
},
// slide6
{
	singletext:[
	{
		textclass: "uptext2",
		textdata: data.string.p4text7
	},
	{
		textclass: "texttype-2",
		textdata: data.string.p4text9
	},
	{
		textclass: "coltxt-1",
		textdata: data.string.p4text6
	},
	{
		textclass: "coltxt-2",
		textdata: data.string.p4text8
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'woodbg',
				imgsrc: ""
			},
			{
				imgclass : "colimg-1",
				imgsrc : '',
				imgid : 'marble'
			},
			{
				imgclass : "colimg-2",
				imgsrc : '',
				imgid : 'ball'
			},
			{
				imgclass : "colimg-3",
				imgsrc : '',
				imgid : 'rubber'
			}
		]
	}]
},
// slide7
{
	singletext:[
	{
		textclass: "texttype-2",
		textdata: data.string.p4text10
	},
	{
		textclass: "coltxt-1",
		textdata: data.string.p4text6
	},
	{
		textclass: "coltxt-2",
		textdata: data.string.p4text17
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'woodbg',
				imgsrc: ""
			},
			{
				imgclass : "colimg-1",
				imgsrc : '',
				imgid : 'marble'
			},
			{
				imgclass : "colimg-2",
				imgsrc : '',
				imgid : 'ball'
			},
			{
				imgclass : "colimg-3",
				imgsrc : '',
				imgid : 'rubber'
			}
		]
	}]
},
// slide8
{
	singletext:[
	{
		textclass: "uptext2",
		textdata: data.string.p4text11
	},
	{
		textclass: "coltxt-1",
		textdata: data.string.p4text6
	},
	{
		textclass: "coltxt-2",
		textdata: data.string.p4text17
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'woodbg',
				imgsrc: ""
			},
			{
				imgclass : "colimg-1",
				imgsrc : '',
				imgid : 'marble'
			},
			{
				imgclass : "colimg-2",
				imgsrc : '',
				imgid : 'ball'
			},
			{
				imgclass : "colimg-3",
				imgsrc : '',
				imgid : 'rubber'
			}
		]
	}]
},
// slide9
{
	singletext:[
	{
		textclass: "uptext2",
		textdata: data.string.p4text11
	},
	{
		textclass: "texttype-2",
		textdata: data.string.p4text13
	},
	{
		textclass: "coltxt-1",
		textdata: data.string.p4text6
	},
	{
		textclass: "coltxt-2",
		textdata: data.string.p4text17
	},
	{
		textclass: "coltxt-3",
		textdata: data.string.p4text18
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'woodbg',
				imgsrc: ""
			},
			{
				imgclass : "colimg-1",
				imgsrc : '',
				imgid : 'marble'
			},
			{
				imgclass : "colimg-2",
				imgsrc : '',
				imgid : 'ball'
			},
			{
				imgclass : "colimg-3",
				imgsrc : '',
				imgid : 'rubber'
			}
		]
	}]
},
// slide10
{
	singletext:[
	{
		textclass: "uptext2",
		textdata: data.string.p4text14
	},
	{
		textclass: "coltxt-1",
		textdata: data.string.p4text6
	},
	{
		textclass: "coltxt-2",
		textdata: data.string.p4text17
	},
	{
		textclass: "coltxt-3",
		textdata: data.string.p4text18
	},
	{
		textclass: "plus-left",
		textdata: "+"
	},
	{
		textclass: "plus-right",
		textdata: "+"
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'woodbg',
				imgsrc: ""
			},
			{
				imgclass : "colimg-1",
				imgsrc : '',
				imgid : 'marble'
			},
			{
				imgclass : "colimg-2",
				imgsrc : '',
				imgid : 'ball'
			},
			{
				imgclass : "colimg-3",
				imgsrc : '',
				imgid : 'rubber'
			}
		]
	}]
},
// slide11
{
	singletext:[
	{
		textclass: "uptext3",
		textdata: data.string.p4text15
	},
	{
		textclass: "coltxt-1",
		textdata: data.string.p4text6
	},
	{
		textclass: "coltxt-2",
		textdata: data.string.p4text17
	},
	{
		textclass: "coltxt-3",
		textdata: data.string.p4text18
	},
	{
		textclass: "plus-left",
		textdata: "+"
	},
	{
		textclass: "plus-right",
		textdata: "+"
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'woodbg',
				imgsrc: ""
			},
			{
				imgclass : "colimg-1",
				imgsrc : '',
				imgid : 'marble'
			},
			{
				imgclass : "colimg-2",
				imgsrc : '',
				imgid : 'ball'
			},
			{
				imgclass : "colimg-3",
				imgsrc : '',
				imgid : 'rubber'
			}
		]
	}]
},
// slide12
{
	singletext:[
	{
		textclass: "uptext3",
		textdata: data.string.p4text15
	},
	{
		textclass: "texttype-2",
		textdata: data.string.p4text16
	},
	{
		textclass: "coltxt-1",
		textdata: data.string.p4text6
	},
	{
		textclass: "coltxt-2",
		textdata: data.string.p4text17
	},
	{
		textclass: "coltxt-3",
		textdata: data.string.p4text18
	},
	{
		textclass: "plus-left",
		textdata: "+"
	},
	{
		textclass: "plus-right",
		textdata: "+"
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'woodbg',
				imgsrc: ""
			},
			{
				imgclass : "colimg-1",
				imgsrc : '',
				imgid : 'marble'
			},
			{
				imgclass : "colimg-2",
				imgsrc : '',
				imgid : 'ball'
			},
			{
				imgclass : "colimg-3",
				imgsrc : '',
				imgid : 'rubber'
			}
		]
	}]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "fairy", src: 'images/fairy/fairy_fly.gif', type: createjs.AbstractLoader.IMAGE},
			{id: "bg1", src: imgpath+"shop_inside01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg2", src: imgpath+"shop_inside02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "woodbg", src: imgpath+"table01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble", src: imgpath+"ten_marble.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ball", src: imgpath+"ball.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rubber", src: imgpath+"rubberband.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-3", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"s4_p1.ogg"},
			{id: "sound_2", src: soundAsset+"s4_p2.ogg"},
			{id: "sound_3", src: soundAsset+"s4_p3.ogg"},
			{id: "sound_4", src: soundAsset+"s4_p4.ogg"},
			{id: "sound_5", src: soundAsset+"s4_p5.ogg"},
			{id: "sound_6", src: soundAsset+"s4_p6.ogg"},
			{id: "sound_7", src: soundAsset+"s4_p7.ogg"},
			{id: "sound_8", src: soundAsset+"s4_p8.ogg"},
			{id: "sound_9", src: soundAsset+"s4_p9.ogg"},
			{id: "sound_10", src: soundAsset+"s4_p10.ogg"},
			{id: "sound_11", src: soundAsset+"s4_p11.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		sound_player(`sound_${countNext+1}`,1);
		switch(countNext){
			default:
				// navigationcontroller();
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// if(countNext == 0)
		// navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

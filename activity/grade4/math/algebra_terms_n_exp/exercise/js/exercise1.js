var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var sound_1 = new buzz.sound(soundAsset+"ex_ins.ogg");



var content=[
	//ex1
	{
    instruction: data.string.exques1,
		exetype1: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "clickplace",
        exeoptions:[
          {
            optaddclass: "correct",
          },
          {
          }
        ]
			}
		]
	},
	//ex2
	{
    instruction: data.string.exques1,
		exetype1: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "clickplace",
        exeoptions:[
          {
            optaddclass: "correct",
          },
          {
          }
        ]
			}
		]
	},
	//ex3
	{
		instruction: data.string.exques3,
		fronttextclass: "typ2front",
		exetype2: [
			{
				type2loopblock:[
					{
						textclass: "eachloop",
					},
					{
						textclass: "eachloop",
					}
				]
			}
		]
	},
	//ex4
	{
		instruction: data.string.exques3,
		fronttextclass: "typ2front",
		exetype2: [
			{
				type2loopblock:[
					{
						textclass: "eachloop",
					},
					{
						textclass: "eachloop",
					}
				]
			}
		]
	},
	//ex5
	{
		instruction: data.string.exques3,
		fronttextclass: "typ2front",
		exetype2: [
			{
				type2loopblock:[
					{
						textclass: "eachloop",
					},
					{
						textclass: "eachloop",
					}
				]
			}
		]
	},
	//ex6
	{
		instruction: data.string.exques3,
		fronttextclass: "typ2front fracmaintain",
		splitintofractionsflag: true,
		exetype2: [
			{
				type2loopblock:[
					{
						splitintofractionsflag: true,
						textclass: "eachloop fracmaintain",
					},
					{
						splitintofractionsflag: true,
						textclass: "eachloop",
					}
				]
			}
		]
	},
	//ex7
	{
		instruction: data.string.exques3,
		fronttextclass: "typ2front fracmaintain",
		splitintofractionsflag: true,
		extratext:[
			{
				extraclass: "typ2extra",
			}
		],
		exetype2: [
			{
				type2loopblock:[
					{
						splitintofractionsflag: true,
						textclass: "eachloop fracmaintain",
					},
					{
						splitintofractionsflag: true,
						textclass: "eachloop",
					}
				]
			}
		]
	},
	//ex8
	{
		instruction: data.string.exques3,
		fronttextclass: "typ2front",
		splitintofractionsflag: true,
		extratext:[
			{
				extraclass: "typ2extra",
			}
		],
		exetype2: [
			{
				type2loopblock:[
					{
						splitintofractionsflag: true,
						textclass: "eachloop",
					},
					{
						splitintofractionsflag: true,
						textclass: "eachloop",
					}
				]
			}
		]
	},
	{
		//ex9
    instruction: data.string.exques1,
		extratext:[
			{
				extraclass: "typ2extra",
			}
		],
		exetype1: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "clickplace",
        exeoptions:[
          {
						optcontainerextra: "threeops",
            optaddclass: "correct",
						startposextra: "thrciico"
          },
          {
						optcontainerextra: "threeops",
						startposextra: "thrciico"
          },
          {
						optcontainerextra: "threeops",
						startposextra: "thrciico"
          }
        ]
			}
		]
	},
	//ex10
	{
		instruction: data.string.exques3,
		fronttextclass: "typ2front bigtype2",
		splitintofractionsflag: true,
		extratext:[
			{
				extraclass: "typ2extra",
			}
		],
		exetype2: [
			{
				type2loopblockacla: "bigblock",
				type2loopblock:[
					{
						splitintofractionsflag: true,
						textclass: "eachloop",
					},
					{
						splitintofractionsflag: true,
						textclass: "eachloop",
					},
					{
						splitintofractionsflag: true,
						textclass: "eachloop",
					}
				]
			}
		]
	},
	{
		instruction: data.string.exques11,
		exetype3: [
			{
				multiopsblock: [
					{
							multiopstextstyle: "forhover multimcqiten corritem",
					},
					{
							multiopstextstyle: "forhover multimcqiten corritem",
					},
					{
							multiopstextstyle: "forhover multimcqiten corritem",
					},
					{
							multiopstextstyle: "forhover multimcqiten",
					},
					{
							multiopstextstyle: "forhover multimcqiten",
					},
					{
							multiopstextstyle: "forhover multimcqiten",
					},
					{
							multiopstextstyle: "forhover multimcqiten",
					},
					{
							multiopstextstyle: "forhover multimcqiten",
					}
				]
			}
		]
	},
	{
		instruction: data.string.exques12,
		exetype3: [
			{
				multiopsblock: [
					{
							multiopstextstyle: "forhover multimcqiten corritem",
					},
					{
							multiopstextstyle: "forhover multimcqiten corritem",
					},
					{
							multiopstextstyle: "forhover multimcqiten corritem",
					},
					{
							multiopstextstyle: "forhover multimcqiten",
					},
					{
							multiopstextstyle: "forhover multimcqiten",
					},
					{
							multiopstextstyle: "forhover multimcqiten",
					},
					{
							multiopstextstyle: "forhover multimcqiten",
					},
					{
							multiopstextstyle: "forhover multimcqiten",
					}
				]
			}
		]
	},
	{
		instruction: data.string.exques13,
		exetype3: [
			{
				multiopsblock: [
					{
							multiopstextstyle: "forhover multimcqiten corritem",
					},
					{
							multiopstextstyle: "forhover multimcqiten corritem",
					},
					{
							multiopstextstyle: "forhover multimcqiten corritem",
					},
					{
							multiopstextstyle: "forhover multimcqiten",
					},
					{
							multiopstextstyle: "forhover multimcqiten",
					},
					{
							multiopstextstyle: "forhover multimcqiten",
					},
					{
							multiopstextstyle: "forhover multimcqiten",
					},
					{
							multiopstextstyle: "forhover multimcqiten",
					}
				]
			}
		]
	}
];

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("multiopscontent", $("#multiopscontent-partial").html());

	/*for limiting the questions to 10*/
	var $total_page = content.length;

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	var score = 0;
	var exeTemp = new NumberTemplate();

 	exeTemp.init(13);
	/*values in this array is same as the name of images of eggs in image folder*/
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide();
		$prevBtn.hide();

		/*generate question no at the beginning of question*/
		exeTemp.numberOfQuestions();

		var ansClicked = false;
		var wrngClicked = false;
		var multiCorrCounter = 0;
		var multiClick = 0;

    switch (countNext) {
      case 0:
			sound_1.play();
      var quesNo = rand_generator(5);
        $('.question').html(eval('data.string.exetyp1_o'+quesNo));
        $(".buttonsel:eq(0)").html(eval('data.string.exetyp1opt1_o'+quesNo));
        $(".buttonsel:eq(1)").html(eval('data.string.exetyp1opt2_o'+quesNo));
      break;
      case 1:
      var quesNo = rand_generator(5);
        $('.question').html(eval('data.string.exetyp2_o'+quesNo));
        $(".buttonsel:eq(0)").html(eval('data.string.exetyp2opt1_o'+quesNo));
        $(".buttonsel:eq(1)").html(eval('data.string.exetyp2opt2_o'+quesNo));
      break;
			case 2:
			var scoreFlagSet = true;
			var quesNo = rand_generator(5);
        $('.typ2front').html(eval('data.string.exques3text1_o'+quesNo));
        $(".eachloop:eq(0)").html(eval('data.string.exques3loop1_o'+quesNo));
        $(".eachloop:eq(1)").html(eval('data.string.exques3loop2_o'+quesNo));
			var corrAnswerstpy2 = [[10,15],[2,10],[7,15],[15,22],[9,24]];
			var loopCounter = 0;
			$(".eachloop:eq("+loopCounter+")").show(0);

			$(".chckbtn").click(function(){
				var $curBox = $(this);
				var typ2Num = $curBox.parent().find("input").val();
				if(typ2Num == corrAnswerstpy2[quesNo-1][loopCounter]){
					correct_action($curBox);
				}
				else{
					$me = $(this);
					incorrect_action($me);
				}
			});
			break;
			case 3:
			var scoreFlagSet = true;
			var quesNo = rand_generator(5);
      $('.typ2front').html(eval('data.string.exques4text1_o'+quesNo));
      $(".eachloop:eq(0)").html(eval('data.string.exques4loop1_o'+quesNo));
      $(".eachloop:eq(1)").html(eval('data.string.exques4loop2_o'+quesNo));
			var corrAnswerstpy2 = [[10,5],[2,6],[7,11],[15,8],[9,6]];
			var loopCounter = 0;
			$(".eachloop:eq("+loopCounter+")").show(0);

			$(".chckbtn").click(function(){
				var $curBox = $(this);
				var typ2Num = $curBox.parent().find("input").val();
				if(typ2Num == corrAnswerstpy2[quesNo-1][loopCounter]){
					correct_action($curBox);
				}
				else{
					$me = $(this);
					incorrect_action($me);
				}
			});
			break;
			case 4:
			var scoreFlagSet = true;
			var quesNo = rand_generator(5);
        $('.typ2front').html(eval('data.string.exques5text1_o'+quesNo));
        $(".eachloop:eq(0)").html(eval('data.string.exques5loop1_o'+quesNo));
        $(".eachloop:eq(1)").html(eval('data.string.exques5loop2_o'+quesNo));
				var corrAnswerstpy2 = [[10, 50],[2,16],[7,49],[5,40],[9,54]];
			var loopCounter = 0;
			$(".eachloop:eq("+loopCounter+")").show(0);

			$(".chckbtn").click(function(){
				var $curBox = $(this);
				var typ2Num = $curBox.parent().find("input").val();
				if(typ2Num == corrAnswerstpy2[quesNo-1][loopCounter]){
					correct_action($curBox);
				}
				else{
					$me = $(this);
					incorrect_action($me);
				}
			});
			break;
			case 5:
			var scoreFlagSet = true;
			var quesNo = rand_generator(5);
      $('.typ2front').html(eval('data.string.exques6text1_o'+quesNo));
      $(".eachloop:eq(0)").html(eval('data.string.exques6loop1_o'+quesNo));
      $(".eachloop:eq(1)").html(eval('data.string.exques6loop2_o'+quesNo));
			var corrAnswerstpy2 = [[10, 2],[4,3],[20,5],[28,14],[6,7]];
			var loopCounter = 0;
			$(".eachloop:eq("+loopCounter+")").show(0);
			$(".eachloop:eq(0) #input1").css("width","80%");
			splitintofractions($board);

			$(".chckbtn").click(function(){
				var $curBox = $(this);
				var typ2Num = $curBox.parent().find("input").val();
				if(typ2Num == corrAnswerstpy2[quesNo-1][loopCounter]){
					correct_action($curBox, true);
				}
				else{
					$me = $(this);
					incorrect_action($me);
				}
			});
			break;
			case 6:
			var scoreFlagSet = true;
			var quesNo = rand_generator(5);
      $('.typ2front').html(eval('data.string.exques7text1_o'+quesNo));
      $('.typ2extra').html(eval('data.string.exques7text2_o'+quesNo));
      $(".eachloop:eq(0)").html(eval('data.string.exques7loop1_o'+quesNo));
      $(".eachloop:eq(1)").html(eval('data.string.exques7loop2_o'+quesNo));
			var corrAnswerstpy2 = [[[3,5],8],[[10,4],14],[[15,5],10],[[7,6],13],[[25,16],9]];
			var loopCounter = 0;
			$(".eachloop:eq("+loopCounter+")").show(0);

			$(".chckbtn").click(function(){
				var $curBox = $(this);
				if(corrAnswerstpy2[quesNo-1][loopCounter].length > 1){
					for(var i = 0; i < corrAnswerstpy2[quesNo-1][loopCounter].length; i++){
						var typ2Num = $curBox.parent().find("input:eq("+i+")").val();
						if(typ2Num != corrAnswerstpy2[quesNo-1][loopCounter][i]){
								$me = $(this);
								incorrect_action($me);
								break;
						}
						if(i == (corrAnswerstpy2[quesNo-1][loopCounter].length-1)){
							correct_action($curBox);
						}
					}
				}
				else{
					var typ2Num = $curBox.parent().find("input").val();
					if(typ2Num == corrAnswerstpy2[quesNo-1][loopCounter]){
						correct_action($curBox);
					}
					else{
						$me = $(this);
						incorrect_action($me);
					}
				}

			});
			break;
			case 7:
			var scoreFlagSet = true;
			var quesNo = rand_generator(5);
      $('.typ2front').html(eval('data.string.exques8text1_o'+quesNo));
      $('.typ2extra').html(eval('data.string.exques8text2_o'+quesNo));
      $(".eachloop:eq(0)").html(eval('data.string.exques8loop1_o'+quesNo));
      $(".eachloop:eq(1)").html(eval('data.string.exques8loop2_o'+quesNo));
			var corrAnswerstpy2 = [[[3,5],15],[[10,4],40],[[7,8],56],[[10,8],80],[[14,2],28]];
			var loopCounter = 0;
			$(".eachloop:eq("+loopCounter+")").show(0);

			$(".chckbtn").click(function(){
				var $curBox = $(this);
				if(corrAnswerstpy2[quesNo-1][loopCounter].length > 1){
					for(var i = 0; i < corrAnswerstpy2[quesNo-1][loopCounter].length; i++){
						var typ2Num = $curBox.parent().find("input:eq("+i+")").val();
						if(typ2Num != corrAnswerstpy2[quesNo-1][loopCounter][i]){
								$me = $(this);
								incorrect_action($me);
								break;
						}
						if(i == (corrAnswerstpy2[quesNo-1][loopCounter].length-1)){
							correct_action($curBox);
						}
					}
				}
				else{
					var typ2Num = $curBox.parent().find("input").val();
					if(typ2Num == corrAnswerstpy2[quesNo-1][loopCounter]){
						correct_action($curBox);
					}
					else{
						$me = $(this);
						incorrect_action($me);
					}
				}

			});
			break;
			case 8:
				var quesNo = rand_generator(5);
				$('.question').html(eval('data.string.exetyp9_o'+quesNo));
				$('.typ2extra').html(eval('data.string.exetyp9tins_o'+quesNo));
				$(".buttonsel:eq(0)").html(eval('data.string.exetyp9opt1_o'+quesNo));
				$(".buttonsel:eq(1)").html(eval('data.string.exetyp9opt2_o'+quesNo));
				$(".buttonsel:eq(2)").html(eval('data.string.exetyp9opt3_o'+quesNo));
			break;
			case 9:
			var scoreFlagSet = true;
			var quesNo = rand_generator(5);
      $('.typ2front').html(eval('data.string.exques10text1_o'+quesNo));
      $('.typ2extra').html(eval('data.string.exques10text2_o'+quesNo));
      $(".eachloop:eq(0)").html(eval('data.string.exques10loop1_o'+quesNo));
      $(".eachloop:eq(1)").html(eval('data.string.exques10loop2_o'+quesNo));
      $(".eachloop:eq(2)").html(eval('data.string.exques10loop3_o'+quesNo));
			var corrAnswerstpy2 = [[[2,10,5],[8,20,15],43],[[10,2,5],[30,8,15],53],[[3,2,5],[18,8,15],11],[[3,5,2],[15,15,2],28],[[5,3,6],[20,15,12],47]];
			var loopCounter = 0;
			$(".eachloop:eq("+loopCounter+")").show(0);

			$(".chckbtn").click(function(){
				var $curBox = $(this);
				if(corrAnswerstpy2[quesNo-1][loopCounter].length > 1){
					for(var i = 0; i < corrAnswerstpy2[quesNo-1][loopCounter].length; i++){
						var typ2Num = $curBox.parent().find("input:eq("+i+")").val();
						if(typ2Num != corrAnswerstpy2[quesNo-1][loopCounter][i]){
								$me = $(this);
								incorrect_action($me);
								break;
						}
						if(i == (corrAnswerstpy2[quesNo-1][loopCounter].length-1)){
							correct_action($curBox);
						}
					}
				}
				else{
					var typ2Num = $curBox.parent().find("input").val();
					if(typ2Num == corrAnswerstpy2[quesNo-1][loopCounter]){
						correct_action($curBox);
					}
					else{
						$me = $(this);
						incorrect_action($me);
					}
				}
			});
			break;
			case 10:
			var corrArray = ["binopt1","binopt2","binopt3","binopt4","binopt5","binopt6"];
			var incoArray = ["monopt1","monopt2","monopt3","monopt4","monopt5","monopt6","triopt1","triopt2","triopt3","triopt4","triopt5","triopt6"];

			var finalArray = [];

			corrArray.shufflearray();
			incoArray.shufflearray();

			for(var i = 0; i < 3; i++){
				finalArray.push(corrArray[i]);
				$(".multimcqiten:eq("+i+")").html(eval('data.string.'+finalArray[i]));
			}
			for(var i = 3; i < 8; i++){
				finalArray.push(incoArray[i]);
				$(".multimcqiten:eq("+i+")").html(eval('data.string.'+finalArray[i]));
			}

			var parent = $(".multiopsblock");
			var divs = parent.children();
				 while (divs.length) {
				        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
				  }
			break;
			case 11:
			var corrArray = ["monopt1","monopt2","monopt3","monopt4","monopt5","monopt6"];
			var incoArray = ["binopt1","binopt2","binopt3","binopt4","binopt5","binopt6","triopt1","triopt2","triopt3","triopt4","triopt5","triopt6"];

			var finalArray = [];

			corrArray.shufflearray();
			incoArray.shufflearray();

			for(var i = 0; i < 3; i++){
				finalArray.push(corrArray[i]);
				$(".multimcqiten:eq("+i+")").html(eval('data.string.'+finalArray[i]));
			}
			for(var i = 3; i < 8; i++){
				finalArray.push(incoArray[i]);
				$(".multimcqiten:eq("+i+")").html(eval('data.string.'+finalArray[i]));
			}

			var parent = $(".multiopsblock");
			var divs = parent.children();
				 while (divs.length) {
				        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
				  }
			break;
			case 12:
			var corrArray = ["triopt1","triopt2","triopt3","triopt4","triopt5","triopt6"];
			var incoArray = ["monopt1","monopt2","monopt3","monopt4","monopt5","monopt6","binopt1","binopt2","binopt3","binopt4","binopt5","binopt6"];

			var finalArray = [];

			corrArray.shufflearray();
			incoArray.shufflearray();

			for(var i = 0; i < 3; i++){
				finalArray.push(corrArray[i]);
				$(".multimcqiten:eq("+i+")").html(eval('data.string.'+finalArray[i]));
			}
			for(var i = 3; i < 8; i++){
				finalArray.push(incoArray[i]);
				$(".multimcqiten:eq("+i+")").html(eval('data.string.'+finalArray[i]));
			}

			var parent = $(".multiopsblock");
			var divs = parent.children();
				 while (divs.length) {
				        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
				  }
			break;

      default:
    }
		texthighlight($board);

		function correct_action($curBox, fracfl){
			var addfactor = $curBox.parent().position();
			var posIt = $curBox.position();
			var icoLeft = posIt.left + addfactor.left + $curBox.width() + 50;
			var icoTop = posIt.top + 10;
				$curBox.parent().append("<img class='imagestarttyp2' src= 'images/correct.png'>");
				if(fracfl){
					if($(".fraction2").find("input").parent(".top").length)
						$(".imagestarttyp2:eq(0)").addClass("corrfrac");
					else
						$(".imagestarttyp2:eq(0)").addClass("corrfrac2");
				}
				loopCounter++;
				$curBox.hide(0);
				$curBox.parent().find("input").prop('disabled', true).css({
					"background":"#98C02E",
					"border":"0.1em solid #DEEF3C",
					"color":"white"
				});
				if($(".eachloop:eq("+loopCounter+")").length){
					$(".eachloop:eq("+loopCounter+")").show(0);
					$(".eachloop:eq("+loopCounter+")").find("#input1").focus();
				}
				else{
					if(scoreFlagSet == true)
						exeTemp.update(true);
					if(countNext != $total_page)
					$nextBtn.show();
				}
				play_correct_incorrect_sound(true);
		}

		function incorrect_action($me){
			scoreFlagSet = false;
			play_correct_incorrect_sound(false);
			$me.css({
				"background":"#FF0000",
				"border":"0.1em solid #980000",
				"color":"white"
			});
		}

		$("input").keypress(function (e) {
			if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {
				return false;
			}
			if(e.which == 13) {
				if($(this).siblings("input").length){
					var totinpNumbers = $(this).siblings("input").length;
					var curinpNumber = $(this).index();
					console.log(curinpNumber, totinpNumbers);
					if(curinpNumber == totinpNumbers)
						$(this).siblings(".chckbtn").trigger("click");
					else
					$(this).siblings("input:eq("+curinpNumber+")").focus();
				}
				else if($(this).siblings(".chckbtn").length)
					$(this).siblings(".chckbtn").trigger("click");
				else {
					$(this).parent().parent().siblings(".chckbtn").trigger("click");
				}
			}
		});
		/*for randomizing the options*/
		var parent = $(".optionsdiv");
		var divs = parent.children();
			 while (divs.length) {
			        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			  }

    function rand_generator(limit){
      var randNum = Math.floor((Math.random() * limit) + 1);
      return randNum;
    }

		$(".buttonsel").click(function(){
      var $curBox = $(this);
      var addfactor = $(".optionsdiv").position();
      var posIt = $curBox.position();
      var icoLeft;
			if($(".buttonsel").length > 2)
				icoLeft = posIt.left + addfactor.left + $curBox.width();
			else
				icoLeft = posIt.left + addfactor.left + $curBox.width() + 15;

			$(this).removeClass('forhover');
				if(ansClicked == false){
					if($(this).hasClass("correct")){

						if(wrngClicked == false){
							exeTemp.update(true);
						}
						var corval = $(this).text();
						$(".clickplace").text(corval).css("color","#0B9620");
						play_correct_incorrect_sound(1);
						$(this).css("background","#bed62f");
						$(this).css("border","5px solid #deef3c");
            $(this).css("color","white");
						//$('.hint_image').show(0);
            $(this).siblings(".corctopt").show(0).css({
              "left": icoLeft
            });

						$('.buttonsel').removeClass('forhover forhoverimg');
						ansClicked = true;


						if(countNext != $total_page)
						$nextBtn.show();
					}
					else{
						exeTemp.update(false);
						play_correct_incorrect_sound(0);
						$(this).css("background","#FF0000");
						$(this).css("border","5px solid #980000");
						$(this).css("color","white");
            $(this).siblings(".wrngopt").show(0).css({
              "left": icoLeft
            });
						wrngClicked = true;
					}
				}
			});

			$(".multimcqiten").click(function(){
				var $curBox = $(this);
				if($curBox.hasClass("forhover")){
					var addfactor = $(".multiopsblock").position();
					var posIt = $curBox.position();
					var icoLeft;
					icoLeft = posIt.left + $curBox.width() + 15;
					$(this).removeClass('forhover');
							if($(this).hasClass("corritem")){
								multiClick++;
								if(wrngClicked == false){
									multiCorrCounter++;
								}
								if(multiCorrCounter == 3){
									exeTemp.update(true);
								}
								if(multiClick == 3){
									$(".multimcqiten").removeClass("forhover");
									$nextBtn.show();
								}
								play_correct_incorrect_sound(1);
								$(this).css("background","#bed62f");
								$(this).css("border","5px solid #deef3c");
								$(this).css("color","white");
								//$('.hint_image').show(0);
								$(this).siblings(".corctopt").show(0).css({
									"left": icoLeft
								});
							}
							else{
								exeTemp.update(false);
								play_correct_incorrect_sound(0);
								$(this).css("background","#FF0000");
								$(this).css("border","5px solid #980000");
								$(this).css("color","white");
								$(this).siblings(".wrngopt").show(0).css({
									"left": icoLeft
								});
								wrngClicked = true;
							}
				}

				});
		/*======= SCOREBOARD SECTION ==============*/
	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		// for exeTempg purpose
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		// exeTempg purpose code ends


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		exeTemp.gotoNext();
		templateCaller();

	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

/*===============================================
  =            data highlight function            =
  ===============================================*/
  function texthighlight($highlightinside){
     //check if $highlightinside is provided
     typeof $highlightinside !== "object" ?
     alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
     null ;

     var $alltextpara = $highlightinside.find("*[data-highlight='true']");
     var stylerulename;
     var replaceinstring;
     var texthighlightstarttag;
     var texthighlightendtag   = "</span>";


     if($alltextpara.length > 0){
       $.each($alltextpara, function(index, val) {
         /*if there is a data-highlightcustomclass attribute defined for the text element
         use that or else use default 'parsedstring'*/
         $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
         (stylerulename = $(this).attr("data-highlightcustomclass")) :
         (stylerulename = "parsedstring") ;

         texthighlightstarttag = "<span class='"+stylerulename+"'>";
         replaceinstring       = $(this).html();
         replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
         replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


         $(this).html(replaceinstring);
       });
     }
   }
   /*=====  End of data highlight function  ======*/

	 /*===== This function splits the string in data into convential fraction used in mathematics =====*/
	 function splitintofractions($splitinside){
			typeof $splitinside !== "object" ?
			 alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			 null ;

			 var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
				if($splitintofractions.length > 0){
					$.each($splitintofractions, function(index, value){
					$this = $(this);
					var tobesplitfraction = $this.html();
					if($this.hasClass('fraction')){
						tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
						tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
					}else{
						tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
						tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
					}


			tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
					$this.html(tobesplitfraction);
				});
				}
		}

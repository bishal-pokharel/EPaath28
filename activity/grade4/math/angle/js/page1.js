var imgpath = $ref+"/images/page1/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

content = [

    {
        hascanvasblock: false,

        additionalclasscontentblock: 'opening-bg',
        uppertextblock: [
            {
                textdata: data.lesson.chapter,
                textclass: 'chapter-title'
            }
        ]
    },

    //1st slide
    {
        hasheaderblock: false,
		hascanvasblock: true,

		canvasid: 'canvas-first',
        additionalclasscontentblock: "page1-bg",
        uppertextblock: [
            {
                textdata: data.string.p1head1,
                textclass: 'lesson-title'
            }
        ],
        definitionblockadditionclass: "",

        definitionblock: [
			{
				definitiontext: data.string.p1text1,
				definitionclass: 'definition highlightbg definition1',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'

			}
        ],
    },

	//2nd slide
    {
        hasheaderblock: false,
		hascanvasblock: true,

		canvasid: 'canvas-first',
        additionalclasscontentblock: "page1-bg",
        uppertextblock: [
            {
                textdata: data.string.p1head1,
                textclass: 'lesson-title',
            }
        ],
        definitionblockadditionclass: "",

        definitionblock: [
			{
				definitiontext: data.string.p1text1,
				definitionclass: 'definition definition1',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p1text2,
				definitionclass: 'definition highlightbg definition2',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			}
        ],
    },

	//3rd slide
    {
        hasheaderblock: false,
		hascanvasblock: true,

		canvasid: 'canvas-first',
        additionalclasscontentblock: "page1-bg",
        uppertextblock: [
            {
                textdata: data.string.p1head1,
                textclass: 'lesson-title',
            }
        ],
        definitionblockadditionclass: "",

        definitionblock: [
			{
				definitiontext: data.string.p1text1,
				definitionclass: 'definition definition1',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p1text2,
				definitionclass: 'definition definition2',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p1text3,
				definitionclass: 'definition highlightbg definition3',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			}
        ],
    },

    // 4th slide
    {
        hasheaderblock: false,
		hascanvasblock: true,
        hasangleblock: true,

		canvasid: 'canvas-first',
        additionalclasscontentblock: "page1-bg",
        uppertextblock: [
            {
                textdata: data.string.p1head1,
                textclass: 'lesson-title',
            }
        ],
        definitionblock: [
			{
				definitiontext: data.string.p1text1,
				definitionclass: 'definition definition1',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p1text2,
				definitionclass: 'definition definition2',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p1text3,
				definitionclass: 'definition definition3',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p1text4,
				definitionclass: 'definition highlightbg definition4',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			}
        ],

        angleblock: [
			{
				angletext: data.string.angle1,
				angleclass: 'angle angle1',
			},
			{
				angletext: data.string.angle2,
				angleclass: 'angle angle2',
			},
			{
				angletext: data.string.angle3,
				angleclass: 'angle angle3',
			}
        ],
    },

   //5th slide
    {
      hasheaderblock: false,
	    hascanvasblock: true,
	    hasangleblock: true,

		  canvasid: 'canvas-first',
	        additionalclasscontentblock: "page1-bg",
	        definitionblockadditionclass: "",
			uppertextblock: [
	      {
	        textdata: data.string.p1head1,
	        textclass: 'lesson-title',
	      }
	    ],
	    definitionblock: [
				{
					definitiontext: data.string.p1text1,
					definitionclass: 'definition definition1',
					datahighlightflag: true,
					datahighlightcustomclass: 'highlighttext'
				},
				{
					definitiontext: data.string.p1text2,
					definitionclass: 'definition definition2',
					datahighlightflag: true,
					datahighlightcustomclass: 'highlighttext'
				},
				{
					definitiontext: data.string.p1text3,
					definitionclass: 'definition definition3',
					datahighlightflag: true,
					datahighlightcustomclass: 'highlighttext'
				},
				{
					definitiontext: data.string.p1text4,
					definitionclass: 'definition definition4',
					datahighlightflag: true,
					datahighlightcustomclass: 'highlighttext'
				},
				{
					definitiontext: data.string.p1text5,
					definitionclass: 'definition highlightbg definition5',
					datahighlightflag: true,
					datahighlightcustomclass: 'highlighttext'
				},
				{
					definitiontext: data.string.big,
					definitionclass: 'big-angle',
				},
				{
					definitiontext: data.string.small,
					definitionclass: 'small-angle',
				}
	    ],

	    angleblock: [
				{
					angletext: data.string.angle1,
					angleclass: 'angle angle1',
				},
				{
					angletext: data.string.angle2,
					angleclass: 'angle angle2',
				},
				{
					angletext: data.string.angle3,
					angleclass: 'angle angle3',
				}
	    ],
  	},

    //6th slide
    {
        hasheaderblock: false,
		hascanvasblock: true,
		hasangleblock: true,
		canvasid: 'canvas-first',
        additionalclasscontentblock: "page1-bg",
        definitionblockadditionclass: "",
		uppertextblock: [
            {
                textdata: data.string.p1head1,
                textclass: 'lesson-title',
            }
        ],
        definitionblock: [
			{
				definitiontext: data.string.p1text1,
				definitionclass: 'definition definition1',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p1text2,
				definitionclass: 'definition definition2',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p1text3,
				definitionclass: 'definition definition3',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p1text4,
				definitionclass: 'definition definition4',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p1text5,
				definitionclass: 'definition definition5',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p1text6,
				definitionclass: 'definition highlightbg definition6',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			}
        ],

        angleblock: [
			{
				angletext: data.string.angle1,
				angleclass: 'angle angle1',
			},
			{
				angletext: data.string.angle2,
				angleclass: 'angle angle2',
			},
			{
				angletext: data.string.angle3,
				angleclass: 'angle angle3',
			}
        ],
    },

];



$(function() {
    // $(window).resize(function() {
    //     generalTemplate();
  	// 	/*
		//   for (var i = 0; i < content.length; i++) {
		//     slides(i);
		//     $($('.totalsequence')[i]).html(i);
		//     $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		//   "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		//   }
		//   function slides(i){
		//       $($('.totalsequence')[i]).click(function(){
		//         countNext = i;
		//         templateCaller();
		//       });
		//     }
		// */
    //
    // });

    var $board = $(".board");
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var total_page = 0;

    var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

    // recalculateHeightWidth();
  	var angle1Show, angle2Show, angleShow, angle1Show2, outerTimeout1, outerTimeout2, smallAngle2, angle1HideTimer;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
			{id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
			{id: "s1_p4", src: soundAsset+"s1_p4.ogg"},
			{id: "s1_p5", src: soundAsset+"s1_p5.ogg"},
			{id: "s1_p6_1", src: soundAsset+"s1_p6_1.ogg"},
			{id: "s1_p6_big_angle", src: soundAsset+"s1_p6_big_angle.ogg"},
			{id: "s1_p6_small_angle", src: soundAsset+"s1_p6_small_angle.ogg"},
			{id: "s1_p7", src: soundAsset+"s1_p7.ogg"}
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();


    /*
    	inorder to use the handlebar partials we need to register them
    	to their respective handlebar partial pointer first
    */
    Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

  // Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	// Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

    // Handlebars.registerPartial("tablecontent", $("#tablecontent-partial").html());


    // controls the navigational state of the program
    function navigationController() {
    		if(countNext == 0 && total_page != 1){
					$nextBtn.show(0);
				}else if(countNext > 0 && countNext < (total_page-1)){
		      $nextBtn.show(0);
		      $prevBtn.show(0);
				}else if(countNext == total_page-1){
					$prevBtn.show(0);
		      $nextBtn.hide(0);
		    }

		    if (countNext != 5) {
		    	clearTimeout(angleShow);
		    	clearTimeout(angle2Show);
		    	clearTimeout(angle1Show);
		    	clearTimeout(angle1Show2);
		    	clearTimeout(smallAngle2);
		    	clearTimeout(outerTimeout1);
		    	clearTimeout(outerTimeout2);
		    	clearTimeout(angle1HideTimer);
		    }
    }
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        var requestAnimationFrame = window.requestAnimationFrame ;

        loadTimelineProgress($total_page,countNext+1);
        $board.html(html);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);
		vocabcontroller.findwords(countNext);
        $('.objecttext13').hide(0);
        $('.objecttext14').hide(0);
        $('.objecttext15').hide(0);

				var canvasBlock = $('.canvasblock');
				var canvas = $('#canvas-first');

				var $ang1 = 38;
				var $ang2 = 40;
				var $radius = 50;
				var $radius2 = 10;
				var $division1 = 180;
				var $division2 = 20;
				var $division3 = 10;
        // for last slide, canvas is not loaded so
        // validate canvas[0] object
        if (canvas[0]) {
            canvas[0].height = canvasBlock.height();
            canvas[0].width = canvasBlock.width();
            var ctx = canvas[0].getContext('2d');

            // For moving line
            var perpenStart = {
                x: canvas.width()*0.6,
                y: canvas.height()*0.6
            };
            var perpenEnd = {
                x: canvas.width()*0.87,
                y: canvas.height()*0.22
            };

            // For base line
            var baseStart = {
                x: canvas.width()*0.6,
                y: canvas.height()*0.6
            };
            var baseEnd = {
                x: canvas.width()*1,
                y: canvas.height()*0.6
            };

            var alpha = 0;
            var angle = 0;

            function drawBaseLine(width, color) {
                ctx.beginPath();
                ctx.moveTo(baseStart.x, baseStart.y);
                ctx.lineTo(baseEnd.x, baseEnd.y);
                ctx.lineWidth = width;
                ctx.strokeStyle = color;
                ctx.fillStyle = color;
                ctx.stroke();
            }

            function drawSecondLine(width, color) {
                ctx.beginPath();
                ctx.moveTo(perpenStart.x, perpenStart.y);
                ctx.lineTo(perpenEnd.x, perpenEnd.y);
                ctx.lineWidth = width;
                ctx.strokeStyle = color;
                ctx.fillStyle = color;
                ctx.stroke();
            }

            function drawArc(ang, radius, division) {
            	angle = ang;

              ctx.beginPath();
              ctx.arc(baseStart.x, baseStart.y, radius, 0, - angle * Math.PI / division, true);
   						ctx.strokeStyle="#FF9A4A";
	            ctx.lineWidth = 3;
	            ctx.stroke();
            }

            function drawArcSec(ang, radius, division,color) {
            	angle = ang;

              ctx.beginPath();
              ctx.arc(baseStart.x, baseStart.y, radius, 0, - angle * Math.PI / division, true);
   						ctx.strokeStyle=color;
	            ctx.lineWidth = 3;
	            ctx.stroke();
            }

            function angle1Hide(timeout) {
            	angle1HideTimer = setTimeout(function() {
            		$('.angle1').hide(0);
            	}, timeout);
            }
        }

        $prevBtn.hide(0);
        $nextBtn.hide(0);

        switch (countNext) {
				case 0:
					sound_player_nav("s1_p"+(countNext+1));
					break;
		    	case 1:
						$('.definition1').fadeIn(1000);
		    		$('.definition').show(0);

						//Line
						drawBaseLine(3, "#000000");
						drawSecondLine(3, "#000000");

						//Arc
						drawArc($ang1, $radius, $division1);

            function highlightArc(count){
              if(count%2 == 0){
                drawArcSec($ang1, $radius, $division1,"#000");
              }else{
                drawArcSec($ang1, $radius, $division1,"#ff9a4a");
              }
              count+=1;
              count<10?setTimeout(function(){highlightArc(count)},500):'';
            }
            setTimeout(function(){
              highlightArc(0);
            },2500);
            sound_player_nav("s1_p"+(countNext+1));
					break;

					case 2:
          sound_player_nav("s1_p"+(countNext+1));
						$('.definition2').fadeIn(1000);
		    		$('.definition').show(0);

						// Arc
						drawArc($ang2, $radius, $division1);

						// Line
						drawBaseLine(3, "#000000");
						drawSecondLine(3, "#000000");



		        setTimeout(function(){
		        	ctx.clearRect(0, 0, canvas.width(), canvas.height());
		        	drawArc($ang1, $radius, $division1);

		        	drawSecondLine(3, "#FF9A4A");
							drawBaseLine(3, "#000000");
		        }, 2000);
		        setTimeout(function(){
		        	ctx.clearRect(0, 0, canvas.width(), canvas.height());
		        	drawArc($ang1, $radius, $division1);

		        	drawSecondLine(3, "#000000");
							drawBaseLine(3, "#000000");
		        }, 2500);
		        setTimeout(function(){
		        	ctx.clearRect(0, 0, canvas.width(), canvas.height());
		        	drawArc($ang1, $radius, $division1);

		        	drawSecondLine(3, "#FF9A4A");
							drawBaseLine(3, "#000000");
		        }, 3000);
		        setTimeout(function(){
		        	ctx.clearRect(0, 0, canvas.width(), canvas.height());
		        	drawArc($ang1, $radius, $division1);

		        	drawSecondLine(3, "#000000");
							drawBaseLine(3, "#000000");
		        }, 3500);



		        setTimeout(function(){
		        	ctx.clearRect(0, 0, canvas.width(), canvas.height());
		        	drawArc($ang1, $radius, $division1);

		        	drawSecondLine(3, "#000000");
							drawBaseLine(3, "#FF9A4A");
		        }, 4000);
		        setTimeout(function(){
		        	ctx.clearRect(0, 0, canvas.width(), canvas.height());
		        	drawArc($ang1, $radius, $division1);

		        	drawSecondLine(3, "#000000");
							drawBaseLine(3, "#000000");
		        }, 4500);
		        setTimeout(function(){
		        	ctx.clearRect(0, 0, canvas.width(), canvas.height());
		        	drawArc($ang1, $radius, $division1);

		        	drawSecondLine(3, "#000000");
							drawBaseLine(3, "#FF9A4A");
		        }, 5000);
		        setTimeout(function(){
		        	ctx.clearRect(0, 0, canvas.width(), canvas.height());
		        	drawArc($ang1, $radius, $division1);

		        	drawSecondLine(3, "#000000");
							drawBaseLine(3, "#000000");
		        }, 5500);
		      break;

					case 3:
          sound_player_nav("s1_p"+(countNext+1));
						$('.definition3').fadeIn(1000);
        		$('.definition').show(0);
						// Angle
	  				drawArc($ang1, $radius, $division1);
							$('.angle1').css({
								transition: "0s",
								animation: "none",
								right: "24%",
								top: "33%",
							}).animate({animation: "none"});
						// Line
						drawBaseLine(3, "#000000");
						drawSecondLine(3, "#000000");


						// Circle point
            setTimeout(function(){
							drawArc($ang1, $radius2, $division2);
		        	ctx.fillStyle = "#FF9A4A";
		        	ctx.fill();
            }, 3000);
          break;

          case 4:
          sound_player_nav("s1_p"+(countNext+1));
						$('.definition4').fadeIn(1000);
        		$('.definition').show(0);

						// angle
	  				drawArc($ang1, $radius, $division1);

						drawBaseLine(3, "#000000");
						drawSecondLine(3, "#000000");

						// circle point
	  				drawArc($ang1, $radius2, $division3);
          	ctx.fillStyle = "#FF9A4A";
          	ctx.fill();
          break;

          case 5:
          sound_player_nav("s1_p"+(countNext+1+"_1"));
            angle = 40;

						$('.definition5').fadeIn(1000);
        		$('.definition').show(0);

						//Removing Css animation
						$('.angle1, .angle2, .angle3').css({
							opacity: 1,
							animation: "none",
						});

						$('.definition').fadeIn(1000);

						// angle
	  				drawArc($ang1, $radius, $division1);

						//Draw lines
						drawBaseLine(3, "#000000");
						drawSecondLine(3, "#000000");

						// circle point
	  				drawArc($ang1, $radius2, $division3);
          	ctx.fillStyle = "#FF9A4A";
          	ctx.fill();

		 				movingStart = {
	              x: canvas.width()*0.35,
	              y: canvas.height()*-0.1
	          };


            function rotateLineAntiClock() {
              ctx.clearRect(0, 0, canvas.width(), canvas.height());

	    				// Draw lines
              drawBaseLine(3, "#000000");
              // Draw arc
              ctx.beginPath();
              ctx.arc(baseStart.x, baseStart.y, 50, 0, -angle * Math.PI / 180, true);
              ctx.strokeStyle="#FF9A4A";
              ctx.stroke();

              // Rotate the context and draw line
              // To save canvas grid
              ctx.save();
              ctx.beginPath();
              ctx.translate(baseStart.x, baseStart.y);
              ctx.rotate(-(angle) * Math.PI / 180);
              //Drawing a line
              ctx.moveTo(0, 0);
              ctx.lineTo(movingStart.x, 0);
              // ctx.lineTo(100, 0);
              ctx.strokeStyle="#000000";
              ctx.stroke();
              ctx.restore();

              angle += 0.6;
              if (angle <= 110) {
                window.requestAnimationFrame(rotateLineAntiClock);

              } else {
								angle1Hide(2000);
								angle1Show2 = setTimeout( function() {
									$('.angle1').css({
										right: "18%",
										top: "45%",
									}).fadeIn(500);
								}, 4000);

                angleShow = setTimeout(function(){
                	$(".small-angle").fadeIn();
									$(".big-angle").fadeOut();
                	rotateLineClock();
                }, 3000);
              }


              // circle point
              ctx.beginPath();
            	ctx.arc(baseStart.x, baseStart.y, 10, 0, -360 * Math.PI / 180, true);
            	ctx.fillStyle = "#FF9A4A";
            	ctx.fill();
            }

						function rotateLineClock(){
              ctx.clearRect(0, 0, canvas.width(), canvas.height());
							// Draw a line
							drawBaseLine(3, "#000000");
							//Draw an arc
							ctx.beginPath();
							ctx.arc(baseStart.x, baseStart.y, 50, 0, -angle * Math.PI / 180, true);
              ctx.strokeStyle="#FF9A4A";
              ctx.stroke();

							//Rotate context and draw lines
							ctx.save();
							ctx.beginPath();
							ctx.translate(baseStart.x, baseStart.y);
							ctx.rotate(-(angle) * Math.PI / 180);
							ctx.moveTo(0, 0);
							ctx.lineTo(movingStart.x, 0);
              ctx.strokeStyle="#000000";
              ctx.stroke();
							ctx.restore();

          		angle -= 0.6;
          		if(angle >= 20) {
          			window.requestAnimationFrame(rotateLineClock);
          		}else {
          			smallAngle2 = setTimeout(function() {
									$(".small-angle").fadeOut();
          			}, 2000);
          		}

          		// circle point
              ctx.beginPath();
            	ctx.arc(baseStart.x, baseStart.y, 10, 0, -360 * Math.PI / 180, true);
            	ctx.fillStyle = "#FF9A4A";
            	ctx.fill();

						}

						outerTimeout1 = setTimeout(function(){
							window.requestAnimationFrame(rotateLineAntiClock);
							angle1Hide(1000);
							angle1Show = setTimeout( function() {
								$('.angle1').css({
									right: "51%",
									top: "21%",
								}).fadeIn(500);
								$(".big-angle").fadeIn();
							}, 1500);

 	           	outerTimeout2 = setTimeout(function() {
            		window.requestAnimationFrame(rotateLineAntiClock);
            		angle1Hide(1000);
								angle2Show = setTimeout( function() {
									$('.angle1').css({
										right: "51%",
										top: "21%",
									}).fadeIn(500);
									$(".big-angle").fadeIn();
								}, 2000);
            	}, 8500)
            }, 3500);
          break;

          case 6:
          sound_player_nav("s1_p"+(countNext+1));
          	$('.definition6').fadeIn(1000);
        		$('.definition').show(0);

						$('.angle1, .angle2, .angle3').css({
							opacity: 1,
							animation: "none",
						});

						// Draw line
						drawBaseLine(3);
						drawSecondLine(3);

						// Draw Arc
						drawArc($ang1, $radius, $division1);

						//Draw circle point
						drawArc($ang1, $radius2, $division2);
	        	ctx.fillStyle = "#FF9A4A";
	        	ctx.fill();

				ole.footerNotificationHandler.pageEndSetNotification();
          break;
          default:
            sound_player_nav("s1_p"+(countNext+1));
          break;
        }
    };

    	function sound_player(sound_id){
    		createjs.Sound.stop();
    		current_sound = createjs.Sound.play(sound_id);
    		current_sound.play();
    		// current_sound.on('complete', function(){
    		// 	navigationcontroller();
    		// });
    	}

    	function sound_player_nav(sound_id){
    		createjs.Sound.stop();
    		current_sound = createjs.Sound.play(sound_id);
    		current_sound.play();
    		current_sound.on('complete', function(){
    			nav_button_controls();
    		});
    	}
    function templateCaller() {
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

        navigationController();
		loadTimelineProgress($total_page, countNext + 1);

        generalTemplate();
    }

    $nextBtn.on("click", function() {
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
        countNext--;
        templateCaller();
    });

    // document.addEventListener('xmlLoad', function(e) {
        total_page = content.length;
        templateCaller();
    // });


});



/*===============================================
	 =            data highlight function            =
	 ===============================================*/
function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";


    if ($alltextpara.length > 0) {
        $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                (stylerulename = $(this).attr("data-highlightcustomclass")) :
                (stylerulename = "parsedstring");

            texthighlightstarttag = "<span>";


            replaceinstring = $(this).html();
            replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
            replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);


            $(this).html(replaceinstring);
        });
    }
}
/*=====  End of data highlight function  ======*/




var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

content = [
    // slide 1
    {
      additionalclasscontentblock: 'opening-bg',
      uppertextblock: [
        {
          textdata: data.string.p4text19,
          textclass: 'chapter-title'
        }
      ]
    },
    // // slide 2
    // {
    //   uppertextblock: [
    //     {
    //       textdata: data.string.p4text20,
    //       textclass: 'lesson-title'
    //     }
    //   ],
    //
    //   definitionblock: [{
    //       definitiontext: data.string.p4text21,
    //       definitionclass: 'definition definition1',
    //     },{
    //       definitiontext: data.string.p4text22,
    //       definitionclass: 'definition definition2',
    //     },{
    //       definitiontext: data.string.p4text23,
    //       definitionclass: 'definition definition3',
    //     },{
    //       definitiontext: data.string.p4text24,
    //       definitionclass: 'definition definition4',
    //     },{
    //       definitiontext: data.string.p4text25,
    //       definitionclass: 'definition definition5',
    //     }],
    //
    //   imageblock: [{
    //     imagetoshow: [{
    //         imgsrc: imgpath + 'page4/protractor.png',
    //         imgclass: 'protractor'
    //       },{
    //         imgsrc: imgpath + 'page4/scale.png',
    //         imgclass: 'ruler'
    //       },{
    //         imgsrc: imgpath + 'page4/pencil.png',
    //         imgclass: 'pencil'
    //       }],
    //   }],
    // },
    // slide 3
    {
      uppertextblock: [{
        textdata: data.string.p4text20,
        textclass: 'lesson-title'
      }],
      extratextblock:[{
        textdata: data.string.p4text21,
        textclass: 'drawInstruction'
      }],
      imageblock: [{
        imagetoshow: [{
            imgsrc: imgpath + 'page4/protractor.png',
            imgclass: 'protractor'
          },{
            imgsrc: imgpath + 'page4/scale.png',
            imgclass: 'ruler ruler3Anim' //ruler
          },{
            imgsrc: imgpath + 'page4/pencil.png',
            imgclass: 'pencil pencil3_Anim' //pencil
          }],
        imagelabels:[{
          imagelabelclass:"lineAnim stLineAnim_1",
          imagelabeldata:""
        }]
      }],
    },
    // slide 4
    {
      uppertextblock: [{
        textdata: data.string.p4text20,
        textclass: 'lesson-title'
      }],
      extratextblock:[{
        textdata: data.string.p4text22,
        textclass: 'drawInstruction'
      }],
      imageblock: [{
        imagetoshow: [{
            imgsrc: imgpath + 'page4/protractor.png',
            imgclass: 'protractor protractorAnim_4'
          },{
            imgsrc: imgpath + 'page4/scale.png',
            imgclass: 'ruler' //ruler
          },{
            imgsrc: imgpath + 'page4/pencil.png',
            imgclass: 'pencil' //pencil
          }],
        imagelabels:[{
          imagelabelclass:"lineAnimlong",
          imagelabeldata:""
        }]
      }],
    },
    // slide 5
    {
      uppertextblock: [{
        textdata: data.string.p4text20,
        textclass: 'lesson-title'
      }],
      extratextblock:[{
        textdata: data.string.p4text23,
        textclass: 'drawInstruction'
      }],
      imageblock: [{
        imagetoshow: [{
            imgsrc: imgpath + 'page4/protractor.png',
            imgclass: 'protractor_5'
          },{
            imgsrc: imgpath + 'page4/scale.png',
            imgclass: 'ruler' //ruler
          },{
            imgsrc: imgpath + 'page4/pencil.png',
            imgclass: 'pencil s5PencilAnim' //pencil
          }],
        imagelabels:[{
          imagelabelclass:"lineAnimlong",
          imagelabeldata:""
        },{
          imagelabelclass:"dot_50 showDot",
          imagelabeldata:""
        }]
      }],
    },
    // slide 6
    {
      uppertextblock: [{
        textdata: data.string.p4text20,
        textclass: 'lesson-title'
      }],
      extratextblock:[{
        textdata: data.string.p4text24,
        textclass: 'drawInstruction'
      }],
      imageblock: [{
        imagetoshow: [{
            imgsrc: imgpath + 'page4/protractor.png',
            imgclass: 'protractor'
          },{
            imgsrc: imgpath + 'page4/scale.png',
            imgclass: 'ruler ruler_s6' //ruler
          },{
            imgsrc: imgpath + 'page4/pencil.png',
            imgclass: 'pencil pencil_s6' //pencil
          }],
        imagelabels:[{
          imagelabelclass:"lineAnimlong",
          imagelabeldata:""
        },{
          imagelabelclass:"dot_50",
          imagelabeldata:""
        },{
          imagelabelclass:"line_50 drawLine_50",
          imagelabeldata:""
        }]
      }],
    },
    // slide 7
    {
      uppertextblock: [{
        textdata: data.string.p4text20,
        textclass: 'lesson-title'
      }],
      extratextblock:[{
        textdata: data.string.p4text25,
        textclass: 'drawInstruction'
      }],
      imageblock: [{
        imagetoshow: [{
            imgsrc: imgpath + 'page4/protractor.png',
            imgclass: 'protractor'
          },{
            imgsrc: imgpath + 'page4/scale.png',
            imgclass: 'ruler' //ruler
          },{
            imgsrc: imgpath + 'page4/pencil.png',
            imgclass: 'pencil' //pencil
          }],
        imagelabels:[{
          imagelabelclass:"lineAnimlong",
          imagelabeldata:""
        },{
          imagelabelclass:"dot_50",
          imagelabeldata:""
        },{
          imagelabelclass:"line_50_s7",
          imagelabeldata:""
        },{
          imagelabelclass:"arc_s7",
          imagelabeldata:""
        },{
          imagelabelclass:"DegTxt",
          imagelabeldata:data.string.q6_2
        }]
      }],
    },
];



$(function() {
    $(window).resize(function() {
        generalTemplate();
    });

    var $board = $(".board");
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var total_page = 0;

    var $total_page = content.length;
  	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
    // recalculateHeightWidth();

    	var preload;
    	var timeoutvar = null;
    	var current_sound;
      var stOut1,stOut2,stOut3,stOut4, stOut5;

    	function init() {
    		//specify type otherwise it will load assests as XHR
    		manifest = [
    			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
    			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
    			//   ,
    			//images
    			// {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

    			// soundsicon-orange
          {id: "s4_p1", src: soundAsset+"s4_p1.ogg"},
          {id: "s4_p2_1", src: soundAsset+"s4_p2_1.ogg"},
          {id: "s4_p2_2", src: soundAsset+"s4_p2_2.ogg"},
          {id: "s4_p2_3", src: soundAsset+"s4_p2_3.ogg"},
          {id: "s4_p2_4", src: soundAsset+"s4_p2_4.ogg"},
          {id: "s4_p2_5", src: soundAsset+"s4_p2_5.ogg"},
          {id: "s4_p2_6", src: soundAsset+"s4_p2_6.ogg"}
    		];
    		preload = new createjs.LoadQueue(false);
    		preload.installPlugin(createjs.Sound);//for registering sounds
    		preload.on("progress", handleProgress);
    		preload.on("complete", handleComplete);
    		preload.on("fileload", handleFileLoad);
    		preload.loadManifest(manifest, true);
    	}
    	function handleFileLoad(event) {
    		// console.log(event.item);
    	}
    	function handleProgress(event) {
    		$('#loading-text').html(parseInt(event.loaded*100)+'%');
    	}
    	function handleComplete(event) {
    		$('#loading-wrapper').hide(0);
    		//initialize varibales
    		current_sound = createjs.Sound.play('sound_1');
    		current_sound.stop();
    		// call main function
    		templateCaller();
    	}
    	//initialize
    	init();

    /*
      inorder to use the handlebar partials we need to register them
      to their respective handlebar partial pointer first
    */
    Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("degreecontent", $("#degreecontent-partial").html());

    // controls the navigational state of the program
    function navigationController(iflastpageflag) {
      iflastpageflag == false;

       if (countNext == 0 && total_page != 1) {
            $nextBtn.show(0);
        } else if (countNext > 0 && countNext < (total_page - 1)) {
            $nextBtn.show(0);
            $prevBtn.show(0);
        } else if (countNext == total_page - 1) {
            $prevBtn.show(0);
        }
   }
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

    // var canvasBlock;
    // var canvas;

    // function resizeCanvas() {

    // }

    function generalTemplate() {
      var source = $("#general-template").html();
      var template = Handlebars.compile(source);
      var html = template(content[countNext]);

      loadTimelineProgress($total_page,countNext+1);
      $board.html(html);
	  vocabcontroller.findwords(countNext);

      clearTimeout(stOut1);
      clearTimeout(stOut2);
      clearTimeout(stOut3);
      clearTimeout(stOut4);
      clearTimeout(stOut5);

      $prevBtn.hide(0);
      $nextBtn.hide(0);
      switch (countNext) {
        case 0:
          sound_player_nav("s4_p"+(countNext+1));
        break;
        case 1:
          sound_player("s4_p2_1");
        		createjs.Sound.stop();
        		current_sound = createjs.Sound.play("s4_p2_1");
        		current_sound.play();
            current_sound.on('complete', function(){
          		createjs.Sound.stop();
          		current_sound = createjs.Sound.play("s4_p2_2");
          		current_sound.play();
              current_sound.on('complete', function(){
          			nav_button_controls();
              });
            });
        break;
        case 2:
          sound_player_nav("s4_p2_3");
        break;
        case 3:
          sound_player_nav("s4_p2_4");
        break;
        case 4:
          sound_player_nav("s4_p2_5");
        break;
        case 5:
          sound_player_nav("s4_p2_6");
        break;
        default:
    			nav_button_controls();
          // sound_player_nav("s1_p"+(countNext+1));
        break;
      }

      };

  	function sound_player(sound_id){
  		createjs.Sound.stop();
  		current_sound = createjs.Sound.play(sound_id);
  		current_sound.play();
  		// current_sound.on('complete', function(){
  		// 	navigationcontroller();
  		// });
  	}

  	function sound_player_nav(sound_id){
  		createjs.Sound.stop();
  		current_sound = createjs.Sound.play(sound_id);
  		current_sound.play();
  		current_sound.on('complete', function(){
  			nav_button_controls();
  		});
  	}
    function templateCaller() {
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

        navigationController();
	      loadTimelineProgress($total_page, countNext + 1);

        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


    }

     // document.addEventListener('xmlLoad', function(e) {
      total_page = content.length;
      templateCaller();
    // });

    $nextBtn.click(function() {
        countNext++;
        templateCaller();
        clearTimeout(stOut1);
        clearTimeout(stOut2);
        clearTimeout(stOut3);
        clearTimeout(stOut4);
        clearTimeout(stOut5);
    });

    $prevBtn.click(function() {
        countNext--;
        templateCaller();
    });




});

function nextBtnNotifier(nextBtnNotify, $nextBtn) {
  if (nextBtnNotify == 4) {
    console.log(nextBtnNotify);
    // ole.footerNotificationHandler.lessonEndSetNotification();
  }
}

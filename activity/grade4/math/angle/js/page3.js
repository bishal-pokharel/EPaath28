  var imgpath = $ref+"/images/";
  var soundAsset = $ref+"/sounds/"+$lang+"/";

  content = [
//Slide 1
    {
    hascanvasblock: false,

    additionalclasscontentblock: 'opening-bg',
    uppertextblock: [
    {
      textdata: data.string.p3head1,
      textclass: 'chapter-title'
    }
    ]
  },

// //Slide 2
//     {
//         hasheaderblock: false,
//         hascanvasblock: true,
//
//         canvasid: 'canvas-first',
//         additionalclasscontentblock: "page1-bg",
//         uppertextblock: [
//         {
//           textdata: data.string.p3subhead1,
//           textclass: 'lesson-title'
//         }
//         ],
//         definitionblockadditionclass: "",
//
//         definitionblock: [
//         {
//           definitiontext: data.string.p3text1,
//           definitionclass: 'definition definition1'
//         },
//         {
//           definitiontext: data.string.p3text2,
//           definitionclass: 'definition definition2'
//         },
//         {
//           definitiontext: data.string.p3text3,
//           definitionclass: 'definition definition3'
//         },
//         {
//           definitiontext: data.string.p3text4,
//           definitionclass: 'definition definition4'
//         },
//         ],
//         animationblock: [{
//           imagetoshow: [{
//             imgsrc: imgpath + 'page4/protractor.png',
//             imgclass: 'protractor'
//           }],
//         }],
//       },

//Slide 2
  {
    uppertextblock: [{
      textdata: data.string.p3subhead1,
      textclass: 'lesson-title'
    }],
    extratextblock:[{
      textdata: data.string.p3text1,
      textclass: 'readInstrn'
    }],
    animationblock:[{
        imagetoshow:[{
          imgsrc: imgpath +"page3/protractor.png",
          imgclass:"protratctor prot_s2_deformed",
        }],
        imagelabels:[{
          imagelabelclass:"stLine",
          imagelabeldata:""
        },{
          imagelabelclass:"angledLine",
          imagelabeldata:""
        }]
      }]
  },
  //Slide 3
    {
      uppertextblock: [{
        textdata: data.string.p3subhead1,
        textclass: 'lesson-title'
      }],
      extratextblock:[{
        textdata: data.string.p3text2,
        textclass: 'readInstrn'
      }],
      animationblock:[{
          imagetoshow:[{
            imgsrc: imgpath +"page3/protractor.png",
            imgclass:"protratctor prot_s3",
          }],
          imagelabels:[{
            imagelabelclass:"stLine",
            imagelabeldata:""
          },{
            imagelabelclass:"angledLine",
            imagelabeldata:""
          }]
        }]
    },
  //Slide 4
    {
      uppertextblock: [{
        textdata: data.string.p3subhead1,
        textclass: 'lesson-title'
      }],
      extratextblock:[{
        textdata: data.string.p3text3,
        textclass: 'readInstrn'
      }],
      animationblock:[{
          imagetoshow:[{
            imgsrc: imgpath +"page3/protractor.png",
            imgclass:"protratctor prot_s4",
          }],
          imagelabels:[{
            imagelabelclass:"stLine",
            imagelabeldata:""
          },{
            imagelabelclass:"angledLine",
            imagelabeldata:""
          }]
        }]
    },
  //Slide 5
    {
      uppertextblock: [{
        textdata: data.string.p3subhead1,
        textclass: 'lesson-title'
      }],
      extratextblock:[{
        textdata: data.string.p3text4,
        textclass: 'readInstrn'
      }],
      animationblock:[{
          imagetoshow:[{
            imgsrc: imgpath +"page3/protractor.png",
            imgclass:"protratctor prot_s4",
          }],
          imagelabels:[{
            imagelabelclass:"stLine",
            imagelabeldata:""
          },{
            imagelabelclass:"angledLine",
            imagelabeldata:""
          },{
            imagelabelclass:"numCross",
            imagelabeldata:""
          }]
        }]
    },

//Slide 6
    {
      uppertextblock: [{
        textdata: data.string.p3subhead1,
        textclass: 'lesson-title'
      }],
      textcontent: [{
        textblock: [{
          text: data.string.right1,
          textclass: "right-text1",
        }],
      }],
      extratextblock:[{
        textdata: data.string.p3text5_1,
        textclass: "mcqQn",
      }],
      imageblock: [{
        mcqimg: [{
          imgsrc: imgpath + "page3/img01.png",
          imgclass: "qnImg"
        }]
      }],
      mcqblock:[{
        mcqcontainerclass:"mcqContainer",
        mcqoption:[{
          optiondata:data.string.p3deg1,
          optionaddnalclass:"class2",
        },{
          optiondata:data.string.p3deg2,
          optionaddnalclass:"class1",
        },{
          optiondata:data.string.p3deg3,
          optionaddnalclass:"class3",
        }]
      }]
    },
//Slide 7
    {
      uppertextblock: [{
        textdata: data.string.p3subhead1,
        textclass: 'lesson-title'
      }],
      textcontent: [{
        textblock: [{
          text: data.string.right1,
          textclass: "right-text1",
        }],
      }],
      extratextblock:[{
        textdata: data.string.p3text5_1,
        textclass: "mcqQn",
      }],
      imageblock: [{
        mcqimg: [{
          imgsrc: imgpath + "page3/img02.png",
          imgclass: "qnImg"
        }]
      }],
      mcqblock:[{
        mcqcontainerclass:"mcqContainer",
        mcqoption:[{
          optiondata:data.string.p3deg8,
          optionaddnalclass:"class2",
        },{
          optiondata:data.string.p3deg7,
          optionaddnalclass:"class1",
        },{
          optiondata:data.string.p3deg4,
          optionaddnalclass:"class3",
        }]
      }]
},
//Slide 8
    {
      uppertextblock: [{
        textdata: data.string.p3subhead1,
        textclass: 'lesson-title'
      }],
      textcontent: [{
        textblock: [{
          text: data.string.right1,
          textclass: "right-text1",
        }],
      }],
      extratextblock:[{
        textdata: data.string.p3text5_1,
        textclass: "mcqQn",
      }],
      imageblock: [{
        mcqimg: [{
          imgsrc: imgpath + "page3/img03.png",
          imgclass: "qnImg"
        }]
      }],
      mcqblock:[{
        mcqcontainerclass:"mcqContainer",
        mcqoption:[{
          optiondata:data.string.p3deg2,
          optionaddnalclass:"class2",
        },{
          optiondata:data.string.p3deg8,
          optionaddnalclass:"class1",
        },{
          optiondata:data.string.p3deg1,
          optionaddnalclass:"class3",
        }]
      }]
    },
//Slide 9
    {
      uppertextblock: [{
        textdata: data.string.p3subhead1,
        textclass: 'lesson-title'
      }],
      textcontent: [{
        textblock: [{
          text: data.string.right1,
          textclass: "right-text1",
        }],
      }],
      extratextblock:[{
        textdata: data.string.p3text5_1,
        textclass: "mcqQn",
      }],
      imageblock: [{
        mcqimg: [{
          imgsrc: imgpath + "page3/img04.png",
          imgclass: "qnImg"
        }]
      }],
      mcqblock:[{
        mcqcontainerclass:"mcqContainer",
        mcqoption:[{
          optiondata:data.string.p3deg7,
          optionaddnalclass:"class2",
        },{
          optiondata:data.string.p3deg4,
          optionaddnalclass:"class1",
        },{
          optiondata:data.string.p3deg1,
          optionaddnalclass:"class3",
        }]
      }]
    },
  ];

  $(function() {
    $(window).resize(function() {
      generalTemplate();
    });

    var $board = $(".board");
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var total_page = 0;
    var settime;

    var $total_page = content.length;
    loadTimelineProgress($total_page,countNext+1);
  	var preload;
  	var timeoutvar = null;
  	var current_sound;


  	function init() {
  		//specify type otherwise it will load assests as XHR
  		manifest = [
  			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
  			//images
  			// {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

  			// soundsicon-orange
        {id: "s3_p1", src: soundAsset+"s3_p1.ogg"},
        {id: "s3_p2_1", src: soundAsset+"s3_p2_1.ogg"},
        {id: "s3_p2_2", src: soundAsset+"s3_p2_2.ogg"},
        {id: "s3_p2_3", src: soundAsset+"s3_p2_3.ogg"},
        {id: "s3_p2_4", src: soundAsset+"s3_p2_4.ogg"},
        {id: "s3_p3", src: soundAsset+"s3_p3_1.ogg"},
        {id: "s3_p3a", src: soundAsset+"s3_p3.ogg"},
        {id: "s3_p4", src: soundAsset+"s3_p4.ogg"},
        {id: "s3_p5", src: soundAsset+"s3_p5.ogg"}
  		];
  		preload = new createjs.LoadQueue(false);
  		preload.installPlugin(createjs.Sound);//for registering sounds
  		preload.on("progress", handleProgress);
  		preload.on("complete", handleComplete);
  		preload.on("fileload", handleFileLoad);
  		preload.loadManifest(manifest, true);
  	}
  	function handleFileLoad(event) {
  		// console.log(event.item);
  	}
  	function handleProgress(event) {
  		$('#loading-text').html(parseInt(event.loaded*100)+'%');
  	}
  	function handleComplete(event) {
  		$('#loading-wrapper').hide(0);
  		//initialize varibales
  		current_sound = createjs.Sound.play('sound_1');
  		current_sound.stop();
  		// call main function
  		templateCaller();
  	}
  	//initialize
  	init();


  /*
    inorder to use the handlebar partials we need to register them
    to their respective handlebar partial pointer first
    */
    Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("degreecontent", $("#degreecontent-partial").html());
    Handlebars.registerPartial("animationcontent", $("#animationcontent-partial").html());

  // controls the navigational state of the program
  function navigationController(iflastpageflag) {
    iflastpageflag == false;

    if (countNext == 0 && total_page != 1) {
      $nextBtn.show();
    } else if (countNext > 0 && countNext < (total_page - 1)) {
      $nextBtn.show();
      $prevBtn.show();
    } else if (countNext == total_page - 1) {
      $prevBtn.show();
    }
  }
function nav_button_controls(delay_ms){
	timeoutvar = setTimeout(function(){
		if(countNext==0){
			$nextBtn.show(0);
		} else if( countNext>0 && countNext == $total_page-1){
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		} else{
			$prevBtn.show(0);
			$nextBtn.show(0);
		}
	},delay_ms);
}

  // var canvasBlock;
  // var canvas;

  // function resizeCanvas() {

  // }

  /*for randomizing the options*/
  function randomize(){
    var parent = $(".optionsblock");
    var divs = parent.children();
    while (divs.length) {
      parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
    }
  }
  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    var requestAnimationFrame = window.requestAnimationFrame ;

    loadTimelineProgress($total_page,countNext+1);
    $board.html(html);

      // highlight any text inside board div with datahighlightflag set true
      texthighlight($board);
      // vocabcontroller.findwords(countNext);
      randomize();
      $('.objecttext13').hide();
      $('.objecttext14').hide();
      $('.objecttext15').hide();

      var canvasBlock = $('.canvasblock');
      var canvas = $('#canvas-first');

      // for last slide, canvas is not loaded so
      // validate canvas[0] object
      if (canvas[0]) {
        canvas[0].height = canvasBlock.height();
        canvas[0].width = canvasBlock.width();
        var ctx = canvas[0].getContext('2d');

      }
      $prevBtn.hide(0);
      $nextBtn.hide(0);
      switch (countNext) {
        case 1:
          sound_player_nav("s3_p2_1");
        break;
        case 2:
          sound_player_nav("s3_p2_2");
        break;
        case 3:
          sound_player_nav("s3_p4");
        break;
        case 4:
          sound_player_nav("s3_p5");
        break;
        case 5:
        case 6:
        case 7:
        case 8:
          sound_player("s3_p3a");
          $nextBtn.hide(0);
          $(".optclass").click(function(){
            current_sound.stop();
            if($(this).hasClass('class1')){
              play_correct_incorrect_sound(1);
              $(this).siblings(".correct").show();
              $(this).addClass("corAns");
              $(".optclass").css("pointer-events","none");
        			nav_button_controls();
            }else{
              play_correct_incorrect_sound(0);
              $(this).siblings(".incorrect").show();
              $(this).addClass("incorAns");
            }
          });
        break;
        default:
          sound_player_nav("s3_p"+(countNext+1));
        break;
      }

    };
  	function sound_player(sound_id){
  		createjs.Sound.stop();
  		current_sound = createjs.Sound.play(sound_id);
  		current_sound.play();
  		// current_sound.on('complete', function(){
  		// 	navigationcontroller();
  		// });
  	}

  	function sound_player_nav(sound_id){
  		createjs.Sound.stop();
  		current_sound = createjs.Sound.play(sound_id);
  		current_sound.play();
  		current_sound.on('complete', function(){
  			nav_button_controls();
  		});
  	}
    function templateCaller() {
      //convention is to always hide the prev and next button and show them based
      //on the convention or page index
      $prevBtn.hide(0);
      $nextBtn.hide(0);

      navigationController();
      loadTimelineProgress($total_page, countNext + 1);

      generalTemplate();
		/*
	  for (var i = 0; i < content.length; i++) {
	    slides(i);
	    $($('.totalsequence')[i]).html(i);
	    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
	  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
	  }
	  function slides(i){
	      $($('.totalsequence')[i]).click(function(){
	        countNext = i;
	        templateCaller();
	      });
	    }
	*/


    }

  // document.addEventListener('xmlLoad', function(e) {
    total_page = content.length;
    templateCaller();
  // });

  $nextBtn.click(function() {
    createjs.Sound.stop();
    countNext++;
    templateCaller();
  });

  $prevBtn.click(function() {
    countNext--;
    templateCaller();
  });




});

  function nextBtnNotifier(nextBtnNotify, $nextBtn) {
  if (nextBtnNotify == 4) {
    console.log(nextBtnNotify);
    ole.footerNotificationHandler.pageEndSetNotification();
  }
}

/*===============================================
   =            data highlight function            =
   ===============================================*/
   function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
    alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
    null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";


    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
            (stylerulename = $(this).attr("data-highlightcustomclass")) :
            (stylerulename = "parsedstring");

            texthighlightstarttag = "<span>";


            replaceinstring = $(this).html();
            replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
            replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);


            $(this).html(replaceinstring);
          });
    }
  }
  /*=====  End of data highlight function  ======*/

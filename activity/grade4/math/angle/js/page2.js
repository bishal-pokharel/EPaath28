var imgpath = $ref+ "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

content = [

    {
        additionalclasscontentblock: 'opening-bg',
        uppertextblock: [
            {
                textdata: data.string.p2head1,
                textclass: 'chapter-title'
            }
        ]
    },

    // 1st slide
    {
       	hascanvasblock: true,

		canvasid: 'canvas-first',
        additionalclasscontentblock: "page1-bg",
        uppertextblock: [
            {
                textdata: data.string.p2subhead1,
                textclass: 'lesson-title'
            }
        ],

        definitionblock: [
			{
				definitiontext: data.string.p2text1,
				definitionclass: 'definition highlightbg definition1',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'

			}
        ],

        imageblock: [
        	{
		    	imagetoshow: [
		    		{
		    			imgsrc: imgpath + 'page2/protractor01.png',
		    			imgclass: 'protractor'
		    		}
		    	],
	    	}
        ],
    },

	// 2nd slide
    {
		hascanvasblock: true,

		canvasid: 'canvas-first',
        additionalclasscontentblock: "page1-bg",
        uppertextblock: [
            {
                textdata: data.string.p2subhead1,
                textclass: 'lesson-title',
            }
        ],
        definitionblockadditionclass: "",

        definitionblock: [
			{
				definitiontext: data.string.p2text1,
				definitionclass: 'definition definition1',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p2text2,
				definitionclass: 'definition highlightbg definition2',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			}
        ],

        imageblock: [
        	{
		    	imagetoshow: [
		    		{
		    			imgsrc: imgpath + 'page2/protractor02.png',
		    			imgclass: 'protractor'
		    		}
		    	],
	    	}
        ],
    },

	// 3rd slide
    {
		hascanvasblock: true,

		canvasid: 'canvas-first',
        additionalclasscontentblock: "page1-bg",
        uppertextblock: [
            {
                textdata: data.string.p2subhead1,
                textclass: 'lesson-title',
            }
        ],
        definitionblockadditionclass: "",

        definitionblock: [
			{
				definitiontext: data.string.p2text1,
				definitionclass: 'definition definition1',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p2text2,
				definitionclass: 'definition definition2',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p2text3,
				definitionclass: 'definition highlightbg definition3',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			}
        ],

        imageblock: [
        	{
		    	imagetoshow: [
		    		{
		    			imgsrc: imgpath + 'page2/protractor03.png',
		    			imgclass: 'protractor'
		    		}
		    	],
	    	}
        ],
    },

	// 4th slide
    {
		hascanvasblock: true,

		canvasid: 'canvas-first',
        additionalclasscontentblock: "page1-bg",
        uppertextblock: [
            {
                textdata: data.string.p2subhead1,
                textclass: 'lesson-title',
            }
        ],
        definitionblock: [
			{
				definitiontext: data.string.p2text1,
				definitionclass: 'definition definition1',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p2text2,
				definitionclass: 'definition definition2',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p2text3,
				definitionclass: 'definition definition3',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p2text4,
				definitionclass: 'definition highlightbg definition4',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			}
        ],

        imageblock: [
        	{
		    	imagetoshow: [
		    		{
		    			imgsrc: imgpath + 'page2/protractor04.png',
		    			imgclass: 'protractor'
		    		}
		    	],
	    	}
        ],
    },

    // 5th slide
    {
	    hascanvasblock: true,

	  	canvasid: 'canvas-first',
        additionalclasscontentblock: "page1-bg",
        definitionblockadditionclass: "",
		uppertextblock: [
            {
                textdata: data.string.p2subhead1,
                textclass: 'lesson-title',
            }
        ],
        definitionblock: [
			{
				definitiontext: data.string.p2text1,
				definitionclass: 'definition definition1',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p2text2,
				definitionclass: 'definition definition2',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p2text3,
				definitionclass: 'definition definition3',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p2text4,
				definitionclass: 'definition definition4',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p2text5,
				definitionclass: 'definition highlightbg definition5',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			}
        ],

        imageblock: [
        	{
		    	imagetoshow: [
		    		{
		    			imgsrc: imgpath + 'page2/protractor01.png',
		    			imgclass: 'protractor protractor1'
		    		},

		    		{
		    			imgsrc: imgpath + 'page2/protractor02.png',
		    			imgclass: 'protractor protractor2'
		    		},

		    		{
		    			imgsrc: imgpath + 'page2/protractor03.png',
		    			imgclass: 'protractor protractor3'
		    		},

		    		{
		    			imgsrc: imgpath + 'page2/protractor04.png',
		    			imgclass: 'protractor protractor4'
		    		},
		    	],
	    	}
        ],
    },

    //6th slide
    {
        additionalclasscontentblock: 'opening-bg',
        uppertextblock: [
            {
                textdata: data.string.p2head2,
                textclass: 'chapter-title'
            }
        ]
	},

	// 7th slide
	{
	    hascanvasblock: true,

	  	canvasid: 'canvas-first',
        additionalclasscontentblock: "page1-bg",
        definitionblockadditionclass: "",
		uppertextblock: [
            {
                textdata: data.string.p2subhead1,
                textclass: 'lesson-title',
            }
        ],

		definitionblock: [
			{
				definitiontext: data.string.p2text6,
				definitionclass: 'definition highlightbg definition6',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			}
		],

		imageblock: [
			{
				imagetoshow: [
					{
						imgsrc: imgpath + 'page2/protractor02.png',
						imgclass: 'protractor'
					}
				]
			}
		],

	},

	// 8th slide
	{
	    hascanvasblock: true,

	  	canvasid: 'canvas-first',
        additionalclasscontentblock: "page1-bg",
        definitionblockadditionclass: "",
		uppertextblock: [
            {
                textdata: data.string.p2subhead1,
                textclass: 'lesson-title',
            }
        ],

		definitionblock: [
			{
				definitiontext: data.string.p2text6,
				definitionclass: 'definition definition6',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p2text7,
				definitionclass: 'definition highlightbg definition7',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			}
		],

		subdefinitionblock: [
			{
				subdefinitiontext: data.string.p2subtext1,
				subdefinitionclass: 'subdefinition subtext1',
			},
		],

		imageblock: [
			{
				imagetoshow: [
					{
						imgsrc: imgpath + 'page2/protractor02.png',
						imgclass: 'protractor'
					},
					{
						imgsrc: imgpath + "handicon.gif",
						imgclass: 'handicon'
					},
					{
						imgsrc: imgpath + "arrowicon.png",
						imgclass: 'arrowicon arrowicon1'
					}
				]
			}
		],
	},
	// 9th slide
	{
	    hascanvasblock: true,

	  	canvasid: 'canvas-first',
        additionalclasscontentblock: "page1-bg",
        definitionblockadditionclass: "",
		uppertextblock: [
            {
                textdata: data.string.p2subhead1,
                textclass: 'lesson-title',
            }
        ],

		definitionblock: [
			{
				definitiontext: data.string.p2text6,
				definitionclass: 'definition definition6',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p2text8,
				definitionclass: 'definition highlightbg definition8',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			}
		],

		subdefinitionblock: [
			{
				subdefinitiontext: data.string.p2subtext1,
				subdefinitionclass: 'subdefinition subtext1',
			}
		],

		imageblock: [
			{
				imagetoshow: [
					{
						imgsrc: imgpath + 'page2/protractor02.png',
						imgclass: 'protractor'
					}
				]
			}
		],
	},

	// 10th slide
	{
	    hascanvasblock: true,

	  	canvasid: 'canvas-first',
        additionalclasscontentblock: "page1-bg",
        definitionblockadditionclass: "",
		uppertextblock: [
            {
                textdata: data.string.p2subhead1,
                textclass: 'lesson-title',
            }
        ],

		definitionblock: [
			{
				definitiontext: data.string.p2text6,
				definitionclass: 'definition definition6',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p2text8,
				definitionclass: 'definition definition8',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p2text9,
				definitionclass: 'definition highlightbg definition9',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			}
		],

		subdefinitionblock: [
			{
				subdefinitiontext: data.string.p2subtext1,
				subdefinitionclass: 'subdefinition subtext1',
			},
			{
				subdefinitiontext: data.string.p2subtext2,
				subdefinitionclass: 'subdefinition subtext2',
			},
		],

		imageblock: [
			{
				imagetoshow: [
					{
						imgsrc: imgpath + 'page2/protractor02.png',
						imgclass: 'protractor'
					},
					{
						imgsrc: imgpath + "handicon.gif",
						imgclass: 'handicon'
					},
					{
						imgsrc: imgpath + "arrowicon.png",
						imgclass: 'arrowicon arrowicon1'
					},
					{
						imgsrc: imgpath + "arrowicon.png",
						imgclass: 'arrowicon arrowicon2'
					}
				]
			}
		],
	},

	// 11th slide
	{
	    hascanvasblock: true,

	  	canvasid: 'canvas-first',
        additionalclasscontentblock: "page1-bg",
        definitionblockadditionclass: "",
		uppertextblock: [
            {
                textdata: data.string.p2subhead1,
                textclass: 'lesson-title',
            }
        ],

		definitionblock: [
			{
				definitiontext: data.string.p2text6,
				definitionclass: 'definition definition6',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p2text8,
				definitionclass: 'definition definition8',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p2text10,
				definitionclass: 'definition highlightbg definition10',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			}
		],

		subdefinitionblock: [
			{
				subdefinitiontext: data.string.p2subtext1,
				subdefinitionclass: 'subdefinition subtext1',
			},
			{
				subdefinitiontext: data.string.p2subtext2,
				subdefinitionclass: 'subdefinition subtext2',
			},
			{
				subdefinitiontext: data.string.p2subtext3,
				subdefinitionclass: 'subdefinition subtext3',
			},
		],

		imageblock: [
			{
				imagetoshow: [
					{
						imgsrc: imgpath + 'page2/protractor02.png',
						imgclass: 'protractor'
					},
					{
						imgsrc: imgpath + "arrowicon.png",
						imgclass: 'arrowicon arrowicon1'
					},
					{
						imgsrc: imgpath + "arrowicon.png",
						imgclass: 'arrowicon arrowicon2'
					},
					{
						imgsrc: imgpath + "arrowicon.png",
						imgclass: 'arrowicon arrowicon3'
					}
				]
			}
		],
	},

	// 12th slide
	{
	    hascanvasblock: true,

	  	canvasid: 'canvas-first',
        additionalclasscontentblock: "page1-bg",
        definitionblockadditionclass: "",
		uppertextblock: [
            {
                textdata: data.string.p2subhead1,
                textclass: 'lesson-title',
            }
        ],

		definitionblock: [
			{
				definitiontext: data.string.p2text6,
				definitionclass: 'definition definition6',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p2text8,
				definitionclass: 'definition definition8',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p2text10,
				definitionclass: 'definition definition10',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p2text11,
				definitionclass: 'definition highlightbg definition11',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			},
			{
				definitiontext: data.string.p2text12,
				definitionclass: 'definition highlightbg definition12',
				datahighlightflag: true,
				datahighlightcustomclass: 'highlighttext'
			}
		],

		subdefinitionblock: [
			{
				subdefinitiontext: data.string.p2subtext1,
				subdefinitionclass: 'subdefinition subtext1',
			},
			{
				subdefinitiontext: data.string.p2subtext2,
				subdefinitionclass: 'subdefinition subtext2',
			},
			{
				subdefinitiontext: data.string.p2subtext3,
				subdefinitionclass: 'subdefinition subtext3',
			},
			{
				subdefinitiontext: data.string.p2subtext4,
				subdefinitionclass: 'subdefinition subtext4',
			},
		],

		imageblock: [
			{
				imagetoshow: [
					{
						imgsrc: imgpath + 'page2/protractor02.png',
						imgclass: 'protractor'
					},
					{
						imgsrc: imgpath + "arrowicon.png",
						imgclass: 'arrowicon arrowicon1'
					},
					{
						imgsrc: imgpath + "arrowicon.png",
						imgclass: 'arrowicon arrowicon2'
					},
					{
						imgsrc: imgpath + "arrowicon.png",
						imgclass: 'arrowicon arrowicon3'
					},
					{
						imgsrc: imgpath + "arrowicon.png",
						imgclass: 'arrowicon arrowicon4'
					}
				]
			}
		],
	},

	// 13th slide
	{
	    hascanvasblock: true,

	  	canvasid: 'canvas-first',
        additionalclasscontentblock: "page1-bg",
        definitionblockadditionclass: "",
		uppertextblock: [
            {
                textdata: data.string.p2subhead1,
                textclass: 'lesson-title',
            }
        ],

		definitionblock: [
			{
				definitiontext: data.string.p2_s14,
				definitionclass: 'definition_1 definition6',
				datahighlightflag: true,
				datahighlightcustomclass: 'obo_text'
			}
		],

		subdefinitionblock: [
			{
				subdefinitiontext: data.string.p2subtext1,
				subdefinitionclass: 'subdefinition subtext1',
			},
			{
				subdefinitiontext: data.string.p2subtext2,
				subdefinitionclass: 'subdefinition subtext2',
			},
			{
				subdefinitiontext: data.string.p2subtext3,
				subdefinitionclass: 'subdefinition subtext3',
			},
			{
				subdefinitiontext: data.string.p2subtext4,
				subdefinitionclass: 'subdefinition subtext4',
			},
		],

		imageblock: [
			{
				imagetoshow: [
					{
						imgsrc: imgpath + 'page2/protractor02.png',
						imgclass: 'protractor'
					},
					{
						imgsrc: imgpath + "arrowicon.png",
						imgclass: 'arrowicon arrowicon1'
					},
					{
						imgsrc: imgpath + "arrowicon.png",
						imgclass: 'arrowicon arrowicon2'
					},
					{
						imgsrc: imgpath + "arrowicon.png",
						imgclass: 'arrowicon arrowicon3'
					},
					{
						imgsrc: imgpath + "arrowicon.png",
						imgclass: 'arrowicon arrowicon4'
					}
				]
			}
		],
	},
];


$(function() {
    $(window).resize(function() {
        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

    });

    var $board = $(".board");
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var total_page = 0;

    var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
    // recalculateHeightWidth();

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var preload;
	var timeoutvar = null;
	var current_sound;

  	function init() {
  		//specify type otherwise it will load assests as XHR
  		manifest = [
  			//images
  			// {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

  			// soundsicon-orange
        {id: "s2_p1", src: soundAsset+"s2_p1.ogg"},
        {id: "s2_p2", src: soundAsset+"s2_p2.ogg"},
        {id: "s2_p3", src: soundAsset+"s2_p3.ogg"},
        {id: "s2_p4", src: soundAsset+"s2_p4.ogg"},
        {id: "s2_p5", src: soundAsset+"s2_p5.ogg"},
        {id: "s2_p6", src: soundAsset+"s2_p6.ogg"},
        {id: "s2_p7", src: soundAsset+"s2_p7.ogg"},
        {id: "s2_p8", src: soundAsset+"s2_p8.ogg"},
        {id: "s2_p9", src: soundAsset+"s2_p9.ogg"},
        {id: "s2_p10", src: soundAsset+"s2_p10.ogg"},
        {id: "s2_p11", src: soundAsset+"s2_p11.ogg"},
        {id: "s2_p12", src: soundAsset+"s2_p12.ogg"},
        {id: "s2_p13", src: soundAsset+"s2_p13.ogg"},
        {id: "s2_p13_1", src: soundAsset+"s2_p13_1.ogg"},
        {id: "s2_p14_1", src: soundAsset+"s2_p14_1.ogg"},
        {id: "s2_p14_2", src: soundAsset+"s2_p14_2.ogg"},
        {id: "s2_p14_3", src: soundAsset+"s2_p14_3.ogg"},
        {id: "s2_p14_4", src: soundAsset+"s2_p14_4.ogg"}
  		];
  		preload = new createjs.LoadQueue(false);
  		preload.installPlugin(createjs.Sound);//for registering sounds
  		preload.on("progress", handleProgress);
  		preload.on("complete", handleComplete);
  		preload.on("fileload", handleFileLoad);
  		preload.loadManifest(manifest, true);
  	}
  	function handleFileLoad(event) {
  		// console.log(event.item);
  	}
  	function handleProgress(event) {
  		$('#loading-text').html(parseInt(event.loaded*100)+'%');
  	}
  	function handleComplete(event) {
  		$('#loading-wrapper').hide(0);
  		//initialize varibales
  		current_sound = createjs.Sound.play('sound_1');
  		current_sound.stop();
  		// call main function
  		templateCaller();
  	}
  	//initialize
  	init();

    /*
    	inorder to use the handlebar partials we need to register them
    	to their respective handlebar partial pointer first
    */
    Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
    Handlebars.registerPartial("subdefinitioncontent", $("#subdefinitioncontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

    // Handlebars.registerPartial("tablecontent", $("#tablecontent-partial").html());


    // controls the navigational state of the program
    function navigationController() {
        if(countNext == 0 && total_page != 1){
					$nextBtn.show(0);
				}else if(countNext > 0 && countNext < (total_page-1)){
					$prevBtn.show(0);
					$nextBtn.show(0);
				}else if(countNext == total_page-1){
					$prevBtn.show(0);
				}
    }
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

    function generalTemplate() {
      var source = $("#general-template").html();
      var template = Handlebars.compile(source);
      var html = template(content[countNext]);
      var requestAnimationFrame = window.requestAnimationFrame ;

      loadTimelineProgress($total_page,countNext+1);
      $board.html(html);

      // highlight any text inside board div with datahighlightflag set true
      texthighlight($board);
	  vocabcontroller.findwords(countNext);

			var canvasBlock = $('.canvasblock');
			var canvas = $('#canvas-first');
      var angle;

        $prevBtn.hide(0);
        $nextBtn.hide(0);
      // for last slide, canvas is not loaded so
      // validate canvas[0] object
      if (canvas[0]) {
        canvas[0].height = canvasBlock.height();
        canvas[0].width = canvasBlock.width();
        var ctx = canvas[0].getContext('2d');


				// For line
	      var lineStart = {
	        x: canvas.width()*0.112,
	        y: canvas.height()*0.65
	      };
	      var lineEnd = {
	        x: canvas.width()*0.607,
	        y: canvas.height()*0.65
	      };

				function line(color) {
	    		ctx.beginPath();
	    		ctx.moveTo(lineStart.x, lineStart.y);
	    		ctx.lineTo(lineEnd.x, lineEnd.y);
	    		ctx.lineWidth= 4;
	    		ctx.strokeStyle= color;
	    		ctx.stroke();
				}

        // For point
        var ang = 38;
        var ang2 = 37;
        var radius = 6;
        var division = 10;
        var pointStart = {
          x: canvas.width()*0.36,
          y: canvas.height()*0.655
        };

        function drawArc(ang, radius, division, fillColor) {
    	  angle = ang;

          ctx.beginPath();
          ctx.arc(pointStart.x, pointStart.y, radius, 0, -angle * Math.PI / division, true);
 					ctx.strokeStyle="#111111";
          ctx.stroke();
          ctx.fillStyle = fillColor;
	   		 ctx.fill();
        }
      }

    switch (countNext) {
      case 0:
					createjs.Sound.stop();
      		current_sound = createjs.Sound.play("s2_p"+(countNext+1));
      		current_sound.play();
          current_sound.on('complete', function(){
          			nav_button_controls(100);
          });
				break;

    	case 1:
        // sound_player_nav("s2_p"+(countNext+1));
      		createjs.Sound.stop();
      		current_sound = createjs.Sound.play("s2_p"+(countNext+1));
      		current_sound.play();
          current_sound.on('complete', function(){
            $(".protractor").addClass("hghlghtPrtctr");
						nav_button_controls(100);
          });
				$('.definition1').fadeIn(500);
	  		$('.definition').show(0);
			break;

			case 2:
        sound_player_nav("s2_p"+(countNext+1));
				$('.definition2').fadeIn(500);
				$('.definition').show(0);
	   	break;

			case 3:
        sound_player_nav("s2_p"+(countNext+1));
				$('.definition3').fadeIn(500);
				$('.definition').show(0);
	  	break;

	  	case 4:
        sound_player_nav("s2_p"+(countNext+1));
				$('.definition4').fadeIn(500);
				$('.definition').show(0);
	  	break;

      case 5:
        sound_player_nav("s2_p"+(countNext+1));
       	$('.definition5').fadeIn(500);
    		$('.definition').show(0);

    		$('.image-item img').css({
    			width: '50%'
    		});

    		// First Image
    		$('.image-item:first-child').css({
    			top: "20%",
    		});
    		$('.protractor1').css({
    			position: "relative",
    			left: "-22%",
    		});

    		// Second Image
    		$('.image-item:nth-child(2)').css({
    			top: "37%",
    		});
    		$('.protractor2').css({
    			position: "relative",
    			right: "-22%",
    		});

    		// Third Image
    		$('.image-item:nth-child(3)').css({
    			top: "55%",
    		});
    		$('.protractor3').css({
    			position: "relative",
    			left: "-20%",
    		});

    		// Fourth Image
    		$('.image-item:last-child').css({
    			top: "75%",
    		});
    		$('.protractor4').css({
    			position: "relative",
    			right: "-20%",
    		});
      break;

      case 6:
        sound_player_nav("s2_p"+(countNext+1));
      	$('.definition6').fadeIn(500);
	  		$('.definition').show(0);
      break;

      case 7:
        sound_player_nav("s2_p"+(countNext+1));
      	$('.definition7').fadeIn(500);
	  		$('.definition').show(0);
      break;

      case 8:
        sound_player("s2_p"+(countNext+1));
      $nextBtn.hide(0);
      	$('.definition8').fadeIn(500);
  			$('.definition').show(0);

  	    $(".canvasblock").css({
					position: "absolute",
					height: "52%",
					width: "50%",
					left: "47%"
    		});

    		// Handicon
    		$('.image-item:nth-child(2)').css({
    			top: "67%",
    		});
    		$('.handicon').css({
    			position: "relative",
    			right: "-0.9%",
    			cursor: "pointer",
    			zIndex: "9999",
    		});

    		// Arrowicon
    		$('.image-item:nth-child(3)').css({
    			top: "72%",
    		});
    		$('.arrowicon1').css({
    			transform: "rotate(-49deg)",
    		});

    		$('.handicon').click(function() {
      			$nextBtn.show(0);
					$('.handicon').hide(0);
	  			$('.definition7').hide(0);

	  			// point blink
	    		drawArc(ang, radius, division, "#FF9A4A");
	    		setTimeout(function(){
					ctx.clearRect(0, 0, canvas.width(), canvas.height());
	        		drawArc(ang, radius, division, "#3C78D8");
	    		}, 1000);
	    		setTimeout(function(){
					ctx.clearRect(0, 0, canvas.width(), canvas.height());
	      		drawArc(ang, radius, division, "#FF9A4A");
	    		}, 2000);

	    		$('.arrowicon1').fadeIn(1500);
	    		$('.subtext1').fadeIn(2000);
				});
      break;

      case 9:
        sound_player_nav("s2_p"+(countNext+1));
      	$('.definition9').fadeIn(500);
	  		$('.definition').show(0);
  			$(".canvasblock").css({
  				position: "absolute",
					height: "52%",
					width: "50%",
					left: "47%"
  			});

  			drawArc(ang, radius, division, "#3C78D8");
      break;

      case 10:
        sound_player("s2_p"+(countNext+1));
      $nextBtn.hide(0);
      	$('.definition10').fadeIn(500);
  			$('.definition').show(0);


  			$(".canvasblock").css({
	  			position: "absolute",
					height: "52%",
					width: "50%",
					left: "47%"
  			});

        		// Subtext
    		$(".subtext1").css({
		    	top: "0",
					position: "relative",
					display: "block",
					left: "0%",
    		});

    		// Handicon
    		$('.image-item:nth-child(2)').css({
    			top: "67%",
    		});

    		$('.handicon').css({
    			position: "relative",
    			left : "-27%",
    			cursor: "pointer",
    			zIndex: "9999",
    		});

    		// Arrowicon
    		$('.image-item:nth-child(3), .image-item:nth-child(4)').css({
    			top: "72%",
    		});

    		$('.arrowicon1').css({
    			transform: "rotate(-49deg)",
    			display: "block",
    			position: "relative",
    			left: "42%",
    		});

    		$('.arrowicon2').css({
    			transform: "rotate(-49deg)",
					position: "relative",
					left: "-30%",
    		});


  			$('.handicon').click(function(){
      			$nextBtn.show(0);
				$('.handicon').hide(0);
	  			$('.definition9').hide(0);

	  			$('.arrowicon2').fadeIn(1500);
	  			$(".subtext2").fadeIn(2000);

	  			// Subtext
	    		$(".subtext1").css({
		    	top: "0%",
			    position: "absolute",
			    display: "block",
			    left: "43.8%",
	    		});

	    		// Line
	  			line("#FF9A4A");

	  			setTimeout(function(){
	  				line("#3C78D8");
							drawArc(ang2, radius, division, "#3C78D8");
					},1000);

					setTimeout(function(){
						line("#FF9A4A");
						drawArc(ang2, radius, division, "#3C78D8");
					},2000);

					drawArc(ang2, radius, division, "#3C78D8");
				});

  			// Point in last to make it visible in highet index
  			drawArc(ang2, radius, division, "#3C78D8");
      break;

      case 11:
        sound_player_nav("s2_p"+(countNext+1));
      	$('.definition11').fadeIn(500);
    		$('.definition').show(0);
    		$(".canvasblock").css({
    			position: "absolute",
					height: "52%",
					width: "50%",
					left: "47%"
    		});

    		// Arrowicon
    		$('.image-item:nth-child(2), .image-item:nth-child(3)').css({
    			top: "72%",
    		});
    		$('.arrowicon1').css({
    			transform: "rotate(-49deg)",
    			position: "relative",
    			left: "0",
    		});

    		$('.arrowicon2').css({
    			transform: "rotate(-49deg)",
					position: "relative",
					left: "-30%",
    		});

    		$('.arrowicon3').css({
    			transform: "rotate(-180deg)",
					position: "relative",
					right: "-37%",
    		});


    		// ?subtext

    		$(".subdefinitionblock").css({
      		position: "absolute",
					top: "25%",
					right: "3%",
					width: "50%",
					height: "65%",
    		});

    		$(".subtext1").css({
			    position: "absolute",
			    left: "43.8%",
		    	top: "88%",
    		});

    		$(".subtext2").css({
		    position: "absolute",
		    left: "13%",
	    	top: "88%",
    		});

    		$(".subtext3").css({
		    position: "absolute",
		    right: "0%",
	    	top: "4%",
	    	display: "none"
    		});

    		line("#3C78D8");
    		drawArc(ang2, radius, division, "#3C78D8");
				$(".subtext1").show(0);
				$(".subtext2").show(0);
				$(".arrowicon1").show(0);
				$(".arrowicon2").show(0);
				setTimeout(function() {
  				$(".arrowicon3").fadeIn(1500);
  				$(".subtext3").fadeIn(2000);
				}, 1000);
      break;

      case 12:
        sound_player_nav("s2_p"+(countNext+1));
      		createjs.Sound.stop();
      		current_sound = createjs.Sound.play("s2_p13");
      		current_sound.play();
      		current_sound.on("complete", function(){
            sound_player_nav("s2_p"+(countNext+1));
          		createjs.Sound.stop();
          		current_sound = createjs.Sound.play("s2_p13_1");
          		current_sound.play();
          		current_sound.on("complete", function(){
          			nav_button_controls();
          		});
      		});
      	$('.definition12').fadeIn(500);
    		$('.definition').show(0);
	  		$(".canvasblock").css({
   	 			position: "absolute",
					height: "52%",
					width: "50%",
					left: "47%"
				});

    		// Arrowicon
    		$('.image-item:nth-child(2), .image-item:nth-child(3)').css({
    			top: "72%",
    		});

    		$('.image-item:nth-child(5)').css({
    			top: "26%",
    		});
    		$('.arrowicon1').css({
    			transform: "rotate(-49deg)",
    			position: "relative",
    			left: "0",
    		});

    		$('.arrowicon2').css({
    			transform: "rotate(-49deg)",
					position: "relative",
					left: "-30%",
  			});

  			$('.arrowicon3').css({
  				transform: "rotate(-180deg)",
					position: "relative",
					right: "-37%",
    		});

  			$('.arrowicon4').css({
  				transform: "rotate(80deg)",
					position: "relative",
					left: "-13%",
					top: "-10%"
  			});

    		// Subtext

    		$(".subdefinitionblock").css({
      		position: "absolute",
					top: "25%",
					right: "3%",
					width: "50%",
					height: "65%",
    		});

    		$(".subtext1").css({
			    position: "absolute",
			    left: "43.8%",
		    	top: "88%",
    		});

    		$(".subtext2").css({
			    position: "absolute",
			    left: "13%",
		    	top: "88%",
    		});

    		$(".subtext3").css({
			    position: "absolute",
			    right: "0%",
		    	top: "4%",
		    	display: "none"
    		});

    		$(".subtext4").css({
			    position: "absolute",
			    left: "0%",
		    	top: "-2%",
		    	display: "none"
    		});

    		line("#3C78D8");
    		drawArc(ang2, radius, division, "#3C78D8");
				$(".subtext1").show(0);
				$(".subtext2").show(0);
				$(".subtext3").show(0);
				$(".arrowicon1").show(0);
				$(".arrowicon2").show(0);
				$(".arrowicon3").show(0);
				setTimeout(function() {
	   			$(".subtext4").fadeIn(2000);
	  			$(".arrowicon4").fadeIn(1500);
    		}, 1000);

    		// setTimeout(function() {
        //   $nextBtn.show(0);
    		// }, 3500);
      break;
      case 13:
        // sound_player_nav("s2_p"+(countNext+1));
	  		$(".canvasblock").css({
   	 			position: "absolute",
					height: "52%",
					width: "50%",
					left: "47%"
				});

    		// Arrowicon
    		$('.image-item:nth-child(2), .image-item:nth-child(3)').css({
    			top: "72%",
    		});

    		$('.image-item:nth-child(5)').css({
    			top: "26%",
    		});
    		$('.arrowicon1').css({
    			transform: "rotate(-49deg)",
    			position: "relative",
    			left: "0",
    		});

    		$('.arrowicon2').css({
    			transform: "rotate(-49deg)",
					position: "relative",
					left: "-30%",
  			});

  			$('.arrowicon3').css({
  				transform: "rotate(-180deg)",
					position: "relative",
					right: "-37%",
    		});

  			$('.arrowicon4').css({
  				transform: "rotate(80deg)",
					position: "relative",
					left: "-13%",
					top: "-10%"
  			});

    		// Subtext

    		$(".subdefinitionblock").css({
      		position: "absolute",
					top: "25%",
					right: "3%",
					width: "50%",
					height: "65%",
    		});

    		$(".subtext1").css({
			    position: "absolute",
			    left: "43.8%",
		    	top: "88%",
    		});

    		$(".subtext2").css({
			    position: "absolute",
			    left: "13%",
		    	top: "88%",
    		});

    		$(".subtext3").css({
			    position: "absolute",
			    right: "0%",
		    	top: "4%",
		    	display: "none"
    		});

    		$(".subtext4").css({
			    position: "absolute",
			    left: "0%",
		    	top: "-2%",
		    	display: "none"
    		});

    		line("#3C78D8");
    		drawArc(ang2, radius, division, "#3C78D8");
        $(".subdefinition, .arrowicon").hide(0);
        $(".orange_text").parent().hide(0);
				$(".subtext1").show(0);//center
				$(".subtext2").show(0);//baseline
				$(".subtext3").show(0);//degree marks
				$(".arrowicon1").show(0);//center
				$(".arrowicon2").show(0);//baseline
				$(".arrowicon3").show(0);//degree marks
   			$(".subtext4").fadeIn(0);//degree measure
  			$(".arrowicon4").fadeIn(0);//degree measure

          // sound_player_nav("s2_p"+(countNext+1));
          var p_count = 0;


          function show_points(p_count){
            $(".orange_text:eq("+p_count+")").parent().fadeIn(0);
            createjs.Sound.stop();
            current_sound = createjs.Sound.play("s2_p14_"+(p_count+1));
            current_sound.play();
            current_sound.on("complete", function(){
              p_count += 1;
              if(p_count <=3){
                show_points(p_count);
              }else{
    	        	ole.footerNotificationHandler.pageEndSetNotification();
              }
            });
          }

        show_points(0);
      break;
    }
  };


	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		// current_sound.on('complete', function(){
		// 	navigationcontroller();
		// });
	}

	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls();
		});
	}
    function templateCaller() {
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

        navigationController();
		loadTimelineProgress($total_page, countNext + 1);

        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


    }

    $nextBtn.on("click", function() {
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
        countNext--;
        templateCaller();
    });

    // document.addEventListener('xmlLoad', function(e) {
        total_page = content.length;
        templateCaller();
    // });


});



/*===============================================
	 =            data highlight function            =
	 ===============================================*/
function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";


    if ($alltextpara.length > 0) {
        $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                (stylerulename = $(this).attr("data-highlightcustomclass")) :
                (stylerulename = "parsedstring");

            texthighlightstarttag = "<span>";


            replaceinstring = $(this).html();
            replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
            replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);


            $(this).html(replaceinstring);
        });
    }
}
/*=====  End of data highlight function  ======*/

var imgpath = $ref+"/exercise/images/newImg/";
var content=[
	//exercise 1
		{
			contentblockadditionalclass:"creambg",
      extratextblock:[{
        textdata: data.string.p3text5_1,
        textclass: "mcqQn",
      }],
			imageblock:[{
				imagestoshow:[{
					imgclass: "QnImg",
					imgid : 'q01',
					imgsrc: ""
				}]
			}],
      mcqblock:[{
        mcqcontainerclass:"mcqContainer",
        mcqoption:[{
          optiondata:data.string.p3deg1,
          optionaddnalclass:"class2",
        },{
          optiondata:data.string.p3deg4,
          optionaddnalclass:"class1",
        },{
          optiondata:data.string.p3deg7,
          optionaddnalclass:"class3",
        }]
      }]
		},
	//exercise 2
		{
			contentblockadditionalclass:"creambg",
      extratextblock:[{
        textdata: data.string.p3text5_1,
        textclass: "mcqQn",
      }],
			imageblock:[{
				imagestoshow:[{
					imgclass: "QnImg",
					imgid : 'q02',
					imgsrc: ""
				}]
			}],
      mcqblock:[{
        mcqcontainerclass:"mcqContainer",
        mcqoption:[{
          optiondata:data.string.p3deg4,
          optionaddnalclass:"class2",
        },{
          optiondata:data.string.p3deg1,
          optionaddnalclass:"class1",
        },{
          optiondata:data.string.p3deg7,
          optionaddnalclass:"class3",
        }]
      }]
		},
	//exercise 3
		{
			contentblockadditionalclass:"creambg",
      extratextblock:[{
        textdata: data.string.p3text5_1,
        textclass: "mcqQn",
      }],
			imageblock:[{
				imagestoshow:[{
					imgclass: "QnImg",
					imgid : 'q03',
					imgsrc: ""
				}]
			}],
      mcqblock:[{
        mcqcontainerclass:"mcqContainer",
        mcqoption:[{
          optiondata:data.string.q3_2,
          optionaddnalclass:"class2",
        },{
          optiondata:data.string.q3_1,
          optionaddnalclass:"class1",
        },{
          optiondata:data.string.q3_3,
          optionaddnalclass:"class3",
        }]
      }]
		},
	//exercise 4--->same img as 4
		{
			contentblockadditionalclass:"creambg",
      extratextblock:[{
        textdata: data.string.p3text5_1,
        textclass: "mcqQn",
      }],
			imageblock:[{
				imagestoshow:[{
					imgclass: "QnImg",
					imgid : 'q13',
					imgsrc: ""
				}]
			}],
      mcqblock:[{
        mcqcontainerclass:"mcqContainer",
        mcqoption:[{
          optiondata:data.string.q4_2,
          optionaddnalclass:"class2",
        },{
          optiondata:data.string.q4_1,
          optionaddnalclass:"class1",
        },{
          optiondata:data.string.q4_3,
          optionaddnalclass:"class3",
        }]
      }]
		},
	//exercise 5
		{
			contentblockadditionalclass:"creambg",
      extratextblock:[{
        textdata: data.string.p3text5_1,
        textclass: "mcqQn",
      }],
			imageblock:[{
				imagestoshow:[{
					imgclass: "QnImg",
					imgid : 'q05',
					imgsrc: ""
				}]
			}],
      mcqblock:[{
        mcqcontainerclass:"mcqContainer",
        mcqoption:[{
          optiondata:data.string.q5_1,
          optionaddnalclass:"class2",
        },{
          optiondata:data.string.q5_2,
          optionaddnalclass:"class1",
        },{
          optiondata:data.string.q5_3,
          optionaddnalclass:"class3",
        }]
      }]
		},
	//exercise 6
		{
			contentblockadditionalclass:"creambg",
      extratextblock:[{
        textdata: data.string.p3text5_1,
        textclass: "mcqQn",
      }],
			imageblock:[{
				imagestoshow:[{
					imgclass: "QnImg",
					imgid : 'q06',
					imgsrc: ""
				}]
			}],
      mcqblock:[{
        mcqcontainerclass:"mcqContainer",
        mcqoption:[{
          optiondata:data.string.q6_1,
          optionaddnalclass:"class2",
        },{
          optiondata:data.string.q6_2,
          optionaddnalclass:"class1",
        },{
          optiondata:data.string.q6_3,
          optionaddnalclass:"class3",
        }]
      }]
		},
	//exercise 7
		{
			contentblockadditionalclass:"creambg",
      extratextblock:[{
        textdata: data.string.p3text5_1,
        textclass: "mcqQn",
      }],
			imageblock:[{
				imagestoshow:[{
					imgclass: "QnImg",
					imgid : 'q07',
					imgsrc: ""
				}]
			}],
      mcqblock:[{
        mcqcontainerclass:"mcqContainer",
        mcqoption:[{
          optiondata:data.string.q7_2,
          optionaddnalclass:"class2",
        },{
          optiondata:data.string.q7_1,
          optionaddnalclass:"class1",
        },{
          optiondata:data.string.q3_1,
          optionaddnalclass:"class3",
        }]
      }]
		},
	//exercise 8
		{
			contentblockadditionalclass:"creambg",
      extratextblock:[{
        textdata: data.string.p3text5_1,
        textclass: "mcqQn",
      }],
			imageblock:[{
				imagestoshow:[{
					imgclass: "QnImg",
					imgid : 'q08',
					imgsrc: ""
				}]
			}],
      mcqblock:[{
        mcqcontainerclass:"mcqContainer",
        mcqoption:[{
          optiondata:data.string.q7_1,
          optionaddnalclass:"class2",
        },{
          optiondata:data.string.q8_2,
          optionaddnalclass:"class1",
        },{
          optiondata:data.string.q3_1,
          optionaddnalclass:"class3",
        }]
      }]
		},
	//exercise 9
		{
			contentblockadditionalclass:"creambg",
      extratextblock:[{
        textdata: data.string.p3text5_1,
        textclass: "mcqQn",
      }],
			imageblock:[{
				imagestoshow:[{
					imgclass: "QnImg",
					imgid : 'q09',
					imgsrc: ""
				}]
			}],
      mcqblock:[{
        mcqcontainerclass:"mcqContainer",
        mcqoption:[{
          optiondata:data.string.q9_2,
          optionaddnalclass:"class2",
        },{
          optiondata:data.string.q9_1,
          optionaddnalclass:"class1",
        },{
          optiondata:data.string.q7_2,
          optionaddnalclass:"class3",
        }]
      }]
		},
	//exercise10
		{
			contentblockadditionalclass:"creambg",
      extratextblock:[{
        textdata: data.string.p3text5_1,
        textclass: "mcqQn",
      }],
			imageblock:[{
				imagestoshow:[{
					imgclass: "QnImg",
					imgid : 'q15',
					imgsrc: ""
				}]
			}],
      mcqblock:[{
        mcqcontainerclass:"mcqContainer",
        mcqoption:[{
          optiondata:data.string.q10_1,
          optionaddnalclass:"class2",
        },{
          optiondata:data.string.q10_3,
          optionaddnalclass:"class1",
        },{
          optiondata:data.string.q10_2,
          optionaddnalclass:"class3",
        }]
      }]
		},
];
	content.shufflearray();
$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;
	function init() {

			//specify type otherwise it will load assests as XHR
			manifest = [
				//images
				{id: "q01", src: imgpath+"q01.png", type: createjs.AbstractLoader.IMAGE},
				{id: "q02", src: imgpath+"q02.png", type: createjs.AbstractLoader.IMAGE},
				{id: "q03", src: imgpath+"q03.png", type: createjs.AbstractLoader.IMAGE},
				{id: "q13", src: imgpath+"q13.png", type: createjs.AbstractLoader.IMAGE},
				{id: "q05", src: imgpath+"q05.png", type: createjs.AbstractLoader.IMAGE},
				{id: "q06", src: imgpath+"q06.png", type: createjs.AbstractLoader.IMAGE},
				{id: "q07", src: imgpath+"q07.png", type: createjs.AbstractLoader.IMAGE},
				{id: "q08", src: imgpath+"q08.png", type: createjs.AbstractLoader.IMAGE},
				{id: "q09", src: imgpath+"q09.png", type: createjs.AbstractLoader.IMAGE},
				{id: "q15", src: imgpath+"q15.png", type: createjs.AbstractLoader.IMAGE},
				// sounds
			];

			preload = new createjs.LoadQueue(false);
			preload.installPlugin(createjs.Sound);//for registering sounds
			preload.on("progress", handleProgress);
			preload.on("complete", handleComplete);
			preload.on("fileload", handleFileLoad);
			preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		//scoring.init(10);
		templateCaller();
	}
	//initialize
	init();

	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provideimgpathd
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
 	}

	 /*values in this array is same as the name of images of eggs in image folder*/

	 //create eggs
	 var testin = new NumberTemplate();

	 	//eggTemplate.eggMove(countNext);
 		testin.init($total_page);

	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);
		put_image(content, countNext);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);

	 	/*generate question no at the beginning of question*/
	 	// testin.numberOfQuestions();

	 	/*for randomizing the options*/
		function randomize(parent){
			// alert(parent);
			var parent = $(parent);
			var divs = parent.children();
			while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}
		}
		randomize(".optionsblock");

	 	/*======= SCOREBOARD SECTION ==============*/
	 	/*random scoreboard eggs*/
	 	//var i = Math.floor(Math.random() * imageArray.length);
	 	//var randImg = imageArray[i];
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	var wrongClick = false;
 	switch(countNext){
	// case 0:
	// 	//handle mcq
	// break;
	default:
		handleMcq();
 	}

	function handleMcq(){
		$(".optclass").click(function(){
			if($(this).hasClass('class1')){
				play_correct_incorrect_sound(1);
				$(this).siblings(".correct").show();
				$(this).addClass("corAns");
				$(".optclass").css("pointer-events","none");
				// nav_button_controls();
				!wrongClick?testin.update(true):testin.update(false);
				if(countNext != $total_page)
					$nextBtn.show(0);
			}else{
				play_correct_incorrect_sound(0);
				$(this).siblings(".incorrect").show();
				$(this).addClass("incorAns");
				wrongClick = true;
			}
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				console.log(imageClass.length)
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	 	/*======= SCOREBOARD SECTION ==============*/
	 }

	 function templateCaller(){
		/*always hide next and previousloadimage navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators


	}

	// first call to template caller
	// templateCaller(); loadimage($(".myimg3").find("img"),$(".myimg3").find("p"),preload.getResult('ex1typ3qn'+quesNo).src,eval('data.string.ex2typ1op'+quesNo));


	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
			countNext++;
			if(countNext < 11){
				testin.gotoNext();
				templateCaller();
			}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
 	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	// function loadimage(imgrep,imgtext,imgsrc,quesNo){
	// 	imgrep.attr("src",imgsrc);
	// 	imgtext.text(quesNo);
	// }

/*=====  End of Templates Controller Block  ======*/
});

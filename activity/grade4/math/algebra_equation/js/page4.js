var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-p4',

		uppertextblockadditionalclass: 'big-textblock',
		uppertextblock:[{
			textdata: data.string.p4text1,
			textclass: "",
		}
		],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy-inside-2',

		uppertextblockadditionalclass: 'top-text-block-2',
		uppertextblock:[{
			textdata: data.string.p3diy1t1,
			textclass: "top-equation-text-2",
		}],
		extratextblock:[{
			textdata: 'asd',
			textclass: "feedbacktext-2",
		}],
		equationblock2:[{
			equationblockclass: '',
			textdata1: data.string.p3diy1t2,
			textdata2: data.string.p3diy1t3,
			sliderdata: '',
			btndata: data.string.check
		}],
		lowertextblockadditionalclass: 'bottom-number-labels',
		lowertextblock: true,
		sliderblock: true
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy-inside-2',

		uppertextblockadditionalclass: 'top-text-block-2',
		uppertextblock:[{
			textdata: data.string.p3diy1t1,
			textclass: "top-equation-text-2",
		}],
		extratextblock:[{
			textdata: 'asd',
			textclass: "feedbacktext-2",
		}],
		equationblock2:[{
			equationblockclass: '',
			textdata1: data.string.p3diy1t4,
			textdata2: data.string.p3diy1t5,
			sliderdata: '',
			btndata: data.string.check
		}],
		lowertextblockadditionalclass: 'bottom-number-labels',
		lowertextblock: true,
		sliderblock: true
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-p4',

		uppertextblockadditionalclass: 'big-textblock',
		uppertextblock:[{
			textdata: data.string.p4text2,
			textclass: "",
		}
		],
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-p4',

		uppertextblockadditionalclass: 'big-textblock',
		uppertextblock:[{
			textdata: data.string.p4text3,
			textclass: "",
		}
		],
	},
	// slide5 multiply
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-p4-2',

		uppertextblockadditionalclass: 'top-instruction',
		uppertextblock:[{
			textdata: data.string.p4text4,
			textclass: "top-instruction-text",
			datahighlightflag : true,
			datahighlightcustomclass : 'color-black',
		}],
		extratextblock:[{
			textdata: data.string.check,
			textclass: "submit-button",
		},{
			textdata: data.string.tryagain,
			textclass: "tryagain-button",
		}],

		equationblock:[{
			equationblockclass: 'equation-multiply equation-ex-1',
			textclass1: 'eq-ques-lhs',
			textdata1: data.string.n6n,
			textdata3: data.string.n48,
		},{
			equationblockclass: 'equation-multiply equation-ex-2',
			textdata1: data.string.tn,
			optionclass3: 'input-option',
			inputclass3: 'default-input-p2',
		}],


		lowertextblockadditionalclass: 'feedback-soln',
		lowertextblock:[{
			textdata: data.string.p4text5,
			textclass: 'left-aligned soln-step-0'
		},{
			textdata: data.string.p4text6,
			textclass: 'total-soln-text soln-step-1',
			datahighlightflag : true,
			datahighlightcustomclass : 'soln-lhs',
		},{
			textdata: data.string.p4text7,
			textclass: 'total-soln-text soln-step-2',
			datahighlightflag : true,
			datahighlightcustomclass : 'text-hidden',
		},{
			textdata: data.string.p4text8,
			textclass: 'total-soln-text soln-step-3',
			datahighlightflag : true,
			datahighlightcustomclass : 'soln-lhs',
		},{
			textdata: data.string.p4text9,
			textclass: 'soln-conclusion-text soln-step-4',

		}],
	},
	// slide6 divide
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-p4-2',

		uppertextblockadditionalclass: 'top-instruction',
		uppertextblock:[{
			textdata: data.string.p4text4,
			textclass: "top-instruction-text",
			datahighlightflag : true,
			datahighlightcustomclass : 'color-black',
		}],
		extratextblock:[{
			textdata: data.string.check,
			textclass: "submit-button",
		},{
			textdata: data.string.tryagain,
			textclass: "tryagain-button",
		}],

		equationblock:[{
			equationblockclass: 'equation-divide equation-ex-1',
			textclass1: 'eq-ques-lhs',
			textdata1: data.string.d96n,
			splitintofractionsflag: true,
			textdata3: data.string.n12,
		},{
			equationblockclass: 'equation-multiply equation-ex-2',
			textdata1: data.string.tn,
			optionclass3: 'input-option',
			inputclass3: 'default-input-p2',
		}],


		lowertextblockadditionalclass: 'feedback-soln',
		lowertextblock:[{
			textdata: data.string.p4text5,
			textclass: 'left-aligned soln-step-0'
		},{
			textdata: data.string.p4text12,
			textclass: 'total-soln-text div-soln-step-1 soln-step-1',
			datahighlightflag : true,
			splitintofractionsflag: true,
			datahighlightcustomclass : 'soln-lhs',
		},{
			textdata: data.string.p4text7,
			textclass: 'total-soln-text soln-step-2',
			datahighlightflag : true,
			datahighlightcustomclass : 'text-hidden',
		},{
			textdata: data.string.p4text8,
			textclass: 'total-soln-text soln-step-3',
			datahighlightflag : true,
			datahighlightcustomclass : 'soln-lhs',
		},{
			textdata: data.string.p4text9,
			textclass: 'soln-conclusion-text soln-step-4',

		}],
	},

	// slide7 addition
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-p4-2',

		uppertextblockadditionalclass: 'top-instruction',
		uppertextblock:[{
			textdata: data.string.p4text4,
			textclass: "top-instruction-text",
			datahighlightflag : true,
			datahighlightcustomclass : 'color-black',
		}],
		extratextblock:[{
			textdata: data.string.check,
			textclass: "submit-button",
		},{
			textdata: data.string.tryagain,
			textclass: "tryagain-button",
		}],

		equationblock:[{
			equationblockclass: 'equation-ex-1',
			signsrc: imgpath + 'plus.svg',
			textclass1: 'eq-ques-lhs',
			textdata1: data.string.n4n,
			textdata2: data.string.n2,
			textdata3: data.string.n10,
		},{
			equationblockclass: 'equation-multiply equation-ex-2',
			textdata1: data.string.tn,
			optionclass3: 'input-option',
			inputclass3: 'default-input-p2',
		}],


		lowertextblockadditionalclass: 'feedback-soln',
		lowertextblock:[{
			textdata: data.string.p4text14,
			textclass: 'left-aligned soln-step-0'
		},{
			textdata: data.string.p4text15,
			textclass: 'left-aligned soln-step-0'
		},{
			textdata: data.string.p4text16,
			textclass: 'total-soln-text soln-step-1',
		},{
			textdata: data.string.p4text17,
			textclass: 'total-soln-text soln-step-2',
		},{
			textdata: data.string.p4text18,
			textclass: 'total-soln-text soln-step-3',
		},{
			textdata: data.string.p4text19,
			textclass: 'soln-conclusion-text soln-step-4',

		}],
	},


	// slide8 subtraction
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-p4-2',

		uppertextblockadditionalclass: 'top-instruction',
		uppertextblock:[{
			textdata: data.string.p4text4,
			textclass: "top-instruction-text",
			datahighlightflag : true,
			datahighlightcustomclass : 'color-black',
		}],
		extratextblock:[{
			textdata: data.string.check,
			textclass: "submit-button",
		},{
			textdata: data.string.tryagain,
			textclass: "tryagain-button",
		}],

		equationblock:[{
			equationblockclass: 'equation-ex-1',
			signsrc: imgpath + 'minus.svg',
			textclass1: 'eq-ques-lhs',
			textdata1: data.string.n9n,
			textdata2: data.string.n6,
			textdata3: data.string.n57,
		},{
			equationblockclass: 'equation-multiply equation-ex-2',
			textdata1: data.string.tn,
			optionclass3: 'input-option',
			inputclass3: 'default-input-p2',
		}],


		lowertextblockadditionalclass: 'feedback-soln',
		lowertextblock:[{
			textdata: data.string.p4text22,
			textclass: 'left-aligned soln-step-0'
		},{
			textdata: data.string.p4text15,
			textclass: 'left-aligned soln-step-0'
		},{
			textdata: data.string.p4text23,
			textclass: 'total-soln-text soln-step-1',
		},{
			textdata: data.string.p4text17,
			textclass: 'total-soln-text soln-step-2',
		},{
			textdata: data.string.p4text18,
			textclass: 'total-soln-text soln-step-3',
		},{
			textdata: data.string.p4text19,
			textclass: 'soln-conclusion-text soln-step-4',

		}],
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images

			{id: "fulcrum", src: imgpath+"fulcrum.png", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s4_p1", src: soundAsset+"s4_p1.ogg"},
			{id: "s4_p2", src: soundAsset+"s4_p2_inst.ogg"},
			{id: "s4_p4", src: soundAsset+"s4_p4.ogg"},
			{id: "s4_p5", src: soundAsset+"s4_p5.ogg"},
			{id: "s4_p6", src: soundAsset+"s4_p6_inst.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}


	function splitintofractions($splitinside) {
		typeof $splitinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;
		var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
		if ($splitintofractions.length > 0) {
			$.each($splitintofractions, function(index, value) {
				$this = $(this);
				var tobesplitfraction = $this.html();
				if ($this.hasClass('fraction')) {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
				} else {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
				}

				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
				$this.html(tobesplitfraction);
			});
		}
	}

	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		splitintofractions($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);

		$('.correct-icon').attr('src', preload.getResult('correct').src);
		$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);
		switch(countNext) {
			case 0:
				sound_nav("s4_p1");
			break;
			case 1:
			case 2:
				sound_player("s4_p"+(countNext+1));
				$prevBtn.show(0);
				var sign = '+';
				var maxVal = 20-initPos;
				if(countNext==2){
					sign ='-';
					$('.sign-img2').attr('src', imgpath + 'minus.svg');
					maxVal = initPos;
				}
				var hasClicked = false;
				var incrementValue = 4.75;
				var initValue = 0;
				var initPos = parseInt($('.eq-data-1').html());
				var finalPos = parseInt($('.eq-data-2').html());
				$('.slider-arrow').css({
					'left': (initPos)*incrementValue+'%'
				});
				$('.initial-slider').css({
					'width': (initPos)*incrementValue+'%'
				});
				$('.added-slider').animate({
					'left': (initPos)*incrementValue+'%',
				});
				function updateSlider(flag){
					var tempSign = 0;
					var sliderPos = initValue+initPos;
					if(sign == '-'){
						tempSign = initValue;
						sliderPos = initPos-initValue;
					}
					$('.slider-arrow').animate({
						'left': sliderPos*incrementValue+'%'
					}, 500, function(){
						$('.feedbacktext-2').show(0);
					});
					$('.added-slider').animate({
						'left': (initPos-tempSign)*incrementValue+'%',
						'width': (initValue)*incrementValue+'%'
					}, 500);
				}
				//adding bottom label
				for(var i=1; i<21; i++){
					var bottomTag = document.createElement("p");
					bottomTag.className = 'bottom-label-text blt-'+i;
					bottomTag.style.left = (i*incrementValue)+'%';
					var labelNumber = i;
					if($lang != 'en') labelNumber = ole.nepaliNumber(i,$lang);
					bottomTag.innerHTML = labelNumber;
					$('.bottom-number-labels').append(bottomTag);
				}
				$('.incrementer').click(function(){
					hasClicked = true;
					initValue++;
					if(initValue>maxVal) initValue = maxVal;
					$('.slider-data').html(initValue);
				});
				$('.decrementer').click(function(){
					hasClicked = true;
					initValue--;
					if(initValue<0) initValue = 0;
					$('.slider-data').html(initValue);
				});
				$('.eq-button').click(function(){
					createjs.Sound.stop();
					if(hasClicked){
						var answer = finalPos-initPos;
						if(sign=='-'){
							answer *= -1;
						}
						var lhsString = initPos + ' ' + sign + ' ' + initValue;
						var rhsString = eval(lhsString);
						var checkUserInput = lhsString+ ' = ' + rhsString;

						var resultText = ' ';
						if(rhsString<finalPos){
							resultText = ' <b>(too low)</b>';
						} else if(rhsString>finalPos){
							resultText = ' <b>(too high)</b>';
						}
						var feedBackText = checkUserInput + resultText;
						$('.feedbacktext-2').html(feedBackText);
						if(initValue == answer){
							play_correct_incorrect_sound(1);
							$('.added-slider').css('background-color', '#A2AE27');
							$('.input-slider-data').addClass('correct-answer');
							$nextBtn.show(0);
						} else{
							play_correct_incorrect_sound(0);
							$('.added-slider').css('background-color', '#FF0000');
							$('.input-slider-data').addClass('incorrect-answer');
						}
						updateSlider();
						$(this).addClass('try-again-button');
						$(this).html(data.string.tryagain);
					}
				});
				break;
			case 5:
			case 6:
				sound_player("s4_p"+(countNext+1));
				$prevBtn.show(0);
				var answer = 8;
				input_box('.default-input-p2', 2, null);

				$('.default-input-p2').keyup(function(event) {
					if($(this).val().length>0){
						$('.submit-button').addClass('active-submit');
					} else{
						$('.submit-button').removeClass('active-submit');
					}
				});
				$('.submit-button').click(function(){
					createjs.Sound.stop();
					var inputVal = parseInt($('.default-input-p2').val());
					if(countNext==5){
						fill_solution_multiply(answer, inputVal);
					} else if(countNext == 6){
						fill_solution_divide(answer, inputVal);
					}
					if(inputVal ==  answer){
						play_correct_incorrect_sound(1);
						$('.submit-button').removeClass('active-submit');
						$('.tryagain-button').hide(0);
						$('.default-input-p2').attr('disabled', 'true');
						$('.default-input-p2').addClass('correct-answer');
						$('.default-input-p2').parent().children('.correct-icon').show(0);
						nav_button_controls(5000);
					} else{
						play_correct_incorrect_sound(0);
						$('.tryagain-button').show(0);
						$('.default-input-p2').addClass('incorrect-answer');
						$('.default-input-p2').parent().children('.incorrect-icon').show(0);
						$('.default-input-p2').attr('disabled', 'true');
					}
					$('.submit-button').removeClass('active-submit');
				});
				$('.tryagain-button').click(function(){
					templateCaller();
				});
				function fill_solution_multiply(correctAnswer, userAnswer){
					if( userAnswer == correctAnswer){
						$('.soln-step-3').html(data.string.p4text10);
						$('.soln-conclusion-text').html(data.string.p4text11);
					}
					$('.user-input').html(userAnswer);
					var step1_str = $('.soln-step-1>.lhs-soln').html();
					var step1_var = step1_str.replace(/^[0-9]/g, '');
					var step1_strFinal = putData(step1_str, step1_var, userAnswer, 'x');
					var step2_strFinal = eval( putData(step1_str, step1_var, userAnswer, '*') );
					$('.soln-step-1>.rhs-soln').html('= ' + step1_strFinal);
					$('.soln-step-2>.rhs-soln').html('= ' + step2_strFinal);
					$('.soln-step-3>.lhs-soln').html(step2_strFinal);
					$('.user-ans').html(step2_strFinal);
					$('.feedback-soln').show(0);
					for(var i=0; i<$('.feedback-soln>*').length; i++){
						$('.feedback-soln>*').eq(i).addClass('soln-fade-in-'+i);
					}
				}

				function fill_solution_divide(correctAnswer, userAnswer){
					if( userAnswer == correctAnswer){
						$('.soln-step-3').html(data.string.p4text10);
						$('.soln-conclusion-text').html(data.string.p4text13);
					}
					$('.user-input').html(userAnswer);
					var numerator_lhs = $('.soln-step-1>.lhs-soln>.fraction2>.top');
					var denominator_lhs = $('.soln-step-1>.lhs-soln>.fraction2>.bottom');
					var numerator_rhs = $('.soln-step-1>.rhs-soln>.fraction2>.top');
					var denominator_rhs = $('.soln-step-1>.rhs-soln>.fraction2>.bottom');
					var step1_str = $('.soln-step-1>.lhs-soln').html();
					var step2_strFinal = Math.round((parseInt(numerator_lhs.html())/userAnswer)*100)/100;
					numerator_rhs.html(numerator_lhs.html());
					denominator_rhs.html(userAnswer);
					$('.actual-ans').html(parseInt(numerator_lhs.html())/correctAnswer);
					$('.soln-step-2>.rhs-soln').html('= ' + step2_strFinal);
					$('.soln-step-3>.lhs-soln').html(step2_strFinal);
					$('.user-ans').html(step2_strFinal);
					$('.feedback-soln').show(0);
					for(var i=0; i<$('.feedback-soln>*').length; i++){
						$('.feedback-soln>*').eq(i).addClass('soln-fade-in-'+i);
					}
				}
				break;
			case 7:
			case 8:
				$prevBtn.show(0);
				var answer = 2;
				var rightsideVal = 10;
				input_box('.default-input-p2', 2, null);
				if( countNext == 8){
					answer = 7;
					rightsideVal = 57;
				}

				$('.default-input-p2').keyup(function(event) {
					if($(this).val().length>0){
						$('.submit-button').addClass('active-submit');
					} else{
						$('.submit-button').removeClass('active-submit');
					}
				});
				$('.submit-button').click(function(){
					var inputVal = parseInt($('.default-input-p2').val());
					fill_solution(answer, inputVal, rightsideVal);
					if(inputVal ==  answer){
						play_correct_incorrect_sound(1);
						$('.submit-button').removeClass('active-submit');
						$('.tryagain-button').hide(0);
						$('.default-input-p2').attr('disabled', 'true');
						$('.default-input-p2').addClass('correct-answer');
						$('.default-input-p2').parent().children('.correct-icon').show(0);
						nav_button_controls(6000);
					} else{
						play_correct_incorrect_sound(0);
						$('.tryagain-button').show(0);
						$('.default-input-p2').addClass('incorrect-answer');
						$('.default-input-p2').parent().children('.incorrect-icon').show(0);
						$('.default-input-p2').attr('disabled', 'true');
					}
					$('.submit-button').removeClass('active-submit');
				});
				$('.tryagain-button').click(function(){
					templateCaller();
				});
				function fill_solution(correctAnswer, userAnswer, rhsValue){
					if( userAnswer == correctAnswer){
						$('.soln-step-3').html(data.string.p4text20);
						$('.soln-conclusion-text').html(data.string.p4text21);
					}
					$('.user-input').html(userAnswer);
					var step1_str = $('.soln-step-1>.lhs-soln2').html();
					var step1_var = step1_str.replace(/[^A-Za-z]/g, '');
					console.log(step1_var);
					var step1_strFinal = putData(step1_str, step1_var, userAnswer, 'x');
					var step2_strFinal = eval( putData(step1_str, step1_var, userAnswer, '*') );
					$('.actual-ans').html(rhsValue);
					$('.soln-step-1>.rhs-soln2').html('= ' + rhsValue);
					$('.soln-step-2>.lhs-soln2').html(step1_strFinal);
					$('.soln-step-2>.rhs-soln2').html('= ' + rhsValue);
					$('.soln-step-3>.lhs-soln2').html(step2_strFinal);
					$('.user-ans').html(step2_strFinal);
					$('.feedback-soln').show(0);
					for(var i=0; i<$('.feedback-soln>*').length; i++){
						$('.feedback-soln>*').eq(i).addClass('soln-fade-in-'+i);
					}
				}
				break;
			default:
				sound_nav("s4_p"+(countNext+1));
				break;
		}
	}

	function putData(stringdata, pattern, data, sign){
		reg = new RegExp(pattern, 'gi');
		return stringdata.replace(reg, function(str) {return sign + data;});
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
				$prevBtn.hide(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event) {
			var charCode = (event.which) ? event.which : event.keyCode;
			/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
			if (charCode === 13 && button_class != null) {
				$(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
			if (!condition) {
				return true;
			}
			//check if user inputs more than one '.'
			if ((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
				return false;
			}
			//check . and 0-9 separately after checking arrow and other keys
			if ((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110) {
				return false;
			}
			//check max no of allowed digits
			if (String(event.target.value).length >= max_number) {
				return false;
			}
			return true;
		});
	}
});

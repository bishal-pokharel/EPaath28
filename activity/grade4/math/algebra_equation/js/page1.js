var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',

		extratextblock:[{
			textdata: data.string.name,
			textclass: "lesson-title",
		},
		{
			textdata: data.string.gradeno,
			textclass: "lesson-title2",
		}],
		imageblock:[{
			imagestoshow : [{
				imgclass : "fullimage",
				imgid : 'cover_page'
		}]
	}],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-p1',

		uppertextblockadditionalclass: 'header-p1',
		uppertextblock:[{
			textdata: data.string.p1text1,
			textclass: "header-p1-text",
		}],
		equationblock:[{
			equationblockclass: 'equation-top',
			textclass1: 'data-lhs',
			textdata1: data.string.n2,
			textclass2: 'data-lhs',
			textdata2: data.string.n3,
			textclass3: 'data-rhs',
			textdata3: data.string.n5,
		},{
			equationblockclass: 'equation-bottom',
			imgid1: 'apple-2',
			imgclass1: 'apple-lhs apple-1',
			imgid2: 'apple-3',
			imgclass2: 'apple-lhs  apple-2',
			imgid3: 'apple-5',
			imgclass3: 'apple-rhs  apple-3',
		}],
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-p1',

		uppertextblockadditionalclass: 'header-p1',
		uppertextblock:[{
			textdata: data.string.p1text2,
			textclass: "header-p1-text",
		}],
		extratextblock:[{
			textdata: data.string.p1text3,
			textclass: "middle-p1-text",
		}],
		equationblock:[{
			equationblockclass: 'equation-top',
			textclass1: 'data-lhs',
			textdata1: data.string.n4,
			textclass2: 'data-lhs blank-data',
			textdata2: data.string.qmark,
			textclass3: 'data-rhs',
			textdata3: data.string.n9,
		}],
		optioncontainer: 'bottom-option-container',
		optionblock:[{
			optionclass: 'bottom-option opt-1',
			textdivclass: 'bttexxt correct-ans',
			textdata: data.string.n5,
		},{
			optionclass: 'bottom-option opt-2',
			textdivclass: 'bttexxt',
			textdata: data.string.n2,
		},{
			optionclass: 'bottom-option opt-3',
			textdivclass: 'bttexxt',
			textdata: data.string.n3,
		},{
			optionclass: 'bottom-option opt-4',
			textdivclass: 'bttexxt',
			textdata: data.string.n7,
		}]
	},

	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-p1',

		uppertextblockadditionalclass: 'header-p1',
		uppertextblock:[{
			textdata: data.string.p1text2,
			textclass: "header-p1-text",
		}],
		equationblock:[{
			equationblockclass: 'equation-top',
			textclass1: 'data-lhs',
			textdata1: data.string.n4,
			optionclass2: 'fill-option',
			optdivclass2: 'bttexxt correct-answer',
			optclass2: '',
			optdata2: data.string.n5,
			coriconclass: 'display-block',
			textclass3: 'data-rhs',
			textdata3: data.string.n9,
		},{
			equationblockclass: 'equation-bottom-2',
			imgid1: 'apple-4',
			imgclass1: 'apple-lhs-2 apple-1',
			imgid2: 'apple-5-2',
			imgclass2: 'apple-lhs-2  apple-2',
			imgid3: 'apple-9',
			imgclass3: 'apple-rhs-2  apple-3',
		}],
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "apple-2", src: imgpath+"2apple.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cover_page", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},
			{id: "apple-3", src: imgpath+"3apple.png", type: createjs.AbstractLoader.IMAGE},
			{id: "apple-5", src: imgpath+"5apple.png", type: createjs.AbstractLoader.IMAGE},
			{id: "apple-5-2", src: imgpath+"5apple_01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "apple-4", src: imgpath+"4apple.png", type: createjs.AbstractLoader.IMAGE},
			{id: "apple-9", src: imgpath+"9apple.png", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
			{id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);

		switch(countNext) {
			case 0:
				sound_nav('s1_p1');
			break;
			case 1:
				sound_nav('s1_p2');
			break;
			case 2:
				sound_nav('s1_p3');
				//randomize options
				var parent = $(".bottom-option-container");
				var divs = parent.children();
				while (divs.length) {
					parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
				}
				$('.correct-icon').attr('src', preload.getResult('correct').src);
				$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);
				$('.bttexxt').click(function(){
					createjs.Sound.stop();
					if($(this).hasClass('correct-ans')){
						play_correct_incorrect_sound(1);
						$(this).parent().children('.correct-icon').show(0);
						$('.bttexxt').css('pointer-events', 'none');
						$(this).addClass('correct-answer');
						if(countNext < $total_page-1){
							$nextBtn.show(0);
						} else{
							ole.footerNotificationHandler.pageEndSetNotification();
						}
					} else{
						play_correct_incorrect_sound(0);
						$(this).css('pointer-events', 'none');
						$(this).parent().children('.incorrect-icon').show(0);
						$(this).addClass('incorrect-answer');
					}
				});
				break;
			case 3:
				$prevBtn.show(0);
				$('.correct-icon').attr('src', preload.getResult('correct').src);
				$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);
				nav_button_controls(0);
				break;
			default:
				$prevBtn.show(0);
				nav_button_controls(0);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count) {
		if (content[count].hasOwnProperty('equationblock')) {
			var equationblock = content[count].equationblock;
			for (var i = 0; i < equationblock.length; i++) {
				var image_src1,
				    image_src2,
				    image_src3,
				    classes_list1,
				    classes_list2,
				    classes_list3,
				    selector1,
				    selector2,
				    selector3;
				if (equationblock[i].imgid1) {
					image_src1 = preload.getResult(equationblock[i].imgid1).src;
					classes_list1 = equationblock[i].imgclass1.match(/\S+/g) || [];
					selector1 = ('.' + classes_list1[classes_list1.length - 1]);
					$(selector1).attr('src', image_src1);
				}
				if (equationblock[i].imgid2) {
					image_src2 = preload.getResult(equationblock[i].imgid2).src;
					classes_list2 = equationblock[i].imgclass2.match(/\S+/g) || [];
					selector2 = ('.' + classes_list2[classes_list2.length - 1]);
					$(selector2).attr('src', image_src2);
				}
				if (equationblock[i].imgid3) {
					image_src3 = preload.getResult(equationblock[i].imgid3).src;
					classes_list3 = equationblock[i].imgclass3.match(/\S+/g) || [];
					selector3 = ('.' + classes_list3[classes_list3.length - 1]);
					$(selector3).attr('src', image_src3);
				}
			}
		}
	}


	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

﻿function equationMaker(letterArr, type, level, answersign) {
	var Letter = "x";
	var var1 = 0;
	var var2 = 0;
	var var3 = 0;
	var ans = 0;
	var answerSign = 'Positive';
	var calculationType = 'Type 2';
	var calculationLevel = "Simple";
	var LetterArray = new Array("o", "x", "y", "z", "a", "b", "c", "d", "m", "n", "w");
	var Solution = new Array();

	if ( typeof (letterArr) != 'undefined') {
		LetterArray = letterArr;
	}
	LetterArray.shufflearray();
	//Select a letter for the equation
	Letter = LetterArray[0];
	// Letter = "<i>" + Letter + "</i>"
		
		
	if ( typeof (answersign) != 'undefined') {
		answerSign = answersign;
	}
	if ( typeof (type) != 'undefined') {
		calculationType = type;
	}
	if ( typeof (level) != 'undefined') {
		calculationLevel = level;
	}
	ShakeDice();
	if (answerSign == 'Negative') {
		ans = 0 - ans;
	};
	if (answerSign == 'Mixed') {
		var temp = Math.floor(Math.random() * 1.99);
		if (temp == 1) {
			ans = 0 - ans;
		}
	};
	if (calculationType == 'Type 1') {
		Type1();
	};
	if (calculationType == 'Type 2') {
		Type2();
	};
	if (calculationType == 'Type 3') {
		Type3();
	};
	if (calculationType == 'Type 4') {
		Type4();
	};
	if (calculationType == 'Type 5') {
		Type5();
	};
	if (calculationType == 'Mixed') {
		var temp = Math.floor(Math.random() * 4.5);
		if (temp == 0) {
			Type1();
		};
		if (temp == 1) {
			Type2();
		};
		if (temp == 2) {
			Type3();
		};
		if (temp == 3) {
			Type4();
		};
		if (temp == 4) {
			Type5();
		};
	};

	var reutrnVar = {
		'question' : Question,
		'answer' : ans,
		'letter' : Letter,
		'type' : calculationType,
	};
	return (reutrnVar);

	//functions
	function ShakeDice() {
		ans = RandomNumber();
		var1 = RandomNumber();
		var2 = RandomNumber();
		var temp = Math.floor(Math.random() * 2.99);
		if (temp == 1) {
			var2 = 0 - var2;
		}
		var3 = RandomNumber();
		var4 = RandomNumber();
		var temp = Math.floor(Math.random() * 1.99);
		if (temp == 1) {
			var4 = 0 - var4;
		}
		var5 = RandomNumber();
	}

	function Type1() {
		Question = var1 + Letter + ' = ' + (var1 * ans);
	}

	function Type2() {
		Question = var1 + Letter + ' + ' + var2 + ' = ' + (var1 * ans + var2);
		if (var2 < 0) {
			Question = var1 + Letter + ' - ' + (0 - var2) + ' = ' + (var1 * ans + var2);
		};
	}

	function Type3() {
		if (var1 == var3) {
			var1 = var1 + 1
		};
		Question = var1 + Letter
		if (var2 < 0) {
			Question = Question + ' - ' + (0 - var2) + ' = ' + var3 + Letter
		} else {
			Question = Question + ' + ' + var2 + ' = ' + var3 + Letter
		}
		if (var1 * ans + var2 - var3 * ans < 0) {
			Question = Question + ' - ' + (0 - (var1 * ans + var2 - var3 * ans));
		} else {
			Question = Question + ' + ' + (var1 * ans + var2 - var3 * ans);
		}
	}

	function Type4() {
		var temp = Math.floor(Math.random() * 1.99);
		if (temp == 1) {
			var3 = 0 - var3;
		}
		var2 = Math.abs(var2);
		Question = var1 + '(' + var2 + Letter
		if (var3 < 0) {
			Question = Question + ' - ' + (0 - var3) + ')'
		} else {
			Question = Question + ' + ' + var3 + ')'
		}
		if (var4 < 0) {
			Question = Question + ' - ' + (0 - var4);
		} else {
			Question = Question + ' + ' + var4;
		}
		Question = Question + ' = ' + (var1 * (var2 * ans + var3) + var4);
	}

	function Type5() {

		var temp = Math.floor(Math.random() * 1.99);
		if (temp == 1) {
			var3 = 0 - var3;
		}
		var2 = Math.abs(var2);
		if (var5 == var1 * var2) {
			var5 = var5 + 1
		};//In case the number of x on each side is the same.
		Question = var1 + '(' + var2 + Letter
		if (var3 < 0) {
			Question = Question + ' - ' + (0 - var3) + ')'
		} else {
			Question = Question + ' + ' + var3 + ')'
		}
		if (var4 < 0) {
			Question = Question + ' - ' + (0 - var4);
		} else {
			Question = Question + ' + ' + var4;
		}
		Question = Question + ' = ' + var5 + Letter
		if (var1 * (var2 * ans + var3) + var4 - var5 * ans < 0) {
			Question = Question + ' - ' + (0 - (var1 * (var2 * ans + var3) + var4 - var5 * ans));
		} else {
			Question = Question + ' + ' + (var1 * (var2 * ans + var3) + var4 - var5 * ans);
		}
	}

	function RandomNumber() {
		var max = 0;
		if (calculationLevel == 'Simple') {
			max = 9.9999;
		};
		if (calculationLevel == 'Medium') {
			max = 99;
		};
		if (calculationLevel == 'Hard') {
			max = 999;
		};
		return Math.floor(Math.random() * max + 2);
	}

}

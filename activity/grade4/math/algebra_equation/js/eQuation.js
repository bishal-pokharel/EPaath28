﻿var Letter = "x";
var var1 = 0;
var var2 = 0;
var var3 = 0;
var ans = 0;
var Speed = 5;
var Question = "Loading...";
var LetterArray = new Array("o","x","y","z","a","b","c","d","m","n","w");
var Solution = new Array();
function AskQuestion(){
			$('#AnswerDiv').html('<p>&nbsp;</p>');
			var temp = Math.floor(Math.random()*9.5+1);//Select a letter for the equation
			Letter = LetterArray[temp];
			Letter = "<i>" + Letter + "</i>"
			ShakeDice();
		if($('#CalculationScope').val()=='Negative'){ans = 0-ans;};
		if($('#CalculationScope').val()=='Mixed'){
			var temp = Math.floor(Math.random()*1.99);
			if(temp==1){ans = 0-ans;}
		};
		if($('#CalculationType').val()=='Type 1'){Type1();};
		if($('#CalculationType').val()=='Type 2'){Type2();};
		if($('#CalculationType').val()=='Type 3'){Type3();};
		if($('#CalculationType').val()=='Type 4'){Type4();};
		if($('#CalculationType').val()=='Type 5'){Type5();};
		if($('#CalculationType').val()=='Mixed'){
		var temp = Math.floor(Math.random()*4.5);
			if(temp==0){Type1();};
			if(temp==1){Type2();};
			if(temp==2){Type3();};
			if(temp==3){Type4();};
			if(temp==4){Type5();};
		};
		$('#Quickulation').html('<p>' + Question + '</p>');
	}
function ShakeDice() {
		ans = RandomNumber();
		var1 = RandomNumber();
		var2 = RandomNumber();
			var temp = Math.floor(Math.random()*2.99);
			if(temp==1){var2 = 0-var2;}
		var3 = RandomNumber();
		var4 = RandomNumber();
			var temp = Math.floor(Math.random()*1.99);
			if(temp==1){var4 = 0-var4;}
		var5 = RandomNumber();
		}
function Type1() {
		Question = var1 + Letter + ' = ' + (var1*ans);
	}
function Type2() {
		Question = var1 + Letter + ' + ' + var2 + ' = ' + (var1*ans+var2);
		if(var2<0){Question = var1 + Letter + ' - ' + (0-var2) + ' = ' + (var1*ans+var2);};
	}
function Type3() {
		if(var1==var3){var1=var1+1};
		Question = var1 + Letter
		if(var2<0){
			Question = Question + ' - ' + (0-var2) + ' = ' + var3 + Letter
			}else{
			Question = Question + ' + ' + var2 + ' = ' + var3 + Letter
			}
		if(var1*ans+var2 - var3*ans<0){	
			Question = Question + ' - ' + (0-(var1*ans+var2 - var3*ans));
			}else{
			Question = Question + ' + ' + (var1*ans+var2 - var3*ans);
			}
	}
function Type4() {
		var temp = Math.floor(Math.random()*1.99);
		if(temp==1){var3 = 0-var3;}
		var2 = Math.abs(var2);
		Question = var1 + '(' + var2 + Letter
		if(var3<0){
			Question = Question + ' - ' + (0-var3) + ')'
		}else{
			Question = Question + ' + ' + var3 + ')'
		}
		if(var4<0){
			Question = Question + ' - ' + (0-var4);
		}else{
			Question = Question + ' + ' + var4;
		}
		Question = Question + ' = ' + (var1*(var2*ans+var3)+var4);
	}
function Type5() {
		
		var temp = Math.floor(Math.random()*1.99);
		if(temp==1){var3 = 0-var3;}
		var2 = Math.abs(var2);
		if(var5==var1*var2){var5=var5+1};//In case the number of x on each side is the same.
		Question = var1 + '(' + var2 + Letter
		if(var3<0){
			Question = Question + ' - ' + (0-var3) + ')'
		}else{
			Question = Question + ' + ' + var3 + ')'
		}
		if(var4<0){
			Question = Question + ' - ' + (0-var4);
		}else{
			Question = Question + ' + ' + var4;
		}
		Question = Question + ' = ' + var5 + Letter
		if(var1*(var2*ans+var3)+var4-var5*ans<0){
			Question = Question + ' - ' + (0-(var1*(var2*ans+var3)+var4-var5*ans));
		}else{
			Question = Question + ' + ' + (var1*(var2*ans+var3)+var4-var5*ans);
		}
	}
function answer() {
		$('#AnswerDiv').html('<p>' + Letter + " = " + ans + '</p>');
	} // end function answer
function Exercise() {
		var EqType = $('#CalculationType').val();
		var EqLevel = $('#CalculationLevel').val();
			if(EqLevel=='Simple'){EqL=1};
			if(EqLevel=='Medium'){EqL=2};
			if(EqLevel=='Hard'){EqL=3};
		var EqScope = $('#CalculationScope').val();
			if(EqScope=='Positive'){EqScope=1};
			if(EqScope=='Negative'){EqScope=2};
			if(EqScope=='Mixed'){EqScope=3};
		var EqQs = EqType.replace('Type ','')+ EqL+EqScope;
		var EndOfSentence='';
		if(EqType=='Mixed'){
			EndOfSentence='mixed questions';
			EqQs = '6' + EqL+EqScope;
			}else{
			EndOfSentence='questions of ' + EqType;
			}
		$('#AnswerDiv').html('<p style=\"font-size:40px;\"><a style=\"text-decoration:none;\" href=\"/Go/Eq/?n='+EqQs+'\">Transum.org/Go/Eq/?n='+EqQs+'</a></p>');
		$('#Quickulation').html('<p style=font-size:28px;>Go to the URL below for a self-marking<br>exercise containing 10 ' + EqLevel.toLowerCase() + '<br>' + EndOfSentence + '.</p>');
	} // end function Exercise
function WriteQuestions() {
		for (var i=1;i<=10;i++){
		Letter = LetterArray[i];
		Letter = "<i>" + Letter + "</i>"
		ShakeDice();
		if($('#CalculationScope').val()=='Negative'){ans = 0-ans;};
		if($('#CalculationScope').val()=='Mixed'){
			var temp = Math.floor(Math.random()*1.99);
			if(temp==1){ans = 0-ans;}
		};
		if($('#CalculationType').val()=='Type 1'){Type1();};
		if($('#CalculationType').val()=='Type 2'){Type2();};
		if($('#CalculationType').val()=='Type 3'){Type3();};
		if($('#CalculationType').val()=='Type 4'){Type4();};
		if($('#CalculationType').val()=='Type 5'){Type5();};
		if($('#CalculationType').val()=='Mixed'){
			if(i==1 || i==2){Type1();};
			if(i==3 || i==4){Type2();};
			if(i==5 || i==6){Type3();};
			if(i==7 || i==8){Type4();};
			if(i==9 || i==10){Type5();};
		};

 			$('#QSpace'+i+' p').html('<span style=color:purple>' + i + ')</span>&nbsp;&nbsp;' + Question);
 			$('#Span'+i).html(Letter+' = ');
 			$('#Answers').html($('#Answers').html() + i + ') ' + ans + '<br>');
 			Solution[i] = ans;
 		}
	} // end WriteQuestions
function RandomNumber() {
	var max = 0;
	if($('#CalculationLevel').val()=='Simple'){max = 3;};
	if($('#CalculationLevel').val()=='Medium'){max = 99;};
	if($('#CalculationLevel').val()=='Hard'){max = 999;};
	return Math.floor(Math.random()*max+2);
	}

var score = 0;
function trim(str) {
	return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
}

function checkAnswers(form) {
	score = 0;
	for (var i=1;i<=10;i++){
		if ($('#Guess'+i).val() != '') {
			if (trim($('#Guess'+i).val()) == Solution[i]) {
				document.getElementById('tick'+i).style.visibility ="visible";
				document.getElementById('cross'+i).style.visibility ="hidden";
				score = score + 1;
			} else {
				document.getElementById('cross'+i).style.visibility ="visible";
				document.getElementById('tick'+i).style.visibility ="hidden";
			}
		}
}

if (score < 10) {
	document.getElementById("Checkbuttontext").value ="Check again";
}
if (score >= 8) {
	document.getElementById("Trophybutton").style.visibility ="visible";
	document.getElementById("Trophybutton").value ="Claim Your Trophy for " + score + " out of 10";
}
if (score == 10) {
	document.getElementById("Checkbutton").style.visibility ="hidden";
}
}//end function checkAnswers(form)

function ClaimTrophy(form) {
	form.submit();
}





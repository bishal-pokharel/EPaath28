var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',

		extratextblock:[{
			textdata: data.string.p5text1,
			textclass: "diy-text",
		}],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy-inside',

		uppertextblockadditionalclass: 'top-text-block',
		uppertextblock:[{
			textdata: 'asd',
			textclass: "top-equation-text",
		},{
			textdata: data.string.p3diy2t1,
			textclass: "new-equation-btn",
		},{
			textdata: '',
			textclass: "left-side-soln",
		}],
		balanceblock:[{
			balanceblock: '',
			leftdata: 'a',
			rightdata: 'a',
		}],
		optionblockblockadditionalclass: 'bottom-answer-block',
		optionblock:[{
			headerclass: 'bottom-opt-header',
			headerdata: data.string.p3diy2t2,
			optioncontainer: 'bottom-option-container',
			option:[{
				optionclass: 'bottom-option opt-1',
				textclass: 'correct-ans',
				textdata: 'asd',
			},{
				optionclass: 'bottom-option opt-2',
				textclass: '',
				textdata: 'asd',
			},{
				optionclass: 'bottom-option opt-3',
				textclass: '',
				textdata: 'asd',
			},{
				optionclass: 'bottom-option opt-4',
				textclass: '',
				textdata: 'asd',
			},{
				optionclass: 'bottom-option opt-5',
				textclass: '',
				textdata: 'asd',
			}]
		}]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images

			{id: "fulcrum", src: imgpath+"fulcrum.png", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s5_p1", src: soundAsset+"s5_p1.ogg"}
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);

		switch(countNext) {
			case 1:
				$prevBtn.show(0);
				$('.new-equation-btn').hide(0);
				$('.balancediv').removeClass('center-balanced');
				$('.fulcrum').attr('src', preload.getResult('fulcrum').src);
				$('.correct-icon').attr('src', preload.getResult('correct').src);
				$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);
				var equationdata = equationMaker(["y", "z", "a", "b", "c", "d", "m", "n", "w"], 'Type 2', 'Simple', 'Positive');
				var question = equationdata.question;
				var variableName = equationdata.letter;
				$('.top-equation-text').html(question);
				$('.variable-name-bt').html(variableName);
				var quessplits = question.split('=');
				var leftside = quessplits[0];
				var rightside = quessplits[1];
				console.log(leftside);
				var leftsideFinal = modifyVariable(leftside, variableName, 'balane-var-data');
				var rightsideFinal = modifyVariable(rightside, variableName, 'balane-var-data');
				$('.balance-left-text').html(leftsideFinal);
				$('.balance-right-text').html(rightsideFinal);


				var animationclass = ['left-balanced', 'center-balanced', 'right-balanced'];
				var answer = parseInt(equationdata.answer);
				var incorrect1 = answer*2;
				var incorrect2 = Math.floor(answer/2);
				var incorrect3 = answer+1;
				var incorrect4 = answer+Math.round(Math.random()*5);
				while(incorrect4 == incorrect1 || incorrect4 == incorrect3 || incorrect4 ==answer){
					incorrect4 = answer+Math.round(Math.random()*5);
				}

				$('.opt-1>p').html(answer);
				var incorrectAns = [incorrect1, incorrect2, incorrect3, incorrect4];
				for(var i = 2; i< 6; i++){
					$('.opt'+-i+'>p').html(incorrectAns[i-2]);
				}
				for(var i = 1; i< 6; i++){
					$('.opt'+-i).css('order', Math.round(Math.random()*10));
				}

				var balanceFlag = 0;
				var leftAnim =['left-balanced', 'left-balanced', 'left-balanced-r'];
				var rightAnim =['right-balanced', 'right-balanced-l', 'right-balanced'];
				var centerAnim =['center-balanced', 'center-balanced-l', 'center-balanced-r'];

				var leftsidevalue = parseInt(eval(putData(leftside, variableName, answer)));
				var rightsidevalue = parseInt(eval(putData(rightside, variableName, answer)));

				$('.optiontext').click(function(){
					var currentAns = $(this).html();
					leftsidevalue = parseInt(eval(putData(leftside, variableName, currentAns)));
					rightsidevalue = parseInt(eval(putData(rightside, variableName, currentAns)));
					$('.left-side-soln').html($('.balance-left-text').html()+' = '+leftsidevalue);
					if($(this).hasClass('correct-ans')){
						$('.balane-var-data').html(data.string.spacecharacter+'x'+data.string.spacecharacter+currentAns);
						play_correct_incorrect_sound(1);
						$(this).parent().children('.correct-icon').show(0);
						$('.optiontext').css('pointer-events', 'none');
						$(this).addClass('correct-answer');
						$('.new-equation-btn').show(0);
						if(countNext < $total_page-1){
							$nextBtn.show(0);
						} else{
							ole.footerNotificationHandler.lessonEndSetNotification();
						}
						$('.balancediv').removeClass('right-balanced');
						$('.balancediv').removeClass('left-balanced');
						$('.balancediv').removeClass('right-balanced-l');
						$('.balancediv').removeClass('left-balanced-r');
						$('.balancediv').addClass(centerAnim[balanceFlag]);
						// $('.left-side-soln').html(leftsidevalue);
					} else{
						$('.balane-var-data').html('x'+currentAns);
						play_correct_incorrect_sound(0);
						$(this).css('pointer-events', 'none');
						$(this).parent().children('.incorrect-icon').show(0);
						$(this).addClass('incorrect-answer');
						console.log('lsv' + leftsidevalue);
						console.log('rsv' + rightsidevalue);
						if(leftsidevalue>rightsidevalue){
							$('.balancediv').removeClass('right-balanced');
							$('.balancediv').removeClass('right-balanced-l');
							$('.balancediv').addClass(leftAnim[balanceFlag]);
							balanceFlag = 1;
						}else{
							$('.balancediv').removeClass('left-balanced');
							$('.balancediv').removeClass('left-balanced-r');
							$('.balancediv').addClass(rightAnim[balanceFlag]);
							balanceFlag = 2;
						}
					}
				});

				$('.new-equation-btn').click(function(){
					templateCaller();
				});
				//function to make variable black
				function modifyVariable(stringdata, pattern, classname){
					reg = new RegExp(pattern, 'gi');
					return stringdata.replace(reg, function(str) {return '<span class='+classname+'>'+str+'</span>';});
				}
				function putData(stringdata, pattern, data){
					reg = new RegExp(pattern, 'gi');
					return stringdata.replace(reg, function(str) {return '*'+data;});
				}
				break;
			default:
				sound_nav("s5_p1");
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

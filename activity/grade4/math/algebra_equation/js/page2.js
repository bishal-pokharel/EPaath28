var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-p2',

		uppertextblockadditionalclass: 'top-text-p2',
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "obo",
		},{
			textdata: data.string.p2text2,
			textclass: "obo",
		},{
			textdata: data.string.p2text3,
			textclass: "obo",
		}],
		extratextblock:[{
			textdata: data.string.p2text4,
			textclass: "middle-p2-text",
		}],
		equationblock:[{
			signsrc: imgpath + 'plus.svg',
			equationblockclass: 'equation-top-left',
			textclass1: 'data-lhs',
			textdata1: data.string.n3,
			textclass2: 'data-lhs blank-data',
			textdata2: data.string.qmark,
			textclass3: 'data-rhs',
			textdata3: data.string.n6,
		},{
			signsrc: imgpath + 'plus.svg',
			equationblockclass: 'equation-top-left-2',
			textclass1: 'data-lhs',
			textdata1: data.string.n3,
			textclass2: 'data-lhs-20',
			textdata2: data.string.tr,
			textclass3: 'data-rhs',
			textdata3: data.string.n6,
		}],
		optioncontainer: 'bottom-option-container',
		optionblock:[
			{
				optionclass: 'bottom-option opt-1',
				textdivclass: 'bttexxt correct-ans',
				textdata: data.string.n3,
			},{
				optionclass: 'bottom-option opt-2',
				textdivclass: 'bttexxt',
				textdata: data.string.n2,
			},{
				optionclass: 'bottom-option opt-3',
				textdivclass: 'bttexxt',
				textdata: data.string.n5,
			},{
				optionclass: 'bottom-option opt-4',
				textdivclass: 'bttexxt',
				textdata: data.string.n7,
			}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-p2',

		uppertextblockadditionalclass: 'bottom-text-p2',
		uppertextblock:[{
			textdata: data.string.p2text5,
			textclass: "",
		},{
			textdata: data.string.p2text6,
			textclass: "",
		}],
		equationblock:[{
			signsrc: imgpath + 'plus.svg',
			equationblockclass: 'equation-top-1',
			textclass1: 'data-lhs',
			textdata1: data.string.n3,
			textclass2: 'data-lhs width-15',
			textdata2: data.string.tr,
			textclass3: 'data-rhs',
			textdata3: data.string.n6,
		},{
			signsrc: imgpath + 'plus.svg',
			equationblockclass: 'equation-top-2',
			textclass1: 'data-lhs',
			textdata1: data.string.n3,
			optionclass2: 'fill-option',
			optdivclass2: 'bttexxt-nohover correct-answer',
			optclass2: '',
			optdata2: data.string.n3,
			coriconclass: 'display-block',
			textclass3: 'data-rhs',
			textdata3: data.string.n6,
		}],
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-p2',

		uppertextblockadditionalclass: 'header-p2',
		uppertextblock:[{
			textdata: data.string.p2text7,
			textclass: "header-p2-text",
		}],
		extratextblock:[{
			textdata: data.string.p2text8,
			textclass: "middle-p2-text",
			datahighlightflag : true,
			datahighlightcustomclass : 'color-black',
		}],
		equationblock:[{
			equationblockclass: 'equation-top',
			signsrc: imgpath + 'minus.svg',
			textclass1: 'data-lhs',
			textdata1: data.string.n4,
			textclass2: 'data-lhs blank-data',
			textdata2: data.string.tm,
			textclass3: 'data-rhs',
			textdata3: data.string.n2,
		}],
		optioncontainer: 'bottom-option-container',
		optionblock:[{
			optionclass: 'bottom-option opt-1',
			textdivclass: 'bttexxt correct-ans',
			textdata: data.string.n2,
		},{
			optionclass: 'bottom-option opt-2',
			textdivclass: 'bttexxt',
			textdata: data.string.n1,
		},{
			optionclass: 'bottom-option opt-3',
			textdivclass: 'bttexxt',
			textdata: data.string.n4,
		},{
			optionclass: 'bottom-option opt-4',
			textdivclass: 'bttexxt',
			textdata: data.string.n3,
		}]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-p2',

		uppertextblockadditionalclass: 'bottom-text-p2',
		uppertextblock:[{
			textdata: data.string.p2text9,
			textclass: "",
		},{
			textdata: data.string.p2text10,
			textclass: "",
		}],
		equationblock:[{
			equationblockclass: 'equation-top-1',
			signsrc: imgpath + 'minus.svg',
			textclass1: 'data-lhs',
			textdata1: data.string.n4,
			textclass2: 'data-lhs width-15',
			textdata2: data.string.tm,
			textclass3: 'data-rhs',
			textdata3: data.string.n2,
		},{
			equationblockclass: 'equation-top-2',
			signsrc: imgpath + 'minus.svg',
			textclass1: 'data-lhs',
			textdata1: data.string.n4,
			optionclass2: 'fill-option',
			optdivclass2: 'bttexxt-nohover correct-answer',
			optclass2: '',
			optdata2: data.string.n2,
			coriconclass: 'display-block',
			textclass3: 'data-rhs',
			textdata3: data.string.n2,
		}],
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-p2',

		extratextblock:[{
			textdata: data.string.p2text11,
			textclass: "middle-p2-text",
		},{
			textdata: data.string.submit,
			textclass: "submit-button",
		},{
			textdata: data.string.tryagain,
			textclass: "tryagain-button",
		}],

		equationblock:[{
			equationblockclass: 'equation-top-3',
			signsrc: imgpath + 'minus.svg',
			textclass1: 'data-lhs',
			textdata1: data.string.n4,
			textclass2: 'data-lhs guess-number',
			textdata2: data.string.tf,
			textclass3: 'data-rhs',
			textdata3: data.string.n2,
		},{
			equationblockclass: 'equation-bottom-1',
			textclass1: 'data-lhs',
			textdata1: data.string.tf,
			optionclass3: 'input-option',
			inputclass3: 'default-input-p2',
		}],

	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var intervalvar = null;
	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s2_p1", src: soundAsset+"s2_p1.ogg"},
			{id: "s2_p2", src: soundAsset+"s2_p2.ogg"},
			{id: "s2_p3", src: soundAsset+"s2_p3.ogg"},
			{id: "s2_p4", src: soundAsset+"s2_p4.ogg"},
			{id: "s2_p5", src: soundAsset+"s2_p5.ogg"},
			// {id: "sound_2", src: soundAsset+"p2_s1.ogg"},
			// {id: "sound_3", src: soundAsset+"p2_s2.ogg"},
			// {id: "sound_3a", src: soundAsset+"p2_s2a.ogg"},
			// {id: "sound_4", src: soundAsset+"p2_s3.ogg"},
			// {id: "sound_4a", src: soundAsset+"p2_s3a.ogg"},
			// {id: "sound_4b", src: soundAsset+"p2_s3b.ogg"},
			// {id: "sound_5", src: soundAsset+"p2_s4.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);

		switch(countNext) {
			case 0:
			case 2:
				sound_player("s2_p"+(countNext+1));
				//randomize options
				if(countNext==0){
					$(".obo, .equation-top-left, .equation-top-left-2, .middle-p2-text").hide(0);
					$(".obo:eq(0), .equation-top-left").fadeIn(300);
					$(".obo:eq(1), .equation-top-left-2").delay(4190).fadeIn(300);
					$(".obo:eq(2)").delay(10000).fadeIn(300);
					$(".middle-p2-text").delay(14000).fadeIn(300);
				}
				if(countNext>0) $prevBtn.show(0);
				var parent = $(".bottom-option-container");
				var divs = parent.children();
				while (divs.length) {
					parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
				}
				$('.correct-icon').attr('src', preload.getResult('correct').src);
				$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);
				$('.bttexxt').click(function(){
					createjs.Sound.stop();
					if($(this).hasClass('correct-ans')){
						play_correct_incorrect_sound(1);
						$(this).parent().children('.correct-icon').show(0);
						$('.bttexxt').css('pointer-events', 'none');
						$(this).addClass('correct-answer');
						if(countNext < $total_page-1){
							$nextBtn.show(0);
						} else{
							ole.footerNotificationHandler.pageEndSetNotification();
						}
					} else{
						play_correct_incorrect_sound(0);
						$(this).css('pointer-events', 'none');
						$(this).parent().children('.incorrect-icon').show(0);
						$(this).addClass('incorrect-answer');
					}
				});
				break;
			case 1:
				sound_nav("s2_p"+(countNext+1));
				$('.correct-icon').attr('src', preload.getResult('correct').src);
				$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);
				break;
			case 4:
				sound_player("s2_p"+(countNext+1));
				$prevBtn.show(0);
				$('.correct-icon').attr('src', preload.getResult('correct').src);
				$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);
				input_box('.default-input-p2', 1, null);
				$('.default-input-p2').keyup(function(event) {
					if($(this).val().length>0){
						$('.submit-button').addClass('active-submit');
					} else{
						$('.submit-button').removeClass('active-submit');
					}
				});
				$('.submit-button').click(function(){
					createjs.Sound.stop();
					var inputVal = parseInt($('.default-input-p2').val());
					var answer = 2;
					if(inputVal ==  answer){
						play_correct_incorrect_sound(1);
						$('.submit-button').removeClass('active-submit');
						$('.tryagain-button').hide(0);
						$('.default-input-p2').attr('disabled', 'true');
						$('.default-input-p2').addClass('correct-answer');
						$('.default-input-p2').parent().children('.correct-icon').show(0);
						nav_button_controls(0);
					} else{
						play_correct_incorrect_sound(0);
						$('.tryagain-button').show(0);
						$('.default-input-p2').addClass('incorrect-answer');
						$('.default-input-p2').parent().children('.incorrect-icon').show(0);
						$('.default-input-p2').attr('disabled', 'true');
					}
				});
				$('.tryagain-button').click(function(){
					templateCaller();
				});
				break;
			default:
				sound_nav("s2_p"+(countNext+1));
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearInterval(intervalvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearInterval(intervalvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event) {
			var charCode = (event.which) ? event.which : event.keyCode;
			/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
			if (charCode === 13 && button_class != null) {
				$(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
			if (!condition) {
				return true;
			}
			//check if user inputs more than one '.'
			if ((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
				return false;
			}
			//check . and 0-9 separately after checking arrow and other keys
			if ((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110) {
				return false;
			}
			//check max no of allowed digits
			if (String(event.target.value).length >= max_number) {
				return false;
			}
			return true;
		});
	}


});

var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentblockadditionalclass: 'default-bg',
		uppertextblockadditionalclass: 'instruction-text',
		uppertextblock:[{
			textdata: data.string.eins1,
			textclass: "",
		}],

		extratextblock:[{
			textdata: data.string.submit,
			textclass: "submit-button default-submit",
		},{
			textdata: data.string.tryagain,
			textclass: "tryagain-button default-submit",
		}],
		equation:[{
			equationwrapperclass: 'question-div',
			quesno: true,
			equationblock:[{
				equationblockclass: 'equation-ques',
				signdata1: true,
				textclass1: '',
				textdata1: true,
				textclass2: '',
				textdata2:true,
				textclass3: '',
				textdata3: true,
			}],
		}],

		inputdiv:[{
			inputdiv: 'default-input-div',
			inputclass: 'default-input'
		}]
	},
	// slide1
	{
		contentblockadditionalclass: 'default-bg',
		uppertextblockadditionalclass: 'instruction-text',
		uppertextblock:[{
			textdata: data.string.eins1,
			textclass: "",
		}],


		extratextblock:[{
			textdata: data.string.submit,
			textclass: "submit-button default-submit",
		},{
			textdata: data.string.tryagain,
			textclass: "tryagain-button default-submit",
		}],
		equation:[{
			equationwrapperclass: 'question-div',
			quesno: true,
			equationblock:[{
				equationblockclass: 'equation-ques',
				signdata1: true,
				textclass1: '',
				textdata1: true,
				textclass2: '',
				textdata2:true,
				textclass3: '',
				textdata3: true,
			}],
		}],
		inputdiv:[{
			inputdiv: 'default-input-div',
			inputclass: 'default-input'
		}]
	},
	// slide2
	{
		contentblockadditionalclass: 'default-bg',
		uppertextblockadditionalclass: 'instruction-text',
		uppertextblock:[{
			textdata: data.string.eins1,
			textclass: "",
		}],


		extratextblock:[{
			textdata: data.string.submit,
			textclass: "submit-button default-submit",
		},{
			textdata: data.string.tryagain,
			textclass: "tryagain-button default-submit",
		}],
		equation:[{
			equationwrapperclass: 'question-div',
			quesno: true,
			equationblock:[{
				equationblockclass: 'equation-ques',
				signdata1: true,
				textclass1: '',
				textdata1: true,
				textclass2: '',
				textdata2:true,
				textclass3: '',
				textdata3: true,
			}],
		}],
		inputdiv:[{
			inputdiv: 'default-input-div',
			inputclass: 'default-input'
		}]
	},
	// slide3
	{
		contentblockadditionalclass: 'default-bg',
		uppertextblockadditionalclass: 'instruction-text',
		uppertextblock:[{
			textdata: data.string.eins1,
			textclass: "",
		}],

		extratextblock:[{
			textdata: data.string.submit,
			textclass: "submit-button default-submit",
		},{
			textdata: data.string.tryagain,
			textclass: "tryagain-button default-submit",
		}],
		equation:[{
			equationwrapperclass: 'question-div',
			quesno: true,
			equationblock:[{
				equationblockclass: 'equation-ques',
				signdata1: true,
				textclass1: '',
				textdata1: true,
				textclass2: '',
				textdata2:true,
				textclass3: '',
				textdata3: true,
			}],
		}],
		inputdiv:[{
			inputdiv: 'default-input-div',
			inputclass: 'default-input'
		}]
	},
	// slide4
	{
		contentblockadditionalclass: 'default-bg',
		uppertextblockadditionalclass: 'instruction-text',
		uppertextblock:[{
			textdata: data.string.eins1,
			textclass: "",
		}],

		extratextblock:[{
			textdata: data.string.submit,
			textclass: "submit-button default-submit",
		},{
			textdata: data.string.tryagain,
			textclass: "tryagain-button default-submit",
		}],
		equation:[{
			equationwrapperclass: 'question-div',
			quesno: true,
			equationblock:[{
				equationblockclass: 'equation-ques',
				signdata1: true,
				textclass1: '',
				textdata1: true,
				textclass2: '',
				textdata2:true,
				textclass3: '',
				textdata3: true,
			}],
		}],
		inputdiv:[{
			inputdiv: 'default-input-div',
			inputclass: 'default-input'
		}]
	},
	// slide5
	{
		contentblockadditionalclass: 'default-bg',
		uppertextblockadditionalclass: 'instruction-text',
		uppertextblock:[{
			textdata: data.string.eins1,
			textclass: "",
		}],


		extratextblock:[{
			textdata: data.string.submit,
			textclass: "submit-button default-submit",
		},{
			textdata: data.string.tryagain,
			textclass: "tryagain-button default-submit",
		}],
		equation:[{
			equationwrapperclass: 'question-div',
			quesno: true,
			equationblock:[{
				equationblockclass: 'equation-ques',
				signdata1: true,
				textclass1: '',
				textdata1: true,
				textclass2: '',
				textdata2:true,
				textclass3: '',
				textdata3: true,
			}],
		}],
		inputdiv:[{
			inputdiv: 'default-input-div',
			inputclass: 'default-input'
		}]
	},
	// slide6
	{
		contentblockadditionalclass: 'default-bg',
		uppertextblockadditionalclass: 'instruction-text',
		uppertextblock:[{
			textdata: data.string.eins2,
			textclass: "",
		}],
		equation:[{
			equationwrapperclass: 'question-div',
			quesno: true,
			equationblock:[{
				equationblockclass: 'equation-ques',
				signdata1: true,
				textclass1: '',
				textdata1: true,
				textclass2: '',
				textdata2:true,
				textclass3: '',
				textdata3: true,
			}],
		},{
			equationwrapperclass: 'answer-div',
			equationblock:[{
				equationblockclass: 'steps step-1',
				signdata1: 'x',
				textclass1: '',
				textdata1: true,
				textclass2: 'width-15',
				textdata2:true,
				textclass3: '',
				textdata3: true,
			},{
				equationblockclass: 'steps step-2',
				signdata1: 'x',
				textclass1: '',
				textdata1: true,
				textclass2: 'width-15 its-hidden',
				textdata2: true,
				optionclass2: 'input-option',
				inputclass2: 'default-input-2',
				textclass3: '',
				textdata3: true,
			},{
				equationblockclass: 'steps step-3',

				optionclass2: 'input-option',
				inputclass2: 'default-input-2',
				textclass3: '',
				textdata3: true,
			},{
				equationblockclass: 'steps step-4',
				equaldata: data.string.notequal,
				textdata2: true,
				textclass3: '',
				textdata3: true,
			}],
			button:[{
				btndata: data.string.submit,
				btnclass: 'submit-button-2 btn-step-2',
			},{
				btndata: data.string.check,
				btnclass: 'check-btn btn-step-3',
			},{
				btndata: data.string.tryagain,
				btnclass: 'try-again-button-2 btn-step-3'
			}]
		}],
	},
	// slide 7
	{
		contentblockadditionalclass: 'default-bg',
		uppertextblockadditionalclass: 'instruction-text',
		uppertextblock:[{
			textdata: data.string.eins2,
			textclass: "",
		}],
		equation:[{
			equationwrapperclass: 'question-div',
			quesno: true,
			equationblock:[{
				equationblockclass: 'equation-ques',
				signdata1: true,
				textclass1: '',
				textdata1: true,
				textclass2: '',
				textdata2:true,
				textclass3: '',
				textdata3: true,
			}],
		},{
			equationwrapperclass: 'answer-div answer-div-frac',
			equationblock:[{
				equationblockclass: 'steps step-1',
				fractionclass: 'fraction-1',
				fracdata1: data.string.n10 ,
				fracdata2: data.string.tm ,
				fractionclass2: 'fill-option',
				fracdivclass2: 'guess-number-3',
				fracclass2: '',
				textdata3: data.string.n2,
			},{
				equationblockclass: 'steps step-2',
				fractionclass: 'fraction-2',
				fracdata1: data.string.n10 ,
				inputclass: 'frac-input',
				fracclass2:'display-none',
				fracdivclass2: 'guess-number',
				textdata3: data.string.n2,
			},{
				equationblockclass: 'steps step-3',
				optionclass2: 'input-option-2',
				inputclass2: 'default-input-2',
				textclass3: '',
				textdata3: true,
			},{
				equationblockclass: 'steps step-4',
				equaldata: data.string.notequal,
				textdata2: true,
				textclass3: '',
				textdata3: true,
			}],
			button:[{
				btndata: data.string.submit,
				btnclass: 'submit-button-2 btn-step-2-frac',
			},{
				btndata: data.string.check,
				btnclass: 'check-btn btn-step-3-frac',
			},{
				btndata: data.string.tryagain,
				btnclass: 'try-again-button-2 btn-step-3-frac'
			}]
		}],
	},
	// slide8
	{
		contentblockadditionalclass: 'default-bg',
		uppertextblockadditionalclass: 'instruction-text',
		uppertextblock:[{
			textdata: data.string.eins1,
			textclass: "",
		}],

		equation:[{
			equationwrapperclass: 'question-div',
			quesno: true,
			equationblock:[{
				equationblockclass: 'equation-ques',
				signdata1: true,
				textclass1: '',
				textdata1: true,
				textclass2: '',
				textdata2:true,
				textclass3: '',
				textdata3: true,
			}],
		},{
			equationwrapperclass: 'answer-div',
			equationblock:[{
				equationblockclass: 'steps step-1a',
				signdata1: 'x',
				textclass1: '',
				textdata1: true,
				textclass2: 'width-15',
				textdata2:true,
				textclass3: '',
				textdata3: true,
			},{
				equationblockclass: 'steps step-2a',
				signdata1: 'x',
				textclass1: '',
				textdata1: true,
				textclass2: 'width-15',
				textdata2: true,
				textclass3: '',
				textdata3: true,
			},{
				equationblockclass: 'steps step-3a',
				signdata1: 'x',
				textclass1: '',
				textdata1: true,
				textclass2: 'width-15',
				textdata2: true,
				textclass3: '',
				textdata3: true,
			},{
				equationblockclass: 'steps step-4a',

				optionclass1: 'input-option',
				inputclass1: 'default-input-2',
				textclass3: '',
				textdata3: true,
			},{
				equationblockclass: 'steps step-5a',
				equaldata: data.string.notequal,
				textdata2: true,
				textclass3: '',
				textdata3: true,
			}],
			button:[{
				btndata: data.string.submit,
				btnclass: 'submit-button-2 btn-step-2-last',
			},{
				btndata: data.string.check,
				btnclass: 'check-btn btn-step-3-last',
			},{
				btndata: data.string.check,
				btnclass: 'check-btn btn-step-4-last'
			},{
				btndata: data.string.tryagain,
				btnclass: 'try-again-button-2 btn-step-4-last'
			}]
		}],
	},
	// slide9
	{
		contentblockadditionalclass: 'default-bg',
		uppertextblockadditionalclass: 'instruction-text',
		uppertextblock:[{
			textdata: data.string.eins1,
			textclass: "",
		}],

		equation:[{
			equationwrapperclass: 'question-div',
			quesno: true,
			equationblock:[{
				equationblockclass: 'equation-ques',
				signdata1: true,
				textclass1: '',
				textdata1: true,
				textclass2: '',
				textdata2:true,
				textclass3: '',
				textdata3: true,
			}],
		},{
			equationwrapperclass: 'answer-div',
			equationblock:[{
				equationblockclass: 'steps step-1a',
				signdata1: 'x',
				textclass1: '',
				textdata1: true,
				textclass2: 'width-15',
				textdata2:true,
				textclass3: '',
				textdata3: true,
			},{
				equationblockclass: 'steps step-2a',
				signdata1: 'x',
				textclass1: '',
				textdata1: true,
				textclass2: 'width-15',
				textdata2: true,
				textclass3: '',
				textdata3: true,
			},{
				equationblockclass: 'steps step-3a',
				signdata1: 'x',
				textclass1: '',
				textdata1: true,
				textclass2: 'width-15',
				textdata2: true,
				textclass3: '',
				textdata3: true,
			},{
				equationblockclass: 'steps step-4a',

				optionclass1: 'input-option',
				inputclass1: 'default-input-2',
				textclass3: '',
				textdata3: true,
			},{
				equationblockclass: 'steps step-5a',
				equaldata: data.string.notequal,
				textdata2: true,
				textclass3: '',
				textdata3: true,
			}],
			button:[{
				btndata: data.string.submit,
				btnclass: 'submit-button-2 btn-step-2-last',
			},{
				btndata: data.string.check,
				btnclass: 'check-btn btn-step-3-last',
			},{
				btndata: data.string.check,
				btnclass: 'check-btn btn-step-4-last'
			},{
				btndata: data.string.tryagain,
				btnclass: 'try-again-button-2 btn-step-4-last'
			}]
		}],
	},
];
// content.shufflearray();

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	//for question generation
	var hasAnsweredArr = [];
	// remember to use muptiply sign(×) not alphabet x
	var questions =[
		[['?+12=14', 2], ['?+6=9', 3], ['?-3=5', 8], ['?-10=5',15], ['?+7=10', 3]],
		[['3×?=12', 4], ['4×?=32', 8], ['12×?=60', 5], ['63/?=9', 7], ['39/?=3', 13]],
		[['y+10=16', 6], ['y+6=16', 10], ['7+y=9', 2], ['2+y=12',10], ['10+y=13', 3]],
		[['30-y=14', 16], ['16-y=4', 12], ['y-3=7', 10], ['y-12=19',31], ['40-y=31', 9]],
		[['3y=9', 3], ['12y=108', 9], ['7y=56', 8], ['5y=45',9], ['4y=28', 7]],
		[['108/y=12', 9], ['60/y=5', 12], ['49/y=7', 7], ['24/y=6',4]],
		[['8y=64', 8], ['4y=32', 8], ['12y=96', 8], ['12y=144',12], ['6y=42',7]],
		[['56/y=7', 8], ['39/y=3', 13], ['96/y=12', 8], ['117/y=9',13]],
		[['3y+2=14', 4], ['7x+9=16', 1], ['6+3d=27', 7], ['2+9x=20', 2], ['4x+5=25',5]],
		[['7y-3=11', 2], ['2x-2=10', 6], ['16-4x=8', 2], ['6x-2=16', 3], ['12x-4=68',6]],
	];
	for(var questionIdx=0; questionIdx<questions.length; questionIdx++){
		questions[questionIdx] = questions[questionIdx].shufflearray();
		hasAnsweredArr.push(false);
	}





	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	var scoring = new NumberTemplate();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [

			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "ex_1", src: soundAsset+"ex_1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		scoring.init($total_page);
		templateCaller();
	}
	//initialize
	init();


	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templatecaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templatecaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
	/**
	 How to:
	 - Just call instructionblockcontroller() from the template
	 */


	/**
	 What it does:
	 - It inserts and handles closing and opening of instruction block
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	// var wrongclick = false;

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$('#quesno').html(ole.nepaliNumber(countNext+1, $lang)+'.');
		$('.correct-icon').attr('src', preload.getResult('correct').src);
		$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);

		var currentQuestion= questions[countNext][0][0];
		var answer = questions[countNext][0][1];
		$refreshBtn.hide();
		var wrongclick = false;
		switch(countNext) {
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
				countNext==0?sound_player("ex_1"):'';
				questionParser(currentQuestion);
				input_box('.default-input', 2, null);
				$('.default-input').keyup(function(event) {
					if($(this).val().length>0){
						$('.submit-button').addClass('active-submit');
					} else{
						$('.submit-button').removeClass('active-submit');
					}
				});
				$('.tryagain-button').click(function(){
					hasAnsweredArr[countNext] = true;
					templateCaller();
				});
				$('.submit-button').click(function(){
					createjs.Sound.stop();
					var inputVal = parseInt($('.default-input').val());
					if(inputVal ==  answer){
						if(!hasAnsweredArr[countNext]) scoring.update(true);
						$('.submit-button').removeClass('active-submit');
						$('.default-input').attr('disabled', 'true');
						$('.default-input').addClass('correct-answer');
						$('.default-input').parent().children('.correct-icon').show(0);
						$('.default-input').parent().children('.incorrect-icon').hide(0);
						play_correct_incorrect_sound(1);
						nav_button_controls_ex(0);
					} else{
						if(!hasAnsweredArr[countNext]) scoring.update(false);
						$('.default-input').attr('disabled', 'true');
						$('.default-input').addClass('incorrect-answer');
						$('.default-input').parent().children('.incorrect-icon').show(0);
						$(this).hide(0);
						play_correct_incorrect_sound(0);
						$('.tryagain-button').show(0);
					}
				});
				break;
			case 6:
				questionParser(currentQuestion);
				var lhsData = $('.equation-ques .eq-data-2').html();
				$('.steps .eq-data-1').html(lhsData.replace(/[A-Za-z]/g, ''));
				$('.step-1 .eq-data-2').html(lhsData.replace(/[0-9]/g, ''));
				$('.steps .eq-data-3').html($('.equation-ques .eq-data-3').html());

				correctHits = [false, false];
				input_box('.default-input-2', 3, null);
				activateInput('.step-2 .default-input-2', '.btn-step-2');
				activateInput('.step-3 .default-input-2', '.check-btn');
				checkAnswer1('.step-2 .default-input-2', answer ,'.btn-step-2', 0);
				checkAnswer1('.step-3 .default-input-2', answer ,'.check-btn', 1);
				$('.try-again-button-2').click(function(){
					hasAnsweredArr[countNext] = true;
					templateCaller();
				});

				function checkAnswer1(inputClass, answerData, buttonClass, index){
					$(buttonClass).click(function(){
						var inputVal = parseInt($(inputClass).val());
						var currentAns = answerData;
						if(index>0){
							currentAns = $('.step-2 .eq-data-2').html()*$('.step-2 .eq-data-1').html();
							$('.step-4 .eq-data-2').html(inputVal);
							if(inputVal ==  currentAns){
								correctHits[1] = true;
								$(buttonClass).removeClass('active-submit');
								$(inputClass).attr('disabled', 'true');
								$(inputClass).addClass('correct-answer');
								$(inputClass).parent().children('.correct-icon').show(0);
								$(inputClass).parent().children('.incorrect-icon').hide(0);
								play_correct_incorrect_sound(1);
							} else{
								$(inputClass).addClass('incorrect-answer');
								$(inputClass).parent().children('.incorrect-icon').show(0);
								play_correct_incorrect_sound(0);
							}
							if(currentAns != parseInt($('.step-2 .eq-data-3').html())){
								$('.step-4').css('display', 'flex');
							}
							if(correctHits[0]==true && correctHits[1]==true){
								if(!hasAnsweredArr[countNext]) scoring.update(true);
								$nextBtn.show(0);
								$(buttonClass).hide(0);
							}else{
								if(!hasAnsweredArr[countNext]) scoring.update(false);
								$('.try-again-button-2').show(0);
								$(buttonClass).hide(0);
								$nextBtn.show(0);
							}
						} else{
							if(inputVal ==  currentAns){
								!wrongclick?correctHits[0] = true:'';
								$(buttonClass).hide(0);
								$(inputClass).attr('disabled', 'true');
								$(inputClass).parent().hide(0);
								$(inputClass).parent().parent().children('.eq-data-2').html(inputVal);
								$(inputClass).parent().parent().children('.eq-data-2').show(0);
								$('.step-3').css('display', 'flex');
								$('.check-btn').show(0);
								play_correct_incorrect_sound(1);
							}else{
								play_correct_incorrect_sound(0);
								wrongclick= true;
							}
							// $(buttonClass).hide(0);
							// $(inputClass).attr('disabled', 'true');
							// $(inputClass).parent().hide(0);
							// $(inputClass).parent().parent().children('.eq-data-2').html(inputVal);
							// $(inputClass).parent().parent().children('.eq-data-2').show(0);
							// $('.step-3').css('display', 'flex');
							// $('.check-btn').show(0);
						}
					});
				}
				break;
			case 7:
				questionParser(currentQuestion);
				$('.fraction-2 .top-frac').html($('.fraction-1 .top-frac').html());
				$('.steps .eq-data-3').html($('.equation-ques .eq-data-3').html());
				$('.steps .eq-data-3').html($('.equation-ques .eq-data-3').html());

				correctHits = [false, false];
				input_box('.default-input-2', 4, null);
				input_box('.frac-input', 3, null);
				activateInput('.frac-input', '.submit-button-2');
				activateInput('.step-3 .default-input-2', '.check-btn');
				checkAnswer2('.frac-input', answer ,'.submit-button-2', 0);
				checkAnswer2('.step-3 .default-input-2', answer ,'.check-btn', 1);
				$('.try-again-button-2').click(function(){
					hasAnsweredArr[countNext] = true;
					templateCaller();
				});

				function checkAnswer2(inputClass, answerData, buttonClass, index){
					$(buttonClass).click(function(){
						var inputVal = parseFloat($(inputClass).val());
						var currentAns = answerData;
						if(index>0){
							currentAns = Math.round(($('.data-ttf').html()/$('.data-btf').html())*10)/10;
							$('.step-4 .eq-data-2').html(inputVal);
							if(inputVal ==  currentAns){
								correctHits[1] = true;
								$(buttonClass).removeClass('active-submit');
								$(inputClass).attr('disabled', 'true');
								$(inputClass).addClass('correct-answer');
								$(inputClass).parent().children('.correct-icon').show(0);
								play_correct_incorrect_sound(1);
								$(inputClass).parent().children('.incorrect-icon').hide(0);
							} else{
								$(inputClass).addClass('incorrect-answer');
								play_correct_incorrect_sound(0);
								$(inputClass).parent().children('.incorrect-icon').show(0);
							}
							if(inputVal != parseInt($('.step-2 .eq-data-3').html())){
								$('.step-4').css('display', 'flex');
							}
							if(correctHits[0]==true && correctHits[1]==true){
								if(!hasAnsweredArr[countNext]) scoring.update(true);
								$nextBtn.show(0);
								$(buttonClass).hide(0);
							}else{
								if(!hasAnsweredArr[countNext]) scoring.update(false);
								$('.try-again-button-2').show(0);
								$(buttonClass).hide(0);
								$nextBtn.show(0);
							}
						} else{
							if(parseInt(inputVal) ==  currentAns){
								!wrongclick?correctHits[0] = true:'';
								$(buttonClass).hide(0);
								$(inputClass).attr('disabled', 'true');
								$(inputClass).parent().hide(0);
								console.log($('.data-btf'));
								$('.data-btf').html(inputVal);
								$('.data-btf').show(0);
								$('.step-3').css('display', 'flex');
								$('.check-btn').show(0);
								play_correct_incorrect_sound(1);
							}else{
								wrongclick=true;
								play_correct_incorrect_sound(0);
							}
							// $(buttonClass).hide(0);
							// $(inputClass).attr('disabled', 'true');
							// $(inputClass).parent().hide(0);
							// console.log($('.data-btf'));
							// $('.data-btf').html(inputVal);
							// $('.data-btf').show(0);
							// $('.step-3').css('display', 'flex');
							// $('.check-btn').show(0);
						}
					});
				}
			case 8:
				questionParser(currentQuestion);
				correctHits = [false, false, false];
				var coefficient = 0;
				var varIndex = 0;
				var inputString = '<div class="input-option"><input class="default-input-2" value=""></input><img class="cor-incor correct-icon" src=""><img class="cor-incor incorrect-icon" src=""></div>';
				var pString = '<p class="eq-data eq-coeff">coeffdata</p><p class="eq-data eq-sign-new">×</p><p class="eq-data eq-var">vardata</p>';
				var pString2 = '<p class="eq-data eq-coeff">coeffdata</p><p class="eq-data eq-sign-new">×</p><input class="new-input default-input-2" value=""></input>';
				$('.steps .eq-data-1').html(equationVar[0]);
				$('.steps .eq-data-2').html(equationVar[1]);
				$('.steps .eq-data-3').html(equationVar[2]);
				$('.steps .eq-sign-1').html(equationVar[3]);
				for(var i=0; i<equationVar.length; i++){
					var tempString = equationVar[i];
					if(tempString.match(/[a-zA-z]/) != null){
						varIndex = i;
						coefficient = tempString.replace(/[a-zA-z]/g, '');
						var variable = tempString.replace(/[0-9]/g, '');
						pString = pString.replace('coeffdata', coefficient);
						pString = pString.replace('vardata', variable);
						pString2 = pString2.replace('coeffdata', coefficient);
						pString2 = pString2.replace('vardata', variable);
						$('.step-1a .eq-data-'+(i+1)).replaceWith(pString);
						$('.step-2a .eq-data-'+(i+1)).replaceWith(pString2);
						$('.step-3a .eq-data-'+(i+1)).replaceWith(inputString);
					}
				}
				// put images again
				$('.correct-icon').attr('src', preload.getResult('correct').src);
				$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);

				input_box('.default-input-2', 3, null);
				input_box('.new-input', 3, null);

				activateInput('.new-input', '.btn-step-2-last');
				checkAnswer3('.new-input', answer ,'.btn-step-2-last', 0);

				activateInput('.step-3a .default-input-2', '.btn-step-3-last');
				checkAnswer3('.step-3a .default-input-2', answer ,'.btn-step-3-last', 1);

				activateInput('.step-4a .default-input-2', '.btn-step-4-last.check-btn');
				checkAnswer3('.step-4a .default-input-2', answer ,'.btn-step-4-last.check-btn', 2);

				$('.try-again-button-2').click(function(){
					hasAnsweredArr[countNext] = true;
					templateCaller();
				});

				var userAnswers = [0,0,0];
				function checkAnswer3(inputClass, answerData, buttonClass, index){
					$(buttonClass).click(function(){
						var inputVal = parseFloat($(inputClass).val());
						var currentAns = answerData;
						if(index>1){
							currentAns = Math.abs(eval(userAnswers[1] +equationVar[3]+ parseInt(equationVar[Math.abs(varIndex-1)])));
							$('.step-5a .eq-data-2').html(inputVal);
							if(inputVal ==  currentAns){
								correctHits[2] = true;
								$(buttonClass).removeClass('active-submit');
								$(inputClass).attr('disabled', 'true');
								$(inputClass).addClass('correct-answer');
								$(inputClass).parent().children('.correct-icon').show(0);
								$(inputClass).parent().children('.incorrect-icon').hide(0);
								play_correct_incorrect_sound(1);
							} else{
								$(inputClass).addClass('incorrect-answer');
								$(inputClass).parent().children('.incorrect-icon').show(0);
								play_correct_incorrect_sound(0);
							}
							if(inputVal != parseInt(equationVar[2])){
								$('.step-5a').css('display', 'flex');
							}
							if(correctHits[0]==true && correctHits[1]==true && correctHits[2]==true){
								if(!hasAnsweredArr[countNext]) scoring.update(true);
								$nextBtn.show(0);
								$(buttonClass).hide(0);
							}else{
								if(!hasAnsweredArr[countNext]) scoring.update(false);
								$('.try-again-button-2').show(0);
								$(buttonClass).hide(0);
								$nextBtn.show(0);
							}
						} else if(index==1){
							currentAns = coefficient*userAnswers[0];
							if(inputVal ==  currentAns){
								!wrongclick?correctHits[1] = true:'';
								$(inputClass).addClass('correct-answer');
								play_correct_incorrect_sound(1);
								userAnswers[1] = inputVal;
								$(buttonClass).hide(0);
								$(inputClass).attr('disabled', 'true');
								$('.step-4a').css('display', 'flex');
								$('.btn-step-4-last.check-btn').show(0);
								// $(inputClass).parent().children('.correct-icon').show(0);
							}else{
								$(inputClass).addClass('incorrect-answer');
								play_correct_incorrect_sound(0);
								wrongclick = true;
								// $(inputClass).parent().children('.incorrect-icon').show(0);
							}
							// userAnswers[1] = inputVal;
							// $(buttonClass).hide(0);
							// $(inputClass).attr('disabled', 'true');
							// $('.step-4a').css('display', 'flex');
							// $('.btn-step-4-last.check-btn').show(0);
						} else{
							if(inputVal ==  currentAns){
								!wrongclick?correctHits[0] = true:'';
								userAnswers[0] = inputVal;
								$(buttonClass).hide(0);
								var pElement = document.createElement("p");
								var textnode = document.createTextNode(inputVal);1
								pElement.appendChild(textnode);
								pElement.className = 'eq-data';
								$(inputClass).replaceWith(pElement);
								$('.step-3a').css('display', 'flex');
								$('.btn-step-3-last').show(0);
								play_correct_incorrect_sound(1);
							}else{
								play_correct_incorrect_sound(0);
								wrongclick = true;
							}
							// userAnswers[0] = inputVal;
							// $(buttonClass).hide(0);
							// var pElement = document.createElement("p");
							// var textnode = document.createTextNode(inputVal);
							// pElement.appendChild(textnode);
							// pElement.className = 'eq-data';
							// $(inputClass).replaceWith(pElement);
							// $('.step-3a').css('display', 'flex');
							// $('.btn-step-3-last').show(0);
						}
					});
				}
				break;
				case 9:
					wrongclick = false;
					questionParser(currentQuestion);
					correctHits = [false, false, false];
					var coefficient = 0;
					var varIndex = 0;
					var inputString = '<div class="input-option"><input class="default-input-2" value=""></input><img class="cor-incor correct-icon" src=""><img class="cor-incor incorrect-icon" src=""></div>';
					var pString = '<p class="eq-data eq-coeff">coeffdata</p><p class="eq-data eq-sign-new">×</p><p class="eq-data eq-var">vardata</p>';
					var pString2 = '<p class="eq-data eq-coeff">coeffdata</p><p class="eq-data eq-sign-new">×</p><input class="new-input default-input-2" value=""></input>';
					$('.steps .eq-data-1').html(equationVar[0]);
					$('.steps .eq-data-2').html(equationVar[1]);
					$('.steps .eq-data-3').html(equationVar[2]);
					$('.steps .eq-sign-1').html(equationVar[3]);
					for(var i=0; i<equationVar.length; i++){
						var tempString = equationVar[i];
						if(tempString.match(/[a-zA-z]/) != null){
							varIndex = i;
							coefficient = tempString.replace(/[a-zA-z]/g, '');
							var variable = tempString.replace(/[0-9]/g, '');
							pString = pString.replace('coeffdata', coefficient);
							pString = pString.replace('vardata', variable);
							pString2 = pString2.replace('coeffdata', coefficient);
							pString2 = pString2.replace('vardata', variable);
							$('.step-1a .eq-data-'+(i+1)).replaceWith(pString);
							$('.step-2a .eq-data-'+(i+1)).replaceWith(pString2);
							$('.step-3a .eq-data-'+(i+1)).replaceWith(inputString);
						}
					}
					// put images again
					$('.correct-icon').attr('src', preload.getResult('correct').src);
					$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);

					input_box('.default-input-2', 3, null);
					input_box('.new-input', 3, null);

					activateInput('.new-input', '.btn-step-2-last');
					checkAnswer3('.new-input', answer ,'.btn-step-2-last', 0);

					activateInput('.step-3a .default-input-2', '.btn-step-3-last');
					checkAnswer3('.step-3a .default-input-2', answer ,'.btn-step-3-last', 1);

					activateInput('.step-4a .default-input-2', '.btn-step-4-last.check-btn');
					checkAnswer3('.step-4a .default-input-2', answer ,'.btn-step-4-last.check-btn', 2);

					$('.try-again-button-2').click(function(){
						hasAnsweredArr[countNext] = true;
						templateCaller();
					});

					var userAnswers = [0,0,0];
					function checkAnswer3(inputClass, answerData, buttonClass, index){
						$(buttonClass).click(function(){
							var inputVal = parseFloat($(inputClass).val());
							var currentAns = answerData;
							if(index>1){
								currentAns = Math.abs(eval(userAnswers[1] +equationVar[3]+ parseInt(equationVar[Math.abs(varIndex-1)])));
								$('.step-5a .eq-data-2').html(inputVal);
								if(inputVal ==  currentAns){
									correctHits[2] = true;
									$(buttonClass).removeClass('active-submit');
									$(inputClass).attr('disabled', 'true');
									$(inputClass).addClass('correct-answer');
									$(inputClass).parent().children('.correct-icon').show(0);
									$(inputClass).parent().children('.incorrect-icon').hide(0);
									play_correct_incorrect_sound(1);
								} else{
									$(inputClass).addClass('incorrect-answer');
									$(inputClass).parent().children('.incorrect-icon').show(0);
									play_correct_incorrect_sound(0);
								}
								if(inputVal != parseInt(equationVar[2])){
									$('.step-5a').css('display', 'flex');
								}
								if(correctHits[0]==true && correctHits[1]==true && correctHits[2]==true){
									if(!hasAnsweredArr[countNext]) scoring.update(true);
									$nextBtn.show(0);
									$(buttonClass).hide(0);
								}else{
									if(!hasAnsweredArr[countNext]) scoring.update(false);
									$('.try-again-button-2').show(0);
									$(buttonClass).hide(0);
									$nextBtn.show(0);
								}
							} else if(index==1){
								currentAns = coefficient*userAnswers[0];
								if(inputVal ==  currentAns){
									!wrongclick?correctHits[1] = true:'';
									$(inputClass).addClass('correct-answer');
									play_correct_incorrect_sound(1);
									userAnswers[1] = inputVal;
									$(buttonClass).hide(0);
									$(inputClass).attr('disabled', 'true');
									$('.step-4a').css('display', 'flex');
									$('.btn-step-4-last.check-btn').show(0);
									// $(inputClass).parent().children('.correct-icon').show(0);
								}else{
									$(inputClass).addClass('incorrect-answer');
									play_correct_incorrect_sound(0);
									wrongclick = true;
									// $(inputClass).parent().children('.incorrect-icon').show(0);
								}
								// userAnswers[1] = inputVal;
								// $(buttonClass).hide(0);
								// $(inputClass).attr('disabled', 'true');
								// $('.step-4a').css('display', 'flex');
								// $('.btn-step-4-last.check-btn').show(0);
							} else{
								if(inputVal ==  currentAns){
									!wrongclick?correctHits[0] = true:'';
									userAnswers[0] = inputVal;
									$(buttonClass).hide(0);
									var pElement = document.createElement("p");
									var textnode = document.createTextNode(inputVal);1
									pElement.appendChild(textnode);
									pElement.className = 'eq-data';
									$(inputClass).replaceWith(pElement);
									$('.step-3a').css('display', 'flex');
									$('.btn-step-3-last').show(0);
									play_correct_incorrect_sound(1);
								}else{
									play_correct_incorrect_sound(0);
									wrongclick = true;
								}
								// userAnswers[0] = inputVal;
								// $(buttonClass).hide(0);
								// var pElement = document.createElement("p");
								// var textnode = document.createTextNode(inputVal);
								// pElement.appendChild(textnode);
								// pElement.className = 'eq-data';
								// $(inputClass).replaceWith(pElement);
								// $('.step-3a').css('display', 'flex');
								// $('.btn-step-3-last').show(0);
							}
						});
					}
				break;
			default:
				break;
		}
	}

	function nav_button_controls_ex(){
		if( countNext>0 && countNext == $total_page-1){
			ole.footerNotificationHandler.pageEndSetNotification();
		} else{
			$nextBtn.show(0);
		}
	}
	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();
		generalTemplate();
	}
	// a + b = c
	var equationVar = ['a', 'b', 'c','sign'];
	function questionParser(questionData){
		questionData= questionData.replace(' ', '');
		var sign= questionData.replace(/[0-9A-Za-z]/g, '');
		sign= sign.replace('?', '');
		sign= sign.replace('=', '');
		var vals = questionData.split('=');
		var rhsVal = vals[1];
		$('.equation-ques .eq-data-3').html(rhsVal);
		var lhsVals = vals[0];
		if(sign == ''){
			$('.equation-ques .eq-sign-1').hide(0);
			lhsVals = ['', vals[0]];
			equationVar = ['', '', vals[0], rhsVal];
		}else{
			$('.equation-ques .eq-sign-1').html(sign);
			lhsVals = lhsVals.split(sign);
			equationVar = [lhsVals[0], lhsVals[1], rhsVal, sign];
		}
		if(sign=='/'){
			$('.equation-ques .eq-sign-1').hide(0);
			$('.equation-ques .eq-data-1').hide(0);
			createfractionItem('.eq-data-2', lhsVals);
		} else{
			$('.equation-ques .eq-data-2').html(lhsVals[1]);
			if(lhsVals[1]=='?'){
				$('.equation-ques .eq-data-2').addClass('blank-data');
			}
			$('.equation-ques .eq-data-1').html(lhsVals[0]);
			if(lhsVals[0]=='?'){
				$('.equation-ques .eq-data-1').addClass('blank-data');
			}
		}
	}
	function createfractionItem(parent, fracvVlues){
		$(parent).addClass('fraction-1');
		$(parent).html('<span class="top-frac"></span><span class="bt-frac"></span>');
		$('.fraction-1 .top-frac').html(fracvVlues[0]);
		$('.fraction-1 .bt-frac').html(fracvVlues[1]);
		if(fracvVlues[1]=='?'){
			$('.fraction-1 .bt-frac').addClass('blank-data');
		}
		if(fracvVlues[0]=='?'){
			$('.fraction-1 .top-frac').addClass('blank-data');
		}
	}

	function activateInput(inputClass, btnclass){
		$(inputClass).keyup(function(event) {
			if($(this).val().length>0){
				$(btnclass).addClass('active-submit');
			} else{
				$(btnclass).removeClass('active-submit');
			}
		});
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	$nextBtn.on('click', function() {
		countNext++;
		scoring.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){

	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event) {
			var charCode = (event.which) ? event.which : event.keyCode;
			/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
			if (charCode === 13 && button_class != null) {
				$(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
			if (!condition) {
				return true;
			}
			//check if user inputs more than one '.'
			if ((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
				return false;
			}
			//check . and 0-9 separately after checking arrow and other keys
			if ((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110) {
				return false;
			}
			//check max no of allowed digits
			if (String(event.target.value).length >= max_number) {
				return false;
			}
			return true;
		});
	}

});

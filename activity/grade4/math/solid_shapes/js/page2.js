var imgpath = $ref + "/images/";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
var soundAsset = $ref+"/sounds/"+$lang+'/';

var sound_1 = new buzz.sound(soundAsset+"s2_p1_1.ogg");
var sound_2 = new buzz.sound(soundAsset+"s2_p1_2.ogg");
var sound_3 = new buzz.sound(soundAsset+"s2_p2.ogg");
var sound_4 = new buzz.sound(soundAsset+"s2_p4.ogg");
var sound_5 = new buzz.sound(soundAsset+"s2_p5_1.ogg");
var sound_6 = new buzz.sound(soundAsset+"s2_p5_2.ogg");
var sound_7 = new buzz.sound(soundAsset+"s2_p6_1.ogg");
var sound_8 = new buzz.sound(soundAsset+"s2_p6_2.ogg");

var content = [
{
	//0slide
	contentblockadditionalclass : "bluebg",
	uppertextblock : [{
		textclass : "question",
		textdata : data.string.p2_question,
	},{
		textclass : "answer",
		textdata : data.string.p2_q1_answer1,
	}],
	imageblockadditionalclass : "shapequestion",
	imageblock : [{
		imagestoshow : [{
			imgclass : "square",
			imgsrc : imgpath + "shape02.png",
			answer: data.string.p2_q1_option2
		},{
			imgclass : "correct",
			imgsrc : "images/correct.png"
		},{
			imgclass : "incorrect01",
			imgsrc : "images/wrong.png"
		},{
			imgclass : "incorrect02",
			imgsrc : "images/wrong.png"
		}],
		imagelabels : [{
			imagelabelclass : "firstOption",
			imagelabeldata : data.string.p2_q1_option2
		},{
			imagelabelclass : "secondOption",
			imagelabeldata : data.string.p2_q1_option1
		},{
			imagelabelclass : "thirdOption",
			imagelabeldata : data.string.p2_q1_option3
		}]
	}]
},
{
	//1slide
	contentblockadditionalclass : "bluebg",
	uppertextblock : [{
		textclass : "question1",
		datahighlightflag : true,
		textdata : data.string.p2_s1,
	}],
	imageblockadditionalclass : "shapequestion",
	imageblock : [{
		imagestoshow : [{
			imgclass : "square1",
			imgsrc : imgpath + "square.png"
		},{
			imgclass : "cubeobject1",
			imgsrc : imgpath + "cube.png"
		},{
			imgclass : "recttocubearrow",
			imgsrc : imgpath + "arrow03.png"
		}],
		imagelabels : [{
			imagelabelclass : "imagelabel1 labelcorrect",
			imagelabeldata : data.string.p2_q1_option2
		},{
			imagelabelclass : "imagelabel3 labelcorrect",
			imagelabeldata : data.string.p2_s2
		}]
	}]
},
{
	//2slide
	contentblockadditionalclass : "bluebg",
	uppertextblock : [{
		textclass : "question1",
		datahighlightflag : true,
		textdata : data.string.p2_s1,
	}],
	imageblockadditionalclass : "shapequestion",
	imageblock : [{
		imagestoshow : [{
			imgclass : "square1",
			imgsrc : imgpath + "square.png"
		},{
			imgclass : "cubeobject1",
			imgsrc : imgpath + "cube.gif"
		},{
			imgclass : "recttocubearrow",
			imgsrc : imgpath + "arrow03.png"
		}],
		imagelabels : [{
			imagelabelclass : "imagelabel1 labelcorrect",
			imagelabeldata : data.string.p2_q1_option2
		},{
			imagelabelclass : "imagelabel3 labelcorrect",
			imagelabeldata : data.string.p2_s2
		}]
	}]
},


{
	//3slide
	contentblockadditionalclass : "bluebg",
	filterdiv: true,
	filterdivclass: "purplefilterdiv",
	// filterimageblock : [{
		// imagestoshow : [{
			// imgclass : "circle01",
			// imgsrc : imgpath + "shape03.png"
		// }]
	// }],
	containsdialog : [
	{
		dialogcontainer: "dialogcontainer01",
		dialogimageclass : "dialog",
		dialogimagesrc : imgpath + "narratordialog.png",
		dialogcontentclass: "dialogcontent",
		dialogcontent : data.string.p2_q1_homer,
	}],
	imageblockadditionalclass : "shapequestion",
	imageblock : [{
		imagestoshow : [{
			imgclass : "cubeobject1",
			imgsrc : imgpath + "cube.png"
		},{
			imgclass : "homer02",
			imgsrc : imgpath + "sundar.png"
		},{
			imgclass : "dice",
			imgsrc : imgpath + "dice.png"
		},{
			imgclass : "giftbox",//giftbox
			imgsrc : imgpath + "cube-gift.png"
		},{
			imgclass : "box",
			imgsrc : imgpath + "cube_box.png"
		},{
			imgclass : "rubiks",
			imgsrc : imgpath + "rubiks.png"
		}],
		imagelabels : [{
			imagelabelclass : "imagelabel2 ",
			imagelabeldata : data.string.p2_s3
		},{
			imagelabelclass : "imagelabel1",
			imagelabeldata : data.string.p2_s5
		},{
			imagelabelclass : "imagelabel3 labelcorrect",
			imagelabeldata : data.string.p2_s2
		},{
			imagelabelclass : "imagelabel4",
			imagelabeldata : data.string.p2_s4
		},{
			imagelabelclass : "imagelabel5",
			imagelabeldata : data.string.p2_s6
		}]
	}]
},
{
	//4slide
	uppertextblock : [{
		textclass : "circlequestion",
		textdata : data.string.p2_question,
	},{
		textclass : "answer",
		datahighlightflag : true,
		textdata : data.string.p2_q1_answer2,
	}],

	imageblockadditionalclass : "shapequestion",
	imageblock :[{
		imagestoshow : [{
			imgclass : "circle",
			imgsrc : imgpath + "shape03.png",
			answer: data.string.p2_q1_option3
		},{
			imgclass : "correct1",
			imgsrc : "images/correct.png"
		},{
			imgclass : "incorrect03",
			imgsrc : "images/wrong.png"
		},{
			imgclass : "incorrect02",
			imgsrc : "images/wrong.png"
		}],
		imagelabels : [{
			imagelabelclass : "imagelabel2 firstOption",
			imagelabeldata : data.string.p2_q1_option2
		},{
			imagelabelclass : "imagelabel1 secondOption",
			imagelabeldata : data.string.p2_q1_option1
		},{
			imagelabelclass : "imagelabel3 thirdOption",
			imagelabeldata : data.string.p2_q1_option3
		}]
	}]
},
{
	//5slide

	uppertextblock : [{
		textclass : "description1",
		datahighlightflag : true,
		textdata : data.string.p2_s18,
	},{
		textclass : "description2",
		datahighlightflag : true,
		textdata : data.string.p2_s20,
	}],
	imageblockadditionalclass : "shapequestion",
	imageblock : [{
		imagestoshow : [{
			imgclass : "cylinder",
			imgsrc : imgpath + "cylinder.gif"
		},{
			imgclass : "sphere",
			imgsrc : imgpath + "rotating_sphere.gif"
		},{
			imgclass : "spotlight",
			imgsrc : imgpath + "collection-item-spotlight.png"
		},{
			imgclass : "cylinderexample1",
			imgsrc : imgpath + "battery.png"
		},{
			imgclass : "cylinderexample2",
			imgsrc : imgpath + "diet-coke.png"
		},{
			imgclass : "cylinderexample3",
			imgsrc : imgpath + "penholder_empty.png"
		},{
			imgclass : "cylinderexample4",
			imgsrc : imgpath + "water-bottle.png"
		},{
			imgclass : "sphereexample1",
			imgsrc : imgpath + "world-globe.png"
		},{
			imgclass : "sphereexample2",
			imgsrc : imgpath + "football.png"
		},{
			imgclass : "sphereexample3",
			imgsrc : imgpath + "pearls.png"
		},{
			imgclass : "sphereexample4",
			imgsrc : imgpath + "tenis-ball.png"
		},],
		imagelabels : [{
			imagelabelclass : "imagelabel1 labelcorrect",
			imagelabeldata : data.string.p2_s19
		},{
			imagelabelclass : "imagelabel3 labelcorrect",
			imagelabeldata : data.string.p2_s21
		}]
	}]
}
// ,
// {
	// //5slide
	// hasheaderblock : true,
	// headerblock : [{
		// textclass : "introductionheader",
		// textdata : data.string.p2_title_square
	// }],
//
	// uppertextblock : [{
		// textclass : "question",
		// textdata : data.string.p2_s14,
	// }],
	// imageblockadditionalclass : "shapequestion",
	// imageblock :{
			// imgclass : "correct",
			// imgsrc : "images/correct.png"
		// },{
			// imgclass : "incorrect01",
			// imgsrc : "images/wrong.png"
		// },{
			// imgclass : "incorrect02",
			// imgsrc : "images/wrong.png"
		// } [{
		// imagestoshow : [{
			// imgclass : "square",
			// imgsrc : imgpath + "cube.gif"
		// },{
			// imgclass : "dice",
			// imgsrc : imgpath + "dice.png"
		// },{
			// imgclass : "penholderfadeaway",
			// imgsrc : imgpath + "flowerbox01.png"
		// },{
			// imgclass : "penholderfadein",
			// imgsrc : imgpath + "flowerbox.png"
		// }],
		// imagelabels : [{
			// imagelabelclass : "imagelabel2",
			// imagelabeldata : data.string.p2_s10
		// },{
			// imagelabelclass : "imagelabel1",
			// imagelabeldata : data.string.p2_s12
		// },{
			// imagelabelclass : "imagelabel3",
			// imagelabeldata : data.string.p2_s13
		// }]
	// }]
// }
];

$(function() {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var lastpagecountNext = 0;
	loadTimelineProgress($total_page, countNext + 1);

	/*==================================================
	 =            Handlers and helpers Block            =
	 ==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	/*=====  End of data highlight function  ======*/

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templateCaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templateCaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		if(countNext==5) ole.footerNotificationHandler.pageEndSetNotification();
		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
	/**
	 How to:
	 - Just call instructionblockcontroller() from the template
	 */

	/**
	 What it does:
	 - It inserts and handles closing and opening of instruction block
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=====  End of Handlers and helpers Block  ======*/

	/*=======================================
	 =            Templates Block            =
	 =======================================*/
	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		vocabcontroller.findwords(countNext);

		switch (countNext){
			case 0:
					soundplayer(sound_1,1);
    			$(".answer").hide(0);
    			$(".incorrect02, .incorrect01, .correct").hide(0);
    			$nextBtn.hide(0);
				$(".firstOption, .secondOption, .thirdOption").click(function() {
    				var $this = $(this);
    				if ($this.html().trim().toLowerCase() === $(".square").attr("data-answer").toLowerCase()) {
    					// $this.toggleClass('labelcorrect');
							play_correct_incorrect_sound(1);
							setTimeout(()=>soundplayer(sound_2),800);
    					$(".correct").show(0);
    					$this.siblings("label").fadeOut(1000);
    					// $image.attr("src", imgpath+"candel.gif");
    					$(".question").hide(0);
    					$(".answer").show(0);

    					 // else {
    						// setTimeout(function() {
    							// ole.footerNotificationHandler.pageEndSetNotification();
    						// }, 1000);
    					// }
    				} else {
    					if ($this.hasClass("secondOption")) {
								play_correct_incorrect_sound(0);
    						$(".incorrect02").show(0);
    						setTimeout(function() {
    							$(".incorrect02").hide(0);
    						}, 2000);
    					} else {
								play_correct_incorrect_sound(0);
    						$(".incorrect01").show(0);
    						setTimeout(function() {
    							$(".incorrect01").hide(0);
    						}, 2000);
    					}
    				}
				});

				break;

			case 1:
				soundplayer(sound_3);

				break;
			case 3:
				$(".imagelabel1, .imagelabel2, .imagelabel4, .imagelabel5").hide(0);
				$(".imagelabel1").delay(3500).show(1000);
				setTimeout(function(){
					$(".dialogcontainer01").show(0,()=>soundplayer(sound_4));
				}, 2000);
				setTimeout(function(){
					$(".imagelabel2, .imagelabel4, .imagelabel5").show(1000);
				}, 3500);
				break;

			case 4:
				$(".contentblock").css('background','#4B335D');
				$(".answer").hide(0);

				$(".incorrect02, .incorrect03, .correct1").hide(0,()=>soundplayer(sound_5,1));
				$(".imagelabel2, .imagelabel1, .imagelabel3").click(function() {
	                var $this = $(this);
					if ($this.html().trim().toLowerCase() === $(".circle").attr("data-answer").toLowerCase()) {
						// $this.toggleClass('labelcorrect');
						play_correct_incorrect_sound(1);
						setTimeout(()=>soundplayer(sound_6),800);
						$(".correct1").show(0);
						$this.siblings("label").fadeOut(1000);
						// $image.attr("src", imgpath+"candel.gif");
						$(".circlequestion").hide(0);
						$(".answer").show(0);
						// else {
						// setTimeout(function() {
						// ole.footerNotificationHandler.pageEndSetNotification();
						// }, 1000);
						// }
					} else {
						if ($this.hasClass("secondOption")) {
							play_correct_incorrect_sound(0);
							$(".incorrect02").show(0);
							setTimeout(function() {
								$(".incorrect02").hide(0);
							}, 2000);
						} else {
							play_correct_incorrect_sound(0);
							$(".incorrect03").show(0);
							setTimeout(function() {
								$(".incorrect03").hide(0);
							}, 2000);
						}
					}
				});

				break;

			case 5:
				lastpagecountNext = 0;
                buzz.all().stop();
                sound_7.play().bind("ended",function(){
                    soundplayer(sound_8,false);
                    $(".description2, .sphere, .imagelabel3").show(0);
                });
				$(".contentblock").css('background', '#674EA7');
				// $(".contentblock").css('background', '#957CD5');
				$(".imagelabel1").css('left', '8.5%');
				$(".imagelabel3").css('right', '8.5%');
				$(".textblock").css('height', '30%');
				$(".description2, .sphere, .sphereexample1, .sphereexample2, .sphereexample3, .sphereexample4, .cylinderexample1, .cylinderexample2, .cylinderexample3, .cylinderexample4, .imagelabel3").hide(0);
				break;

			default:
			navigationcontroller();
				break;
		}

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//call instruction block controller
		instructionblockcontroller($board);

		//call notifyuser
		// notifyuser($anydiv);

		// find if there is linehorizontal div in the slide
		var $linehorizontal = $board.find("div.linehorizontal");
		if ($linehorizontal.length > 0) {
			$linehorizontal.attr('data-isdrawn', 'draw');
		}
	}

	/*=====  End of Templates Block  ======*/

	/*==================================================
	=            Templates Controller Block            =
	==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
	 Motivation :
	 - Make a single function call that handles all the
	 template load easier

	 How To:
	 - Update the template caller with the required templates
	 - Call template caller

	 What it does:
	 - According to value of the Global Variable countNext
	 the slide templates are updated
	 */
	 function soundplayer(i,next){
     buzz.all().stop();
     i.play().bind("ended",function(){
        if(!next) navigationcontroller();
     });
   }

	function templateCaller(calledfromprevbtnclick) {
		/*always hide next and previous navigation button unless
		 explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		// navigationcontroller();

		// call the template
		if(countNext == 4 && !calledfromprevbtnclick){
			$(".purplefilterdiv").show(0);
			setTimeout(function (){
				generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

			}, 3000);
		}else{
			generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

		}


		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page, countNext + 1);

		// just for development purpose to see total slide vs current slide number
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller(false);

	/* navigation buttons event handlers */

	$nextBtn.on('click', function() {
		if(countNext == 5){
			switch(lastpagecountNext){
				case 0:
					$(".spotlight").hide(0).css({
						'left' : '75%',
						'transform' : 'translate(-50%)'
					}).show(500);
					//TODO: do using css
					$(".description1").hide(0);
					$(".sphere").show(400);
					$(".description2").show(0);
					$(".imagelabel3").show(0);
					break;
				case 1:
					$(".spotlight").hide(0).css({
						'left' : '25%',
						'transform' : 'translate(-50%)'
					}).show(500);
					$(".description2").hide(0);
					setTimeout(function(){
						$(".cylinderexample1, .cylinderexample2, .cylinderexample3, .cylinderexample4").show(0);
					}, 1000);
					break;
				case 2:
					$(".spotlight").hide(0).css({
						'left' : '75%',
						'transform' : 'translate(-50%)'
					}).show(500);
					setTimeout(function(){
						$(".sphereexample1, .sphereexample2, .sphereexample3, .sphereexample4").show(0);
					}, 1000);

					setTimeout(function(){
						$(".spotlight").hide(0);
						ole.footerNotificationHandler.pageEndSetNotification();
					}, 2500);
					$nextBtn.hide(0);

					break;

				default:

					break;
			}
			lastpagecountNext++;
		}else{
			countNext++;
			templateCaller(false);
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller(true);

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/*=====  End of Templates Controller Block  ======*/
});

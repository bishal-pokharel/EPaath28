var imgpath = $ref + "/images/";
// TODO: Let's remove this
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";


var soundAsset = $ref+"/sounds/"+$lang+'/';

var sound_1 = new buzz.sound(soundAsset+"s4_p1.ogg");
var sound_2 = new buzz.sound(soundAsset+"s4_p2.ogg");
var sound_3 = new buzz.sound(soundAsset+"s4_p3_1.ogg");
var sound_4 = new buzz.sound(soundAsset+"s4_p3_2.ogg");
var sound_5 = new buzz.sound(soundAsset+"s4_p4.ogg");
var sound_6 = new buzz.sound(soundAsset+"s4_p5.ogg");

var content = [
{
	contentblockadditionalclass : "mainTitle",
	contentblocknocenteradjust : true,
	uppertextblock : [{
		textclass : "firsttitle",
		datahighlightflag : true,
		textdata : data.string.introductioncube
	}]

},
{
	contentblockadditionalclass : "mainTitle",

	uppertextblock : [{
		textclass : "introductionheader",
		textdata : data.string.p5_title_cube
	},{
		textclass : "question",
		datahighlightflag : true,
		textdata : data.string.p5_s1,
	}],
	imageblockadditionalclass : "shapequestion",
	cuboidfaces: [
	{
		faceid: "face1",
		faceposition: "front",
		facenumber: 1,
		associatedimage: [
		{
			imgclass: "cornertopleft",
			imgsrc: imgpath+"topleft.png"
		},{
			imgclass: "cornertopright",
			imgsrc: imgpath+"topright.png"
		},{
			imgclass: "cornerbottomleft",
			imgsrc: imgpath+"bottomleft.png"
		},{
			imgclass: "cornerbottomright",
			imgsrc: imgpath+"bottomright.png"
		}
		]

	},
	{
		faceid: "face6",
		faceposition: "back",
		facenumber: 6,
		associatedimage: [
		{
			imgclass: "cornertopleft",
			imgsrc: imgpath+"topleft.png"
		},{
			imgclass: "cornertopright",
			imgsrc: imgpath+"topright.png"
		},{
			imgclass: "cornerbottomleft",
			imgsrc: imgpath+"bottomleft.png"
		},{
			imgclass: "cornerbottomright",
			imgsrc: imgpath+"bottomright.png"
		}
		]

	},
	{
		faceid: "face5",
		faceposition: "top",
		facenumber: 5,
		associatedimage: [
		{
			imgclass: "cornertopleft",
			imgsrc: imgpath+"topleft.png"
		},{
			imgclass: "cornertopright",
			imgsrc: imgpath+"topright.png"
		},{
			imgclass: "cornerbottomleft",
			imgsrc: imgpath+"bottomleft.png"
		},{
			imgclass: "cornerbottomright",
			imgsrc: imgpath+"bottomright.png"
		}
		]

	},
	{
		faceid: "face2",
		faceposition: "bottom",
		facenumber: 2,
		associatedimage: [
		{
			imgclass: "cornertopleft",
			imgsrc: imgpath+"topleft.png"
		},{
			imgclass: "cornertopright",
			imgsrc: imgpath+"topright.png"
		},{
			imgclass: "cornerbottomleft",
			imgsrc: imgpath+"bottomleft.png"
		},{
			imgclass: "cornerbottomright",
			imgsrc: imgpath+"bottomright.png"
		}
		]

	},
	{
		faceid: "face4",
		faceposition: "left",
		facenumber: 4,
		associatedimage: [
		{
			imgclass: "cornertopleft",
			imgsrc: imgpath+"topleft.png"
		},{
			imgclass: "cornertopright",
			imgsrc: imgpath+"topright.png"
		},{
			imgclass: "cornerbottomleft",
			imgsrc: imgpath+"bottomleft.png"
		},{
			imgclass: "cornerbottomright",
			imgsrc: imgpath+"bottomright.png"
		}
		]

	},
	{
		faceid: "face3",
		faceposition: "right",
		facenumber: 3,
		associatedimage: [
		{
			imgclass: "cornertopleft",
			imgsrc: imgpath+"topleft.png"
		},{
			imgclass: "cornertopright",
			imgsrc: imgpath+"topright.png"
		},{
			imgclass: "cornerbottomleft",
			imgsrc: imgpath+"bottomleft.png"
		},{
			imgclass: "cornerbottomright",
			imgsrc: imgpath+"bottomright.png"
		}
		]

	}
	]
},
{
	contentblockadditionalclass : "mainTitle",

	uppertextblock : [{
		textclass : "introductionheader",
		textdata : data.string.p5_title_cube
	},{
		textclass : "question",
		datahighlightflag : true,
		textdata : data.string.p5_s2,
	}],
	imageblockadditionalclass : "shapequestion",
	cuboidfaces: [
	{
		faceid: "face1",
		faceposition: "front",
		facenumber: 1,
		associatedimage: [
		{
			imgclass: "cornertopleft",
			imgsrc: imgpath+"topleft.png"
		},{
			imgclass: "cornertopright",
			imgsrc: imgpath+"topright.png"
		},{
			imgclass: "cornerbottomleft",
			imgsrc: imgpath+"bottomleft.png"
		},{
			imgclass: "cornerbottomright",
			imgsrc: imgpath+"bottomright.png"
		}
		]

	},
	{
		faceid: "face6",
		faceposition: "back",
		facenumber: 6,
		associatedimage: [
		{
			imgclass: "cornertopleft",
			imgsrc: imgpath+"topleft.png"
		},{
			imgclass: "cornertopright",
			imgsrc: imgpath+"topright.png"
		},{
			imgclass: "cornerbottomleft",
			imgsrc: imgpath+"bottomleft.png"
		},{
			imgclass: "cornerbottomright",
			imgsrc: imgpath+"bottomright.png"
		}
		]

	},
	{
		faceid: "face5",
		faceposition: "top",
		facenumber: 5,
		associatedimage: [
		{
			imgclass: "cornertopleft",
			imgsrc: imgpath+"topleft.png"
		},{
			imgclass: "cornertopright",
			imgsrc: imgpath+"topright.png"
		},{
			imgclass: "cornerbottomleft",
			imgsrc: imgpath+"bottomleft.png"
		},{
			imgclass: "cornerbottomright",
			imgsrc: imgpath+"bottomright.png"
		}
		]

	},
	{
		faceid: "face2",
		faceposition: "bottom",
		facenumber: 2,
		associatedimage: [
		{
			imgclass: "cornertopleft",
			imgsrc: imgpath+"topleft.png"
		},{
			imgclass: "cornertopright",
			imgsrc: imgpath+"topright.png"
		},{
			imgclass: "cornerbottomleft",
			imgsrc: imgpath+"bottomleft.png"
		},{
			imgclass: "cornerbottomright",
			imgsrc: imgpath+"bottomright.png"
		}
		]

	},
	{
		faceid: "face4",
		faceposition: "left",
		facenumber: 4,
		associatedimage: [
		{
			imgclass: "cornertopleft",
			imgsrc: imgpath+"topleft.png"
		},{
			imgclass: "cornertopright",
			imgsrc: imgpath+"topright.png"
		},{
			imgclass: "cornerbottomleft",
			imgsrc: imgpath+"bottomleft.png"
		},{
			imgclass: "cornerbottomright",
			imgsrc: imgpath+"bottomright.png"
		}
		]

	},
	{
		faceid: "face3",
		faceposition: "right",
		facenumber: 3,
		associatedimage: [
		{
			imgclass: "cornertopleft",
			imgsrc: imgpath+"topleft.png"
		},{
			imgclass: "cornertopright",
			imgsrc: imgpath+"topright.png"
		},{
			imgclass: "cornerbottomleft",
			imgsrc: imgpath+"bottomleft.png"
		},{
			imgclass: "cornerbottomright",
			imgsrc: imgpath+"bottomright.png"
		}
		]

	}
	],

	lowertextblock: [{
		textclass : "surfaces surfaces1",
		textdata : data.string.p5_s3,
	},{
		textclass : "counter",
		textdata : data.string.p5_s6,
	}]
},
{
	contentblockadditionalclass : "mainTitle",

	uppertextblock : [{
		textclass : "introductionheader",
		textdata : data.string.p5_title_cube
	},{
		textclass : "question",
		datahighlightflag : true,
		textdata : data.string.p5_s2,
	}],
	imageblockadditionalclass : "shapequestion",
	cuboidfaces: [
	{
		faceid: "face1",
		faceposition: "front",
		facenumber: 1,
		associatedimage: [
		{
			imgclass: "cornertopleft",
			imgsrc: imgpath+"topleft.png"
		},{
			imgclass: "cornertopright",
			imgsrc: imgpath+"topright.png"
		},{
			imgclass: "cornerbottomleft",
			imgsrc: imgpath+"bottomleft.png"
		},{
			imgclass: "cornerbottomright",
			imgsrc: imgpath+"bottomright.png"
		}
		]

	},
	{
		faceid: "face6",
		faceposition: "back",
		facenumber: 6,
		associatedimage: [
		{
			imgclass: "cornertopleft",
			imgsrc: imgpath+"topleft.png"
		},{
			imgclass: "cornertopright",
			imgsrc: imgpath+"topright.png"
		},{
			imgclass: "cornerbottomleft",
			imgsrc: imgpath+"bottomleft.png"
		},{
			imgclass: "cornerbottomright",
			imgsrc: imgpath+"bottomright.png"
		}
		]

	},
	{
		faceid: "face5",
		faceposition: "top",
		facenumber: 5,
		associatedimage: [
		{
			imgclass: "cornertopleft",
			imgsrc: imgpath+"topleft.png"
		},{
			imgclass: "cornertopright",
			imgsrc: imgpath+"topright.png"
		},{
			imgclass: "cornerbottomleft",
			imgsrc: imgpath+"bottomleft.png"
		},{
			imgclass: "cornerbottomright",
			imgsrc: imgpath+"bottomright.png"
		}
		]

	},
	{
		faceid: "face2",
		faceposition: "bottom",
		facenumber: 2,
		associatedimage: [
		{
			imgclass: "cornertopleft",
			imgsrc: imgpath+"topleft.png"
		},{
			imgclass: "cornertopright",
			imgsrc: imgpath+"topright.png"
		},{
			imgclass: "cornerbottomleft",
			imgsrc: imgpath+"bottomleft.png"
		},{
			imgclass: "cornerbottomright",
			imgsrc: imgpath+"bottomright.png"
		}
		]

	},
	{
		faceid: "face4",
		faceposition: "left",
		facenumber: 4,
		associatedimage: [
		{
			imgclass: "cornertopleft",
			imgsrc: imgpath+"topleft.png"
		},{
			imgclass: "cornertopright",
			imgsrc: imgpath+"topright.png"
		},{
			imgclass: "cornerbottomleft",
			imgsrc: imgpath+"bottomleft.png"
		},{
			imgclass: "cornerbottomright",
			imgsrc: imgpath+"bottomright.png"
		}
		]

	},
	{
		faceid: "face3",
		faceposition: "right",
		facenumber: 3,
		associatedimage: [
		{
			imgclass: "cornertopleft",
			imgsrc: imgpath+"topleft.png"
		},{
			imgclass: "cornertopright",
			imgsrc: imgpath+"topright.png"
		},{
			imgclass: "cornerbottomleft",
			imgsrc: imgpath+"bottomleft.png"
		},{
			imgclass: "cornerbottomright",
			imgsrc: imgpath+"bottomright.png"
		}
		]

	}
	],

	lowertextblock: [{
		textclass : "surfaces surfaces1",
		textdata : data.string.p5_s3,
	},{
		textclass : "edges edges1",
		textdata : data.string.p5_s4,
	},{
		textclass : "counter",
		textdata : data.string.p5_s6,
	}]
},
{
	contentblockadditionalclass : "mainTitle",

	uppertextblock : [{
		textclass : "introductionheader",
		textdata : data.string.p5_title_cube
	},{
		textclass : "question",
		datahighlightflag : true,
		textdata : data.string.p5_s2,
	}],
	imageblockadditionalclass : "shapequestion",
	cuboidfaces: [
	{
		faceid: "face1",
		faceposition: "front",
		facenumber: 1,
		associatedimage: [
		{
			imgclass: "cornertopleft",
			imgsrc: imgpath+"topleft.png"
		},{
			imgclass: "cornertopright",
			imgsrc: imgpath+"topright.png"
		},{
			imgclass: "cornerbottomleft",
			imgsrc: imgpath+"bottomleft.png"
		},{
			imgclass: "cornerbottomright",
			imgsrc: imgpath+"bottomright.png"
		}
		]

	},
	{
		faceid: "face6",
		faceposition: "back",
		facenumber: 6,
		associatedimage: [
		{
			imgclass: "cornertopleft",
			imgsrc: imgpath+"topleft.png"
		},{
			imgclass: "cornertopright",
			imgsrc: imgpath+"topright.png"
		},{
			imgclass: "cornerbottomleft",
			imgsrc: imgpath+"bottomleft.png"
		},{
			imgclass: "cornerbottomright",
			imgsrc: imgpath+"bottomright.png"
		}
		]

	},
	{
		faceid: "face5",
		faceposition: "top",
		facenumber: 5,
		associatedimage: [
		{
			imgclass: "cornertopleft",
			imgsrc: imgpath+"topleft.png"
		},{
			imgclass: "cornertopright",
			imgsrc: imgpath+"topright.png"
		},{
			imgclass: "cornerbottomleft",
			imgsrc: imgpath+"bottomleft.png"
		},{
			imgclass: "cornerbottomright",
			imgsrc: imgpath+"bottomright.png"
		}
		]

	},
	{
		faceid: "face2",
		faceposition: "bottom",
		facenumber: 2,
		associatedimage: [
		{
			imgclass: "cornertopleft",
			imgsrc: imgpath+"topleft.png"
		},{
			imgclass: "cornertopright",
			imgsrc: imgpath+"topright.png"
		},{
			imgclass: "cornerbottomleft",
			imgsrc: imgpath+"bottomleft.png"
		},{
			imgclass: "cornerbottomright",
			imgsrc: imgpath+"bottomright.png"
		}
		]

	},
	{
		faceid: "face4",
		faceposition: "left",
		facenumber: 4,
		associatedimage: [
		{
			imgclass: "cornertopleft",
			imgsrc: imgpath+"topleft.png"
		},{
			imgclass: "cornertopright",
			imgsrc: imgpath+"topright.png"
		},{
			imgclass: "cornerbottomleft",
			imgsrc: imgpath+"bottomleft.png"
		},{
			imgclass: "cornerbottomright",
			imgsrc: imgpath+"bottomright.png"
		}
		]

	},
	{
		faceid: "face3",
		faceposition: "right",
		facenumber: 3,
		associatedimage: [
		{
			imgclass: "cornertopleft",
			imgsrc: imgpath+"topleft.png"
		},{
			imgclass: "cornertopright",
			imgsrc: imgpath+"topright.png"
		},{
			imgclass: "cornerbottomleft",
			imgsrc: imgpath+"bottomleft.png"
		},{
			imgclass: "cornerbottomright",
			imgsrc: imgpath+"bottomright.png"
		}
		]

	}
	],

	lowertextblock: [{
		textclass : "surfaces surfaces1",
		textdata : data.string.p5_s3,
	},{
		textclass : "edges edges1",
		textdata : data.string.p5_s4,
	},{
		textclass : "vertices vertices1",
		textdata : data.string.p5_s5,
	},{
		textclass : "counter",
		textdata : data.string.p5_s6,
	}]
}
];

$(function() {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);

	var cantconsumeclick = false;
	/*==================================================
	 =            Handlers and helpers Block            =
	 ==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	/*=====  End of data highlight function  ======*/

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templateCaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templateCaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			if(countNext == 1)
			 	$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
	/**
	 How to:
	 - Just call instructionblockcontroller() from the template
	 */

	/**
	 What it does:
	 - It inserts and handles closing and opening of instruction block
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}


	/*cubemovement movement controller*/

	function cuboidController() {
		var dragging = false;
		var rotationXprev = 0;//rotation from previous transform
		var rotationYprev = 0;//rotation from previous transform

		var degreechangeX = 0;//x degree change in current rotation
		var degreechangeY = 0;//y degree change in current rotation

		var mouse_0x = 0;//x-coordinate at mouse down
		var mouse_0y = 0;//y-coordinate ar mouse down
		var additionCompleted = false;
		var target = $('.a3dbox');
		var halfheight_width_for_center = target.width() / 2;
		var offset = target.offset();

		var rotator;
		var ygoingupto30 = true;
		var infiniteRotation = {
			start: function(){
				rotator = setInterval(function(){
					if(ygoingupto30){
						if (rotationYprev >= 25){
							ygoingupto30 = false;
						}
						(((25 - rotationYprev) > 0)? rotationYprev += 5 : rotationYprev -= 5);
					}else{
						(rotationYprev <= -25)? ygoingupto30 = true : rotationYprev -= 5;
					}

					rotationXprev += 5;

					// console.log(rotationYprev +" : x,   "+rotationXprev+"   :y !!!");
					target.css({
						'-moz-transform': 'translate(-50%, -50%) rotateX(' + rotationYprev + 'deg) rotateY(' + rotationXprev + 'deg)',
						'-moz-transform-origin': '50% 50%',
						'-moz-transition': '-moz-transform 1.5s linear',
						'-webkit-transform': 'translate(-50%, -50%) rotateX(' + rotationYprev + 'deg) rotateY(' + rotationXprev + 'deg)',
						'-webkit-transform-origin': '50% 50%',
						'-webkit-transition': '-webkit-transform 1.5s linear'
						});
				}, 1520);
			},
			stop: function(){
				clearInterval(rotator);
			}
		};
		infiniteRotation.stop();
		infiniteRotation.start();
		var clickedontarget = false;
		target.mousedown(function(e) {
			dragging = true;
			clickedontarget = true;
			mouse_0x = e.pageX;
			mouse_0y = e.pageY;
			infiniteRotation.stop();
		});

		$(document).mouseup(function() {
			if (dragging) {
				rotationXprev = degreechangeX;
				rotationYprev = degreechangeY;
			}
			degreechangeX = 0;
			degreechangeY = 0;
			dragging = false;
			additionCompleted = false;
			if(clickedontarget){
				setTimeout(function(){
					if(!dragging){
						infiniteRotation.start();
					}
				}, 1000);
				clickedontarget= false;
			}
			ygoingupto30 = false;
		});

		$(document).mousemove(function(e) {
			e.preventDefault();
			if (dragging) {

				var mouse_x = e.pageX;
				var mouse_y = e.pageY;

				if (Math.abs(mouse_0x - mouse_x) > 0.5) {
					((mouse_0x - mouse_x) < 0) ? ++degreechangeX : --degreechangeX;
					mouse_0x = mouse_x;
				}
				if (Math.abs(mouse_0y - mouse_y) > 0.5) {
					((mouse_0y - mouse_y) > 0) ? ++degreechangeY : --degreechangeY;
					mouse_0y = mouse_y;
				}
				if (rotationXprev != 0 && rotationYprev != 0 && !additionCompleted) {
					degreechangeX += rotationXprev;
					degreechangeY += rotationYprev;
					additionCompleted = true;
				}

				degreechangeY %= 360;
				degreechangeX %= 360;

				target.css({
					'-moz-transform': 'translate(-50%, -50%) rotateX(' + degreechangeY + 'deg) rotateY(' + degreechangeX + 'deg)',
					'-moz-transform-origin': '50% 50%',
					'-moz-transition': '',
					'-webkit-transform': 'translate(-50%, -50%) rotateX(' + degreechangeY + 'deg) rotateY(' + degreechangeX + 'deg)',
					'-webkit-transform-origin': '50% 50%',
					'-webkit-transition': ''
				});
			}
		});
	}

	/* cuboid movement controller end */

	/*=====  End of InstructionBlockController  ======*/

	/*=====  End of Handlers and helpers Block  ======*/

	/*=======================================
	 =            Templates Block            =
	 =======================================*/
	/*=================================================
	 =            general template function            =
	 =================================================*/
	var source = $("#general-template").html();
	var template = Handlebars.compile(source);
	function generalTemplate() {

		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//call instruction block controller
		instructionblockcontroller($board);

		//call notifyuser
		// notifyuser($anydiv);

		// find if there is linehorizontal div in the slide
		var $linehorizontal = $board.find("div.linehorizontal");
		if ($linehorizontal.length > 0) {
			$linehorizontal.attr('data-isdrawn', 'draw');
		}

		switch(countNext){
			case 0:
				soundplayer(sound_1);
			break;
			case 1:
				soundplayer(sound_2);
			break;
			case 2:
				soundplayertwo(sound_3,sound_4,1);
			break;
			case 3:
				soundplayer(sound_5,1);
			break;
			case 4:
				soundplayer(sound_6,1);
			break;
		}

		if(countNext != 0)
			cuboidController();

		if(countNext == 2 || countNext == 3 || countNext == 4){
			var counter = 1;

			$(".surfaces").click(function(){
				// counter = 1;
				if (cantconsumeclick)
					return false;
				cantconsumeclick = true;
				$(this).toggleClass("selected");
				$(this).toggleClass("surfaces1");
				$(".counter").html(counter);
				$("#face"+counter).css('background', 'rgba(245, 209, 81, 0.5)');
				// counter++;
				var surfaces = setInterval(function(){
					$(".counter").html(counter);
					$("#face"+counter).css('background', 'rgba(245, 209, 81, 0.5)');
					if(counter == 6){
						clearInterval(surfaces);
						setTimeout(function(){
							counter = 1;
							$(".a3dface").css('background', 'rgba(0, 209, 195, 0.5)');
							$(".counter").html(0);
							$(".surfaces").toggleClass("selected");
							$(".surfaces").toggleClass("surfaces1");
							if(countNext == 2)
								$nextBtn.show(1000);
							cantconsumeclick = false;
						}, 5000);
					}
					counter++;
				}, 2000);
			});

			$(".edges").click(function(){

				if (cantconsumeclick)
					return false;
				cantconsumeclick = true;
				$("#face1").css('border-top', '#F9CF4E solid 5px');
				$("#face5").css('border-bottom', '#F9CF4E solid 5px');
				$(this).toggleClass("selected");
				$(this).toggleClass("edges1");
				$(".counter").html(counter);
				var edges = setInterval(function() {
					$(".counter").html(counter);
					switch(counter) {
					case 2:
						$("#face2").css('border-top', '#F9CF4E solid 5px');
						$("#face1").css('border-bottom', '#F9CF4E solid 5px');
						break;
					case 3:
						$("#face5").css('border-top', '#F9CF4E solid 5px');
						$("#face6").css('border-top', '#F9CF4E solid 5px');
						break;
					case 4:
						$("#face2").css('border-bottom', '#F9CF4E solid 5px');
						$("#face6").css('border-bottom', '#F9CF4E solid 5px');
						break;
					case 5:
						$("#face1").css('border-left', '#F9CF4E solid 5px');
						$("#face3").css('border-right', '#F9CF4E solid 5px');
						break;
					case 6:
						$("#face1").css('border-right', '#F9CF4E solid 5px');
						$("#face4").css('border-left', '#F9CF4E solid 5px');
						break;
					case 7:
						$("#face4").css('border-right', '#F9CF4E solid 5px');
						$("#face6").css('border-left', '#F9CF4E solid 5px');
						break;
					case 8:
						$("#face6").css('border-right', '#F9CF4E solid 5px');
						$("#face3").css('border-left', '#F9CF4E solid 5px');
						break;
					case 9:
						$("#face2").css('border-right', '#F9CF4E solid 5px');
						$("#face4").css('border-bottom', '#F9CF4E solid 5px');
						break;
					case 10:
						$("#face3").css('border-top', '#F9CF4E solid 5px');
						$("#face5").css('border-left', '#F9CF4E solid 5px');
						break;
					case 11:
						$("#face4").css('border-top', '#F9CF4E solid 5px');
						$("#face5").css('border-right', '#F9CF4E solid 5px');
						break;
					case 12:
						$("#face2").css('border-left', '#F9CF4E solid 5px');
						$("#face3").css('border-bottom', '#F9CF4E solid 5px');
						break;
					default:
						break;
					}
					if(counter == 12){
						clearInterval(edges);
						// $(".edges").css('background', '#66d9ff');
						setTimeout(function(){
							counter = 1;
							$(".a3dface").css('border', '3px solid #00B398');
							$(".counter").html(0);
							$(".edges").toggleClass("selected");
							$(".edges").toggleClass("edges1");
							if(countNext == 3)
								$nextBtn.show(1000);
							cantconsumeclick = false;
						}, 5000);
					}
					counter++;
				}, 2000);
			});

			$(".vertices").click(function(){

				if (cantconsumeclick)
					return false;
				cantconsumeclick = true;
				$("#face1> .cornertopleft").show(0);
				$("#face3> .cornertopright").show(0);
				$("#face5> .cornerbottomleft").show(0);
				$(this).toggleClass("selected");
				$(this).toggleClass("vertices1");
// 				cornerbottomright
				$(".counter").html(counter);
				var vertices = setInterval(function(){
					counter++;
					$(".counter").html(counter);
					switch(counter) {
					case 2:
						$("#face1> .cornerbottomleft").show(0);
						$("#face3> .cornerbottomright").show(0);
						$("#face2> .cornertopleft").show(0);
						break;
					case 3:
						$("#face1> .cornertopright").show(0);
						$("#face4> .cornertopleft").show(0);
						$("#face5> .cornerbottomright").show(0);
						break;
					case 4:
						$("#face1> .cornerbottomright").show(0);
						$("#face4> .cornerbottomleft").show(0);
						$("#face2> .cornertopright").show(0);
						break;
					case 5:
						$("#face4> .cornertopright").show(0);
						$("#face6> .cornertopleft").show(0);
						$("#face5> .cornertopright").show(0);
						break;
					case 6:
						$("#face4> .cornerbottomright").show(0);
						$("#face6> .cornerbottomleft").show(0);
						$("#face2> .cornerbottomright").show(0);
						break;
					case 7:
						$("#face5> .cornertopleft").show(0);
						$("#face6> .cornertopright").show(0);
						$("#face3> .cornertopleft").show(0);
						break;
					case 8:
						$("#face2> .cornerbottomleft").show(0);
						$("#face3> .cornerbottomleft").show(0);
						$("#face6> .cornerbottomright").show(0);
						break;
					default:
						break;
					}
					if(counter == 8){
						counter = 1;
						clearInterval(vertices);
						setTimeout(function(){
							$(".counter").html(0);
							$(".cornertopleft, .cornertopright, .cornerbottomleft, .cornerbottomright").hide(0);
							ole.footerNotificationHandler.lessonEndSetNotification();
							$(".vertices").toggleClass("selected");
							$(".vertices").toggleClass("vertices1");
							cantconsumeclick = false;
						}, 5000);
					}
				}, 2000);
			});
		}
	}

	/*=====  End of Templates Block  ======*/

	/*==================================================
	=            Templates Controller Block            =
	==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
	 Motivation :
	 - Make a single function call that handles all the
	 template load easier

	 How To:
	 - Update the template caller with the required templates
	 - Call template caller

	 What it does:
	 - According to value of the Global Variable countNext
	 the slide templates are updated
	 */
	 function soundplayer(i,next){
     buzz.all().stop();
     i.play().bind("ended",function(){
        if(!next) navigationcontroller();
     });
   }

	  function soundplayertwo(i, j, next){
	    buzz.all().stop();
	    i.play().bind("ended",()=>{
	      j.play().bind("ended",()=>{
	         if(!next) navigationcontroller();
	      });
	    });
	  }

	function templateCaller() {
		/*always hide next and previous navigation button unless
		 explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		if(countNext == 2 || countNext == 3 || countNext == 4){
			$(".board").empty();
		}

		// call navigation controller
		// navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page, countNext + 1);

		// just for development purpose to see total slide vs current slide number
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click', function() {
		if(cantconsumeclick)
			return false;
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		if(cantconsumeclick)
			return false;
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/*=====  End of Templates Controller Block  ======*/

});

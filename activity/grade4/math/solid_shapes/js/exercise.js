Array.prototype.shufflearray = function(){
  var i = this.length, j, temp;
	    while(--i > 0){
	        j = Math.floor(Math.random() * (i+1));
	        temp = this[j];
	        this[j] = this[i];
	        this[i] = temp;
	    }
	    return this;
};

var imgpath = $ref+"/images/";

var soundAsset = $ref+"/sounds/"+$lang+'/';

var sound_1 = new buzz.sound(soundAsset+"exe_ins.ogg");

var content=[
	{
		contentnocenteradjust: true,
		uppertextblockadditionalclass:"addn_utb",
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.e1_s1
		}],
		flextablecolumn:[{
			flextablecolumnclass: "flexcolumn1",
			flextablerow:[{
				optionclass: "description",
				rowdivext: "header",
				textdata: data.string.e1_s2
			},{
				imgclass: "imageclass1",
				imgsrc: imgpath+"cuboid.png"
			},{
				imgclass: "imageclass2",
				imgsrc: imgpath+"cube.png"
			},{
				imgclass: "imageclass3",
				imgsrc: imgpath+"cylinder_exe.png"
			},{
				imgclass: "imageclass4",
				imgsrc: imgpath+"pearls.png"
			}]
		},{
			flextablecolumnclass: "flexcolumn2",
			flextablerow:[{
				optionclass: "description",
				rowdivext: "header",
				textdata: data.string.e1_s3
			},{
				inputclass: "surface1",
				dataanswerinput: "6",
				inputtype: "text"
			},{
				inputclass: "surface2",
				dataanswerinput: "6",
				inputtype: "text"
			},{
				inputclass: "surface3",
				dataanswerinput: "3",
				inputtype: "text"
			},{
				inputclass: "surface4",
				dataanswerinput: "1",
				inputtype: "text"
			}]
		},{
			flextablecolumnclass: "flexcolumn3",
			flextablerow:[{
				optionclass: "description",
				rowdivext: "header",
				textdata: data.string.e1_s4
			},{
				inputclass: "edge1",
				dataanswerinput: "12",
				inputtype: "text"
			},{
				inputclass: "edge2",
				dataanswerinput: "12",
				inputtype: "text"
			},{
				inputclass: "edge3",
				dataanswerinput: "2",
				inputtype: "text"
			},{
				inputclass: "edge4",
				dataanswerinput: "0",
				inputtype: "text"
			}]
		},{
			flextablecolumnclass: "flexcolumn4",
			flextablerow:[{
				optionclass: "description",
				rowdivext: "header",
				textdata: data.string.e1_s5
			},{
				inputclass: "vertices1",
				dataanswerinput: "8",
				inputtype: "text"
			},{
				inputclass: "vertices2",
				dataanswerinput: "8",
				inputtype: "text"
			},{
				inputclass: "vertices3",
				dataanswerinput: "0",
				inputtype: "text"
			},{
				inputclass: "vertices4",
				dataanswerinput: "0",
				inputtype: "text"
			}]
		},{
			flextablecolumnclass: "flexcolumn5",
			flextablerow:[{
				optionclass: "description",
				rowdivext: "header",
				textdata: data.string.e1_s6
			},{
				selectid: "select1",
				dataanswerdrop: data.string.e1_s7,
				dropdown: [{
					optionclass: "option",
					optionname: ""
				},{
					optionclass: "option",
					optionname: data.string.e1_s7
				},{
					optionclass: "option",
					optionname: data.string.e1_s8
				},{
					optionclass: "option",
					optionname: data.string.e1_s9
				}]
			},{
				selectid: "select2",
				dataanswerdrop: data.string.e1_s8,
				dropdown: [{
					optionclass: "option",
					optionname: ""
				},{
					optionclass: "option",
					optionname: data.string.e1_s7
				},{
					optionclass: "option",
					optionname: data.string.e1_s8
				},{
					optionclass: "option",
					optionname: data.string.e1_s9
				}]
			},{
				selectid: "select3",
				dataanswerdrop: data.string.e1_s9,
				dropdown: [{
					optionclass: "option",
					optionname: ""
				},{
					optionclass: "option",
					optionname: data.string.e1_s7
				},{
					optionclass: "option",
					optionname: data.string.e1_s8
				},{
					optionclass: "option",
					optionname: data.string.e1_s9
				}]
			},{
				selectid: "select4",
				dataanswerdrop: data.string.e1_s9,
				dropdown: [{
					optionclass: "option",
					optionname: ""
				},{
					optionclass: "option",
					optionname: data.string.e1_s7
				},{
					optionclass: "option",
					optionname: data.string.e1_s8
				},{
					optionclass: "option",
					optionname: data.string.e1_s9
				}]
			}]
		}]
	}
];


/*remove this for non random questions*/
/*content.shufflearray();*/


$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	/*==================================================
	 =            Handlers and helpers Block            =
	 ==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("flexblocktable", $("#flexblocktable-partial").html());

	/*===============================================
	=            data highlight function            =
	===============================================*/

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			//islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			// $prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			// $prevBtn.show(0);

			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }

	var score = 0;
	var testin = new EggTemplate();

	testin.init(10);
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		var $flextableblock  = $(".flextableblock ");
		var ansClicked = false;
		var wrngClicked = false;
    if(countNext==0) soundplayer(sound_1);
		var $correctanswers;
		 $board.find("input[type = 'text']").each(function(index, value) {
			$(value).keydown(function(evt) {
				$nextBtn.hide(0);
				var className2 = $(this).attr('class');
				var className = evt.target.className;
				console.log(className2);
				var charVal = parseInt(evt.key);
				var prevValue = parseInt(evt.target.value) * 10 + charVal;
				var charCode = (evt.which) ? evt.which : evt.keyCode;
				if(charCode == 13){
						 $('.'+className).blur();
				}
				if ((charCode > 31 && (charCode < 48 || charCode > 57)) && isNaN(charVal)) {
					// console.log("inside");
					// console.log(evt.target.value);
					return false;
				}

				if (prevValue > 999) {
					return false;
				}
				// if(isNaN(prevValue)){
				// prevValue = charVal;
				// }else{
				// prevValue += charVal;
				// }
				// var returnvalue = prevValue+"cm&sup3;";
				// evt.target.value = returnvalue;
				// return evt.preventDefault();
				return true;
			}).blur(function(evt){
        // alert("hree");
				var prevValue = parseInt(evt.target.value);
				var $this = $(this);
				var answer = parseInt($this.data("answer"));
				if(isNaN(prevValue)){
					$this[0].dataset.correct = 0;
					$this.css("background-color", "#FFF");
				}else if( answer == prevValue){
					$this.css("background-color", "#4CAF50");
					$this.css("color", "white");
					if(!$this.data("answerflag")){
						countNext++;
             			testin.update(true);
     					testin.gotoNext();
                play_correct_incorrect_sound(1);
						$this.data("answerflag", true);

					}
            play_correct_incorrect_sound(1);
					$this[0].dataset.correct = 1;
					$correctanswers = $flextableblock.find("*[data-correct='1']");
					if($correctanswers.length == 16){
						$nextBtn.show(0);
					}
				}else{
					$this[0].dataset.correct = 0;
					$this.css("background-color", "#D0553E");
					$this.css("color", "white");
					if(!$this.data("answerflag")){
            play_correct_incorrect_sound(0);
						countNext++;
             			testin.update(false);
     					testin.gotoNext();
						$this.data("answerflag", true);
					}
            play_correct_incorrect_sound(0);
					$nextBtn.hide(0);
				}
			});


	    });

		$board.find("select").each(function(index, value) {
			$(value).focus(function(){
				$(this).keydown(function(evt){
					var charCode = (evt.which) ? evt.which : evt.keyCode;
					if(charCode == 13){
						// $("input").next().focus();
						$this.trigger( "focusout" );
					}
					if(charCode == 40 || charCode == 39 || charCode == 38 || charCode == 37){
						evt.preventDefault();
						// $(this).trigger('click');
						$(this).select();
						// $(this).attr('size',4);
						 // $(this).next().focus();

						return true;
					}

				});
			});

			$(value).change(function(evt){
				var $this = $(this);
				var answer = $this.data("answer");
				var val = $this.find(":selected").text();
				if(val == ""){
					$this[0].dataset.correct = 0;
					$this.css("background-color", "#FFF");
				}else if(val === answer){
					$this.css("background-color", "#4CAF50");
					$this.css("color", "white");
					if(!$this.data("answerflag")){
						countNext++;
             			testin.update(true);
                    play_correct_incorrect_sound(1);
     					testin.gotoNext();
						$this.data("answerflag", true);

					}
					$this[0].dataset.correct = 1;
					$correctanswers = $flextableblock.find("*[data-correct='1']");
					if($correctanswers.length == 16){
						$nextBtn.show(0);
					}
				}else{
					$this[0].dataset.correct = 0;
					$this.css("background-color", "#D0553E");
					$this.css("color", "white");
					if(!$this.data("answerflag")){
						countNext++;
              play_correct_incorrect_sound(0);
             			testin.update(false);
     					testin.gotoNext();
						$this.data("answerflag", true);
					}
					$nextBtn.hide(0);
				}
			}).blur(function(){
				// openflag = false;
			});
		});


		if(countNext == 0){
				var rand_row;
				var rand_col;
				var rowval;
				var i = 0;

				while(i<6){
					rand_row = Math.round(Math.random()*3+1);
					rand_col = Math.round(Math.random()*3+1);
					var $class;
					switch(rand_col){
						case 1:
							$class = $(".surface"+rand_row);
							if($class[0].dataset.correct == 0){
								$class.val($class.data("answer"));
								$class[0].dataset.correct = 1;
								$class.prop('disabled', true);
								i++;
							}
							break;
						case 2:
							$class = $(".edge"+rand_row);
							if($class[0].dataset.correct == 0){
								$class.val($class.data("answer"));
								$class[0].dataset.correct = 1;
								$class.prop('disabled', true);
								i++;
							}
							break;
						case 3:
							$class = $(".vertices"+rand_row);
							if($class[0].dataset.correct == 0){
								$class.val($class.data("answer"));
								// $(".vertices"+rand_row).data("correct", 1);
								$class[0].dataset.correct = 1;
								$class.prop('disabled', true);
								i++;
							}
							break;
						case 4:
							$class = $("#select"+rand_row);
							if($class[0].dataset.correct == 0){
								$("#select"+rand_row+ " option:contains("+$class.data("answer")+")").prop('selected',true);
								$class[0].dataset.correct = 1;
								$class.attr("disabled", "disabled");
								i++;
							}
							break;
						default:
							break;
					}
						$class.css("background-color", "#4CAF50");
						$class.css("color", "white");
				}
		}
	}

  function soundplayer(i,next){
    buzz.all().stop();
    i.play().bind("ended",function(){
       if(!next) navigationcontroller();
    });
  }


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/



	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		if(countNext == 11){
			$nextBtn.hide(0);
				$('#score').html(score);
				$('[select=yes]').fadeTo(1000,0).hide(0);
				$('.exefin').show(0);
				$('.contentblock').hide(0);
				$('.congratulation').show(0);
		}else{
			countNext++;
			templateCaller();
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext = 0;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

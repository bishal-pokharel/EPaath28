var imgpath = $ref + "/images/";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var soundAsset = $ref+"/sounds/"+$lang+'/';

var sound_1 = new buzz.sound(soundAsset+"s1_p1.ogg");
var sound_2 = new buzz.sound(soundAsset+"s1_p2.ogg");
var sound_3 = new buzz.sound(soundAsset+"s1_p3.ogg");
var sound_3_1 = new buzz.sound(soundAsset+"s1_p3_2.ogg");
var sound_4 = new buzz.sound(soundAsset+"s1_p4_1.ogg");
var sound_5 = new buzz.sound(soundAsset+"s1_p4_2.ogg");
var sound_6 = new buzz.sound(soundAsset+"s1_p4_3.ogg");
var sound_7 = new buzz.sound(soundAsset+"s1_p4_4.ogg");
var sound_8 = new buzz.sound(soundAsset+"s1_p5.ogg");
var sound_9 = new buzz.sound(soundAsset+"s1_p7.ogg");
var sound_10 = new buzz.sound(soundAsset+"s1_p8_1_book.ogg");
var sound_11 = new buzz.sound(soundAsset+"s1_p8_1_bricks.ogg");
var sound_12 = new buzz.sound(soundAsset+"s1_p8_1_cupboard.ogg");
var sound_13 = new buzz.sound(soundAsset+"s1_p8_1_geometrybox.ogg");
var sound_14 = new buzz.sound(soundAsset+"s1_p8_1_giftbox.ogg");
var sound_15 = new buzz.sound(soundAsset+"s1_p8_2.ogg");

// var csv_as_array = [];
// var jsonobject;
// var wordindex = [];
//
// function readCSV() {
    // console.log("eunnng");
    // $.ajax({
        // url: $ref + "/libsvocab/data.csv",
        // async: true,
        // success: function(csvdata) {
            // csv_as_array = $.csv.toObjects(csvdata);
        // },
        // dataType: "text",
        // complete: function() {
            // console.table(csv_as_array);
            // var myJson = JSON.stringify(csv_as_array);
            // jsonobject = JSON.parse(myJson);
            // var jsonobj;
            // for(var i=0; i<jsonobject.length; i++){
            	// jsonobj = jsonobject[i];
            	// wordindex.push(jsonobj.english_word.trim().toLowerCase());
            // }
        // }
    // });
// }


var content = [
  {
        //starting page
        contentblockadditionalclass: "firstpagebackground",
        contentblocknocenteradjust: true,
        uppertextblock: [{
            textclass: "firsttitle",
            textdata: data.string.firsttitle
        }]

  },
  {
        //page 1
        contentblockadditionalclass: "mainTitle",

        uppertextblock: [{
            textclass: "introductionheader",
            datahighlightflag: true,
            textdata: data.string.introduction
        }],
        imageblockadditionalclass: "shapequestion",
        imageblock: [{
            imagestoshow: [{
                imgclass: "rectangle",
                imgsrc: imgpath + "shape01.png"
            }, {
                imgclass: "square01",
                imgsrc: imgpath + "shape02.png"
            }, {
                imgclass: "circle01",
                imgsrc: imgpath + "shape03.png"
            }, {
                imgclass: "triangle01",
                imgsrc: imgpath + "shape05.png"
            }]
        }]
  },
  {
        //page 2
        contentblockadditionalclass: "mainTitle",

        filterdiv: true,
        filterdivclass: "purplefilterdiv",
        filtertextblock: [{
                textclass: "firstOption",
                datahighlightflag: true,
                textdata: data.string.p1_q1_option1
            }, {
                textclass: "secondOption",
                textdata: data.string.p1_q1_option2
            },
            {
                textclass: "thirdOption",
                datahighlightflag: true,
                textdata: data.string.p1_q1_option3
            }, {
                textclass: "question",
                textdata: data.string.p1_question,
            },
            {
                textclass: "answer",
                datahighlightflag: true,
                textdata: data.string.p1_q1_answer1,
            }
        ],
        filterimageblock: [{
            imagestoshow: [{
                imgclass: "arrow01",
                imgsrc: imgpath + "arrow01.png"
            }, {
                imgclass: "correct",
                imgsrc: "images/correct.png"
            }, {
                imgclass: "incorrect01",
                imgsrc: "images/wrong.png"
            }, {
                imgclass: "incorrect02",
                imgsrc: "images/wrong.png"
            }]
        }],
        uppertextblock: [{
            textclass: "introductionheader",
            datahighlightflag: true,
            textdata: data.string.introduction
        }],
        imageblockadditionalclass: "shapequestion",
        imageblock: [{
            imagestoshow: [{
                imgclass: "rectangle rectangle01purplefilter",
                imgsrc: imgpath + "shape01.png",
                answer: data.string.p1_q1_option1
            }, {
                imgclass: "square01",
                imgsrc: imgpath + "shape02.png"
            }, {
                imgclass: "circle01",
                imgsrc: imgpath + "shape03.png"
            }, {
                imgclass: "triangle01",
                imgsrc: imgpath + "shape05.png"
            }]
        }]
    },
  {
        //page 3
        contentblockadditionalclass: "mainTitle",

        filterdiv: true,
        filterdivclass: "purplefilterdiv",
        filtertextblock: [ {
                textclass: "boxquestion1",
                textdata: data.string.p1_q1_boxq1
            }, {
                textclass: "boxanswer1",
                textdata: data.string.p1_q1_boxa1
            }, {
                textclass: "boxquestion2",
                textdata: data.string.p1_q1_boxq2
            }, {
                textclass: "boxanswer2",
                textdata: data.string.p1_q1_boxa2
            }
            // ,{
            // textclass : "boxanswer3",
            // textdata : data.string.p1_q1_boxa3
            // }
        ],
        filterimageblock: [{
            imagestoshow: [{
                imgclass: "envelope",
                imgsrc: imgpath + "envelope.png"
            }, {
                imgclass: "picture-frame01",
                imgsrc: imgpath + "picture-frame01.png"
            }, {
                imgclass: "playing-card",
                imgsrc: imgpath + "playing-card.png"
            }, {
                imgclass: "picture-frame",
                imgsrc: imgpath + "picture-frame.png"
            }, {
                imgclass: "post-card",
                imgsrc: imgpath + "post-card.png"
            }, {
                imgclass: "box",
                imgsrc: imgpath + "box01.png"
            }, {
                imgclass: "sundar",
                imgsrc: imgpath + "sundar.png"
            }, {
                imgclass: "sundari",
                imgsrc: imgpath + "sundari.png"
            }, {
                imgclass: "dialog_sundar",
                imgsrc: imgpath + "narratordialog.png"
            }, {
                imgclass: "dialog_sundari",
                imgsrc: imgpath + "narratordialog.png"
            }, {
                imgclass: "dialog_sundari02",
                imgsrc: imgpath + "narratordialog.png"
            }]
        }],
        uppertextblock: [{
            textclass: "introductionheader",
            datahighlightflag: true,
            textdata: data.string.introduction
        }],
        imageblockadditionalclass: "shapequestion",
        imageblock: [{
            imagestoshow: [{
                imgclass: "rectangle rectangle01purplefilter rectangle_slideout",
                imgsrc: imgpath + "shape01.png",
                answer: data.string.p1_q1_option1
            }, {
                imgclass: "square01",
                imgsrc: imgpath + "shape02.png"
            }, {
                imgclass: "circle01",
                imgsrc: imgpath + "shape03.png"
            }, {
                imgclass: "triangle01",
                imgsrc: imgpath + "shape05.png"
            }]
        }]
  },
  {

        //page 4
        // hasheaderblock : true,
        // headerblock : [{
        // textclass : "introductionheader",
        // datahighlightflag : true,
        // textdata : data.string.introduction
        // }],
        contentblockadditionalclass : "bluebg",
        uppertextblock: [{
            textclass: "introductionheader",
            datahighlightflag: true,
            textdata: data.string.p1_q1_boxa3
        }],

        imageblockadditionalclass: "shapequestion",
        imageblock: [{
            imagestoshow: [{
                    imgclass: "rectangletocuboid",
                    imgsrc: imgpath + "rectangle.png"
                },
                {
                    imgclass: "cuboid",
                    imgsrc: imgpath + "cuboid.png"
                },
                {
                    imgclass: "recttocuboidarrow",
                    imgsrc: imgpath + "arrow03.png"
                }
            ],
            imagelabels: [{
                imagelabelclass: "cuboidlabel01",
                imagelabeldata: data.string.p1_q1_option1
            }, {
                imagelabelclass: "cuboidlabel03",
                imagelabeldata: data.string.p4_title_cuboid
            }]
        }]
    },
  {

        //page 4
        // hasheaderblock : true,
        // headerblock : [{
        // textclass : "introductionheader",
        // datahighlightflag : true,
        // textdata : data.string.introduction
        // }],
        contentblockadditionalclass: "bluebg",
        uppertextblock: [{
            textclass: "introductionheader",
            datahighlightflag: true,
            textdata: data.string.p1_q1_boxa3
        }],

        imageblockadditionalclass: "shapequestion",
        imageblock: [{
            imagestoshow: [{
                    imgclass: "rectangletocuboid",
                    imgsrc: imgpath + "rectangle.png"
                },
                {
                    imgclass: "cuboid",
                    imgsrc: imgpath + "cuboid.gif"
                },
                {
                    imgclass: "recttocuboidarrow",
                    imgsrc: imgpath + "arrow03.png"
                }
            ],
            imagelabels: [{
                imagelabelclass: "cuboidlabel01",
                imagelabeldata: data.string.p1_q1_option1
            }, {
                imagelabelclass: "cuboidlabel03",
                imagelabeldata: data.string.p4_title_cuboid
            }]
        }]
    },
  {

        //page 5
        contentblockadditionalclass: "bluebg",
        containsdialog: [{
            dialogcontainer: "dialogcontainer01",
            dialogimageclass: "dialog",
            dialogimagesrc: imgpath + "narratordialog.png",
            dialogcontentclass: "dialogcontent",
            dialogcontent: data.string.p1_q1_homer,
        }],
        imageblockadditionalclass: "shapequestion",
        imageblock: [{
            imagestoshow: [{
                imgclass: "cuboid",
                imgsrc: imgpath + "cuboid.png"
            }, {
                imgclass: "homer02",
                imgsrc: imgpath + "sundar.png"
            }],
            imagelabels: [{
                imagelabelclass: "cuboidlabel03",
                imagelabeldata: data.string.p4_title_cuboid
            }]
        }]
    },
  {

      //page 6
      contentblockadditionalclass: "bluebg",
      filterdiv: true,
      filterdivclass: "finalfilterdiv",
      filtertextblock: [{
          textclass: "canyouquestion",
          datahighlightflag: true,
          textdata: data.string.p1_can_you_question
      }],
      filterimageblock: [{
          imagestoshow: [{
              imgclass: "canyoucloud",
              imgsrc: imgpath + "cloud.png"
          }]
      }],
      imageblockadditionalclass: "shapequestion",
      imageblock: [{
          imagestoshow: [{
              imgclass: "cuboidleft",
              imgsrc: imgpath + "book.png"
          }, {
              imgclass: "cuboid01",
              imgsrc: imgpath + "gift-box.png"
          }, {
              imgclass: "cuboidright",
              imgsrc: imgpath + "cuboid.png"
          }, {
              imgclass: "cuboidtopleft",
              imgsrc: imgpath + "wooden-box01.png"
          }, {
              imgclass: "cuboidtopright",
              imgsrc: imgpath + "cornflakes.png"
          }, {
              imgclass: "cuboidbottom",
              imgsrc: imgpath + "brick.png"
          }],
          imagelabels: [{
              imagelabelclass: "cuboidlabel01",
              imagelabeldata: data.string.p1_example1
          }, {
              imagelabelclass: "cuboidlabel03",
              imagelabeldata: data.string.p1_example2
          }, {
              imagelabelclass: "cuboidlabel02",
              imagelabeldata: data.string.p1_example3
          }, {
              imagelabelclass: "cuboidlabel04",
              imagelabeldata: data.string.p1_example4
          }, {
              imagelabelclass: "cuboidlabel05",
              imagelabeldata: data.string.p1_example5
          }, {
              imagelabelclass: "cuboidlabel06",
              imagelabeldata: data.string.p1_example6
          }]
      }]
  }
];

var compcomplete;
$(function() {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;

    var $total_page = content.length;
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
    var boxquestionflag = false;
    var finalpageflag = false; // TODO: Use of this?
    var boxquestioncountnext = 0;
    loadTimelineProgress($total_page, countNext + 1);

    /*==================================================
     =            Handlers and helpers Block            =
     ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

    /*===============================================
    =            data highlight function            =
    ===============================================*/
    /**

     What it does:
     - send an element where the function has to see
     for data to highlight
     - this function searches for all nodes whose
     data-highlight element is set to true
     -searches for # character and gives a start tag
     ;span tag here, also for @ character and replaces with
     end tag of the respective
     - if provided with data-highlightcustomclass value for highlight it
     applies the custom class or else uses parsedstring class

     E.g: caller : texthighlight($board);
     */
    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) : (stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

	function voabunderline ($vocabinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) : (stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
	}

    /*=====  End of data highlight function  ======*/

    /*===============================================
    =            user notification function        =
    ===============================================*/
    /**
     How to:
     - First set any html element with
     "data-usernotification='notifyuser'" attribute,
     and "data-isclicked = ''".
     - Then call this function to give notification
     */

    /**
     What it does:
     - You send an element where the function has to see
     for data to notify user
     - this function searches for all text nodes whose
     data-usernotification attribute is set to notifyuser
     - applies event handler for each of the html element which
     removes the notification style.
     */
    function notifyuser($notifyinside) {
        //check if $notifyinside is provided
        typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

        /*variable that will store the element(s) to remove notification from*/
        var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
        // if there are any notifications removal required add the event handler
        if ($allnotifications.length > 0) {
            $allnotifications.one('click', function() {
                /* Act on the event */
                $(this).attr('data-isclicked', 'clicked');
                $(this).removeAttr('data-usernotification');
            });
        }
    }

    /*=====  End of user notification function  ======*/

    /*======================================================
    =            Navigation Controller Function            =
    ======================================================*/
    /**
     How To:
     - Just call the navigation controller if it is to be called from except the
     last page of lesson
     - If called from last page set the islastpageflag to true such that
     footernotification is called for continue button to navigate to exercise
     */

    /**
     What it does:
     - If not explicitly overriden the method for navigation button
     controls, it shows the navigation buttons as required,
     according to the total count of pages and the countNext variable
     - If for a general use it can be called from the templateCaller
     function
     - Can be put anywhere in the template function as per the need, if
     so should be taken out from the templateCaller function
     - If the total page number is
     */

    function navigationcontroller(islastpageflag) {
        typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
        if(countNext==7){
          ole.footerNotificationHandler.pageEndSetNotification();
        }
        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
            $prevBtn.css('display', 'none');
        } else if ($total_page == 1) {
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');

            // if lastpageflag is true
            islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
        } else if (countNext > 0 && countNext < $total_page - 1) {
            $nextBtn.show(0);
            $prevBtn.show(0);
        } else if (countNext == $total_page - 1) {
            $nextBtn.css('display', 'none');
            $prevBtn.show(0);

            // if lastpageflag is true
            // islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
        }
    }

    /*=====  End of user navigation controller function  ======*/

    /*==================================================
    =            InstructionBlockController            =
    ==================================================*/
    /**
     How to:
     - Just call instructionblockcontroller() from the template
     */

    /**
     What it does:
     - It inserts and handles closing and opening of instruction block
     - this function searches for all text nodes whose
     data-usernotification attribute is set to notifyuser
     - applies event handler for each of the html element which
     removes the notification style.
     */
    function instructionblockcontroller() {
        var $instructionblock = $board.find("div.instructionblock");
        if ($instructionblock.length > 0) {
            var $contentblock = $board.find("div.contentblock");
            var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
            var instructionblockisvisibleflag;

            $contentblock.css('pointer-events', 'none');

            $toggleinstructionblockbutton.on('click', function() {
                instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
                if (instructionblockisvisibleflag == 'true') {
                    instructionblockisvisibleflag = 'false';
                    $contentblock.css('pointer-events', 'auto');
                } else if (instructionblockisvisibleflag == 'false') {
                    instructionblockisvisibleflag = 'true';
                    $contentblock.css('pointer-events', 'none');
                }

                $instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
            });
        }
    }

    /*=====  End of InstructionBlockController  ======*/

    /*=====  End of Handlers and helpers Block  ======*/

    /*=======================================
     =            Templates Block            =
     =======================================*/
    /*=================================================
     =            general template function            =
     =================================================*/
    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        if (countNext < 8) {
            var html = template(content[countNext]);
            $board.html(html);
        }


        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);

        //call instruction block controller
        instructionblockcontroller($board);

        //call notifyuser
        // notifyuser($anydiv);

        // find if there is linehorizontal div in the slide
        var $linehorizontal = $board.find("div.linehorizontal");
        if ($linehorizontal.length > 0) {
            $linehorizontal.attr('data-isdrawn', 'draw');
        }
        vocabcontroller.findwords(countNext);



        switch (countNext) {
          case 0:
          soundplayer(sound_1);
          break;
            case 1:
            soundplayer(sound_2);
              break;
            case 2:
            soundplayer(sound_3,1);

                //beware multiple images means that this code will not work
                $(".incorrect02, .incorrect01, .correct").hide(0);
                $(".firstOption, .secondOption, .thirdOption").click(function() {
                    var $this = $(this);
                    if ($this.html().trim().toLowerCase() === $(".rectangle").attr("data-answer").toLowerCase()) {
                        $this.toggleClass("correctAns");
                        $this.siblings("label").fadeOut(1000);
                        // $image.attr("src", imgpath+"candel.gif");
                        $(".question").hide(0);
                        $(".answer").show(0);

                        // TODO: Why is countNext being checked inside switch case block here?
                        // Does it not always be 2 insdide case 2 block?
                        if (countNext == 2 || countNext == 1) {
                            $(".correct").show(0);
                            play_correct_incorrect_sound(1);
                            setTimeout(function(){
                                soundplayer(sound_3_1,1);
                            },1500)

                            $(".secondOption, .thirdOption, .arrow01").hide(0);
                            $nextBtn.css('z-index', '1000');
                            $nextBtn.show(0);
                        } else {
                            setTimeout(function() {
                                ole.footerNotificationHandler.pageEndSetNotification();
                            }, 1000);
                        }
                    } else {
                        if ($this.hasClass("secondOption")) {
                          play_correct_incorrect_sound(0);
                            $(".incorrect02").show(0);
                            setTimeout(function() {
                                $(".incorrect02").hide(0);
                            }, 2000);
                        } else {
                          play_correct_incorrect_sound(0);
                            $(".incorrect01").show(0);
                            setTimeout(function() {
                                $(".incorrect01").hide(0);
                            }, 2000);
                        }
                    }
                });
                break;
            case 3:


                $(".answer").show(0);
                setTimeout(function() {
                    $(".answer").hide(0);
                }, 5000);
                $nextBtn.hide();
                setTimeout(function() {
                    $(".sundar").show(0);
                    $(".sundari").show(0);
                    $(".dialog_sundar").delay(150).show(100);
                    $(".boxquestion1").delay(300).show(0,()=> soundplayer(sound_4));
                    boxquestionflag = true;
                    $nextBtn.css('z-index', '1000');
                }, 12500);
                break;
            case 4:
            finalpageflag = false;

            soundplayer(sound_8);
            break;
            case 5:
                navigationcontroller();
                finalpageflag = false;
                break;
            case 6:
                setTimeout(function() {
                    $(".dialogcontainer01").show(0,()=>soundplayer(sound_9));
                }, 2300);
                finalpageflag = false;
                break;
            case 7:
                $(".cuboidlabel06, .cuboidlabel05, .cuboidlabel04, .cuboidlabel01, .cuboidlabel02").hide(0);
                setTimeout(function() {
                    $(".cuboidlabel01").show(0,()=>soundplayer(sound_10));
                }, 2200);
                setTimeout(function() {
                    $(".cuboidlabel02").show(0,()=>soundplayer(sound_14));
                }, 3500);
                setTimeout(function() {
                  // $(".cuboidlabel06, .cuboidlabel05, .cuboidlabel04").show(0,()=>soundplayerthree(sound_12,sound_11,sound_13));
                    $(".cuboidlabel04").show(0,()=>soundplayer(sound_12));
                }, 5500);
                setTimeout(function() {
                    $(".cuboidlabel05").show(0,()=>soundplayer(sound_11));
                }, 7500);
                setTimeout(function() {
                    $(".cuboidlabel06").show(0,()=>soundplayer(sound_13));
                }, 9500);
                finalpageflag = true;
                break;
            case 8:
                finalpageflag = false;
                $(".finalfilterdiv").show(0);
                $nextBtn.hide(0);
                $prevBtn.show(0);
                setTimeout(function() {
                    $(".finalfilterdiv").show(0);
                }, 2500);
                break;
            default:
                break;
        }
    }

    /*=====  End of Templates Block  ======*/

    /*==================================================
    =            Templates Controller Block            =
    ==================================================*/

    /*==================================================
    =            function to call templates            =
    ==================================================*/
    /**
     Motivation :
     - Make a single function call that handles all the
     template load easier

     How To:
     - Update the template caller with the required templates
     - Call template caller

     What it does:
     - According to value of the Global Variable countNext
     the slide templates are updated
     */
   function soundplayer(i,next){
     buzz.all().stop();
     i.play().bind("ended",function(){
        if(!next) navigationcontroller();
     });
   }

   function soundplayerthree(i, j, k, next){
     buzz.all().stop();
     i.play().bind("ended",()=>{
       j.play().bind("ended",()=>{
         k.play().bind("ended",()=>{
          if(!next) navigationcontroller();
        });
       });
     });
   }

    function templateCaller() {
        if (boxquestionflag) {
            switch (boxquestioncountnext) {
                case 1:
                  $nextBtn.hide(0);

                    $(".boxquestion1").hide(0);
                    $(".dialog_sundar").hide(0);
                    $(".dialog_sundari").delay(150).show(100,()=>soundplayer(sound_5));
                    $(".boxanswer1").delay(300).show(0);
                    break;
                    /*
                    TODO: the homer dialog response in the mockup goes here
                    case 2:
                    					$(".boxquestion2").show(0);
                    					$(".boxanswer1").hide(0);
                    					 break;
                    */

                case 2:
                    $nextBtn.hide();
                    $(".boxquestion2").delay(300).show(0,()=>soundplayer(sound_6));
                    $(".dialog_sundari").hide(0);
                    $(".dialog_sundar").delay(150).show(100);
                    $(".boxanswer1").hide(0);
                    break;
                case 3:
                $nextBtn.hide();

                    $(".boxquestion2").hide(0);
                    $(".boxanswer2").delay(300).show(0,()=>soundplayer(sound_7));
                    $(".dialog_sundar").hide(0);
                    $(".dialog_sundari02").delay(150).show(100);
                    // break;
                    // case 4:
                    // $(".boxanswer2").hide(0);
                    // $(".boxanswer3").show(0);
                    boxquestionflag = false;
                    boxquestioncountnext = 0;

                    break;
                default:
                    boxquestioncountnext = 0;
                    boxquestionflag = false;
                    break;
            }
        } else {

            /*always hide next and previous navigation button unless
             explicitly called from inside a template*/
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');

            // call navigation controller
            // navigationcontroller();

            // call the template
            generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


            //call the slide indication bar handler for pink indicators
            countNext<8?loadTimelineProgress($total_page, countNext + 1):ole.footerNotificationHandler.pageEndSetNotification();;

            // just for development purpose to see total slide vs current slide number
            // $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
        }
    }

    /*this countNext variable change here is solely for development phase and
    should be commented out for deployment*/
    // countNext+=1;

    // first call to template caller
    templateCaller();

    /* navigation buttons event handlers */

    $nextBtn.on('click', function() {
        if (boxquestionflag) {
            boxquestioncountnext++;
        } else {
          countNext++
          if(countNext>7){

            setTimeout(function() {
                $(".finalfilterdiv").show(0);
            }, 2500);
          }
        }
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
        countNext--;
        templateCaller();

        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    /*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref+"/exercise/images/";

var content=[
	//ex1
	{
    instruction: data.string.exques1,
		exetype1: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "clickplace",
        exeoptions:[
          {
            optaddclass: "correct",
          },
          {
          },{
          },{
          }
        ]
			}
		]
	},
	//ex2
	{
    instruction: data.string.exques2,
		exetype1: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "clickplace",
        exeoptions:[
          {
            optaddclass: "correct",
          },
          {
          },{
          },{
          }
        ]
			}
		]
	},
	//ex3
	{
    // instruction: data.string.exques2,
		exetype1: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "clickplace",
        exeoptions:[
          {
            optaddclass: "correct",
          },
          {
          },{
          },{
          }
        ]
			}
		]
	},
	//ex4
	{
    // instruction: data.string.exques2,
		exetype1: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "clickplace",
        exeoptions:[
          {
            optaddclass: "correct",
          },
          {
          },{
          },{
          }
        ]
			}
		]
	},
	//ex5
	{
    // instruction: data.string.exques2,
		exetype1: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "clickplace",
        exeoptions:[
          {
            optaddclass: "correct",
          },
          {
          },{
          },{
          }
        ]
			}
		]
	},
	//ex6
	{
    instruction: data.string.exques6,
		exetype1: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "clickplace",
        exeoptions:[
          {
            optaddclass: "correct",
          },
          {
          },{
          },{
          }
        ]
			}
		]
	},
	//ex7
	{
    instruction: data.string.exques7,
		exetype1: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "clickplace",
        exeoptions:[
          {
            optaddclass: "correct",
          },
          {
          },{
          },{
          }
        ]
			}
		]
	},
	//ex8
	{
    instruction: data.string.exques8,
		exetypesec: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "clickplace",
        exeoptions:[
          {
            optaddclass: "correct",
          },
          {
          },{
          },{
          }
        ]
			}
		]
	},
	//ex9
	{
    instruction: data.string.exques9,
		exetypesec: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "clickplace",
        exeoptions:[
          {
            optaddclass: "correct",
          },
          {
          },{
          },{
          }
        ]
			}
		]
	},
	//ex10
	{
    instruction: data.string.exques10,
		exetypesec: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "clickplace",
        exeoptions:[
          {
            optaddclass: "correct",
          },
          {
          },{
          },{
          }
        ]
			}
		]
	},
];

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("multiopscontent", $("#multiopscontent-partial").html());

	/*for limiting the questions to 10*/
	var $total_page = content.length;

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	var score = 0;
	var exeTemp = new NumberTemplate();

 	exeTemp.init(10);
	/*values in this array is same as the name of images of eggs in image folder*/
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide();
		$prevBtn.hide();

		/*generate question no at the beginning of question*/
		exeTemp.numberOfQuestions();

		var ansClicked = false;
		var wrngClicked = false;
		var multiCorrCounter = 0;
		var multiClick = 0;

    switch (countNext) {
      case 0:
      var quesNo = rand_generator(1);
        $(".buttonsel:eq(0)").html(eval('data.string.exetyp1opt1_o'+quesNo));
        $(".buttonsel:eq(1)").html(eval('data.string.exetyp1opt2_o'+quesNo));
        $(".buttonsel:eq(2)").html(eval('data.string.exetyp1opt3_o'+quesNo));
        $(".buttonsel:eq(3)").html(eval('data.string.exetyp1opt4_o'+quesNo));
      break;
      case 1:
			var quesNo = rand_generator(1);
        $(".buttonsel:eq(0)").html(eval('data.string.exetyp2opt1_o'+quesNo));
        $(".buttonsel:eq(1)").html(eval('data.string.exetyp2opt2_o'+quesNo));
        $(".buttonsel:eq(2)").html(eval('data.string.exetyp2opt3_o'+quesNo));
        $(".buttonsel:eq(3)").html(eval('data.string.exetyp2opt4_o'+quesNo));
      break;
      case 2:
			var quesNo = rand_generator(5);
				$('.instruction').html((countNext+1 )+". "+ eval('data.string.exetyp3_o'+quesNo));
        $(".buttonsel:eq(0)").html(eval('data.string.exetyp3opt1_o'+quesNo));
        $(".buttonsel:eq(1)").html(eval('data.string.exetyp3opt2_o'+quesNo));
        $(".buttonsel:eq(2)").html(eval('data.string.exetyp3opt3_o'+quesNo));
        $(".buttonsel:eq(3)").html(eval('data.string.exetyp3opt4_o'+quesNo));
      break;
      case 3:
			var quesNo = rand_generator(5);
				$('.instruction').html((countNext+1 )+". "+ eval('data.string.exetyp4_o'+quesNo));
        $(".buttonsel:eq(0)").html(eval('data.string.exetyp4opt1_o'+quesNo));
        $(".buttonsel:eq(1)").html(eval('data.string.exetyp4opt2_o'+quesNo));
        $(".buttonsel:eq(2)").html(eval('data.string.exetyp4opt3_o'+quesNo));
        $(".buttonsel:eq(3)").html(eval('data.string.exetyp4opt4_o'+quesNo));
      break;
      case 4:
			var quesNo = rand_generator(5);
				$('.instruction').html((countNext+1 )+". "+ eval('data.string.exetyp5_o'+quesNo));
				// $('.instruction').prepend("<span>"+countNext+"</span>");
        $(".buttonsel:eq(0)").html(eval('data.string.exetyp5opt1_o'+quesNo));
        $(".buttonsel:eq(1)").html(eval('data.string.exetyp5opt2_o'+quesNo));
        $(".buttonsel:eq(2)").html(eval('data.string.exetyp5opt3_o'+quesNo));
        $(".buttonsel:eq(3)").html(eval('data.string.exetyp5opt4_o'+quesNo));
      break;
      case 5:
			var quesNo = rand_generator(5);
			// alert(quesNo);
        $(".buttonsel:eq(0)").html(eval('data.string.exetyp6opt1_o'+quesNo));
        $(".buttonsel:eq(1)").html(eval('data.string.exetyp6opt2_o'+quesNo));
        $(".buttonsel:eq(2)").html(eval('data.string.exetyp6opt3_o'+quesNo));
        $(".buttonsel:eq(3)").html(eval('data.string.exetyp6opt4_o'+quesNo));
      break;
      case 6:
			var quesNo = rand_generator(5);
        $(".buttonsel:eq(0)").html(eval('data.string.exetyp7opt1_o'+quesNo));
        $(".buttonsel:eq(1)").html(eval('data.string.exetyp7opt2_o'+quesNo));
        $(".buttonsel:eq(2)").html(eval('data.string.exetyp7opt3_o'+quesNo));
        $(".buttonsel:eq(3)").html(eval('data.string.exetyp7opt4_o'+quesNo));
      break;
      case 7:
			var quesNo = rand_generator(5);
				$('.question').html(eval('data.string.exetyp8_o'+quesNo));
        $(".buttonsel:eq(0)").html(eval('data.string.exetyp8opt1_o'+quesNo));
        $(".buttonsel:eq(1)").html(eval('data.string.exetyp8opt2_o'+quesNo));
        $(".buttonsel:eq(2)").html(eval('data.string.exetyp8opt3_o'+quesNo));
        $(".buttonsel:eq(3)").html(eval('data.string.exetyp8opt4_o'+quesNo));
      break;
      case 8:
			var quesNo = rand_generator(5);
				$('.question').html(eval('data.string.exetyp9_o'+quesNo));
        $(".buttonsel:eq(0)").html(eval('data.string.exetyp9opt1_o'+quesNo));
        $(".buttonsel:eq(1)").html(eval('data.string.exetyp9opt2_o'+quesNo));
        $(".buttonsel:eq(2)").html(eval('data.string.exetyp9opt3_o'+quesNo));
        $(".buttonsel:eq(3)").html(eval('data.string.exetyp9opt4_o'+quesNo));
      break;
      case 9:
			var quesNo = rand_generator(5);
				$('.question').html(eval('data.string.exetyp10_o'+quesNo));
        $(".buttonsel:eq(0)").html(eval('data.string.exetyp10opt1_o'+quesNo));
        $(".buttonsel:eq(1)").html(eval('data.string.exetyp10opt2_o'+quesNo));
        $(".buttonsel:eq(2)").html(eval('data.string.exetyp10opt3_o'+quesNo));
        $(".buttonsel:eq(3)").html(eval('data.string.exetyp10opt4_o'+quesNo));
      break;



      default:
    }
		texthighlight($board);


		/*for randomizing the options*/
		var parent = $(".optionsdiv");
		var divs = parent.children();
			 while (divs.length) {
			        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			  }

    function rand_generator(limit){
      var randNum = Math.floor((Math.random() * limit) + 1);
      return randNum;
    }

		$(".buttonsel").click(function(){

			$(this).removeClass('forhover');
				if(ansClicked == false){
					if($(this).hasClass("correct")){

						if(wrngClicked == false){
							exeTemp.update(true);
						}
						var corval = $(this).text();
						$(".clickplace").text(corval).css("color","#0B9620");
						play_correct_incorrect_sound(1);
						$(this).css("background","#bed62f");
						$(this).css("border","5px solid #deef3c");
            $(this).css("color","white");
            $(this).siblings(".corctopt").show(0);

						$('.buttonsel').removeClass('forhover forhoverimg');
						ansClicked = true;


						if(countNext != $total_page)
						$nextBtn.show();
					}
					else{
						exeTemp.update(false);
						play_correct_incorrect_sound(0);
						$(this).css("background","#FF0000");
						$(this).css("border","5px solid #980000");
						$(this).css("color","white");
            $(this).siblings(".wrngopt").show(0);

						wrngClicked = true;
					}
				}
			});
		/*======= SCOREBOARD SECTION ==============*/
	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		// for exeTempg purpose
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		// exeTempg purpose code ends


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		exeTemp.gotoNext();
		templateCaller();

	});
	$refreshBtn.click(function(){
		templateCaller();
	});
	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

/*===============================================
  =            data highlight function            =
  ===============================================*/
  function texthighlight($highlightinside){
     //check if $highlightinside is provided
     typeof $highlightinside !== "object" ?
     alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
     null ;

     var $alltextpara = $highlightinside.find("*[data-highlight='true']");
     var stylerulename;
     var replaceinstring;
     var texthighlightstarttag;
     var texthighlightendtag   = "</span>";


     if($alltextpara.length > 0){
       $.each($alltextpara, function(index, val) {
         /*if there is a data-highlightcustomclass attribute defined for the text element
         use that or else use default 'parsedstring'*/
         $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
         (stylerulename = $(this).attr("data-highlightcustomclass")) :
         (stylerulename = "parsedstring") ;

         texthighlightstarttag = "<span class='"+stylerulename+"'>";
         replaceinstring       = $(this).html();
         replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
         replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


         $(this).html(replaceinstring);
       });
     }
   }
   /*=====  End of data highlight function  ======*/

	 /*===== This function splits the string in data into convential fraction used in mathematics =====*/
	 function splitintofractions($splitinside){
			typeof $splitinside !== "object" ?
			 alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			 null ;

			 var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
				if($splitintofractions.length > 0){
					$.each($splitintofractions, function(index, value){
					$this = $(this);
					var tobesplitfraction = $this.html();
					if($this.hasClass('fraction')){
						tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
						tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
					}else{
						tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
						tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
					}


			tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
					$this.html(tobesplitfraction);
				});
				}
		}

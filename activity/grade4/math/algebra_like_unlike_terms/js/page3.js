var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		extratextblock:[{
				textclass:"toptxt purple",
				textdata:data.string.p3s0
			},{
				textclass:"instruction",
				textdata:data.string.p3s0_1
		}],
		bag:	[{
			bagclass:"bag1"
		},{
			bagclass:"bag2"
		},{
			bagclass:"bag3"
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "cover",
					imgid : 'woodbg',
					imgsrc: ""
				},{
					imgclass: "niti_see",
					imgid : 'niti_sidesee',
					imgsrc: ""
				}]
		}],
	},
	// slide2---
	{
		extratextblock:[{
				textclass:"toptxt purple",
				textdata:data.string.p3s1
			}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "cover",
					imgid : 'woodbg',
					imgsrc: ""
				},{
					imgclass: "niti_see",
					imgid : 'niti_sidesee',
					imgsrc: ""
				},{
					imgclass: "apple_11",
					imgid : 'elapbag',
					imgsrc: ""
				},{
					imgclass: "banana_4",
					imgid : 'banbag',
					imgsrc: ""
				}]
		}],
	},
	// slide1
	{
		extratextblock:[{
				textclass:"toptxt purple",
				textdata:data.string.p3s2_sec
			},{
				textclass:"instruction",
				textdata:data.string.p3s2_sec_1
		}],
		bag:	[{
			bagclass:"bag2"
		},{
			bagclass:"bag3"
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "cover",
					imgid : 'woodbg',
					imgsrc: ""
				},{
					imgclass: "niti_think",
					imgid : 'niti_sidesee',
					imgsrc: ""
				}]
		}],
	},
	// slide2
	{
		extratextblock:[{
			textclass:"toptxt purple",
			textdata:data.string.p3s2
		},{
			textclass:"p3s2_1",
			textdata:data.string.p3s2_1
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "cover",
					imgid : 'woodbg',
					imgsrc: ""
				},{
					imgclass: "bag_s2",
					imgid : 'apndbn',
					imgsrc: ""
				},{
					imgclass: "niti_1",
					imgid : 'niti_attention',
					imgsrc: ""
				}]
		}],
	},
	// slide4
	{
		extratextblock:[{
			textclass:"toptxt",
			textdata:data.string.p3s4
		},{
			textclass:"btn1_no_bg",
			textdata:data.string.p3s2_1
		},{
			textclass:"btntxt btn2",
			textdata:data.string.apples
		},{
			textclass:"btntxt btn3",
			textdata:data.string.bananas
		},{
			textclass:"btntxt btn4",
			textdata:data.string.qmark
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "cover",
					imgid : 'woodbg',
					imgsrc: ""
				},{
					imgclass: "bag_s2",
					imgid : 'apndbn',
					imgsrc: ""
				},{
					imgclass: "niti_think",
					imgid : 'niti_thinking',
					imgsrc: ""
				},{
					imgclass: "plus_1",
					imgid : 'plus',
					imgsrc: ""
				},{
					imgclass: "equal_1",
					imgid : 'equal',
					imgsrc: ""
				}]
		}],
	},
	// slide5
	{
		extratextblock:[{
			textclass:"toptxt",
			textdata:data.string.p3s4
		},{
			textclass:"btntxt_1 btn2_1",
			textdata:data.string.a
		},{
			textclass:"btntxt_1 btn3_1",
			textdata:data.string.b
		},{
			textclass:"btntxt_1 btn4_1",
			textdata:data.string.qmark
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "cover",
					imgid : 'woodbg',
					imgsrc: ""
				},{
					imgclass: "bag_s6",
					imgid : 'apndbn',
					imgsrc: ""
				},{
					imgclass: "fairy",
					imgid : 'fairy',
					imgsrc: ""
				},{
					imgclass: "plus_1_1",
					imgid : 'plus',
					imgsrc: ""
				},{
					imgclass: "equal_1_1",
					imgid : 'equal',
					imgsrc: ""
				}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textclass: "ans1",
			textdata: data.string.p3s5,
			imgclass: '',
			imgid : 'tb-3',
			imgsrc: '',
		}]
	},
	// slide6
	{
		extratextblock:[{
			textclass:"toptxt purple",
			textdata:data.string.p3s7_top
		},{
			datahighlightflag: 'true',
			datahighlightcustomclass:'fade_out_1',
			textclass:"btntxt_1 btn2_1",
			textdata:data.string.apple
		},{
			datahighlightflag: 'true',
			datahighlightcustomclass:'fade_out_2',
			textclass:"btntxt_1 btn3_1",
			textdata:data.string.banana
		},{
			textclass:"btntxt_1 btn2_1 fade_in_3",
			textdata:data.string.fda
		},{
			textclass:"btntxt_1 btn3_1 fade_in_4",
			textdata:data.string.fdb
		},{
			textclass:"btntxt_1 btn4_1",
			textdata:data.string.qmark
		},{
			textclass:"btntxt_1 btn_blue",
			textdata:data.string.p3s7_1
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "cover",
					imgid : 'woodbg',
					imgsrc: ""
				},{
					imgclass: "bag_s6",
					imgid : 'apndbn',
					imgsrc: ""
				},{
					imgclass: "plus_1_1",
					imgid : 'plus',
					imgsrc: ""
				},{
					imgclass: "equal_1_1",
					imgid : 'equal',
					imgsrc: ""
				},{
					imgclass: "fairy",
					imgid : 'fairy',
					imgsrc: ""
				},{
					imgclass: "arrow_1",
					imgid : 'arrow',
					imgsrc: ""
				},{
					imgclass: "arrow_2",
					imgid : 'arrow',
					imgsrc: ""
				}]
			}],
		speechbox:[{
			speechbox: 'sp-1',
			textclass: "ans1",
			textdata: data.string.p3s7,
			imgclass: '',
			imgid : 'tb-3',
			imgsrc: '',
		}]
	},
	// slide7
	{
		extratextblock:[{
			textclass:"toptxt purple",
			textdata:data.string.p3s7_top
		},{
			textclass:"btntxt_1 btn2_1",
			textdata:data.string.a
		},{
			textclass:"btntxt_1 btn3_1",
			textdata:data.string.b
		},{
			textclass:"btntxt_1 btn4_1",
			textdata:data.string.qmark
		},{
			textclass:"btntxt_1 btn_blue",
			textdata:data.string.p3s7_1
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "cover",
					imgid : 'woodbg',
					imgsrc: ""
				},{
					imgclass: "bag_s6",
					imgid : 'apndbn',
					imgsrc: ""
				},{
					imgclass: "fairy",
					imgid : 'fairy',
					imgsrc: ""
				},{
					imgclass: "plus_1_1",
					imgid : 'plus',
					imgsrc: ""
				},{
					imgclass: "equal_1_1",
					imgid : 'equal',
					imgsrc: ""
				},{
					imgclass: "arrow_1",
					imgid : 'arrow',
					imgsrc: ""
				},{
					imgclass: "arrow_2",
					imgid : 'arrow',
					imgsrc: ""
				}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textclass: "ans1",
			textdata: data.string.p3s8,
			imgclass: '',
			imgid : 'tb-3',
			imgsrc: '',
		}]
	},
	// slide8
	{
		extratextblock:[{
			textclass:"toptxt purple",
			textdata:data.string.p3s7_top
		},{
			textclass:"btntxt_1 btn2_1",
			textdata:data.string.a
		},{
			textclass:"btntxt_1 btn3_1",
			textdata:data.string.b
		},{
			textclass:"btntxt_1 btn4_8",
			textdata:data.string.noans
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "cover",
					imgid : 'woodbg',
					imgsrc: ""
				},{
					imgclass: "bag_s6",
					imgid : 'apndbn',
					imgsrc: ""
				},{
					imgclass: "fairy",
					imgid : 'fairy',
					imgsrc: ""
				},{
					imgclass: "plus_1_1",
					imgid : 'plus',
					imgsrc: ""
				},{
					imgclass: "equal_1_1",
					imgid : 'equal',
					imgsrc: ""
				}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textclass: "ans1",
			textdata: data.string.p3s9,
			imgclass: '',
			imgid : 'tb-3',
			imgsrc: '',
		}]
	},
	// slide9
	{
		extratextblock:[{
			textclass:"toptxt purple",
			textdata:data.string.p3s7_top
		},{
			textclass:"btntxt_1 btn2_1",
			textdata:data.string.a
		},{
			textclass:"btntxt_1 btn3_1",
			textdata:data.string.b
		},{
			textclass:"btntxt_1 btn4_8",
			textdata:data.string.noans
		},{
			textclass:"btntxt_1 btn4_8 lateshow",
			textdata:data.string.p3s10_1
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "cover",
					imgid : 'woodbg',
					imgsrc: ""
				},{
					imgclass: "bag_s6",
					imgid : 'apndbn',
					imgsrc: ""
				},{
					imgclass: "fairy",
					imgid : 'fairy',
					imgsrc: ""
				},{
					imgclass: "plus_1_1",
					imgid : 'plus',
					imgsrc: ""
				},{
					imgclass: "equal_1_1",
					imgid : 'equal',
					imgsrc: ""
				}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textclass: "ans1",
			textdata: data.string.p3s10,
			imgclass: '',
			imgid : 'tb-3',
			imgsrc: '',
		}]
	},
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "woodbg", src: imgpath+"board.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bag", src: imgpath+"shopping-bag.png", type: createjs.AbstractLoader.IMAGE},
			{id: "apple", src: imgpath+"apple.png", type: createjs.AbstractLoader.IMAGE},
			{id: "banana", src: imgpath+"banana.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fairy", src: 'images/fairy/fairy_fly.gif', type: createjs.AbstractLoader.IMAGE},
			{id: "corrimg", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "niti_attention", src: imgpath+"niti01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "niti_sidesee", src: imgpath+"niti02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "niti_handside_raise", src: imgpath+"niti04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "niti_thinking", src: imgpath+"niti_thinking.png", type: createjs.AbstractLoader.IMAGE},
			{id: "niti_thinking", src: imgpath+"niti_thinking.png", type: createjs.AbstractLoader.IMAGE},
			{id: "apple5", src: imgpath+"apple_05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "apndbn", src: imgpath+"apple-and-banana.png", type: createjs.AbstractLoader.IMAGE},
			{id: "plus", src: imgpath+"plus.png", type: createjs.AbstractLoader.IMAGE},
			{id: "equal", src: imgpath+"equal.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src: imgpath+"arrow01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "banbag", src: imgpath+"4banana_in_bag.png", type: createjs.AbstractLoader.IMAGE},
			{id: "elapbag", src: imgpath+"11apple_in_bag.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-3", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s3_p1", src: soundAsset+"s3_p1.ogg"},
			{id: "s3_p2", src: soundAsset+"s3_p2.ogg"},
			{id: "s3_p3", src: soundAsset+"s3_p3.ogg"},
			{id: "s3_p4", src: soundAsset+"s3_p4.ogg"},
			{id: "s3_p5", src: soundAsset+"s3_p5.ogg"},
			{id: "s3_p6", src: soundAsset+"s3_p6.ogg"},
			{id: "s3_p7", src: soundAsset+"s3_p7.ogg"},
			{id: "s3_p8", src: soundAsset+"s3_p8.ogg"},
			{id: "s3_p9", src: soundAsset+"s3_p9.ogg"},
			{id: "s3_p10", src: soundAsset+"s3_p10.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	// $nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext){
			case 0:
				sound_player("s3_p"+(countNext+1),0);
			var bag1=$(".bag1");
			var bag2=$(".bag2");
			var bag3=$(".bag3");
			$(".bag1").prepend( "<img class= 'bag_1' src='"+preload.getResult('bag').src+"'/>");
			for (var i= 1;i<=5; i++){
				$(".bag1").prepend( "<img class= 'apple' src='"+preload.getResult('apple').src+"'/>");
			}
			$(".apple").css({"cursor":"pointer"});
			var apple_1=$(".apple");
  		var bottom = 15;
  		var left = 9;
  		var z_index = 110;
  		var bottom_addition = 0;
			for (var i= 0; i< $(apple_1).length; i++){
				if(i== 3){
					bottom = 41;
					left = 16.6;
					z_index = 109;
					bottom_addition = 3;
				}
				$($(apple_1)[i]).css({
					"position": "absolute",
					"width": "19%",
					"left": left+"%",
					"bottom": bottom+"%",
					"z-index": z_index
				});
				left += 22;
			}
			$(".bag2").prepend( "<img class= 'bag_2' src='"+preload.getResult('bag').src+"'/>");
			for (var i= 1;i<=6; i++){
				$(".bag2").prepend( "<img class= 'apple_2' src='"+preload.getResult('apple').src+"'/>");
			}
			$(".apple_2").css({"cursor":"pointer"});
			var apple_2=$(".apple_2");
  		var bottom = 15;
  		var left = 9;
  		var z_index = 110;
  		var bottom_addition = 0;
			for (var i= 0; i< $(apple_2).length; i++){
				if(i== 3){
					bottom = 41;
					left = 12;
					z_index = 109;
					bottom_addition = 5;
				}
				$($(apple_2)[i]).css({
					"position": "absolute",
					"width": "19%",
					"left": left+"%",
					"bottom": bottom+"%",
					"z-index": z_index
				});
				left += 22;
			}
			$(bag3).prepend( "<img class= 'bag_3' src='"+preload.getResult('bag').src+"'/>");
			for (var i= 1;i<=11; i++){
				$(".bag3").prepend( "<img class= 'apple_3' src='"+preload.getResult('apple').src+"'/>");
			}
			var apple_3=$(".apple_3");
  		var bottom = 3;
  		var left = 15;
  		var z_index = 110;
  		var bottom_addition = 0;
			for (var i= 0; i< $(apple_3).length; i++){
				if(i== 5){
					bottom = 15;
					left = 15.6;
					z_index = 109;
					bottom_addition = 5;
				}else if (i == 9) {
					bottom = 27;
					left = 21.6;
					z_index = 109;
					bottom_addition = 5;

				}
				$($(apple_3)[i]).css({
					"position": "absolute",
					"width": "12%",
					"left": left+"%",
					"bottom": bottom+"%",
					"z-index": z_index,
					"display":"none"
				});
				left += 12;
			}
			var countAplClk = 0;
			$.each(apple_1, function(i, val){
				$((apple_1)[i]).click(function(){
					$((apple_1)[i]).css({"display":"none"});
					$((apple_3)[i]).css({"display":"block"});
					countAplClk+=1;
					countAplClk==11?nav_button_controls():'';
				});
			})
			$.each(apple_2, function(i, val){
				$((apple_2)[i]).click(function(){
					$((apple_2)[i]).css({"display":"none"});
					$((apple_3)[i+5]).css({"display":"block"});
					countAplClk+=1;
					countAplClk==11?nav_button_controls():'';
				});
			})
			break;

			case 2:
				sound_player("s3_p"+(countNext+1),1);
			var bag3=$(".bag3");
			$(".bag2").prepend( "<img class= 'bag_2' src='"+preload.getResult('bag').src+"'/>");
			for (var i= 1;i<=4; i++){
				$(".bag2").prepend( "<img class= 'banana_2' src='"+preload.getResult('banana').src+"'/>");
			}
			$(".banana_2").css("cursor","pointer");
			var banana_2=$(".banana_2");
  		var bottom = 3;
  		var left = 15;
			var z_index = 110;
			var bottom_addition = 0;
			for (var i= 0; i< $(banana_2).length; i++){
				if(i== 2){
					bottom = 32;
					left = 13.6;
					z_index = 109;
					bottom_addition = 5;
				}
				$($(banana_2)[i]).css({
					"position": "absolute",
					"width": "24%",
					"left": left+"%",
					"bottom": bottom+"%",
					"z-index": z_index
				});
				left += 30;
			}
			$(bag3).prepend( "<img class= 'bag_3' src='"+preload.getResult('bag').src+"'/>");
			for (var i= 1;i<=11; i++){
				$(".bag3").prepend( "<img class= 'apple_3' src='"+preload.getResult('apple').src+"'/>");
			}
			var apple_3=$(".apple_3");
			var bottom = 3;
			var left = 5;
			var z_index = 110;
			var bottom_addition = 0;
			for (var i= 0; i< $(apple_3).length; i++){
				if(i== 5){
					bottom = 15;
					left = 15.6;
					z_index = 109;
					bottom_addition = 5;
				}else if (i == 9) {
					bottom = 27;
					left = 21.6;
					z_index = 109;
					bottom_addition = 5;

				}
				$($(apple_3)[i]).css({
					"position": "absolute",
					"width": "12%",
					"left": left+"%",
					"bottom": bottom+"%",
					"z-index": z_index,
				});
				left += 12;
			}
			for (var i= 1;i<=4; i++){
				$(".bag3").prepend( "<img class= 'banana' src='"+preload.getResult('banana').src+"'/>");
			}
			var banana=$(".banana");
			var bottom = 46;
			var left = 18;
			var z_index = 110;
			var bottom_addition = 0;
			for (var i= 0; i< $(banana).length; i++){
				if(i== 2){
					bottom = 54;
					left = 19.6;
					z_index = 109;
					bottom_addition = 5;
				}
				$($(banana)[i]).css({
					"position": "absolute",
					"width": "19%",
					"left": left+"%",
					"bottom": bottom+"%",
					"z-index": z_index,
					"display" : "none"
				});
				left += 20;
			}
			var bnCount = 0;
			$.each(banana_2, function(i, val){
				$((banana_2)[i]).click(function(){
					$((banana_2)[i]).css({"display":"none"});
					$((banana)[i]).css({"display":"block"});
					bnCount+=1;
					bnCount==4?nav_button_controls():'';
				});
			})
			break;
			case 6:
				$(".fade_out_1,.fade_out_2").parent().delay(1000).fadeOut(1000);
				sound_player("s3_p"+(countNext+1),1);
      break;
			default:
				sound_player("s3_p"+(countNext+1),1);
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls():'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

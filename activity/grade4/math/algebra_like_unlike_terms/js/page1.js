var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// cover
	{
		singletext:[
		{
			textclass: "covertext",
			textdata: data.lesson.chapter
		}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "cover",
					imgid : 'cover',
					imgsrc: ""
				}
			]
		}]
	},
	// slide0
	{
		singletext:[
			{
				textclass: "toptxt",
				textdata: data.string.p1s1
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "cover",
					imgid : 'market',
					imgsrc: ""
				},{
					imgclass: "niti_s1",
					imgid : 'niti_smile',
					imgsrc: ""
				}]
		}]
	},
	// slide1
	{
		singletext:[
		{
			textclass: "toptxt",
			textdata: data.string.p1s2
		}
		],
		imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'bg1',
				imgsrc: ""
			},{
				imgclass: "niti_sml",
				imgid : 'niti_smile',
				imgsrc: ""
			},{
				imgclass: "f_bag fb_1_1",
				imgid : 'fv_apl',
				imgsrc: ""
			},{
				imgclass: "f_bag fb_2_1",
				imgid : 'sx_apl',
				imgsrc: ""
			},{
				imgclass: "f_bag fb_3_1",
				imgid : 'banbag',
				imgsrc: ""
			}
		]
	}]
	},
	// slide3
	{
		singletext:[
		{
			textclass: "toptxt",
			textdata: data.string.p1s3
		},{
			textclass: "box_1 txt",
		},{
			textclass: "box_2 txt",
		},{
			textclass: "box_3 txt",
		},{
			textclass: "submit",
			textdata: data.string.submit
		},{
			textclass: "tryagain",
			textdata: data.string.tryagain
		},{
			textclass: "instrn",
			textdata: data.string.p1s4_1
		}
		],
		imageblock:[{
		imagestoshow:[{
			imgclass: "cover",
			imgid : 'bg1',
			imgsrc: ""
		},{
				imgclass: "buton btn_up_1",
				imgid : 'button',
				imgsrc: ""
			},{
				imgclass: "buton btn_up_2",
				imgid : 'button',
				imgsrc: ""
			},{
				imgclass: "buton btn_up_3",
				imgid : 'button',
				imgsrc: ""
			},{
				imgclass: "buton_dwn btn_dwn_1",
				imgid : 'button',
				imgsrc: ""
			},{
				imgclass: "buton_dwn btn_dwn_2",
				imgid : 'button',
				imgsrc: ""
			},{
				imgclass: "buton_dwn btn_dwn_3",
				imgid : 'button',
				imgsrc: ""
			},{
				imgclass: "niti_sml",
				imgid : 'niti_smile',
				imgsrc: ""
			},{
				imgclass: "f_bag fb_1",
				imgid : 'fv_apl',
				imgsrc: ""
			},{
				imgclass: "f_bag fb_2",
				imgid : 'sx_apl',
				imgsrc: ""
			},{
				imgclass: "f_bag fb_3",
				imgid : 'banbag',
				imgsrc: ""
			}]
		}]
	},
	// slide4
	{
			singletext:[
			{
				textclass: "toptxt",
				textdata: data.string.p1s4
			},{
				textclass: "btmbox bb_1_1 lateshow1",
				textdata: data.string.fv_ap
			},{
				textclass: "btmbox bb_2_1 lateshow3 ",
				textdata: data.string.sx_ap
			},{
				textclass: "ipbox lateshow5",
				textdata: data.string.p1ls
			},{
				textclass: "submit_1 lateshow5",
				textdata: data.string.submit
			}
			],imageblock:[{
			imagestoshow:[{
					imgclass: "cover",
					imgid : 'bg1',
					imgsrc: ""
				},{
					imgclass: "niti_sml",
					imgid : 'niti_smile',
					imgsrc: ""
				},{
					imgclass: "f_bag fb_1_1",
					imgid : 'fv_apl',
					imgsrc: ""
				},{
					imgclass: "f_bag fb_2_1",
					imgid : 'sx_apl',
					imgsrc: ""
				},{
					imgclass: "f_bag fb_3_1",
					imgid : 'banbag',
					imgsrc: ""
				},{
					imgclass: "plus lateshow2",
					imgid : 'plus',
					imgsrc: ""
				},{
					imgclass: "equal lateshow4",
					imgid : 'equal',
					imgsrc: ""
				},{
					imgclass: "box lateshow0",
					imgid : 'box',
					imgsrc: ""
				}]
			}]
	}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "tb-3", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "right", src: 'images/right.png', type: createjs.AbstractLoader.IMAGE},
			{id: "wrong", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
			{id: "fairy", src: 'images/fairy/fairy_fly.gif', type: createjs.AbstractLoader.IMAGE},
			{id: "button", src:imgpath+'button.png', type: createjs.AbstractLoader.IMAGE},
			{id: "cover", src: imgpath+"cover_algebra.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg1", src: imgpath+"board.png", type: createjs.AbstractLoader.IMAGE},
			{id: "niti_smile", src:imgpath+'niti01.png', type: createjs.AbstractLoader.IMAGE},
			{id: "fv_apl", src:imgpath+'apple_05.png', type: createjs.AbstractLoader.IMAGE},
			{id: "sx_apl", src:imgpath+'apple_06.png', type: createjs.AbstractLoader.IMAGE},
			{id: "market", src:imgpath+'super-market.png', type: createjs.AbstractLoader.IMAGE},
			{id: "plus", src:imgpath+'plus.png', type: createjs.AbstractLoader.IMAGE},
			{id: "equal", src:imgpath+'equal.png', type: createjs.AbstractLoader.IMAGE},
			{id: "banana", src:imgpath+'banana.png', type: createjs.AbstractLoader.IMAGE},
			{id: "banbag", src:imgpath+'4banana_in_bag.png', type: createjs.AbstractLoader.IMAGE},
			{id: "box", src:imgpath+'box.png', type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-3", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
			{id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
			{id: "s1_p4", src: soundAsset+"s1_p4.ogg"},
			{id: "s1_p5", src: soundAsset+"s1_p5.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext){
			case 0:
			case 1:
			case 2:
				sound_player("s1_p"+(countNext+1),1);
			break;
			case 3:
				sound_player("s1_p"+(countNext+1),0);
				var count_1=0;
				var count_2=0;
				var count_3=0;
				var flag1= false;
				var flag2= false;
				var flag3= false;
				$(".btn_up_1").click(function(){
					count_1++;
					$(".box_1").html(count_1);
					button_pointers("btn_dwn_1", "btn_up_1", count_1);
					flag1 = true;
					chksub(flag1,flag2,flag3);
				});
				$(".btn_dwn_1").click(function(){
					(count_1 > 0)?count_1--: true;
					$(".box_1").html(count_1);
					button_pointers("btn_dwn_1", "btn_up_1", count_1);
				});
				$(".btn_up_2").click(function(){
					count_2++;
					$(".box_2").html(count_2);
					button_pointers("btn_dwn_2", "btn_up_2", count_2);
					flag2 = true;
					chksub(flag1,flag2,flag3);
				});
				$(".btn_dwn_2").click(function(){
					(count_2 > 0)?count_2--: true;
					$(".box_2").html(count_2);
					button_pointers("btn_dwn_2", "btn_up_2", count_2);
				});

				$(".btn_up_3").click(function(){
					count_3++;
					$(".box_3").html(count_3);
					button_pointers("btn_dwn_3", "btn_up_3", count_3);
					flag3 = true;
					chksub(flag1,flag2,flag3);
				});
				$(".btn_dwn_3").click(function(){
					(count_3 > 0)?count_3--: true;
					$(".box_3").html(count_3);
					button_pointers("btn_dwn_3", "btn_up_3", count_3);
				});
				function chksub (f1,f2,f3){
					if((f1 == true) && (f2 == true) && (f3 == true)){
						$(".submit").css({"pointer-events":"auto"});
					}
				}
			function button_pointers ( button_down, button_up, countvar){
				if(countvar == 0){
						$("."+button_down).css({"pointer-events":"none"});
				}	else{
						$("."+button_down).css({"pointer-events":"auto"});
				}

				if(countvar >= 10){
						$("."+button_up).css({"pointer-events":"none"});
				}	else{
						$("."+button_up).css({"pointer-events":"auto"});
				}
			}

			$(".submit").click(function(){
				if((count_1 == 5) && (count_2 == 6) && (count_3 == 4)){
					$(".txt").prepend( "<img class= 'rightwrong' src="+preload.getResult("right").src+">").addClass("right_show");
					navigationcontroller();
						play_correct_incorrect_sound(1);
				}
				// for 1 coorrect
				else if((count_1 == 5) && (count_2 !== 6) && (count_3 !== 4)){
					box_prop_control_fst(".box_1",".box_2", ".box_3" );
						play_correct_incorrect_sound(0);
				}
				else if((count_1 !== 5) && (count_2 == 6) && (count_3 !== 4)){
					box_prop_control_fst(".box_2",".box_1", ".box_3" );
						play_correct_incorrect_sound(0);
				}
				else if((count_1 !== 5) && (count_2 !== 6) && (count_3 == 4)){
					box_prop_control_fst(".box_3",".box_1", ".box_2" );
						play_correct_incorrect_sound(0);
				}
				// for 2 correct
				else if((count_1 !== 5) && (count_2 == 6) && (count_3 == 4)){
					box_prop_control(".box_1",".box_2", ".box_3" );
						play_correct_incorrect_sound(0);
				}
				else if((count_1 == 5) && (count_2 !== 6) && (count_3 == 4)){
					box_prop_control(".box_2",".box_1", ".box_3" );
						play_correct_incorrect_sound(0);
				}
				else if((count_1 == 5) && (count_2 == 6) && (count_3 !== 4)){
					box_prop_control(".box_3",".box_1", ".box_2" );
						play_correct_incorrect_sound(0);
				}
				else{
					$(".txt").prepend( "<img class= 'rightwrong' src="+preload.getResult("wrong").src+">").addClass("wrong_show");
					$(".tryagain").css({"display":"block"});
						play_correct_incorrect_sound(0);
				}
				$(".buton, .buton_dwn, .submit ").hide(0);
				$(".tryagain").click(function(){
					templatecaller();
				})
				function box_prop_control_fst(bx_1, bx_2, bx_3){
					$(bx_1).prepend( "<img class= 'rightwrong' src="+preload.getResult("right").src+">").addClass("right_show")
					$(bx_2).prepend( "<img class= 'rightwrong' src="+preload.getResult("wrong").src+">").addClass("wrong_show ");
					$(bx_3).prepend( "<img class= 'rightwrong' src="+preload.getResult("wrong").src+">").addClass("wrong_show");
					$(".tryagain").css({"display":"block"});
				}
				function box_prop_control(bx_1, bx_2, bx_3){
					$(bx_1).prepend( "<img class= 'rightwrong' src="+preload.getResult("wrong").src+">").addClass("wrong_show ")
					$(bx_2).prepend( "<img class= 'rightwrong' src="+preload.getResult("right").src+">").addClass("right_show");
					$(bx_3).prepend( "<img class= 'rightwrong' src="+preload.getResult("right").src+">").addClass("right_show");
					$(".tryagain").css({"display":"block"});
				}
			});
			break;
			case 4:
				sound_player("s1_p"+(countNext+1),0);
			$(".submit_1").click(function(e){
				var $curBox = $("#input1")
				inputFinisher($curBox, 11);
			})
			function inputFinisher($curBox, answer){
				if($curBox.val() == answer){
					$(".submit_1").hide(0);
					$curBox.css({
						"background":"#98C02E",
						"border":"0.1em solid #DEEF3C",
						"color":"white"
					});
					var apples = data.string.applestrns
					$(".ipbox").append("<span class='ans'>"+apples+"</span>"+"<img class= 'rightwrong_1' src="+preload.getResult("right").src+">");
					navigationcontroller();
					play_correct_incorrect_sound(1);	
				}
				else{
						play_correct_incorrect_sound(0);
						$curBox.css({
						"background-color":"#FF0000",
						"border":"0.1em solid #980000",
						"color":"white"
					});
				}

			}
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// if(countNext == 0)
		// navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/";
var soundAsset2 = $ref+"/sounds/"+$lang+'/';

var sound_1 = new buzz.sound(soundAsset2+"s3_p1.ogg");
var sound_2 = new buzz.sound(soundAsset2+"s3_p2.ogg");
var sound_3 = new buzz.sound(soundAsset2+"s3_p3_1.ogg");
var sound_4 = new buzz.sound(soundAsset2+"s3_p3_2.ogg");
var sound_5 = new buzz.sound(soundAsset2+"s3_p3_3.ogg");
var sound_6 = new buzz.sound(soundAsset2+"s3_p3_4.ogg");
var sound_7 = new buzz.sound(soundAsset2+"s3_p4.ogg");
var sound_8 = new buzz.sound(soundAsset2+"s3_p5.ogg");


var sound_behind = new buzz.sound((soundAsset + "behind.mp3"));
var sound_next_to = new buzz.sound((soundAsset + "next-to.mp3"));
var sound_on_top_of = new buzz.sound((soundAsset + "on-top-of.mp3"));
var sound_in = new buzz.sound((soundAsset + "in.mp3"));

var sound_group_prep = [sound_behind, sound_next_to, sound_on_top_of, sound_in];
// position of clouds and sprite
var cloud_pos_0 = 0;
var cloud_pos_1 = 60;
var cloud_pos_2 = 35;

var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'main_bg',

		uppertextblockadditionalclass : 'reason_block happymonkey',
		uppertextblock : [{
			textdata : data.string.p3text1,
			textclass : 'text_reason'
		},],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "moon",
				imgsrc : imgpath + "moon1.png",
			},
			{
				imgclass : "cloud_0",
				imgsrc : imgpath + "cloud01.png",
			},
			{
				imgclass : "elephant_radar",
				imgsrc : imgpath + "hill.png",
			},
			{
				imgclass : "cloud_1",
				imgsrc : imgpath + "cloud02.png",
			},
			{
				imgclass : "cloud_2",
				imgsrc : imgpath + "cloud03.png",
			},
			{
				imgclass : "banana_stall",
				imgsrc : imgpath + "watermelon.png",
			},
			{
				imgclass : "watermelon_stall",
				imgsrc : imgpath + "pineapple.png",
			}],
		}],
		spriteblockadditionalclass:'',
		spriteblock: [{
			imagelabels : [
			{
				imagelabelclass : "tiny_elephant",
				imagelabeldata : ''
			}]
		}],
		extratextblock:[{
			textdata : '',
			textclass : 'green_bottom_layer'
		},
		{
			textdata : data.string.watermelonshop,
			textclass : 'bananashop'
		},
		{
			textdata : data.string.pineappleshop,
			textclass : 'watermelonshop'
		}]
	},

	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'main_bg',

		uppertextblockadditionalclass : 'utextblock happymonkey',
		uppertextblock : [{
			textdata : data.string.p3text2,
			textclass : 'text_qna'
		},
		{
			textdata : data.string.pqna4,
			textclass : 'text_answer text_banana '
		},
		{
			textdata : data.string.pqna5,
			textclass : 'text_answer text_watermelon '
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "moon",
				imgsrc : imgpath + "moon1.png",
			},
			{
				imgclass : "cloud_0",
				imgsrc : imgpath + "cloud01.png",
			},
			{
				imgclass : "elephant_radar",
				imgsrc : imgpath + "hill.png",
			},
			{
				imgclass : "cloud_1",
				imgsrc : imgpath + "cloud02.png",
			},
			{
				imgclass : "cloud_2",
				imgsrc : imgpath + "cloud03.png",
			},
			{
				imgclass : "banana_stall",
				imgsrc : imgpath + "watermelon.png",
			},
			{
				imgclass : "watermelon_stall",
				imgsrc : imgpath + "pineapple.png",
			}],
		}],
		spriteblockadditionalclass:'',
		spriteblock: [{
			imagelabels : [
			{
				imagelabelclass : "tiny_elephant",
				imagelabeldata : ''
			}]
		}],
		extratextblock:[{
			textdata : '',
			textclass : 'green_bottom_layer'
		},{
			textdata : '',
			textclass : 'correct_answer_tick_r'
		},{
			textdata : '',
			textclass : 'incorrect_ans'
		},
		{
			textdata : data.string.watermelonshop,
			textclass : 'bananashop'
		},
		{
			textdata : data.string.pineappleshop,
			textclass : 'watermelonshop'
		}]
	},

	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'main_bg',

		uppertextblockadditionalclass : 'reason_block happymonkey',
		uppertextblock : [{
			textdata : data.string.p3text3,
			textclass : 'text_reason'
		},
		{
			textdata : data.string.p3text4,
			textclass : 'text_reason text_1'
		},
		{
			textdata : data.string.p3text5,
			textclass : 'text_reason text_2'
		},
		{
			textdata : data.string.p3text6,
			textclass : 'text_reason text_3'
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "moon",
				imgsrc : imgpath + "moon1.png",
			},
			{
				imgclass : "cloud_0",
				imgsrc : imgpath + "cloud01.png",
			},
			{
				imgclass : "elephant_radar",
				imgsrc : imgpath + "hill.png",
			},
			{
				imgclass : "cloud_1",
				imgsrc : imgpath + "cloud02.png",
			},
			{
				imgclass : "cloud_2",
				imgsrc : imgpath + "cloud03.png",
			},
			{
				imgclass : "banana_stall",
				imgsrc : imgpath + "watermelon.png",
			},
			{
				imgclass : "watermelon_stall",
				imgsrc : imgpath + "pineapple.png",
			}],
		}],
		spriteblockadditionalclass:'',
		spriteblock: [{
			imagelabels : [
			{
				imagelabelclass : "tiny_elephant walking",
				imagelabeldata : ''
			}]
		}],
		extratextblock:[{
			textdata : '',
			textclass : 'green_bottom_layer'
		},
		{
			textdata : data.string.watermelonshop,
			textclass : 'bananashop'
		},
		{
			textdata : data.string.pineappleshop,
			textclass : 'watermelonshop'
		}]
	},

	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust: true,
		contentblockadditionalclass : 'shore',
		uppertextblockadditionalclass : 'reason_block happymonkey',
		uppertextblock : [{
			textdata : data.string.p3text7,
			textclass : 'text_reason'
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "elephant_radar",
				imgsrc : imgpath + "hill02.png",
			}],
		}],
	},

	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust: true,
		contentblockadditionalclass : 'shore',
		uppertextblockadditionalclass : 'reason_block happymonkey',
		uppertextblock : [{
			textdata : data.string.p3text7,
			textclass : 'text_reason'
		},
		{
			textdata : data.string.p3text8,
			textclass : 'text_reason'
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "elephant_radar",
				imgsrc : imgpath + "hill02.png",
			}],
		}],
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust: true,
		contentblockadditionalclass : 'shore',
		uppertextblockadditionalclass : 'full_text',
		uppertextblock : [{
			textdata : data.string.p3text9,
			textclass : 'text_qna',
			datahighlightflag : true,
			datahighlightcustomclass : 'number_highlight'
		},
		{
			textdata : '1',
			textclass : 'choice_1 choice'
		},
		{
			textdata : '2',
			textclass : 'choice_2 choice'
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "number_scale",
				imgsrc : imgpath + "hill03.png",
			}],
		}],
		lowertextblockadditionalclass : 'scale_text',
		lowertextblock : [{
			textdata : '',
			textclass : 'scale_data sd_0',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_1',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_2',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_3',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_4',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_5',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_6',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_7',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_8',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_9',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_10',
		}],
		extratextblock:[{
			textdata : '',
			textclass : 'correct_answer_tick_1'
		},{
			textdata : '',
			textclass : 'correct_answer_tick_2'
		}]
	},

	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust: true,
		contentblockadditionalclass : 'shore',
		uppertextblockadditionalclass : 'full_text',
		uppertextblock : [{
			textdata : data.string.p3text9,
			textclass : 'text_qna',
			datahighlightflag : true,
			datahighlightcustomclass : 'number_highlight'
		},
		{
			textdata : '1',
			textclass : 'choice_1 choice'
		},
		{
			textdata : '2',
			textclass : 'choice_2 choice'
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "number_scale",
				imgsrc : imgpath + "hill03.png",
			}],
		}],
		lowertextblockadditionalclass : 'scale_text',
		lowertextblock : [{
			textdata : '',
			textclass : 'scale_data sd_0',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_1',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_2',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_3',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_4',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_5',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_6',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_7',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_8',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_9',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_10',
		}],
		extratextblock:[{
			textdata : '',
			textclass : 'correct_answer_tick_1'
		},{
			textdata : '',
			textclass : 'correct_answer_tick_2'
		}]
	},

	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust: true,
		contentblockadditionalclass : 'shore',
		uppertextblockadditionalclass : 'full_text',
		uppertextblock : [{
			textdata : data.string.p3text9,
			textclass : 'text_qna',
			datahighlightflag : true,
			datahighlightcustomclass : 'number_highlight'
		},
		{
			textdata : '1',
			textclass : 'choice_1 choice'
		},
		{
			textdata : '2',
			textclass : 'choice_2 choice'
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "number_scale",
				imgsrc : imgpath + "hill03.png",
			}],
		}],
		lowertextblockadditionalclass : 'scale_text',
		lowertextblock : [{
			textdata : '',
			textclass : 'scale_data sd_0',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_1',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_2',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_3',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_4',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_5',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_6',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_7',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_8',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_9',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_10',
		}],
		extratextblock:[{
			textdata : '',
			textclass : 'correct_answer_tick_1'
		},{
			textdata : '',
			textclass : 'correct_answer_tick_2'
		}]
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	var last_page = false;
	loadTimelineProgress($total_page, countNext + 1);

	var keep_moving = true;
	var go_next = 0;
	var c_time = 0;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page

    function navigationcontroller(islastpageflag) {
        typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
        if(countNext==4){
          ole.footerNotificationHandler.pageEndSetNotification();
        }
        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
            $prevBtn.css('display', 'none');
        } else if ($total_page == 1) {
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');

            // if lastpageflag is true
            islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
        } else if (countNext > 0 && countNext < $total_page - 1) {
            $nextBtn.show(0);
            $prevBtn.show(0);
        } else if (countNext == $total_page - 1) {
            $nextBtn.css('display', 'none');
            $prevBtn.show(0);

            // if lastpageflag is true
            // islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
        }
    }

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress("5", countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch (countNext) {
		case 0:
			soundplayer(sound_1);
			break;
		case 1:
			soundplayer(sound_2,1);

			// the options to be clicked and correct response
			$('.text_banana').click(function(){
				$('.text_banana').css({"background-color": "#BA6B82", 'pointer-events': 'none', 'border': 'none'});
				play_correct_incorrect_sound(0);
				$(".incorrect_ans").show(0);
			});
			$('.text_watermelon').click(function(){
				$('.text_watermelon').css({"background-color": "#B6D7A8", 'pointer-events': 'none'});
				$('.text_banana').css({'pointer-events': 'none'});
				$('.correct_answer_tick_r').show(0);
				play_correct_incorrect_sound(1);
				$nextBtn.show(0);
			});
			break;
		case 2:
			soundplayer(sound_3);

			background_mover();
			var date = new Date();
			c_time = date.getSeconds();
			// set keep_moving true to enable sprite and cloud movement
			keep_moving = true;
			go_next = 1;
			$prevBtn.show(0);
			$('.spriteblock').css({
				'transform': 'rotate(21deg)'
			});
			$('.spriteblock').addClass('to_shop');
			setTimeout(function(){
				$('.tiny_elephant').removeClass("walking");
				$('.tiny_elephant').css({
					'background-image': 'url(' +imgpath+'sitting.png)',
					'background-size': '100% 100%'
				});
			}, 5000);
			break;
			case 3:
			soundplayer(sound_7);
			break;
			case 4:
			soundplayer(sound_8);
			break;
		case 5:


			/* get a random variable and get maximum and minimum option to get correct rounded off
			 * choice and then set the correct answers with .html method and click function to see
			 * the correct answer is clicked or not	 */
			var random_no = ole.getRandom(1, 21, 89);
			while(random_no%10==0){
				random_no = ole.getRandom(1, 21, 89);
			}
			var choices = rounded_to(random_no);
			$('.number_highlight').html(random_no);
			$('.choice_1').html(choices[0]);
			$('.choice_2').html(choices[1]);
			$('.choice_1').click(function(){
				choice_click('.choice_1', choices[2]);
			});
			$('.choice_2').click(function(){
				choice_click('.choice_2', !choices[2]);
			});
			fill_scale(choices[0], '.sd_', 1);
			break;
		case 6:
			/* get a random variable and get maximum and minimum option to get correct rounded off
			 * choice and then set the correct answers with .html method and click function to see
			 * the correct answer is clicked or not	 */
			$nextBtn.hide(0);
			var random_no = ole.getRandom(1, 21, 89);
			while(random_no%10==0){
				random_no = ole.getRandom(1, 21, 89);
			}
			var choices = rounded_to(random_no);
			$('.number_highlight').html(random_no);
			$('.choice_1').html(choices[0]);
			$('.choice_2').html(choices[1]);
			$('.choice_1').click(function(){
				choice_click('.choice_1', choices[2]);
			});
			$('.choice_2').click(function(){
				choice_click('.choice_2', !choices[2]);
			});
			/* fill the scale in the hill with correct answer */
			fill_scale(choices[0], '.sd_', 1);
			break;
		case 7:
			$nextBtn.hide(0);
			var random_no = 95;
			var choices = rounded_to(random_no);
			$('.number_highlight').html(random_no);
			$('.choice_1').html(choices[0]);
			$('.choice_2').html(choices[1]);
			$('.choice_1').click(function(){
				choice_click('.choice_1', choices[2]);
			});
			$('.choice_2').click(function(){
				choice_click('.choice_2', !choices[2]);
			});
			fill_scale(choices[0], '.sd_', 1);
			break;
		default:
			$nextBtn.show(0);
			$prevBtn.show(0);
			break;
		}
	}

	function soundplayer(i,next){
		buzz.all().stop();
		i.play().bind("ended",function(){
			 if(!next) navigationcontroller();
		});
	}


	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		// navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	// function to add sound event on click
	function sound_caller(sound_box_class, sound_var){
		$(sound_box_class).click(function(){
			sound_var.play();
		});
	}


	$nextBtn.on("click", function() {
		switch(countNext){
			// in most cases nextbtn.click doesnt increase the countNext and call Templete caller
			// we need to switch page only when background object leaves the screen and is thus called in obj_mover
			case 2:
				// in most cases nextbtn.click doesnt increase the countNext and call Templete caller
				// we need to switch page only when background object leaves the screen and is thus called in obj_mover
				switch(go_next){
					case 1:
						$prevBtn.hide(0);
						$nextBtn.hide(0);

						$('.text_1').show(0);
						soundplayer(sound_4);

						go_next = 2;
						break;
					case 2:
						$prevBtn.hide(0);
						$nextBtn.hide(0);
						soundplayer(sound_5);

						$('.text_2').show(0);
						go_next = 3;
						break;
					case 3:
						$prevBtn.hide(0);
						$nextBtn.hide(0);

						soundplayer(sound_6);

						$('.text_3').show(0);
						go_next = 4;
						var date = new Date();
						var new_time = date.getSeconds();
						if( (new_time-c_time) > 5){
						}
						break;
					default:
						countNext++;
						templateCaller();
						break;
				}
				break;
			default:
				countNext++;
				templateCaller();
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();

	function next_btn_caller (next_display) {
		if(!next_display) {
			return false;
		} else {
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
	}

	// function used to move clouds
	function background_mover() {
		setInterval(function() {
			if(!keep_moving) {
				return false;
			}
			if(cloud_pos_1 > 100){
				cloud_pos_1 = -20;
			}
			if(cloud_pos_0 > 115){
				cloud_pos_0 = -20;
			}
			if(cloud_pos_2 > 110){
				cloud_pos_2 = -20;
			}
			cloud_pos_0+= 0.01;
			cloud_pos_1+= 0.01;
			cloud_pos_2+= 0.01;
			$('.cloud_0').css('right', (cloud_pos_0+'%'));
			$('.cloud_1').css('right', (cloud_pos_1+'%'));
			$('.cloud_2').css('right', (cloud_pos_2+'%'));
		}, 20);
	}


	// answer has 3 parts, answer[0]=min, answer[1]=max and answer[2] is 1 if correct choice is min and 0 if it is incorrect
	function rounded_to(rounding_no){
		var answer = [0,0,0];
		if( rounding_no < 10) {
			answer = [0, 10, 0];
		} else if( rounding_no > 10 && rounding_no < 100) {
			answer[0] = Math.floor(rounding_no/10)*10;
			answer[1] = answer[0] + 10;
		} else if( rounding_no > 100 && rounding_no < 1000) {
			answer[0] = Math.floor(rounding_no/100)*100;
			answer[1] = answer[0] + 100;
		}
		if((rounding_no-answer[0])<(answer[1]-rounding_no)){
			answer[2]=1;
		}
		return answer;
	}

	// to get data to fill the scale in the hill
	function scale_data(min, incrementor){
		var answer = [0,0,0,0,0,0,0,0,0,0,0];
		for(var m = 0; m<=10; m++){
			answer[m] = min+m*incrementor;
		}
		return answer;
	}

	// to fill the scale in the hill with data
	function fill_scale(min, class_name, incrementor){
		var answer = scale_data(min, incrementor);
		for( var m = 0; m <=10; m++){
			var my_class = $(class_name+m);
			my_class.html(answer[m]);
		}
	}

	// to see if correct answer was clicked or not
	function choice_click(class_name, correct) {
		if(!correct){
			$(class_name).css({"background-color": "#BA6B82", 'pointer-events': 'none', 'border': 'none'});
		} else {
			$(class_name).css({"background-color": "#6EB260", 'pointer-events': 'none', 'border': 'none'});
			$('.choice').css({'pointer-events': 'none'});
			var classnamechoice = class_name.replace(/\D/g, '');
			$('.correct_answer_tick_'+classnamechoice).show(0);
			if(countNext == $total_page - 1){
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				$nextBtn.show(0);
			}
		}
	}

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

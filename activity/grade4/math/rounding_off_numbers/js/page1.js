var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+'/';

var sound_1 = new buzz.sound(soundAsset+"s1_p1.ogg");
var sound_2 = new buzz.sound(soundAsset+"s1_p2.ogg");
var sound_3 = new buzz.sound(soundAsset+"s1_p3.ogg");
var sound_4 = new buzz.sound(soundAsset+"s1_p4.ogg");
var sound_5 = new buzz.sound(soundAsset+"s1_p5.ogg");
var sound_6 = new buzz.sound(soundAsset+"s1_p6.ogg");

// position of clouds and sprite
var cloud_pos_0 = 0;
var cloud_pos_1 = 60;
var cloud_pos_2 = 35;
// variables defining the positions where elephant stops
var stop_positions =['stop_animation_1', 'stop_animation_2', 'stop_animation_3', 'stop_animation_4'];
var left_positions = ['51.5%', '60%', '68%', '75.5%'];
var bottom_positions = ['43.3%', '37.5%', '31.8%', '26.7%'];
var to_shop = ['to_shop_1', 'to_shop_2','to_shop_3','to_shop_4'];

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust: true,
		additionalclasscontentblock : '',
		uppertextblock : [{
			textdata : data.lesson.chapter,
			textclass : 'lesson-title'
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "coverbg",
				imgsrc : imgpath + "cover_page.png",
			}]
		}]
	},

	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust: true,
		contentblockadditionalclass : 'main_bg',
		uppertextblockadditionalclass : 'lesson_intro ProximaNova',
		uppertextblock : [{
			textdata : data.string.p1text1,
			textclass : 'lesson_intro_data'
		},
		{
			textdata : data.string.p1text2,
			textclass : 'lesson_intro_data'
		},
		{
			textdata : data.string.p1text3,
			textclass : 'lesson_intro_data'
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "moon",
				imgsrc : imgpath + "moon1.png",
			},
			{
				imgclass : "cloud_0",
				imgsrc : imgpath + "cloud01.png",
			},
			{
				imgclass : "elephant_radar",
				imgsrc : imgpath + "hill0.png",
			},
			{
				imgclass : "cloud_1",
				imgsrc : imgpath + "cloud02.png",
			},
			{
				imgclass : "cloud_2",
				imgsrc : imgpath + "cloud03.png",
			},
			{
				imgclass : "banana_stall",
				imgsrc : imgpath + "banana.png",
			},
			{
				imgclass : "watermelon_stall",
				imgsrc : imgpath + "watermelon.png",
			}],
		}],
		spriteblockadditionalclass:'',
		spriteblock: [{
			imagelabels : [
			{
				imagelabelclass : "tiny_elephant rotated",
				imagelabeldata : ''
			}]
		}],
		extratextblock:[{
			textdata : '',
			textclass : 'green_bottom_layer'
		},
		{
			textdata : data.string.bananashop,
			textclass : 'bananashop'
		},
		{
			textdata : data.string.watermelonshop,
			textclass : 'watermelonshop'
		}]
	},

	// //slide2
	// {
		// hasheaderblock : false,
		// contentblocknocenteradjust : true,
		// contentblockadditionalclass : 'main_bg',
//
		// uppertextblockadditionalclass : 'utextblock',
		// uppertextblock : [{
			// textdata : '',
			// textclass : 'text_qna its_hidden'
		// }],
		// imageblock : [{
			// imagestoshow : [
			// {
				// imgclass : "moon",
				// imgsrc : imgpath + "moon1.png",
			// },
			// {
				// imgclass : "cloud_0",
				// imgsrc : imgpath + "cloud01.png",
			// },
			// {
				// imgclass : "elephant_radar",
				// imgsrc : imgpath + "hill0.png",
			// },
			// {
				// imgclass : "cloud_1",
				// imgsrc : imgpath + "cloud02.png",
			// },
			// {
				// imgclass : "cloud_2",
				// imgsrc : imgpath + "cloud03.png",
			// },
			// {
				// imgclass : "banana_stall",
				// imgsrc : imgpath + "banana.png",
			// },
			// {
				// imgclass : "watermelon_stall",
				// imgsrc : imgpath + "watermelon.png",
			// }],
		// }],
		// spriteblockadditionalclass:'',
		// spriteblock: [{
			// imagelabels : [
			// {
				// imagelabelclass : "tiny_elephant rotated",
				// imagelabeldata : ''
			// }]
		// }],
		// extratextblock:[{
			// textdata : '',
			// textclass : 'green_bottom_layer'
		// }]
	// },

	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'main_bg',

		uppertextblockadditionalclass : 'utextblock',
		uppertextblock : [{
			textdata : data.string.pqna1,
			textclass : 'text_qna1 ProximaNova its_hidden'
		},],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "moon",
				imgsrc : imgpath + "moon1.png",
			},
			{
				imgclass : "cloud_0",
				imgsrc : imgpath + "cloud01.png",
			},
			{
				imgclass : "elephant_radar",
				imgsrc : imgpath + "hill0.png",
			},
			{
				imgclass : "cloud_1",
				imgsrc : imgpath + "cloud02.png",
			},
			{
				imgclass : "cloud_2",
				imgsrc : imgpath + "cloud03.png",
			},
			{
				imgclass : "banana_stall",
				imgsrc : imgpath + "banana.png",
			},
			{
				imgclass : "watermelon_stall",
				imgsrc : imgpath + "watermelon.png",
			}],
		}],

		spriteblock: [{
			imagelabels : [
			{
				imagelabelclass : "walking tiny_elephant",
				imagelabeldata : ''
			}]
		}],
		extratextblock:[{
			textdata : '',
			textclass : 'green_bottom_layer'
		},
		{
			textdata : data.string.bananashop,
			textclass : 'bananashop'
		},
		{
			textdata : data.string.watermelonshop,
			textclass : 'watermelonshop'
		}]
	},

	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'main_bg',

		uppertextblockadditionalclass : 'utextblock',
		uppertextblock : [{
			textdata : data.string.pqna2,
			textclass : 'text_qna ProximaNova '
		},
		{
			textdata : data.string.pqna3,
			textclass : 'text_answer ProximaNova text_banana'
		},
		{
			textdata : data.string.pqna4,
			textclass : 'text_answer ProximaNova text_watermelon'
		},],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "moon",
				imgsrc : imgpath + "moon1.png",
			},
			{
				imgclass : "cloud_0",
				imgsrc : imgpath + "cloud01.png",
			},
			{
				imgclass : "elephant_radar",
				imgsrc : imgpath + "hill0.png",
			},
			{
				imgclass : "cloud_1",
				imgsrc : imgpath + "cloud02.png",
			},
			{
				imgclass : "cloud_2",
				imgsrc : imgpath + "cloud03.png",
			},
			{
				imgclass : "banana_stall",
				imgsrc : imgpath + "banana.png",
			},
			{
				imgclass : "watermelon_stall",
				imgsrc : imgpath + "watermelon.png",
			}],
		}],
		spriteblockadditionalclass:'',
		spriteblock: [{
			imagelabels : [
			{
				imagelabelclass : "tiny_elephant",
				imagelabeldata : ''
			}]
		}],
		extratextblock:[{
			textdata : '',
			textclass : 'green_bottom_layer'
		},{
			textdata : '',
			textclass : 'correct_answer_tick_r'
		},{
			textdata : '',
			textclass : 'incorrect_ans'
		},
		{
			textdata : data.string.bananashop,
			textclass : 'bananashop'
		},
		{
			textdata : data.string.watermelonshop,
			textclass : 'watermelonshop'
		}]
	},

	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'main_bg',

		uppertextblockadditionalclass : 'reason_block ProximaNova',
		uppertextblock : [{
			textdata : data.string.p1text4,
			textclass : 'text_reason text_0'
		},
		{
			textdata : data.string.p1text5,
			textclass : 'text_reason text_1',
			datahighlightflag : true,
			datahighlightcustomclass : 'number_highlight'
		},
		{
			textdata : data.string.p1text6,
			textclass : 'text_reason text_2'
		},
		{
			textdata : data.string.p1text7,
			textclass : 'text_reason text_3'
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "moon",
				imgsrc : imgpath + "moon1.png",
			},
			{
				imgclass : "cloud_0",
				imgsrc : imgpath + "cloud01.png",
			},
			{
				imgclass : "elephant_radar",
				imgsrc : imgpath + "hill0.png",
			},
			{
				imgclass : "cloud_1",
				imgsrc : imgpath + "cloud02.png",
			},
			{
				imgclass : "cloud_2",
				imgsrc : imgpath + "cloud03.png",
			},
			{
				imgclass : "banana_stall",
				imgsrc : imgpath + "banana.png",
			},
			{
				imgclass : "watermelon_stall",
				imgsrc : imgpath + "watermelon.png",
			}],
		}],
		spriteblockadditionalclass:'',
		spriteblock: [{
			imagelabels : [
			{
				imagelabelclass : "tiny_elephant walking",
				imagelabeldata : ''
			}]
		}],
		extratextblock:[{
			textdata : '',
			textclass : 'green_bottom_layer'
		},
		{
			textdata : data.string.bananashop,
			textclass : 'bananashop'
		},
		{
			textdata : data.string.watermelonshop,
			textclass : 'watermelonshop'
		}]
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	var last_page = false;
	var go_next = 0;
	loadTimelineProgress($total_page, countNext + 1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var random_index = ole.getRandom(1, 3, 0);
	var keep_moving = true;

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page

	    function navigationcontroller(islastpageflag) {
	        typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
	        if(countNext==4){
	          ole.footerNotificationHandler.pageEndSetNotification();
	        }
	        if (countNext == 0 && $total_page != 1) {
	            $nextBtn.show(0);
	            $prevBtn.css('display', 'none');
	        } else if ($total_page == 1) {
	            $prevBtn.css('display', 'none');
	            $nextBtn.css('display', 'none');

	            // if lastpageflag is true
	            islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	        } else if (countNext > 0 && countNext < $total_page - 1) {
	            $nextBtn.show(0);
	            $prevBtn.show(0);
	        } else if (countNext == $total_page - 1) {
	            $nextBtn.css('display', 'none');
	            $prevBtn.show(0);

	            // if lastpageflag is true
	            // islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	        }
	    }

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch (countNext) {
		case 0:
			soundplayer(sound_1);
			break;
		case 1:
			soundplayer(sound_2);
			break;
		// case 2:
			// $nextBtn.show(0);
			// break;
		case 2:
			// set keep_moving true to enable sprite and cloud movement
			keep_moving = true;
			$('.spriteblock').addClass(stop_positions[random_index]);
			var timer = random_index*1000+6000;
			setTimeout(function(){
				// $nextBtn.show(0);
				$('.tiny_elephant').removeClass("walking");
				$('.tiny_elephant').css({
					'background-image': 'url(' +imgpath+'sitting.png)',
					'background-size': '100% 100%'
				});
				$('.text_qna1').show(0, ()=>soundplayer(sound_3));
			}, timer);

			break;
		case 3:
			soundplayer(sound_4,1);

			$('.tiny_elephant').css({
					'background-image': 'url(' +imgpath+'sitting.png)',
					'background-size': '100% 100%'
				});
			$('.spriteblock').css({
				'left': left_positions[random_index],
				'bottom': bottom_positions[random_index],
				'transform': 'rotate(21deg)'
			});
			$('.text_banana').click(function(){
				play_correct_incorrect_sound(0);
				$(".incorrect_ans").show(0);

				$('.text_banana').css({"background-color": "#BA6B82", 'pointer-events': 'none', 'border': 'none'});
			});
			$('.text_watermelon').click(function(){
				$('.text_watermelon').css({"background-color": "#6EB260", 'pointer-events': 'none', 'border': 'none'});
				$('.text_banana').css({'pointer-events': 'none'});
				play_correct_incorrect_sound(1);
				$('.correct_answer_tick_r').show(0);
				$nextBtn.show(0);

			});
			break;
		case 4:

			soundplayer(sound_6,1);
			go_next = 0;
			$nextBtn.show(0);
			$prevBtn.show(0);
			keep_moving = true;
			$('.spriteblock').css({
				'left': left_positions[random_index],
				'bottom': bottom_positions[random_index],
				'transform': 'rotate(21deg)'
			});
			var current_number = 6+parseInt(random_index);
			$('.number_highlight').html(current_number);
			$('.spriteblock').addClass(to_shop[random_index]);
			var timer = (4-random_index)*1000;
			setTimeout(function(){
				$('.tiny_elephant').removeClass("walking");
				$('.tiny_elephant').css({
					'background-image': 'url(' +imgpath+'sitting.png)',
					'background-size': '100% 100%'
				});
			}, timer);
			break;

		default:
			break;
		}
	}

	function soundplayer(i,next){
		buzz.all().stop();
		i.play().bind("ended",function(){
			 if(!next) navigationcontroller();
		});
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		// navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		if(countNext==4){
			switch(go_next){
				case 0:
					go_next = 1;
				case 1:
					$('.text_'+go_next).show(0);
					$nextBtn.show(0);
					go_next = 2;
					break;
				case 2:
					$('.text_'+go_next).show(0);
					$nextBtn.show(0);
					go_next = 3;
					break;
				case 3:
					$('.text_'+go_next).show(0);
					go_next = 4;
					$nextBtn.hide(0);
					soundplayer(sound_5);
					break;
			}
		} else {
				countNext++;
				templateCaller();
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
		loadTimelineProgress($total_page, countNext + 1);
	});

	total_page = content.length;
	templateCaller();

	function next_btn_caller (next_display) {
		if(!next_display) {
			return false;
		} else {
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
	}

	// function used to move clouds
	function background_mover() {
		setInterval(function() {
			if(!keep_moving) {
				return false;
			}
			if(cloud_pos_1 > 100){
				cloud_pos_1 = -20;
			}
			if(cloud_pos_0 > 115){
				cloud_pos_0 = -20;
			}
			if(cloud_pos_2 > 110){
				cloud_pos_2 = -20;
			}
			cloud_pos_0+= 0.01;
			cloud_pos_1+= 0.01;
			cloud_pos_2+= 0.01;
			$('.cloud_0').css('right', (cloud_pos_0+'%'));
			$('.cloud_1').css('right', (cloud_pos_1+'%'));
			$('.cloud_2').css('right', (cloud_pos_2+'%'));
		}, 20);
	}

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

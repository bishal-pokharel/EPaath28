var imgpath = $ref + "/images/";

var soundAsset = $ref+"/sounds/"+$lang+'/';

var sound_1 = new buzz.sound(soundAsset+"s4_p1.ogg");
var sound_2 = new buzz.sound(soundAsset+"s4_p2.ogg");
var sound_3 = new buzz.sound(soundAsset+"s4_p3.ogg");

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust: true,
		contentblockadditionalclass : 'shore',
		uppertextblockadditionalclass : 'lesson_intro',
		uppertextblock : [{
			textdata : data.string.p4text1,
			textclass : 'lesson_intro_data'
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "number_scale",
				imgsrc : imgpath + "hill03.png",
			}],
		}],
		lowertextblockadditionalclass : 'scale_text',
		lowertextblock : [{
			textdata : '',
			textclass : 'scale_data sd_0',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_1',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_2',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_3',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_4',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_5',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_6',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_7',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_8',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_9',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_10',
		}],
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust: true,
		contentblockadditionalclass : 'shore',
		uppertextblockadditionalclass : 'lesson_intro',
		uppertextblock : [{
			textdata : data.string.p4text1,
			textclass : 'lesson_intro_data'
		},
		{
			textdata : data.string.p4text2,
			textclass : 'lesson_intro_data'
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "number_scale",
				imgsrc : imgpath + "hill03.png",
			}],
		}],
		lowertextblockadditionalclass : 'scale_text',
		lowertextblock : [{
			textdata : '',
			textclass : 'scale_data sd_0',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_1',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_2',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_3',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_4',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_5',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_6',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_7',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_8',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_9',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_10',
		}],
	},

	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust: true,
		contentblockadditionalclass : 'shore',
		uppertextblockadditionalclass : 'lesson_intro',
		uppertextblock : [{
			textdata : data.string.p4text1,
			textclass : 'lesson_intro_data'
		},
		{
			textdata : data.string.p4text2,
			textclass : 'lesson_intro_data'
		},
		{
			textdata : data.string.p4text3,
			textclass : 'lesson_intro_data'
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "number_scale",
				imgsrc : imgpath + "hill03.png",
			}],
		}],
		lowertextblockadditionalclass : 'scale_text',
		lowertextblock : [{
			textdata : '',
			textclass : 'scale_data sd_0',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_1',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_2',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_3',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_4',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_5',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_6',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_7',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_8',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_9',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_10',
		}],
	},

	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust: true,
		contentblockadditionalclass : 'shore',
		uppertextblockadditionalclass : 'full_text',
		uppertextblock : [{
			textdata : data.string.p4text4,
			textclass : 'text_qna',
			datahighlightflag : true,
			datahighlightcustomclass : 'number_highlight'
		},
		{
			textdata : '1',
			textclass : 'choice_1 choice'
		},
		{
			textdata : '2',
			textclass : 'choice_2 choice'
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "number_scale",
				imgsrc : imgpath + "hill03.png",
			}],
		}],
		lowertextblockadditionalclass : 'scale_text',
		lowertextblock : [{
			textdata : '',
			textclass : 'scale_data sd_0',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_1',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_2',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_3',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_4',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_5',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_6',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_7',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_8',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_9',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_10',
		}],
		extratextblock:[{
			textdata : '',
			textclass : 'correct_answer_tick_1'
		},{
			textdata : '',
			textclass : 'correct_answer_tick_2'
		},{
			textdata : '',
			textclass : 'wrong_answer_tick_1'
		},{
			textdata : '',
			textclass : 'wrong_answer_tick_2'
		}]
	},

	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust: true,
		contentblockadditionalclass : 'shore',
		uppertextblockadditionalclass : 'full_text',
		uppertextblock : [{
			textdata : data.string.p4text4,
			textclass : 'text_qna',
			datahighlightflag : true,
			datahighlightcustomclass : 'number_highlight'
		},
		{
			textdata : '1',
			textclass : 'choice_1 choice'
		},
		{
			textdata : '2',
			textclass : 'choice_2 choice'
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "number_scale",
				imgsrc : imgpath + "hill03.png",
			}],
		}],
		lowertextblockadditionalclass : 'scale_text',
		lowertextblock : [{
			textdata : '',
			textclass : 'scale_data sd_0',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_1',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_2',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_3',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_4',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_5',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_6',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_7',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_8',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_9',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_10',
		}],
		extratextblock:[{
			textdata : '',
			textclass : 'correct_answer_tick_1'
		},{
			textdata : '',
			textclass : 'correct_answer_tick_2'
		},{
			textdata : '',
			textclass : 'wrong_answer_tick_1'
		},{
			textdata : '',
			textclass : 'wrong_answer_tick_2'
		}]
	},

	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust: true,
		contentblockadditionalclass : 'shore',
		uppertextblockadditionalclass : 'full_text',
		uppertextblock : [{
			textdata : data.string.p4text4,
			textclass : 'text_qna',
			datahighlightflag : true,
			datahighlightcustomclass : 'number_highlight'
		},
		{
			textdata : '1',
			textclass : 'choice_1 choice'
		},
		{
			textdata : '2',
			textclass : 'choice_2 choice'
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "number_scale",
				imgsrc : imgpath + "hill03.png",
			}],
		}],
		lowertextblockadditionalclass : 'scale_text',
		lowertextblock : [{
			textdata : '',
			textclass : 'scale_data sd_0',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_1',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_2',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_3',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_4',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_5',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_6',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_7',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_8',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_9',
		},
		{
			textdata : '',
			textclass : 'scale_data sd_10',
		}],
		extratextblock:[{
			textdata : '',
			textclass : 'correct_answer_tick_1'
		},{
			textdata : '',
			textclass : 'correct_answer_tick_2'
		},{
			textdata : '',
			textclass : 'wrong_answer_tick_1'
		},{
			textdata : '',
			textclass : 'wrong_answer_tick_2'
		}]
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	var last_page = false;
	loadTimelineProgress($total_page, countNext + 1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationcontroller(islastpageflag) {
			typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
			if(countNext==4){
				ole.footerNotificationHandler.pageEndSetNotification();
			}
			if (countNext == 0 && $total_page != 1) {
					$nextBtn.show(0);
					$prevBtn.css('display', 'none');
			} else if ($total_page == 1) {
					$prevBtn.css('display', 'none');
					$nextBtn.css('display', 'none');

					// if lastpageflag is true
					islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
			} else if (countNext > 0 && countNext < $total_page - 1) {
					$nextBtn.show(0);
					$prevBtn.show(0);
			} else if (countNext == $total_page - 1) {
					$nextBtn.css('display', 'none');
					$prevBtn.show(0);

					// if lastpageflag is true
					// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
			}
	}
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch (countNext) {
		case 0:
			soundplayer(sound_1);
			fill_scale(100, '.sd_', 10);
			break;
		case 1:
			soundplayer(sound_2);
			fill_scale(100, '.sd_', 10);
			break;
		case 2:
			soundplayer(sound_3);
			fill_scale(100, '.sd_', 10);
			break;
		case 3:
			$nextBtn.hide(0);
			/* get a random variable and get maximum and minimum option to get correct rounded off
			 * choice and then set the correct answers with .html method and click function to see
			 * the correct answer is clicked or not	 */
			var random_no = ole.getRandom(1, 101, 999);
			var choices = rounded_to(random_no);
			if(random_no%50 == 0){
					random_no++;
			}
			$('.number_highlight').html(random_no);
			$('.choice_1').html(choices[0]);
			$('.choice_2').html(choices[1]);
			$('.choice_1').click(function(){
				choice_click('.choice_1', choices[2]);
			});
			$('.choice_2').click(function(){
				choice_click('.choice_2', !choices[2]);
			});
			fill_scale(choices[0], '.sd_', 10);
			break;
		case 4:
			$nextBtn.hide(0);
			var random_no = 450;
			var choices = rounded_to(random_no);
			$('.number_highlight').html(random_no);
			$('.choice_1').html(choices[0]);
			$('.choice_2').html(choices[1]);
			$('.choice_1').click(function(){
				choice_click('.choice_1', choices[2]);
			});
			$('.choice_2').click(function(){
				choice_click('.choice_2', !choices[2]);
			});
			fill_scale(choices[0], '.sd_', 10);
			break;
		case 5:
			$nextBtn.hide(0);
			var random_no = ole.getRandom(1, 101, 999);
			var choices = rounded_to(random_no);
			if(random_no%50 == 0){
					random_no--;
			}
			$('.number_highlight').html(random_no);
			$('.choice_1').html(choices[0]);
			$('.choice_2').html(choices[1]);
			$('.choice_1').click(function(){
				choice_click('.choice_1', choices[2]);
			});
			$('.choice_2').click(function(){
				choice_click('.choice_2', !choices[2]);
			});
			fill_scale(choices[0], '.sd_', 10);
			break;
		default:
			$nextBtn.show(0);
			break;
		}
	}

	function soundplayer(i,next){
		buzz.all().stop();
		i.play().bind("ended",function(){
			 if(!next) navigationcontroller();
		});
	}


	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		// navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	// function to add sound event on click
	function sound_caller(sound_box_class, sound_var){
		$(sound_box_class).click(function(){
			sound_var.play();
		});
	}


	$nextBtn.on("click", function() {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();

	function next_btn_caller (next_display) {
		if(!next_display) {
			return false;
		} else {
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
	}

	// answer has 3 parts, answer[0]=min, answer[1]=max and answer[2] is 1 if correct choice is min and 0 if it is incorrect
	function rounded_to(rounding_no){
		var answer = [0,0,0];
		if( rounding_no < 10) {
			answer = [0, 10, 0];
		} else if( rounding_no > 10 && rounding_no < 100) {
			answer[0] = Math.floor(rounding_no/10)*10;
			answer[1] = answer[0] + 10;
		} else if( rounding_no > 100 && rounding_no < 1000) {
			answer[0] = Math.floor(rounding_no/100)*100;
			answer[1] = answer[0] + 100;
		}
		if((rounding_no-answer[0])<(answer[1]-rounding_no)){
			answer[2]=1;
		}
		return answer;
	}

	// to get data to fill the scale in the hill
	function scale_data(min, incrementor){
		var answer = [0,0,0,0,0,0,0,0,0,0,0];
		for(var m = 0; m<=10; m++){
			answer[m] = min+m*incrementor;
		}
		return answer;
	}

	// to fill the scale in the hill with data
	function fill_scale(min, class_name, incrementor){
		var answer = scale_data(min, incrementor);
		for( var m = 0; m <=10; m++){
			var my_class = $(class_name+m);
			my_class.html(answer[m]);
		}
	}

	// to see if correct answer was clicked or not
	function choice_click(class_name, correct) {
		var choice;
		if(!correct){
			$(class_name).css({"background-color": "#BA6B82", 'pointer-events': 'none', 'border': 'none'});
			play_correct_incorrect_sound(0);
			var classnamechoice = class_name.replace(/\D/g, '');
			classnamechoice==2?choice=1:choice =2;
			$('.wrong_answer_tick_'+choice).show(0);
		} else {
			$(class_name).css({"background-color": "#6EB260", 'pointer-events': 'none', 'border': 'none'});
			$('.choice').css({'pointer-events': 'none'});
			var classnamechoice = class_name.replace(/\D/g, '');
			$('.correct_answer_tick_'+classnamechoice).show(0);
			play_correct_incorrect_sound(1);
			if(countNext == $total_page - 1){
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else {
				$nextBtn.show(0);
			}
		}
	}
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

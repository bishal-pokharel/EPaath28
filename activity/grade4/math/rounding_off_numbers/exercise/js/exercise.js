var imgpath = $ref + "/images/";
var no_of_draggable = 4; //no of draggable to display at a time
Array.prototype.shufflearray = function(){
  	var i = this.length, j, temp;
    while(--i > 0){
        j = Math.floor(Math.random() * (i+1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
	return this;
};


var soundAsset = $ref+"/sounds/"+$lang+'/';

var ex_ins = new buzz.sound(soundAsset+"ex_ins.ogg");

var content=[
	//1st slide
	{
  		contentnocenteradjust: true,
	  	uppertextblock: [{
	  		textdata: data.string.ex1,
	  		textclass: 'question'
  		}],
      extratextblock:[{
          textdata: data.string.play1text1,
          textclass: 'question1'
      }],
  		textblock: [
      {
  			textclass: "opt option1"
  		},{
  			textclass: "opt option2"
  		},{
  			textclass: "opt option3"
  		},{
  			textclass: "opt option4"
  		}]
	}
];

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 10;
  var corSrc = imgpath + "correct.png";
  var incorSrc = imgpath + "wrongicon.png";
	Handlebars.registerPartial("uppertextcontent", $("#uppertextcontent-partial").html());

	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	var score = 0;
	/*random scoreboard eggs*/
	var wrngClicked = [false, false, false, false, false, false, false, false, false, false];

	function twoDigit(multiplier){
		var correct = 0;
		var wrongClicked = false;

		var num = Math.floor(Math.random() * multiplier) + 1;
			console.log(num)
		if(multiplier === 99) {
			while(num%10 == 0) {
				num = Math.floor(Math.random() * multiplier) + 1;
			}
			$(".question span").html(num);

			var option1 = (Math.floor(num/10)) * 10;
			var option2 = (Math.ceil(num/10)) * 10;
			var option3 = Math.floor(Math.random() * multiplier) * 1;
			while(option3 == num || option3 == option1 || option3 == option2) {
				var option3 = Math.floor(Math.random() * multiplier) * 1;
			}

			var option4 = Math.floor(Math.random() * multiplier) * 1;
			while(option4 == num || option4 == option1 || option4 == option2 || option4 == option3) {
				var option4 = Math.floor(Math.random() * multiplier) * 1;
			}
		}else if(multiplier === 999){
			while(num%5 == 0 || num > 999 || num < 100) {
				num = Math.floor(Math.random() * multiplier) + 100;
				console.log(num)
			}
			$(".question span").html(num);

			var option1 = (Math.floor(num/10)) * 10;
			var option2 = (Math.ceil(num/10)) * 10;
			var option3 = Math.floor(Math.random() * multiplier) * 1;
			while(option3 == num || option3 == option1 || option3 == option2) {
				var option3 = Math.floor(Math.random() * multiplier) * 1;
			}

			var option4 = Math.floor(Math.random() * multiplier) * 1;
			while(option4 == num || option4 == option1 || option4 == option2 || option4 == option3) {
				var option4 = Math.floor(Math.random() * multiplier) * 1;
			}
		}else {
			console.log("hiiii");
			console.log(multiplier);
			while(num%5 != 0 || num%10 == 0 || num > 999 || num < 100) {
				num = Math.floor(Math.random() * multiplier) + 100;
				console.log(num)
			}
			$(".question span").html(num);

			var option1 = (Math.floor(num/10)) * 10;
			var option2 = (Math.ceil(num/10)) * 10;
			var option3 = Math.floor(Math.random() * multiplier) * 1;
			while(option3 == num || option3 == option1 || option3 == option2) {
				var option3 = Math.floor(Math.random() * multiplier) * 1;
			}

			var option4 = Math.floor(Math.random() * multiplier) * 1;
			while(option4 == num || option4 == option1 || option4 == option2 || option4 == option3) {
				var option4 = Math.floor(Math.random() * multiplier) * 1;
			}
		}

		// Checking Correct
		if((num - option1) < (option2 - num)){
			correct = option1;
		} else {
			correct = option2;
		}

		var optionArray = [option1, option2, option3, option4];
		optionArray.shufflearray();
		$(".option1").html(optionArray[0]);
		$(".option2").html(optionArray[1]);
		$(".option3").html(optionArray[2]);
		$(".option4").html(optionArray[3]);

		$(".opt").click(function() {
			if($(this).html() == correct.toString()) {
				play_correct_incorrect_sound(1);
				$(this).css({
					backgroundColor : "#4CAF50"
				});
        $(this).append("<img class='corincor right' src='"+corSrc+"' />")
				if(wrongClicked == false) {
					testin.update(true);
				}
				countNext++;
				$nextBtn.show(0);
				$(".opt").css({
					pointerEvents : "none"
				})
			}else {
				play_correct_incorrect_sound(0);
        $(this).append("<img class='corincor right' src='"+incorSrc+"' />")
				$(this).css({
					backgroundColor : "#D0553E"
				});
				testin.update(false);
				wrongClicked = true;
			}
		})
		return optionArray;
	}

	var testin = new EggTemplate();

 	testin.init(10);

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[0]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);
    if(countNext==0) soundplayer(ex_ins,1);

		/*generate question no at the beginning of question*/
    //
    // $(".opt").append("<img class='corincor right' src='"+corSrc+"' />");
    // $(".opt").append("<img class='corincor wrong' src='"+incorSrc+"' />");
		switch(countNext) {
			case 0:
			case 1:
				twoDigit(99);
			break;

			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
				twoDigit(999);
			break;

			case 7:
			case 8:
				twoDigit(995);
			break;

			case 9:
				twoDigit(995);
			break;
		}
	}

  function soundplayer(i,next){
		buzz.all().stop();
		i.play().bind("ended",function(){
			 if(!next) navigationcontroller();
		});
	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
			templateCaller();
			testin.gotoNext();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	//slide 0
	{
		contentblockadditionalclass: '',
		bgcolor: '#E6B8AF',
		fontcolor: '#5B0F00',
		optcolor: '#BC7C76',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.eins,
			textclass : 'instruction',
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : '',
			textclass : 'class1 options'
		},
		{
			textdata : '',
			textclass : 'class2 options'
		},
		{
			textdata : '',
			textclass : 'class3 options'
		},
		{
			textdata : '',
			textclass : 'class4 options'
		},
		],
	},
	//slide 1
	{
		contentblockadditionalclass: '',
		bgcolor: '#FCE5CD',
		fontcolor: '#5B0F00',
		optcolor: '#CBA681',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.eins,
			textclass : 'instruction',
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : '',
			textclass : 'class1 options'
		},
		{
			textdata : '',
			textclass : 'class2 options'
		},
		{
			textdata : '',
			textclass : 'class3 options'
		},
		{
			textdata : '',
			textclass : 'class4 options'
		},
		],
	},
	//slide2
	{
		contentblockadditionalclass: '',
		bgcolor: '#B6D7A8',
		fontcolor: '#274E13',
		optcolor: '#86A976',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.eins,
			textclass : 'instruction',
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : '',
			textclass : 'class1 options'
		},
		{
			textdata : '',
			textclass : 'class2 options'
		},
		{
			textdata : '',
			textclass : 'class3 options'
		},
		{
			textdata : '',
			textclass : 'class4 options'
		},
		],
	},
	//slide 3
	{
		contentblockadditionalclass: '',
		bgcolor: '#D0E0E3',
		fontcolor: '#0C343D',
		optcolor: '#98AFB3',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.eins1,
			textclass : 'instruction',
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : '',
			textclass : 'class1 options'
		},
		{
			textdata : '',
			textclass : 'class2 options'
		},
		{
			textdata : '',
			textclass : 'class3 options'
		},
		{
			textdata : '',
			textclass : 'class4 options'
		},
		],
	},
	//slide 4
	{
		contentblockadditionalclass: '',
		bgcolor: '#D9D2E9',
		fontcolor: '#20124D',
		optcolor: '#9F95B8',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.eins1,
			textclass : 'instruction',
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : '',
			textclass : 'class1 options'
		},
		{
			textdata : '',
			textclass : 'class2 options'
		},
		{
			textdata : '',
			textclass : 'class3 options'
		},
		{
			textdata : '',
			textclass : 'class4 options'
		},
		],
	},
	//slide 5
	{
		contentblockadditionalclass: '',
		bgcolor: '#EAD1DC',
		fontcolor: '#4C1130',
		optcolor: '#BB97A8',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.eins2,
			textclass : 'instruction',
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : '',
			textclass : 'class1 options'
		},
		{
			textdata : '',
			textclass : 'class2 options'
		},
		{
			textdata : '',
			textclass : 'class3 options'
		},
		{
			textdata : '',
			textclass : 'class4 options'
		},
		],
	},
	//slide 6
	{
		contentblockadditionalclass: '',
		bgcolor: '#DD7E6B',
		fontcolor: '#5B0F00',
		optcolor: '#AF5745',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.eins2,
			textclass : 'instruction',
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : '',
			textclass : 'class1 options'
		},
		{
			textdata : '',
			textclass : 'class2 options'
		},
		{
			textdata : '',
			textclass : 'class3 options'
		},
		{
			textdata : '',
			textclass : 'class4 options'
		},
		],
	},
	//slide 7
	{
		contentblockadditionalclass: '',
		bgcolor: '#F9CB9C',
		fontcolor: '#5B0F00',
		optcolor: '#C99663',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.eins,
			textclass : 'instruction',
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : '',
			textclass : 'class1 options'
		},
		{
			textdata : '',
			textclass : 'class2 options'
		},
		{
			textdata : '',
			textclass : 'class3 options'
		},
		{
			textdata : '',
			textclass : 'class4 options'
		},
		],
	},
	//slide 8
	{
		contentblockadditionalclass: '',
		bgcolor: '#FFE599',
		fontcolor: '#5B0F00',
		optcolor: '#CCA661',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.eins,
			textclass : 'instruction',
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : '',
			textclass : 'class1 options'
		},
		{
			textdata : '',
			textclass : 'class2 options'
		},
		{
			textdata : '',
			textclass : 'class3 options'
		},
		{
			textdata : '',
			textclass : 'class4 options'
		},
		],
	},
	//slide 9
	{
		contentblockadditionalclass: '',
		bgcolor: '#9FC5E8',
		fontcolor: '#073763',
		optcolor: '#638DB4',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.eins,
			textclass : 'instruction',
		}],
		optionsblock : [{
			textdata : '',
			textclass : 'class1 options'
		},
		{
			textdata : '',
			textclass : 'class2 options'
		},
		{
			textdata : '',
			textclass : 'class3 options'
		},
		{
			textdata : '',
			textclass : 'class4 options'
		},
		],
	},
];

// content.shufflearray();


$(function ()
{
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	var score = 0;
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			// sounds
			{id: "sound_a", src: soundAsset+"ex1.ogg"},
			{id: "sound_b", src: soundAsset+"ex2.ogg"},
			{id: "sound_c", src: soundAsset+"ex3.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}
	var rhino = new LampTemplate();
	rhino.init($total_page);

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);

		//randomize options
		var parent = $(".optionsblock");
		var divs = parent.children();

		var ques_count = ['1. ','2. ','3. ','4. ','5. ','6. ','7. ','8. ','9. ', '10. '];
		if($lang=='np'){
			ques_count = ['१. ', '२. ', '३. ', '४. ', '५. ', '६. ', '७. ', '८. ', '९. ', '१०. '];
		}
		$('#ques-no').html(ques_count[countNext]);
		while (divs.length) {
			parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
		if(countNext<$total_page){
			$('.contentblock').css('background-color', content[countNext].bgcolor);
			$('.options').css('background-color', content[countNext].optcolor);
			$('.options, .instruction').css('color', content[countNext].fontcolor);
		}
		var wrong_clicked = false;
		$(".options").click(function(){
			if($(this).hasClass("class1")){
				if(!wrong_clicked){
					rhino.update(true);
				}
				$(".options").css('pointer-events', 'none');
				$(this).css({
					'border': '3px solid #FCD172',
					'background-color': '#6EB260',
					'color': 'white'
				});
				$(this).parent().children('.cor').show(0);
				play_correct_incorrect_sound(1);
				createjs.Sound.stop();
				if(countNext != $total_page)
					$nextBtn.show(0);
			}
			else{
				if(!wrong_clicked){
					rhino.update(false);
				}
				$(this).css({
					'background-color': '#BA6B82',
					'pointer-events': 'none',
					'border': 'none'
				});
				$(this).parent().children('.incor').show(0);
				wrong_clicked = true;
				play_correct_incorrect_sound(0);
				createjs.Sound.stop();
			}
		});
		function make_blocks(num){
			var blockSize = Math.sqrt(num);
			if(blockSize%1!==0) blockSize = Math.ceil(blockSize);
			var startPosX = Math.floor(Math.random()*(10-blockSize))+1;
			var startPosY = Math.floor(Math.random()*(10-blockSize));
			var endPosX = (startPosX+blockSize);
			var endPosY = (startPosY+blockSize);
			var blockCount = 0;
			var blockOutput = [];
			for(var j = startPosY; j<endPosY; j++ ){
				for(var i = startPosX; i<endPosX; i++ ){
					if(blockCount<num){
						blockOutput.push('#rect-'+(10*j+i));
					}
					blockCount++;
				}
			}
			return blockOutput;
		}
		var rand_1, rand_2, rand_3, rand_4;
		function randGen1(){
			rand_1 = 1+Math.floor(Math.random()*9);
			rand_2 = 1+Math.floor(Math.random()*9);
			rand_3 = 1+Math.floor(Math.random()*9);
			rand_4 = 1+Math.floor(Math.random()*9);
			while(rand_2 == rand_1){
				rand_2 = 1+Math.floor(Math.random()*9);
			}
			while(rand_3 == rand_1 || rand_3 == rand_2){
				rand_3 = 1+Math.floor(Math.random()*9);
			}
			while(rand_4 == rand_1 || rand_4 == rand_2 || rand_4 == rand_3){
				rand_4 = 1+Math.floor(Math.random()*9);
			}
		}
		function randGen2(){
			rand_1 = 10+Math.floor(Math.random()*89);
			rand_2 = 10+Math.floor(Math.random()*89);
			rand_3 = 10+Math.floor(Math.random()*89);
			rand_4 = 10+Math.floor(Math.random()*89);
			while(rand_2 == rand_1){
				rand_2 = 10+Math.floor(Math.random()*89);
			}
			while(rand_3 == rand_1 || rand_3 == rand_2){
				rand_3 = 10+Math.floor(Math.random()*89);
			}
			while(rand_4 == rand_1 || rand_4 == rand_2 || rand_4 == rand_3){
				rand_4 = 10+Math.floor(Math.random()*89);
			}
		}
		function createGraph(){
			var s = Snap('#graph-svg');
			var rects = [];
			var svg = Snap.load(imgpath+'graph.svg', function ( loadedFragment ) {
				s.append(loadedFragment);
				var graphPlot = make_blocks(rand_1);
				for(var i=0; i<graphPlot.length; i++){
					rects.push(Snap.select(graphPlot[i]));
				}
				for(var i=0; i<graphPlot.length; i++){
					rects[i].attr({
						'fill-opacity': '1',
						'fill': content[countNext].optcolor
					});
				}
			} );
		}

		function createFraction(top, bottom){
			var fracStr = '<span class="fraction"><span class="top">'+top+'</span><span class="bottom">'+bottom+'</span></span>';
			return fracStr;
		}
		switch(countNext){
			case 0:
			sound_player("sound_a");
				randGen1();
				createGraph();
				$('.q-data').html(rand_1);
				$('.class1').html(rand_1+'%');
				$('.class2').html(rand_2+'%');
				$('.class3').html(rand_3+'%');
				$('.class4').html(rand_4+'%');
				break;
			case 1:
			case 2:
				randGen2();
				createGraph();
				$('.q-data').html(rand_1);
				$('.class1').html(rand_1+'%');
				$('.class2').html(rand_2+'%');
				$('.class3').html(rand_3+'%');
				$('.class4').html(rand_4+'%');
				break;
			case 3:
			sound_player("sound_c");
				randGen1();
				createGraph();
				$('.q-data').html(rand_1);
				$('.class1').html(createFraction(rand_1, 100));
				$('.class2').html(createFraction(rand_2, 100));
				$('.class3').html(createFraction(rand_3, 100));
				$('.class4').html(createFraction(rand_4, 100));
				break;
			case 4:
				randGen2();
				createGraph();
				$('.q-data').html(rand_1);
				$('.class1').html(createFraction(rand_1, 100));
				$('.class2').html(createFraction(rand_2, 100));
				$('.class3').html(createFraction(rand_3, 100));
				$('.class4').html(createFraction(rand_4, 100));
				break;
			case 5:
				sound_player("sound_b");
				randGen2();
				createGraph();
				$('.q-data').html(rand_1);
				$('.class1').html('0.'+rand_1);
				$('.class2').html('0.'+rand_2);
				$('.class3').html('0.'+rand_3);
				$('.class4').html('0.'+rand_4);
				break;
			case 6:
				randGen1();
				createGraph();
				$('.q-data').html(rand_1);
				$('.class1').html('0.0'+rand_1);
				$('.class2').html('0.0'+rand_2);
				$('.class3').html('0.0'+rand_3);
				$('.class4').html('0.0'+rand_4);
				break;
			case 7:
			sound_player("sound_a");
				randGen1();
				createGraph();
				$('.class1').html(rand_1+'%');
				$('.class2').html(rand_2+'%');
				$('.class3').html(rand_3+'%');
				$('.class4').html(rand_4+'%');
				break;
			case 8:
			case 9:
				randGen2();
				createGraph();
				$('.class1').html(rand_1+'%');
				$('.class2').html(rand_2+'%');
				$('.class3').html(rand_3+'%');
				$('.class4').html(rand_4+'%');
				break;
			default:
				break;
		}
	}

	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		if(countNext < 13){
			templateCaller();
			rhino.gotoNext();
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

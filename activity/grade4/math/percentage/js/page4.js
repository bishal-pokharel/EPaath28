var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide 1
	{
		//slide 0
		uppertextblock:[
		{
			textclass: "ole_temp_diytext",
			textdata: data.string.diy
		}
		],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "ole_temp_diyimg",
				imgsrc: "images/lokharke/2.png",
			}
			]
		}
		],
	},
	// slide 2
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			textdata: data.string.p3t8
		}
		],
		lowertextblock:[
		{
				textclass: "q1frac",
				splitintofractionsflag: true,
				textdata: data.string.p3_frac2
		},
		{
				textclass: "q1per",
				textdata: data.string.per
		},
		{
			textclass: "ole-template-check-btn-default submitbtn2",
			textdata: data.string.btn2
		}
		],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "hiddenmon sundarinc",
				imgsrc: "images/sundar/incorrect-2.png",
			},
			{
				imgclass: "hiddenmon sundarcor",
				imgsrc: "images/sundar/normal.png",
			}]
		}
		],
		inputbox:[
		{
			inputclass: "ole-template-input-box-default input1"
		}],
	},
	// slide 3
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			textdata: data.string.p3t8
		}
		],
		lowertextblock:[
		{
				textclass: "q1frac",
				splitintofractionsflag: true,
				textdata: data.string.p3_frac5
		},
		{
				textclass: "hinttext",
				splitintofractionsflag: true,
				textdata: data.string.p3_frac4
		},
		{
				textclass: "q1per",
				textdata: data.string.per
		},
		{
			textclass: "ole-template-check-btn-default submitbtn2",
			textdata: data.string.btn2
		}
		],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "hiddenmon sundarinc",
				imgsrc: "images/sundar/incorrect-2.png",
			},
			{
				imgclass: "hiddenmon sundarcor",
				imgsrc: "images/sundar/normal.png",
			}]
		}
		],
		inputbox:[
		{
			inputclass: "ole-template-input-box-default input1"
		}],
	},
	// slide 4
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			textdata: data.string.p3t8
		}
		],
		threblkcontainer:[{
			mainblk:[
			{
				subblock:[{
					subblkclass:"lftBlk",
					fractxt:[{
						textclass:"fractxt fractop",
						textdata:data.string.thr
					},{
						textclass:"fractxt fracBtm",
						textdata:data.string.hun
					}]
				},{
					subblkclass:"rghtBlk",
					dividedsubblk:[{
						dividedsubblkclass:"divBlk dbFst",
						fractxtsec:[{
							datahighlightflag:true,
							datahighlightcustomclass:"num_1_1",
							textclass:"fractxt frcTp",
							// textdata:data.string.fstmul
						},{
							datahighlightflag:true,
							datahighlightcustomclass:"num_1_2",
							textclass:"fractxt frcBtm",
							// textdata:data.string.secmul
						}]
					},{
						dividedsubblkclass:"divBlk dbSec",
						sbmt:true,
						submitclass:"sbmt-1",
						submitdata:data.string.btn2,
						inputbox:[{
							inputclass:"iput ip1_1",
						},{
							inputclass:"iput ip1_2",
						}],
						fractxtsec:[{
							textclass:"fractxt fractop ans_1_1 hidn",
							textdata:data.string.thr
						},{
							textclass:"fractxt fracBtm ans_1_2 hidn",
							textdata:data.string.hun
						}]
					}]

				}]
			},
			{
				mainblkclass:"mainBlkSec hidn",
				subblock:[{
					subblkclass:"lftBlk",
					blkLineclass:"hidn",
				},{
					subblkclass:"rghtBlk",
						blkLineclass:"hidn",
					dividedsubblk:[{
						dividedsubblkclass:"divBlk dbFst",
						sbmt:true,
						submitclass:"sbmt-2",
						submitdata:data.string.btn2,
						inputbox:[{
							inputclass:"iput ip2_1",
						},{
							inputclass:"iput ip2_2",
						}],
						fractxtsec:[{
							textclass:"fractxt fractop ans_2_1 hidn",
							textdata:data.string.thr
						},{
							textclass:"fractxt fracBtm ans_2_2 hidn",
							textdata:data.string.hun
						}]
					},{
						blkLineclass:"hidn",
							dividedsubblkclass:"divBlk dbSec",
					}]

				}]
			},
			{
				mainblkclass:"mainBlkThird hidn",
				subblock:[{
					subblkclass:"lftBlk",
					blkLineclass:"hidn",
				},{
					subblkclass:"rghtBlk",
					blkLineclass:"hidn",
					dividedsubblk:[{
						blkLineclass:"hidn",
						dividedsubblkclass:"divBlk dbFst prcnt",
						sbmt:true,
						submitclass:"sbmt-3",
						submitdata:data.string.btn2,
						inputbox:[{
							inputclass:"iput ip3_1",
						}],
						fractxtsec:[{
							textclass:"fractxt fractop ans_3_1 hidn",
							textdata:data.string.thr
						},{
							textclass:"fractxt fracBtm ans_3_2 hidn",
							textdata:data.string.hun
						}]
					},{
						blkLineclass:"hidn",
						dividedsubblkclass:"divBlk dbSec",
					}]

				}]
			}
		]
		}]
	},
	// slide 5
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			textdata: data.string.p3t8
		}
		],
		threblkcontainer:[{
			mainblk:[
			{
				subblock:[{
					subblkclass:"lftBlk",
					fractxt:[{
						textclass:"fractxt fractop",
						textdata:data.string.thr
					},{
						textclass:"fractxt fracBtm",
						textdata:data.string.hun
					}]
				},{
					subblkclass:"rghtBlk",
					dividedsubblk:[{
						dividedsubblkclass:"divBlk dbFst",
						fractxtsec:[{
							datahighlightflag:true,
							datahighlightcustomclass:"num_1_1",
							textclass:"fractxt frcTp",
							// textdata:data.string.fstmul
						},{
							datahighlightflag:true,
							datahighlightcustomclass:"num_1_2",
							textclass:"fractxt frcBtm",
							// textdata:data.string.secmul
						}]
					},{
						dividedsubblkclass:"divBlk dbSec",
						sbmt:true,
						submitclass:"sbmt-1",
						submitdata:data.string.btn2,
						inputbox:[{
							inputclass:"iput ip1_1",
						},{
							inputclass:"iput ip1_2",
						}],
						fractxtsec:[{
							textclass:"fractxt fractop ans_1_1 hidn",
							textdata:data.string.thr
						},{
							textclass:"fractxt fracBtm ans_1_2 hidn",
							textdata:data.string.hun
						}]
					}]

				}]
			},
			{
				mainblkclass:"mainBlkSec hidn",
				subblock:[{
					subblkclass:"lftBlk",
					blkLineclass:"hidn",
				},{
					subblkclass:"rghtBlk",
						blkLineclass:"hidn",
					dividedsubblk:[{
						dividedsubblkclass:"divBlk dbFst",
						sbmt:true,
						submitclass:"sbmt-2",
						submitdata:data.string.btn2,
						inputbox:[{
							inputclass:"iput ip2_1",
						},{
							inputclass:"iput ip2_2",
						}],
						fractxtsec:[{
							textclass:"fractxt fractop ans_2_1 hidn",
							textdata:data.string.thr
						},{
							textclass:"fractxt fracBtm ans_2_2 hidn",
							textdata:data.string.hun
						}]
					},{
						blkLineclass:"hidn",
							dividedsubblkclass:"divBlk dbSec",
					}]

				}]
			},
			{
				mainblkclass:"mainBlkThird hidn",
				subblock:[{
					subblkclass:"lftBlk",
					blkLineclass:"hidn",
				},{
					subblkclass:"rghtBlk",
					blkLineclass:"hidn",
					dividedsubblk:[{
						blkLineclass:"hidn",
						dividedsubblkclass:"divBlk dbFst prcnt",
						sbmt:true,
						submitclass:"sbmt-3",
						submitdata:data.string.btn2,
						inputbox:[{
							inputclass:"iput ip3_1",
						}],
						fractxtsec:[{
							textclass:"fractxt fractop ans_3_1 hidn",
							textdata:data.string.thr
						},{
							textclass:"fractxt fracBtm ans_3_2 hidn",
							textdata:data.string.hun
						}]
					},{
						blkLineclass:"hidn",
						dividedsubblkclass:"divBlk dbSec",
					}]

				}]
			}
		]
		}]
	}
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			// sounds
			{id: "sound_1", src: soundAsset+"s4_p2.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

   	Handlebars.registerPartial("fractioncontent", $("#fractioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	function navigationcontroller(islastpageflag){
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;

	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
	    splitintofractions($board);




			function corFunctionTwcls(class1,class2,submitCLs,ans2){
				$(class1).removeClass("incorrectclass");
				$(class2).removeClass("incorrectclass");
				if(ans2){
					play_correct_incorrect_sound(1);
					$(class1).attr("disabled",'true').addClass("correctclass");
					$(class2).attr("disabled",'true').addClass("correctclass");
					$(submitCLs).hide(0);
				}else{
					$(class1).attr("disabled",'true').addClass("correctclass");
					play_correct_incorrect_sound(0);
				}
			}
			function IncorFunctionTwcls(class1,class2,ans2){
				play_correct_incorrect_sound(0);
				if(ans2){
					$(class1).addClass("incorrectclass");
					$(class2).addClass("incorrectclass");
				}else {
					$(class1).addClass("incorrectclass");
				}
			}

	    switch(countNext){
				case 0:
					play_diy_audio();
					nav_button_controls(2000);
				break;
				case 1:
	    	case 2:
					sound_nav("sound_"+countNext);
					var denumArray = [4,5,10,20,25,50,100];
					var DenNum = Math.floor(Math.random()*(7-0)+1);
					var numerNum = Math.floor(Math.random()*(100-0)+1);
					// while(numerNum>denumArray[DenNum]){
					// 	var numerNum = Math.floor(Math.random()*(100-0)+1);
					// }
					console.log(numerNum+" "+denumArray[DenNum]);
					$(".top").html(numerNum);

					// $(".bottom").html(denumArray[DenNum]);
	    		$(".input1").keypress(function (e) {
			 		if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {
			 			return false;
			 		}
			 		if(e.which == 13) {
				        $(".submitbtn2").trigger("click");
					}
			 	});

			 	$(".submitbtn2").click(function(){
					if($(".input1").val() == numerNum){
						createjs.Sound.stop();
						$(".input1").removeClass("ole-template-input-box-default-incorrect").addClass("ole-template-input-box-default-correct");
						$(".submitbtn2").removeClass("ole-template-check-btn-default-incorrect").addClass("ole-template-check-btn-default-correct");
						$(".sundarinc").hide(0);
						$(".sundarcor").show(0);
						nav_button_controls(200);
						play_correct_incorrect_sound(1);
					}
					else{
						createjs.Sound.stop();
						$(".input1").addClass("ole-template-input-box-default-incorrect");
						$(".submitbtn2").addClass("ole-template-check-btn-default-incorrect");
						$(".sundarcor").hide(0);
						$(".sundarinc").show(0);
						play_correct_incorrect_sound(0);
					}
				});
	    	break;
				case 3:
				case 4:
					var denumArray = [4,5,10,20,25,50,100];
					var DenNum = Math.floor(Math.random()*(6-0+1))+0;
					var numerNum = Math.floor(Math.random()*(100-0+1))+1;
					while(numerNum>denumArray[DenNum]){
						var numerNum = Math.floor(Math.random()*(100-0+1))+1;
					}
					// console.log(numerNum+" "+DenNum+" "+denumArray[DenNum]);

					$(".fractop").html(numerNum);
					$(".fracBtm").html(denumArray[DenNum]);
					$(".frcTp").html(numerNum+" x ");
					$(".frcBtm").html(denumArray[DenNum]+" x ");

					var ans1 = 100/denumArray[DenNum];

					var ans2 = numerNum *(100/denumArray[DenNum]);
					// var ans3 = (numerNum==denumArray[DenNum])?1:ans2;
					var ans3 =ans2;
					$(".prcnt").append("<p class='prcntclas'>"+"%"+"</p>");
					$(".lftBlk").append("<p class='eqlcls'>"+"="+"</p>")
					console.log(ans1+" "+ans2+" "+ans3);

					// console.log(ans1);
					$(".sbmt-1").click(function(){

						var inp1 = $(".ip1_1").val();
						var inp2 = $(".ip1_2").val();//&& (inp2==ans1)
						if(inp1==ans1){
							corFunctionTwcls(".ip1_1",".ip1_1",".sbmt-1",false);
							if(inp2==ans1){
								corFunctionTwcls(".ip1_1",".ip1_2",".sbmt-1",true);
								$(".mainBlkSec").show();
							}else{
								$(".ip1_2").addClass("incorrectclass");
							}
						}else if (inp2==ans1) {
							corFunctionTwcls(".ip1_2",".ip1_2",".sbmt-1",false);
							IncorFunctionTwcls(".ip1_1",".ip1_1",false);
						}else{
							IncorFunctionTwcls(".ip1_1",".ip1_2",true);
							$(".ip1_1, .ip1_2").addClass("incorrectclass");
							play_correct_incorrect_sound(0);
						}
					});

					$(".sbmt-2").click(function(){
						var inp1 = $(".ip2_1").val();
						var inp2 = $(".ip2_2").val();
						if(inp1==ans2){
							corFunctionTwcls(".ip2_1",".ip2_2",".sbmt-1",false);
							if(inp2==100){
								corFunctionTwcls(".ip2_1",".ip2_2",".sbmt-1",true);
								$(".mainBlkThird").show();
								$(".sbmt-2").hide(0);
							}else{
								$(".ip2_2").addClass("incorrectclass");
							}
						}else if (inp2==100) {
							corFunctionTwcls(".ip2_2",".ip2_2",".sbmt-1",false);
							IncorFunctionTwcls(".ip2_1",".ip2_1",false);
						}else{
							// IncorFunctionTwcls(".ip2_1",".ip1_2",true);
							$(".ip2_1, .ip2_2").addClass("incorrectclass");
							play_correct_incorrect_sound(0);
						}
					});

					$(".sbmt-3").click(function(){
						var inp1 = $(".ip3_1").val();
						if(inp1==ans3){
							corFunctionTwcls(".ip3_1",".ip3_2",".sbmt-3",true);
							nav_button_controls(200);
						}else{
							IncorFunctionTwcls(".ip3_1",".ip2_2",false);
						}
					});

				break;
				default:
	    }
	}


	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_nav(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}


	function templateCaller(){
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		// if(countNext<=6)
		// navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
	total_page = content.length;
	templateCaller();
});
/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

    /*===== This function splits the string in data into convential fraction used in mathematics =====*/
    function splitintofractions($splitinside){
   		typeof $splitinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
       	if($splitintofractions.length > 0){
       		$.each($splitintofractions, function(index, value){
	        	$this = $(this);
	        	var tobesplitfraction = $this.html();
	        	if($this.hasClass('fraction')){
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
	        	}else{
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
	        	}


				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
	        	$this.html(tobesplitfraction);
	        });
       	}
   	}
   	/*===== split into fractions end =====*/

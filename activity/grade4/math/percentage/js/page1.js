var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	{
		//slide 0
		imageblock : [
			{
			imagelabels:[{
				imagelabelclass: "chapter",
				imagelabeldata: data.string.chtitle
			}],
			imagetoshow : [{
					imgclass : "cvrpage",
					imgsrc : imgpath + "cover_page.png"
			}],
			}
		],
	 },
	{
		//slide 1
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock fadein",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p1t1
		}
		],
	},
	{
		//slide 2
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p1t1
		}
		],
		lowertextblock:[
		{
			textclass: "ole_translate sidetext1 fadein",
			textdata: data.string.p1t2
		}
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100 fadein",
				flexblock:[
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
				]
			}
		]
	},
	{
		//slide 3
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			textclass:"changeText",
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p1t1
		}
		],
		lowertextblock:[
		// {
		// 	textclass: "ole_translate sidetext1",
		// 	textdata: data.string.p1t2
		// },
		{
			textclass: "ole_translate sidetext2 fadein",
			textdata: data.string.p1t3
		},
		{
			textclass: "ole_translate ole-template-check-btn-default submitbtn",
			textdata: data.string.btn
		},
		{
			textclass: "ole-template-check-btn-default submitbtn2",
			textdata: data.string.btn
		}
		],
		inputbox:[
		{
			inputclass: "ole-template-input-box-default input1"
		}],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "hiddenmon sundarinc",
				imgsrc: "images/sundar/incorrect-2.png",
			},
			{
				imgclass: "hiddenmon sundarcor",
				imgsrc: "images/sundar/normal.png",
			}]
		}
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100 flexglow",
				flexblock:[
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						]
					},
				]
			}
		]
	},
	{
		//slide 4
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p1t9
		}
		],
		lowertextblock:[
		{
			splitintofractionsflag: true,
			textclass: "ole_translate sidetext2 fadein",
			textdata: data.string.p1t8
		},
		{
			textclass: "ole_translate sidetext3 fadein fadeinlast",
			textdata: data.string.p1t10
		},
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",
			}
		]
	},
	//slide 5
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p1t9
		}
		],
		lowertextblock:[
		{
			textclass: "ole_translate sidetext2 fadein",
			textdata: data.string.p1t11
		}
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",
			}
		]
	},
	{
		//slide 6
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p1t9
		}
		],
		lowertextblock:[
		{
			textclass: "ole_translate sidetext2 fadein",
			textdata: data.string.p1t6
		},
		{
			textclass: "ole_translate sidetext4 fadein",
			textdata: data.string.p1t7
		},
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",
			}
		]
	},
	// {
	// 	//slide 7
	// 	contentblockadditionalclass: "ole_temp_bluecre_background1",
	// 	headerblockadditionalclass: "ole_temp_bluecre_header",
	// 	headerblock:[
	// 	{
	// 		textdata : data.lesson.chapter
	// 	}
	// 	],
	// 	uppertextblockadditionalclass: "ole_temp_uppertextblock",
	// 	uppertextblock:[
	// 	{
	// 		datahighlightflag: true,
	// 		datahighlightcustomclass: "letshigh",
	// 		textdata: data.string.p1t9
	// 	}
	// 	],
	// 	lowertextblock:[
	// 	{
	// 		textclass: "ole_translate sidetext2 fadeout",
	// 		textdata: data.string.p1t7
	// 	},
	// 	{
	// 		textclass: "ole_translate sidetext4 finalmove",
	// 		textdata: data.string.p1t6
	// 	},
	// 	],
	// 	flexblockcontainers: [
	// 		{
	// 			flexblockadditionalclass: "flexcontainerblock100",
	// 		}
	// 	]
	// },

];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;
	var $flexshifter;
	var ClickCount = 0;

	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			// sounds
			{id: "sound_0", src: soundAsset+"s1_p1.ogg"},
			{id: "sound_1a", src: soundAsset+"s1_p2_1.ogg"},
			{id: "sound_1b", src: soundAsset+"s1_p2_2.ogg"},
			{id: "sound_2", src: soundAsset+"s1_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s1_p4.ogg"},
			{id: "sound_5", src: soundAsset+"s1_p6.ogg"},
			{id: "sound_6", src: soundAsset+"s1_p7.ogg"},
			{id: "sound_7", src: soundAsset+"s1_p8.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	function navigationcontroller(islastpageflag){
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;

	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		splitintofractions($board);
		vocabcontroller.findwords(countNext);

		switch(countNext){
			case 1:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_1a");
			current_sound.play();
			current_sound.on('complete', function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_1b");
			current_sound.play();
			current_sound.on('complete', function(){
				nav_button_controls(0);
			});
		});
			break;
			case 3:
			sound_nav("sound_"+countNext);
				ClickCount = 0;
				$(".submitbtn").hide(0);
				$(".submitbtn2").hide(0);
				$(".hiddenmon").hide(0);
				$(".input1").hide(0);
				$nextBtn.hide(0);
				$(".rownormal").click(function(){
					$(this).toggleClass("clicked");
					if($(this).hasClass("clicked")){
						ClickCount++;
						$(this).addClass("selectthis");
					}
					else{
						ClickCount--;
						$(this).removeClass("selectthis");
					}
					showhidebtn();
				});

				function showhidebtn(){
					if(ClickCount > 0)
						$(".submitbtn").show(0);
					else
						$(".submitbtn").hide(0);
				}

				$(".input1").keypress(function (e) {
			 		if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {
			 			return false;
			 		}
			 		if(e.which == 13) {
				        $(".submitbtn2").trigger("click");
					}
			 	});

				$(".submitbtn").click(function(){
					$(".changeText").html(data.string.p1t1_1);
					$(".rownormal").removeClass("activate").unbind( "click");
					$(".sidetext1").text(data.string.p1t4);
					$(".sidetext2").hide(0);
					$(".input1").show(0);
					$(".submitbtn2").show(0);
					$(this).hide(0);
				});

				$(".submitbtn2").click(function(){
					if($(".input1").val() == ClickCount){
						createjs.Sound.stop();
						$(".input1").removeClass("ole-template-input-box-default-incorrect").addClass("ole-template-input-box-default-correct");
						$(".submitbtn2").removeClass("ole-template-check-btn-default-incorrect").addClass("ole-template-check-btn-default-correct");
						$(".sundarinc").hide(0);
						$(".sundarcor").show(0);
						nav_button_controls(200);
						play_correct_incorrect_sound(1);
					}
					else{
						createjs.Sound.stop();
						$(".input1").addClass("ole-template-input-box-default-incorrect");
						$(".submitbtn2").addClass("ole-template-check-btn-default-incorrect");
						$(".sundarcor").hide(0);
						$(".sundarinc").show(0);
						play_correct_incorrect_sound(0);
					}
				});
			break;
			case 4:
				$(".flexcontainerblock100").html('');
				$(".flexcontainerblock100").append($flexshifter);
				$(".flexcontainerblock100").append("<p class='flextext fadein'>"+ ClickCount +"%</p>");
				$(".numPass").html(ClickCount);
				nav_button_controls(1000);
			break;
			case 5:
				sound_player("sound_"+countNext);
				$(".flexcontainerblock100").html('');
				$(".flexcontainerblock100").append($flexshifter);
				$(".flexcontainerblock100").append("<p class='flextext'>"+ ClickCount +"%</p>");
				$(".numPass").html(ClickCount);
			break;
			case 6:
				sound_player("sound_"+countNext);
				$(".flexcontainerblock100").html('');
				$(".flexcontainerblock100").append($flexshifter);
				$(".flexcontainerblock100").append("<p class='flextext'>"+ ClickCount +"%</p>");
				$(".numPass").html(ClickCount);
			break;
			case 7:
				sound_player("sound_"+countNext);
				$(".flexcontainerblock100").html('');
				$(".flexcontainerblock100").append($flexshifter);
				$(".flexcontainerblock100").append("<p class='flextext'>"+ ClickCount +"%</p>");
				$(".numPass").html(ClickCount);
			break;
			default:
			sound_player("sound_"+countNext);
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(0);
		});
	}
	function sound_nav(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}


	function templateCaller(){
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function(){
		if(countNext == 3){
			$flexshifter = $(".flexcontainerblock100").children().clone();
		}
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
	total_page = content.length;
	templateCaller();
});

 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightstarttag2;
        var texthighlightstarttag3;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
        	$.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
            (stylerulename = $(this).attr("data-highlightcustomclass")) :
            (stylerulename = "parsedstring") ;

            $(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
            (stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
            (stylerulename2 = "parsedstring2") ;

            $(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
            (stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
            (stylerulename3 = "parsedstring3") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
            texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
        });
        }
    }
    /*=====  End of data highlight function  ======*/

		/*===== This function splits the string in data into convential fraction used in mathematics =====*/
		function splitintofractions($splitinside){
			typeof $splitinside !== "object" ?
				alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
				null ;

				var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
				if($splitintofractions.length > 0){
					$.each($splitintofractions, function(index, value){
						$this = $(this);
						var tobesplitfraction = $this.html();
						if($this.hasClass('fraction')){
							tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
							tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
						}else{
							tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
							tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
						}


				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
						$this.html(tobesplitfraction);
					});
				}
		}
		/*===== split into fractions end =====*/

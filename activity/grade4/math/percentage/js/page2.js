var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	{
		//slide 0
		uppertextblock:[
		{
			textclass: "ole_temp_diytext",
			textdata: data.string.diy
		}
		],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "ole_temp_diyimg",
				imgsrc: "images/lokharke/2.png",
			}
			]
		}
		],
	},
	{
		//slide 1
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p2t1
		}
		],
		lowertextblock:[
		{
			textclass: "ole-template-check-btn-default submitbtn2",
			textdata: data.string.btn
		},
		{
			textclass: "pericon",
			textdata: data.string.per
		}
		],
		inputbox:[
		{
			inputclass: "ole-template-input-box-default input1"
		}],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "hiddenmon sundarinc",
				imgsrc: "images/sundar/incorrect-2.png",
			},
			{
				imgclass: "hiddenmon sundarcor",
				imgsrc: "images/sundar/normal.png",
			}]
		}
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",
				flexblock:[
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal ",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
				]
			}
		]
	},
	{
		//slide 2
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p2t1
		}
		],
		lowertextblock:[
		{
			textclass: "ole-template-check-btn-default submitbtn2",
			textdata: data.string.btn
		},
		{
			textclass: "pericon",
			textdata: data.string.per
		}
		],
		inputbox:[
		{
			inputclass: "ole-template-input-box-default input1"
		}],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "hiddenmon sundarinc",
				imgsrc: "images/sundar/incorrect-2.png",
			},
			{
				imgclass: "hiddenmon sundarcor",
				imgsrc: "images/sundar/normal.png",
			}]
		}
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",
				flexblock:[
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						]
					},
				]
			}
		]
	},
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			// sounds
			{id: "sound_1", src: soundAsset+"s2_p2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	function navigationcontroller(islastpageflag){
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;

	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		switch(countNext){
			case 0:
			play_diy_audio();
			nav_button_controls(2000);
			break;
			case 1:
			sound_nav("sound_1");
				$(".input1").keypress(function (e) {
			 		if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {
			 			return false;
			 		}
			 		if(e.which == 13) {
				        $(".submitbtn2").trigger("click");
					}
			 	});

				$(".submitbtn2").click(function(){
					if($(".input1").val() == 12){
						createjs.Sound.stop();
						$(".input1").removeClass("ole-template-input-box-default-incorrect").addClass("ole-template-input-box-default-correct");
						$(".submitbtn2").removeClass("ole-template-check-btn-default-incorrect").addClass("ole-template-check-btn-default-correct");
						$(".sundarinc").hide(0);
						$(".sundarcor").show(0);
						nav_button_controls(200);
						play_correct_incorrect_sound(1);
					}
					else{
						createjs.Sound.stop();
						$(".input1").addClass("ole-template-input-box-default-incorrect");
						$(".submitbtn2").addClass("ole-template-check-btn-default-incorrect");
						$(".sundarcor").hide(0);
						$(".sundarinc").show(0);
						play_correct_incorrect_sound(0);
					}
				});
			break;
			case 2:
				$(".input1").keypress(function (e) {
			 		if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {
			 			return false;
			 		}
			 		if(e.which == 13) {
				        $(".submitbtn2").trigger("click");
					}
			 	});

				$(".submitbtn2").click(function(){
					if($(".input1").val() == 15){
						createjs.Sound.stop();
						$(".input1").removeClass("ole-template-input-box-default-incorrect").addClass("ole-template-input-box-default-correct");
						$(".submitbtn2").removeClass("ole-template-check-btn-default-incorrect").addClass("ole-template-check-btn-default-correct");
						$(".sundarinc").hide(0);
						$(".sundarcor").show(0);
						nav_button_controls(200);
						play_correct_incorrect_sound(1);
					}
					else{
						createjs.Sound.stop();
						$(".input1").addClass("ole-template-input-box-default-incorrect");
						$(".submitbtn2").addClass("ole-template-check-btn-default-incorrect");
						$(".sundarcor").hide(0);
						$(".sundarinc").show(0);
						play_correct_incorrect_sound(0);
					}
				});
			break;
		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_nav(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}


	function templateCaller(){
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		if(countNext == 0)
		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function(){
		if(countNext == 3){
			$flexshifter = $(".flexcontainerblock100").children().clone();
		}
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
	total_page = content.length;
	templateCaller();
});

 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightstarttag2;
        var texthighlightstarttag3;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
        	$.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
            (stylerulename = $(this).attr("data-highlightcustomclass")) :
            (stylerulename = "parsedstring") ;

            $(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
            (stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
            (stylerulename2 = "parsedstring2") ;

            $(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
            (stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
            (stylerulename3 = "parsedstring3") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
            texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
        });
        }
    }
    /*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	{
		//slide 0
		contentblockcenteradjust: true,
		uppertextblock:[
		{
			textclass: "chapter fadein",
			textdata: data.string.p3t1
		}
		],
	},
	{
		//slide 1
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textclass: "ole_translate lefttext fadein",
			textdata: data.string.p3t2
		},
		{
			textclass: "ole_translate leftbig fadein",
			textdata: data.string.divicon
		}
		],
	},
	{
		//slide 2
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textclass: "ole_translate lefttext",
			textdata: data.string.p3t2
		},
		{
			textclass: "ole_translate leftbig",
			textdata: data.string.divicon
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textclass: "ole_translate righttext fadein",
			textdata: data.string.p3t3
		},
		{
			textclass: "ole_translate rightbig fadein",
			textdata: data.string.hund
		}
		],
	},
	{
		//slide 3
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textclass: "ole_translate lefttext",
			textdata: data.string.p3t2
		},
		{
			textclass: "ole_translate leftbig",
			textdata: data.string.divicon
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textclass: "ole_translate righttext",
			textdata: data.string.p3t3
		},
		{
			textclass: "ole_translate rightbig",
			textdata: data.string.hund
		},
		{

            datahighlightflag: true,
            datahighlightcustomclass: "letshigh",
			textclass: "ole_translate p3middle fadein",
			textdata: data.string.p3t4
		}
		],
	},
	{
		//slide 4
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textclass: "ole_translate lefttext",
			textdata: data.string.p3t2
		},
		{
			textclass: "ole_translate leftbig",
			textdata: data.string.divicon
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textclass: "ole_translate righttext",
			textdata: data.string.p3t3
		},
		{
			textclass: "ole_translate rightbig",
			textdata: data.string.hund
		},
		{

      datahighlightflag: true,
      datahighlightcustomclass: "letshigh",
			textclass: "ole_translate p3middle",
			textdata: data.string.p3t4_sec
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh2",
			textclass: "ole_translate eqtext fadein",
			textdata: data.string.p3t7
		},
		{
			splitintofractionsflag: true,
			textclass: "frac-4 fadein",
			textdata: data.string.p3_frac1
		},
		],
	},
	{
		//slide 5
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblock:[
		{
			splitintofractionsflag: true,
			textclass: " new-txt1",
			textdata: data.string.p3t9
		},
		{
			textclass: " new-txt2",
			textdata: data.string.p3t10
		}
		],
	},
	{
		//slide 6
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblock:[
		{

			textclass: " new-txt1",
			textdata: data.string.p3t11
		},
		{
			textclass: " new-txt2",
			textdata: data.string.p3t12
		}
		]
	},
	//slide 7
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblock:[
		{

			textclass: " new-txt1",
			textdata: data.string.p3t11
		},
		{
			textclass: " new-txt2",
			textdata: data.string.p3t12
		},{
			splitintofractionsflag: true,
			textclass: " new-txt3",
			textdata: data.string.p3t13
		}
		]
	},
	{
		//slide 8
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "border-num",
			splitintofractionsflag: true,
			textclass: " new-txt4",
			textdata: data.string.p3t14
		},{
			textclass: " new-txt5",
			textdata: data.string.p3t17
		},{
			textclass: " new-txt4 cli nume",
			textdata: data.string.n3
		},{
			textclass: " new-txt4 nume1",
			textdata: ""
		},{
			textclass: " new-txt4 cli nume2 correct",
			textdata: data.string.n4
		}
		]

	},
	{
		//slide 9
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "color-red",
			// splitintofractionsflag: true,
			textclass: " new-txt1",
			textdata: data.string.p3t15
		},{
			datahighlightflag: true,
			datahighlightcustomclass: "border-num",
			splitintofractionsflag: true,
			textclass: " new-txt4",
			textdata: data.string.fr3_4
		},{
			textclass: " new-txt6",
			textdata: data.string.p3t18
		}
	],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optcontainerextra:"opti opti1",
				optaddclass:"",
				optdata:data.string.n1
			},{
					optcontainerextra:"opti opti2 correct",
				optaddclass:"",
				optdata:data.string.n25
			},{
				optcontainerextra:"opti opti3 ",
				optaddclass:"",
				optdata:data.string.n50
			}]
		}]
	},{
		//slide 10
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "color-red",
			textclass: " new-txt1",
			textdata: data.string.p3t19
		},{
			datahighlightflag: true,
			datahighlightcustomclass: "nxteqn",
			splitintofractionsflag: true,
			textclass: " new-txt4",
			textdata: data.string.fr3_4_2
		}
	],
},{
	//slide 11
	contentblockadditionalclass: "ole_temp_bluecre_background1",
	headerblockadditionalclass: "ole_temp_bluecre_header",
	headerblock:[
	{
		textdata : data.lesson.chapter
	}
	],
	uppertextblock:[
	{
		datahighlightflag: true,
		datahighlightcustomclass: "color-red",
		textclass: " new-txt1",
		textdata: data.string.p3t20
	},{
		datahighlightflag: true,
		datahighlightcustomclass: "border-num",
		splitintofractionsflag: true,
		textclass: " new-txt4",
		textdata: data.string.fr3_4_3
	},{
		datahighlightflag: true,
		datahighlightcustomclass: "color-red",
		textclass: " new-txt7 fadeinx",
		textdata: data.string.p3t21
	}
]
},{
	//slide 12
	contentblockadditionalclass: "ole_temp_bluecre_background1",
	headerblockadditionalclass: "ole_temp_bluecre_header",
	headerblock:[
	{
		textdata : data.lesson.chapter
	}
	],
	uppertextblock:[
	{
		datahighlightflag: true,
		datahighlightcustomclass: "border-num",
		splitintofractionsflag: true,
		textclass: " new-txt4",
		textdata: data.string.fr3_4_3
	},{
		splitintofractionsflag: true,
		textclass: " new-txt7",
		textdata: data.string.p3t22
	}
]
},{
	//slide 13
	contentblockadditionalclass: "ole_temp_bluecre_background1",
	headerblockadditionalclass: "ole_temp_bluecre_header",
	headerblock:[
	{
		textdata : data.lesson.chapter
	}
	],
	uppertextblock:[
	{
		datahighlightflag: true,
		datahighlightcustomclass: "color-orange",
		textclass: " new-txt1",
		textdata: data.string.p3t23
	},{
		splitintofractionsflag: true,
		textclass: " new-txt4",
		textdata: data.string.fr7_5
	}
]
},{
	//slide 14
	contentblockadditionalclass: "ole_temp_bluecre_background1",
	headerblockadditionalclass: "ole_temp_bluecre_header",
	headerblock:[
	{
		textdata : data.lesson.chapter
	}
	],
	uppertextblock:[
	{
		datahighlightflag: true,
		datahighlightcustomclass: "color-orange",
		splitintofractionsflag: true,
		textclass: " new-txt1",
		textdata: data.string.p3t24
	},{
		splitintofractionsflag: true,
		textclass: " new-txt4",
		textdata: data.string.fr_4_3
	},{
		splitintofractionsflag: true,
		textclass: " new-txt7",
		textdata: data.string.p3t25
	}
]
}

];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			// sounds
			{id: "sound_0", src: soundAsset+"s3_p1.ogg"},
			{id: "sound_1", src: soundAsset+"s3_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s3_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s3_p4.ogg"},
			{id: "sound_4", src: soundAsset+"s3_p5.ogg"},
			{id: "sound_5", src: soundAsset+"s3_p6.ogg"},
			{id: "sound_6", src: soundAsset+"s3_p7.ogg"},
			{id: "sound_7", src: soundAsset+"s3_p8.ogg"},
			{id: "sound_8a", src: soundAsset+"s3_p9_1.ogg"},
			{id: "sound_8b", src: soundAsset+"s3_p9_2.ogg"},
			{id: "sound_9a", src: soundAsset+"s3_p10_1.ogg"},
			{id: "sound_9b", src: soundAsset+"s3_p10_2.ogg"},
			{id: "sound_10", src: soundAsset+"s3_p11.ogg"},
			{id: "sound_11", src: soundAsset+"s3_p12.ogg"},
			{id: "sound_12", src: soundAsset+"s3_p13.ogg"},
			{id: "sound_13", src: soundAsset+"s3_p14.ogg"},
			{id: "sound_14", src: soundAsset+"s3_p15.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

   	Handlebars.registerPartial("fractioncontent", $("#fractioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	function navigationcontroller(islastpageflag){
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;

	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
	    splitintofractions($board);

			// random number generator
				function rand_generator(limit){
					var randNum = Math.floor(Math.random() * (limit - 1 +1)) + 1;
					return randNum;
				}

				/*for randomizing the options*/
				function randomize(parent){
					var parent = $(parent);
					var divs = parent.children();
					while (divs.length) {
					parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
					}
				}

	    switch(countNext){
				case 8:
				sound_nav("sound_8a");
				$(".cli").click(function(){
					if($(this).hasClass("correct")){
						createjs.Sound.stop();
						$(this).children(".corctopt").show(0);
						// play_correct_incorrect_sound(1);
						$(this).addClass('corrects');
						$(".border-num").css("pointer-events","none");
						sound_player("sound_8b");
						$(".new-txt5").show(0);

					}else{
						createjs.Sound.stop();
						$(this).children(".wrngopt").show(0);
						play_correct_incorrect_sound(0);
						$(this).addClass('incorrect');
						$(this).css("pointer-events","none");
					}
				});
				break;
				case 9:
				sound_nav("sound_9a");
			 	randomize(".optionsdiv");
				$(".border-num:eq(0)").addClass("top-num");
				$(".border-num:eq(1)").addClass("top-num");
				$(".optionscontainer").click(function(){
					if($(this).hasClass("correct")){
						createjs.Sound.stop();
						$(this).children(".corctopt").show(0);
						// play_correct_incorrect_sound(1);
						$(this).addClass('corrects');
						$(".optionscontainer").css("pointer-events","none");
						sound_player("sound_9b");
						$(".new-txt6").show(0);
					}else{
						createjs.Sound.stop();
						$(this).children(".wrngopt").show(0);
						play_correct_incorrect_sound(0);
						$(this).addClass('incorrect');
						$(this).css("pointer-events","none");
					}
				});
				break;
				case 10:
				$(".nxteqn:eq(1)").addClass("cssdown");
				sound_player("sound_"+countNext);
				break;
				default:
				sound_player("sound_"+countNext);
				break;
			}
	}


	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(0);
		});
	}

	function sound_nav(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function templateCaller(){
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		if(countNext<=6)
		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
	total_page = content.length;
	templateCaller();
});
/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

    /*===== This function splits the string in data into convential fraction used in mathematics =====*/
    function splitintofractions($splitinside){
   		typeof $splitinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
       	if($splitintofractions.length > 0){
       		$.each($splitintofractions, function(index, value){
	        	$this = $(this);
	        	var tobesplitfraction = $this.html();
	        	if($this.hasClass('fraction')){
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
	        	}else{
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
	        	}


				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
	        	$this.html(tobesplitfraction);
	        });
       	}
   	}
   	/*===== split into fractions end =====*/

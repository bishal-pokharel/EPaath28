var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	{
		//slide 0
		contentblockcenteradjust: true,
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textclass: "chapter fadein",
			textdata: data.string.p4t1
		}
		],
	},
	{
		//slide 1
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textclass: "fadein",
			textdata: data.string.p4t2
		}
		],
	},
	{
		//slide 2
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p4t2
		}
		],
		lowertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh2",
			textclass: "ole_translate lt1big fadein",
			textdata: data.string.p13
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textclass: "ole_translate lt1text fadein",
			textdata: data.string.p4t3
		}
		],
	},
	{
		//slide 3
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p4t2
		}
		],
		lowertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh2",
			textclass: "ole_translate lt1big",
			textdata: data.string.p13
		},
		{
			textclass: "ole_translate eq1 fadein",
			textdata: "="
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textclass: "ole_translate lt1text",
			textdata: data.string.p4t3
		},
		{
			splitintofractionsflag: true,
			textclass: "ole_translate lt2big fadein",
			textdata: data.string.p4_frac1
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textclass: "ole_translate lt2text fadein",
			textdata: data.string.p4t4
		}
		],
	},
	{
		//slide 4
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p4t2
		}
		],
		lowertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh2",
			textclass: "ole_translate lt1big",
			textdata: data.string.p13
		},
		{
			textclass: "ole_translate eq1",
			textdata: "="
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textclass: "ole_translate lt1text",
			textdata: data.string.p4t3
		},
		{
			splitintofractionsflag: true,
			textclass: "ole_translate lt2big",
			textdata: data.string.p4_frac1
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textclass: "ole_translate lt2text",
			textdata: data.string.p4t4
		},
		{
			textclass: "ole_translate eq2 fadein",
			textdata: "="
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh2",
			textclass: "ole_translate lt3big fadein",
			textdata: data.string.d13
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textclass: "ole_translate lt3text fadein",
			textdata: data.string.p4t5
		}
		],
	},

];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			// sounds
			{id: "sound_0", src: soundAsset+"s5_p1.ogg"},
			{id: "sound_1", src: soundAsset+"s5_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s5_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s5_p4.ogg"},
			{id: "sound_4", src: soundAsset+"s5_p5.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	function navigationcontroller(islastpageflag){
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;

	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
	    splitintofractions($board);

	    switch(countNext){
				default:
				sound_player("sound_"+countNext);
				break;
    	}


	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(0);
		});
	}

	function templateCaller(){
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		if(countNext < 6)
		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
	total_page = content.length;
	templateCaller();
});

/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/


    /*===== This function splits the string in data into convential fraction used in mathematics =====*/
    function splitintofractions($splitinside){
   		typeof $splitinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
       	if($splitintofractions.length > 0){
       		$.each($splitintofractions, function(index, value){
	        	$this = $(this);
	        	var tobesplitfraction = $this.html();
	        	if($this.hasClass('fraction')){
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
	        	}else{
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
	        	}


				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
	        	$this.html(tobesplitfraction);
	        });
       	}
   	}
   	/*===== split into fractions end =====*/

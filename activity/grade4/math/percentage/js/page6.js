var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	{
		//slide 5
		uppertextblock:[
		{
			textclass: "ole_temp_diytext",
			textdata: data.string.diy
		}
		],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "ole_temp_diyimg",
				imgsrc: "images/lokharke/2.png",
			}
			]
		}
		],
	},
	{
		//slide 6
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		lowertextblock:[
		{
			textclass: "ole_translate sidetext1 fadein",
			textdata: data.string.p2t1
		},
		{
			textclass: "ole_translate sidetext2 fadein",
			textdata: data.string.p4t6
		},
		{
			textclass: "ole_translate sidetext3 fadein",
			textdata: data.string.p4t7
		}
		],
		buttonsblock:[
		{
			btnblockadditionalclass: "ole_translate firstbtnset fadein",
			indbtn: [
			{
				textclass: "ole-template-check-btn-default correct",
				textdata: "12%"
			},
			{
				textclass: "ole-template-check-btn-default",
				textdata: "13%"
			},
			{
				textclass: "ole-template-check-btn-default",
				textdata: "14%"
			}
			]
		},
		{
			btnblockadditionalclass: "ole_translate secbtnset",
			indbtn: [
			{
				textclass: "ole-template-check-btn-default",
				splitintofractionsflag: true,
				textdata: "::25_/_100;;"
			},
			{
				textclass: "ole-template-check-btn-default correct",
				splitintofractionsflag: true,
				textdata: "::12_/_100;;"
			},
			{
				textclass: "ole-template-check-btn-default",
				splitintofractionsflag: true,
				textdata: "::120_/_100;;"
			}
			]
		},
		{
			btnblockadditionalclass: "ole_translate thibtnset",
			indbtn: [
			{
				textclass: "ole-template-check-btn-default",
				textdata: "0.15"
			},
			{
				textclass: "ole-template-check-btn-default",
				textdata: "0.012"
			},
			{
				textclass: "ole-template-check-btn-default correct",
				textdata: "0.12"
			}
			]
		},
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",
				flexblock:[
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
				]
			}
		]
	},
	{
		//slide 7
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p4t9
		}
		],
		lowertextblock:[
		{
			textclass: "ole_translate sidetext1 fadein",
			textdata: data.string.p2t1
		},
		{
			textclass: "ole_translate sidetext2 fadein",
			textdata: data.string.p4t6
		},
		{
			textclass: "ole_translate sidetext3 fadein",
			textdata: data.string.p4t7
		}
		],
		buttonsblock:[
		{
			btnblockadditionalclass: "ole_translate firstbtnset fadein",
			indbtn: [
			{
				textclass: "ole-template-check-btn-default",
				textdata: "30%"
			},
			{
				textclass: "ole-template-check-btn-default correct",
				textdata: "26%"
			},
			{
				textclass: "ole-template-check-btn-default",
				textdata: "0.26%"
			}
			]
		},
		{
			btnblockadditionalclass: "ole_translate secbtnset",
			indbtn: [
			{
				textclass: "ole-template-check-btn-default",
				splitintofractionsflag: true,
				textdata: "::260_/_100;;"
			},
			{
				textclass: "ole-template-check-btn-default correct",
				splitintofractionsflag: true,
				textdata: "::26_/_100;;"
			},
			{
				textclass: "ole-template-check-btn-default",
				splitintofractionsflag: true,
				textdata: "::28_/_100;;"
			}
			]
		},
		{
			btnblockadditionalclass: "ole_translate thibtnset",
			indbtn: [
			{
				textclass: "ole-template-check-btn-default correct",
				textdata: "0.26"
			},
			{
				textclass: "ole-template-check-btn-default",
				textdata: "0.026"
			},
			{
				textclass: "ole-template-check-btn-default",
				textdata: "0.2"
			}
			]
		},
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",
				flexblock:[
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
				]
			}
		]
	},
	{
		//slide 8
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p4t8
		}
		],
		lowertextblock:[
		{
			textclass: "ole_translate ole-template-check-btn-default submitbtn",
			textdata: data.string.btn
		},
		{
			textclass: "ole_translate sidetext1",
			textdata: data.string.p2t1
		},
		{
			textclass: "ole_translate sidetext2",
			textdata: data.string.p4t6
		},
		{
			textclass: "ole_translate sidetext3",
			textdata: data.string.p4t7
		}
		],
		buttonsblock:[
		{
			btnblockadditionalclass: "ole_translate firstbtnset",
			indbtn: [
			{
				textclass: "ole-template-check-btn-default",
				textdata: "30%"
			},
			{
				textclass: "ole-template-check-btn-default correct",
				textdata: "26%"
			},
			{
				textclass: "ole-template-check-btn-default",
				textdata: "0.26%"
			}
			]
		},
		{
			btnblockadditionalclass: "ole_translate secbtnset",
			indbtn: [
			{
				textclass: "ole-template-check-btn-default",
				splitintofractionsflag: true,
				textdata: "::260_/_100;;"
			},
			{
				textclass: "ole-template-check-btn-default correct",
				splitintofractionsflag: true,
				textdata: "::26_/_100;;"
			},
			{
				textclass: "ole-template-check-btn-default",
				splitintofractionsflag: true,
				textdata: "::28_/_100;;"
			}
			]
		},
		{
			btnblockadditionalclass: "ole_translate thibtnset",
			indbtn: [
			{
				textclass: "ole-template-check-btn-default ",
				textdata: "0.26"
			},
			{
				textclass: "ole-template-check-btn-default correct",
				textdata: "0.026"
			},
			{
				textclass: "ole-template-check-btn-default ",
				textdata: "0.2"
			}
			]
		},
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100 flexglow",
				flexblock:[
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						{flexboxrowclass :"rownormal activate",},
						]
					},
				]
			}
		]
	},
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			// sounds
			{id: "sound_1a", src: soundAsset+"s6_p2_1.ogg"},
			{id: "sound_1b", src: soundAsset+"s6_p2_2.ogg"},
			{id: "sound_1c", src: soundAsset+"s6_p2_3.ogg"},
			{id: "sound_2a", src: soundAsset+"s6_p3_1.ogg"},
			{id: "sound_3", src: soundAsset+"s6_p4.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	function navigationcontroller(islastpageflag){
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;

	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
	    splitintofractions($board);

	    switch(countNext){
				case 0:
					play_diy_audio();
					nav_button_controls(2000);
				break;
	    	case 1:
	    	case 2:
				sound_player("sound_"+(countNext)+"a");
			var caseCount= 0;
			$(".btnblock").each(function(){
				var parent = $(this);
				var divs = parent.children();
				while (divs.length) {
					parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
				}
			});
	    	$(".btnblock > p").click(function(){
	    		if($(this).hasClass("correct")){
						createjs.Sound.stop();
	    			$(this).addClass("ole-template-check-btn-default-correct");
	    			$(this).siblings().fadeOut();
	    			if(caseCount == 0){
							setTimeout(function(){
								sound_player("sound_"+(countNext)+"b");
							},800);
	    				$('.sidetext2').show(0);
	    				$('.secbtnset').css("display","flex").addClass("fadein");
	    				caseCount++;
	    			}
	    			else if(caseCount == 1){
							setTimeout(function(){
								sound_player("sound_"+(countNext)+"c");
							},800);
	    				$('.sidetext3').show(0);
	    				$('.thibtnset').css("display","flex").addClass("fadein");
	    				caseCount++;
	    			}
	    			else{
	    				nav_button_controls(200);

	    			}
					play_correct_incorrect_sound(1);
	    		}
	    		else{
						createjs.Sound.stop();
	    			$(this).addClass("ole-template-check-btn-default-incorrect");
					play_correct_incorrect_sound(0);
	    		}
	    	});
	    	break;
	    	case 3:
				sound_player("sound_"+(countNext));
			var ClickCount = 0;
			$(".submitbtn").hide(0);
			$(".sidetext1").hide(0);
			$(".firstbtnset").hide(0);
	    	$(".rownormal").click(function(){
					$(this).toggleClass("clicked");
					if($(this).hasClass("clicked")){
						ClickCount++;
						$(this).addClass("selectthis");
					}
					else{
						ClickCount--;
						$(this).removeClass("selectthis");
					}
					showhidebtn();
				});

	    	function showhidebtn(){
				if(ClickCount > 0)
					$(".submitbtn").show(0);
				else
					$(".submitbtn").hide(0);
			}


	    	$(".submitbtn").click(function(){
		    	var rand = Math.floor((Math.random() * 10) + 1);
	    		$(this).hide(0);
				$(".rownormal").removeClass("activate").unbind( "click");
				$(".sidetext1").fadeIn();
				$(".firstbtnset").fadeIn();

				$(".firstbtnset > p").each(function(i, obj){
					if(i == 0){
						$(obj).html(ClickCount+rand+"%");
					}
					else if(i == 1){
						$(obj).html(ClickCount+"%");
					}
					else{
						$(obj).html("0."+ClickCount+"%");
					}
				});

				$(".secbtnset > p").each(function(i, obj){
					if(i == 0){
						$(obj).html("::"+ClickCount*10+"_/_100;;");
	    				splitintofractions($board);
					}
					else if(i == 1){
						$(obj).html("::"+ClickCount+"_/_100;;");
	    				splitintofractions($board);
					}
					else{
						$(obj).html("::"+ClickCount+rand+"_/_100;;");
	    				splitintofractions($board);
					}
				});

				$(".thibtnset > p").each(function(i, obj){
					if(i == 0){
						var ans = ClickCount/1000;
						$(obj).html(ans);
					}
					else if(i == 1){
						var ans = ClickCount/100;
						$(obj).html(ans);
					}
					else{
						$(obj).html("0."+ClickCount+rand);
					}
				});
	    	});

	    	var caseCount= 0;
	    	$(".btnblock > p").click(function(){
	    		if($(this).hasClass("correct")){
	    			$(this).addClass("ole-template-check-btn-default-correct");
	    			$(this).siblings().fadeOut();
	    			if(caseCount == 0){
	    				$('.sidetext2').show(0);
	    				$('.secbtnset').css("display","flex").addClass("fadein");
	    				caseCount++;
	    			}
	    			else if(caseCount == 1){
	    				$('.sidetext3').show(0);
	    				$('.thibtnset').css("display","flex").addClass("fadein");
	    				caseCount++;
	    			}
	    			else{
							nav_button_controls(200);
	    			}
					play_correct_incorrect_sound(1);
	    		}
	    		else{
	    			$(this).addClass("ole-template-check-btn-default-incorrect");
					play_correct_incorrect_sound(0);
	    		}
	    	});
	    	break;
	    }
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function templateCaller(){
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		// if(countNext < 6)
		// navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
	total_page = content.length;
	templateCaller();
});

/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/


    /*===== This function splits the string in data into convential fraction used in mathematics =====*/
    function splitintofractions($splitinside){
   		typeof $splitinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
       	if($splitintofractions.length > 0){
       		$.each($splitintofractions, function(index, value){
	        	$this = $(this);
	        	var tobesplitfraction = $this.html();
	        	if($this.hasClass('fraction')){
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
	        	}else{
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
	        	}


				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
	        	$this.html(tobesplitfraction);
	        });
       	}
   	}
   	/*===== split into fractions end =====*/

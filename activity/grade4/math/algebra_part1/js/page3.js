var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg1",
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p3text1,
			datahighlightflag:true,
			datahighlightcustomclass:'brown',
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'box1',
			imgsrc: '',
		}],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "rhyno",
				imgid : 'rhyno',
				imgsrc: ""
			}

		]
	}]
},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg1",
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p3s1txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'box1',
			imgsrc: '',
		}],
		extratextblock:[
		{
		textclass:"s1f10",
		textdata:data.string.p3s10
		},
		{
		textclass:"s1s10",
		textdata:data.string.p3s10
		},
		{
		textclass:"s1t10",
		textdata:data.string.p3s10
		},
		{
		textclass:"s1fth10",
		textdata:data.string.p3s10
		}
		],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "rhyno",
				imgid : 'rhyno',
				imgsrc: ""
			},
			{
				imgclass:"s1fstapl",
				imgid:'apple2',
				imgsrc:" "
			},
						{
				imgclass:"s1secapl",
				imgid:'apple2',
				imgsrc:" "
			},
						{
				imgclass:"s1thapl",
				imgid:'apple2',
				imgsrc:" "
			},
						{
				imgclass:"s1fthapl",
				imgid:'apple2',
				imgsrc:" "
			},
			{
				imgclass:"pricetagl",
				imgid:'pricetag',
				imgsrc:" "
			},
			{
				imgclass:"pricetag2",
				imgid:'pricetag',
				imgsrc:" "
			},
			{
				imgclass:"pricetag3",
				imgid:'pricetag',
				imgsrc:" "
			},
			{
				imgclass:"pricetag4",
				imgid:'pricetag',
				imgsrc:" "
			}

		]
	}]
},
// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg1",
		speechbox:[{
			speechbox: 'sp-2',
			textdata : data.string.p3s2txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'box1',
			imgsrc: '',
		}],
		extratextblock:[
		{
		textclass:"s2f10",
		textdata:data.string.p3s10
		},
		{
		textclass:"s2s10",
		textdata:data.string.p3s10
		},
		{
		textclass:"s2t10",
		textdata:data.string.p3s10
		},
		{
		textclass:"s2fth10",
		textdata:data.string.p3s10
		},
		{
		textclass:"lowertext",
		textdata:data.string.p3s2lwblk
		}

		],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "rhyno",
				imgid : 'rhyno',
				imgsrc: ""
			},
			{
				imgclass:"s1fstapl",
				imgid:'apple2',
				imgsrc:" "
			},
						{
				imgclass:"s1secapl",
				imgid:'apple2',
				imgsrc:" "
			},
						{
				imgclass:"s1thapl",
				imgid:'apple2',
				imgsrc:" "
			},
						{
				imgclass:"s1fthapl",
				imgid:'apple2',
				imgsrc:" "
			},
			{
				imgclass:"pricetagls2",
				imgid:'pricetag',
				imgsrc:" "
			},
			{
				imgclass:"pricetag2s2",
				imgid:'pricetag',
				imgsrc:" "
			},
			{
				imgclass:"pricetag3s2",
				imgid:'pricetag',
				imgsrc:" "
			},
			{
				imgclass:"pricetag4s2",
				imgid:'pricetag',
				imgsrc:" "
			}

		]
	}]
},
// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg1",
		speechbox:[{
			speechbox: 'sp-2',
			textdata : data.string.p3s3txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'box1',
			imgsrc: '',
		}],
		extratextblock:[
		{
		textclass:"s3f10",
		textdata:data.string.p3s10
		},
		{
		textclass:"s3s10",
		textdata:data.string.p3s10
		},
		{
		textclass:"s3t10",
		textdata:data.string.p3s10
		},
		{
		textclass:"s3fth10",
		textdata:data.string.p3s10
		},
		{
		textclass:"lowertext",
		textdata:data.string.p3s3txt2
		}

		],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "rhyno",
				imgid : 'rhyno',
				imgsrc: ""
			},
			{
				imgclass:"s1fstapl",
				imgid:'apple2',
				imgsrc:" "
			},
						{
				imgclass:"s1secapl",
				imgid:'apple2',
				imgsrc:" "
			},
						{
				imgclass:"s1thapl",
				imgid:'apple2',
				imgsrc:" "
			},
						{
				imgclass:"s1fthapl",
				imgid:'apple2',
				imgsrc:" "
			},
			{
				imgclass:"pricetagls3",
				imgid:'pricetag',
				imgsrc:" "
			},
			{
				imgclass:"pricetag2s3",
				imgid:'pricetag',
				imgsrc:" "
			},
			{
				imgclass:"pricetag3s3",
				imgid:'pricetag',
				imgsrc:" "
			},
			{
				imgclass:"pricetag4s3",
				imgid:'pricetag',
				imgsrc:" "
			}

		]
	}]
},
// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg1",
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p3s4txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'box1',
			imgsrc: '',
		}],
		extratextblock:[
		{
		textclass:"s4f10",
		textdata:data.string.p3s15
		},
		{
		textclass:"s4s10",
		textdata:data.string.p3s15
		},
		{
		textclass:"s4t10",
		textdata:data.string.p3s15
		},
		{
		textclass:"s4fth10",
		textdata:data.string.p3s15
		},
		{
		textclass:"lowertext",
		textdata:data.string.p3s4txt2
		}

		],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "rhyno",
				imgid : 'rhyno',
				imgsrc: ""
			},
			{
				imgclass:"s1fstapl",
				imgid:'apple2',
				imgsrc:" "
			},
						{
				imgclass:"s1secapl",
				imgid:'apple2',
				imgsrc:" "
			},
						{
				imgclass:"s1thapl",
				imgid:'apple2',
				imgsrc:" "
			},
						{
				imgclass:"s1fthapl",
				imgid:'apple2',
				imgsrc:" "
			},
			{
				imgclass:"pricetagls4",
				imgid:'pricetag',
				imgsrc:" "
			},
			{
				imgclass:"pricetag2s4",
				imgid:'pricetag',
				imgsrc:" "
			},
			{
				imgclass:"pricetag3s4",
				imgid:'pricetag',
				imgsrc:" "
			},
			{
				imgclass:"pricetag4s4",
				imgid:'pricetag',
				imgsrc:" "
			}

		]
	}]
},
// slide4++
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg1",
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p3s4_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'box1',
			imgsrc: '',
		}],
		extratextblock:[
		{
		textclass:"s4_f10",
		textdata:data.string.p4m
		},
		{
		textclass:"s4_s10",
		textdata:data.string.p4m
		},
		{
		textclass:"s4_t10",
		textdata:data.string.p4m
		},
		{
		textclass:"s4_fth10",
		textdata:data.string.p4m
		},
		{
		textclass:"lowertext",
		textdata:data.string.p4low
		}

		],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "rhyno",
				imgid : 'rhyno',
				imgsrc: ""
			},
			{
				imgclass:"s1fstapl",
				imgid:'apple2',
				imgsrc:" "
			},
						{
				imgclass:"s1secapl",
				imgid:'apple2',
				imgsrc:" "
			},
						{
				imgclass:"s1thapl",
				imgid:'apple2',
				imgsrc:" "
			},
						{
				imgclass:"s1fthapl",
				imgid:'apple2',
				imgsrc:" "
			},
			{
				imgclass:"pricetagls4",
				imgid:'pricetag',
				imgsrc:" "
			},
			{
				imgclass:"pricetag2s4",
				imgid:'pricetag',
				imgsrc:" "
			},
			{
				imgclass:"pricetag3s4",
				imgid:'pricetag',
				imgsrc:" "
			},
			{
				imgclass:"pricetag4s4",
				imgid:'pricetag',
				imgsrc:" "
			}

		]
	}]
},
// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg1",
		speechbox:[{
			speechbox: 'sp-2',
			textdata : data.string.p3s5txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'box1',
			imgsrc: '',
		}],
		extratextblock:[
		{
		textclass:"s5f10",
		textdata:data.string.p3s15
		},
		{
		textclass:"s5s10",
		textdata:data.string.p3s15
		},
		{
		textclass:"s5t10",
		textdata:data.string.p3s15
		},
		{
		textclass:"s5fth10",
		textdata:data.string.p3s15
		},
		{
		textclass:"s56th10",
		textdata:data.string.p3s4m
		},
		{
		textclass:"lowertext",
		textdata:data.string.p3s5txt2
		}

		],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "rhyno",
				imgid : 'rhyno',
				imgsrc: ""
			},
			{
				imgclass:"s5fstapl",
				imgid:'apple2',
				imgsrc:" "
			},
						{
				imgclass:"s5secapl",
				imgid:'apple2',
				imgsrc:" "
			},
						{
				imgclass:"s5thapl",
				imgid:'apple2',
				imgsrc:" "
			},
						{
				imgclass:"s5fthapl",
				imgid:'apple2',
				imgsrc:" "
			},
			{
				imgclass:"pricetagls5",
				imgid:'pricetag',
				imgsrc:" "
			},
			{
				imgclass:"pricetag2s5",
				imgid:'pricetag',
				imgsrc:" "
			},
			{
				imgclass:"pricetag3s5",
				imgid:'pricetag',
				imgsrc:" "
			},
			{
				imgclass:"pricetag4s5",
				imgid:'pricetag',
				imgsrc:" "
			},
			{
				imgclass:"pricetag5s5",
				imgid:'pricetag',
				imgsrc:" "
			}

		]
	}]
},
// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg1",
		speechbox:[{
			speechbox: 'sp-2',
			textdata : data.string.p3s7txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'box1',
			imgsrc: '',
		}],
		extratextblock:[
		{
	    datahighlightflag:"true",
	    datahighlightcustomclass:"symbol",
		textclass:"s7tag10",
		textdata:data.string.p3s7txt2
		}
		],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "rhyno",
				imgid : 'rhyno',
				imgsrc: ""
			},
			{
				imgclass:"s6fstapl",
				imgid:'apple2',
				imgsrc:" "
			},
						{
				imgclass:"s6secapl",
				imgid:'apple2',
				imgsrc:" "
			},
						{
				imgclass:"s6thapl",
				imgid:'apple2',
				imgsrc:" "
			},
						{
				imgclass:"s6fthapl",
				imgid:'apple2',
				imgsrc:" "
			},
			{
				imgclass:"pricetag5s6",
				imgid:'pricetag',
				imgsrc:" "
			}

		]
	}]
},
// slide8
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg1",
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p3s8txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'box1',
			imgsrc: '',
		}],
		extratextblock:[
		{
	    datahighlightflag:"true",
	    datahighlightcustomclass:"symbol1",
		textclass:"s7tag10",
		textdata:data.string.p3s8txt2
		}
		],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "rhyno",
				imgid : 'rhyno',
				imgsrc: ""
			},
			{
				imgclass:"s6fthapl",
				imgid:'apple2',
				imgsrc:" "
			},
			{
				imgclass:"pricetag5s6",
				imgid:'pricetag',
				imgsrc:" "
			}

		]
	}]
},
// slide9
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg1",
		speechbox:[{
			speechbox: 'sp-2',
			textdata : data.string.p3s9txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'box1',
			imgsrc: '',
		}],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "rhyno",
				imgid : 'rhyno',
				imgsrc: ""
			}
		]
	}]
},
// slide10
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg2",
		extratextblock:[
		{
		datahighlightflag:"true",
		datahighlightcustomclass:"coff5",
		textclass:"s10coeff",
		textdata:data.string.p3s10coeff
		},
		{
		datahighlightflag:"true",
		datahighlightcustomclass:"coff5",
		textclass:"s10var",
		textdata:data.string.p3s10var
		},
	  {
		textclass:"s10q5",
		textdata:data.string.p3s10q5
		},
	  {
		textclass:"top_text",
		textdata:data.string.p3text2
		}
		],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "teddy1",
				imgid : 'teddy',
				imgsrc: ""
			},
			{
				imgclass: "teddy2",
				imgid : 'teddy',
				imgsrc: ""
			},
			{
				imgclass: "teddy3",
				imgid : 'teddy',
				imgsrc: ""
			},
			{
				imgclass: "teddy4",
				imgid : 'teddy',
				imgsrc: ""
			},
			{
				imgclass: "teddy5",
				imgid : 'teddy',
				imgsrc: ""
			}


		]
	}]
},
// slide11
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg2",
		extratextblock:[
		{
		datahighlightflag:"true",
		datahighlightcustomclass:"coff5",
		textclass:"s11coeff",
		textdata:data.string.p3s11coeff
		},
		{
		datahighlightflag:"true",
		datahighlightcustomclass:"coff5",
		textclass:"s11var",
		textdata:data.string.p3s11var
		},
	  {
		textclass:"s11q5",
		textdata:data.string.p3s11q5
		},
	  {
		textclass:"top_text",
		textdata:data.string.p3text3
		}
		],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "car1",
				imgid : 'car',
				imgsrc: ""
			},
			{
				imgclass: "car2",
				imgid : 'car',
				imgsrc: ""
			},
			{
				imgclass: "car3",
				imgid : 'car',
				imgsrc: ""
			},
			{
				imgclass: "car4",
				imgid : 'car',
				imgsrc: ""
			},
			{
				imgclass: "car5",
				imgid : 'car',
				imgsrc: ""
			},
			{
				imgclass: "car6",
				imgid : 'car',
				imgsrc: ""
			},
			{
				imgclass: "car7",
				imgid : 'car',
				imgsrc: ""
			},
			{
				imgclass: "car8",
				imgid : 'car',
				imgsrc: ""
			}



		]
	}]
}

];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0 ;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "rhyno", src: imgpath+"rhyno.png", type: createjs.AbstractLoader.IMAGE},
			{id: "txtbox", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "apple2", src: imgpath+"apple02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pricetag", src: imgpath+"pricetag.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src: imgpath+"arrow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "teddy", src: imgpath+"toy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "car", src: imgpath+"car.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box1", src: imgpath+"text_box01.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s3_p1", src: soundAsset+"s3_p1.ogg"},
			{id: "s3_p2", src: soundAsset+"s3_p2.ogg"},
			{id: "s3_p3_1", src: soundAsset+"s3_p3_1.ogg"},
			{id: "s3_p3_2", src: soundAsset+"s3_p3_2.ogg"},
			{id: "s3_p4_1", src: soundAsset+"s3_p4_1.ogg"},
			{id: "s3_p4_2", src: soundAsset+"s3_p4_2.ogg"},
			{id: "s3_p5_1", src: soundAsset+"s3_p5_1.ogg"},
			{id: "s3_p5_2", src: soundAsset+"s3_p5_2.ogg"},
			{id: "s3_p6_1", src: soundAsset+"s3_p6_1.ogg"},
			{id: "s3_p6_2", src: soundAsset+"s3_p6_2.ogg"},
			{id: "s3_p7_1", src: soundAsset+"s3_p7_1.ogg"},
			{id: "s3_p7_2", src: soundAsset+"s3_p7_2.ogg"},
			{id: "s3_p8", src: soundAsset+"s3_p8.ogg"},
			{id: "s3_p9", src: soundAsset+"s3_p9.ogg"},
			{id: "s3_p10", src: soundAsset+"s3_p10.ogg"},
			{id: "s3_p11", src: soundAsset+"s3_p11.ogg"},
			{id: "s3_p12", src: soundAsset+"s3_p12.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification():ole.footerNotificationHandler.pageEndSetNotification();
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		 put_image2(content, countNext);
		 put_speechbox_image(content, countNext);

		switch(countNext){
					case 2:
					case 3:
					case 4:
					case 5:
					case 6:
					createjs.Sound.stop();
					var current_sound = createjs.Sound.play('s3_p'+(countNext+1)+'_1');
					current_sound.play();
					current_sound.on('complete', function(){
						sound_player('s3_p'+(countNext+1)+'_2');
					});
					break;
					default:
					sound_player('s3_p'+(countNext+1));
					break;
				}


	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(100);
		});
	}

	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2);
		});

	}
		function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				var selector = ('.'+speechbox[i].speechbox+' > .speechbg');
				console.log("imgsrc---"+image_src, $(selector));
				$(selector).attr('src', image_src);
			}
		}
	}

	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";



var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_blue',

		extratextblock : [{
			textdata : data.lesson.chapter,
			textclass : 'lesson-title pangolin'
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "coverimage",
					imgsrc : imgpath + "cover_algebra.png",
				}
			],
		}],
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_lightblue',
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "fly-1",
					imgsrc : imgpath + "fly.png",
				},
				{
					imgclass : "plus_sign",
					imgsrc : imgpath + "plus02.png",
				},
				{
					imgclass : "multiply_sign",
					imgsrc : imgpath + "multiple.png",
				},
				{
					imgclass : "minus_sign",
					imgsrc : imgpath + "minus.png",
				},
				{
					imgclass : "division_sign",
					imgsrc : imgpath + "divide.png",
				}
			],
		}
	],
	speechbox:[{
		speechbox: 'sp-1',
		textdata : data.string.p1text1,
		textclass : 'txt1',
		imgclass: 'box',
		imgid : 'box1',
		imgsrc: '',
	}],
},
//slide2
{
	hasheaderblock : false,
	contentblocknocenteradjust : true,
	contentblockadditionalclass : 'bg_pool',

	extratextblock:[{
		textclass: 'block-1a-text',
		textdata: data.string.p15,
	},{
		textclass: 'block-2a-text',
		textdata: data.string.p12,
	}],
	imageblock : [{
		imagestoshow : [
			{
				imgclass : "plus-1",
				imgsrc : imgpath + "plus.png",
			},
			{
				imgclass : "equal-1",
				imgsrc : imgpath + "equal.png",
			},
			{
				imgclass : "cross-1",
				imgsrc : imgpath + "question01.png",
			},
			{
				imgclass : "fly-2",
				imgsrc : imgpath + "fly.png",
			}
		],
	}],
	speechbox:[{
		speechbox: 'sp-2',
		textdata : data.string.p1text2,
		textclass : 'txt1',
		imgclass: 'box',
		imgid : 'box1',
		imgsrc: '',
	}],
	block:[
		{
			blockclass: 'block-1a',
			imgclass: 'img-1a',
			imgsrc: imgpath + "block_pink.png",
			textdata: data.string.t_1,
			textclass: 'its_hidden'
		},
		{
			blockclass: 'block-1b',
			imgclass: '',
			imgsrc: imgpath + "block_purple.png",
			textdata: data.string.t_2,
			textclass: 'its_hidden'
		},
		{
			blockclass: 'block-1c',
			imgclass: '',
			imgsrc: imgpath + "block_pink.png",
			textdata: data.string.t_3,
			textclass: 'its_hidden'
		},
		{
			blockclass: 'block-1d',
			imgclass: '',
			imgsrc: imgpath + "block_purple.png",
			textdata: data.string.t_3,
			textclass: 'its_hidden'
		},
		{
			blockclass: 'block-1e',
			imgclass: '',
			imgsrc: imgpath + "block_pink.png",
			textdata: data.string.t_3,
			textclass: 'its_hidden'
		},
		{
			blockclass: 'block-2a',
			imgclass: '',
			imgsrc: imgpath + "block_purple.png",
			textdata: data.string.t_3,
			textclass: 'its_hidden'
		},
		{
			blockclass: 'block-2b',
			imgclass: '',
			imgsrc: imgpath + "block_pink.png",
			textdata: data.string.t_3,
			textclass: 'its_hidden'
		}],
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pool',

		extratextblock:[{
			textclass: 'block-1a-text',
			textdata: data.string.p15,
		},{
			textclass: 'block-2a-text',
			textdata: data.string.p12,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "plus-1",
					imgsrc : imgpath + "plus.png",
				},
				{
					imgclass : "equal-1",
					imgsrc : imgpath + "equal.png",
				},
				{
					imgclass : "cross-1",
					imgsrc : imgpath + "question01.png",
				},
				{
					imgclass : "fly-2",
					imgsrc : imgpath + "fly.png",
				}
			],
		}],
		speechbox:[{
			speechbox: 'sp-2',
			textdata : data.string.p1text3,
			textclass : 'txt1',
			datahighlightflag:true,
			datahighlightcustomclass:'blue',
			imgclass: 'box',
			imgid : 'box1',
			imgsrc: '',
		}],
		block:[
			{
				blockclass: 'block-1a',
				imgclass: 'img-1a',
				imgsrc: imgpath + "block_pink.png",
				textdata: data.string.t_1,
				textclass: 'its_hidden'
			},
			{
				blockclass: 'block-1b',
				imgclass: '',
				imgsrc: imgpath + "block_purple.png",
				textdata: data.string.t_2,
				textclass: 'its_hidden'
			},
			{
				blockclass: 'block-1c',
				imgclass: '',
				imgsrc: imgpath + "block_pink.png",
				textdata: data.string.t_3,
				textclass: 'its_hidden'
			},
			{
				blockclass: 'block-1d',
				imgclass: '',
				imgsrc: imgpath + "block_purple.png",
				textdata: data.string.t_3,
				textclass: 'its_hidden'
			},
			{
				blockclass: 'block-1e',
				imgclass: '',
				imgsrc: imgpath + "block_pink.png",
				textdata: data.string.t_3,
				textclass: 'its_hidden'
			},
			{
				blockclass: 'block-2a',
				imgclass: '',
				imgsrc: imgpath + "block_purple.png",
				textdata: data.string.t_3,
				textclass: 'its_hidden'
			},
			{
				blockclass: 'block-2b',
				imgclass: '',
				imgsrc: imgpath + "block_pink.png",
				textdata: data.string.t_3,
				textclass: 'its_hidden'
			}],
		},
		//slide4
		{
			hasheaderblock : false,
			contentblocknocenteradjust : true,
			contentblockadditionalclass : 'bg_pool',

			extratextblock:[{
				textclass: 'block-1a-text',
				textdata: data.string.p15,
			},{
				textclass: 'block-2a-text',
				textdata: data.string.p12,
			}],
			imageblock : [{
				imagestoshow : [
					{
						imgclass : "plus-1",
						imgsrc : imgpath + "plus.png",
					},
					{
						imgclass : "equal-1",
						imgsrc : imgpath + "equal.png",
					},
					{
						imgclass : "cross-1",
						imgsrc : imgpath + "question01.png",
					},
					{
						imgclass : "fly-2",
						imgsrc : imgpath + "fly.png",
					}
				],
			}],
			speechbox:[{
				speechbox: 'sp-2',
				textdata : data.string.p1text4,
				textclass : 'txt1',
				imgclass: 'box',
				imgid : 'box1',
				imgsrc: '',
			}],
			block:[
				{
					blockclass: 'block-1a',
					imgclass: 'img-1a',
					imgsrc: imgpath + "block_pink.png",
					textdata: data.string.t_1,
					textclass: 'its_hidden'
				},
				{
					blockclass: 'block-1b',
					imgclass: '',
					imgsrc: imgpath + "block_purple.png",
					textdata: data.string.t_2,
					textclass: 'its_hidden'
				},
				{
					blockclass: 'block-1c',
					imgclass: '',
					imgsrc: imgpath + "block_pink.png",
					textdata: data.string.t_3,
					textclass: 'its_hidden'
				},
				{
					blockclass: 'block-1d',
					imgclass: '',
					imgsrc: imgpath + "block_purple.png",
					textdata: data.string.t_3,
					textclass: 'its_hidden'
				},
				{
					blockclass: 'block-1e',
					imgclass: '',
					imgsrc: imgpath + "block_pink.png",
					textdata: data.string.t_3,
					textclass: 'its_hidden'
				},
				{
					blockclass: 'block-2a',
					imgclass: '',
					imgsrc: imgpath + "block_purple.png",
					textdata: data.string.t_3,
					textclass: 'its_hidden'
				},
				{
					blockclass: 'block-2b',
					imgclass: '',
					imgsrc: imgpath + "block_pink.png",
					textdata: data.string.t_3,
					textclass: 'its_hidden'
				}],
			},
			//slide5
			{
				hasheaderblock : false,
				contentblocknocenteradjust : true,
				contentblockadditionalclass : 'bg_pool',

				extratextblock:[{
					textclass: 'block-1a-text',
					textdata: data.string.p15,
				},{
					textclass: 'block-2a-text',
					textdata: data.string.p12,
				},
				{
					textclass: 's5a',
					textdata: data.string.p1s5a,
				},
				{
					textclass: 's5b',
					textdata: data.string.p1s5b,
				}
				,
				{
					textclass: 's5p',
					textdata: data.string.p1s5p,
				}
				,
				{
					textclass: 's5q',
					textdata: data.string.p1s5q,
				}],
				imageblock : [{
					imagestoshow : [
						{
							imgclass : "plus-1",
							imgsrc : imgpath + "plus.png",
						},
						{
							imgclass : "equal-1",
							imgsrc : imgpath + "equal.png",
						},
						{
							imgclass : "fly-2",
							imgsrc : imgpath + "fly.png",
						}
					],
				}],
				speechbox:[{
					speechbox: 'sp-2',
					textdata : data.string.p1text5,
					textclass : 'txt1',
					imgclass: 'box',
					imgid : 'box1',
					imgsrc: '',
				}],
				block:[
					{
						blockclass: 'block-1a',
						imgclass: 'img-1a',
						imgsrc: imgpath + "block_pink.png",
						textdata: data.string.t_1,
						textclass: 'its_hidden'
					},
					{
						blockclass: 'block-1b',
						imgclass: '',
						imgsrc: imgpath + "block_purple.png",
						textdata: data.string.t_2,
						textclass: 'its_hidden'
					},
					{
						blockclass: 'block-1c',
						imgclass: '',
						imgsrc: imgpath + "block_pink.png",
						textdata: data.string.t_3,
						textclass: 'its_hidden'
					},
					{
						blockclass: 'block-1d',
						imgclass: '',
						imgsrc: imgpath + "block_purple.png",
						textdata: data.string.t_3,
						textclass: 'its_hidden'
					},
					{
						blockclass: 'block-1e',
						imgclass: '',
						imgsrc: imgpath + "block_pink.png",
						textdata: data.string.t_3,
						textclass: 'its_hidden'
					},
					{
						blockclass: 'block-2a',
						imgclass: '',
						imgsrc: imgpath + "block_purple.png",
						textdata: data.string.t_3,
						textclass: 'its_hidden'
					},
					{
						blockclass: 'block-2b',
						imgclass: '',
						imgsrc: imgpath + "block_pink.png",
						textdata: data.string.t_3,
						textclass: 'its_hidden'
					},
					{
						blockclass: 'happyface1',
						imgclass: '',
						imgsrc: imgpath + "happy-face01.png",
						textdata: data.string.t_3,
						textclass: 'its_hidden'
					},
					{
						blockclass: 'happyface2',
						imgclass: '',
						imgsrc: imgpath + "happy-face02.png",
						textdata: data.string.t_3,
						textclass: 'its_hidden'
					}
					,
					{
						blockclass: 'star',
						imgclass: '',
						imgsrc: imgpath + "star.png",
						textdata: data.string.t_3,
						textclass: 'its_hidden'
					}
					,
					{
						blockclass: 'sun',
						imgclass: '',
						imgsrc: imgpath + "sun.png",
						textdata: data.string.t_3,
						textclass: 'its_hidden'
					}]
				},
				//slide6
				{
					hasheaderblock : false,
					contentblocknocenteradjust : true,
					contentblockadditionalclass : 'bg_pool',

					extratextblock:[{
						textclass: 'block-1a-text',
						textdata: data.string.p15,
					},{
						textclass: 'block-2a-text',
						textdata: data.string.p12,
					}],
					imageblock : [{
						imagestoshow : [
							{
								imgclass : "plus-1",
								imgsrc : imgpath + "plus.png",
							},
							{
								imgclass : "equal-1",
								imgsrc : imgpath + "equal.png",
							},
							{
								imgclass : "cross-1",
								imgsrc : imgpath + "cross.png",
							},
							{
								imgclass : "fly-2",
								imgsrc : imgpath + "fly.png",
							}
						],
					}],
					speechbox:[{
						speechbox: 'sp-2',
						textdata : data.string.p1text6,
						textclass : 'txt1',
						imgclass: 'box',
						imgid : 'box1',
						imgsrc: '',
					}],
					block:[
						{
							blockclass: 'block-1a',
							imgclass: 'img-1a',
							imgsrc: imgpath + "block_pink.png",
							textdata: data.string.t_1,
							textclass: 'its_hidden'
						},
						{
							blockclass: 'block-1b',
							imgclass: '',
							imgsrc: imgpath + "block_purple.png",
							textdata: data.string.t_2,
							textclass: 'its_hidden'
						},
						{
							blockclass: 'block-1c',
							imgclass: '',
							imgsrc: imgpath + "block_pink.png",
							textdata: data.string.t_3,
							textclass: 'its_hidden'
						},
						{
							blockclass: 'block-1d',
							imgclass: '',
							imgsrc: imgpath + "block_purple.png",
							textdata: data.string.t_3,
							textclass: 'its_hidden'
						},
						{
							blockclass: 'block-1e',
							imgclass: '',
							imgsrc: imgpath + "block_pink.png",
							textdata: data.string.t_3,
							textclass: 'its_hidden'
						},
						{
							blockclass: 'block-2a',
							imgclass: '',
							imgsrc: imgpath + "block_purple.png",
							textdata: data.string.t_3,
							textclass: 'its_hidden'
						},
						{
							blockclass: 'block-2b',
							imgclass: '',
							imgsrc: imgpath + "block_pink.png",
							textdata: data.string.t_3,
							textclass: 'its_hidden'
						}],
					},
					//slide7
					{
						hasheaderblock : false,
						contentblocknocenteradjust : true,
						contentblockadditionalclass : 'bg_pool',

						extratextblock:[
							{
								textclass: 'left_equation_text_1',
								textdata: data.string.p1text18,
							},
							{
								textclass: 'left_equation_text_2',
								textdata: data.string.p1text19,
							}
						],
						imageblock : [{
							imagestoshow : [
								{
									imgclass : "plus-1",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1",
									imgsrc : imgpath + "equal.png",
								},
								{
									imgclass : "cross-1",
									imgsrc : imgpath + "cross.png",
								},
								{
									imgclass : "fly-2",
									imgsrc : imgpath + "fly.png",
								}
							],
						}],
						speechbox:[{
							speechbox: 'sp-2',
							textdata : data.string.p1text7,
							textclass : 'txt1',
							imgclass: 'box',
							imgid : 'box1',
							imgsrc: '',
						}]
					},
					//slide8
					{
						hasheaderblock : false,
						contentblocknocenteradjust : true,
						contentblockadditionalclass : 'bg_pool',

						extratextblock:[{
							textclass: 's3-5',
							textdata: data.string.p15,
						},{
							textclass: 's3-2',
							textdata: data.string.p12,
						},
						{
							textclass: 's1f7',
							textdata: data.string.s1f7,
						}
					],
					imageblock : [{
						imagestoshow : [
							{
								imgclass : "plus-1s3",
								imgsrc : imgpath + "plus.png",
							},
							{
								imgclass : "equal-1",
								imgsrc : imgpath + "equal.png",
							},
							{
								imgclass : "cross-1",
								imgsrc : imgpath + "cross.png",
							},
							{
								imgclass : "fly-2",
								imgsrc : imgpath + "fly.png",
							}
						],
					}],
					speechbox:[{
						speechbox: 'sp-2',
						textdata : data.string.p1text8,
						textclass : 'txt1',
						imgclass: 'box',
						imgid : 'box1',
						imgsrc: '',
					}],
					block:[
						{
							blockclass: 'block-1a',
							imgclass: 'img-1a',
							imgsrc: imgpath + "block_pink.png",
							textdata: data.string.t_1,
							textclass: 'its_hidden'
						},
						{
							blockclass: 'block-1b',
							imgclass: '',
							imgsrc: imgpath + "block_purple.png",
							textdata: data.string.t_2,
							textclass: 'its_hidden'
						},
						{
							blockclass: 'block-1c',
							imgclass: '',
							imgsrc: imgpath + "block_pink.png",
							textdata: data.string.t_3,
							textclass: 'its_hidden'
						},
						{
							blockclass: 'block-1d',
							imgclass: '',
							imgsrc: imgpath + "block_purple.png",
							textdata: data.string.t_3,
							textclass: 'its_hidden'
						},
						{
							blockclass: 'block-1e',
							imgclass: '',
							imgsrc: imgpath + "block_pink.png",
							textdata: data.string.t_3,
							textclass: 'its_hidden'
						},
						{
							blockclass: 'block-2a',
							imgclass: '',
							imgsrc: imgpath + "block_purple.png",
							textdata: data.string.t_3,
							textclass: 'its_hidden'
						},
						{
							blockclass: 'block-2b',
							imgclass: '',
							imgsrc: imgpath + "block_pink.png",
							textdata: data.string.t_3,
							textclass: 'its_hidden'
						}],
					},


					//slide 9
					{
						hasheaderblock : false,
						contentblocknocenteradjust : true,
						contentblockadditionalclass : 'bg_pool',

						extratextblock:[
							{
								textclass: 'left_equation_text_1',
								textdata: data.string.p1text21,
							},
							{
								textclass: 'left_equation_text_2',
								textdata: data.string.p1text22,
							},{
								textclass: 'left_equation_text_3',
								textdata: data.string.p1text23,
							}
						],
						imageblock : [{
							imagestoshow : [
								{
									imgclass : "plus-1",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1",
									imgsrc : imgpath + "equal.png",
								},
								// {
								// 	imgclass : "cross-1",
								// 	imgsrc : imgpath + "cross.png",
								// },
								{
									imgclass : "fly-2",
									imgsrc : imgpath + "fly.png",
								}
							],
						}],
						speechbox:[{
							speechbox: 'sp-2',
							textdata : data.string.p1text9,
							datahighlightflag:true,
							datahighlightcustomclass:'later_text',
							textclass : 'txt1',
							imgclass: 'box',
							imgid : 'box1',
							imgsrc: '',
						}]
					},

					//slide 10
					{
						hasheaderblock : false,
						contentblocknocenteradjust : true,
						contentblockadditionalclass : 'bg_pool',

						extratextblock:[
							//first
							{
								textclass: 'left_equation_text_1_a',
								textdata: data.string.p1text21,
							},
							{
								textclass: 'left_equation_text_2_a',
								textdata: data.string.p1text22,
							},
							{
								textclass: 'left_equation_text_3_a',
								textdata: data.string.p1text23,
							},

							//second
							{
								textclass: 'left_equation_text_1_b black',
								textdata: data.string.p1text25,
							},
							{
								textclass: 'left_equation_text_2_b black',
								textdata: data.string.p1text25,
							},
							{
								textclass: 'left_equation_text_3_b black',
								textdata: data.string.p1text23,
							},

							//second
							//third
							{
								textclass: 'left_equation_text_1_c black',
								textdata: data.string.p1text23,
							},
							{
								textclass: 'left_equation_text_2_c black',
								textdata: data.string.p1text26,
							},
							{
								textclass: 'left_equation_text_3_c black',
								textdata: data.string.p1text23,
							},


							//fourth
							{
								textclass: 'left_equation_text_1_d black',
								textdata: data.string.p1text26,
							},
							{
								textclass: 'left_equation_text_2_d black',
								textdata: data.string.p1text23,
							},
							{
								textclass: 'left_equation_text_3_d black',
								textdata: data.string.p1text23,
							}
						],
						imageblock : [{
							imagestoshow : [
								//1
								{
									imgclass : "plus-1_a",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_a",
									imgsrc : imgpath + "equal.png",
								},


								//2
								{
									imgclass : "plus-1_b",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_b",
									imgsrc : imgpath + "equal.png",
								},


								//3
								{
									imgclass : "plus-1_c",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_c",
									imgsrc : imgpath + "equal.png",
								},


								//4
								{
									imgclass : "plus-1_d",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_d",
									imgsrc : imgpath + "equal.png",
								},

								//flyfairy
								{
									imgclass : "fly-top",
									imgsrc : imgpath + "fly.png",
								}
							],
						}],
						speechbox:[{
							speechbox: 'sp-top',
							textdata : data.string.p1text10,
							textclass : 'txt1',
							imgclass: 'box',
							imgid : 'box1',
							imgsrc: '',
						}]
					},


					//slide 11
					{
						hasheaderblock : false,
						contentblocknocenteradjust : true,
						contentblockadditionalclass : 'bg_pool',

						extratextblock:[
							//first
							{
								textclass: 'left_equation_text_1_a',
								textdata: data.string.p1text21,
							},
							{
								textclass: 'left_equation_text_2_a',
								textdata: data.string.p1text22,
							},
							{
								textclass: 'left_equation_text_3_a',
								textdata: data.string.p1text23,
							},

							//second
							{
								textclass: 'left_equation_text_1_b black',
								textdata: data.string.p1text25,
							},
							{
								textclass: 'left_equation_text_2_b black',
								textdata: data.string.p1text25,
							},
							{
								textclass: 'left_equation_text_3_b black',
								textdata: data.string.p1text23,
							},

							//second
							//third
							{
								textclass: 'left_equation_text_1_c black',
								textdata: data.string.p1text23,
							},
							{
								textclass: 'left_equation_text_2_c black',
								textdata: data.string.p1text26,
							},
							{
								textclass: 'left_equation_text_3_c black',
								textdata: data.string.p1text23,
							},


							//fourth
							{
								textclass: 'left_equation_text_1_d black',
								textdata: data.string.p1text26,
							},
							{
								textclass: 'left_equation_text_2_d black',
								textdata: data.string.p1text23,
							},
							{
								textclass: 'left_equation_text_3_d black',
								textdata: data.string.p1text23,
							}
						],
						imageblock : [{
							imagestoshow : [
								//1
								{
									imgclass : "plus-1_a",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_a",
									imgsrc : imgpath + "equal.png",
								},


								//2
								{
									imgclass : "plus-1_b",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_b",
									imgsrc : imgpath + "equal.png",
								},


								//3
								{
									imgclass : "plus-1_c",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_c",
									imgsrc : imgpath + "equal.png",
								},


								//4
								{
									imgclass : "plus-1_d",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_d",
									imgsrc : imgpath + "equal.png",
								},

								//flyfairy
								{
									imgclass : "fly-top",
									imgsrc : imgpath + "fly.png",
								}
							],
						}],
						speechbox:[{
							speechbox: 'sp-top',
							textdata : data.string.p1text12,
							textclass : 'txt1',
							imgclass: 'box',
							imgid : 'box1',
							imgsrc: '',
						}]
					},


					//slide 12
					{
						hasheaderblock : false,
						contentblocknocenteradjust : true,
						contentblockadditionalclass : 'bg_pool',

						extratextblock:[
							//first
							{
								textclass: 'left_equation_text_1_a',
								textdata: data.string.p1text21,
							},
							{
								textclass: 'left_equation_text_2_a',
								textdata: data.string.p1text22,
							},
							{
								textclass: 'left_equation_text_3_a',
								textdata: data.string.p1text23,
							},

							//second
							{
								textclass: 'left_equation_text_1_b orange',
								textdata: data.string.p1text25,
							},
							{
								textclass: 'left_equation_text_2_b orange',
								textdata: data.string.p1text25,
							},
							{
								textclass: 'left_equation_text_3_b black',
								textdata: data.string.p1text23,
							},

							//second
							//third
							{
								textclass: 'left_equation_text_1_c orange',
								textdata: data.string.p1text23,
							},
							{
								textclass: 'left_equation_text_2_c orange',
								textdata: data.string.p1text26,
							},
							{
								textclass: 'left_equation_text_3_c black',
								textdata: data.string.p1text23,
							},


							//fourth
							{
								textclass: 'left_equation_text_1_d orange',
								textdata: data.string.p1text26,
							},
							{
								textclass: 'left_equation_text_2_d orange',
								textdata: data.string.p1text23,
							},
							{
								textclass: 'left_equation_text_3_d black',
								textdata: data.string.p1text23,
							}
						],
						imageblock : [{
							imagestoshow : [
								//1
								{
									imgclass : "plus-1_a",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_a",
									imgsrc : imgpath + "equal.png",
								},


								//2
								{
									imgclass : "plus-1_b",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_b",
									imgsrc : imgpath + "equal.png",
								},


								//3
								{
									imgclass : "plus-1_c",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_c",
									imgsrc : imgpath + "equal.png",
								},


								//4
								{
									imgclass : "plus-1_d",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_d",
									imgsrc : imgpath + "equal.png",
								},

								//flyfairy
								{
									imgclass : "fly-top",
									imgsrc : imgpath + "fly.png",
								}
							],
						}],
						speechbox:[{
							speechbox: 'sp-top',
							textdata : data.string.p1text13,
							textclass : 'txt1',
							imgclass: 'box',
							imgid : 'box1',
							imgsrc: '',
						}]
					},


					//slide 13
					{
						hasheaderblock : false,
						contentblocknocenteradjust : true,
						contentblockadditionalclass : 'bg_pool',

						extratextblock:[
							//first
							{
								textclass: 'left_equation_text_1_a',
								textdata: data.string.p1text21,
							},
							{
								textclass: 'left_equation_text_2_a',
								textdata: data.string.p1text22,
							},
							{
								textclass: 'left_equation_text_3_a',
								textdata: data.string.p1text23,
							},

							//second
							{
								textclass: 'left_equation_text_1_b orange',
								textdata: data.string.p1text25,
							},
							{
								textclass: 'left_equation_text_2_b orange',
								textdata: data.string.p1text25,
							},
							{
								textclass: 'left_equation_text_3_b black',
								textdata: data.string.p1text23,
							},

							//second
							//third
							{
								textclass: 'left_equation_text_1_c orange',
								textdata: data.string.p1text23,
							},
							{
								textclass: 'left_equation_text_2_c orange',
								textdata: data.string.p1text26,
							},
							{
								textclass: 'left_equation_text_3_c black',
								textdata: data.string.p1text23,
							},


							//fourth
							{
								textclass: 'left_equation_text_1_d orange',
								textdata: data.string.p1text26,
							},
							{
								textclass: 'left_equation_text_2_d orange',
								textdata: data.string.p1text23,
							},
							{
								textclass: 'left_equation_text_3_d black',
								textdata: data.string.p1text23,
							},

							//variabletag
							{
								textclass: 'variable',
								textdata: data.string.p1text27,
							}
						],
						imageblock : [{
							imagestoshow : [
								//1
								{
									imgclass : "plus-1_a",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_a",
									imgsrc : imgpath + "equal.png",
								},


								//2
								{
									imgclass : "plus-1_b",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_b",
									imgsrc : imgpath + "equal.png",
								},


								//3
								{
									imgclass : "plus-1_c",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_c",
									imgsrc : imgpath + "equal.png",
								},


								//4
								{
									imgclass : "plus-1_d",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_d",
									imgsrc : imgpath + "equal.png",
								},

								//flyfairy
								{
									imgclass : "fly-top",
									imgsrc : imgpath + "fly.png",
								},

								//arrows
								{
									imgclass : "arrow1",
									imgsrc : imgpath + "arrow1.png",
								},
								{
									imgclass : "arrow2",
									imgsrc : imgpath + "arrow1.png",
								}
							],
						}],
						speechbox:[{
							speechbox: 'sp-top',
							textdata : data.string.p1text13,
							textclass : 'txt1',
							imgclass: 'box',
							imgid : 'box1',
							imgsrc: '',
						}]
					},


					//slide 13
					{
						hasheaderblock : false,
						contentblocknocenteradjust : true,
						contentblockadditionalclass : 'bg_pool',

						extratextblock:[
							//first
							{
								textclass: 'left_equation_text_1_a',
								textdata: data.string.p1text21,
							},
							{
								textclass: 'left_equation_text_2_a',
								textdata: data.string.p1text22,
							},
							{
								textclass: 'left_equation_text_3_a',
								textdata: data.string.p1text23,
							},

							//second
							{
								textclass: 'left_equation_text_1_b orange',
								textdata: data.string.p1text25,
							},
							{
								textclass: 'left_equation_text_2_b orange',
								textdata: data.string.p1text25,
							},
							{
								textclass: 'left_equation_text_3_b black',
								textdata: data.string.p1text23,
							},

							//second
							//third
							{
								textclass: 'left_equation_text_1_c orange',
								textdata: data.string.p1text23,
							},
							{
								textclass: 'left_equation_text_2_c orange',
								textdata: data.string.p1text26,
							},
							{
								textclass: 'left_equation_text_3_c black',
								textdata: data.string.p1text23,
							},


							//fourth
							{
								textclass: 'left_equation_text_1_d orange',
								textdata: data.string.p1text26,
							},
							{
								textclass: 'left_equation_text_2_d orange',
								textdata: data.string.p1text23,
							},
							{
								textclass: 'left_equation_text_3_d black',
								textdata: data.string.p1text23,
							},

							//variabletag
							{
								textclass: 'variable',
								textdata: data.string.p1text27,
							},

							//atoz
							{
								textclass: 'atoz',
								textdata: data.string.p1text28,
								datahighlightflag:true,
								datahighlightcustomclass:'orange'
							},

						],
						imageblock : [{
							imagestoshow : [
								//1
								{
									imgclass : "plus-1_a",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_a",
									imgsrc : imgpath + "equal.png",
								},


								//2
								{
									imgclass : "plus-1_b",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_b",
									imgsrc : imgpath + "equal.png",
								},


								//3
								{
									imgclass : "plus-1_c",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_c",
									imgsrc : imgpath + "equal.png",
								},


								//4
								{
									imgclass : "plus-1_d",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_d",
									imgsrc : imgpath + "equal.png",
								},

								//flyfairy
								{
									imgclass : "fly-top",
									imgsrc : imgpath + "fly.png",
								},

								//arrows
								{
									imgclass : "arrow1",
									imgsrc : imgpath + "arrow1.png",
								},
								{
									imgclass : "arrow2",
									imgsrc : imgpath + "arrow1.png",
								}
							],
						}],
						speechbox:[{
							speechbox: 'sp-top',
							textdata : data.string.p1text14,
							textclass : 'txt1',
							imgclass: 'box',
							imgid : 'box1',
							imgsrc: '',
						}]
					},

					//slide 14
					{
						hasheaderblock : false,
						contentblocknocenteradjust : true,
						contentblockadditionalclass : 'bg_pool',

						extratextblock:[
							//first
							{
								textclass: 'left_equation_text_1_a',
								textdata: data.string.p1text21,
							},
							{
								textclass: 'left_equation_text_2_a',
								textdata: data.string.p1text22,
							},
							{
								textclass: 'left_equation_text_3_a orange',
								textdata: data.string.p1text23,
							},

							//second
							{
								textclass: 'left_equation_text_1_b black',
								textdata: data.string.p1text25,
							},
							{
								textclass: 'left_equation_text_2_b black',
								textdata: data.string.p1text25,
							},
							{
								textclass: 'left_equation_text_3_b orange',
								textdata: data.string.p1text23,
							},

							//second
							//third
							{
								textclass: 'left_equation_text_1_c black',
								textdata: data.string.p1text23,
							},
							{
								textclass: 'left_equation_text_2_c black',
								textdata: data.string.p1text26,
							},
							{
								textclass: 'left_equation_text_3_c orange',
								textdata: data.string.p1text23,
							},


							//fourth
							{
								textclass: 'left_equation_text_1_d black',
								textdata: data.string.p1text26,
							},
							{
								textclass: 'left_equation_text_2_d black',
								textdata: data.string.p1text23,
							},
							{
								textclass: 'left_equation_text_3_d orange',
								textdata: data.string.p1text23,
							},


						],
						imageblock : [{
							imagestoshow : [
								//1
								{
									imgclass : "plus-1_a",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_a",
									imgsrc : imgpath + "equal.png",
								},


								//2
								{
									imgclass : "plus-1_b",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_b",
									imgsrc : imgpath + "equal.png",
								},


								//3
								{
									imgclass : "plus-1_c",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_c",
									imgsrc : imgpath + "equal.png",
								},


								//4
								{
									imgclass : "plus-1_d",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_d",
									imgsrc : imgpath + "equal.png",
								},

								//flyfairy
								{
									imgclass : "fly-top",
									imgsrc : imgpath + "fly.png",
								}
							],
						}],
						speechbox:[{
							speechbox: 'sp-top',
							textdata : data.string.p1text15,
							textclass : 'txt1',
							imgclass: 'box',
							imgid : 'box1',
							imgsrc: '',
						}]
					},

					//slide 15
					{
						hasheaderblock : false,
						contentblocknocenteradjust : true,
						contentblockadditionalclass : 'bg_pool',

						extratextblock:[
							//first
							{
								textclass: 'left_equation_text_1_a',
								textdata: data.string.p1text21,
							},
							{
								textclass: 'left_equation_text_2_a',
								textdata: data.string.p1text22,
							},
							{
								textclass: 'left_equation_text_3_a orange',
								textdata: data.string.p1text23,
							},

							//second
							{
								textclass: 'left_equation_text_1_b black',
								textdata: data.string.p1text25,
							},
							{
								textclass: 'left_equation_text_2_b black',
								textdata: data.string.p1text25,
							},
							{
								textclass: 'left_equation_text_3_b orange',
								textdata: data.string.p1text23,
							},

							//second
							//third
							{
								textclass: 'left_equation_text_1_c black',
								textdata: data.string.p1text23,
							},
							{
								textclass: 'left_equation_text_2_c black',
								textdata: data.string.p1text26,
							},
							{
								textclass: 'left_equation_text_3_c orange',
								textdata: data.string.p1text23,
							},


							//fourth
							{
								textclass: 'left_equation_text_1_d black',
								textdata: data.string.p1text26,
							},
							{
								textclass: 'left_equation_text_2_d black',
								textdata: data.string.p1text23,
							},
							{
								textclass: 'left_equation_text_3_d orange',
								textdata: data.string.p1text23,
							},


						],
						imageblock : [{
							imagestoshow : [
								//1
								{
									imgclass : "plus-1_a",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_a",
									imgsrc : imgpath + "equal.png",
								},


								//2
								{
									imgclass : "plus-1_b",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_b",
									imgsrc : imgpath + "equal.png",
								},


								//3
								{
									imgclass : "plus-1_c",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_c",
									imgsrc : imgpath + "equal.png",
								},


								//4
								{
									imgclass : "plus-1_d",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_d",
									imgsrc : imgpath + "equal.png",
								},

								//flyfairy
								{
									imgclass : "fly-top",
									imgsrc : imgpath + "fly.png",
								}
							],
						}],
						speechbox:[{
							speechbox: 'sp-top',
							textdata : data.string.p1text16,
							textclass : 'txt1',
							imgclass: 'box',
							imgid : 'box1',
							imgsrc: '',
						}]
					},

					//slide 16
					{
						hasheaderblock : false,
						contentblocknocenteradjust : true,
						contentblockadditionalclass : 'bg_pool',

						extratextblock:[
							//first
							{
								textclass: 'left_equation_text_1_a',
								textdata: data.string.p1text21,
							},
							{
								textclass: 'left_equation_text_2_a',
								textdata: data.string.p1text22,
							},
							{
								textclass: 'left_equation_text_3_a orange',
								textdata: data.string.p1text23,
							},

							//second
							{
								textclass: 'left_equation_text_1_b black',
								textdata: data.string.p1text25,
							},
							{
								textclass: 'left_equation_text_2_b black',
								textdata: data.string.p1text25,
							},
							{
								textclass: 'left_equation_text_3_b orange',
								textdata: data.string.p1text23,
							},

							//second
							//third
							{
								textclass: 'left_equation_text_1_c black',
								textdata: data.string.p1text23,
							},
							{
								textclass: 'left_equation_text_2_c black',
								textdata: data.string.p1text26,
							},
							{
								textclass: 'left_equation_text_3_c orange',
								textdata: data.string.p1text23,
							},


							//fourth
							{
								textclass: 'left_equation_text_1_d black',
								textdata: data.string.p1text26,
							},
							{
								textclass: 'left_equation_text_2_d black',
								textdata: data.string.p1text23,
							},
							{
								textclass: 'left_equation_text_3_d orange',
								textdata: data.string.p1text23,
							},


						],
						imageblock : [{
							imagestoshow : [
								//1
								{
									imgclass : "plus-1_a",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_a",
									imgsrc : imgpath + "equal.png",
								},


								//2
								{
									imgclass : "plus-1_b",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_b",
									imgsrc : imgpath + "equal.png",
								},


								//3
								{
									imgclass : "plus-1_c",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_c",
									imgsrc : imgpath + "equal.png",
								},


								//4
								{
									imgclass : "plus-1_d",
									imgsrc : imgpath + "plus.png",
								},
								{
									imgclass : "equal-1_d",
									imgsrc : imgpath + "equal.png",
								},

								//flyfairy
								{
									imgclass : "fly-top",
									imgsrc : imgpath + "fly.png",
								}
							],
						}],
						speechbox:[{
							speechbox: 'sp-top',
							textdata : data.string.p1text17,
							textclass : 'txt1',
							imgclass: 'box',
							imgid : 'box1',
							imgsrc: '',
						}]
					},

					//slide17
					{
						hasheaderblock : false,
						contentblocknocenteradjust : true,
						// contentblockadditionalclass : 'bg_pool',

						imageblock : [{
							imagestoshow : [
								{
									imgclass : "fly-1anim",
									imgsrc : imgpath + "fly.png",
								}
							],
						}],
						speechbox:[{
							speechbox: 'sp-1',
							textdata : data.string.p1s8txt,
							textclass : 'txt1',
							imgclass: 'box',
							imgid : 'box1',
							imgsrc: '',
						}]
					},

					//slide18
					{
						hasheaderblock : false,
						contentblocknocenteradjust : true,
						//contentblockadditionalclass : 'bg_pool',
						extratextblock:[
							{
								textclass: 'p1s9txt1box',
								textdata: data.string.p1s9txt1,
							},
							{
								datahighlightflag:true,
								datahighlightcustomclass:"proxmatxt",
								textclass: 'p1s9txt2box',
								textdata: data.string.p1s9txt2,
							},

							{
								textclass: 'p1s95',
								textdata: data.string.p1s95,
							},

							{
								textclass: 'p1s9w',
								textdata: data.string.p1s9w,
							},
							{
								textclass: 'p1s9wt',
								textdata: data.string.p1s9wt,
							},
							{
								textclass: 'p1s9nw3',
								textdata: data.string.p1s9nw,
							},
							{
								textclass: 'p1s9nw4',
								textdata: data.string.p1s9nw,
							}


						],
						imageblock : [{
							imagestoshow : [
								{
									imgclass : "s10kg",
									imgsrc : imgpath + "kilogram.png",
								},
								{
									imgclass : "s10machine",
									imgsrc : imgpath + "machine.png",
								},
								{
									imgclass : "s10boy",
									imgsrc : imgpath + "boy01.png",
								}



							],
						}]
					},
					//slide19
					{
						hasheaderblock : false,
						contentblocknocenteradjust : true,
						//contentblockadditionalclass : 'bg_pool',
						extratextblock:[
							{
								textclass: 'p1s10txt1box',
								textdata: data.string.p1s10txt1,
							},
							{
								textclass: 'p1s10txt2box',
								textdata: data.string.p1s10txt2,
							},

							{
								textclass: 'p1s10p',
								textdata: data.string.p1s10p,
							},

							{
								textclass: 'p1s10w10',
								textdata: data.string.p1s10w10,
							},
							{
								textclass: 'p1s10price',
								textdata: data.string.p1s10price,
							},
							{
								textclass: 'p1s9nw1',
								textdata: data.string.p1s9nw,
							},
							{
								textclass: 'p1s9nw2',
								textdata: data.string.p1s9nw,
							}

						],
						imageblock : [{
							imagestoshow : [
								{
									imgclass : "s10note10",
									imgsrc : imgpath + "10-front.png",
								},
								{
									imgclass : "pricetag",
									imgsrc : imgpath + "pricetag.png",
								},
								{
									imgclass : "apples10",
									imgsrc : imgpath + "apple01.png",
								}



							],
						}]
					},
					//slide20
					{
						hasheaderblock : false,
						contentblocknocenteradjust : true,
						//contentblockadditionalclass : 'bg_pool',
						extratextblock:[
							{
								textclass: 'p1s11txt1box',
								textdata: data.string.p1s11txt1,
							},
							{
								textclass: 'p1s11txt2box',
								textdata: data.string.p1s11txt2,
							},

							{
								textclass: 'p1s11p',
								textdata: data.string.p1s11p,
							},

							{
								textclass: 'p1s11w',
								textdata: data.string.p1s11w,
							},
							{
								textclass: 'p1s11x',
								textdata: data.string.p1s11x,
							},
							{
								textclass: 'p1s11y',
								textdata: data.string.p1s11y,
							},
							{
								textclass: 'p1s11w10',
								textdata: data.string.p1s11w10,
							},
							{
								textclass: 'p1s11w7',
								textdata: data.string.p1s11w7,
							},
							{
								textclass: 'p1s11w5',
								textdata: data.string.p1s11w5,
							},
							{
								textclass: 'p1s11w15',
								textdata: data.string.p1s11w15,
							},
							{
								textclass: 'p1s9nw1',
								textdata: data.string.p1s9nw,
							},
							{
								textclass: 'p1s9nw2',
								textdata: data.string.p1s9nw,
							}

						],
						imageblock : [{
							imagestoshow : [
								{
									imgclass : "fly-1",
									imgsrc : imgpath + "fly.png",
								}
							]
						}],
						speechbox:[{
							speechbox: 'sp-last',
							textdata : data.string.p1text29,
							textclass : 'txt1',
							imgclass: 'box',
							imgid : 'box1',
							imgsrc: '',
						}]
					}
				];

				$(function() {

					/*
					var $board = $(".board");
					var $nextBtn = $("#activity-page-next-btn-enabled");
					var $refreshBtn= $("#activity-page-refresh-btn");
					var $prevBtn = $("#activity-page-prev-btn-enabled");
					var countNext = 0;
					var $total_page = content.length;
					var current_sound = sound_1;
					var myTimeout =  null;
					var myTimeout2 =  null;
					var timeoutvar =  null;
					var vocabcontroller =  new Vocabulary();
					vocabcontroller.init();*/
					/*
					inorder to use the handlebar partials we need to register them
					to their respective handlebar partial pointer first
					*/
					var $board = $('.board');
					var $nextBtn = $("#activity-page-next-btn-enabled");
					var $prevBtn = $("#activity-page-prev-btn-enabled");
					var $refreshBtn= $("#activity-page-refresh-btn");
					var countNext = 0;

					var $total_page = content.length;
					loadTimelineProgress($total_page,countNext+1);
					var vocabcontroller =  new Vocabulary();
					vocabcontroller.init();

					var preload;
					var timeoutvar = null;
					var current_sound;

					function init() {
						//specify type otherwise it will load assests as XHR
						manifest = [
							//images
							{id: "box1", src: "images/textbox/white/lb-1.png", type: createjs.AbstractLoader.IMAGE},
							{id: "box2", src: "images/textbox/white/lb-1.png", type: createjs.AbstractLoader.IMAGE},


							// soundsa
							{id: "sound_1", src: soundAsset+"s1_p1.ogg"},
							{id: "sound_2", src: soundAsset+"s1_p2.ogg"},
							{id: "sound_3", src: soundAsset+"s1_p3.ogg"},
							{id: "sound_4", src: soundAsset+"s1_p4.ogg"},
							{id: "sound_5", src: soundAsset+"s1_p5.ogg"},
							{id: "sound_6", src: soundAsset+"s1_p6.ogg"},
							{id: "sound_7", src: soundAsset+"s1_p7.ogg"},
							{id: "sound_8", src: soundAsset+"s1_p8.ogg"},
							{id: "sound_9", src: soundAsset+"s1_p9.ogg"},
							{id: "sound_10", src: soundAsset+"s1_p10.ogg"},
							{id: "sound_11", src: soundAsset+"s1_p11.ogg"},
							{id: "sound_12", src: soundAsset+"s1_p12.ogg"},
							{id: "sound_13", src: soundAsset+"s1_p13.ogg"},
							{id: "sound_14", src: soundAsset+"s1_p14.ogg"},
							{id: "sound_15", src: soundAsset+"s1_p15.ogg"},
							{id: "sound_16", src: soundAsset+"s1_p16.ogg"},
							{id: "sound_17", src: soundAsset+"s1_p17.ogg"},
							{id: "sound_18", src: soundAsset+"s1_p18.ogg"},
							{id: "sound_19", src: soundAsset+"s1_p19.ogg"},
							{id: "sound_20_1", src: soundAsset+"s1_p20_1.ogg"},
							{id: "sound_20_2", src: soundAsset+"s1_p20_2.ogg"},
							{id: "sound_21_1", src: soundAsset+"s1_p21_1.ogg"},
							{id: "sound_21_2", src: soundAsset+"s1_p21_2.ogg"},
							{id: "sound_22", src: soundAsset+"s1_p22.ogg"},

						];
						preload = new createjs.LoadQueue(false);
						preload.installPlugin(createjs.Sound);//for registering sounds
						preload.on("progress", handleProgress);
						preload.on("complete", handleComplete);
						preload.on("fileload", handleFileLoad);
						preload.loadManifest(manifest, true);
					}
					function handleFileLoad(event) {
						// console.log(event.item);
					}
					function handleProgress(event) {
						$('#loading-text').html(parseInt(event.loaded*100)+'%');
					}
					function handleComplete(event) {
						$('#loading-wrapper').hide(0);
						//initialize varibales
						current_sound = createjs.Sound.play('sound_1');
						current_sound.stop();
						// call main function
						templateCaller();
					}
					//initialize
					init();

					Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
					Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
					Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

					// controls the navigational state of the program
					// next btn is disabled for this page
					function navigationController(islastpageflag) {
						typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
						// if lastpageflag is true
						// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
					}

					var count=0;
					function generalTemplate() {
						var source = $("#general-template").html();
						var template = Handlebars.compile(source);
						var html = template(content[countNext]);
						is_last_page= false;
						$board.html(html);
						loadTimelineProgress($total_page, countNext + 1);
						// highlight any text inside board div with datahighlightflag set true
						texthighlight($board);
						vocabcontroller.findwords(countNext);
						put_speechbox_image(content, countNext);
						if(countNext<19){
							sound_player('sound_'+(countNext+1));
						}
						switch (countNext) {
							case 1:
							$('.plus_sign,.minus_sign,.multiply_sign,.division_sign').hide(0);
							setTimeout(function(){
								$('.plus_sign').fadeIn(500,function(){
									$('.minus_sign').fadeIn(500,function(){
										$('.multiply_sign').fadeIn(500,function(){
											$('.division_sign').fadeIn(500,function(){
												// nav_button_controls(100);
											});
										});
									});
								});
							},4000);
							break;

							case 8:
							$prevBtn.show(0);
							$('.what-2-add').html(data.string.p1s3txt1);
							$('.block-1a').appendTo('.board').animate({
								'left': '31%',
								'bottom': '47%',
								'z-index': '4'
							}, 1000, function(){
								$('.what-2-add').html(data.string.p1s3txt2);
								$('.block-1a').eq(1).children('p').fadeIn(1000, function(){	});
								$('.block-1b').appendTo('.board').animate({
									'left': '31%',
									'bottom': '54%',
									'z-index': '4'
								},
								1000, function(){
									$('.what-2-add').html(data.string.p1s3txt3);
									$('.block-1b').eq(1).children('p').fadeIn(1000, function(){	});
									$('.block-1c').appendTo('.board').animate({
										'left': '31%',
										'bottom': '61%',
										'z-index': '4'
									},
									1000, function(){
										$('.what-2-add').html(data.string.p1s3txt4);
										$('.block-1c').eq(1).children('p').fadeIn(1000, function(){	});
										$('.block-1d').appendTo('.board').animate({
											'left': '31%',
											'bottom': '68%',
											'z-index': '4'
										},
										1000, function(){
											$('.what-2-add').html(data.string.p1s3txt5);
											$('.block-1d').eq(1).children('p').fadeIn(1000, function(){	});
											$('.block-1e').appendTo('.board').animate({
												'left': '31%',
												'bottom': '75%',
												'z-index': '4'
											}, 1000, function(){
												$('.what-2-add').html(data.string.t_3);
												$('.block-1e').eq(1).children('p').fadeIn(1000, function(){
													$nextBtn.show(0);
												});
											});
										});
									});
								});
							});
							break;

							case 9:
							$('.later_text').hide(0).delay(1000).fadeIn(1000);
							$('.sp-2').css({"left": "21%",
							"width": "59%"});
							$nextBtn.show(0);

							break;

							case 14:
							$('.atoz').hide(0);
							$('.sp-top').css({
								"top": "1%",

								"left": "27%",

								"width": "55%",

								"height": "31%"
							}).hide(0).fadeIn(500,function(){
								$('.atoz').fadeIn(500);
							});

							break;

							case 17:
							$('.sp-top').css({
								"top": "1%",

								"left": "27%",

								"width": "55%",

								"height": "31%"
							}).hide(0).fadeIn(500);
							break;

							case 18:
							$('.sp-1').css('height','29%');
							wtInc();
							break;
							case 19:
							createjs.Sound.stop();
							current_sound = createjs.Sound.play('sound_20_1');
							current_sound.play();
							current_sound.on('complete', function(){
								sound_player('sound_20_2');
							});
							prInc();
							break;
							case 20:
							createjs.Sound.stop();
							current_sound = createjs.Sound.play('sound_21_1');
							current_sound.play();
							current_sound.on('complete', function(){
								sound_player('sound_21_2');
							});
							break;
							case 21:
							sound_player('sound_22');
							break;

							// default:
							// 	nav_button_controls(100);
							// 	break;

						}
					}

					var interval;
					function wtInc() {
						interval = setInterval(function() {
							if (count < 10) {
								$('.p1s9wt').html("" + (25 + count * 2));
								count++;
							} else {
								count = 0;
							}
						}, 1000);
					}


					function prInc(){
						var interval1=setInterval(function(){
							if(count<10)
							{
								$('.p1s10price').html(""+(10+count*2));
								count++;
							}
							else
							{
								count=0;
							}
						},1000);

					}

					function nav_button_controls(delay_ms){
						timeoutvar = setTimeout(function(){
							if(countNext==0){
								$nextBtn.show(0);
							} else if( countNext>0 && countNext == $total_page-1){
								$prevBtn.show(0);
								ole.footerNotificationHandler.pageEndSetNotification();
							} else{
								$prevBtn.show(0);
								$nextBtn.show(0);
							}
						},delay_ms);
					}
					function sound_player(sound_id){
						createjs.Sound.stop();
						current_sound = createjs.Sound.play(sound_id);
						current_sound.play();
						current_sound.on('complete', function(){
							nav_button_controls(100);
						});
					}
					function put_speechbox_image(content, count){
						if(content[count].hasOwnProperty('speechbox')){
							var speechbox = content[count].speechbox;
							for(var i=0; i<speechbox.length; i++){
								var image_src = preload.getResult(speechbox[i].imgid).src;
								var selector = ('.'+speechbox[i].speechbox+' > .speechbg');
								console.log("imgsrc---"+image_src, $(selector));
								$(selector).attr('src', image_src);
							}
						}
					}
					function templateCaller() {
						//convention is to always hide the prev and next button and show them based
						//on the convention or page index
						$prevBtn.hide(0);
						$nextBtn.hide(0);
						navigationController();

						generalTemplate();

					}


					$nextBtn.on("click", function() {
						clearTimeout(timeoutvar);
						// clearTimeout(myTimeout);
						// clearTimeout(myTimeout2);
						switch(countNext){
							default:
							current_sound.stop();
							countNext++;
							templateCaller();
							break;
						}

					});

					$refreshBtn.click(function(){
						templateCaller();
					});

					$prevBtn.on("click", function() {
						clearTimeout(timeoutvar);
						// clearTimeout(myTimeout);
						// clearTimeout(myTimeout2);
						current_sound.stop();
						countNext--;
						templateCaller();
					});

					total_page = content.length;
					//templateCaller();

				});



				/*===============================================
				=            data highlight function            =
				===============================================*/
				function texthighlight($highlightinside) {
					//check if $highlightinside is provided
					typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

					var $alltextpara = $highlightinside.find("*[data-highlight='true']");
					var stylerulename;
					var replaceinstring;
					var texthighlightstarttag;
					var texthighlightendtag = "</span>";

					if ($alltextpara.length > 0) {
						$.each($alltextpara, function(index, val) {
							/*if there is a data-highlightcustomclass attribute defined for the text element
							use that or else use default 'parsedstring'*/
							$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
							( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

							texthighlightstarttag = "<span class = " + stylerulename + " >";

							replaceinstring = $(this).html();
							replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
							replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

							$(this).html(replaceinstring);
						});
					}
				}

				/*=====  End of data highlight function  ======*/

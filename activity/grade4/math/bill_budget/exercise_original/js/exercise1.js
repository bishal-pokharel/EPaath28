var imgpath = $ref+"/images/";

var shops = [
				[data.string.shop1_name, data.string.shop1_address, data.string.shop1_owner, imgpath+'shop_bill/logo_flower.png', [data.string.s1_item_1, data.string.s1_item_2, data.string.s1_item_3, data.string.s1_item_4, data.string.s1_item_5, data.string.s1_item_6], [200, 150, 250, 120, 175, 225] ],
				[data.string.shop2_name, data.string.shop2_address, data.string.shop2_owner, imgpath+'shop_bill/logo_toy.png', [data.string.s2_item_1, data.string.s2_item_2, data.string.s2_item_3, data.string.s2_item_4, data.string.s2_item_5, data.string.s2_item_6], [100, 120, 300, 500, 150, 350] ],
				[data.string.shop3_name, data.string.shop3_address, data.string.shop3_owner, imgpath+'shop_bill/logo_store.png', [data.string.s3_item_1, data.string.s3_item_2, data.string.s3_item_3, data.string.s3_item_4, data.string.s3_item_5, data.string.s3_item_6], [20, 200, 25, 30, 5, 20] ],
				[data.string.shop4_name, data.string.shop4_address, data.string.shop4_owner, imgpath+'shop_bill/logo_cloth.png', [data.string.s4_item_1, data.string.s4_item_2, data.string.s4_item_3, data.string.s4_item_4, data.string.s4_item_5, data.string.s4_item_6], [1200, 500, 600, 1000, 1400, 100] ],
				[data.string.shop5_name, data.string.shop5_address, data.string.shop5_owner, imgpath+'shop_bill/logo_station.png', [data.string.s5_item_1, data.string.s5_item_2, data.string.s5_item_3, data.string.s5_item_4, data.string.s5_item_5, data.string.s5_item_6], [50, 70, 80, 250, 75, 50] ]
			];
			// name, location, shopkeeper, logo, items, price
var custs = [
				[data.string.ecustomer_name_1, data.string.ecustomer_address_1],
				[data.string.ecustomer_name_2, data.string.ecustomer_address_2],
				[data.string.ecustomer_name_3, data.string.ecustomer_address_3],
				[data.string.ecustomer_name_4, data.string.ecustomer_address_4],
				[data.string.ecustomer_name_5, data.string.ecustomer_address_5],
			];
			//name, address
var signs  = [
	imgpath+'signatures/s01.png',
	imgpath+'signatures/s02.png',
	imgpath+'signatures/s03.png',
	imgpath+'signatures/s04.png',
	imgpath+'signatures/s05.png'
]

var questions = [data.string.ex_q1, data.string.ex_q2, data.string.ex_q3, data.string.ex_q4, data.string.ex_q5];
var q_array = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
var shop_array = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
var cust_array = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
var item_array = [0, 1, 2, 3, 4, 5];


var content=[
	{
	//slide 0
		exerciseblock: [
		{
			ques_class: 'my_font_big',
			textdata: "asd,sadsa sadas",
			imageoptions: [
			{
				textclass: "option option-1 ole-template-check-btn-default",
				textdata: 'asd 1'
			},
			{
				textclass: "option option-2 ole-template-check-btn-default",
				textdata: 'asdsad 2'
			},
			{
				textclass: "option option-3 ole-template-check-btn-default",
				textdata: 'asdsadsad 3'
			},
			{
				textclass: "option option-4 ole-template-check-btn-default",
				textdata: 'asdsadsad 4'
			}],
		}],
		bill_item:[{
			bill_container: 'bill_right',
			shop_logo: imgpath + "shop_bill/logo_1.svg",
			shop_name: data.string.shop2_name,
			shop_address: data.string.shop2_address,
			customer_name:'Mark',
			customer_address:'USA',
			customer_bill: '300',
			customer_date: '12/12',
			signature: imgpath + "shop_bill/sign.png",
			shopkeeper_name: data.string.shop2_owner
		}],


	},
];

/*remove this for non random questions*/
q_array.shufflearray();
shop_array.shufflearray();
cust_array.shufflearray();

$(function (){
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 10;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	var score = 0;
	var testin = new EggTemplate();

 	testin.init(10);

 	var rand_ques = 0;
 	var rand_shop = 0;
 	var rand_cust = 0;
 	var rand_item = 0;
 	var current_list = [1, 2, 3];

	/*values in this array is same as the name of images of eggs in image folder*/
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[0]);
		$board.html(html);

		/*generate question no at the beginning of question*/
		testin.numberOfQuestions();
		rand_ques = q_array[countNext]%5;
		rand_shop = q_array[countNext]%5;
		rand_cust = q_array[countNext]%5;

		rand_item = 3 + Math.floor(Math.random()*4);
		item_array.shufflearray();
		current_list = item_array.slice(0, rand_item);

		$('.question').html(questions[rand_ques]);
		var ansClicked = false;
		var wrngClicked = false;

		var parent = $(".optionsdiv");
		var divs = parent.children();
		while (divs.length) {
			parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
		$(".question").prepend((countNext+1)+". ");
		switch(countNext) {
			default:
				var my_bill = new bill();
				my_bill.reset_bill_items();
				my_bill.init(shops[rand_shop][3], shops[rand_shop][0], shops[rand_shop][1], custs[rand_cust][0], custs[rand_cust][1], 'bill_container');
				var new_items = get_items(current_list, rand_shop);
				my_bill.set_shopkeeper_name(shops[rand_shop][2]);
				my_bill.add_items(new_items);
				my_bill.update_bill('.bill_center');
				my_bill.set_bill_sign(signs[rand_shop]);
				//bill sn no fix
				for(var i=1; i<current_list.length; i++){
					$('.bill_table>tbody>tr').eq(i).children('td').eq(0).html(i);
				}
				switch(rand_ques){
					case 0://total money
						$('.option').css('width', '35%');
						$('.option-1').html($('.total_amount_data').html());
						$('.option-2').html($('.customer_bill').html());
						$('.option-3').html(new_items[0].get_amount());
						$('.option-4').html(new_items[1].get_item_rate());
						break;

					case 1://bill no
						$('.option').css('width', '35%');
						var r_item_idx = 1 +  Math.floor(Math.random()*(new_items.length-1));
						$('.option-1').html($('.customer_bill').html());
						$('.option-2').html($('.total_amount_data').html());
						$('.option-3').html(new_items[r_item_idx].get_amount());
						$('.option-4').html(new_items[r_item_idx-1].get_item_rate());
						break;

					case 2://cust name
						$('.option-1').html(custs[rand_cust][0]);
						$('.option-2').html(shops[rand_shop][2]);
						$('.option-3').html(shops[rand_shop][0]);
						$('.option-4').html(custs[rand_cust][1]);
						break;

					case 3://no of item buy
						$('.option').css('width', '35%');
						var r_item_idx = 1 +  Math.floor(Math.random()*(new_items.length-1));
						$('.q_span_1').html($('.bill_table>tbody>tr').eq(r_item_idx).children('td').eq(1).html());
						$('.q_span_2').html(custs[rand_cust][0]);
						console.log($('.bill_table>tbody>tr').eq(r_item_idx).children('td').eq(1).html());
						$('.option-1').html($('.bill_table>tbody>tr').eq(r_item_idx).children('td').eq(2).html());
						$('.option-2').html(parseInt($('.bill_table>tbody>tr').eq(r_item_idx).children('td').eq(2).html()) + 2);
						$('.option-3').html($('.bill_table>tbody>tr').eq(r_item_idx).children('td').eq(4).html());
						$('.option-4').html($('.bill_table>tbody>tr').eq(r_item_idx).children('td').eq(3).html());
						break;
					case 4://shop name
					default:
						$('.option-1').html(shops[rand_shop][0]);
						$('.option-3').html(custs[rand_cust][0]);
						$('.option-2').html(shops[rand_shop][2]);
						$('.option-4').html(custs[rand_shop][1]);
						break;
				}

				$('.option').click(function(){
					if (ansClicked == false) {
						if ($(this).hasClass("option-1")) {
							if (wrngClicked == false) {
								testin.update(true);
							}
							play_correct_incorrect_sound(1);
							$(this).removeClass('ole-template-check-btn-default-incorrect');
							$(this).addClass('ole-template-check-btn-default-correct');
							$(this).siblings(".corctopt").show(0);
							ansClicked = true;
							$('.option').css('pointer-events', 'none');
							nav_button_controls(100);
						} else {
							$(this).css('pointer-events', 'none');
							testin.update(false);
							play_correct_incorrect_sound(0);
							$(this).addClass('ole-template-check-btn-default-incorrect');
							$(this).siblings(".wrngopt").show(0);
							wrngClicked = true;
						}
					}
				});
			break;
		}

		function get_items(rand_list, shop_no){
			var i = 0;
			var new_items = [];
			while(i < rand_list.length){
				var rand_no_of_item = 1 + Math.floor(Math.random()*5);
				new_items.push(new bill_element(shops[shop_no][4][rand_list[i]], rand_no_of_item, shops[shop_no][5][rand_list[i]]));
				i++;
			}
			return new_items;
		}
	}
	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
		testin.gotoNext();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

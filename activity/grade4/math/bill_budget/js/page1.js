var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+ $lang + "/";

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblockadditionalclass: 'main-bg',
		contentblocknocenteradjust : true,

		uppertextblockadditionalclass : "block_cover",
		uppertextblock : [{
			textdata : data.lesson.chapter,
			textclass : 'lesson-title vertical-horizontal-center'
		}]
	},

	//slide1
	{
		contentblockadditionalclass: 'go_shop_bg',
		contentblocknocenteradjust : true,
		extratextblock : [{
			textdata : data.string.p1text1,
			textclass : 'template-dialougebox2-top-flipped-yellow dg-1'
		},]
	},
	//slide2
	{
		contentblockadditionalclass: 'shop_bg_2',
		contentblocknocenteradjust : true,

		uppertextblockadditionalclass : "shop_name_board shop_name_2 top_text fntthr",
		uppertextblock : [
		{
			textdata : data.string.shop5_name_1,
			textclass : 'shop_name_text'
		},
		{
			textdata : data.string.p1text4,
			textclass : 'take_bill',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		},
		],
		lowertextblockadditionalclass : "done_shopping my_font_big front_image",
		lowertextblock : [
		{
			textdata : data.string.done_shopping,
			textclass : '',
		}],
		imageblockadditionalclass: 'block_cover',
		imageblock : [{
			imagelabels: [{
				imagelabelclass : "shelf_top shelf_2",
				imagelabeldata : '',
			},
			{
				imagelabelclass : "shelf_bottom shelf_2",
				imagelabeldata : '',
			},
			{
				imagelabelclass : "floor floor_2",
				imagelabeldata : '',
			},
			{
				imagelabelclass : "black_image",
				imagelabeldata : '',
			}]
		}],
		basketblockadditionalclass: 'basket',
		basketblock:[{
			imgclass : "basket_front",
			imgsrc : imgpath + "basket_front.png",
		},
		{
			imgclass : "basket_back",
			imgsrc : imgpath + "basketback.png",
		}],
		shoppingblockadditionalclass: 'block_cover',
		shoppingblock : [
			{
				imgclass : "shop_items shop_item_1 top_shelf",
				item_name : data.string.s5_item_13,
				imgsrc : imgpath + "s_item_1.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 60'
			},
			{
				imgclass : "shop_items shop_item_1 top_shelf",
				item_name : data.string.s5_item_13,
				imgsrc : imgpath + "s_item_1.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 60'
			},
			{
				imgclass : "shop_items shop_item_1 top_shelf",
				item_name : data.string.s5_item_13,
				imgsrc : imgpath + "s_item_1.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 60'
			},
			{
				imgclass : "shop_items shop_item_1 top_shelf",
				item_name : data.string.s5_item_13,
				imgsrc : imgpath + "s_item_1.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 60'
			},
			{
				imgclass : "shop_items shop_item_2 top_shelf",
				item_name : data.string.s5_item_10,
				imgsrc : imgpath + "s_item_2.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 50'
			},
			{
				imgclass : "shop_items shop_item_2 top_shelf",
				item_name : data.string.s5_item_10,
				imgsrc : imgpath + "s_item_2.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 50'
			},
			{
				imgclass : "shop_items shop_item_3 top_shelf",
				item_name : data.string.s5_item_11,
				imgsrc : imgpath + "s_item_3.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 50'
			},
			{
				imgclass : "shop_items shop_item_4 bottom_shelf",
				item_name : data.string.s5_item_12,
				imgsrc : imgpath + "s_item_4.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 40'
			},
			{
				imgclass : "shop_items shop_item_4 bottom_shelf",
				item_name : data.string.s5_item_12,
				imgsrc : imgpath + "s_item_4.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 40'
			},
			{
				imgclass : "shop_items shop_item_4 bottom_shelf",
				item_name : data.string.s5_item_12,
				imgsrc : imgpath + "s_item_4.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 40'
			},
			{
				imgclass : "shop_items shop_item_5 bottom_shelf",
				item_name : data.string.s5_item_4,
				imgsrc : imgpath + "s_item_5.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 120'
			},
			{
				imgclass : "shop_items shop_item_5 bottom_shelf",
				item_name : data.string.s5_item_4,
				imgsrc : imgpath + "s_item_5.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 120'
			},

			{
				imgclass : "shop_items shop_item_6 bottom_shelf",
				item_name : data.string.s5_item_5,
				imgsrc : imgpath + "s_item_6.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 80'
			},
			{
				imgclass : "shop_items shop_item_6 bottom_shelf",
				item_name : data.string.s5_item_5,
				imgsrc : imgpath + "s_item_6.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 80'
			},
		],
		infolabel:[
			{
				inputclass: 'user_input my_font_medium input_name',
				textdata : data.string.p1text2,
				submit: data.string.sbmttext,
				textclass : '',
				infolabelclass: '',
				infolabeldata: '',
			},
			{
				inputclass: 'user_input my_font_medium input_address',
				textdata : data.string.p1text3,
				submit: data.string.sbmttext,
				textclass : '',
				infolabelclass: '',
				infolabeldata: '',
			}
		],
		bill_item:[{
			bill_container: 'bill_center slide_from_top bill_hidden',
			shop_logo: imgpath + "shop_bill/logo_station.png",
			shop_name: data.string.shop5_name,
			shop_address: data.string.shop5_address,
			customer_name:'Mark',
			customer_address:'USA',
			customer_bill: '300',
			customer_date: '12/12',
			signature: imgpath + "shop_bill/sign.png",
			shopkeeper_name: data.string.shop5_owner
		}],
	}
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();


	var go_to_next_slide = 0;
	var  global_save_val = '';

	var user_name = '';
	var user_address = '';

	var shopped_item = new Map();

	loadTimelineProgress($total_page, countNext + 1);

		var preload;
		var timeoutvar = null;
		var current_sound;

		function init() {
			//specify type otherwise it will load assests as XHR
			manifest = [
				// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
				// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
				//   ,
				//images
				// {id: "bg01", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},

				// soundsicon-orange
				{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
				{id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
				{id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
				{id: "s1_p3_1", src: soundAsset+"s1_p3_1.ogg"},
			];
			preload = new createjs.LoadQueue(false);
			preload.installPlugin(createjs.Sound);//for registering sounds
			preload.on("progress", handleProgress);
			preload.on("complete", handleComplete);
			preload.on("fileload", handleFileLoad);
			preload.loadManifest(manifest, true);
		}
		function handleFileLoad(event) {
			// console.log(event.item);
		}
		function handleProgress(event) {
			$('#loading-text').html(parseInt(event.loaded*100)+'%');
		}
		function handleComplete(event) {
			$('#loading-wrapper').hide(0);
			//initialize varibales
			current_sound = createjs.Sound.play('sound_1');
			current_sound.stop();
			// call main function
			templateCaller();
		}
		//initialize
		init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
		case 0:
		case 1:
			sound_player_nav("s1_p"+(countNext+1),1);
			// $nextBtn.show(0);
			break;
		case 2:
			sound_player_nav("s1_p3",0);
			items = [];
			shopped_item.clear();
			go_to_next_slide = 0;
			var item_count = 0;
			arrage_items_in_shelf();
			$('.shop_items').click(function(){
				item_count++;setTimeout(function(){
					$('.done_shopping').show(0);
				}, 2000);
				var item_cost = parseInt($(this).find('label').text().split(" ")[1]);
				add_to_map($(this).data('name'), item_cost);
				$(this).css('pointer-events','none');
				var item_basket_y = 55-13*(Math.floor(item_count/5));
				var item_basket_x = 8+12*(item_count%5);
				$(this).fadeOut(500, function(){
					$(this).detach().css({
						'width': '20%',
						'z-index': '10',
						'top': item_basket_y+'%',
						'left': item_basket_x+'%',
						}).appendTo($('.basket'));
					$(this).fadeIn(500);
				});
			});
			$('.done_shopping').click(function(){
				$('.done_shopping').css({'pointer-events': 'none', 'opacity': '0'});
				$('.shop_items').css('pointer-events', 'none');
				$('.input_name').show(0);
				$('.black_image').show(0);
				$(".sbmtbtn").hide(0);
			});
			$(".sbmtbtn").click(function(){
		        $nextBtn.trigger("click");
			});
			input_box('.input_name', $nextBtn);
			input_box('.input_address', $nextBtn);
			function add_to_map(key, price){
				var key_val_1 = 1;
				if(shopped_item.has(key)){
					key_val_1 +=  shopped_item.get(key)[0];
				}
				shopped_item.set(key, [key_val_1, price]);
			}
			$prevBtn.show(0);
			break;
		default:
			$nextBtn.show(0);
			$prevBtn.show(0);
			break;
		}
	}

		function nav_button_controls(delay_ms){
			timeoutvar = setTimeout(function(){
				if(countNext==0){
					$nextBtn.show(0);
				} else if( countNext>0 && countNext == $total_page-1){
					$prevBtn.show(0);
					ole.footerNotificationHandler.pageEndSetNotification();
				} else{
					$prevBtn.show(0);
					$nextBtn.show(0);
				}
			},delay_ms);
		}
		function sound_player_nav(sound_id,next){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
			current_sound.on("complete", function(){
				next?nav_button_controls():"";
			});
		}
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	function arrage_items_in_shelf(){
		for(var i=1; i<7; i++){
			var shelf_array = document.getElementsByClassName('shop_item_'+i);
			var total_in_array = shelf_array.length;
			for(var m=0; m<shelf_array.length; m++){
				var left_position = $('.shop_item_'+i).eq(0).position().left+m*$board.width()*0.06;
				$('.shop_item_'+i).eq(m).css({
					'left': left_position,
					'z-index':1 + (total_in_array-m)*1
				});
				// shelf_array[m].style.left = 1 + (total_in_array-m)*1;
			}
		}
	}
	function input_box(input_class, button_class) {
		$(input_class).keydown(function(event){
    		var charCode = (event.which) ? event.which : event.keyCode;
    		/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
    		if(charCode === 13 && button_class!=null) {
		        $(button_class).trigger("click");
			}
            // var condition = charCode != 8 && charCode != 16 && charCode != 20 && (charCode < 37 || charCode > 40) && charCode != 46;
            // //check if user inputs del, shift, caps , backspace or arrow keys
   			// if (!condition) {
    		// 	return true;
    		// }
    		// //check if user inputs more than one '.'
            // if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
        		// return false;
    		// }
    		// //check . and 0-9 separately after checking arrow and other keys
    		// if((charCode < 65 || charCode > 90)){
    		// 	return false;
    		// }
  			return true;
		});
		$(input_class).keyup(function(event){
    		if (String(event.target.value).length >= 1) {
    			$(".sbmtbtn").show(0);
    			$(button_class).show(0);
    			global_save_val = String(event.target.value);
    		}
    		else{
    			$(".sbmtbtn").hide(0);
    			$(button_class).hide(0);
    		}
  			return true;
		});
	}
	$( window ).resize(function() {
		arrage_items_in_shelf();
	});


	$nextBtn.on("click", function() {
		switch(countNext){
			case 2:
				if(go_to_next_slide==0){
					user_name = global_save_val;
					$('.input_name').hide(0);
					$(".sbmtbtn").hide(0);
					$('.input_address').show(0);
					$nextBtn.hide(0);
					go_to_next_slide++;
				} else if (go_to_next_slide==1){
					user_address = global_save_val;
					$nextBtn.hide(0);
					$('.input_address').hide(0);
					$('.take_bill').show(0);
					$('.shop_name_text').hide(0);
					$('.top_text').removeClass('shop_name_board shop_name_2 my_font_big');
					$('.top_text').addClass('take_bill_div my_font_medium');
					var my_bill = new bill();
					items = [];
					my_bill.init(imgpath + "shop_bill/logo_station.png", data.string.shop5_name, data.string.shop5_address, user_name, user_address, 'bill_container');
					shopped_item.forEach(arrayElements);
					my_bill.add_items(items);
					my_bill.update_bill('.bill_center');
					$('.bill_hidden').show(0);
					setTimeout(function(){
						ole.footerNotificationHandler.pageEndSetNotification();
					}, 3000);
				}
				break;
			default:
				countNext++;
				templateCaller();
				break;
		}
	});
	var items = [];
	function arrayElements(value, key, map) {
		items.push(new bill_element(key, value[0], value[1]));
	}

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

var bill_element = function(name, qty, rate){
	var item_name = (typeof name === 'string') ?  name : 'pen';
	var item_quantity = (typeof qty === 'number') ?  qty : 0;
	var item_rate = (typeof rate === 'number') ?  rate : 0;
	var amount = item_quantity* item_rate;
	this.get_item_name = function(){
		return item_name;
	};
	this.get_item_quantity = function(){
		return item_quantity;
	};
	this.get_item_rate = function(){
		return item_rate;
	};
	this.get_amount = function(){
		return amount;
	};
};
var bill = function(){
	var shop_logo = $('.bill_container').find('.shop_logo_class').attr('src');
	var shop_name = $('.bill_container').find('.shop_name').html();
	var shop_address  = $('.bill_container').find('.shop_address').html();
	var customer_name  = $('.bill_container').find('.customer_name').html();
	var customer_address  = $('.bill_container').find('.customer_address').html();
	var temp_date = new Date();
	var bill_date = temp_date.getMonth()+1 +'/'+ temp_date.getDate()+'/'+'2018';
	var bill_no = ole.getRandom(1, 9999, 1000)[0];
	var item_count = 0;
	var shopkeeper_name  = $('.bill_container').find('.shopkeeper_name').html();
	var item_list = new Map();
	var last_row = "bill_table_header";
	var bill_classname;
	var total_amount = 0;

	this.init = function(logo, name, address, cust_name, cust_address, bill_name){
		shop_logo = (typeof logo === 'string') ?  logo : imgpath + "shop_bill/logo_1.svg";
		shop_name = (typeof name === 'string') ?  name : "Facebook Inc";
		shop_address = (typeof address === 'string') ?  address : "Everywhere";
		customer_name = (typeof cust_name === 'string') ?  cust_name : "Jacky";
		customer_address = (typeof cust_address === 'string') ?  cust_address : "England";
		bill_classname = (typeof bill_name === 'string') ?  '.'+bill_name : '.bill_container';

		if($lang=='np'){
			change_to_nepali();
		}
	};
	this.set_bill_classname= function(bill_classname_s){
		bill_classname = bill_classname_s;
	};
	this.set_bill_number= function(bill_number){
		bill_no = bill_number;
	};
	this.reset_bill_items = function(){
		item_count = 0;
		item_list.clear();
	};
	this.set_bill_date= function(bill_date_s){
		bill_date = bill_date_s;
	};
	this.set_shopkeeper_name= function(shopkeeper_name_s){
		shopkeeper_name = shopkeeper_name_s;
		$(bill_classname).find('.shopkeeper_name').html(shopkeeper_name);
	};
	this.set_bill_sign= function(sign){
		$(bill_classname).find('.signature_shopkeeper').attr('src', sign);
	};
	this.add_item = function(bill_item){
		if(!item_list.has(bill_item.get_item_name())){
			item_count++;
		};
		item_list.set(bill_item.get_item_name(), [item_count, bill_item]);
	};
	this.add_items = function(bill_item_array){
		for( var i = 0 ; i<bill_item_array.length; i++){
			if(!item_list.has(bill_item_array[i].get_item_name())){
				item_count++;
			};
			item_list.set(bill_item_array[i].get_item_name(), [item_count, bill_item_array[i]]);
		}
	};

	this.update_bill = function(){
		$(bill_classname).find('.shop_logo_class').attr('src', shop_logo);
		$(bill_classname).find('.shop_name').html(shop_name);
		$(bill_classname).find('.shop_address').html(shop_address);
		$(bill_classname).find('.customer_name').html(customer_name);
		$(bill_classname).find('.customer_address').html(customer_address);
		$(bill_classname).find('.customer_bill').html(bill_no);
		$(bill_classname).find('.customer_date').html(bill_date);
		item_list.forEach(update_items);
		if(item_count<10){
			pad_rows(10-item_count);
		}
		// var bottom_html = '<tr class="total_amount_row"><td colspan="4">Total</td><td class="total_amount_data">94</td></tr>';
	};
	function update_items(value, key, map){
		$(bill_classname).find('.'+last_row).after('<tr class='+value[1].get_item_name().replace(/ /g, '')+'last_row'+'><td>'+value[0]+'</td><td>'+value[1].get_item_name()+'</td><td>'+ value[1].get_item_quantity() +'</td><td>'+ value[1].get_item_rate() +'</td><td>'+ value[1].get_amount() +'</td></td>');
		last_row = value[1].get_item_name().replace(/ /g, '') +'last_row';
		total_amount += value[1].get_amount();
		$(bill_classname).find('.total_amount_data').html(total_amount);
	}
	function pad_rows(pad_number){
		for (var m=0; m<pad_number; m++){
			$(bill_classname).find('.'+last_row).after('<tr><td> </td><td> </td><td> </td><td> </td><td> </td></td>');
		}
	}
	function reset_bill(){
		item_count = 0;
		item_list.clear();
	};
	function change_to_nepali(){
		$('.customer_name_div>p:first-child>span:first-child').html(data.string.bname);
		$('.customer_name_div>p:last-child>span:first-child').html(data.string.baddress);
		$('.bill_number_div>p:first-child>span:first-child').html(data.string.bno);
		$('.bill_number_div>p:last-child>span:first-child').html(data.string.bdate);
		$('.bill_table_header>th:first-child').html(data.string.bsn);
		$('.bill_table_header>th:nth-child(2)').html(data.string.bparticulars);
		$('.bill_table_header>th:nth-child(3)').html(data.string.bqty);
		$('.bill_table_header>th:nth-child(4)').html(data.string.brate);
		$('.bill_table_header>th:last-child').html(data.string.bamt);
		$('.total_amount_row>td:first-child').html(data.string.btotal);
		$('.bill_footer>p>span:first-child').html(data.string.bfor);
	}
};

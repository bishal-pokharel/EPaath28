var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var content=[
    {
        //slide 0
        uppertextblock:[
            {
                textclass : "diytitle",
                textdata : data.string.diy,
            }
        ],
        imageblock:[
            {
                imagetoshow: [
                    {
                        imgclass: "diyimage",
                        imgsrc: "images/diy_bg/a_07.png"
                    },
                ]
            }]
    },
	// {
	// 	uppertextblock:[
	// 	{
	// 		textclass : "centertext fadein",
	// 		textdata : data.string.p4text3,
	// 	}
	// 	],
	// },
	{
	//slide 0
		uppertextblock:[
		{
			textclass : "centertitle fadein",
			textdata : data.string.p4text1,
		}
		],
		quesHidAnsBlock:[
		{
			questioncontainer: "forhover",
			answercontainer: "forclick",

			chilrenadditionalclass1: "incorrect",
			question1: data.string.p4ques1,
			answer1: data.string.p4ans1,

			chilrenadditionalclass2: "incorrect",
			question2: data.string.p4ques2,
			answer2: data.string.p4ans2,

			chilrenadditionalclass3: "incorrect",
			question3: data.string.p4ques3,
			answer3: data.string.p4ans3,

			chilrenadditionalclass4: "correct",
			question4: data.string.p4ques4,
			answer4: data.string.p4ans4,

		}
		]
	},
	{
	//slide 0
		uppertextblock:[
		{
			textclass : "centertitle fadein",
			textdata : data.string.p4text2,
		}
		],
		quesHidAnsBlock:[
		{
			questioncontainer: "forhover",
			answercontainer: "forclick",

			chilrenadditionalclass1: "incorrect",
			question1: data.string.p4ques5,
			answer1: data.string.p4ans5,

			chilrenadditionalclass2: "incorrect",
			question2: data.string.p4ques6,
			answer2: data.string.p4ans6,

			chilrenadditionalclass3: "incorrect",
			question3: data.string.p4ques6,
			answer3: data.string.p4ans7,

			chilrenadditionalclass4: "correct",
			question4: data.string.p4ques8,
			answer4: data.string.p4ans8,

		}
		]
	},
    {
        //slide 0
        uppertextblock:[
            {
                textclass : "centertitle fadein",
                textdata : data.string.p4text3,
            }
        ],
        quesHidAnsBlock:[
            {
                questioncontainer: "forhover",
                answercontainer: "forclick",

                chilrenadditionalclass1: "incorrect",
                question1: data.string.p4ques9,
                answer1: data.string.p4ans5,

                chilrenadditionalclass2: "incorrect",
                question2: data.string.p4ques10,
                answer2: data.string.p4ans6,

                chilrenadditionalclass3: "incorrect",
                question3: data.string.p4ques11,
                answer3: data.string.p4ans7,

                chilrenadditionalclass4: "correct",
                question4: data.string.p4ques12,
                answer4: data.string.p4ans8,

            }
        ]
    },
    {
        //slide 0
        uppertextblock:[
            {
                textclass : "centertitle fadein",
                textdata : data.string.p4text4,
            }
        ],
        quesHidAnsBlock:[
            {
                questioncontainer: "forhover",
                answercontainer: "forclick",

                chilrenadditionalclass1: "incorrect",
                question1: data.string.p4ques13,
                answer1: data.string.p4ans5,

                chilrenadditionalclass2: "incorrect",
                question2: data.string.p4ques14,
                answer2: data.string.p4ans6,

                chilrenadditionalclass3: "incorrect",
                question3: data.string.p4ques15,
                answer3: data.string.p4ans7,

                chilrenadditionalclass4: "correct",
                question4: data.string.p4ques16,
                answer4: data.string.p4ans8,

            }
        ]
    },
];
// 	return content;
// }


$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	/*recalculateHeightWidth();*/

	var total_page = 0;

	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	loadTimelineProgress($total_page, countNext + 1);

	var preload;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
      {id: "cvpg", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},
      {id: "wrong", src:"images/wrongicon.png", type: createjs.AbstractLoader.IMAGE},
			{id: "right", src:"images/right.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
      {id: "s4_p2", src: soundAsset+"s4_click on the correct option.ogg"},
      {id: "s4_p3", src: soundAsset+"s4_p3.ogg"},
      {id: "s4_p4", src: soundAsset+"s4_p4.ogg"},
			{id: "s4_p5", src: soundAsset+"s4_p5.ogg"},


		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
	*/
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */
 	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		$(".contentblock").addClass("ole-background-gradient-midnight");
		if(countNext == 0 ) {
			play_diy_audio();
      $nextBtn.show(0);
    }else if (countNext ==1) {
      sound_player("s4_p2",0);
    }


		var parent = $(".parentcontainer");
		var divs = parent.children();
			 while (divs.length) {
			        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			  }

		var corFlag = false;
    $(".children").append("<img class='corIncor right' src='"+preload.getResult("right").src+"'>")
    $(".children").append("<img class='corIncor wrong' src='"+preload.getResult("wrong").src+"'>")
		$(".children > p:nth-of-type(1)").one("click", function(){
				if(corFlag == false){
				$(this).parent().css("visibility","visible");
				$(this).siblings("p").hide(0).delay(500).fadeIn("slow");
				$(this).removeClass("forhover");

				if($(this).parent().hasClass("correct")){
					play_correct_incorrect_sound(1);
					$(this).addClass("childcorr");
					$(this).parent().addClass("parcorr");
          $(this).siblings(".right").show(0);
					corFlag = true;
					console.log(corFlag);
					setTimeout(function(){
							$(".incorrect").addClass("parinc").css("visibility","visible");
							$(".incorrect > .forhover").addClass("childinc").removeClass("forhover");
							navigationcontroller();
							$(".incorrect > .forclick").hide(0).delay(500).fadeIn("slow").removeClass("forclick");
					}, 2000);
				}
				else{
                    play_correct_incorrect_sound(0);
                    $(this).siblings(".wrong").show(0);
                    console.log(corFlag);
						$(this).addClass("childinc");
						$(this).parent().addClass("parinc");
						$(this).siblings().removeClass("forclick");
					}
				}
			});
	}


	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, nxtBtnFlag){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		if(nxtBtnFlag){
			current_sound.on('complete',function(){
				nav_button_controls(0);
			});
		}
	}
	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

	loadTimelineProgress($total_page, countNext + 1);

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
		total_page = content.length;
		// templateCaller();
	// });

});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span>";


					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

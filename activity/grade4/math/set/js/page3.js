var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var content=[
	{
	//slide 0
		uppertextblock:[
		{
			textclass : "centertext content fadein",
			textdata : data.string.p3text1,
		}
		],
	},
	{
	//slide 1
		uppertextblock:[
		{
			textclass : "lefttitle",
			textdata : data.string.rule1,
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "hightext1",
			datahighlightcustomclass2: "hightext2",
			textclass : "defintext fadein",
			textdata : data.string.p3text2,
		},
		],
	},
	{
	//slide 2
		uppertextblock:[
		{
			textclass : "lefttitle",
			textdata : data.string.rule1,
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "hightext1",
			datahighlightcustomclass2: "hightext2",
			textclass : "defintext fadein",
			textdata : data.string.p3text3,
		},
		],
	},
	{
	//slide 3
		uppertextblock:[
		{
			textclass : "lefttitle",
			textdata : data.string.rule1,
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "hightext1",
			datahighlightcustomclass2: "hightext2",
			textclass : "defintext",
			textdata : data.string.p3text3,
		},
		{
			textclass : "egtext fadein",
			datahighlightflag: true,
			datahighlightcustomclass3: "hightext3",
			textdata : data.string.vset1,
		},
		{
			textclass : "bottomtext fadein",
			textdata : data.string.p3text4,
		}
		],
	},
	{
	//slide 4
		uppertextblock:[
		{
			textclass : "lefttitle",
			textdata : data.string.rule1,
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "hightext1",
			datahighlightcustomclass2: "hightext2",
			textclass : "defintext",
			textdata : data.string.p3text3,
		},
		{
			textclass : "egtext fadein",
			datahighlightflag: true,
			datahighlightcustomclass3: "hightext3",
			textdata : data.string.vset2,
		},
		{
			textclass : "bottomtext fadein",
			textdata : data.string.p3text5,
		}
		],
	},
	{
	//slide 5
		uppertextblock:[
		{
			textclass : "lefttitle",
			textdata : data.string.rule1,
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "hightext1",
			datahighlightcustomclass2: "hightext2",
			textclass : "defintext",
			textdata : data.string.p3text3,
		},
		{
			textclass : "egtext fadein",
			datahighlightflag: true,
			datahighlightcustomclass3: "hightext3",
			textdata : data.string.vset3,
		},
		{
			textclass : "bottomtext fadein",
			textdata : data.string.p3text6,
		}
		],
	},
	{
	//slide 6
		uppertextblock:[
		{
			textclass : "lefttitle",
			textdata : data.string.rule1,
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "hightext1",
			datahighlightcustomclass2: "hightext2",
			textclass : "defintext",
			textdata : data.string.p3text3,
		},
		{
			textclass : "egtext fadein",
			datahighlightflag: true,
			datahighlightcustomclass3: "hightext3",
			textdata : data.string.vset,
		},
		{
			textclass : "bottomtext fadein",
			textdata : data.string.p3text7,
		}
		],
	},
	{
	//slide 7
		uppertextblock:[
		{
			textclass : "lefttitle",
			textdata : data.string.rule1,
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "hightext1",
			datahighlightcustomclass2: "hightext2",
			textclass : "defintext",
			textdata : data.string.p3text2,
		},
		{
			textclass : "egtext fadein",
			datahighlightflag: true,
			datahighlightcustomclass3: "",
			textdata : data.string.vset,
		},
		{
			textclass : "bottomtext fadein",
			textdata : data.string.p3text8,
		}
		],
	},
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	loadTimelineProgress($total_page, countNext + 1);

	var preload;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "cvpg", src: imgpath+"cover_page.jpg", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s3_p1", src: soundAsset+"s3_p1.ogg"},
			{id: "s3_p2", src: soundAsset+"s3_p2.ogg"},
			{id: "s3_p3", src: soundAsset+"s3_p3.ogg"},
			{id: "s3_p4", src: soundAsset+"s3_p4.ogg"},
			{id: "s3_p5", src: soundAsset+"s3_p5.ogg"},
			{id: "s3_p6", src: soundAsset+"s3_p6.ogg"},
			// {id: "s3_p7", src: soundAsset+"s3_p7.ogg"},
			{id: "s3_p8", src: soundAsset+"s3_p8.ogg"},


		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
	*/
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	//controls the navigational state of the program

	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */
 	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		$(".contentblock").addClass("ole-background-gradient-midnight");
		switch (countNext) {
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 7:
				sound_player("s3_p"+(countNext+1),1);
			break;
			case 6:
				nav_button_controls(1000);
			break;
			default:

		}

	}


		function nav_button_controls(delay_ms){
			timeoutvar = setTimeout(function(){
				if(countNext==0){
					$nextBtn.show(0);
				} else if( countNext>0 && countNext == $total_page-1){
					$prevBtn.show(0);
					ole.footerNotificationHandler.pageEndSetNotification();
				} else{
					$prevBtn.show(0);
					$nextBtn.show(0);
				}
			},delay_ms);
		}
		function sound_player(sound_id, nxtBtnFlag){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
			if(nxtBtnFlag){
				current_sound.on('complete',function(){
					nav_button_controls(0);
				});
			}
		}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

	loadTimelineProgress($total_page, countNext + 1);

		// navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}


	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
		total_page = content.length;
		// templateCaller();
	// });

});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightstarttag2;
        var texthighlightstarttag3;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
              $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

              $(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
              (stylerulename2 = "parsedstring2") ;

              $(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
              (stylerulename3 = "parsedstring3") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
            texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
		/*=====  End of data highlight function  ======*/

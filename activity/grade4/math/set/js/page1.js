var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var content=[
	{
	//slide 0
		additionalclasscontentblock: "ole-background-gradient-midnight",
		uppertextblock:[
		{
			textclass : "centertext titletext",
			textdata : data.string.title,
		}
		],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "coverpage",
				imgsrc : imgpath + "cover_page.jpg"
			}

			]
		}]
	},
	{
	//slide 1
		additionalclasscontentblock: "ole-background-gradient-midnight",
		uppertextblock:[
		{
			textclass : "defintext fadein content2",
			textdata : data.string.p1text1,
		},
		{
			textclass : "lefttitle fadein title",
			textdata : data.string.title,
		},
		],
	},
	{
	//slide 2
		additionalclasscontentblock: "ole-background-gradient-midnight",
		uppertextblock:[
		{
			textclass : "defintext  content2",
			textdata : data.string.p1text1,
		},
		{
			textclass : "lefttitle title",
			textdata : data.string.title,
		},
		{
			textclass : "eg1 fadein",
			textdata : data.string.eg1,
		},
		],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "booksone fadein",
				imgsrc : imgpath + "q01.png"
			},
			]
		}]
	},
	{
	//slide 3
		additionalclasscontentblock: "ole-background-gradient-midnight",
		uppertextblock:[
		{
			textclass : "defintext content2",
			textdata : data.string.p1text1,
		},
		{
			textclass : "lefttitle title",
			textdata : data.string.title,
		},
		{
			textclass : "eg1",
			textdata : data.string.eg1,
		},
		{
			textclass : "eg2 fadein",
			textdata : data.string.eg2,
		}
		],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "booksone",
				imgsrc : imgpath + "q01.png"
			},
			{
				imgclass: "crayonsone fadein",
				imgsrc : imgpath + "vegetable.png"
			},
			]
		}]
	},
	{
	//slide 4
		additionalclasscontentblock: "ole-background-gradient-midnight",
		uppertextblock:[
		{
			textclass : "defintext content2",
			textdata : data.string.p1text1,
		},
		{
			textclass : "lefttitle title",
			textdata : data.string.title,
		},
		{
			textclass : "eg1",
			textdata : data.string.eg1,
		},
		{
			textclass : "eg2",
			textdata : data.string.eg2,
		},
		{
			textclass : "eg3 fadein",
			textdata : data.string.eg3,
		}
		],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "booksone",
				imgsrc : imgpath + "q01.png"
			},
			{
				imgclass: "crayonsone",
				imgsrc : imgpath + "vegetable.png"
			},
			{
				imgclass: "geomsone fadein",
				imgsrc : imgpath + "animals.png"
			},
			]
		}]
	},
	// {
	// //slide 5
	// 	additionalclasscontentblock: "ole-background-gradient-midnight",
	// 	uppertextblock:[
	// 	{
	// 		textclass : "defintext fadein content2",
	// 		textdata : data.string.p1text1,
	// 	},
	// 	{
	// 		textclass : "lefttitle title",
	// 		textdata : data.string.title,
	// 	},
	// 	],
	// },
	{
	//slide 6
		additionalclasscontentblock: "ole-background-gradient-midnight",
		uppertextblock:[
		{
			textclass : "defintext fadein content2",
			textdata : data.string.p1text2,
		},
		{
			textclass : "lefttitle title",
			textdata : data.string.elements,
		},
		],
	},
	{
	//slide 7
		additionalclasscontentblock: "ole-background-gradient-midnight",
		uppertextblock:[
		{
			textclass : "defintext content2",
			textdata : data.string.p1text2,
		},
		{
			textclass : "lefttitle title2",
			textdata : data.string.elements,
		},
		{
			textclass : "eg1 textalign fadein",
			textdata : data.string.eg4,
		},
		],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "middleimg fadein",
				imgsrc : imgpath + "q01.png"
			},
			]
		}]
	},
	{
	//slide 8
		additionalclasscontentblock: "ole-background-gradient-midnight",
		uppertextblock:[
		{
			textclass : "defintext  content2",
			textdata : data.string.p1text2,
		},
		{
			textclass : "lefttitle title",
			textdata : data.string.elements,
		},
		{
			textclass : "eg1 textalign fadein",
			textdata : data.string.eg5,
		},
		],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "middleimg fadein",
				imgsrc : imgpath + "vegetable.png"
			},
			]
		}]
	},
	{
	//slide 9
		additionalclasscontentblock: "ole-background-gradient-midnight",
		uppertextblock:[
		{
			textclass : "defintext content2",
			textdata : data.string.p1text2,
		},
		{
			textclass : "lefttitle title",
			textdata : data.string.elements,
		},
		{
			textclass : "eg1 textalign fadein",
			textdata :data.string.eg6,
		},
		],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "middleimg fadein",
				imgsrc : imgpath + "animals.png"
			},
			]
		}]
	},
	{
	//slide 10
		additionalclasscontentblock: "ole-background-gradient-midnight",
		uppertextblock:[
		{
			textclass : "centertext fadein content",
			textdata : data.string.p1text3,
		},
		],
	},
	{
	//slide 11
		additionalclasscontentblock: "ole-background-gradient-midnight",
		uppertextblock:[
		{
			textclass : "centertext fadein",
			textdata : data.string.review,
		}
		],
	},
	{
	//slide 12
		additionalclasscontentblock: "ole-background-gradient-midnight",
		uppertextblock:[
		{
			textclass : "leftdefintext fadein content2",
			textdata : data.string.p1text1_review,
		},
		{
			textclass : "lefttitle fadein title",
			textdata : data.string.title,
		},
		],
	},
	{
	//slide 13
		additionalclasscontentblock: "ole-background-gradient-midnight",
		uppertextblock:[
		{
			textclass : "leftdefintext",
			textdata : data.string.p1text1,
		},
		{
			textclass : "lefttitle title",
			textdata : data.string.title,
		},
		{
			textclass : "rightdefintext fadein",
			textdata : data.string.p1text2,
		},
		{
			textclass : "righttitle fadein",
			textdata : data.string.elements,
		},
		],
	},
	{
	//slide 16
		additionalclasscontentblock: "ole-background-gradient-midnight",
		uppertextblock:[
		{
			textclass : "leftdefintext",
			textdata : data.string.p1text1,
		},
		{
			textclass : "lefttitle title",
			textdata : data.string.title,
		},
		{
			textclass : "rightdefintext",
			textdata : data.string.p1text2,
		},
		{
			textclass : "righttitle",
			textdata : data.string.elements,
		},
            {
                textclass : "emptydiv",
                textdata : "",
            }
		],
		setscoll:[
		{
			thecontainer: "thiscontain fadein",
			contelems:[
				{
					containertext: data.string.seteg1,
				}
			]
		},
		{
		thecontainer: "thatcontain fadein",
		contelems:[
				{
					containertext: data.string.eleeg1,
				}
			]

		}
		],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "arrow fadein",
				imgsrc : imgpath + "q01.png"
			}
			]
		}]
	},
	{
	//slide 16
		additionalclasscontentblock: "ole-background-gradient-midnight",
		uppertextblock:[
		{
			textclass : "leftdefintext",
			textdata : data.string.p1text1,
		},
		{
			textclass : "lefttitle title",
			textdata : data.string.title,
		},
		{
			textclass : "rightdefintext",
			textdata : data.string.p1text2,
		},
		{
			textclass : "righttitle",
			textdata : data.string.elements,
		},
            {
                textclass : "emptydiv",
                textdata : "",
            }
		],
		setscoll:[
		{
			thecontainer: "thiscontain fadein",
			contelems:[
				{
					containertext: data.string.seteg2,
				}
			]
		},
		{
		thecontainer: "thatcontain fadein",
		contelems:[
				{
					containertext: data.string.eleeg2,
				}
			]

		}
		],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "arrow fadein",
				imgsrc : imgpath + "vegetable.png"
			}
			]
		}]
	},
	{
	//slide 16
		additionalclasscontentblock: "ole-background-gradient-midnight",
		uppertextblock:[
		{
			textclass : "leftdefintext",
			textdata : data.string.p1text1,
		},
		{
			textclass : "lefttitle title",
			textdata : data.string.title,
		},
		{
			textclass : "rightdefintext",
			textdata : data.string.p1text2,
		},
		{
			textclass : "righttitle",
			textdata : data.string.elements,
		},
            {
                textclass : "emptydiv",
                textdata : "",
            }
		],
		setscoll:[
		{
			thecontainer: "thiscontain fadein",
			contelems:[
				{
					containertext: data.string.seteg3,
				}
			]
		},
		{
		thecontainer: "thatcontain fadein",
		contelems:[
				{
					containertext: data.string.eleeg3,
				}
			]

		}
		],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "arrow fadein",
				imgsrc : imgpath + "animals.png"
			}
			]
		}]
	}
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	loadTimelineProgress($total_page, countNext + 1);

	var preload;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "cvpg", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
			{id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
			{id: "s1_p4", src: soundAsset+"s1_p4.ogg"},
			{id: "s1_p5", src: soundAsset+"s1_p5.ogg"},
			{id: "s1_p6", src: soundAsset+"s1_p6.ogg"},
			{id: "s1_p7", src: soundAsset+"s1_p7.ogg"},
			{id: "s1_p8", src: soundAsset+"s1_p8.ogg"},
			{id: "s1_p9", src: soundAsset+"s1_p9.ogg"},
			{id: "s1_p10", src: soundAsset+"s1_p10.ogg"},
			{id: "s1_p11", src: soundAsset+"s1_p11.ogg"},
			{id: "s1_p12", src: soundAsset+"s1_p12.ogg"},


		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
	*/
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */
 	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
		switch (countNext) {
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
			case 10:
					sound_player("s1_p"+(countNext+1),1);
			break;
			case 11:
				sound_player("s1_p12",1);
			break;
			case 12:
				sound_player("s1_p6",1);
			break;
			case 13:
				sound_player("s1_p7",1);
			break;
			case 14:
				sound_player("s1_p8",1);
			break;
			case 15:
				sound_player("s1_p9",1);
			break;
			default:

		}

	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, nxtBtnFlag){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		if(nxtBtnFlag){
			current_sound.on('complete',function(){
				nav_button_controls(0);
			});
		}
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

	loadTimelineProgress($total_page, countNext + 1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
		total_page = content.length;
		// templateCaller();
	// });

});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span>";


					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

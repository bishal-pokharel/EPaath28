var imgpath = $ref+"/images/";

var content=[
	//slide 0
	{
		contentblockadditionalclass:'bg',
		extratextblock:[
		{
			textclass: "box2",
			textdata: data.string.diytext
		}
	],
		imageblock:[
		{
			imgclass:'diy',
			imgsrc: imgpath + 'img_diy.png'
		}
		]
	},
	//slide 1
	{
		contentblockadditionalclass:'bg',
				extratextblock:[
		{
			textclass: "box1",
			textdata: data.string.diytxt
		}
	],
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.diyq1,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.diyq1op1,
					},
					{
						option_class: "class2",
						optiondata: data.string.diyq1op2,
					},
					{
						option_class: "class3",
						optiondata: data.string.diyq1op3,
					},
					{
						option_class: "class4",
						optiondata: data.string.diyq1op4,
					}],
			}
		]
	},
	//slide 2
	{
		contentblockadditionalclass:'bg',
				extratextblock:[
		{
			textclass: "box1",
			textdata: data.string.diytxt2
		}
	],
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.diyq2,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.diyq2op1,
					},
					{
						option_class: "class2",
						optiondata: data.string.diyq2op2,
					},
					{
						option_class: "class3",
						optiondata: data.string.diyq2op3,
					},
					{
						option_class: "class4",
						optiondata: data.string.diyq2op4,
					}],
			}
		]
	},
	//slide 3
	{
		contentblockadditionalclass:'bg',
				extratextblock:[
		{
			textclass: "box1",
			textdata: data.string.diytxt4
		}
	],
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.diyq4,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.diyq4op1,
					},
					{
						option_class: "class2",
						optiondata: data.string.diyq4op2,
					},
					{
						option_class: "class3",
						optiondata: data.string.diyq4op3,
					},
					{
						option_class: "class4",
						optiondata: data.string.diyq4op4,
					}],
			}
		]
	},
	//slide 4
	{
		contentblockadditionalclass:'bg',
				extratextblock:[
		{
			textclass: "box1",
			textdata: data.string.diytxt5
		}
	],
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.diyq5,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.diyq5op1,
					},
					{
						option_class: "class2",
						optiondata: data.string.diyq5op2,
					},
					{
						option_class: "class3",
						optiondata: data.string.diyq5op3,
					},
					{
						option_class: "class4",
						optiondata: data.string.diyq5op4,
					}],
			}
		]
	},
		//slide 4
	{
		contentblockadditionalclass:'bg',
				extratextblock:[
		{
			textclass: "box1",
			textdata: data.string.diytxt3
		}
	],
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.diyq3,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.diyq3op1,
					},
					{
						option_class: "class2",
						optiondata: data.string.diyq3op2,
					},
					{
						option_class: "class3",
						optiondata: data.string.diyq3op3,
					},
					{
						option_class: "class4",
						optiondata: data.string.diyq3op4,
					}],
			}
		]
	}
];

/*remove this for non random questions*/
$(function () {
	var testin = new NumberTemplate();

	var exercise = new template_exercise_mcq_monkey(content, testin, true);
	exercise.create_exercise();
});

function template_exercise_mcq_monkey(content, scoring){
	var $board			= $('.board');
	var $nextBtn		= $("#activity-page-next-btn-enabled");
	var $prevBtn		= $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext		= 0;
	var testin			= new EggTemplate();
	var wrong_clicked 	= false;

	var total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	function generaltemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);

		testin.numberOfQuestions();
		vocabcontroller.findwords(countNext);
		switch(countNext)
		{
			case 0:
			//$('.center-sundar').css({"display":"none"});
			$nextBtn.show(0);
			break;
		}

		/*for randomizing the options*/
		var option_position = [1,2,3,4];
		option_position.shufflearray();
		for(var op=0; op<4; op++){
			$('.main-container').eq(op).addClass('opt'+option_position[op]);
		}

		var wrong_clicked = 0;
		var correct_images = ['correct-1.png', 'correct-2.png', 'correct-3.png'];
		$(".option-container").click(function(){
			if($(this).hasClass("class1")){
				if(wrong_clicked<1){
					testin.update(true);
				}
				var rand_img = Math.floor(Math.random()*correct_images.length);
				$('.opti1, .opt2, .opt3, .opt-4').off('mouseenter mouseleave');
				//$('.center-sundar').attr('src', 'images/sundar/'+correct_images[rand_img]);
				$(".option-container").css('pointer-events','none');
				$(this).css({"background":"#6AA84F","border-radius":"1vmin","border-color":"rgb(197, 224, 123)"});
	 			play_correct_incorrect_sound(1);
				$(this).addClass('correct-ans');
				$(this).parent().children('.correct-icon').show(0);
				wrong_clicked = 0;
				total_page = content.length;
				if(countNext != total_page)
				{
					$nextBtn.show(0);
				}
				if(countNext == total_page-1)
				{
					$nextBtn.hide(0);
					ole.footerNotificationHandler.pageEndSetNotification() ;
				}

			}
			else{
				testin.update(false);
				$(this).css({"pointer-events":"none","background":"rgb(191, 13, 13)","border-radius":"1vmin","border-color":"rgb(226, 129, 129)"});
	 			play_correct_incorrect_sound(0);
				$(this).addClass('incorrect-ans');
				$(this).parent().children('.incorrect-icon').show(0);
				wrong_clicked++;
			}
		});
	};


	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		// call the template
		generaltemplate();

	};

	this.create_exercise = function(){
		// content.shufflearray();
		if(typeof scoring != 'undefined'){
			testin = scoring;
		}
	 	testin.init(total_page);

		templateCaller();
		$nextBtn.on("click", function(){
			countNext++;
			testin.gotoNext();
			templateCaller();
		});
		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			countNext--;
			templateCaller();
		});
	};
}

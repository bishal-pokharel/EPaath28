var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/";

var content = [
  // slide0
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "thebg1",

    extratextblock: [
      {
        textclass: "p1s0",
        textdata: data.string.p1s0
      },
      {
        textclass: "p1d hidethis",
        textdata: data.string.p1d
      },
      {
        textclass: "p1i hidethis",
        textdata: data.string.p1i
      },
      {
        textclass: "p1v hidethis",
        textdata: data.string.p1v
      },
      {
        textclass: "p1e hidethis",
        textdata: data.string.p1e
      },
      {
        datahighlightflag: "true",
        datahighlightcustomclass: "bold",
        textclass: "btmtxt",
        textdata: data.string.p1s0btmtxt
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "coverpage",
            imgid: "coverpage",
            imgsrc: ""
          }
        ]
      }
    ]
  },
  // slide0
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "thebg1",

    extratextblock: [
      {
        textclass: "p1s0txt2box",
        textdata: data.string.p1s0text2
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg",
            imgid: "bg",
            imgsrc: ""
          }
        ]
      }
    ]
  },
  // slide1
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: " ",

    uppertextblock: [
      {
        textclass: "p1s1txtbox",
        textdata: data.string.p1s1text,
        datahighlightcustomclass: "yellowcolor",
        datahighlightflag: true
      }
    ],
    extratextblock: [
      {
        textclass: "p1s1txt1box",
        textdata: data.string.p1s1text1
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "ship",
            imgid: "ship",
            imgsrc: ""
          },
          {
            imgclass: "birds",
            imgid: "birds",
            imgsrc: ""
          },
          {
            imgclass: "txtbox",
            imgid: "txtbox",
            imgsrc: ""
          },
          {
            imgclass: "ring",
            imgid: "ring",
            imgsrc: ""
          }
        ]
      }
    ]
  },
  // slide2
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: " ",

    uppertextblock: [
      {
        textclass: "p1s2txtbox slideL",
        textdata: data.string.p1s2text
      }
    ],
    extratextblock: [
      {
        textclass: "p1s2txtbox1",
        textdata: data.string.p1s2text2
      },
      {
        textclass: "p1s2txtbox2",
        textdata: data.string.p1s2text3
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "onboat",
            imgid: "onboat",
            imgsrc: ""
          },
          {
            imgclass: "arrowonboat",
            imgid: "arrow",
            imgsrc: ""
          },
          {
            imgclass: "finarrow",
            imgid: "arrow",
            imgsrc: ""
          },
          {
            imgclass: "cloud2",
            imgid: "cloud",
            imgsrc: ""
          },
          {
            imgclass: "cloud3",
            imgid: "cloud",
            imgsrc: ""
          }
        ]
      }
    ]
  },
  // slide3
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: " ",

    uppertextblock: [
      {
        textclass: "p1s3txtbox slideR",
        textdata: data.string.p1s3text
      }
    ],
    extratextblock: [
      {
        textclass: "p1s3txtbox2 slideR",
        textdata: data.string.p1s3text1
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "dive",
            imgid: "dive",
            imgsrc: ""
          },
          {
            imgclass: "arrowdive1",
            imgid: "arrow",
            imgsrc: ""
          },
          {
            imgclass: "arrowdive2",
            imgid: "arrow",
            imgsrc: ""
          },
          {
            imgclass: "bubbles",
            imgid: "bubbles",
            imgsrc: ""
          },
          {
            imgclass: "bubbles1",
            imgid: "bubbles",
            imgsrc: ""
          }
        ]
      }
    ]
  },
  // slide4
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: " ",

    uppertextblock: [
      {
        textclass: "p1s4txtbox slideL",
        textdata: data.string.p1s4text
      }
    ],
    extratextblock: [
      {
        textclass: "p1s4txtbox2",
        textdata: data.string.p1s4text1
      },
      {
        textclass: "p1s4txtbox3",
        textdata: data.string.p1s4text2
      },
      {
        textclass: "p1s4txtbox4",
        textdata: data.string.p1s4text3
      },
      {
        textclass: "p1s4txtbox5",
        textdata: data.string.p1s4text4
      },
      {
        textclass: "p1s4txtbox6",
        textdata: data.string.p1s4text5
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "school",
            imgid: "school",
            imgsrc: ""
          },
          {
            imgclass: "arrowschool1",
            imgid: "arrow",
            imgsrc: ""
          },
          {
            imgclass: "arrowschool2",
            imgid: "arrow",
            imgsrc: ""
          },
          {
            imgclass: "arrowschool3",
            imgid: "arrow",
            imgsrc: ""
          },
          {
            imgclass: "arrowschool4",
            imgid: "arrow",
            imgsrc: ""
          }
        ]
      }
    ]
  },
  // slide5
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: " ",

    uppertextblock: [
      {
        textclass: "p1s5txtbox slideL",
        textdata: data.string.p1s5text
      }
    ],
    extratextblock: [
      {
        textclass: "p1s5txtbox1",
        textdata: data.string.p1s5text1
      },
      {
        textclass: "p1s5txtbox2",
        textdata: data.string.p1s5text2
      },
      {
        textclass: "p1s5txtbox3",
        textdata: data.string.p1s5text3
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "yellow",
            imgid: "yellow",
            imgsrc: ""
          },
          {
            imgclass: "arrowylw1",
            imgid: "arrow",
            imgsrc: ""
          },
          {
            imgclass: "arrowylw2",
            imgid: "arrow",
            imgsrc: ""
          },
          {
            imgclass: "arrowylw3",
            imgid: "arrow",
            imgsrc: ""
          },
          {
            imgclass: "arrowylw4",
            imgid: "arrow",
            imgsrc: ""
          }
        ]
      }
    ]
  },
  // slide6
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: " ",

    uppertextblock: [
      {
        textclass: "p1s6txtbox slideR",
        textdata: data.string.p1s6text
      }
    ],
    extratextblock: [
      {
        textclass: "p1s6txtbox1",
        textdata: data.string.p1s6text1
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "lion",
            imgid: "lion",
            imgsrc: ""
          },
          {
            imgclass: "arrowlion",
            imgid: "arrow",
            imgsrc: ""
          }
        ]
      }
    ]
  },
  // slide7
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: " ",

    uppertextblock: [
      {
        textclass: "p1s7txtbox slideR",
        textdata: data.string.p1s7text
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "complete",
            imgid: "complete",
            imgsrc: ""
          },
          {
            imgclass: "birds1",
            imgid: "birds",
            imgsrc: ""
          }
        ]
      }
    ]
  },
  // slide0
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg_grn",
    extratextblock: [
      {
        textclass: "p1s0txt1box",
        textdata: data.string.p1s0text1
      },
      {
        textclass: "end bold",
        textdata: data.string.end
      },
      {
        textclass: "credit",
        textdata: data.string.credit
      }
    ]
  }
];

$(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page, countNext + 1);

  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();

  var preload;
  var timeoutvar = null;
  var current_sound;

  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      // {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
      // {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
      //   ,
      //images
      {
        id: "dummy",
        src: imgpath + "1.jpg",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "ship",
        src: imgpath + "2.jpg",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "onboat",
        src: imgpath + "3.jpg",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "dive",
        src: imgpath + "4.jpg",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "school",
        src: imgpath + "5.jpg",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "yellow",
        src: imgpath + "6.jpg",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "lion",
        src: imgpath + "7.jpg",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "complete",
        src: imgpath + "8.jpg",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "bg",
        src: imgpath + "bg.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "arrow",
        src: imgpath + "arrow_white.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "birds",
        src: imgpath + "flying-birds.gif",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "eagle",
        src: imgpath + "flying-eagle-gif",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "cloud",
        src: imgpath + "cloud03.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "bubbles",
        src: imgpath + "water-bubbles.gif",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "txtbox",
        src: imgpath + "text_box.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "ring",
        src: imgpath + "Circle-PNG-Image.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "coverpage",
        src: imgpath + "coverpage.jpg",
        type: createjs.AbstractLoader.IMAGE
      },
      //textboxes
      {
        id: "tb-2",
        src: "images/textbox/white/tl-1.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "tb-1",
        src: "images/textbox/white/tr-2.png",
        type: createjs.AbstractLoader.IMAGE
      },
      // sounds
      { id: "sound_1", src: soundAsset + "p1_s1_sec.ogg" },
      { id: "sound_2", src: soundAsset + "p1_s2.ogg" },
      { id: "sound_3", src: soundAsset + "p1_s3.ogg" },
      { id: "sound_4", src: soundAsset + "p1_s4.ogg" },
      { id: "sound_5", src: soundAsset + "p1_s5.ogg" },
      { id: "sound_6", src: soundAsset + "p1_s6.ogg" },
      { id: "sound_7", src: soundAsset + "p1_s7.ogg" },
      { id: "sound_8", src: soundAsset + "p1_s8.ogg" },
      { id: "sound_0", src: soundAsset + "p1_s0.ogg" }
    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound); //for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }
  function handleFileLoad(event) {
    // console.log(event.item);
  }
  function handleProgress(event) {
    $("#loading-text").html(parseInt(event.loaded * 100) + "%");
  }
  function handleComplete(event) {
    $("#loading-wrapper").hide(0);
    //initialize varibales
    current_sound = createjs.Sound.play("sound_1");
    current_sound.stop();
    // call main function
    templateCaller();
  }
  //initialize
  init();

  /*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  /*===============================================
	=            data highlight function            =
	===============================================*/
  function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object"
      ? alert(
          "Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted"
        )
      : null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";
    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
        $(this).attr(
          "data-highlightcustomclass"
        ) /*if there is data-highlightcustomclass defined it is true else it is not*/
          ? (stylerulename = $(this).attr("data-highlightcustomclass"))
          : (stylerulename = "parsedstring");

        texthighlightstarttag = "<span class='" + stylerulename + "'>";
        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
        $(this).html(replaceinstring);
      });
    }
  }
  /*=====  End of data highlight function  ======*/

  /*===============================================
	 =            user notification function        =
	 ===============================================*/
  function notifyuser($notifyinside) {
    //check if $notifyinside is provided
    typeof $notifyinside !== "object"
      ? alert(
          "Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style."
        )
      : null;

    /*variable that will store the element(s) to remove notification from*/
    var $allnotifications = $notifyinside.find(
      "*[data-usernotification='notifyuser']"
    );
    // if there are any notifications removal required add the event handler
    if ($allnotifications.length > 0) {
      $allnotifications.one("click", function() {
        /* Act on the event */
        $(this).attr("data-isclicked", "clicked");
        $(this).removeAttr("data-usernotification");
      });
    }
  }

  /*=====  End of user notification function  ======*/

  /*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

  function navigationcontroller(islastpageflag) {
    typeof islastpageflag === "undefined"
      ? (islastpageflag = false)
      : typeof islastpageflag != "boolean"
      ? alert(
          "NavigationController : Hi Master, please provide a boolean parameter"
        )
      : null;

    if (countNext == 0 && $total_page != 1) {
      $nextBtn.show(0);
      $prevBtn.css("display", "none");
    } else if ($total_page == 1) {
      $prevBtn.css("display", "none");
      $nextBtn.css("display", "none");

      // if lastpageflag is true
      islastpageflag
        ? ole.footerNotificationHandler.lessonEndSetNotification()
        : ole.footerNotificationHandler.lessonEndSetNotification();
    } else if (countNext > 0 && countNext < $total_page - 1) {
      $nextBtn.show(0);
      $prevBtn.show(0);
    } else if (countNext == $total_page - 1) {
      $nextBtn.css("display", "none");
      $prevBtn.show(0);

      // if lastpageflag is true
      islastpageflag
        ? ole.footerNotificationHandler.lessonEndSetNotification()
        : ole.footerNotificationHandler.pageEndSetNotification();
    }
  }

  /*=====  End of user navigation controller function  ======*/

  /*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

  function instructionblockcontroller() {
    var $instructionblock = $board.find("div.instructionblock");
    if ($instructionblock.length > 0) {
      var $contentblock = $board.find("div.concasetentblock");
      var $toggleinstructionblockbutton = $instructionblock.find(
        "div.toggleinstructionblock"
      );
      var instructionblockisvisibleflag;

      $contentblock.css("pointer-events", "none");

      $toggleinstructionblockbutton.on("click", function() {
        instructionblockisvisibleflag = $instructionblock.attr(
          "data-instructionblockshow"
        );
        if (instructionblockisvisibleflag == "true") {
          instructionblockisvisibleflag = "false";
          $contentblock.css("pointer-events", "auto");
        } else if (instructionblockisvisibleflag == "false") {
          instructionblockisvisibleflag = "true";
          $contentblock.css("pointer-events", "none");
        }

        $instructionblock.attr(
          "data-instructionblockshow",
          instructionblockisvisibleflag
        );
      });
    }
  }

  /*=====  End of InstructionBlockController  ======*/

  /*=================================================
	 =            general template function            =
	 =================================================*/
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    vocabcontroller.findwords(countNext);
    put_image(content, countNext);
    put_image2(content, countNext);
    put_speechbox_image(content, countNext);
    switch (countNext) {
      case 0:
        $nextBtn.hide();
        $(".hidethis").hide();
        setTimeout(function() {
          $(".p1s0").animate({ width: "13%" }, 1000);
          setTimeout(function() {
            $(".p1d").fadeIn();
            setTimeout(function() {
              $(".p1i").fadeIn();
              setTimeout(function() {
                $(".p1v").fadeIn();
                setTimeout(function() {
                  $(".p1e").fadeIn();
                  setTimeout(function() {
                    sound_player("sound_0");
                  }, 500);
                }, 500);
              }, 500);
            }, 500);
          }, 1000);
        }, 1000);
        break;
      case 1:
        sound_player("sound_1");
        break;
      case 2:
        setTimeout(function() {
          $(".yellowcolor").addClass("ringclass");
        }, 13000);
        sound_player("sound_2");
        break;
      case 3:
        setTimeout(function() {
          sound_player("sound_3");
        }, 1000);
        break;
      case 4:
        setTimeout(function() {
          sound_player("sound_4");
        }, 1000);
        break;
      case 5:
        setTimeout(function() {
          sound_player("sound_5");
        }, 1000);
        break;
      case 6:
        setTimeout(function() {
          sound_player("sound_6");
        }, 1000);
        break;
      case 7:
        setTimeout(function() {
          sound_player("sound_7");
        }, 1000);
        break;
      case 8:
        sound_player("sound_8");
        break;
      case 9:
        ole.footerNotificationHandler.pageEndSetNotification();
        break;
    }
  }
  function nav_button_controls(delay_ms) {
    timeoutvar = setTimeout(function() {
      if (countNext == 0) {
        $nextBtn.show(0);
      } else if (countNext > 0 && countNext == $total_page - 1) {
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else {
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    }, delay_ms);
  }
  function sound_player(sound_id, next) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on("complete", function() {
      if (next == null) navigationcontroller();
    });
  }

  function sound_player_duo(sound_id, sound_id_2) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    //current_sound_2 = createjs.Sound.play(sound_id_2);
    current_sound.play();
    current_sound.on("complete", function() {
      $(".dotext").show(0);
      sound_player(sound_id_2);
    });
  }

  function put_image(content, count) {
    if (content[count].hasOwnProperty("imageblock")) {
      var imageblock = content[count].imageblock[0];
      if (imageblock.hasOwnProperty("imagestoshow")) {
        var imageClass = imageblock.imagestoshow;
        for (var i = 0; i < imageClass.length; i++) {
          var image_src = preload.getResult(imageClass[i].imgid).src;
          //get list of classes
          var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
          var selector = "." + classes_list[classes_list.length - 1];
          $(selector).attr("src", image_src);
        }
      }
    }
  }

  function put_image2(content, count) {
    if (content[count].hasOwnProperty("livinnonlivin")) {
      var lncontent = content[count].livinnonlivin[0];
      if (lncontent.hasOwnProperty("imageblock")) {
        var imageblock = lncontent.imageblock[0];
        if (imageblock.hasOwnProperty("imagestoshow")) {
          var imageClass = imageblock.imagestoshow;
          for (var i = 0; i < imageClass.length; i++) {
            var image_src = preload.getResult(imageClass[i].imgid).src;
            //get list of classes
            var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
            var selector = "." + classes_list[classes_list.length - 1];
            $(selector).attr("src", image_src);
          }
        }
      }
      var lncontent = content[count].livinnonlivin[1];
      if (lncontent.hasOwnProperty("imageblock")) {
        var imageblock = lncontent.imageblock[0];
        if (imageblock.hasOwnProperty("imagestoshow")) {
          var imageClass = imageblock.imagestoshow;
          for (var i = 0; i < imageClass.length; i++) {
            var image_src = preload.getResult(imageClass[i].imgid).src;
            //get list of classes
            var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
            var selector = "." + classes_list[classes_list.length - 1];
            $(selector).attr("src", image_src);
          }
        }
      }
    }
  }
  function put_speechbox_image(content, count) {
    if (content[count].hasOwnProperty("speechbox")) {
      var speechbox = content[count].speechbox;
      for (var i = 0; i < speechbox.length; i++) {
        var image_src = preload.getResult(speechbox[i].imgid).src;
        //get list of classes
        var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
        var selector =
          "." + classes_list[classes_list.length - 1] + ">.speechbg";
        // console.log(selector);
        $(selector).attr("src", image_src);
      }
    }
  }
  function templateCaller() {
    $prevBtn.css("display", "none");
    $nextBtn.css("display", "none");

    if (countNext == 0) navigationcontroller();

    generaltemplate();
    loadTimelineProgress($total_page, countNext + 1);
  }

  $nextBtn.on("click", function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    switch (countNext) {
      default:
        countNext++;
        templateCaller();
        break;
    }
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    countNext--;
    templateCaller();
    /* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
    countNext < $total_page
      ? ole.footerNotificationHandler.hideNotification()
      : null;
  });
});

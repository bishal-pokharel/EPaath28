var imgpath = $ref+"/exercise/images/";

var content=[
	//slide 0
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.excq1,
			textclass : 'topbox',
		}],
		passageblock:[
		{
			textdata : data.string.exctext,
			textclass : 'passage',
			 datahighlightflag:true,
            datahighlightcustomclass:"color",
		}],
		optionsblockadditionalclass: 'optionblock2',
		optionsblock : [{
			textdata : data.string.p1q1op1stext,
			optionclass : 'class1'
		},
		{
			textdata : data.string.p1q1op2stext,
			optionclass : 'class2'
		},
		{
			textdata : data.string.p1q1op3text,
			optionclass : 'class3'
		},
		{
			textdata : data.string.p1q1op4text,
			optionclass : 'class4'
		}],
	},
	//slide 1
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.excq2,
			textclass : 'topbox',
		}],
		passageblock:[
		{
			textdata : data.string.exctext,
			textclass : 'passage',
		    datahighlightflag:true,
            datahighlightcustomclass:"color",
		}],
		optionsblockadditionalclass: 'optionblock2',
		optionsblock : [{
			textdata : data.string.q2op1stext,
			optionclass : 'class1'
		},
		{
			textdata : data.string.q2op2stext,
			optionclass : 'class2'
		},
		{
			textdata : data.string.q2op3text,
			optionclass : 'class3'
		},
		{
			textdata : data.string.q2op4text,
			optionclass : 'class4'
		}],
	},
	//slide 2
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.excq3,
			textclass : 'topbox',
		}],
		passageblock:[
		{
			textdata : data.string.exctext,
			textclass : 'passage',
			datahighlightflag:true,
            datahighlightcustomclass:"color",
		}],
		optionsblockadditionalclass: 'optionblock2',
		optionsblock : [{
			textdata : data.string.q3op1stext,
			optionclass : 'class1'
		},
		{
			textdata : data.string.q3op2stext,
			optionclass : 'class2'
		},
		{
			textdata : data.string.q3op3text,
			optionclass : 'class3'
		},
		{
			textdata : data.string.q3op4text,
			optionclass : 'class4'
		}],
	},
	//slide 3
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.excq4,
			textclass : 'topbox',
		}],
		passageblock:[
		{
			textdata : data.string.exctext,
			textclass : 'passage',
			datahighlightflag:true,
            datahighlightcustomclass:"color",
		}],
		optionsblockadditionalclass: 'optionblock2',
		optionsblock : [{
			textdata : data.string.q4op1stext,
			optionclass : 'class1'
		},
		{
			textdata : data.string.q4op2stext,
			optionclass : 'class2'
		},
		{
			textdata : data.string.q4op3text,
			optionclass : 'class3'
		},
		{
			textdata : data.string.q4op4text,
			optionclass : 'class4'
		}],
	},
	//slide 4
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.excq5,
			textclass : 'topbox',
		}],
		passageblock:[
		{
			textdata : data.string.exctext,
			textclass : 'passage',
			datahighlightflag:true,
            datahighlightcustomclass:"color",
		}],
		optionsblockadditionalclass: 'optionblock2',
		optionsblock : [{
			textdata : data.string.q5op1stext,
			optionclass : 'class1'
		},
		{
			textdata : data.string.q5op2stext,
			optionclass : 'class2'
		},
		{
			textdata : data.string.q5op3text,
			optionclass : 'class3'
		},
		{
			textdata : data.string.q5op4text,
			optionclass : 'class4'
		}],
	},
	//slide 5
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.excq6,
			textclass : 'topbox',
		}],
		passageblock:[
		{
			textdata : data.string.exctext,
			textclass : 'passage',
			datahighlightflag:true,
            datahighlightcustomclass:"color",
		}],
		optionsblockadditionalclass: 'optionblock2',
		optionsblock : [{
			textdata : data.string.q6op1stext,
			optionclass : 'class1'
		},
		{
			textdata : data.string.q6op2stext,
			optionclass : 'class2'
		},
		{
			textdata : data.string.q6op3text,
			optionclass : 'class3'
		},
		{
			textdata : data.string.q6op4text,
			optionclass : 'class4'
		}],
	},
	//slide 6
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.excq7,
			textclass : 'topbox',
		}],
		passageblock:[
		{
			textdata : data.string.exctext,
			textclass : 'passage',
			datahighlightflag:true,
            datahighlightcustomclass:"color",
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.q7op1stext,
			optionclass : 'class1'
		},
		{
			textdata : data.string.q7op2stext,
			optionclass : 'class2'
		},
		{
			textdata : data.string.q7op3text,
			optionclass : 'class3'
		},
		{
			textdata : data.string.q7op4text,
			optionclass : 'class4'
		}],
	},
	//slide 7
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.excq8,
			textclass : 'topbox',
		}],
		passageblock:[
		{
			textdata : data.string.exctext,
			textclass : 'passage',
			datahighlightflag:true,
            datahighlightcustomclass:"color",
		}],
		optionsblockadditionalclass: 'optionblock2',
		optionsblock : [{
			textdata : data.string.q8op1stext,
			optionclass : 'class1'
		},
		{
			textdata : data.string.q8op2stext,
			optionclass : 'class2'
		},
		{
			textdata : data.string.q8op3text,
			optionclass : 'class3'
		},
		{
			textdata : data.string.q8op4text,
			optionclass : 'class4'
		}],
	},
		//slide 8
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.excq9,
			textclass : 'topbox',
		}],
		passageblock:[
		{
			textdata : data.string.exctext,
			textclass : 'passage',
			datahighlightflag:true,
            datahighlightcustomclass:"color",
		}],
		optionsblockadditionalclass: 'optionblock2',
		optionsblock : [{
			textdata : data.string.q9op1stext,
			optionclass : 'class1'
		},
		{
			textdata : data.string.q9op2stext,
			optionclass : 'class2'
		},
		{
			textdata : data.string.q9op3text,
			optionclass : 'class3'
		},
		{
			textdata : data.string.q9op4text,
			optionclass : 'class4'
		}],
	},
			//slide 9
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.excq10,
			textclass : 'topbox',
		}],
		passageblock:[
		{
			textdata : data.string.exctext,
			textclass : 'passage',
			datahighlightflag:true,
            datahighlightcustomclass:"color",
		}],
		optionsblockadditionalclass: 'optionblock2',
		optionsblock : [{
			textdata : data.string.q10op1stext,
			optionclass : 'class1'
		},
		{
			textdata : data.string.q10op2stext,
			optionclass : 'class2'
		},
		{
			textdata : data.string.q10op3text,
			optionclass : 'class3'
		},
		{
			textdata : data.string.q10op4text,
			optionclass : 'class4'
		}],
	}

];

 // content.shufflearray();


$(function ()
{
	var $board    = $('.board');
	var $nextBtn  = $("#activity-page-next-btn-enabled");
	var $prevBtn  = $("#activity-page-prev-btn-enabled");
	var countNext = 0;

	var $total_page = content.length;
	var score = 0;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

	//add bg to rhino
	// add_bg(['bg_1.png','bg_2.png','bg_3.png']);
	// add_bg(['bg01.png','bg02.png','bg03.png']);
	// add_bg(['city_1.png','city_2.png','city_3.png']);
	var rhino = new RhinoTemplate();

	rhino.init($total_page);



	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		$("#box_icon_rhino").hide(0);

		//randomize options
		var parent = $(".optionsblock");
		var divs = parent.children();
		while (divs.length) {
	    	parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
		$('.topbox').prepend(countNext+1+". ");
		//$('.question_number').html(' '+ (countNext+1));
		var wrong_clicked = false;
		$(".optioncontainer").click(function(){
			if($(this).hasClass("class1")){
				if(!wrong_clicked){
					rhino.update(true);
				}
				$(this).children('.correct-icon').show(0);
				$(".optioncontainer").css('pointer-events', 'none');
				$(this).css({
					'border': '3px solid #FCD172',
					'background-color': '#6EB260',
					'color': 'white'
				});
				play_correct_incorrect_sound(1);
				if(countNext != $total_page)
					$nextBtn.show(0);
			}
			else{
				if(!wrong_clicked){
					rhino.update(false);
				}
				$(this).children('.incorrect-icon').show(0);
				$(this).css({
					'background-color': '#BA6B82',
					'border': 'none'
				});
				wrong_clicked = true;
				play_correct_incorrect_sound(0);
			}
		});
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		if(countNext < 13){
			templateCaller();
			rhino.gotoNext();
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

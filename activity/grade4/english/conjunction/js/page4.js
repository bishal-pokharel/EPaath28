var imgpath = $ref + "/images/page1/";
var imgpath2 = $ref + "/images/all/";
var	soundAsset = $ref+"/sounds/";

// var sound_dg2 = new buzz.sound((soundAsset + "p4_s1.ogg"));
// var sound_dg2a = new buzz.sound((soundAsset + "p4_s1_1.ogg"));
// var sound_dg3 = new buzz.sound((soundAsset + "p4_s2.ogg"));
// var sound_dg4 = new buzz.sound((soundAsset + "p4_s3.ogg"));
// var sound_dg5 = new buzz.sound((soundAsset + "p4_s4.ogg"));
// var sound_dg6 = new buzz.sound((soundAsset + "p4_s5.ogg"));

content = [
//slide 0
{
    hasheaderblock: false,
    contentblockadditionalclass: 'ole-background-gradient-brick',
    headerblock: [
    {
        textdata: data.lesson.chapter,
        textclass: 'chapter-title'
    }
    ],
    imageblock: [{
         imagestoshow: [{
            imgclass: "for_but makeinvisible",
            imgsrc: imgpath2 + "but.png",
        },
        {
            imgclass: "for_and makeinvisible",
            imgsrc: imgpath2 + "and.png",
        },
        {
            imgclass: "for_or makeinvisible",
            imgsrc: imgpath2 + "or.png",
        },
        {
            imgclass: "for_because makeinvisible",
            imgsrc: imgpath2 + "because.png",
        }],
    }
    ]
},

//slide 1
{
    hasheaderblock: false,
    contentblockadditionalclass: 'ole-background-gradient-rose_glow',

    imageblock: [{
        imagestoshow: [
        {
            imgclass: "for_or animateimg",
            imgsrc: imgpath2 + "or.png",
        }
        ],
    }
    ],
    flipcontainer: [
    {
        sectioncontainer: "section",
        topfront: data.string.orcapital,
        fronttext: data.string.orcapital + " " + data.string.frontortext,
        backtext: data.string.orcapital + " " + data.string.backortext,
        toptextclass: "toptext",
        buttontextclass: "buttontext"
    },
    ]
},

 //slide 2
 {
   contentblockadditionalclass: 'ole-background-gradient-rose_glow',
    hasheaderblock: false,

    imageblock: [{
        imagestoshow: [
        {
            imgclass: "imgback",
            imgsrc: imgpath2 + "or.png",
        },
        {
            imgclass: "centerimage",
            imgsrc: imgpath + "animated-snoopy-image-0046.gif",
        }
        ],
        imagelabels: [
        {
            imagelabelclass: "lefttext1",
            imagelabeldata: data.string.p4t31,
            datahighlightflag: true,
            datahighlightcustomclass: "colorme",
        },
        {
            imagelabelclass: "righttext1",
            imagelabeldata: data.string.p4t32,
            datahighlightflag: true,
            datahighlightcustomclass: "colorme",
        },
        {
            imagelabelclass: "textcomplete",
            imagelabeldata: data.string.p4t33complete,
            datahighlightflag: true,
            datahighlightcustomclass: "colorme",
            datahighlightcustomclass2: "colorme5",
        }
        ]
    }
    ]
},

//slide 4
{
  contentblockadditionalclass: 'ole-background-gradient-rose_glow',
    hasheaderblock: false,

    imageblock: [{
        imagestoshow: [
        {
            imgclass: "imgback",
            imgsrc: imgpath2 + "or.png",
        },
        {
            imgclass: "leftimage1",
            imgsrc: imgpath + "ball02.png",
        },
        {
            imgclass: "rightimage1",
            imgsrc: imgpath + "ball01.png",
        }
        ],
        imagelabels: [
        {
            imagelabelclass: "lefttext1",
            imagelabeldata: data.string.p4t41,
            datahighlightflag: true,
            datahighlightcustomclass: "colorme",

        },
        {
            imagelabelclass: "righttext1",
            imagelabeldata: data.string.p4t42,
            datahighlightflag: true,
            datahighlightcustomclass: "colorme",

        },
        {
            imagelabelclass: "textcomplete",
            imagelabeldata: data.string.p4t43complete,
            datahighlightflag: true,
            datahighlightcustomclass: "colorme",
            datahighlightcustomclass2: "colorme5",

        }
        ]

    }
    ]
},

//slide 5
{
  contentblockadditionalclass: 'ole-background-gradient-rose_glow',
    hasheaderblock: false,

    imageblock: [{
        imagestoshow: [
        {
            imgclass: "imgback",
            imgsrc: imgpath2 + "or.png",
        },
        {
            imgclass: "leftimage1",
            imgsrc: imgpath + "umbrella.png",
        },
        {
            imgclass: "rightimage1",
            imgsrc: imgpath + "jacket.png",
        }
        ],
        imagelabels: [
        {
            imagelabelclass: "lefttext1",
            imagelabeldata: data.string.p4t51,
            datahighlightflag: true,
            datahighlightcustomclass: "colorme",
        },
        {
            imagelabelclass: "righttext1",
            imagelabeldata: data.string.p4t52,
            datahighlightflag: true,
            datahighlightcustomclass: "colorme",
        },
        {
            imagelabelclass: "textcomplete",
            imagelabeldata: data.string.p4t53complete,
            datahighlightflag: true,
            datahighlightcustomclass: "colorme",
            datahighlightcustomclass2: "colorme5",
        }
        ]
    }
    ]
},

//slide 6
{
  contentblockadditionalclass: 'ole-background-gradient-rose_glow',
    hasheaderblock: false,

    imageblock: [{
        imagestoshow: [
        {
            imgclass: "imgback",
            imgsrc: imgpath2 + "or.png",
        },
        {
            imgclass: "leftimage1",
            imgsrc: imgpath + "pokhara.jpg",
        },
        {
            imgclass: "rightimage1",
            imgsrc: imgpath + "chitwan.jpg",
        }
        ],
        imagelabels: [
        {
            imagelabelclass: "lefttext1",
            imagelabeldata: data.string.p4t61,
            datahighlightflag: true,
            datahighlightcustomclass: "colorme",
        },
        {
            imagelabelclass: "righttext1",
            imagelabeldata: data.string.p4t62,
            datahighlightflag: true,
            datahighlightcustomclass: "colorme",
        },
        {
            imagelabelclass: "textcomplete",
            imagelabeldata: data.string.p4t63complete,
            datahighlightflag: true,
            datahighlightcustomclass: "colorme",
            datahighlightcustomclass2: "colorme5",
        }
        ]
    }
    ]
}
];

$(function () {
    $(window).resize(function () {
        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   
    });

    var $board = $(".board");
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var total_page = 0;

    var $total_page = content.length;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    loadTimelineProgress($total_page, countNext + 1);
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            // sounds
            {id: "sound_1", src: soundAsset+"p4_s1.ogg"},
            {id: "sound_1_1", src: soundAsset+"p4_s1_1.ogg"},
            {id: "sound_2", src: soundAsset+"p4_s2.ogg"},
            {id: "sound_3", src: soundAsset+"p4_s3.ogg"},
            {id: "sound_4", src: soundAsset+"p4_s4.ogg"},
            {id: "sound_5", src: soundAsset+"p4_s5.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }
    function handleFileLoad(event) {
        // console.log(event.item);
    }
    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded*100)+'%');
    }
    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }
    //initialize
    init();
    /*
      inorder to use the handlebar partials we need to register them
      to their respective handlebar partial pointer first
      */
      Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
      Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
      Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

    // Handlebars.registerPartial("tablecontent", $("#tablecontent-partial").html());


    	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightstarttag2;
        var texthighlightstarttag3;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
              $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

              $(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
              (stylerulename2 = "parsedstring2") ;

              $(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
              (stylerulename3 = "parsedstring3") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
            texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

	/*=====  End of data highlight function  ======*/


    // controls the navigational state of the program
    function navigationcontroller(islastpageflag){
        // check if the parameter is defined and if a boolean,
        // update islastpageflag accordingly
        typeof islastpageflag === "undefined" ?
        islastpageflag = false :
        typeof islastpageflag != 'boolean'?
        alert("NavigationController : Hi Master, please provide a boolean parameter") :
        null;

        if(countNext == 0 && $total_page!=1){
            $nextBtn.show(0);
            $prevBtn.css('display', 'none');
        }
        else if($total_page == 1){
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');

            // if lastpageflag is true
            islastpageflag ?
            ole.footerNotificationHandler.lessonEndSetNotification() :
            ole.footerNotificationHandler.lessonEndSetNotification() ;
        }
        else if(countNext > 0 && countNext < $total_page-1){
            $nextBtn.show(0);
            $prevBtn.show(0);
        }
        else if(countNext == $total_page-1){
            $nextBtn.css('display', 'none');
            $prevBtn.show(0);

            // if lastpageflag is true
            islastpageflag ?
            ole.footerNotificationHandler.lessonEndSetNotification() :
            ole.footerNotificationHandler.pageEndSetNotification() ;
        }
    }

    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        var requestAnimationFrame = window.requestAnimationFrame;

        loadTimelineProgress($total_page, countNext + 1);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        $nextBtn.hide(0);
        switch (countNext) {
            case 0:
                setTimeout(function(){
                    navigationcontroller();
                },1000);
                break;
            case 1:
                // $nextBtn.hide(0);
                sound_player("sound_1",false,true);
                var flipsound = true;
                $(".card").on("click", function () {
                    $(this).removeClass("blinkEffect");
                    flipsound?sound_player("sound_1_1",true):sound_player("sound_1",true);
                    flipsound = flipsound?false:true;
                    $(this).toggleClass("flipped");
                });
                break;
            case 2:
                sound_player("sound_2",true);
                $(".textcomplete").hide().delay(5000).fadeIn(1000);
                break;
            case 3:
                sound_player("sound_3",true);
                $(".textcomplete").hide().delay(4000).fadeIn(1000);
                break;
            case 4:
                sound_player("sound_4",true);
                $(".textcomplete").hide().delay(5000).fadeIn(1000);
                break;
            case 5:
                sound_player("sound_5",true);
                $(".textcomplete").hide().delay(4000).fadeIn(1000);
                break;
        }
    };
    function sound_player(sound_id, next,addclass){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function(){
            next?navigationcontroller():'';
            addclass?$(".card").addClass("blinkEffect"):"";
        });
    }
    function playaudio(sound_data, $dialog_container){
        var playing = true;
        $dialog_container.removeClass("playable");
        $dialog_container.click(function(){
          if(!playing){
            playaudio(sound_data, $dialog_container);
          }
          return false;
        });
        $prevBtn.hide(0);
        if((countNext+1) == content.length){
          ole.footerNotificationHandler.hideNotification();
        }else{
          $nextBtn.hide(0);
        }
        sound_data.play();
        sound_data.bind('ended', function(){
          setTimeout(function(){
            if(countNext != 0)
            $prevBtn.show(0);
            $dialog_container.addClass("playable");
            playing = false;
            sound_data.unbind('ended');
            if((countNext+1) == content.length){
              ole.footerNotificationHandler.pageEndSetNotification();
            }else{
              $nextBtn.show(0);
            }
          }, 1000);
        });
      }

    function templateCaller() {
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);
        generalTemplate();

    }

    $nextBtn.on("click", function () {
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function () {
        countNext--;
        templateCaller();
    });

    // document.addEventListener('xmlLoad', function(e) {
        total_page = content.length;
        templateCaller();
    // });


});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_1 = new buzz.sound((soundAsset + "p1_s0.ogg"));
var sound_2 = new buzz.sound((soundAsset + "p1_s1.ogg"));
var sound_3 = new buzz.sound((soundAsset + "p1_s2.ogg"));
var sound_4 = new buzz.sound((soundAsset + "p1_s3.ogg"));
var sound_5 = new buzz.sound((soundAsset + "p1_s4.ogg"));
var sound_6 = new buzz.sound((soundAsset + "s1_p6.ogg"));
var current_sound = sound_1;

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'grassbg',

		uppertextblock : [{
			textdata : data.lesson.chapter,
			textclass : 'lesson-title sniglet'
		}],
		imageblock : [{
			imagestoshow : [
				// {
				// 	imgclass : "frontimg",
				// 	imgsrc : imgpath + "map.png",
				// },
				{
					imgclass : "cv_img left_dir",
					imgsrc : imgpath + "left.png",
				},{
					imgclass : "cv_img mid_dir",
					imgsrc : imgpath + "straight.png",
				},{
					imgclass : "cv_img right_dir",
					imgsrc : imgpath + "right.png",
				}
			],
		}]
	},

	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'main_bg',

		uppertextblockadditionalclass: 'blue_heading my_font_ultra_big luckiestguy',
		uppertextblock : [{
			textdata : data.string.p1text1,
			textclass : ''
		}],

		lowertextblockadditionalclass: 'yellow_banner my_font_big luckiestguy',
		lowertextblock : [{
			textdata : data.string.p1text2,
			textclass : ''
		}],
		extratextblock:[{
			textdata : data.string.left,
			textclass : 'text-left-dir luckiestguy my_font_big'
		},{
			textdata : data.string.right,
			textclass : 'text-right-dir luckiestguy my_font_big'
		},{
			textdata : data.string.straight,
			textclass : 'text-center-dir luckiestguy my_font_big'
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "center_img",
					imgsrc : imgpath + "straight.png",
				},
				{
					imgclass : "left_img",
					imgsrc : imgpath + "left.png",
				},
				{
					imgclass : "right_img",
					imgsrc : imgpath + "right.png",
				}
			],
		}]
	},
	// slide 2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'grassbg',
		uppertextblockadditionalclass: 'bottom_ins',
		uppertextblock : [{
			textdata : data.string.p1text3,
			textclass : 'my_font_big luckiestguy'
		}],

		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "arrow",
						imgsrc : imgpath + "arrow_s.png",
					},
					{
						imgclass : "car car_up",
						imgsrc : imgpath + "yellowcar.png",
					},
					{
						imgclass : "cinema",
						imgsrc : imgpath + "cinema01.png",
					},
					{
						imgclass : "restaurant",
						imgsrc : imgpath + "restaurant.png",
					},
					{
						imgclass : "school",
						imgsrc : imgpath + "school.png",
					},
					{
						imgclass : "central_arrow highlight_image",
						imgsrc : imgpath + "straight.png",
					}

				],
				imagelabels : [
					{
						imagelabelclass : "road_horizontal",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "road_vertical",
						imagelabeldata : '',
					}
				]
			}
		],
	},
	// slide 3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'grassbg',

		uppertextblockadditionalclass: 'bottom_ins',
		uppertextblock : [{
			textdata : data.string.p1text4,
			textclass : 'my_font_big luckiestguy'
		}],

		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "arrow",
						imgsrc : imgpath + "arrow_l.png",
					},
					{
						imgclass : "car car_up",
						imgsrc : imgpath + "yellowcar.png",
					},
					{
						imgclass : "cinema",
						imgsrc : imgpath + "cinema01.png",
					},
					{
						imgclass : "restaurant",
						imgsrc : imgpath + "restaurant.png",
					},
					{
						imgclass : "school",
						imgsrc : imgpath + "school.png",
					},
					{
						imgclass : "central_arrow highlight_image",
						imgsrc : imgpath + "left.png",
					}

				],
				imagelabels : [
					{
						imagelabelclass : "road_horizontal",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "road_vertical",
						imagelabeldata : '',
					}
				]
			}
		],
	},
	// slide 4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'grassbg',

		uppertextblockadditionalclass: 'bottom_ins',
		uppertextblock : [{
			textdata : data.string.p1text5,
			textclass : 'my_font_big luckiestguy'
		}],

		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "arrow",
						imgsrc : imgpath + "arrow_r.png",
					},
					{
						imgclass : "car car_up",
						imgsrc : imgpath + "yellowcar.png",
					},
					{
						imgclass : "cinema",
						imgsrc : imgpath + "cinema01.png",
					},
					{
						imgclass : "restaurant",
						imgsrc : imgpath + "restaurant.png",
					},
					{
						imgclass : "school",
						imgsrc : imgpath + "school.png",
					},
					{
						imgclass : "central_arrow highlight_image",
						imgsrc : imgpath + "right.png",
					}

				],
				imagelabels : [
					{
						imagelabelclass : "road_horizontal",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "road_vertical",
						imagelabeldata : '',
					}
				]
			}
		],
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'light-green',

		extratextblock : [{
			textdata : data.string.p1text6,
			textclass : 'great-job luckiestguy my_font_super_big'
		},{
			textdata : data.string.p1text7,
			textclass : 'move-on sniglet my_font_big'
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "sundar",
					imgsrc : "images/sundar/correct-1.png",
				}
			],
		}]
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var myTimeout =  null;
	var timeoutvar =  null;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		is_last_page= false;

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
			case 0:
				sound_player(sound_1,1);
				// nav_button_controls(100);
				break;
			case 1:
				sound_player(sound_2,1);
				// nav_button_controls(100);
				break;
			case 2:
				sound_player(sound_3);
				$prevBtn.show(0);
				$('.central_arrow').click(function(){
					$('.central_arrow').removeClass('highlight_image');
					$('.central_arrow').css('pointer-events', 'none');
						$('.car').addClass('go_up');
					myTimeout = setTimeout(function(){
						$('.car').removeClass('go_up');
						$('.central_arrow').css('pointer-events', 'all');
						$nextBtn.show(0);
					}, 2500);
				});
				break;
			case 3:
				sound_player(sound_4);
				$prevBtn.show(0);
				$('.central_arrow').click(function(){
					$('.central_arrow').removeClass('highlight_image');
					$('.central_arrow').css('pointer-events', 'none');
						$('.car').addClass('go_left');
					myTimeout = setTimeout(function(){
						$('.car').removeClass('go_left');
						$('.central_arrow').css('pointer-events', 'all');
						$nextBtn.show(0);
					}, 3500);
				});
				break;
			case 4:
				sound_player(sound_5);
				$prevBtn.show(0);
				$('.central_arrow').click(function(){
					$('.central_arrow').removeClass('highlight_image');
					$('.central_arrow').css('pointer-events', 'none');
						$('.car').addClass('go_right');
					myTimeout = setTimeout(function(){
						$('.car').removeClass('go_right');
						$('.central_arrow').css('pointer-events', 'all');
						$nextBtn.show(0);
					}, 3500);
				});
				break;
			default:
			case 5:
				sound_player(sound_6, 1);
			break;
				nav_button_controls(100);
				break;
		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}


	function sound_player(sound_data, next){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			next?nav_button_controls():'';
		});
	}


	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		clearTimeout(timeoutvar);
		switch(countNext){
			case 1:
			case 2:
				clearTimeout(myTimeout);
			default:
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		clearTimeout(timeoutvar);
		if(countNext==2||countNext==3){
			clearTimeout(myTimeout);
		}
		clearTimeout(myTimeout);
		current_sound.stop();
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

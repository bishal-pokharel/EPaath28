var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_correct = new buzz.sound(("sounds/common/correct.ogg"));
var sound_beep = new buzz.sound((soundAsset + "beep.ogg"));

var sound_0 = new buzz.sound((soundAsset + "s6_p1.ogg"));
var sound_1 = new buzz.sound((soundAsset + "p6_s0.ogg"));
var sound_2 = new buzz.sound((soundAsset + "p6_s1.ogg"));
var sound_3 = new buzz.sound((soundAsset + "p6_s2.ogg"));
var sound_4 = new buzz.sound((soundAsset + "p6_s3.ogg"));
var sound_5 = new buzz.sound((soundAsset + "p6_s4.ogg"));
var sound_6 = new buzz.sound((soundAsset + "p6_s5.ogg"));

var sound_7 = new buzz.sound((soundAsset + "p6_s7.ogg"));
var sound_8 = new buzz.sound((soundAsset + "p6_s8.ogg"));
var sound_9 = new buzz.sound((soundAsset + "p6_s9.ogg"));
var sound_10 = new buzz.sound((soundAsset + "p6_s10.ogg"));
var sound_11 = new buzz.sound((soundAsset + "p6_s12.ogg"));

var current_sound = sound_correct;
var content = [
	// slide 0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'main_bg',

		uppertextblockadditionalclass: 'blue_heading my_font_ultra_big luckiestguy',
		uppertextblock : [{
			textdata : data.string.p6text1,
			textclass : ''
		}],

		lowertextblockadditionalclass: 'bottom-light-blue sniglet my_font_big',
		lowertextblock : [{
			textdata : data.string.p6text3,
			textclass : ' ',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'

		}],
		lowertextblock2additionalclass: 'yellow_banner my_font_big luckiestguy',
		lowertextblock2:[{
			textdata : data.string.p6text2,
			textclass : ''
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "boy-a",
					imgsrc : imgpath + "sagar-09.png",
				},
				{
					imgclass : "goat-1",
					imgsrc : imgpath + "goat.png",
				},
			],
		}],
	},
	// slide 1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'main_bg',

		uppertextblockadditionalclass: 'blue_heading my_font_ultra_big luckiestguy',
		uppertextblock : [{
			textdata : data.string.p6text1,
			textclass : ''
		}],

		lowertextblockadditionalclass: 'bottom-light-blue sniglet my_font_big',
		lowertextblock : [{
			textdata : data.string.p6text4,
			textclass : ' ',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'

		}],
		lowertextblock2additionalclass: 'yellow_banner my_font_big luckiestguy',
		lowertextblock2:[{
			textdata : data.string.p6text2,
			textclass : ''
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "boy-a",
					imgsrc : imgpath + "sagar-09.png",
				},
				{
					imgclass : "goat-1",
					imgsrc : imgpath + "goat.png",
				},
			],
		}],
	},

	// slide 2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'main_bg',

		uppertextblockadditionalclass: 'blue_heading my_font_ultra_big luckiestguy',
		uppertextblock : [{
			textdata : data.string.p6text1,
			textclass : ''
		}],

		lowertextblockadditionalclass: 'bottom-light-blue sniglet my_font_big',
		lowertextblock : [{
			textdata : data.string.p6text5,
			textclass : ' ',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'

		}],
		lowertextblock2additionalclass: 'yellow_banner my_font_big luckiestguy',
		lowertextblock2:[{
			textdata : data.string.p6text2,
			textclass : ''
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "boy-a",
					imgsrc : imgpath + "sagar-09.png",
				},
				{
					imgclass : "goat-1",
					imgsrc : imgpath + "goat.png",
				},
				{
					imgclass : "tara-1",
					imgsrc : "images/fairy/fairy_fly.gif",
				}
			],
		}],
	},
	// slide 3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'diy-bg',


		lowertextblockadditionalclass: 'bottom-ins sniglet',
		lowertextblock : [{
			textdata : data.string.p6text6,
			textclass : ' ',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'

		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "sagar-house",
					imgsrc : imgpath + "house05.png",
				},
				{
					imgclass : "tara",
					imgsrc : "images/fairy/fairy_fly.gif",
				},
				{
					imgclass : "tree",
					imgsrc : imgpath + "tree01.png",
				},
				{
					imgclass : "pond",
					imgsrc : imgpath + "pond.png",
				}
			],
			imagelabels : [{
				imagelabelclass: 'house-label not-hidden my_font_small',
				imagelabeldata: data.string.p6text15,
			},{
				imagelabelclass: 'place_counters one_place',
				imagelabeldata: "1",
			},{
				imagelabelclass: 'place_counters two_place',
				imagelabeldata: "2",
			},{
				imagelabelclass: 'place_counters three_place',
				imagelabeldata: "3",
			}]
		}],
		controlblock:[
			{
				leftclass : "btn-left",
				leftsrc : imgpath + "left.png",
				rightclass : "btn-right",
				rightsrc : imgpath + "right.png",
				straightclass : "btn-straight",
				straightsrc : imgpath + "straight.png",
				stopclass : "btn-stop",
				stopsrc : imgpath + "stop.png",
			},
		],

		spriteblock : [{
			divclass : 'boysprite boy-1',
			spriteclass : ''
		},
		{
			divclass : 'goatsprite',
			spriteclass : ''
		}],
	},

	// slide 4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'diy-bg',


		lowertextblockadditionalclass: 'bottom-ins sniglet',
		lowertextblock : [{
			textdata : data.string.p6text7,
			textclass : ' ',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'

		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "sagar-house",
					imgsrc : imgpath + "house05.png",
				},
				{
					imgclass : "tara",
					imgsrc : "images/fairy/fairy_fly.gif",
				},
				{
					imgclass : "tree",
					imgsrc : imgpath + "tree01.png",
				},
				{
					imgclass : "pond",
					imgsrc : imgpath + "pond.png",
				},
				{
					imgclass : "arow s5_arw",
					imgsrc : imgpath + "arrow.png",
				}
			],
			imagelabels : [{
				imagelabelclass: 'house-label my_font_small',
				imagelabeldata: data.string.p6text15,
			}]
		}],
		controlblock:[
			{
				leftclass : "btn-left",
				leftsrc : imgpath + "left.png",
				rightclass : "btn-right",
				rightsrc : imgpath + "right.png",
				straightclass : "btn-straight",
				straightsrc : imgpath + "straight.png",
				stopclass : "btn-stop",
				stopsrc : imgpath + "stop.png",
			},
		],

		spriteblock : [{
			divclass : 'boysprite boy-2',
			spriteclass : ''
		},
		{
			divclass : 'goatsprite',
			spriteclass : ''
		}],
	},

	// slide 5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'diy-bg',


		lowertextblockadditionalclass: 'bottom-ins sniglet',
		lowertextblock : [{
			textdata : data.string.p6text8,
			textclass : ' ',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "sagar-house",
					imgsrc : imgpath + "house05.png",
				},
				{
					imgclass : "tara",
					imgsrc : "images/fairy/fairy_fly.gif",
				},
				{
					imgclass : "tree",
					imgsrc : imgpath + "tree01.png",
				},
				{
					imgclass : "pond",
					imgsrc : imgpath + "pond.png",
				},
				{
					imgclass : "s6_arw",
					imgsrc : imgpath + "arro06.png",
				}
			],
			imagelabels : [{
				imagelabelclass: 'house-label my_font_small',
				imagelabeldata: data.string.p6text15,
			}]
		}],
		controlblock:[
			{
				leftclass : "btn-left",
				leftsrc : imgpath + "left.png",
				rightclass : "btn-right",
				rightsrc : imgpath + "right.png",
				straightclass : "btn-straight",
				straightsrc : imgpath + "straight.png",
				stopclass : "btn-stop",
				stopsrc : imgpath + "stop.png",
			},
		],

		spriteblock : [{
			divclass : 'boysprite boy-3',
			spriteclass : ''
		},
		{
			divclass : 'goatsprite',
			spriteclass : ''
		}],
	},
	// slide 6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'diy-bg',


		lowertextblockadditionalclass: 'bottom-ins sniglet',
		lowertextblock : [{
			textdata : data.string.p6text9,
			textclass : ' ',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'

		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "sagar-house",
					imgsrc : imgpath + "house05.png",
				},
				{
					imgclass : "tara",
					imgsrc : "images/fairy/fairy_fly.gif",
				},
				{
					imgclass : "tree",
					imgsrc : imgpath + "tree01.png",
				},
				{
					imgclass : "pond",
					imgsrc : imgpath + "pond.png",
				},
				{
					imgclass : "s7_arw",
					imgsrc : imgpath + "arro07.png",
				},
				{
					imgclass : "s7_arw_stght",
					imgsrc : imgpath + "arro03.png",
				}
			],
			imagelabels : [{
				imagelabelclass: 'house-label my_font_small',
				imagelabeldata: data.string.p6text15,
			}]
		}],
		controlblock:[
			{
				leftclass : "btn-left",
				leftsrc : imgpath + "left.png",
				rightclass : "btn-right",
				rightsrc : imgpath + "right.png",
				straightclass : "btn-straight",
				straightsrc : imgpath + "straight.png",
				stopclass : "btn-stop",
				stopsrc : imgpath + "stop.png",
			},
		],

		spriteblock : [{
			divclass : 'boysprite boy-4',
			spriteclass : ''
		},
		{
			divclass : 'goatsprite',
			spriteclass : ''
		}],
	},
	// slide 7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'diy-bg',


		lowertextblockadditionalclass: 'bottom-ins sniglet',
		lowertextblock : [{
			textdata : data.string.p6text10,
			textclass : ' ',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'

		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "sagar-house",
					imgsrc : imgpath + "house05.png",
				},
				{
					imgclass : "tara",
					imgsrc : "images/fairy/fairy_fly.gif",
				},
				{
					imgclass : "tree",
					imgsrc : imgpath + "tree01.png",
				},
				{
					imgclass : "pond",
					imgsrc : imgpath + "pond.png",
				}
			],
			imagelabels : [{
				imagelabelclass: 'house-label my_font_small',
				imagelabeldata: data.string.p6text15,
			}]
		}],
		controlblock:[
			{
				leftclass : "btn-left",
				leftsrc : imgpath + "left.png",
				rightclass : "btn-right",
				rightsrc : imgpath + "right.png",
				straightclass : "btn-straight",
				straightsrc : imgpath + "straight.png",
				stopclass : "btn-stop",
				stopsrc : imgpath + "stop.png",
			},
		],

		spriteblock : [{
			divclass : 'boysprite boy-5',
			spriteclass : ''
		},
		{
			divclass : 'goatsprite',
			spriteclass : ''
		}],
	},

	// slide 8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'diy-bg',


		lowertextblockadditionalclass: 'bottom-ins sniglet',
		lowertextblock : [{
			textdata : data.string.p6text11,
			textclass : ' ',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'

		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "sagar-house",
					imgsrc : imgpath + "house05.png",
				},
				{
					imgclass : "tara",
					imgsrc : "images/fairy/fairy_fly.gif",
				},
				{
					imgclass : "tree",
					imgsrc : imgpath + "tree01.png",
				},
				{
					imgclass : "pond",
					imgsrc : imgpath + "pond.png",
				},
				{
					imgclass : "s9_arw",
					imgsrc : imgpath + "arro05.png",
				}
			],
			imagelabels : [{
				imagelabelclass: 'house-label my_font_small',
				imagelabeldata: data.string.p6text15,
			}]
		}],
		controlblock:[
			{
				leftclass : "btn-left",
				leftsrc : imgpath + "left.png",
				rightclass : "btn-right",
				rightsrc : imgpath + "right.png",
				straightclass : "btn-straight",
				straightsrc : imgpath + "straight.png",
				stopclass : "btn-stop",
				stopsrc : imgpath + "stop.png",
			},
		],

		spriteblock : [{
			divclass : 'boysprite boy-6',
			spriteclass : ''
		},
		{
			divclass : 'goatsprite',
			spriteclass : 'goatSpriteSec'
		}],
	},
	// slide 9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'diy-bg',


		lowertextblockadditionalclass: 'bottom-ins sniglet',
		lowertextblock : [{
			textdata : data.string.p6text12,
			textclass : ' ',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'

		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "sagar-house",
					imgsrc : imgpath + "house05.png",
				},
				{
					imgclass : "tara",
					imgsrc : "images/fairy/fairy_fly.gif",
				},
				{
					imgclass : "tree",
					imgsrc : imgpath + "tree01.png",
				},
				{
					imgclass : "pond",
					imgsrc : imgpath + "pond.png",
				},
				{
					imgclass : "s9_arw_stght",
					imgsrc : imgpath + "arro01.png",
				}
			],
			imagelabels : [{
				imagelabelclass: 'house-label my_font_small',
				imagelabeldata: data.string.p6text15,
			}]
		}],
		controlblock:[
			{
				leftclass : "btn-left",
				leftsrc : imgpath + "left.png",
				rightclass : "btn-right",
				rightsrc : imgpath + "right.png",
				straightclass : "btn-straight",
				straightsrc : imgpath + "straight.png",
				stopclass : "btn-stop",
				stopsrc : imgpath + "stop.png",
			},
		],

		spriteblock : [{
			divclass : 'boysprite boy-7',
			spriteclass : ''
		},
		{
			divclass : 'goatsprite goat-7',
			spriteclass : ''
		}],
	},
	// slide 10
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'main_bg',

		uppertextblockadditionalclass: 'blue_heading my_font_ultra_big luckiestguy',
		uppertextblock : [{
			textdata : data.string.p6text1,
			textclass : ''
		}],

		lowertextblockadditionalclass: 'side-mark luckiestguy my_font_super_big',
		lowertextblock : [{
			textdata : data.string.p6text13,
			textclass : ' ',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'

		}],
		lowertextblock2additionalclass: 'yellow_banner my_font_big luckiestguy',
		lowertextblock2:[{
			textdata : data.string.p6text2,
			textclass : ''
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "boy-b",
					imgsrc : imgpath + "sagar-08.png",
				},
				{
					imgclass : "goat-b",
					imgsrc : imgpath + "goat.png",
				},
			],
		}],
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;

	var myTimeout =  null;
	var timeouts = [];
	var to_click = -1;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		is_last_page= false;

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
			case 0:
				current_sound = sound_0;
				current_sound.play();
				current_sound.bindOnce('ended', function(){
					sound_player(sound_1,1);
				});
				// nav_button_controls(500);
				break;
			case 1:
				sound_player(sound_2,1);
				$prevBtn.show(0);
				// nav_button_controls(100);
				break;
			case 2:
				sound_player(sound_3,1);
				$prevBtn.show(0);
				// nav_button_controls(100);
				break;
			case 3:
				sound_player(sound_4);
				$('.btn-4').click(function(){
					play_correct_incorrect_sound();
				});
				to_click = 2;
				$prevBtn.show(0);
				$('.dir-btn').click(function(){
					if($(this).hasClass('btn-'+to_click)){
						$('.dir-btn').unbind('click');
						$(".one_place").css("animation","none");
						to_click = 2;
						//move down
						change_direction('walking-down');
						$('.boysprite').animate({'bottom': '52%'}, 2000, function(){
							change_direction('');
							$('.dir-btn').click(function(){
								if($(this).hasClass('btn-'+to_click)){
									$('.dir-btn').unbind('click');
									$(".two_place").css("animation","none");
									to_click = 1;
									//move down
									change_direction('walking-down');
									$('.boysprite').animate({'bottom': '25%'}, 1500, function(){
										change_direction('');
										$('.dir-btn').click(function(){
											if($(this).hasClass('btn-'+to_click)){
												$('.dir-btn').unbind('click');
												$(".three_place").css("animation","none");
												//move down
												change_direction('walking-down');
												$('.boysprite').animate({'bottom': '16%'}, 800, function(){
													//move right
													change_direction('walking-right');
													$('.boysprite').animate({'left': '21%'}, 800, function(){
														$(".boysprite > .spriteclass").css({"background-position": "600% 33%"});
														$nextBtn.trigger('click');
													});
												});
											} else{
												play_correct_incorrect_sound();
											}
										});
									});
								} else{
									play_correct_incorrect_sound();
								}
							});
						});
					} else{
						play_correct_incorrect_sound();
					}
				});
				break;
			case 4:
				sound_player(sound_5);
				$('.btn-4').click(function(){
					play_correct_incorrect_sound();
				});
				to_click = 2;
				$prevBtn.show(0);
				$(".boysprite > .spriteclass").css({"background-position": "600% 33%"});
				$('.dir-btn').click(function(){
					if($(this).hasClass('btn-'+to_click)){
						$('.dir-btn').unbind('click');
						$(".s5_arw").css("animation","none");
						//move right
						change_direction('walking-right');
						$('.boysprite').animate({'left': '46%'}, 2300, function(){
							change_direction('walking-right');
							$nextBtn.trigger('click');
							$(".boysprite > .spriteclass").css({"background-position": "600% 33%"});
						});
					} else{
						play_correct_incorrect_sound();
					}
				});
				break;
			case 5:
				sound_player(sound_6);
				$('.btn-4').click(function(){
					play_correct_incorrect_sound();
				});
				to_click = 1;
				$prevBtn.show(0);
				$('.dir-btn').click(function(){
					if($(this).hasClass('btn-'+to_click)){
						$('.dir-btn').unbind('click');
						//move right
						change_direction('walking-right');
						$('.boysprite').animate({'left': '59%'}, 1500, function(){
							//move up
							change_direction('walking-up');
							$('.boysprite').animate({'bottom':'25%'}, 900, function(){
								change_direction('');
								$nextBtn.trigger('click');
							});
						});
					} else{
						play_correct_incorrect_sound();
					}
				});
				break;
			case 6:
				sound_player(sound_7);
				$('.btn-4').click(function(){
					play_correct_incorrect_sound();
				});
				to_click = 3;
				$prevBtn.show(0);
				$(".boysprite > .spriteclass").css({"background-position": "600% 69%"});
				$('.dir-btn').click(function(){
					if($(this).hasClass('btn-'+to_click)){
						$('.dir-btn').unbind('click');
						$(".s7_arw").hide(0);
						to_click = 2;
						//move up
						change_direction('walking-up');
						$('.boysprite').animate({'bottom': '42%'}, 1500, function(){
							//move right
							change_direction('walking-right');
							$('.boysprite').animate({'left':'63%'}, 700, function(){
								change_direction('');
								$(".boysprite > .spriteclass").css({"background-position": "600% 33%"});
								$(".s7_arw_stght").show(0);
								$('.dir-btn').click(function(){
									if($(this).hasClass('btn-'+to_click)){
										$('.dir-btn').unbind('click');
										//move right
										change_direction('walking-right');
										$('.boysprite').animate({'left': '90%'}, 2500, function(){
											change_direction('');
											$(".boysprite > .spriteclass").css({"background-position": "600% 33%"});
											$nextBtn.trigger('click');
										});
									} else{
										play_correct_incorrect_sound();
									}
								});
							});
						});
					} else{
						play_correct_incorrect_sound();
					}
				});
				break;
			case 7:
				sound_player(sound_8,1);
				$prevBtn.show(0);
				//move up
				change_direction('walking-up');
				$('.boysprite').animate({'bottom': '51%'}, 600, function(){
					change_direction('');
					$(".boysprite > .spriteclass").css({"background-position": "600% 100%"});
					// nav_button_controls(100);
				});
				break;
			case 8:
				$(".boysprite > .spriteclass").css({"background-position": "600% 67%"});
				sound_player(sound_9);
				$prevBtn.show(0);
				$('.btn-4').click(function(){
					play_correct_incorrect_sound();
				});
				//move left
				change_direction('walking-left');
				goat_direction('goat-left');
				$('.goatsprite').animate({'left': '78%'}, 1200);
				$('.boysprite').animate({'left': '81%'}, 1200, function(){
					$(".s9_arw").show(0);
					change_direction('');
					goat_direction('');
					to_click = 1;
					$prevBtn.show(0);
					$(".goatsprite  .spriteclass").css({
						"background-image":"url('"+imgpath+"goat01.png')",
						"background-size":"100% 100%"
					});
					$('.dir-btn').click(function(){
						if($(this).hasClass('btn-'+to_click)){
							$('.dir-btn').unbind('click');
							//move up
							$(".goatsprite  .spriteclass").css({
								"background-image":"url('"+imgpath+"goat01.png')",
								"background-size":"100% 100%"
							});
							change_direction('walking-up');
							goat_direction('goat-up');
							$('.goatsprite').animate({'bottom': '67%'}, 1400);
							$('.boysprite').animate({'bottom': '67%'}, 1400, function(){
								//move left
								change_direction('walking-left');
								goat_direction('goat-left');
								$(".goatsprite  .spriteclass").css({
									"background-image":"url('"+imgpath+"goat02.png')",
									"background-size":"100% 100%"
								});
								$('.goatsprite').animate({'left': '74%'}, 800);
								$('.boysprite').animate({'left': '77%'}, 800, function(){
									change_direction('');
									goat_direction('');
									$nextBtn.trigger('click');
								});
							});
						} else{
							play_correct_incorrect_sound();
						}
					});
				});
				break;
			case 9:
			$(".boysprite > .spriteclass").css({"background-position": "600% 0%"});
			$(".goatsprite  .spriteclass").css({
				"background-image":"url('"+imgpath+"goat02.png')",
				"background-size":"100% 100%"
			});
				sound_player(sound_10);
				var stop_clicked= false;
				$('.btn-4').click(function(){
					play_correct_incorrect_sound();
				});
				to_click = 2;
				$prevBtn.show(0);
				$('.dir-btn').click(function(){
					if($(this).hasClass('btn-'+to_click)){
						$('.dir-btn').unbind('click');
						$(".s9_arw_stght").hide(0);
						timeouts.push(setTimeout(function(){
							$('.btn-4').unbind('click');
							$('.btn-4').click(function(){
								stop_clicked = true;
								sound_player(sound_correct);
								nav_button_controls(100);
								$('.goatsprite').addClass('go-in');
								$('.boysprite').addClass('go-in');
								$('.sagar-house').removeClass('blink');
								change_direction('');
								goat_direction('');
							});
						}, 4500));
						timeouts.push(setTimeout(function(){
							if(!stop_clicked){
								$('.house-label').fadeIn(300);
								$('.sagar-house').addClass('blink');
							}
						}, 5200));
						//move left
						change_direction('walking-left');
						goat_direction('goat-left');
						$('.goatsprite').animate({'left': '1%'}, 5000);
						$('.boysprite').animate({'left': '4%'}, 5000, function(){
							//move up
							// $('.boysprite').animate({'bottom':'25%'}, 500, function(){
								// nav_button_controls(100);
							// });
						});
					} else{
						play_correct_incorrect_sound();
					}
				});
				break;
			case 10:
				sound_player(sound_11,1);
				$prevBtn.show(0);
				// nav_button_controls(100);
				break;
			default:
				$prevBtn.show(0);
				nav_button_controls(100);
				break;
		}
	}

	function change_direction(class_walk){
		$('.boysprite>p').removeClass('walking-right');
		$('.boysprite>p').removeClass('walking-up');
		$('.boysprite>p').removeClass('walking-down');
		$('.boysprite>p').removeClass('walking-left');
		$('.boysprite>p').addClass(class_walk);
	}
	function goat_direction(class_walk){

	}

	function nav_button_controls(delay_ms){
		myTimeout = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_data, next){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			next?nav_button_controls():'';
		});
	}


	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		clearTimeout(myTimeout);
		switch(countNext){
			default:
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		clearTimeout(myTimeout);
		current_sound.stop();
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

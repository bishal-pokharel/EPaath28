var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_0 = new buzz.sound((soundAsset + "s3_p1.ogg"));
var sound_1 = new buzz.sound((soundAsset + "p3_s0.ogg"));
var sound_2 = new buzz.sound((soundAsset + "p3_s1.ogg"));
var sound_3 = new buzz.sound((soundAsset + "p3_s2.ogg"));
var sound_4 = new buzz.sound((soundAsset + "p3_s3.ogg"));
var sound_5 = new buzz.sound((soundAsset + "p3_s3.ogg"));
var sound_6 = new buzz.sound((soundAsset + "p3_s3.ogg"));
var sound_7 = new buzz.sound((soundAsset + "p3_s4.ogg"));
var current_sound = sound_1;

var content = [
	// slide 0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'main_bg',

		uppertextblockadditionalclass: 'blue_heading my_font_ultra_big luckiestguy',
		uppertextblock : [{
			textdata : data.lesson.chapter,
			textclass : ''
		}],

		lowertextblockadditionalclass: 'bottom-light-blue sniglet my_font_big',
		lowertextblock : [{
			textdata : data.string.p3text2,
			textclass : ' ',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'

		}],
		lowertextblock2additionalclass: 'yellow_banner my_font_big luckiestguy',
		lowertextblock2:[{
			textdata : data.string.p3text1,
			textclass : ''
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "center_img",
					imgsrc : imgpath + "fountain.png",
				},
				{
					imgclass : "left_img",
					imgsrc : imgpath + "tree01.png",
				},
				{
					imgclass : "right_img",
					imgsrc : imgpath + "house05.png",
				}
			],
		}],
	},
	// slide 1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'main_bg',

		uppertextblockadditionalclass: 'blue_heading my_font_ultra_big luckiestguy',
		uppertextblock : [{
			textdata : data.lesson.chapter,
			textclass : ''
		}],

		lowertextblockadditionalclass: 'bottom-light-blue sniglet my_font_big',
		lowertextblock : [{
			textdata : data.string.p3text3,
			textclass : ' ',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'

		}],
		lowertextblock2additionalclass: 'yellow_banner my_font_big luckiestguy',
		lowertextblock2:[{
			textdata : data.string.p3text1,
			textclass : ''
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "center_img",
					imgsrc : imgpath + "fountain.png",
				},
				{
					imgclass : "left_img",
					imgsrc : imgpath + "tree01.png",
				},
				{
					imgclass : "right_img",
					imgsrc : imgpath + "house05.png",
				}
			],
		}],
	},
	// slide 2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		uppertextblockadditionalclass: 'blue_heading my_font_ultra_big luckiestguy',
		uppertextblock : [{
			textdata : data.lesson.chapter,
			textclass : ''
		}],

		lowertextblockadditionalclass: 'bottom-light-blue sniglet my_font_big',
		lowertextblock : [{
			textdata : data.string.p3text4,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'

		}],
		lowertextblock2additionalclass: 'yellow_banner my_font_big luckiestguy',
		lowertextblock2:[{
			textdata : data.string.p3text1,
			textclass : ''
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "car car-move-full-1",
					imgsrc : imgpath + "car.png",
				},{
					imgclass : "map-house house-1",
					imgsrc : imgpath + "house05.png",
				},{
					imgclass : "map-fountain fountain-1",
					imgsrc : imgpath + "fountain.png",
				},
			],
		}],
	},
	// slide 3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		uppertextblockadditionalclass: 'blue_heading my_font_ultra_big luckiestguy',
		uppertextblock : [{
			textdata : data.lesson.chapter,
			textclass : ''
		}],

		lowertextblockadditionalclass: 'bottom-light-blue sniglet my_font_big',
		lowertextblock : [{
			textdata : data.string.p3text5,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'

		}],
		lowertextblock2additionalclass: 'yellow_banner my_font_big luckiestguy',
		lowertextblock2:[{
			textdata : data.string.p3text1,
			textclass : ''
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "car-a car-move-full-2",
					imgsrc : imgpath + "car.png",
				},{
					imgclass : "map-house house-1",
					imgsrc : imgpath + "house05.png",
				},{
					imgclass : "map-fountain fountain-1",
					imgsrc : imgpath + "fountain.png",
				},
			],
		}],

	},

	// slide 4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		uppertextblockadditionalclass: 'blue_heading my_font_ultra_big luckiestguy',
		uppertextblock : [{
			textdata : data.lesson.chapter,
			textclass : ''
		}],

		lowertextblockadditionalclass: 'bottom-light-blue sniglet my_font_big',
		lowertextblock : [{
			textdata : data.string.p3text6,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'

		}],
		lowertextblock2additionalclass: 'yellow_banner my_font_big luckiestguy',
		lowertextblock2:[{
			textdata : data.string.p3text1,
			textclass : ''
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "car-b car-move-full-3",
					imgsrc : imgpath + "car.png",
				},{
					imgclass : "map-house house-2",
					imgsrc : imgpath + "house05.png",
				},{
					imgclass : "map-fountain fountain-2",
					imgsrc : imgpath + "fountain.png",
				},
			],
		}],
	},
	// slide 5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		uppertextblockadditionalclass: 'blue_heading my_font_ultra_big luckiestguy',
		uppertextblock : [{
			textdata : data.lesson.chapter,
			textclass : ''
		}],

		lowertextblockadditionalclass: 'bottom-light-blue sniglet my_font_big',
		lowertextblock : [{
			textdata : data.string.p3text7,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'

		}],
		lowertextblock2additionalclass: 'yellow_banner my_font_big luckiestguy',
		lowertextblock2:[{
			textdata : data.string.p3text1,
			textclass : ''
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "car-c car-move-full-4",
					imgsrc : imgpath + "car.png",
				},{
					imgclass : "map-house house-1",
					imgsrc : imgpath + "house05.png",
				},{
					imgclass : "map-fountain fountain-3",
					imgsrc : imgpath + "fountain.png",
				},
			],
		}],
	},
	// slide 6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		uppertextblockadditionalclass: 'blue_heading my_font_ultra_big luckiestguy',
		uppertextblock : [{
			textdata : data.lesson.chapter,
			textclass : ''
		}],

		lowertextblockadditionalclass: 'bottom-light-blue sniglet my_font_big',
		lowertextblock : [{
			textdata : data.string.p3text8,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'

		}],
		lowertextblock2additionalclass: 'yellow_banner my_font_big luckiestguy',
		lowertextblock2:[{
			textdata : data.string.p3text1,
			textclass : ''
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "car-c",
					imgsrc : imgpath + "car.png",
				},{
					imgclass : "map-house house-1",
					imgsrc : imgpath + "house05.png",
				},{
					imgclass : "map-fountain fountain-3",
					imgsrc : imgpath + "fountain.png",
				},
			],
		}],
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var myTimeout =  null;
	var timeouts = [];

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		is_last_page= false;

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
			case 0:
				current_sound = sound_0;
				current_sound.play();
				current_sound.bindOnce('ended', function(){
					sound_player(sound_1,1);
				});
				// nav_button_controls(500);
				break;
			case 1:
				$prevBtn.show(0);
				sound_player(sound_2,1);
				// nav_button_controls(4500);
				break;
			case 2:
				$prevBtn.show(0);
				sound_player(sound_3,1);
				// nav_button_controls(4500);
				break;
			case 3:
				$prevBtn.show(0);
				sound_player(sound_4,1);
				// nav_button_controls(4500);
				break;
			case 4:
				$prevBtn.show(0);
				sound_player(sound_5,1);
				// nav_button_controls(4500);
				break;
			case 5:
				$prevBtn.show(0);
				sound_player(sound_6,1);
				// nav_button_controls(4500);
				break;
			case 6:
				$prevBtn.show(0);
				sound_player(sound_7,1);
				// nav_button_controls(1000);
				break;
			default:
				$prevBtn.show(0);
				// nav_button_controls(4500);
				break;
		}
	}

	function nav_button_controls(delay_ms){
		myTimeout = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_data, next){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			next?nav_button_controls():'';
		});
	}


	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		clearTimeout(myTimeout);
		switch(countNext){
			default:
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		clearTimeout(myTimeout);
		current_sound.stop();
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

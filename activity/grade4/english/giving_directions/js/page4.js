var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/";

var sound_correct = new buzz.sound("sounds/common/correct.ogg");
var sound_incorrect = new buzz.sound(soundAsset + "beep.ogg");

var sound_0 = new buzz.sound(soundAsset + "s4_p1.ogg");
var sound_1 = new buzz.sound(soundAsset + "p4_s0.ogg");
var sound_2 = new buzz.sound(soundAsset + "p4_s1.ogg");
var sound_3 = new buzz.sound(soundAsset + "p4_s2.ogg");
var sound_4 = new buzz.sound(soundAsset + "p4_s3.ogg");
var sound_5 = new buzz.sound(soundAsset + "p4_s4.ogg");
var sound_6 = new buzz.sound(soundAsset + "p4_s5.ogg");
var sound_7 = new buzz.sound(soundAsset + "p4_s6.ogg");
var sound_8 = new buzz.sound(soundAsset + "p4_s7.ogg");
var sound_9 = new buzz.sound(soundAsset + "p4_s8.ogg");
var sound_10 = new buzz.sound(soundAsset + "p4_s9.ogg");
var sound_11 = new buzz.sound(soundAsset + "p4_s10.ogg");
var sound_12 = new buzz.sound(soundAsset + "p4_s11.ogg");
var current_sound = sound_1;

var content = [
  // slide 0
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "main_bg",

    uppertextblockadditionalclass: "blue_heading my_font_ultra_big luckiestguy",
    uppertextblock: [
      {
        textdata: data.lesson.chapter,
        textclass: ""
      }
    ],

    lowertextblockadditionalclass: "bottom-light-blue sniglet my_font_big",
    lowertextblock: [
      {
        textdata: data.string.p4text2,
        textclass: " ",
        datahighlightflag: true,
        datahighlightcustomclass: "ul"
      }
    ],
    lowertextblock2additionalclass: "yellow_banner my_font_big luckiestguy",
    lowertextblock2: [
      {
        textdata: data.string.p4text1,
        textclass: ""
      }
    ],
    extratextblock: [
      {
        textdata: data.string.p4left1,
        textclass: "house-text house-text-1"
      },
      {
        textdata: data.string.p4left2,
        textclass: "house-text house-text-2"
      },
      {
        textdata: data.string.p4left3,
        textclass: "house-text house-text-3"
      },
      {
        textdata: data.string.p4left4,
        textclass: "house-text house-text-4"
      },
      {
        textdata: data.string.p4right1,
        textclass: "house-text house-text-5"
      },
      {
        textdata: data.string.p4right2,
        textclass: "house-text house-text-6"
      },
      {
        textdata: data.string.p4right3,
        textclass: "house-text house-text-7"
      },
      {
        textdata: data.string.p4right4,
        textclass: "house-text house-text-8"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "house house-1",
            imgsrc: imgpath + "house05.png"
          },
          {
            imgclass: "house house-2",
            imgsrc: imgpath + "house06.png"
          },
          {
            imgclass: "house house-3",
            imgsrc: imgpath + "house07.png"
          },
          {
            imgclass: "house house-4",
            imgsrc: imgpath + "house08.png"
          },
          {
            imgclass: "house house-5",
            imgsrc: imgpath + "house09.png"
          },
          {
            imgclass: "house house-6",
            imgsrc: imgpath + "house10.png"
          },
          {
            imgclass: "house house-7",
            imgsrc: imgpath + "house11.png"
          },
          {
            imgclass: "house house-8",
            imgsrc: imgpath + "house12.png"
          },
          {
            imgclass: "car-l",
            imgsrc: imgpath + "car.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "road road-1",
            imagelabeldata: ""
          }
        ]
      }
    ]
  },
  // slide 1
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "main_bg",

    uppertextblockadditionalclass: "blue_heading my_font_ultra_big luckiestguy",
    uppertextblock: [
      {
        textdata: data.lesson.chapter,
        textclass: ""
      }
    ],

    lowertextblockadditionalclass: "bottom-light-blue sniglet my_font_big",
    lowertextblock: [
      {
        textdata: data.string.p4text2,
        textclass: " ",
        datahighlightflag: true,
        datahighlightcustomclass: "ul"
      }
    ],
    lowertextblock2additionalclass: "yellow_banner my_font_big luckiestguy",
    lowertextblock2: [
      {
        textdata: data.string.p4text1,
        textclass: ""
      }
    ],
    extratextblock: [
      {
        textdata: data.string.p4left1,
        textclass: "house-texta house-texta-3"
      },
      {
        textdata: data.string.p4left2,
        textclass: "house-texta house-texta-2"
      },
      {
        textdata: data.string.p4left3,
        textclass: "house-texta house-texta-1"
      },
      {
        textdata: data.string.p4right1,
        textclass: "house-texta house-texta-6"
      },
      {
        textdata: data.string.p4right2,
        textclass: "house-texta house-texta-5"
      },
      {
        textdata: data.string.p4right3,
        textclass: "house-texta house-texta-4"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "housea housea-1",
            imgsrc: imgpath + "house01.png"
          },
          {
            imgclass: "housea housea-2",
            imgsrc: imgpath + "house02.png"
          },
          {
            imgclass: "housea housea-3",
            imgsrc: imgpath + "house04.png"
          },
          {
            imgclass: "housea housea-4",
            imgsrc: imgpath + "house03.png"
          },
          {
            imgclass: "housea housea-5",
            imgsrc: imgpath + "house04.png"
          },
          {
            imgclass: "housea housea-6",
            imgsrc: imgpath + "house02.png"
          },
          {
            imgclass: "car-u",
            imgsrc: imgpath + "car.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "road road-2",
            imagelabeldata: ""
          }
        ]
      }
    ]
  },
  // slide 2
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "main_bg",

    uppertextblockadditionalclass: "blue_heading my_font_ultra_big luckiestguy",
    uppertextblock: [
      {
        textdata: data.lesson.chapter,
        textclass: ""
      }
    ],

    lowertextblockadditionalclass: "bottom-light-blue sniglet my_font_big",
    lowertextblock: [
      {
        textdata: data.string.p4text2,
        textclass: " ",
        datahighlightflag: true,
        datahighlightcustomclass: "ul"
      }
    ],
    lowertextblock2additionalclass: "yellow_banner my_font_big luckiestguy",
    lowertextblock2: [
      {
        textdata: data.string.p4text1,
        textclass: ""
      }
    ],
    extratextblock: [
      {
        textdata: data.string.p4left1,
        textclass: "house-text house-text-8"
      },
      {
        textdata: data.string.p4left2,
        textclass: "house-text house-text-7"
      },
      {
        textdata: data.string.p4left3,
        textclass: "house-text house-text-6"
      },
      {
        textdata: data.string.p4left4,
        textclass: "house-text house-text-5"
      },
      {
        textdata: data.string.p4right1,
        textclass: "house-text house-text-4"
      },
      {
        textdata: data.string.p4right2,
        textclass: "house-text house-text-3"
      },
      {
        textdata: data.string.p4right3,
        textclass: "house-text house-text-2"
      },
      {
        textdata: data.string.p4right4,
        textclass: "house-text house-text-1"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "house house-1",
            imgsrc: imgpath + "house05.png"
          },
          {
            imgclass: "house house-2",
            imgsrc: imgpath + "house06.png"
          },
          {
            imgclass: "house house-3",
            imgsrc: imgpath + "house07.png"
          },
          {
            imgclass: "house house-4",
            imgsrc: imgpath + "house08.png"
          },
          {
            imgclass: "house house-5",
            imgsrc: imgpath + "house09.png"
          },
          {
            imgclass: "house house-6",
            imgsrc: imgpath + "house10.png"
          },
          {
            imgclass: "house house-7",
            imgsrc: imgpath + "house11.png"
          },
          {
            imgclass: "house house-8",
            imgsrc: imgpath + "house12.png"
          },
          {
            imgclass: "car-r",
            imgsrc: imgpath + "car.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "road road-1",
            imagelabeldata: ""
          }
        ]
      }
    ]
  },
  // slide 3
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "main_bg",

    uppertextblockadditionalclass: "blue_heading my_font_ultra_big luckiestguy",
    uppertextblock: [
      {
        textdata: data.lesson.chapter,
        textclass: ""
      }
    ],

    lowertextblockadditionalclass: "bottom-light-blue sniglet my_font_big",
    lowertextblock: [
      {
        textdata: data.string.p4text2,
        textclass: " ",
        datahighlightflag: true,
        datahighlightcustomclass: "ul"
      }
    ],
    lowertextblock2additionalclass: "yellow_banner my_font_big luckiestguy",
    lowertextblock2: [
      {
        textdata: data.string.p4text1,
        textclass: ""
      }
    ],
    extratextblock: [
      {
        textdata: data.string.p4left1,
        textclass: "house-texta house-texta-4"
      },
      {
        textdata: data.string.p4left2,
        textclass: "house-texta house-texta-5"
      },
      {
        textdata: data.string.p4left3,
        textclass: "house-texta house-texta-6"
      },
      {
        textdata: data.string.p4right1,
        textclass: "house-texta house-texta-1"
      },
      {
        textdata: data.string.p4right2,
        textclass: "house-texta house-texta-2"
      },
      {
        textdata: data.string.p4right3,
        textclass: "house-texta house-texta-3"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "housea housea-1",
            imgsrc: imgpath + "house01.png"
          },
          {
            imgclass: "housea housea-2",
            imgsrc: imgpath + "house02.png"
          },
          {
            imgclass: "housea housea-3",
            imgsrc: imgpath + "house04.png"
          },
          {
            imgclass: "housea housea-4",
            imgsrc: imgpath + "house03.png"
          },
          {
            imgclass: "housea housea-5",
            imgsrc: imgpath + "house04.png"
          },
          {
            imgclass: "housea housea-6",
            imgsrc: imgpath + "house02.png"
          },
          {
            imgclass: "car-d",
            imgsrc: imgpath + "car.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "road road-2",
            imagelabeldata: ""
          }
        ]
      }
    ]
  },
  // slide 4
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "main_bg",

    uppertextblockadditionalclass: "blue_heading my_font_ultra_big luckiestguy",
    uppertextblock: [
      {
        textdata: data.lesson.chapter,
        textclass: ""
      }
    ],

    lowertextblockadditionalclass: "bottom-light-blue sniglet my_font_big",
    lowertextblock: [
      {
        textdata: data.string.p4text3,
        textclass: " ",
        datahighlightflag: true,
        datahighlightcustomclass: "ul"
      }
    ],
    lowertextblock2additionalclass: "yellow_banner my_font_big luckiestguy",
    lowertextblock2: [
      {
        textdata: data.string.p4text1,
        textclass: ""
      }
    ],
    extratextblock: [
      {
        textdata: data.string.p4text4,
        textclass: "house-text house-text-3 not-hidden"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "house house-1",
            imgsrc: imgpath + "house05.png"
          },
          {
            imgclass: "house house-2",
            imgsrc: imgpath + "house06.png"
          },
          {
            imgclass: "house house-3",
            imgsrc: imgpath + "house07.png"
          },
          {
            imgclass: "house house-4",
            imgsrc: imgpath + "house08.png"
          },
          {
            imgclass: "house house-5",
            imgsrc: imgpath + "house09.png"
          },
          {
            imgclass: "house house-6",
            imgsrc: imgpath + "house10.png"
          },
          {
            imgclass: "house house-7",
            imgsrc: imgpath + "house11.png"
          },
          {
            imgclass: "house house-8",
            imgsrc: imgpath + "house12.png"
          },
          {
            imgclass: "car-l",
            imgsrc: imgpath + "car.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "road road-1",
            imagelabeldata: ""
          }
        ]
      }
    ]
  },

  // slide 5
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "main_bg",

    uppertextblockadditionalclass: "blue_heading my_font_ultra_big luckiestguy",
    uppertextblock: [
      {
        textdata: data.lesson.chapter,
        textclass: ""
      }
    ],

    lowertextblockadditionalclass: "bottom-light-blue sniglet my_font_big",
    lowertextblock: [
      {
        textdata: data.string.p4text5,
        textclass: " ",
        datahighlightflag: true,
        datahighlightcustomclass: "ul"
      }
    ],
    lowertextblock2additionalclass: "yellow_banner my_font_big luckiestguy",
    lowertextblock2: [
      {
        textdata: data.string.p4text1,
        textclass: ""
      }
    ],
    extratextblock: [
      {
        textdata: data.string.p4text4,
        textclass: "house-text house-text-3 not-hidden"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "house house-1",
            imgsrc: imgpath + "house05.png"
          },
          {
            imgclass: "house house-2",
            imgsrc: imgpath + "house06.png"
          },
          {
            imgclass: "house house-3",
            imgsrc: imgpath + "house07.png"
          },
          {
            imgclass: "house house-4",
            imgsrc: imgpath + "house08.png"
          },
          {
            imgclass: "house house-5",
            imgsrc: imgpath + "house09.png"
          },
          {
            imgclass: "house house-6",
            imgsrc: imgpath + "house10.png"
          },
          {
            imgclass: "house house-7",
            imgsrc: imgpath + "house11.png"
          },
          {
            imgclass: "house house-8",
            imgsrc: imgpath + "house12.png"
          },
          {
            imgclass: "car-l",
            imgsrc: imgpath + "car.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "road road-1",
            imagelabeldata: ""
          }
        ]
      }
    ]
  },
  //slide6
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "light-green",

    extratextblock: [
      {
        textdata: data.string.p4text6,
        textclass: "sundar-t-1 luckiestguy my_font_super_big"
      },
      {
        textdata: data.string.p4text7,
        textclass: "sundar-t-2 luckiestguy my_font_super_big"
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "sundar-1",
            imgsrc: "images/sundar/bottom.png"
          }
        ]
      }
    ]
  },
  // slide 7
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "main_bg",

    lowertextblockadditionalclass: "top-light-blue sniglet my_font_big",
    lowertextblock: [
      {
        textdata: data.string.p4text8,
        textclass: " ",
        datahighlightflag: true,
        datahighlightcustomclass: "ul"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "house-b house-b-1",
            imgsrc: imgpath + "house05.png"
          },
          {
            imgclass: "house-b house-b-2",
            imgsrc: imgpath + "house06.png"
          },
          {
            imgclass: "house-b house-b-3 correct-house",
            imgsrc: imgpath + "house07.png"
          },
          {
            imgclass: "house-b house-b-4",
            imgsrc: imgpath + "house08.png"
          },
          {
            imgclass: "house-b house-b-5",
            imgsrc: imgpath + "house06.png"
          },
          {
            imgclass: "house-b house-b-6",
            imgsrc: imgpath + "house05.png"
          },
          {
            imgclass: "house-b house-b-7",
            imgsrc: imgpath + "house11.png"
          },
          {
            imgclass: "house-b house-b-8",
            imgsrc: imgpath + "house12.png"
          },
          {
            imgclass: "house-b house-b-9",
            imgsrc: imgpath + "house09.png"
          },
          {
            imgclass: "house-b house-b-10",
            imgsrc: imgpath + "house10.png"
          },
          {
            imgclass: "house-b house-b-11",
            imgsrc: imgpath + "house12.png"
          },
          {
            imgclass: "house-b house-b-12",
            imgsrc: imgpath + "house09.png"
          },
          {
            imgclass: "car-l1",
            imgsrc: imgpath + "car.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "road road-1a",
            imagelabeldata: ""
          }
        ]
      }
    ]
  },

  // slide 8
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "main_bg",

    lowertextblockadditionalclass: "top-light-blue sniglet my_font_big",
    lowertextblock: [
      {
        textdata: data.string.p4text9,
        textclass: " ",
        datahighlightflag: true,
        datahighlightcustomclass: "ul"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "house-c house-c-1",
            imgsrc: imgpath + "house08.png"
          },
          {
            imgclass: "house-c house-c-2",
            imgsrc: imgpath + "house06.png"
          },
          {
            imgclass: "house-c house-c-3",
            imgsrc: imgpath + "house07.png"
          },
          {
            imgclass: "house-c house-c-4 correct-house",
            imgsrc: imgpath + "house05.png"
          },
          {
            imgclass: "house-c house-c-5",
            imgsrc: imgpath + "house03.png"
          },
          {
            imgclass: "house-c house-c-6",
            imgsrc: imgpath + "house05.png"
          },
          {
            imgclass: "house-c house-c-7",
            imgsrc: imgpath + "house07.png"
          },
          {
            imgclass: "house-c house-c-8",
            imgsrc: imgpath + "house06.png"
          },
          {
            imgclass: "car-u1",
            imgsrc: imgpath + "car.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "road road-2",
            imagelabeldata: ""
          }
        ]
      }
    ]
  },

  // slide 9
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "main_bg",

    lowertextblockadditionalclass: "top-light-blue sniglet my_font_big",
    lowertextblock: [
      {
        textdata: data.string.p4text10,
        textclass: " ",
        datahighlightflag: true,
        datahighlightcustomclass: "ul"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "house-b house-b-1",
            imgsrc: imgpath + "house05.png"
          },
          {
            imgclass: "house-b house-b-2",
            imgsrc: imgpath + "house06.png"
          },
          {
            imgclass: "house-b house-b-3",
            imgsrc: imgpath + "house07.png"
          },
          {
            imgclass: "house-b house-b-4",
            imgsrc: imgpath + "house08.png"
          },
          {
            imgclass: "house-b house-b-5",
            imgsrc: imgpath + "house06.png"
          },
          {
            imgclass: "house-b house-b-6",
            imgsrc: imgpath + "house05.png"
          },
          {
            imgclass: "house-b house-b-7",
            imgsrc: imgpath + "house11.png"
          },
          {
            imgclass: "house-b house-b-8",
            imgsrc: imgpath + "house12.png"
          },
          {
            imgclass: "house-b house-b-9",
            imgsrc: imgpath + "house09.png"
          },
          {
            imgclass: "house-b house-b-10",
            imgsrc: imgpath + "house10.png"
          },
          {
            imgclass: "house-b house-b-11 correct-house",
            imgsrc: imgpath + "house12.png"
          },
          {
            imgclass: "house-b house-b-12",
            imgsrc: imgpath + "house09.png"
          },
          {
            imgclass: "car-l1",
            imgsrc: imgpath + "car.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "road road-3",
            imagelabeldata: ""
          }
        ]
      }
    ]
  },
  // slide 10
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "main_bg",

    lowertextblockadditionalclass: "top-light-blue sniglet my_font_big",
    lowertextblock: [
      {
        textdata: data.string.p4text11,
        textclass: " ",
        datahighlightflag: true,
        datahighlightcustomclass: "ul"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "house-c house-c-1",
            imgsrc: imgpath + "house08.png"
          },
          {
            imgclass: "house-c house-c-2",
            imgsrc: imgpath + "house06.png"
          },
          {
            imgclass: "house-c house-c-3",
            imgsrc: imgpath + "house07.png"
          },
          {
            imgclass: "house-c house-c-4",
            imgsrc: imgpath + "house05.png"
          },
          {
            imgclass: "house-c house-c-5",
            imgsrc: imgpath + "house03.png"
          },
          {
            imgclass: "house-c house-c-6 correct-house",
            imgsrc: imgpath + "house05.png"
          },
          {
            imgclass: "house-c house-c-7",
            imgsrc: imgpath + "house07.png"
          },
          {
            imgclass: "house-c house-c-8",
            imgsrc: imgpath + "house06.png"
          },
          {
            imgclass: "car-u1",
            imgsrc: imgpath + "car.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "road road-2",
            imagelabeldata: ""
          }
        ]
      }
    ]
  },

  // slide 11
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "main_bg",

    lowertextblockadditionalclass: "top-light-blue sniglet my_font_big",
    lowertextblock: [
      {
        textdata: data.string.p4text13,
        textclass: " ",
        datahighlightflag: true,
        datahighlightcustomclass: "ul"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "house-b house-b-1",
            imgsrc: imgpath + "house05.png"
          },
          {
            imgclass: "house-b house-b-2",
            imgsrc: imgpath + "house06.png"
          },
          {
            imgclass: "house-b house-b-3",
            imgsrc: imgpath + "house07.png"
          },
          {
            imgclass: "house-b house-b-4 correct-house",
            imgsrc: imgpath + "house08.png"
          },
          {
            imgclass: "house-b house-b-5",
            imgsrc: imgpath + "house06.png"
          },
          {
            imgclass: "house-b house-b-6",
            imgsrc: imgpath + "house05.png"
          },
          {
            imgclass: "house-b house-b-7",
            imgsrc: imgpath + "house11.png"
          },
          {
            imgclass: "house-b house-b-8",
            imgsrc: imgpath + "house12.png"
          },
          {
            imgclass: "house-b house-b-9",
            imgsrc: imgpath + "house09.png"
          },
          {
            imgclass: "house-b house-b-10",
            imgsrc: imgpath + "house10.png"
          },
          {
            imgclass: "house-b house-b-11",
            imgsrc: imgpath + "house12.png"
          },
          {
            imgclass: "house-b house-b-12",
            imgsrc: imgpath + "house09.png"
          },
          {
            imgclass: "car-r1",
            imgsrc: imgpath + "car.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "road road-3",
            imagelabeldata: ""
          }
        ]
      }
    ]
  },
  // slide 12
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "main_bg",

    lowertextblockadditionalclass: "top-light-blue sniglet my_font_big",
    lowertextblock: [
      {
        textdata: data.string.p4text9,
        textclass: " ",
        datahighlightflag: true,
        datahighlightcustomclass: "ul"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "house-c house-c-1",
            imgsrc: imgpath + "house08.png"
          },
          {
            imgclass: "house-c house-c-2",
            imgsrc: imgpath + "house06.png"
          },
          {
            imgclass: "house-c house-c-3",
            imgsrc: imgpath + "house07.png"
          },
          {
            imgclass: "house-c house-c-4",
            imgsrc: imgpath + "house05.png"
          },
          {
            imgclass: "house-c house-c-5 correct-house",
            imgsrc: imgpath + "house03.png"
          },
          {
            imgclass: "house-c house-c-6",
            imgsrc: imgpath + "house05.png"
          },
          {
            imgclass: "house-c house-c-7",
            imgsrc: imgpath + "house07.png"
          },
          {
            imgclass: "house-c house-c-8",
            imgsrc: imgpath + "house06.png"
          },
          {
            imgclass: "car-d1",
            imgsrc: imgpath + "car.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "road road-2",
            imagelabeldata: ""
          }
        ]
      }
    ]
  },
  // slide 13
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "main_bg",

    lowertextblockadditionalclass: "top-light-blue sniglet my_font_big",
    lowertextblock: [
      {
        textdata: data.string.p4text14,
        textclass: " ",
        datahighlightflag: true,
        datahighlightcustomclass: "ul"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "house-b house-b-1",
            imgsrc: imgpath + "house05.png"
          },
          {
            imgclass: "house-b house-b-2",
            imgsrc: imgpath + "house06.png"
          },
          {
            imgclass: "house-b house-b-3",
            imgsrc: imgpath + "house07.png"
          },
          {
            imgclass: "house-b house-b-4",
            imgsrc: imgpath + "house08.png"
          },
          {
            imgclass: "house-b house-b-5 correct-house",
            imgsrc: imgpath + "house06.png"
          },
          {
            imgclass: "house-b house-b-6",
            imgsrc: imgpath + "house05.png"
          },
          {
            imgclass: "house-b house-b-7",
            imgsrc: imgpath + "house11.png"
          },
          {
            imgclass: "house-b house-b-8",
            imgsrc: imgpath + "house12.png"
          },
          {
            imgclass: "house-b house-b-9",
            imgsrc: imgpath + "house09.png"
          },
          {
            imgclass: "house-b house-b-10",
            imgsrc: imgpath + "house10.png"
          },
          {
            imgclass: "house-b house-b-11",
            imgsrc: imgpath + "house12.png"
          },
          {
            imgclass: "house-b house-b-12",
            imgsrc: imgpath + "house09.png"
          },
          {
            imgclass: "car-r1",
            imgsrc: imgpath + "car.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "road road-3",
            imagelabeldata: ""
          }
        ]
      }
    ]
  },
  //slide14
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "light-green",

    extratextblock: [
      {
        textdata: data.string.p4text15,
        textclass: "great-job luckiestguy my_font_super_big"
      },
      {
        textdata: data.string.p4text16,
        textclass: "move-on sniglet my_font_ultra_big"
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "sundar",
            imgsrc: "images/sundar/correct-1.png"
          }
        ]
      }
    ]
  }
];

$(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;
  var $total_page = content.length;

  var current_sound = sound_correct;
  var myTimeout = null;
  var timeouts = [];

  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();
  /*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
  Handlebars.registerPartial(
    "definitioncontent",
    $("#definitioncontent-partial").html()
  );
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

  // controls the navigational state of the program
  // next btn is disabled for this page
  function navigationController(islastpageflag) {
    typeof islastpageflag === "undefined"
      ? (islastpageflag = false)
      : typeof islastpageflag != "boolean"
      ? alert(
          "NavigationController : Hi Master, please provide a boolean parameter"
        )
      : null;
    // if lastpageflag is true
    // islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
  }

  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    is_last_page = false;

    $board.html(html);
    loadTimelineProgress($total_page, countNext + 1);
    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    vocabcontroller.findwords(countNext);

    switch (countNext) {
      case 0:
        current_sound = sound_0;
        current_sound.play();
        current_sound.bindOnce("ended", function() {
          current_sound.stop();
          current_sound = sound_1;
          current_sound.play();
          current_sound.bindOnce("ended", function() {
            $(".car-l").animate({ left: "21%" }, 750, function() {
              $(".house-text-1, .house-text-5").fadeIn(500, function() {});
            });
            timeouts.push(
              setTimeout(function() {
                $(".car-l").animate({ left: "39%" }, 750, function() {
                  $(".house-text-2, .house-text-6").fadeIn(500, function() {});
                });
              }, 1500)
            );
            timeouts.push(
              setTimeout(function() {
                $(".car-l").animate({ left: "58%" }, 750, function() {
                  $(".house-text-3, .house-text-7").fadeIn(500, function() {});
                });
              }, 3000)
            );
            timeouts.push(
              setTimeout(function() {
                $(".car-l").animate({ left: "76%" }, 750, function() {
                  $(".house-text-4, .house-text-8").fadeIn(500, function() {
                    nav_button_controls(0);
                    $(".car-l").animate({ left: "90%" }, 200);
                  });
                });
              }, 4500)
            );
          });
        });
        break;
      case 1:
        // sound_player(sound_1);
        $(".car-u").animate({ bottom: "34%" }, 750, function() {
          $(".house-texta-3, .house-texta-6").fadeIn(500, function() {});
        });
        timeouts.push(
          setTimeout(function() {
            $(".car-u").animate({ bottom: "49%" }, 750, function() {
              $(".house-texta-2, .house-texta-5").fadeIn(500, function() {});
            });
          }, 1500)
        );
        timeouts.push(
          setTimeout(function() {
            $(".car-u").animate({ bottom: "65%" }, 750, function() {
              $(".house-texta-1, .house-texta-4").fadeIn(500, function() {
                nav_button_controls(0);
                $(".car-u").animate({ bottom: "75%" }, 200);
              });
            });
          }, 3000)
        );
        break;
      case 2:
        // sound_player(sound_1);
        $(".car-r").animate({ left: "76%" }, 750, function() {
          $(".house-text-4, .house-text-8").fadeIn(500, function() {});
        });
        timeouts.push(
          setTimeout(function() {
            $(".car-r").animate({ left: "58%" }, 750, function() {
              $(".house-text-3, .house-text-7").fadeIn(500, function() {});
            });
          }, 1500)
        );
        timeouts.push(
          setTimeout(function() {
            $(".car-r").animate({ left: "39%" }, 750, function() {
              $(".house-text-2, .house-text-6").fadeIn(500, function() {});
            });
          }, 3000)
        );
        timeouts.push(
          setTimeout(function() {
            $(".car-r").animate({ left: "21%" }, 750, function() {
              $(".house-text-1, .house-text-5").fadeIn(500, function() {
                nav_button_controls(0);
                $(".car-r").animate({ left: "5%" }, 200);
              });
            });
          }, 4500)
        );
        break;
      case 3:
        // sound_player(sound_1);
        $(".car-d").animate({ bottom: "65%" }, 750, function() {
          $(".house-texta-1, .house-texta-4").fadeIn(500, function() {});
        });
        timeouts.push(
          setTimeout(function() {
            $(".car-d").animate({ bottom: "49%" }, 750, function() {
              $(".house-texta-2, .house-texta-5").fadeIn(500, function() {});
            });
          }, 1500)
        );
        timeouts.push(
          setTimeout(function() {
            $(".car-d").animate({ bottom: "34%" }, 750, function() {
              $(".house-texta-3, .house-texta-6").fadeIn(500, function() {
                nav_button_controls(0);
                $(".car-d").animate({ bottom: "22%" }, 200);
              });
            });
          }, 3000)
        );
        break;
      case 4:
        $prevBtn.show(0);
        sound_player(sound_2);
        nav_button_controls(2000);
        break;
      case 5:
        $prevBtn.show(0);
        sound_player(sound_3);
        nav_button_controls(3000);
        break;
      case 6:
        $prevBtn.show(0);
        sound_player(sound_4);
        nav_button_controls(3000);
        break;
      case 7:
      case 9:
      case 11:
      case 13:
        $prevBtn.show(0);
        switch (countNext) {
          case 7:
            sound_player(sound_5);
            break;
          case 9:
            sound_player(sound_7);
            break;
          case 11:
            sound_player(sound_9);
            break;
          case 13:
            sound_player(sound_11);
            break;
        }
        $(".house-b").click(function() {
          if ($(this).hasClass("correct-house")) {
            play_correct_incorrect_sound(1);
            nav_button_controls(0);
            $(".house-b").css({ "pointer-events": "none" });
            $(this).css({ border: "4px solid #0f0" });
          } else {
            play_correct_incorrect_sound(0);
            $(this).css({ border: "4px solid #f00" });
          }
        });
        break;
      case 8:
      case 10:
      case 12:
        switch (countNext) {
          case 8:
            sound_player(sound_6);
            break;
          case 10:
            sound_player(sound_8);
            break;
          case 12:
            sound_player(sound_10);
            break;
        }
        $prevBtn.show(0);
        $(".house-c").click(function() {
          if ($(this).hasClass("correct-house")) {
            play_correct_incorrect_sound(1);
            nav_button_controls(0);
            $(".house-c").css("pointer-events", "none");
            $(this).css({ border: "4px solid #0f0" });
          } else {
            play_correct_incorrect_sound(0);
            $(this).css({ border: "4px solid #f00" });
          }
        });
        break;
      case 13:
        $prevBtn.show(0);
        nav_button_controls(500);
        break;
      default:
        $prevBtn.show(0);
        sound_player(sound_12);
        nav_button_controls(500);
        break;
    }
  }

  function nav_button_controls(delay_ms) {
    myTimeout = setTimeout(function() {
      if (countNext == 0) {
        $nextBtn.show(0);
      } else if (countNext > 0 && countNext == $total_page - 1) {
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else {
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    }, delay_ms);
  }

  function sound_player(sound_data) {
    current_sound.stop();
    current_sound = sound_data;
    current_sound.play();
  }

  function templateCaller() {
    //convention is to always hide the prev and next button and show them based
    //on the convention or page index
    $prevBtn.hide(0);
    $nextBtn.hide(0);
    navigationController();

    generalTemplate();
    /*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
  }

  $nextBtn.on("click", function() {
    clearTimeout(myTimeout);
    for (var i = 0; i < timeouts.length; i++) {
      clearTimeout(timeouts[i]);
    }
    switch (countNext) {
      default:
        current_sound.stop();
        countNext++;
        templateCaller();
        break;
    }
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    for (var i = 0; i < timeouts.length; i++) {
      clearTimeout(timeouts[i]);
    }
    clearTimeout(myTimeout);
    current_sound.stop();
    countNext--;
    templateCaller();
  });

  total_page = content.length;
  templateCaller();
});

/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
  //check if $highlightinside is provided
  typeof $highlightinside !== "object"
    ? alert(
        "Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted"
      )
    : null;

  var $alltextpara = $highlightinside.find("*[data-highlight='true']");
  var stylerulename;
  var replaceinstring;
  var texthighlightstarttag;
  var texthighlightendtag = "</span>";

  if ($alltextpara.length > 0) {
    $.each($alltextpara, function(index, val) {
      /*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
      $(this).attr(
        "data-highlightcustomclass"
      ) /*if there is data-highlightcustomclass defined it is true else it is not*/
        ? (stylerulename = $(this).attr("data-highlightcustomclass"))
        : (stylerulename = "parsedstring");

      texthighlightstarttag = "<span class = " + stylerulename + " >";

      replaceinstring = $(this).html();
      replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
      replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

      $(this).html(replaceinstring);
    });
  }
}

/*=====  End of data highlight function  ======*/

var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide 1
	{
	    // instruction: data.string.exques1,
			contentblockadditionalclass : "greenbg",
			exetype1:[{
	        datahighlightflag: true,
	        datahighlightcustomclass: "clickplace",
	        exeoptions:[
	          {
	            // optaddclass: "correct",
	          },
	          {
	          },{
	          },{
	          }
	        ]
				}],
				imageblock:[{
					imagestoshow:[
						{
							imgclass: "car1",
							imgid : 'car',
							imgsrc: ""
						},{
							imgclass: "low_img",
							imgid : 'f_page',
							imgsrc: ""
						}
					]
				}]
		},
	// slide 2
	{
	    // instruction: data.string.exques1,
			contentblockadditionalclass : "greenbg",
			exetype1:[{
	        datahighlightflag: true,
	        datahighlightcustomclass: "clickplace",
	        exeoptions:[
	          {
	            // optaddclass: "correct",
	          },
	          {
	          },{
	          },{
	          }
	        ]
				}],
				imageblock:[{
					imagestoshow:[
						{
							imgclass: "car1",
							imgid : 'car',
							imgsrc: ""
						},{
							imgclass: "low_img",
							imgid : 'f_page',
							imgsrc: ""
						}
					]
				}]
		},
	// slide 3
	{
	    // instruction: data.string.exques1,
			contentblockadditionalclass : "greenbg",
			exetype2:[{
	        datahighlightflag: true,
	        datahighlightcustomclass: "clickplace",
	        exeoptions:[
	          {
	            // optaddclass: "correct",
	          },
	          {
	          },{
	          }
	        ]
				}],
				imageblock:[{
					imagestoshow:[
						{
							imgclass: "car_2",
							imgid : 'car',
							imgsrc: ""
						},{
							imgclass: "side_img",
							imgid : 's_page',
							imgsrc: ""
						}
					]
				}]
		},
	// slide 4
	{
		contentblockadditionalclass : "greenbg",
		exetype3:[{
        datahighlightflag: true,
        datahighlightcustomclass: "clickplace",
        exeoptions:[
          {
          },
          {
          },{
          },{
          }
        ]
			}],
			imageblock:[{
				imagestoshow:[
					{
						imgclass: "car1_sec",
						imgid : 'car',
						imgsrc: ""
					},{
						imgclass: "low_img",
						imgid : 'bg_wide',
						imgsrc: ""
					},{
						imgclass: "opt_img_1 up op1",
						imgid : 'house_red_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img_1 up op2",
						imgid : 'pond_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img_1 up op3",
						imgid : 'tree_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img_1 up op4",
						imgid : 'house_blue_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img_1 up op5",
						imgid : 'house_2str_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img_1 dwn op6",
						imgid : 'house_red_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img_1 dwn op7",
						imgid : 'tree_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img_1 dwn op8",
						imgid : 'house_2str_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img_1 dwn op9",
						imgid : 'tree_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img_1 dwn op10",
						imgid : 'pond_bg',
						imgsrc: ""
					}
				]
			}]
		},
	// slide 5
	{
		// instruction: data.string.exques1,
		contentblockadditionalclass : "greenbg",
		exetype3:[{
				datahighlightflag: true,
				datahighlightcustomclass: "clickplace",
				exeoptions:[
					{
						// optaddclass: "correct",
					},
					{
					},{
					},{
					}
				]
			}],
			imageblock:[{
				imagestoshow:[
					{
						imgclass: "car1_sec",
						imgid : 'car',
						imgsrc: ""
					},{
						imgclass: "low_img",
						imgid : 'bg_wide',
						imgsrc: ""
					},{
						imgclass: "opt_img_1 up op1",
						imgid : 'house_red_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img_1 up op2",
						imgid : 'pond_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img_1 up op3",
						imgid : 'tree_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img_1 up op4",
						imgid : 'house_blue_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img_1 up op5",
						imgid : 'house_2str_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img_1 dwn op6",
						imgid : 'house_red_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img_1 dwn op7",
						imgid : 'tree_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img_1 dwn op8",
						imgid : 'house_2str_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img_1 dwn op9",
						imgid : 'tree_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img_1 dwn op10",
						imgid : 'pond_bg',
						imgsrc: ""
					}
				]
			}]
	},
	// slide 6
	{
	    // instruction: data.string.exques1,
			contentblockadditionalclass : "greenbg",
			exetype4:[{
	        datahighlightflag: true,
	        datahighlightcustomclass: "clickplace",
	        exeoptions:[
	          {
	            // optaddclass: "correct",
	          },
	          {
	          },{
	          },{
	          }
	        ]
			}],
			imageblock:[{
				imagestoshow:[
						{
							imgclass: "car_2",
							imgid : 'car',
							imgsrc: ""
						},{
						imgclass: "side_img",
						imgid : 'fth_page',
						imgsrc: ""
					},{
						imgclass: "opt_img_2 lf ops1",
						imgid : 'house_5',
						imgsrc: ""
					},{
						imgclass: "opt_img_2 lf ops2",
						imgid : 'house_7',
						imgsrc: ""
					},{
						imgclass: "opt_img_2 lf ops3",
						imgid : 'tree',
						imgsrc: ""
					},{
						imgclass: "opt_img_2 lf ops4",
						imgid : 'fountain',
						imgsrc: ""
					},{
						imgclass: "opt_img_2 rt ops5",
						imgid : 'house_2',
						imgsrc: ""
					},{
						imgclass: "opt_img_2 rt ops6",
						imgid : 'pond',
						imgsrc: ""
					},{
						imgclass: "opt_img_2 rt ops7",
						imgid : 'pond',
						imgsrc: ""
					},{
						imgclass: "opt_img_2 rt ops8",
						imgid : 'tree',
						imgsrc: ""
					}
				]
			}]
		},
	// slide 7
	{
	    // instruction: data.string.exques1,
			contentblockadditionalclass : "greenbg",
			exetype4:[{
	        datahighlightflag: true,
	        datahighlightcustomclass: "clickplace",
	        exeoptions:[
	          {
	            // optaddclass: "correct",
	          },
	          {
	          },{
	          },{
	          }
	        ]
			}],
			imageblock:[{
				imagestoshow:[
						{
							imgclass: "car_2",
							imgid : 'car',
							imgsrc: ""
						},{
						imgclass: "side_img",
						imgid : 'fth_page',
						imgsrc: ""
					},{
						imgclass: "opt_img_2 lf ops1",
						imgid : 'house_5',
						imgsrc: ""
					},{
						imgclass: "opt_img_2 lf ops2",
						imgid : 'house_7',
						imgsrc: ""
					},{
						imgclass: "opt_img_2 lf ops3",
						imgid : 'tree',
						imgsrc: ""
					},{
						imgclass: "opt_img_2 lf ops4",
						imgid : 'fountain',
						imgsrc: ""
					},{
						imgclass: "opt_img_2 rt ops5",
						imgid : 'house_2',
						imgsrc: ""
					},{
						imgclass: "opt_img_2 rt ops6",
						imgid : 'pond',
						imgsrc: ""
					},{
						imgclass: "opt_img_2 rt ops7",
						imgid : 'pond',
						imgsrc: ""
					},{
						imgclass: "opt_img_2 rt ops8",
						imgid : 'tree',
						imgsrc: ""
					}
				]
			}]
		},
	// slide 8
	{
		// instruction: data.string.exques1,
		contentblockadditionalclass : "greenbg",
		exetype5:[{
				datahighlightflag: true,
				datahighlightcustomclass: "clickplace",
				exeoptions:[
					{optaddclass: "droppable fhl_drp",},
					{optaddclass: "droppable shl_drp", },
					{optaddclass: "droppable thl_drp", },
					{optaddclass: "droppable fthl_drp", },
					{optaddclass: "droppable fhr_drp", },
					{optaddclass: "droppable shr_drp",  },
					{optaddclass: "droppable thr_drp",  },
					{optaddclass: "droppable fthr_drp",  }
				],
				exeoptions_sec:[
					{optaddclass:"draggable"},
				]
			}],
			imageblock:[{
				imagestoshow:[
					{
						imgclass: "car_5",
						imgid : 'car',
						imgsrc: ""
					},{
						imgclass: "low_img",
						imgid : 'bg_wide',
						imgsrc: ""
					},{
						imgclass: "opt_img_sec up op1_sec",
						imgid : 'house_red_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img_sec up op2_sec",
						imgid : 'house_2str_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img_sec up op3_sec",
						imgid : 'house_red_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img_sec up op4_sec",
						imgid : 'house_blue_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img_sec dwn op6_sec",
						imgid : 'house_blue_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img_sec dwn op7_sec",
						imgid : 'house_red_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img_sec dwn op8_sec",
						imgid : 'house_blue_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img_sec dwn op9_sec",
						imgid : 'house_2str_bg',
						imgsrc: ""
					}
				]
			}]
	},
	// slide 9
	{
		// instruction: data.string.exques1,
		contentblockadditionalclass : "greenbg",
			exetypeextra:[{}],
			imageblock:[{
				imagestoshow:[
					{
						imgclass: "car_6",
						imgid : 'car',
						imgsrc: ""
					},{
						imgclass: "low_img",
						imgid : 'bg_wide',
						imgsrc: ""
					},{
						imgclass: "opt_img up op1",
						imgid : 'house_red_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img up op2",
						imgid : 'pond_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img up op3",
						imgid : 'tree_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img up op4",
						imgid : 'pond_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img up op5",
						imgid : 'house_blue_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img dwn op6",
						imgid : 'house_2str_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img dwn op7",
						imgid : 'tree_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img dwn op8",
						imgid : 'fountain_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img dwn op9",
						imgid : 'house_red_bg',
						imgsrc: ""
					},{
						imgclass: "opt_img dwn op10",
						imgid : 'fountain_bg',
						imgsrc: ""
					}
				]
			}]
	},
	// slide 10
	{
		contentblockadditionalclass : "greenbg",
		exetypeextra_sec:[{}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "car_2",
					imgid : 'car',
					imgsrc: ""
				},{
				imgclass: "side_img_sec",
				imgid : 'bg_tall',
				imgsrc: ""
				},{
					imgclass: "opt_img lf ops1",
					imgid : 'house_blue_bg',
					imgsrc: ""
				},{
					imgclass: "opt_img lf ops2",
					imgid : 'house_red_bg',
					imgsrc: ""
				},{
					imgclass: "opt_img lf ops3",
					imgid : 'tree_bg',
					imgsrc: ""
				},{
					imgclass: "opt_img lf ops4",
					imgid : 'pond_bg',
					imgsrc: ""
				},{
					imgclass: "opt_img rt ops5",
					imgid : 'house_2str_bg',
					imgsrc: ""
				},{
					imgclass: "opt_img rt ops6",
					imgid : 'pond_bg',
					imgsrc: ""
				},{
					imgclass: "opt_img rt ops7",
					imgid : 'fountain_bg',
					imgsrc: ""
				},{
					imgclass: "opt_img rt ops8",
					imgid : 'tree_bg',
					imgsrc: ""
				}
			]
		}]
	},

];

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	/*for limiting the questions to 10*/
	var $total_page = 10;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "car", src: imgpath+"car.png", type: createjs.AbstractLoader.IMAGE},
			{id: "f_page", src: imgpath+"01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "s_page", src: imgpath+"02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "th_page", src: imgpath+"03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fth_page", src: imgpath+"08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pond", src: imgpath+"pond.png", type: createjs.AbstractLoader.IMAGE},
			{id: "house_8", src: imgpath+"house08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tree", src: imgpath+"tree01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "house_3", src: imgpath+"house03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "house_7", src: imgpath+"house07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fountain", src: imgpath+"fountain.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wrong", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
			{id: "right", src: 'images/right.png', type: createjs.AbstractLoader.IMAGE},
			{id: "house_5", src: imgpath+"house05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "house_2", src: imgpath+"house02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_wide", src: imgpath+"bgwide_grey.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_tall", src: imgpath+"bgtall_grey.png", type: createjs.AbstractLoader.IMAGE},
			{id: "house_red_bg", src: imgpath+"09.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pond_bg", src: imgpath+"10.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tree_bg", src: imgpath+"11.png", type: createjs.AbstractLoader.IMAGE},
			{id: "house_blue_bg", src: imgpath+"12.png", type: createjs.AbstractLoader.IMAGE},
			{id: "house_2str_bg", src: imgpath+"13.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fountain_bg", src: imgpath+"14.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		//scoring.init(10);
		templateCaller();
	}
	//initialize
	init();

	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
 	}

	 /*values in this array is same as the name of images of eggs in image folder*/

	 //create eggs
	 var testin = new EggTemplate();

	 	//eggTemplate.eggMove(countNext);
 		testin.init(10);

	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);
		put_image(content, countNext);
	 	var testcount = 0;

	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);

	 	/*generate question no at the beginning of question*/
	 	testin.numberOfQuestions();

	 	/*for randomizing the options*/
		// 	var parent = $(".optionsdiv");
		// 	var divs = parent.children();
		// 	while (divs.length) {
	 // 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		// 	}


	 	/*======= SCOREBOARD SECTION ==============*/
	 	/*random scoreboard eggs*/
	 	//var i = Math.floor(Math.random() * imageArray.length);
	 	//var randImg = imageArray[i];
	 	var ansClicked = false;
	 	var wrngClicked = false;
		var count = 0;
	 	switch(countNext){
	 		case 0:
		      var quesNo = rand_generator(4);
						$(".car1").addClass("rightshow");
				switch (quesNo){
				case 1:
					$(".buttonsel:eq(0)").addClass("correct");
				break;
				case 2:
					$(".buttonsel:eq(3)").addClass("correct");
				break;
				case 3:
					$(".buttonsel:eq(1)").addClass("correct");
				break;
				case 4:
					$(".buttonsel:eq(2)").addClass("correct");
				break;
				}
				$('.question').html((countNext+1 )+". "+eval('data.string.exetyp1_o'+quesNo));
        $(".buttonsel:eq(0)").html(eval('data.string.exetyp1opt1_o'+quesNo));
        $(".buttonsel:eq(1)").html(eval('data.string.exetyp1opt2_o'+quesNo));
        $(".buttonsel:eq(2)").html(eval('data.string.exetyp1opt3_o'+quesNo));
        $(".buttonsel:eq(3)").html(eval('data.string.exetyp1opt4_o'+quesNo));
			break;
	 		case 1:
		      var quesNo = rand_generator(4);
						$(".car1").addClass("leftshow");
				switch (quesNo){
				case 1:
					$(".buttonsel:eq(3)").addClass("correct");
				break;
				case 2:
					$(".buttonsel:eq(0)").addClass("correct");
				break;
				case 3:
					$(".buttonsel:eq(2)").addClass("correct");
				break;
				case 4:
					$(".buttonsel:eq(1)").addClass("correct");
				break;
				}
				$('.question').html((countNext+1 )+". "+eval('data.string.exetyp1_o'+quesNo));
        $(".buttonsel:eq(0)").html(eval('data.string.exetyp1opt1_o'+quesNo));
        $(".buttonsel:eq(1)").html(eval('data.string.exetyp1opt2_o'+quesNo));
        $(".buttonsel:eq(2)").html(eval('data.string.exetyp1opt3_o'+quesNo));
        $(".buttonsel:eq(3)").html(eval('data.string.exetyp1opt4_o'+quesNo));
			break;
			case 2:
		        var quesNo = rand_generator(6);
				if( (quesNo >= 1) && (quesNo < 4)){
					$(".car_2").removeClass("car_bot");
				}
				else{
					$(".car_2").addClass("car_bot");
				}
				switch (quesNo){
				case 1:
					$(".buttonsel:eq(0)").addClass("correct");
				break;
				case 2:
					$(".buttonsel:eq(1)").addClass("correct");
				break;
				case 3:
					$(".buttonsel:eq(2)").addClass("correct");
				break;
				case 4:
					$(".buttonsel:eq(2)").addClass("correct");
				break;
				case 5:
					$(".buttonsel:eq(1)").addClass("correct");
				break;
				case 6:
					$(".buttonsel:eq(0)").addClass("correct");
				break;
				}
				$('.question_2 ').html((countNext+1 )+". "+eval('data.string.exetyp2_o'+quesNo));
        $(".buttonsel:eq(0)").html(eval('data.string.exetyp2opt1_o'+quesNo));
        $(".buttonsel:eq(1)").html(eval('data.string.exetyp2opt2_o'+quesNo));
        $(".buttonsel:eq(2)").html(eval('data.string.exetyp2opt3_o'+quesNo));
        $(".buttonsel:eq(3)").html(eval('data.string.exetyp2opt4_o'+quesNo));
	 		break;
			case 3:
				var quesNo = rand_generator(6);
				// var quesNo = 6;
				switch (quesNo){
					case 1:
						$(".buttonsel:eq(0)").addClass("correct");
						$(".op1").addClass("highlight");
						$(".car1_sec").addClass("rightshow");
					break;
					case 2:
						$(".buttonsel:eq(1)").addClass("correct");
						$(".op4").addClass("highlight");
						$(".car1_sec").addClass("rightshow");
					break;
					case 3:
						$(".buttonsel:eq(2)").addClass("correct");
						$(".op9").addClass("highlight");
						$(".car1_sec").addClass("rightshow");
					break;
					case 4:
						$(".buttonsel:eq(3)").addClass("correct");
						$(".op7").addClass("highlight");
						$(".car1_sec").addClass("rightshow");
					break;
					case 5:
						$(".buttonsel:eq(0)").addClass("correct");
						$(".op2").addClass("highlight");
						$(".car1_sec").addClass("rightshow");
					break;
					case 6:
						$(".buttonsel:eq(1)").addClass("correct");
						$(".op10").addClass("highlight");
						$(".car1_sec").addClass("rightshow");
					break;
				}
			$('.question_3 ').html((countNext+1 )+". "+eval('data.string.exetyp3_o'+quesNo));
			$(".buttonsel:eq(0)").html(eval('data.string.exetyp3opt1_o'+quesNo));
			$(".buttonsel:eq(1)").html(eval('data.string.exetyp3opt2_o'+quesNo));
			$(".buttonsel:eq(2)").html(eval('data.string.exetyp3opt3_o'+quesNo));
			$(".buttonsel:eq(3)").html(eval('data.string.exetyp3opt4_o'+quesNo));
		break;
		case 4:
			var quesNo = rand_generator(6);
			switch (quesNo){
				case 1:
					$(".buttonsel:eq(0)").addClass("correct");
					$(".op5").addClass("highlight");
					$(".car1_sec").addClass("leftshow");
				break;
				case 2:
					$(".buttonsel:eq(1)").addClass("correct");
					$(".op8").addClass("highlight");
					$(".car1_sec").addClass("leftshow");
				break;
				case 3:
					$(".buttonsel:eq(2)").addClass("correct");
					$(".op3").addClass("highlight");
					$(".car1_sec").addClass("leftshow");
				break;
				case 4:
					$(".buttonsel:eq(3)").addClass("correct");
					$(".op9").addClass("highlight");
					$(".car1_sec").addClass("leftshow");
				break;
				case 5:
					$(".buttonsel:eq(0)").addClass("correct");
					$(".op10").addClass("highlight");
					$(".car1_sec").addClass("leftshow");
				break;
				case 6:
					$(".buttonsel:eq(1)").addClass("correct");
					$(".op2").addClass("highlight");
					$(".car1_sec").addClass("leftshow");
				break;
			}
			$('.question_3').html((countNext+1 )+". "+eval('data.string.exetyp8_o'+quesNo));
			$(".buttonsel:eq(0)").html(eval('data.string.exetyp8opt1_o'+quesNo));
			$(".buttonsel:eq(1)").html(eval('data.string.exetyp8opt2_o'+quesNo));
			$(".buttonsel:eq(2)").html(eval('data.string.exetyp8opt3_o'+quesNo));
			$(".buttonsel:eq(3)").html(eval('data.string.exetyp8opt4_o'+quesNo));
		break;
		case 5:
			var quesNo = rand_generator(6);
			// var quesNo = 6;
			switch (quesNo){
				case 1:
					$(".buttonsel:eq(0)").addClass("correct");
					$(".car_2").addClass("car_bot");
					$(".ops2").addClass("highlight");
				break;
				case 2:
					$(".buttonsel:eq(1)").addClass("correct");
					$(".car_2").addClass("car_bot");
					$(".ops1").addClass("highlight");
				break;
				case 3:
					$(".buttonsel:eq(2)").addClass("correct");
					$(".car_2").addClass("car_bot");
					$(".ops5").addClass("highlight");
				break;
				case 4:
					$(".buttonsel:eq(3)").addClass("correct");
					$(".car_2").addClass("car_bot");
					$(".ops8").addClass("highlight");
				break;
				case 5:
					$(".buttonsel:eq(0)").addClass("correct");
					$(".car_2").addClass("car_bot");
					$(".ops7").addClass("highlight");
				break;
				case 6:
					$(".buttonsel:eq(1)").addClass("correct");
					$(".car_2").addClass("car_bot");
					$(".ops6").addClass("highlight");
				break;
			}
			$('.question_4 ').html((countNext+1 )+". "+eval('data.string.exetyp4_o'+quesNo));
			$(".buttonsel:eq(0)").html(eval('data.string.exetyp4opt1_o'+quesNo));
			$(".buttonsel:eq(1)").html(eval('data.string.exetyp4opt2_o'+quesNo));
			$(".buttonsel:eq(2)").html(eval('data.string.exetyp4opt3_o'+quesNo));
			$(".buttonsel:eq(3)").html(eval('data.string.exetyp4opt4_o'+quesNo));
		break;
		case 6:
			var quesNo = rand_generator(6);
			switch (quesNo){
				case 1:
					$(".buttonsel:eq(0)").addClass("correct");
					$(".car_2").removeClass("car_bot");
					$(".ops5").addClass("highlight");
				break;
				case 2:
					$(".buttonsel:eq(1)").addClass("correct");
					$(".car_2").removeClass("car_bot");
					$(".ops1").addClass("highlight");
				break;
				case 3:
					$(".buttonsel:eq(2)").addClass("correct");
					$(".car_2").removeClass("car_bot");
					$(".ops5").addClass("highlight");
				break;
				case 4:
					$(".buttonsel:eq(3)").addClass("correct");
					$(".car_2").removeClass("car_bot");
					$(".ops3").addClass("highlight");
				break;
				case 5:
					$(".buttonsel:eq(0)").addClass("correct");
					$(".car_2").removeClass("car_bot");
					$(".ops6").addClass("highlight");
				break;
				case 6:
					$(".buttonsel:eq(1)").addClass("correct");
					$(".car_2").removeClass("car_bot");
					$(".ops7").addClass("highlight");
				break;
				}
			$('.question_4').html((countNext+1 )+". "+eval('data.string.exetyp9_o'+quesNo));
			$(".buttonsel:eq(0)").html(eval('data.string.exetyp9opt1_o'+quesNo));
			$(".buttonsel:eq(1)").html(eval('data.string.exetyp9opt2_o'+quesNo));
			$(".buttonsel:eq(2)").html(eval('data.string.exetyp9opt3_o'+quesNo));
			$(".buttonsel:eq(3)").html(eval('data.string.exetyp9opt4_o'+quesNo));
		break;
		case 7:
			var quesNo = rand_generator(5);
			switch(quesNo){
				case 1:
					$(".draggable").addClass("fhl");
				break;
				case 2:
					$(".draggable").addClass("shl");
				break;
				case 3:
					$(".draggable").addClass("thl");
				break;
				case 4:
					$(".draggable").addClass("fthl");
				break;
				case 5:
					$(".draggable").addClass("fhr");
				break;
				case 6:
					$(".draggable").addClass("shr");
				break;
				case 7:
					$(".draggable").addClass("thr");
				break;
				case 8:
					$(".draggable").addClass("fthr");
				break;
			}
			$('.question_5').html((countNext+1 )+". "+eval('data.string.exetyp5_o1'));
			$(".options").html(eval('data.string.exetyp5opt'+quesNo+'_o'+quesNo));

			$(".draggable").draggable({
				revert : true,
			});
			$(".fhl_drp").droppable({
				accept : ".draggable",
				hoverClass: "hovered",
				drop : function(event, ui){
				if(ui.draggable.hasClass("fhl")){
					testin.update(true);
					handleDrop(event, ui, ".fhl", ".fhl_drp");
				}
				else{
					testin.update(false);
				}
				}
			});
				$(".shl_drp").droppable({
          accept : ".draggable",
					hoverClass: "hovered",
					drop : function(event, ui){
					if(ui.draggable.hasClass("shl")){
						testin.update(true);
						handleDrop(event, ui, ".shl", ".shl_drp");
					}
					else{
						testin.update(false);
					}
					}
				});
				$(".thl_drp").droppable({
          accept : ".draggable",
					hoverClass: "hovered",
					drop : function(event, ui){
					if(ui.draggable.hasClass("thl")){
						testin.update(true);
						handleDrop(event, ui, ".thl", ".thl_drp");
					}
					else{
						testin.update(false);
					}
					}
				});
				$(".fthl_drp").droppable({
          accept : ".draggable",
					hoverClass: "hovered",
					drop : function(event, ui){
					if(ui.draggable.hasClass("fthl")){
						testin.update(true);
						handleDrop(event, ui, ".fthl", ".fthl_drp");
					}
					else{
						testin.update(false);
					}
					}
				});
				$(".fhr_drp").droppable({
          accept : ".draggable",
					hoverClass: "hovered",
					drop : function(event, ui){
					if(ui.draggable.hasClass("fhr")){
						testin.update(true);
						handleDrop(event, ui, ".fhr", ".fhr_drp");
					}
					else{
						testin.update(false);
					}
					}
				});
				$(".shr_drp").droppable({
          accept : ".draggable",
					hoverClass: "hovered",
					drop : function(event, ui){
					if(ui.draggable.hasClass("shr")){
						testin.update(true);
						handleDrop(event, ui, ".shr", ".shr_drp");
					}
					else{
						testin.update(false);
					}
					}
				});
				$(".thr_drp").droppable({
          accept : ".draggable",
					hoverClass: "hovered",
					drop : function(event, ui){
					if(ui.draggable.hasClass("thr")){
						testin.update(true);
						handleDrop(event, ui, ".thr", ".thr_drp");
					}
					else{
						testin.update(false);
					}
					}
				});
				$(".fthr_drp").droppable({
          accept : ".draggable",
					hoverClass: "hovered",
					drop : function(event, ui){
					if(ui.draggable.hasClass("fthr")){
						testin.update(true);
						handleDrop(event, ui, ".fthr", ".fthr_drp");
					}
					else{
						testin.update(false);
					}
					}
				});
			break;
		case 8:
			var quesNo = rand_generator(5);
			switch (quesNo) {
			case 1:
				$(".op7").addClass("correct");
				$(".car_6").addClass("leftshow");
				break;
			case 2:
				$(".op9").addClass("correct");
				$(".car_6").addClass("leftshow");
				break;
			case 3:
				$(".op2").addClass("correct");
				$(".car_6").addClass("leftshow");
				break;
			case 4:
				$(".op4").addClass("correct");
				$(".car_6").addClass("rightshow");
				break;
			case 5:
				$(".op8").addClass("correct");
				$(".car_6").addClass("rightshow");
				break;
			case 6:
				$(".op6").addClass("correct");
				$(".car_6").addClass("rightshow");
				break;
			}
			$('.question_5 ').html((countNext+1 )+". "+eval('data.string.exetyp6_o'+quesNo));
		break;

		case 9:
			var quesNo = rand_generator(5);
			switch (quesNo) {
			case 1:
				$(".ops8").addClass("correct");
				$(".car_2").removeClass("car_bot");
				break;
			case 2:
				$(".ops2").addClass("correct");
				$(".car_2").removeClass("car_bot");
				break;
			case 3:
				$(".ops6").addClass("correct");
				$(".car_2").removeClass("car_bot");
				break;
			case 4:
				$(".ops6").addClass("correct");
				$(".car_2").addClass("car_bot");
				break;
			case 5:
				$(".ops3").addClass("correct");
				$(".car_2").addClass("car_bot");
				break;
			case 6:
				$(".ops1").addClass("correct");
				$(".car_2").addClass("car_bot");
				break;
			}
			$('.question_2 ').html((countNext+1 )+". "+eval('data.string.exetyp7_o'+quesNo));
		break;
	 	}
		// image options click
		$(".opt_img").click(function(){
			if($(this).hasClass("correct")){
			testin.update(true);
			play_correct_incorrect_sound(1);
			var addfactor = $(this).parent().position();
			var posIt = $(this).position();
			var icoLeft = posIt.left + addfactor.left + $(this).width() - 50;
			var icoTop = posIt.top;
			// $(this).parent().prepend( "<img class= 'rightwrong' src="+preload.getResult("right").src+"/>");
			$(".rightwrong").css({
				"position" :"absolute",
				"left" : icoLeft,
				"top"  : icoTop,
				"width" : "5%",
				"z-index" : "100"
			});
			$(".opt_img").not(".correct").addClass("grayscale").css({"pointer-events":"none"});
			$(this).css({"pointer-events":"none"});
			$nextBtn.show(0);
			}
			else{
			testin.update(false);
			play_correct_incorrect_sound(0);
			$(this).css({"pointer-events":"none"}).addClass("grayscale");
			}
		});

  //  function to handle correct dropped events in drag and drop
		function handleDrop(event, ui,dragclass, dropclass){
			count++;
			var txtholder = $(dragclass).text();
			ui.draggable.draggable('disable');
				play_correct_incorrect_sound(1);
				$(dropclass).text(txtholder).css({
					"background" : "#bed62f",
					"border"		 : "5px solid #deef3c",
					"color"			 : "#000",
					"font-size"	 : "1.3vw",
					"color"			 : "#000",
					"text-align" : "left"
				});
				$(dropclass).siblings(".corctopt").show(0);
				$(dragclass).remove();
					$nextBtn.show();
		}

    function rand_generator(limit){
      var randNum = Math.floor((Math.random() * limit) + 1);
      return randNum;
    }
		$(".buttonsel").click(function(){
			$(this).removeClass('forhover');
				if(ansClicked == false){
					if($(this).hasClass("correct")){

						if(wrngClicked == false){
							testin.update(true);
						}
						var corval = $(this).text();
						$(".clickplace").text(corval).css("color","#0B9620");
						play_correct_incorrect_sound(1);
						$(this).css("background","#bed62f");
						$(this).css("border","5px solid #deef3c");
            $(this).css("color","white");
            $(this).siblings(".corctopt").show(0);
						$('.buttonsel').removeClass('forhover forhoverimg');
						ansClicked = true;
						if(countNext != $total_page)
						$nextBtn.show();
					}
					else{
						testin.update(false);
						play_correct_incorrect_sound(0);
						$(this).css("background","#FF0000");
						$(this).css("border","5px solid #980000");
						$(this).css("color","white");
      			$(this).siblings(".wrngopt").show(0);
						wrngClicked = true;
					}
				}
			});


	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					// console.log(selector);
					$(selector).attr('src',image_src);
				}
			}
		}
	}
	 	/*======= SCOREBOARD SECTION ==============*/
	 }

	 function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators


	}

	// first call to template caller
	// templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

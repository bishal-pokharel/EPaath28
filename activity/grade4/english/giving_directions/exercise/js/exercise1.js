var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";

var content=[	
	
	//ex0
	{
		contentblockadditionalclass: "grassbg",
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "car car_up",
						imgsrc : imgpath + "yellowcar.png",
					},
					{
						imgid: "straight",
						imgclass : "bottom_arrows central_arrow forhover wrongItem",
						imgsrc : imgpath + "straight.png",
					},
					{
						imgid: "left",
						imgclass : "bottom_arrows central_arrow_left forhover correctItem",
						imgsrc : imgpath + "left.png",
					},
					{
						imgid: "right",
						imgclass : "bottom_arrows central_arrow_right forhover wrongItem",
						imgsrc : imgpath + "right.png",
					},
					{
						imgclass : "cinema",
						imgsrc : imgpath + "cinema01.png",
					},
					{
						imgclass : "restaurant",
						imgsrc : imgpath + "restaurant.png",
					},
					{
						imgclass : "school",
						imgsrc : imgpath + "school.png",
					},
				],
				imagelabels : [
					{
						imagelabelclass : "bottom_ins",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "road_horizontal",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "road_vertical",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "label_dir my_font_medium",
						imagelabeldata : data.string.exeq1,
					},
					{
						imagelabelclass : "help_mom my_font_small sniglet",
						imagelabeldata : data.string.p3text13,
					}
				]
			}
		],
	},
	//ex1
	{
		contentblockadditionalclass: "grassbg",
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "car car_up",
						imgsrc : imgpath + "yellowcar.png",
					},
					{
						imgid: "straight",
						imgclass : "bottom_arrows central_arrow forhover wrongItem",
						imgsrc : imgpath + "straight.png",
					},
					{
						imgid: "left",
						imgclass : "bottom_arrows central_arrow_left forhover wrongItem",
						imgsrc : imgpath + "left.png",
					},
					{
						imgid: "right",
						imgclass : "bottom_arrows central_arrow_right forhover correctItem",
						imgsrc : imgpath + "right.png",
					},
					{
						imgclass : "cinema",
						imgsrc : imgpath + "cinema01.png",
					},
					{
						imgclass : "restaurant",
						imgsrc : imgpath + "restaurant.png",
					},
					{
						imgclass : "school",
						imgsrc : imgpath + "school.png",
					},
				],
				imagelabels : [
					{
						imagelabelclass : "bottom_ins",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "road_horizontal",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "road_vertical",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "label_dir my_font_medium",
						imagelabeldata : data.string.exeq2,
					},
					{
						imagelabelclass : "help_mom my_font_small sniglet",
						imagelabeldata : data.string.p3text13,
					}
				]
			}
		],
	},
	//ex2
	{
		contentblockadditionalclass: "grassbg",
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "car car_up",
						imgsrc : imgpath + "yellowcar.png",
					},
					{
						imgid: "straight",
						imgclass : "bottom_arrows central_arrow forhover correctItem",
						imgsrc : imgpath + "straight.png",
					},
					{
						imgid: "left",
						imgclass : "bottom_arrows central_arrow_left forhover wrongItem",
						imgsrc : imgpath + "left.png",
					},
					{
						imgid: "right",
						imgclass : "bottom_arrows central_arrow_right forhover wrongItem",
						imgsrc : imgpath + "right.png",
					},
					{
						imgclass : "cinema",
						imgsrc : imgpath + "cinema01.png",
					},
					{
						imgclass : "restaurant",
						imgsrc : imgpath + "restaurant.png",
					},
					{
						imgclass : "school",
						imgsrc : imgpath + "school.png",
					},
				],
				imagelabels : [
					{
						imagelabelclass : "bottom_ins",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "road_horizontal",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "road_vertical",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "label_dir my_font_medium",
						imagelabeldata : data.string.exeq3,
					},
					{
						imagelabelclass : "help_mom my_font_small sniglet",
						imagelabeldata : data.string.p3text13,
					}
				]
			}
		],
	},
	//ex3
	{
		contentblockadditionalclass: "grassbg",
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "car car_right",
						imgsrc : imgpath + "yellowcar.png",
					},
					{
						imgid: "straight",
						imgclass : "bottom_arrows central_arrow forhover wrongItem",
						imgsrc : imgpath + "straight.png",
					},
					{
						imgid: "left",
						imgclass : "bottom_arrows central_arrow_left forhover correctItem",
						imgsrc : imgpath + "left.png",
					},
					{
						imgid: "right",
						imgclass : "bottom_arrows central_arrow_right forhover wrongItem",
						imgsrc : imgpath + "right.png",
					},
					{
						imgclass : "cinema_rt",
						imgsrc : imgpath + "cinema01.png",
					},
					{
						imgclass : "restaurant_rt",
						imgsrc : imgpath + "restaurant.png",
					},
					{
						imgclass : "school_rt",
						imgsrc : imgpath + "school.png",
					},
				],
				imagelabels : [
					{
						imagelabelclass : "bottom_ins",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "road_horizontal_rt",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "road_vertical_rt",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "label_dir my_font_medium",
						imagelabeldata : data.string.exeq1a,
					},
					{
						imagelabelclass : "help_mom my_font_small sniglet",
						imagelabeldata : data.string.p3text13,
					}
				]
			}
		],
	},
	//ex4
	{
		contentblockadditionalclass: "grassbg",
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "car car_left",
						imgsrc : imgpath + "yellowcar.png",
					},
					{
						imgid: "straight",
						imgclass : "bottom_arrows central_arrow forhover wrongItem",
						imgsrc : imgpath + "straight.png",
					},
					{
						imgid: "left",
						imgclass : "bottom_arrows central_arrow_left forhover wrongItem",
						imgsrc : imgpath + "left.png",
					},
					{
						imgid: "right",
						imgclass : "bottom_arrows central_arrow_right forhover correctItem",
						imgsrc : imgpath + "right.png",
					},
					{
						imgclass : "cinema_lt",
						imgsrc : imgpath + "cinema01.png",
					},
					{
						imgclass : "restaurant_lt",
						imgsrc : imgpath + "restaurant.png",
					},
					{
						imgclass : "school_lt",
						imgsrc : imgpath + "school.png",
					},
				],
				imagelabels : [
					{
						imagelabelclass : "bottom_ins",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "road_horizontal_lt",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "road_vertical_lt",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "label_dir my_font_medium",
						imagelabeldata : data.string.exeq1a,
					},
					{
						imagelabelclass : "help_mom my_font_small sniglet",
						imagelabeldata : data.string.p3text13,
					}
				]
			}
		],
	},
	//ex5
	{
		contentblockadditionalclass: "grassbg",
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "car car_down",
						imgsrc : imgpath + "yellowcar.png",
					},
					{
						imgid: "straight",
						imgclass : "bottom_arrows central_arrow forhover wrongItem",
						imgsrc : imgpath + "straight.png",
					},
					{
						imgid: "left",
						imgclass : "bottom_arrows central_arrow_left forhover correctItem",
						imgsrc : imgpath + "left.png",
					},
					{
						imgid: "right",
						imgclass : "bottom_arrows central_arrow_right forhover wrongItem",
						imgsrc : imgpath + "right.png",
					},
					{
						imgclass : "cinema_td",
						imgsrc : imgpath + "cinema01.png",
					},
					{
						imgclass : "restaurant_td",
						imgsrc : imgpath + "restaurant.png",
					},
					{
						imgclass : "school_td",
						imgsrc : imgpath + "school.png",
					},
				],
				imagelabels : [
					{
						imagelabelclass : "bottom_ins",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "road_horizontal_td",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "road_vertical_td",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "label_dir my_font_medium",
						imagelabeldata : data.string.exeq2a,
					},
					{
						imagelabelclass : "help_mom my_font_small sniglet",
						imagelabeldata : data.string.p3text13,
					}
				]
			}
		],
	},
	//ex6
	{
		contentblockadditionalclass: "grassbg",
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "car car_down",
						imgsrc : imgpath + "yellowcar.png",
					},
					{
						imgid: "straight",
						imgclass : "bottom_arrows central_arrow forhover correctItem",
						imgsrc : imgpath + "straight.png",
					},
					{
						imgid: "left",
						imgclass : "bottom_arrows central_arrow_left forhover wrongItem",
						imgsrc : imgpath + "left.png",
					},
					{
						imgid: "right",
						imgclass : "bottom_arrows central_arrow_right forhover wrongItem",
						imgsrc : imgpath + "right.png",
					},
					{
						imgclass : "cinema_td",
						imgsrc : imgpath + "cinema01.png",
					},
					{
						imgclass : "restaurant_td",
						imgsrc : imgpath + "restaurant.png",
					},
					{
						imgclass : "school_td",
						imgsrc : imgpath + "school.png",
					},
				],
				imagelabels : [
					{
						imagelabelclass : "bottom_ins",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "road_horizontal_td",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "road_vertical_td",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "label_dir my_font_medium",
						imagelabeldata : data.string.exeq3a,
					},
					{
						imagelabelclass : "help_mom my_font_small sniglet",
						imagelabeldata : data.string.p3text13,
					}
				]
			}
		],
	},
	//ex7
	{
		contentblockadditionalclass: "grassbg",
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "car car_up",
						imgsrc : imgpath + "yellowcar.png",
					},
					{
						imgclass : "cinema forhoverimg wrongItem",
						imgid: "cinema",
						imgsrc : imgpath + "cinema01.png",
					},
					{
						imgclass : "restaurant forhoverimg correctItem",
						imgid: "restaurant",
						imgsrc : imgpath + "restaurant.png",
					},
					{
						imgclass : "school forhoverimg wrongItem",
						imgid: "school",
						imgsrc : imgpath + "school.png",
					},
				],
				imagelabels : [
					{
						imagelabelclass : "road_horizontal",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "road_vertical",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "label_dir my_font_medium",
						imagelabeldata : data.string.exeq4,
					}
				]
			}
		],
	},
	//ex7
	{
		contentblockadditionalclass: "grassbg",
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "car car_up",
						imgsrc : imgpath + "yellowcar.png",
					},
					{
						imgclass : "cinema forhoverimg correctItem",
						imgid: "cinema",
						imgsrc : imgpath + "cinema01.png",
					},
					{
						imgclass : "restaurant forhoverimg wrongItem",
						imgid: "restaurant",
						imgsrc : imgpath + "restaurant.png",
					},
					{
						imgclass : "school forhoverimg wrongItem",
						imgid: "school",
						imgsrc : imgpath + "school.png",
					},
				],
				imagelabels : [
					{
						imagelabelclass : "road_horizontal",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "road_vertical",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "label_dir my_font_medium",
						imagelabeldata : data.string.exeq5,
					}
				]
			}
		],
	},
	//ex7
	{
		contentblockadditionalclass: "grassbg",
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "car car_up",
						imgsrc : imgpath + "yellowcar.png",
					},
					{
						imgclass : "cinema forhoverimg wrongItem",
						imgid: "cinema",
						imgsrc : imgpath + "cinema01.png",
					},
					{
						imgclass : "restaurant forhoverimg wrongItem",
						imgid: "restaurant",
						imgsrc : imgpath + "restaurant.png",
					},
					{
						imgclass : "school forhoverimg correctItem",
						imgid: "school",
						imgsrc : imgpath + "school.png",
					},
				],
				imagelabels : [
					{
						imagelabelclass : "road_horizontal",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "road_vertical",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "label_dir my_font_medium",
						imagelabeldata : data.string.exeq6,
					}
				]
			}
		],
	},
];

$(function () {	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	/*for limiting the questions to 10*/
	var $total_page = 10;
	
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
 	}

	 /*values in this array is same as the name of images of eggs in image folder*/

	 //create eggs
	 var testin = new EggTemplate();
   
	 	//eggTemplate.eggMove(countNext);
 		testin.init(10);

	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);
	 	var testcount = 0;
	 	
	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);

	 	/*generate question no at the beginning of question*/
	 	testin.numberOfQuestions();

	 	/*for randomizing the options*/
	 	var parent = $(".optionsdiv");
	 	var divs = parent.children();
	 	while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
	 	}


	 	/*======= SCOREBOARD SECTION ==============*/
	 	/*random scoreboard eggs*/
	 	//var i = Math.floor(Math.random() * imageArray.length);
	 	//var randImg = imageArray[i];
	 	var ansClicked = false;
	 	var wrngClicked = false;

	 	switch(countNext){
	 		case 0:
	 		case 1:
	 		case 2:
	 		case 3:
	 		case 4:
	 		case 5:
	 		case 6:
		 		$(".correctItem").click(function(){
		 			if($(this).hasClass("forhover")){
				 		testin.update(true);
				 		$nextBtn.show(0);
				 		var ourId = $(this).attr("id");
				 		$(".bottom_arrows").removeClass("forhover");
				 		$(this).attr("src", imgpath+ourId+"correct.png");
						play_correct_incorrect_sound(1);
				}
			 	});

			 	$(".wrongItem").click(function(){
		 			if($(this).hasClass("forhover")){
				 		testin.update(false);
				 		$(this).removeClass("forhover");
				 		var ourId = $(this).attr("id");
				 		//$(this).addClass('wrongans selected');
				 		$(this).attr("src", imgpath+ourId+"wrong.png");
						play_correct_incorrect_sound(0);
				}
			 	});

			 	$(".forhover").mouseenter(function(){
					var audio = new buzz.sound((soundAsset + $(this).attr("id")+".wav"));
		 			if($(this).hasClass("forhover")){
						buzz.all().stop();
		 				audio.play();
		 			}
			 	});
	 		break;

	 		case 7:
	 		case 8:
	 		case 9:
		 		$(".correctItem").click(function(){
			 		testin.update(true);
			 		$nextBtn.show(0);
			 		$(".forhoverimg").removeClass("forhoverimg");
					play_correct_incorrect_sound(1);
			 	});

			 	$(".wrongItem").click(function(){
			 		testin.update(false);
					play_correct_incorrect_sound(0);
			 	});

			 	$(".forhoverimg").mouseenter(function(){
					var audio = new buzz.sound((soundAsset + $(this).attr("id")+".wav"));
		 			if($(this).hasClass("forhoverimg")){
						buzz.all().stop();
		 				audio.play();
		 			}
			 	});
	 		break;
	 	}

	 	

	 	/*======= SCOREBOARD SECTION ==============*/
	 }

	 function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');		

		// call navigation controller
		navigationcontroller();	

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   

		//call the slide indication bar handler for pink indicators
		

	}

	// first call to template caller
	templateCaller();	

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
		testin.gotoNext();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
		previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});
var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_dg2a = new buzz.sound((soundAsset + "s4_p2.ogg"));
var sound_dg2b = new buzz.sound((soundAsset + "s4_p3.ogg"));
var sound_dg3 = new buzz.sound((soundAsset + "s4_p3_opt1.ogg"));
var sound_dg4 = new buzz.sound((soundAsset + "s4_p3_opt2.ogg"));
var sound_dg5 = new buzz.sound((soundAsset + "s4_p3_rightOpt.ogg"));
var sound_dg6 = new buzz.sound((soundAsset + "s4_p3_wrongOpt.ogg"));
var sound_dg7 = new buzz.sound((soundAsset + "s4_p4_rightOpt.ogg"));
var sound_dg8 = new buzz.sound((soundAsset + "s4_p4_wrongOpt.ogg"));
var sound_dg9 = new buzz.sound((soundAsset + "s4_p5_rightOpt.ogg"));
var sound_dg10 = new buzz.sound((soundAsset + "s4_p5_wrongOpt.ogg"));
var sound_dg11= new buzz.sound((soundAsset + "s4_p6_rightOpt.ogg"));
var sound_dg12= new buzz.sound((soundAsset + "s4_p6_wrongOpt.ogg"));
var sound_dg13= new buzz.sound((soundAsset + "s4_p6a_wrongOpt.ogg"));

var jump = new buzz.sound((soundAsset + "jump.ogg"));
var fall = new buzz.sound((soundAsset + "fall.ogg"));
var meteor = new buzz.sound((soundAsset + "meteor.ogg"));
var boom = new buzz.sound((soundAsset + "boom.ogg"));
var ouch = new buzz.sound((soundAsset + "ouch.ogg"));
var huh = new buzz.sound((soundAsset + "huh.ogg"));
var rocket = new buzz.sound((soundAsset + "rocket.ogg"));
var bg = new buzz.sound((soundAsset + "p4all.ogg"));

var content=[
	//slide 0
	{
		contentblockadditionalclass : "contentwithbg2",
		uppertextblock:[
		{
			textdata : data.string.diy,
			textclass : 'diytext'
		}
		],
	},
	//slide 1
	{
		contentblockadditionalclass : "contentwithbg",
		uppertextblock:[
		{
			textdata : data.string.p1ques1,
			textclass : 'template-dialougebox2-top-white dg-1'
		}
	],
		imageblock : [
		{
			imagetoshow : [
			{
				imgclass : "astroman",
				imgsrc : imgpath + "assttrrooo-07.png"
			},
			],
		}
		]
	},
	//slide 2
	{
		contentblockadditionalclass : "contentwithbg",
		errordata: data.string.p1ques4,
		uppertextblock:[
			{
				textdata : data.string.p1ques2,
				textclass : 'hinttext'
			},
			{
				textdata : data.string.p1ques3,
				textclass : 'template-dialougebox2-top-flipped-white dg-2'
			}
		],
		imageblock : [
		{
			imagetoshow : [
				{
					imgclass : "astroguy",
					imgsrc : imgpath + "assttrrooo-07.png"
				},
				{
					imgclass : "crater",
					imgsrc : imgpath + "khalto.png"
				},
			],
		}
	],
	exerciseblock: [
		{
			textdata: data.string.p6diyq1,

			exeoptions: [
				{
					forshuffle: "class1",
					optdata: data.string.q1opt1,
				},
				{
					forshuffle: "class2",
					optdata: data.string.q1opt2,
				}
			]
		}
	]
},
//slide 3
{
	contentblockadditionalclass : "contentwithbg",
	errordata: data.string.p1ques6,
	uppertextblock:[
		{
			textdata : data.string.p1ques2,
			textclass : 'hinttext'
		},
		{
			textdata : data.string.p1ques5,
			textclass : 'template-dialougebox2-top-flipped-white dg-X'
		}
	],
	imageblock : [
	{
		imagetoshow : [
			{
				imgclass : "astroguy1",
				imgsrc : imgpath + "assttrrooo-07.png"
			},
			{
				imgclass : "meteor",
				imgsrc : imgpath + "meteor-hi.png"
			},
			{
				imgclass : "boom",
				imgsrc : imgpath + "boom.png"
			},
		],
	}
],
exerciseblock: [
	{
		textdata: data.string.p6diyq1,

		exeoptions: [
			{
				forshuffle: "class1",
				optdata: data.string.q2opt1,
			},
			{
				forshuffle: "class2",
				optdata: data.string.q2opt2,
			}
		]
	}
]
},
//slide 4
{
	contentblockadditionalclass : "contentwithbg",
	errordata: data.string.p1ques8,
	uppertextblock:[
		{
			textdata : data.string.p1ques2,
			textclass : 'hinttext'
		},
		{
			textdata : data.string.p1ques7,
			textclass : 'template-dialougebox2-top-flipped-white dg-2'
		}
	],
	imageblock : [
	{
		imagetoshow : [
			{
				imgclass : "astroguy",
				imgsrc : imgpath + "assttrrooo-07.png"
			},
			{
				imgclass : "stone",
				imgsrc : imgpath + "stone.png"
			},
		],
	}
],
exerciseblock: [
	{
		textdata: data.string.p6diyq1,

		exeoptions: [
			{
				forshuffle: "class1",
				optdata: data.string.q3opt1,
			},
			{
				forshuffle: "class2",
				optdata: data.string.q3opt2,
			}
		]
	}
]
},
//slide 5
{
	contentblockadditionalclass : "contentwithbg",
	errordata: data.string.p1ques11,
	uppertextblock:[
		{
			textdata : data.string.p1ques2,
			textclass : 'hinttext'
		},
		{
			textdata : data.string.p1ques9,
			textclass : 'template-dialougebox2-top-flipped-white dg-2'
		},
		{
			textdata : data.string.p1ques10,
			textclass : 'template-dialougebox2-top-flipped-white dg-4'
		}
	],
	imageblock : [
	{
		imagetoshow : [
			{
				imgclass : "astroguy",
				imgsrc : imgpath + "assttrrooo-07.png"
			},
			{
				imgclass : "rocket",
				imgsrc : imgpath + "rocket01.png"
			},
		],
	}
],
exerciseblock: [
	{
		textdata: data.string.p6diyq1,

		exeoptions: [
			{
				forshuffle: "class1",
				optdata: data.string.q4opt1,
			},
			{
				forshuffle: "class2",
				optdata: data.string.q4opt2,
			}
		]
	}
]
},
{
	contentblockadditionalclass : "contentwithbg",
	errordata: data.string.p1ques11,
	imageblock : [
	{
		imagetoshow : [
			{
				imgclass : "rocket buildup",
				imgsrc : imgpath + "rockets.gif"
			},
		],
	}
]
}
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	// bg.play().loop();

	var vocabcontroller = new Vocabulary();
	vocabcontroller.init();

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);

		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


      function navigationcontroller(islastpageflag){
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessionEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		var itsClicked = false;

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		vocabcontroller.findwords(countNext);

		$(".repimg").click(function(){
			generalTemplate();
		});
		switch(countNext){
			case 0:
				play_diy_audio();
				break;
			case 1:
						playaudio(sound_dg2a, $(".dialog1"));
						$(".dg-1").click(function(){
							playaudio(sound_dg2a, $(".dialog1"));
						});
			break;
			case 2:
					playaudio(sound_dg2b, $(".dialog1"));
					$nextBtn.hide(0);
					$prevBtn.hide(0);
				$(".forhover").click(function(){
					if($(this).hasClass("class1") && itsClicked == false){
						play_correct_incorrect_sound(1);
						itsClicked = true;
						$(this).addClass("corrans").removeClass("forhover");
						$(".class2").removeClass("forhover");
						$(".astroguy").addClass("animjump");
						$(this).siblings(".corctopt").show(0);
						$(".astroguy").attr("src",imgpath + "astronaut-animation.gif");
						$(".dg-2").delay(4000).show(0);

						setTimeout(function(){
								playaudio(sound_dg5, $(".dialog1"));
								setTimeout(function(){
									$nextBtn.show(0);
									$prevBtn.show(0);
								},6000);
						}, 4000);
					}
					else if($(this).hasClass("class2") && itsClicked == false){
						itsClicked = true;
						play_correct_incorrect_sound(0);
						$(this).addClass("incans").removeClass("forhover");
						$(".class1").removeClass("forhover");
						$(".astroguy").addClass("animfall");
						$(this).siblings(".wrngopt").show(0);
						setTimeout(function(){
							fall.play();
						}, 2000);
						setTimeout(function(){
							$(".errordiv").show(0);
							$(".optionsdiv").hide(0);
									playaudio(sound_dg6, $(".dialog1"));
						}, 4000);
					}
				});
				$(".dg-2").click(function(){
					playaudio(sound_dg5, $(".dialog1"));
					setTimeout(function(){
						$nextBtn.show(0);
						$prevBtn.show(0);
					},6000);
				});
			break;
			case 3:
				$(".forhover").click(function(){

					$(".meteor").addClass("met-move");
					if($(this).hasClass("class1") && itsClicked == false){
						play_correct_incorrect_sound(1);
						itsClicked = true;
						$(this).addClass("corrans").removeClass("forhover");
						$(".class2").removeClass("forhover");
						$(this).siblings(".corctopt").show(0);
						$(".astroguy1").addClass("animdodge");
						meteor.play();
						$(".astroguy1").attr("src",imgpath + "astronaut-animation.gif");
						$(".dg-X").delay(4000).show(0);
						setTimeout(function(){
								playaudio(sound_dg7, $(".dialog1"));
								setTimeout(function(){
									$nextBtn.show(0);
									$prevBtn.show(0);
								},10000);
						}, 4000);
					}
					else if($(this).hasClass("class2") && itsClicked == false){
						itsClicked = true;
						play_correct_incorrect_sound(0);
						$(".boom").delay(2000).show(0).delay(1000).hide(0);
						$(this).addClass("incans").removeClass("forhover");
						$(".class1").removeClass("forhover");
						$(this).siblings(".wrngopt").show(0);
						$(".astroguy1").addClass("flyoff");
						setTimeout(function(){
							boom.play();
						}, 2000);
						setTimeout(function(){
							$(".errordiv").show(0);
							$(".optionsdiv").hide(0);
							playaudio(sound_dg8, $(".dialog1"));
						}, 4000);
					}
				});
				$(".dg-X").click(function(){
					playaudio(sound_dg7, $(".dialog1"));
					setTimeout(function(){
						$nextBtn.show(0);
						$prevBtn.show(0);
					},10000);
				});
			break;
			case 4:
				$(".forhover").click(function(){
					if($(this).hasClass("class1") && itsClicked == false){
						play_correct_incorrect_sound(1);
						itsClicked = true;
						$(this).addClass("corrans").removeClass("forhover");
						$(".class2").removeClass("forhover");
						$(".astroguy").addClass("animjump");
						$(this).siblings(".corctopt").show(0);
						$(".astroguy").attr("src",imgpath + "astronaut-animation.gif");
						$(".dg-2").delay(4000).show(0);
						setTimeout(function(){
								playaudio(sound_dg9, $(".dialog1"));
								setTimeout(function(){
									$nextBtn.show(0);
									$prevBtn.show(0);
								},6000);
						}, 4000);
					}
					else if($(this).hasClass("class2") && itsClicked == false){
						itsClicked = true;
						$(this).addClass("incans").removeClass("forhover");
						$(".class1").removeClass("forhover");
						$(".astroguy").addClass("animhit");
						$(this).siblings(".wrngopt").show(0);
						setTimeout(function(){
							ouch.play();
						}, 2000);
						play_correct_incorrect_sound(0);
						setTimeout(function(){
							$(".errordiv").show(0);
							$(".optionsdiv").hide(0);
							playaudio(sound_dg10, $(".dialog1"));
						}, 4000);
					}
				});
				$(".dg-2").click(function(){
					playaudio(sound_dg9, $(".dialog1"));
					setTimeout(function(){
						$nextBtn.show(0);
						$prevBtn.show(0);
					},6000);
				});
			break;
			case 5:
				$(".forhover").click(function(){
					if($(this).hasClass("class2") && itsClicked == false){
						play_correct_incorrect_sound(1);
						itsClicked = true;
						$(this).addClass("corrans").removeClass("forhover");
						$(".class2").removeClass("forhover");
						// $(".astroguy").addClass("animjump");
						$(".astroguy").addClass("animdodge1");
						$(this).siblings(".corctopt").show(0);
						$(".astroguy").attr("src",imgpath + "astronaut-animation.gif");
						$(".dg-2").delay(4000).show(0);
						setTimeout(function(){
								playaudio(sound_dg11, $(".dialog1"));
								setTimeout(function(){
									$nextBtn.show(0);
									$prevBtn.show(0);
								},7000);
						}, 4000);
					}
					else if($(this).hasClass("class1") && itsClicked == false){
						itsClicked = true;
						$(this).addClass("incans").removeClass("forhover");
						play_correct_incorrect_sound(0);
						$(".class1").removeClass("forhover");
						$(".dg-3").show(0).delay(4000).hide(0);
						$(this).siblings(".wrngopt").show(0);
						setTimeout(function(){
							$(".dg-4").show(0);
							$(".optionsdiv").hide(0);
							playaudio(sound_dg12, $(".dialog1"));
							setTimeout(function(){
							$(".errordiv").show(0);
						},3300);
						}, 4000);
					}
				});
				$(".dg-4").click(function(){
					playaudio(sound_dg13, $(".dialog1"));
				});
				$(".dg-2").click(function(){
					playaudio(sound_dg11, $(".dialog1"));
					setTimeout(function(){
						$nextBtn.show(0);
						$prevBtn.show(0);
					},7000);
				});
			break;
			case 6:
			rocket.play();
			break;
		}
	}

	function playaudio(sound_data, $dialog_container){
			var playing = true;
			$dialog_container.removeClass("playable");
			$dialog_container.click(function(){
				if(!playing){
					playaudio(sound_data, $dialog_container);
				}
				return false;
			});
			$prevBtn.hide(0);
			if((countNext+1) == content.length){
				ole.footerNotificationHandler.hideNotification();
			}else{
				$nextBtn.hide(0);
			}
			sound_data.play();
			sound_data.bind('ended', function(){
				setTimeout(function(){
					if(countNext != 0)
					// $prevBtn.show(0);
					$dialog_container.addClass("playable");
					playing = false;
					sound_data.unbind('ended');
					if((countNext+1) == content.length){
						ole.footerNotificationHandler.pageEndSetNotification();
					}else
					if(countNext==2 || countNext==3 || countNext==4 || countNext==5 || countNext==6){
						// $nextBtn.hide(0);
						// $prevBtn.hide(0);
					}else {
						$nextBtn.show(0);
						$prevBtn.show(0);
					}{

					}
				}, 1000);
			});
		}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		if(countNext < 2 || countNext == 6)
		navigationcontroller();

		generalTemplate();

	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});


 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

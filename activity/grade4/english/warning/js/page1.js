var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_dg1 = new buzz.sound((soundAsset + "s1_p1.ogg"));
var sound_dg2 = new buzz.sound((soundAsset + "s1_p2.ogg"));
var sound_dg2a = new buzz.sound((soundAsset + "s1_p2_1.ogg"));
var sound_dg3 = new buzz.sound((soundAsset + "s1_p3.ogg"));
var sound_dg4 = new buzz.sound((soundAsset + "s1_p4.ogg"));
var sound_dg5 = new buzz.sound((soundAsset + "s1_p5.ogg"));
var sound_dg6 = new buzz.sound((soundAsset + "s1_p6.ogg"));
var sound_dg7 = new buzz.sound((soundAsset + "s1_p7.ogg"));
var sound_dg8 = new buzz.sound((soundAsset + "s1_p8.ogg"));
var sound_dg9 = new buzz.sound((soundAsset + "s1_p9.ogg"));
var sound_dg10 = new buzz.sound((soundAsset + "s1_p10.ogg"));
var sound_dg11 = new buzz.sound((soundAsset + "s1_p11.ogg"));
var content=[
	{
		contentblockadditionalclass: "ole-background-gradient-turqiose",
		imageblock : [
		{
			imagetoshow : [
				{
					imgclass : "screamingguy",
					imgsrc : imgpath + "extra-screaming-guy-02.png"
				}
			],
		}
	],
	containsdialog:[{

			dialogcontainer: "dialog-cover",
			dialogimageclass: "dialogimage1",
			dialogimagesrc: "images/textbox/white/tl-1.png",
			dialogcontentclass: "dialogcontent-cover",
			dialogcontent: data.lesson.chapter
		}]
	},
	{
	    // slide 1
	    contentblockadditionalclass: 'background',
	    flipperBlock: [{
	        balloonSrc: imgpath + 'balloon_loku_rope.png',
	        flippers: [{
	            flipperID: 'present-flipper',
	            sides: [{
	                sideClass: 'front',
	                titleText: data.lesson.chapter,
	                textdata: data.string.p1text1
	            }, {
	                sideClass: 'back',
									titleText: data.lesson.chapter,
	                textdata: data.string.p1text2
	            }]
	        }]
	    }]
	},
	{
		contentblockadditionalclass: "orgback",
		uppertextblock:[
		{
			textclass : 'middletext',
			textdata : data.string.p1text3,
		}
		]
	},
	{
		contentblockadditionalclass: "ole-background-gradient-turqiose",
		imageblock : [
		{
			imagetoshow : [
				{
					imgclass : "screamingguy",
					imgsrc : imgpath + "extra-screaming-guy-02.png"
				},
				{
					imgclass : "fireburn",
					imgsrc : imgpath + "fire.gif"
				},
				{
					imgclass : "fireboy",
					imgsrc : imgpath + "upaya's-son01.png"
				},
			],
		}
	],
	containsdialog:[{

			dialogcontainer: "dialog1",
			dialogimageclass: "dialogimage1",
			dialogimagesrc: imgpath + "blue.png",
			dialogcontentclass: "dialogcontent",
			dialogcontent: data.string.p1text4
		}],
	},
	{
		contentblockadditionalclass: "ole-background-gradient-opal",
		imageblock : [
		{
			imagetoshow : [
				{
					imgclass : "screamingguy",
					imgsrc : imgpath + "extra-screaming-guy-02.png"
				},
				{
					imgclass : "brochair",
					imgsrc : imgpath + "broken-chair-sitting-girl-02.png"
				},
			],
		}
	],
	containsdialog:[{

			dialogcontainer: "dialog1",
			dialogimageclass: "dialogimage1",
			dialogimagesrc: imgpath + "blue.png",
			dialogcontentclass: "dialogcontent",
			dialogcontent: data.string.p1text5
		}],
	},
	{
		contentblockadditionalclass: "ole-background-gradient-aquamarine",
		imageblock : [
		{
			imagetoshow : [
				{
					imgclass : "screamingguy",
					imgsrc : imgpath + "extra-screaming-guy-02.png"
				},
				{
					imgclass : "inoboy",
					imgsrc : imgpath + "upaya's-son.png"
				},
				{
					imgclass : "hitboy",
					imgsrc : imgpath + "throwing-water-balloon.png"
				},
			],
		}
	],
	containsdialog:[{

			dialogcontainer: "dialog1",
			dialogimageclass: "dialogimage1",
			dialogimagesrc: imgpath + "blue.png",
			dialogcontentclass: "dialogcontent",
			dialogcontent: data.string.p1text6
		}],
	},
	{
		contentblockadditionalclass: "carbg",
		imageblock : [
		{
			imagetoshow : [
				{
					imgclass : "screamingguy",
					imgsrc : imgpath + "extra-screaming-guy-02.png"
				}
			],
		}
	],
	containsdialog:[{

			dialogcontainer: "dialog1",
			dialogimageclass: "dialogimage1",
			dialogimagesrc: imgpath + "blue.png",
			dialogcontentclass: "dialogcontent",
			dialogcontent: data.string.p1text7
		}],
	},
	{
		contentblockadditionalclass: "bridgebg",
		imageblock : [
		{
			imagetoshow : [
				{
					imgclass : "screamingguy",
					imgsrc : imgpath + "extra-screaming-guy-02.png"
				}
			],
		}
	],
	containsdialog:[{

			dialogcontainer: "dialog1",
			dialogimageclass: "dialogimage1",
			dialogimagesrc: imgpath + "blue.png",
			dialogcontentclass: "dialogcontent",
			dialogcontent: data.string.p1text8
		}],
	},
	{
		contentblockadditionalclass: "ole-background-gradient-opal",
		imageblock : [
		{
			imagetoshow : [
				{
					imgclass : "screamingguy",
					imgsrc : imgpath + "extra-screaming-guy-02.png"
				},
				{
					imgclass : "teaboy",
					imgsrc : imgpath + "drinking-tea.png"
				},
			],
		}
	],
	containsdialog:[{

			dialogcontainer: "dialog1",
			dialogimageclass: "dialogimage1",
			dialogimagesrc: imgpath + "blue.png",
			dialogcontentclass: "dialogcontent",
			dialogcontent: data.string.p1text9
		}],
	},
	{
		contentblockadditionalclass: "branchbg",
		imageblock : [
		{
			imagetoshow : [
				{
					imgclass : "screamingguy",
					imgsrc : imgpath + "extra-screaming-guy-02.png"
				}
			],
		}
	],
	containsdialog:[{

			dialogcontainer: "dialog1",
			dialogimageclass: "dialogimage1",
			dialogimagesrc: imgpath + "blue.png",
			dialogcontentclass: "dialogcontent",
			dialogcontent: data.string.p1text10
		}],
	},
	{
		contentblockadditionalclass: "treebg",
		imageblock : [
		{
			imagetoshow : [
				{
					imgclass : "screamingguy",
					imgsrc : imgpath + "extra-screaming-guy-02.png"
				}
			],
		}
	],
	containsdialog:[{

			dialogcontainer: "dialog1",
			dialogimageclass: "dialogimage1",
			dialogimagesrc: imgpath + "blue.png",
			dialogcontentclass: "dialogcontent",
			dialogcontent: data.string.p1text11
		}],
	}
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();

    var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);

		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


      function navigationcontroller(islastpageflag){
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
		//
	 	// if(countNext == 0 && $total_page!=1){
	 	// 	$nextBtn.show(0);
	 	// 	$prevBtn.css('display', 'none');
	 	// }
	 	// else if($total_page == 1){
	 	// 	$prevBtn.css('display', 'none');
	 	// 	$nextBtn.css('display', 'none');
		//
		// 	// if lastpageflag is true
		// 	islastpageflag ?
		// 	ole.footerNotificationHandler.lessonEndSetNotification() :
		// 	ole.footerNotificationHandler.lessonEndSetNotification() ;
		// }
		// else if(countNext > 0 && countNext < $total_page-1){
		// 	$nextBtn.show(0);
		// 	$prevBtn.show(0);
		// }
		// else if(countNext == $total_page-1){
		// 	$nextBtn.css('display', 'none');
		// 	$prevBtn.show(0);
		//
		// 	// if lastpageflag is true
		// 	islastpageflag ?
		// 	ole.footerNotificationHandler.lessonEndSetNotification() :
		// 	ole.footerNotificationHandler.pageEndSetNotification() ;
		// }
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true

        vocabcontroller.findwords(countNext);

		texthighlight($board);
		$(".repimg").click(function(){
			generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
		});
		$nextBtn.hide(0);
		switch(countNext){
			case 0:
			playaudio(sound_dg1, $(".dg-1"));
			break;
			case 1:
					setTimeout(function() {
						buzz.all().stop();
					playaudio(sound_dg2, $(".dg-1"));
				},2000);
					$('.flipper-block').addClass('animated slideInUp');
					$('.front').on('click', function() {
						buzz.all().stop();
						playaudio(sound_dg2a, $(".dg-1"));
							$('#present-flipper').toggleClass('doFlip');
					});
					$('.back').on('click', function() {
					buzz.all().stop();
					playaudio(sound_dg2, $(".dg-1"));
					$('#present-flipper').toggleClass('doFlip');
				});
			break;
			case 2:
			playaudio(sound_dg3, $(".dg-1"));
			break;
			case 3:
			playaudio(sound_dg4, $(".dialog1"));
			break;
			case 4:
			playaudio(sound_dg5, $(".dialog1"));
			break;
			case 5:
			playaudio(sound_dg6, $(".dialog1"));
			break;
			case 6:
			playaudio(sound_dg7, $(".dialog1"));
			break;
			case 7:
			playaudio(sound_dg8, $(".dialog1"));
			break;
			case 8:
			playaudio(sound_dg9, $(".dialog1"));
			break;
			case 9:
			playaudio(sound_dg10, $(".dialog1"));
			break;
			case 10:
			playaudio(sound_dg11, $(".dialog1"));
			break;
		}
	}

  	function playaudio(sound_data, $dialog_container){
  			var playing = true;
  			$dialog_container.removeClass("playable");
  			$dialog_container.click(function(){
  				if(!playing){
  					playaudio(sound_data, $dialog_container);
  				}
  				return false;
  			});
  			$prevBtn.hide(0);
  			if((countNext+1) == content.length){
  				ole.footerNotificationHandler.hideNotification();
  			}else{
  				$nextBtn.hide(0);
  			}
  			sound_data.play();
  			sound_data.bind('ended', function(){
  				setTimeout(function(){
  					if(countNext != 0)
										$prevBtn.show(0);
  					$dialog_container.addClass("playable");
  					playing = false;
  					sound_data.unbind('ended');
  					if((countNext+1) == content.length){
  						ole.footerNotificationHandler.pageEndSetNotification();
									$prevBtn.show(0);
  					}else{
								$nextBtn.show(0);
							}

  				}, 100);
  			});
  		}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});


 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[

	// slide0
	{
		uppertextblock:[
		{
			textdata: data.string.p4text1,
			textclass: "text-top",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'img1',
				imgclass:'bg-full'
			}]
		}]

	},

	// slide1
	{
		uppertextblock:[
		{
			textdata: data.string.p4text2,
			textclass: "text-top",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'img8',
				imgclass:'bg-full'
			}]
		}]

	},

	// slide2
	{
		uppertextblock:[
		{
			textdata: data.string.p4text3,
			textclass: "text-top",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'img2',
				imgclass:'bg-full'
			}]
		}]

	},

	// slide3
	{
		uppertextblock:[
		{
			textdata: data.string.p4text4,
			textclass: "text-top",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'img3',
				imgclass:'bg-full'
			}]
		}]

	},

	// slide4
	{
		uppertextblock:[
		{
			textdata: data.string.p4text5,
			textclass: "text-top",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'img2',
				imgclass:'bg-full'
			}]
		}]

	},

	// slide5
	{
		imageblock:[{
			imagestoshow:[{
				imgid:'img5',
				imgclass:'bg-full'
			}]
		}]

	},

	// slide6
	{

		imageblock:[{
			imagestoshow:[{
				imgid:'img6',
				imgclass:'bg-full'
			},
			{
				imgid:'destroyed_house',
				imgclass:'destroyed_house'
			},
			{
				imgid:'img9',
				imgclass:'cloud'
			},
		]
		}]

	},

	// slide7
	{
		uppertextblock:[
		{
			textdata: data.string.p4text6,
			textclass: "text-top",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'img7',
				imgclass:'bg-full'
			}]
		}]

	},


		// slide8
		{
			uppertextblock:[
			{
				textdata: data.string.p4text6,
				textclass: "text-top",
			},
			{
				textdata: data.string.p3text7,
				textclass: "main-question quest",
			},
			{
				textdata: data.string.p4text7,
				textclass: "sentence quest",
			},
			{
				textdata: data.string.p3text8,
				textclass: "opt-2 quest",
			},
			{
				textdata: data.string.p3text9,
				textclass: "opt-3 quest",
			},
			{
				textdata: data.string.p3text10,
				textclass: "opt-4  quest",
			},
			{
				textdata: data.string.p3text11,
				textclass: "opt-1 correct quest",
			},],
			imageblock:[{
				imagestoshow:[{
					imgid:'img7',
					imgclass:'bg-full'
				}]
			}]

		},

		// slide9
		{
			uppertextblock:[
			{
				textdata: data.string.p4text6,
				textclass: "text-top",
			},
			{
				textdata: data.string.p3text7,
				textclass: "main-question quest",
			},
			{
				textdata: data.string.p4text8,
				textclass: "sentence quest",
			},
			{
				textdata: data.string.p3text8,
				textclass: "opt-2 quest",
			},
			{
				textdata: data.string.p3text9,
				textclass: "opt-3 correct quest",
			},
			{
				textdata: data.string.p3text10,
				textclass: "opt-4  quest",
			},
			{
				textdata: data.string.p3text11,
				textclass: "opt-1  quest",
			},],
			imageblock:[{
				imagestoshow:[{
					imgid:'img7',
					imgclass:'bg-full'
				}]
			}]

		},



];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "img1", src: imgpath+"27.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img2", src: imgpath+"12.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img3", src: imgpath+"13.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img4", src: imgpath+"09.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img5", src: imgpath+"11.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img6", src: imgpath+"15.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img7", src: imgpath+"14.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img8", src: imgpath+"30.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img9", src: imgpath+"cloud.png", type: createjs.AbstractLoader.IMAGE},
			{id: "destroyed_house", src: imgpath+"destroyed_house02.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0", src: soundAsset+"p4_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p4_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p4_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p4_s3.ogg"},
			{id: "sound_4", src: soundAsset+"p4_s4.ogg"},
			// {id: "sound_5", src: soundAsset+"p4_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p4_s6.ogg"},
			{id: "sound_7", src: soundAsset+"p4_s7.ogg"},
			// {id: "sound_8", src: soundAsset+"p4_s8.ogg"},
			// {id: "sound_9", src: soundAsset+"p4_s9.ogg"},
			// {id: "sound_10", src: soundAsset+"p4_s10.ogg"},
			{id: "s4_p9", src: soundAsset+"s4_p9.ogg"},
			{id: "s4_p10", src: soundAsset+"s4_p10.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);

		$('.destroyed_house').hide();

		switch(countNext) {
			case 0:
					sound_play_click('sound_0','doctor_dialogue1');
					//nav_button_controls(1500);
					break;
			case 1:
					sound_play_click('sound_1','doctor_dialogue1');
					//nav_button_controls(1500);
					break;
			case 2:
					sound_play_click('sound_2','doctor_dialogue1');
					//nav_button_controls(1500);
					break;
			case 3:
								$prevBtn.show(0);
						//		nav_button_controls(2000);
								sound_play_click('sound_3');
						break;
			case 4:
						$prevBtn.show(0);
						$('.burner').animate({"opacity":"0"},2000);
						$('.burner1').css({"opacity":"0"}).animate({"opacity":"1"},2000);
						sound_play_click('sound_4');
						//nav_button_controls(2000);
						break;
			case 5:
						sound_play_click('sound_5');
						nav_button_controls(2000);
						break;

			case 6:

						sound_play_click('sound_6');
						setTimeout(function(){
               $('.destroyed_house').show();
						},4000);
						//nav_button_controls(2000);
						break;

			case 7:
						sound_play_click('sound_7');
						//nav_button_controls(2000);
						break;

			case 8:
						createjs.Sound.stop();
						var current_sound = createjs.Sound.play('s4_p9');
						current_sound.play();
						$('.bg-full,.text-top').css({"filter": "grayscale(100%)"});
						$("#activity-page-prev-btn-enabled").hide(0);
						// $("#activity-page-next-btn-enabled").css({'background-image':'url("'+imgpath+'next.png")',"top": "80%",
						// "left": "83%",
						// "width": "5%",
						// "height": "7%"});
						$('.bg-full,.text-top').css({"filter": "grayscale(100%)"});
						$('.quest').css({'opacity':'0'}).animate({'opacity':'1'},1000);
						// sound_play_click('sound_9');
						$('.opt-1, .opt-2, .opt-3, .opt-4').click(function(){
							if($(this).hasClass('correct'))
							{
								var $this = $(this);
								var position = $this.position();
								var width = $this.width();
								var height = $this.height();
								var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
								var centerY = ((position.top + height)*100)/$board.height()+'%';
								$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(316%,-75%)" src="'+imgpath +'correct.png" />').insertAfter(this);
								$('.opt-1, .opt-2, .opt-3, .opt-4').css({"pointer-events":"none"});
								$(this).css({"background":"rgb(190,214,47)","color":"white"});
								play_correct_incorrect_sound(1);
								nav_button_controls(100);
							}
							else {
								var $this = $(this);
								var position = $this.position();
								var width = $this.width();
								var height = $this.height();
								var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
								var centerY = ((position.top + height)*100)/$board.height()+'%';
								$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(316%,-75%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
								play_correct_incorrect_sound(0);
								$(this).css({"background":"rgb(168, 33, 36)","color":"white","pointer-events":"none"});
							}
						});

						break;
						case 9:

						createjs.Sound.stop();
						var current_sound = createjs.Sound.play('s4_p10');
						current_sound.play();
									$("#activity-page-prev-btn-enabled").hide(0);
									$('.quest').css({'opacity':'0'}).animate({'opacity':'1'},1000);
									// sound_play_click('sound_9');
									$('.opt-1, .opt-2, .opt-3, .opt-4').click(function(){
										if($(this).hasClass('correct'))
										{
											var $this = $(this);
											var position = $this.position();
											var width = $this.width();
											var height = $this.height();
											var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
											var centerY = ((position.top + height)*100)/$board.height()+'%';
											$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(316%,-75%)" src="'+imgpath +'correct.png" />').insertAfter(this);
											$('.opt-1, .opt-2, .opt-3, .opt-4').css({"pointer-events":"none"});
											$(this).css({"background":"rgb(190,214,47)","color":"white"});
											play_correct_incorrect_sound(1);
											nav_button_controls(100);
										}
										else {
											var $this = $(this);
											var position = $this.position();
											var width = $this.width();
											var height = $this.height();
											var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
											var centerY = ((position.top + height)*100)/$board.height()+'%';
											$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(316%,-75%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
											play_correct_incorrect_sound(0);
											$(this).css({"background":"rgb(168, 33, 36)","color":"white","pointer-events":"none"});
										}
									});

									break;
			default:
				$prevBtn.show(0);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_play_click(sound_id, click_class){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$('.'+click_class).click(function(){
					current_sound.play();
				});
			}
		  nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/";

var sound1 = new buzz.sound((soundAsset + "ex_instr.ogg"));

var content=[
	//slide 0
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{
			textclass: "instruction2",
			textdata: data.string.exercise_instruction
		},{
			textclass: "scoreboard",
			textdata: "0/0"
		}],
		droppable:[{
			droppableclass: "droppable1",
			answer: data.string.e2_q0_o1,
			imgsrc: imgpath+"slum.jpg"
		},{
			droppableclass: "droppable2",
			answer: data.string.e2_q0_o2,
			imgsrc: imgpath+"prize.jpg"
		},{
			droppableclass: "droppable3",
			answer: data.string.e2_q0_o3,
			imgsrc: imgpath+"nun.jpg"
		},{
			droppableclass: "droppable4",
			answer: data.string.e2_q0_o4,
			imgsrc: imgpath+"flood_image.jpg"
		}],
		draggable:[{
			draggableclass: "draggable1",
			data_p: data.string.e2_q0_o1
		},{
			draggableclass: "draggable2",
			data_p: data.string.e2_q0_o2
		},{
			draggableclass: "draggable3",
			data_p: data.string.e2_q0_o3
		},{
			draggableclass: "draggable4",
			data_p: data.string.e2_q0_o4
		}]
	},
	//slide 1
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{
			textclass: "instruction2",
			textdata: data.string.exercise_instruction
		},{
			textclass: "scoreboard",
			textdata: "0/0"
		}],
		droppable:[{
			droppableclass: "droppable1",
			answer: data.string.e2_q1_o1,
			imgsrc: imgpath+"volunteer.jpg"
		},{
			droppableclass: "droppable2",
			answer: data.string.e2_q1_o2,
			imgsrc: imgpath+"task.jpg"
		},{
			droppableclass: "droppable3",
			answer: data.string.e2_q1_o3,
			imgsrc: imgpath+"funds.jpg"
		},{
			droppableclass: "droppable4",
			answer: data.string.e2_q1_o4,
			imgsrc: imgpath+"vow.jpg"
		}],
		draggable:[{
			draggableclass: "draggable1",
			data_p: data.string.e2_q1_o1
		},{
			draggableclass: "draggable2",
			data_p: data.string.e2_q1_o2
		},{
			draggableclass: "draggable3",
			data_p: data.string.e2_q1_o3
		},{
			draggableclass: "draggable4",
			data_p: data.string.e2_q1_o4
		}]
	},
	//slide 2
	{
		contentblockadditionalclass: 'main_bg',
		imageblock:[{
			imageblockclass: "block1 fade_in",
			imagestoshow:[{
				imgclass: "droppable_image",
				imgsrc: imgpath+"slum.jpg"
			}],
			imagelabels: [{
				imagelabelclass: "droppable_p",
				imagelabeldata: data.string.e2_q0_o1
			}]
		},{
			imageblockclass: "block2 fade_in",
			imagestoshow:[{
				imgclass: "droppable_image",
				imgsrc: imgpath+"prize.jpg"
			}],
			imagelabels: [{
				imagelabelclass: "droppable_p",
				imagelabeldata: data.string.e2_q0_o2
			}]
		},{
			imageblockclass: "block3 fade_in",
			imagestoshow:[{
				imgclass: "droppable_image",
				imgsrc: imgpath+"nun.jpg"
			}],
			imagelabels: [{
				imagelabelclass: "droppable_p",
				imagelabeldata: data.string.e2_q0_o3
			}]
		},{
			imageblockclass: "block4 fade_in",
			imagestoshow:[{
				imgclass: "droppable_image",
				imgsrc: imgpath+"flood_image.jpg"
			}],
			imagelabels: [{
				imagelabelclass: "droppable_p",
				imagelabeldata: data.string.e2_q0_o4
			}]
		}]

	},
	//slide 3
	{
		contentblockadditionalclass: 'main_bg',
		imageblock:[{
			imageblockclass: "block1 fade_in",
			imagestoshow:[{
				imgclass: "droppable_image",
				imgsrc: imgpath+"volunteer.jpg"
			}],
			imagelabels: [{
				imagelabelclass: "droppable_p",
				imagelabeldata: data.string.e2_q1_o1
			}]
		},{
			imageblockclass: "block2 fade_in",
			imagestoshow:[{
				imgclass: "droppable_image",
				imgsrc: imgpath+"task.jpg"
			}],
			imagelabels: [{
				imagelabelclass: "droppable_p",
				imagelabeldata: data.string.e2_q1_o2
			}]
		},{
			imageblockclass: "block3 fade_in",
			imagestoshow:[{
				imgclass: "droppable_image",
				imgsrc: imgpath+"funds.jpg"
			}],
			imagelabels: [{
				imagelabelclass: "droppable_p",
				imagelabeldata: data.string.e2_q1_o3
			}]
		},{
			imageblockclass: "block4 fade_in",
			imagestoshow:[{
				imgclass: "droppable_image",
				imgsrc: imgpath+"vow.jpg"
			}],
			imagelabels: [{
				imagelabelclass: "droppable_p",
				imagelabeldata: data.string.e2_q1_o4
			}]
		}]
	}
];

// content.shufflearray();


$(function ()
{
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	// var score = 0;
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
	function soundplayer(i){
		buzz.all().stop();
		i.play().bind("ended",function(){
				navigationcontroller();
		});
	}
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		if (countNext==0) soundplayer(sound1);

		$(".scoreboard").html(score+"/"+8);
		//randomize options
		var parent = $(".draggable_containers");
		var divs = parent.children();
		parent.append(divs.splice(0, 1)[0]);
		while (divs.length) {
	    	parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}

		$(".drggable").draggable({
			containment : "body",
			cursor : "pointer",
			revert : "invalid",
			appendTo : "body",
			helper : "clone",
			zindex : 10000
		});

		$('.droppables').droppable({
			accept : ".drggable",
			hoverClass : "hovered",
			drop : handleCardDrop
		});

		function handleCardDrop(event, ui){
					var dropped = ui.draggable;
					var droppedon =  $(this);
					var $p = $(droppedon.find("p"));
					$p.html(dropped.html()).css({
						"background-color": "#00c9db",
						"color": "#fff"
					});
					if(droppedon.data('answer') === dropped.html() ){
						$p.attr('data-correctans', "true");
					}else{
						$p.attr('data-correctans', "false");
					}

					var $droppable_p = $(".droppable_p");
					var count = 0;
					var showcheck = true;

					while(count < 4){
						count++;
						if ($($droppable_p[count]).html() == "")
							showcheck = false;
					}

					if(showcheck){
						$(".check_button").show(0);
					}
		}
		var checkdone = false;
		$(".check_button").click(function(){
			if(checkdone){
				return checkdone;
			}
			$(".drggable").removeClass("ui-draggable ui-draggable-handle");
			var $droppable_p = $(".droppable_p");
			var $droppable = $(".droppables").removeClass("ui-droppable");
			var $correctsign = $(".correctsign");
			var $incorrectsign = $(".incorrectsign");
			var count = 0;

			function checkfunction(){
				if($($droppable_p[count]).data("correctans")){
					$($correctsign[count]).show(0);
					score++;
					$(".scoreboard").html(score+"/"+8);
					play_correct_incorrect_sound(1);

				}else{
					$($incorrectsign[count]).show(0);
					play_correct_incorrect_sound(0);
				}
				count++;
			}
			checkfunction();
			var intervalid = setInterval(function(){
				checkfunction();
				if(count ==4){
					$nextBtn.show(0);
					clearInterval(intervalid);
				}
			}, 2200);
			checkdone = true;


		});
		switch(countNext){
			case 2:
			case 3:
					$(".block1").show(0);
					$(".block2").delay(1200).show(0);
					$(".block3").delay(2400).show(0);
					$(".block4").delay(3600).show(0);

					setTimeout(function(){
						$nextBtn.show(0);
					}, 4000);
				break;
			default:
				break;
		}
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
        loadTimelineProgress($total_page,countNext+1);

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		if(countNext == $total_page){
			// for this template we are using modified rhino so we need to specify total correct answers
			TotalQues = 8;//4 questions per page
			$(".board").html("").css("background-color", "#9DC4DD");
			create_exercise_menu_bar();
			$nextBtn.hide(0);
		}else{
			templateCaller();
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var soundfirst = new buzz.sound((soundAsset + "s1_p1.ogg"));
var soundlets = new buzz.sound((soundAsset + "s1_p2.ogg"));

var sound_1_1 = new buzz.sound((soundAsset + "11.ogg"));
var sound_1_2 = new buzz.sound((soundAsset + "12.ogg"));
var sound_1_3 = new buzz.sound((soundAsset + "13.ogg"));
var sound_1_4 = new buzz.sound((soundAsset + "14.ogg"));
var sound_1_5 = new buzz.sound((soundAsset + "15.ogg"));
var sound_1_6 = new buzz.sound((soundAsset + "16.ogg"));
var sound_group_p1 = [sound_1_1, sound_1_2, sound_1_3, sound_1_4, sound_1_5, sound_1_6];

var sound_2_1 = new buzz.sound((soundAsset + "21.ogg"));
var sound_2_2 = new buzz.sound((soundAsset + "22.ogg"));
var sound_2_3 = new buzz.sound((soundAsset + "23.ogg"));
var sound_2_4 = new buzz.sound((soundAsset + "24.ogg"));
var sound_group_p2 = [sound_2_1, sound_2_2, sound_2_3, sound_2_4];

var sound_3_1 = new buzz.sound((soundAsset + "31.ogg"));
var sound_3_2 = new buzz.sound((soundAsset + "32.ogg"));
var sound_3_3 = new buzz.sound((soundAsset + "33.ogg"));
var sound_group_p3 = [sound_3_1, sound_3_2, sound_3_3];

var sound_4_1 = new buzz.sound((soundAsset + "41.ogg"));
var sound_4_2 = new buzz.sound((soundAsset + "42.ogg"));
var sound_group_p4 = [sound_4_1, sound_4_2];

var sound_5_1 = new buzz.sound((soundAsset + "51.ogg"));
var sound_5_2 = new buzz.sound((soundAsset + "52.ogg"));
var sound_group_p5 = [sound_5_1, sound_5_2];

var sound_6_1 = new buzz.sound((soundAsset + "61.ogg"));
var sound_6_2 = new buzz.sound((soundAsset + "62.ogg"));
var sound_group_p6 = [sound_6_1, sound_6_2];

var sound_7_1 = new buzz.sound((soundAsset + "71.ogg"));
var sound_7_2 = new buzz.sound((soundAsset + "72.ogg"));
var sound_7_3 = new buzz.sound((soundAsset + "73.ogg"));
var sound_group_p7 = [sound_7_1, sound_7_2];

var sound_8_1 = new buzz.sound((soundAsset + "81.ogg"));
var sound_group_p8 = [sound_8_1];

var sound_data = sound_1_1;

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblockadditionalclass:'pattern1',

		uppertextblockadditionalclass : 'lesson-title',
		uppertextblock : [{
			textdata : data.lesson.chapter,
			textclass : 'my_font_super_big'
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "cover",
					imgsrc : imgpath + "01.png",
				}
			],
		}]
	},

	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_black',

		uppertextblockadditionalclass : 'instruction',
		uppertextblock : [{
			textdata : data.string.p1text1,
			textclass : 'my_font_very_big'
		}]
	},

	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_black',

		storytextblockadditionalclass : "bottom_para my_font_medium",
		storytextblock : [
		{
			textclass : "text_story my_font_big text_l0",
			textdata : data.string.p1text2,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		},
		{
			textclass : "text_story my_font_big text_l1",
			textdata : data.string.p1text24,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		},
		{
			textclass : "text_story my_font_big text_l2",
			textdata : data.string.p1text3,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		},
		{
			textclass : "text_story my_font_big text_l3",
			textdata : data.string.p1text4,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		},
		{
			textclass : "text_story my_font_big text_l4",
			textdata : data.string.p1text5,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		},
		{
			textclass : "text_story my_font_big text_l5",
			textdata : data.string.p1text6,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		}],


		imageblock : [{
			imagestoshow : [
				{
					imgclass : "center-image",
					imgsrc : imgpath + "01.png",
				}
			],
		}]

	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_black',

		storytextblockadditionalclass : "bottom_para my_font_medium",
		storytextblock : [
		{
			textclass : "text_story my_font_big text_l0",
			textdata : data.string.p1text7,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		},
		{
			textclass : "text_story my_font_big text_l1",
			textdata : data.string.p1text8,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		},
		{
			textclass : "text_story my_font_big text_l2",
			textdata : data.string.p1text9,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		},
		{
			textclass : "text_story my_font_big text_l3",
			textdata : data.string.p1text10,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "center-image",
					imgsrc : imgpath + "02.png",
				}
			],
		}]
	},

	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_black',

		storytextblockadditionalclass : "bottom_para my_font_medium",
		storytextblock : [
		{
			textclass : "text_story my_font_big text_l0",
			textdata : data.string.p1text11,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		},
		{
			textclass : "text_story my_font_big text_l1",
			textdata : data.string.p1text12,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		},
		{
			textclass : "text_story my_font_big text_l2",
			textdata : data.string.p1text13,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "center-image",
					imgsrc : imgpath + "03.png",
				}
			],
		}]
	},

	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_black',

		storytextblockadditionalclass : "bottom_para my_font_medium",
		storytextblock : [
		{
			textclass : "text_story my_font_big text_l0",
			textdata : data.string.p1text14,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		},
		{
			textclass : "text_story my_font_big text_l1",
			textdata : data.string.p1text15,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "center-image",
					imgsrc : imgpath + "04.png",
				}
			],
		}]
	},

	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_black',

		storytextblockadditionalclass : "bottom_para my_font_medium",
		storytextblock : [
		{
			textclass : "text_story my_font_big text_l0",
			textdata : data.string.p1text16,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		},
		{
			textclass : "text_story my_font_big text_l1",
			textdata : data.string.p1text17,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "center-image",
					imgsrc : imgpath + "05.png",
				}
			],
		}]
	},

	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_black',

		storytextblockadditionalclass : "bottom_para my_font_medium",
		storytextblock : [
		{
			textclass : "text_story my_font_big text_l0",
			textdata : data.string.p1text18,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		},
		{
			textclass : "text_story my_font_big text_l1",
			textdata : data.string.p1text19,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "center-image",
					imgsrc : imgpath + "06.png",
				}
			],
		}]
	},

	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_black',

		storytextblockadditionalclass : "bottom_para my_font_medium",
		storytextblock : [
		{
			textclass : "text_story my_font_big text_l0",
			textdata : data.string.p1text20,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		},
		{
			textclass : "text_story my_font_big text_l1",
			textdata : data.string.p1text21,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "center-image",
					imgsrc : imgpath + "07.png",
				}
			],
		}]
	},

	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_black',

		storytextblockadditionalclass : "bottom_para my_font_medium centered_text",
		storytextblock : [
		{
			textclass : "text_story my_font_big text_l0",
			textdata : data.string.p1text23,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "center-image",
					imgsrc : imgpath + "08.png",
				}
			],
		}]
	}
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	var last_page = false;

	var text_counter = 0;
	var current_sound_data = sound_group_p1;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();


	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		vocabcontroller.findwords(countNext);
		switch (countNext) {
			case 0:
			soundplayer(soundfirst);
			nav_button_controls(1900);
			break;
			case 1:
			soundplayer(soundlets);
			nav_button_controls(6000);
			break;
			case 2:
				text_counter = 0;
				current_sound_data = sound_group_p1;
	    		for_all_slides('text_l', sound_group_p1, false);
				break;
			case 3:
				text_counter = 0;
				current_sound_data = sound_group_p2;
	    		for_all_slides('text_l', sound_group_p2, false);
				break;
			case 4:
				text_counter = 0;
				current_sound_data = sound_group_p3;
	    		for_all_slides('text_l', sound_group_p3, false);
				break;
			case 5:
				text_counter = 0;
				current_sound_data = sound_group_p4;
	    		for_all_slides('text_l', sound_group_p4, false);
				break;
			case 6:
				text_counter = 0;
				current_sound_data = sound_group_p5;
	    		for_all_slides('text_l', sound_group_p5, false);
				break;
			case 7:
				text_counter = 0;
				current_sound_data = sound_group_p6;
	    		for_all_slides('text_l', sound_group_p6, false);
				break;
			case 8:
				text_counter = 0;
				current_sound_data = sound_group_p7;
	    	for_all_slides('text_l', sound_group_p7, false);
				break;
			case 9:
				text_counter = 0;
				current_sound_data = sound_group_p8;
	    		for_all_slides('text_l', sound_group_p8, true);
				break;
			default:
				nav_button_controls(500);
				break;
		}
	}

	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function soundplayer(i){
		buzz.all().stop();
		i.play().bind("ended",function(){
				navigationcontroller();
		});
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {

		switch(countNext){
			case 2:
				text_counter++;
				var $textnew = $('.text_l'+text_counter);
				var $textnew2 = $('.text_l'+(text_counter+1));
				if(typeof $textnew2.html() ==  'undefined'){
					$('.text_story').css('pointer-events', 'all');
				}
				if(typeof $textnew.html() ==  'undefined'){
					sound_data.stop();
					countNext++;
					templateCaller();
				} else {
					for_all_slides('text_l', sound_group_p1, false);
				}
				console.log('count: '+countNext,'counter_test: '+text_counter);
				break;

			case 3:
				text_counter++;
				var $textnew = $('.text_l'+text_counter);
				if(typeof $textnew.html() ==  'undefined'){
					sound_data.stop();
					countNext++;
					templateCaller();
				} else {
					for_all_slides('text_l', sound_group_p2, false);
				}
				break;
			case 4:
				text_counter++;
				var $textnew = $('.text_l'+text_counter);
				if(typeof $textnew.html() ==  'undefined'){
					sound_data.stop();
					countNext++;
					templateCaller();
				} else {
					for_all_slides('text_l', sound_group_p3, false);
				}
				break;
			case 5:
				text_counter++;
				var $textnew = $('.text_l'+text_counter);
				if(typeof $textnew.html() ==  'undefined'){
					sound_data.stop();
					countNext++;
					templateCaller();
				} else {
					for_all_slides('text_l', sound_group_p4, false);
				}
				break;
			case 6:
				text_counter++;
				var $textnew = $('.text_l'+text_counter);
				if(typeof $textnew.html() ==  'undefined'){
					sound_data.stop();
					countNext++;
					templateCaller();
				} else {
					for_all_slides('text_l', sound_group_p5, false);
				}
				break;
			case 7:
				text_counter++;
				var $textnew = $('.text_l'+text_counter);
				if(typeof $textnew.html() ==  'undefined'){
					sound_data.stop();
					countNext++;
					templateCaller();
				} else {
					for_all_slides('text_l', sound_group_p6, false);
				}
				break;
			case 8:
				text_counter++;
				var $textnew = $('.text_l'+text_counter);
				if(typeof $textnew.html() ==  'undefined'){
					sound_data.stop();
					countNext++;
					templateCaller();
				} else {
					for_all_slides('text_l', sound_group_p7, false);
				}
				break;

			default:
				sound_data.stop();
				countNext++;
				templateCaller();
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		sound_data.stop();
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();

	function for_all_slides(text_class, my_sound_data, last_page_flag){
		$nextBtn.hide(0);
		var $textblack = $("."+text_class+text_counter);
		// $textblack.addClass('text_active');
		sound_data =  my_sound_data[text_counter];
		var current_text = $textblack.html();
		// current_text.replace(/<.*>/, '');
		play_text($textblack, current_text);
		sound_data.bind('ended', function(){
			change_slides($textblack, last_page_flag);
		});
	}

	function change_slides($textclass_new, last_page_flag){
		var checking_interval = setInterval(function(){

     // console.log('in change slides');
		 // console.log('lastpageflag: '+last_page_flag);

			if(textanimatecomplete){
				// $textclass_new.removeClass('text_active');
				if(!last_page_flag){
					  console.log('lastpageflag next btn show');
    				$nextBtn.show(0);
	    		}else{
	    			ole.footerNotificationHandler.pageEndSetNotification();
	    		}
    			$prevBtn.show(0);
    			var $textnew2 = $('.text_l'+(text_counter+1));
				if(typeof $textnew2.html() ==  'undefined'){
					enable_click();
       				vocabcontroller.findwords(countNext);
								console.log('in change slides enable click');
				}
    			clearInterval(checking_interval);
			} else{
				console.log('in change slides else');
				$prevBtn.hide(0);
				$nextBtn.hide(0);
			}
		},50);
	}

/* For typing animation appends the text to an element specified by target class or id */
	function show_text($this,  $span_speec_text, message, interval) {

		var stags_counter = 1;
		var stags = message.split(/[<>]/);
		var gt_encounters=[];
	  	if (0 < message.length) {
		  	textanimatecomplete = false;
		  	var nextText = message.substring(0,1);
		  	var brText = message.substring(0,4);
		  	if(brText =='<br>'){
		  		$span_speec_text.append("<br>");
	  			message = message.substring(4, message.length);
		  	}
		  	else if(nextText == "<" ){
		  		gt_encounters.push($span_speec_text.html().length);
		  		// $span_speec_text.append('<'+stags[stags_counter]+'>');
		  		message = message.substring(stags[stags_counter].length+2, message.length);
		  		stags_counter+=2;
		  	}else{
		  		$span_speec_text.append(nextText);
		  		message = message.substring(1, message.length);
		  	}
		  	$this.html($span_speec_text);
		  	$this.append(message);
		    setTimeout(function () {
		    	show_text($this,  $span_speec_text, message, interval);
		  	}, interval);
			} else{
	  		textanimatecomplete = true;
	  	}
	}

	var intervalid;
	var soundplaycomplete = false;
	var textanimatecomplete = false;
	// uses the show text to add typing effect with sound and glowing animations
	function play_text($this, text/*, sound_data*/){
		var new_span_class = 'span_speec_text-'+text_counter;
		$this.html("<span id="+new_span_class+"></span>"+text);
		$prevBtn.hide(0);
		var $span_speec_text = $("#"+new_span_class);
		// $this.css("background-color", "#faf");
		show_text($this, $span_speec_text,text, 65);	// 65 ms is the interval found out by hit and trial
		sound_data.play();
		sound_data.bind('ended', function(){
			// $this.removeClass('text_on');
			// $this.addClass('text_off');
			sound_data.unbind('ended');
			soundplaycomplete = true;
			ternimatesound_play_animate(text);
		});

		function ternimatesound_play_animate(text){
			 intervalid = setInterval(function () {
				if(textanimatecomplete && soundplaycomplete){
					$this.html($span_speec_text.html(text));
					$this.css("background-color", "transparent");
					clearInterval(intervalid);
					intervalid = null;
					animationinprogress = false;
       				vocabcontroller.findwords(countNext);
				}
			}, 250);
		}
	}

	/* Typing animation ends */
	function enable_click(){
		$('.text_story').css('pointer-events', 'all');
		setTimeout(function(){
			$('.text_story>span').click(function(){
				sound_data.stop();
				$('[id|="span_speec_text"]').removeClass('playing');
				var new_id = $(this).attr('id');
				var id_number = parseInt(new_id.substr(new_id.length - 1));
				$(this).addClass('playing');

				sound_data =  current_sound_data[id_number];
				sound_data.play();
				sound_data.bind('ended', function(){
					$('[id|="span_speec_text"]').removeClass('playing');
				});
			});
		}, 500);

	}

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

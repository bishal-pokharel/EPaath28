var imgpath = $ref + "/";
// var content;
var soundAsset = $ref+"/sounds/";

var s1_p1 = new buzz.sound((soundAsset + "s1_p1.ogg"));
var s2_p2 = new buzz.sound((soundAsset + "s2_p2.ogg"));
var s2_p3_1 = new buzz.sound((soundAsset + "s2_p3(1).ogg"));
var s2_p3_2 = new buzz.sound((soundAsset + "s2_p3(2).ogg"));
var s2_p4 = new buzz.sound((soundAsset + "s2_p4.ogg"));
var s2_p5_1 = new buzz.sound((soundAsset + "s2_p5(1).ogg"));
var s2_p5_2 = new buzz.sound((soundAsset + "s2_p5(2).ogg"));
var s2_p5_3 = new buzz.sound((soundAsset + "s2_p5(3).ogg"));

var current_sound = s1_p1;
// function getContent(data){
var content=[
{
	//slide 0
	additionalclasscontentblock: "titlebg",
	uppertextblock:[
	{
		textclass : "titletext",
		textdata : data.string.title,
	}
	],
	punctuation:[
	{
		whichpunc: "fullstop",
		topictext: ".",
		topicdesc: data.string.fulls
	},
	{
		whichpunc: "comma",
		topictext: ",",
		topicdesc: data.string.comma
	},
	{
		whichpunc: "questionm",
		topictext: "?",
		topicdesc: data.string.quesm
	},
	{
		whichpunc: "exclam",
		topictext: "!",
		topicdesc: data.string.exclm
	}
	]
},
{
	//slide 1
	additionalclasscontentblock: "titlebg",
	uppertextblock:[
	{
		textclass : "titletext",
		textdata : data.string.title,
	}
	],
	punctuation:[
	{
		whichpunc: "fullstop fadeout",
		topictext: ".",
		topicdesc: data.string.fulls
	},
	{
		whichpunc: "comma selectme",
		topictext: ",",
		topicdesc: data.string.comma
	},
	{
		whichpunc: "questionm fadeout",
		topictext: "?",
		topicdesc: data.string.quesm
	},
	{
		whichpunc: "exclam fadeout",
		topictext: "!",
		topicdesc: data.string.exclm
	}
	]
},
{
	//slide 2
	additionalclasscontentblock: "titlebg",
	headerblock:[
	{
		textclass : "fadein",
		textdata : data.string.p2text1,
	}
	],
	punctuation:[
	{
		whichpunc: "comma afterselect",
		topictext: ",",
		topicdesc: data.string.comma
	}
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "mainimg",
			imgsrc : imgpath + "images/sweets.png"
		},
		],
	}
	],
	lowertextblock:[
	{
		datahighlightflag: true,
		datahighlightcustomclass: "letshighlight",
		textclass : "lowercaption fadein",
		textdata : data.string.p2text2,
	}
	],
},
{
	//slide 3
	additionalclasscontentblock: "titlebg",
	headerblock:[
	{
		textdata : data.string.p2text1,
	}
	],
	punctuation:[
	{
		whichpunc: "comma afterselect",
		topictext: ",",
		topicdesc: data.string.comma
	}
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "mainimg",
			imgsrc : imgpath + "images/groupoffriends.png"
		},
		],
	}
	],
	lowertextblock:[
	{
		datahighlightflag: true,
		datahighlightcustomclass: "letshighlight",
		textclass : "lowercaption fadein",
		textdata : data.string.p2text3,
	}
	],
},
{
	//slide 4
	additionalclasscontentblock: "titlebg",
	headerblock:[
	{
		textdata : data.string.p2text1,
	}
	],
	punctuation:[
	{
		whichpunc: "comma afterselect",
		topictext: ",",
		topicdesc: data.string.comma
	}
	],
	punctuationimg:[
	{
		imgplace: "onediv",
		imgclass: "insidethediv",
		imgsrc : imgpath + "images/sunandstar.png",
		topicdesc: data.string.p2text4

	},
	{
		imgplace: "twodiv",
		imgclass: "insidethediv",
		imgsrc : imgpath + "images/musical_set.png",
		topicdesc: data.string.p2text5

	},
	{
		imgplace: "threediv",
		imgclass: "insidethediv",
		imgsrc : imgpath + "images/animals.png",
		topicdesc: data.string.p2text6

	}
	]
},
{
	additionalclasscontentblock:"blueBg",
	dndexc:[{
		imgclass: "dragImg",
		imgsrc : imgpath + "images/diy_images/sun-and-stars.png",
		// qntxt:data.string.p2text4_1,
		// dropboxclass:"droppable fadein_1",
		dragables:"dragables",
		dragableoptns:[{
			dragableoptn: "drgbl",
			punctn:data.string.fulstop
		},{
			dragableoptn: "drgbl correct_1",
			punctn:data.string.coma
		},{
			dragableoptn: "drgbl",
			punctn:data.string.qn
		},{
			dragableoptn: "drgbl",
			punctn:data.string.xclm
		},{
			dragableoptn: "drgbl",
			punctn:data.string.invcoma1
		},{
			dragableoptn: "drgbl",
			punctn:data.string.invcoma2
		}],
		multipledropables:[{
				qntxt:data.string.p2text4_1_1,
				qnTextClass:"qt1",
				withdrpbox:true,
				dropboxclass:"flxdrp flexDroppable_1"
		},{
				qntxt:data.string.p2text4_1_2,
				qnTextClass:"qt2",
		}]
	}]
},
{
	additionalclasscontentblock:"blueBg",
	dndexc:[{
		imgclass: "dragImg",
		imgsrc : imgpath + "images/diy_images/madal.png",
		qntxt:data.string.p2text5_1,
		dropboxclass:"droppable fadein_1",
		dragables:"dragables",
		dragableoptns:[{
			dragableoptn: "drgbl",
			punctn:data.string.fulstop
		},{
			dragableoptn: "drgbl correct_1",
			punctn:data.string.coma
		},{
			dragableoptn: "drgbl",
			punctn:data.string.qn
		},{
			dragableoptn: "drgbl",
			punctn:data.string.xclm
		},{
			dragableoptn: "drgbl",
			punctn:data.string.invcoma1
		},{
			dragableoptn: "drgbl correct_1",
			punctn:data.string.coma
		}],
		multipledropables:[{
				qntxt:data.string.p2text5_1_1,
				qnTextClass:"qt1",
				withdrpbox:true,
				dropboxclass:"flxdrp flexDroppable_1"
		},{
				qntxt:data.string.p2text5_1_2,
				qnTextClass:"qt2",
				withdrpbox:true,
				dropboxclass:"flxdrp flexDroppable_1"
		},{
				qntxt:data.string.p2text5_1_3,
				qnTextClass:"qt3"
		}]
	}]
},
{
	additionalclasscontentblock:"blueBg",
	dndexc:[{
		imgclass: "dragImg",
		imgsrc : imgpath + "images/diy_images/animals.png",
		qntxt:data.string.p2text6_1,
		dropboxclass:"droppable fadein_1",
		dragables:"dragables",
		dragableoptns:[{
			dragableoptn: "drgbl",
			punctn:data.string.fulstop
		},{
			dragableoptn: "drgbl correct_1",
			punctn:data.string.coma
		},{
			dragableoptn: "drgbl",
			punctn:data.string.qn
		},{
			dragableoptn: "drgbl",
			punctn:data.string.xclm
		},{
			dragableoptn: "drgbl",
			punctn:data.string.invcoma1
		},{
			dragableoptn: "drgbl correct_1",
			punctn:data.string.coma
		}],
		multipledropables:[{
				qntxt:data.string.p2text6_1_1,
				qnTextClass:"qt1",
				withdrpbox:true,
				dropboxclass:"flxdrp flexDroppable_1"
		},{
				qntxt:data.string.p2text6_1_2,
				qnTextClass:"qt2",
				withdrpbox:true,
				dropboxclass:"flxdrp flexDroppable_1"
		},{
				qntxt:data.string.p2text6_1_3,
				qnTextClass:"qt3"
		}]
	}]
}
];
// 	return content;
// }


$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	/*recalculateHeightWidth();*/

	var total_page = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
	*/
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */
 	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.hide(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.hide(0);
			$prevBtn.hide(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// islastpageflag ?
			// ole.footerNotificationHandler.lessonEndSetNotification() :
			// ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		switch(countNext){
			case 0:
				sound_player(s1_p1, 1);
			break;
			case 1:
				sound_player(s2_p2, 1);
			break;
			case 2:
				$(".lowercaption").hide(0);
				current_sound.stop();
				current_sound = s2_p3_1;
				current_sound.play();
				current_sound.bindOnce('ended', function(){
					current_sound.stop();
					$(".lowercaption").show(0);
					current_sound = s2_p3_2;
					current_sound.play();
					current_sound.bindOnce('ended', function(){
						nav_button_controls();
					});
				});
			break;
			case 3:
				sound_player(s2_p4, 1);
			break;
			case 4:
				// sound_player(s1_p5, 1);
				$(".twodiv, .threediv").hide(0);
				current_sound.stop();
				current_sound = s2_p5_1;
				current_sound.play();
				current_sound.bindOnce('ended', function(){
					current_sound.stop();
					$(".twodiv").fadeIn(200);
					current_sound = s2_p5_2;
					current_sound.play();
					current_sound.bindOnce('ended', function(){
						current_sound.stop();
						$(".threediv").fadeIn(200);
						current_sound = s2_p5_3;
						current_sound.play();
						current_sound.bindOnce('ended', function(){
							nav_button_controls();
						});
					});
				});
			case 5:
			case 6:
			case 7:
				$nextBtn.hide(0);
				randomize(".dragables");
				dragAndDrop(0);
			break;
		}
	}


		function sound_player(sound_data, nextButn){
			current_sound.stop();
			current_sound = sound_data;
			current_sound.play();
			current_sound.bindOnce('ended', function(){
				nextButn?nav_button_controls():'';
			});
		}
		function nav_button_controls(delay_ms){
			timeoutvar = setTimeout(function(){
				if(countNext==0){
					$nextBtn.show(0);
				} else if( countNext>0 && countNext == $total_page-1){
					$prevBtn.show(0);
					ole.footerNotificationHandler.pageEndSetNotification();
				} else{
					$prevBtn.show(0);
					$nextBtn.show(0);
				}
			},delay_ms);
		}
 	/*for randomizing the options*/
	function randomize(parent){
		var parent = $(parent);
		var divs = parent.children();
		while (divs.length) {
 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
	}
	function dragAndDrop(count){
		$(".drgbl").draggable({
				containment: "body",
				revert: true,
				appendTo: "body",
				zindex: 1000
		});
		$('.flexDroppable_1').droppable({
				accept : ".drgbl",
				hoverClass: "hovered",
				drop: function(event, ui) {
						if(ui.draggable.hasClass("correct_1")) {
								count+=1;
								play_correct_incorrect_sound(1);
								ui.draggable.hide(0);
								// $(".flexDroppable_1").prepend("<img class='correctImg' src='images/right.png'/>");
								$(this).prepend("<img class='correctImg' src='images/right.png'/>");
								$(this).append(ui.draggable.find("p")).addClass("correct");
								switch (countNext) {
									case 5:
										if(count===1){
											$(".dragables").addClass("avoid-clicks");
												$nextBtn.show(0);
										}
									break;
									case 6:
									case 7:
										if(count===2){
											$(".dragables").addClass("avoid-clicks");
											countNext===6?$nextBtn.show(0):ole.footerNotificationHandler.pageEndSetNotification();
										}
									break;
									default:

								}
						}
						else{
								play_correct_incorrect_sound(0);
						}
				}
		});
	}


	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

	loadTimelineProgress($total_page, countNext + 1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
		total_page = content.length;
		templateCaller();
	// });

});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";


					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

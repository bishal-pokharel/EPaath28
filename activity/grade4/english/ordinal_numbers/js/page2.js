var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";


var content=[
	// slide0
	{
		extratextblock:[{
			textdata:  data.string.p2text1,
			textclass: "diytext",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg-1'
				},
			]
		}],
	},
	// slide1
	{
		contentblockadditionalclass:'ole-background-gradient-shore',
		extratextblock:[{
			textdata:  data.string.p2text2,
			textclass: "instruction01",
		},
		{
			textdata:  data.string.p2text3,
			textclass: "instruction02",
		},
		{
			textdata:  data.string.p2text4,
			textclass: "instruction03",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "fruit01",
					imgsrc : '',
					imgid : 'fruit01'
				},
				{
					imgclass : "fruit02",
					imgsrc : '',
					imgid : 'fruit02'
				},
				{
					imgclass : "fruit03",
					imgsrc : '',
					imgid : 'fruit03'
				},
				{
					imgclass : "fruit04",
					imgsrc : '',
					imgid : 'fruit04'
				},
				{
					imgclass : "fruit05",
					imgsrc : '',
					imgid : 'fruit05'
				},
				{
					imgclass : "fruit06",
					imgsrc : '',
					imgid : 'fruit06'
				},
				{
					imgclass : "fruit07",
					imgsrc : '',
					imgid : 'fruit07'
				},
			]
		}],
	},
	// slide2
	{
		contentblockadditionalclass:'ole-background-gradient-shore',
		extratextblock:[{
			textdata:  data.string.p2text2,
			textclass: "instruction01",
		},
		{
			textdata:  data.string.p2text3,
			textclass: "instruction02",
		},
		{
			textdata:  data.string.p2text5,
			textclass: "instruction03",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "fruit01",
					imgsrc : '',
					imgid : 'fruit01'
				},
				{
					imgclass : "fruit02",
					imgsrc : '',
					imgid : 'fruit02'
				},
				{
					imgclass : "fruit03",
					imgsrc : '',
					imgid : 'fruit03'
				},
				{
					imgclass : "fruit04",
					imgsrc : '',
					imgid : 'fruit04'
				},
				{
					imgclass : "fruit05",
					imgsrc : '',
					imgid : 'fruit05'
				},
				{
					imgclass : "fruit06",
					imgsrc : '',
					imgid : 'fruit06'
				},
				{
					imgclass : "fruit07",
					imgsrc : '',
					imgid : 'fruit07'
				},
			]
		}],
	},
	// slide3
	{
		contentblockadditionalclass:'ole-background-gradient-shore',
		extratextblock:[{
			textdata:  data.string.p2text2,
			textclass: "instruction01",
		},
		{
			textdata:  data.string.p2text3,
			textclass: "instruction02",
		},
		{
			textdata:  data.string.p2text6,
			textclass: "instruction03",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "fruit01",
					imgsrc : '',
					imgid : 'fruit01'
				},
				{
					imgclass : "fruit02",
					imgsrc : '',
					imgid : 'fruit02'
				},
				{
					imgclass : "fruit03",
					imgsrc : '',
					imgid : 'fruit03'
				},
				{
					imgclass : "fruit04",
					imgsrc : '',
					imgid : 'fruit04'
				},
				{
					imgclass : "fruit05",
					imgsrc : '',
					imgid : 'fruit05'
				},
				{
					imgclass : "fruit06",
					imgsrc : '',
					imgid : 'fruit06'
				},
				{
					imgclass : "fruit07",
					imgsrc : '',
					imgid : 'fruit07'
				},
			]
		}],
	},
	// slide4
	{
		contentblockadditionalclass:'ole-background-gradient-shore',
		extratextblock:[{
			textdata:  data.string.p2text2,
			textclass: "instruction01",
		},
		{
			textdata:  data.string.p2text3,
			textclass: "instruction02",
		},
		{
			textdata:  data.string.p2text7,
			textclass: "instruction03",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "fruit01",
					imgsrc : '',
					imgid : 'fruit01'
				},
				{
					imgclass : "fruit02",
					imgsrc : '',
					imgid : 'fruit02'
				},
				{
					imgclass : "fruit03",
					imgsrc : '',
					imgid : 'fruit03'
				},
				{
					imgclass : "fruit04",
					imgsrc : '',
					imgid : 'fruit04'
				},
				{
					imgclass : "fruit05",
					imgsrc : '',
					imgid : 'fruit05'
				},
				{
					imgclass : "fruit06",
					imgsrc : '',
					imgid : 'fruit06'
				},
				{
					imgclass : "fruit07",
					imgsrc : '',
					imgid : 'fruit07'
				},
			]
		}],
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg-1", src: imgpath+"bg_diy01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy-2", src: imgpath+"blue-boy-1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fruit01", src: imgpath+"fruit01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fruit02", src: imgpath+"fruit02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fruit03", src: imgpath+"fruit03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fruit04", src: imgpath+"fruit04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fruit05", src: imgpath+"fruit05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fruit06", src: imgpath+"fruit06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fruit07", src: imgpath+"fruit07.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes

			// sounds
			{id: "sound_1", src: soundAsset+"p2_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p2_s1_1.ogg"},
			{id: "sound_3", src: soundAsset+"p2_s1.ogg"},
			{id: "sound_4", src: soundAsset+"p2_s2.ogg"},
			{id: "sound_5", src: soundAsset+"p2_s3.ogg"},
			{id: "sound_6", src: soundAsset+"p2_s4.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		// createjs.Sound.play('para-1');
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_poem_image(content, countNext);

		vocabcontroller.findwords(countNext);
		switch (countNext) {
			case 0:
				sound_play_click('sound_1');
				nav_button_controls(200);
				break;
			case 1:
				sound_play_click('sound_3');
				setTimeout(function(){
					sound_play_click('sound_2');
				},7000);
					$nextBtn.hide(0);
					checkright('.fruit01');
					break;
				case 2:
					$nextBtn.hide(0);
					sound_play_click('sound_4');
					checkright('.fruit04');
					break;
				case 3:
					$nextBtn.hide(0);
					sound_play_click('sound_5');
					checkright('.fruit06');
					break;
				case 4:
				sound_play_click('sound_6');
					checkright('.fruit02');
					break;
					default:
					nav_button_controls(200);
					break;
		}
	}

	function checkright(rightfruit){
		$('.fruit02, .fruit03, .fruit04, .fruit05, .fruit06, .fruit07,.fruit01').not(rightfruit).click(function(){
				createjs.Sound.stop();
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
				var centerY = ((position.top + height)*100)/$board.height()+'%';
				$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-30%,20%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
				$(this).css({"pointer-events":"none","background":"rgb(255,0,0)","border-radius":".5em"});
				play_correct_incorrect_sound(0);
		});
		$(rightfruit).click(function(){
			createjs.Sound.stop();
			var $this = $(this);
			var position = $this.position();
			var width = $this.width();
			var height = $this.height();
			var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
			var centerY = ((position.top + height)*100)/$board.height()+'%';
			$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-30%,20%)" src="'+imgpath +'correct.png" />').insertAfter(this);
				$(this).css({"background":"rgb(152,192,46)","border-radius":".5em"});
				$('.fruit01,.fruit02, .fruit03, .fruit04, .fruit05, .fruit06, .fruit07').css({"pointer-events":"none"});
				play_correct_incorrect_sound(1);
				nav_button_controls(200);
		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_play_click(sound_id, click_class){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			// nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_poem_image(content, count){
		if(content[count].hasOwnProperty('poemimage')){
			var imageblock = content[count].poemimage[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
			for (var i = 0; i < content.length; i++) {
				slides(i);
				$($('.totalsequence')[i]).html(i);
				$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
			"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
			}
			function slides(i){
					$($('.totalsequence')[i]).click(function(){
						countNext = i;
						templateCaller();
					});
				}
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

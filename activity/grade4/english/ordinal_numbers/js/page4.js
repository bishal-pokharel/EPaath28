var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'purple_bg',

		extratextblock:[{
			textdata:  data.string.p4text1,
			textclass: "title-1",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "girl1",
					imgsrc : '',
					imgid : 'girl1'
				}
			]
		}]
	},


	// slide1
	{
		extratextblock:[{
			textdata:  data.string.p4text2,
			textclass: "title-2",
		},
		{
			textdata:  data.string.p4text3,
			textclass: "title-3",
		},
		{
			textdata:  data.string.p4text4,
			textclass: "title-4",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg1'
				}
			]
		}]
	},

	// slide2
	{
		extratextblock:[{
			textdata:  data.string.p4text2,
			textclass: "title-2",
		},
		{
			textdata:  data.string.p4text5,
			textclass: "title-3",
		},
		{
			textdata:  data.string.p4text3,
			textclass: "title-3",
		},
		{
			textdata:  data.string.p4text4,
			textclass: "title-4",
		},
		{
			textdata:  data.string.p4text6,
			textclass: "title-4",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full bg2",
					imgsrc : '',
					imgid : 'bg2'
				},
				{
					imgclass : "bg_full bg1",
					imgsrc : '',
					imgid : 'bg1'
				}
			]
		}]
	},
	// slide3
	{
		extratextblock:[{
			textdata:  data.string.p4text7,
			textclass: "title-4",
		},
		{
			textdata:  data.string.p4text8,
			textclass: "title-5",
			datahighlightflag: true,
			datahighlightcustomclass: "thirdhigh",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full ",
					imgsrc : '',
					imgid : 'bg3'
				}
			]
		}]
	},
	// slide4
	{
		extratextblock:[{
			textdata:  data.string.p4text7,
			textclass: "title-4",
		},
		{
			textdata:  data.string.p4text9,
			textclass: "title-5",
			datahighlightflag: true,
			datahighlightcustomclass: "thirdhigh",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full ",
					imgsrc : '',
					imgid : 'bg4'
				}
			]
		}]
	},

	// slide5
	{
		extratextblock:[{
			textdata:  data.string.p4text7,
			textclass: "title-4",
		},
		{
			textdata:  data.string.p4text10,
			textclass: "title-6",
			datahighlightflag: true,
			datahighlightcustomclass: "secondhigh",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full ",
					imgsrc : '',
					imgid : 'bg5'
				}
			]
		}]
	},

	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'blue_bg',

		extratextblock:[{
			textdata:  data.string.p4text11,
			textclass: "title-7",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "girl1",
					imgsrc : '',
					imgid : 'girl2'
				}
			]
		}]
	},


	// slide7
	{
		contentblockadditionalclass:'ole-background-gradient-rose',
		extratextblock:[{
			textdata:  data.string.p4text12,
			textclass: "title-2",
		},
		{
			textdata:  data.string.p4text13,
			textclass: "title-3",
		},
		{
			textdata:  data.string.p4text14,
			textclass: "title-4",
		}]
	},

	// slide8
	{
		extratextblock:[{
			textdata:  data.string.p4text15,
			textclass: "title-4",
		},
		{
			textdata:  data.string.p4text16,
			textclass: "title-8",
			datahighlightflag: true,
			datahighlightcustomclass: "secondhigh",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full ",
					imgsrc : '',
					imgid : 'bg6'
				}
			]
		}]
	},

	// slide9
	{
		extratextblock:[{
			textdata:  data.string.p4text15,
			textclass: "title-4",
		},
		{
			textdata:  data.string.p4text17,
			textclass: "title-9",
			datahighlightflag: true,
			datahighlightcustomclass: "secondhigh",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full ",
					imgsrc : '',
					imgid : 'bg7'
				}
			]
		}]
	},

	// slide10
	{
		extratextblock:[{
			textdata:  data.string.p4text15,
			textclass: "title-4",
		},
		{
			textdata:  data.string.p4text18,
			textclass: "title-9",
			datahighlightflag: true,
			datahighlightcustomclass: "secondhigh",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full ",
					imgsrc : '',
					imgid : 'bg8'
				}
			]
		}]
	},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "girl1", src: imgpath+"asha01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg1", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg2", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg3", src: imgpath+"bg03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg4", src: imgpath+"bg04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg5", src: imgpath+"bg05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg6", src: imgpath+"bg06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg7", src: imgpath+"bg07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg8", src: imgpath+"bg08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl2", src: imgpath+"niti01.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_1", src: soundAsset+"p4_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p4_s1_1.ogg"},
			{id: "sound_3", src: soundAsset+"p4_s1.ogg"},
			{id: "sound_4", src: soundAsset+"p4_s2.ogg"},
			{id: "sound_5", src: soundAsset+"p4_s3.ogg"},
			{id: "sound_6", src: soundAsset+"p4_s4.ogg"},
			{id: "sound_7", src: soundAsset+"p4_s5.ogg"},
			{id: "sound_8", src: soundAsset+"p4_s6.ogg"},
			{id: "sound_9", src: soundAsset+"p4_s7_1.ogg"},
			{id: "sound_10", src: soundAsset+"p4_s7.ogg"},
			{id: "sound_11", src: soundAsset+"p4_s8.ogg"},
			{id: "sound_12", src: soundAsset+"p4_s9.ogg"},
			{id: "sound_13", src: soundAsset+"p4_s10.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch (countNext) {
			case 0:
				sound_player('sound_1',1);
				break;
			case 1:
				doubleHigh("colorme",12);
				sound_player('sound_3', 0);
				setTimeout(function(){
					sound_player('sound_2', 1);
				},4000);
				$('.list3').css({"left":"120%"}).animate({"left":"25%"},800);
				$('.list4').css({"left":"140%"}).animate({"left":"51%"},800);
				break;
			case 2:
				doubleHigh("colorme",24);
				sound_player('sound_4',1);
				$('.list3').animate({"left":"-55%"},800);
				$('.list4').animate({"left":"-81%"},800);
				$('.list5').css({"left":"120%"}).animate({"left":"25%"},800);
				$('.list6').css({"left":"140%"}).animate({"left":"51%"},800);
				$('.bg1').animate({"opacity":"0"},800);
				$('.bg2').css({"opacity":"0"}).animate({"opacity":"1"},800);
				break;
			case 3:
				sound_player('sound_5',1);
				$('.title-5').css({"opacity":"0"}).animate({"opacity":"1"},500);
				break;
			case 4:
				sound_player('sound_6',1);
				$('.title-5').css({"background":"rgb(0,135,58)","opacity":"0"}).animate({"opacity":"1"},500);
				break;
			case 5:
				sound_player('sound_7',1);
				$('.title-6').css({"opacity":"0"}).animate({"opacity":"1"},500);
				break;
			case 6:
				sound_player('sound_8',1);
				$('.title-7').css({"opacity":"0"}).animate({"opacity":"1"},500);
				$('.girl1').css({"width":"14%"});
				break;
			case 7:
				doubleHigh('colorme',14);
				sound_player('sound_10',0);
				setTimeout(function(){
					sound_player('sound_9', 1);
				},4100);
				$('.list5,.list6').css({"line-height":"8vmin"});
				break;
			case 8:
				sound_player('sound_11',1);
				$('.title-8').css({"opacity":"0"}).animate({"opacity":"1"},500);
				break;
			case 9:
				sound_player('sound_12',1);
				break;
			case 10:
				sound_player('sound_13',1);
				$('.title-9').css({"opacity":"0"}).animate({"opacity":"1"},500);
				break;
			default:
				nav_button_controls(1000);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	//change_src-> put 1 for girl speak first else 2 for boy-speak first, 3 for boy-3
	function conversation(class1, sound_data1, class2, sound_data2, change_src){
		$(class1).fadeIn(500, function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_data1);
			current_sound.play();
			current_sound.on("complete", function(){
				if(change_src==1){
					$('.boy').attr('src', preload.getResult('boy-1').src);
					$('.girl').attr('src', preload.getResult('girl-1').src);
				} else if(change_src==2){
					$('.boy').attr('src', preload.getResult('boy-2').src);
					$('.girl').attr('src', preload.getResult('girl-2').src);
				} else if(change_src==3){
					$('.boy').attr('src', preload.getResult('boy-3').src);
					$('.girl').attr('src', preload.getResult('girl-1').src);
				}
				$(class2).fadeIn(500, function(){
					current_sound = createjs.Sound.play(sound_data2);
					current_sound.play();
					current_sound.on("complete", function(){
						nav_button_controls(0);
						$(class1).click(function(){
							sound_player(sound_data1);
						});
						$(class2).click(function(){
							sound_player(sound_data2);
						});
					});
				});
			});
		});
	}
	function doubleHigh(classname,number){
		for(var i=0; i<number; i++){
			if(i%2==0){
				$('.'+classname).eq(i).addClass('firsthigh');
			}
			else{
				$('.'+classname).eq(i).addClass('secondhigh');
			}
		}
	}
	function sound_play_click(sound_id, click_class){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(0);
		});
	}
	function sound_player(sound_id,next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next) {nav_button_controls(1);}
		});
}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
			for (var i = 0; i < content.length; i++) {
				slides(i);
				$($('.totalsequence')[i]).html(i);
				$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
			"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
			}
			function slides(i){
					$($('.totalsequence')[i]).click(function(){
						countNext = i;
						templateCaller();
					});
				}
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

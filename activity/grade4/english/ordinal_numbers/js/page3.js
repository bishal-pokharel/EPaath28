var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		extratextblock:[{
			textdata:  data.string.p3text1,
			textclass: "diytext ",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg1'
				},
			]
		}],
	},
	// slide1
	{
		contentblockadditionalclass:'ole-background-gradient-pruplex',
		extratextblock:[{
			textdata:  data.string.p3text2,
			textclass: "instruction",
		},
		{
			textdata:  data.string.p3text3,
			textclass: "oneone ",
		},
		{
			textdata:  data.string.p3text4,
			textclass: "onetwo ",
		},
		{
			textdata:  '',
			textclass: "first1",
		},
		{
			textdata:  '',
			textclass: "second1 ",
		},
		{
			textdata:  '',
			textclass: "third1 ",
		},
		{
			textdata:  '',
			textclass: "fourth1 ",
		},
		{
			textdata:  '',
			textclass: "sixth1 ",
		},
		{
			textdata:  '',
			textclass: "seventh1 ",
		},
		{
			textdata:  '',
			textclass: "eighth1 ",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "first",
					imgsrc : '',
					imgid : 'first'
				},{
					imgclass : "second",
					imgsrc : '',
					imgid : 'second'
				},{
					imgclass : "third",
					imgsrc : '',
					imgid : 'third'
				},{
					imgclass : "fourth",
					imgsrc : '',
					imgid : 'fourth'
				},{
					imgclass : "fifth",
					imgsrc : '',
					imgid : 'fifth'
				},{
					imgclass : "sixth",
					imgsrc : '',
					imgid : 'sixth'
				},{
					imgclass : "seventh",
					imgsrc : '',
					imgid : 'seventh'
				},{
					imgclass : "eighth",
					imgsrc : '',
					imgid : 'eighth'
				},
			]
		}],
	},
	// slide2
	{
		contentblockadditionalclass:'ole-background-gradient-pruplex',
		extratextblock:[{
			textdata:  data.string.p3text2,
			textclass: "instruction",
		},
		{
			textdata:  data.string.p3text5,
			textclass: "oneone ",
		},
		{
			textdata:  data.string.p3text4,
			textclass: "onetwo ",
		},
		{
			textdata:  '',
			textclass: "first1",
		},
		{
			textdata:  '',
			textclass: "second1 ",
		},
		{
			textdata:  '',
			textclass: "third1 ",
		},
		{
			textdata:  '',
			textclass: "fourth1 ",
		},
		{
			textdata:  '',
			textclass: "sixth1 ",
		},
		{
			textdata:  '',
			textclass: "seventh1 ",
		},
		{
			textdata:  '',
			textclass: "eighth1 ",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "first",
					imgsrc : '',
					imgid : 'first'
				},{
					imgclass : "second",
					imgsrc : '',
					imgid : 'second'
				},{
					imgclass : "third",
					imgsrc : '',
					imgid : 'third'
				},{
					imgclass : "fourth",
					imgsrc : '',
					imgid : 'fourth'
				},{
					imgclass : "fifth",
					imgsrc : '',
					imgid : 'fifth'
				},{
					imgclass : "sixth",
					imgsrc : '',
					imgid : 'sixth'
				},{
					imgclass : "seventh",
					imgsrc : '',
					imgid : 'seventh'
				},{
					imgclass : "eighth",
					imgsrc : '',
					imgid : 'eighth'
				},
			]
		}],
	},
	// slide3
	{
		contentblockadditionalclass:'ole-background-gradient-pruplex',
		extratextblock:[{
			textdata:  data.string.p3text2,
			textclass: "instruction",
		},
		{
			textdata:  data.string.p3text6,
			textclass: "oneone ",
		},
		{
			textdata:  data.string.p3text4,
			textclass: "onetwo ",
		},
		{
			textdata:  '',
			textclass: "first1",
		},
		{
			textdata:  '',
			textclass: "second1 ",
		},
		{
			textdata:  '',
			textclass: "third1 ",
		},
		{
			textdata:  '',
			textclass: "fourth1 ",
		},
		{
			textdata:  '',
			textclass: "sixth1 ",
		},
		{
			textdata:  '',
			textclass: "seventh1 ",
		},
		{
			textdata:  '',
			textclass: "eighth1 ",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "first",
					imgsrc : '',
					imgid : 'first'
				},{
					imgclass : "second",
					imgsrc : '',
					imgid : 'second'
				},{
					imgclass : "third",
					imgsrc : '',
					imgid : 'third'
				},{
					imgclass : "fourth",
					imgsrc : '',
					imgid : 'fourth'
				},{
					imgclass : "fifth",
					imgsrc : '',
					imgid : 'fifth'
				},{
					imgclass : "sixth",
					imgsrc : '',
					imgid : 'sixth'
				},{
					imgclass : "seventh",
					imgsrc : '',
					imgid : 'seventh'
				},{
					imgclass : "eighth",
					imgsrc : '',
					imgid : 'eighth'
				},
			]
		}],
	},
	// slide4
	{
		contentblockadditionalclass:'ole-background-gradient-pruplex',
		extratextblock:[{
			textdata:  data.string.p3text2,
			textclass: "instruction",
		},
		{
			textdata:  data.string.p3text7,
			textclass: "oneone ",
		},
		{
			textdata:  data.string.p3text4,
			textclass: "onetwo ",
		},
		{
			textdata:  '',
			textclass: "first1",
		},
		{
			textdata:  '',
			textclass: "second1 ",
		},
		{
			textdata:  '',
			textclass: "third1 ",
		},
		{
			textdata:  '',
			textclass: "fourth1 ",
		},
		{
			textdata:  '',
			textclass: "sixth1 ",
		},
		{
			textdata:  '',
			textclass: "seventh1 ",
		},
		{
			textdata:  '',
			textclass: "eighth1 ",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "first",
					imgsrc : '',
					imgid : 'first'
				},{
					imgclass : "second",
					imgsrc : '',
					imgid : 'second'
				},{
					imgclass : "third",
					imgsrc : '',
					imgid : 'third'
				},{
					imgclass : "fourth",
					imgsrc : '',
					imgid : 'fourth'
				},{
					imgclass : "fifth",
					imgsrc : '',
					imgid : 'fifth'
				},{
					imgclass : "sixth",
					imgsrc : '',
					imgid : 'sixth'
				},{
					imgclass : "seventh",
					imgsrc : '',
					imgid : 'seventh'
				},{
					imgclass : "eighth",
					imgsrc : '',
					imgid : 'eighth'
				},
			]
		}],
	},
	// slide5
	{
		contentblockadditionalclass:'ole-background-gradient-pruplex',
		extratextblock:[{
			textdata:  data.string.p3text2,
			textclass: "instruction",
		},
		{
			textdata:  data.string.p3text8,
			textclass: "oneone ",
		},
		{
			textdata:  data.string.p3text4,
			textclass: "onetwo ",
		},
		{
			textdata:  '',
			textclass: "first1",
		},
		{
			textdata:  '',
			textclass: "second1 ",
		},
		{
			textdata:  '',
			textclass: "third1 ",
		},
		{
			textdata:  '',
			textclass: "fourth1 ",
		},
		{
			textdata:  '',
			textclass: "sixth1 ",
		},
		{
			textdata:  '',
			textclass: "seventh1 ",
		},
		{
			textdata:  '',
			textclass: "eighth1 ",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "first",
					imgsrc : '',
					imgid : 'first'
				},{
					imgclass : "second",
					imgsrc : '',
					imgid : 'second'
				},{
					imgclass : "third",
					imgsrc : '',
					imgid : 'third'
				},{
					imgclass : "fourth",
					imgsrc : '',
					imgid : 'fourth'
				},{
					imgclass : "fifth",
					imgsrc : '',
					imgid : 'fifth'
				},{
					imgclass : "sixth",
					imgsrc : '',
					imgid : 'sixth'
				},{
					imgclass : "seventh",
					imgsrc : '',
					imgid : 'seventh'
				},{
					imgclass : "eighth",
					imgsrc : '',
					imgid : 'eighth'
				},
			]
		}],
	},
	// slide6
	{
		contentblockadditionalclass:'ole-background-gradient-pruplex',
		extratextblock:[{
			textdata:  data.string.p3text2,
			textclass: "instruction",
		},
		{
			textdata:  data.string.p3text9,
			textclass: "oneone ",
		},
		{
			textdata:  data.string.p3text4,
			textclass: "onetwo ",
		},
		{
			textdata:  '',
			textclass: "first1",
		},
		{
			textdata:  '',
			textclass: "second1 ",
		},
		{
			textdata:  '',
			textclass: "third1 ",
		},
		{
			textdata:  '',
			textclass: "fourth1 ",
		},
		{
			textdata:  '',
			textclass: "sixth1 ",
		},
		{
			textdata:  '',
			textclass: "seventh1 ",
		},
		{
			textdata:  '',
			textclass: "eighth1 ",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "first",
					imgsrc : '',
					imgid : 'first'
				},{
					imgclass : "second",
					imgsrc : '',
					imgid : 'second'
				},{
					imgclass : "third",
					imgsrc : '',
					imgid : 'third'
				},{
					imgclass : "fourth",
					imgsrc : '',
					imgid : 'fourth'
				},{
					imgclass : "fifth",
					imgsrc : '',
					imgid : 'fifth'
				},{
					imgclass : "sixth",
					imgsrc : '',
					imgid : 'sixth'
				},{
					imgclass : "seventh",
					imgsrc : '',
					imgid : 'seventh'
				},{
					imgclass : "eighth",
					imgsrc : '',
					imgid : 'eighth'
				},
			]
		}],
	},
	// slide7
	{
		contentblockadditionalclass:'ole-background-gradient-pruplex',
		extratextblock:[{
			textdata:  data.string.p3text2,
			textclass: "instruction",
		},
		{
			textdata:  data.string.p3text10,
			textclass: "oneone ",
		},
		{
			textdata:  data.string.p3text4,
			textclass: "onetwo ",
		},
		{
			textdata:  '',
			textclass: "first1",
		},
		{
			textdata:  '',
			textclass: "second1 ",
		},
		{
			textdata:  '',
			textclass: "third1 ",
		},
		{
			textdata:  '',
			textclass: "fourth1 ",
		},
		{
			textdata:  '',
			textclass: "sixth1 ",
		},
		{
			textdata:  '',
			textclass: "seventh1 ",
		},
		{
			textdata:  '',
			textclass: "eighth1 ",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "first",
					imgsrc : '',
					imgid : 'first'
				},{
					imgclass : "second",
					imgsrc : '',
					imgid : 'second'
				},{
					imgclass : "third",
					imgsrc : '',
					imgid : 'third'
				},{
					imgclass : "fourth",
					imgsrc : '',
					imgid : 'fourth'
				},{
					imgclass : "fifth",
					imgsrc : '',
					imgid : 'fifth'
				},{
					imgclass : "sixth",
					imgsrc : '',
					imgid : 'sixth'
				},{
					imgclass : "seventh",
					imgsrc : '',
					imgid : 'seventh'
				},{
					imgclass : "eighth",
					imgsrc : '',
					imgid : 'eighth'
				},
			]
		}],
	},
	//slide8
	{
		contentblockadditionalclass:'ole-background-gradient-pruplex',
		extratextblock:[{
			textdata:  data.string.job,
			textclass: "instruction",
		},
		{
			textdata:  data.string.p3text4,
			textclass: "onetwo ",
		},
		{
			textdata:  '',
			textclass: "first1",
		},
		{
			textdata:  '',
			textclass: "second1 ",
		},
		{
			textdata:  '',
			textclass: "third1 ",
		},
		{
			textdata:  '',
			textclass: "fourth1 ",
		},
		{
			textdata:  '',
			textclass: "sixth1 ",
		},
		{
			textdata:  '',
			textclass: "seventh1 ",
		},
		{
			textdata:  '',
			textclass: "eighth1 ",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "first",
					imgsrc : '',
					imgid : 'first'
				},{
					imgclass : "second",
					imgsrc : '',
					imgid : 'second'
				},{
					imgclass : "third",
					imgsrc : '',
					imgid : 'third'
				},{
					imgclass : "fourth",
					imgsrc : '',
					imgid : 'fourth'
				},{
					imgclass : "fifth",
					imgsrc : '',
					imgid : 'fifth'
				},{
					imgclass : "sixth",
					imgsrc : '',
					imgid : 'sixth'
				},{
					imgclass : "seventh",
					imgsrc : '',
					imgid : 'seventh'
				},{
					imgclass : "eighth",
					imgsrc : '',
					imgid : 'eighth'
				},
			]
		}],
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bg1", src: imgpath+"bg_diy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "first", src: imgpath+"elephant.png", type: createjs.AbstractLoader.IMAGE},
			{id: "second", src: imgpath+"monkey.png", type: createjs.AbstractLoader.IMAGE},
			{id: "third", src: imgpath+"girafee.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fourth", src: imgpath+"tiger.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fifth", src: imgpath+"cow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sixth", src: imgpath+"goat.png", type: createjs.AbstractLoader.IMAGE},
			{id: "seventh", src: imgpath+"rabbit.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eighth", src: imgpath+"parrot.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes

			// sounds
			{id: "sound_1", src: soundAsset+"p3_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p3_s1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_poem_image(content, countNext);

		$('.oneone').draggable({
								containment : ".generalTemplateblock",
								revert : true,
								cursor : "move",
								zIndex: 100000,
						});
			switch (countNext) {
				case 0:
						sound_play_click('sound_1');
						nav_button_controls(1000);
						break;
				case 1:
							createjs.Sound.stop();
							sound_play_click('sound_2');
							dragges('third1');
							break;
				case 2:
							dragges('seventh1');
							break;
				case 3:
							dragges('second1');
							break;
				case 4:
							dragges('fourth1');
							break;
				case 5:
							dragges('first1');
							break;
				case 6:
							dragges('eighth1');
							break;
				case 7:
							dragges('sixth1');
							break;
				case 8:
						$('.instruction').css({"top":"6%","font-size":"4.6vmin"});
						finalanime("first","4","1st");
						finalanime("second","28","2nd");
						finalanime("third","52","3rd");
						finalanime("fourth","76","4th");
						finalanime("fifth","4","5th");
						finalanime("onetwo","4","5th");
						finalanime("sixth","28","6th");
						finalanime("seventh","52","7th");
						finalanime("eighth","76","8th");
					nav_button_controls(1500);

					break;
				default:
							nav_button_controls(1000);
							break;
			}
		function dragges(correctdrop){
			$(".first1,.second1,.third1,.fourth1,.sixth1,.seventh1,.eighth1").droppable({
									accept: ".oneone",
									// activeClass: "borderclass",
									over: function(event, ui) {
												 $(this).addClass("borderclass");
								 		 },
										 out: function(event, ui) {
											 $(this).removeClass("borderclass");
							     },
									drop: function (event, ui){
										if($(this).hasClass(correctdrop))
										{
											createjs.Sound.stop();
											$(this).removeClass("borderclass");
											var $this = $(this);
											var position = $this.position();
											var width = $this.width();
											var height = $this.height();
											var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
											var centerY = ((position.top + height)*100)/$board.height()+'%';
											$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(80%,-95%)" src="'+imgpath +'correct.png" />').insertAfter(this);
												nav_button_controls(1000);
												var texto=ui.draggable.text();
												ui.draggable.css({"display":"none"});
												$('.'+correctdrop).html(texto).css({"background":"rgb(190,214,47)"});
												play_correct_incorrect_sound(1);
												// ui.draggable({ revert: 'invalid' });
										}
										else{
												var $this = $(this);
												$(this).removeClass("borderclass");
												var position = $this.position();
												var width = $this.width();
												var height = $this.height();
												var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
												var centerY = ((position.top + height)*100)/$board.height()+'%';
												$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(80%,-95%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
												play_correct_incorrect_sound(0);
												$(this).css({"background":"rgb(168, 33, 36)"});
										}
									}
							});
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function finalanime(txtclass,leftpos,htmlval){
		$('.'+txtclass+',.'+txtclass+'1').html(htmlval).css({"left":"100%","font-size":"4vmin"}).animate({"left":leftpos+"%"},1200,"linear");
	}

	function sound_play_click(sound_id, click_class){
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			// nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_poem_image(content, count){
		if(content[count].hasOwnProperty('poemimage')){
			var imageblock = content[count].poemimage[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
			for (var i = 0; i < content.length; i++) {
				slides(i);
				$($('.totalsequence')[i]).html(i);
				$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
			"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
			}
			function slides(i){
					$($('.totalsequence')[i]).click(function(){
						countNext = i;
						templateCaller();
					});
				}
		*/
	}


	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

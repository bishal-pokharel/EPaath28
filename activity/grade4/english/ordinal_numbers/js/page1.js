var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		extratextblock:[{
			textdata: data.lesson.chapter,
			textclass: "lesson-title chelseamarket",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "1st",
					imgsrc : '',
					imgid : '1st'
				},
				{
					imgclass : "2nd",
					imgsrc : '',
					imgid : '2nd'
				},
				{
					imgclass : "3rd",
					imgsrc : '',
					imgid : '3rd'
				},
				{
					imgclass : "4th",
					imgsrc : '',
					imgid : '4th'
				},{
					imgclass : "5th",
					imgsrc : '',
					imgid : '5th'
				},
				{
					imgclass : "6th",
					imgsrc : '',
					imgid : '6th'
				},
			]
		}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'blue-bg',

		extratextblock:[{
			textdata:  data.string.p1text1,
			textclass: "toptext",
		},{
			textdata:  data.string.p1text2,
			textclass: "list1",
			datahighlightflag: true,
			datahighlightcustomclass: "colorme",
		},{
			textdata:  data.string.p1text3,
			textclass: "list2",
			datahighlightflag: true,
			datahighlightcustomclass: "colorme",
		},{
			textdata:  data.string.p1txt1_2,
			textclass: "btmtxt",
		}],
			imageblock:[{
			imagestoshow : [
				{
					imgclass : "char1",
					imgsrc : '',
					imgid : 'girl1'
				}
			]
		}]

	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'blue-bg',

		extratextblock:[{
			textdata:  data.string.p1text4,
			textclass: "midtext",
		}
		],
			imageblock:[{
			imagestoshow : [
				{
					imgclass : "char1",
					imgsrc : '',
					imgid : 'girl1'
				}
			]
		}]

	},
	// slide3
	{
		extratextblock:[{
			textdata: data.string.p1text5,
			textclass: "toptext bigtext",
			datahighlightflag: true,
			datahighlightcustomclass: "secondhigh",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-2'
				},
				{
					imgclass : "girl1 wbg",
					imgsrc : '',
					imgid : 'girl1'
				},
				{
					imgclass : "girla1",
					imgsrc : '',
					imgid : 'girla1'
				},
				{
					imgclass : "boy1",
					imgsrc : '',
					imgid : 'boy1'
				},
			]
		}]
	},
	// slide4
	{
		extratextblock:[{
			textdata: data.string.p1text6,
			textclass: "toptext bigtext",
			datahighlightflag: true,
			datahighlightcustomclass: "secondhigh",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-2'
				},
				{
					imgclass : "girl1",
					imgsrc : '',
					imgid : 'girl1'
				},
				{
					imgclass : "girla1 wbg",
					imgsrc : '',
					imgid : 'girla1'
				},
				{
					imgclass : "boy1",
					imgsrc : '',
					imgid : 'boy1'
				},
			]
		}]
	},
	// slide5
	{
		extratextblock:[{
			textdata: data.string.p1text7,
			textclass: "toptext bigtext",
			datahighlightflag: true,
			datahighlightcustomclass: "secondhigh",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-2'
				},
				{
					imgclass : "girl1",
					imgsrc : '',
					imgid : 'girl1'
				},
				{
					imgclass : "girla1",
					imgsrc : '',
					imgid : 'girla1'
				},
				{
					imgclass : "boy1 wbg",
					imgsrc : '',
					imgid : 'boy1'
				},
			]
		}]
	},
	// slide6
	{
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-2'
				},
				{
					imgclass : "s5-1",
					imgsrc : '',
					imgid : 'girl2'
				},
				{
					imgclass : "s5-2",
					imgsrc : '',
					imgid : 'girla2'
				},
				{
					imgclass : "s5-3",
					imgsrc : '',
					imgid : 'boy2'
				},
				{
					imgclass : "girl1",
					imgsrc : '',
					imgid : 'girl1'
				},
				{
					imgclass : "girla1",
					imgsrc : '',
					imgid : 'girla1'
				},
				{
					imgclass : "boy1 wbg",
					imgsrc : '',
					imgid : 'boy1'
				},
			]
		}]
	},
	// slide6
	{
		extratextblock:[{
			textdata: data.string.p1text8,
			textclass: "toptext whitebig",
			datahighlightflag: true,
			datahighlightcustomclass: "secondhigh",
		},{
			textdata: data.string.rumi,
			textclass: "first",
		},{
			textdata: data.string.asha,
			textclass: "sixth",
		},],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "1st",
					imgsrc : '',
					imgid : '1st'
				},
				{
					imgclass : "2nd",
					imgsrc : '',
					imgid : '2nd'
				},
				{
					imgclass : "3rd",
					imgsrc : '',
					imgid : '3rd'
				},
				{
					imgclass : "4th",
					imgsrc : '',
					imgid : '4th'
				},{
					imgclass : "5th",
					imgsrc : '',
					imgid : '5th'
				},
				{
					imgclass : "6th",
					imgsrc : '',
					imgid : '6th'
				},
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-3'
				},
			]
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bg-1", src: imgpath+"park01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-2", src: imgpath+"park02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-3", src: imgpath+"school_bus.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl1", src: imgpath+"asha01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl2", src: imgpath+"asha02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girla1", src: imgpath+"niti01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girla2", src: imgpath+"niti02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy1", src: imgpath+"sagar.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy2", src: imgpath+"sagar01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "1st", src: imgpath+"04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "2nd", src: imgpath+"02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "3rd", src: imgpath+"06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "4th", src: imgpath+"01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "5th", src: imgpath+"05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "6th", src: imgpath+"03.png", type: createjs.AbstractLoader.IMAGE},
			// soundsa
			{id: "sound_1", src: soundAsset+"p1_s0.ogg"},
			{id: "sound_1_1", src: soundAsset+"p1_s1_1.ogg"},
			{id: "sound_2", src: soundAsset+"p1_s1_1.ogg"},
			{id: "sound_3", src: soundAsset+"p1_s1.ogg"},
			{id: "sound_3_1", src: soundAsset+"p1_s1_sec.ogg"},
			{id: "sound_4", src: soundAsset+"p1_s2.ogg"},
			{id: "sound_5", src: soundAsset+"p1_s3.ogg"},
			{id: "sound_6", src: soundAsset+"p1_s4.ogg"},
			{id: "sound_7", src: soundAsset+"p1_s5.ogg"},
			{id: "sound_8", src: soundAsset+"p1_s6.ogg"},
			{id: "sound_9", src: soundAsset+"p1_s7.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_card_image(content, countNext);
		vocabcontroller.findwords(countNext);
		switch(countNext) {
			case 0:
				sound_player('sound_1', 1);
				break;
			case 1:
				doubleHigh("colorme",20);
				createjs.Sound.stop();
				$('.btmtxt').css({"opacity":"0"});
				$('.lista,.listb').children().css({"opacity":"0"});
				current_sound = createjs.Sound.play("sound_3");
				current_sound.play();
				current_sound.on("complete", function(){
						current_sound_1 = createjs.Sound.play("sound_2");
						for (var i = 0; i < 5; i++) {
							$('.lista').find('li:nth-child('+(i+1)+')').delay(i*1700).animate({"opacity":"1","z-index":"99"},5000);
							$('.listb').find('li:nth-child('+(i+1)+')').delay(7200+i*1700).animate({"opacity":"1","z-index":"99"},5000);
						}
						current_sound_1.on("complete", function(){
							$('.btmtxt').css({"opacity":"1"});
							current_sound_2 = createjs.Sound.play("sound_3_1");
							current_sound_2.play();
							current_sound_2.on("complete", function(){
								nav_button_controls(0);
							});
						});
				});
				$('.girl1,.girla1,.boy1').animate({"opacity":"0"},1000);
				$('.s3-1,.s3-2,.s3-3').css({"opacity":"0"}).animate({"opacity":"1"},1000);
				break;
			case 2:
			sound_player('sound_4',1);
				$('.midtext').css({"opacity":"0"}).animate({"opacity":"1"},1000);
				break;
			case 3:
				sound_player('sound_5',1);
				break;
			case 4:
				sound_player('sound_6',1);
				$('.s3-1,.s3-2,.s3-3').css({"opacity":"0"}).animate({"opacity":"1"},1000);
			break;
			case 5:
				sound_player('sound_7',1);
				break;
			case 6:
				$('.girl1, .girla1, .boy1').animate({"opacity":"0"},1000);
				$('.s5-1,.s5-2,.s5-3').css({"opacity":"0"}).animate({"opacity":"1"},1000);
				$prevBtn.show(0);
				$nextBtn.show(0);
				break;
			case 7:
				sound_player('sound_8',1);
				break;
			default:
				$prevBtn.show(0);
				// sound_player('sound_2');
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next) {nav_button_controls(100);}
		});
	}


	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_card_image(content, count){
		if(content[count].hasOwnProperty('card')){
			var card = content[count].card;
			for(var i=0; i<card.length; i++){
				var image_src = preload.getResult(card[i].imgid).src;
				console.log(image_src);
				var classes_list = card[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function doubleHigh(classname,number){
		for(var i=0; i<number; i++){
			if(i%2==0){
				$('.'+classname).eq(i).addClass('firsthigh');
			}
			else{
				$('.'+classname).eq(i).addClass('secondhigh');
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}
	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});
	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";

var content1=[
 //ex1
{
	extratextblock:[
		{
			textclass: "excp1",
			textdata: data.string.excp1
		}],
	exerciseblock: [
		{
			quesimg:[
			{
				imgclass: "p1img",
				imgid: "table"					
			},
			{
				imgclass: "strbg",
				imgid: "strbg"					
			}
			]
		}
	]
},
//ex0
{
    additionalclasscontentblock:"bg",
extratextblock:[
{
	textclass: "excopt",
	textdata: data.string.excopt
}],
exerciseblock: [
	{
		datahighlightflag: true,
		datahighlightcustomclass: "hightext1",
	textdata: data.string.excq1,
	//containerclass: 'container-2',
	quesimg:[
	{
		imgclass: "img1",
		imgid: "table"					
	},
	 {
	 	 imgclass:"inc",
	 	 imgid:"incorrect"
	 },
	 {
	 	 imgclass:"clickimg hand ",
	 	 imgid:"hand"
	 },
	 {
	 	 imgclass:"clickimg img2 ",
	 	 imgid:'table1'
	 }
	],
	exeoptions: [
		{
			forshuffle: "class1",
			optdata: data.string.xq1op1,
		},
		{
			forshuffle: "class2",
			optdata: data.string.xq1op2,
		},
		{
			forshuffle: "class3",
			optdata: data.string.xq1op3,
		},
		{
			forshuffle: "class4",
					optdata: data.string.xq1op4,
				}],
		}
	]
},	
 //ex1
{
	
extratextblock:[
{
	textclass: "excopt",
	textdata: data.string.excopt
}],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "correct",
			imgid : 'correct',
			imgsrc: ""
		}
	]
}],
	exerciseblock: [
		{
			datahighlightflag: true,
			datahighlightcustomclass: "hightext1",
			textdata: data.string.excq2,
			containerclass: 'container-2',
			quesimg:[
			{
				imgclass: "img1",
				imgid: "table"					
			},
			 {
				  imgclass:"inc",
				  imgid:"incorrect"
			 },
			 {
				  imgclass:"hand clickimg",
				  imgid:"hand"
			 },
		 {
			  imgclass:"clickimg img2",
			  imgid:'table1'
		 }
			],					
			exeoptions: [
				{
					forshuffle: "class1",
					optdata: data.string.xq2op1,
				},
				{
					forshuffle: "class2",
					optdata: data.string.xq2op2,
				},
				{
					forshuffle: "class3",
					optdata: data.string.xq2op3,
				},
				{
					forshuffle: "class4",
					optdata: data.string.xq2op4,
				}],
		}
	]
},

//ex2
{
	
	extratextblock:[
	{
		textclass: "excopt",
		textdata: data.string.excopt
	}],
	imageblock:[{
	imagestoshow:[
		{
			imgclass: "correct",
			imgid : 'correct',
			imgsrc: ""
		}
	]
}],
	exerciseblock: [
		{
			datahighlightflag: true,
			datahighlightcustomclass: "hightext1",
			textdata: data.string.excq3,
			containerclass: 'container-2',
			quesimg:[
			{
				imgclass: "img1",
				imgid: "table"					
			},
			 {
				  imgclass:"inc",
				  imgid:"incorrect"
			 },
			 {
				  imgclass:"hand clickimg",
				  imgid:"hand"
			 },
		 {
			  imgclass:"clickimg img2",
			  imgid:'table1'
		 }
			],				
			exeoptions: [
				{
					forshuffle: "class1",
					optdata: data.string.xq3op1,
				},
				{
					forshuffle: "class2",
					optdata: data.string.xq3op2,
				},
				{
					forshuffle: "class3",
					optdata: data.string.xq3op3,
				},
				{
					forshuffle: "class4",
					optdata: data.string.xq3op4,
				}],
		}
	]
},

//ex3
{
	 
	extratextblock:[
	{
		textclass: "excopt",
		textdata: data.string.excopt
	}],
	imageblock:[{
	imagestoshow:[
		{
			imgclass: "correct",
			imgid : 'correct',
			imgsrc: ""
		}
	]
}],
	exerciseblock: [
		{
			datahighlightflag: true,
			datahighlightcustomclass: "hightext1",
			textdata: data.string.excq4,
			containerclass: 'container-2',
			quesimg:[
			{
				imgclass: "img1",
				imgid: "table"					
			},
			 {
				  imgclass:"inc",
				  imgid:"incorrect"
			 },
			 {
				  imgclass:"hand clickimg",
				  imgid:"hand"
			 },
		 {
			  imgclass:"clickimg img2",
			  imgid:'table1'
		 }
			],			
			exeoptions: [
				{
					forshuffle: "class1",
					optdata: data.string.xq4op1,
				},
				{
					forshuffle: "class2",
					optdata: data.string.xq4op2,
				},
				{
					forshuffle: "class3",
					optdata: data.string.xq4op3,
				},
				{
					forshuffle: "class4",
					optdata: data.string.xq4op4,
				}],
		}
	]
},

//ex4
{
	
	extratextblock:[
	{
		textclass: "excopt",
		textdata: data.string.excopt
	}],
	imageblock:[{
	imagestoshow:[
		{
			imgclass: "correct",
			imgid : 'correct',
			imgsrc: ""
		}
	]
}],
	exerciseblock: [
		{
			datahighlightflag: true,
			datahighlightcustomclass: "hightext1",
			textdata: data.string.excq5,
			containerclass: 'container-2',
			quesimg:[
			{
				imgclass: "img1",
				imgid: "table"					
			},
			 {
				  imgclass:"inc",
				  imgid:"incorrect"
			 },
			 {
				  imgclass:"hand clickimg",
				  imgid:"hand"
			 },
		 {
			  imgclass:"clickimg img2",
			  imgid:'table1'
		 }
			],				
			exeoptions: [
				{
					forshuffle: "class1",
					optdata: data.string.xq5op1,
				},
				{
					forshuffle: "class2",
					optdata: data.string.xq5op2,
				},
				{
					forshuffle: "class3",
					optdata: data.string.xq5op3,
				},
				{
					forshuffle: "class4",
					optdata: data.string.xq5op4,
				}],
		}
	]
},

//ex5
{
	
	extratextblock:[
	{
		textclass: "excopt",
		textdata: data.string.excopt
	}],
	imageblock:[{
	imagestoshow:[
		{
			imgclass: "correct",
			imgid : 'correct',
			imgsrc: ""
		}
	]
}],
	exerciseblock: [
		{
			datahighlightflag: true,
			datahighlightcustomclass: "hightext1",
			textdata: data.string.excq6,
			containerclass: 'container-2',
			quesimg:[
			{
				imgclass: "img1",
				imgid: "table"					
			},
			 {
				  imgclass:"inc",
				  imgid:"incorrect"
			 },
			 {
				  imgclass:"hand clickimg",
				  imgid:"hand"
			 },
		 {
			  imgclass:"clickimg img2",
			  imgid:'table1'
		 }
			],
			exeoptions: [
				{
					forshuffle: "class1",
					optdata: data.string.xq6op1,
				},
				{
					forshuffle: "class2",
					optdata: data.string.xq6op2,
				},
				{
					forshuffle: "class3",
					optdata: data.string.xq6op3,
				},
				{
					forshuffle: "class4",
					optdata: data.string.xq6op4,
				}],
		}
	]
},

//ex6
{
	
	extratextblock:[
	{
		textclass: "excopt",
		textdata: data.string.excopt
	}],
	imageblock:[{
	imagestoshow:[
		{
			imgclass: "correct",
			imgid : 'correct',
			imgsrc: ""
		}
	]
}],
	exerciseblock: [
		{
			datahighlightflag: true,
			datahighlightcustomclass: "hightext1",
			textdata: data.string.excq7,
			containerclass: 'container-2',
			quesimg:[
			{
				imgclass: "img1",
				imgid: "table"					
			},
			 {
				  imgclass:"inc",
				  imgid:"incorrect"
			 },
			 {
				  imgclass:"hand clickimg",
				  imgid:"hand"
			 },
		 {
			  imgclass:"clickimg img2",
			  imgid:'table1'
		 }
			],
			exeoptions: [
				{
					forshuffle: "class1",
					optdata: data.string.xq7op1,
				},
				{
					forshuffle: "class2",
					optdata: data.string.xq7op2,
				},
				{
					forshuffle: "class3",
					optdata: data.string.xq7op3,
				},
				{
					forshuffle: "class4",
					optdata: data.string.xq7op4,
				}],
		}
	]
},

//ex7
{
	
	extratextblock:[
	{
		textclass: "excopt",
		textdata: data.string.excopt
	}],
	imageblock:[{
	imagestoshow:[
		{
			imgclass: "correct",
			imgid : 'correct',
			imgsrc: ""
		}
	]
}],
	exerciseblock: [
		{
			datahighlightflag: true,
			datahighlightcustomclass: "hightext1",
			textdata: data.string.excq8,
			containerclass: 'container-2',
			quesimg:[
			{
				imgclass: "img1",
				imgid: "table"					
			},
			 {
				  imgclass:"inc",
				  imgid:"incorrect"
			 },
			 {
				  imgclass:"hand clickimg",
				  imgid:"hand"
			 },
		 {
			  imgclass:"clickimg img2",
			  imgid:'table1'
		 }
			],
			exeoptions: [
				{
					forshuffle: "class1",
					optdata: data.string.xq8op1,
				},
				{
					forshuffle: "class2",
					optdata: data.string.xq8op2,
				},
				{
					forshuffle: "class3",
					optdata: data.string.xq8op3,
				},
				{
					forshuffle: "class4",
					optdata: data.string.xq8op4,
				}],
		}
	]
},

//ex8
{
	
	extratextblock:[
	{
		textclass: "excopt",
		textdata: data.string.excopt
	}],
	imageblock:[{
	imagestoshow:[
		{
			imgclass: "correct",
			imgid : 'correct',
			imgsrc: ""
		}
	]
}],
	exerciseblock: [
		{
			datahighlightflag: true,
			datahighlightcustomclass: "hightext1",
			textdata: data.string.excq9,
			containerclass: 'container-2',
				quesimg:[
			{
				imgclass: "img1",
				imgid: "table"					
			},
			 {
				  imgclass:"inc",
				  imgid:"incorrect"
			 },
			 {
				  imgclass:"hand clickimg",
				  imgid:"hand"
			 },
		 {
			  imgclass:"clickimg img2",
			  imgid:'table1'
		 }
			],
			exeoptions: [
				{
					forshuffle: "class1",
					optdata: data.string.xq9op1,
				},
				{
					forshuffle: "class2",
					optdata: data.string.xq9op2,
				},
				{
					forshuffle: "class3",
					optdata: data.string.xq9op3,
				},
				{
					forshuffle: "class4",
					optdata: data.string.xq9op4,
				}],
		}
	]
},

//ex9
{
	
	extratextblock:[
	{
		textclass: "excopt",
		textdata: data.string.excopt
	}],
	imageblock:[{
	imagestoshow:[
		{
			imgclass: "correct",
			imgid : 'correct',
			imgsrc: ""
		}
	]
}],
	exerciseblock: [
		{
			datahighlightflag: true,
			datahighlightcustomclass: "hightext1",
			textdata: data.string.excq10,
			containerclass: 'container-2',
			quesimg:[
			{
				imgclass: "img1",
				imgid: "table"					
			},
			 {
				  imgclass:"inc",
				  imgid:"incorrect"
			 },
			 {
				  imgclass:"hand clickimg",
				  imgid:"hand"
			 },
			 {
				  imgclass:"clickimg img2",
				  imgid:'table1'
			 }
			],
			exeoptions: [
				{
					forshuffle: "class1",
					optdata: data.string.xq10op1,
				},
				{
					forshuffle: "class2",
					optdata: data.string.xq10op2,
				},
				{
					forshuffle: "class3",
					optdata: data.string.xq10op3,
				},
				{
					forshuffle: "class4",
					optdata: data.string.xq10op4,
				}],
		}
	]
}


];
//content1.shufflearray();
var content = content1;


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;


	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	
	var preload;
	var timeoutvar = null;
	var current_sound;
	
	var scoring = new EggTemplate(10);
	
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "body", src: imgpath+"clock2/clock.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hrhand", src: imgpath+"clock2/hr_hand.png", type: createjs.AbstractLoader.IMAGE},
			{id: "minhand", src: imgpath+"clock2/min_hand.png", type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: "images/wrong.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hand", src:imgpath+"hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "table", src:imgpath+"table.png", type: createjs.AbstractLoader.IMAGE},
			{id: "table1", src:imgpath+"table.png", type: createjs.AbstractLoader.IMAGE},
			{id: "strbg", src:imgpath+"bgbgbg.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
            {id: "sound_1", src: soundAsset+"ex.ogg"},

        ];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		scoring.init(10);
		templateCaller();
	}
	//initialize
	init();

	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	/*=====  End of data highlight function  ======*/

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templatecaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templatecaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
	/**
	 How to:
	 - Just call instructionblockcontroller() from the template
	 */


	/**
	 What it does:
	 - It inserts and handles closing and opening of instruction block
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}
	   //for shuffling al except first page
		 var array=[0];
	     var array1=[1,2,3,4,5,6,7,8,9,10];	 
	     array1.shufflearray();
	     var narr=array.concat(array1);
	     console.log(narr);
    function sound_player(sound_id, next){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function(){
            if(next)
                $nextBtn.show(0);
        });
    }
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[narr[countNext]]);
		$board.html(html);
		texthighlight($board);
		
		put_image(content, countNext);
		put_image2(content, countNext);
		$('.correct-icon').attr('src', preload.getResult('correct').src);
		$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);
		
		scoring.numberOfQuestions(10);
		countNext==0?sound_player("sound_1",true):"";
		/*for randomizing the options*/
		var parent = $(".optionsdiv");
		var divs = parent.children();
		while (divs.length) {
			parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
		var ansClicked = false;
		var wrngClicked = false;
		$('.correct-answer').attr('src', preload.getResult('correct').src);
		$('.incorrect-answer').attr('src', preload.getResult('incorrect').src);
	

	
	
	
		$(".buttonsel").click(function(){
			$(this).removeClass('forhover');
				if(ansClicked == false){
					/*class 1 is always for the right answer. updates scoreboard and disables other click if
					right answer is clicked*/
					if($(this).hasClass("class1")){
						if(wrngClicked == false){
							scoring.update(true);
						}
						play_correct_incorrect_sound(1);
						$(this).css({
							"background": "#bed62fff",
							"border":"5px solid #deef3c",
							"color":"white",
							'pointer-events': 'none'
						});
						$(this).siblings(".correct-answer").show(0);
						$(this).children(".correct-answer").show(0);
						$('.buttonsel').removeClass('forhover forhoverimg');
						$('.buttonsel').removeClass('clock-hover');
						ansClicked = true;
						if(countNext != $total_page)
						$nextBtn.show(0);
					}
					else{
						scoring.update(false);
						play_correct_incorrect_sound(0);
						$(this).css({
							"background":"#FF0000",
							"border":"5px solid #980000",
							"color":"white",
							'pointer-events': 'none'
						});
						$(this).siblings(".incorrect-answer").show(0);
						$(this).children(".correct-answer").show(0);
						wrngClicked = true;
					}
				}
			}); 
			
		switch(countNext){
			case 0:
			$('#num_ques').hide(0);
			$('.scoreboard').css('display','none');
			$('.SequenceTimeLine').css('display','none');
			break;
			case 1:
			$('.scoreboard').css('display','block');
			extraimage();
			break;
			case 2:
			extraimage();
			break
			case 3:
			extraimage();
			break;
			case 4:
			extraimage();
			break;
			case 5:
			extraimage();
			break;
			case 6:
			extraimage();
			break;
			case 7:
			extraimage();
			break;
			case 8:
			extraimage();
			break;
			case 9:
			extraimage();
			break;
			case 10:
			extraimage();
			break;		
		}
		
		$("#num_ques").text(countNext+". ");


	}
	function extraimage(){
		$('.inc').hide(0);
		$('.img1').hide(0);
			$('.clickimg').click(function(){
              $('.img1').show(0);
              $('.hand').hide(0);
              $('.img2').hide(0);
	          		cross();
           		});
	}
   function cross(){
       	$('.inc').show(0);
       	$('.inc').click(function(){
	       	$('.inc').hide(0);
			$('.img1').hide(0);
			$('.hand').show(0);
			$('.img2').show(0);
   			$('.inc').hide(0);
       	});
   }
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
		if(content[count].exerciseblock[0].hasOwnProperty('quesimg')){
			for(var i = 0; i < content[count].exerciseblock[0].quesimg.length; i++){
				var quesimg = content[count].exerciseblock[0].quesimg[i];
				var id = quesimg.imgid;
				$("#"+ id).attr("src", preload.getResult(id).src);
				
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('popupblock')){
			var imageClass = content[count].popupblock;
			for(var i=0; i<imageClass.length; i++){
				var image_src = preload.getResult(imageClass[i].imgid).src;
				//get list of classes
				var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	
	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		generalTemplate();
		// scoringg purpose code ends
		loadTimelineProgress($total_page,countNext+1);
	}

	$nextBtn.on('click', function() {
		countNext++;
		if(countNext==0 || countNext==1){
		} else{
			scoring.gotoNext();
		}
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
});

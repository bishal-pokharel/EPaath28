var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg1",
	extratextblock:[
		{
			textclass: "s0title",
			textdata: data.lesson.chapter
		}],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "coverpage",
				imgid : 'coverpage',
				imgsrc: ""
			}
		]
	}]
},
// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg1",

	    extratextblock:[
		{
			textclass: "s1txt",
			textdata: data.string.s1txt
		},
		{
			textclass: "chaptern",
			textdata: data.string.ttable
		}
		],
			imageblock:[{
				imagestoshow:[
					{
						imgclass: "coverpageintro",
						imgid : 'coverpageintro',
						imgsrc: ""
					}
				]
			}]

},
// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: " ",
	 extratextblock:[
		{
			textclass: "s2txt",
			textdata: data.string.s2txt
		},
		{
			textclass: "chaptern",
			textdata: data.string.ttable
		}
	],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "coverpageintro",
					imgid : 'coverpageintro',
					imgsrc: ""
				}
			]
		}]
},
// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: " ",
	extratextblock:[
	{
		textclass:"p1s2txtbox1",
		textdata:data.string.p1s2text2
	},
		{
		textclass:"g4table",
		textdata:data.string.g4table
	}
	],
	flexblockcontainers:[
	{
		flexblockadditionalclass: "flexcontainerblock100",
			flexblock:[
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top",
					textdata:data.string.Period
				   },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.First
				    },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.Second
				    },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.Third
				    },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.Fourth
				    },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.break
				    },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.Fifth
				    },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.Sixth
				    },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.Seventh
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top",
					textdata:data.string.Time
				   },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Firstt
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Secondt
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Thirdt
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Fourtht
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.breakt
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Fiftht
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Sixtht
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Seventht
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top",
					textdata:data.string.Sunday
				   },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Art
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Art
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top",
					textdata:data.string.Monday
				   },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Health
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Physical
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top",
					textdata:data.string.Tuesday
				   },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Health
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Physical
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top",
					textdata:data.string.Wednesday
				   },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Health
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Art
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Art
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top",
					textdata:data.string.Thursday
				   },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Physical
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top",
					textdata:data.string.Friday
				   },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.dsh
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.dsh
				    },
				   {
				   	 flexboxrowclass:"rownormal ",
					 textdata:data.string.dsh
				    }
				]
			}]
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "coverpageintro",
					imgid : 'tablebg',
					imgsrc: ""
				}
			]
		}]
},
// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: " ",
	extratextblock:[
	{
		textclass:"s4txt",
		textdata:data.string.s4txt
	}
	],
	flexblockcontainers:[
	{
		flexblockadditionalclass: "fbup",
			flexblock:[
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top",
					textdata:data.string.Period
				   },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.First
				    },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.Second
				    },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.Third
				    },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.Fourth
				    },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.break
				    },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.Fifth
				    },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.Sixth
				    },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.Seventh
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top",
					textdata:data.string.Time
				   },
				   {
				   	 flexboxrowclass:"rownormal timefst",
					 textdata:data.string.Firstt
				    },
				   {
				   	 flexboxrowclass:"rownormal timefst",
					 textdata:data.string.Secondt
				    },
				   {
				   	 flexboxrowclass:"rownormal timefst",
					 textdata:data.string.Thirdt
				    },
				   {
				   	 flexboxrowclass:"rownormal timefst",
					 textdata:data.string.Fourtht
				    },
				   {
				   	 flexboxrowclass:"rownormal timefst",
					 textdata:data.string.breakt
				    },
				   {
				   	 flexboxrowclass:"rownormal timefst",
					 textdata:data.string.Fiftht
				    },
				   {
				   	 flexboxrowclass:"rownormal timefst",
					 textdata:data.string.Sixtht
				    },
				   {
				   	 flexboxrowclass:"rownormal timefst",
					 textdata:data.string.Seventht
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top",
					textdata:data.string.Sunday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Art
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Art
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top",
					textdata:data.string.Monday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Health
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Physical
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top",
					textdata:data.string.Tuesday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Health
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Physical
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top",
					textdata:data.string.Wednesday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Health
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Art
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Art
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top",
					textdata:data.string.Thursday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Physical
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top",
					textdata:data.string.Friday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.dsh
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.dsh
				    },
				   {
				   	 flexboxrowclass:"rownormal chap1",
					 textdata:data.string.dsh
				    }
				]
			}]
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "coverpageintro",
					imgid : 'tablebg',
					imgsrc: ""
				}
			]
		}]
},
// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: " ",
	extratextblock:[
	{
		textclass:"s5txt",
		textdata:data.string.s5txt
	}
	],
	flexblockcontainers:[
	{
		flexblockadditionalclass: "fbup",
			flexblock:[
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top1",
					textdata:data.string.Time
				   },
				   {
				   	 flexboxrowclass:"rownormal tclr",
					 textdata:data.string.Firstt
				    },
				   {
				   	 flexboxrowclass:"rownormal tclr",
					 textdata:data.string.Secondt
				    },
				   {
				   	 flexboxrowclass:"rownormal tclr",
					 textdata:data.string.Thirdt
				    },
				   {
				   	 flexboxrowclass:"rownormal tclr red",
					 textdata:data.string.Fourtht
				    },
				   {
				   	 flexboxrowclass:"rownormal tclr",
					 textdata:data.string.breakt
				    },
				   {
				   	 flexboxrowclass:"rownormal tclr",
					 textdata:data.string.Fiftht
				    },
				   {
				   	 flexboxrowclass:"rownormal tclr",
					 textdata:data.string.Sixtht
				    },
				   {
				   	 flexboxrowclass:"rownormal tclr",
					 textdata:data.string.Seventht
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal red top1",
					textdata:data.string.Sunday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap red",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Art
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Art
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top1",
					textdata:data.string.Monday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Health
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Physical
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top1",
					textdata:data.string.Tuesday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Health
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Physical
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top1",
					textdata:data.string.Wednesday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Health
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Art
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Art
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top1",
					textdata:data.string.Thursday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Physical
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top1",
					textdata:data.string.Friday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.dsh
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.dsh
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.dsh
				    }
				]
			}]
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "coverpageintro",
					imgid : 'tablebg',
					imgsrc: ""
				},
				{
					imgclass: "s5arrow1",
					imgid : 'arrows',
					imgsrc: ""
				},
				{
					imgclass: "s5arrow2",
					imgid : 'arrowd',
					imgsrc: ""
				}

			]
		}]
},
// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: " ",
	extratextblock:[
	{
		textclass:"s5txt",
		textdata:data.string.s7txt
	}
	],
	flexblockcontainers:[
	{
		flexblockadditionalclass: "fbup",
			flexblock:[
/*
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top",
					textdata:data.string.Period
				   },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.First
				    },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.Second
				    },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.Third
				    },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.Fourth
				    },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.break
				    },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.Fifth
				    },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.Sixth
				    },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.Seventh
				    }
				]
			},*/

			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top1",
					textdata:data.string.Time
				   },
				   {
				   	 flexboxrowclass:"rownormal tclr",
					 textdata:data.string.Firstt
				    },
				   {
				   	 flexboxrowclass:"rownormal tclr",
					 textdata:data.string.Secondt
				    },
				   {
				   	 flexboxrowclass:"rownormal tclr",
					 textdata:data.string.Thirdt
				    },
				   {
				   	 flexboxrowclass:"rownormal tclr",
					 textdata:data.string.Fourtht
				    },
				   {
				   	 flexboxrowclass:"rownormal tclr",
					 textdata:data.string.breakt
				    },
				   {
				   	 flexboxrowclass:"rownormal tclr",
					 textdata:data.string.Fiftht
				    },
				   {
				   	 flexboxrowclass:"rownormal tclr",
					 textdata:data.string.Sixtht
				    },
				   {
				   	 flexboxrowclass:"rownormal  red",
					 textdata:data.string.Seventht
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top1",
					textdata:data.string.Sunday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Art
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Art
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal red top1",
					textdata:data.string.Monday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Health
				    },
				   {
				   	 flexboxrowclass:"rownormal chap red",
					 textdata:data.string.Physical
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top1",
					textdata:data.string.Tuesday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Health
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Physical
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top1",
					textdata:data.string.Wednesday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Health
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Art
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Art
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top1",
					textdata:data.string.Thursday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Physical
				    }
				]
			},
			{
			flexboxcolumnclass:"column ",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top1",
					textdata:data.string.Friday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.dsh
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.dsh
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.dsh
				    }
				]
			}]
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "coverpageintro",
					imgid : 'tablebg',
					imgsrc: ""
				},
				{
					imgclass: "s6arrws",
					imgid : 'arrows',
					imgsrc: ""
				},
				{
					imgclass: "s6arrwd",
					imgid : 'arrowd',
					imgsrc: ""
				}
			]
		}]
},
// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: " ",
	extratextblock:[
	{
		textclass:"s5txt",
		textdata:data.string.s6txt
	}
	],
	flexblockcontainers:[
	{
		flexblockadditionalclass: "fbup",
			flexblock:[
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top1",
					textdata:data.string.Time
				   },
				   {
				   	 flexboxrowclass:"rownormal tclr",
					 textdata:data.string.Firstt
				    },
				   {
				   	 flexboxrowclass:"rownormal tclr",
					 textdata:data.string.Secondt
				    },
				   {
				   	 flexboxrowclass:"rownormal tclr",
					 textdata:data.string.Thirdt
				    },
				   {
				   	 flexboxrowclass:"rownormal tclr",
					 textdata:data.string.Fourtht
				    },
				   {
				   	 flexboxrowclass:"rownormal tclr red",
					 textdata:data.string.breakt
				    },
				   {
				   	 flexboxrowclass:"rownormal tclr",
					 textdata:data.string.Fiftht
				    },
				   {
				   	 flexboxrowclass:"rownormal tclr",
					 textdata:data.string.Sixtht
				    },
				   {
				   	 flexboxrowclass:"rownormal tclr",
					 textdata:data.string.Seventht
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top1",
					textdata:data.string.Sunday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap red",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Art
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Art
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top1",
					textdata:data.string.Monday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap red",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Health
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Physical
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top1",
					textdata:data.string.Tuesday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap red",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Health
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Physical
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top1",
					textdata:data.string.Wednesday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal chap red",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Health
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Art
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Art
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top1",
					textdata:data.string.Thursday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap red",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Physical
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top1",
					textdata:data.string.Friday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap red",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.dsh
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.dsh
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.dsh
				    }
				]
			}]
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "s7arrows",
					imgid : 'arrows',
					imgsrc: ""
				},
				{
					imgclass: "s7arrow d1",
					imgid : 'arrowd',
					imgsrc: ""
				},
				{
					imgclass: "s7arrow d2",
					imgid : 'arrowd',
					imgsrc: ""
				},
				{
					imgclass: "s7arrow d3",
					imgid : 'arrowd',
					imgsrc: ""
				},
				{
					imgclass: "s7arrow d4",
					imgid : 'arrowd',
					imgsrc: ""
				},
				{
					imgclass: "s7arrow d5",
					imgid : 'arrowd',
					imgsrc: ""
				},
				{
					imgclass: "s7arrow d6",
					imgid : 'arrowd',
					imgsrc: ""
				},
				{
					imgclass: "coverpageintro",
					imgid : 'tablebg',
					imgsrc: ""
				}

			]
		}]
},
// slide8
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: " ",
	extratextblock:[
	{
		textclass:"s5txt",
		textdata:data.string.s8txt
	}
	],
	flexblockcontainers:[
	{
		flexblockadditionalclass: "fbup",
			flexblock:[

			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top1",
					textdata:data.string.Period
				   },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.First
				    },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.Second
				    },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.Third
				    },
				   {
				   	 flexboxrowclass:"rownormal pername red",
					 textdata:data.string.Fourth
				    },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.break
				    },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.Fifth
				    },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.Sixth
				    },
				   {
				   	 flexboxrowclass:"rownormal pername",
					 textdata:data.string.Seventh
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top1",
					textdata:data.string.Time
				   },
				   {
				   	 flexboxrowclass:"rownormal tclr",
					 textdata:data.string.Firstt
				    },
				   {
				   	 flexboxrowclass:"rownormal tclr",
					 textdata:data.string.Secondt
				    },
				   {
				   	 flexboxrowclass:"rownormal tclr",
					 textdata:data.string.Thirdt
				    },
				   {
				   	 flexboxrowclass:"rownormal tclr",
					 textdata:data.string.Fourtht
				    },
				   {
				   	 flexboxrowclass:"rownormal tclr",
					 textdata:data.string.breakt
				    },
				   {
				   	 flexboxrowclass:"rownormal tclr",
					 textdata:data.string.Fiftht
				    },
				   {
				   	 flexboxrowclass:"rownormal tclr",
					 textdata:data.string.Sixtht
				    },
				   {
				   	 flexboxrowclass:"rownormal tclr",
					 textdata:data.string.Seventht
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal red top1",
					textdata:data.string.Sunday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap red",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Art
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Art
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top1",
					textdata:data.string.Monday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Health
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Physical
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top1",
					textdata:data.string.Tuesday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Health
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Physical
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top1",
					textdata:data.string.Wednesday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Health
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Art
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Art
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top1",
					textdata:data.string.Thursday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Science
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Physical
				    }
				]
			},
			{
			flexboxcolumnclass:"column",
	         flexblockcolumn:[
				{
					flexboxrowclass:"rownormal top1",
					textdata:data.string.Friday
				   },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Nepali
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Social
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.English
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Maths
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.Break
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.dsh
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.dsh
				    },
				   {
				   	 flexboxrowclass:"rownormal chap",
					 textdata:data.string.dsh
				    }
				]
			}]
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "s8arrws",
					imgid : 'arrows',
					imgsrc: ""
				},
				{
					imgclass: "s8arrwd",
					imgid : 'arrowd',
					imgsrc: ""
				},
				{
					imgclass: "coverpageintro",
					imgid : 'tablebg',
					imgsrc: ""
				}
			]
		}]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "dummy", src: imgpath+"1.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "coverpage",    src: imgpath+"img_coverpage.png", type: createjs.AbstractLoader.IMAGE},
			{id: "coverpageintro",    src: imgpath+"intro.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tablebg",    src: imgpath+"bgbgbg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrowd",    src: imgpath+"arrowd.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrows",    src: imgpath+"arrows.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"p1_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p1_s1.ogg"},
			{id: "sound_3", src: soundAsset+"p1_s2.ogg"},
			{id: "sound_5", src: soundAsset+"p1_s4.ogg"},
			{id: "sound_6", src: soundAsset+"p1_s5.ogg"},
			{id: "sound_7", src: soundAsset+"p1_s6.ogg"},
			{id: "sound_8", src: soundAsset+"p1_s7.ogg"},
			{id: "sound_9", src: soundAsset+"p1_s8.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);

		switch(countNext){
			case 0:
			sound_player("sound_1",1);
			break;
			case 1:
			sound_player("sound_2",1);
			break;
			case 2:
			sound_player("sound_3",1);
			break;
			case 3:
			nav_button_controls(100);
			break;
			case 4:
			sound_player("sound_5",1);
			break;
			case 5:
			sound_player("sound_6",1);
			break;
			case 6:
			sound_player("sound_7",1);
			break;
			case 7:
			sound_player("sound_8",1);
			break;
			case 8:
			sound_player("sound_9",1);
			break;
		}


	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2);
		});

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

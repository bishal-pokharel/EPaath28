var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_l_1 = new buzz.sound((soundAsset + "p1_s0.ogg"));
var sound_l_2 = new buzz.sound((soundAsset + "p3_s1.ogg"));
var sound_l_3 = new buzz.sound((soundAsset + "p3_s2.ogg"));
var sound_l_4 = new buzz.sound((soundAsset + "p3_s3.ogg"));
var sound_l_5 = new buzz.sound((soundAsset + "p3_s4.ogg"));
var sound_l_6 = new buzz.sound((soundAsset + "p3_s5.ogg"));
var sound_l_7 = new buzz.sound((soundAsset + "p3_s6.ogg"));

var sound_group_p1 = [sound_l_1, sound_l_2, sound_l_3, sound_l_4, sound_l_5, sound_l_6, sound_l_7];

var content=[
{
	//slide 0
	leftsidediv:[{
		textclass: "slideslide",
		textdata: data.string.hma
	}],
	rightsidediv:[{
		textclass: "slideslide",
		textdata: data.string.hmu
	}]
},
{
	headerblock:[
	{
		textclass : "fadein",
		textdata : data.string.p3text1,
	}
	],
	containsdialog:[{
			currentaudio: true,
			dialogcontainer: "dialog1",
			dialogimageclass: "dialogimage1",
			dialogimagesrc: imgpath + "blue.png",
			dialogcontentclass: "dialogcontent",
			dialogcontent: data.string.p3diag1
		}],
		imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "person1",
			imgsrc : imgpath + "sundar.png"
		},
		{
			imgclass: "item1",
			imgsrc : imgpath + "mangoes.png"
		}
		],
	}
	],
},
{
	headerblock:[
	{
		textclass : "fadein",
		textdata : data.string.p3text1,
	}
	],
	containsdialog:[{
			dialogcontainer: "dialog1",
			dialogimageclass: "dialogimage1",
			dialogimagesrc: imgpath + "blue.png",
			dialogcontentclass: "dialogcontent",
			dialogcontent: data.string.p3diag1
		},
		{
			currentaudio: true,
			dialogcontainer: "dialog2",
			dialogimageclass: "dialogimage2",
			dialogimagesrc: imgpath + "pink.png",
			dialogcontentclass: "dialogcontent",
			dialogcontent: data.string.p3diag2
		}],
		imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "person1",
			imgsrc : imgpath + "sundar.png"
		},
		{
			imgclass: "person2",
			imgsrc : imgpath + "sundari.png"
		},
		{
			imgclass: "item1",
			imgsrc : imgpath + "mangoes.png"
		},
		{
			imgclass: "item2",
			imgsrc : imgpath + "mango-juice.png"
		}
		],
	}
	],
},
{
	headerblock:[
	{
		textclass : "fadein",
		textdata : data.string.p3text1,
	}
	],
	containsdialog:[{
			currentaudio: true,
			dialogcontainer: "dialog1",
			dialogimageclass: "dialogimage1",
			dialogimagesrc: imgpath + "blue.png",
			dialogcontentclass: "dialogcontent",
			dialogcontent: data.string.p3diag3
		}],
		imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "person1",
			imgsrc : imgpath + "sundar.png"
		},
		{
			imgclass: "item1",
			imgsrc : imgpath + "eggs.png"
		}
		],
	}
	],
},
{
	headerblock:[
	{
		textclass : "fadein",
		textdata : data.string.p3text1,
	}
	],
	containsdialog:[{
			dialogcontainer: "dialog1",
			dialogimageclass: "dialogimage1",
			dialogimagesrc: imgpath + "blue.png",
			dialogcontentclass: "dialogcontent",
			dialogcontent: data.string.p3diag3
		},
		{
			currentaudio: true,
			dialogcontainer: "dialog2",
			dialogimageclass: "dialogimage2",
			dialogimagesrc: imgpath + "pink.png",
			dialogcontentclass: "dialogcontent",
			dialogcontent: data.string.p3diag4
		}],
		imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "person1",
			imgsrc : imgpath + "sundar.png"
		},
		{
			imgclass: "person2",
			imgsrc : imgpath + "sundari.png"
		},
		{
			imgclass: "item1",
			imgsrc : imgpath + "eggs.png"
		},
		{
			imgclass: "item2",
			imgsrc : imgpath + "soup.png"
		}
		],
	}
	],
},

{
	headerblock:[
	{
		textclass : "fadein",
		textdata : data.string.p3text1,
	}
	],
	containsdialog:[{
			currentaudio: true,
			dialogcontainer: "dialog1",
			dialogimageclass: "dialogimage1",
			dialogimagesrc: imgpath + "blue.png",
			dialogcontentclass: "dialogcontent",
			dialogcontent: data.string.p3diag5
		}],
		imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "person1",
			imgsrc : imgpath + "sundar.png"
		},
		{
			imgclass: "item1",
			imgsrc : imgpath + "brush.png"
		}
		],
	}
	],
},
{
	headerblock:[
	{
		textclass : "fadein",
		textdata : data.string.p3text1,
	}
	],
	containsdialog:[{
			dialogcontainer: "dialog1",
			dialogimageclass: "dialogimage1",
			dialogimagesrc: imgpath + "blue.png",
			dialogcontentclass: "dialogcontent",
			dialogcontent: data.string.p3diag5
		},
		{
			currentaudio: true,
			dialogcontainer: "dialog2",
			dialogimageclass: "dialogimage2",
			dialogimagesrc: imgpath + "pink.png",
			dialogcontentclass: "dialogcontent",
			dialogcontent: data.string.p3diag6
		}],
		imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "person1",
			imgsrc : imgpath + "sundar.png"
		},
		{
			imgclass: "person2",
			imgsrc : imgpath + "sundari.png"
		},
		{
			imgclass: "item1",
			imgsrc : imgpath + "brush.png"
		},
		{
			imgclass: "item2",
			imgsrc : imgpath + "paint.png"
		}
		],
	}
	],
}
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	/*recalculateHeightWidth();*/

	var total_page = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
      */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
      */
      function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch(countNext){
			case 0:
			playaudio(sound_group_p1[0], $(".dialog1"));
			break;
			case 1:
			playaudio(sound_group_p1[1], $(".dialog1"));
			break;
			case 2:
			playaudio(sound_group_p1[2], $(".dialog2"));
			break;
			case 3:
			playaudio(sound_group_p1[3], $(".dialog1"));
			break;
			case 4:
			playaudio(sound_group_p1[4], $(".dialog2"));
			break;
			case 5:
			playaudio(sound_group_p1[5], $(".dialog1"));
			break;
			case 6:
			playaudio(sound_group_p1[6], $(".dialog2"));
			break;
			default:
			break;
		}
	}

	function playaudio(sound_data, $dialog_container){
		var playing = true;
		$dialog_container.removeClass("playable");
		$dialog_container.click(function(){
			if(!playing){
				playaudio(sound_data, $dialog_container);
			}
			return false;
		});
		$prevBtn.hide(0);
		if((countNext+1) == content.length){
			ole.footerNotificationHandler.hideNotification();
		}else{
			$nextBtn.hide(0);
		}
		sound_data.play();
		sound_data.bind('ended', function(){
			setTimeout(function(){
				if(countNext != 0)
				$prevBtn.show(0);
				$dialog_container.addClass("playable");
				playing = false;
				sound_data.unbind('ended');
				if((countNext+1) == content.length){
					ole.footerNotificationHandler.pageEndSetNotification();
				}else{
					$nextBtn.show(0);
				}
			}, 1000);
		});
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});


 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_l_1 = new buzz.sound((soundAsset + "p1_s0.ogg"));
var sound_l_2 = new buzz.sound((soundAsset + "p1_s1.ogg"));
var sound_l_3 = new buzz.sound((soundAsset + "p1_s3.ogg"));
var sound_l_4 = new buzz.sound((soundAsset + "p1_s2.ogg"));
var sound_l_5 = new buzz.sound((soundAsset + "p1_s4.ogg"));
var sound_l_6 = new buzz.sound((soundAsset + "p1_s5.ogg"));
var sound_l_7 = new buzz.sound((soundAsset + "p1_s6.ogg"));
var sound_l_8 = new buzz.sound((soundAsset + "p1_s7.ogg"));
var sound_l_9 = new buzz.sound((soundAsset + "p1_s8.ogg"));
var sound_l_10 = new buzz.sound((soundAsset + "p1_s9.ogg"));

var sound_group_p1 = [sound_l_1, sound_l_2, sound_l_3, sound_l_4, sound_l_5, sound_l_6, sound_l_7, sound_l_8, sound_l_9, sound_l_10];

var content=[
{
	//slide 0
	leftsidediv:[{
		textclass: "slideslide",
		textdata: data.string.hma
	}],
	rightsidediv:[{
		textclass: "slideslide",
		textdata: data.string.hmu
	}]
},
	//slide 1
{
	headerblock:[
	{
		textclass : "fadein",
		textdata : data.string.p1text1,
	}
	],
	uppertextblock:[
	{
		textclass : "youcan",
		textdata : data.string.p1text2,
	},
	{
		textclass : "foralltext onetext",
		textdata : data.string.orange,
	},
	{
		textclass : "foralltext twotext",
		textdata : data.string.seed,
	},
	{
		textclass : "foralltext threetext",
		textdata : data.string.leaf,
	},
	{
		textclass : "foralltext fourtext",
		textdata : data.string.bucket,
	},
	{
		textclass : "foralltext fivetext",
		textdata : data.string.tree,
	},
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "object oneimg",
			imgsrc : imgpath + "orange.png"
		},
		{
			imgclass: "object twoimg",
			imgsrc : imgpath + "seeds.png"
		},
		{
			imgclass: "object threeimg",
			imgsrc : imgpath + "leaves.png"
		},
		{
			imgclass: "object fourimg",
			imgsrc : imgpath + "buckets.png"
		},
		{
			imgclass: "object fiveimg",
			imgsrc : imgpath + "trees.png"
		},
		],
	}
	],
},

	//slide 2
{
	headerblock:[
	{
		textclass : "fadein",
		textdata : data.string.p1text1,
	}
	],
	uppertextblock:[
	{
		textclass : "youcan",
		textdata : data.string.p1text3,
	},
	{
		datahighlightflag:true,
		datahighlightcustomclass:"prplTxt",
		textclass : "foralltext onetext2",
		textdata : data.string.fiveo,
	},
	{
		datahighlightflag:true,
		datahighlightcustomclass:"prplTxt",
		textclass : "foralltext twotext2",
		textdata : data.string.twob,
	},
	{
		datahighlightflag:true,
		datahighlightcustomclass:"prplTxt",
		textclass : "foralltext threetext2",
		textdata : data.string.tens,
	},
	{
		textclass : "foralltext belowtext",
		textdata : data.string.forthings,
	}
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "objectmiddle p2oneimg",
			imgsrc : imgpath + "five-oranges.png"
		},
		{
			imgclass: "objectmiddle p2twoimg",
			imgsrc : imgpath + "two-buckets.png"
		},
		{
			imgclass: "objectmiddle p2threeimg",
			imgsrc : imgpath + "ten-seeds.png"
		},
		],
	}
	],
},
	//slide 3
{
	headerblock:[
	{
		textclass : "fadein",
		textdata : data.string.p1text3_sec,
	}
	],
	uppertextblock:[
	{
		datahighlightflag: true,
		datahighlightcustomclass: "letshighlight",
		textclass : "middletext",
		textdata : data.string.p1text4,
	},
	],
},
	//slide 4
{
	headerblock:[
	{
		textclass : "fadein",
		textdata : data.string.p1text3_sec,
	}
	],
	containsdialog:[{
			currentaudio: true,
			dialogcontainer: "dialog1",
			dialogimageclass: "dialogimage1",
			dialogimagesrc: imgpath + "blue.png",
			dialogcontentclass: "dialogcontent",
			dialogcontent: data.string.p1diag1
		}],

	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "person1",
			imgsrc : imgpath + "boy.png"
		},
		{
			imgclass: "person2",
			imgsrc : imgpath + "ducklin.png"
		},
		{
			imgclass: "item1",
			imgsrc : imgpath + "five-oranges.png"
		}
		],
	}
	],
},
	//slide 5
{
	headerblock:[
	{
		textclass : "fadein",
		textdata : data.string.p1text3_sec,
	}
	],
	containsdialog:[{
			dialogcontainer: "dialog1",
			dialogimageclass: "dialogimage1",
			dialogimagesrc: imgpath + "blue.png",
			dialogcontentclass: "dialogcontent",
			dialogcontent: data.string.p1diag1
		},
		{
			currentaudio: true,
			dialogcontainer: "dialog2",
			dialogimageclass: "dialogimage2",
			dialogimagesrc: imgpath + "pink.png",
			dialogcontentclass: "dialogcontent",
			datahighlightflag:true,
			datahighlightcustomclass:"prplTxt",
			dialogcontent: data.string.p1diag2
		}],
		imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "person1",
			imgsrc : imgpath + "boy.png"
		},
		{
			imgclass: "person2",
			imgsrc : imgpath + "ducklin.png"
		},
		{
			imgclass: "item1",
			imgsrc : imgpath + "five-oranges.png"
		}
		],
	}
	],
},
	// slide 6
{
	headerblock:[
	{
		textclass : "fadein",
		textdata : data.string.p1text3_sec,
	}
	],
	containsdialog:[{
			currentaudio: true,
			dialogcontainer: "dialog1",
			dialogimageclass: "dialogimage1",
			dialogimagesrc: imgpath + "blue.png",
			dialogcontentclass: "dialogcontent",
			dialogcontent: data.string.p1diag3
		}],

	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "person1",
			imgsrc : imgpath + "boy.png"
		},
		{
			imgclass: "person2",
			imgsrc : imgpath + "ducklin.png"
		},
		{
			imgclass: "item1",
			imgsrc : imgpath + "two-buckets.png"
		}
		],
	}
	],
},
	// slide 7
{
	headerblock:[
	{
		textclass : "fadein",
		textdata : data.string.p1text3_sec,
	}
	],
	containsdialog:[{
			dialogcontainer: "dialog1",
			dialogimageclass: "dialogimage1",
			dialogimagesrc: imgpath + "blue.png",
			dialogcontentclass: "dialogcontent",
			dialogcontent: data.string.p1diag3
		},
		{
			currentaudio: true,
			dialogcontainer: "dialog2",
			dialogimageclass: "dialogimage2",
			dialogimagesrc: imgpath + "pink.png",
			datahighlightflag:true,
			datahighlightcustomclass:"prplTxt",
			dialogcontentclass: "dialogcontent",
			dialogcontent: data.string.p1diag4
		}],
		imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "person1",
			imgsrc : imgpath + "boy.png"
		},
		{
			imgclass: "person2",
			imgsrc : imgpath + "ducklin.png"
		},
		{
			imgclass: "item1",
			imgsrc : imgpath + "two-buckets.png"
		}
		],
	}
	],
},
	// slide 8
{
	headerblock:[
	{
		textclass : "fadein",
		textdata : data.string.p1text3_sec,
	}
	],
	containsdialog:[{
			currentaudio: true,
			dialogcontainer: "dialog1",
			dialogimageclass: "dialogimage1",
			dialogimagesrc: imgpath + "blue.png",
			dialogcontentclass: "dialogcontent",
			dialogcontent: data.string.p1diag5
		}],

	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "person1",
			imgsrc : imgpath + "boy.png"
		},
		{
			imgclass: "person2",
			imgsrc : imgpath + "ducklin.png"
		},
		{
			imgclass: "item1",
			imgsrc : imgpath + "three-seed.png"
		}
		],
	}
	],
},
	// slide 9
{
	headerblock:[
	{
		textclass : "fadein",
		textdata : data.string.p1text3_sec,
	}
	],
	containsdialog:[{
			dialogcontainer: "dialog1",
			dialogimageclass: "dialogimage1",
			dialogimagesrc: imgpath + "blue.png",
			dialogcontentclass: "dialogcontent",
			dialogcontent: data.string.p1diag5
		},
		{
			currentaudio: true,
			dialogcontainer: "dialog2",
			dialogimageclass: "dialogimage2",
			dialogimagesrc: imgpath + "pink.png",
			dialogcontentclass: "dialogcontent",
			datahighlightflag:true,
			datahighlightcustomclass:"prplTxt",
			dialogcontent: data.string.p1diag6
		}],
		imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "person1",
			imgsrc : imgpath + "boy.png"
		},
		{
			imgclass: "person2",
			imgsrc : imgpath + "ducklin.png"
		},
		{
			imgclass: "item1",
			imgsrc : imgpath + "three-seed.png"
		}
		],
	}
	],
}
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	/*recalculateHeightWidth();*/

	var total_page = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);


		var vocabcontroller =  new Vocabulary();
		vocabcontroller.init();
	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
      */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
      */
			function navigationcontroller(islastpageflag){
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;

		if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch(countNext){
			case 0:
			playaudio(sound_group_p1[0], $(".dialog1"));
			break;
			case 1:
			playaudio(sound_group_p1[1], $(".dialog1"));
			break;
			case 2:
			playaudio(sound_group_p1[2], $(".dialog1"));
			break;
			case 3:
			playaudio(sound_group_p1[3], $(".dialog1"));
			break;
			case 4:
			playaudio(sound_group_p1[4], $(".dialog1"));
			break;
			case 5:
			playaudio(sound_group_p1[5], $(".dialog2"));
			break;
			case 6:
			playaudio(sound_group_p1[6], $(".dialog1"));
			break;
			case 7:
			playaudio(sound_group_p1[7], $(".dialog2"));
			break;
			case 8:
			playaudio(sound_group_p1[8], $(".dialog1"));
			break;
			case 9:
			playaudio(sound_group_p1[9], $(".dialog2"));
			break;
			default:
			break;
		}
	}

	function playaudio(sound_data, $dialog_container){
			var playing = true;
			$dialog_container.removeClass("playable");
			$dialog_container.click(function(){
				if(!playing){
					playaudio(sound_data, $dialog_container);
				}
				return false;
			});
			$prevBtn.hide(0);
			if((countNext+1) == content.length){
				ole.footerNotificationHandler.hideNotification();
			}else{
				$nextBtn.hide(0);
			}
			sound_data.play();
			sound_data.bind('ended', function(){
				setTimeout(function(){
					if(countNext != 0)
					$prevBtn.show(0);
					$dialog_container.addClass("playable");
					playing = false;
					sound_data.unbind('ended');
					if((countNext+1) == content.length){
						ole.footerNotificationHandler.pageEndSetNotification();
					}else{
						$nextBtn.show(0);
					}
				}, 1000);
			});
		}
	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});


 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

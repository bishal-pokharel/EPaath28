var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/TellTime/";

var sound_dg1 = new buzz.sound((soundAsset + "s7_p1.ogg"));
var sound_dg2 = new buzz.sound((soundAsset + "s7_p2_1.ogg"));
var sound_dg2_1 = new buzz.sound((soundAsset + "S7_P2_2.ogg"));
var soundArr = [sound_dg2, sound_dg2_1]; //this has been used for passing audios to function named show_obo
var current_sound = sound_dg1;

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-summary',

		extratextblock : [{
			textdata : data.string.p4text1,
			textclass : 'summary-text',
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "summary_fairy",
					imgsrc : "images/fairy/fairy_fly.gif",
				},
			],
		}],
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock min-margin added-class-1',
		uppertextblock : [{
			textdata : data.string.p4texta,
			textclass : 'my_font_big proximanova  fnt_3half',
			datahighlightflag: true,
			datahighlightcustomclass: 'ul my_font_very_big'
		},
		{
			textdata : data.string.p4textb,
			textclass : 'my_font_medium proximanova hideThis_1 fnt_3half',
			datahighlightflag: true,
			datahighlightcustomclass: 'ul'
		},
		{
			textdata : data.string.p4textc,
			textclass : 'my_font_medium my_font_big proximanova hideThis_2 fnt_3half',
			datahighlightflag: true,
			datahighlightcustomclass: 'ul'
		}
		],
		clockblock : [{
			haspasttext: true,
			pastimgclass: '',
			pastimgsrc: imgpath + 'past.png',
			pastimgclass2: '',
			pastimgsrc2: imgpath + 'pink.png',
			clockblockclass: 'clock-right-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_fulltransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-3',
			minclass: 'hr-12',
			dotclass: '',
			past0: data.string.past0,
			past30: data.string.past30,
			past5: data.string.past5,
			past10: data.string.past10,
			past15: data.string.past15,
			past20: data.string.past20,
			past25: data.string.past25,

			past35: data.string.past35,
			past40: data.string.past40,
			past45: data.string.past45,
			past50: data.string.past50,
			past55: data.string.past55,
		}],
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		clockblock : [{
			haspasttext: true,
			pastimgclass: '',
			pastimgsrc: imgpath + 'past.png',
			pastimgclass2: '',
			pastimgsrc2: imgpath + 'pink.png',
			clockblockclass: 'clock-middle',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_fulltransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-4',
			minclass: 'hr-12',
			dotclass: '',
			past0: data.string.past0,
			past0class: 'past0class',

			past30: data.string.past30,
			past30class: 'past0class',

			past5: data.string.past5,
			past10: data.string.past10,

			past15: data.string.past15,
			past15class: 'past0class',

			past20: data.string.past20,
			past25: data.string.past25,

			past35: data.string.past35,
			past40: data.string.past40,

			past45: data.string.past45,
			past45class: 'past0class',

			past50: data.string.past50,
			past55: data.string.past55,
		}],
	}
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;


var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();


	var timeoutvar = null;

	// var vocabcontroller =  new Vocabulary();
	// vocabcontroller.init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// vocabcontroller.findwords(countNext);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		var secparFlag = 0;
		function show_obo(count, maxCount, secparFlag){
			$(".past-"+count).show(0);
			timeoutvar = setTimeout(function(){
				if(count<maxCount){
					count+=5;
					show_obo(count, maxCount, secparFlag);
				}else{
					if(!secparFlag){
						$(".hideThis_1").show(0);
						sound_player(soundArr[0], 0, 1);
					}else{
						$(".hideThis_2").show(0);
						sound_player(soundArr[1], 1, 0);
					}
				}
			},750);
		}
		function sound_player(sound_id, navFlag, secparFlag){
			current_sound.stop();
			current_sound = sound_id;
			current_sound.play();
			current_sound.bind("ended", function(){
				if(secparFlag){
					show_obo(35, 55, 1);
				}else{
					navFlag?nav_button_controls():'';
				}
			});
		}
		switch (countNext) {
			case 0:
				sound_and_nav(sound_dg1);
				// nav_button_controls(100);
				break;
			case 1:
				// sound_and_nav(sound_dg2);
				$(".hideThis_1, .hideThis_2").hide(0);
				for(let i=0; i<=55; i+=5){
					$(".past-"+i).hide(0);
				}
				show_obo(0, 30, 0);
				break;
			case 2:
				nav_button_controls(2000);
				break;
		}
	}

	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}
		function sound_and_nav(sound_data, clickfunction , a){
			current_sound.stop();
			current_sound = sound_data;
			current_sound.play();
			current_sound.bindOnce('ended', function(){
				if(typeof clickfunction != 'undefined'){
					clickfunction(a, sound_data);
				}
				if(countNext==0){
					$nextBtn.show(0);
				} else if( countNext>0 && countNext == $total_page-1){
					$prevBtn.show(0);
					ole.footerNotificationHandler.pageEndSetNotification();
				} else{
					$prevBtn.show(0);
					$nextBtn.show(0);
				}
			});
		}
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				current_sound.stop();
				clearTimeout(timeoutvar);
				timeoutvar = null;
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		current_sound.stop();
		clearTimeout(timeoutvar);
		timeoutvar = null;
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

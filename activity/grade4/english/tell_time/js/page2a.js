var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/TellTime/";

var sound_dg1 = new buzz.sound((soundAsset + "s3_p1.ogg"));
var sound_dg2 = new buzz.sound((soundAsset + "s3_p2.ogg"));
var sound_dg3 = new buzz.sound((soundAsset + "s3_p3.ogg"));
var sound_dg4 = new buzz.sound((soundAsset + "s3_p4.ogg"));
var sound_dg5 = new buzz.sound((soundAsset + "s3_p5.ogg"));
var sound_dg6 = new buzz.sound((soundAsset + "s3_p6.ogg"));
var sound_dg7 = new buzz.sound((soundAsset + "s3_p7.ogg"));

var soundArr=[sound_dg1, sound_dg2, sound_dg3, sound_dg4,
							sound_dg5, sound_dg6, sound_dg7];
var current_sound = sound_dg1;

var content = [
	//slide13
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock min-margin added-class-1',
		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul my_font_very_big'
		},
		{
			textdata : data.string.p2text20,
			textclass : 'my_font_big proximanova fade_in_1 let-eg-1',
			datahighlightflag: true,
			datahighlightcustomclass: 'ul'
		},],

		clockblock : [{
			haspasttext: true,
			pastimgclass: '',
			pastimgsrc: imgpath + 'past.png',
			clockblockclass: 'clock-right-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_halftransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-3-20',
			minclass: 'hr-4',
			dotclass: '',
			past0: data.string.past0,
			past30: data.string.past30,
			past5: data.string.past5,
			past10: data.string.past10,
			past15: data.string.past15,
			past20: data.string.past20,
			past25: data.string.past25,
		}]
	},
	//slide14
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock min-margin added-class-1',
		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul my_font_very_big'
		},
		{
			textdata : data.string.p2text20a,
			textclass : 'my_font_big proximanova fade_in_1',
			datahighlightflag: true,
			datahighlightcustomclass: 'ul'
		},{
			textdata : data.string.p2text20b,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'ul'
		}],

		clockblock : [{
			haspasttext: true,
			pastimgclass: '',
			pastimgsrc: imgpath + 'past.png',
			clockblockclass: 'clock-right-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_halftransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-3-20',
			minclass: 'hr-4',
			dotclass: '',
			past0: data.string.past0,
			past30: data.string.past30,
			past5: data.string.past5,
			past10: data.string.past10,
			past15: data.string.past15,
			past20: data.string.past20,
			past25: data.string.past25,
		}],
		optionsblock:[{
			option:[{
				textclass:"",
				textdata:data.string.Twentyfive
			},{
				textclass:"class_1",
				textdata:data.string.Twenty
			}]
		}]
	},
	//slide15
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock min-margin added-class-1',
		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul my_font_very_big'
		},
		{
			textdata : data.string.p2text21,
			textclass : 'my_font_big proximanova fade_in_1 let-eg-1',
			datahighlightflag: true,
			datahighlightcustomclass: 'y-text'
		},],

		clockblock : [{
			haspasttext: true,
			pastimgclass: '',
			pastimgsrc: imgpath + 'past.png',
			clockblockclass: 'clock-right-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_halftransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-3-20',
			minclass: 'hr-4',
			dotclass: '',
			past0: data.string.past0,
			past30: data.string.past30,
			past5: data.string.past5,
			past10: data.string.past10,
			past15: data.string.past15,
			past20: data.string.past20,
			past25: data.string.past25,
			past20class: 'highlighted-1'
		}]
	},
	//slide16
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock min-margin added-class-1',
		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul my_font_very_big'
		},
		{
			textdata : data.string.p2text22,
			textclass : 'my_font_big proximanova fade_in_1',
			datahighlightflag: true,
			datahighlightcustomclass: 'ul'
		},{
			textdata : data.string.p2text22a,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'ul'
		}],

		clockblock : [{
			haspasttext: true,
			pastimgclass: '',
			pastimgsrc: imgpath + 'past.png',
			clockblockclass: 'clock-right-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_halftransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-3-20',
			minclass: 'hr-4',
			dotclass: '',
			past0: data.string.past0,
			past30: data.string.past30,
			past5: data.string.past5,
			past10: data.string.past10,
			past15: data.string.past15,
			past20: data.string.past20,
			past25: data.string.past25,
		}],
		optionsblock:[{
			optionclass:"optn_blk_sec",
			option:[{
				textclass:"grp_2",
				textdata:data.string.four
			},{
				textclass:"grp_2 class_1",
				textdata:data.string.three
			}]
		}]
	},
	//slide17
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock min-margin added-class-1',
		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul my_font_very_big'
		},
		{
			textdata : data.string.p2text23,
			textclass : 'my_font_big proximanova fade_in_1 let-eg-1',
			datahighlightflag: true,
			datahighlightcustomclass: 'y-text'
		},],

		clockblock : [{
			haspasttext: true,
			pastimgclass: '',
			hashl: true,
			hlclass: 'hr-3 highlighted-2',
			pastimgsrc: imgpath + 'past.png',
			clockblockclass: 'clock-right-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_halftransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-3-20',
			minclass: 'hr-4',
			dotclass: '',
			past0: data.string.past0,
			past30: data.string.past30,
			past5: data.string.past5,
			past10: data.string.past10,
			past15: data.string.past15,
			past20: data.string.past20,
			past25: data.string.past25,
			past5class: 'highlighted-1'
		}]
	},
	//slide18
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock min-margin added-class-1',
		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul my_font_very_big'
		},
		{
			textdata : data.string.p2text24,
			textclass : 'my_font_big proximanova fade_in_1',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul'
		},{
			textdata : data.string.p2text25,
			textclass : 'my_font_big proximanova fade_in-2',
		},{
			textdata : data.string.p2text17a,
			textclass : 'my_font_big proximanova fade_in-3',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold'
		}],

		clockblock : [{
			haspasttext: true,
			pastimgclass: '',
			pastimgsrc: imgpath + 'past.png',
			clockblockclass: 'clock-right-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_halftransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-3-20',
			minclass: 'hr-4',
			dotclass: '',
			past0: data.string.past0,
			past30: data.string.past30,
			past5: data.string.past5,
			past10: data.string.past10,
			past15: data.string.past15,
			past20: data.string.past20,
			past25: data.string.past25,
		}]
	},
	//slide19
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock min-margin added-class-1',
		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul my_font_very_big'
		},{
			textdata : data.string.p2text26,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul'
		},{
			textdata : data.string.p2text18a,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold'
		}],
		extratextblock : [{
			textdata : data.string.p2text27,
			textclass : 'my_font_big proximanova added-bottom',
		}],
		clockblock : [{
			haspasttext: true,
			pastimgclass: '',
			pastimgsrc: imgpath + 'past.png',
			clockblockclass: 'clock-right-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_halftransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-3-10',
			minclass: 'hr-1',
			dotclass: '',
			past0: data.string.past0,
			past30: data.string.past30,
			past5: data.string.past5,
			past10: data.string.past10,
			past15: data.string.past15,
			past20: data.string.past20,
			past25: data.string.past25,
		}]
	}
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;


	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();


	var timeoutvar = null;

	// var vocabcontroller =  new Vocabulary();
	// vocabcontroller.init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// vocabcontroller.findwords(countNext);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		function correct_Incor(this_class, corFlag, navFlag){
			if(corFlag){
				this_class.addClass("correct");
				this_class.parent().parent().css("pointer-events", "none");
				play_correct_incorrect_sound(1);
				this_class.siblings(".correct-icon").show();
				navFlag?setTimeout(function(){$nextBtn.trigger("click")}, 1000):'';
			}else{
				this_class.addClass("incorrect");
				this_class.siblings(".incorrect-icon").show();
				play_correct_incorrect_sound(0);
			}
		}

		switch (countNext) {
			case 0:
				for(var i=0;i<=30;i+=5){
					$(".past-"+i).hide(0);
				}
				sound_and_nav(soundArr[countNext]);
			break;
			case 1:
			case 3:
			sound_player(soundArr[countNext]);
				$(".opn_txt").click(function(){
					if($(this).hasClass("class_1")){
						correct_Incor($(this), 1, 1);
					}else{
						correct_Incor($(this), 0, 0);
					}
				});
				break;
			case 5:
			case 6:
			sound_and_nav(soundArr[countNext]);
				break;
			default:
			sound_and_nav(soundArr[countNext]);
				break;
		}
	}

	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}

	function sound_and_nav(sound_data, clickfunction , a){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			if(typeof clickfunction != 'undefined'){
				clickfunction(a, sound_data);
			}
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		});
	}
	function click_dg(dg_class, audio){
		$(dg_class).click(function(){
			sound_player(audio);
		});
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				current_sound.stop();
				clearTimeout(timeoutvar);
				timeoutvar = null;
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		current_sound.stop();
		clearTimeout(timeoutvar);
		timeoutvar = null;
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/TellTime/";

var sound_dg1 = new buzz.sound((soundAsset + "s8_p2.ogg"));

var current_sound = sound_dg1;

var content1 = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-diy',

		extratextblock : [{
			textdata : data.string.diytext,
			textclass : 'diy-text',
		}]
	}
];


var content2 = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-re-1',

		extratextblock:[{
			textdata: data.string.p4text8,
			textclass: 'ques-p4 my_font_very_big'
		}],

		option:[{
			optionclass: 'ans-1 class1 my_font_big',
			textclass: '',
			textdata: data.string.p4text9a,
		},{
			optionclass: 'ans-2 class2 my_font_big',
			textclass: '',
			textdata: data.string.p4text9b,
		},{
			optionclass: 'ans-3 class3 my_font_big',
			textclass: '',
			textdata: data.string.p4text9c,
		},{
			optionclass: 'ans-4 class4 my_font_big',
			textclass: '',
			textdata: data.string.p4text9d,
		}],

		clockblock : [{
			clockblockclass: 'clock-center-ques',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_fulltransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-7',
			minclass: 'hr-12',
			dotclass: '',
		}],
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-re-1',

		extratextblock:[{
			textdata: data.string.p4text8,
			textclass: 'ques-p4 my_font_very_big'
		}],

		option:[{
			optionclass: 'ans-1 class1 my_font_big',
			textclass: '',
			textdata: data.string.p4text10a,
		},{
			optionclass: 'ans-2 class2 my_font_big',
			textclass: '',
			textdata: data.string.p4text10b,
		},{
			optionclass: 'ans-3 class3 my_font_big',
			textclass: '',
			textdata: data.string.p4text10c,
		},{
			optionclass: 'ans-4 class4 my_font_big',
			textclass: '',
			textdata: data.string.p4text10d,
		}],

		clockblock : [{
			clockblockclass: 'clock-center-ques',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_fulltransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-2',
			minclass: 'hr-12',
			dotclass: '',
		}],
	},

	//half past
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-re-1',

		extratextblock:[{
			textdata: data.string.p4text8,
			textclass: 'ques-p4 my_font_very_big'
		}],

		option:[{
			optionclass: 'ans-1 class1 my_font_big',
			textclass: '',
			textdata: data.string.p4text11a,
		},{
			optionclass: 'ans-2 class2 my_font_big',
			textclass: '',
			textdata: data.string.p4text11b,
		},{
			optionclass: 'ans-3 class3 my_font_big',
			textclass: '',
			textdata: data.string.p4text11c,
		},{
			optionclass: 'ans-4 class4 my_font_big',
			textclass: '',
			textdata: data.string.p4text11d,
		}],

		clockblock : [{
			clockblockclass: 'clock-center-ques',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_fulltransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-2-30',
			minclass: 'hr-6',
			dotclass: '',
		}],
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-re-1',

		extratextblock:[{
			textdata: data.string.p4text8,
			textclass: 'ques-p4 my_font_very_big'
		}],

		option:[{
			optionclass: 'ans-1 class1 my_font_big',
			textclass: '',
			textdata: data.string.p4text12a,
		},{
			optionclass: 'ans-2 class2 my_font_big',
			textclass: '',
			textdata: data.string.p4text12b,
		},{
			optionclass: 'ans-3 class3 my_font_big',
			textclass: '',
			textdata: data.string.p4text12c,
		},{
			optionclass: 'ans-4 class4 my_font_big',
			textclass: '',
			textdata: data.string.p4text12d,
		}],

		clockblock : [{
			clockblockclass: 'clock-center-ques',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_fulltransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-10-30',
			minclass: 'hr-6',
			dotclass: '',
		}],
	},
	//quater past
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-re-1',

		extratextblock:[{
			textdata: data.string.p4text8,
			textclass: 'ques-p4 my_font_very_big'
		}],

		option:[{
			optionclass: 'ans-1 class1 my_font_big',
			textclass: '',
			textdata: data.string.p4text13a,
		},{
			optionclass: 'ans-2 class2 my_font_big',
			textclass: '',
			textdata: data.string.p4text13b,
		},{
			optionclass: 'ans-3 class3 my_font_big',
			textclass: '',
			textdata: data.string.p4text13c,
		},{
			optionclass: 'ans-4 class4 my_font_big',
			textclass: '',
			textdata: data.string.p4text13d,
		}],

		clockblock : [{
			clockblockclass: 'clock-center-ques',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_fulltransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-4-15',
			minclass: 'hr-3',
			dotclass: '',
		}],
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-re-1',

		extratextblock:[{
			textdata: data.string.p4text8,
			textclass: 'ques-p4 my_font_very_big'
		}],

		option:[{
			optionclass: 'ans-1 class1 my_font_big',
			textclass: '',
			textdata: data.string.p4text14a,
		},{
			optionclass: 'ans-2 class2 my_font_big',
			textclass: '',
			textdata: data.string.p4text14b,
		},{
			optionclass: 'ans-3 class3 my_font_big',
			textclass: '',
			textdata: data.string.p4text14c,
		},{
			optionclass: 'ans-4 class4 my_font_big',
			textclass: '',
			textdata: data.string.p4text14d,
		}],

		clockblock : [{
			clockblockclass: 'clock-center-ques',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_fulltransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-8-15',
			minclass: 'hr-3',
			dotclass: '',
		}],
	},
	//quater to
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-re-1',

		extratextblock:[{
			textdata: data.string.p4text8,
			textclass: 'ques-p4 my_font_very_big'
		}],

		option:[{
			optionclass: 'ans-1 class1 my_font_big',
			textclass: '',
			textdata: data.string.p4text15a,
		},{
			optionclass: 'ans-2 class2 my_font_big',
			textclass: '',
			textdata: data.string.p4text15b,
		},{
			optionclass: 'ans-3 class3 my_font_big',
			textclass: '',
			textdata: data.string.p4text15c,
		},{
			optionclass: 'ans-4 class4 my_font_big',
			textclass: '',
			textdata: data.string.p4text15d,
		}],

		clockblock : [{
			clockblockclass: 'clock-center-ques',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_fulltransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-1-45',
			minclass: 'hr-9',
			dotclass: '',
		}],
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-re-1',

		extratextblock:[{
			textdata: data.string.p4text8,
			textclass: 'ques-p4 my_font_very_big'
		}],

		option:[{
			optionclass: 'ans-1 class1 my_font_big',
			textclass: '',
			textdata: data.string.p4text16a,
		},{
			optionclass: 'ans-2 class2 my_font_big',
			textclass: '',
			textdata: data.string.p4text16b,
		},{
			optionclass: 'ans-3 class3 my_font_big',
			textclass: '',
			textdata: data.string.p4text16c,
		},{
			optionclass: 'ans-4 class4 my_font_big',
			textclass: '',
			textdata: data.string.p4text16d,
		}],

		clockblock : [{
			clockblockclass: 'clock-center-ques',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_fulltransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-9-45',
			minclass: 'hr-9',
			dotclass: '',
		}],
	},
	//to
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-re-1',

		extratextblock:[{
			textdata: data.string.p4text8,
			textclass: 'ques-p4 my_font_very_big'
		}],

		option:[{
			optionclass: 'ans-1 class1 my_font_big',
			textclass: '',
			textdata: data.string.p4text17a,
		},{
			optionclass: 'ans-2 class2 my_font_big',
			textclass: '',
			textdata: data.string.p4text17b,
		},{
			optionclass: 'ans-3 class3 my_font_big',
			textclass: '',
			textdata: data.string.p4text17c,
		},{
			optionclass: 'ans-4 class4 my_font_big',
			textclass: '',
			textdata: data.string.p4text17d,
		}],

		clockblock : [{
			clockblockclass: 'clock-center-ques',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_fulltransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-11-40',
			minclass: 'hr-8',
			dotclass: '',
		}],
	},
	//past
	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-re-1',

		extratextblock:[{
			textdata: data.string.p4text8,
			textclass: 'ques-p4 my_font_very_big'
		}],

		option:[{
			optionclass: 'ans-1 class1 my_font_big',
			textclass: '',
			textdata: data.string.p4text18a,
		},{
			optionclass: 'ans-2 class2 my_font_big',
			textclass: '',
			textdata: data.string.p4text18b,
		},{
			optionclass: 'ans-3 class3 my_font_big',
			textclass: '',
			textdata: data.string.p4text18c,
		},{
			optionclass: 'ans-4 class4 my_font_big',
			textclass: '',
			textdata: data.string.p4text18d,
		}],

		clockblock : [{
			clockblockclass: 'clock-center-ques',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_fulltransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-5-20',
			minclass: 'hr-4',
			dotclass: '',
		}],
	}
];


content2.shufflearray();
var content = content1.concat(content2);

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;


var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();


	var timeoutvar = null;

	// var vocabcontroller =  new Vocabulary();
	// vocabcontroller.init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// vocabcontroller.findwords(countNext);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
			case 0:
				play_diy_audio();
				nav_button_controls(3000);
				break;
			default:
				countNext===1?sound_player(sound_dg1):'';
				$prevBtn.show(0);
				for( var mm =1; mm<5; mm++){
					$('.option-div').removeClass('ans-'+mm);
				}
				var option_position = [1,2,3,4];
				option_position.shufflearray();
				for(var op=0; op<4; op++){
					$('.option-div').eq(op).addClass('ans-'+option_position[op]);
				}
				var wrong_clicked = false;
				$(".option-div").click(function(){
					if($(this).hasClass("class1")){
						$(".option-div").css('pointer-events', 'none');
						$(this).css({
							'border-color': '#DEEF3C',
							'background-color': '#98C02E',
							'color': 'white'
						});
						$(this).children('.correct-ans').show(0);
						play_correct_incorrect_sound(1);
						nav_button_controls(0);
					}
					else{
						$(this).css('pointer-events', 'none');
						$(this).css({
							'border-color': '#980000',
							'background-color': '#FF0000',
						});
						$(this).children('.incorrect-ans').show(0);
						play_correct_incorrect_sound(0);
					}
				});
				break;
		}
	}

	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				current_sound.stop();
				clearTimeout(timeoutvar);
				timeoutvar = null;
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		current_sound.stop();
		clearTimeout(timeoutvar);
		timeoutvar = null;
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

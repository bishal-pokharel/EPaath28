var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/TellTime/";

var sound_dg1 = new buzz.sound((soundAsset + "s6_p1.ogg"));
var sound_dg2 = new buzz.sound((soundAsset + "s6_p2.ogg"));
var sound_dg3 = new buzz.sound((soundAsset + "s6_p3.ogg"));
var sound_dg4 = new buzz.sound((soundAsset + "s6_p4.ogg"));
var sound_dg5 = new buzz.sound((soundAsset + "s6_p5.ogg"));
var sound_dg6 = new buzz.sound((soundAsset + "s6_p6.ogg"));
var sound_dg7 = new buzz.sound((soundAsset + "s6_p7.ogg"));
var sound_dg8 = new buzz.sound((soundAsset + "s6_p7.ogg"));
var soundArray = [sound_dg1, sound_dg2, sound_dg3, sound_dg4, sound_dg5,
									sound_dg6, sound_dg7, sound_dg8];
var current_sound = sound_dg1;

var content = [
	//slide10
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock min-margin added-class-1',
		uppertextblock : [{
			textdata : data.string.p3text1,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul my_font_very_big'
		},
		{
			textdata : data.string.p3text17,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: ' '
		},
		{
			textdata : data.string.p3text18,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: ' bold-ul'
		}],

		clockblock : [{
			haspasttext: true,
			pastimgclass: '',
			pastimgsrc: imgpath + 'past.png',
			pastimgclass2: '',
			pastimgsrc2: imgpath + 'pink.png',
			clockblockclass: 'clock-right-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_fulltransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-3-40',
			minclass: 'hr-8',
			dotclass: '',
			past0: data.string.past0,
			past30: data.string.past30,
			past5: data.string.past5,
			past10: data.string.past10,
			past15: data.string.past15,
			past20: data.string.past20,
			past25: data.string.past25,

			past35: data.string.past35,
			past40: data.string.past40,
			past45: data.string.past45,
			past50: data.string.past50,
			past55: data.string.past55,

			hashl: true,
			hlclass: 'hl_num_hidden',
			hashl2: true,
			hlclass2: 'hl_num_hidden2',
		}],
		optionsblock:[{
			option:[{
				textclass:"class_1",
				textdata:data.string.forty
			},{
				textclass:"",
				textdata:data.string.thirtyfive
			}]
		}]
	},
	//slide11
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock min-margin added-class-1',
		uppertextblock : [{
			textdata : data.string.p3text1,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul my_font_very_big'
		},
		{
			textdata : data.string.p3text19,
			textclass : 'my_font_big proximanova fade_in_1 let-eg-1',
			datahighlightflag: true,
			datahighlightcustomclass: 'y-text'
		},],

		clockblock : [{
			haspasttext: true,
			pastimgclass: '',
			pastimgsrc: imgpath + 'past.png',
			pastimgclass2: '',
			pastimgsrc2: imgpath + 'pink.png',
			clockblockclass: 'clock-right-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_fulltransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-3-40',
			minclass: 'hr-8',
			dotclass: '',
			past0: data.string.past0,
			past30: data.string.past30,
			past5: data.string.past5,
			past10: data.string.past10,
			past15: data.string.past15,
			past20: data.string.past20,
			past25: data.string.past25,

			past35: data.string.past35,
			past40: data.string.past40,
			past45: data.string.past45,
			past50: data.string.past50,
			past55: data.string.past55,

			hashl: true,
			hlclass: 'hl_num_hidden',
			hashl2: true,
			hlclass2: 'hl_num_hidden2',
		}]
	},
	//slide12
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock min-margin added-class-1',
		uppertextblock : [{
			textdata : data.string.p3text1,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul my_font_very_big'
		},
		{
			textdata : data.string.p3text20,
			textclass : 'my_font_big proximanova fade_in_1',
			datahighlightflag: true,
			datahighlightcustomclass: ' '
		},
		{
			textdata : data.string.p3text20a,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: ' bold-ul'
		}],

		clockblock : [{
			haspasttext: true,
			pastimgclass: '',
			pastimgsrc: imgpath + 'past.png',
			pastimgclass2: '',
			pastimgsrc2: imgpath + 'pink.png',
			clockblockclass: 'clock-right-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_fulltransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-3-40',
			minclass: 'hr-8',
			dotclass: '',
			past0: data.string.past0,
			past30: data.string.past30,
			past5: data.string.past5,
			past10: data.string.past10,
			past15: data.string.past15,
			past20: data.string.past20,
			past25: data.string.past25,

			past35: data.string.past35,
			past40: data.string.past40,
			past45: data.string.past45,
			past50: data.string.past50,
			past55: data.string.past55,
		}],
		optionsblock:[{
			option:[{
				textclass:"class_1",
				textdata:data.string.four
			},{
				textclass:"",
				textdata:data.string.three
			}]
		}]
	},
	//slide13
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock min-margin added-class-1',
		uppertextblock : [{
			textdata : data.string.p3text1,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul my_font_very_big'
		},
		{
			textdata : data.string.p3text20b,
			textclass : 'my_font_big proximanova fade_in_1 let-eg-1',
			datahighlightflag: true,
			datahighlightcustomclass: 'y-text'
		},],

		clockblock : [{
			haspasttext: true,
			pastimgclass: '',
			pastimgsrc: imgpath + 'past.png',
			pastimgclass2: '',
			pastimgsrc2: imgpath + 'pink.png',
			clockblockclass: 'clock-right-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_fulltransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-3-40',
			minclass: 'hr-8',
			dotclass: '',
			past0: data.string.past0,
			past30: data.string.past30,
			past5: data.string.past5,
			past10: data.string.past10,
			past15: data.string.past15,
			past20: data.string.past20,
			past25: data.string.past25,

			past35: data.string.past35,
			past40: data.string.past40,
			past45: data.string.past45,
			past50: data.string.past50,
			past55: data.string.past55,
		}]
	},
	//slide14
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock min-margin added-class-1',
		uppertextblock : [{
			textdata : data.string.p3text1,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul my_font_very_big'
		},
		{
			textdata : data.string.p3text21,
			textclass : 'my_font_big proximanova fade_in_1',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul '
		},
		{
			textdata : data.string.p3text21a,
			textclass : 'my_font_big proximanova fade_in-2',
			datahighlightflag: true,
			datahighlightcustomclass: ' '
		}],

		clockblock : [{
			haspasttext: true,
			pastimgclass: '',
			pastimgsrc: imgpath + 'past.png',
			pastimgclass2: '',
			pastimgsrc2: imgpath + 'pink.png',
			clockblockclass: 'clock-right-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_fulltransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-3-40',
			minclass: 'hr-8',
			dotclass: '',
			past0: data.string.past0,
			past30: data.string.past30,
			past5: data.string.past5,
			past10: data.string.past10,
			past15: data.string.past15,
			past20: data.string.past20,
			past25: data.string.past25,

			past35: data.string.past35,
			past40: data.string.past40,
			past45: data.string.past45,
			past50: data.string.past50,
			past55: data.string.past55,
		}]
	},
	//slide15
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock min-margin added-class-1',
		uppertextblock : [{
			textdata : data.string.p3text1,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul my_font_very_big'
		},
		{
			textdata : data.string.p3text22,
			textclass : 'my_font_big proximanova fade_in_1',
			datahighlightflag: true,
			datahighlightcustomclass: ' '
		}],

		extratextblock : [{
			textdata : data.string.p3text22a,
			textclass : 'my_font_big proximanova added-bottom fade_in-3',
		}],

		clockblock : [{
			haspasttext: true,
			pastimgclass: '',
			pastimgsrc: imgpath + 'past.png',
			pastimgclass2: '',
			pastimgsrc2: imgpath + 'pink.png',
			clockblockclass: 'clock-right-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_fulltransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-3-35',
			minclass: 'hr-7',
			dotclass: '',
			past0: data.string.past0,
			past30: data.string.past30,
			past5: data.string.past5,
			past10: data.string.past10,
			past15: data.string.past15,
			past20: data.string.past20,
			past25: data.string.past25,

			past35: data.string.past35,
			past40: data.string.past40,
			past45: data.string.past45,
			past50: data.string.past50,
			past55: data.string.past55,
		}]
	},






	//slide16
	// {
		// hasheaderblock : false,
		// contentblocknocenteradjust : true,
		// contentblockadditionalclass : 'bg-diy-final',
//
		// extratextblock : [{
			// textdata : data.string.diytext,
			// textclass : 'diy-text-final',
		// }]
	// },
	//slide17
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock aligned-left min-margin rework-1',
		uppertextblock : [{
			textdata : data.string.p3text25,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'hand-text-topic my_font_very_big'
		},
		{
			textdata : data.string.p3text26,
			textclass : 'options-clock my_font_big proximanova class-2',
		},
		{
			textdata : data.string.p3text27,
			textclass : 'options-clock my_font_big proximanova class-1',
		},
		{
			textdata : data.string.p3text28,
			textclass : 'options-clock my_font_big proximanova class-3',
		},
		{
			textdata : data.string.p3text29,
			textclass : 'options-clock my_font_big proximanova class-4',
		}
		],
		clockblock : [{
			haspasttext: true,
			pastimgclass: '',
			pastimgsrc: imgpath + 'past.png',
			pastimgclass2: '',
			pastimgsrc2: imgpath + 'pink.png',
			clockblockclass: 'clock-right-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_fulltransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-4-50',
			minclass: 'hr-10',
			dotclass: '',
			past0: data.string.past0,
			past30: data.string.past30,
			past5: data.string.past5,
			past10: data.string.past10,
			past15: data.string.past15,
			past20: data.string.past20,
			past25: data.string.past25,

			past35: data.string.past35,
			past40: data.string.past40,
			past45: data.string.past45,
			past50: data.string.past50,
			past55: data.string.past55,
		}],

		extratextblock : [{
			textdata : data.string.p3text23,
			textclass : 'p2-hint-text p2-hint-1',
		},
		{
			textdata : data.string.p3text24,
			textclass : 'p2-hint-text p2-hint-2',
		},
		{
			textdata : '',
			textclass : 'yellow-half',
		},
		{
			textdata : data.string.practice,
			textclass : 'practice-text',
		}],
	},

	//slide18
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock aligned-left min-margin rework-1',
		uppertextblock : [{
			textdata : data.string.p3text30,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'hand-text-topic my_font_very_big'
		},
		{
			textdata : data.string.p3text32,
			textclass : 'options-clock my_font_big proximanova class-1',
		},
		{
			textdata : data.string.p3text31,
			textclass : 'options-clock my_font_big proximanova class-2',
		},
		{
			textdata : data.string.p3text34,
			textclass : 'options-clock my_font_big proximanova class-4',
		},
		{
			textdata : data.string.p3text33,
			textclass : 'options-clock my_font_big proximanova class-3',
		}
		],
		clockblock : [{
			haspasttext: true,
			pastimgclass: '',
			pastimgsrc: imgpath + 'past.png',
			pastimgclass2: '',
			pastimgsrc2: imgpath + 'pink.png',
			clockblockclass: 'clock-right-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_fulltransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-1-40',
			minclass: 'hr-8',
			dotclass: '',
			past0: data.string.past0,
			past30: data.string.past30,
			past5: data.string.past5,
			past10: data.string.past10,
			past15: data.string.past15,
			past20: data.string.past20,
			past25: data.string.past25,

			past35: data.string.past35,
			past40: data.string.past40,
			past45: data.string.past45,
			past50: data.string.past50,
			past55: data.string.past55,
		}],

		extratextblock : [{
			textdata : data.string.p3text23,
			textclass : 'p2-hint-text p2-hint-1',
		},
		{
			textdata : data.string.p3text24,
			textclass : 'p2-hint-text p2-hint-2',
		},
		{
			textdata : '',
			textclass : 'yellow-half',
		},
		{
			textdata : data.string.practice,
			textclass : 'practice-text',
		}],
	}
];


$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;


	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var timeoutvar = null;

	// var vocabcontroller =  new Vocabulary();
	// vocabcontroller.init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// vocabcontroller.findwords(countNext);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);


		function correct_Incor(this_class, corFlag, navFlag){
			if(corFlag){
				this_class.addClass("correct");
				this_class.parent().parent().css("pointer-events", "none");
				play_correct_incorrect_sound(1);
				this_class.siblings(".correct-icon").show();
				navFlag?setTimeout(function(){$nextBtn.trigger("click")}, 1000):'';
			}else{
				this_class.addClass("incorrect");
				this_class.siblings(".incorrect-icon").show();
				play_correct_incorrect_sound(0);
			}
		}
		switch (countNext) {
			case 0:
				sound_player(soundArray[countNext]);
				$('.hl_num_hidden').css('transform','rotate(150deg)').show(0);
				$(".opn_txt").click(function(){
					if($(this).hasClass("class_1")){
						$(".optn_blk_sec, .text-5").show(0);
						$(this).hasClass("grp_1")?correct_Incor($(this), 1, 0):
						correct_Incor($(this), 1, 1);
					}else{
						correct_Incor($(this), 0, 0);
					}
				});
			break;
			case 2:
				sound_player(soundArray[countNext]);
				$(".opn_txt").click(function(){
					if($(this).hasClass("class_1")){
						$(".optn_blk_sec, .text-5").show(0);
						$(this).hasClass("grp_1")?correct_Incor($(this), 1, 0):
						correct_Incor($(this), 1, 1);
					}else{
						correct_Incor($(this), 0, 0);
					}
				});
			break;
			case 1:
				$prevBtn.show(0);
					sound_and_nav(soundArray[countNext]);
				timeoutvar =  setTimeout(function(){
					$('.hl_num_hidden2').css('transform','rotate(150deg)').show(0);
					// nav_button_controls(100);
				}, 2000);
				break;
			case 6:
			case 7:
				$prevBtn.show(0);
					sound_player(soundArray[countNext]);
				$('[class*="past-"]').hide(0);
				$('.options-clock').click(function(){
					$(this).addClass('selected');
					if( $(this).hasClass('class-1') ){
						$(this).css({
							'background-color': '#98c02e',
							'border':'4px solid #ff0'
						});
						$('.options-clock').unbind('click');
						$('.options-clock').addClass('selected');
						play_correct_incorrect_sound(1);
						nav_button_controls(500);
					} else {
						$(this).css({
							'background-color': '#ff0000',
							'border':'4px solid #000'
						});
						if( $(this).hasClass('class-2') ){
							$('.p2-hint-2').animate({
								'right':'-45%'
							}, 400, function(){
								$('.p2-hint-1').animate({
									'right':'0%'
								}, 400);
							});
						} else if( $(this).hasClass('class-3') ){
							$('[class*="past-"]').delay(400).fadeIn(400, function(){
								$('[class*="past-"]').removeClass('blink_numbers');
								timeoutvar = setTimeout(function(){
									$('[class*="past-"]').addClass('blink_numbers');
								}, 17);
							});
						} else if( $(this).hasClass('class-4') ){
							$('.p2-hint-1').animate({
								'right':'-45%'
							}, 400, function(){
								$('.p2-hint-2').animate({
									'right':'0%'
								}, 400);
							});
							$('[class*="past-"]').delay(400).fadeIn(400, function(){
								$('[class*="past-"]').removeClass('blink_numbers');
								timeoutvar = setTimeout(function(){
									$('[class*="past-"]').addClass('blink_numbers');
								}, 17);
							});
						}
						$(this).unbind('click');
						play_correct_incorrect_sound(0);
					}
				});
				break;
			// case 4:
			// case 5:
			// 	$prevBtn.show(0);
			// 	nav_button_controls(3000);
			// 	break;
			default:
				sound_and_nav(soundArray[countNext]);
				break;
		}
	}

	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}

	function sound_and_nav(sound_data, clickfunction , a){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			if(typeof clickfunction != 'undefined'){
				clickfunction(a, sound_data);
			}
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		});
	}
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				current_sound.stop();
				clearTimeout(timeoutvar);
				timeoutvar = null;
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		current_sound.stop();
		clearTimeout(timeoutvar);
		timeoutvar = null;
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

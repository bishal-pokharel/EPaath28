var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/TellTime/";

var sound_dg1 = new buzz.sound((soundAsset + "s1_p1.ogg"));
var sound_dg2 = new buzz.sound((soundAsset + "s1_p2.ogg"));
var sound_dg3 = new buzz.sound((soundAsset + "s1_p3.ogg"));
var sound_dg4 = new buzz.sound((soundAsset + "s1_p4.ogg"));
// var sound_dg5 = new buzz.sound((soundAsset + "s1_p1.ogg"));

var soundsarr = [sound_dg1, sound_dg2, sound_dg3, sound_dg4];

var current_sound = sound_dg1;

var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-cover',

		extratextblock : [{
			textdata : data.lesson.chapter,
			textclass : 'cover-text',
		}]
	},

	// //slide1
	// {
		// hasheaderblock : false,
		// contentblocknocenteradjust : true,
		// contentblockadditionalclass : 'bg-blue',
//
		// extratextblock : [{
			// textdata : data.string.p1text1,
			// textclass : 'template-dialougebox2-top-flipped-yellow dg-5 patrickhand my_font_big'
		// }],
//
		// imageblock : [{
			// imagestoshow : [
				// {
					// imgclass : "boy boy-1",
					// imgsrc : imgpath + "rahul.png",
				// },
				// {
					// imgclass : "girl girl-1",
					// imgsrc : imgpath + "anjana02.png",
				// }
			// ],
		// }],
//
		// clockblock : [{
			// clockblockclass: 'clock-right-1',
			// clockbodyclass: '',
			// clockbodysrc: imgpath + 'clockbody.png',
			// clocktextclass: '',
			// clocktextsrc: imgpath + 'numbers.png',
			// hrclass: 'hr-3',
			// minclass: 'hr-12',
			// dotclass: '',
		// }]
	// },
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		extratextblock : [{
			textdata : data.string.p1text2,
			textclass : 'template-dialougebox2-top-flipped-yellow dg-new patrickhand my_font_big'
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "boy boy-1",
					imgsrc : imgpath + "rahul.png",
				},
				{
					imgclass : "girl girl-1",
					imgsrc : imgpath + "anjana02.png",
				}
			],
		}],

		clockblock : [{
			clockblockclass: 'clock-right-1',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: '',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'hr-3',
			minclass: 'hr-12',
			dotclass: '',
			q1data: data.string.q0,
			q1class: 'my_font_big patrickhand',
			q2data: data.string.q1,
			q2class: 'my_font_big patrickhand',
			q3data: data.string.q2,
			q3class: 'my_font_big patrickhand',
			q4data: data.string.q3,
			q4class: 'my_font_big patrickhand',
		}]
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		extratextblock : [{
			textdata : data.string.p1text3,
			textclass : 'template-dialougebox2-top-yellow dg-1 patrickhand my_font_big'
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "boy boy-1",
					imgsrc : imgpath + "rahul01.png",
				},
				{
					imgclass : "girl girl-1",
					imgsrc : imgpath + "anjana04.png",
				}
			],
		}],

		clockblock : [{
			clockblockclass: 'clock-right-1',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: '',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'hr-3',
			minclass: 'hr-12',
			dotclass: '',
			q1data: data.string.q0,
			q1class: 'my_font_big patrickhand',
			q2data: data.string.q1,
			q2class: 'my_font_big patrickhand',
			q3data: data.string.q2,
			q3class: 'my_font_big patrickhand',
			q4data: data.string.q3,
			q4class: 'my_font_big patrickhand',
		}]
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		extratextblock : [{
			textdata : data.string.p1text4,
			textclass : 'template-dialougebox2-top-flipped-yellow dg-5 patrickhand my_font_big'
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "boy boy-1",
					imgsrc : imgpath + "rahul.png",
				},
				{
					imgclass : "girl girl-1",
					imgsrc : imgpath + "anjana02.png",
				}
			],
		}],

		clockblock : [{
			clockblockclass: 'clock-right-1',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: '',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'hr-3',
			minclass: 'hr-12',
			dotclass: '',
			q1data: data.string.q0,
			q1class: 'my_font_big patrickhand',
			q2data: data.string.q1,
			q2class: 'my_font_big patrickhand',
			q3data: data.string.q2,
			q3class: 'my_font_big patrickhand',
			q4data: data.string.q3,
			q4class: 'my_font_big patrickhand',
		}]
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;

	var timeoutvar = null;
	var timeoutvar2 = null;
	var timeoutvar3 = null;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);

		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
			case 0:
				sound_and_nav(soundsarr[countNext], click_dg, '.dg-1');
				// nav_button_controls(100);
				break;
			case 1:
			case 3:
				$prevBtn.show(0);
				sound_and_nav(soundsarr[countNext], click_dg, '.dg-5');
				break;
			case 2:
				$prevBtn.show(0);
				sound_and_nav(soundsarr[countNext], click_dg, '.dg-1');
				break;
			default:
				$prevBtn.show(0);
				sound_and_nav(soundsarr[countNext], click_dg, '.dg-1');
				break;
		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar3 = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}

	function sound_and_nav(sound_data, clickfunction , a){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			if(typeof clickfunction != 'undefined'){
				clickfunction(a, sound_data);
			}
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		});
	}
	function click_dg(dg_class, audio){
		$(dg_class).click(function(){
			sound_player(audio);
		});
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				current_sound.stop();
				clearTimeout(timeoutvar);
				clearTimeout(timeoutvar2);
				clearTimeout(timeoutvar3);
				timeoutvar = null;
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		current_sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		clearTimeout(timeoutvar3);
		timeoutvar = null;
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/TellTime/";

var sound_dg1 = new buzz.sound((soundAsset + "s4_p2.ogg"));
var sound_dg2 = new buzz.sound((soundAsset + "s4_p3.ogg"));
var sound_dg3 = new buzz.sound((soundAsset + "s4_p4.ogg"));
var sound_dg4 = new buzz.sound((soundAsset + "s4_p6.ogg"));

var current_sound = sound_dg1;

var content = [
	//slide20
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-diy-final',

		extratextblock : [{
			textdata : data.string.diytext,
			textclass : 'diy-text-final',
		}]
	},
	//slide21
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'relative-textblock p2-ques',
		uppertextblock : [{
			textdata : data.string.p2text28,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'hand-text-topic my_font_very_big'
		},
		],

		clockblock : [{
			clockblockclass: 'clock-options clock-class-2',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-4-40',
			minclass: 'hr-8',
			dotclass: '',
			hascorrect: true
		},
		{
			clockblockclass: 'clock-options clock-class-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-5-20',
			minclass: 'hr-4',
			dotclass: '',
			hascorrect: true
		},
		{
			clockblockclass: 'clock-options clock-class-4',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-4',
			minclass: 'hr-12',
			dotclass: '',
			hascorrect: true
		},
		{
			clockblockclass: 'clock-options clock-class-1',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-4-20',
			minclass: 'hr-4',
			dotclass: '',
			hascorrect: true
		}],
	},
	//slide22
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'relative-textblock p2-ques',
		uppertextblock : [{
			textdata : data.string.p2text29,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'hand-text-topic my_font_very_big'
		},
		],

		clockblock : [{
			clockblockclass: 'clock-options clock-class-2',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-5',
			minclass: 'hr-12',
			dotclass: '',
			hascorrect: true
		},
		{
			clockblockclass: 'clock-options clock-class-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-4-25',
			minclass: 'hr-5',
			dotclass: '',
			hascorrect: true
		},
		{
			clockblockclass: 'clock-options clock-class-4',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-5-35',
			minclass: 'hr-7',
			dotclass: '',
			hascorrect: true
		},
		{
			clockblockclass: 'clock-options clock-class-1',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-5-25',
			minclass: 'hr-5',
			dotclass: '',
			hascorrect: true
		}],
	},

	//slide23
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock aligned-left min-margin',
		uppertextblock : [{
			textdata : data.string.p2text30,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'hand-text-topic my_font_very_big'
		},
		{
			textdata : data.string.p2text32,
			textclass : 'options-clock my_font_big proximanova class-2',
		},
		{
			textdata : data.string.p2text33,
			textclass : 'options-clock my_font_big proximanova class-3',
		},
		{
			textdata : data.string.p2text34,
			textclass : 'options-clock my_font_big proximanova class-1',
		},
		{
			textdata : data.string.p2text35,
			textclass : 'options-clock my_font_big proximanova class-4',
		}
		],

		clockblock : [{
			haspasttext: true,
			pastimgclass: '',
			pastimgsrc: imgpath + 'past.png',
			clockblockclass: 'clock-right-4',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_halftransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-6-25',
			minclass: 'hr-5',
			dotclass: '',
			past0: data.string.past0,
			past30: data.string.past30,
			past5: data.string.past5,
			past10: data.string.past10,
			past15: data.string.past15,
			past20: data.string.past20,
			past25: data.string.past25,
		}],

		extratextblock : [{
			textdata : data.string.p2text40,
			textclass : 'p2-hint-text p2-hint-1',
		},
		{
			textdata : data.string.p2text41,
			textclass : 'p2-hint-text p2-hint-2',
		}],
	},

	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock aligned-left min-margin',
		uppertextblock : [{
			textdata : data.string.p2text31,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'hand-text-topic my_font_very_big'
		},
		{
			textdata : data.string.p2text36,
			textclass : 'options-clock my_font_big proximanova class-2',
		},
		{
			textdata : data.string.p2text37,
			textclass : 'options-clock my_font_big proximanova class-1',
		},
		{
			textdata : data.string.p2text38,
			textclass : 'options-clock my_font_big proximanova class-3',
		},
		{
			textdata : data.string.p2text39,
			textclass : 'options-clock my_font_big proximanova class-4',
		}
		],

		clockblock : [{
			haspasttext: true,
			pastimgclass: '',
			pastimgsrc: imgpath + 'past.png',
			clockblockclass: 'clock-right-4',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_halftransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-9-05',
			minclass: 'hr-1',
			dotclass: '',
			past0: data.string.past0,
			past30: data.string.past30,
			past5: data.string.past5,
			past10: data.string.past10,
			past15: data.string.past15,
			past20: data.string.past20,
			past25: data.string.past25,
		}],

		extratextblock : [{
			textdata : data.string.p2text40,
			textclass : 'p2-hint-text p2-hint-1',
		},
		{
			textdata : data.string.p2text41,
			textclass : 'p2-hint-text p2-hint-2',
		}],
	},
	//slide24
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		extratextblock : [{
			textdata : data.string.p2text55,
			textclass : 'template-dialougebox2-top-yellow dg-1'
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "boy boy-1",
					imgsrc : imgpath + "rahul.png",
				},
				{
					imgclass : "girl girl-1",
					imgsrc : imgpath + "anjana.png",
				}
			],
		}],

		clockblock : [{
			clockblockclass: 'clock-right',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: 'no-brightness',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'hr-3-50',
			minclass: 'hr-10',
			dotclass: ''
		}]
	},
];

content[1].clockblock.shufflearray();
content[2].clockblock.shufflearray();

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;


	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();


	var timeoutvar = null;

	// var vocabcontroller =  new Vocabulary();
	// vocabcontroller.init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// vocabcontroller.findwords(countNext);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
			case 0:
				play_diy_audio();
				nav_button_controls(3000);
			break;
			case 1:
			case 2:
				countNext===1?
					sound_player(sound_dg1):
					sound_player(sound_dg2);
				$prevBtn.show(0);
				$('.clock-options').click(function(){
					$(this).addClass('selected');
					if( $(this).hasClass('clock-class-1') ){
						$(this).children('.correct-icon').show(0);
						$('.clock-options').unbind('click');
						$('.clock-options').addClass('selected');
						play_correct_incorrect_sound(1);
						nav_button_controls(500);
					} else {
						$(this).children('.incorrect-icon').show(0);
						$(this).unbind('click');
						play_correct_incorrect_sound(0);
					}
				});
				break;
			case 3:
			case 4:
					sound_player(sound_dg3);
				if(countNext >0){
					$prevBtn.show(0);
				}
				$('[class*="past-"]').hide(0);
				$('.options-clock').click(function(){
					$('.min-hand').removeClass('hand-hl');
					$(this).addClass('selected');
					if( $(this).hasClass('class-1') ){
						$(this).css({
							'background-color': '#98c02e',
							'border':'4px solid #ff0'
						});
						$('.options-clock').unbind('click');
						$('.options-clock').addClass('selected');
						play_correct_incorrect_sound(1);
						nav_button_controls(500);
					} else {
						$(this).css({
							'background-color': '#ff0000',
							'border':'4px solid #000'
						});

						if( $(this).hasClass('class-2') ){
							$('.p2-hint-2').animate({
								'right':'-45%'
							}, 400, function(){
								$('.p2-hint-1').animate({
									'right':'0%'
								}, 400);
							});
							$('.min-hand').addClass('hand-hl');
						} else if( $(this).hasClass('class-3') ){
							$('[class*="past-"]').delay(400).fadeIn(400, function(){
								$('[class*="past-"]').removeClass('blink_numbers');
								timeoutvar = setTimeout(function(){
									$('[class*="past-"]').addClass('blink_numbers');
								}, 17);
							});
						} else if( $(this).hasClass('class-4') ){
							$('.p2-hint-1').animate({
								'right':'-45%'
							}, 400, function(){
								$('.p2-hint-2').animate({
									'right':'0%'
								}, 400);
							});
							$('[class*="past-"]').delay(400).fadeIn(400, function(){
								$('[class*="past-"]').removeClass('blink_numbers');
								timeoutvar = setTimeout(function(){
									$('[class*="past-"]').addClass('blink_numbers');
								}, 17);
							});
						}
						$(this).unbind('click');
						play_correct_incorrect_sound(0);
					}
				});
				break;
			case 5:
				$prevBtn.show(0);
				sound_and_nav(sound_dg4, click_dg, '.dg-1');
				break;
			default:
				nav_button_controls(100);
				break;
		}
	}

	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}

	function sound_and_nav(sound_data, clickfunction , a){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			if(typeof clickfunction != 'undefined'){
				clickfunction(a, sound_data);
			}
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		});
	}
	function click_dg(dg_class, audio){
		$(dg_class).click(function(){
			sound_player(audio);
		});
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				current_sound.stop();
				clearTimeout(timeoutvar);
				timeoutvar = null;
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		current_sound.stop();
		clearTimeout(timeoutvar);
		timeoutvar = null;
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

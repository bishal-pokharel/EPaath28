var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/TellTime/";

var sound_dg1 = new buzz.sound((soundAsset + "s2_p1(1).ogg"));
var sound_dg2 = new buzz.sound((soundAsset + "s2_p1(2).ogg"));
var sound_dg3 = new buzz.sound((soundAsset + "s2_p1(3).ogg"));
var sound_dg4 = new buzz.sound((soundAsset + "s2_p2.ogg"));
var sound_dg4_1 = new buzz.sound((soundAsset + "s2_p2_1.ogg"));
var sound_dg5 = new buzz.sound((soundAsset + "s2_p3.ogg"));
var sound_dg6 = new buzz.sound((soundAsset + "s2_p4.ogg"));
var sound_dg7 = new buzz.sound((soundAsset + "s2_p4(2).ogg"));
var sound_dg8 = new buzz.sound((soundAsset + "s2_p5_1.ogg"));
var sound_dg8_1 = new buzz.sound((soundAsset + "s2_p5_2.ogg"));
var sound_dg8_2 = new buzz.sound((soundAsset + "s2_p5_3.ogg"));
var sound_dg9 = new buzz.sound((soundAsset + "s2_p6.ogg"));
var sound_dg10 = new buzz.sound((soundAsset + "s2_p7.ogg"));
var sound_dg11 = new buzz.sound((soundAsset + "s2_p8.ogg"));
var sound_dg12 = new buzz.sound((soundAsset + "s2_p9.ogg"));
var sound_dg13 = new buzz.sound((soundAsset + "s2_p10.ogg"));
var sound_dg14 = new buzz.sound((soundAsset + "s2_p11.ogg"));
var sound_dg15 = new buzz.sound((soundAsset + "s2_p12.ogg"));
var sound_dg15_1 = new buzz.sound((soundAsset + "s2_p12_1.ogg"));
var sound_dg15_2 = new buzz.sound((soundAsset + "s2_p12_2.ogg"));
var sound_dg16 = new buzz.sound((soundAsset + "s2_p13.ogg"));
var sound_dg16_1 = new buzz.sound((soundAsset + "s2_p13_1.ogg"));
var sound_dg16_2 = new buzz.sound((soundAsset + "s2_p13_2.ogg"));
var sndArr = [sound_dg2, sound_dg3, sound_dg4, sound_dg4_1, sound_dg5,
							sound_dg6, sound_dg7, sound_dg8, sound_dg9,
						  sound_dg10, sound_dg11, sound_dg12, sound_dg13,
						  sound_dg14, sound_dg15, sound_dg16];
var current_sound = sound_dg1;

var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock min-margin added-class-2',
		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul my_font_very_big'
		},
		{
			textdata : data.string.p2text9,
			textclass : 'my_font_big proximanova its_hidden text-2',
		},
		{
			textdata : data.string.p2text9a,
			textclass : 'my_font_big proximanova its_hidden text-3'
		}],

		// clockblock : [{
		// 	hashl: true,
		// 	hlclass: 'hl-num-05',
		// 	clockblockclass: 'clock-right',
		// 	clockbodyclass: '',
		// 	clockbodysrc: imgpath + 'clock01.png',
		// 	hrclass: 'hr-3-10',
		// 	minclass: 'hr-2',
		// 	dotclass: ''
		// }],
		svgblock:[{
			svgblock_id:"clock_svg_container"
		}]
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock aligned-left min-margin-p2 added-class-1',
		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : 'past-title my_font_very_big proximanova fade_in_1',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul my_font_very_big'
		},
		{
			textdata : data.string.p2text2,
			textclass : 'my_font_big proximanova its_hidden text-2'
		},
		{
			textdata : data.string.p2text2a,
			textclass : 'my_font_big proximanova its_hidden text-3',
		},
		],

		clockblock : [{
			clockblockclass: 'clock-right',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-3-10',
			minclass: 'hr-2',
			dotclass: ''
		}]
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock aligned-left min-margin-p2 added-class-1',
		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : 'past-title my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul my_font_very_big'
		},
		{
			textdata : data.string.p2text3,
			textclass : 'my_font_big proximanova its_hidden text-2'
		}],

		clockblock : [{
			clockblockclass: 'clock-right',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-3-10',
			minclass: 'hr-2',
			dotclass: ''
		}]
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock min-margin added-class-2',
		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul my_font_very_big'
		},
		{
			textdata : data.string.p2text4a,
			textclass : 'my_font_big proximanova text-2'
		},
		{
			textdata : data.string.p2text4b,
			textclass : 'my_font_big proximanova text-3'
		},
		{
			textdata : data.string.p2text4,
			textclass : 'n-text my_font_big proximanova op_0 text-4'
		},
		{
			textdata : data.string.p2text5,
			textclass : 'my_font_big proximanova text-5'
		},
		{
			textdata : data.string.p2text4,
			textclass : 'n-text my_font_big proximanova op_0 text-6'
		}
	],

		clockblock : [{
			clockblockclass: 'clock-right',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-3-10',
			minclass: 'hr-2',
			dotclass: ''
		}],
		optionsblock:[{
			option:[{
				textclass:"grp_1",
				textdata:data.string.yes
			},{
				textclass:"grp_1 class_1",
				textdata:data.string.no
			}]
		},{
			optionclass:"optn_blk_sec",
			option:[{
				textclass:"grp_2",
				textdata:data.string.yes
			},{
				textclass:"grp_2 class_1",
				textdata:data.string.no
			}]
		}]
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock min-margin added-class-2',
		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul my_font_very_big'
		},
		{
			textdata : data.string.p2text6,
			textclass : 'my_font_big proximanova its_hidden text-2',
			datahighlightflag: true,
			datahighlightcustomclass: 'y-text'
		},
		{
			textdata : data.string.p2text7,
			textclass : 'my_font_big proximanova its_hidden text-3'
		},
		{
			textdata : data.string.p2text8,
			textclass : 'my_font_big proximanova its_hidden text-4'
		}],

		clockblock : [{
			clockblockclass: 'clock-right',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-3-10',
			minclass: 'hr-2',
			dotclass: ''
		}]
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock min-margin added-class-2',
		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul my_font_very_big'
		},
		{
			textdata : data.string.p2text10,
			textclass : 'my_font_big proximanova fade_in_1',
			datahighlightflag: true,
			datahighlightcustomclass: 'ul'
		}],

		clockblock : [{
			haspasttext: true,
			pastimgclass: '',
			pastimgsrc: imgpath + 'past.png',
			clockblockclass: 'clock-right-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_halftransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-3-10',
			minclass: 'hr-2',
			dotclass: '',
			past0: data.string.past0,
			past30: data.string.past30,
			past5: data.string.past5,
			past10: data.string.past10,
			past15: data.string.past15,
			past20: data.string.past20,
			past25: data.string.past25,
		}],
		draganddrop:[{
			question:true,
			dnd_qn_txt:data.string.dnd_qn,
			drag_group:true,
			dragcomponent:[{
				dragadditonalclass:"drag-0",
				dragdata:data.string.past0
			},{
				dragadditonalclass:"drag-15",
				dragdata:data.string.past15
			},{
				dragadditonalclass:"drag-30",
				dragdata:data.string.past30
			}]
		},{
			drop_group:true,
			dropcomponent:[{
				dropadditionalclass:"drop-0"
			},{
				dropadditionalclass:"drop-15"
			},{
				dropadditionalclass:"drop-30"
			}]
		}]
	},
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock min-margin added-class-1',
		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul my_font_very_big'
		},
		{
			textdata : data.string.p2text11,
			textclass : 'my_font_big proximanova fade_in_1 let-eg-1',
			datahighlightflag: true,
			datahighlightcustomclass: 'ul'
		},],

		clockblock : [{
			haspasttext: true,
			pastimgclass: '',
			pastimgsrc: imgpath + 'past.png',
			clockblockclass: 'clock-right-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_halftransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-3-10',
			minclass: 'hr-2',
			dotclass: '',
			past0: data.string.past0,
			past30: data.string.past30,
			past5: data.string.past5,
			past10: data.string.past10,
			past15: data.string.past15,
			past20: data.string.past20,
			past25: data.string.past25,
		}]
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock min-margin added-class-1',
		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul my_font_very_big'
		},
		{
			textdata : data.string.p2text12,
			textclass : 'my_font_big proximanova fade_in_1',
			datahighlightflag: true,
			datahighlightcustomclass: 'ul'
		},{
			textdata : data.string.p2text12a,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'ul'
		}],

		clockblock : [{
			haspasttext: true,
			pastimgclass: '',
			pastimgsrc: imgpath + 'past.png',
			clockblockclass: 'clock-right-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_halftransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-3-10',
			minclass: 'hr-2',
			dotclass: '',
			past0: data.string.past0,
			past30: data.string.past30,
			past5: data.string.past5,
			past10: data.string.past10,
			past15: data.string.past15,
			past20: data.string.past20,
			past25: data.string.past25,
		}],
		optionsblock:[{
			option:[{
				textclass:"",
				textdata:data.string.fiften
			},{
				textclass:"class_1",
				textdata:data.string.ten
			}]
		}]
	},
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock min-margin added-class-1',
		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul my_font_very_big'
		},
		{
			textdata : data.string.p2text13,
			textclass : 'my_font_big proximanova fade_in_1 let-eg-1',
			datahighlightflag: true,
			datahighlightcustomclass: 'y-text'
		},],

		clockblock : [{
			haspasttext: true,
			pastimgclass: '',
			pastimgsrc: imgpath + 'past.png',
			clockblockclass: 'clock-right-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_halftransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-3-10',
			minclass: 'hr-2',
			dotclass: '',
			past0: data.string.past0,
			past30: data.string.past30,
			past5: data.string.past5,
			past10: data.string.past10,
			past15: data.string.past15,
			past20: data.string.past20,
			past25: data.string.past25,
			past10class: 'highlighted-1'
		}]
	},
	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock min-margin added-class-1',
		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul my_font_very_big'
		},
		{
			textdata : data.string.p2text14,
			textclass : 'my_font_big proximanova fade_in_1',
			datahighlightflag: true,
			datahighlightcustomclass: 'ul'
		},{
			textdata : data.string.p2text14a,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'ul'
		}],

		clockblock : [{
			haspasttext: true,
			pastimgclass: '',
			pastimgsrc: imgpath + 'past.png',
			clockblockclass: 'clock-right-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_halftransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-3-10',
			minclass: 'hr-2',
			dotclass: '',
			past0: data.string.past0,
			past30: data.string.past30,
			past5: data.string.past5,
			past10: data.string.past10,
			past15: data.string.past15,
			past20: data.string.past20,
			past25: data.string.past25,
		}],
		optionsblock:[{
			optionclass:"optn_blk_sec",
			option:[{
				textclass:"",
				textdata:data.string.four
			},{
				textclass:"class_1",
				textdata:data.string.three
			}]
		}]
	},
	//slide10
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock min-margin added-class-1',
		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul my_font_very_big'
		},
		{
			textdata : data.string.p2text15,
			textclass : 'my_font_big proximanova fade_in_1 let-eg-1',
			datahighlightflag: true,
			datahighlightcustomclass: 'y-text'
		},],

		clockblock : [{
			haspasttext: true,
			pastimgclass: '',
			hashl: true,
			hlclass: 'hr-3 highlighted-2',
			pastimgsrc: imgpath + 'past.png',
			clockblockclass: 'clock-right-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_halftransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-3-10',
			minclass: 'hr-2',
			dotclass: '',
			past0: data.string.past0,
			past30: data.string.past30,
			past5: data.string.past5,
			past10: data.string.past10,
			past15: data.string.past15,
			past20: data.string.past20,
			past25: data.string.past25,
		}]
	},
	//slide11
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock min-margin added-class-1',
		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul my_font_very_big'
		},
		{
			textdata : data.string.p2text16,
			textclass : 'my_font_big proximanova its_hidden text-2',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul'
		},{
			textdata : data.string.p2text17,
			textclass : 'my_font_big proximanova its_hidden text-3',
		},{
			textdata : data.string.p2text17a,
			textclass : 'my_font_big proximanova its_hidden text-4',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold'
		}],

		clockblock : [{
			haspasttext: true,
			pastimgclass: '',
			pastimgsrc: imgpath + 'past.png',
			clockblockclass: 'clock-right-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_halftransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-3-10',
			minclass: 'hr-2',
			dotclass: '',
			past0: data.string.past0,
			past30: data.string.past30,
			past5: data.string.past5,
			past10: data.string.past10,
			past15: data.string.past15,
			past20: data.string.past20,
			past25: data.string.past25,
		}]
	},
	//slide12
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		uppertextblockadditionalclass: 'half-yellow-textblock min-margin added-class-1',
		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : 'my_font_big proximanova',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul my_font_very_big'
		},{
			textdata : data.string.p2text18,
			textclass : 'my_font_big proximanova its_hidden text-2',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold-ul'
		},{
			textdata : data.string.p2text18a,
			textclass : 'my_font_big proximanova its_hidden text-3',
			datahighlightflag: true,
			datahighlightcustomclass: 'bold'
		}],
		extratextblock : [{
			textdata : data.string.p2text19,
			textclass : 'my_font_big proximanova added-bottom its_hidden text-4',
		}],
		clockblock : [{
			haspasttext: true,
			pastimgclass: '',
			pastimgsrc: imgpath + 'past.png',
			clockblockclass: 'clock-right-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock_halftransparent.png',
			clocktextsrc: imgpath + 'numbers01.png',
			hrclass: 'hr-3-10',
			minclass: 'hr-2',
			dotclass: '',
			past0: data.string.past0,
			past30: data.string.past30,
			past5: data.string.past5,
			past10: data.string.past10,
			past15: data.string.past15,
			past20: data.string.past20,
			past25: data.string.past25,
		}]
	}
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;


	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();


	var timeoutvar = null;

	// var vocabcontroller =  new Vocabulary();
	// vocabcontroller.init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// vocabcontroller.findwords(countNext);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		var cor_counter = 0;

		function correct_Incor(this_class, corFlag, navFlag){
			if(corFlag){
				this_class.addClass("correct");
				this_class.parent().parent().css("pointer-events", "none");
				play_correct_incorrect_sound(1);
				this_class.siblings(".correct-icon").show();
				navFlag?setTimeout(function(){$nextBtn.trigger("click")}, 1000):'';
			}else{
				this_class.addClass("incorrect");
				this_class.siblings(".incorrect-icon").show();
				play_correct_incorrect_sound(0);
			}
		}

		// function to fadeIn the value itertively
		function iterative_class_fadeIn(class_name, initial_count, max_count, time_gap, incrementor){
				$(class_name+"-"+initial_count).fadeIn(100);
				timeoutvar = setTimeout(function(){
					initial_count+=incrementor;
					if(initial_count<=max_count){
						iterative_class_fadeIn(class_name, initial_count, max_count, time_gap, incrementor)
					}else{
						nav_button_controls();
					}
				}, time_gap);
			}
		switch (countNext) {
			case 0:
					$(".textblock").append("<p class='submit_btn'> Click Here </p>");
					current_sound.stop();
					current_sound = sndArr[0];
					current_sound.play();
					$('.text-2').fadeIn(0);
					current_sound.bind('ended', function(){
						current_sound = sndArr[1];
						current_sound.play();
						$('.text-3').fadeIn(0);
						current_sound.bind('ended', function(){
							$(".submit_btn").show();
						});
					});
					var s = Snap("#clock_svg_container");
					var svg = Snap.load(imgpath + 'clock_1_15.svg', function(loadedFragment){
						s.append(loadedFragment);
						var count = 1;
						var circle_show = function (count){
								count>1?$("#circle"+(count-1)).css("display", "none"):'';
								$("#circle"+count).css("display", "block");
								count+=1;
								timeoutvar = setTimeout(function(){
									count<=12?circle_show(count):nav_button_controls(0);
								},500);
						}

						$(".submit_btn").click(function(){
							$(".submit_btn").css("pointer-events", "none");
							circle_show(1);
						});
					});
			break;
			case 1:
				$prevBtn.show(0);
				$(".clockblock").append("<p class='dot_line_3'> <p>");
				current_sound.stop();
				current_sound = sound_dg4;
				current_sound.play();
				$('.text-2').fadeIn(0);
				current_sound.bind('ended', function(){
					current_sound = sound_dg4_1;
					current_sound.play();
					$('.text-3').fadeIn(0);
					current_sound.bind('ended', function(){
						$('.hr-hand').addClass('hand-hl-0');
						nav_button_controls(2500);
					});
				});
				break;
			case 2:
				$prevBtn.show(0);
				$(".clockblock").append("<p class='dot_line_3'> <p>");
				sound_and_nav(sound_dg5);
				$('.text-2').fadeIn(1000);
				break;
			case 3:
				$prevBtn.show(0);
				$(".optn_blk_sec, .text-5").hide(0);
				current_sound.stop();
				current_sound = sound_dg6;
				current_sound.play();
				// current_sound.bind('ended', function(){
				// 	current_sound = sound_dg7;
				// 	current_sound.play();
				// });
				$(".grp_1, .grp_2").click(function(){
					if($(this).hasClass("class_1")){
						$(".optn_blk_sec, .text-5").show(0);
						$(this).hasClass("grp_1")?correct_Incor($(this), 1, 0):
						correct_Incor($(this), 1, 1);
					}else{
						correct_Incor($(this), 0, 0);
					}
				});
				break;
			case 4:
				$prevBtn.show(0);
				current_sound.stop();
				current_sound = sound_dg8;
				current_sound.play();
				$('.text-2').fadeIn(0);
				current_sound.bind('ended', function(){
					current_sound = sound_dg8_1;
					current_sound.play();
					$('.text-3').fadeIn(0);
					current_sound.bind('ended', function(){
						current_sound = sound_dg8_2;
						current_sound.play();
						$('.text-4').fadeIn(0);
						current_sound.bind('ended', function(){
							nav_button_controls(2500);
						});
					});
				});
				break;
			case 5:
				$prevBtn.show(0);
				$('[class*="past-"]').hide(0);
				current_sound.stop();
				current_sound = sound_dg9;
				current_sound.play();
				$('.text-2').fadeIn(0);
				current_sound.bind('ended', function(){
					$(".dragable").draggable({
						revert:true,
						cursor:"all-scroll"
					});

					$(".dropable").droppable({
						accept:".dragable",
						hoverClass: "drop-hover",
						drop: function(event, ui){
							drop_handler(event, ui, $(this));
						}
					});
				});


				break;
			case 6:
				sound_and_nav(sound_dg10);
			break;
			case 7:
			case 9:
				$prevBtn.show(0);
					countNext===7?sound_player(sound_dg11):sound_player(sound_dg13);
				$(".opn_txt").click(function(){
					if($(this).hasClass("class_1")){
						correct_Incor($(this), 1, 1);
					}else{
						correct_Incor($(this), 0, 0);
					}
				});
			break;
			case 8:
			case 10:
				countNext===8?
				 sound_and_nav(sound_dg12):
				 sound_and_nav(sound_dg14);
			break;
			case 11:
				$prevBtn.show(0);
				current_sound.stop();
				current_sound = sound_dg15;
				current_sound.play();
				$('.text-2').fadeIn(0);
				current_sound.bind('ended', function(){
					current_sound = sound_dg15_1;
					current_sound.play();
					$('.text-3').fadeIn(0);
					current_sound.bind('ended', function(){
						current_sound = sound_dg15_2;
						current_sound.play();
						$('.text-4').fadeIn(0);
						current_sound.bind('ended', function(){
							nav_button_controls(200);
						});
					});
				});
			break;
			case 12:
				$prevBtn.show(0);
				current_sound.stop();
				current_sound = sound_dg16;
				current_sound.play();
				$('.text-2').fadeIn(0);
				current_sound.bind('ended', function(){
					current_sound = sound_dg16_1;
					current_sound.play();
					$('.text-3').fadeIn(0);
					current_sound.bind('ended', function(){
						current_sound = sound_dg16_2;
						current_sound.play();
						$('.text-4').fadeIn(0);
						current_sound.bind('ended', function(){
							nav_button_controls(200);
						});
					});
				});
				break;
			default:
				$prevBtn.show(0);
				nav_button_controls(100);
				break;
		}

		function	drop_handler(event, ui, this_class){
							var drag_classes = ui.draggable.attr("class").split(" ");
							var drop_classes = this_class.attr("class").split(" ");
							var drag_id = drag_classes[1].split("-");
							var drop_id = drop_classes[1].split("-");
							if(drag_id[1]==drop_id[1]){
								cor_counter+=1;
								play_correct_incorrect_sound(1);
								this_class.removeClass("incorrect");
								this_class.addClass("correct");
								var cor_text = ui.draggable.text();
								this_class.children("p").text(cor_text);
								ui.draggable.detach();
								if(cor_counter==3){
									$(".past-5").delay(1000).fadeIn(100, function(){
										$(".past-10").delay(1000).fadeIn(100, function(){
											$(".past-20").delay(1000).fadeIn(100, function(){
												$(".past-25").delay(1000).fadeIn(100, function(){
													nav_button_controls();
												});
											});
										});
									});
								}
							}else{
								play_correct_incorrect_sound(0);
								this_class.addClass("incorrect");
							}
						}
	}

	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}

	function sound_and_nav(sound_data, clickfunction , a){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			if(typeof clickfunction != 'undefined'){
				clickfunction(a, sound_data);
			}
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		});
	}
	function click_dg(dg_class, audio){
		$(dg_class).click(function(){
			sound_player(audio);
		});
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				current_sound.stop();
				clearTimeout(timeoutvar);
				timeoutvar = null;
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		current_sound.stop();
		clearTimeout(timeoutvar);
		timeoutvar = null;
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

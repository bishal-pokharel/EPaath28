var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/TellTime/";

var s9_p1 = new buzz.sound((soundAsset + "s9_p1.ogg"));
var s9_p2 = new buzz.sound((soundAsset + "s9_p2.ogg"));
var s9_p3 = new buzz.sound((soundAsset + "s9_p3.ogg"));
var s9_p4 = new buzz.sound((soundAsset + "s9_p4.ogg"));
var s9_p5 = new buzz.sound((soundAsset + "s9_p5.ogg"));
var s9_p6 = new buzz.sound((soundAsset + "s9_p6.ogg"));
var s9_p7 = new buzz.sound((soundAsset + "s9_p7.ogg"));
var s9_p8 = new buzz.sound((soundAsset + "s9_p8.ogg"));
var s9_p9 = new buzz.sound((soundAsset + "s9_p9.ogg"));
var s9_p10 = new buzz.sound((soundAsset + "s9_p10.ogg"));
var s9_p11 = new buzz.sound((soundAsset + "s9_p11.ogg"));
var s9_p12 = new buzz.sound((soundAsset + "s9_p12.ogg"));
var s9_p13 = new buzz.sound((soundAsset + "s9_p13.ogg"));
var s9_p14= new buzz.sound((soundAsset + "s9_p14.ogg"));
var s9_p15 = new buzz.sound((soundAsset + "s9_p15.ogg"));
var s9_p16 = new buzz.sound((soundAsset + "s9_p16.ogg"));

var current_sound = s9_p1;


var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-new',

		extratextblock : [{
			textdata : data.string.p5text1,
			textclass : 'center-top-text fnt_Sans come-from-left my_font_super_big bold-text chelseamarket rt-1'
		},
		{
			textdata : data.string.p5text2,
			textclass : 'center-bottom-text fnt_Sans come-from-right my_font_super_big bold-text chelseamarket rt-2'
		}]
	},

	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		extratextblock : [{
			textdata : data.string.p5text3,
			textclass : 'bottom-text fnt_Sans bg-light-gray my_font_big proximanova bold-text fade_in_1 z-15'
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "full-bg dayfadein",
				imgsrc : imgpath + "dbg.png",
			},
			{
				imgclass : "full-bg nightfadeout",
				imgsrc : imgpath + "nbg.png",
			},
			{
				imgclass : "sun-moon sun sunanimation",
				imgsrc : imgpath + "sun.png",
			},
			{
				imgclass : "sun-moon moon moonanimation",
				imgsrc : imgpath + "fullmoon.png",
			}
			],
		}],
		clockblock : [{
			containscanvaselement: true,
			containscanvaselement2: true,
			clockblockclass: 'clock-left-2',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: '',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'hr-full-rotate',
			minclass: 'min-full-rotate',
			dotclass: ''
		}],

	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-room',

		extratextblock : [{
			textdata : data.string.p5text4,
			textclass : 'bottom-text fnt_Sans my_font_very_big proximanova bold-text fade_in_1 z-15'
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "window-p5 frame-1",
				imgsrc : imgpath + "window_frame.png",
			},
			{
				imgclass : "window-p5 frame-2",
				imgsrc : imgpath + "window_frame.png",
			},
			{
				imgclass : "behind-window-p5 day-bg",
				imgsrc : imgpath + "daybg.png",
			},
			{
				imgclass : "behind-window-p5 night-bg",
				imgsrc : imgpath + "nightbg.png",
			},
			{
				imgclass : "behind-window-sun sun-1",
				imgsrc : imgpath + "sun.png",
			},
			{
				imgclass : "behind-window-sun moon-1",
				imgsrc : imgpath + "fullmoon.png",
			}
			],
		}],
		clockblock : [{
			clockblockclass: 'clock-center-1',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: '',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'hr-9',
			minclass: 'hr-12',
			dotclass: ''
		}],
	},

	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-room',

		extratextblock : [{
			textdata : data.string.p5text5,
			textclass : 'bottom-text my_font_very_big proximanova bold-text fade_in_1 z-15',
			datahighlightflag: true,
			datahighlightcustomclass: 'text-purple'
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "window-p5",
				imgsrc : imgpath + "window_frame.png",
			},
			{
				imgclass : "behind-window-p5",
				imgsrc : imgpath + "nightbg.png",
			},
			{
				imgclass : "behind-window-sun moon",
				imgsrc : imgpath + "fullmoon.png",
			}
			],
		}],
		clockblock : [{
			clockblockclass: 'clock-left-1',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: '',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'hr-12',
			minclass: 'hr-12',
			dotclass: ''
		}],
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-room',

		extratextblock : [{
			textdata : data.string.p5text6,
			textclass : 'bottom-text my_font_very_big proximanova bold-text fade_in_1 z-15',
			datahighlightflag: true,
			datahighlightcustomclass: 'text-blue'
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "window-p5",
				imgsrc : imgpath + "window_frame.png",
			},
			{
				imgclass : "behind-window-p5",
				imgsrc : imgpath + "daybg.png",
			},
			{
				imgclass : "behind-window-sun sun",
				imgsrc : imgpath + "sun.png",
			},
			],
		}],
		clockblock : [{
			clockblockclass: 'clock-left-1',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: '',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'hr-12',
			minclass: 'hr-12',
			dotclass: ''
		}],
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-new',

		extratextblock : [{
			textdata : data.string.p5text7,
			textclass : 'center-top-text fnt_Sans come-from-left my_font_ultra_big bold-text chelseamarket rt-3'
		},
		{
			textdata : data.string.p5text8,
			textclass : 'center-bottom-text fnt_Sans come-from-right my_font_ultra_big bold-text chelseamarket rt-3'
		}]
	},

	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-new-2',

		extratextblock : [{
			textdata : data.string.p5text9,
			textclass : 'bottom-text my_font_very_big proximanova bold-text fade_in_1 z-15',
			datahighlightflag: true,
			datahighlightcustomclass: 'text-purple'
		},{
			textdata : data.string.am,
			textclass : 'am-label my_font_big z-15 label_highlight',
		},{
			textdata : data.string.pm,
			textclass : 'pm-label my_font_big z-15',
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "hr_block blk_1",
				imgsrc : imgpath + "24-hours_new.png",
			},{
				imgclass : "hr_block blk_2",
				imgsrc : imgpath + "24-hours_new01.png",
			},{
				imgclass : "hr_block blk_3",
				imgsrc : imgpath + "24-hours_new02.png",
			}
			],
		}],
	},

	//slide7
	{

		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-new-2',

		extratextblock : [{
			textdata : data.string.p5text10,
			textclass : 'bottom-text my_font_very_big proximanova bold-text fade_in_1 z-15',
			datahighlightflag: true,
			datahighlightcustomclass: 'text-purple'
		},{
			textdata : data.string.am,
			textclass : 'am-label my_font_big z-15',
		},{
			textdata : data.string.pm,
			textclass : 'pm-label my_font_big z-15 label_highlight',
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "hr_block blk_1",
				imgsrc : imgpath + "24-hours_new.png",
			},{
				imgclass : "hr_block blk_2",
				imgsrc : imgpath + "24-hours_new01.png",
			},{
				imgclass : "hr_block blk_3",
				imgsrc : imgpath + "24-hours_new02.png",
			}
			],
		}],
	},

	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		extratextblock : [{
			textdata : data.string.p5text11,
			textclass : 'template-dialougebox2-top-yellow dg-r1 patrickhand my_font_big'
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "boy boy-1",
					imgsrc : imgpath + "rahul.png",
				}
			],
		}],

		clockblock : [{
			clockblockclass: 'clock-right',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: 'no-brightness',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: '',
			minclass: 'hr-12',
			dotclass: ''
		}]
	},

	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		uppertextblockadditionalclass: 'bt-textblock',
		uppertextblock : [{
			textdata : data.string.am,
			textclass : 'my_font_very_big proximanova bt-title am-text'
		},
		{
			textdata : data.string.p5text12,
			textclass : 'my_font_big proximanova bt-text'
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "full-bg",
				imgsrc : imgpath + "dbg.png",
			},
			{
				imgclass : "full-bg",
				imgsrc : imgpath + "nbg.png",
			},
			{
				imgclass : "sun-moon moon-2",
				imgsrc : imgpath + "fullmoon.png",
			}
			],
		}],
		clockblock : [{
			clockblockclass: 'clock-left-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: '',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'hr-12',
			minclass: 'hr-12',
			dotclass: ''
		}],
	},
	//slide10
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		uppertextblockadditionalclass: 'bt-textblock',
		uppertextblock : [{
			textdata : data.string.am,
			textclass : 'my_font_very_big proximanova bt-title am-text'
		},
		{
			textdata : data.string.p5text13,
			textclass : 'my_font_big proximanova bt-text fade_in_6'
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "full-bg dayanim-1",
				imgsrc : imgpath + "dbg.png",
			},
			{
				imgclass : "full-bg nightanim-1",
				imgsrc : imgpath + "nbg.png",
			},
			{
				imgclass : "sun-moon sun sunanim-1",
				imgsrc : imgpath + "sun.png",
			},
			{
				imgclass : "sun-moon moon moonanim-1",
				imgsrc : imgpath + "fullmoon.png",
			}
			],
		}],
		clockblock : [{
			clockblockclass: 'clock-left-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: '',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'hr-anim-1',
			minclass: 'min-anim-1',
			dotclass: ''
		}],
	},
	//slide11
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		uppertextblockadditionalclass: 'bt-textblock',
		uppertextblock : [{
			textdata : data.string.am,
			textclass : 'my_font_very_big proximanova bt-title am-text'
		},
		{
			textdata : data.string.p5text14,
			textclass : 'my_font_big proximanova bt-text fade_in_4'
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "full-bg dayanim-2",
				imgsrc : imgpath + "dbg.png",
			},
			{
				imgclass : "sun-moon sun sunanim-2",
				imgsrc : imgpath + "sun.png",
			},
			],
		}],
		clockblock : [{
			clockblockclass: 'clock-left-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: '',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'hr-anim-2',
			minclass: 'min-anim-2',
			dotclass: ''
		}],
	},

	//slide12
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		uppertextblockadditionalclass: 'bt-textblock',
		uppertextblock : [{
			textdata : data.string.am,
			textclass : 'my_font_very_big proximanova bt-title am-text-to-pm'
		},
		{
			textdata : data.string.p5text15,
			textclass : 'my_font_big proximanova bt-text fade_in-2'
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "full-bg dayanim-3",
				imgsrc : imgpath + "dbg.png",
			},
			{
				imgclass : "sun-moon sun sunanim-3",
				imgsrc : imgpath + "sun.png",
			},
			],
		}],
		clockblock : [{
			clockblockclass: 'clock-left-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: '',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'hr-anim-3',
			minclass: 'min-anim-3',
			dotclass: ''
		}],
	},
	//slide13
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		uppertextblockadditionalclass: 'bt-textblock',
		uppertextblock : [{
			textdata : data.string.pm,
			textclass : 'my_font_very_big proximanova bt-title pm-text'
		},
		{
			textdata : data.string.p5text16,
			textclass : 'my_font_big proximanova bt-text fade_in_4'
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "full-bg dayanim-4",
				imgsrc : imgpath + "dbg.png",
			},
			{
				imgclass : "sun-moon sun sunanim-4",
				imgsrc : imgpath + "sun.png",
			},
			],
		}],
		clockblock : [{
			clockblockclass: 'clock-left-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: '',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'hr-anim-4',
			minclass: 'min-anim-4',
			dotclass: ''
		}],
	},

	//slide14
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		uppertextblockadditionalclass: 'bt-textblock',
		uppertextblock : [{
			textdata : data.string.pm,
			textclass : 'my_font_very_big proximanova bt-title pm-text'
		},
		{
			textdata : data.string.p5text17,
			textclass : 'my_font_big proximanova bt-text fade_in-3'
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "full-bg dayanim-5",
				imgsrc : imgpath + "dbg.png",
			},
			{
				imgclass : "full-bg nightanim-5",
				imgsrc : imgpath + "nbg.png",
			},
			{
				imgclass : "sun-moon sun sunanim-5",
				imgsrc : imgpath + "sun.png",
			},
			{
				imgclass : "sun-moon moon moonanim-5",
				imgsrc : imgpath + "fullmoon.png",
			}
			],
		}],
		clockblock : [{
			clockblockclass: 'clock-left-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: '',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'hr-anim-5',
			minclass: 'min-anim-5',
			dotclass: ''
		}],
	},

	//slide15
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		uppertextblockadditionalclass: 'bt-textblock',
		uppertextblock : [{
			textdata : data.string.pm,
			textclass : 'my_font_very_big proximanova bt-title pm-text'
		},
		{
			textdata : data.string.p5text18,
			textclass : 'my_font_big proximanova bt-text fade_in-2'
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "full-bg nightanim-6",
				imgsrc : imgpath + "nbg.png",
			},
			{
				imgclass : "sun-moon moon moonanim-6",
				imgsrc : imgpath + "fullmoon.png",
			}
			],
		}],
		clockblock : [{
			clockblockclass: 'clock-left-3',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: '',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'hr-anim-6',
			minclass: 'min-anim-6',
			dotclass: ''
		}],
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;


var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();


	var timeoutvar = null;

	var canvas;
	var ctx;

	var canvas2;
	var ctx2;

	var g_color;
	var g_ctx;

	var g_color2;
	var g_ctx2;

	var g_angle1 = -1/2*Math.PI;
	var g_angle2;

	var center_clock;
	var dx = 0;
	var radius = $board.width()*.4;
	var color1 = '#FFA6CD';
	var color2 = '#FF883D';
	var global_animation;
	// var vocabcontroller =  new Vocabulary();
	// vocabcontroller.init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// vocabcontroller.findwords(countNext);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
			case 0:
				sound_and_nav(s9_p1);
				break;
			case 1:
				$prevBtn.show(0);
					sound_player(s9_p2);
				nav_button_controls(24100);
				canvas = document.getElementById("clock-canvas");
				ctx = canvas.getContext("2d");


				canvas2 = document.getElementById("clock-canvas2");
				ctx2 = canvas2.getContext("2d");

				dx = 0;
				g_angle1 = -1/2*Math.PI;
				g_angle2 = 3/2*Math.PI;
				g_color = color1;
				g_ctx = ctx;
				init_canvas();
				global_animation = requestAnimationFrame(repeatOften);

				timeoutvar = setTimeout(function(){
					dx = 0;
					g_angle1 = -1/2*Math.PI;
					g_angle2 = 3/2*Math.PI;
					g_color = color2;
					g_ctx = ctx2;
					init_canvas2();
					global_animation = requestAnimationFrame(repeatOften);
				}, 24000);
				break;
			case 2:
				sound_and_nav(s9_p3);
			break;
			case 3:
				sound_and_nav(s9_p4);
			break;
			case 4:
				sound_and_nav(s9_p5);
			break;
			case 5:
				$prevBtn.show(0);
					sound_and_nav(s9_p6);
				break;
			case 6:
				$(".pm-label, .blk_3").hide(0);
					sound_and_nav(s9_p7);
			break;
			case 7:
				sound_and_nav(s9_p8);
			break;
			case 8:
				$prevBtn.show(0);
				sound_and_nav(s9_p9, click_dg, '.dg-1');
				break;
			case 9:
				sound_and_nav(s9_p10, click_dg, '.dg-1');
			break;
			case 10:
				$prevBtn.show(0);
				timeoutvar = setTimeout(function(){
					sound_and_nav(s9_p11, click_dg, '.dg-1')
				}, 7000);
				break;
			case 11:
			case 13:
				$prevBtn.show(0);
				timeoutvar = setTimeout(function(){
					if(countNext == 11){
						$nextBtn.show(0);
						sound_and_nav(s9_p12, click_dg, '.dg-1');
					}else{
						sound_and_nav(s9_p14, click_dg, '.dg-1');
					}
				}, 4000);
				// nav_button_controls(4000);
				break;
			case 12:
				$prevBtn.show(0);
				timeoutvar = setTimeout(function(){
					$('.am-text-to-pm').html(data.string.pm);
						sound_and_nav(s9_p13, click_dg, '.dg-1');
				}, 2000);
				// nav_button_controls(2000);
				break;
			case 14:
				$prevBtn.show(0);
				timeoutvar = setTimeout(function(){
					sound_and_nav(s9_p15, click_dg, '.dg-1')
				}, 3000);
				break;
			case 15:
				$prevBtn.show(0);
				timeoutvar = setTimeout(function(){
					sound_and_nav(s9_p16, click_dg, '.dg-1')
				}, 2000);
				break;
			default:
				$prevBtn.show(0);
				nav_button_controls(100);
				break;
		}
	}

	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function init_canvas(){
		canvasheight = $('.clockblock').height();
		canvaswidth = $('.clockblock').width();

		canvas.height = canvasheight;
		canvas.width = canvaswidth;

		center_clock = [canvaswidth/2, canvasheight/2];
		radius = canvaswidth/5;

		ctx.clearRect(0, 0, canvaswidth, canvasheight);
	}
	function init_canvas2(){
		canvasheight = $('.clockblock').height();
		canvaswidth = $('.clockblock').width();

		canvas2.height = canvasheight;
		canvas2.width = canvaswidth;

		center_clock = [canvaswidth/2, canvasheight/2];
		radius = canvaswidth/7;

		ctx2.clearRect(0, 0, canvaswidth, canvasheight);
	}
	function draw_sector(angle1, angle2, contexta, color){
		contexta.clearRect(0, 0, canvaswidth, canvasheight);
		contexta.moveTo(0,0);
		contexta.beginPath();
    	contexta.fillStyle = color;
		contexta.moveTo(center_clock[0], center_clock[1]);
		contexta.lineTo(center_clock[0] + radius*Math.cos(angle1), center_clock[1] + radius*Math.sin(angle1));
		contexta.arc(center_clock[0], center_clock[1], radius, angle1, angle2);
		contexta.lineTo(center_clock[0], center_clock[1]);
		contexta.fill();
	}


	var fps = 8;
	var time_init = 0;
	var time_elapsed = 0;
	var time_then = 0;
	time_then = Date.now();
	function repeatOften() {
		global_animation = requestAnimationFrame(repeatOften);
		time_init = Date.now();
		time_elapsed = time_init - time_then;
		if (time_elapsed > 1000/fps)
		{
			time_then = time_init - (time_elapsed % (1000/fps));
			draw_sector(g_angle1, g_angle1+dx, g_ctx, g_color);
			if(dx < g_angle2-g_angle1){
				dx += 0.033;
				if(dx > g_angle2-g_angle1){
					draw_sector(g_angle1, g_angle2, g_ctx, g_color);
					cancelAnimationFrame(global_animation);
				}
			} else {
				draw_sector(g_angle1, g_angle2, g_ctx, g_color);
				cancelAnimationFrame(global_animation);
			}
		}
	}

	function animate_sector(angle1, angle2, context, color){
		if(dx < angle2-angle1){
			dx += 0.1;
		} else {
			dx = angle2 - angle1;
		}
		draw_sector(angle1, angle1+dx, context, color);
		globalID = requestAnimationFrame(animate_sector);
	}


	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}
	function sound_and_nav(sound_data, clickfunction , a){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			if(typeof clickfunction != 'undefined'){
				clickfunction(a, sound_data);
			}
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		});
	}
	function click_dg(dg_class, audio){
		$(dg_class).click(function(){
			sound_player(audio);
		});
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		current_sound.stop();
		switch(countNext){
			default:
				clearTimeout(timeoutvar);
				timeoutvar = null;
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		clearTimeout(timeoutvar);
		timeoutvar = null;
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();

	$(window).resize(function() {
		init_canvas();
		init_canvas2();
	});
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

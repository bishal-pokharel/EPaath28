var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/TellTime/";

var s10_p1 = new buzz.sound((soundAsset + "s10_p1.ogg"));
var s10_p1_2 = new buzz.sound((soundAsset + "s10_p1(2).ogg"));
var s10_p2 = new buzz.sound((soundAsset + "s10_p2.ogg"));
var s10_p3 = new buzz.sound((soundAsset + "s10_p3.ogg"));
var s10_p4 = new buzz.sound((soundAsset + "s10_p4.ogg"));
var s10_p5 = new buzz.sound((soundAsset + "s10_p5.ogg"));
var s10_p6 = new buzz.sound((soundAsset + "s10_p6.ogg"));
var s10_p7 = new buzz.sound((soundAsset + "s10_p7.ogg"));
var s10_p9 = new buzz.sound((soundAsset + "s10_p9.ogg"));
var current_sound = s10_p1;
var soundsArray = [s10_p2, s10_p3, s10_p4, s10_p5, s10_p6, s10_p7, s10_p9];
var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		uppertextblockadditionalclass: 'header-text-block-1',
		uppertextblock : [{
			textdata : data.string.p6text1,
			textclass : ''
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "fairy",
					imgsrc : imgpath + "fairy.png",
				},
			],
		}],
		extratextblock: [
		{
			textdata : data.string.am,
			textclass : 'option am-option my_font_very_big ',
		},
		{
			textdata : data.string.pm,
			textclass : 'option pm-option my_font_very_big ',
		},
		{
			textdata : data.string.p6text3,
			textclass : 'what-ques-1 my_font_big ',
		},
		{
			textdata : data.string.p6text4,
			textclass : 'bt-ans my_font_very_big ',
			datahighlightflag: true,
			datahighlightcustomclass: 'what-ans-2'
		},
		],

		clockblock : [{
			containscanvaselement: true,
			containscanvaselement2: true,
			clockblockclass: 'clock-left-5',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: '',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'can-drag',
			minclass: 'can-drag hr-12',
			dotclass: ''
		}],
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		uppertextblockadditionalclass: 'header-text-block-1',
		uppertextblock : [{
			textdata : data.string.p6text1,
			textclass : ''
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "fairy",
					imgsrc : imgpath + "fairy.png",
				},
			],
		}],
		extratextblock: [
		{
			textdata : data.string.am,
			textclass : 'option am-option my_font_very_big ',
		},
		{
			textdata : data.string.pm,
			textclass : 'option pm-option my_font_very_big ',
		},
		{
			textdata : data.string.p6text5,
			textclass : 'what-ques-1 my_font_big ',
		},
		{
			textdata : data.string.p6text6,
			textclass : 'bt-ans my_font_very_big ',
			datahighlightflag: true,
			datahighlightcustomclass: 'what-ans-2'
		},
		],

		clockblock : [{
			containscanvaselement: true,
			containscanvaselement2: true,
			clockblockclass: 'clock-left-5',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: '',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'can-drag',
			minclass: 'can-drag hr-12',
			dotclass: ''
		}],
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		uppertextblockadditionalclass: 'header-text-block-1',
		uppertextblock : [{
			textdata : data.string.p6text1,
			textclass : ''
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "fairy",
					imgsrc : imgpath + "fairy.png",
				},
			],
		}],
		extratextblock: [
		{
			textdata : data.string.am,
			textclass : 'option am-option my_font_very_big ',
		},
		{
			textdata : data.string.pm,
			textclass : 'option pm-option my_font_very_big ',
		},
		{
			textdata : data.string.p6text7,
			textclass : 'what-ques-1 my_font_big ',
		},
		{
			textdata : data.string.p6text8,
			textclass : 'bt-ans my_font_very_big ',
			datahighlightflag: true,
			datahighlightcustomclass: 'what-ans-2'
		},
		],

		clockblock : [{
			containscanvaselement: true,
			containscanvaselement2: true,
			clockblockclass: 'clock-left-5',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: '',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'can-drag',
			minclass: 'can-drag hr-12',
			dotclass: ''
		}],
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		uppertextblockadditionalclass: 'header-text-block-1',
		uppertextblock : [{
			textdata : data.string.p6text1,
			textclass : ''
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "fairy",
					imgsrc : imgpath + "fairy.png",
				},
			],
		}],
		extratextblock: [
		{
			textdata : data.string.am,
			textclass : 'option am-option my_font_very_big ',
		},
		{
			textdata : data.string.pm,
			textclass : 'option pm-option my_font_very_big ',
		},
		{
			textdata : data.string.p6text9,
			textclass : 'what-ques-1 my_font_big ',
		},
		{
			textdata : data.string.p6text10,
			textclass : 'bt-ans my_font_very_big ',
			datahighlightflag: true,
			datahighlightcustomclass: 'what-ans-2'
		},
		],

		clockblock : [{
			containscanvaselement: true,
			containscanvaselement2: true,
			clockblockclass: 'clock-left-5',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: '',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'can-drag',
			minclass: 'can-drag hr-12',
			dotclass: ''
		}],
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		uppertextblockadditionalclass: 'header-text-block-1',
		uppertextblock : [{
			textdata : data.string.p6text1,
			textclass : ''
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "fairy",
					imgsrc : imgpath + "fairy.png",
				},
			],
		}],
		extratextblock: [
		{
			textdata : data.string.am,
			textclass : 'option am-option my_font_very_big ',
		},
		{
			textdata : data.string.pm,
			textclass : 'option pm-option my_font_very_big ',
		},
		{
			textdata : data.string.p6text11,
			textclass : 'what-ques-1 my_font_big ',
		},
		{
			textdata : data.string.p6text12,
			textclass : 'bt-ans my_font_very_big ',
			datahighlightflag: true,
			datahighlightcustomclass: 'what-ans-2'
		},
		],

		clockblock : [{
			containscanvaselement: true,
			containscanvaselement2: true,
			clockblockclass: 'clock-left-5',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: '',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'can-drag',
			minclass: 'can-drag hr-12',
			dotclass: ''
		}],
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		uppertextblockadditionalclass: 'header-text-block-1',
		uppertextblock : [{
			textdata : data.string.p6text1,
			textclass : ''
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "fairy",
					imgsrc : imgpath + "fairy.png",
				},
			],
		}],
		extratextblock: [
		{
			textdata : data.string.am,
			textclass : 'option am-option my_font_very_big ',
		},
		{
			textdata : data.string.pm,
			textclass : 'option pm-option my_font_very_big ',
		},
		{
			textdata : data.string.p6text13,
			textclass : 'what-ques-1 my_font_big ',
		},
		{
			textdata : data.string.p6text14,
			textclass : 'bt-ans my_font_very_big ',
			datahighlightflag: true,
			datahighlightcustomclass: 'what-ans-2'
		},
		],

		clockblock : [{
			containscanvaselement: true,
			containscanvaselement2: true,
			clockblockclass: 'clock-left-5',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: '',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'can-drag',
			minclass: 'can-drag hr-12',
			dotclass: ''
		}],
	},
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		uppertextblockadditionalclass: 'header-text-block-1',
		uppertextblock : [{
			textdata : data.string.p6text1,
			textclass : ''
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "fairy",
					imgsrc : imgpath + "fairy.png",
				},
			],
		}],
		extratextblock: [
		{
			textdata : data.string.am,
			textclass : 'option am-option my_font_very_big ',
		},
		{
			textdata : data.string.pm,
			textclass : 'option pm-option my_font_very_big ',
		},
		{
			textdata : data.string.p6text15,
			textclass : 'what-ques-1 my_font_big ',
		},
		{
			textdata : data.string.p6text16,
			textclass : 'bt-ans my_font_very_big ',
			datahighlightflag: true,
			datahighlightcustomclass: 'what-ans-2'
		},
		],

		clockblock : [{
			containscanvaselement: true,
			containscanvaselement2: true,
			clockblockclass: 'clock-left-5',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: '',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'can-drag',
			minclass: 'can-drag hr-12',
			dotclass: ''
		}],
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		hasinput: true,
		inputclass: ' my_font_big',

		extratextblock: [
		{
			textdata : data.string.p6text17,
			textclass : 'fnt_Sans your-name'
		},
		{
			textdata : data.string.submit,
			textclass : 'submit'
		}
		],
	},
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		extratextblock: [
		{
			textdata : data.string.p6text19,
			textclass : 'fnt_Sans fnt_Sans your-name'
		},
		{
			textdata : data.string.p6text20,
			textclass : 'option-2 b-option my_font_very_big ',
		},
		{
			textdata : data.string.p6text21,
			textclass : 'option-2 fnt_Sans g-option my_font_very_big ',
		},
		],
	},
	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		uppertextblockadditionalclass: 'p7-summary my_font_big ',
		uppertextblock : [{
			textdata : data.string.p6text23,
			textclass : 'fade_in_1'
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "bubble",
					imgsrc : imgpath + "bubble.png",
				},
			],
		}],
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;


var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();


	// var vocabcontroller =  new Vocabulary();
	// vocabcontroller.init();

	var angle_hr = 180;
	var angle_min = 0;
	var hr_time = 3;
	var min_time = 0;
	var change_time_str;
	var amp_pm_str;
	var answer_arr = ['1','2','3','4','5','6','7'];
	var option_selected = [false, false];
	var uname = "Ram";
	var usex = 0;


	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// vocabcontroller.findwords(countNext);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
			case 0:
				angle_hr = 180;
				angle_min = 0;
				hr_time = 3;
				min_time = 0;
				option_selected = [false,false];
				ampm_selector();
				current_sound.stop();
				current_sound = s10_p1;
				current_sound.play();
				current_sound.bind('ended', function(){
					current_sound = s10_p1_2;
					current_sound.play();
				});
				$('.min-hand').on('mousedown', function(){
					$(".board").on('mousemove', function(event){
						option_selected[0]=true;
						rotateAnnotationCropper(event.pageX, event.pageY, $('.min-hand'), 1);
					});
				});
				$('.hr-hand').on('mousedown', function(){
					$(".board").on('mousemove', function(event){
						option_selected[0]=true;
						rotateAnnotationCropper(event.pageX, event.pageY, $('.hr-hand'), 0);
					});
				});
				break;
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
				sound_player(soundsArray[countNext-1]);
				$prevBtn.show(0);
				angle_hr = 180;
				angle_min = 0;
				hr_time = 3;
				min_time = 0;
				option_selected = [false,false];
				ampm_selector();
				$('.min-hand').on('mousedown', function(){
					$(".board").on('mousemove', function(event){
						option_selected[0]=true;
						rotateAnnotationCropper(event.pageX, event.pageY, $('.min-hand'), 1);
					});
				});
				$('.hr-hand').on('mousedown', function(){
					$(".board").on('mousemove', function(event){
						option_selected[0]=true;
						rotateAnnotationCropper(event.pageX, event.pageY, $('.hr-hand'), 0);
					});
				});
				break;
			case 7:
				$prevBtn.show(0);
					$(".submit").click(function(){
						uname = $('.name-input').val();
						if(uname.length > 0){
							$(".submit").addClass("sub_inactive");
							$(".name-input").css("pointer-events", "none");
							nav_button_controls(0);
						} else {
							$nextBtn.hide(0);
						}
					});
				$('.name-input').focusout(function(){
					uname = $(this).val();
					if(uname.length > 0){
						nav_button_controls(0);
					} else {
						$nextBtn.hide(0);
					}
				});
				break;
			case 8:
				$prevBtn.show(0);
				sound_player(s10_p9);
				$('.option-2').click(function(){
					$('.option-2').removeClass('selected-option');
					$(this).addClass('selected-option');
					if($(this).hasClass('b-option')){
						usex = 0;
					} else {
						usex = 1;
					}
					nav_button_controls(0);
				});
				break;
			case 9:
				$prevBtn.show(0);
				nav_button_controls(3000);
				if(usex==1){
					$('.p7-summary>p').html($('.p7-summary>p').html().replace(/He/g, 'She'));
				}
				$('.uname').html(uname);
				for( var i=1; i<8; i++){
					$('.fill-'+i).html(answer_arr[i-1]);
				}
				break;
			default:
				nav_button_controls(100);
				break;
		}
	}

	function rotateAnnotationCropper(xCoordinate, yCoordinate, cropper, is_min){
		//alert(offsetSelector.left);

		var x = xCoordinate - $('.center-dot').offset().left - $('.center-dot').width()/2;
		var y = (yCoordinate) - $('.center-dot').offset().top - $('.center-dot').height()/2;
		var theta = Math.atan2(y,x)*(180/Math.PI);
		if(is_min==0){
			angle_hr = (Math.floor(theta) + 180)%360;
			if(angle_hr>90){
				hr_time =  Math.floor((angle_hr-90)/30);
				if(hr_time==0){
					hr_time = 12;
				}
			} else{
				hr_time =  Math.floor(9 + angle_hr/30);
			}
		}
		else{
			angle_min = (Math.floor(theta) + 180)%360;
			if(angle_min>90){
				min_time = Math.floor((angle_min-90)/6);
			} else{
				min_time = (45 + Math.floor(angle_min/6))%60;
			}
		}
		var rotate = 'rotate(' + theta + 'deg)';
		change_time_str = ("0" + hr_time).slice(-2) + ':' + ("0" + min_time).slice(-2);
		$('.what-ans-1').html(change_time_str);
		$('.what-ans-2').html(change_time_str);
		cropper.css({'-moz-transform': rotate, 'transform' : rotate, '-webkit-transform': rotate, '-ms-transform': rotate});
		$('body').on('mouseup', function(event){
			$(".board").unbind('mousemove');
			var angle_of_hr = Math.floor((angle_hr-180)/30)*30+min_time/2;
			var rotate2 = 'rotate('+angle_of_hr+'deg)';
			$('.hr-hand').css({'-moz-transform': rotate2, 'transform' : rotate2, '-webkit-transform': rotate2, '-ms-transform': rotate2});
			if(option_selected[0]&&option_selected[1]){
				$nextBtn.show(0);
				option_selected[0]=false;
			}
		});
	}

	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}
	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function ampm_selector(){
		$('.option').click(function(){
			option_selected[1] = true;
			$('.option').removeClass('selected-option');
			$(this).addClass('selected-option');
			amp_pm_str = $(this).html();
			$('.am-pm-select').html(amp_pm_str);
			if(option_selected[0]&&option_selected[1]){
				$nextBtn.show(0);
			}
		});
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		current_sound.stop();
		switch(countNext){
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
				option_selected = [false,false];
				answer_arr[countNext] = change_time_str + ' '+amp_pm_str;
				countNext++;
				templateCaller();
				break;
			case 7:
				// uname = $('.name-input').val();
				// // $(".submit").click(function(){
				// // });
					countNext++;
					templateCaller();
				break;
			default:
				answer_arr[countNext] = change_time_str + ' '+amp_pm_str;
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

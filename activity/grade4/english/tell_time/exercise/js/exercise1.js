var imgpath = $ref+"/images/";

var content=[
	// slide1
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "bg-inside",
			textdata: ''
		},{
			textclass: "title",
			textdata: data.string.eins
		},{
			textclass: "question",
			textdata: data.string.eq_1
		},{
			textclass: "am_pm",
			textdata: data.string.eam
		},{
			textclass: "answer",
			textdata: data.string.ea_1,
			datahighlightflag: true,
			datahighlightcustomclass: 'blank-space'
		}],
		clockblock : [{
			clockblockclass: 'clock-ex',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-7-45',
			minclass: 'hr-9',
			dotclass: ''
		}],
	},
	// slide2
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "bg-inside",
			textdata: ''
		},{
			textclass: "title",
			textdata: data.string.eins
		},{
			textclass: "question",
			textdata: data.string.eq_2
		},{
			textclass: "am_pm",
			textdata: data.string.epm
		},{
			textclass: "answer",
			textdata: data.string.ea_2,
			datahighlightflag: true,
			datahighlightcustomclass: 'blank-space'
		}],
		clockblock : [{
			clockblockclass: 'clock-ex',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-3-05',
			minclass: 'hr-1',
			dotclass: ''
		}],
	},
	// slide3
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "bg-inside",
			textdata: ''
		},{
			textclass: "title",
			textdata: data.string.eins
		},{
			textclass: "question",
			textdata: data.string.eq_3
		},{
			textclass: "am_pm",
			textdata: data.string.epm
		},{
			textclass: "answer",
			textdata: data.string.ea_3,
			datahighlightflag: true,
			datahighlightcustomclass: 'blank-space'
		}],
		clockblock : [{
			clockblockclass: 'clock-ex',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-4-30',
			minclass: 'hr-6',
			dotclass: ''
		}],
	},
	// slide4
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "bg-inside",
			textdata: ''
		},{
			textclass: "title",
			textdata: data.string.eins
		},{
			textclass: "question",
			textdata: data.string.eq_4
		},{
			textclass: "am_pm",
			textdata: data.string.eam
		},{
			textclass: "answer",
			textdata: data.string.ea_4,
			datahighlightflag: true,
			datahighlightcustomclass: 'blank-space'
		}],
		clockblock : [{
			clockblockclass: 'clock-ex',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-6-10',
			minclass: 'hr-2',
			dotclass: ''
		}],
	},
	// slide5
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "bg-inside",
			textdata: ''
		},{
			textclass: "title",
			textdata: data.string.eins
		},{
			textclass: "question",
			textdata: data.string.eq_5
		},{
			textclass: "am_pm",
			textdata: data.string.epm
		},{
			textclass: "answer",
			textdata: data.string.ea_5,
			datahighlightflag: true,
			datahighlightcustomclass: 'blank-space'
		}],
		clockblock : [{
			clockblockclass: 'clock-ex',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-5-40',
			minclass: 'hr-8',
			dotclass: ''
		}],
	},
	// slide6
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "bg-inside",
			textdata: ''
		},{
			textclass: "title",
			textdata: data.string.eins
		},{
			textclass: "question",
			textdata: data.string.eq_6
		},{
			textclass: "am_pm",
			textdata: data.string.eam
		},{
			textclass: "answer",
			textdata: data.string.ea_6,
			datahighlightflag: true,
			datahighlightcustomclass: 'blank-space'
		}],
		clockblock : [{
			clockblockclass: 'clock-ex',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-10-15',
			minclass: 'hr-3',
			dotclass: ''
		}],
	},
	// slide7
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "bg-inside",
			textdata: ''
		},{
			textclass: "title",
			textdata: data.string.eins
		},{
			textclass: "question",
			textdata: data.string.eq_7
		},{
			textclass: "am_pm",
			textdata: data.string.epm
		},{
			textclass: "answer",
			textdata: data.string.ea_7,
			datahighlightflag: true,
			datahighlightcustomclass: 'blank-space'
		}],
		clockblock : [{
			clockblockclass: 'clock-ex',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-5-55',
			minclass: 'hr-11',
			dotclass: ''
		}],
	},
	// slide8
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "bg-inside",
			textdata: ''
		},{
			textclass: "title",
			textdata: data.string.eins
		},{
			textclass: "question",
			textdata: data.string.eq_8
		},{
			textclass: "am_pm",
			textdata: data.string.epm
		},{
			textclass: "answer",
			textdata: data.string.ea_8,
			datahighlightflag: true,
			datahighlightcustomclass: 'blank-space'
		}],
		clockblock : [{
			clockblockclass: 'clock-ex',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-9-20',
			minclass: 'hr-4',
			dotclass: ''
		}],
	},
	// slide9
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "bg-inside",
			textdata: ''
		},{
			textclass: "title",
			textdata: data.string.eins
		},{
			textclass: "question",
			textdata: data.string.eq_9
		},{
			textclass: "am_pm",
			textdata: data.string.eam
		},{
			textclass: "answer",
			textdata: data.string.ea_9,
			datahighlightflag: true,
			datahighlightcustomclass: 'blank-space'
		}],
		clockblock : [{
			clockblockclass: 'clock-ex',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-8-50',
			minclass: 'hr-10',
			dotclass: ''
		}],
	},
	// slide10
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "bg-inside",
			textdata: ''
		},{
			textclass: "title",
			textdata: data.string.eins
		},{
			textclass: "question",
			textdata: data.string.eq_10
		},{
			textclass: "am_pm",
			textdata: data.string.epm
		},{
			textclass: "answer",
			textdata: data.string.ea_10,
			datahighlightflag: true,
			datahighlightcustomclass: 'blank-space'
		}],
		clockblock : [{
			clockblockclass: 'clock-ex',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-5-35',
			minclass: 'hr-7',
			dotclass: ''
		}],
	}
];
content.shufflearray();

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;


	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

  /*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
      typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

    if (countNext == 0 && $total_page != 1) {
      $nextBtn.show(0);
      $prevBtn.css('display', 'none');
    } else if ($total_page == 1) {
      $prevBtn.css('display', 'none');
      $nextBtn.css('display', 'none');

      // if lastpageflag is true
      islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
    } else if (countNext > 0 && countNext < $total_page - 1) {
      $nextBtn.show(0);
      $prevBtn.show(0);
    } else if (countNext == $total_page - 1) {
      $nextBtn.css('display', 'none');
      $prevBtn.show(0);

      // if lastpageflag is true
      // setTimeout(function(){
        // if(countNext == $total_page - 1)
          // islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
      // }, 1000);
    }
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }

	var rhino = new NumberTemplate();

	rhino.init(10);

	var wrongclick = false;
	var total_count = 0;
	var no_of_incorr = 0;
	var no_of_select = 0;

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		texthighlight($board);

		$prevBtn.hide(0);

		no_of_incorr = 0;
		no_of_select = 0;
		total_count = $('.blank-space').length;

		exercise_span_selector2($('.correct'),$('.incorrect'),rhino,$nextBtn);

	}
	function exercise_span_selector2(class1, class2,rhino,nextbutton){
		nextbutton.hide(0);
		class1.click(function(){
			var $current = $(this);
			no_of_select++;
			play_correct_incorrect_sound(true);
			// $(this).addClass("done");
			// $(this).css('pointer-events', 'none');

			$(this).parent().fadeOut(500, function(){
				$current.parent().prev(".blank-space").html($current.html());
				$current.parent().prev(".blank-space").css('display', 'inline');
				if(no_of_select == total_count){
					nextbutton.show(0);
					if(no_of_incorr == 0){
						rhino.update(true);
					}
				}
			});
		});

		class2.click(function(){
			if(!wrongclick){
				rhino.update(false);
			}
			no_of_incorr++;
			// no_of_select++;
			play_correct_incorrect_sound(false);
			$(this).addClass("notdone");
			$(this).css('pointer-events', 'none');
			wrongclick = true;
		});
	}

	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		navigationcontroller();
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

		loadTimelineProgress($total_page,countNext+1);
	}
	templateCaller();

	$nextBtn.on('click', function() {
		countNext++;
		rhino.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
});

var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sound/";
var sound_exe = new buzz.sound((soundAsset + "exe.ogg"));

var content=[
	//slide 0
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{
			textclass: "instruction2",
			textdata: data.string.exe_txt
		},{
			textclass: "scoreboard",
			textdata: "0/0"
		}],
		hintimageblock:[
		{
			hintclass: "letter_textalign_right",
			hintdata : data.string.exe_letter_to
		},{
			hintclass: "letter_textalign_left",
			hintdata : data.string.exe_letter_body
		},{
			hintclass: "letter_textalign_left",
			hintdata : data.string.exe_letter_from
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e1_q1,
			textclass : 'instruction my_font_big',
		},{
			textdata : data.string.e1_q1_o1,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1_q1_o2,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e1_q1_o3,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.e1_q1_o4,
			textclass : 'class4 options'
		}],
	},
	//slide 1
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{
			textclass: "instruction2",
			textdata: data.string.exe_txt
		},{
			textclass: "scoreboard",
			textdata: "0/0"
		}],
		hintimageblock:[
		{
			hintclass: "letter_textalign_right",
			hintdata : data.string.exe_letter_to
		},{
			hintclass: "letter_textalign_left",
			hintdata : data.string.exe_letter_body
		},{
			hintclass: "letter_textalign_left",
			hintdata : data.string.exe_letter_from
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e1_q2,
			textclass : 'instruction my_font_big',
		},{
			textdata : data.string.e1_q2_o1,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1_q2_o2,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e1_q2_o3,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.e1_q2_o4,
			textclass : 'class3 options'
		}],
	},
	//slide 2
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{
			textclass: "instruction2",
			textdata: data.string.exe_txt
		},{
			textclass: "scoreboard",
			textdata: "0/0"
		}],
		hintimageblock:[
		{
			hintclass: "letter_textalign_right",
			hintdata : data.string.exe_letter_to
		},{
			hintclass: "letter_textalign_left",
			hintdata : data.string.exe_letter_body
		},{
			hintclass: "letter_textalign_left",
			hintdata : data.string.exe_letter_from
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e1_q3,
			textclass : 'instruction my_font_big',
		},{
			textdata : data.string.e1_q3_o1,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1_q3_o2,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e1_q3_o3,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.e1_q3_o4,
			textclass : 'class3 options'
		}],
	},
	//slide 3
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{
			textclass: "instruction2",
			textdata: data.string.exe_txt
		},{
			textclass: "scoreboard",
			textdata: "0/0"
		}],
		hintimageblock:[
		{
			hintclass: "letter_textalign_right",
			hintdata : data.string.exe_letter_to
		},{
			hintclass: "letter_textalign_left",
			hintdata : data.string.exe_letter_body
		},{
			hintclass: "letter_textalign_left",
			hintdata : data.string.exe_letter_from
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e1_q4,
			textclass : 'instruction my_font_big',
		},{
			textdata : data.string.e1_q4_o1,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1_q4_o2,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e1_q4_o3,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.e1_q4_o4,
			textclass : 'class3 options'
		}],
	},
	//slide 4
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{
			textclass: "instruction2",
			textdata: data.string.exe_txt
		},{
			textclass: "scoreboard",
			textdata: "0/0"
		}],
		hintimageblock:[
		{
			hintclass: "letter_textalign_right",
			hintdata : data.string.exe_letter_to
		},{
			hintclass: "letter_textalign_left",
			hintdata : data.string.exe_letter_body
		},{
			hintclass: "letter_textalign_left",
			hintdata : data.string.exe_letter_from
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e1_q5,
			textclass : 'instruction my_font_big',
		},{
			textdata : data.string.e1_q5_o1,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1_q5_o2,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e1_q5_o3,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.e1_q5_o4,
			textclass : 'class3 options'
		}],
	},
	//slide 5
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{
			textclass: "instruction2",
			textdata: data.string.exe_txt
		},{
			textclass: "scoreboard",
			textdata: "0/0"
		}],
		hintimageblock:[
		{
			hintclass: "letter_textalign_right",
			hintdata : data.string.exe_letter_to
		},{
			hintclass: "letter_textalign_left",
			hintdata : data.string.exe_letter_body
		},{
			hintclass: "letter_textalign_left",
			hintdata : data.string.exe_letter_from
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e1_q6,
			textclass : 'instruction my_font_big',
		},{
			textdata : data.string.e1_q6_o1,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1_q6_o2,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e1_q6_o3,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.e1_q6_o4,
			textclass : 'class3 options'
		}],
	},
	//slide 6
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{
			textclass: "instruction2",
			textdata: data.string.exe_txt
		},{
			textclass: "scoreboard",
			textdata: "0/0"
		}],
		hintimageblock:[
		{
			hintclass: "letter_textalign_right",
			hintdata : data.string.exe_letter_to
		},{
			hintclass: "letter_textalign_left",
			hintdata : data.string.exe_letter_body
		},{
			hintclass: "letter_textalign_left",
			hintdata : data.string.exe_letter_from
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e1_q7,
			textclass : 'instruction my_font_big',
		},{
			textdata : data.string.e1_q7_o1,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1_q7_o2,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e1_q7_o3,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.e1_q7_o4,
			textclass : 'class3 options'
		}],
	},
	//slide 7
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{
			textclass: "instruction2",
			textdata: data.string.exe_txt
		},{
			textclass: "scoreboard",
			textdata: "0/0"
		}],
		hintimageblock:[
		{
			hintclass: "letter_textalign_right",
			hintdata : data.string.exe_letter_to
		},{
			hintclass: "letter_textalign_left",
			hintdata : data.string.exe_letter_body
		},{
			hintclass: "letter_textalign_left",
			hintdata : data.string.exe_letter_from
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e1_q8,
			textclass : 'instruction my_font_big',
		},{
			textdata : data.string.e1_q8_o1,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1_q8_o2,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e1_q8_o3,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.e1_q8_o4,
			textclass : 'class3 options'
		}],
	}
];

// content.shufflearray();


$(function ()
{
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	// var score = 0;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		if (countNext==0) soundplayer(sound_exe);
		//randomize options
		var parent = $(".optionsblock");
		var divs = parent.children();
		parent.append(divs.splice(0, 1)[0]);
		while (divs.length) {
	    	parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}

		$('.question_number').html(data.string.e1_title);
		var wrong_clicked = false;
		$(".scoreboard").html(score+"/"+$total_page);

		$(".options").click(function(){
			if($(this).hasClass("class1")){
				if(!wrong_clicked){
					// rhino.update(true);
					score++;
				}
				$(".options").css('pointer-events', 'none');
				$(this).css({
					'border': '3px solid #FCD172',
					'background-color': '#6EB260',
					'color': 'white'
				});
				play_correct_incorrect_sound(1);
				if(countNext != $total_page)
					$nextBtn.show(0);
				$(".scoreboard").html(score+"/"+$total_page);
			}
			else{
				if(!wrong_clicked){
					// rhino.update(false);
				}
				$(".hint"+(countNext+1)).css("background-color", "#A8CAE1");
				$(this).css({
					'background-color': '#BA6B82',
					'border': 'none'
				});
				wrong_clicked = true;
				play_correct_incorrect_sound(0);
			}
		});
	}

	function sound_play_click(sound_id, click_class){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(0);
		});
	}

	function soundplayer(i){
		buzz.all().stop();
		i.play().bind("ended",function(){
			if (next==1)	navigationcontroller();
		});
	}
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

		loadTimelineProgress($total_page,countNext+1);


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		if(countNext == $total_page){
			TotalQues = $total_page;
			$(".board").html("").css("background-color", "#9DC4DD");
			create_exercise_menu_bar();
			$nextBtn.hide(0);
		}else{
			templateCaller();
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

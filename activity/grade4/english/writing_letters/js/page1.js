var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sound/";

var sound_s0 = new buzz.sound(soundAsset + "1.1.ogg");
var sound_s1 = new buzz.sound(soundAsset + "1.2.ogg");
var sound_s2 = new buzz.sound(soundAsset + "1.3.ogg");
var sound_s3 = new buzz.sound(soundAsset + "1.4.ogg");
var sound_s4 = new buzz.sound(soundAsset + "1.5.ogg");
var sound_s5 = new buzz.sound(soundAsset + "1.6.ogg");
var sound_s6 = new buzz.sound(soundAsset + "1.7.ogg");
var sound_s7 = new buzz.sound(soundAsset + "1.8.ogg");
var sound_s8 = new buzz.sound(soundAsset + "1.9.ogg");
var sound_s8_1 = new buzz.sound(soundAsset + "s1_p9_1.ogg");
var sound_s8_2 = new buzz.sound(soundAsset + "s1_p9_2.ogg");
var sound_s9 = new buzz.sound(soundAsset + "1.10.ogg");
var sound_s10 = new buzz.sound(soundAsset + "1.11.ogg");
var sound_s11 = new buzz.sound(soundAsset + "1.12.ogg");
var sound_intro = new buzz.sound(soundAsset + "intro.ogg");

var sound_group_p1 = [
  sound_s0,
  sound_s1,
  sound_s2,
  sound_s3,
  sound_s4,
  sound_s5,
  sound_s6,
  sound_s7,
  sound_s8,
  sound_s9,
  sound_s10,
  sound_s11
];

var content = [
  {
    uppertextblock: [
      {
        textclass: "maintext",
        textdata: data.lesson.chapter
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "mainimg",
            imgsrc: imgpath + "cover-letter.png"
          }
        ]
      }
    ]
  },
  {
    // slide0
    contentnocenteradjust: true,
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "sabina",
            imgsrc: imgpath + "sabina.png"
          }
        ]
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "sabinadialog",
        dialogimageclass: "dialogimage",
        dialogimagesrc: imgpath + "dialog.png",
        dialogcontentclass: "dialogtext",
        dialogcontent: data.string.p1_s0
      }
    ]
  },
  {
    // slide1
    contentnocenteradjust: true,
    uppertextblock: [
      {
        textclass: "description",
        textdata: data.string.p1_s1
      }
    ],
    lowertextblockadditionalclass: "letter_ltb",
    lowertextblock: [
      {
        textclass: "description_right",
        textdata: data.string.p1_s2
      },
      {
        textclass: "description_right",
        textdata: data.string.p1_s3
      },
      {
        textclass: "description_right",
        textdata: data.string.p1_s4
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s5
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s6
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s7
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s8
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s9
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s10
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s11
      }
    ]
  },
  {
    // slide2
    contentnocenteradjust: true,
    uppertextblock: [
      {
        textclass: "description",
        textdata: data.string.p1_s20
      }
    ],
    lowertextblockadditionalclass: "letter_ltb",
    lowertextblock: [
      {
        textclass: "description_right",
        textdata: data.string.p1_s2
      },
      {
        textclass: "description_right",
        textdata: data.string.p1_s3
      },
      {
        textclass: "description_right",
        textdata: data.string.p1_s4
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s5
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s6
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s7
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s8
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s9
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s10
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s11
      }
    ]
  },
  {
    // slide3
    contentnocenteradjust: true,
    uppertextblock: [
      {
        textclass: "description",
        textdata: data.string.p1_s20
      }
    ],
    imageblock: [
      {
        imageblockclass: "imagediv1",
        imagestoshow: [
          {
            imgclass: "image1",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_right",
            imagelabeldata: data.string.p1_s12
          }
        ]
      }
    ],
    lowertextblockadditionalclass: "letter_ltb",
    lowertextblock: [
      {
        textclass: "description_right bluehighlight",
        textdata: data.string.p1_s2
      },
      {
        textclass: "description_right bluehighlight",
        textdata: data.string.p1_s3
      },
      {
        textclass: "description_right bluehighlight",
        textdata: data.string.p1_s4
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s5
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s6
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s7
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s8
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s9
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s10
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s11
      }
    ]
  },
  {
    // slide4
    contentnocenteradjust: true,
    uppertextblock: [
      {
        textclass: "description",
        textdata: data.string.p1_s20
      }
    ],
    imageblock: [
      {
        imageblockclass: "imagediv1",
        imagestoshow: [
          {
            imgclass: "image1",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_right",
            imagelabeldata: data.string.p1_s12
          }
        ]
      },
      {
        imageblockclass: "imagediv2",
        imagestoshow: [
          {
            imgclass: "image2",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_left",
            imagelabeldata: data.string.p1_s13
          }
        ]
      }
    ],
    lowertextblockadditionalclass: "letter_ltb",
    lowertextblock: [
      {
        textclass: "description_right",
        textdata: data.string.p1_s2
      },
      {
        textclass: "description_right",
        textdata: data.string.p1_s3
      },
      {
        textclass: "description_right",
        textdata: data.string.p1_s4
      },
      {
        textclass: "description_left bluehighlight",
        textdata: data.string.p1_s5
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s6
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s7
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s8
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s9
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s10
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s11
      }
    ]
  },
  {
    // slide5
    contentnocenteradjust: true,
    uppertextblock: [
      {
        textclass: "description",
        textdata: data.string.p1_s20
      }
    ],
    imageblock: [
      {
        imageblockclass: "imagediv1",
        imagestoshow: [
          {
            imgclass: "image1",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_right",
            imagelabeldata: data.string.p1_s12
          }
        ]
      },
      {
        imageblockclass: "imagediv2",
        imagestoshow: [
          {
            imgclass: "image2",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_left",
            imagelabeldata: data.string.p1_s13
          }
        ]
      },
      {
        imageblockclass: "imagediv3",
        imagestoshow: [
          {
            imgclass: "image3",
            imgsrc: imgpath + "pink_arrow.png"
          },
          {
            imgclass: "image4",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_right",
            imagelabeldata: data.string.p1_s14
          }
        ]
      }
    ],
    lowertextblockadditionalclass: "letter_ltb",
    lowertextblock: [
      {
        textclass: "description_right",
        textdata: data.string.p1_s2
      },
      {
        textclass: "description_right",
        textdata: data.string.p1_s3
      },
      {
        textclass: "description_right",
        textdata: data.string.p1_s4
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s5
      },
      {
        textclass: "description_left bluehighlight",
        textdata: data.string.p1_s6
      },
      {
        textclass: "description_left bluehighlight",
        textdata: data.string.p1_s7
      },
      {
        textclass: "description_left bluehighlight",
        textdata: data.string.p1_s8
      },
      {
        textclass: "description_left bluehighlight",
        textdata: data.string.p1_s9
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s10
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s11
      }
    ]
  },
  {
    // slide6
    contentnocenteradjust: true,
    uppertextblock: [
      {
        textclass: "description",
        textdata: data.string.p1_s20
      }
    ],
    imageblock: [
      {
        imageblockclass: "imagediv1",
        imagestoshow: [
          {
            imgclass: "image1",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_right",
            imagelabeldata: data.string.p1_s12
          }
        ]
      },
      {
        imageblockclass: "imagediv2",
        imagestoshow: [
          {
            imgclass: "image2",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_left",
            imagelabeldata: data.string.p1_s13
          }
        ]
      },
      {
        imageblockclass: "imagediv3",
        imagestoshow: [
          {
            imgclass: "image3",
            imgsrc: imgpath + "pink_arrow.png"
          },
          {
            imgclass: "image4",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_right",
            imagelabeldata: data.string.p1_s14
          }
        ]
      },
      {
        imageblockclass: "imagediv4",
        imagestoshow: [
          {
            imgclass: "image5",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_left",
            imagelabeldata: data.string.p1_s15
          }
        ]
      }
    ],
    lowertextblockadditionalclass: "letter_ltb",
    lowertextblock: [
      {
        textclass: "description_right",
        textdata: data.string.p1_s2
      },
      {
        textclass: "description_right",
        textdata: data.string.p1_s3
      },
      {
        textclass: "description_right",
        textdata: data.string.p1_s4
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s5
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s6
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s7
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s8
      },
      {
        textclass: "description_left",
        textdata: data.string.p1_s9
      },
      {
        textclass: "description_left bluehighlight",
        textdata: data.string.p1_s10
      },
      {
        textclass: "description_left bluehighlight",
        textdata: data.string.p1_s11
      }
    ]
  },
  {
    // slide7
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentblock2",
    uppertextblock: [
      {
        textclass: "description",
        textdata: data.string.p1_s21
      }
    ],
    imageblock: [
      {
        imageblockclass: "imagediv1",
        imagestoshow: [
          {
            imgclass: "image1",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_right highlight",
            imagelabeldata: data.string.p1_s12
          }
        ]
      },
      {
        imageblockclass: "imagediv2",
        imagestoshow: [
          {
            imgclass: "image2",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_left",
            imagelabeldata: data.string.p1_s13
          }
        ]
      },
      {
        imageblockclass: "imagediv3",
        imagestoshow: [
          {
            imgclass: "image3",
            imgsrc: imgpath + "pink_arrow.png"
          },
          {
            imgclass: "image4",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_right",
            imagelabeldata: data.string.p1_s14
          }
        ]
      },
      {
        imageblockclass: "imagediv4",
        imagestoshow: [
          {
            imgclass: "image5",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_left",
            imagelabeldata: data.string.p1_s15
          }
        ]
      }
    ],
    lowertextblockadditionalclass: "letter_ltb",
    lowertextblock: [
      {
        textclass: "description_right",
        textdata: data.string.p1_s2
      },
      {
        textclass: "description_right",
        textdata: data.string.p1_s3
      },
      {
        textclass: "description_right",
        textdata: data.string.p1_s4
      },
      {
        textclass: "description_left greeting",
        textdata: data.string.p1_s5
      },
      {
        textclass: "description_left body",
        textdata: data.string.p1_s6
      },
      {
        textclass: "description_left body",
        textdata: data.string.p1_s7
      },
      {
        textclass: "description_left body",
        textdata: data.string.p1_s8
      },
      {
        textclass: "description_left body",
        textdata: data.string.p1_s9
      },
      {
        textclass: "description_left closing",
        textdata: data.string.p1_s10
      },
      {
        textclass: "description_left closing",
        textdata: data.string.p1_s11
      }
    ],
    filterdiv: true
  },
  {
    // slide8
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentblock2",
    uppertextblock: [
      {
        textclass: "description",
        textdata: data.string.p1_s21
      }
    ],
    imageblock: [
      {
        imageblockclass: "imagediv1",
        imagestoshow: [
          {
            imgclass: "image1",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_right",
            imagelabeldata: data.string.p1_s12
          }
        ]
      },
      {
        imageblockclass: "imagediv2",
        imagestoshow: [
          {
            imgclass: "image2",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_left highlight",
            imagelabeldata: data.string.p1_s13
          }
        ]
      },
      {
        imageblockclass: "imagediv3",
        imagestoshow: [
          {
            imgclass: "image3",
            imgsrc: imgpath + "pink_arrow.png"
          },
          {
            imgclass: "image4",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_right",
            imagelabeldata: data.string.p1_s14
          }
        ]
      },
      {
        imageblockclass: "imagediv4",
        imagestoshow: [
          {
            imgclass: "image5",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_left",
            imagelabeldata: data.string.p1_s15
          }
        ]
      }
    ],
    lowertextblockadditionalclass: "letter_ltb",
    lowertextblock: [
      {
        textclass: "description_right",
        textdata: data.string.p1_s2
      },
      {
        textclass: "description_right",
        textdata: data.string.p1_s3
      },
      {
        textclass: "description_right",
        textdata: data.string.p1_s4
      },
      {
        textclass: "description_left greeting",
        textdata: data.string.p1_s5
      },
      {
        textclass: "description_left body",
        textdata: data.string.p1_s6
      },
      {
        textclass: "description_left body",
        textdata: data.string.p1_s7
      },
      {
        textclass: "description_left body",
        textdata: data.string.p1_s8
      },
      {
        textclass: "description_left body",
        textdata: data.string.p1_s9
      },
      {
        textclass: "description_left closing",
        textdata: data.string.p1_s10
      },
      {
        textclass: "description_left closing",
        textdata: data.string.p1_s11
      }
    ],
    filterdiv: true
  },
  {
    // slide9
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentblock2",
    uppertextblock: [
      {
        textclass: "description",
        textdata: data.string.p1_s21
      }
    ],
    imageblock: [
      {
        imageblockclass: "imagediv1",
        imagestoshow: [
          {
            imgclass: "image1",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_right",
            imagelabeldata: data.string.p1_s12
          }
        ]
      },
      {
        imageblockclass: "imagediv2",
        imagestoshow: [
          {
            imgclass: "image2",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_left",
            imagelabeldata: data.string.p1_s13
          }
        ]
      },
      {
        imageblockclass: "imagediv3",
        imagestoshow: [
          {
            imgclass: "image3",
            imgsrc: imgpath + "pink_arrow.png"
          },
          {
            imgclass: "image4",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_right highlight",
            imagelabeldata: data.string.p1_s14
          }
        ]
      },
      {
        imageblockclass: "imagediv4",
        imagestoshow: [
          {
            imgclass: "image5",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_left",
            imagelabeldata: data.string.p1_s15
          }
        ]
      }
    ],
    lowertextblockadditionalclass: "letter_ltb",
    lowertextblock: [
      {
        textclass: "description_right",
        textdata: data.string.p1_s2
      },
      {
        textclass: "description_right",
        textdata: data.string.p1_s3
      },
      {
        textclass: "description_right",
        textdata: data.string.p1_s4
      },
      {
        textclass: "description_left greeting",
        textdata: data.string.p1_s5
      },
      {
        textclass: "description_left body",
        textdata: data.string.p1_s6
      },
      {
        textclass: "description_left body",
        textdata: data.string.p1_s7
      },
      {
        textclass: "description_left body",
        textdata: data.string.p1_s8
      },
      {
        textclass: "description_left body",
        textdata: data.string.p1_s9
      },
      {
        textclass: "description_left closing",
        textdata: data.string.p1_s10
      },
      {
        textclass: "description_left closing",
        textdata: data.string.p1_s11
      }
    ],
    filterdiv: true
  },
  {
    // slide10
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentblock2",
    uppertextblock: [
      {
        textclass: "description",
        textdata: data.string.p1_s21
      }
    ],
    imageblock: [
      {
        imageblockclass: "imagediv1",
        imagestoshow: [
          {
            imgclass: "image1",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_right",
            imagelabeldata: data.string.p1_s12
          }
        ]
      },
      {
        imageblockclass: "imagediv2",
        imagestoshow: [
          {
            imgclass: "image2",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_left",
            imagelabeldata: data.string.p1_s13
          }
        ]
      },
      {
        imageblockclass: "imagediv3",
        imagestoshow: [
          {
            imgclass: "image3",
            imgsrc: imgpath + "pink_arrow.png"
          },
          {
            imgclass: "image4",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_right",
            imagelabeldata: data.string.p1_s14
          }
        ]
      },
      {
        imageblockclass: "imagediv4",
        imagestoshow: [
          {
            imgclass: "image5",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_left highlight",
            imagelabeldata: data.string.p1_s15
          }
        ]
      }
    ],
    lowertextblockadditionalclass: "letter_ltb",
    lowertextblock: [
      {
        textclass: "description_right",
        textdata: data.string.p1_s2
      },
      {
        textclass: "description_right",
        textdata: data.string.p1_s3
      },
      {
        textclass: "description_right",
        textdata: data.string.p1_s4
      },
      {
        textclass: "description_left greeting",
        textdata: data.string.p1_s5
      },
      {
        textclass: "description_left body",
        textdata: data.string.p1_s6
      },
      {
        textclass: "description_left body",
        textdata: data.string.p1_s7
      },
      {
        textclass: "description_left body",
        textdata: data.string.p1_s8
      },
      {
        textclass: "description_left body",
        textdata: data.string.p1_s9
      },
      {
        textclass: "description_left closing",
        textdata: data.string.p1_s10
      },
      {
        textclass: "description_left closing",
        textdata: data.string.p1_s11
      }
    ],
    filterdiv: true
  },
  {
    // slide11
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentblock2",
    uppertextblock: [
      {
        textclass: "description",
        textdata: data.string.p1_s22
      }
    ],
    imageblock: [
      {
        imageblockclass: "imagediv1",
        imagestoshow: [
          {
            imgclass: "image1",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_right heading_btn upon_hover",
            imagelabeldata: data.string.p1_s12
          }
        ]
      },
      {
        imageblockclass: "imagediv2",
        imagestoshow: [
          {
            imgclass: "image2",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_left greeting_btn upon_hover",
            imagelabeldata: data.string.p1_s13
          }
        ]
      },
      {
        imageblockclass: "imagediv3",
        imagestoshow: [
          {
            imgclass: "image3",
            imgsrc: imgpath + "pink_arrow.png"
          },
          {
            imgclass: "image4",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_right body_btn upon_hover",
            imagelabeldata: data.string.p1_s14
          }
        ]
      },
      {
        imageblockclass: "imagediv4",
        imagestoshow: [
          {
            imgclass: "image5",
            imgsrc: imgpath + "pink_arrow.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_left closing_btn upon_hover",
            imagelabeldata: data.string.p1_s15
          }
        ]
      }
    ],
    lowertextblockadditionalclass: "letter_ltb",
    lowertextblock: [
      {
        textclass: "description_right heading",
        textdata: data.string.p1_s2
      },
      {
        textclass: "description_right heading",
        textdata: data.string.p1_s3
      },
      {
        textclass: "description_right heading",
        textdata: data.string.p1_s4
      },
      {
        textclass: "description_left greeting",
        textdata: data.string.p1_s5
      },
      {
        textclass: "description_left body",
        textdata: data.string.p1_s6
      },
      {
        textclass: "description_left body",
        textdata: data.string.p1_s7
      },
      {
        textclass: "description_left body",
        textdata: data.string.p1_s8
      },
      {
        textclass: "description_left body",
        textdata: data.string.p1_s9
      },
      {
        textclass: "description_left closing",
        textdata: data.string.p1_s10
      },
      {
        textclass: "description_left closing",
        textdata: data.string.p1_s11
      }
    ],
    filterdiv: true
  }
];

$(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();

  loadTimelineProgress($total_page, countNext + 1);

  /*==================================================
=            Handlers and helpers Block            =
==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  /*===============================================
     =            data highlight function            =
     ===============================================*/
  /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
  function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object"
      ? alert(
          "Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted"
        )
      : null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";
    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
        $(this).attr(
          "data-highlightcustomclass"
        ) /*if there is data-highlightcustomclass defined it is true else it is not*/
          ? (stylerulename = $(this).attr("data-highlightcustomclass"))
          : (stylerulename = "parsedstring");

        texthighlightstarttag = "<span class='" + stylerulename + "'>";
        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
        $(this).html(replaceinstring);
      });
    }
  }

  /*=====  End of data highlight function  ======*/

  /*===============================================
     =            user notification function        =
     ===============================================*/
  /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

  /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
  function notifyuser($notifyinside) {
    //check if $notifyinside is provided
    typeof $notifyinside !== "object"
      ? alert(
          "Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style."
        )
      : null;

    /*variable that will store the element(s) to remove notification from*/
    var $allnotifications = $notifyinside.find(
      "*[data-usernotification='notifyuser']"
    );
    // if there are any notifications removal required add the event handler
    if ($allnotifications.length > 0) {
      $allnotifications.one("click", function() {
        /* Act on the event */
        $(this).attr("data-isclicked", "clicked");
        $(this).removeAttr("data-usernotification");
      });
    }
  }
  /*=====  End of user notification function  ======*/

  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
  /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag) {
    typeof islastpageflag === "undefined"
      ? (islastpageflag = false)
      : typeof islastpageflag != "boolean"
      ? alert(
          "NavigationController : Hi Master, please provide a boolean parameter"
        )
      : null;

    if (countNext == 0 && $total_page != 1) {
      $nextBtn.show(0);
      $prevBtn.css("display", "none");
    } else if ($total_page == 1) {
      $prevBtn.css("display", "none");
      $nextBtn.css("display", "none");

      // if lastpageflag is true
      islastpageflag
        ? ole.footerNotificationHandler.lessonEndSetNotification()
        : ole.footerNotificationHandler.pageEndSetNotification();
    } else if (countNext > 0 && countNext < $total_page - 1) {
      $nextBtn.show(0);
      $prevBtn.show(0);
    } else if (countNext == $total_page - 1) {
      $nextBtn.css("display", "none");
      $prevBtn.show(0);

      // if lastpageflag is true
      setTimeout(function() {
        if (countNext == $total_page - 1)
          islastpageflag
            ? ole.footerNotificationHandler.lessonEndSetNotification()
            : ole.footerNotificationHandler.pageEndSetNotification();
      }, 2500);
    }
  }
  /*=====  End of user navigation controller function  ======*/

  /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
  /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

  /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
  function instructionblockcontroller() {
    var $instructionblock = $board.find("div.instructionblock");
    if ($instructionblock.length > 0) {
      var $contentblock = $board.find("div.contentblock");
      var $toggleinstructionblockbutton = $instructionblock.find(
        "div.toggleinstructionblock"
      );
      var instructionblockisvisibleflag;

      $contentblock.css("pointer-events", "none");

      $toggleinstructionblockbutton.on("click", function() {
        instructionblockisvisibleflag = $instructionblock.attr(
          "data-instructionblockshow"
        );
        if (instructionblockisvisibleflag == "true") {
          instructionblockisvisibleflag = "false";
          $contentblock.css("pointer-events", "auto");
        } else if (instructionblockisvisibleflag == "false") {
          instructionblockisvisibleflag = "true";
          $contentblock.css("pointer-events", "none");
        }

        $instructionblock.attr(
          "data-instructionblockshow",
          instructionblockisvisibleflag
        );
      });
    }
  }
  /*=====  End of InstructionBlockController  ======*/

  /*=====  End of Handlers and helpers Block  ======*/

  /*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    vocabcontroller.findwords(countNext);

    function playaudio(audio, show_hide_next_prev_btns) {
      show_hide_next_prev_btns =
        show_hide_next_prev_btns == null ? true : show_hide_next_prev_btns;
      // if(show_hide_next_prev_btns){
      countNext < $total_page ? $nextBtn.hide(0) : true;
      countNext > 0 ? $prevBtn.hide(0) : true;
      // }
      if (countNext > 6 && countNext < $total_page) {
        $(".imageclose").hide(0);
      }

      audio.play();
      audio.bind("ended", function() {
        // if(show_hide_next_prev_btns){
        countNext < $total_page - 1 ? $nextBtn.show(0) : true;
        countNext > 0 ? $prevBtn.show(0) : true;
        // }

        if (countNext == $total_page - 1) {
          $(".imageclose").show(0);
        }
        $(this).unbind("ended");
      });
    }

    switch (countNext) {
      case 0:
        playaudio(sound_intro, true);
        break;
      case 1:
        playaudio(sound_group_p1[0], true);
        break;
      case 2:
        playaudio(sound_group_p1[1], true);
        break;
      case 3:
        playaudio(sound_group_p1[2], true);
        break;
      case 4:
        $(".letter_ltb").css("color", "#aaa");
        playaudio(sound_group_p1[3], true);
        break;
      case 5:
        $(".letter_ltb").css("color", "#aaa");
        playaudio(sound_group_p1[4], true);
        break;
      case 6:
        $(".letter_ltb").css("color", "#aaa");
        playaudio(sound_group_p1[5], true);
        break;
      case 7:
        $(".letter_ltb").css("color", "#aaa");
        playaudio(sound_group_p1[6], true);
        break;
      case 8:
        var $sabinadialog2 = $(".sabinadialog2");
        $sabinadialog2.addClass("sabinadialog3");

        sound_s8_1.play();
        // sound_s8_1.bind("ended", function(){
        // 	sound_s8_2.play()
        // });
        var $filterdiv = $(".filterdiv");
        var $highlight = $(".highlight");
        var $p = $(".description_right");
        var $dialogtext2 = $(".dialogtext2");
        var $dialogimage = $(".dialogimage");
        var $about = $(".about");

        $nextBtn.hide(0);

        $highlight.click(function() {
          sound_s8_2.play();
          $highlight.removeClass("highlight");
          $(".letter_ltb").css("color", "#aaa");
          // $p.css({"z-index": "210", "color":"#F8F395"});
          $p.css({ "z-index": "210", color: "#0f0f0f" });
          $about.html($highlight.html());
          $filterdiv.show(0);
          $dialogimage.addClass("mirror_image");
          $dialogtext2.html(data.string.p1_s16);
          texthighlight($filterdiv);
          // playaudio(sound_group_p1[7]);
          $(".description").hide(0);
        });

        var $imageclose = $(".imageclose");
        $imageclose.click(function() {
          $nextBtn.show(0);
          $p.css({ "z-index": "", color: "#333" });
          $filterdiv.hide(0);
        });

        break;
      case 9:
        var $sabinadialog2 = $(".sabinadialog2");
        $sabinadialog2.addClass("sabinadialog3");

        var $filterdiv = $(".filterdiv");
        var $highlight = $(".highlight");
        var $p = $(".greeting");
        var $dialogtext2 = $(".dialogtext2");
        var $dialogimage = $(".dialogimage");
        var $about = $(".about");

        $nextBtn.hide(0);

        $highlight.click(function() {
          $highlight.removeClass("highlight");
          $(".letter_ltb").css("color", "#aaa");
          // $p.css({"z-index": "210", "color":"#F8F395"});
          $p.css({ "z-index": "210", color: "#0f0f0f" });
          $about.html($highlight.html());
          $filterdiv.show(0);
          $dialogimage.addClass("mirror_image");
          $dialogtext2.html(data.string.p1_s17);
          texthighlight($filterdiv);
          playaudio(sound_group_p1[8]);
          $(".description").hide(0);
        });

        var $imageclose = $(".imageclose");
        $imageclose.click(function() {
          $p.css({ "z-index": "", color: "#333" });
          $filterdiv.hide(0);
          // $nextBtn.show(0);
        });

        break;
      case 10:
        var $sabinadialog2 = $(".sabinadialog2");
        $sabinadialog2.addClass("sabinadialog5");

        var $filterdiv = $(".filterdiv");
        var $highlight = $(".highlight");
        var $p = $(".body");
        var $dialogtext2 = $(".dialogtext2");
        var $dialogimage = $(".dialogimage");
        var $about = $(".about");

        $nextBtn.hide(0);

        $highlight.click(function() {
          $highlight.removeClass("highlight");
          $(".letter_ltb").css("color", "#aaa");
          // $p.css({"z-index": "210", "color":"#F8F395"});
          $p.css({ "z-index": "210", color: "#0f0f0f" });
          $about.html($highlight.html());
          $filterdiv.show(0);
          $dialogimage.addClass("mirror_image");
          $dialogtext2.html(data.string.p1_s18);
          texthighlight($filterdiv);
          playaudio(sound_group_p1[9]);
          $(".description").hide(0);
        });

        var $imageclose = $(".imageclose");
        $imageclose.click(function() {
          $p.css({ "z-index": "", color: "#333" });
          $filterdiv.hide(0);
          // $nextBtn.show(0);
        });

        break;
      case 11:
        var $sabinadialog2 = $(".sabinadialog2");

        var $filterdiv = $(".filterdiv");
        var $highlight = $(".highlight");
        var $p = $(".closing");
        var $dialogtext2 = $(".dialogtext2");
        var $dialogimage = $(".dialogimage");
        var $about = $(".about");

        $nextBtn.hide(0);

        $highlight.click(function() {
          $highlight.removeClass("highlight");
          $(".letter_ltb").css("color", "#aaa");
          // $p.css({"z-index": "210", "color":"#F8F395"});
          $p.css({ "z-index": "210", color: "#0f0f0f" });
          $about.html($highlight.html());
          $filterdiv.show(0);
          $dialogimage.addClass("mirror_imageX");
          $dialogtext2.html(data.string.p1_s19);
          texthighlight($filterdiv);
          playaudio(sound_group_p1[10]);
          $(".description").hide(0);
        });

        var $imageclose = $(".imageclose");
        $imageclose.click(function() {
          $p.css({ "z-index": "", color: "#333" });
          $filterdiv.hide(0);
          // $nextBtn.show(0);
        });

        break;
      case 12:
        playaudio(sound_group_p1[11]);
        var $heading_btn = $(".heading_btn");
        var $greeting_btn = $(".greeting_btn");
        var $body_btn = $(".body_btn");
        var $closing_btn = $(".closing_btn");

        var $sabinadialog2 = $(".sabinadialog2");

        var $filterdiv = $(".filterdiv");
        var $highlight = $(".highlight");

        var $p_heading = $(".heading");
        var $p_greeting = $(".greeting");
        var $p_body = $(".body");
        var $p_closing = $(".closing");

        var $dialogtext2 = $(".dialogtext2");
        var $dialogimage = $(".dialogimage");
        var $about = $(".about");

        $nextBtn.hide(0);
        $(".imagediv1 label").css({ cursor: "pointer" });
        $(".imagediv2 label").css({ cursor: "pointer" });
        $(".imagediv3 label").css({ cursor: "pointer" });
        $(".imagediv4 label").css({ cursor: "pointer" });
        $heading_btn.click(function() {
          $sabinadialog2.addClass("sabinadialog3");
          $p_greeting.css({ "z-index": "", color: "#aaa" });
          $p_body.css({ "z-index": "", color: "#aaa" });
          $p_closing.css({ "z-index": "", color: "#aaa" });
          // $p_heading.css({"z-index": "210", "color":"#F8F395"});
          $p_heading.css({ "z-index": "210", color: "#0f0f0f" });
          $about.html($heading_btn.html());
          $filterdiv.show(0);
          $dialogimage.addClass("mirror_image");
          $dialogtext2.html(data.string.p1_s16);
          texthighlight($filterdiv);
          playaudio(sound_group_p1[7]);
          $(".description").hide(0);
        });

        $greeting_btn.click(function() {
          $sabinadialog2.addClass("sabinadialog3");
          $p_heading.css({ "z-index": "", color: "#aaa" });
          $p_body.css({ "z-index": "", color: "#aaa" });
          $p_closing.css({ "z-index": "", color: "#aaa" });
          // $p_greeting.css({"z-index": "210", "color":"#F8F395"});
          $p_greeting.css({ "z-index": "210", color: "#0f0f0f" });
          $about.html($greeting_btn.html());
          $filterdiv.show(0);
          $dialogimage.addClass("mirror_image");
          $dialogtext2.html(data.string.p1_s17);
          texthighlight($filterdiv);
          playaudio(sound_group_p1[8]);
          $(".description").hide(0);
        });

        $body_btn.click(function() {
          $sabinadialog2.addClass("sabinadialog5");
          $p_heading.css({ "z-index": "", color: "#aaa" });
          $p_greeting.css({ "z-index": "", color: "#aaa" });
          $p_closing.css({ "z-index": "", color: "#aaa" });
          // $p_body.css({"z-index": "210", "color":"#F8F395"});
          $p_body.css({ "z-index": "210", color: "#0f0f0f" });
          $about.html($body_btn.html());
          $filterdiv.show(0);
          $dialogimage.addClass("mirror_image");
          $dialogtext2.html(data.string.p1_s18);
          texthighlight($filterdiv);
          playaudio(sound_group_p1[9]);
          $(".description").hide(0);
        });

        $closing_btn.click(function() {
          $p_heading.css({ "z-index": "", color: "#aaa" });
          $p_greeting.css({ "z-index": "", color: "#aaa" });
          $p_body.css({ "z-index": "", color: "#aaa" });
          // $p_closing.css({"z-index": "210", "color":"#F8F395"});
          $p_closing.css({ "z-index": "210", color: "#0f0f0f" });
          $about.html($closing_btn.html());
          $filterdiv.show(0);
          $dialogimage.addClass("mirror_imageX");
          $dialogtext2.html(data.string.p1_s19);
          texthighlight($filterdiv);
          playaudio(sound_group_p1[10]);
          $(".description").hide(0);
        });

        var $imageclose = $(".imageclose");
        $imageclose.click(function() {
          $dialogimage.removeClass("mirror_imageX");
          $dialogimage.removeClass("mirror_image");
          $sabinadialog2.removeClass("sabinadialog3");
          $sabinadialog2.removeClass("sabinadialog4");
          $sabinadialog2.removeClass("sabinadialog5");
          $p_heading.css({ "z-index": "", color: "#333" });
          $p_greeting.css({ "z-index": "", color: "#333" });
          $p_body.css({ "z-index": "", color: "#333" });
          $p_closing.css({ "z-index": "", color: "#333" });

          $(".description").show(0);
          $filterdiv.hide(0);
        });

        break;
      default:
        break;
    }

    // splitintofractions($(".fractionblock"));
  }

  /*=====  End of Templates Block  ======*/

  /*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller() {
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css("display", "none");
    $nextBtn.css("display", "none");

    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();
    /*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page, countNext + 1);
  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */

  $nextBtn.on("click", function() {
    countNext++;
    templateCaller();
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    countNext--;
    templateCaller();
    /* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
    countNext < $total_page
      ? ole.footerNotificationHandler.hideNotification()
      : null;
  });
  /*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sound/";

var sound_s0 = new buzz.sound((soundAsset + "s3_p1.ogg"));
var sound_s1 = new buzz.sound((soundAsset + "s3_p2_1.ogg"));
var sound_s2 = new buzz.sound((soundAsset + "s3_p2_2.ogg"));
var sound_s3 = new buzz.sound((soundAsset + "s3_p3.ogg"));
var sound_s4 = new buzz.sound((soundAsset + "s3_p4.ogg"));

var content=[
	{
		// slide0
		contentblockadditionalclass: "content_front_bg",
		uppertextblock:[{
			textclass: "title",
			textdata: data.string.p3_s0
		}]
	},{
		// slide1
		contentnocenteradjust: true,
		headerblock:[{
			textclass: "description_title1",
			textdata: data.string.p3_s1
		},{
			textclass: "description_title2",
			textdata: data.string.p3_s2
		}],
		uppertextblock: [{
			textclass : "message",
			textdata: data.string.p3_s18
		}]
	},{
		// slide2
		contentnocenteradjust: true,
		headerblock:[{
			textclass: "description_title1",
			textdata: data.string.p3_s1
		},{
			textclass: "description_title2",
			textdata: data.string.p3_s9
		}],
		uppertextblock: [{}],
		imageblock: [{
			imagestoshow: [{
					imgclass: "arrow1 mirror",
					imgsrc: imgpath + "pink_arrow.png"
				},{
					imgclass: "arrow2 mirror",
					imgsrc: imgpath + "pink_arrow.png"
				},{
					imgclass: "arrow3 mirror",
					imgsrc: imgpath + "pink_arrow.png"
				},{
					imgclass: "arrow4 mirror",
					imgsrc: imgpath + "pink_arrow.png"
				},{
					imgclass: "arrow5 mirror",
					imgsrc: imgpath + "pink_arrow.png"
				},{
					imgclass: "arrow6 mirror",
					imgsrc: imgpath + "pink_arrow.png"
				},{
					imgclass: "arrow7 mirror",
					imgsrc: imgpath + "pink_arrow.png"
				}]
		}]
	},{
		// slide3
		contentnocenteradjust: true,
		headerblock:[{
			textclass: "description_title1",
			textdata: data.string.p3_s1
		},{
			textclass: "description_title2",
			textdata: data.string.p3_s21
		}],
		uppertextblock: [{
			textclass : "message",
			textdata: data.string.p3_s21a
		}]
	},{
		// slide4
		contentblockadditionalclass: "white_bg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb_100",
		uppertextblock: [{
			textclass : "title_top",
			textdata: data.string.p3_s20
		}]
	}
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
	var current_sound = sound_s0;
  var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
  loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

		 function navigationcontroller(islastpageflag){
		 		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		 	if (countNext == 0 && $total_page != 1) {
		 		$nextBtn.show(0);
		 		$prevBtn.css('display', 'none');
		 	} else if ($total_page == 1) {
		 		$prevBtn.css('display', 'none');
		 		$nextBtn.css('display', 'none');

		 		// if lastpageflag is true
		 		islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		 	} else if (countNext > 0 && countNext < $total_page - 1) {
		 		$nextBtn.show(0);
		 		$prevBtn.show(0);
		 	} else if (countNext == $total_page - 1) {
		 		$nextBtn.css('display', 'none');
		 		$prevBtn.show(0);

		 		// if lastpageflag is true
		 		setTimeout(function(){
		 			if(countNext == $total_page - 1)
		 				islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		 		}, 1000);
		 	}
		  }

   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/

	var letter = {
		to : "",
		from : "",
		gender : "",
		address : "",
		ward_no : "",
		district : "",
		zone : "",
		country : "",
		letter_body : ""
	};

  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    vocabcontroller.findwords(countNext);
		// splitintofractions($(".fractionblock"));
		switch(countNext){
			case 0:
				soundplayer(sound_s0);
				$nextBtn.hide();
				break;
			case 1:
				current_sound = sound_s1;
				current_sound.play();
				current_sound.bind("ended",function(){
					current_sound = sound_s2;
					current_sound.play();
				});

				var $uppertextblock = $(".textblock");
				var $p1;

				var top = 16;

				for(var i = 0; i < 6;i++){
					if(i == 0){
						$p1 = $("<p class='question'>");
						$p1.css("top", top+"%");
						$p1.html(data.string.p3_s3);
						top = 30;
					} else {

						$p1 = $("<p class='option'>");
						$p1.css("top", top+"%");
						top += 10;
						switch(i){
							case 1:
								$p1.html(data.string.p3_s4);
								break;
							case 2:
								$p1.html(data.string.p3_s5);
								break;
							case 3:
								$p1.html(data.string.p3_s6);
								break;
							case 4:
								$p1.html(data.string.p3_s7);
								break;
							case 5:
								$p1.html(data.string.p3_s8);
								break;
							default:
								break;
						}
					}

					$uppertextblock.append($p1);

				}

				$(".option").click(function(){
					current_sound.stop();
					letter.to = $(this).html();
					if(!$(this).hasClass("clicked")){
						$(".clicked").removeClass("clicked");
						$(this).addClass("clicked");
					}
					$(".message").hide(0);

				});
				break;
			case 2:
				current_sound = sound_s3;
				current_sound.play();
				var $uppertextblock = $(".textblock");
				var $p1;
				var $p2;

				var top = 0;

				for(var i = 0; i < 7;i++){
					if(i == 1){
						top = 20;
						var $form = $("<form>");
						$form.html(' <input type="radio" class="gender" value="male"> <span class="male" >'+ data.string.p3_s16 +'</span>  <input type="radio" class="gender" value="female"> <span class="female">'+ data.string.p3_s17 +'</span> ');
						$form.css("top", top+"%");
						$uppertextblock.append($form);
					}else{
						$p1 = $("<p>");
						$p1.addClass("description1");

						$p2 = $("<input type='text'>");

						$p2.addClass("description2");

						switch(i){
							case 0:
								top = 8;
								$p1.html(data.string.p3_s10);
								$p2.html();
								break;
							case 2:
								top = 32;
								$p1.html(data.string.p3_s11);
								$p2.html();
								break;
							case 3:
								top = 52;
								$p1.html(data.string.p3_s12);
								$p2.html();
								break;
							case 4:
								top = 64;
								$p1.html(data.string.p3_s13);
								$p2.html();
								break;
							case 5:
								top = 76;
								$p1.html(data.string.p3_s14);
								$p2.html();
								break;
							case 6:
								top = 88;
								$p1.html(data.string.p3_s15);
								$p2.html();
								break;
							default:
								break;
						}

						$p1.css({ "top": top+"%"});
						$p2.css({ "top": top+"%"});
						$uppertextblock.append($p1);
						$uppertextblock.append($p2);
					}
				}

				$(".male").click(function(){
					$("input:radio:last").prop("checked", false);
					$("input:radio:first").prop("checked", true).trigger("click");
				});

				$(".female").click(function(){
					$("input:radio:first").prop("checked", false);
					$("input:radio:last").prop("checked", true).trigger("click");
				});

				$("form input").click(function(){
					if($(this).hasClass("required")){
						$(".female").removeClass("required");
						$(".male").removeClass("required");
					}
					if(($(this).val()).toLowerCase() == "male"){
						$(".female").removeClass("clicked");
						$(".male").addClass("clicked");
					} else {
						$(".male").removeClass("clicked");
						$(".female").addClass("clicked");
					}
				});
				var $description2 = $(".description2");
				$.each($description2, function(index, val){
					switch(index){
						case 0:
							input_box($(val), 30, true );
							break;
						case 2:
							input_box($(val), 5, true );
							break;
						case 1:
						case 3:
						case 4:
						case 5:
							input_box($(val), 20, true );
							break;
						default:
						 	break;
					}
				});

				$(".gender").click(function(){
					letter.gender = $(this).val();
				});
				break;
			case 3:
				current_sound = sound_s4;
				current_sound.play();
				var $uppertextblock = $(".textblock");
				var $p1;

				var top = 0;
				var right = 15;
				var left = 15;

				for (var i = 0; i < 8; i++){
					if (i == 0){
						$p1 = $("<p class='question'>");
						$p1.css("top", top+"%");
						$p1.html(data.string.p3_s22);
						top = 20;
					} else if(i == 5){
						$p1 =  $("<textarea class = 'letter' maxlength='500'>");
						top = 37;
						$p1.css("top", top+"%");
					} else{
						switch (i){
							case 1:
								 $p1 = $("<p class='leanright'>");
								 $p1.html(letter.address+" - "+letter.ward_no+", "+letter.district);
								 top = 14;
								 $p1.css({"top" : top+"%",
								 		"right" : right+"%"});
								 break;
							case 2:
								 $p1 = $("<p class='leanright'>");
								 $p1.html(letter.zone+", "+letter.country);
								 $p1.css({"top" : top+"%",
								 		"right" : right+"%"});
								 break;
							case 3:
								 $p1 = $("<p class='leanright'>");
								 var m_names = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

								 var d = new Date();
								 var curr_date = d.getDate();
								 var curr_month = d.getMonth();
								 var curr_year = d.getFullYear();
								 $p1.html(curr_date + " " + m_names[curr_month] + ", " + curr_year);
								 $p1.css({"top" : top+"%",
								 		"right" : right+"%"});
								 break;
							case 4:

								 $p1 = $("<p class='leanleft'>");
								 $p1.html("<br> Dear "+ (letter.to).toLowerCase()+",");
								 $p1.css({"top" : top+"%",
								 		"left" : left+"%"});
								 break;
							case 6:
								 top = 62;
								 $p1 = $("<p class='leanleft'>");
								 switch(letter.to){
								 	 case "Father":
								 	 case "Mother":
									 	 var childtag = (letter.gender == "male")? " son,": " daughter,";
										 $p1.html("<br> Your loving"+ childtag);
										 break;

									 case "Teacher":
										 $p1.html("<br> Yours Truly,");
										 break;
									 case "Grandmother":
									 case "Grandfather":
										 var childtag = (letter.gender == "male")? " grandson,": " granddaughter,";
										 $p1.html("<br> Your loving"+ childtag);
										 break;
									 default:
										 break;
									}
									$p1.css({"top" : top+"%",
								 		"left" : left+"%"});
									break;
							case 7:
									$p1 = $("<p class='leanleft'>");
									$p1.html(letter.from);
									$p1.css({"top" : top+"%",
								 		"left" : right+"%"});
								 break;
							default:
								 break;

						}
					}
					$uppertextblock.append($p1);
				}

				$("textarea").keyup(function(event){
					if(String(event.target.value).length == 0){
		    			$(this).removeClass("clicked2");
		    		} else {
		    			if($(this).hasClass("required")){
			    			$(this).removeClass("required");
		    			}
		    			$(this).addClass("clicked2");
		    		}
				});
				break;
			case 4:
				var to =  letter.to;
				$(".title_top").append(" "+(letter.to).toUpperCase());

				var $letterfinal = $("<div class='letter2'>");
				$(".utb_100").append($letterfinal);
				var $p_add1 = $("<p class='leanright'>");
				var $p_add2 = $("<p class='leanright'>");
				var $p_date = $("<p class='leanright'>");

				$letterfinal.append($p_add1);
				$letterfinal.append($p_add2);
				$letterfinal.append($p_date);

				$p_add1.html(letter.address+" - "+letter.ward_no+", "+letter.district);
				$p_add2.html(letter.zone+", "+letter.country);

				var m_names = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

				var d = new Date();
				var curr_date = d.getDate();
				var curr_month = d.getMonth();
				var curr_year = d.getFullYear();
				$p_date.html(curr_date + " " + m_names[curr_month] + ", " + curr_year);

				var $p_dear = $("<p class='leanleft'>");
				var $p_body = $("<p class='leanleft'>");
				var $p_sincerely = $("<p class='leanleft'>");
				var $p_name = $("<p class='leanleft'>");
				$letterfinal.append($p_dear);
				$letterfinal.append($p_body);
				$letterfinal.append($p_sincerely);
				$letterfinal.append($p_name);

				$p_dear.html("<br> Dear "+ (letter.to).toLowerCase()+",");
				$p_body.html("<br> "+letter.letter_body);
				$p_name.html(letter.from);
				switch(letter.to){
					case "Father":
					case "Mother":
						var childtag = (letter.gender == "male")? " son,": " daughter,";
						$p_sincerely.html("<br> Your loving"+ childtag);
						break;

					case "Teacher":
						$p_sincerely.html("<br> Yours Truly,");
						break;
					case "Grandmother":
					case "Grandfather":
						var childtag = (letter.gender == "male")? " grandson,": " granddaughter,";
						$p_sincerely.html("<br> Your loving"+ childtag);
						break;
					default:
						break;
				}

				break;
			default:
				break;
		}
  }

  /** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box($input_class, max_number, inputistext ) {
		$input_class.keydown(function(event){
    		var charCode = (event.which) ? event.which : event.keyCode;
    		/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */

			var condition;
			if(inputistext){
				condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			}else{
				condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			}

			//check if user inputs del, backspace or arrow keys
   			if (!condition) {
    			return true;
    		}

    		//check if user inputs more than one '.'
			if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
        		return false;
    		}
    		//check . and 0-9 separately after checking arrow and other keys
    		if(!inputistext){
    			if((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110 ){
	    			return false;
	    		}
    		}

    		//check max no of allowed digits
    		if (String(event.target.value).length >= max_number) {
    			return false;
    		}

  			return true;
		}).keyup(function(event){
			if(String(event.target.value).length == 0){
    			$(this).removeClass("clicked2");
    		} else {
    			if($(this).hasClass("required")){
	    			$(this).removeClass("required");
    			}
    			$(this).addClass("clicked2");
    		}
		});
	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */


  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */
	function soundplayer(i){
		buzz.all().stop();
		i.play().bind("ended",function(){
				navigationcontroller();
		});
	}
	function verifybeforejump(){
		var continueflag = true;
		switch(countNext){
			case 1:
				if(letter.to == ""){
					continueflag = false;
					$(".message").show(0);
				}

				break;
			case 2:
				var $description2 = $(".description2");

				letter.from = ($($description2[0]).val() != null)? $($description2[0]).val(): "";
				if(letter.from == ""){
					continueflag = false;
					$(".arrow1").show(0);
					$($description2[0]).addClass("required");
				}


				if(letter.gender == ""){
					continueflag = false;
					$(".arrow2").show(0);
					$(".male, .female").addClass("required");
				}

				letter.address = ($($description2[1]).val() != null)? $($description2[1]).val(): "";
				if(letter.address == ""){
					continueflag = false;
					$(".arrow3").show(0);
					$($description2[1]).addClass("required");
				}

				letter.ward_no = ($($description2[2]).val() != null)? $($description2[2]).val(): "";
				if(letter.ward_no == ""){
					continueflag = false;
					$(".arrow4").show(0);
					$($description2[2]).addClass("required");
				}

				letter.district = ($($description2[3]).val() != null)? $($description2[3]).val(): "";
				if(letter.district == ""){
					continueflag = false;
					$(".arrow5").show(0);
					$($description2[3]).addClass("required");
				}

				letter.zone = ($($description2[4]).val() != null)? $($description2[4]).val(): "";

				if(letter.zone == ""){
					continueflag = false;
					$(".arrow6").show(0);
					$($description2[4]).addClass("required");
				}

				letter.country = ($($description2[5]).val() != null)? $($description2[5]).val(): "";

				if(letter.country == ""){
					continueflag = false;
					$(".arrow7").show(0);
					$($description2[5]).addClass("required");
				}
				break;
			case 3:
				letter.letter_body = ($(".letter").val() != null) ? $(".letter")[0].value.replace(/\n/g, '<br/>'): "";
				if(letter.letter_body == ""){
					continueflag = false;
					$(".letter").addClass("required");
					$(".message").show(0);
				}
				break;
			default:
			 	break;
		}
		return continueflag;
	}

	$nextBtn.on('click', function() {
		current_sound.stop();
			if(verifybeforejump()){
				countNext++;
				templateCaller();
			}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "theblue",
		uppertextblock:[
			{
				textclass: "covertext textrolldown",
				textdata: data.string.diy
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "diybox diyrolldown",
					imgid : 'diybox',
					imgsrc: ""
				}
			]
		}]
},
// slide1
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "theblue",
	formblock:[
	{

	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "diybox diyrolldown",
				imgid : 'diybox',
				imgsrc: ""
			}
		]
	}]
},
// slide2
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "theblue",
	uppertextblock:[
		{
			textclass: "covertext textrolldown2",
			textdata: data.string.p3text2
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "diybox squeeze",
				imgid : 'diybox',
				imgsrc: ""
			},
			{
				imgclass: "forhover boy",
				imgid : 'boy',
				imgsrc: ""
			},
			{
				imgclass: "forhover girl",
				imgid : 'girl',
				imgsrc: ""
			}
		]
	}]
},
// slide3
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg1",
	uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "child_name",
			textclass: "uptext",
			textdata: data.string.p3text5
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "selected",
				imgid : 'boy',
				imgsrc: ""
			},
		]
	}]
},
// slide4
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "theblue",
	uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "child_name",
			textclass: "uptext",
			textdata: data.string.p3text6
		},
		{
			textclass: "open",
			textdata: data.string.open
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "child_name",
			textclass: "lettertext",
			textdata: data.string.p3text7
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "letter",
				imgid : 'letter',
				imgsrc: ""
			},
			{
				imgclass: "letter letop",
				imgid : 'letter_top',
				imgsrc: ""
			},
		]
	}]
},
// slide5
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg2",
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "selected",
				imgid : 'boy',
				imgsrc: ""
			},
			{
				imgclass: "bus",
				imgid : 'bus',
				imgsrc: ""
			},
		]
	}]
},
// slide6
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg3",
	uppertextblock:[
		{
			textclass: "uptext",
			textdata: data.string.p3text8
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "selected",
				imgid : 'boy',
				imgsrc: ""
			},
			{
				imgclass: "bus",
				imgid : 'bus',
				imgsrc: ""
			},
		]
	}]
},
// slide7
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg3",
	uppertextblock:[
		{
			textclass: "battext",
			textdata: data.string.p3text9
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "selected nofade",
				imgid : 'boy',
				imgsrc: ""
			},
			{
				imgclass: "battery",
				imgid : 'battery',
				imgsrc: ""
			}
		]
	}]
},
// slide8
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg3",
	uppertextblock:[
		{
			textclass: "battext",
			textdata: data.string.p3text10
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "selected nofade",
				imgid : 'boy',
				imgsrc: ""
			},
			{
				imgclass: "battery",
				imgid : 'battery',
				imgsrc: ""
			}
		]
	}]
},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	var global_save_val;
	var img_type;
	var filename;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "diybox", src: imgpath+"diy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl", src: imgpath+"girl.png", type: createjs.AbstractLoader.IMAGE},
			{id: "letter", src: imgpath+"envelope.png", type: createjs.AbstractLoader.IMAGE},
			{id: "letter_top", src: imgpath+"envelope_top.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bus", src: imgpath+"bus.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy", src: imgpath+"boy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "battery", src: imgpath+"battery/battery05.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"p3_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p3_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p3_s3.ogg"},
			{id: "sound_4", src: soundAsset+"p3_s4_a.ogg"},
			{id: "sound_5", src: soundAsset+"p3_s4_b.ogg"},
			{id: "sound_6", src: soundAsset+"p3_s6.ogg"},
			{id: "sound_7", src: soundAsset+"p3_s7.ogg"},
			{id: "sound_8", src: soundAsset+"p3_s8.ogg"},
			{id: "bus_sound", src: soundAsset+"bus_sound.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext){
			case 0:
			sound_player("sound_1");
			break;
			case 1:
			$(".inpbox").keyup(function(event){
	    		if (String(event.target.value).length >= 1) {
	    			$nextBtn.show(0);
	    			global_save_val = String(event.target.value);
	    		}
	    		else{
	    			$nextBtn.hide(0);
	    		}
	  			return true;
			});
			break;
			case 2:
			sound_player("sound_2",1);
			$(".forhover").click(function(){
				navigationcontroller();
				if($(this).hasClass("boy")){
					$(".girl").fadeOut();
					img_type = $(this).attr("src");
					filename = img_type.replace(/^.*[\\\/]/, '');
				}
				else {
					$(".boy").fadeOut();
					img_type = $(this).attr("src");
					filename = img_type.replace(/^.*[\\\/]/, '');
					//alert(filename);
				}
			});
			break;
			case 3:
			setTimeout(function(){sound_player("sound_3");},2000);
			//alert(filename);
			$(".selected").attr("src", imgpath + filename);
			$(".child_name").eq(0).html(global_save_val);
			$(".child_name").eq(1).css({"opacity":"0"});
			$('.child_name').addClass('opacity');
			break;
			case 4:
			sound_player("sound_4",1);
			$(".child_name").html(global_save_val);
			$(".open").click(function(){
				$(this).hide(0);
				$(".letop").addClass("letter-flip");
				$(".letter").delay(2000).fadeOut(1000);
				setTimeout(function(){
						$(".lettertext").addClass("letterzoom");
						sound_player("sound_5");

				},3000);
			})
			break;
			case 5:
			$(".selected").attr("src", imgpath + filename);
			sound_player("bus_sound");
			$( ".bus" ).animate({
				left: "-=90%",
			}, 5000, function() {
				$(".selected").hide(0);
				$( ".bus" ).animate({
					left: "-=90%",
				}, 5000);
				setTimeout(function(){
						navigationcontroller();
				},5000);
			//	Animation complete.
			});
			break;
			case 6:
			$(".selected").hide(0);
			$(".selected").attr("src", imgpath + filename);
			$(".uptext").hide(0);
			$( ".bus" ).animate({
				left: "-=90%",
			}, 5000, function() {
				$(".selected").fadeIn(1000);
				$(".uptext").delay(2000).fadeIn(1000);
				setTimeout(function(){
					sound_player("sound_6");
				},2000);
				$( ".bus" ).animate({
					left: "-=90%",
				}, 5000);
				// Animation complete.
			});
			break;
			case 7:
			sound_player("sound_7");
			$(".selected").attr("src", imgpath + filename);
			break;
			case 8:
			$(".selected").attr("src", imgpath + filename);
			sound_player("sound_8");
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next == null)
			navigationcontroller();
		});
	}
	function sound_player_loop(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			sound_player_loop(sound_id);
		});
	}

	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2);
		});

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
			for (var i = 0; i < content.length; i++) {
				slides(i);
				$($('.totalsequence')[i]).html(i);
				$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
			"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
			}
			function slides(i){
					$($('.totalsequence')[i]).click(function(){
						countNext = i;
						templateCaller();
					});
				}
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		case 0:
		$(".diybox").removeClass("diyrolldown").addClass("diyrollup");
		$(".covertext").removeClass("textrolldown").addClass("textrollup");
		setTimeout(function(){
			countNext++;
			templateCaller();
		}, 1000);
		break;
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

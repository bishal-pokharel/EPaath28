var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var imgpath = $ref+"/images/";

var content=[
// slide0
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg1",
	uppertextblock:[
		{
			textclass: "battext",
			textdata: data.string.p4text1
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "battery",
				imgid : 'battery',
				imgsrc: ""
			},
			{
				imgclass: "garb1",
				imgid : 'garbage1',
				imgsrc: ""
			},
			{
				imgclass: "garb2",
				imgid : 'garbage2',
				imgsrc: ""
			}
		]
	}]
},
// slide1
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg1",
	uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "musmusnt",
			textclass: "battext",
			textdata: data.string.p4q1sent
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "battery",
				imgid : 'battery',
				imgsrc: ""
			},
			{
				imgclass: "garb1",
				imgid : 'garbage1',
				imgsrc: ""
			},
			{
				imgclass: "garb2",
				imgid : 'garbage2',
				imgsrc: ""
			}
		]
	}],
	exerciseblock: [
		{

			exeoptions: [
				{
					forshuffle: "class2",
					optdata: data.string.must,
				},
				{
					forshuffle: "class1",
					optdata: data.string.musnt,
				}],
		}
	]
},
// slide2
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg2",
	uppertextblock:[
		{
			textclass: "battext",
			textdata: data.string.p4text2
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "battery",
				imgid : 'battery',
				imgsrc: ""
			}
		]
	}]
},
// slide3
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "diybg1",
	uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "musmusnt",
			textclass: "battext",
			textdata: data.string.p4q2sent
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "battery",
				imgid : 'battery',
				imgsrc: ""
			},
			{
				imgclass: "sleep",
				imgid : 'sleep',
				imgsrc: ""
			}
		]
	}],
	exerciseblock: [
		{

			exeoptions: [
				{
					forshuffle: "class1",
					optdata: data.string.must,
				},
				{
					forshuffle: "class2",
					optdata: data.string.musnt,
				}],
		}
	]
},
// slide4
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "diybg2",
	uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "musmusnt",
			textclass: "battext",
			textdata: data.string.p4q3sent
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "battery",
				imgid : 'battery',
				imgsrc: ""
			},
			{
				imgclass: "bin",
				imgid : 'bin',
				imgsrc: ""
			}
		]
	}],
	exerciseblock: [
		{

			exeoptions: [
				{
					forshuffle: "class2",
					optdata: data.string.must,
				},
				{
					forshuffle: "class1",
					optdata: data.string.musnt,
				}],
		}
	]
},
// slide5
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "diybg3",
	uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "musmusnt",
			textclass: "battext",
			textdata: data.string.p4q4sent
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "battery",
				imgid : 'battery',
				imgsrc: ""
			}
		]
	}],
	exerciseblock: [
		{

			exeoptions: [
				{
					forshuffle: "class1",
					optdata: data.string.must,
				},
				{
					forshuffle: "class2",
					optdata: data.string.musnt,
				}],
		}
	]
},
// slide6
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "diybg4",
	uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "musmusnt",
			textclass: "battext",
			textdata: data.string.p4q5sent
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "battery",
				imgid : 'battery',
				imgsrc: ""
			},
			{
				imgclass: "twoboys",
				imgid : 'boys1',
				imgsrc: ""
			}
		]
	}],
	exerciseblock: [
		{

			exeoptions: [
				{
					forshuffle: "class1",
					optdata: data.string.must,
				},
				{
					forshuffle: "class2",
					optdata: data.string.musnt,
				}],
		}
	],
	speechbox:[{
		speechbox: 'sp-1',
		textclass: "answer",
		textdata: data.string.p4text5,
		datahighlightflag: true,
		datahighlightcustomclass: 'blank-space',
		imgclass: '',
		imgid : 'tb-2',
		imgsrc: '',
		// audioicon: true,
	}]
},
// slide7
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "diybg6",
	uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "musmusnt",
			textclass: "battext",
			textdata: data.string.p4q6sent
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "battery",
				imgid : 'battery',
				imgsrc: ""
			}
		]
	}],
	exerciseblock: [
		{

			exeoptions: [
				{
					forshuffle: "class1",
					optdata: data.string.must,
				},
				{
					forshuffle: "class2",
					optdata: data.string.musnt,
				}],
		}
	]
},
// slide8
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "diybg7",
	uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "musmusnt",
			textclass: "battext",
			textdata: data.string.p4q7sent
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "battery",
				imgid : 'battery',
				imgsrc: ""
			}
		]
	}],
	exerciseblock: [
		{

			exeoptions: [
				{
					forshuffle: "class1",
					optdata: data.string.must,
				},
				{
					forshuffle: "class2",
					optdata: data.string.musnt,
				}],
		}
	]
},
// slide9
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "theblue",
	uppertextblock:[
		{
			textclass: "uptext",
			textdata: data.string.p4text1
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "batteryfin",
				imgid : 'battery',
				imgsrc: ""
			},
			{
				imgclass: "replay",
				imgid : 'replay',
				imgsrc: ""
			}
		]
	}]
},
// slide10
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "theblue",
	uppertextblock:[
		{
			textclass: "uptext",
			textdata: data.string.p4text3
		},
		{
			textclass: "listtext",
			textdata: data.string.p4text4
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "girlimg",
				imgid : 'girl',
				imgsrc: ""
			},
			{
				imgclass: "boyimg",
				imgid : 'boy',
				imgsrc: ""
			}
		]
	}]
}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	var global_save_val;
	var img_type;
	var batPos = 5;
	var filename;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "diybox", src: imgpath+"diy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy", src: imgpath+"boy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl", src: imgpath+"girl.png", type: createjs.AbstractLoader.IMAGE},
			{id: "battery", src: imgpath+"battery/battery05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "replay", src: "images/replay.png", type: createjs.AbstractLoader.IMAGE},
			{id: "garbage1", src: imgpath+"garbage01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "garbage2", src: imgpath+"garbage02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sleep", src: imgpath+"eyeclosed.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wake", src: imgpath+"eyeopen.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boys1", src: imgpath+"prem-suraj02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boys2", src: imgpath+"prem-suraj01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bin", src: imgpath+"new/dustbin01.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"p4_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p4_s2.ogg"},
			{id: "sound_5", src: soundAsset+"p4_s10.ogg"},
			{id: "sound_a1", src: soundAsset+"p4_s9_0.ogg"},
			{id: "sound_a2", src: soundAsset+"p4_s9_1.ogg"},
			{id: "sound_a3", src: soundAsset+"p4_s9_2.ogg"},
			{id: "sound_a4", src: soundAsset+"p4_s9_3.ogg"},
			{id: "sound_a5", src: soundAsset+"p4_s9_4.ogg"},
			{id: "sound_a6", src: soundAsset+"p4_s9_5.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);

		var ansClicked = false;
		var wrngClicked = false;

		$(".buttonsel").click(function(){
			$(this).removeClass('forhover');
				if(ansClicked == false){

					/*class 1 is always for the right answer. updates scoreboard and disables other click if
					right answer is clicked*/
					if($(this).hasClass("class1")){

						if(wrngClicked == false){
							//testin.update(true);
							if(batPos < 8)
								batPos++;
							$(".battery").attr("src", imgpath + "battery/battery0" + batPos + ".png");

						}
						corr_action();
						play_correct_incorrect_sound(1);
						$(this).css("background","#98c02e");
						$(this).css("border","5px solid #deef3c");
            $(this).css("color","white");
						$(this).siblings(".corctopt").show(0);
						$(".musmusnt").html($(this).text().toLowerCase());
						//$('.hint_image').show(0);
						$('.buttonsel').removeClass('forhover forhoverimg');
						ansClicked = true;

						if(countNext != $total_page)
						navigationcontroller();
					}
					else{
						//testin.update(false);
						play_correct_incorrect_sound(0);
						$(this).css("background","#FF0000");
						$(this).css("border","5px solid #980000");
						$(this).css("color","white");
						$(this).siblings(".wrngopt").show(0);
						console.log(batPos);
						if(batPos != 1)
						batPos--;
						$(".battery").attr("src", imgpath + "battery/battery0" + batPos + ".png");
						wrngClicked = true;
					}
				}
			});
			$(".battery").attr("src", imgpath + "battery/battery0" + batPos + ".png");

			function corr_action(){
					if(countNext == 1){
						$(".garb1").fadeOut(1000);
						$(".garb2").fadeOut(1000);
					}
					else if(countNext == 3){
						$(".sleep").attr("src",imgpath + "eyeopen.png");
					}
					else if(countNext == 6){
						$(".twoboys").attr("src",imgpath + "prem-suraj01.png");
						$(".sp-1").show(0);
					}
			}

		switch(countNext){
			case 0:
			sound_player("sound_1");
			break;
			case 2:
			sound_player("sound_2");
			break;
			case 4:
				$('.class1').click(function(){
					$('.bin').attr("src",imgpath + "new/dustbin02.png").css({'opacity':'0'}).animate({'opacity':'1'},500);
					$(this).css('pointer-events','none');
				});
				break;
			case 9:
			$(".batteryfin").attr("src", imgpath + "battery/battery0" + batPos + ".png");
			$(".uptext").html(eval('data.string.p4feed' + batPos));
			if(batPos == 1){
				sound_player("sound_a1", "no");
				$(".replay").show(0);
			}
			else if(batPos < 4){
				sound_player("sound_a2", "no");
				$(".replay").show(0);
			}
			else if(batPos < 6){
				sound_player("sound_a3", "no");
				$(".replay").show(0);
			}
			else if(batPos == 6){
				sound_player("sound_a4");
			}
			else if(batPos == 7){
				sound_player("sound_a5");
			}
			else{
				sound_player("sound_a6");
			}
			$(".replay").click(function(){
				//alert("kl;s");
				countNext = 0;
				batPos = 5;
				templateCaller();
			});
			break;

			case 10:
			sound_player("sound_5");
			break;

		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next == null)
			nav_button_controls();
		});
	}

	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2);
		});

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
			for (var i = 0; i < content.length; i++) {
				slides(i);
				$($('.totalsequence')[i]).html(i);
				$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
			"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
			}
			function slides(i){
					$($('.totalsequence')[i]).click(function(){
						countNext = i;
						templateCaller();
					});
				}
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		case 0:
		$(".diybox").removeClass("diyrolldown").addClass("diyrollup");
		$(".covertext").removeClass("textrolldown").addClass("textrollup");
		setTimeout(function(){
			countNext++;
			templateCaller();
		}, 1000);
		break;
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

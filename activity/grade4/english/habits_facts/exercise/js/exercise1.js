var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/en/";

var content1=[

	//ex1
	{
		exerciseblock: [
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				textdata: data.string.exins1,
				containerclass: 'container-2',
				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.ex1a,
					},
					{
						forshuffle: "class2",
						optdata: data.string.ex1b,
					},
					{
						forshuffle: "class3",
						optdata: data.string.ex1c,
					},
					{
						forshuffle: "class4",
						optdata: data.string.ex1d,
					}],
			}
		]
	},
	//ex2
	{
		exerciseblock: [
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				textdata: data.string.exins1,
				containerclass: 'container-2',
				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.ex2a,
					},
					{
						forshuffle: "class2",
						optdata: data.string.ex2b,
					},
					{
						forshuffle: "class3",
						optdata: data.string.ex2c,
					},
					{
						forshuffle: "class4",
						optdata: data.string.ex2d,
					}],
			}
		]
	},
	//ex3
	{
		exerciseblock: [
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				textdata: data.string.exins1,
				containerclass: 'container-2',
				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.ex3a,
					},
					{
						forshuffle: "class2",
						optdata: data.string.ex3b,
					},
					{
						forshuffle: "class3",
						optdata: data.string.ex3c,
					},
					{
						forshuffle: "class4",
						optdata: data.string.ex3d,
					}],
			}
		]
	},
	//ex4
	{
		exerciseblock: [
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				textdata: data.string.exins1,
				containerclass: 'container-2',
				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.ex4a,
					},
					{
						forshuffle: "class2",
						optdata: data.string.ex4b,
					},
					{
						forshuffle: "class3",
						optdata: data.string.ex4c,
					},
					{
						forshuffle: "class4",
						optdata: data.string.ex4d,
					}],
			}
		]
	}
];

var content2=[
	// slide1
	{
		exerciseblock: [
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				textdata: data.string.exins2,
				ques_class: 'clock-ques',
				sentdata: data.string.ex5,
				containerclass: 'container-1',
				clockcontainerclass: 'clock-div',
				clock:[{
					clockclass: 'class1',
					hrclass:'hr-7',
					minclass:'hr-12'
				},{
					clockclass: 'class2',
					hrclass:'hr-6',
					minclass:'hr-12'
				},{
					clockclass: 'class3',
					hrclass:'hr-8',
					minclass:'hr-12'
				},{
					clockclass: 'class4',
					hrclass:'hr-9',
					minclass:'hr-12'
				}]
			}
		]
	},
	// slide2
	{
		exerciseblock: [
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				textdata: data.string.exins2,
				ques_class: 'clock-ques',
				sentdata: data.string.ex6,
				containerclass: 'container-1',
				clockcontainerclass: 'clock-div',
				clock:[{
					clockclass: 'class1',
					hrclass:'hr-1-40',
					minclass:'hr-8'
				},{
					clockclass: 'class2',
					hrclass:'hr-2-20',
					minclass:'hr-4'
				},{
					clockclass: 'class3',
					hrclass:'hr-2-30',
					minclass:'hr-6'
				},{
					clockclass: 'class4',
					hrclass:'hr-2',
					minclass:'hr-12'
				}]
			}
		]
	},
	//slide3
	{
		exerciseblock: [
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				textdata: data.string.exins2,
				ques_class: 'clock-ques',
				sentdata: data.string.ex7,
				containerclass: 'container-1',
				clockcontainerclass: 'clock-div',
				clock:[{
					clockclass: 'class1',
					hrclass:'hr-7-15',
					minclass:'hr-3'
				},{
					clockclass: 'class2',
					hrclass:'hr-6-45',
					minclass:'hr-9'
				},{
					clockclass: 'class3',
					hrclass:'hr-7-30',
					minclass:'hr-6'
				},{
					clockclass: 'class4',
					hrclass:'hr-7',
					minclass:'hr-12'
				}]
			}
		]
	}
];

var content3=[
	//ex1
	{
		exerciseblock: [
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				textdata: data.string.exins3,
				containerclass: 'container-2',
				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.ex8a,
					},
					{
						forshuffle: "class2",
						optdata: data.string.ex8b,
					},
					{
						forshuffle: "class3",
						optdata: data.string.ex8c,
					},
					{
						forshuffle: "class4",
						optdata: data.string.ex8d,
					}],
			}
		]
	},
	//ex2
	{
		exerciseblock: [
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				textdata: data.string.exins3,
				containerclass: 'container-2',
				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.ex9a,
					},
					{
						forshuffle: "class2",
						optdata: data.string.ex9b,
					},
					{
						forshuffle: "class3",
						optdata: data.string.ex9c,
					},
					{
						forshuffle: "class4",
						optdata: data.string.ex9d,
					}],
			}
		]
	},
	//ex3
	{
		exerciseblock: [
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				textdata: data.string.exins3,
				containerclass: 'container-2',
				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.ex10a,
					},
					{
						forshuffle: "class2",
						optdata: data.string.ex10b,
					},
					{
						forshuffle: "class3",
						optdata: data.string.ex10c,
					},
					{
						forshuffle: "class4",
						optdata: data.string.ex10d,
					}],
			}
		]
	},
];
/*remove this for non random questions*/
content1.shufflearray();
content2.shufflearray();
content3.shufflearray();

var content = content1.concat(content2);
content = content.concat(content3);

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;


	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	var scoring = new EggTemplate();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "body", src: imgpath+"clock2/clock.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hrhand", src: imgpath+"clock2/hr_hand.png", type: createjs.AbstractLoader.IMAGE},
			{id: "minhand", src: imgpath+"clock2/min_hand.png", type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: "images/wrong.png", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: "images/wrong.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "ex_instrn", src: soundAsset+"ex_instrn.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		scoring.init($total_page);
		templateCaller();
	}
	//initialize
	init();

	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	/*=====  End of data highlight function  ======*/

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templatecaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templatecaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
	/**
	 How to:
	 - Just call instructionblockcontroller() from the template
	 */


	/**
	 What it does:
	 - It inserts and handles closing and opening of instruction block
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		texthighlight($board);

		put_image(content, countNext);
		put_image2(content, countNext);
		$('.correct-icon').attr('src', preload.getResult('correct').src);
		$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);

		scoring.numberOfQuestions();
		countNext==0?sound_player("ex_instrn"):'';
		/*for randomizing the options*/
		var parent = $(".optionsdiv");
		var divs = parent.children();
		while (divs.length) {
			parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
		var ansClicked = false;
		var wrngClicked = false;
		var alphs = ["a) ", "b) ", "c) ", "d) "];
		var alcount = 0;
		$(".optionsdiv").children().children(".forhover").each(function() {
			$(this).prepend(alphs[alcount]);
			alcount++;
		});

		$('.correct-answer').attr('src', preload.getResult('correct').src);
		$('.incorrect-answer').attr('src', preload.getResult('incorrect').src);
		$('.clock-body').attr('src', preload.getResult('body').src);
		$('.hr-hand').attr('src', preload.getResult('hrhand').src);
		$('.min-hand').attr('src', preload.getResult('minhand').src);

		$(".buttonsel").click(function(){
			createjs.Sound.stop();
			$(this).removeClass('forhover');
				if(ansClicked == false){
					/*class 1 is always for the right answer. updates scoreboard and disables other click if
					right answer is clicked*/
					if($(this).hasClass("class1")){
						if(wrngClicked == false){
							scoring.update(true);
						}
						play_correct_incorrect_sound(1);
						$(this).css({
							"background": "#bed62fff",
							"border":"5px solid #deef3c",
							"color":"white",
							'pointer-events': 'none'
						});
						$(this).siblings(".correct-answer").show(0);
						$(this).children(".correct-answer").show(0);
						$('.buttonsel').removeClass('forhover forhoverimg');
						$('.buttonsel').removeClass('clock-hover');
						ansClicked = true;
						if(countNext != $total_page)
						$nextBtn.show(0);
					}
					else{
						scoring.update(false);
						play_correct_incorrect_sound(0);
						$(this).css({
							"background":"#FF0000",
							"border":"5px solid #980000",
							"color":"white",
							'pointer-events': 'none'
						});
						$(this).siblings(".incorrect-answer").show(0);
						$(this).children(".incorrect-answer").show(0);
						wrngClicked = true;
					}
				}
			});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('popupblock')){
			var imageClass = content[count].popupblock;
			for(var i=0; i<imageClass.length; i++){
				var image_src = preload.getResult(imageClass[i].imgid).src;
				//get list of classes
				var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();
		generalTemplate();
		// scoringg purpose code ends
		loadTimelineProgress($total_page,countNext+1);
	}

	$nextBtn.on('click', function() {
		countNext++;
		scoring.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
});

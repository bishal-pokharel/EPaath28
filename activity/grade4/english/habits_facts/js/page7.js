var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/en/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		extratextblock:[{
			textdata: data.string.p7text1,
			textclass: "top-text my_font_big comingsoon",
		}],
		uppertextblockadditionalclass: 'text-story',
		uppertextblock:[{
			textdata: data.string.p7text6,
			textclass: "",
		},{
			textdata: data.string.p7text7,
			textclass: "",
		},{
			textdata: data.string.p7text8,
			textclass: "",
		},{
			textdata: data.string.p7text9,
			textclass: "",
		},{
			textdata: data.string.p7text10,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "girl-1",
					imgsrc : '',
					imgid : 'girl-1'
				},
				{
					imgclass : "s-img img-1",
					imgsrc : '',
					imgid : 'paste'
				},
				{
					imgclass : "s-img img-2",
					imgsrc : '',
					imgid : 'wash'
				},
				{
					imgclass : "s-img img-3",
					imgsrc : '',
					imgid : 'playing'
				},
				{
					imgclass : "s-img img-4",
					imgsrc : '',
					imgid : 'bed'
				}
			]
		}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		extratextblock:[{
			textdata: data.string.p7text2,
			textclass: "top-text my_font_big comingsoon",
		},{
			textdata: data.string.p7text3,
			textclass: "btn btn-1",
		},{
			textdata: data.string.p7text4,
			textclass: "btn btn-2",
		},{
			textdata: data.string.p7text5,
			textclass: "btn btn-3",
		}],
		uppertextblockadditionalclass: 'text-story-2',
		uppertextblock:[{
			textdata: data.string.p7text6,
			textclass: "",
		},{
			textdata: data.string.p7text7,
			textclass: "",
		},{
			textdata: data.string.p7text8,
			textclass: "",
		},{
			textdata: data.string.p7text9,
			textclass: "",
		},{
			textdata: data.string.p7text10,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "girl-1",
					imgsrc : '',
					imgid : 'girl-1'
				}
			]
		}]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "girl-1", src: imgpath+"rubi.png", type: createjs.AbstractLoader.IMAGE},
			{id: "playing", src: imgpath+"playing2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "paste", src: imgpath+"toothpaste.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bed", src: imgpath+"bed2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wash", src: imgpath+"page1/hand-washing-gif-file-transparent.gif", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s7_p1", src: soundAsset+"s7_p1.ogg"},
			{id: "s7_p1_1", src: soundAsset+"s7_p1_1.ogg"},
			{id: "s7_p2", src: soundAsset+"s7_p2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
    vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_card_image(content, countNext);
		switch(countNext) {
			case 0:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s7_p1");
				current_sound.play();
				current_sound.on('complete', function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s7_p1_1");
					current_sound.play();
					current_sound.on('complete', function(){
						nav_button_controls(100);
					});
				});
			break;
			case 1:
				sound_player("s7_p2",0);
				$prevBtn.show(0);
				var allClicked = [0,0,0];
				$('.btn-1').click(function(){
					$('.btn').removeClass('selected-class');
					$('.facts').css('background-color', '');
					$('.habit').css('background-color', '');
					$('.hbt').css('background-color', '');
					if(!$(this).hasClass('selected-class')){
						$(this).addClass('selected-class');
						$('.facts').css('background-color', '#ADEBFB');
						allClicked[0] = 1;
						check_allClicked();
					}
				});
				$('.btn-2').click(function(){
					$('.btn').removeClass('selected-class');
					$('.facts').css('background-color', '');
					$('.habit').css('background-color', '');
					$('.hbt').css('background-color', '');
					if(!$(this).hasClass('selected-class')){
						$(this).addClass('selected-class');
						$('.habit').css('background-color', '#EA8B88');
						allClicked[1] = 1;
						check_allClicked();
					}
				});
				$('.btn-3').click(function(){
					$('.btn').removeClass('selected-class');
					$('.facts').css('background-color', '');
					$('.habit').css('background-color', '');
					$('.hbt').css('background-color', '');
					if(!$(this).hasClass('selected-class')){
						$(this).addClass('selected-class');
						$('.hbt').css('background-color', '#FFDB5C');
						allClicked[2] = 1;
						check_allClicked();
					}
				});
				function check_allClicked(){
					for(var i=0; i<3; i++){
						if(allClicked[i]==0){
							return false;
						}
					}
					ole.footerNotificationHandler.pageEndSetNotification();
				}
				break;
			default:
				$prevBtn.show(0);
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls();
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_card_image(content, count){
		if(content[count].hasOwnProperty('card')){
			var card = content[count].card;
			for(var i=0; i<card.length; i++){
				var image_src = preload.getResult(card[i].imgid).src;
				console.log(image_src);
				var classes_list = card[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

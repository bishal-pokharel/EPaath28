var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/en/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		extratextblock:[{
			textdata:  '',
			textclass: "diy-rect",
		},{
			textdata:  data.string.p2text1,
			textclass: "diy-text sniglet",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "diy-boy",
					imgsrc : '',
					imgid : 'boy-1'
				},
			]
		}],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-puzzle',
		puzzlecontainer: true,
		extratextblock:[{
			textdata:  data.string.p2text2,
			textclass: "instruction my_font_medium sniglet",
		}],
		uppertextblockadditionalclass: 'word-list my_font_medium sniglet',
		uppertextblock:[{
			textdata:  data.string.p2text3,
			textclass: "word-1",
		},{
			textdata:  data.string.p2text4,
			textclass: "word-2",
		},{
			textdata:  data.string.p2text5,
			textclass: "word-3",
		},{
			textdata:  data.string.p2text6,
			textclass: "word-4",
		},{
			textdata:  data.string.p2text7,
			textclass: "word-5",
		},{
			textdata:  data.string.p2text8,
			textclass: "word-6",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "puzzle-boy",
					imgsrc : '',
					imgid : 'boy-2'
				},
			]
		}],
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;

    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "boy-1", src: imgpath+"blue-boy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy-2", src: imgpath+"blue-boy-1.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes

			// sounds
			{id: "s2_p2", src: soundAsset+"s2_p2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		// createjs.Sound.play('para-1');
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
    vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_poem_image(content, countNext);
		switch(countNext) {
			case 0:
			//	$nextBtn.show(0);
				play_diy_audio();
				nav_button_controls(2000)
				break;
			case 1:
			//	$prevBtn.show(0);
        // nav_button_controls(60000)
				sound_player("s2_p2",0);
				var count = 0;
				var answers = [ "SHARING", "WASHINGHANDS", "BRUSHING", "QUEUEING", "READING", "EXERCISING" ];
				var ans_idx = [1,2,3,4,5,6];
				var total_ans = answers.length;
				var is_click = false;
				var startpt, endpt;
				var start_y, start_x;
				var wordtext = '';
				var direction = 1;
				var orientation = 1;//0 means vertical

				function get_row(id){
					return parseInt(id.split(/-/)[0]);
				}
				function get_col(id){
					return parseInt(id.split(/-/)[1]);
				}
				$('.letter-box').mousedown(function(){
					is_click = true;
					startpt = $(this).attr('id');
					start_y = get_row(startpt);
					start_x = get_col(startpt);
					direction = 1;//reset direction
					$('.letter-box').removeClass('current-hover');
				});

				$('.letter-box').mouseover(function(){
					if(is_click){
						endpt = $(this).attr('id');
						var end_y = get_row(endpt);
						var end_x = get_col(endpt);
						var diff_x = Math.abs(end_x-start_x);
						var diff_y = Math.abs(end_y-start_y);
						wordtext = '';
						if(diff_x>diff_y){
							orientation = 1;
							var smaller = start_x;
							var larger =  end_x;
							direction = 1;
							if(start_x>end_x){
								smaller = end_x;
								larger = start_x;
								direction = 0;
							}
							$('.letter-box').removeClass('current-hover');
							for(var i=smaller; i<=larger; i++){
								$('#'+start_y+'-'+i).addClass('current-hover');
								wordtext += $('#'+start_y+'-'+i+'>p').html();
							}
						} else{
							orientation = 0;
							var smaller = start_y;
							var larger =  end_y;
							direction = 1;
							if(start_y>end_y){
								smaller = end_y;
								larger = start_y;
								direction = 0;
							}
							$('.letter-box').removeClass('current-hover');
							for(var i=smaller; i<=larger; i++){
								$('#'+i+'-'+start_x).addClass('current-hover');
								wordtext += $('#'+i+'-'+start_x+'>p').html();
							}
						}
					}
				});
				$('.container').mouseleave(function(){
					is_click=false;
				});
				$('.letter-box').mouseup(function(){
					if(is_click){
						if(direction==0){
							wordtext = reverseString(wordtext);
						}
						var idx = null;
						for(var i=0; i<answers.length; i++){
							if(wordtext===answers[i]){
								idx = i;
								var $thisclass = $('.current-hover');
								if(orientation==0){
									$('.current-hover').addClass('done-answer');
									setTimeout(function(){
										$thisclass.removeClass('done-answer');
									}, 600);
								} else{
									$('.current-hover').addClass('done-answer-1');
									setTimeout(function(){
										$thisclass.removeClass('done-answer-1');
									}, 600);
								}
								$('.current-hover').addClass('ans-present');
								$('.letter-box').removeClass('current-hover');
								$('.word-'+ans_idx[i]).append(' &#x2714;');
								$('.word-'+ans_idx[i]).addClass('found-word');
								count++;
								createjs.Sound.play('sound_1');
								if(count==total_ans){
									ole.footerNotificationHandler.pageEndSetNotification();
								}
							}
						}
						is_click = false;
						if(idx!=null){
							answers.splice(idx, 1);
							ans_idx.splice(idx, 1);
						}
						console.log(answers);
					}
				});
				function reverseString(str) {
					return str.split("").reverse().join("");
				}
				break;
			default:
				$prevBtn.show(0);
				// createjs.Sound.play('sound_3');
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls(100):"";
		});
	}
	function sound_play_click(sound_id, click_class){
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_poem_image(content, count){
		if(content[count].hasOwnProperty('poemimage')){
			var imageblock = content[count].poemimage[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

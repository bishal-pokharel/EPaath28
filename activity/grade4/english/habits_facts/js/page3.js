var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/en/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-green',

		extratextblock:[{
			textdata:  '',
			textclass: "diy-rect",
		},{
			textdata:  data.string.p3text1,
			textclass: "diy-text sniglet",
		}],
		imageblockclass: 'image-diyblock',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "diy-boy",
					imgsrc : '',
					imgid : 'boy-1'
				},
			]
		}],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		extratextblock:[{
			textdata: '',
			textclass: "bg_inside",
		},{
			textdata:  data.string.p3text2,
			textclass: "diy-instruction my_font_very_big sniglet",
		}],
		imageblockclass: 'wheelblock',
		pointerclass: 'pointer',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "disc",
					imgsrc : '',
					imgid : 'disc'
				},
				{
					imgclass : "click-img",
					imgsrc : '',
					imgid : 'click-img'
				},
				{
					imgclass : "spinimg-1",
					imgsrc : '',
					imgid : 'teeth'
				},
				{
					imgclass : "spinimg-2",
					imgsrc : '',
					imgid : 'skipping'
				},
				{
					imgclass : "spinimg-3",
					imgsrc : '',
					imgid : 'washing'
				},
				{
					imgclass : "spinimg-4",
					imgsrc : '',
					imgid : 'stretching'
				},
				{
					imgclass : "spinimg-5",
					imgsrc : '',
					imgid : 'bed'
				},
				{
					imgclass : "spinimg-6",
					imgsrc : '',
					imgid : 'nail-clip'
				},
				{
					imgclass : "spinimg-7",
					imgsrc : '',
					imgid : 'friends'
				},
				{
					imgclass : "spinimg-8",
					imgsrc : '',
					imgid : 'climb'
				}
			]
		}],
		fillblock:[{
			instructionclass: 'sniglet my_font_big',
			instructiondata: data.string.p3text3,
		}]
	},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;

    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "boy-1", src: imgpath+"green-boy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bed", src: imgpath+"bed.png", type: createjs.AbstractLoader.IMAGE},
			{id: "climb", src: imgpath+"helping-eachother.png", type: createjs.AbstractLoader.IMAGE},
			{id: "friends", src: imgpath+"threekids.png", type: createjs.AbstractLoader.IMAGE},
			{id: "nail-clip", src: imgpath+"cutting-nails.png", type: createjs.AbstractLoader.IMAGE},
			{id: "skipping", src: imgpath+"playing.png", type: createjs.AbstractLoader.IMAGE},
			{id: "stretching", src: imgpath+"yoga.png", type: createjs.AbstractLoader.IMAGE},
			{id: "washing", src: imgpath+"washing-hand.png", type: createjs.AbstractLoader.IMAGE},
			{id: "disc", src: imgpath+"wheel_empty.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pointer", src: imgpath+"pointer.png", type: createjs.AbstractLoader.IMAGE},
			{id: "teeth", src: imgpath+"brushingteeth01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "click-img", src: imgpath+"click.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes

			// sounds
			// {id: "sound_correct", src: soundAsset+"alert_correct.ogg"},
			// {id: "sound_1", src: soundAsset+"curtain.ogg"},
			{id: "s3_p2", src: soundAsset+"s3_p2.ogg"},
			{id: "s3_p2_1", src: soundAsset+"s3_p2_1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
    vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_poem_image(content, countNext);
		switch(countNext) {
			case 0:
				play_diy_audio();
				nav_button_controls(2000);
			break;
			case 1:
				sound_player("s3_p2",0);
				var counter = 0;
				var rand_array = [1,2,3,4,5,6,7,8];
				rand_array.shufflearray();
				var rand_num = 0;
				$('.pointer').attr('src',preload.getResult('pointer').src);
				var blanks_text = ['BRUSHING TEETH', 'SHARING TOYS', 'WASHING HANDS', 'EXERCISING', 'WAKING UP EARLY', 'CUTTING NAILS', 'QUEING', 'HELPING OTHERS'];
				var images_src = ['teeth', 'skipping', 'washing', 'stretching', 'bed', 'nail-clip', 'friends', 'climb'];

				var current_text;

				$('.container').on('click', '.alphabet-select', function(){
                    $curr_class = $(this);
					var current_char = $(this).text().replace(/\s/g, '');
					for (var i=0; i<current_text.length; i++){
						if(current_char.charCodeAt(0) == current_text[i].charCodeAt(0)){
							$curr_class.addClass('fade-out-alphabet');
						} else{
							$curr_class.addClass('done-select');
						}
					}
					$('.textclass-'+current_char+'>p').fadeIn(500);
					var replace = new RegExp(current_char,"g");
					current_text = current_text.replace(replace, '');
					console.log(typeof(current_text.match(/\S/g)));
					if(current_text.match(/\S/g) == null){
						play_correct_incorrect_sound(1);
						$('.ss-close-btn').show(0);
						$('.alphabet-select').css('pointer-events', 'none');
					}
				});
				$('.ss-close-btn').click(function(){
					$('.container').fadeOut(500, function(){
						$('.wheelblock>*:not(.has-been-selected').removeClass('go-gray').addClass('not-go-gray');
						$('.wheelblock').css('pointer-events', 'all');
						$('.container').css({
							'width': '0%',
							'height': '0%'
						});
						$('.coverboardfull>*:not(.container)').css({'filter': '','-webkit-filter': ''});
					});
					counter++;
					if(counter==8){
						$('.wheelblock').css('pointer-events', 'none');
						ole.footerNotificationHandler.pageEndSetNotification();
					}
				});
				$('.click-img').click(function(){
					$('.wheelblock').css('pointer-events', 'none');
					$('.wheelblock').removeClass('wheel-rotate-'+rand_num);

					rand_num = rand_array[counter];
					current_text = blanks_text[rand_num-1];
					$('.wheelblock').addClass('wheel-rotate-'+rand_num);
					$('.game-img').attr('src', preload.getResult(images_src[rand_num-1]).src);

					$('.filler').html('');
					$('.alphabets').html('');
					$('.ss-close-btn').hide(0);

					for (var i=0; i<current_text.length; i++){
						if(current_text[i] != ' '){
							$('.filler').append('<div class="not-blank textclass-'+current_text[i]+'"> <p>' + current_text[i] + '</p></div>');
						} else{
							$('.filler').append('<div class="blank"> <p></p> </div>');
						}
					}
					current_text = current_text.replace(' ', '');

					for (var i = 65; i <= 90; i++) {
						$('.alphabets').append('<div class="alphabet-select"> <p>' + String.fromCharCode(i) + '</p></div>');
					}
					timeoutvar = setTimeout(function(){
						$('.wheelblock>*:not(.spinimg-'+rand_num+')').addClass('go-gray');
						$('.spinimg-'+rand_num).addClass('has-been-selected');
						setTimeout(function(){
                            counter==0?sound_player("s3_p2_1"):"";
							$('.container').show(0);
							$('.container').animate({
								'width': '82%',
								'height': '70%'
							}, 500, function(){
								$('.coverboardfull>*:not(.container)').css({'filter': 'brightness(50%)','-webkit-filter': 'brightness(50%)'});
							});
						}, 1000);
					}, 6000);
				});
				//
				break;
			default:
				$prevBtn.show(0);
				$nextBtn.show(0);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls(100):"";
		});
	}
	function sound_play_click(sound_id, click_class){
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_poem_image(content, count){
		if(content[count].hasOwnProperty('poemimage')){
			var imageblock = content[count].poemimage[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}


	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

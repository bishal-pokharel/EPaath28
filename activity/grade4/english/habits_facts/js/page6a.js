var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-orange',

		extratextblock:[{
			textdata:  '',
			textclass: "diy-rect",
		},{
			textdata:  data.string.diytext,
			textclass: "diy-text sniglet",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "diy-girl",
					imgsrc : '',
					imgid : 'girl-1'
				}
			]
		}],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-p1',

		extratextblock:[{
			textdata:  data.string.p6text19,
			textclass: "instruction ",
		},{
			textdata:  data.string.p6text20,
			textclass: "sentence my_font_medium ",
		},
		{
			textdata:  data.string.p6text17,
			textclass: "option option-1 correct",
		},
		{
			textdata:  data.string.p6text18,
			textclass: "option option-2 ",
		},
		],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "diy-img",
					imgsrc : '',
					imgid : 'img-1'
				},
				{
					imgclass : "cor-incor-1",
					imgsrc : '',
					imgid : 'correct'
				},
				{
					imgclass : "cor-incor-2",
					imgsrc : '',
					imgid : 'correct'
				}
			]
		}]
	},

	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-p1',

		extratextblock:[{
			textdata:  data.string.p6text19,
			textclass: "instruction ",
		},{
			textdata:  data.string.p6text21,
			textclass: "sentence my_font_medium ",
		},
		{
			textdata:  data.string.p6text17,
			textclass: "option option-1 ",
		},
		{
			textdata:  data.string.p6text18,
			textclass: "option option-2 correct",
		},
		],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "diy-img",
					imgsrc : '',
					imgid : 'img-2'
				},
				{
					imgclass : "cor-incor-1",
					imgsrc : '',
					imgid : 'correct'
				},
				{
					imgclass : "cor-incor-2",
					imgsrc : '',
					imgid : 'correct'
				}
			]
		}]
	},

	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-p1',

		extratextblock:[{
			textdata:  data.string.p6text19,
			textclass: "instruction ",
		},{
			textdata:  data.string.p6text22,
			textclass: "sentence my_font_medium ",
		},
		{
			textdata:  data.string.p6text17,
			textclass: "option option-1 correct",
		},
		{
			textdata:  data.string.p6text18,
			textclass: "option option-2 ",
		},
		],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "diy-img",
					imgsrc : '',
					imgid : 'img-3'
				},
				{
					imgclass : "cor-incor-1",
					imgsrc : '',
					imgid : 'correct'
				},
				{
					imgclass : "cor-incor-2",
					imgsrc : '',
					imgid : 'correct'
				}
			]
		}]
	},

	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-p1',

		extratextblock:[{
			textdata:  data.string.p6text19,
			textclass: "instruction ",
		},{
			textdata:  data.string.p6text23,
			textclass: "sentence my_font_medium ",
		},
		{
			textdata:  data.string.p6text17,
			textclass: "option option-1 ",
		},
		{
			textdata:  data.string.p6text18,
			textclass: "option option-2 correct",
		},
		],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "diy-img",
					imgsrc : '',
					imgid : 'img-4'
				},
				{
					imgclass : "cor-incor-1",
					imgsrc : '',
					imgid : 'correct'
				},
				{
					imgclass : "cor-incor-2",
					imgsrc : '',
					imgid : 'correct'
				}
			]
		}]
	},

	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-p1',

		extratextblock:[{
			textdata:  data.string.p6text19,
			textclass: "instruction ",
		},{
			textdata:  data.string.p6text24,
			textclass: "sentence my_font_medium ",
		},
		{
			textdata:  data.string.p6text17,
			textclass: "option option-1 correct",
		},
		{
			textdata:  data.string.p6text18,
			textclass: "option option-2 ",
		},
		],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "diy-img",
					imgsrc : '',
					imgid : 'img-5'
				},
				{
					imgclass : "cor-incor-1",
					imgsrc : '',
					imgid : 'correct'
				},
				{
					imgclass : "cor-incor-2",
					imgsrc : '',
					imgid : 'correct'
				}
			]
		}]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "girl-1", src: imgpath+"page6/anjana01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy-1", src: imgpath+"sagar02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-1", src: imgpath+"page6/bg_new.png", type: createjs.AbstractLoader.IMAGE},

			{id: "img-1", src: imgpath+"page6/eating_grass.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-2", src: imgpath+"page6/monkey.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-3", src: imgpath+"page6/giraffes-01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-4", src: imgpath+"page6/elephant.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-5", src: imgpath+"page6/tiger.png", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: "images/wrong.png", type: createjs.AbstractLoader.IMAGE},

			{id: "tb-1", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-2", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"alert_correct.ogg"},
			{id: "sound_2", src: soundAsset+"curtain.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
        vocabcontroller.findwords(countNext);

		switch(countNext) {
			case 0:
				$nextBtn.show(0);
				break;
			default:
				$prevBtn.css('display', 'none');
				$('.option').click(function(){
					if( $(this).hasClass('correct') ){
						$('.option').css('pointer-events', 'none');
						play_correct_incorrect_sound(1);
						if($(this).hasClass('option-1')){
							$('.cor-incor-1').show(0);
						} else{
							$('.cor-incor-2').show(0);
						}
						nav_button_controls(0);
					} else{
						$(this).css('pointer-events', 'none');
						play_correct_incorrect_sound(0);
						if($(this).hasClass('option-1')){
							$('.cor-incor-1').show(0);
							$('.cor-incor-1').attr('src', preload.getResult('incorrect').src);
						} else{
							$('.cor-incor-2').show(0);
							$('.cor-incor-2').attr('src', preload.getResult('incorrect').src);
						}
						nav_button_controls(0);
					}
				});
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}
	//change_src-> put 1 for girl speak first else 2 for boy-speak first, 3 for boy-3
	function conversation(class1, sound_data1, class2, sound_data2){
		$(class1).fadeIn(500, function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_data1);
			current_sound.play();
			current_sound.on("complete", function(){
				$(class2).fadeIn(500, function(){
					current_sound = createjs.Sound.play(sound_data2);
					current_sound.play();
					current_sound.on("complete", function(){
						nav_button_controls(0);
						$(class1).click(function(){
							sound_player(sound_data1);
						});
						$(class2).click(function(){
							sound_player(sound_data2);
						});
					});
				});
			});
		});
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

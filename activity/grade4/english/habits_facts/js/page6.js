var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/en/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-p1',

		extratextblock:[{
			textdata:  data.string.p6text1,
			textclass: "title-1 ",
		},
		{
			textdata:  data.string.p6text2,
			textclass: "text-1 my_font_very_big",
			datahighlightflag : true,
			datahighlightcustomclass : 'ul',
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "boy-1",
					imgsrc : '',
					imgid : 'boy-1'
				}
			]
		}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-p1',

		extratextblock:[{
			textdata:  data.string.p6text3,
			textclass: "example text-blue",
		},
		{
			textdata:  data.string.p6text7,
			textclass: "text-3 my_font_big text-blue",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-3'
				}
			]
		}]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		extratextblock:[{
			textdata:  data.string.p6text3,
			textclass: "example ",
		},{
			textdata:  data.string.p6text6a,
			textclass: "bt-hf ",
		}],
		speechbox:[{
			speechbox: 'speeches-p4 sp-1',
			textdata : data.string.p6text5,
			imgclass: '',
			textclass : '',
			imgid : 'tb-1',
			imgsrc: '',
			// audioicon: true,
		},
		{
			speechbox: 'speeches-p4 sp-2',
			textdata : data.string.p6text6,
			imgclass: 'flipped-h',
			textclass : '',
			imgid : 'tb-1',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-2'
				}
			]
		}]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-p1',

		extratextblock:[{
			textdata:  data.string.p6text3,
			textclass: "example ",
		},
		{
			textdata:  data.string.p6text4,
			textclass: "text-2 my_font_very_big",
		},{
			textdata:  data.string.p6text6a,
			textclass: "bt-hf2 ",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-1'
				}
			]
		}]
	},
	// slide4-0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-p1',

		extratextblock:[{
			textdata:  data.string.p6extra1,
			textclass: "title-1 ",
		},
		{
			textdata:  data.string.p6extra2,
			textclass: "text-1 my_font_big",
			datahighlightflag : true,
			datahighlightcustomclass : 'ul',
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "boy-1",
					imgsrc : '',
					imgid : 'boy-1'
				}
			]
		}]
	},
	// slide4-1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-p1',

		extratextblock:[
		{
			textdata:  data.string.p6text8,
			textclass: "text-4 my_font_big",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-4'
				}
			]
		}]
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		speechbox:[{
			speechbox: 'speeches-p4 sp-3',
			textdata : data.string.p6text9,
			imgclass: 'flipped-h',
			textclass : '',
			imgid : 'tb-3',
			imgsrc: '',
			// audioicon: true,
		},
		{
			speechbox: 'speeches-p4 sp-4',
			textdata : data.string.p6text10,
			imgclass: 'flipped-h',
			textclass : '',
			imgid : 'tb-1',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-5'
				}
			]
		}]
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		extratextblock:[
		{
			textdata:  data.string.p6text11,
			textclass: "text-click my_font_medium click-1",
		}],
		speechbox:[{
			speechbox: 'sp-common speech_2 sp-5',
			textdata : data.string.p6text12,
			imgclass: '',
			textclass : '',
			imgid : 'tb-3',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-6'
				},
				{
					imgclass : "animal monkey",
					imgsrc : '',
					imgid : 'monkey'
				},
				{
					imgclass : "girl-1",
					imgsrc : '',
					imgid : 'girl-1'
				}
			]
		}]
	},
	// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		extratextblock:[
		{
			textdata:  data.string.p6text11,
			textclass: "text-click my_font_medium click-2",
		}],
		speechbox:[{
			speechbox: 'sp-common speech_2  sp-6',
			textdata : data.string.p6text13,
			imgclass: '',
			textclass : '',
			imgid : 'tb-3',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-7'
				},
				{
					imgclass : "animal elephant",
					imgsrc : '',
					imgid : 'elephant'
				},
				{
					imgclass : "girl-2",
					imgsrc : '',
					imgid : 'girl-4'
				}
			]
		}]
	},
	// slide8
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		extratextblock:[
		{
			textdata:  data.string.p6text11,
			textclass: "text-click my_font_medium click-2",
		}],
		speechbox:[{
			speechbox: 'sp-common speech_2  sp-7',
			textdata : data.string.p6text14,
			imgclass: 'flipped-h',
			textclass : '',
			imgid : 'tb-3',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-8'
				},
				{
					imgclass : "animal cow",
					imgsrc : '',
					imgid : 'cow'
				},
				{
					imgclass : "girl-3",
					imgsrc : '',
					imgid : 'girl-2'
				}
			]
		}]
	},
	// slide9
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		extratextblock:[
		{
			textdata:  data.string.p6text11,
			textclass: "text-click my_font_medium click-2",
		}],
		speechbox:[{
			speechbox: 'sp-common speech_2  sp-8',
			textdata : data.string.p6text15,
			imgclass: 'flipped-h',
			textclass : '',
			imgid : 'tb-3',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-9'
				},
				{
					imgclass : "animal giraffe",
					imgsrc : '',
					imgid : 'giraffe'
				},
				{
					imgclass : "grass-1",
					imgsrc : '',
					imgid : 'grass-1'
				},
				{
					imgclass : "girl-4",
					imgsrc : '',
					imgid : 'girl-5'
				}
			]
		}]
	},
	// slide10
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		extratextblock:[
		{
			textdata:  data.string.p6text11,
			textclass: "text-click my_font_medium click-1",
		}],
		speechbox:[{
			speechbox: 'sp-common speech_2  sp-9',
			textdata : data.string.p6text16,
			imgclass: '',
			textclass : '',
			imgid : 'tb-3',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-10'
				},
				{
					imgclass : "animal tiger",
					imgsrc : '',
					imgid : 'tiger'
				},
				{
					imgclass : "grass-1",
					imgsrc : '',
					imgid : 'grass-2'
				},
				{
					imgclass : "girl-5",
					imgsrc : '',
					imgid : 'girl-2'
				}
			]
		}]
	},
	// slide11
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		extratextblock:[
		{
			textdata:  data.string.p6extra3,
			textclass: "text-last my_font_big",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-11'
				}
			]
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "boy-1", src: imgpath+"prem01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-1", src: imgpath+"page6/goingschool.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-2", src: imgpath+"page6/palpa.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-3", src: imgpath+"page6/dolphins.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-4", src: imgpath+"page6/zoo.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-5", src: imgpath+"page6/suresh.png", type: createjs.AbstractLoader.IMAGE},


			{id: "bg-6", src: imgpath+"page6/bg_for_monkey.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-7", src: imgpath+"page6/bg_for_elephant.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-8", src: imgpath+"page6/bg_for_cow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-9", src: imgpath+"page6/bg_for_giraffes.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-10", src: imgpath+"page6/bg_for_tiger.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-11", src: imgpath+"page6/bg_page62.png", type: createjs.AbstractLoader.IMAGE},

			{id: "monkey", src: imgpath+"page6/monkey.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cow", src: imgpath+"page6/eating_grass.png", type: createjs.AbstractLoader.IMAGE},
			{id: "elephant", src: imgpath+"page6/elephant.png", type: createjs.AbstractLoader.IMAGE},
			{id: "giraffe", src: imgpath+"page6/giraffes.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tiger", src: imgpath+"page6/tiger.png", type: createjs.AbstractLoader.IMAGE},

			{id: "grass-1", src: imgpath+"page6/grass_giraffes.png", type: createjs.AbstractLoader.IMAGE},
			{id: "grass-2", src: imgpath+"page6/grass_01.png", type: createjs.AbstractLoader.IMAGE},


			{id: "girl-1", src: imgpath+"page6/anjana01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-2", src: imgpath+"page6/anjana02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-3", src: imgpath+"page6/anjana03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-4", src: imgpath+"page6/anjana04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-5", src: imgpath+"page6/anjana05.png", type: createjs.AbstractLoader.IMAGE},

			{id: "tb-1", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-2", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-3", src: 'images/textbox/white/lb-1-b.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			// {id: "sound_1", src: soundAsset+"alert_correct.ogg"},
			// {id: "sound_2", src: soundAsset+"curtain.ogg"},
			{id: "s6_p1", src: soundAsset+"s6_p1.ogg"},
			{id: "s6_p2", src: soundAsset+"s6_p2.ogg"},
			{id: "s6_p3", src: soundAsset+"s6_p3.ogg"},
			{id: "s6_p3_1", src: soundAsset+"s6_p3_1.ogg"},
			{id: "s6_p3_2", src: soundAsset+"s6_p3_2.ogg"},
			{id: "s6_p4", src: soundAsset+"s6_p4.ogg"},
			{id: "s6_p5", src: soundAsset+"s6_p5.ogg"},
			{id: "s6_p6", src: soundAsset+"s6_p6.ogg"},
			{id: "s6_p7", src: soundAsset+"s6_p7.ogg"},
			{id: "s6_p7_1", src: soundAsset+"s6_p7_1.ogg"},
			{id: "s6_p8", src: soundAsset+"s6_p8.ogg"},
			{id: "s6_p8_1", src: soundAsset+"s6_p8_1.ogg"},
			{id: "s6_p9", src: soundAsset+"s6_p9.ogg"},
			{id: "s6_p10", src: soundAsset+"s6_p10.ogg"},
			{id: "s6_p11", src: soundAsset+"s6_p11.ogg"},
			{id: "s6_p12", src: soundAsset+"s6_p12.ogg"},
			{id: "s6_p13", src: soundAsset+"s6_p13.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
    vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext) {
			case 0:
			case 1:
			case 4:
			case 5:
				sound_player("s6_p"+(countNext+1),1);
				break;
			case 2:
				$prevBtn.show(0);
				$('.sp-1').fadeIn(500, function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play('s6_p3_1');
					current_sound.play();
					current_sound.on("complete", function(){
						$('.sp-2').fadeIn(500, function(){
							current_sound = createjs.Sound.play('s6_p3_2');
							current_sound.play();
							current_sound.on("complete", function(){
								$('.bt-hf').fadeIn(500, function(){
									current_sound = createjs.Sound.play('s6_p3');
									current_sound.play();
									current_sound.on("complete", function(){
										nav_button_controls(0);
										$('.sp-1').click(function(){
											sound_player('s6_p3_1');
										});
										$('.sp-2').click(function(){
											sound_player('s6_p3_2');
										});
									});
								});
							});
						});
					});
				});
				break;
			case 3:
				$prevBtn.show(0);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play('s6_p4');
				current_sound.play();
				current_sound.on("complete", function(){
					nav_button_controls(0);
				});
				break;
			case 6:
				$prevBtn.show(0);
				conversation($('.sp-3'), 's6_p7', $('.sp-4'), 's6_p7_1');
				break;
			case 7:
			case 8:
			case 9:
			case 10:
			case 11:
				sound_player("s6_p8",0);
				$prevBtn.show(0);
				$('.animal').one('click', function(){
					$('.animal').css('pointer-events', 'none');
					$('.sp-common').fadeIn(500, function(){
						createjs.Sound.stop();
						if(countNext==7){
							current_sound = createjs.Sound.play("s6_p8_1");
						}else{
							current_sound = createjs.Sound.play("s6_p"+(countNext+1));
						}
						current_sound.play();
						current_sound.on("complete", function(){
							nav_button_controls(0);
							$('.sp-common').click(function(){
								countNext==7?sound_player('s6_p8_1'):sound_player("s6_p"+(countNext+1));
							});
						});
					});
				});
				break;
			default:
				sound_player("s6_p"+(countNext+1),1);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls(100):'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}
	//change_src-> put 1 for girl speak first else 2 for boy-speak first, 3 for boy-3
	function conversation(class1, sound_data1, class2, sound_data2){
		$(class1).fadeIn(500, function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_data1);
			current_sound.play();
			current_sound.on("complete", function(){
				$(class2).fadeIn(500, function(){
					current_sound = createjs.Sound.play(sound_data2);
					current_sound.play();
					current_sound.on("complete", function(){
						nav_button_controls(0);
						$(class1).click(function(){
							sound_player(sound_data1);
						});
						$(class2).click(function(){
							sound_player(sound_data2);
						});
					});
				});
			});
		});
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

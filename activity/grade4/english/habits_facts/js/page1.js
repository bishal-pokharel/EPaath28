var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/en/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		extratextblock:[{
			textdata: data.lesson.chapter,
			textclass: "lesson-title chelseamarket",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-1'
				}
			]
		}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-in-1',

		extratextblock:[{
			textdata:  data.string.p1text1,
			textclass: "p1-title my_font_super_big chelseamarket",
		},{
			textdata:  data.string.p1text2,
			textclass: "p1-text-1 my_font_very_big sniglet"
		},{
			textdata:  data.string.p1text3,
			textclass: "p1-text-2 my_font_very_big sniglet",
		}],
			imageblock:[{
			imagestoshow : [
				{
					imgclass : "teeth",
					imgsrc : '',
					imgid : 'teeth'
				}
			]
		}]

	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		extratextblock:[{
			textdata:  data.string.p1text4,
			textclass: "p1-text-3 my_font_very_big sniglet",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-2'
				}
			]
		}]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		extratextblock:[{
			textdata:  data.string.p1text5,
			textclass: "p1-text-4 my_font_very_big sniglet",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-6'
				},{
					imgclass : "washing",
					imgsrc : '',
					imgid : 'washing_gif'
				}
			]
		}]
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		extratextblock:[{
			textdata:  data.string.p1text6,
			textclass: "p1-text-5 my_font_very_big sniglet",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-3'
				}
			]
		}]
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		extratextblock:[{
			textdata:  data.string.p1text7,
			textclass: "p1-text-6 my_font_very_big sniglet",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-4'
				}
			]
		}]
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		extratextblock:[{
			textdata:  data.string.p1text8,
			textclass: "p1-text-7 my_font_very_big sniglet",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-5'
				}
			]
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bg-1", src: imgpath+"page1/bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-2", src: imgpath+"page1/bg-2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-3", src: imgpath+"page1/bg-3.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-4", src: imgpath+"page1/bg-4.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-5", src: imgpath+"page1/bg-5.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-6", src: imgpath+"page1/bg-1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "teeth", src: imgpath+"page1/brushingteeth.png", type: createjs.AbstractLoader.IMAGE},
			{id: "washing_gif", src: imgpath+"page1/hand-washing-gif-file-transparent.gif", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
			{id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
			{id: "s1_p4", src: soundAsset+"s1_p4.ogg"},
			{id: "s1_p5", src: soundAsset+"s1_p5.ogg"},
			{id: "s1_p6", src: soundAsset+"s1_p6.ogg"},
			{id: "s1_p7", src: soundAsset+"s1_p7.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
        vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_card_image(content, countNext);

		$prevBtn.hide(0);
		$nextBtn.hide(0);
		switch(countNext) {
			// case 0:
			//
			// case 1:
			//
      // case 2:
			//
      // case 3:
			//
      // case 4:
			//
      // case 5:
			//
      // 	nav_button_controls(100)
			//
			// break;

		default:
			sound_player("s1_p"+(countNext+1),1);
		break;
	}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				console.log("countNext:",countNext)
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls(100):"";
		});
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_card_image(content, count){
		if(content[count].hasOwnProperty('card')){
			var card = content[count].card;
			for(var i=0; i<card.length; i++){
				var image_src = preload.getResult(card[i].imgid).src;
				console.log(image_src);
				var classes_list = card[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

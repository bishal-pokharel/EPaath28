var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/en/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		extratextblock:[{
			textdata:  data.string.p4text1,
			textclass: "title-1",
		},{
			textdata:  data.string.p4text2,
			textclass: "box-1 my_font_big ",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "monkey-1",
					imgsrc : '',
					imgid : 'monkey-1'
				}
			]
		}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		extratextblock:[{
			textdata:  data.string.p4text3,
			textclass: "title-2 my_font_very_big",
		}],
		option:[{
			optionclass: 'ans-1 class1 my_font_big',
			textclass: '',
			textdata: data.string.p4text4a,
		},{
			optionclass: 'ans-2 class2 my_font_big',
			textclass: '',
			textdata: data.string.p4text4b,
		},{
			optionclass: 'ans-3 class3 my_font_big',
			textclass: '',
			textdata: data.string.p4text4c,
		},{
			optionclass: 'ans-4 class4 my_font_big',
			textclass: '',
			textdata: data.string.p4text4d,
		}],
		clock:[{
			hrclass:'hr-6',
			minclass:''
		}]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		extratextblock:[{
			textdata:  data.string.p4text3,
			textclass: "title-2 my_font_very_big",
		}],
		option:[{
			optionclass: 'ans-1 class1 my_font_big',
			textclass: '',
			textdata: data.string.p4text5a,
		},{
			optionclass: 'ans-2 class2 my_font_big',
			textclass: '',
			textdata: data.string.p4text5b,
		},{
			optionclass: 'ans-3 class3 my_font_big',
			textclass: '',
			textdata: data.string.p4text5c,
		},{
			optionclass: 'ans-4 class4 my_font_big',
			textclass: '',
			textdata: data.string.p4text5d,
		}],
		clock:[{
			hrclass:'hr-9-30',
			minclass:'hr-6'
		}]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		extratextblock:[{
			textdata:  data.string.p4text3,
			textclass: "title-2 my_font_very_big",
		}],
		option:[{
			optionclass: 'ans-1 class1 my_font_big',
			textclass: '',
			textdata: data.string.p4text6a,
		},{
			optionclass: 'ans-2 class2 my_font_big',
			textclass: '',
			textdata: data.string.p4text6b,
		},{
			optionclass: 'ans-3 class3 my_font_big',
			textclass: '',
			textdata: data.string.p4text6c,
		},{
			optionclass: 'ans-4 class4 my_font_big',
			textclass: '',
			textdata: data.string.p4text6d,
		}],
		clock:[{
			hrclass:'hr-7-15',
			minclass:'hr-3'
		}]
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		extratextblock:[{
			textdata:  data.string.p4text3,
			textclass: "title-2 my_font_very_big",
		}],
		option:[{
			optionclass: 'ans-1 class1 my_font_big',
			textclass: '',
			textdata: data.string.p4text7a,
		},{
			optionclass: 'ans-2 class2 my_font_big',
			textclass: '',
			textdata: data.string.p4text7b,
		},{
			optionclass: 'ans-3 class3 my_font_big',
			textclass: '',
			textdata: data.string.p4text7c,
		},{
			optionclass: 'ans-4 class4 my_font_big',
			textclass: '',
			textdata: data.string.p4text7d,
		}],
		clock:[{
			hrclass:'hr-7-45',
			minclass:'hr-9'
		}]
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		extratextblock:[{
			textdata:  data.string.p4text3,
			textclass: "title-2 my_font_very_big",
		}],
		option:[{
			optionclass: 'ans-1 class1 my_font_big',
			textclass: '',
			textdata: data.string.p4text8a,
		},{
			optionclass: 'ans-2 class2 my_font_big',
			textclass: '',
			textdata: data.string.p4text8b,
		},{
			optionclass: 'ans-3 class3 my_font_big',
			textclass: '',
			textdata: data.string.p4text8c,
		},{
			optionclass: 'ans-4 class4 my_font_big',
			textclass: '',
			textdata: data.string.p4text8d,
		}],
		clock:[{
			hrclass:'hr-4-50',
			minclass:'hr-10'
		}]
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		extratextblock:[{
			textdata:  data.string.p4text1,
			textclass: "title-1",
		},{
			textdata:  data.string.p4text9,
			textclass: "box-2 my_font_big ",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "duck-1",
					imgsrc : '',
					imgid : 'duck-1'
				}
			]
		}]
	},
	// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		extratextblock:[{
			textdata:  data.string.p4text10,
			textclass: "box-3 my_font_big ",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "boy-1",
					imgsrc : '',
					imgid : 'boy-1'
				},
				{
					imgclass : "girl-1",
					imgsrc : '',
					imgid : 'girl-1'
				}
			]
		}]
	},
	// slide8
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		speechbox:[{
			speechbox: 'speeches-p4 sp-1',
			textdata : data.string.p4text11,
			imgclass: '',
			textclass : '',
			imgid : 'tb-1',
			imgsrc: '',
			// audioicon: true,
		},
		{
			speechbox: 'speeches-p4 sp-2',
			textdata : data.string.p4text12,
			imgclass: 'flipped-h',
			textclass : '',
			imgid : 'tb-1',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "boy boy-1",
					imgsrc : '',
					imgid : 'boy-2'
				},
				{
					imgclass : "girl girl-1",
					imgsrc : '',
					imgid : 'girl-2'
				}
			]
		}]
	},
	// slide9
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		speechbox:[{
			speechbox: 'speeches-p4 sp-3',
			textdata : data.string.p4text13,
			imgclass: '',
			textclass : '',
			imgid : 'tb-1',
			imgsrc: '',
			// audioicon: true,
		},
		{
			speechbox: 'speeches-p4 sp-4',
			textdata : data.string.p4text14,
			imgclass: 'flipped-h',
			textclass : '',
			imgid : 'tb-1',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-2'
				},
				{
					imgclass : "boy boy-2",
					imgsrc : '',
					imgid : 'boy-1'
				},
				{
					imgclass : "girl girl-2",
					imgsrc : '',
					imgid : 'girl-1'
				}
			]
		}]
	},
	// slide10
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		speechbox:[{
			speechbox: 'speeches-p4 sp-5',
			textdata : data.string.p4text15,
			imgclass: '',
			textclass : '',
			imgid : 'tb-1',
			imgsrc: '',
			// audioicon: true,
		},
		{
			speechbox: 'speeches-p4 sp-6',
			textdata : data.string.p4text16,
			imgclass: 'flipped-h',
			textclass : '',
			imgid : 'tb-1',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-3'
				},
				{
					imgclass : "boy boy-3",
					imgsrc : '',
					imgid : 'boy-2'
				},
				{
					imgclass : "girl girl-3",
					imgsrc : '',
					imgid : 'girl-2'
				}
			]
		}]
	},
	// slide11
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		speechbox:[{
			speechbox: 'speeches-p4 sp-7',
			textdata : data.string.p4text17,
			imgclass: '',
			textclass : '',
			imgid : 'tb-1',
			imgsrc: '',
			// audioicon: true,
		},
		{
			speechbox: 'speeches-p4 sp-8',
			textdata : data.string.p4text18,
			imgclass: 'flipped-h',
			textclass : '',
			imgid : 'tb-1',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-4'
				},
				{
					imgclass : "boy boy-4",
					imgsrc : '',
					imgid : 'boy-3'
				},
				{
					imgclass : "girl girl-4",
					imgsrc : '',
					imgid : 'girl-1'
				}
			]
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "monkey-1", src: imgpath+"showing.png", type: createjs.AbstractLoader.IMAGE},
			{id: "duck-1", src: imgpath+"pointing.png", type: createjs.AbstractLoader.IMAGE},

			{id: "body", src: imgpath+"clock/clock.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hrhand", src: imgpath+"clock/hr_hand.png", type: createjs.AbstractLoader.IMAGE},
			{id: "minhand", src: imgpath+"clock/min_hand.png", type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: "images/wrong.png", type: createjs.AbstractLoader.IMAGE},

			{id: "boy-1", src: imgpath+"pasang01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy-2", src: imgpath+"pasang02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy-3", src: imgpath+"pasang03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-1", src: imgpath+"sabina01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-2", src: imgpath+"sabina02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-3", src: imgpath+"sabina03.png", type: createjs.AbstractLoader.IMAGE},

			{id: "bg-1", src: imgpath+"page4/wakeup.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-2", src: imgpath+"page4/eating-breakfast.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-3", src: imgpath+"page4/going-to-school.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-4", src: imgpath+"page4/going-home.png", type: createjs.AbstractLoader.IMAGE},

			{id: "tb-1", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-2", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			// {id: "sound_1", src: soundAsset+"alert_correct.ogg"},
			// {id: "sound_2", src: soundAsset+"curtain.ogg"},
			{id: "s4_p1", src: soundAsset+"s4_p1.ogg"},
			{id: "s4_p2", src: soundAsset+"s4_p2.ogg"},
			{id: "s4_p7", src: soundAsset+"s4_p7.ogg"},
			{id: "s4_p8", src: soundAsset+"s4_p8.ogg"},
			{id: "s4_p9_1", src: soundAsset+"s4_p9_1.ogg"},
			{id: "s4_p9_2", src: soundAsset+"s4_p9_2.ogg"},
			{id: "s4_p10_1", src: soundAsset+"s4_p10_1.ogg"},
			{id: "s4_p10_2", src: soundAsset+"s4_p10_2.ogg"},
			{id: "s4_p11_1", src: soundAsset+"s4_p11_1.ogg"},
			{id: "s4_p11_2", src: soundAsset+"s4_p11_2.ogg"},
			{id: "s4_p12_1", src: soundAsset+"s4_p12_1.ogg"},
			{id: "s4_p12_2", src: soundAsset+"s4_p12_2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
    vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext) {
			case 0:
				sound_player("s4_p1",1);
			break;
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
				countNext==1?sound_player("s4_p2",0):'';
				$prevBtn.show(0);
				$('.correct-ans').attr('src', preload.getResult('correct').src);
				$('.incorrect-ans').attr('src', preload.getResult('incorrect').src);
				$('.clock-body').attr('src', preload.getResult('body').src);
				$('.hr-hand').attr('src', preload.getResult('hrhand').src);
				$('.min-hand').attr('src', preload.getResult('minhand').src);
				for( var mm =1; mm<5; mm++){
					$('.option-div').removeClass('ans-'+mm);
				}
				var option_position = [1,2,3,4];
				option_position.shufflearray();
				for(var op=0; op<4; op++){
					$('.option-div').eq(op).addClass('ans-'+option_position[op]);
				}
				var wrong_clicked = false;
				$(".option-div").click(function(){
					if($(this).hasClass("class1")){
						$(".option-div").css('pointer-events', 'none');
						$(this).css({
							'border-color': '#FCD172',
							'background-color': '#6EB260',
						});
						$(this).children('.correct-ans').show(0);
						play_correct_incorrect_sound(1);
						nav_button_controls(0);
					}
					else{
						$(this).css('pointer-events', 'none');
						$(this).css({
							'border': 'none',
							'background-color': '#BA6B82',
						});
						$(this).children('.incorrect-ans').show(0);
						play_correct_incorrect_sound(0);
					}
				});
				break;
			case 6:
			case 7:
				sound_player("s4_p"+(countNext+1),1);
			break;
			case 8:
				$prevBtn.show(0);
				conversation($('.sp-1'), 's4_p9_1', $('.sp-2'), 's4_p9_2', 1);
				break;
			case 9:
				$prevBtn.show(0);
				conversation($('.sp-3'), 's4_p10_1', $('.sp-4'), 's4_p10_2', 2);
				break;
			case 10:
				$prevBtn.show(0);
				conversation($('.sp-5'), 's4_p11_1', $('.sp-6'), 's4_p11_2', 3);
				break;
			case 11:
				$prevBtn.show(0);
				conversation($('.sp-7'), 's4_p12_1', $('.sp-8'), 's4_p12_2', 2);
				break;
			default:
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	//change_src-> put 1 for girl speak first else 2 for boy-speak first, 3 for boy-3
	function conversation(class1, sound_data1, class2, sound_data2, change_src){
		$(class1).fadeIn(500, function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_data1);
			current_sound.play();
			current_sound.on("complete", function(){
				if(change_src==1){
					$('.boy').attr('src', preload.getResult('boy-1').src);
					$('.girl').attr('src', preload.getResult('girl-1').src);
				} else if(change_src==2){
					$('.boy').attr('src', preload.getResult('boy-2').src);
					$('.girl').attr('src', preload.getResult('girl-2').src);
				} else if(change_src==3){
					$('.boy').attr('src', preload.getResult('boy-3').src);
					$('.girl').attr('src', preload.getResult('girl-1').src);
				}
				$(class2).fadeIn(500, function(){
					current_sound = createjs.Sound.play(sound_data2);
					current_sound.play();
					current_sound.on("complete", function(){
						nav_button_controls(0);
						$(class1).click(function(){
							sound_player(sound_data1);
						});
						$(class2).click(function(){
							sound_player(sound_data2);
						});
					});
				});
			});
		});
	}
	function sound_player(sound_id,next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete',function(){
			next?nav_button_controls():'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/en/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,

		extratextblock:[{
			textdata:  data.string.diytext,
			textclass: "title-diy my_font_super_big",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-1'
				}
			]
		}]
	},
	// slide1
	{
		contentnocenteradjust: true,

		extratextblock:[{
			textdata:  data.string.p5text1,
			textclass: "title-ins my_font_big",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-2'
				}
			]
		}],
		containerclass: 'container-1',
		textclass: '',
		textdata: data.string.p5text2,
		clock:[{
			clockclass: 'class1',
			hrclass:'hr-8-30',
			minclass:'hr-6'
		},{
			clockclass: 'class2',
			hrclass:'hr-10-15',
			minclass:'hr-3'
		},{
			clockclass: 'class3',
			hrclass:'hr-9-30',
			minclass:'hr-6'
		},{
			clockclass: 'class4',
			hrclass:'hr-8-15',
			minclass:'hr-3'
		}]
	},
	// slide2
	{
		contentnocenteradjust: true,

		extratextblock:[{
			textdata:  data.string.p5text1,
			textclass: "title-ins my_font_big",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-3'
				}
			]
		}],
		containerclass: 'container-2',
		textclass: '',
		textdata: data.string.p5text3,
		clock:[{
			clockclass: 'class1',
			hrclass:'hr-8-45',
			minclass:'hr-9'
		},{
			clockclass: 'class2',
			hrclass:'hr-9-50',
			minclass:'hr-10'
		},{
			clockclass: 'class3',
			hrclass:'hr-9-30',
			minclass:'hr-6'
		},{
			clockclass: 'class4',
			hrclass:'hr-10-15',
			minclass:'hr-3'
		}]
	},
	// slide3
	{
		contentnocenteradjust: true,

		extratextblock:[{
			textdata:  data.string.p5text1,
			textclass: "title-ins my_font_big",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-4'
				},{
					imgclass : "preetiRdn",
					imgsrc : '',
					imgid : 'preeti_riding'
				},{
					imgclass : "preetiRdn_png",
					imgsrc : '',
					imgid : 'preeti_riding_png'
				}
			]
		}],
		containerclass: 'container-3',
		textclass: '',
		textdata: data.string.p5text4,
		clock:[{
			clockclass: 'class1',
			hrclass:'hr-7-45',
			minclass:'hr-9'
		},{
			clockclass: 'class2',
			hrclass:'hr-9-45',
			minclass:'hr-9'
		},{
			clockclass: 'class3',
			hrclass:'hr-9-30',
			minclass:'hr-6'
		},{
			clockclass: 'class4',
			hrclass:'hr-9',
			minclass:'hr-12'
		}]
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		extratextblock:[{
			textdata:  data.string.p5text5,
			textclass: "fill-instruction my_font_very_big",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "squirrel",
					imgsrc : '',
					imgid : 'squirrel'
				}
			]
		}],
		fillblank:[{
			textdata: data.string.p5text6,
			textdata2: data.string.p5text6a,
			ampmdata: data.string.am,
			hrclass: 'hr1',
			minclass: 'min1'
		},{
			textdata: data.string.p5text7,
			textdata2: data.string.p5text7a,
			ampmdata: data.string.am,
			hrclass: 'hr2',
			minclass: 'min2'
		},{
			textdata: data.string.p5text9,
			textdata2: data.string.p5text9a,
			ampmdata: data.string.pm,
			hrclass: 'hr3',
			minclass: 'min3'
		},{
			textdata: data.string.p5text10,
			textdata2: data.string.p5text10a,
			ampmdata: data.string.pm,
			hrclass: 'hr4',
			minclass: 'min4'
		},{
			textdata: data.string.p5text8,
			textdata2: data.string.p5text8a,
			ampmdata: data.string.pm,
			hrclass: 'hr5',
			minclass: 'min5'
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bg-1", src: imgpath+"bg_diy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-2", src: imgpath+"page5/grandmum.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-3", src: imgpath+"page5/brushing.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-4", src: imgpath+"page5/city.png", type: createjs.AbstractLoader.IMAGE},
			{id: "preeti_riding", src: imgpath+"preeti_riding.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "preeti_riding_png", src: imgpath+"preeti_riding.png", type: createjs.AbstractLoader.IMAGE},

			{id: "body", src: imgpath+"clock2/clock.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hrhand", src: imgpath+"clock2/hr_hand.png", type: createjs.AbstractLoader.IMAGE},
			{id: "minhand", src: imgpath+"clock2/min_hand.png", type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: "images/wrong.png", type: createjs.AbstractLoader.IMAGE},

			{id: "squirrel", src: imgpath+"squirrel.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			// {id: "sound_1", src: soundAsset+"alert_correct.ogg"},
			// {id: "sound_2", src: soundAsset+"curtain.ogg"},
			{id: "s5_p2", src: soundAsset+"s5_p2.ogg"},
			{id: "s5_p3", src: soundAsset+"s5_p3.ogg"},
			{id: "s5_p4", src: soundAsset+"s5_p4.ogg"},
			{id: "s5_p5", src: soundAsset+"s5_p5.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
    vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		switch(countNext) {
			case 0:
				play_diy_audio();
				nav_button_controls(2000);
			break;
			case 1:
			case 2:
			case 3:
				sound_player("s5_p"+(countNext+1),0);
				$prevBtn.show(0);
				var parent = $(".clock-flex");
				var divs = parent.children();
				while (divs.length) {
					parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
				}
				var colors= [1,2,3,4];
				colors.shufflearray();
				for (var i=0; i<4; i++){
					$('.clock').eq(i).addClass('clock-'+colors[i]);
				}
				$('.correct-ans').attr('src', preload.getResult('correct').src);
				$('.incorrect-ans').attr('src', preload.getResult('incorrect').src);
				$('.clock-body').attr('src', preload.getResult('body').src);
				$('.hr-hand').attr('src', preload.getResult('hrhand').src);
				$('.min-hand').attr('src', preload.getResult('minhand').src);

				$(".clock").click(function(){
					if($(this).hasClass("class1")){
						$(".clock").css('pointer-events', 'none');
						$(this).children('.correct-ans').show(0);
						play_correct_incorrect_sound(1);
						createjs.Sound.stop();
						nav_button_controls(0);
					}
					else{
						$(this).css('pointer-events', 'none');
						$(this).children('.incorrect-ans').show(0);
						createjs.Sound.stop();
						play_correct_incorrect_sound(0);
					}
				});
				setTimeout(function(){
					$(".preetiRdn").delay(100).fadeOut(100);
					$(".preetiRdn_png").fadeIn(100);
				},7000);
				break;
			case 4:
				sound_player("s5_p"+(countNext+1),0);
				$prevBtn.show(0);
				for(var i=1; i<6; i++){
					input_box('.hr'+i, 2, 12);
					input_box('.min'+i, 2, 59);
				}
				break;
			default:
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls():'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}
	function input_box(input_class, max_number, max) {
		$(input_class).keydown(function(event) {
			var charCode = (event.which) ? event.which : event.keyCode;
			/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
			if (!condition) {
				return true;
			}
			//check if user inputs more than one '.'
			if ((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
				return false;
			}
			//check . and 0-9 separately after checking arrow and other keys
			if ((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110) {
				return false;
			}
			//check max no of allowed digits
			if (String(event.target.value).length >= max_number) {
				return false;
			}
		});
		$(input_class).blur(function(event) {
			var curr_val = event.target.value;
			if(String(curr_val).length==1){
				$(input_class).val('0'+curr_val);
			}
			if (curr_val > max) {
				$(input_class).val(max);
			}
			if (curr_val == '') {
				$(input_class).val('00');
			} else{
				$(input_class).data('enter','1');
			}
			for(var i =1; i<6; i++){
				if(parseInt($('.hr'+i).data('enter')) == 0 || parseInt($('.min'+i).data('enter')) == 0){
					return false;
				}
			}
			ole.footerNotificationHandler.pageEndSetNotification();
		});
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

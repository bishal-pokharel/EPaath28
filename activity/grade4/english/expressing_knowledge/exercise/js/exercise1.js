var imgpath = $ref + "/exercise/images/";
var soundAsset = $ref+"/sounds/en/";

var content=[
	//slide0
	{
		contentblockadditionalclass: 'bluebg',
		extratextblock:[{
			textdata: data.string.e1s2,
			textclass: "convotext",
		},
		{
			textdata: data.string.e1s1,
			textclass: "toptext",
		}
		],
		imageblock:[{
			imagestoshow : [{
				imgid:'hand',
				imgclass:'handclass'
			},
			{
					imgid:'president',
					imgclass:'modalmidimage',
				},
				{
					imgid:'rumi02',
					imgclass:'modalrumi',
				},
				{
					imgid:'sagar03',
					imgclass:'modalsagar',
				},
				{
					imgid:'close',
					imgclass:'closebtn',
				}
			]
		}],
		speechbox:[{
			speechbox: 'speechbox1',
			textdata : data.string.e1s3,
			imgclass: 'sp-1',
			textclass : 'answer',
			imgid : 'text_box01',
			imgsrc: '',
		},
		{
			speechbox: 'speechbox2',
			textdata : data.string.e1s4,
			imgclass: 'sp-2',
			textclass : 'answer1',
			imgid : 'text_box02',
			imgsrc: '',
		}],
	},
	//slide1
	{
		contentblockadditionalclass: 'bluebg',
		extratextblock:[{
			textdata: data.string.e1s2,
			textclass: "convotext",
		},
		{
			textdata: data.string.e1s1,
			textclass: "toptext",
		},
		{
			textdata: data.string.e1s5,
			textclass: "questionclass",
		},
		{
			textdata: data.string.e1s6,
			textclass: "option1 correct",
		},
		{
			textdata: data.string.e1s7,
			textclass: "option2",
		},
		{
			textdata: data.string.e1s8,
			textclass: "option3",
		},
		],
		imageblock:[{
			imagestoshow : [{
				imgid:'hand',
				imgclass:'handclass'
			},
			{
					imgid:'president',
					imgclass:'modalmidimage',
				},
				{
					imgid:'rumi02',
					imgclass:'modalrumi',
				},
				{
					imgid:'sagar03',
					imgclass:'modalsagar',
				},
				{
					imgid:'close',
					imgclass:'closebtn',
				},
				{
					imgid:'sagar02',
					imgclass:'optimage1',
				},
				{
					imgid:'rumi01',
					imgclass:'optimage2',
				},
				{
					imgid:'both',
					imgclass:'optimage3',
				},
			]
		}],
		speechbox:[{
			speechbox: 'speechbox1',
			textdata : data.string.e1s3,
			imgclass: 'sp-1',
			textclass : 'answer',
			imgid : 'text_box01',
			imgsrc: '',
		},
		{
			speechbox: 'speechbox2',
			textdata : data.string.e1s4,
			imgclass: 'sp-2',
			textclass : 'answer1',
			imgid : 'text_box02',
			imgsrc: '',
		}],
	},
	//slide2
	{
		contentblockadditionalclass: 'bluebg',
		extratextblock:[{
			textdata: data.string.e1s2,
			textclass: "convotext",
		},
		{
			textdata: data.string.e1s1,
			textclass: "toptext",
		},
		{
			textdata: data.string.e1s9,
			textclass: "questionclass",
		},
		{
			textdata: data.string.e1s10,
			textclass: "choice1 correct",
		},
		{
			textdata: data.string.e1s11,
			textclass: "choice2",
		},
		{
			textdata: data.string.e1s12,
			textclass: "choice3",
		},
		{
			textdata: data.string.e1s13,
			textclass: "choice4",
		},
		],
		imageblock:[{
			imagestoshow : [{
				imgid:'hand',
				imgclass:'handclass'
			},
			{
					imgid:'president',
					imgclass:'modalmidimage',
				},
				{
					imgid:'rumi02',
					imgclass:'modalrumi',
				},
				{
					imgid:'sagar03',
					imgclass:'modalsagar',
				},
				{
					imgid:'close',
					imgclass:'closebtn',
				}
			]
		}],
		speechbox:[{
			speechbox: 'speechbox1',
			textdata : data.string.e1s3,
			imgclass: 'sp-1',
			textclass : 'answer',
			imgid : 'text_box01',
			imgsrc: '',
		},
		{
			speechbox: 'speechbox2',
			textdata : data.string.e1s4,
			imgclass: 'sp-2',
			textclass : 'answer1',
			imgid : 'text_box02',
			imgsrc: '',
		}],
	},
	//slide3
	{
		contentblockadditionalclass: 'bluebg',
		extratextblock:[{
			textdata: data.string.e1s2,
			textclass: "convotext",
		},
		{
			textdata: data.string.e1s1,
			textclass: "toptext",
		},
		{
			textdata: data.string.e1s5a,
			textclass: "questionclass",
		},
		{
			textdata: data.string.e1s6,
			textclass: "option1",
		},
		{
			textdata: data.string.e1s7,
			textclass: "option2 correct",
		},
		{
			textdata: data.string.e1s8,
			textclass: "option3",
		},
		],
		imageblock:[{
			imagestoshow : [{
				imgid:'hand',
				imgclass:'handclass'
			},
			{
					imgid:'cloud02',
					imgclass:'modalmidimage',
				},
				{
					imgid:'rumi02',
					imgclass:'modalrumi',
				},
				{
					imgid:'sagar03',
					imgclass:'modalsagar',
				},
				{
					imgid:'close',
					imgclass:'closebtn',
				},
				{
					imgid:'sagar02',
					imgclass:'optimage1',
				},
				{
					imgid:'rumi01',
					imgclass:'optimage2',
				},
				{
					imgid:'both',
					imgclass:'optimage3',
				},
			]
		}],
		speechbox:[{
			speechbox: 'speechbox1',
			textdata : data.string.e1s15,
			imgclass: 'sp-1',
			textclass : 'answer',
			imgid : 'text_box01',
			imgsrc: '',
		},
		{
			speechbox: 'speechbox2',
			textdata : data.string.e1s16,
			imgclass: 'sp-2',
			textclass : 'answer1',
			imgid : 'text_box02',
			imgsrc: '',
		}],
	},
	//slide4
	{
		contentblockadditionalclass: 'bluebg',
		extratextblock:[{
			textdata: data.string.e1s2,
			textclass: "convotext",
		},
		{
			textdata: data.string.e1s1,
			textclass: "toptext",
		},
		{
			textdata: data.string.e1s16a,
			textclass: "questionclass",
		},
		{
			textdata: data.string.e1s18,
			textclass: "choose1",
		},
		{
			textdata: data.string.e1s17,
			textclass: "choose2 correct",
		}
		],
		imageblock:[{
			imagestoshow : [{
				imgid:'hand',
				imgclass:'handclass'
			},
			{
					imgid:'cloud02',
					imgclass:'modalmidimage',
				},
				{
					imgid:'rumi02',
					imgclass:'modalrumi',
				},
				{
					imgid:'sagar03',
					imgclass:'modalsagar',
				},
				{
					imgid:'close',
					imgclass:'closebtn',
				},
			]
		}],
		speechbox:[{
			speechbox: 'speechbox1',
			textdata : data.string.e1s15,
			imgclass: 'sp-1',
			textclass : 'answer',
			imgid : 'text_box01',
			imgsrc: '',
		},
		{
			speechbox: 'speechbox2',
			textdata : data.string.e1s16,
			imgclass: 'sp-2',
			textclass : 'answer1',
			imgid : 'text_box02',
			imgsrc: '',
		}],
	},
	//slide5
	{
		contentblockadditionalclass: 'bluebg',
		extratextblock:[{
			textdata: data.string.e1s2,
			textclass: "convotext",
		},
		{
			textdata: data.string.e1s1,
			textclass: "toptext",
		},
		{
			textdata: data.string.e1s21,
			textclass: "questionclass",
		},
		{
			textdata: data.string.e1s22,
			textclass: "choice1 correct",
		},
		{
			textdata: data.string.e1s23,
			textclass: "choice2",
		},
		{
			textdata: data.string.e1s24,
			textclass: "choice3",
		},
		{
			textdata: data.string.e1s25,
			textclass: "choice4",
		},
		],
		imageblock:[{
			imagestoshow : [{
				imgid:'hand',
				imgclass:'handclass'
			},
			{
					imgid:'everest',
					imgclass:'modalmidimage',
				},
				{
					imgid:'rumi02',
					imgclass:'modalrumi',
				},
				{
					imgid:'sagar03',
					imgclass:'modalsagar',
				},
				{
					imgid:'close',
					imgclass:'closebtn',
				}
			]
		}],
		speechbox:[{
			speechbox: 'speechbox1',
			textdata : data.string.e1s19,
			imgclass: 'sp-1',
			textclass : 'answer',
			imgid : 'text_box01',
			imgsrc: '',
		},
		{
			speechbox: 'speechbox2',
			textdata : data.string.e1s20,
			imgclass: 'sp-2',
			textclass : 'answer1',
			imgid : 'text_box02',
			imgsrc: '',
		}],
	},
	//slide6
	{
		contentblockadditionalclass: 'bluebg',
		extratextblock:[{
			textdata: data.string.e1s2,
			textclass: "convotext",
		},
		{
			textdata: data.string.e1s1,
			textclass: "toptext",
		},
		{
			textdata: data.string.e1s26,
			textclass: "questionclass",
		},
		{
			textdata: data.string.e1s6,
			textclass: "choose1 correct",
		},
		{
			textdata: data.string.e1s7,
			textclass: "choose2",
		}
		],
		imageblock:[{
			imagestoshow : [{
				imgid:'hand',
				imgclass:'handclass'
			},
			{
					imgid:'everest',
					imgclass:'modalmidimage',
				},
				{
					imgid:'rumi02',
					imgclass:'modalrumi',
				},
				{
					imgid:'sagar03',
					imgclass:'modalsagar',
				},
				{
					imgid:'close',
					imgclass:'closebtn',
				},
				{
					imgid:'sagar02',
					imgclass:'optimage1',
				},
				{
					imgid:'rumi01',
					imgclass:'optimage2',
				},
			]
		}],
		speechbox:[{
			speechbox: 'speechbox1',
			textdata : data.string.e1s19,
			imgclass: 'sp-1',
			textclass : 'answer',
			imgid : 'text_box01',
			imgsrc: '',
		},
		{
			speechbox: 'speechbox2',
			textdata : data.string.e1s20,
			imgclass: 'sp-2',
			textclass : 'answer1',
			imgid : 'text_box02',
			imgsrc: '',
		}],
	},
	//slide7
	{
		contentblockadditionalclass: 'bluebg',
		extratextblock:[{
			textdata: data.string.e1s2,
			textclass: "convotext",
		},
		{
			textdata: data.string.e1s1,
			textclass: "toptext",
		},
		{
			textdata: data.string.e1s31,
			textclass: "questionclass",
		},
		{
			textdata: data.string.e1s32,
			textclass: "choice1",
		},
		{
			textdata: data.string.e1s34,
			textclass: "choice2",
		},
		{
			textdata: data.string.e1s33,
			textclass: "choice3 correct",
		},
		{
			textdata: data.string.e1s35,
			textclass: "choice4",
		},
		],
		imageblock:[{
			imagestoshow : [{
				imgid:'hand',
				imgclass:'handclass'
			},
			{
					imgid:'changu_narayan_temple',
					imgclass:'modalmidimage',
				},
				{
					imgid:'rumi02',
					imgclass:'modalrumi',
				},
				{
					imgid:'sagar03',
					imgclass:'modalsagar',
				},
				{
					imgid:'close',
					imgclass:'closebtn',
				}
			]
		}],
		speechbox:[{
			speechbox: 'speechbox1',
			textdata : data.string.e1s29,
			imgclass: 'sp-1',
			textclass : 'answer',
			imgid : 'text_box01',
			imgsrc: '',
		},
		{
			speechbox: 'speechbox2',
			textdata : data.string.e1s30,
			imgclass: 'sp-2',
			textclass : 'answer1',
			imgid : 'text_box02',
			imgsrc: '',
		}],
	},
	//slide8
	{
		contentblockadditionalclass: 'bluebg',
		extratextblock:[{
			textdata: data.string.e1s2,
			textclass: "convotext",
		},
		{
			textdata: data.string.e1s1,
			textclass: "toptext",
		},
		{
			textdata: data.string.e1s5b,
			textclass: "questionclass",
		},
		{
			textdata: data.string.e1s6,
			textclass: "option1",
		},
		{
			textdata: data.string.e1s7,
			textclass: "option2 correct",
		},
		{
			textdata: data.string.e1s8,
			textclass: "option3",
		},
		],
		imageblock:[{
			imagestoshow : [{
				imgid:'hand',
				imgclass:'handclass'
			},
			{
					imgid:'changu_narayan_temple',
					imgclass:'modalmidimage',
				},
				{
					imgid:'rumi02',
					imgclass:'modalrumi',
				},
				{
					imgid:'sagar03',
					imgclass:'modalsagar',
				},
				{
					imgid:'close',
					imgclass:'closebtn',
				},
				{
					imgid:'sagar02',
					imgclass:'optimage1',
				},
				{
					imgid:'rumi01',
					imgclass:'optimage2',
				},
				{
					imgid:'both',
					imgclass:'optimage3',
				},
			]
		}],
		speechbox:[{
			speechbox: 'speechbox1',
			textdata : data.string.e1s29,
			imgclass: 'sp-1',
			textclass : 'answer',
			imgid : 'text_box01',
			imgsrc: '',
		},
		{
			speechbox: 'speechbox2',
			textdata : data.string.e1s30,
			imgclass: 'sp-2',
			textclass : 'answer1',
			imgid : 'text_box02',
			imgsrc: '',
		}],
	},
	//slide9
	{
		contentblockadditionalclass: 'bluebg',
		extratextblock:[{
			textdata: data.string.e1s2,
			textclass: "convotext",
		},
		{
			textdata: data.string.e1s1,
			textclass: "toptext",
		},
		{
			textdata: data.string.e1s38,
			textclass: "questionclass",
		},
		{
			textdata: data.string.e1s32,
			textclass: "choice1",
		},
		{
			textdata: data.string.e1s33,
			textclass: "choice2",
		},
		{
			textdata: data.string.e1s39,
			textclass: "choice3 correct",
		},
		{
			textdata: data.string.e1s34,
			textclass: "choice4",
		},
		],
		imageblock:[{
			imagestoshow : [{
				imgid:'hand',
				imgclass:'handclass'
			},
			{
					imgid:'mayadevi_temple',
					imgclass:'modalmidimage',
				},
				{
					imgid:'rumi02',
					imgclass:'modalrumi',
				},
				{
					imgid:'sagar03',
					imgclass:'modalsagar',
				},
				{
					imgid:'close',
					imgclass:'closebtn',
				}
			]
		}],
		speechbox:[{
			speechbox: 'speechbox1',
			textdata : data.string.e1s36,
			imgclass: 'sp-1',
			textclass : 'answer',
			imgid : 'text_box01',
			imgsrc: '',
		},
		{
			speechbox: 'speechbox2',
			textdata : data.string.e1s37,
			imgclass: 'sp-2',
			textclass : 'answer1',
			imgid : 'text_box02',
			imgsrc: '',
		}],
	},
	//slide10
	{
		contentblockadditionalclass: 'bluebg',
		extratextblock:[{
			textdata: data.string.e1s2,
			textclass: "convotext",
		},
		{
			textdata: data.string.e1s1,
			textclass: "toptext",
		},
		{
			textdata: data.string.e1s5c,
			textclass: "questionclass",
		},
		{
			textdata: data.string.e1s6,
			textclass: "option1",
		},
		{
			textdata: data.string.e1s7,
			textclass: "option2",
		},
		{
			textdata: data.string.e1s8,
			textclass: "option3 correct",
		},
		],
		imageblock:[{
			imagestoshow : [{
				imgid:'hand',
				imgclass:'handclass'
			},
			{
					imgid:'mayadevi_temple',
					imgclass:'modalmidimage',
				},
				{
					imgid:'rumi02',
					imgclass:'modalrumi',
				},
				{
					imgid:'sagar03',
					imgclass:'modalsagar',
				},
				{
					imgid:'close',
					imgclass:'closebtn',
				},
				{
					imgid:'sagar02',
					imgclass:'optimage1',
				},
				{
					imgid:'rumi01',
					imgclass:'optimage2',
				},
				{
					imgid:'both',
					imgclass:'optimage3',
				},
			]
		}],
		speechbox:[{
			speechbox: 'speechbox1',
			textdata : data.string.e1s36,
			imgclass: 'sp-1',
			textclass : 'answer',
			imgid : 'text_box01',
			imgsrc: '',
		},
		{
			speechbox: 'speechbox2',
			textdata : data.string.e1s37,
			imgclass: 'sp-2',
			textclass : 'answer1',
			imgid : 'text_box02',
			imgsrc: '',
		}],
	},
	//slide11
	{
		contentblockadditionalclass: 'bluebg',
		extratextblock:[{
			textdata: data.string.e1s2,
			textclass: "convotext",
		},
		{
			textdata: data.string.e1s1,
			textclass: "toptext",
		},
		{
			textdata: data.string.e1s42,
			textclass: "questionclass",
		},
		{
			textdata: data.string.e1s44,
			textclass: "choice1",
		},
		{
			textdata: data.string.e1s45,
			textclass: "choice2",
		},
		{
			textdata: data.string.e1s46,
			textclass: "choice3",
		},
		{
			textdata: data.string.e1s43,
			textclass: "choice4 correct",
		},
		],
		imageblock:[{
			imagestoshow : [{
				imgid:'hand',
				imgclass:'handclass'
			},
			{
					imgid:'living_goddess',
					imgclass:'modalmidimage',
				},
				{
					imgid:'rumi02',
					imgclass:'modalrumi',
				},
				{
					imgid:'sagar03',
					imgclass:'modalsagar',
				},
				{
					imgid:'close',
					imgclass:'closebtn',
				}
			]
		}],
		speechbox:[{
			speechbox: 'speechbox1',
			textdata : data.string.e1s41,
			imgclass: 'sp-1',
			textclass : 'answer',
			imgid : 'text_box01',
			imgsrc: '',
		},
		{
			speechbox: 'speechbox2',
			textdata : data.string.e1s40,
			imgclass: 'sp-2',
			textclass : 'answer1',
			imgid : 'text_box02',
			imgsrc: '',
		}],
	},
	//slide12
	{
		contentblockadditionalclass: 'bluebg',
		extratextblock:[{
			textdata: data.string.e1s2,
			textclass: "convotext",
		},
		{
			textdata: data.string.e1s1,
			textclass: "toptext",
		},
		{
			textdata: data.string.e1s5d,
			textclass: "questionclass",
		},
		{
			textdata: data.string.e1s6,
			textclass: "option1 correct",
		},
		{
			textdata: data.string.e1s7,
			textclass: "option2",
		},
		{
			textdata: data.string.e1s8,
			textclass: "option3",
		},
		],
		imageblock:[{
			imagestoshow : [{
				imgid:'hand',
				imgclass:'handclass'
			},
			{
					imgid:'living_goddess',
					imgclass:'modalmidimage',
				},
				{
					imgid:'rumi02',
					imgclass:'modalrumi',
				},
				{
					imgid:'sagar03',
					imgclass:'modalsagar',
				},
				{
					imgid:'close',
					imgclass:'closebtn',
				},
				{
					imgid:'sagar02',
					imgclass:'optimage1',
				},
				{
					imgid:'rumi01',
					imgclass:'optimage2',
				},
				{
					imgid:'both',
					imgclass:'optimage3',
				},
			]
		}],
		speechbox:[{
			speechbox: 'speechbox1',
			textdata : data.string.e1s41,
			imgclass: 'sp-1',
			textclass : 'answer',
			imgid : 'text_box01',
			imgsrc: '',
		},
		{
			speechbox: 'speechbox2',
			textdata : data.string.e1s40,
			imgclass: 'sp-2',
			textclass : 'answer1',
			imgid : 'text_box02',
			imgsrc: '',
		}],
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bg01", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg02", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg03", src: imgpath+"bg03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hand", src: imgpath+"hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "rumi01", src: imgpath+"rumi01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rumi02", src: imgpath+"rumi02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rumi03", src: imgpath+"rumi03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rumi04", src: imgpath+"rumi04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sagar01", src: imgpath+"sagar01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sagar02", src: imgpath+"sagar02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sagar03", src: imgpath+"sagar03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sagar04", src: imgpath+"sagar04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sagar05", src: imgpath+"sagar05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "president", src: imgpath+"first_president_of_nepal.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "museum", src: imgpath+"meseum.png", type: createjs.AbstractLoader.IMAGE},
			//tb
			{id: "text_box01", src: imgpath+"text_box01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box02", src: imgpath+"text_box02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box03", src: imgpath+"text_box03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box04", src: imgpath+"text_box04.png", type: createjs.AbstractLoader.IMAGE},

			{id: "mayadevi_temple", src: imgpath+"mayadevi_temple.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "changu_narayan_temple", src: imgpath+"changu_narayan_temple.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "handicon", src: imgpath+"hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "living_goddess", src: imgpath+"living_goddess.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "close", src: imgpath+"close.png", type: createjs.AbstractLoader.IMAGE},
			{id: "both", src: imgpath+"both.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud02", src: imgpath+"cloud02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "everest", src: imgpath+"everest.jpg", type: createjs.AbstractLoader.IMAGE},//
			// sounds
			{id: "click", src: soundAsset+"click.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}
	var score = 0;
	var testin = new NumberTemplate();
	testin.init(12);
	var updater = 1;
	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		testin.numberOfQuestions();
		$('.optimage3').css({"width":"21%","left":"66%"});
		updater = 1;
		switch(countNext) {
			case 0:
				sound_player("click");
				modalControl(1);
			break;
			case 1: case 3:
				modalControl();
				threeOptions();
			break;
			case 2:
				modalControl();
				fourOptionsFiller();
			break;
			case 4:
				modalControl();
				twoOptions();
			break;
			case 5:
				modalControl();
				fourOptionsFiller();
				$('.sp-2').css({"top":"11.2%"});
				$('.answer1').css({"top":"19.5%"});
			break;
			case 6:
				modalControl();
				twoOptions();
				$('.sp-2').css({"top":"11.2%"});
				$('.answer1').css({"top":"19.5%"});
				$('.choose1,.choose2').css({"top":"70%"});
				$('.optimage2').css({"left":"68%"});
				break;
			case 7:
				$('.choice1,.choice2,.choice3,.choice4').css({"width":"30%"});
				$('.choice1').css({"top":"50%","left":"36%"});
				$('.choice2').css({"left":"36%","top":"36%"});
				$('.choice3').css({"left":"36%","top":"65%"});
				$('.choice4').css({"left":"36%","top":"80%"});
				modalControl();
				fourOptions();
				break;
			case 8:
				modalControl();
				threeOptions();
			break;
			case 9:
				$('.choice1,.choice2,.choice3,.choice4').css({"width":"30%"});
				$('.choice1').css({"top":"50%","left":"36%"});
				$('.choice2').css({"left":"36%","top":"36%"});
				$('.choice3').css({"left":"36%","top":"65%"});
				$('.choice4').css({"left":"36%","top":"80%"});
				$('.sp-2').css({"top":"11.2%"});
				$('.answer1').css({"top":"19.5%"});
				modalControl();
				fourOptions();
			break;
			case 10:
				modalControl();
				threeOptions();
				$('.sp-2').css({"top":"11.2%"});
				$('.answer1').css({"top":"19.5%"});
			break;
			case 11:
				$('.choice1,.choice2,.choice3,.choice4').css({"width":"27%"});
				$('.choice1').css({"top":"50%","left":"36%"});
				$('.choice2').css({"left":"36%","top":"36%"});
				$('.choice3').css({"left":"36%","top":"65%"});
				$('.choice4').css({"left":"36%","top":"80%"});
				modalControl();
				fourOptions();
			break;
			case 12:
				modalControl();
				threeOptions();
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function conversation(class1, sound_data1, class2, sound_data2){
		$(class1).fadeIn(500, function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_data1);
			current_sound.play();
			current_sound.on("complete", function(){
				$(class2).fadeIn(500, function(){
					current_sound = createjs.Sound.play(sound_data2);
					current_sound.play();
					current_sound.on("complete", function(){
						nav_button_controls(0);
						$(class1).click(function(){
							sound_player(sound_data1);
						});
						$(class2).click(function(){
							sound_player(sound_data2);
						});
					});
				});
			});
		});
	}
	function conversation_solo(class1, sound_data1){
		$(class1).fadeIn(500, function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_data1);
			current_sound.play();
			current_sound.on("complete", function(){
				nav_button_controls(0);
				$(class1).click(function(){
					sound_player(sound_data1);
				});
			});
		});
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function modalControl(next){
		$('.modaldiv,.modalrumi,.modalsagar,.modalmidimage,.closebtn').hide();
		$('.handclass,.convotext').click(function(){
			$('.modaldiv,.modalrumi,.modalsagar,.modalmidimage,.closebtn').show(200);
			if(next) nav_button_controls(100);
		});
		$('.closebtn').click(function(){
			$('.modaldiv,.modalrumi,.modalsagar,.modalmidimage,.closebtn').hide(200);
		});
	}
	function threeOptions(){
		$('.option1,.option2,.option3').click(function(){
				if($(this).hasClass('correct')){
                    $(this).append("<img class='correctwrongImg' src='images/correct.png'/>");
                    play_correct_incorrect_sound(1);
					$(this).addClass('corrclass');
					$('.option1,.option2,.option3').addClass('nopoint');
					countNext==12?$nextBtn.show():nav_button_controls(100);
					if(updater==1){
						testin.update(true);
					}

				}
			else{
                    $(this).append("<img class='correctwrongImg' src='images/wrong.png'/>");
                    $(this).addClass('incorrclass');
				updater = 0;
				testin.update(false);
				play_correct_incorrect_sound(0);
		}
		});
	}

	function twoOptions(){
		$('.choose1,.choose2').click(function(){
				if($(this).hasClass('correct')){
                    $(this).append("<img class='correctwrongImg' src='images/correct.png'/>");
                    play_correct_incorrect_sound(1);
					$(this).addClass('corrclass');
					$('.choose1,.choose2').addClass('nopoint');
					nav_button_controls(100);
					if(updater==1){
						testin.update(true);
					}
				}
			else{
                    $(this).append("<img class='correctwrongImg' src='images/wrong.png'/>");
                    $(this).addClass('incorrclass');
				updater = 0;
				testin.update(false);
				play_correct_incorrect_sound(0);
			}
		});
	}

	function fourOptionsFiller(){
		$('.choice1,.choice2,.choice3,.choice4').click(function(){
				if($(this).hasClass('correct')){
                    $(this).append("<img class='correctwrongImg' src='images/correct.png'/>");
                    play_correct_incorrect_sound(1);
					$(this).addClass('corrclass');
					$('.choice1,.choice2,.choice3,.choice4').addClass('nopoint');
					$('.ansfiller').text($(this).text()).css({"text-decoration":"underline dotted"});
					nav_button_controls(100);
					if(updater==1){
						testin.update(true);
					}
				}
			else{
                    $(this).append("<img class='correctwrongImg' src='images/wrong.png'/>");
                    $(this).addClass('incorrclass');
				play_correct_incorrect_sound(0);
				updater = 0;
				testin.update(false);
			}
		});
	}
	function fourOptions(){
		$('.choice1,.choice2,.choice3,.choice4').click(function(){
				if($(this).hasClass('correct')){
                    $(this).append("<img class='correctwrongImg' src='images/correct.png'/>");
                    play_correct_incorrect_sound(1);
					$(this).addClass('corrclass');
					$('.choice1,.choice2,.choice3,.choice4').addClass('nopoint');
					nav_button_controls(100);
					if(updater==1){
						testin.update(true);
					}
				}
			else{
                    $(this).append("<img class='correctwrongImg' src='images/wrong.png'/>");
                    $(this).addClass('incorrclass');
				play_correct_incorrect_sound(0);
				updater = 0;
				testin.update(false);
			}
		});
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		countNext++;
		if(countNext>1){
			testin.gotoNext();
		}
		if(countNext<13){
			templateCaller();
		}else {
			$(".ex-number-template-score").hide(0);
			$nextBtn.hide(0);
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

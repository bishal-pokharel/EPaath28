var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/en/";

var imgpath = $ref+"/images/";

var content=[
		// slide0
		// {
		// 	contentnocenteradjust: true,
		// 	contentblockadditionalclass: '',
		//
		//
		// 	imageblock:[{
		// 		imagestoshow : [
		// 			{
		// 				imgclass : "fairy-1",
		// 				imgsrc : '',
		// 				imgid : 'fairy'
		// 			},
		// 			{
		// 				imgclass : "bg-full",
		// 				imgsrc : '',
		// 				imgid : 'bg-1'
		// 			}
		// 		]
		// 	}],
		//
		// 	speechbox:[{
		// 		speechbox: 'speech-fairy sp-1',
		// 		textdata : data.string.p2text1,
		// 		imgclass: 'flipped-v',
		// 		textclass : '',
		// 		imgid : 'tb-2',
		// 		imgsrc: '',
		// 		// audioicon: true,
		// 	}]
		// },
		// slide1
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: '',

			imageblock:[{
				imagestoshow : [
					{
						imgclass : "fairy-1",
						imgsrc : '',
						imgid : 'fairy'
					},
					{
						imgclass : "bg-full",
						imgsrc : '',
						imgid : 'bg-2'
					}
				]
			}],

			speechbox:[{
				speechbox: 'speech-fairy sp-2',
				textdata : data.string.p2text1,
				imgclass: 'flipped-v',
				textclass : '',
				imgid : 'tb-2',
				imgsrc: '',
				// audioicon: true,
			}]
		},
		// slide2
		// {
			// contentnocenteradjust: true,
			// contentblockadditionalclass: 'from-dark',
	//
			// imageblock:[{
				// imagestoshow : [
					// {
						// imgclass : "fairy-1 fly-in",
						// imgsrc : '',
						// imgid : 'fairy-2'
					// },
					// {
						// imgclass : "bg-full",
						// imgsrc : '',
						// imgid : 'bg-2'
					// },
					// {
						// imgclass : "boy-1",
						// imgsrc : '',
						// imgid : 'boy-1'
					// },
					// {
						// imgclass : "girl-1",
						// imgsrc : '',
						// imgid : 'girl-1'
					// },
				// ]
			// }],
		// },
		// slide3
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: '',

			imageblock:[{
				imagestoshow : [
					{
						imgclass : "bg-full",
						imgsrc : '',
						imgid : 'bg-2'
					},
					{
						imgclass : "boy-1",
						imgsrc : '',
						imgid : 'boy-1'
					},
					{
						imgclass : "girl-1",
						imgsrc : '',
						imgid : 'girl-1'
					},
					{
						imgclass : "tempo-body",
						imgsrc : '',
						imgid : 'auto'
					},
					{
						imgclass : "wheel tempo-front",
						imgsrc : '',
						imgid : 'wheel'
					},
					{
						imgclass : "wheel tempo-back",
						imgsrc : '',
						imgid : 'wheel'
					}
				]
			}],

			speechbox:[{
				speechbox: 'speech-person sp-b-1',
				textdata : data.string.p2text3,
				imgclass: '',
				textclass : '',
				imgid : 'tb-3',
				imgsrc: '',
				// audioicon: true,
			},{
				speechbox: 'speech-person sp-g-1',
				textdata : data.string.p2text4,
				imgclass: 'flipped-v',
				textclass : '',
				imgid : 'tb-3',
				imgsrc: '',
				// audioicon: true,
			}]
		},
		// slide4
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: '',

			imageblock:[{
				imagestoshow : [
					{
						imgclass : "bg-full",
						imgsrc : '',
						imgid : 'bg-2'
					},
					{
						imgclass : "boy-1",
						imgsrc : '',
						imgid : 'boy-2'
					},
					{
						imgclass : "girl-1",
						imgsrc : '',
						imgid : 'girl-1'
					},
					{
						imgclass : "shoes",
						imgsrc : '',
						imgid : 'shoes'
					}
				]
			}],

			speechbox:[{
				speechbox: 'speech-person sp-b-2',
				textdata : data.string.p2text5,
				imgclass: '',
				textclass : '',
				imgid : 'tb-3',
				imgsrc: '',
				// audioicon: true,
			},{
				speechbox: 'speech-person sp-g-2',
				textdata : data.string.p2text6,
				imgclass: '',
				textclass : '',
				imgid : 'tb-1',
				imgsrc: '',
				// audioicon: true,
			}]
		},
		// slide5
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: '',

			imageblock:[{
				imagestoshow : [
					{
						imgclass : "fairy-2 fly-in2",
						imgsrc : '',
						imgid : 'fairy-2'
					},
					{
						imgclass : "bg-full",
						imgsrc : '',
						imgid : 'bg-2'
					},
					{
						imgclass : "boy-1",
						imgsrc : '',
						imgid : 'boy-2'
					},
					{
						imgclass : "girl-1",
						imgsrc : '',
						imgid : 'girl-1'
					}
				]
			}],

			speechbox:[{
				speechbox: 'speech-person not-hidden sp-b-2',
				textdata : data.string.p2text5,
				imgclass: '',
				textclass : '',
				imgid : 'tb-3',
				imgsrc: '',
				// audioicon: true,
			},{
				speechbox: 'speech-person not-hidden sp-g-2',
				textdata : data.string.p2text6,
				imgclass: '',
				textclass : '',
				imgid : 'tb-1',
				imgsrc: '',
				// audioicon: true,
			},{
				speechbox: 'speech-fairy sp-f-1',
				textdata : data.string.p2text7,
				imgclass: '',
				textclass : '',
				imgid : 'tb-4',
				imgsrc: '',
				// audioicon: true,
			}]
		},
		// slide6
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: '',

			imageblock:[{
				imagestoshow : [
					{
						imgclass : "fairy-2",
						imgsrc : '',
						imgid : 'fairy-2'
					},
					{
						imgclass : "bg-full",
						imgsrc : '',
						imgid : 'bg-2'
					},
					{
						imgclass : "boy-1",
						imgsrc : '',
						imgid : 'boy-2'
					},
				]
			}],

			speechbox:[{
				speechbox: 'speech-person not-hidden sp-b-2',
				textdata : data.string.p2text5,
				imgclass: '',
				textclass : '',
				imgid : 'tb-3',
				imgsrc: '',
				// audioicon: true,
			},{
				speechbox: 'speech-fairy sp-f-2',
				textdata : data.string.p2text8,
				imgclass: '',
				textclass : '',
				imgid : 'tb-4',
				imgsrc: '',
				// audioicon: true,
			}]
		},
		// slide7
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: '',

			imageblock:[{
				imagestoshow : [
					{
						imgclass : "fairy-3",
						imgsrc : '',
						imgid : 'fairy-2'
					},
					{
						imgclass : "bg-full",
						imgsrc : '',
						imgid : 'bg-2'
					},
					{
						imgclass : "girl-1",
						imgsrc : '',
						imgid : 'girl-1'
					}
				]
			}],

			speechbox:[{
				speechbox: 'speech-person not-hidden sp-g-2',
				textdata : data.string.p2text6,
				imgclass: '',
				textclass : '',
				imgid : 'tb-1',
				imgsrc: '',
				// audioicon: true,
			},{
				speechbox: 'speech-fairy sp-f-3',
				textdata : data.string.p2text9,
				imgclass: 'flipped-v',
				textclass : '',
				imgid : 'tb-4',
				imgsrc: '',
				// audioicon: true,
			}]
		},
		// slide8
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: '',

			imageblock:[{
				imagestoshow : [
					{
						imgclass : "bg-full",
						imgsrc : '',
						imgid : 'bg-2'
					},
					{
						imgclass : "boy-1",
						imgsrc : '',
						imgid : 'boy-3'
					},
					{
						imgclass : "girl-1",
						imgsrc : '',
						imgid : 'girl-1'
					}
				]
			}],

			speechbox:[{
				speechbox: 'speech-person sp-b-2',
				textdata : data.string.p2text10,
				imgclass: '',
				textclass : '',
				imgid : 'tb-3',
				imgsrc: '',
				// audioicon: true,
			},{
				speechbox: 'speech-person sp-g-2',
				textdata : data.string.p2text11,
				imgclass: '',
				textclass : '',
				imgid : 'tb-1',
				imgsrc: '',
				// audioicon: true,
			}]
		},
		// slide9
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: '',

			imageblock:[{
				imagestoshow : [
					{
						imgclass : "fairy-2 its_hidden fly-in2",
						imgsrc : '',
						imgid : 'fairy-2'
					},
					{
						imgclass : "bg-full",
						imgsrc : '',
						imgid : 'bg-2'
					},
					{
						imgclass : "boy-1",
						imgsrc : '',
						imgid : 'boy-3'
					},
					{
						imgclass : "girl-1",
						imgsrc : '',
						imgid : 'girl-1'
					}
				]
			}],

			speechbox:[{
				speechbox: 'speech-person sp-b-4',
				textdata : data.string.p2text12,
				imgclass: '',
				textclass : '',
				imgid : 'tb-3',
				imgsrc: '',
				// audioicon: true,
			},{
				speechbox: 'speech-person sp-g-4',
				textdata : data.string.p2text13,
				imgclass: '',
				textclass : '',
				imgid : 'tb-4',
				imgsrc: '',
				// audioicon: true,
			},{
				speechbox: 'speech-fairy sp-f-4',
				textdata : data.string.p2text14,
				imgclass: '',
				textclass : '',
				imgid : 'tb-4',
				imgsrc: '',
				// audioicon: true,
			}]
		},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();


	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bg-1", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-2", src: imgpath+"bg_for_auto.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shoes", src: imgpath+"shoes-maker.png", type: createjs.AbstractLoader.IMAGE},

			{id: "fairy", src: imgpath+"flying-chibi01.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "fairy-2", src: "images/fairy/flying-chibi-fairy.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "boy-1", src: imgpath+"prem.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy-2", src: imgpath+"prem01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy-3", src: imgpath+"prem02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy-4", src: imgpath+"prem03.png", type: createjs.AbstractLoader.IMAGE},

			{id: "girl-1", src: imgpath+"niti.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-2", src: imgpath+"niti01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-3", src: imgpath+"niti02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-4", src: imgpath+"niti03.png", type: createjs.AbstractLoader.IMAGE},

			{id: "auto", src: imgpath+"auto.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wheel", src: imgpath+"wheel_front.png", type: createjs.AbstractLoader.IMAGE},

			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-2", src: 'images/textbox/white/lb-1-b.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-3", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-4", src: 'images/textbox/white/l-1-b.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s2_p1", src: soundAsset+"s2_p1.ogg"},
			{id: "s2_p2", src: soundAsset+"s2_p2.ogg"},
			{id: "s2_p2_1", src: soundAsset+"s2_p2_1.ogg"},
			{id: "s2_p3", src: soundAsset+"s2_p3.ogg"},
			{id: "s2_p3_1", src: soundAsset+"s2_p3_1.ogg"},
			{id: "s2_p4", src: soundAsset+"s2_p4.ogg"},
			{id: "s2_p5", src: soundAsset+"s2_p5.ogg"},
			{id: "s2_p6", src: soundAsset+"s2_p6.ogg"},
			{id: "s2_p7", src: soundAsset+"s2_p7.ogg"},
			{id: "s2_p7_1", src: soundAsset+"s2_p7_1.ogg"},
			{id: "s2_p8", src: soundAsset+"s2_p8.ogg"},
			{id: "s2_p8_1", src: soundAsset+"s2_p8_1.ogg"},
			{id: "s2_p8_2", src: soundAsset+"s2_p8_2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		vocabcontroller.findwords(countNext);
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext) {
			// case 0:
			// 	conversation_solo('.speech-fairy', 'sound_1');
			// 	break;
			case 0:
				$prevBtn.hide(0);
				$('.speech-fairy').fadeIn(500, function(){
					current_sound = createjs.Sound.play('s2_p1');
					current_sound.play();
					current_sound.on("complete", function(){
						setTimeout(function(){$('.speech-fairy').delay(3000).addClass('fly-out');
							$('.fairy-1').attr('src', preload.getResult('fairy-2').src);
							$('.fairy-1').addClass('fly-out');},2700);
							setTimeout(function(){$nextBtn.trigger("click");},4800);
					});
				});
				break;
			// case 2:
				// $prevBtn.show(0);
				// nav_button_controls(0);
				// // timeoutvar = setTimeout(function(){
					// // $('.fairy-1').attr('src', preload.getResult('fairy').src);
				// // }, 2100);
				// break;
			case 1:
				$prevBtn.show(0);
				$('.tempo-body, .tempo-front').animate({'left': '-50%'}, 4000);
				$('.tempo-back').animate({'left': '-23.5%'}, 4000);
				$('.sp-b-1').fadeIn(500, function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play('s2_p2');
					current_sound.play();
					current_sound.on("complete", function(){
						$('.girl-1').attr('src', preload.getResult('girl-2').src);
						$('.sp-g-1').fadeIn(500, function(){
							current_sound = createjs.Sound.play('s2_p2_1');
							current_sound.play();
							current_sound.on("complete", function(){
								$nextBtn.show(0);
								$('.sp-b-1').click(function(){
									sound_player('s2_p2');
								});
								$('.sp-g-1').click(function(){
									sound_player('s2_p2_1');
								});
							});
						});
					});
				});
				break;
			case 2:
				$prevBtn.show(0);
				$('.sp-b-2').fadeIn(500, function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play('s2_p3');
					current_sound.play();
					current_sound.on("complete", function(){
						$('.boy-1').attr('src', preload.getResult('boy-1').src);
						$('.girl-1').attr('src', preload.getResult('girl-2').src);
						$('.sp-g-2').fadeIn(500, function(){
							current_sound = createjs.Sound.play('s2_p3_1');
							current_sound.play();
							current_sound.on("complete", function(){
								$nextBtn.show(0);
								$('.sp-b-2, .sp-g-2').addClass('reduce-opacity');
								$('.bg-full').addClass('exit-left');
								$('.shoes').addClass('enter-left');
								$('.sp-b-2').click(function(){
									sound_player('s2_p3');
								});
								$('.sp-g-2').click(function(){
									sound_player('s2_p3_1');
								});
							});
						});
					});
				});
				break;
			case 3:
				$prevBtn.show(0);
				timeoutvar=setTimeout(function(){
					conversation_solo('.speech-fairy', 's2_p4');
				}, 1200);
				break;
			case 4:
				$prevBtn.show(0);
				conversation_solo('.speech-fairy', 's2_p5');
				break;
			case 5:
				$prevBtn.show(0);
				conversation_solo('.speech-fairy', 's2_p6');
				break;
			case 6:
				$prevBtn.show(0);
				$('.sp-b-2').fadeIn(500, function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play('s2_p7');
					current_sound.play();
					current_sound.on("complete", function(){
						$('.boy-1').attr('src', preload.getResult('boy-1').src);
						$('.girl-1').attr('src', preload.getResult('girl-3').src);
						$('.sp-g-2').fadeIn(500, function(){
							current_sound = createjs.Sound.play('s2_p7_1');
							current_sound.play();
							current_sound.on("complete", function(){
								$nextBtn.show(0);
								$('.sp-b-2').click(function(){
									sound_player('s2_p7');
								});
								$('.sp-g-2').click(function(){
									sound_player('s2_p7_1');
								});
							});
						});
					});
				});
				break;
			case 7:
				$prevBtn.show(0);
				$('.sp-b-4').fadeIn(500, function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play('s2_p8');
					current_sound.play();
					current_sound.on("complete", function(){
						$('.girl-1').attr('src', preload.getResult('girl-4').src);
						$('.sp-g-4').fadeIn(500, function(){
							current_sound = createjs.Sound.play('s2_p8_1');
							current_sound.play();
							current_sound.on("complete", function(){
								$('.fairy-2').show(0);
								timeoutvar=setTimeout(function(){
									$('.sp-f-4').fadeIn(500, function(){
										current_sound = createjs.Sound.play('s2_p8_2');
										current_sound.play();
										current_sound.on("complete", function(){
											ole.footerNotificationHandler.pageEndSetNotification();
											$('.sp-b-4').click(function(){
												sound_player('s2_p8');
											});
											$('.sp-g-4').click(function(){
												sound_player('s2_p8_1');
											});
											$('.sp-f-4').click(function(){
												sound_player('s2_p8_2');
											});
										});
									});
								}, 1100);
							});
						});
					});
				});
				break;
			default:
				$prevBtn.show(0);
				// sound_player('sound_2');
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function conversation(class1, sound_data1, class2, sound_data2){
		$(class1).fadeIn(500, function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_data1);
			current_sound.play();
			current_sound.on("complete", function(){
				$(class2).fadeIn(500, function(){
					current_sound = createjs.Sound.play(sound_data2);
					current_sound.play();
					current_sound.on("complete", function(){
						nav_button_controls(0);
						$(class1).click(function(){
							sound_player(sound_data1);
						});
						$(class2).click(function(){
							sound_player(sound_data2);
						});
					});
				});
			});
		});
	}
	function conversation_solo(class1, sound_data1){
		$(class1).fadeIn(500, function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_data1);
			current_sound.play();
			current_sound.on("complete", function(){
				nav_button_controls(0);
				$(class1).click(function(){
					sound_player(sound_data1);
				});
			});
		});
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
			default:
				countNext++;
				templateCaller();
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

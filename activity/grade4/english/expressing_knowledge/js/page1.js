var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/en/";

var imgpath = $ref+"/images/";

var content=[
		// slide0
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: '',

			extratextblock:[{
				textdata: data.lesson.chapter,
				textclass: "lesson-title chelseamarket",
			}],
			imageblock:[{
				imagestoshow : [
					{
						imgclass : "fairycover",
						imgsrc : '',
						imgid : 'fairy'
					},
					{
						imgclass : "girlcover",
						imgsrc : '',
						imgid : 'rubi-1'
					},
					{
						imgclass : "bg-full",
						imgsrc : '',
						imgid : 'firstbg'
					}
				]
			}],
		},
		// slide1
	/*	{
			contentnocenteradjust: true,
			contentblockadditionalclass: '',

			extratextblock:[{
				textdata: '',
				textclass: "bg-sky",
			}],

			imageblock:[{
				imagestoshow : [
					{
						imgclass : "fairy-1",
						imgsrc : '',
						imgid : 'fairy'
					},
					{
						imgclass : "hill-1",
						imgsrc : '',
						imgid : 'hill'
					},
					{
						imgclass : "sun-1",
						imgsrc : '',
						imgid : 'sun'
					},
					{
						imgclass : "cloud-1",
						imgsrc : '',
						imgid : 'cloud-1'
					},
					{
						imgclass : "cloud-2",
						imgsrc : '',
						imgid : 'cloud-2'
					},
					{
						imgclass : "cloud-3",
						imgsrc : '',
						imgid : 'cloud-3'
					}
				]
			}],

			speechbox:[{
				speechbox: 'speech-fairy sp-1',
				textdata : data.string.p1text2,
				imgclass: '',
				textclass : '',
				imgid : 'tb-1',
				imgsrc: '',
				// audioicon: true,
			}]
		},
		*/
		// slide2
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: '',

			extratextblock:[{
				textdata: '',
				textclass: "bg-sky",
			}],

			imageblock:[{
				imagestoshow : [
					{
						imgclass : "fairy-1",
						imgsrc : '',
						imgid : 'fairy'
					},
					{
						imgclass : "hill-1",
						imgsrc : '',
						imgid : 'hill'
					},
					{
						imgclass : "sun-1",
						imgsrc : '',
						imgid : 'sun'
					},
					{
						imgclass : "cloud-1",
						imgsrc : '',
						imgid : 'cloud-1'
					},
					{
						imgclass : "cloud-2",
						imgsrc : '',
						imgid : 'cloud-2'
					},
					{
						imgclass : "cloud-3",
						imgsrc : '',
						imgid : 'cloud-3'
					}
				]
			}],

			speechbox:[{
				speechbox: 'speech-fairy sp-2',
				textdata : data.string.p1text2,
				imgclass: 'flipped-v',
				textclass : '',
				imgid : 'tb-2',
				imgsrc: '',
				// audioicon: true,
			}]
		},
		// slide3
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: '',

			extratextblock:[{
				textdata: '',
				textclass: "bg-sky",
			}],

			imageblock:[{
				imagestoshow : [
					{
						imgclass : "fairy-1",
						imgsrc : '',
						imgid : 'fairy'
					},
					{
						imgclass : "hill-1",
						imgsrc : '',
						imgid : 'hill'
					},
					{
						imgclass : "sun-1",
						imgsrc : '',
						imgid : 'sun'
					},
					{
						imgclass : "cloud-1",
						imgsrc : '',
						imgid : 'cloud-1'
					},
					{
						imgclass : "cloud-2",
						imgsrc : '',
						imgid : 'cloud-2'
					},
					{
						imgclass : "cloud-3",
						imgsrc : '',
						imgid : 'cloud-3'
					}
				]
			}],

			speechbox:[{
				speechbox: 'speech-fairy sp-3',
				textdata : data.string.p1text3,
				imgclass: 'flipped-v',
				textclass : '',
				imgid : 'tb-2',
				imgsrc: '',
				// audioicon: true,
			}]
		},
		// slide4
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: '',

			extratextblock:[{
				textdata: '',
				textclass: "bg-sky",
			}],

			imageblock:[{
				imagestoshow : [
					{
						imgclass : "fairy-1",
						imgsrc : '',
						imgid : 'fairy'
					},
					{
						imgclass : "hill-1",
						imgsrc : '',
						imgid : 'hill'
					},
					{
						imgclass : "sun-1",
						imgsrc : '',
						imgid : 'sun'
					},
					{
						imgclass : "cloud-1",
						imgsrc : '',
						imgid : 'cloud-1'
					},
					{
						imgclass : "cloud-2",
						imgsrc : '',
						imgid : 'cloud-2'
					},
					{
						imgclass : "cloud-3",
						imgsrc : '',
						imgid : 'cloud-3'
					},
					{
						imgclass : "girl-1",
						imgsrc : '',
						imgid : 'niti'
					},
					{
						imgclass : "boy-1",
						imgsrc : '',
						imgid : 'prem-1'
					}
				]
			}],

			speechbox:[{
				speechbox: 'speech-fairy sp-4',
				textdata : data.string.p1text4,
				imgclass: '',
				textclass : '',
				imgid : 'tb-1',
				imgsrc: '',
				// audioicon: true,
			}]
		},

		// slide5
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: '',

			extratextblock:[{
				textdata: '',
				textclass: "bg-sky-2",
			}],

			imageblock:[{
				imagestoshow : [
					{
						imgclass : "hill-2",
						imgsrc : '',
						imgid : 'hill'
					},
					{
						imgclass : "sun-2",
						imgsrc : '',
						imgid : 'sun'
					},
					{
						imgclass : "cloud-1a",
						imgsrc : '',
						imgid : 'cloud-1'
					},
					{
						imgclass : "cloud-2a",
						imgsrc : '',
						imgid : 'cloud-2'
					},
					{
						imgclass : "cloud-3a",
						imgsrc : '',
						imgid : 'cloud-3'
					},
					{
						imgclass : "girl-2",
						imgsrc : '',
						imgid : 'niti'
					},
					{
						imgclass : "boy-2",
						imgsrc : '',
						imgid : 'prem-1'
					}
				]
			}],

			speechbox:[{
				speechbox: 'speech-person sp-b-1',
				textdata : data.string.p1text5,
				imgclass: '',
				textclass : '',
				imgid : 'tb-3',
				imgsrc: '',
				// audioicon: true,
			},{
				speechbox: 'speech-person sp-g-1',
				textdata : data.string.p1text6,
				imgclass: 'flipped-v',
				textclass : '',
				imgid : 'tb-3',
				imgsrc: '',
				// audioicon: true,
			}]
		},
		// slide6
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: '',

			extratextblock:[{
				textdata: '',
				textclass: "bg-sky-2",
			},{
				textdata: '',
				textclass: "bg-jatra",
			}],

			imageblock:[{
				imagestoshow : [
					{
						imgclass : "hill-2",
						imgsrc : '',
						imgid : 'hill'
					},
					{
						imgclass : "sun-2",
						imgsrc : '',
						imgid : 'sun'
					},
					{
						imgclass : "cloud-1a",
						imgsrc : '',
						imgid : 'cloud-1'
					},
					{
						imgclass : "cloud-2a",
						imgsrc : '',
						imgid : 'cloud-2'
					},
					{
						imgclass : "cloud-3a",
						imgsrc : '',
						imgid : 'cloud-3'
					},
					{
						imgclass : "girl-2",
						imgsrc : '',
						imgid : 'niti'
					},
					{
						imgclass : "boy-2",
						imgsrc : '',
						imgid : 'prem-1'
					},
					{
						imgclass : "jatra",
						imgsrc : '',
						imgid : 'jatra'
					}
				]
			}],

			speechbox:[{
				speechbox: 'speech-person sp-b-2',
				textdata : data.string.p1text7,
				imgclass: '',
				textclass : '',
				imgid : 'tb-3',
				imgsrc: '',
				// audioicon: true,
			}]
		},
		// slide7
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: '',

			extratextblock:[{
				textdata: '',
				textclass: "bg-sky-2",
			}],

			imageblock:[{
				imagestoshow : [
					{
						imgclass : "hill-2",
						imgsrc : '',
						imgid : 'hill'
					},
					{
						imgclass : "sun-2",
						imgsrc : '',
						imgid : 'sun'
					},
					{
						imgclass : "cloud-1a",
						imgsrc : '',
						imgid : 'cloud-1'
					},
					{
						imgclass : "cloud-2a",
						imgsrc : '',
						imgid : 'cloud-2'
					},
					{
						imgclass : "cloud-3a",
						imgsrc : '',
						imgid : 'cloud-3'
					},
					{
						imgclass : "girl-2",
						imgsrc : '',
						imgid : 'niti'
					},
					{
						imgclass : "boy-2",
						imgsrc : '',
						imgid : 'prem-1'
					},
					{
						imgclass : "fairy-2 come-in",
						imgsrc : '',
						imgid : 'fairy-2'
					}
				]
			}],

			speechbox:[{
				speechbox: 'speech-person sp-b-2 not-hidden',
				textdata : data.string.p1text7,
				imgclass: '',
				textclass : '',
				imgid : 'tb-3',
				imgsrc: '',
				// audioicon: true,
			},
			{
				speechbox: 'speech-fairy sp-f-1 ',
				textdata : data.string.p1text8,
				imgclass: '',
				textclass : '',
				imgid : 'tb-4',
				imgsrc: '',
				// audioicon: true,
			}]
		},
		// slide8
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: '',

			extratextblock:[{
				textdata: '',
				textclass: "bg-sky-2",
			}],

			imageblock:[{
				imagestoshow : [
					{
						imgclass : "hill-2",
						imgsrc : '',
						imgid : 'hill'
					},
					{
						imgclass : "sun-2",
						imgsrc : '',
						imgid : 'sun'
					},
					{
						imgclass : "cloud-1a",
						imgsrc : '',
						imgid : 'cloud-1'
					},
					{
						imgclass : "cloud-2a",
						imgsrc : '',
						imgid : 'cloud-2'
					},
					{
						imgclass : "cloud-3a",
						imgsrc : '',
						imgid : 'cloud-3'
					},
					{
						imgclass : "girl-2",
						imgsrc : '',
						imgid : 'niti'
					},
					{
						imgclass : "boy-2",
						imgsrc : '',
						imgid : 'prem-1'
					},
					{
						imgclass : "fairy-2 ",
						imgsrc : '',
						imgid : 'fairy-2'
					}
				]
			}],

			speechbox:[
			{
				speechbox: 'speech-fairy sp-f-2 ',
				textdata : data.string.p1text9,
				imgclass: '',
				textclass : '',
				imgid : 'tb-4',
				imgsrc: '',
				// audioicon: true,
			}]
		},
		// slide9
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: '',

			extratextblock:[{
				textdata: '',
				textclass: "bg-sky",
			}],

			imageblock:[{
				imagestoshow : [
					{
						imgclass : "fairy-2",
						imgsrc : '',
						imgid : 'fairy-2'
					},
					{
						imgclass : "hill-1",
						imgsrc : '',
						imgid : 'hill'
					},
					{
						imgclass : "sun-1",
						imgsrc : '',
						imgid : 'sun'
					},
					{
						imgclass : "cloud-1",
						imgsrc : '',
						imgid : 'cloud-1'
					},
					{
						imgclass : "cloud-2",
						imgsrc : '',
						imgid : 'cloud-2'
					},
					{
						imgclass : "cloud-3",
						imgsrc : '',
						imgid : 'cloud-3'
					},
					{
						imgclass : "girl-1",
						imgsrc : '',
						imgid : 'niti'
					},
					{
						imgclass : "boy-1",
						imgsrc : '',
						imgid : 'prem-1'
					}
				]
			}],

			speechbox:[{
				speechbox: 'speech-fairy sp-f-3',
				textdata : data.string.p1text10,
				imgclass: '',
				textclass : '',
				imgid : 'tb-1',
				imgsrc: '',
				// audioicon: true,
			}]
		},

		// slide10
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: '',

			imageblock:[{
				imagestoshow : [
					{
						imgclass : "bg-full",
						imgsrc : '',
						imgid : 'bg-1'
					},
					{
						imgclass : "girl-3",
						imgsrc : '',
						imgid : 'rubi-2'
					},
					{
						imgclass : "boy-3",
						imgsrc : '',
						imgid : 'sagar-1'
					}
				]
			}],

			speechbox:[{
				speechbox: 'speech-person sp-b-3',
				textdata : data.string.p1text11,
				imgclass: '',
				textclass : '',
				imgid : 'tb-3',
				imgsrc: '',
				// audioicon: true,
			},{
				speechbox: 'speech-person sp-g-3',
				textdata : data.string.p1text12,
				imgclass: 'flipped-v',
				textclass : '',
				imgid : 'tb-3',
				imgsrc: '',
				// audioicon: true,
			}]
		},
		// slide11
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: '',

			imageblock:[{
				imagestoshow : [
					{
						imgclass : "bg-full",
						imgsrc : '',
						imgid : 'bg-1'
					},
					{
						imgclass : "girl-3",
						imgsrc : '',
						imgid : 'rubi-2'
					},
					{
						imgclass : "boy-3",
						imgsrc : '',
						imgid : 'sagar-1'
					},
					{
						imgclass : "solar-sys",
						imgsrc : '',
						imgid : 'solar-sys'
					},
				]
			}],

			speechbox:[{
				speechbox: 'speech-person sp-b-4',
				textdata : data.string.p1text13,
				imgclass: 'flipped-v',
				textclass : '',
				imgid : 'tb-4',
				imgsrc: '',
				// audioicon: true,
			},{
				speechbox: 'speech-person sp-g-4',
				textdata : data.string.p1text14,
				imgclass: '',
				textclass : '',
				imgid : 'tb-2',
				imgsrc: '',
				// audioicon: true,
			}]
		},
		// slide12
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: '',

			imageblock:[{
				imagestoshow : [
					{
						imgclass : "fairy-4 fly-in",
						imgsrc : '',
						imgid : 'fairy-2'
					},
					{
						imgclass : "bg-full",
						imgsrc : '',
						imgid : 'bg-1'
					},
					{
						imgclass : "girl-3",
						imgsrc : '',
						imgid : 'rubi-2'
					},
					{
						imgclass : "boy-3",
						imgsrc : '',
						imgid : 'sagar-2'
					}
				]
			}],

			speechbox:[{
				speechbox: 'speech-person sp-g-4 not-hidden',
				textdata : data.string.p1text14,
				imgclass: '',
				textclass : '',
				imgid : 'tb-2',
				imgsrc: '',
				// audioicon: true,
			},
			{
				speechbox: 'speech-fairy sp-f-4',
				textdata : data.string.p1text15,
				imgclass: 'flipped-v',
				textclass : '',
				imgid : 'tb-4',
				imgsrc: '',
				// audioicon: true,
			}]
		}

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();


	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "hill", src: imgpath+"bg_grass.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sun", src: imgpath+"sun.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-1", src: imgpath+"clouds.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-2", src: imgpath+"cloud01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-3", src: imgpath+"cloud02.png", type: createjs.AbstractLoader.IMAGE},

			{id: "fairy", src: imgpath+"flying-chibi01.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "fairy-2", src: "images/fairy/flying-chibi-fairy.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "niti", src: imgpath+"niti.png", type: createjs.AbstractLoader.IMAGE},

			{id: "prem-1", src: imgpath+"prem.png", type: createjs.AbstractLoader.IMAGE},
			{id: "prem-2", src: imgpath+"prem01.png", type: createjs.AbstractLoader.IMAGE},

			{id: "jatra", src: imgpath+"indra-jatra.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "solar-sys", src: imgpath+"solarsystem.png", type: createjs.AbstractLoader.IMAGE},

			{id: "bg-1", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "firstbg", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},

			{id: "sagar-1", src: imgpath+"sagar01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sagar-2", src: imgpath+"sagar02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sagar-3", src: imgpath+"sagar03.png", type: createjs.AbstractLoader.IMAGE},

			{id: "rubi-1", src: imgpath+"rubi01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rubi-2", src: imgpath+"rubi02.png", type: createjs.AbstractLoader.IMAGE},

			{id: "tb-1", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-2", src: 'images/textbox/white/lb-1-b.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-3", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-4", src: 'images/textbox/white/l-1-b.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
			{id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
			{id: "s1_p4", src: soundAsset+"s1_p4.ogg"},
			{id: "s1_p5", src: soundAsset+"s1_p5.ogg"},
			{id: "s1_p5_1", src: soundAsset+"s1_p5_1.ogg"},
			{id: "s1_p6", src: soundAsset+"s1_p6.ogg"},
			{id: "s1_p7", src: soundAsset+"s1_p7.ogg"},
			{id: "s1_p8", src: soundAsset+"s1_p8.ogg"},
			{id: "s1_p9", src: soundAsset+"s1_p9.ogg"},
			{id: "s1_p10", src: soundAsset+"s1_p10.ogg"},
			{id: "s1_p10_1", src: soundAsset+"s1_p10_1.ogg"},
			{id: "s1_p11", src: soundAsset+"s1_p11.ogg"},
			{id: "s1_p11_1", src: soundAsset+"s1_p11_1.ogg"},
			{id: "s1_p12", src: soundAsset+"s1_p12.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		vocabcontroller.findwords(countNext);
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext) {
			case 0:
				sound_player("s1_p"+(countNext+1),1);
				break;
			case 1:
			case 2:
				$prevBtn.show(0);
				conversation_solo('.speech-fairy', "s1_p"+(countNext+1));
				break;
			case 3:
				$prevBtn.show(0);
				$('.speech-fairy').fadeIn(500, function(){
					current_sound = createjs.Sound.play("s1_p"+(countNext+1));
					current_sound.play();
					current_sound.on("complete", function(){
						$('.speech-fairy').click(function(){
							sound_player('sound_1');
						});
						$('.fairy-1').addClass('fairy-anim');
						$('.hill-1, .bg-sky').addClass('hill-anim');
						$('.sun-1').addClass('sun-anim');
						$('.cloud-1').addClass('cloud1-anim');
						$('.cloud-2').addClass('cloud2-anim');
						$('.cloud-3').addClass('cloud3-anim');
						$('.girl-1').addClass('boy-girl-anim');
						$('.boy-1').addClass('boy-girl-anim');
						$('.sp-4').addClass('sp-anim');
						nav_button_controls(2000);
					});
				});
				break;
			case 4:
				$prevBtn.show(0);
				conversation('.sp-b-1', 's1_p5', '.sp-g-1', 's1_p5_1');
				break;
			case 5:
				$prevBtn.show(0);
				$('.sp-b-2').fadeIn(500, function(){
					current_sound = createjs.Sound.play("s1_p"+(countNext+1));
					current_sound.play();
					current_sound.on("complete", function(){
						$('.sp-b-2, .sp-g-4').addClass('reduce-opacity');
						$('.bg-sky-2, .hill-2').addClass('exit-left');
						$('.sun-2').addClass('exit-left-1');
						$('.cloud-1a').addClass('exit-left-2');
						$('.cloud-2a').addClass('exit-left-3');
						$('.cloud-3a').addClass('exit-left-4');
						$('.bg-jatra').addClass('enter-left');
						$('.jatra').addClass('enter-left-1');
						nav_button_controls(2000);
					});
				});
				break;
			case 6:
				$prevBtn.show(0);
				timeoutvar = setTimeout(function(){
					conversation_solo('.speech-fairy', "s1_p"+(countNext+1));
				}, 1100);
				break;
			case 7:
				$prevBtn.show(0);
				conversation_solo('.speech-fairy', "s1_p"+(countNext+1));
				break;
			case 8:
				$prevBtn.show(0);
				$('.fairy-2').addClass('fairy-anim2');
				$('.hill-1, .bg-sky').addClass('hill-anim2');
				$('.sun-2').addClass('sun-anim2');
				$('.cloud-1').addClass('cloud1-anim2');
				$('.cloud-2').addClass('cloud2-anim2');
				$('.cloud-3').addClass('cloud3-anim2');
				$('.girl-1').addClass('boy-girl-anim2');
				$('.boy-1').addClass('boy-girl-anim2');
				timeoutvar = setTimeout(function(){
					$('.fairy-2').attr('src', preload.getResult('fairy').src);
					conversation_solo('.speech-fairy', "s1_p"+(countNext+1));
				}, 2000);
				break;
			case 9:
				$prevBtn.show(0);
				$('.sp-b-3').fadeIn(500, function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s1_p10");
					current_sound.play();
					current_sound.on("complete", function(){
						$('.girl-3').attr('src', preload.getResult('rubi-1').src);
						$('.sp-g-3').fadeIn(500, function(){
							current_sound = createjs.Sound.play("s1_p10_1");
							current_sound.play();
							current_sound.on("complete", function(){
								nav_button_controls(0);
								$('.sp-b-3').click(function(){
									sound_player("s1_p10");
								});
								$('.sp-g-3').click(function(){
									sound_player("s1_p10_1");
								});
							});
						});
					});
				});
				break;
			case 10:
				$prevBtn.show(0);
				$('.sp-g-4').fadeIn(500, function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play('s1_p11');
					current_sound.play();
					current_sound.on("complete", function(){
						$('.boy-3').attr('src', preload.getResult('sagar-2').src);
						$('.sp-b-4').fadeIn(500, function(){
							current_sound = createjs.Sound.play('s1_p11_1');
							current_sound.play();
							current_sound.on("complete", function(){
								$('.sp-b-4, .sp-g-4').addClass('reduce-opacity');
								$('.bg-full').addClass('exit-left');
								$('.solar-sys').addClass('enter-left');
								nav_button_controls(2000);
								$('.sp-b-4').click(function(){
									sound_player('s1_p11');
								});
								$('.sp-g-4').click(function(){
									sound_player('s1_p11_1');
								});
							});
						});
					});
				});
				break;
			case 11:
				$prevBtn.show(0);
				timeoutvar = setTimeout(function(){
					conversation_solo('.speech-fairy', "s1_p"+(countNext+1));
				}, 1100);
				break;
			default:
				$prevBtn.show(0);
				// sound_player('sound_2');
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function conversation(class1, sound_data1, class2, sound_data2){
		$(class1).fadeIn(500, function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_data1);
			current_sound.play();
			current_sound.on("complete", function(){
				$(class2).fadeIn(500, function(){
					current_sound = createjs.Sound.play(sound_data2);
					current_sound.play();
					current_sound.on("complete", function(){
						nav_button_controls(0);
						$(class1).click(function(){
							sound_player(sound_data1);
						});
						$(class2).click(function(){
							sound_player(sound_data2);
						});
					});
				});
			});
		});
	}
	function conversation_solo(class1, sound_data1){
		$(class1).fadeIn(500, function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_data1);
			current_sound.play();
			current_sound.on("complete", function(){
				nav_button_controls(0);
				$(class1).click(function(){
					sound_player(sound_data1);
				});
			});
		});
	}
	function sound_player(sound_id,next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete',function(){
			next?nav_button_controls():'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
			case 6:
			case 11:
				$prevBtn.css('display', 'none');
				$nextBtn.css('display', 'none');
				$('.contentblock').addClass('go-dark');
				setTimeout(function(){
					countNext++;
					templateCaller();
				}, 2000);
				break;
			default:
				countNext++;
				templateCaller();
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

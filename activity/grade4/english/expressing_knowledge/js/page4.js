var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/en/";

var imgpath = $ref + "/images/";

var content = [
  // slide0
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "black-bg",

    extratextblock: [
      {
        textdata: data.string.p4text0,
        textclass: "diy-title"
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg-full",
            imgsrc: "",
            imgid: "diyfull"
          }
        ]
      }
    ]
  },
  // slide1
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "black-bg",

    extratextblock: [
      {
        textdata: data.string.p4text1,
        textclass: "top-text"
      },
      {
        textdata: data.string.incorrect,
        textclass: "incorrect-text"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg-full bg-1",
            imgsrc: "",
            imgid: "bg"
          },
          {
            imgclass: "bg-full bg-2",
            imgsrc: "",
            imgid: "img-1"
          },
          {
            imgclass: "flipped-v person person-1 correct-img",
            imgsrc: "",
            imgid: "girl-1b"
          },
          {
            imgclass: "flipped-v person person-2",
            imgsrc: "",
            imgid: "boy-1a"
          },
          {
            imgclass: "cor-incor-1",
            imgsrc: "",
            imgid: "correct"
          },
          {
            imgclass: "cor-incor-2",
            imgsrc: "",
            imgid: "correct"
          }
        ]
      }
    ],

    speechbox: [
      {
        speechbox: "speech-person sp-l-1",
        textdata: data.string.p4text2,
        imgclass: "flipped-v",
        textclass: "",
        imgid: "tb-1",
        imgsrc: ""
        // audioicon: true,
      },
      {
        speechbox: "speech-person sp-r-1",
        textdata: data.string.p4text3,
        imgclass: "flipped-v",
        textclass: "",
        imgid: "tb-3",
        imgsrc: ""
        // audioicon: true,
      }
    ]
  },
  // slide2
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "black-bg",

    extratextblock: [
      {
        textdata: data.string.p4text1,
        textclass: "top-text"
      },
      {
        textdata: data.string.incorrect,
        textclass: "incorrect-text"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg-full bg-1",
            imgsrc: "",
            imgid: "bg"
          },
          {
            imgclass: "bg-full bg-2",
            imgsrc: "",
            imgid: "img-2"
          },
          {
            imgclass: "person person-1 ",
            imgsrc: "",
            imgid: "girl-2b"
          },
          {
            imgclass: "person person-2 correct-img",
            imgsrc: "",
            imgid: "boy-2a"
          },
          {
            imgclass: "cor-incor-1",
            imgsrc: "",
            imgid: "correct"
          },
          {
            imgclass: "cor-incor-2",
            imgsrc: "",
            imgid: "correct"
          }
        ]
      }
    ],

    speechbox: [
      {
        speechbox: "speech-person sp-l-2",
        textdata: data.string.p4text4,
        imgclass: "flipped-v",
        textclass: "",
        imgid: "tb-1",
        imgsrc: ""
        // audioicon: true,
      },
      {
        speechbox: "speech-person sp-r-2",
        textdata: data.string.p4text5,
        imgclass: "flipped-v",
        textclass: "",
        imgid: "tb-3",
        imgsrc: ""
        // audioicon: true,
      }
    ]
  },
  // slide3
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "black-bg",

    extratextblock: [
      {
        textdata: data.string.p4text1,
        textclass: "top-text"
      },
      {
        textdata: data.string.incorrect,
        textclass: "incorrect-text"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg-full bg-1",
            imgsrc: "",
            imgid: "bg"
          },
          {
            imgclass: "bg-full bg-2",
            imgsrc: "",
            imgid: "img-3"
          },
          {
            imgclass: "person person-2 correct-img",
            imgsrc: "",
            imgid: "girl-1b"
          },
          {
            imgclass: "person person-1",
            imgsrc: "",
            imgid: "boy-1a"
          },
          {
            imgclass: "cor-incor-1",
            imgsrc: "",
            imgid: "correct"
          },
          {
            imgclass: "cor-incor-2",
            imgsrc: "",
            imgid: "correct"
          }
        ]
      }
    ],

    speechbox: [
      {
        speechbox: "speech-person sp-l-3",
        textdata: data.string.p4text6,
        imgclass: "",
        textclass: "",
        imgid: "tb-3",
        imgsrc: ""
        // audioicon: true,
      },
      {
        speechbox: "speech-person sp-r-3",
        textdata: data.string.p4text7,
        imgclass: "",
        textclass: "",
        imgid: "tb-1",
        imgsrc: ""
        // audioicon: true,
      }
    ]
  },
  // slide4
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "black-bg",

    extratextblock: [
      {
        textdata: data.string.p4text1,
        textclass: "top-text"
      },
      {
        textdata: data.string.incorrect,
        textclass: "incorrect-text"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg-full bg-1",
            imgsrc: "",
            imgid: "bg"
          },
          {
            imgclass: "bg-full bg-2",
            imgsrc: "",
            imgid: "img-4"
          },
          {
            imgclass: "person person-2 correct-img",
            imgsrc: "",
            imgid: "girl-3b"
          },
          {
            imgclass: "person person-1",
            imgsrc: "",
            imgid: "boy-3a"
          },
          {
            imgclass: "cor-incor-1",
            imgsrc: "",
            imgid: "correct"
          },
          {
            imgclass: "cor-incor-2",
            imgsrc: "",
            imgid: "correct"
          }
        ]
      }
    ],

    speechbox: [
      {
        speechbox: "speech-person sp-l-4",
        textdata: data.string.p4text8,
        imgclass: "",
        textclass: "",
        imgid: "tb-3",
        imgsrc: ""
        // audioicon: true,
      },
      {
        speechbox: "speech-person sp-r-4",
        textdata: data.string.p4text9,
        imgclass: "",
        textclass: "",
        imgid: "tb-1",
        imgsrc: ""
        // audioicon: true,
      }
    ]
  }
];

$(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page, countNext + 1);
  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();

  var preload;
  var timeoutvar = null;
  var current_sound;

  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      //images
      {
        id: "bg",
        src: imgpath + "bg02.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "diyfull",
        src: imgpath + "diyCoverpage.png",
        type: createjs.AbstractLoader.IMAGE
      },

      {
        id: "img-1",
        src: imgpath + "solarsystem.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "img-2",
        src: imgpath + "Mt_everest.jpg",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "img-3",
        src: imgpath + "rara-lake.jpg",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "img-4",
        src: imgpath + "arun-valley.jpg",
        type: createjs.AbstractLoader.IMAGE
      },

      {
        id: "boy-1a",
        src: imgpath + "sagar01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "boy-1b",
        src: imgpath + "sagar02.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "boy-1c",
        src: imgpath + "sagar03.png",
        type: createjs.AbstractLoader.IMAGE
      },

      {
        id: "boy-2a",
        src: imgpath + "suraj04.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "boy-2b",
        src: imgpath + "suraj03.png",
        type: createjs.AbstractLoader.IMAGE
      },

      {
        id: "boy-3a",
        src: imgpath + "prem02.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "boy-3b",
        src: imgpath + "prem01.png",
        type: createjs.AbstractLoader.IMAGE
      },

      {
        id: "girl-1a",
        src: imgpath + "rubi01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "girl-1b",
        src: imgpath + "rubi02.png",
        type: createjs.AbstractLoader.IMAGE
      },

      {
        id: "girl-2a",
        src: imgpath + "asha01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "girl-2b",
        src: imgpath + "asha02.png",
        type: createjs.AbstractLoader.IMAGE
      },

      {
        id: "girl-3a",
        src: imgpath + "niti.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "girl-3b",
        src: imgpath + "niti02.png",
        type: createjs.AbstractLoader.IMAGE
      },

      {
        id: "tb-1",
        src: "images/textbox/white/tr-2.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "tb-2",
        src: "images/textbox/white/lb-1-b.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "tb-3",
        src: "images/textbox/white/tl-1.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "tb-4",
        src: "images/textbox/white/l-1-b.png",
        type: createjs.AbstractLoader.IMAGE
      },

      {
        id: "correct",
        src: "images/correct.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "incorrect",
        src: "images/wrong.png",
        type: createjs.AbstractLoader.IMAGE
      },
      // sounds
      { id: "s4_p1", src: soundAsset + "s4_p1.ogg" },
      { id: "s4_p2", src: soundAsset + "s4_p2.ogg" },
      { id: "s4_p2_2", src: soundAsset + "s4_p2_2.ogg" },
      { id: "s4_p3", src: soundAsset + "s4_p3.ogg" },
      { id: "s4_p3_1", src: soundAsset + "s4_p3_1.ogg" },
      { id: "s4_p4", src: soundAsset + "s4_p4.ogg" },
      { id: "s4_instrucion", src: soundAsset + "s4_instrucion.ogg" },
      { id: "s4_p4_1", src: soundAsset + "s4_p4_1.ogg" },
      { id: "s4_p5", src: soundAsset + "s4_p5.ogg" },
      { id: "s4_p5_1", src: soundAsset + "s4_p5_1.ogg" }
    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound); //for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }
  function handleFileLoad(event) {
    // console.log(event.item);
  }
  function handleProgress(event) {
    $("#loading-text").html(parseInt(event.loaded * 100) + "%");
  }
  function handleComplete(event) {
    $("#loading-wrapper").hide(0);
    //initialize varibales
    // call main function
    templateCaller();
  }
  //initialize
  init();

  /*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  /*===============================================
	=            data highlight function            =
	===============================================*/
  function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object"
      ? alert(
          "Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted"
        )
      : null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";
    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
        $(this).attr(
          "data-highlightcustomclass"
        ) /*if there is data-highlightcustomclass defined it is true else it is not*/
          ? (stylerulename = $(this).attr("data-highlightcustomclass"))
          : (stylerulename = "parsedstring");

        texthighlightstarttag = "<span class='" + stylerulename + "'>";
        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
        $(this).html(replaceinstring);
      });
    }
  }
  /*=====  End of data highlight function  ======*/

  /*===============================================
	 =            user notification function        =
	 ===============================================*/
  function notifyuser($notifyinside) {
    //check if $notifyinside is provided
    typeof $notifyinside !== "object"
      ? alert(
          "Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style."
        )
      : null;

    /*variable that will store the element(s) to remove notification from*/
    var $allnotifications = $notifyinside.find(
      "*[data-usernotification='notifyuser']"
    );
    // if there are any notifications removal required add the event handler
    if ($allnotifications.length > 0) {
      $allnotifications.one("click", function() {
        /* Act on the event */
        $(this).attr("data-isclicked", "clicked");
        $(this).removeAttr("data-usernotification");
      });
    }
  }

  /*=====  End of user notification function  ======*/

  /*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

  function navigationcontroller(islastpageflag) {
    typeof islastpageflag === "undefined"
      ? (islastpageflag = false)
      : typeof islastpageflag != "boolean"
      ? alert(
          "NavigationController : Hi Master, please provide a boolean parameter"
        )
      : null;

    // islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
  }

  /*=====  End of user navigation controller function  ======*/

  /*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

  function instructionblockcontroller() {
    var $instructionblock = $board.find("div.instructionblock");
    if ($instructionblock.length > 0) {
      var $contentblock = $board.find("div.contentblock");
      var $toggleinstructionblockbutton = $instructionblock.find(
        "div.toggleinstructionblock"
      );
      var instructionblockisvisibleflag;

      $contentblock.css("pointer-events", "none");

      $toggleinstructionblockbutton.on("click", function() {
        instructionblockisvisibleflag = $instructionblock.attr(
          "data-instructionblockshow"
        );
        if (instructionblockisvisibleflag == "true") {
          instructionblockisvisibleflag = "false";
          $contentblock.css("pointer-events", "auto");
        } else if (instructionblockisvisibleflag == "false") {
          instructionblockisvisibleflag = "true";
          $contentblock.css("pointer-events", "none");
        }

        $instructionblock.attr(
          "data-instructionblockshow",
          instructionblockisvisibleflag
        );
      });
    }
  }

  /*=====  End of InstructionBlockController  ======*/

  /*=================================================
	 =            general template function            =
	 =================================================*/
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    // highlight any text inside board div with datahighlightflag set true
    vocabcontroller.findwords(countNext);
    texthighlight($board);
    put_image(content, countNext);
    put_speechbox_image(content, countNext);
    switch (countNext) {
      case 0:
        play_diy_audio();
        nav_button_controls(2000);
        break;
      case 1:
        $prevBtn.show(0);
        $(".sp-l-1").fadeIn(500, function() {
          createjs.Sound.stop();
          current_sound = createjs.Sound.play("s4_p2");
          current_sound.play();
          current_sound.on("complete", function() {
            $(".person-1").attr("src", preload.getResult("girl-1a").src);
            $(".bg-1").addClass("bg-out");
            $(".bg-2").addClass("bg-in");
            $(".bg-2").show("0");
            timeoutvar = setTimeout(function() {
              $(".person-2").attr("src", preload.getResult("boy-1b").src);
              $(".sp-r-1").fadeIn(500, function() {
                current_sound = createjs.Sound.play("s4_p2_2");
                current_sound.play();
                current_sound.on("complete", function() {
                  current_sound = createjs.Sound.play("s4_instrucion");
                  current_sound.play();
                  current_sound.on("complete", function() {
                    click_img();
                    $(".person").css("pointer-events", "all");
                    $(".sp-l-1").click(function() {
                      sound_player("s4_p2");
                    });
                    $(".sp-r-1").click(function() {
                      sound_player("s4_p2_2");
                    });
                  });
                });
              });
            }, 1200);
          });
        });
        break;
      case 2:
        $prevBtn.show(0);
        $(".sp-l-2").fadeIn(500, function() {
          createjs.Sound.stop();
          current_sound = createjs.Sound.play("s4_p3");
          current_sound.play();
          current_sound.on("complete", function() {
            $(".person-1").attr("src", preload.getResult("girl-2a").src);
            $(".bg-1").addClass("bg-out");
            $(".bg-2").addClass("bg-in");
            $(".bg-2").show("0");
            timeoutvar = setTimeout(function() {
              $(".person-2").attr("src", preload.getResult("boy-2b").src);
              $(".sp-r-2").fadeIn(500, function() {
                current_sound = createjs.Sound.play("s4_p3_1");
                current_sound.play();
                current_sound.on("complete", function() {
                  current_sound = createjs.Sound.play("s4_instrucion");
                  current_sound.play();
                  current_sound.on("complete", function() {
                    click_img();
                    $(".person").css("pointer-events", "all");
                    $(".sp-l-2").click(function() {
                      sound_player("s4_p3");
                    });
                    $(".sp-r-2").click(function() {
                      sound_player("s4_p3_1");
                    });
                  });
                });
              });
            }, 1200);
          });
        });
        break;
      case 3:
        $prevBtn.show(0);
        $(".sp-r-3").fadeIn(500, function() {
          createjs.Sound.stop();
          current_sound = createjs.Sound.play("s4_p4");
          current_sound.play();
          current_sound.on("complete", function() {
            $(".person-2").attr("src", preload.getResult("girl-1a").src);
            $(".bg-1").addClass("bg-out");
            $(".bg-2").addClass("bg-in");
            $(".bg-2").show("0");
            timeoutvar = setTimeout(function() {
              $(".person-1").attr("src", preload.getResult("boy-1b").src);
              $(".sp-l-3").fadeIn(500, function() {
                current_sound = createjs.Sound.play("s4_p4_1");
                current_sound.play();
                current_sound.on("complete", function() {
                  current_sound = createjs.Sound.play("s4_instrucion");
                  current_sound.play();
                  current_sound.on("complete", function() {
                    click_img();
                    $(".person").css("pointer-events", "all");
                    $(".sp-l-3").click(function() {
                      sound_player("s4_p4_1");
                    });
                    $(".sp-r-3").click(function() {
                      sound_player("s4_p4");
                    });
                  });
                });
              });
            }, 1200);
          });
        });
        break;
      case 4:
        $prevBtn.show(0);
        $(".sp-r-4").fadeIn(500, function() {
          createjs.Sound.stop();
          current_sound = createjs.Sound.play("s4_p5");
          current_sound.play();
          current_sound.on("complete", function() {
            $(".person-2").attr("src", preload.getResult("girl-3a").src);
            $(".bg-1").addClass("bg-out");
            $(".bg-2").addClass("bg-in");
            $(".bg-2").show("0");
            timeoutvar = setTimeout(function() {
              $(".person-1").attr("src", preload.getResult("boy-3b").src);
              $(".sp-l-4").fadeIn(500, function() {
                current_sound = createjs.Sound.play("s4_p5_1");
                current_sound.play();
                current_sound.on("complete", function() {
                  current_sound = createjs.Sound.play("s4_instrucion");
                  current_sound.play();
                  current_sound.on("complete", function() {
                    click_img();
                    $(".person").css("pointer-events", "all");
                    $(".sp-l-3").click(function() {
                      sound_player("s4_p5");
                    });
                    $(".sp-r-3").click(function() {
                      sound_player("s4_p5_1");
                    });
                  });
                });
              });
            }, 1200);
          });
        });
        break;
      default:
        $prevBtn.show(0);
        // sound_player('sound_2');
        nav_button_controls(100);
        break;
    }
  }
  function nav_button_controls(delay_ms) {
    timeoutvar = setTimeout(function() {
      if (countNext == 0) {
        $nextBtn.show(0);
      } else if (countNext > 0 && countNext == $total_page - 1) {
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else {
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    }, delay_ms);
  }
  function click_img() {
    $(".person").click(function() {
      if ($(this).hasClass("correct-img")) {
        $(".incorrect-text").hide(0);
        $(".person").css("pointer-events", "none");
        $(this).css("border-color", "#6AA84F");
        play_correct_incorrect_sound(1);
        nav_button_controls(0);
        if ($(this).hasClass("person-1")) {
          $(".cor-incor-1").attr("src", preload.getResult("correct").src);
          $(".cor-incor-1").show(0);
        } else {
          $(".cor-incor-2").attr("src", preload.getResult("correct").src);
          $(".cor-incor-2").show(0);
        }
      } else {
        $(".incorrect-text").show(0);
        play_correct_incorrect_sound(0);
        $(this).css("pointer-events", "none");
        $(this).css("border-color", "#FF0000");
        if ($(this).hasClass("person-1")) {
          $(".cor-incor-1").attr("src", preload.getResult("incorrect").src);
          $(".cor-incor-1").show(0);
        } else {
          $(".cor-incor-2").attr("src", preload.getResult("incorrect").src);
          $(".cor-incor-2").show(0);
        }
      }
    });
  }
  function sound_player(sound_id) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
  }

  function put_image(content, count) {
    if (content[count].hasOwnProperty("imageblock")) {
      var imageblock = content[count].imageblock[0];
      if (imageblock.hasOwnProperty("imagestoshow")) {
        var imageClass = imageblock.imagestoshow;
        for (var i = 0; i < imageClass.length; i++) {
          var image_src = preload.getResult(imageClass[i].imgid).src;
          //get list of classes
          var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
          var selector = "." + classes_list[classes_list.length - 1];
          $(selector).attr("src", image_src);
        }
      }
    }
  }
  function put_speechbox_image(content, count) {
    if (content[count].hasOwnProperty("speechbox")) {
      var speechbox = content[count].speechbox;
      for (var i = 0; i < speechbox.length; i++) {
        var image_src = preload.getResult(speechbox[i].imgid).src;
        var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
        var selector =
          "." + classes_list[classes_list.length - 1] + ">.speechbg";
        $(selector).attr("src", image_src);
      }
    }
  }
  function templateCaller() {
    $prevBtn.css("display", "none");
    $nextBtn.css("display", "none");

    navigationcontroller();

    generaltemplate();
    loadTimelineProgress($total_page, countNext + 1);
  }

  $nextBtn.on("click", function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    switch (countNext) {
      default:
        countNext++;
        templateCaller();
        break;
    }
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    countNext--;
    templateCaller();
    /* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
    countNext < $total_page
      ? ole.footerNotificationHandler.hideNotification()
      : null;
  });
});

var imgpath = $ref + "/images/";
soundAsset = $ref+"/audio/";

var sound_dg1 = new buzz.sound((soundAsset + "ex_2.ogg"));

var content=[
{
	// slide 0

	contentnocenteradjust: true,
	uppertextblockadditionalclass: "utbaddition",
	uppertextblock:[{
		textclass: "instruction",
		textdata: data.string.e2_s11
	}],
	imageblock:[{
		imageblockclass: "balooncontainer correct animate4",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon03.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel correct",
				imagelabeldata: data.string.e2_s1_o0
		}]
	},{
		imageblockclass: "balooncontainer incorrect animate2",
		imagestoshow:[{
					imgclass: "baloon" ,
					imgsrc: imgpath+"baloon04.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel incorrect",
				imagelabeldata: data.string.e2_s2_o0
		}]
	},{
		imageblockclass: "balooncontainer incorrect animate3",
		imagestoshow:[{
					imgclass: "baloon ",
					imgsrc: imgpath+"baloon05.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel incorrect",
				imagelabeldata: data.string.e2_s3_o0
		}]
	},{
		imageblockclass: "balooncontainer incorrect animate1",
		imagestoshow:[{
					imgclass: "baloon ",
					imgsrc: imgpath+"baloon07.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel incorrect",
				imagelabeldata: data.string.e2_s8_o0
		}]
	}],
	lowertextblockadditionalclass: "ltbaddition",
	lowertextblock:[{
			textclass: "question",
			textdata: data.string.e2_s1_o1
	}]
},{
	// slide 2
	contentnocenteradjust: true,
	uppertextblockadditionalclass: "utbaddition",
	uppertextblock:[{
		textclass: "instruction",
		textdata: data.string.e2_s11
	}],
	lowertextblockadditionalclass: "ltbaddition",
	imageblock:[{
		imageblockclass: "balooncontainer incorrect animate1",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon02.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s1_o0
		}]
	},{
		imageblockclass: "balooncontainer incorrect animate2",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon03.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s9_o0
		}]
	},{
		imageblockclass: "balooncontainer correct animate3",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon05.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s2_o0
		}]
	},{
		imageblockclass: "balooncontainer incorrect animate4",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon05.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s8_o0
		}]
	}],
	lowertextblock:[{
			textclass: "question",
			textdata: data.string.e2_s2_o1
	}]
},{
	// slide 3
	contentnocenteradjust: true,
	uppertextblockadditionalclass: "utbaddition",
	uppertextblock:[{
		textclass: "instruction",
		textdata: data.string.e2_s11
	}],
	lowertextblockadditionalclass: "ltbaddition",
	imageblock:[{
		imageblockclass: "balooncontainer incorrect animate4",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon07.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s1_o0
		}]
	},{
		imageblockclass: "balooncontainer correct animate3",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon01.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s3_o0
		}]
	},{
		imageblockclass: "balooncontainer incorrect animate2",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon04.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s10_o0
		}]
	},{
		imageblockclass: "balooncontainer incorrect animate1",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon02.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s8_o0
		}]
	}],
	lowertextblock:[{
			textclass: "question",
			textdata: data.string.e2_s3_o1
	}]
},{
	// slide 4
	contentnocenteradjust: true,
	uppertextblockadditionalclass: "utbaddition",
	uppertextblock:[{
		textclass: "instruction",
		textdata: data.string.e2_s11
	}],
	lowertextblockadditionalclass: "ltbaddition",
	imageblock:[{
		imageblockclass: "balooncontainer incorrect animate2",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon02.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s7_o0
		}]
	},{
		imageblockclass: "balooncontainer incorrect animate4",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon03.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s6_o0
		}]
	},{
		imageblockclass: "balooncontainer incorrect animate3",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon06.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s5_o0
		}]
	},{
		imageblockclass: "balooncontainer correct animate1",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon07.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s4_o0
		}]
	}],
	lowertextblock:[{
			textclass: "question",
			textdata: data.string.e2_s4_o1
	}]
},{
	// slide 5
	contentnocenteradjust: true,
	uppertextblockadditionalclass: "utbaddition",
	uppertextblock:[{
		textclass: "instruction",
		textdata: data.string.e2_s11
	}],
	lowertextblockadditionalclass: "ltbaddition",
	imageblock:[{
		imageblockclass: "balooncontainer incorrect animate1",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon06.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s7_o0
		}]
	},{
		imageblockclass: "balooncontainer incorrect animate3",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon07.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s6_o0
		}]
	},{
		imageblockclass: "balooncontainer correct animate2",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon02.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s5_o0
		}]
	},{
		imageblockclass: "balooncontainer incorrect animate4",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon05.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s4_o0
		}]
	}],
	lowertextblock:[{
			textclass: "question",
			textdata: data.string.e2_s5_o1
	}]
},{
	// slide 6
	contentnocenteradjust: true,
	uppertextblockadditionalclass: "utbaddition",
	uppertextblock:[{
		textclass: "instruction",
		textdata: data.string.e2_s11
	}],
	lowertextblockadditionalclass: "ltbaddition",
	imageblock:[{
		imageblockclass: "balooncontainer correct animate2",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon04.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s6_o0
		}]
	},{
		imageblockclass: "balooncontainer incorrect animate4",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon03.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s7_o0
		}]
	},{
		imageblockclass: "balooncontainer incorrect animate1",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon02.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s5_o0
		}]
	},{
		imageblockclass: "balooncontainer incorrect animate3",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon01.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s4_o0
		}]
	}],
	lowertextblock:[{
			textclass: "question",
			textdata: data.string.e2_s6_o1
	}]
},{
	// slide 7
	contentnocenteradjust: true,
	uppertextblockadditionalclass: "utbaddition",
	uppertextblock:[{
		textclass: "instruction",
		textdata: data.string.e2_s11
	}],
	lowertextblockadditionalclass: "ltbaddition",
	imageblock:[{
		imageblockclass: "balooncontainer incorrect animate4",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon03.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s10_o0
		}]
	},{
		imageblockclass: "balooncontainer incorrect animate1",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon06.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s6_o0
		}]
	},{
		imageblockclass: "balooncontainer correct animate2",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon04.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s7_o0
		}]
	},{
		imageblockclass: "balooncontainer incorrect animate3",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon02.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s4_o0
		}]
	}],
	lowertextblock:[{
			textclass: "question",
			textdata: data.string.e2_s7_o1
	}]
},{
	// slide 8
	contentnocenteradjust: true,
	uppertextblockadditionalclass: "utbaddition",
	uppertextblock:[{
		textclass: "instruction",
		textdata: data.string.e2_s11
	}],
	lowertextblockadditionalclass: "ltbaddition",
	imageblock:[{
		imageblockclass: "balooncontainer correct animate3",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon07.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s8_o0
		}]
	},{
		imageblockclass: "balooncontainer incorrect animate1",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon05.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s10_o0
		}]
	},{
		imageblockclass: "balooncontainer incorrect animate4",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon03.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s5_o0
		}]
	},{
		imageblockclass: "balooncontainer incorrect animate2",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon01.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s2_o0
		}]
	}],
	lowertextblock:[{
			textclass: "question",
			textdata: data.string.e2_s8_o1
	}]
},{
	// slide 9
	contentnocenteradjust: true,
	uppertextblockadditionalclass: "utbaddition",
	uppertextblock:[{
		textclass: "instruction",
		textdata: data.string.e2_s11
	}],
	lowertextblockadditionalclass: "ltbaddition",
	imageblock:[{
		imageblockclass: "balooncontainer incorrect animate2",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon03.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s7_o0
		}]
	},{
		imageblockclass: "balooncontainer incorrect animate4",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon05.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s1_o0
		}]
	},{
		imageblockclass: "balooncontainer incorrect animate3",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon02.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s5_o0
		}]
	},{
		imageblockclass: "balooncontainer correct animate1",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon07.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s9_o0
		}]
	}],
	lowertextblock:[{
			textclass: "question",
			textdata: data.string.e2_s9_o1
	}]
},{
	// slide 10
	contentnocenteradjust: true,
	uppertextblockadditionalclass: "utbaddition",
	uppertextblock:[{
		textclass: "instruction",
		textdata: data.string.e2_s11
	}],
	lowertextblockadditionalclass: "ltbaddition",
	imageblock:[{
		imageblockclass: "balooncontainer incorrect animate1",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon01.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s2_o0
		}]
	},{
		imageblockclass: "balooncontainer correct animate4",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon05.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s10_o0
		}]
	},{
		imageblockclass: "balooncontainer incorrect animate2",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon06.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s5_o0
		}]
	},{
		imageblockclass: "balooncontainer incorrect animate3",
		imagestoshow:[{
					imgclass: "baloon",
					imgsrc: imgpath+"baloon02.png"
			}],
		imagelabels: [{
				imagelabelclass: "baloonlabel",
				imagelabeldata: data.string.e2_s4_o0
		}]
	}],
	lowertextblock:[{
			textclass: "question",
			textdata: data.string.e2_s10_o1
	}]
}
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;
	var $total_page = content.length;

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
	*/
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


	//controls the navigational state of the program
	 function navigationController(islastpageflag){
	  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

			if (countNext == 0 && $total_page != 1) {
				$nextBtn.show(0);
				$prevBtn.css('display', 'none');
			} else if ($total_page == 1) {
				$prevBtn.css('display', 'none');
				$nextBtn.css('display', 'none');

				// if lastpageflag is true
				islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
			} else if (countNext > 0 && countNext < $total_page - 1) {
				// $nextBtn.show(0);
				// $prevBtn.show(0);
			} else if (countNext == $total_page - 1) {
				$nextBtn.css('display', 'none');
				// $prevBtn.show(0);

				// if lastpageflag is true
				// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
			}
	   }

	var source = $("#general-template").html();
	var template = Handlebars.compile(source);

		var staring = true;
						var eggscorecontroller = new EggTemplate();


	function generalTemplate(){
		var html = template(content[countNext]);
		$board.html(html);
		texthighlight($board);
			switch (countNext) {
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:
				if(countNext==0){
					playaudio(sound_dg1, $(".dg-1"));
				}
						if(staring){
							eggscorecontroller.init(10);
							var $scoreboard = $(".scoreboard");
							$scoreboard.show(0);
							staring = false
						}
						var answered = false;
						$(".incorrect").click(function(){
							play_correct_incorrect_sound(0);
							if(!answered){
								$(this).addClass("incorrected");
								eggscorecontroller.update(false);
								answered = true;
							}
							$(this).hide(0);

						});
						$(".correct").click(function(){
							play_correct_incorrect_sound(1);
							$(this).addClass("corrected");
							if(!answered){
								play_correct_incorrect_sound(1);
								$(this).addClass("corrected");
								eggscorecontroller.update(true);
								answered = true;
							}
							$nextBtn.trigger("click");
							$(this).hide(0);
						});
						break;
				default:
					break;
			}
	}
	function playaudio(sound_data, $dialog_container){
			var playing = true;
			$dialog_container.removeClass("playable");
			$dialog_container.click(function(){
				if(!playing){
					playaudio(sound_data, $dialog_container);
				}
				return false;
			});
			$prevBtn.hide(0);
			if((countNext+1) == content.length){
				ole.footerNotificationHandler.hideNotification();
			}else{
				$nextBtn.hide(0);
			}
			sound_data.play();
			sound_data.bind('ended', function(){
				setTimeout(function(){
					if(countNext != 0)
									$prevBtn.show(0);
					$dialog_container.addClass("playable");
					playing = false;
					sound_data.unbind('ended');
					if((countNext+1) == content.length){
						ole.footerNotificationHandler.pageEndSetNotification();
								$prevBtn.show(0);
					}else{
						// $nextBtn.show(0);
					}
				}, 100);
			});
		}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
		if(countNext>0)
			eggscorecontroller.gotoNext();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext = 0;
		templateCaller();
	});
	templateCaller();
});
 		/*===============================================
	 	=            data highlight function            =
	 	===============================================*/
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

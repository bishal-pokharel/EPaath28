var imgpath = $ref + '/images/';
soundAsset = $ref+"/audio/";
var sound_dg1 = new buzz.sound((soundAsset + "p3_s0_0.ogg"));
var sound_dg2 = new buzz.sound((soundAsset + "p3_s0_1.ogg"));
var sound_dg3 = new buzz.sound((soundAsset + "p3_s0_2.ogg"));
var sound_dg5 = new buzz.sound((soundAsset + "p3_s1_1.ogg"));
var sound_dg6 = new buzz.sound((soundAsset + "p3_s2.ogg"));
var sound_dg7 = new buzz.sound((soundAsset + "p3_s3.ogg"));
var sound_dg8 = new buzz.sound((soundAsset + "p3_s4.ogg"));
var sound_dg9 = new buzz.sound((soundAsset + "p3_s5.ogg"));
var sound_dg10 = new buzz.sound((soundAsset + "p3_s6.ogg"));
var content;
content = [{
    contentblockadditionalclass: 'background',
    flipperBlock: [{
        balloonSrc: imgpath + 'balloon_loku_rope.png',
        flippers: [{
            flipperID: 'present-flipper',
            sides: [{
                sideClass: 'front',
                titleText: data.string.slide_title_2,
                textdata: data.string.p3_text1
            }, {
                sideClass: 'back',
                titleText: data.string.slide_title_2,
                textdata: data.string.p3_text2
            }]
        }]
    }]
}, {
    // slide 1
    titleBlock: [{
        textclass: 'slide-title',
        textdata: data.string.slide_title_2
    } ],
    definitionBlock: [{
        textdata: data.string.p3_text3,
        datahighlightflag: true,
        datahighlightcustomclass: 'highlighter'
    }],
    imageBlock: [{
        imagestoshow: [{
            imgclass: 'past_pic',
            imgsrc: imgpath + 'wakeup.png'
        }, {
            imgclass: 'retro-effect-img',
            imgsrc: imgpath + 'faded_effects01.gif'
        }]
    }]
}, {
    // slide 2
    titleBlock: [{
        textclass: 'slide-title',
        textdata: data.string.slide_title_2
    }],
    definitionBlock: [{
        textdata: data.string.p3_text4,
        datahighlightflag: true,
        datahighlightcustomclass: 'highlighter'
    }],
    imageBlock: [{
        imagestoshow: [{
            imgclass: 'past_pic',
            imgsrc: imgpath + 'brushing.png'
        }, {
            imgclass: 'retro-effect-img',
            imgsrc: imgpath + 'faded_effects01.gif'
        }]
    }]
}, {
    // slide 3
    titleBlock: [{
        textclass: 'slide-title',
        textdata: data.string.slide_title_2
    }],
    definitionBlock: [{
        textdata: data.string.p3_text5,
        datahighlightflag: true,
        datahighlightcustomclass: 'highlighter'
    }],
    imageBlock: [{
        imagestoshow: [{
            imgclass: 'past_pic',
            imgsrc: imgpath + 'mom-cooking.png'
        }, {
            imgclass: 'retro-effect-img',
            imgsrc: imgpath + 'faded_effects01.gif'
        }]
    }]
}, {
    // slide 4
    titleBlock: [{
        textclass: 'slide-title',
        textdata: data.string.slide_title_2
    }],
    definitionBlock: [{
        textdata: data.string.p3_text6,
        datahighlightflag: true,
        datahighlightcustomclass: 'highlighter'
    }],
    imageBlock: [{
        imagestoshow: [{
            imgclass: 'past_pic',
            imgsrc: imgpath + 'boys-playfootball.png'
        }, {
            imgclass: 'retro-effect-img',
            imgsrc: imgpath + 'faded_effects01.gif'
        }]
    }]
}, {
    // slide 5
    titleBlock: [{
        textclass: 'slide-title',
        textdata: data.string.slide_title_2
    }],
    additionalDefinitionBlockClass: 'summary',
    definitionBlock: [{
        textdata: data.string.p3_text7,
        datahighlightflag: true,
        datahighlightcustomclass: 'highlighter'
    }],
    imageBlock: [{
        imagestoshow: [{
            imgclass: 'past_pic',
            imgsrc: imgpath + 'boy-drink-milk.png'
        }, {
            imgclass: 'retro-effect-img',
            imgsrc: imgpath + 'faded_effects01.gif'
        }]
    }]
}, {
    // slide 6
    titleBlock: [{
        textclass: 'slide-title',
        textdata: data.string.slide_title_2
    }],
    additionalDefinitionBlockClass: 'summary',
    definitionBlock: [{
        textdata: data.string.p3_text8,
        datahighlightflag: true,
        datahighlightcustomclass: 'highlighter'
    }],
    summaryBlock: [{
        textdata: data.string.p3_text9
    }, {
        textdata: data.string.p3_text10
    }, {
        textdata: data.string.p3_text11
    }, {
        textdata: data.string.p3_text12
    }, {
        textdata: data.string.p3_text13
    }]
}];



$(function() {
    // var height = $(window).height();
    // var width = $(window).width();
    // $("#board").css({"width": width, "height": (height*580/960)});
    // function recursion(){
    // if(data.string != null){
    // } else{
    // recursion();
    // }
    // }
    // recursion();
    // objectifyActivityData("data.xml");

    $(window).resize(function() {
        recalculateHeightWidth();
    });

    function recalculateHeightWidth() {
        var heightresized = $(window).height();
        var widthresized = $(window).width();
        var factor = 960 / 580;
        var equivalentwidthtoheight = widthresized / factor;

        if (heightresized >= equivalentwidthtoheight) {
            $(".shapes_activity").css({
                "width": widthresized,
                "height": equivalentwidthtoheight
            });
        } else {
            $(".shapes_activity").css({
                "height": heightresized,
                "width": heightresized * 960 / 580
            });
        }
        // $(".shapes_activity").css({"left": "50%" ,
        // "height": "50%" ,
        // "-webkit-transform" : "translate(-50%, - 50%)",
        // "transform" : "translate(-50%, - 50%)"});
    }
    var $board = $(".board");
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

    recalculateHeightWidth();


    /*
    	inorder to use the handlebar partials we need to register them
    	to their respective handlebar partial pointer first
    */
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


    //controls the navigational state of the program
    function navigationController() {
        if (countNext == 0 && total_page != 1) {
            $nextBtn.show(0);
        } else if (countNext > 0 && countNext < (total_page - 1)) {
            $nextBtn.show(0);
            $prevBtn.show(0);
        } else if (countNext == total_page - 1) {
            $prevBtn.show(0);
        }
    }

    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);

        $board.html(html);

        $('.definition-block p').hide(0);
        $nextBtn.hide(0);
        $prevBtn.hide(0);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);
		vocabcontroller.findwords(countNext);
    $nextBtn.hide(0);
        switch (countNext) {
            case 0:
            buzz.all().stop();
            playaudio(sound_dg1, $(".dg-1"));
            setTimeout(function() {
            playaudio(sound_dg2, $(".dg-1"));
          },2000);
          $('.flipper-block').addClass('animated slideInUp');
          $('.front').on('click', function() {
            buzz.all().stop();
            playaudio(sound_dg3, $(".dg-1"));
              $('#present-flipper').toggleClass('doFlip');
          });
          $('.back').on('click', function() {
          buzz.all().stop();
          playaudio(sound_dg2, $(".dg-1"));
          $('#present-flipper').toggleClass('doFlip');
        });
				      break;
            case 1:
            buzz.all().stop();
            playaudio(sound_dg5, $(".dg-1"));
            fadeInDef();
            break;
            case 2:
            buzz.all().stop();
            playaudio(sound_dg6, $(".dg-1"));
            fadeInDef();
            break;
            case 3:
            buzz.all().stop();
            playaudio(sound_dg7, $(".dg-1"));
            fadeInDef();
            break;
            case 4:
            buzz.all().stop();
            playaudio(sound_dg8, $(".dg-1"));
            fadeInDef();
            break;
            case 5:
            buzz.all().stop();
            playaudio(sound_dg9, $(".dg-1"));
                fadeInDef();
                break;
            case 6:
            buzz.all().stop();
            playaudio(sound_dg10, $(".dg-1"));
                $('.definition-block p').show(0);
                var summaryBlock = $('.summary-block');
                var $summaryItems = summaryBlock.find('p');

                $summaryItems.css('opacity', 0);
                setTimeout(function(){
                  $summaryItems.each(function(index) {
                      $(this).delay(1500*index).animate({
                          opacity: 1
                      }, 1500);
                  });
                },5000);

                break;
        }


        function fadeInDef() {
            setTimeout(function() {
                $('.definition-block p').fadeIn(1000);
            }, 200);
        }
    }
    // Handlebars.registerHelper('listItem', function (from, to, context, options){
    // var item = "";
    // for (var i = from, j = to; i <= j; i++) {
    // item = item + options.fn(context[i]);
    // }
    // return item;
    // });

    	function playaudio(sound_data, $dialog_container){
    			var playing = true;
    			$dialog_container.removeClass("playable");
    			$dialog_container.click(function(){
    				if(!playing){
    					playaudio(sound_data, $dialog_container);
    				}
    				return false;
    			});
    			$prevBtn.hide(0);
    			if((countNext+1) == content.length){
    				ole.footerNotificationHandler.hideNotification();
    			}else{
    				$nextBtn.hide(0);
    			}
    			sound_data.play();
    			sound_data.bind('ended', function(){
    				setTimeout(function(){
    					if(countNext != 0)
    					$prevBtn.show(0);
    					$dialog_container.addClass("playable");
    					playing = false;
    					sound_data.unbind('ended');
    					if((countNext+1) == content.length){
    						ole.footerNotificationHandler.pageEndSetNotification();
    					}else{
                switch (countNext) {
                  case 0:
                  setTimeout(function() {
    						$nextBtn.show(0);
              },10500);
                    break;
                  default:
                  $nextBtn.show(0);
                }
    					}
    				}, 1000);
    			});
    		}

    function templateCaller() {
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

        loadTimelineProgress($total_page, countNext + 1);

        navigationController();

        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


    }

    $nextBtn.on("click", function() {
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
        countNext--;
        templateCaller();
    });
    // setTimeout(function(){
    total_page = content.length;
    templateCaller();
    // }, 250);


});



/*===============================================
	 =            data highlight function            =
	 ===============================================*/
function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";


    if ($alltextpara.length > 0) {
        $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                (stylerulename = $(this).attr("data-highlightcustomclass")) :
                (stylerulename = "parsedstring");

            texthighlightstarttag = "<span class=" + stylerulename + ">";


            replaceinstring = $(this).html();
            replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
            replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);


            $(this).html(replaceinstring);
        });
    }
}
/*=====  End of data highlight function  ======*/

//page 1

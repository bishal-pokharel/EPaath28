var imgpath = $ref + "/exercise/images/";
	soundAsset = $ref+"/audio/";

var sound_dg1 = new buzz.sound((soundAsset + "ex_1.ogg"));
var content=[
{
	// slide 0
	contentblockadditionalclass: 'background',
	contentnocenteradjust:true,
	uppertextblock:[{
		textclass: "question",
		textdata: data.string.e1_s1
	},{
		textclass: "top",
	textdata: data.string.e1_s0
	}]
},{
	// slide 1
	contentblockadditionalclass: 'background',
	contentnocenteradjust:true,
	uppertextblock:[{
		textclass: "question",
		textdata: data.string.e1_s2
	},{
		textclass: "top",
	textdata: data.string.e1_s0
	}]
},{
	// slide 2
	contentblockadditionalclass: 'background',
		contentnocenteradjust:true,
	uppertextblock:[{
		textclass: "question",
		textdata: data.string.e1_s3
	},{
		textclass: "top",
	textdata: data.string.e1_s0
	}]
},{
	// slide 3
	contentblockadditionalclass: 'background',
		contentnocenteradjust:true,
	uppertextblock:[{
		textclass: "question",
		textdata: data.string.e1_s4
	},{
		textclass: "top",
	textdata: data.string.e1_s0
	}]
},{
	// slide 4
	contentblockadditionalclass: 'background',
		contentnocenteradjust:true,
	uppertextblock:[{
		textclass: "question",
		textdata: data.string.e1_s5
	},{
		textclass: "top",
	textdata: data.string.e1_s0
	}]
}
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;
	var $total_page = content.length;

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
	*/
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


	//controls the navigational state of the program
	 function navigationController(islastpageflag){
	  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

			if (countNext == 0 && $total_page != 1) {
				$nextBtn.show(0);
				$prevBtn.css('display', 'none');
			} else if ($total_page == 1) {
				$prevBtn.css('display', 'none');
				$nextBtn.css('display', 'none');

				// if lastpageflag is true
				islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
			} else if (countNext > 0 && countNext < $total_page - 1) {
				// $nextBtn.show(0);
				// $prevBtn.show(0);
			} else if (countNext == $total_page - 1) {
				$nextBtn.css('display', 'none');
				// $prevBtn.show(0);

				// if lastpageflag is true
				// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
			}
	   }

	var source = $("#general-template").html();
	var template = Handlebars.compile(source);
	var eggscorecontroller = new EggTemplate();

	eggscorecontroller.init(10);
	var $scoreboard = $(".scoreboard");

	function generalTemplate(){
		var html = template(content[countNext]);
		$board.html(html);
		texthighlight($board);
			switch (countNext) {
				case 0:

				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				if(countNext==0){
				  playaudio(sound_dg1, $(".dg-1"));
				}
						$scoreboard.show(0);
						var answered1 = false;
						var answered2 = false;

						var tried1 = false;
						var tried2 = false;
						$(".correct1").click(function(){
							// $(this).css("background-color", "#0f0");
							play_correct_incorrect_sound(1);
							$(".option1").html( $(this).html() ).css("background-color", "#0f0");
							$(".option_click1").hide(0);
							if(!tried1){
								eggscorecontroller.update(true);
								eggscorecontroller.gotoNext();
								tried1 = true;
							}
							answered1 = true;
							if(answered1 && answered2){
								if(countNext != $total_page-1){
										$nextBtn.show(0);
								}
							}
						});
						$(".correct2").click(function(){
							// $(this).css("background-color", "#0f0");
							play_correct_incorrect_sound(1);
							$(".option2").html( $(this).html() ).css("background-color", "#0f0");
							$(".option_click2").hide(0);
							if(!tried2){
								eggscorecontroller.update(true);
								eggscorecontroller.gotoNext();
								tried2 = true;
							}
							answered2 = true;
							if(answered1 && answered2){
								if(countNext != $total_page-1){
										$nextBtn.show(0);
								}
							}
						});
						$(".incorrect1").click(function(){
							play_correct_incorrect_sound(0);
							$(this).css("background-color", "#f00");
							if(!tried1){
									eggscorecontroller.update(false);
									eggscorecontroller.gotoNext();
									tried1 = true;
							}

						});
						$(".incorrect2").click(function(){
							play_correct_incorrect_sound(0);
							$(this).css("background-color", "#f00");
							if(!tried2){
									eggscorecontroller.update(false);
									eggscorecontroller.gotoNext();
									tried2 = true;
							}
						});
						break;
				default:
					break;
			}
	}
	function playaudio(sound_data, $dialog_container){
			var playing = true;
			$dialog_container.removeClass("playable");
			$dialog_container.click(function(){
				if(!playing){
					playaudio(sound_data, $dialog_container);
				}
				return false;
			});
			$prevBtn.hide(0);
			if((countNext+1) == content.length){
				ole.footerNotificationHandler.hideNotification();
			}else{
				$nextBtn.hide(0);
			}
			sound_data.play();
			sound_data.bind('ended', function(){
				setTimeout(function(){
					if(countNext != 0)
									$prevBtn.show(0);
					$dialog_container.addClass("playable");
					playing = false;
					sound_data.unbind('ended');
					if((countNext+1) == content.length){
						ole.footerNotificationHandler.pageEndSetNotification();
								$prevBtn.show(0);
					}else{
						// $nextBtn.show(0);
					}
				}, 100);
			});
		}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext = 0;
		templateCaller();
	});
	templateCaller();
});
 		/*===============================================
	 	=            data highlight function            =
	 	===============================================*/
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

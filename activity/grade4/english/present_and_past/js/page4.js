var imgpath = $ref + '/images/';
var audioPath = $ref + 'audio/';
soundAsset = $ref+"/audio/";
// Array of audio names
var sounds = ['2-55', '5-35', '6-00', '7-15', '2-55', '5-35', '6-00', '7-15', '2-55', '5-35', '6-00', '7-15',
        '2-55', '5-35', '6-00', '7-15', '2-55', '5-35', '6-00', '7-15', '2-55', '5-35', '6-00', '7-15',
        '2-55', '5-35', '6-00', '7-15', '2-55', '5-35', '6-00', '7-15', '2-55', '5-35', '6-00', '7-15',
        '2-55'];
// Mapping names into buzz objects
sounds = sounds.map(function(sound) {
    var soundPath = audioPath + sound + '.mp3';
    console.log(soundPath)
    return new buzz.sound(soundPath);
});
var sound_dg0 = new buzz.sound((soundAsset + "s4_p1.ogg"));
var sound_dg1 = new buzz.sound((soundAsset + "s4_p2.ogg"));
var sound_dg2 = new buzz.sound((soundAsset + "s4_p3.ogg"));
var sound_dg3 = new buzz.sound((soundAsset + "s4_p4.ogg"));
var sound_dg5 = new buzz.sound((soundAsset + "s4_p5.ogg"));
var sound_dg6 = new buzz.sound((soundAsset + "s4_p6.ogg"));
var sound_dg7 = new buzz.sound((soundAsset + "s4_p7.ogg"));
var sound_dg8 = new buzz.sound((soundAsset + "s4_p8.ogg"));
var sound_dg9 = new buzz.sound((soundAsset + "s4_p9.ogg"));
var sound_dg10 = new buzz.sound((soundAsset + "s4_p10.ogg"));
var sound_dg11 = new buzz.sound((soundAsset + "s4_p11.ogg"));
var sound_dg12 = new buzz.sound((soundAsset + "s4_p12.ogg"));
var sound_dg13 = new buzz.sound((soundAsset + "s4_p13.ogg"));
var sound_dg14 = new buzz.sound((soundAsset + "s4_p14.ogg"));
var sound_dg15 = new buzz.sound((soundAsset + "s4_p15.ogg"));
var sound_dg16 = new buzz.sound((soundAsset + "s4_p16.ogg"));
var sound_dg17 = new buzz.sound((soundAsset + "s4_p17.ogg"));
var sound_dg18= new buzz.sound((soundAsset + "s4_p18.ogg"));
var sound_dg19 = new buzz.sound((soundAsset + "s4_p19.ogg"));
var sound_dg20 = new buzz.sound((soundAsset + "s4_p20.ogg"));
// var sound_dg21 = new buzz.sound((soundAsset + "s4_p20.ogg"));

// slide 0
var slideContent = {
    contentblockadditionalclass: 'slide-0',
    titleBlock: [{
        textclass: 'slide-title',
        textdata: data.string.slide_title_3
    }],
    definitionBlock: [{
        textdata: data.string.p4_text1
    }],
    teachBlock: [{
        presentWord: 'add',
        pastWord: 'added'
    }],
    imageBlock: [{
        imagestoshow: [{
            imgclass: 'sundar-img',
            imgsrc: imgpath + 'sundar.png'
        }]
    }]
};
var slideContent2 = $.extend({}, slideContent, {
    definitionBlock: [{
        textdata: data.string.p4_text2
    }]
});
var slideContent3 = $.extend({}, slideContent, {
    definitionBlock: [{
        textdata: data.string.p4_text3
    }]
});

var content = [
    {
        contentblockadditionalclass:'page4background',
        titleBlock1:[{
            textcontent:data.string.p4_text3_backgroundtext
        }]
    },
    // Slide 0
    slideContent,
    // writing only changed property
    // Slide 1
    $.extend({}, slideContent, {
        teachBlock: [{
            presentWord: data.string.p4_text1_word2,
            pastWord: data.string.p4_text1_word2_past
        }]
    }),
    // Slide 2
    $.extend({}, slideContent, {
        teachBlock: [{
            presentWord: data.string.p4_text1_word3,
            pastWord: data.string.p4_text1_word3_past
        }]
    }),
    // Slide 3
    $.extend({}, slideContent, {
        teachBlock: [{
            presentWord: data.string.p4_text1_word4,
            pastWord: data.string.p4_text1_word4_past
        }]
    }),
    // Slide 4
    $.extend({}, slideContent, {
        teachBlock: [{
            presentWord: data.string.p4_text1_word5,
            pastWord: data.string.p4_text1_word5_past
        }]
    }),
    // Slide 5
    $.extend({}, slideContent, {
        teachBlock: [{
            presentWord: data.string.p4_text1_word6,
            pastWord: data.string.p4_text1_word6_past
        }]
    }),
    //Slide 6
    $.extend({}, slideContent2, {
        teachBlock: [{
            presentWord: data.string.p4_text2_word1,
            pastWord: data.string.p4_text2_word1_past
        }]
    }),
    //Slide 7
    $.extend({}, slideContent2, {
        teachBlock: [{
            presentWord: data.string.p4_text2_word2,
            pastWord: data.string.p4_text2_word2_past
        }]
    }),
    //Slide 8
    $.extend({}, slideContent2, {
        teachBlock: [{
            presentWord: data.string.p4_text2_word3,
            pastWord: data.string.p4_text2_word3_past
        }]
    }),
    //Slide 9
    $.extend({}, slideContent2, {
        teachBlock: [{
            presentWord: data.string.p4_text2_word4,
            pastWord: data.string.p4_text2_word4_past
        }]
    }),
    //Slide 10
    $.extend({}, slideContent2, {
        teachBlock: [{
            presentWord: data.string.p4_text2_word5,
            pastWord: data.string.p4_text2_word5_past
        }]
    }),
    //Slide 11
    $.extend({}, slideContent3, {
        teachBlock: [{
            presentWord: data.string.p4_text3_word1,
            pastWord: data.string.p4_text3_word1_past
        }]
    }),
    //Slide 12
    $.extend({}, slideContent3, {
        teachBlock: [{
            presentWord: data.string.p4_text3_word2,
            pastWord: data.string.p4_text3_word2_past
        }]
    }),
    //Slide 13
    $.extend({}, slideContent3, {
        teachBlock: [{
            presentWord: data.string.p4_text3_word3,
            pastWord: data.string.p4_text3_word3_past
        }]
    }),
    //Slide 14
    $.extend({}, slideContent3, {
        teachBlock: [{
            presentWord: data.string.p4_text3_word4,
            pastWord: data.string.p4_text3_word4_past
        }]
    }),
    //Slide 15
    $.extend({}, slideContent3, {
        teachBlock: [{
            presentWord: data.string.p4_text3_word5,
            pastWord: data.string.p4_text3_word5_past
        }]
    }),
    //Slide 16
    $.extend({}, slideContent3, {
        teachBlock: [{
            presentWord: data.string.p4_text3_word6,
            pastWord: data.string.p4_text3_word6_past
        }]
    }),
    //Slide 17
    $.extend({}, slideContent3, {
        teachBlock: [{
            presentWord: data.string.p4_text3_word7,
            pastWord: data.string.p4_text3_word7_past
        }]
    }),
    //Slide 18
    $.extend({}, slideContent3, {
        teachBlock: [{
            presentWord: data.string.p4_text3_word8,
            pastWord: data.string.p4_text3_word8_past
        }]
    }),
];

$(function() {
    var $board = $(".board");
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var $total_page = content.length;
    // var height = $(window).height();
    // var width = $(window).width();
    // $("#board").css({"width": width, "height": (height*580/960)});
    // function recursion(){
    // if(data.string != null){
    // } else{
    // recursion();
    // }
    // }
    // recursion();
    // objectifyActivityData("data.xml");

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

    $(window).resize(function() {
        recalculateHeightWidth();
    });

    function recalculateHeightWidth() {
        var heightresized = $(window).height();
        var widthresized = $(window).width();
        var factor = 960 / 580;
        var equivalentwidthtoheight = widthresized / factor;

        if (heightresized >= equivalentwidthtoheight) {
            $(".shapes_activity").css({
                "width": widthresized,
                "height": equivalentwidthtoheight
            });
        } else {
            $(".shapes_activity").css({
                "height": heightresized,
                "width": heightresized * 960 / 580
            });
        }
        // $(".shapes_activity").css({"left": "50%" ,
        // "height": "50%" ,
        // "-webkit-transform" : "translate(-50%, - 50%)",
        // "transform" : "translate(-50%, - 50%)"});
    }

    // general DOM elements


    loadTimelineProgress($total_page, countNext + 1);

    recalculateHeightWidth();


    /*
    	inorder to use the handlebar partials we need to register them
    	to their respective handlebar partial pointer first
    */
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


    // controls the navigational state of the program
    function navigationController(islastpageflag) {
        typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
            $prevBtn.css('display', 'none');
        } else if (countNext > 1) {
            $nextBtn.hide(0);
            $prevBtn.show(0);
        } else if ($total_page == 1) {
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');
            // if lastpageflag is true
            islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
        } else if (countNext > 0 && countNext < $total_page - 1) {
            $nextBtn.show(0);
            $prevBtn.show(0);
        } else if (countNext == $total_page - 1) {
            $nextBtn.css('display', 'none');
            $prevBtn.show(0);
            // if lastpageflag is true
            // islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
        }
    }

    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);

        var imageBlock = $('.image-block');

        $board.html(html);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);
		vocabcontroller.findwords(countNext);

        switch (countNext) {
            case 0:
            buzz.all().stop();
            playaudio(sound_dg0, $(".dg-1"));
                break;
            case 1:
                buzz.all().stop();
                playaudio(sound_dg1, $(".dg-1"));
                break;
            case 2:
            buzz.all().stop();
            playaudio(sound_dg2, $(".dg-1"));
                break;
            case 3:
            buzz.all().stop();
            playaudio(sound_dg3, $(".dg-1"));
                break;
            case 4:
            buzz.all().stop();
            playaudio(sound_dg5, $(".dg-1"));
                break;
            case 5:
            buzz.all().stop();
            playaudio(sound_dg6, $(".dg-1"));
                break;
            case 6:
            buzz.all().stop();
            playaudio(sound_dg7, $(".dg-1"));
                break;
            case 7:
            buzz.all().stop();
            playaudio(sound_dg8, $(".dg-1"));
                break;
            case 8:
            buzz.all().stop();
            playaudio(sound_dg9, $(".dg-1"));
                break;
            case 9:
            buzz.all().stop();
            playaudio(sound_dg10, $(".dg-1"));
                break;
            case 10:
            buzz.all().stop();
            playaudio(sound_dg11, $(".dg-1"));
                break;
            case 11:
            buzz.all().stop();
            playaudio(sound_dg12, $(".dg-1"));
                break;
            case 12:
            buzz.all().stop();
            playaudio(sound_dg13, $(".dg-1"));
                break;
            case 13:
            buzz.all().stop();
            playaudio(sound_dg14, $(".dg-1"));
                break;
            case 14:
            buzz.all().stop();
            playaudio(sound_dg15, $(".dg-1"));
                break;
            case 15:
            buzz.all().stop();
            playaudio(sound_dg16, $(".dg-1"));
                break;
            case 16:
            buzz.all().stop();
            playaudio(sound_dg17, $(".dg-1"));
                break;
            case 17:
            buzz.all().stop();
            playaudio(sound_dg18, $(".dg-1"));
                break;
            case 18:
            buzz.all().stop();
            playaudio(sound_dg19, $(".dg-1"));
                break;
            case 19:
            buzz.all().stop();
            playaudio(sound_dg20, $(".dg-1"));
                // ole.footerNotificationHandler.lessonEndSetNotification();
                break;

        }

        function createSlide(sound1, sound2) {
            $nextBtn.hide(0);
            $prevBtn.hide(0)
            setTimeout(function() {
                playPresentPast(sound1, sound2, 1000, function() {
                    if (countNext !== total_page - 1) $nextBtn.show(0);
                    if (countNext !== 0) $prevBtn.show(0);
                });
            }, 500);
        }

        function playPresentPast(sound1, sound2, interval, callback) {
            var PLAYING_COLOR = 'orange';
            var STOPPED_COLOR = 'rgba(255, 255, 255, 0.7)';

            sound1.bind('playing', function() {
                console.log('playing1');
                toggleWordColor($('.present-word'), PLAYING_COLOR);
                toggleMonkeySpeak();
            });
            // Bind events
            sound1.bindOnce('ended', function() {
                // console.log('ended');
                toggleMonkeySpeak();
                toggleWordColor($('.present-word'), STOPPED_COLOR);
                setTimeout(function() {
                    sound2.play();
                }, 1000);
            });
            sound2.bind('playing', function() {
                // console.log('playing2');
                toggleWordColor($('.past-word'), PLAYING_COLOR);
                toggleMonkeySpeak();
            });
            sound2.bindOnce('ended', function() {
                toggleWordColor($('.past-word'), STOPPED_COLOR);
                toggleMonkeySpeak();
                sound1.unbind('playing');
                sound2.unbind('playing');
                callback();
            });


            setTimeout(function() {
                sound1.play();
            }, 1000);
        }
         function showNavBtns() {
            if (countNext < content.length - 1) {
                $nextBtn.show(0)
            } else {
                ole.footerNotificationHandler.pageEndSetNotification();
            }
        }
        function toggleWordColor(wordEl, color) {
            wordEl.css({color: color})
        }

        function toggleMonkeySpeak() {
            var pngURL = imgpath + 'sundar.png';
            var gifURL = imgpath + 'sundar.gif';
            var $monkeyImg = $('.sundar-img').eq(0);
            var $imageParent = $monkeyImg.parent();
            var imageSrc = $monkeyImg.attr('src');
            if (imageSrc.endsWith('.png')) {
                $monkeyImg = $monkeyImg.detach();
                $monkeyImg.attr('src', gifURL);
                $monkeyImg.appendTo($imageParent);
            } else {
                $monkeyImg = $monkeyImg.detach();
                $monkeyImg.attr('src', pngURL);
                $monkeyImg.appendTo($imageParent);
            }
        }

    }
    // Handlebars.registerHelper('listItem', function (from, to, context, options){
    // var item = "";
    // for (var i = from, j = to; i <= j; i++) {
    // item = item + options.fn(context[i]);
    // }
    // return item;
    // });

    	function playaudio(sound_data, $dialog_container){
    			var playing = true;
    			$dialog_container.removeClass("playable");
    			$dialog_container.click(function(){
    				if(!playing){
    					playaudio(sound_data, $dialog_container);
    				}
    				return false;
    			});
    			$prevBtn.hide(0);
    			if((countNext+1) == content.length){
    				ole.footerNotificationHandler.hideNotification();
    			}else{
    				$nextBtn.hide(0);
    			}
    			sound_data.play();
    			sound_data.bind('ended', function(){
    				setTimeout(function(){
    					if(countNext != 0)
    					$prevBtn.show(0);
    					$dialog_container.addClass("playable");
    					playing = false;
    					sound_data.unbind('ended');
    					if((countNext+1) == content.length){
    						ole.footerNotificationHandler.pageEndSetNotification();
    					}else{
    						$nextBtn.show(0);
    					}
    				}, 1000);
    			});
    		}

    function templateCaller() {
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

        loadTimelineProgress($total_page, countNext + 1);

        navigationController();

        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


    }

    $nextBtn.on("click", function() {
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
        countNext--;
        templateCaller();
    });
    // setTimeout(function(){
    total_page = content.length;
    templateCaller();
    // }, 250);


});



/*===============================================
	 =            data highlight function            =
	 ===============================================*/
function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";

    if ($alltextpara.length > 0) {
        $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                (stylerulename = $(this).attr("data-highlightcustomclass")) :
                (stylerulename = "parsedstring");

            texthighlightstarttag = "<span>";


            replaceinstring = $(this).html();
            replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
            replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);


            $(this).html(replaceinstring);
        });
    }
}
/*=====  End of data highlight function  ======*/

//page 1

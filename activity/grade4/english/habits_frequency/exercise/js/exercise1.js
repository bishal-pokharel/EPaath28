var imgpath = $ref + "/images/diy/";
var soundAssets = $ref + "/sounds/";

var content=[
	// slide1
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "title",
			textdata: data.string.exc_title
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"ans",
			textclass: "qstn",
			// textdata: data.string.exc_q1_1
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"opt-1",
				optdata:data.string.exc_opn_1
			},{
				optaddclass:"opt-2",
				optdata:data.string.exc_opn_2
			},{
				optaddclass:"opt-3",
				optdata:data.string.exc_opn_3
			}]
		}]
	},
	// slide2
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "title",
			textdata: data.string.exc_title
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"ans",
			textclass: "qstn",
			// textdata: data.string.exc_q1_1
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"opt-1",
				optdata:data.string.exc_opn_1
			},{
				optaddclass:"opt-2",
				optdata:data.string.exc_opn_2
			},{
				optaddclass:"opt-3",
				optdata:data.string.exc_opn_3
			}]
		}]
	},
	// slide3
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "title",
			textdata: data.string.exc_title
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"ans",
			textclass: "qstn",
			// textdata: data.string.exc_q1_1
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"opt-1",
				optdata:data.string.exc_opn_1_caps
			},{
				optaddclass:"opt-2",
				optdata:data.string.exc_opn_2_caps
			},{
				optaddclass:"opt-3",
				optdata:data.string.exc_opn_3_caps
			}]
		}]
	},
	// slide4
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "title",
			textdata: data.string.exc_title
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"ans",
			textclass: "qstn",
			// textdata: data.string.exc_q1_1
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"opt-1",
				optdata:data.string.exc_opn_1
			},{
				optaddclass:"opt-2",
				optdata:data.string.exc_opn_2
			},{
				optaddclass:"opt-3",
				optdata:data.string.exc_opn_3
			}]
		}]
	},
	// slide5
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "title",
			textdata: data.string.exc_title
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"ans",
			textclass: "qstn",
			// textdata: data.string.exc_q1_1
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"opt-1",
				optdata:data.string.exc_opn_1_caps
			},{
				optaddclass:"opt-2",
				optdata:data.string.exc_opn_2_caps
			},{
				optaddclass:"opt-3",
				optdata:data.string.exc_opn_3_caps
			}]
		}]
	},
	// slide6
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "title",
			textdata: data.string.exc_title
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"ans",
			textclass: "qstn",
			textdata: data.string.exc_q1_8
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"opt-1",
				optdata:data.string.exc_opn3_1
			},{
				optaddclass:"opt-2",
				optdata:data.string.exc_opn3_2
			},{
				optaddclass:"opt-3 class1",
				optdata:data.string.exc_opn3_3
			}]
		}]
	},
	// slide7
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "title",
			textdata: data.string.exc_title
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"ans",
			textclass: "qstn",
			textdata: data.string.exc_q1_9
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"opt-1",
				optdata:data.string.exc_opn3_1
			},{
				optaddclass:"opt-2 class1",
				optdata:data.string.exc_opn3_2
			},{
				optaddclass:"opt-3",
				optdata:data.string.exc_opn3_3
			}]
		}]
	},
	// slide8
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "title",
			textdata: data.string.exc_title
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"ans",
			textclass: "qstn",
			textdata: data.string.exc_q1_10
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"opt-1",
				optdata:data.string.exc_opn2_1
			},{
				optaddclass:"opt-2 class1",
				optdata:data.string.exc_opn2_2
			},{
				optaddclass:"opt-3",
				optdata:data.string.exc_opn2_3
			}]
		}]
	},
	// slide9
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "title",
			textdata: data.string.exc_title
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"ans",
			textclass: "qstn",
			textdata: data.string.exc_q1_11
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"opt-1 class1",
				optdata:data.string.exc_opn2_1
			},{
				optaddclass:"opt-2 ",
				optdata:data.string.exc_opn2_2
			},{
				optaddclass:"opt-3",
				optdata:data.string.exc_opn2_3
			}]
		}]
	},
	// slide10
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "title",
			textdata: data.string.exc_title
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"ans",
			textclass: "qstn",
			textdata: data.string.exc_q1_12
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optaddclass:"opt-1 ",
				optdata:data.string.exc_opn2_1
			},{
				optaddclass:"opt-2 ",
				optdata:data.string.exc_opn2_2
			},{
				optaddclass:"opt-3 class1",
				optdata:data.string.exc_opn2_3
			}]
		}]
	},
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;


	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	var scoring = new EggTemplate();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "i_am_never_late", src: imgpath+"i_am_never_late.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "t-1", src: 'images/textbox/white/tl-2.png', type: createjs.AbstractLoader.IMAGE},
			{id: "t-2", src: 'images/textbox/white/l-1-b.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "ex_ins", src: soundAssets + "ex_ins.ogg"}
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		scoring.init($total_page);
		templateCaller();
	}
	//initialize
	init();


	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	/*=====  End of data highlight function  ======*/

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templatecaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templatecaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// setTimeout(function(){
			// if(countNext == $total_page - 1)
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
			// }, 1000);
		}
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
	/**
	 How to:
	 - Just call instructionblockcontroller() from the template
	 */


	/**
	 What it does:
	 - It inserts and handles closing and opening of instruction block
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	var wrongclick = false;

	var qnArray_1 =  [[1,1],[2,3],[3,2],[4,3],[5,2],
									[6,2],[7,3]];
	// qnArray_1.shufflearray();
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		
		
		countNext == 0 ? sound_player('ex_ins') : '';
		$prevBtn.hide(0);

		no_of_incorr = 0;
		no_of_select = 0;
		total_count = $('.blank-space').length;

			$prevBtn.hide(0);
			$nextBtn.hide(0);
		switch (countNext) {
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
				$(".qstn").html(eval("data.string.exc_q1_"+qnArray_1[countNext][0]));
				$(".qstn").prepend((countNext+1)+". ");
				$(".opt-"+qnArray_1[countNext][1]).addClass("class1");
				texthighlight($board);

		break;
		default:
			texthighlight($board);
			$(".qstn").prepend((countNext+1)+". ");
		break;
		}
		$(".buttonsel").click(function(){
			if($(this).hasClass("class1")){
				$(this).css({
					"background-color":"#b5d02f",
					"border":"5px solid #ff0",
					"pointer-events":"none"
				});
				play_correct_incorrect_sound(1);
				$(".buttonsel").css("pointer-events","none");
				$(".ans").html($(this).text());
				$(this).siblings(".corctopt").show(0);
				!wrongclick?scoring.update(true):"";
				$nextBtn.show(0);
				wrongclick = false;
			}else {
				$(this).css({
					"background-color":"#f00",
					"border":"5px solid #ff8899",
					"pointer-events":"none"
				});
				$(this).siblings(".wrngopt").show(0);
				play_correct_incorrect_sound(0);
				scoring.update(false);
				wrongclick = true;
			}
		});

	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}

	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();
		generalTemplate();
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({
				'color' : 'red',
				"height" : "4.3vmin",
				"width" : "4.3vmin",
				"cursor" : "pointer",
				"text-align" : "center"
			});
		}
		function slides(i) {
			$($('.totalsequence')[i]).click(function() {
				countNext = i;
				templateCaller();
			});
		}
		*/
		loadTimelineProgress($total_page,countNext+1);
	}

	$nextBtn.on('click', function() {
		countNext++;
		scoring.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
});

var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/";

var imgpath = $ref + "/images/";

var content = [
  {
    contentblockadditionalclass: "bg_blue",
    uppertextblock: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "yellow",
        textdata: data.string.p3text2,
        textclass: "ideatext"
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "talker",
            imgsrc: "",
            imgid: "talking1"
          },
          {
            imgclass: "talker hidethis",
            imgsrc: "",
            imgid: "talking"
          }
        ]
      }
    ]
  },
  // slide0
  {
    contentblockadditionalclass: "bg_blue",
    uppertextblock: [
      {
        textdata: data.string.p3text3,
        textclass: "description1"
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "talker",
            imgsrc: "",
            imgid: "talking"
          },
          {
            imgclass: "alarmclock",
            imgsrc: "",
            imgid: "alarmclock"
          }
        ]
      }
    ]
  },
  //slide2
  {
    contentblockadditionalclass: "bg_blue",
    uppertextblock: [
      {
        textdata: data.string.p3text5,
        textclass: "t1"
      },
      {
        textdata: data.string.p3text6,
        textclass: "t2"
      },
      {
        textdata: data.string.p3text7,
        textclass: "t3"
      },
      {
        textdata: data.string.p3text8,
        textclass: "t4"
      },
      {
        textdata: data.string.p3text9,
        textclass: "t5"
      },
      {
        textdata: data.string.p3text10,
        textclass: "t6"
      },
      {
        textdata: data.string.p3text11,
        textclass: "t7"
      },
      {
        textdata: data.string.p3text4,
        textclass: "toptext"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "table1",
            imgsrc: "",
            imgid: "table1"
          },
          {
            imgclass: "boy1",
            imgsrc: "",
            imgid: "boy1"
          }
        ]
      }
    ]
  },
  //slide3
  {
    contentblockadditionalclass: "bg_blue",
    uppertextblock: [
      {
        textdata: data.string.p3text5,
        textclass: "t1"
      },
      {
        textdata: data.string.p3text6,
        textclass: "t2"
      },
      {
        textdata: data.string.p3text7,
        textclass: "t3"
      },
      {
        textdata: data.string.p3text8,
        textclass: "t4"
      },
      {
        textdata: data.string.p3text9,
        textclass: "t5"
      },
      {
        textdata: data.string.p3text10,
        textclass: "t6"
      },
      {
        textdata: data.string.p3text11,
        textclass: "t7"
      },
      {
        textdata: data.string.p3text13,
        textclass: "toptext"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "table1",
            imgsrc: "",
            imgid: "table2"
          },
          {
            imgclass: "boy1",
            imgsrc: "",
            imgid: "boy1"
          }
        ]
      }
    ]
  },
  //slide4
  {
    contentblockadditionalclass: "bg_blue",
    uppertextblock: [
      {
        textdata: data.string.p3text5,
        textclass: "t1"
      },
      {
        textdata: data.string.p3text6,
        textclass: "t2"
      },
      {
        textdata: data.string.p3text7,
        textclass: "t3"
      },
      {
        textdata: data.string.p3text8,
        textclass: "t4"
      },
      {
        textdata: data.string.p3text9,
        textclass: "t5"
      },
      {
        textdata: data.string.p3text10,
        textclass: "t6"
      },
      {
        textdata: data.string.p3text11,
        textclass: "t7"
      },
      {
        textdata: data.string.p3text13,
        textclass: "toptext"
      },
      {
        textdata: data.string.p3text14,
        textclass: "bottext"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "table1",
            imgsrc: "",
            imgid: "table2"
          },
          {
            imgclass: "boy1",
            imgsrc: "",
            imgid: "boy1"
          }
        ]
      }
    ]
  },
  //slide5
  {
    contentblockadditionalclass: "bg_blue",
    uppertextblock: [
      {
        textdata: data.string.p3text5,
        textclass: "t1"
      },
      {
        textdata: data.string.p3text6,
        textclass: "t2"
      },
      {
        textdata: data.string.p3text7,
        textclass: "t3"
      },
      {
        textdata: data.string.p3text8,
        textclass: "t4"
      },
      {
        textdata: data.string.p3text9,
        textclass: "t5"
      },
      {
        textdata: data.string.p3text10,
        textclass: "t6"
      },
      {
        textdata: data.string.p3text11,
        textclass: "t7"
      },
      {
        textdata: data.string.p3text15,
        textclass: "toptext"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "table1",
            imgsrc: "",
            imgid: "table4"
          },
          {
            imgclass: "maam",
            imgsrc: "",
            imgid: "maam"
          }
        ]
      }
    ]
  },
  //slide6
  {
    contentblockadditionalclass: "bg_blue",
    uppertextblock: [
      {
        textdata: data.string.p3text5,
        textclass: "t1"
      },
      {
        textdata: data.string.p3text6,
        textclass: "t2"
      },
      {
        textdata: data.string.p3text7,
        textclass: "t3"
      },
      {
        textdata: data.string.p3text8,
        textclass: "t4"
      },
      {
        textdata: data.string.p3text9,
        textclass: "t5"
      },
      {
        textdata: data.string.p3text10,
        textclass: "t6"
      },
      {
        textdata: data.string.p3text11,
        textclass: "t7"
      },
      {
        textdata: data.string.p3text16,
        textclass: "toptext smalltext"
      },
      {
        textdata: data.string.p3text17,
        textclass: "bottext"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "table1",
            imgsrc: "",
            imgid: "table3"
          },
          {
            imgclass: "maam",
            imgsrc: "",
            imgid: "maam"
          }
        ]
      }
    ]
  },
  //slide7
  {
    contentblockadditionalclass: "bg_blue",
    uppertextblock: [
      {
        textdata: data.string.p3text18,
        textclass: "description1"
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "talker",
            imgsrc: "",
            imgid: "talking"
          },
          {
            imgclass: "alarmclock",
            imgsrc: "",
            imgid: "alarmclock"
          }
        ]
      }
    ]
  },
  //slide8
  {
    uppertextblock: [
      {
        textdata: data.string.p3text19,
        textclass: "toptext1"
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "full_bg",
            imgsrc: "",
            imgid: "grandpa"
          }
        ]
      }
    ]
  },
  //slide9
  {
    uppertextblock: [
      {
        textdata: data.string.p3text20,
        textclass: "toptext1"
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "full_bg",
            imgsrc: "",
            imgid: "prem"
          }
        ]
      }
    ]
  },
  //slide10
  {
    uppertextblock: [
      {
        textdata: data.string.p3text21,
        textclass: "toptext1"
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "full_bg",
            imgsrc: "",
            imgid: "park"
          }
        ]
      }
    ]
  },
  //slide11
  {
    uppertextblock: [
      {
        textdata: data.string.p3text22,
        textclass: "toptext1"
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "full_bg",
            imgsrc: "",
            imgid: "monkeybg"
          },
          {
            imgclass: "monkeysleep",
            imgsrc: "",
            imgid: "monkeynew"
          }
        ]
      }
    ]
  }
];

$(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page, countNext + 1);

  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();

  var preload;
  var timeoutvar = null;

  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      // {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
      // {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
      //   ,
      { id: "data-xml", src: "data.xml", type: createjs.AbstractLoader.XML },
      //images
      {
        id: "talking1",
        src: imgpath + "talking.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "talking",
        src: imgpath + "talking-gif.gif",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "alarmclock",
        src: imgpath + "alarmclock.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "table1",
        src: imgpath + "table01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "table2",
        src: imgpath + "table02.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "table3",
        src: imgpath + "table03.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "table4",
        src: imgpath + "table04.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "shishir",
        src: imgpath + "table03.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "maam",
        src: imgpath + "didi.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "grandpa",
        src: imgpath + "grandpa.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "prem",
        src: imgpath + "ghantaghar.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "park",
        src: imgpath + "park.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "monkey",
        src: imgpath + "monkey_on_tree.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "monkeynew",
        src: imgpath + "monkey_sleeping.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "monkeybg",
        src: imgpath + "bg_monkey_sleeping.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "boy1",
        src: imgpath + "prem.png",
        type: createjs.AbstractLoader.IMAGE
      },

      // sounds
      { id: "s3_p1", src: soundAsset + "s3_p1.ogg" },
      { id: "s3_p2", src: soundAsset + "s3_p2.ogg" },
      { id: "s3_p3", src: soundAsset + "s3_p3.ogg" },
      { id: "s3_p4", src: soundAsset + "s3_p4.ogg" },
      { id: "s3_p5", src: soundAsset + "s3_p5.ogg" },
      { id: "s3_p6", src: soundAsset + "s3_p6.ogg" },
      { id: "s3_p7", src: soundAsset + "s3_p7.ogg" },
      { id: "s3_p8", src: soundAsset + "s3_p8.ogg" },
      { id: "s3_p9", src: soundAsset + "s3_p9.ogg" },
      { id: "s3_p10", src: soundAsset + "s3_p10.ogg" },
      { id: "s3_p11", src: soundAsset + "s3_p11.ogg" },
      { id: "s3_p12", src: soundAsset + "s3_p12.ogg" }
    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound); //for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }
  function handleFileLoad(event) {
    // console.log(event.item);
  }
  function handleProgress(event) {
    $("#loading-text").html(parseInt(event.loaded * 100) + "%");
  }
  function handleComplete(event) {
    $("#loading-wrapper").hide(0);
    //initialize varibales
    // call main function
    templateCaller();
  }
  //initialize
  init();

  /*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  /*===============================================
	=            data highlight function            =
	===============================================*/
  /**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */
  function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object"
      ? alert(
          "Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted"
        )
      : null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightstarttag2;
    var texthighlightstarttag3;
    var texthighlightendtag = "</span>";
    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
        $(this).attr(
          "data-highlightcustomclass"
        ) /*if there is data-highlightcustomclass defined it is true else it is not*/
          ? (stylerulename = $(this).attr("data-highlightcustomclass"))
          : (stylerulename = "parsedstring");

        $(this).attr(
          "data-highlightcustomclass2"
        ) /*if there is data-highlightcustomclass defined it is true else it is not*/
          ? (stylerulename2 = $(this).attr("data-highlightcustomclass2"))
          : (stylerulename2 = "parsedstring2");

        $(this).attr(
          "data-highlightcustomclass3"
        ) /*if there is data-highlightcustomclass defined it is true else it is not*/
          ? (stylerulename3 = $(this).attr("data-highlightcustomclass3"))
          : (stylerulename3 = "parsedstring3");

        texthighlightstarttag = "<span class='" + stylerulename + "'>";
        texthighlightstarttag2 = "<span class='" + stylerulename2 + "'>";
        texthighlightstarttag3 = "<span class='" + stylerulename3 + "'>";
        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

        replaceinstring = replaceinstring.replace(/%/g, texthighlightstarttag2);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

        replaceinstring = replaceinstring.replace(/!/g, texthighlightstarttag3);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
        $(this).html(replaceinstring);
      });
    }
  }

  /*=====  End of data highlight function  ======*/

  /*===============================================
	 =            user notification function        =
	 ===============================================*/
  function notifyuser($notifyinside) {
    //check if $notifyinside is provided
    typeof $notifyinside !== "object"
      ? alert(
          "Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style."
        )
      : null;

    /*variable that will store the element(s) to remove notification from*/
    var $allnotifications = $notifyinside.find(
      "*[data-usernotification='notifyuser']"
    );
    // if there are any notifications removal required add the event handler
    if ($allnotifications.length > 0) {
      $allnotifications.one("click", function() {
        /* Act on the event */
        $(this).attr("data-isclicked", "clicked");
        $(this).removeAttr("data-usernotification");
      });
    }
  }

  /*=====  End of user notification function  ======*/

  /*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

  function navigationcontroller(islastpageflag) {
    typeof islastpageflag === "undefined"
      ? (islastpageflag = false)
      : typeof islastpageflag != "boolean"
      ? alert(
          "NavigationController : Hi Master, please provide a boolean parameter"
        )
      : null;

    // islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
  }

  /*=====  End of user navigation controller function  ======*/

  /*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

  function instructionblockcontroller() {
    var $instructionblock = $board.find("div.instructionblock");
    if ($instructionblock.length > 0) {
      var $contentblock = $board.find("div.contentblock");
      var $toggleinstructionblockbutton = $instructionblock.find(
        "div.toggleinstructionblock"
      );
      var instructionblockisvisibleflag;

      $contentblock.css("pointer-events", "none");

      $toggleinstructionblockbutton.on("click", function() {
        instructionblockisvisibleflag = $instructionblock.attr(
          "data-instructionblockshow"
        );
        if (instructionblockisvisibleflag == "true") {
          instructionblockisvisibleflag = "false";
          $contentblock.css("pointer-events", "auto");
        } else if (instructionblockisvisibleflag == "false") {
          instructionblockisvisibleflag = "true";
          $contentblock.css("pointer-events", "none");
        }

        $instructionblock.attr(
          "data-instructionblockshow",
          instructionblockisvisibleflag
        );
      });
    }
  }

  /*=====  End of InstructionBlockController  ======*/

  /*=================================================
	 =            general template function            =
	 =================================================*/
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    vocabcontroller.findwords(countNext);
    put_image(content, countNext);
    put_speechbox_image(content, countNext);
    switch (countNext) {
      case 0:
        sound_player_nav("s3_p" + (countNext + 1));
        // nav_button_controls(2000);
        $prevBtn.hide(0);
        break;
      default:
        $prevBtn.show(0);
        sound_player_nav("s3_p" + (countNext + 1));
        // nav_button_controls(500);
        break;
    }
  }
  function nav_button_controls(delay_ms) {
    timeoutvar = setTimeout(function() {
      if (countNext == 0) {
        $nextBtn.show(0);
      } else if (countNext > 0 && countNext == $total_page - 1) {
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else {
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    }, delay_ms);
  }

  function sound_player(sound_id) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
  }
  function sound_player_nav(sound_id) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on("complete", function() {
      nav_button_controls(100);
      $(".hidethis").hide();
    });
  }

  function put_image(content, count) {
    if (content[count].hasOwnProperty("imageblock")) {
      var imageblock = content[count].imageblock[0];
      if (imageblock.hasOwnProperty("imagestoshow")) {
        var imageClass = imageblock.imagestoshow;
        for (var i = 0; i < imageClass.length; i++) {
          var image_src = preload.getResult(imageClass[i].imgid).src;
          //get list of classes
          var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
          var selector = "." + classes_list[classes_list.length - 1];
          $(selector).attr("src", image_src);
        }
      }
    }
  }
  function put_speechbox_image(content, count) {
    if (content[count].hasOwnProperty("speechbox")) {
      var speechbox = content[count].speechbox;
      for (var i = 0; i < speechbox.length; i++) {
        var image_src = preload.getResult(speechbox[i].imgid).src;
        //get list of classes
        var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
        var selector =
          "." + classes_list[classes_list.length - 1] + ">.speechbg";
        // console.log(selector);
        $(selector).attr("src", image_src);
      }
    }
  }
  function templateCaller() {
    $prevBtn.css("display", "none");
    $nextBtn.css("display", "none");

    navigationcontroller();

    generaltemplate();
    loadTimelineProgress($total_page, countNext + 1);
    /*
		  // for (var i = 0; i < content.length; i++) {
		  //   slides(i);
		  //   $($('.totalsequence')[i]).html(i);
		  //   $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  // "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  // }
		  // function slides(i){
		  //     $($('.totalsequence')[i]).click(function(){
		  //       countNext = i;
		  //       templateCaller();
		  //     });
		  //   }
		*/
  }

  $nextBtn.on("click", function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    switch (countNext) {
      default:
        countNext++;
        templateCaller();
        break;
    }
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    countNext--;
    templateCaller();
    /* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
    countNext < $total_page
      ? ole.footerNotificationHandler.hideNotification()
      : null;
  });
});

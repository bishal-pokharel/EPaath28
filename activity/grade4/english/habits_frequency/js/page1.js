var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentblockadditionalclass: 'purplebg',
		uppertextblock:[{
			textdata: data.lesson.chapter,
			textclass: "chaptertitle",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "brushingboy",
					imgsrc : '',
					imgid : 'brush'
				},
				{
					imgclass : "skipping",
					imgsrc : '',
					imgid : 'skipping'
				},
				{
					imgclass : "washinghands",
					imgsrc : '',
					imgid : 'washinghands'
				}
			]
		}]
	},
	// slide1
	{
		contentblockadditionalclass: 'bluebg',
		uppertextblock:[{
			textdata: data.string.p1text1,
			textclass: "ducktext",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "duck",
					imgsrc : '',
					imgid : 'duckpoints'
				}
			]
		}]

	},
	// slide2
	{
		contentblockadditionalclass: 'bluebg',
		uppertextblock:[{
			textdata: data.string.p1text2,
			textclass: "textattop",
		},
		{
			textdata: data.string.p1text6,
			textclass: "bottext",
		}],
        flipcontainer: [
        {
            sectioncontainer: "section s0",
            topfront: data.string.p1text3,
            backtext: data.string.p1text7,
            toptextclass: "toptext",
            buttontextclass: "buttontext",
            frontclass: 'front1',
            backclass: 'back1'

        },
        {
            sectioncontainer: "section s1",
            topfront: data.string.p1text4,
            backtext: data.string.p1text8,
            toptextclass: "toptext t1",
            buttontextclass: "buttontext",
            frontclass: 'front1 f1',
            backclass: 'back1 b1'

        },
        {
            sectioncontainer: "section s2",
            topfront: data.string.p1text5,
            backtext: data.string.p1text9,
            toptextclass: "toptext t2",
            buttontextclass: "buttontext",
            frontclass: 'front1 f2',
            backclass: 'back1 f3'

        },
        ],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "junkfood_image",
					imgsrc : '',
					imgid : 'junkfood'
				},
				{
					imgclass : "homework_image",
					imgsrc : '',
					imgid : 'homeworkkid'
				},
				{
					imgclass : "always_image",
					imgsrc : '',
					imgid : 'brush'
				}
			]
		}]
	},
	// slide3
	{
		contentblockadditionalclass: 'bluebg',
		uppertextblock:[{
			textdata: data.string.p1text3,
			textclass: "nevertext",
		},
		{
			textdata: data.string.p1text4,
			textclass: "sometimetext",
		},
		{
			textdata: data.string.p1text5,
			textclass: "alwaystime",
		},
		{
			textdata: data.string.p1text11,
			textclass: "never",
		},
		{
			textdata: data.string.p1text12,
			textclass: "sometimes",
		},
		{
			textdata: data.string.p1text13,
			textclass: "always",
		},
		{
			textdata: data.string.p1text10,
			textclass: "summary",
		},
		{
			textdata: data.string.p1text14,
			textclass: "zero",
		},
		{
			textdata: data.string.p1text15,
			textclass: "hundred",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "box",
					imgsrc : '',
					imgid : 'box'
				}
			]
		}]
	},

];

/*
* bug fixed by swikar
*/
$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "brush", src: imgpath+"brushingteeth.png", type: createjs.AbstractLoader.IMAGE},
			{id: "skipping", src: imgpath+"playingskipping.png", type: createjs.AbstractLoader.IMAGE},
			{id: "washinghands", src: imgpath+"washing-hand.png", type: createjs.AbstractLoader.IMAGE},
			{id: "duckpoints", src: imgpath+"pointing.png", type: createjs.AbstractLoader.IMAGE},
			{id: "junkfood", src: imgpath+"junk-food.png", type: createjs.AbstractLoader.IMAGE},
			{id: "homeworkkid", src: imgpath+"homework.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girlwithglass", src: imgpath+"wears-glasses.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box", src: imgpath+"boxes.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
			{id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
			{id: "s1_p4", src: soundAsset+"s1_p4.ogg"},
			{id: "s1_p3_always", src: soundAsset+"s1_p3_always.ogg"},
			{id: "s1_p3_never", src: soundAsset+"s1_p3_never.ogg"},
			{id: "s1_p3_sometimes", src: soundAsset+"s1_p3_sometimes.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext) {
			case 0:
				sound_player_nav('s1_p'+(countNext+1));
			break;
			case 1:
				sound_player_nav('s1_p'+(countNext+1));
			break;
			case 2:
			var count = 0;
			$nextBtn.hide(0);
			$prevBtn.show(0);
			$(".bottext").hide(0);
			$(".section").css("pointer-events","none");
				createjs.Sound.stop();
				current_sound = createjs.Sound.play('s1_p'+(countNext+1));
				current_sound.play();
				current_sound.on('complete',function(){
					$(".section").css("pointer-events","auto");
				});
						$(".section, .bottext").delay(5200).fadeIn(200);
	            $(".card").click(function () {
	               $(this).toggleClass("flipped");
	          	 });
	            $('.s0').click(function () {
            		$('.junkfood_image').animate({"opacity":"1"});
            		$('.bottext').animate({"opacity":"0"});
									createjs.Sound.stop();
									current_sound = createjs.Sound.play("s1_p3_never");
									current_sound.play();
									// $(".section").css("pointer-events","none");
	            	count++;
								current_sound.on('complete', function(){
									$(".section").css("pointer-events","auto");
									// $(".s0").css("pointer-events","none");
	            		if(count>=3)
		            		{
											$nextBtn.show(0);
										}
								});
	            });
	            $('.s1').click(function () {
            		$('.homework_image').animate({"opacity":"1"});
            		$('.bottext').animate({"opacity":"0"});
									createjs.Sound.stop();
									current_sound = createjs.Sound.play("s1_p3_sometimes");
									current_sound.play();
									// $(".section").css("pointer-events","none");
	            	count++;
								current_sound.on('complete', function(){
									$(".section").css("pointer-events","auto");
									// $(".s1").css("pointer-events","none");
	            		if(count>=3)
		            		{
											$nextBtn.show(0);
										}
								});
	            });
	            $('.s2').click(function () {
            		$('.always_image').animate({"opacity":"1"});
            		$('.bottext').animate({"opacity":"0"});
									createjs.Sound.stop();
									current_sound = createjs.Sound.play("s1_p3_always");
									current_sound.play();
									// $(".section").css("pointer-events","none");
	            	count++;
								current_sound.on('complete', function(){
									$(".section").css("pointer-events","auto");
									// $(".s2").css("pointer-events","none");
	            		if(count>=3)
		            		{
											$nextBtn.show(0);
										}
								});
	            });
				break;
			case 3:
				sound_player_nav('s1_p'+(countNext+1));
				$prevBtn.show(0);
				break;

			default:
				$prevBtn.show(0);
				sound_player('sound_2');
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete',function(){
			nav_button_controls(100);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

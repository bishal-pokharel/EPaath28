var imgpath = $ref + "/images/diy/";
var soundAsset = $ref+"/sounds/p4/";
var a = [false,''];
var b = [false,''];
var c = [false,''];
var d = [false,''];
var e = [false,''];
var f = [false,''];
var g = [false,''];
var h = [false,''];
var i = [false,''];
var j = [false,''];


var content=[
	// slide0
	{
		contentblockadditionalclass: 'bg_blue',
		uppertextblock:[{
			textdata: data.string.diytext,
			textclass: "title",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "diyrabbit",
					imgsrc : '',
					imgid : 'diy'
				},
			]
		}],
	},
	// slide1
	{
		contentblockadditionalclass: 'bg_blue',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "diynamerabbit",
					imgsrc : '',
					imgid : 'entername'
				},
			]
		}],
		uppertextblock:[{
			textdata: data.string.submittext,
			textclass: "submitbutton",
		}],
		textinputbox:[{}]
	},
	// slide2
	{
		contentblockadditionalclass: 'bg_blue',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "exercise",
					imgsrc : '',
					imgid : 'exercise'
				},
				{
					imgclass : "reading",
					imgsrc : '',
					imgid : 'reading'
				},
				{
					imgclass : "yoga",
					imgsrc : '',
					imgid : 'yoga'
				},
				{
					imgclass : "brush",
					imgsrc : '',
					imgid : 'brush'
				},
				{
					imgclass : "washinghands",
					imgsrc : '',
					imgid : 'washinghands'
				},
				{
					imgclass : "coveringmouth",
					imgsrc : '',
					imgid : 'coveringmouth'
				},
				{
					imgclass : "greeting",
					imgsrc : '',
					imgid : 'greeting'
				},
				{
					imgclass : "sharing",
					imgsrc : '',
					imgid : 'sharing'
				},
				{
					imgclass : "bedtime",
					imgsrc : '',
					imgid : 'bedtime'
				},
				{
					imgclass : "helpingothers",
					imgsrc : '',
					imgid : 'helpingothers'
				}
			],
		imagelabels:[{
			imagelabeldata: data.string.p6text2,
			imagelabelclass: "exercise_text",
		},{
			imagelabeldata: data.string.p6text3,
			imagelabelclass: "reading_text",
		},{
			imagelabeldata: data.string.p6text4,
			imagelabelclass: "yoga_text",
		},{
			imagelabeldata: data.string.p6text5,
			imagelabelclass: "brush_text",
		},{
			imagelabeldata: data.string.p6text6,
			imagelabelclass: "washinghands_text",
		},{
			imagelabeldata: data.string.p6text7,
			imagelabelclass: "coveringmouth_text",
		},{
			imagelabeldata: data.string.p6text8,
			imagelabelclass: "greeting_text",
		},{
			imagelabeldata: data.string.p6text9,
			imagelabelclass: "sharing_text",
		},{
			imagelabeldata: data.string.p6text10,
			imagelabelclass: "bedtime_text",
		},{
			imagelabeldata: data.string.p6text11,
			imagelabelclass: "helpingothers_text",
		},{
			imagelabeldata: data.string.p6text13,
			imagelabelclass: "instruction",
		},],
		}],

		radioselection:[{
			radio:'radio1',
			questiontitle:data.string.p6text14,
			frequency1:'a1',
			frequency2:'a2',
			frequency3:'a3',
			frequency4:'a4'
		},{
			radio:'radio2',
			questiontitle:data.string.p6text14,
			frequency1:'b1',
			frequency2:'b2',
			frequency3:'b3',
			frequency4:'b4'
		},{
			radio:'radio3',
			questiontitle:data.string.p6text14,
			frequency1:'c1',
			frequency2:'c2',
			frequency3:'c3',
			frequency4:'c4'
		},{
			radio:'radio4',
			questiontitle:data.string.p6text14,
			frequency1:'d1',
			frequency2:'d2',
			frequency3:'d3',
			frequency4:'d4'
		},{
			radio:'radio5',
			questiontitle:data.string.p6text14,
			frequency1:'e1',
			frequency2:'e2',
			frequency3:'e3',
			frequency4:'e4'
		},{
			radio:'radio6',
			questiontitle:data.string.p6text14,
			frequency1:'f1',
			frequency2:'f2',
			frequency3:'f3',
			frequency4:'f4'
		},{
			radio:'radio7',
			questiontitle:data.string.p6text14,
			frequency1:'g1',
			frequency2:'g2',
			frequency3:'g3',
			frequency4:'g4'
		},{
			questiontitle:data.string.p6text14,
			frequency1:'i1',
			frequency2:'i2',
			frequency3:'i3',
			frequency4:'i4',
			radio:'radio8',
		},{
			questiontitle:data.string.p6text14,
			frequency1:'j1',
			frequency2:'j2',
			frequency3:'j3',
			frequency4:'j4',
			radio:'radio9',
		},{
			questiontitle:data.string.p6text14,
			frequency1:'k1',
			frequency2:'k2',
			frequency3:'k3',
			frequency4:'k4',
			radio:'radio10',
		}]

	},
	//slide3
	{
		contentblockadditionalclass: 'bg_blue',
		uppertextblock:[{
			textdata: data.string.p6text12,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "diynamerabbit",
					imgsrc : '',
					imgid : 'entername'
				},
			]
		}],

	}

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "diy", src: imgpath+"diy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "entername", src: imgpath+"enteryourname.png", type: createjs.AbstractLoader.IMAGE},
			{id: "exercise", src: imgpath+"01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "reading", src: imgpath+"02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "yoga", src: imgpath+"05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "brush", src: imgpath+"03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "washinghands", src: imgpath+"04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "coveringmouth", src: imgpath+"11.png", type: createjs.AbstractLoader.IMAGE},
			{id: "greeting", src: imgpath+"09.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sharing", src: imgpath+"07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "helpingothers", src: imgpath+"08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bedtime", src: imgpath+"10.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "t-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-3", src: 'images/textbox/white/l-1-b.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			// {id: "sound_1", src: soundAsset+"1.ogg"},
			// {id: "sound_2", src: soundAsset+"2.ogg"},
			// {id: "sound_3", src: soundAsset+"3.ogg"},
			// {id: "sound_4", src: soundAsset+"4.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_option_image(content, countNext);
		put_speechbox_image(content, countNext);

		switch(countNext) {
			case 0:
				play_diy_audio();
				$('.title').addClass('blurin');
				$nextBtn.show(0);
				break;
			case 1:
				name = '';
				$nextBtn.hide(0);
				$('.submitbutton').click(function(){
					if($('#nametext').val()!='') {
                        $nextBtn.show(0);
                        name = $('#nametext').val();
                        $('#inputtext').css("pointer-events", "none");
                    }
				});
				input_box('#nametext','.submitbutton');
				break;
			case 2:
				selection("exercise",a,'radio1','a');
				selection("reading",b,'radio2','b');
				selection("yoga",c,'radio3','c');
				selection("brush",d,'radio4','d');
				selection("washinghands",e,'radio5','e');
				selection("coveringmouth",f,'radio6','f');
				selection("greeting",g,'radio7','g');
				selection("sharing",h,'radio8','i');
				selection("bedtime",i,'radio9','j');
				selection("helpingothers",j,'radio10','k');
				break;
			case 3:
				$('.name').html(name);
				if(a[0]==true){
					$('.a1').css({"display":"list-item"});

						if(a[1]=="."){
							$('.sometimes').html("sometimes");
							$('.a1').append(".");
						}
						else{
							$('.a').html(a[1]);
						}
				}

				if(b[0]==true){
					$('.b1').css({"display":"list-item"});

						if(b[1]=="."){
							$('.sometimes1').html("sometimes");
							$('.b1').append(".");
						}
						else{
							$('.b').html(b[1]);
						}
				}

				if(c[0]==true){
					$('.c1').css({"display":"list-item"});

						if(c[1]=="."){
							$('.sometimes2').html("sometimes");
							$('.c1').append(".");
						}
						else{
							$('.c').html(c[1]);
						}
				}

				if(d[0]==true){
					$('.d1').css({"display":"list-item"});

						if(d[1]=="."){
							$('.sometimes3').html("sometimes");
							$('.d1').append(".");
						}
						else{
							$('.d').html(d[1]);
						}
				}

				if(e[0]==true){
					$('.e1').css({"display":"list-item"});

						if(e[1]=="."){
							$('.sometimes4').html("sometimes");
							$('.e1').append(".");
						}
						else{
							$('.e').html(e[1]);
						}
				}

				if(f[0]==true){
					$('.f1').css({"display":"list-item"});

						if(f[1]=="."){
							$('.sometimes5').html("sometimes");
							$('.f1').append(".");
						}
						else{
							$('.f').html(f[1]);
						}
				}
				if(g[0]==true){
					$('.g1').css({"display":"list-item"});

						if(g[1]=="."){
							$('.sometimes6').html("sometimes");
							$('.g1').append(".");
						}
						else{
							$('.g').html(g[1]);
						}
				}
				if(h[0]==true){
					$('.ha1').css({"display":"list-item"});

						if(h[1]=="."){
							$('.sometimes7').html("sometimes");
							$('.ha1').append(".");
						}
						else{
							$('.h').html(h[1]);
						}
				}

				if(i[0]==true){
					$('.i1').css({"display":"list-item"});

						if(i[1]=="."){
							$('.sometimes8').html("sometimes");
							$('.i1').append(".");
						}
						else{
							$('.i').html(i[1]);
						}
				}

				if(j[0]==true){
					$('.j1').css({"display":"list-item"});

						if(j[1]=="."){
							$('.sometimes9').html("sometimes");
							$('.j1').append(".");
						}
						else{
							$('.j').html(j[1]);
						}
				}
				ole.footerNotificationHandler.pageEndSetNotification();
				break;
			default:
				$prevBtn.show(0);
				nav_button_controls(100);
				break;
		}
			function selection(class_name,array,radionumber,formidinitials){
				$('.'+ class_name).click(function(){
					if(class_name == "exercise")
						{
							a[0] = true;
						}
					if(class_name == "reading")
						{
							b[0] = true;
						}
					if(class_name == "yoga")
						{
							c[0] = true;
						}
					if(class_name == "brush")
						{
							d[0] = true;
						}
					if(class_name == "washinghands")
						{
							e[0] = true;
						}
					if(class_name == "coveringmouth")
						{
							f[0] = true;
						}
					if(class_name == "greeting")
						{
							g[0] = true;
						}
					if(class_name == "sharing")
						{
							h[0] = true;
						}
					if(class_name == "bedtime")
						{
							i[0] = true;
						}
					if(class_name == "helpingothers")
						{
							j[0] = true;
						}
					$('.'+ class_name).css({"background":"green","pointer-events":"none"});
					$(".coverboardfull > *:not(."+radionumber+")").css({ "filter":" grayscale(100%)","pointer-events":"none"});
					$('.contentblock').css({"background":"gray"});
					$('.'+radionumber).css({"display":"block","pointer-events":"auto"}).removeClass('fadeout').addClass('fadein');

					$("input[id="+"'"+formidinitials+"1']").click(function(){
							array[1]=$('#'+formidinitials+'1').val();
							$('.'+radionumber).css({"pointer-events":"none"}).addClass('fadeout');
							$nextBtn.show(0);
						 	$(this).attr('checked', false);
							$(".coverboardfull > *").not(".radio img").css({ "filter":" grayscale(0%)","pointer-events":"auto"});
							$('.contentblock').css({"background":"rgb(204,234,242)"});
					});
					$("input[id="+"'"+formidinitials+"2']").click(function(){
							array[1]=$('#'+formidinitials+'2').val();
							$('.'+radionumber).css({"pointer-events":"none"}).addClass('fadeout');
							$nextBtn.show(0);
						 	$(this).attr('checked', false);
							$(".coverboardfull > *").not(".radio img").css({ "filter":" grayscale(0%)","pointer-events":"auto"});
							$('.contentblock').css({"background":"rgb(204,234,242)"});
					});
					$("input[id="+"'"+formidinitials+"3']").click(function(){
							array[1]=$('#'+formidinitials+'3').val();
							$('.'+radionumber).css({"pointer-events":"none"}).addClass('fadeout');
							$nextBtn.show(0);
						 	$(this).attr('checked', false);
							$(".coverboardfull > *").not(".radio img").css({ "filter":" grayscale(0%)","pointer-events":"auto"});
							$('.contentblock').css({"background":"rgb(204,234,242)"});
					});
					$("input[id="+"'"+formidinitials+"4']").click(function(){
							array[1]=$('#'+formidinitials+'4').val();
							$('.'+radionumber).css({"pointer-events":"none"}).addClass('fadeout');
							$nextBtn.show(0);
						 	$(this).attr('checked', false);
							$(".coverboardfull > *").not(".radio img").css({ "filter":" grayscale(0%)","pointer-events":"auto"});
							$('.contentblock').css({"background":"rgb(204,234,242)"});
					});
				});
			}
	}

		function input_box(input_class, button_class) {
				$(input_class).keydown(function(event){
						var charCode = (event.which) ? event.which : event.keyCode;
						/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
						if(charCode === 13 && button_class!=null) {
								$(button_class).trigger("click");
					}
					var condition = charCode != 8 && charCode != 16 && charCode != 20 && (charCode < 37 || charCode > 40) && charCode != 46;
					//check if user inputs del, shift, caps , backspace or arrow keys
						if (!condition) {
							return true;
						}
						//check if user inputs more than one '.'
					if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
								return false;
						}
						//check . and 0-9 separately after checking arrow and other keys
						if((charCode < 65 || charCode > 90)){
							return false;
						}
						return true;
				});
				$(input_class).keyup(function(event){
		    		if (String(event.target.value).length >= 1) {
		    			$(".sbmtbtn").show(0);
		    			$(button_class).show(0);
		    			global_save_val = String(event.target.value);
		    		}
		    		else{
		    			$(".sbmtbtn").hide(0);
		    			$(button_class).hide(0);
		    		}
		  			return true;
				});
			}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/

	 /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		CODE MODIFIED FOR THREE DIFFERENT HIGHTLIGHTS
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
		function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightstarttag2;
        var texthighlightstarttag3;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
              $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

              $(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
              (stylerulename2 = "parsedstring2") ;

              $(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
              (stylerulename3 = "parsedstring3") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
            texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
		/*=====  End of data highlight function  ======*/
	function sound_play_click(sound_id, click_class){
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_option_image(content, count){
		if(content[count].hasOwnProperty('option')){
			var imageClass = content[count].option;
			console.log(imageClass);
			for(var i=0; i<imageClass.length; i++){
				var image_src = preload.getResult(imageClass[i].imgid).src;
				//get list of classes
				var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				// console.log(selector);
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
			/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

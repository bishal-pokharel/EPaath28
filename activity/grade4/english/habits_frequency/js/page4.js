var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentblockadditionalclass:'pink_bg',
		uppertextblock:[{
			textdata: data.string.diytext,
			textclass: "diytitle",
		}]
	},
	// slide1
	{
		contentblockadditionalclass:'ole-background-gradient-idontknow',
		mainsortbox:[{
		sortbox:'box1',
		questiontext: data.string.p4ques,
		optionsdiv:'optionsdiv',
				sortoptions: [
					{
						optid: "1",
						optclass:'a',
						listitem: data.string.p4text1,
					},
					{
						optid: "2",
						optclass:'b',
						listitem: data.string.p4text2,
					},
					{
						optid: "3",
						optclass:'c',
						listitem: data.string.p4text3,
					},
					{
						optclass:'d',
						listitem: data.string.p4text4,
						optid: "4",
					},
					{
						optid: "5",
						optclass:'e',
						listitem: data.string.p4text5,
					},
					{
						optid: "6",
						optclass:'f',
						listitem: data.string.p4text6,
					}
				]
			},
		{
		sortbox:'box2',
		optionsdiv:'optionsdiv1',
				sortoptions: [
					{
						optid: "1",
						listitem: data.string.p4text1a,
					},
					{
						optid: "2",
						listitem: data.string.p4text2a,
					},
					{
						optid: "3",
						listitem: data.string.p4text3a,
					},
					{
						listitem: data.string.p4text4a,
						optid: "4",
					},
					{
						optid: "5",
						listitem: data.string.p4text5a,
					},
					{
						optid: "6",
						listitem: data.string.p4text6a,
					}
				]
		},
		{
		sortbox:'box2',
		optionsdiv:'optionsdiv2',
				sortoptions: [
					{
						optid: "1",
						listitem: data.string.p4text1b,
					},
					{
						optid: "2",
						listitem: data.string.p4text2b,
					},
					{
						optid: "3",
						listitem: data.string.p4text3b,
					},
					{
						listitem: data.string.p4text4b,
						optid: "4",
					},
					{
						optid: "5",
						listitem: data.string.p4text5b,
					},
				]
			},
		{
		sortbox:'box2',
		optionsdiv:'optionsdiv3',
				sortoptions: [
					{
						optid: "1",
						listitem: data.string.p4text1c,
					},
					{
						optid: "2",
						listitem: data.string.p4text2c,
					},
					{
						optid: "3",
						listitem: data.string.p4text3c,
					},
					{
						listitem: data.string.p4text4c,
						optid: "4",
					},
					{
						optid: "5",
						listitem: data.string.p4text5c,
					},
					{
						optid: "6",
						listitem: data.string.p4text6c,
					},
					{
						optid: "7",
						listitem: data.string.p4text7c,
					}
				]
		},
		{
		sortbox:'box2',
		optionsdiv:'optionsdiv4',
				sortoptions: [
					{
						optid: "1",
						listitem: data.string.p4text1d,
					},
					{
						optid: "2",
						listitem: data.string.p4text2d,
					},
					{
						optid: "3",
						listitem: data.string.p4text3d,
					},
					{
						listitem: data.string.p4text4d,
						optid: "4",
					},
					{
						optid: "5",
						listitem: data.string.p4text5d,
					},
					{
						optid: "6",
						listitem: data.string.p4text6d,
					}
				]
		}
		],
		imageblock:[{
			imagestoshow:[{
				imgclass:'niti',
				imgid:'niti'
			}]
		}]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			{id: "niti", src: imgpath+"niti01.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// sounds
			{id: "sound_1", src: soundAsset+"alert_correct.ogg"},
			{id: "s4_p2", src: soundAsset+"s4_p2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_option_image(content, countNext);
		put_speechbox_image(content, countNext);


		 switch(countNext){
			case 0:
				play_diy_audio();
				nav_button_controls(2000);
			break;
		 	case 1:
				sound_player("s4_p2");
			 	var ul = document.querySelector('.optionsdiv');
				for (var i = ul.children.length; i >= 0; i--) {
				    ul.appendChild(ul.children[Math.random() * i | 0]);
				}
				var ul = document.querySelector('.optionsdiv1');
				for (var i = ul.children.length; i >= 0; i--) {
				    ul.appendChild(ul.children[Math.random() * i | 0]);
				}
				var ul = document.querySelector('.optionsdiv2');
				for (var i = ul.children.length; i >= 0; i--) {
				    ul.appendChild(ul.children[Math.random() * i | 0]);
				}
				var ul = document.querySelector('.optionsdiv3');
				for (var i = ul.children.length; i >= 0; i--) {
				    ul.appendChild(ul.children[Math.random() * i | 0]);
				}
				var ul = document.querySelector('.optionsdiv4');
				for (var i = ul.children.length; i >= 0; i--) {
				    ul.appendChild(ul.children[Math.random() * i | 0]);
				}

				 $( ".optionsdiv" ).sortable({
				 	stop: function(event, ui) {
						checkNextButton();
					}
				});
				 $( ".optionsdiv1" ).sortable({
				 	stop: function(event, ui) {
						checkNextButton();
					}
				});
				 $( ".optionsdiv2" ).sortable({
				 	stop: function(event, ui) {
						checkNextButton();
					}
				});
				 $( ".optionsdiv3" ).sortable({
				 	stop: function(event, ui) {
						checkNextButton();
					}
				});
				 $( ".optionsdiv4" ).sortable({
				 	stop: function(event, ui) {
						checkNextButton();
					}
				});
				 var correctcount = 0;
				 function checkNextButton(){
				 	if( $('.optionsdiv li:nth-child(1)').attr('id')==1
							&& $('.optionsdiv li:nth-child(2)').attr('id')==2
							&& $('.optionsdiv li:nth-child(3)').attr('id')==3
							&& $('.optionsdiv li:nth-child(4)').attr('id')==4
							&& $('.optionsdiv li:nth-child(5)').attr('id')==5
							&& $('.optionsdiv li:nth-child(6)').attr('id')==6
							)
							{
								$('.optionsdiv li:nth-child(1)').attr('id',0);
								$('.optionsdiv li:nth-child(2)').attr('id',0);
								$('.optionsdiv li:nth-child(3)').attr('id',0);
								$('.optionsdiv li:nth-child(4)').attr('id',0);
								$('.optionsdiv li:nth-child(5)').attr('id',0);
								$('.optionsdiv li:nth-child(6)').attr('id',0);
								sound_player("sound_1");
								$('.optionsdiv').css({"pointer-events":"none"});
								var child1=$('.optionsdiv').children();
								child1.css({"background":"green","color":"white"});
								correctcount++;
								if(correctcount==5){
									nav_button_controls(500);
								}

							}
					if( $('.optionsdiv1 li:nth-child(1)').attr('id')==1
							&& $('.optionsdiv1 li:nth-child(2)').attr('id')==2
							&& $('.optionsdiv1 li:nth-child(3)').attr('id')==3
							&& $('.optionsdiv1 li:nth-child(4)').attr('id')==4
							&& $('.optionsdiv1 li:nth-child(5)').attr('id')==5
							&& $('.optionsdiv1 li:nth-child(6)').attr('id')==6
							)
							{
								$('.optionsdiv1 li:nth-child(1)').attr('id',0);
								$('.optionsdiv1 li:nth-child(2)').attr('id',0);
								$('.optionsdiv1 li:nth-child(3)').attr('id',0);
								$('.optionsdiv1 li:nth-child(4)').attr('id',0);
								$('.optionsdiv1 li:nth-child(5)').attr('id',0);
								$('.optionsdiv1 li:nth-child(6)').attr('id',0);
								sound_player("sound_1");
								$('.optionsdiv1').css({"pointer-events":"none"});
								$('.optionsdiv1').css({"pointer-events":"none"});
								var child2=$('.optionsdiv1').children();
								child2.css({"background":"green","color":"white"});
								correctcount++;
								if(correctcount==5){
									nav_button_controls(500);
								}

							}
					if( $('.optionsdiv2 li:nth-child(1)').attr('id')==1
							&& $('.optionsdiv2 li:nth-child(2)').attr('id')==2
							&& $('.optionsdiv2 li:nth-child(3)').attr('id')==3
							&& $('.optionsdiv2 li:nth-child(4)').attr('id')==4
							&& $('.optionsdiv2 li:nth-child(5)').attr('id')==5
							)
							{
								$('.optionsdiv2 li:nth-child(1)').attr('id',0);
								$('.optionsdiv2 li:nth-child(2)').attr('id',0);
								$('.optionsdiv2 li:nth-child(3)').attr('id',0);
								$('.optionsdiv2 li:nth-child(4)').attr('id',0);
								$('.optionsdiv2 li:nth-child(5)').attr('id',0);
								sound_player("sound_1");
								$('.optionsdiv2').css({"pointer-events":"none"});
								var child3=$('.optionsdiv2').children();
								child3.css({"background":"green","color":"white"});
								correctcount++;
								if(correctcount==5){
									nav_button_controls(500);
								}

							}
					if( $('.optionsdiv3 li:nth-child(1)').attr('id')==1
							&& $('.optionsdiv3 li:nth-child(2)').attr('id')==2
							&& $('.optionsdiv3 li:nth-child(3)').attr('id')==3
							&& $('.optionsdiv3 li:nth-child(4)').attr('id')==4
							&& $('.optionsdiv3 li:nth-child(5)').attr('id')==5
							&& $('.optionsdiv3 li:nth-child(6)').attr('id')==6
							&& $('.optionsdiv3 li:nth-child(7)').attr('id')==7
							)
							{
								$('.optionsdiv3 li:nth-child(1)').attr('id',0);
								$('.optionsdiv3 li:nth-child(2)').attr('id',0);
								$('.optionsdiv3 li:nth-child(3)').attr('id',0);
								$('.optionsdiv3 li:nth-child(4)').attr('id',0);
								$('.optionsdiv3 li:nth-child(5)').attr('id',0);
								$('.optionsdiv3 li:nth-child(6)').attr('id',0);
								$('.optionsdiv3 li:nth-child(7)').attr('id',0);
								sound_player("sound_1");
								$('.optionsdiv3').css({"pointer-events":"none"});
								var child4=$('.optionsdiv3').children();
								child4.css({"background":"green","color":"white"});
								correctcount++;
								if(correctcount==5){
									nav_button_controls(500);
								}

							}
					if( $('.optionsdiv4 li:nth-child(1)').attr('id')==1
							&& $('.optionsdiv4 li:nth-child(2)').attr('id')==2
							&& $('.optionsdiv4 li:nth-child(3)').attr('id')==3
							&& $('.optionsdiv4 li:nth-child(4)').attr('id')==4
							&& $('.optionsdiv4 li:nth-child(5)').attr('id')==5
							&& $('.optionsdiv4 li:nth-child(6)').attr('id')==6
							)
							{
								$('.optionsdiv4 li:nth-child(1)').attr('id',0);
								$('.optionsdiv4 li:nth-child(2)').attr('id',0);
								$('.optionsdiv4 li:nth-child(3)').attr('id',0);
								$('.optionsdiv4 li:nth-child(4)').attr('id',0);
								$('.optionsdiv4 li:nth-child(5)').attr('id',0);
								$('.optionsdiv4 li:nth-child(6)').attr('id',0);
								sound_player("sound_1");
								$('.optionsdiv4').css({"pointer-events":"none"});
								var child5=$('.optionsdiv4').children();
								child5.css({"background":"green","color":"white"});
								correctcount++;
								if(correctcount==5){
									nav_button_controls(500);
								}

							}

				 	}


				 	break;
		 		default:
		 		nav_button_controls(500);
		 		break;

		 }

	}
	//change_src-> put 1 for girl speak first else 2 for boy-speak first, 3 for boy-3
	function conversation(class1, sound_data1, class2, sound_data2, change_src){
		$(class1).fadeIn(500, function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_data1);
			current_sound.play();
			current_sound.on("complete", function(){
				if(change_src==1){
					$('.boy').attr('src', preload.getResult('boy-1').src);
					$('.girl').attr('src', preload.getResult('girl-1').src);
				} else if(change_src==2){
					$('.boy').attr('src', preload.getResult('boy-2').src);
					$('.girl').attr('src', preload.getResult('girl-2').src);
				} else if(change_src==3){
					$('.boy').attr('src', preload.getResult('boy-3').src);
					$('.girl').attr('src', preload.getResult('girl-1').src);
				}
				$(class2).fadeIn(500, function(){
					current_sound = createjs.Sound.play(sound_data2);
					current_sound.play();
					current_sound.on("complete", function(){
						nav_button_controls(0);
						$(class1).click(function(){
							sound_player(sound_data1);
						});
						$(class2).click(function(){
							sound_player(sound_data2);
						});
					});
				});
			});
		});
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/

	 /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		CODE MODIFIED FOR THREE DIFFERENT HIGHTLIGHTS
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
		function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightstarttag2;
        var texthighlightstarttag3;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
              $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

              $(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
              (stylerulename2 = "parsedstring2") ;

              $(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
              (stylerulename3 = "parsedstring3") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
            texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
		/*=====  End of data highlight function  ======*/

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete',function(){
			nav_button_controls(100);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_option_image(content, count){
		if(content[count].hasOwnProperty('option')){
			var imageClass = content[count].option;
			console.log(imageClass);
			for(var i=0; i<imageClass.length; i++){
				var image_src = preload.getResult(imageClass[i].imgid).src;
				//get list of classes
				var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				// console.log(selector);
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
			/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentblockadditionalclass: 'bg_blue',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "duck",
					imgsrc : '',
					imgid : 'duck'
				}
			]
		}],
		uppertextblock:[{
			textdata: data.string.p5text1,
			textclass: "ducktext",
		}],
	},
	// slide1
	{
		uppertextblock:[{
			textdata: data.string.p5text2,
			textclass: "maintitle",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "queue",
					imgsrc : '',
					imgid : 'queue'
				},
				{
					imgclass : "soaphandwash",
					imgsrc : '',
					imgid : 'soaphandwash'
				},
				{
					imgclass : "sleep-on-time",
					imgsrc : '',
					imgid : 'sleep-on-time'
				},
				{
					imgclass : "exercise",
					imgsrc : '',
					imgid : 'exercise'
				},
				{
					imgclass : "brush",
					imgsrc : '',
					imgid : 'brush'
				},
			],
			imagelabels:[{
				imagelabelclass:'firstimagetitle',
				imagelabeldata: data.string.p5text4
			},{
				imagelabelclass:'secondimagetitle',
				imagelabeldata: data.string.p5text3
			},{
				imagelabelclass:'thirdimagetitle',
				imagelabeldata: data.string.p5text7
			},{
				imagelabelclass:'fourthimagetitle',
				imagelabeldata: data.string.p5text5
			},{
				imagelabelclass:'fifthimagetitle',
				imagelabeldata: data.string.p5text6
			},
			]
		}],

	},
	// slide2
	{
		contentblockadditionalclass: 'pink_bg',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "boysleep",
					imgsrc : '',
					imgid : 'sleep-on-time'
				}
			]
		}],
		uppertextblock:[{
			textdata: data.string.p5text8,
			textclass: "toptext",
		},
		{
			textdata: data.string.p5text9,
			textclass: "exampletext",
		}]

	},
	// slide3
	{
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "fullbg",
					imgsrc : '',
					imgid : 'park'
				}
			]
		}],
		uppertextblock:[{
			textdata: data.string.p5text10,
			textclass: "toptext",
		}],

	},
	// slide4
	{
		contentblockadditionalclass: 'bg_blue',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "niti",
					imgsrc : '',
					imgid : 'mainniti'
				},
				{
					imgclass : "mainsahil",
					imgsrc : '',
					imgid : 'mainsahil'
				},
				{
					imgclass : "mainmala",
					imgsrc : '',
					imgid : 'mainmala'
				},
				{
					imgclass : "mainniti",
					imgsrc : '',
					imgid : 'mainniti'
				},
				{
					imgclass : "mainsagar",
					imgsrc : '',
					imgid : 'mainsagar'
				},

				{
					imgclass : "mala",
					imgsrc : '',
					imgid : 'mala'
				},
				{
					imgclass : "sahil",
					imgsrc : '',
					imgid : 'sahil'
				},
				{
					imgclass : "sagar",
					imgsrc : '',
					imgid : 'sagar'
				},
				{
					imgclass : "sagarbrush",
					imgsrc : '',
					imgid : 'sagarbrush'
				},
				{
					imgclass : "sagarplay",
					imgsrc : '',
					imgid : 'sagarplay'
				},
				{
					imgclass : "crosbutton",
					imgsrc : '',
					imgid : 'crosbutton'
				},
				{
					imgclass : "handwashniti",
					imgsrc : '',
					imgid : 'soaphandwash'
				}
			]
		}],
		uppertextblock:[{
			textdata: data.string.p5text11,
			textclass: "toptext1 main_question",
		},
		{
			textdata: data.string.p5text12,
			textclass: "toptext1 niti_text",
		},
		{
			textdata: data.string.p5text14,
			textclass: "toptext1 mala_text",
		},
		{
			textdata: data.string.p5text16,
			textclass: "toptext1 sahil_text",
		},
		{
			textdata: data.string.p5text18,
			textclass: "toptext1 sagar_text",
		}],
		speechbox:[{
			speechbox: 'sp-2 niti_dialogue',
			textdata : data.string.p5text13,
			imgid : 't-1',
			imgclass:'revert'
			// audioicon: true,
		},
		{
			speechbox: 'sp-1 mala_dialogue',
			textdata : data.string.p5text15,
			imgclass: '',
			textclass : '',
			imgid : 't-1',
			imgsrc: '',
			imgclass:'revert'
			// audioicon: true,
		},
		{
			speechbox: 'sp-1 sahil_dialogue',
			textdata : data.string.p5text17,
			imgclass: '',
			textclass : '',
			imgid : 't-1',
			imgsrc: '',
			imgclass:'revert'
			// audioicon: true,
		},
		{
			speechbox: 'sp-2 sagar_dialogue',
			textdata : data.string.p5text19,
			imgclass: '',
			textclass : '',
			imgid : 't-1',
			imgsrc: '',
			imgclass:'revert'
			// audioicon: true,
		}],

	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "duck", src: imgpath+"pointing.png", type: createjs.AbstractLoader.IMAGE},
			{id: "soaphandwash", src: imgpath+"soaphandwash.png", type: createjs.AbstractLoader.IMAGE},
			{id: "queue", src: imgpath+"queue.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sleep-on-time", src: imgpath+"sleep-on-time.png", type: createjs.AbstractLoader.IMAGE},
			{id: "exercise", src: imgpath+"exercise.png", type: createjs.AbstractLoader.IMAGE},
			{id: "brush", src: imgpath+"brush.png", type: createjs.AbstractLoader.IMAGE},
			{id: "park", src: imgpath+"park01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "family", src: imgpath+"family.png", type: createjs.AbstractLoader.IMAGE},
			{id: "niti", src: imgpath+"niti02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala", src: imgpath+"drink-teainmorning.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sahil", src: imgpath+"cutnailbefore-eat.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sagar", src: imgpath+"sagar.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sagarbrush", src: imgpath+"twotime-brushing.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sagarplay", src: imgpath+"playing.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mainmala", src: imgpath+"mum.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mainsagar", src: imgpath+"son.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mainniti", src: imgpath+"niti03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mainsahil", src: imgpath+"dad.png", type: createjs.AbstractLoader.IMAGE},
			{id: "crosbutton", src: imgpath+"white_wrong.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "t-1", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s5_p1", src: soundAsset+"s5_p1.ogg"},
			{id: "s5_p2", src: soundAsset+"s5_p2.ogg"},
			{id: "s5_p3", src: soundAsset+"s5_p3.ogg"},
			{id: "s5_p4", src: soundAsset+"s5_p4.ogg"},
			{id: "s5_p5", src: soundAsset+"s5_p5.ogg"},
			{id: "s5_p5_1", src: soundAsset+"s5_p5_1.ogg"},
			{id: "s5_p5_boy", src: soundAsset+"s5_p5_boy.ogg"},
			{id: "s5_p5_girl", src: soundAsset+"s5_p5_girl.ogg"},
			{id: "s5_P5_mother", src: soundAsset+"s5_P5_mother.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_option_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext) {
			case 0:
				$('.title').addClass('blurin');
				sound_player_nav("s5_p"+(countNext+1));
				break;
			case 1:
				sound_player_nav("s5_p"+(countNext+1));
				$('.sp-1').addClass('fadein');
				break;
			case 2:
			case 3:
				sound_player_nav("s5_p"+(countNext+1));
				$('.sp-2').addClass('fadein');
				break;
			case 4:
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s5_p5_1");
					current_sound.play();
			  	$prevBtn.show(0);
			  	$nextBtn.hide(0);
			  	var clickout=0;
			  	$('.mainniti').click(function(){
						sound_player("s5_p5_girl");
			  		$('.niti,.handwashniti').css({"display":"block"});
			  		$('.toptext1,.mainniti,.mainsagar,.mainsahil,.mainmala').css({"display":"none"});
			  		$('.niti_text,.niti_dialogue').css({"display":"block"});
			  		clickout++;
			  		checkclickcount();
			  	});
			  	$('.mainsagar').click(function(){
						sound_player("s5_p5_boy");
			  		$('.sagar,.sagarplay,.sagarbrush').css({"display":"block"});
			  		$('.toptext1,.mainniti,.mainsagar,.mainsahil,.mainmala').css({"display":"none"});
			  		$('.sagar_text,.sagar_dialogue').css({"display":"block"});
			  		clickout++;
			  		checkclickcount();
			  	});
			  	$('.mainmala').click(function(){
						sound_player("s5_P5_mother");
			  		$('.mala').css({"display":"block"});
			  		$('.toptext1,.mainniti,.mainsagar,.mainsahil,.mainmala').css({"display":"none"});
			  		$('.mala_text,.mala_dialogue').css({"display":"block"});
			  		clickout++;
			  		checkclickcount();
			  	});
			  	$('.mainsahil').click(function(){
						sound_player("s5_p5");
			  		$('.sahil').css({"display":"block"});
			  		$('.toptext1,.mainniti,.mainsagar,.mainsahil,.mainmala').css({"display":"none"});
			  		$('.sahil_text,.sahil_dialogue').css({"display":"block"});
			  		clickout++;
			  		checkclickcount();
			  	});
			  	$('.crosbutton').click(function(){
			  		$('.mainniti,.mainsagar,.mainsahil,.mainmala,.main_question').css({"display":"block"});
			  		$('.sahil,.mala,.sagar,.sagarplay,.sagarbrush,.niti,.handwashniti,.niti_dialogue,.sagar_dialogue,.mala_dialogue,.sahil_dialogue,.mala_text,.sagar_text,.sahil_text,.niti_text,.crosbutton').css({"display":"none"});
			  	});
			  	function checkclickcount(){
				  	if(clickout==4){
						ole.footerNotificationHandler.pageEndSetNotification();
				  	}
			  	}
				break;
			case 5:
				$nextBtn.hide(0);
				    $('.type7').addClass('typingclass');
				setTimeout(function() {
				    $('.type8').addClass('typingclass');
					}, 2000);

				setTimeout(function() {
					ole.footerNotificationHandler.pageEndSetNotification();
					}, 3000);
				break;
			default:
				$prevBtn.show(0);
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

function sound_player(sound_id){
	$('.crosbutton').hide(0);
	createjs.Sound.stop();
	current_sound = createjs.Sound.play(sound_id);
	current_sound.play();
	current_sound.on('complete',function(){
		$('.crosbutton').show();
	});
}
function sound_player_nav(sound_id){
	createjs.Sound.stop();
	current_sound = createjs.Sound.play(sound_id);
	current_sound.play();
	current_sound.on('complete',function(){
		nav_button_controls(100);
	});
}
	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/

	 /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		CODE MODIFIED FOR THREE DIFFERENT HIGHTLIGHTS
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
		function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightstarttag2;
        var texthighlightstarttag3;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
              $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

              $(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
              (stylerulename2 = "parsedstring2") ;

              $(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
              (stylerulename3 = "parsedstring3") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
            texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
		/*=====  End of data highlight function  ======*/

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_option_image(content, count){
		if(content[count].hasOwnProperty('option')){
			var imageClass = content[count].option;
			console.log(imageClass);
			for(var i=0; i<imageClass.length; i++){
				var image_src = preload.getResult(imageClass[i].imgid).src;
				//get list of classes
				var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				// console.log(selector);
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
			/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentblockadditionalclass:'pink_bg',
		uppertextblock:[{
			textdata: data.string.diytext,
			textclass: "diytitle",
		}]
	},
	// slide1
	{
		contentblockadditionalclass:'lightpurple',
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "mainquestion",
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "girl",
					imgsrc : '',
					imgid : 'girl'
				}
			]
		}]
	},
	// slide2
	{
		contentblockadditionalclass:'lightpurple',
		uppertextblock:[{
			textdata: data.string.p2text2,
			textclass: "commonquestion",
		},{
			textdata: data.string.p2text3,
			textclass: "statement",
		},{
			textdata: data.string.p2text4,
			textclass: "option1 wrong",
		},{
			textdata: data.string.p2text5,
			textclass: "option2 wrong",
		},{
			textdata: data.string.p2text5a,
			textclass: "option3 right",
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "mala",
					imgsrc : '',
					imgid : 'mala'
				},
				{
					imgclass : "basket",
					imgsrc : '',
					imgid : 'basket'
				}
			]
		}]
	},
	// slide3
	{
		contentblockadditionalclass:'lightpurple',
		uppertextblock:[{
			textdata: data.string.p2text2,
			textclass: "commonquestion",
		},{
			textdata: data.string.p2text6,
			textclass: "statement",
		},{
			textdata: data.string.p2text7,
			textclass: "option1 wrong",
		},{
			textdata: data.string.p2text8,
			textclass: "option2 right",
		},{
			textdata: data.string.p2text9,
			textclass: "option3 wrong",
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "asha",
					imgsrc : '',
					imgid : 'asha'
				},
				{
					imgclass : "basket",
					imgsrc : '',
					imgid : 'swimmingpool'
				}
			]
		}]
	},
	// slide4
	{
		contentblockadditionalclass:'lightpurple',
		uppertextblock:[{
			textdata: data.string.p2text2,
			textclass: "commonquestion",
		},{
			textdata: data.string.p2text10,
			textclass: "statement",
		},{
			textdata: data.string.p2text11,
			textclass: "option1 right",
		},{
			textdata: data.string.p2text12,
			textclass: "option2 wrong",
		},{
			textdata: data.string.p2text13,
			textclass: "option3 wrong",
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "full_image",
					imgsrc : '',
					imgid : 'office'
				},
			]
		}]
	},
	// slide5
	{
		contentblockadditionalclass:'lightpurple',
		uppertextblock:[{
			textdata: data.string.p2text2,
			textclass: "commonquestion",
		},{
			textdata: data.string.p2text14,
			textclass: "statement",
		},{
			textdata: data.string.p2text15,
			textclass: "option1 wrong",
		},{
			textdata: data.string.p2text16,
			textclass: "option2 wrong",
		},{
			textdata: data.string.p2text17,
			textclass: "option3 right",
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "sagar",
					imgsrc : '',
					imgid : 'sagar'
				},
				{
					imgclass : "basket",
					imgsrc : '',
					imgid : 'camera'
				}
			]
		}]
	},
	// slide6
	{
		contentblockadditionalclass:'lightpurple',
		uppertextblock:[{
			textdata: data.string.p2text2,
			textclass: "commonquestion",
		},{
			textdata: data.string.p2text18,
			textclass: "statement",
		},{
			textdata: data.string.p2text19,
			textclass: "option1 wrong",
		},{
			textdata: data.string.p2text20,
			textclass: "option2 wrong",
		},{
			textdata: data.string.p2text21,
			textclass: "option3 right",
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "grandma",
					imgsrc : '',
					imgid : 'grandma'
				}
			]
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "girl", src: imgpath+"asha01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala", src: imgpath+"mala.png", type: createjs.AbstractLoader.IMAGE},
			{id: "basket", src: imgpath+"shoopingbasket.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha", src: imgpath+"asha02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "swimmingpool", src: imgpath+"swimmingpool.png", type: createjs.AbstractLoader.IMAGE},
			{id: "office", src: imgpath+"office.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sagar", src: imgpath+"sagar.png", type: createjs.AbstractLoader.IMAGE},
			{id: "camera", src: imgpath+"camera.png", type: createjs.AbstractLoader.IMAGE},
			{id: "grandma", src: imgpath+"gandmum.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s2_p1", src: soundAsset+"s2_p1.ogg"},
			{id: "s2_p2", src: soundAsset+"s2_p2.ogg"},
			{id: "s2_p3", src: soundAsset+"s2_p3.ogg"},
			{id: "p2_s3", src: soundAsset+"p2_s3.ogg"},
			{id: "p2_s4", src: soundAsset+"p2_s4.ogg"},
			{id: "p2_s5", src: soundAsset+"p2_s5.ogg"},
			{id: "p2_s6", src: soundAsset+"p2_s6.ogg"},
			{id: "p2_s7", src: soundAsset+"p2_s7.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext) {
			case 0:
				$prevBtn.hide(0);
				play_diy_audio();
				nav_button_controls(1000);
				break;
			case 1:
				$prevBtn.show(0);
				sound_player_nav("s2_p"+(countNext+1));
				break;
			case 2:
			createjs.Sound.stop();
			var current_sound = createjs.Sound.play("s2_p3");
			current_sound.play();
			current_sound.on('complete',function(){
				var current_sound1 = createjs.Sound.play("p2_s3");
				current_sound1.play();
			});
			$prevBtn.show(0);
			$nextBtn.hide(0);
			$('.right').click(function(){
                $(this).append("<img class='correctwrongImg' src='images/correct.png'/>");
								createjs.Sound.stop();
                play_correct_incorrect_sound(1);
				$(this).css({"background":"green","pointer-events":"none"});
				$('.wrong').css({"pointer-events":"none"});
				$nextBtn.show(0);
			});
			$('.wrong').click(function(){
                $(this).append("<img class='correctwrongImg' src='images/wrong.png'/>");
								createjs.Sound.stop();
                play_correct_incorrect_sound(0);
				$(this).css({"background":"#da7e7e"});
			});
			break;

			case 3:
			createjs.Sound.stop();
			var current_sound = createjs.Sound.play("p2_s4");
			current_sound.play();
				$prevBtn.show(0);
				$nextBtn.hide(0);
				$('.right').click(function(){
                    $(this).append("<img class='correctwrongImg' src='images/correct.png'/>");
										createjs.Sound.stop();
                    play_correct_incorrect_sound(1);
					$(this).css({"background":"green","pointer-events":"none"});
					$('.wrong').css({"pointer-events":"none"});
					$nextBtn.show(0);
				});
				$('.wrong').click(function(){
                    $(this).append("<img class='correctwrongImg' src='images/wrong.png'/>");
										createjs.Sound.stop();
                    play_correct_incorrect_sound(0);
					$(this).css({"background":"#da7e7e"});
				});
				break;
			case 4:
			createjs.Sound.stop();
			var current_sound = createjs.Sound.play("p2_s5");
			current_sound.play();
				$nextBtn.hide(0);
				$prevBtn.show(0);
				$('.right').click(function(){
                    $(this).append("<img class='correctwrongImg' src='images/correct.png'/>");
										createjs.Sound.stop();
                    play_correct_incorrect_sound(1);
					$(this).css({"background":"green","pointer-events":"none"});
					$('.wrong').css({"pointer-events":"none"});
					$nextBtn.show(0);
				});
				$('.wrong').click(function(){
                    $(this).append("<img class='correctwrongImg' src='images/wrong.png'/>");
										createjs.Sound.stop();
                    play_correct_incorrect_sound(0);
					$(this).css({"background":"#da7e7e"});
				});
				$(".option2,.option3,.option1").css({"left":"29.5%"});
				$('.commonquestion,.statement').css({"color":"white"});
				break;
			case 5:
			createjs.Sound.stop();
			var current_sound = createjs.Sound.play("p2_s6");
			current_sound.play();
			$prevBtn.show(0);
			$nextBtn.hide(0);
			$('.right').click(function(){
                $(this).append("<img class='correctwrongImg' src='images/correct.png'/>");
								createjs.Sound.stop();
                play_correct_incorrect_sound(1);
				$(this).css({"background":"green","pointer-events":"none"});
				$('.wrong').css({"pointer-events":"none"});
				nav_button_controls(100);
			});
			$('.wrong').click(function(){
                $(this).append("<img class='correctwrongImg' src='images/wrong.png'/>");
								createjs.Sound.stop();
                play_correct_incorrect_sound(0);
				$(this).css({"background":"#da7e7e"});
			});
			break;

			case 6:
			createjs.Sound.stop();
			var current_sound = createjs.Sound.play("p2_s7");
			current_sound.play();
				$prevBtn.show(0);
				$nextBtn.hide(0);
				$('.right').click(function(){
                    $(this).append("<img class='correctwrongImg' src='images/correct.png'/>");
										createjs.Sound.stop();
                    play_correct_incorrect_sound(1);
					$(this).css({"background":"green","pointer-events":"none"});
					$('.wrong').css({"pointer-events":"none"});
					nav_button_controls(100);
				});
				$('.wrong').click(function(){
                    $(this).append("<img class='correctwrongImg' src='images/wrong.png'/>");
										createjs.Sound.stop();
                    play_correct_incorrect_sound(0);
					$(this).css({"background":"#da7e7e"});
				});
				break;

			default:
				$prevBtn.show(0);
				sound_player('sound_2');
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete',function(){
			nav_button_controls(100);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

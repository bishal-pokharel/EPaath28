var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/";

var content = [
  // slide0
  {
    extratextblock: [
      {
        datahighlightflag: "true",
        datahighlightcustomclass: "bold",
        textclass: "chapter_title",
        textdata: data.string.p1text0
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: "coverpage",
            imgsrc: ""
          }
        ]
      }
    ]
  },

  // slide1
  {
    extratextblock: [
      {
        textclass: "mid_text_1 top_change",
        textdata: data.string.p1text3
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: "coverpage",
            imgsrc: ""
          }
        ]
      }
    ]
  },

  // slide2
  {
    extratextblock: [
      {
        textclass: "bottom_text",
        textdata: data.string.p1text4
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg_image",
            imgid: "02",
            imgsrc: ""
          }
        ]
      }
    ]
  },

  // slide3
  {
    extratextblock: [
      {
        textclass: "bottom_text",
        textdata: data.string.p1text5
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg_image",
            imgid: "03",
            imgsrc: ""
          },
          {
            imgclass: "pea_only",
            imgid: "pea_only",
            imgsrc: ""
          }
        ]
      }
    ]
  },

  // slide4
  {
    extratextblock: [
      {
        textclass: "top_text sniglet_font",
        textdata: data.string.p1text6
      },
      {
        textclass: "mid_text sniglet_font",
        textdata: data.string.p1text7
      },
      {
        textclass: "options",
        textdata: data.string.p1text8
      },
      {
        textclass: "options",
        textdata: data.string.p1text9
      },
      {
        textclass: "options correct",
        textdata: data.string.p1text10
      },
      {
        textclass: "options",
        textdata: data.string.p1text11
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg_full",
            imgid: "coverpage",
            imgsrc: ""
          }
        ]
      }
    ]
  },

  // slide5
  {
    extratextblock: [
      {
        textclass: "right_text",
        textdata: data.string.p1text12
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "left_image",
            imgid: "04",
            imgsrc: ""
          }
        ]
      }
    ]
  },

  // slide6
  {
    extratextblock: [
      {
        textclass: "bottom_text",
        textdata: data.string.p1text13
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg_image",
            imgid: "05",
            imgsrc: ""
          },
          {
            imgclass: "pea_only",
            imgid: "pea_only",
            imgsrc: ""
          }
        ]
      }
    ]
  }
];

$(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page, countNext + 1);

  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();

  var preload;
  var timeoutvar = null;
  var current_sound;

  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      // {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
      // {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
      //   ,
      //images
      {
        id: "coverpage",
        src: imgpath + "coverpage.jpg",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "02",
        src: imgpath + "02.jpg",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "03",
        src: imgpath + "03.jpg",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "04",
        src: imgpath + "04.jpg",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "05",
        src: imgpath + "05.jpg",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "06",
        src: imgpath + "06.jpg",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "07",
        src: imgpath + "07.jpg",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "11",
        src: imgpath + "11.jpg",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "cloudwith_rain",
        src: imgpath + "cloudwith_rain.gif",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "pea_small_grow",
        src: imgpath + "pea-growing.gif",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "small_grow",
        src: imgpath + "pea-growing01.gif",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "12",
        src: imgpath + "12.jpg",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "09",
        src: imgpath + "09.jpg",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "10",
        src: imgpath + "10.jpg",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "pea_only",
        src: imgpath + "pea_only.png",
        type: createjs.AbstractLoader.IMAGE
      },
      //textboxes
      {
        id: "tb-2",
        src: "images/textbox/white/tl-1.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "tb-1",
        src: "images/textbox/white/tr-2.png",
        type: createjs.AbstractLoader.IMAGE
      },
      // sounds
      { id: "sound_1", src: soundAsset + "p1_s1.ogg" },
      { id: "sound_2", src: soundAsset + "p1_s2.ogg" },
      { id: "sound_3", src: soundAsset + "p1_s3.ogg" },
      { id: "sound_4", src: soundAsset + "p1_s4.ogg" },
      { id: "sound_5", src: soundAsset + "p1_s5.ogg" },
      { id: "sound_6", src: soundAsset + "p1_s6.ogg" },
      // { id: "sound_7", src: soundAsset + "p1_s7.ogg" },
      // { id: "sound_8", src: soundAsset + "p1_s8.ogg" },
      { id: "sound_0", src: soundAsset + "p1_s0.ogg" },
      { id: "p1_ss5", src: soundAsset + "p1_ss5.ogg" },
      { id: "active", src: soundAsset + "p2_s6.ogg" },
      { id: "correct", src: soundAsset + "correct.ogg" },
      { id: "incorrect", src: soundAsset + "incorrect.ogg" }
    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound); //for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }
  function handleFileLoad(event) {
    // console.log(event.item);
  }
  function handleProgress(event) {
    $("#loading-text").html(parseInt(event.loaded * 100) + "%");
  }
  function handleComplete(event) {
    $("#loading-wrapper").hide(0);
    //initialize varibales
    current_sound = createjs.Sound.play("sound_1");
    current_sound.stop();
    // call main function
    templateCaller();
  }
  //initialize
  init();

  /*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  /*===============================================
	=            data highlight function            =
	===============================================*/
  function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object"
      ? alert(
          "Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted"
        )
      : null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";
    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
        $(this).attr(
          "data-highlightcustomclass"
        ) /*if there is data-highlightcustomclass defined it is true else it is not*/
          ? (stylerulename = $(this).attr("data-highlightcustomclass"))
          : (stylerulename = "parsedstring");

        texthighlightstarttag = "<span class='" + stylerulename + "'>";
        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
        $(this).html(replaceinstring);
      });
    }
  }
  /*=====  End of data highlight function  ======*/

  /*===============================================
	 =            user notification function        =
	 ===============================================*/
  function notifyuser($notifyinside) {
    //check if $notifyinside is provided
    typeof $notifyinside !== "object"
      ? alert(
          "Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style."
        )
      : null;

    /*variable that will store the element(s) to remove notification from*/
    var $allnotifications = $notifyinside.find(
      "*[data-usernotification='notifyuser']"
    );
    // if there are any notifications removal required add the event handler
    if ($allnotifications.length > 0) {
      $allnotifications.one("click", function() {
        /* Act on the event */
        $(this).attr("data-isclicked", "clicked");
        $(this).removeAttr("data-usernotification");
      });
    }
  }

  /*=====  End of user notification function  ======*/

  /*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

  function navigationcontroller(islastpageflag) {
    typeof islastpageflag === "undefined"
      ? (islastpageflag = false)
      : typeof islastpageflag != "boolean"
      ? alert(
          "NavigationController : Hi Master, please provide a boolean parameter"
        )
      : null;

    if (countNext == 0 && $total_page != 1) {
      $nextBtn.show(0);
      $prevBtn.css("display", "none");
    } else if ($total_page == 1) {
      $prevBtn.css("display", "none");
      $nextBtn.css("display", "none");

      // if lastpageflag is true
      islastpageflag
        ? ole.footerNotificationHandler.lessonEndSetNotification()
        : ole.footerNotificationHandler.lessonEndSetNotification();
    } else if (countNext > 0 && countNext < $total_page - 1) {
      $nextBtn.show(0);
      $prevBtn.show(0);
    } else if (countNext == $total_page - 1) {
      $nextBtn.css("display", "none");
      $prevBtn.show(0);

      // if lastpageflag is true
      islastpageflag
        ? ole.footerNotificationHandler.lessonEndSetNotification()
        : ole.footerNotificationHandler.pageEndSetNotification();
    }
  }

  /*=====  End of user navigation controller function  ======*/

  /*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

  function instructionblockcontroller() {
    var $instructionblock = $board.find("div.instructionblock");
    if ($instructionblock.length > 0) {
      var $contentblock = $board.find("div.contentblock");
      var $toggleinstructionblockbutton = $instructionblock.find(
        "div.toggleinstructionblock"
      );
      var instructionblockisvisibleflag;

      $contentblock.css("pointer-events", "none");

      $toggleinstructionblockbutton.on("click", function() {
        instructionblockisvisibleflag = $instructionblock.attr(
          "data-instructionblockshow"
        );
        if (instructionblockisvisibleflag == "true") {
          instructionblockisvisibleflag = "false";
          $contentblock.css("pointer-events", "auto");
        } else if (instructionblockisvisibleflag == "false") {
          instructionblockisvisibleflag = "true";
          $contentblock.css("pointer-events", "none");
        }

        $instructionblock.attr(
          "data-instructionblockshow",
          instructionblockisvisibleflag
        );
      });
    }
  }

  /*=====  End of InstructionBlockController  ======*/

  /*=================================================
	 =            general template function            =
	 =================================================*/
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    vocabcontroller.findwords(countNext);
    put_image(content, countNext);
    put_image2(content, countNext);
    put_speechbox_image(content, countNext);
    switch (countNext) {
      case 0:
        $nextBtn.hide(0);
        sound_player("sound_0", true);
        break;
      case 1:
        sound_player("sound_1", true);
        break;
      case 2:
        sound_player("sound_2", true);
        break;
      case 3:
        $(".bottom_text").css("padding-top", "2%");
        sound_player("sound_3", true);
        break;
      case 4:
        sound_player("p1_ss5");
        $(".options").click(function() {
          if ($(this).hasClass("correct")) {
            var $this = $(this);
            var position = $this.position();
            $(".options").css("pointer-events", "none");
            var width = $this.width();
            var height = $this.height();
            var centerX =
              ((position.left + width / 2) * 100) / $board.width() + "%";
            var centerY =
              ((position.top + height) * 100) / $board.height() + "%";
            $(
              '<img style="left:' +
                centerX +
                ";top:" +
                centerY +
                ';position:absolute;width:5%;transform:translate(724%,-19%)" src="' +
                imgpath +
                'correct.png" />'
            ).insertAfter(this);
            $(this).css({
              background: "rgb(190,214,47)",
              color: "white",
              "border-color": "#DEEF3C"
            });
            createjs.Sound.stop();
            play_correct_incorrect_sound(1);
            nav_button_controls(100);
          } else {
            var $this = $(this);
            var position = $this.position();
            var width = $this.width();
            var height = $this.height();
            var centerX =
              ((position.left + width / 2) * 100) / $board.width() + "%";
            var centerY =
              ((position.top + height) * 100) / $board.height() + "%";
            $(
              '<img style="left:' +
                centerX +
                ";top:" +
                centerY +
                ';position:absolute;width:5%;transform:translate(724%,-19%)" src="' +
                imgpath +
                'incorrect.png" />'
            ).insertAfter(this);
            createjs.Sound.stop();
            play_correct_incorrect_sound(0);
            $(this).css({
              background: "rgb(168, 33, 36)",
              color: "white",
              "pointer-events": "none",
              "border-color": "#980000"
            });
          }
        });
        break;
      case 5:
        createjs.Sound.stop();
        sound_player("sound_5", true);
        break;
      case 6:
        $(".bottom_text").css("padding-top", "2%");
        $(".pea_only").css("top", "75.5%");
        sound_player("sound_6", true);
        break;
    }
  }
  function nav_button_controls(delay_ms) {
    timeoutvar = setTimeout(function() {
      if (countNext == 0) {
        $nextBtn.show(0);
      } else if (countNext > 0 && countNext == $total_page - 1) {
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else {
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    }, delay_ms);
  }
  function sound_player(sound_id, next) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on("complete", function() {
      next ? nav_button_controls(10) : "";
    });
  }
  // function sound_player1(sound_id, next){
  // 	createjs.Sound.stop();
  // 	current_sound = createjs.Sound.play(sound_id);
  // 	current_sound.play();
  // 	// current_sound.on('complete', function(){
  // 	// 	if(next == null)
  // 	// 	navigationcontroller();
  // 	// });
  // }

  // function sound_player_duo(sound_id, sound_id_2){
  // 	createjs.Sound.stop();
  // 	current_sound = createjs.Sound.play(sound_id);
  // 	//current_sound_2 = createjs.Sound.play(sound_id_2);
  // 	current_sound.play();
  // 	current_sound.on('complete', function(){
  // 		$(".dotext").show(0);
  // 		sound_player(sound_id_2);
  // 	});
  //
  // }

  function put_image(content, count) {
    if (content[count].hasOwnProperty("imageblock")) {
      var imageblock = content[count].imageblock[0];
      if (imageblock.hasOwnProperty("imagestoshow")) {
        var imageClass = imageblock.imagestoshow;
        for (var i = 0; i < imageClass.length; i++) {
          var image_src = preload.getResult(imageClass[i].imgid).src;
          //get list of classes
          var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
          var selector = "." + classes_list[classes_list.length - 1];
          $(selector).attr("src", image_src);
        }
      }
    }
  }

  function put_image2(content, count) {
    if (content[count].hasOwnProperty("livinnonlivin")) {
      var lncontent = content[count].livinnonlivin[0];
      if (lncontent.hasOwnProperty("imageblock")) {
        var imageblock = lncontent.imageblock[0];
        if (imageblock.hasOwnProperty("imagestoshow")) {
          var imageClass = imageblock.imagestoshow;
          for (var i = 0; i < imageClass.length; i++) {
            var image_src = preload.getResult(imageClass[i].imgid).src;
            //get list of classes
            var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
            var selector = "." + classes_list[classes_list.length - 1];
            $(selector).attr("src", image_src);
          }
        }
      }
      var lncontent = content[count].livinnonlivin[1];
      if (lncontent.hasOwnProperty("imageblock")) {
        var imageblock = lncontent.imageblock[0];
        if (imageblock.hasOwnProperty("imagestoshow")) {
          var imageClass = imageblock.imagestoshow;
          for (var i = 0; i < imageClass.length; i++) {
            var image_src = preload.getResult(imageClass[i].imgid).src;
            //get list of classes
            var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
            var selector = "." + classes_list[classes_list.length - 1];
            $(selector).attr("src", image_src);
          }
        }
      }
    }
  }
  function put_speechbox_image(content, count) {
    if (content[count].hasOwnProperty("speechbox")) {
      var speechbox = content[count].speechbox;
      for (var i = 0; i < speechbox.length; i++) {
        var image_src = preload.getResult(speechbox[i].imgid).src;
        //get list of classes
        var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
        var selector =
          "." + classes_list[classes_list.length - 1] + ">.speechbg";
        // console.log(selector);
        $(selector).attr("src", image_src);
      }
    }
  }
  function templateCaller() {
    $prevBtn.css("display", "none");
    $nextBtn.css("display", "none");

    if (countNext == 0) navigationcontroller();

    generaltemplate();
    loadTimelineProgress($total_page, countNext + 1);

    /*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					templateCaller();
				});
			}
	*/
  }

  $nextBtn.on("click", function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    switch (countNext) {
      default:
        countNext++;
        templateCaller();
        break;
    }
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    countNext--;
    templateCaller();
    /* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
    countNext < $total_page
      ? ole.footerNotificationHandler.hideNotification()
      : null;
  });
});

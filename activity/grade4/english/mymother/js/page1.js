var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";

var sound_l_0 = new buzz.sound((soundAsset + "mymother.ogg"));
var sound_l_1 = new buzz.sound((soundAsset + "1.ogg"));
var sound_l_2 = new buzz.sound((soundAsset + "2.ogg"));
var sound_l_3 = new buzz.sound((soundAsset + "3.ogg"));
var sound_l_4 = new buzz.sound((soundAsset + "4.ogg"));
var sound_l_5 = new buzz.sound((soundAsset + "5.ogg"));
var sound_l_6 = new buzz.sound((soundAsset + "6.ogg"));
var sound_l_7 = new buzz.sound((soundAsset + "7.ogg"));
var sound_l_8 = new buzz.sound((soundAsset + "8.ogg"));
var sound_l_9 = new buzz.sound((soundAsset + "9.ogg"));
var sound_l_10 = new buzz.sound((soundAsset + "10.ogg"));
var sound_l_11 = new buzz.sound((soundAsset + "11.ogg"));
var sound_l_12 = new buzz.sound((soundAsset + "12.ogg"));
var sound_l_13 = new buzz.sound((soundAsset + "13.ogg"));
var sound_l_14 = new buzz.sound((soundAsset + "14.ogg"));
var sound_l_15 = new buzz.sound((soundAsset + "15.ogg"));
var sound_l_16 = new buzz.sound((soundAsset + "16.ogg"));

var sound_group_p1 = [sound_l_1, sound_l_2, sound_l_3, sound_l_4, sound_l_5, sound_l_6, sound_l_7, sound_l_8, sound_l_9, sound_l_10, sound_l_11, sound_l_12, sound_l_13, sound_l_14, sound_l_15, sound_l_16];

var content=[
	{
		// slide0
		contentnocenteradjust: true,
		headerblock:[{
			textdata: data.string.p0_title
		}],
		imageblock:[
			{
				imagestoshow:[{
					imgclass: "frontmother",
					imgsrc: imgpath+"mother01.png"
				}]
			}
		],
	},{
		// slide1
		contentblockadditionalclass: "custombg",
		contentnocenteradjust: true,
		headerblock:[{
			textdata: data.string.p0_title
		}],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[{
			textclass: "playthis",
			textdata: data.string.p1_s0
		}],
		imageblock:[
			{
				imagestoshow:[{
					imgclass: "mothercontainer",
					imgsrc: imgpath+"mother01.png"
				}]
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
		{
			textclass: "playthis2",
			textdata: data.string.p1_s1
		}
		],
	},{
		// slide3
		contentblockadditionalclass: "custombg",
		contentnocenteradjust: true,
		headerblock:[{
			textdata: data.string.p0_title
		}],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[{
			textclass: "playthis",
			textdata: data.string.p1_s2
		}],
		imageblock:[
			{
				imagestoshow:[{
					imgclass: "mothercontainer",
					imgsrc: imgpath+"mother02.png"
				}]
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
		{
			textclass: "playthis2",
			textdata: data.string.p1_s3
		}
		],
	},{
		// slide5
		contentblockadditionalclass: "custombg",
		contentnocenteradjust: true,
		headerblock:[{
			textdata: data.string.p0_title
		}],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[{
			textclass: "playthis",
			textdata: data.string.p1_s4
		}],
		imageblock:[
			{
				imagestoshow:[{
					imgclass: "mothercontainer",
					imgsrc: imgpath+"mother03.png"
				}]
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
		{
			textclass: "playthis2",
			textdata: data.string.p1_s5
		}
		],
	},{
		// slide7
		contentblockadditionalclass: "custombg",
		contentnocenteradjust: true,
		headerblock:[{
			textdata: data.string.p0_title
		}],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[{
			textclass: "playthis",
			textdata: data.string.p1_s6
		}],
		imageblock:[
			{
				imagestoshow:[{
					imgclass: "mothercontainer",
					imgsrc: imgpath+"mother04.png"
				}]
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
		{
			textclass: "playthis2",
			textdata: data.string.p1_s7
		}
		],
	},{
		// slide9
		contentblockadditionalclass: "custombg",
		contentnocenteradjust: true,
		headerblock:[{
			textdata: data.string.p0_title
		}],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[{
			textclass: "playthis",
			textdata: data.string.p1_s8
		}],
		imageblock:[
			{
				imagestoshow:[{
					imgclass: "mothercontainer",
					imgsrc: imgpath+"mother05.png"
				}]
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
		{
			textclass: "playthis2",
			textdata: data.string.p1_s9
		}
		],
	},{
		// slide11
		contentblockadditionalclass: "custombg",
		contentnocenteradjust: true,
		headerblock:[{
			textdata: data.string.p0_title
		}],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[{
			textclass: "playthis",
			textdata: data.string.p1_s10
		}],
		imageblock:[
			{
				imagestoshow:[{
					imgclass: "mothercontainer",
					imgsrc: imgpath+"mother06.png"
				}]
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
		{
			textclass: "playthis2",
			textdata: data.string.p1_s11
		}
		],
	},{
		// slide13
		contentblockadditionalclass: "custombg",
		contentnocenteradjust: true,
		headerblock:[{
			textdata: data.string.p0_title
		}],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[{
			textclass: "playthis",
			textdata: data.string.p1_s12
		}],
		imageblock:[
			{
				imagestoshow:[{
					imgclass: "mothercontainer",
					imgsrc: imgpath+"mother07.png"
				}]
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
		{
			textclass: "playthis2",
			textdata: data.string.p1_s13
		}
		],
	},
	{
		// slide15
		contentblockadditionalclass: "custombg",
		contentnocenteradjust: true,
		headerblock:[{
			textdata: data.string.p0_title
		}],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[{
			textclass: "playthis",
			textdata: data.string.p1_s14
		}],
		imageblock:[
			{
				imagestoshow:[{
					imgclass: "mothercontainer",
					imgsrc: imgpath+"mother08.png"
				}]
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
		{
			textclass: "playthis2",
			textdata: data.string.p1_s15
		}
		],
	}
];


$(function () { 
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;
  
  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
   
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;
          
        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) { 
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/       
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
  
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/ 
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
   
  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
   }

    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);          
        });
      }
    } 

  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);
	       
	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
		// splitintofractions($(".fractionblock"));
		/*if(countNext != 0){
		   	var $textblack = $(".playthis");
			sound_data =  sound_group_p1[0];
			play_text($textblack, $textblack.html());  
		}*/
			vocabcontroller.findwords(countNext);
		switch(countNext){
			case 0:
                $nextBtn.hide(0);
				sound_l_0.play().bind("ended",function () {
					$nextBtn.show(0);
                });
				break;
			case 1:
		   	var $textblack = $(".playthis");
		   	var $textblack2 = $(".playthis2");
			sound_data =  sound_group_p1[0];
			sound_data2 =  sound_group_p1[1];
			play_text($textblack, $textblack.html(), $textblack2, $textblack2.html());  
			break;
			case 2:
		   	var $textblack = $(".playthis");
		   	var $textblack2 = $(".playthis2");
			sound_data =  sound_group_p1[2];
			sound_data2 =  sound_group_p1[3];
			play_text($textblack, $textblack.html(), $textblack2, $textblack2.html());  
			break;
			case 3:
		   	var $textblack = $(".playthis");
		   	var $textblack2 = $(".playthis2");
			sound_data =  sound_group_p1[4];
			sound_data2 =  sound_group_p1[5];
			play_text($textblack, $textblack.html(), $textblack2, $textblack2.html());  
			break;
			case 4:
		   	var $textblack = $(".playthis");
		   	var $textblack2 = $(".playthis2");
			sound_data =  sound_group_p1[6];
			sound_data2 =  sound_group_p1[7];
			play_text($textblack, $textblack.html(), $textblack2, $textblack2.html());  
			break;
			case 5:
		   	var $textblack = $(".playthis");
		   	var $textblack2 = $(".playthis2");
			sound_data =  sound_group_p1[8];
			sound_data2 =  sound_group_p1[9];
			play_text($textblack, $textblack.html(), $textblack2, $textblack2.html());  
			break;
			case 6:
		   	var $textblack = $(".playthis");
		   	var $textblack2 = $(".playthis2");
			sound_data =  sound_group_p1[10];
			sound_data2 =  sound_group_p1[11];
			play_text($textblack, $textblack.html(), $textblack2, $textblack2.html());  
			break;
			case 7:
		   	var $textblack = $(".playthis");
		   	var $textblack2 = $(".playthis2");
			sound_data =  sound_group_p1[12];
			sound_data2 =  sound_group_p1[13];
			play_text($textblack, $textblack.html(), $textblack2, $textblack2.html());  
			break;
			case 8:
		   	var $textblack = $(".playthis");
		   	var $textblack2 = $(".playthis2");
			sound_data =  sound_group_p1[14];
			sound_data2 =  sound_group_p1[15];
			play_text($textblack, $textblack.html(), $textblack2, $textblack2.html());  
			break;
		}
	}

function show_text($this,  $span_speec_text, message, interval) {
	  if (0 < message.length) {
	  	var nextText = message.substring(0,1);
	  	if(nextText == "<" ){
	  		$span_speec_text.append("<br>");
	  		message = message.substring(4, message.length);
	  	}else{
	  		$span_speec_text.append(nextText);
	  		message = message.substring(1, message.length);
	  	}
	  	$this.html($span_speec_text);
	  	$this.append(message);
	    setTimeout(function () {
	    	show_text($this,  $span_speec_text, message, interval);
	  	}, interval);
	  } else{
	  	textanimatecomplete = true;
	  }
	}

	function play_text($this, text, $this2, text2){
		$this.html("<span id='span_speec_text'></span>"+text);
		$prevBtn.hide(0);
		var $span_speec_text = $("#span_speec_text");
		show_text($this, $span_speec_text,text, 65);	// 65 ms is the interval found out by hit and trial
		sound_data.play();

		sound_data.bind('ended', function(){
			sound_data.unbind('ended');
			$this2.html("<span id='span_speec_text2'></span>"+text2);
			var $span_speec_text2 = $("#span_speec_text2");
			show_text($this2, $span_speec_text2,text2, 65);	// 65 ms is the interval found out by hit and trial
			sound_data2.play();
				$this.html(text);
			vocabcontroller.findwords(countNext);
		});

		sound_data2.bind('ended', function(){
				sound_data2.unbind('ended');
				soundplaycomplete = true;
				navigationcontroller();
				$this2.html(text2);
				vocabcontroller.findwords(countNext);
		});

		function ternimatesound_play_animate(){
			 intervalid = setInterval(function () {
					if(textanimatecomplete && soundplaycomplete){
						$this.html($span_speec_text.html());
						$this.css("background-color", "transparent");
						clearInterval(intervalid);
						intervalid = null;
						animationinprogress = false;
						vocabcontroller.findwords(countNext);
						if((countNext+1) == content.length){
							ole.footerNotificationHandler.pageEndSetNotification();
						}else{
							$nextBtn.show(0);
						}
						if(countNext>0){
							$prevBtn.show(0);
						}
				}
			}, 250);
		}
	}

	function resetthevocab($p_consideration){
		if(vocabcontroller != null){
			var spanarray = $p_consideration.find(".voacbunderline");
			if(spanarray.length > 0){
				vocabcontroller.reinstantiatehover(spanarray);
			}
		}
	}

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/ 
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');
    
    // call navigation controller
   	if(countNext == 0)
    navigationcontroller();

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */
  
	$nextBtn.on('click', function() {
			countNext++;	
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});
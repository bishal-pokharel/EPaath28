var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";

var sound_l_1 = new buzz.sound((soundAsset + "s3_p1.ogg"));
var sound_l_2 = new buzz.sound((soundAsset + "s3_p2.ogg"));
var content=[
	{
		// slide 0
		headerblock:[{
			textdata: data.string.p3_s0
		}],
	},
	{
		// slide 1
		headerblock:[{
			textdata: data.string.p3_s0
		}],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[{
			textclass: "clickon",
			textdata: data.string.p3_s1
		}],
		flexblock: [
	{
		flextextstyle: "flexitem",
		flexdata: data.string.p3_s2
	},
	{
		flextextstyle: "flexitem",
		flexdata: data.string.p3_s3
	},
	{
		flextextstyle: "flexitem",
		flexdata: data.string.p3_s4
	},
	{
		flextextstyle: "flexitem",
		flexdata: data.string.p3_s5
	},
	{
		flextextstyle: "flexitem",
		flexdata: data.string.p3_s6
	},
	{
		flextextstyle: "flexitem",
		flexdata: data.string.p3_s7
	}
	],
	},
	{
		// slide 1
		headerblock:[{
			textdata: data.string.p3_s0
		}],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[{
			textclass: "clickon",
			textdata: data.string.p3_s1
		}],
		flexblock: [
	{
		flextextstyle: "flexitem",
		flexdata: data.string.p3_s8
	},
	{
		flextextstyle: "flexitem",
		flexdata: data.string.p3_s9
	},
	{
		flextextstyle: "flexitem",
		flexdata: data.string.p3_s10
	},
	{
		flextextstyle: "flexitem",
		flexdata: data.string.p3_s11
	},
	{
		flextextstyle: "flexitem",
		flexdata: data.string.p3_s12
	}
	],
	},
];


$(function () { 
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;
  
  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);
  var vocabcontroller =  new Vocabulary();
  vocabcontroller.init();
/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("flexcontent", $("#flexcontent-partial").html());
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
   
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;
          
        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) { 
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/       
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
  
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/ 
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
  
   
  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.pageEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
   }

    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);          
        });
      }
    } 

  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);
      vocabcontroller.findwords(countNext);
	        
	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);

	    switch(countNext){
			case 0:
                $nextBtn.hide();
                sound_l_1.play().bind("ended",function(){
                	$nextBtn.show();
				});
                 break;
            case 1:
            sound_l_2.play();
            var catflag = true;
	    	var batflag = true;
	    	var satflag = true;
	    	var matflag = true;
	    	var fatflag = true;
	    	var ratflag = true;
	    	$(".flexitem").mouseenter(function(){
				var audio = new buzz.sound(soundAsset + $(this).text().toLowerCase() +".ogg");
				buzz.all().stop();
	    		audio.play();
	    		var disableFlag = $(this).text().toLowerCase();
	    		if(disableFlag == "cat"){
	    			catflag = false;
	    		}
	    		else if(disableFlag == "bat"){
	    			batflag = false;
	    		}
	    		else if(disableFlag == "sat"){
	    			satflag = false;
	    		}
	    		else if(disableFlag == "mat"){
	    			matflag = false;
	    		}
	    		else if(disableFlag == "fat"){
	    			fatflag = false;
	    		}
	    		else if(disableFlag == "rat"){
	    			ratflag = false;
	    		}
	    		checkfornext();
	    		function checkfornext(){
	    		if(catflag == false && batflag == false && satflag == false && matflag == false && fatflag == false && ratflag == false)
	    			navigationcontroller();
	    	}
	    	});
	    	break;
	    	case 2:
            var redflag = true;
	    	var fedflag = true;
	    	var bedflag = true;
	    	var wedflag = true;
	    	var ledflag = true;
	    	$(".flexitem").mouseenter(function(){
				var audio = new buzz.sound(soundAsset + $(this).text().toLowerCase() +".ogg");
				buzz.all().stop();
	    		audio.play();
	    		var disableFlag = $(this).text().toLowerCase();
	    		if(disableFlag == "red"){
	    			redflag = false;
	    		}
	    		else if(disableFlag == "fed"){
	    			fedflag = false;
	    		}
	    		else if(disableFlag == "bed"){
	    			bedflag = false;
	    		}
	    		else if(disableFlag == "wed"){
	    			wedflag = false;
	    		}
	    		else if(disableFlag == "led"){
	    			ledflag = false;
	    		}
	    		checkfornext();
	    	});
	    	function checkfornext(){
	    		if(redflag == false && fedflag == false && bedflag == false && wedflag == false && ledflag == false)
	    			navigationcontroller();
	    	}
	    	break;

	    	
	    }
  }

/*=====  End of Templates Block  ======*/
	function playaudio(sound_data, $dialog_container){
		var playing = true;
		$dialog_container.click(function(){
			if(!playing){
				playaudio(sound_data, $dialog_container);				
			}
			return false;
		});
		$prevBtn.hide(0);
		if((countNext+1) == content.length){
			ole.footerNotificationHandler.hideNotification();
		}else{
			$nextBtn.hide(0);					
		}
		sound_data.play();
		sound_data.bind('ended', function(){
			setTimeout(function(){
				$prevBtn.show(0);
				playing = false;
				sound_data.unbind('ended');
				if((countNext+1) == content.length){
					ole.footerNotificationHandler.pageEndSetNotification();
				}else{
					$nextBtn.show(0);					
				}
			}, 1000);
		});
	}
  
  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/ 
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');
    
    // call navigation controller
    if(countNext == 0)
    navigationcontroller();

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */
  
	$nextBtn.on('click', function() {
			countNext++;	
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});
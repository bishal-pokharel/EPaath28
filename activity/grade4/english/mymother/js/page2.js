var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";

var sound_l_1 = new buzz.sound((soundAsset + "1.ogg"));
var sound_l_2 = new buzz.sound((soundAsset + "2.ogg"));
var sound_l_3 = new buzz.sound((soundAsset + "3.ogg"));
var sound_l_4 = new buzz.sound((soundAsset + "4.ogg"));
var sound_l_5 = new buzz.sound((soundAsset + "5.ogg"));
var sound_l_6 = new buzz.sound((soundAsset + "6.ogg"));
var sound_l_7 = new buzz.sound((soundAsset + "7.ogg"));
var sound_l_8 = new buzz.sound((soundAsset + "8.ogg"));
var sound_l_9 = new buzz.sound((soundAsset + "9.ogg"));
var sound_l_10 = new buzz.sound((soundAsset + "10.ogg"));
var sound_l_11 = new buzz.sound((soundAsset + "11.ogg"));
var sound_l_12 = new buzz.sound((soundAsset + "12.ogg"));
var sound_l_13 = new buzz.sound((soundAsset + "13.ogg"));
var sound_l_14 = new buzz.sound((soundAsset + "14.ogg"));
var sound_l_15 = new buzz.sound((soundAsset + "15.ogg"));
var sound_l_16 = new buzz.sound((soundAsset + "16.ogg"));

var sound_group_p1 = [sound_l_1, sound_l_2, sound_l_3, sound_l_4, sound_l_5, sound_l_6, sound_l_7, sound_l_8, sound_l_9, sound_l_10, sound_l_11, sound_l_12, sound_l_13, sound_l_14, sound_l_15, sound_l_16];

var content=[
	{
		// slide0
		contentnocenteradjust: true,
		headerblock:[{
			textclass: "description_title",
			textdata: data.string.p0_title
		}],
		uppertextblockadditionalclass: "left_50",
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p1_s0
		},{
			textclass: "description",
			textdata: data.string.p1_s1,
			breakafterp: true
		},{
			textclass: "description",
			textdata: data.string.p1_s2
		},{
			textclass: "description",
			textdata: data.string.p1_s3
		}],
		lowertextblockadditionalclass: "right_50",
		lowertextblock:[{
			textclass: "description",
			textdata: data.string.p1_s4
		},{
			textclass: "description",
			textdata: data.string.p1_s5,
			breakafterp: true
		},{
			textclass: "description",
			textdata: data.string.p1_s6
		},{
			textclass: "description",
			textdata: data.string.p1_s7
		}]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);
	
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	

	/*==================================================
	 =            Handlers and helpers Block            =
	 ==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/

	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			// $nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	var audiocontroller = new AudioController($total_page, false,  65, 800, vocabcontroller);
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		vocabcontroller.findwords(countNext);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		// splitintofractions($(".fractionblock"));
		$description = $(".description");
		vocabcontroller.findwords(countNext);
		

		audiocontroller.init($description, sound_group_p1, countNext);
	}

	function templateCaller() {
		/*always hide next and previous navigation button unless
		 explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page, countNext + 1);

	}

	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click', function() {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
	/*=====  End of Templates Controller Block  ======*/
});

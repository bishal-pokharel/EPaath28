var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/";

var sound_l_1 = new buzz.sound((soundAsset + "s4_p1.ogg"));
var content=[
	//slide 0	
	{
		exerciseblock: [
			{					
				textclass : 'instruction my_font_big',
				questiondata: data.string.p4_q1,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.p4_s9,
					},
					{
						option_class: "class2",
						optiondata: data.string.p4_s19,
					},
					{
						option_class: "class3",
						optiondata: data.string.p4_s5, 
					},
					{
						option_class: "class4",
						optiondata: data.string.p4_s11, 
					}],
			} 
		] 
	},
	//slide 1	
	{
		exerciseblock: [
			{					
				textclass : 'instruction my_font_big',
				questiondata: data.string.p4_q2,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.p4_s4,
					},
					{
						option_class: "class2",
						optiondata: data.string.p4_s10,
					},
					{
						option_class: "class3",
						optiondata: data.string.p4_s13, 
					},
					{
						option_class: "class4",
						optiondata: data.string.p4_s18, 
					}],
			} 
		] 
	},
	//slide 2
	{
		exerciseblock: [
			{					
				textclass : 'instruction my_font_big',
				questiondata: data.string.p4_q3,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.p4_s14,
					},
					{
						option_class: "class2",
						optiondata: data.string.p4_s4,
					},
					{
						option_class: "class3",
						optiondata: data.string.p4_s20, 
					},
					{
						option_class: "class4",
						optiondata: data.string.p4_s28, 
					}],
			} 
		] 
	},
	//slide 3
	{
		exerciseblock: [
			{					
				textclass : 'instruction my_font_big',
				questiondata: data.string.p4_q4,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.p4_s19,
					},
					{
						option_class: "class2",
						optiondata: data.string.p4_s15,
					},
					{
						option_class: "class3",
						optiondata: data.string.p4_s10, 
					},
					{
						option_class: "class4",
						optiondata: data.string.p4_s29, 
					}],
			} 
		] 
	},
	//slide 4
	{
		exerciseblock: [
			{					
				textclass : 'instruction my_font_big',
				questiondata: data.string.p4_q5,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.p4_s22,
					},
					{
						option_class: "class2",
						optiondata: data.string.p4_s11,
					},
					{
						option_class: "class3",
						optiondata: data.string.p4_s21, 
					},
					{
						option_class: "class4",
						optiondata: data.string.p4_s28, 
					}],
			} 
		] 
	},
	//slide 5
	{
		exerciseblock: [
			{					
				textclass : 'instruction my_font_big',
				questiondata: data.string.p4_q6,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.p4_s29,
					},
					{
						option_class: "class2",
						optiondata: data.string.p4_s15,
					},
					{
						option_class: "class3",
						optiondata: data.string.p4_s11, 
					},
					{
						option_class: "class4",
						optiondata: data.string.p4_s26, 
					}],
			} 
		] 
	},
];

/*remove this for non random questions*/
$(function () {	
	var testin = new NumberTemplate();
   
	var exercise = new template_exercise_mcq_monkey(content, testin, true);
	exercise.create_exercise();
});

function template_exercise_mcq_monkey(content, scoring){
	var $board			= $('.board');
	var $nextBtn		= $("#activity-page-next-btn-enabled");
	var $prevBtn		= $("#activity-page-prev-btn-enabled");
	var countNext		= 0;
	var testin			= new EggTemplate();
   
	var wrong_clicked 	= false;

	var total_page = content.length;

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		
		testin.numberOfQuestions();
		/*for randomizing the options*/
		var option_position = [1,2,3,4];
		option_position.shufflearray();
		countNext==0?sound_l_1.play():"";
		for(var op=0; op<4; op++){
			$('.main-container').eq(op).addClass('option-pos-'+option_position[op]);
		}
		$('.option-pos-1').hover(function(){
			$('.center-sundar').attr('src', 'images/sundar/top-right.png');
			$('.center-sundar').css('transform','scaleX(-1)');
		}, function(){
			
		});
		//bottom-left
		$('.option-pos-3').hover(function(){
			$('.center-sundar').attr('src', 'images/sundar/bottom-right.png');
			$('.center-sundar').css('transform','scaleX(-1)');
		}, function(){
			
		});
		//top-right
		$('.option-pos-2').hover(function(){
			$('.center-sundar').attr('src', 'images/sundar/top-right.png');
			$('.center-sundar').css('transform','none');
		}, function(){
			
		});
		//bottom-right
		$('.option-pos-4').hover(function(){
			$('.center-sundar').attr('src', 'images/sundar/bottom-right.png');
			$('.center-sundar').css('transform','none');
		}, function(){
			
		});

		var ansClicked = false;
		var wrngClicked = 0;
		var correct_images = ['correct-1.png', 'correct-2.png', 'correct-3.png'];
		$(".option-container").click(function(){
			if($(this).hasClass("class1")){
				if(wrong_clicked<1){
					testin.update(true);
				}
				var rand_img = Math.floor(Math.random()*correct_images.length);
				$('.option-pos-1, .option-pos-2, .option-pos-3, .option-pos-4').off('mouseenter mouseleave');
				$('.center-sundar').attr('src', 'images/sundar/'+correct_images[rand_img]);
				$(".option-container").css('pointer-events','none');
	 			play_correct_incorrect_sound(1);
				$(this).addClass('correct-ans');
				$(this).parent().children('.correct-icon').show(0);
				wrong_clicked = 0;
				if(countNext != 5)
					$nextBtn.show(0);
				else
					ole.footerNotificationHandler.pageEndSetNotification();
			}
			else{
				var classname_monkey = $(this).parent().attr('class').replace(/main-container/, '');
				classname_monkey = classname_monkey.replace(/ /g, '');
				$('.'+classname_monkey).off('mouseenter mouseleave');
				if(wrong_clicked==0){
					$('.center-sundar').attr('src', 'images/sundar/incorrect-1.png');
				} else if(wrong_clicked == 1){
					$('.center-sundar').attr('src', 'images/sundar/incorrect-2.png');
				} else {
					$('.center-sundar').attr('src', 'images/sundar/incorrect-3.png');
				}
				testin.update(false);
	 			play_correct_incorrect_sound(0);
				$(this).addClass('incorrect-ans');
				$(this).parent().children('.incorrect-icon').show(0);
				wrong_clicked++;
			}
		}); 
	};


	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');		
		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   
	};
	
	this.create_exercise = function(){
		content.shufflearray();
		if(typeof scoring != 'undefined'){
			testin = scoring;
		}
	 	testin.init(total_page);
 	
		templateCaller();	
		$nextBtn.on("click", function(){
			countNext++;
			testin.gotoNext();
			templateCaller();
		});
		$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
			countNext--;			
			templateCaller();
		});
	};
}


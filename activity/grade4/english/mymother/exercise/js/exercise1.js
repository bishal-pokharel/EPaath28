var imgpath = $ref+"/images/";

var content=[
	//slide 0	
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.top1,
			textclass : 'instruction my_font_big',
		}],
		passageblock:[
		{
			textdata : data.string.ques1,
			textclass : 'passage',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text'
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.q1opt1,
			optionclass : 'class1'
		},
		{
			textdata : data.string.q1opt3,
			optionclass : 'class3'
		},
		{
			textdata : data.string.q1opt4,
			optionclass : 'class4'
		}],
	},
	//slide 1
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.top2,
			textclass : 'instruction my_font_big',
		}],
		passageblock:[
		{
			textdata : data.string.ques2,
			textclass : 'passage',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text'
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.q2opt1,
			optionclass : 'class1'
		},
		{
			textdata : data.string.q2opt2,
			optionclass : 'class2'
		},
		{
			textdata : data.string.q2opt3,
			optionclass : 'class3'
		}],
	},
	//slide 2
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.top3,
			textclass : 'instruction my_font_big',
		}],
		passageblock:[
		{
			// textdata : data.string.ques3,
			// textclass : 'passage',
			// datahighlightflag : true,
			// datahighlightcustomclass : 'highlight_text',
			imgblcok:true,
            imageblock:[
                {
                    imagestoshow:[{
                        imgclass: "mother",
                        imgsrc: imgpath+"mother02.png"
                    }]
                }
            ]
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.q3opt1,
			optionclass : 'class1'
		},
		{
			textdata : data.string.q3opt3,
			optionclass : 'class3'
		},
		{
			textdata : data.string.q3opt4,
			optionclass : 'class4'
		}],
	},
	//slide 3
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.top4,
			textclass : 'instruction my_font_big',
		}],
		passageblock:[
		{
			textdata : data.string.ques4,
			textclass : 'passage',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text'
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.q4opt1,
			optionclass : 'class1'
		},
		{
			textdata : data.string.q4opt2,
			optionclass : 'class2'
		},
		{
			textdata : data.string.q4opt3,
			optionclass : 'class3'
		}],
	},
	//slide 4	
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.top5,
			textclass : 'instruction my_font_big',
		}],
		passageblock:[
		{
			textdata : data.string.ques5,
			textclass : 'passage',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text'
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.q5opt1,
			optionclass : 'class1'
		},
		{
			textdata : data.string.q5opt2,
			optionclass : 'class2'
		},
		{
			textdata : data.string.q5opt3,
			optionclass : 'class3'
		}],
	},
	//slide 5
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.top6,
			textclass : 'instruction my_font_big',
		}],
		passageblock:[
		{
			textdata : data.string.ques6,
			textclass : 'passage',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text'
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.q6opt1,
			optionclass : 'class1'
		},
		{
			textdata : data.string.q6opt2,
			optionclass : 'class2'
		},
		{
			textdata : data.string.q6opt3,
			optionclass : 'class3'
		}],
	},
	//slide 6
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.top7,
			textclass : 'instruction my_font_big',
		}],
		passageblock:[
		{
			textdata : data.string.ques7,
			textclass : 'passage',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text'
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.q7opt1,
			optionclass : 'class1'
		},
		{
			textdata : data.string.q7opt3,
			optionclass : 'class3'
		},
		{
			textdata : data.string.q7opt4,
			optionclass : 'class4'
		}],
	},
	//slide 7
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.top8,
			textclass : 'instruction my_font_big',
		}],
		passageblock:[
		{
			textdata : data.string.ques8,
			textclass : 'passage',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text'
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.q8opt1,
			optionclass : 'class1'
		},
		{
			textdata : data.string.q8opt2,
			optionclass : 'class2'
		},
		{
			textdata : data.string.q8opt3,
			optionclass : 'class3'
		}],
	},
	//slide 8
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.top9,
			textclass : 'instruction my_font_big',
		}],
		passageblock:[
		{
			textdata : data.string.ques9,
			textclass : 'passage',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text'
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.q9opt1,
			optionclass : 'class1'
		},
		{
			textdata : data.string.q9opt3,
			optionclass : 'class3'
		},
		{
			textdata : data.string.q9opt4,
			optionclass : 'class4'
		}],
	},
	//slide 9
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.top10,
			textclass : 'instruction my_font_big',
		}],
		passageblock:[
		{
			textdata : data.string.ques10,
			textclass : 'passage',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text'
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.q10opt1,
			optionclass : 'class1'
		},
		{
			textdata : data.string.q10opt3,
			optionclass : 'class3'
		},
		{
			textdata : data.string.q10opt4,
			optionclass : 'class4'
		}],
	}
];

// content.shufflearray();


$(function () 
{	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	var score = 0;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ? 
		islastpageflag = false : 
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}
	
	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;
          
        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) { 
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/       
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

	//add bg to rhino
	// add_bg(['bg_1.png','bg_2.png','bg_3.png']);
	// add_bg(['bg01.png','bg02.png','bg03.png']);
	// add_bg(['city_1.png','city_2.png','city_3.png']);
	var rhino = new RhinoTemplate();
   
	rhino.init($total_page);



	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		
		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		
		//randomize options
		var parent = $(".optionsblock");
		var divs = parent.children();
		rhino.numberOfQuestions();
		while (divs.length) {
	    	parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
		$('.question_number').html((countNext+1));
		var wrong_clicked = false;
		$(".optioncontainer").click(function(){
			if($(this).hasClass("class1")){
				if(!wrong_clicked){
					rhino.update(true);
				}
				$(this).children('.correct-icon').show(0);
				$(".optioncontainer").css('pointer-events', 'none');
				$(this).css({
					'border': '3px solid #FCD172',
					'background-color': '#6EB260',
					'color': 'white'
				});
				play_correct_incorrect_sound(1);
				if(countNext != $total_page)
					$nextBtn.show(0);
			}
			else{
				if(!wrong_clicked){
					rhino.update(false);
				}
				$(this).children('.incorrect-icon').show(0);
				$(this).css({
					'background-color': '#BA6B82',
					'border': 'none'
				});
				wrong_clicked = true;
				play_correct_incorrect_sound(0);
			}
		}); 
	}
	
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');		

		// call navigation controller
		navigationcontroller();	

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   

	}

	// first call to template caller
	templateCaller();	

	/* navigation buttons event handlers */
	
	$nextBtn.on("click", function(){
		countNext++;
		if(countNext < 13){
			templateCaller();
			rhino.gotoNext();
		}		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
	
});
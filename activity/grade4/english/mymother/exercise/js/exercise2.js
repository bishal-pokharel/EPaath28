var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/";

var sound_l_1 = new buzz.sound((soundAsset + "ex.ogg"));
var balloon_src = [
						"images/ex_balloon/balloon01.png",
						"images/ex_balloon/balloon02.png",
						"images/ex_balloon/balloon03.png",
						"images/ex_balloon/balloon04.png",
						"images/ex_balloon/balloon05.png",
						"images/ex_balloon/balloon06.png",
					];
var pop_balloon_src = "images/ex_balloon/pop.png";
var pop_star_src = "images/star_1.gif";
var pop_sound = new buzz.sound(('sounds/common/balloon-pop.ogg'));

var content=[
	//slide 0	
	{	
		contentblockadditionalclass: 'main_bg',
		exerciseblock: [
			{
				instructiondata : data.string.e1instruction,
				instructionclass : 'my_font_big',
				
				questionclass: 'my_font_big',
				questiondata: data.string.etext1,
				datahighlightflag: true,
				datahighlightcustomclass: 'blank-space',
				
				option: [
					{
						mc_class: "class1",
						optiondata: data.string.eopt1,
					},
					{
						mc_class: "class2",
						optiondata: data.string.eopt2,
					},
					{
						mc_class: "class3",
						optiondata: data.string.eopt3,
					},
					{
						mc_class: "class4",
						optiondata: data.string.eopt4,
					}],
			} 
		] 
	},
	//slide 1	
	{	
		contentblockadditionalclass: 'main_bg',
		exerciseblock: [
			{
				instructiondata : data.string.e1instruction,
				instructionclass : 'my_font_big',
				
				questionclass: 'my_font_big',
				questiondata: data.string.etext2,
				datahighlightflag: true,
				datahighlightcustomclass: 'blank-space',
				
				option: [
					{
						mc_class: "class1",
						optiondata: data.string.eopt2,
					},
					{
						mc_class: "class2",
						optiondata: data.string.eopt5,
					},
					{
						mc_class: "class3",
						optiondata: data.string.eopt6,
					},
					{
						mc_class: "class4",
						optiondata: data.string.eopt7,
					}],
			} 
		] 
	},
	//slide 2
	{	
		contentblockadditionalclass: 'main_bg',
		exerciseblock: [
			{
				instructiondata : data.string.e1instruction,
				instructionclass : 'my_font_big',
				
				questionclass: 'my_font_big',
				questiondata: data.string.etext3,
				datahighlightflag: true,
				datahighlightcustomclass: 'blank-space',
				
				option: [
					{
						mc_class: "class1",
						optiondata: data.string.eopt3,
					},
					{
						mc_class: "class2",
						optiondata: data.string.eopt8,
					},
					{
						mc_class: "class3",
						optiondata: data.string.eopt9,
					},
					{
						mc_class: "class4",
						optiondata: data.string.eopt10,
					}],
			} 
		] 
	},
	//slide 3
	{	
		contentblockadditionalclass: 'main_bg',
		exerciseblock: [
			{
				instructiondata : data.string.e1instruction,
				instructionclass : 'my_font_big',
				
				questionclass: 'my_font_big',
				questiondata: data.string.etext4,
				datahighlightflag: true,
				datahighlightcustomclass: 'blank-space',
				
				option: [
					{
						mc_class: "class1",
						optiondata: data.string.eopt4,
					},
					{
						mc_class: "class2",
						optiondata: data.string.eopt11,
					},
					{
						mc_class: "class3",
						optiondata: data.string.eopt12,
					},
					{
						mc_class: "class4",
						optiondata: data.string.eopt13,
					}],
			} 
		] 
	},
	//slide 4
	{	
		contentblockadditionalclass: 'main_bg',
		exerciseblock: [
			{
				instructiondata : data.string.e1instruction,
				instructionclass : 'my_font_big',
				
				questionclass: 'my_font_big',
				questiondata: data.string.etext5,
				datahighlightflag: true,
				datahighlightcustomclass: 'blank-space',
				
				option: [
					{
						mc_class: "class1",
						optiondata: data.string.eopt5,
					},
					{
						mc_class: "class2",
						optiondata: data.string.eopt14,
					},
					{
						mc_class: "class3",
						optiondata: data.string.eopt15,
					},
					{
						mc_class: "class4",
						optiondata: data.string.eopt16,
					}],
			} 
		] 
	},
	//slide 5
	{	
		contentblockadditionalclass: 'main_bg',
		exerciseblock: [
			{
				instructiondata : data.string.e1instruction,
				instructionclass : 'my_font_big',
				
				questionclass: 'my_font_big',
				questiondata: data.string.etext6,
				datahighlightflag: true,
				datahighlightcustomclass: 'blank-space',
				
				option: [
					{
						mc_class: "class1",
						optiondata: data.string.eopt6,
					},
					{
						mc_class: "class2",
						optiondata: data.string.eopt17,
					},
					{
						mc_class: "class3",
						optiondata: data.string.eopt18,
					},
					{
						mc_class: "class4",
						optiondata: data.string.eopt19,
					}],
			} 
		] 
	},
	
	//slide 6
	{	
		contentblockadditionalclass: 'main_bg',
		exerciseblock: [
			{
				instructiondata : data.string.e1instruction,
				instructionclass : 'my_font_big',
				
				questionclass: 'my_font_big',
				questiondata: data.string.etext7,
				datahighlightflag: true,
				datahighlightcustomclass: 'blank-space',
				
				option: [
					{
						mc_class: "class1",
						optiondata: data.string.eopt7,
					},
					{
						mc_class: "class2",
						optiondata: data.string.eopt20,
					},
					{
						mc_class: "class3",
						optiondata: data.string.eopt21,
					},
					{
						mc_class: "class4",
						optiondata: data.string.eopt22,
					}],
			} 
		] 
	},
	//slide 7
	{	
		contentblockadditionalclass: 'main_bg',
		exerciseblock: [
			{
				instructiondata : data.string.e1instruction,
				instructionclass : 'my_font_big',
				
				questionclass: 'my_font_big',
				questiondata: data.string.etext8,
				datahighlightflag: true,
				datahighlightcustomclass: 'blank-space',
				
				option: [
					{
						mc_class: "class1",
						optiondata: data.string.eopt8,
					},
					{
						mc_class: "class2",
						optiondata: data.string.eopt1,
					},
					{
						mc_class: "class3",
						optiondata: data.string.eopt3,
					},
					{
						mc_class: "class4",
						optiondata: data.string.eopt5,
					}],
			} 
		] 
	},
	//slide 8
	{	
		contentblockadditionalclass: 'main_bg',
		exerciseblock: [
			{
				instructiondata : data.string.e1instruction,
				instructionclass : 'my_font_big',
				
				questionclass: 'my_font_big',
				questiondata: data.string.etext9,
				datahighlightflag: true,
				datahighlightcustomclass: 'blank-space',
				
				option: [
					{
						mc_class: "class1",
						optiondata: data.string.eopt9,
					},
					{
						mc_class: "class2",
						optiondata: data.string.eopt2,
					},
					{
						mc_class: "class3",
						optiondata: data.string.eopt11,
					},
					{
						mc_class: "class4",
						optiondata: data.string.eopt6,
					}],
			} 
		] 
	},
	//slide 9
	{	
		contentblockadditionalclass: 'main_bg',
		exerciseblock: [
			{
				instructiondata : data.string.e1instruction,
				instructionclass : 'my_font_big',
				
				questionclass: 'my_font_big',
				questiondata: data.string.etext10,
				datahighlightflag: true,
				datahighlightcustomclass: 'blank-space',
				
				option: [
					{
						mc_class: "class1",
						optiondata: data.string.eopt10,
					},
					{
						mc_class: "class2",
						optiondata: data.string.eopt20,
					},
					{
						mc_class: "class3",
						optiondata: data.string.eopt21,
					},
					{
						mc_class: "class4",
						optiondata: data.string.eopt22,
					}],
			} 
		] 
	}
];

content.shufflearray();
// var content = content2;

$(function () 
{	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	var score = 0;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ? 
		islastpageflag = false : 
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}
	
	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;
          
        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) { 
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/       
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
      
	//create scoretemplate
	var scoretemplate = new NumberTemplate();
   
 	//eggTemplate.eggMove(countNext);
	scoretemplate.init(10);

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		
		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		balloon_src.shufflearray();

        countNext==0?sound_l_1.play():"";

        scoretemplate.numberOfQuestions();

		$('.class1>.balloon-icon').attr('src', balloon_src[0]);
		$('.class2>.balloon-icon').attr('src', balloon_src[1]);
		$('.class3>.balloon-icon').attr('src', balloon_src[2]);
		$('.class4>.balloon-icon').attr('src', balloon_src[3]);
		// for( var mm =1; mm<5; mm++){
			// $('.main-container').removeClass('option_'+mm);
		// }
		var option_position = [1,2,3,4];
		option_position.shufflearray();
		for(var op=0; op<4; op++){
			$('.main-container').eq(op).addClass('option_'+option_position[op]);
		}
		var wrong_clicked = false;
        hoverOption();
		$(".main-container").click(function(){
			if($(this).hasClass("class1")){
				if(!wrong_clicked){
					scoretemplate.update(true);
				}
				
				setTimeout( function(){
					$textballoon.hide(0);
					play_correct_incorrect_sound(1);
				}, 1000);
				
				$(".main-container").css('pointer-events', 'none');
				pop_sound.play();
				var $balloon = $(this);
				var dt = new Date();
				$(this).children('.balloon-icon').css({
					'z-index': 100,
					'height': '40%',
					'top': '20%',
					'width': '100%'}).attr('src', pop_star_src+'?' + dt.getTime());
				$balloon.children('.correct-icon').fadeIn(600, function(){
					if(countNext != $total_page)
						$nextBtn.show(0);
				});
			}
			else{
				if(!wrong_clicked){
					scoretemplate.update(false);
				}
				$(this).css('transform', 'scale(0.8)');
				$(this).children('.incorrect-icon').show(0);
				wrong_clicked = true;
				play_correct_incorrect_sound(0);
				wrong_clicked = true;
			}
		});

	}
	function hoverOption(){
        $( ".main-container" ).hover(
            function() {
               $(this).find('p').css("color","black");
            }, function() {
                $(this).find('p').css("color","white");
            }
        );
	}
	
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');		

		// call navigation controller
		navigationcontroller();	

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   

	}

	// first call to template caller
	templateCaller();	

	/* navigation buttons event handlers */
	
	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
		scoretemplate.gotoNext();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
	
});
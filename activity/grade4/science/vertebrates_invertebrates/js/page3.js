var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: " commontext text1",
                textdata: data.string.invertebratesanim
            },
            {
                textclass: "commontext text2 slideL",
                textdata: data.string.p3text1
            }
        ],
        imageblock:[{
            imagestoshow:[
                {
                    imgdiv:"earthworm",
                    imgclass: "relativecls earthwormimg",
                    imgid : 'earthwormImg',
                    imgsrc: "",
                }
            ]
        }]
    },
   // slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: " commontext text1",
                textdata: data.string.invertebratesanim
            },
            {
                textclass: "commontext slideR text2",
                textdata: data.string.p3text2
            }
        ],
        imageblock:[{
            imagestoshow:[
                {
                    imgdiv:"butterfly",
                    imgclass: "relativecls zoomInEffect butterflyimg",
                    imgid : 'butterflyImg',
                    imgsrc: "",
                },
                {
                    imgdiv:"ladybug",
                    imgclass: "relativecls zoomInEffect ladybugimg",
                    imgid : 'ladybugImg',
                    imgsrc: "",
                },
                {
                    imgdiv:"earthworm1",
                    imgclass: "relativecls zoomInEffect earthwormimg",
                    imgid : 'earthwormImg',
                    imgsrc: "",
                }
            ]
        }]
    },
    // //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: " commontext text1",
                textdata: data.string.invertebratesanim
            },
            {
                textclass: "commontext slideR text2",
                textdata: data.string.p3text3
            }
        ],
        imageblock:[{
            imagestoshow:[
                {
                    imgdiv:"slug",
                    imgclass: "relativecls fadeIn slugimg",
                    imgid : 'slugImg',
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: " commontext text1",
                textdata: data.string.invertebratesanim
            },
            {
                textclass: "commontext zoomInEffect text2",
                textdata: data.string.p3text4
            }
        ],
        imageblock:[{
            imagestoshow:[
                {
                    imgdiv:"snail",
                    imgclass: "relativecls slideL snailimg",
                    imgid : 'snailImg',
                    imgsrc: "",
                },
                {
                    imgdiv:"crab",
                    imgclass: "relativecls slideR crabimg",
                    imgid : 'crabImg',
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: " commontext text1",
                textdata: data.string.invertebratesanim
            }
        ],
        middletext:[
            {
                textdiv:"middlebox",
                textclass: "clicktext",
                textdata: data.string.clickpic,
                textchng:data.string.invertanimals

            }
        ],
        imageblock:[{
            imagestoshow:[
                {
                    imgdiv:"animaldiv butterflyrealdiv sound_butterfly",
                    imgclass: "relativecls butterflyrealimg ",
                    imgid : 'butterfly',
                    imgsrc: "",
                    imgtxt:data.string.butterfly,
                },
                {
                    imgdiv:"animaldiv crabrealdiv sound_crab",
                    imgclass: "relativecls crabrrealimg",
                    imgid : 'crab',
                    imgsrc: "",
                    imgtxt:data.string.crab,
                },
                {
                    imgdiv:"animaldiv gadaularealdiv sound_earthworm",
                    imgclass: "relativecls gadaularealimg",
                    imgid : 'gadaula',
                    imgsrc: "",
                    imgtxt:data.string.earthworm,
                },
                {
                    imgdiv:"animaldiv grasshopperrealdiv sound_grasshopper",
                    imgclass: "relativecls grasshopperrealimg",
                    imgid : 'grasshopper',
                    imgsrc: "",
                    imgtxt:data.string.grasshopper,
                },
                {
                    imgdiv:"animaldiv ladybugrealdiv sound_ladybug",
                    imgclass: "relativecls ladybugrealimg",
                    imgid : 'ladybug',
                    imgsrc: "",
                    imgtxt:data.string.ladybug,
                },
                {
                    imgdiv:"animaldiv leachrealdiv sound_leech",
                    imgclass: "relativecls leachrealimg",
                    imgid : 'leach',
                    imgsrc: "",
                    imgtxt:data.string.leach,
                },
                {
                    imgdiv:"animaldiv musquitorealdiv sound_mosquito",
                    imgclass: "relativecls musquitorealimg",
                    imgid : 'musquito',
                    imgsrc: "",
                    imgtxt:data.string.musquito,
                },
                {
                    imgdiv:"animaldiv snailrealdiv sound_snail",
                    imgclass: "relativecls snailrealimg",
                    imgid : 'snail',
                    imgsrc: "",
                    imgtxt:data.string.snail,
                }
            ]
        }]
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "common-css", src: $ref + "css/common.css", type: createjs.AbstractLoader.CSS},
            {id: "page1-css", src: $ref + "css/page1.css", type: createjs.AbstractLoader.CSS},

            {id: "earthwormImg", src: imgpath+"Page03/earthworm.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "slugImg", src: imgpath+"Page03/slug.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "butterflyImg", src: imgpath+"Page03/butterfly-animated-gif-45.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "ladybugImg", src: imgpath+"Page01/ladybug.png", type: createjs.AbstractLoader.IMAGE},
            {id: "snailImg", src: imgpath+"Page01/snail.png", type: createjs.AbstractLoader.IMAGE},
            {id: "crabImg", src: imgpath+"Page01/crab.png", type: createjs.AbstractLoader.IMAGE},

            {id: "bee", src: imgpath+"real_images/bee.png", type: createjs.AbstractLoader.IMAGE},
            {id: "butterfly", src: imgpath+"real_images/butterfly.png", type: createjs.AbstractLoader.IMAGE},
            {id: "crab", src: imgpath+"real_images/crab.png", type: createjs.AbstractLoader.IMAGE},
            {id: "gadaula", src: imgpath+"real_images/gadaula.png", type: createjs.AbstractLoader.IMAGE},
            {id: "grasshopper", src: imgpath+"real_images/grasshopper7.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ladybug", src: imgpath+"real_images/ladybug.png", type: createjs.AbstractLoader.IMAGE},
            {id: "leach", src: imgpath+"real_images/leach.png", type: createjs.AbstractLoader.IMAGE},
            {id: "musquito", src: imgpath+"real_images/musquito.png", type: createjs.AbstractLoader.IMAGE},
            {id: "snail", src: imgpath+"real_images/snail.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset+"p3_s0.ogg"},
            {id: "sound_1", src: soundAsset+"p3_s1.ogg"},
            {id: "sound_2", src: soundAsset+"p3_s2.ogg"},
            {id: "sound_3", src: soundAsset+"p3_s3.ogg"},
            {id: "sound_4", src: soundAsset+"p3_s4.ogg"},

            {id: "sound_butterfly", src: soundAsset+"Inve/butterfly.ogg"},
            {id: "sound_crab", src: soundAsset+"Inve/crab.ogg"},
            {id: "sound_earthworm", src: soundAsset+"Inve/earthworm.ogg"},
            {id: "sound_grasshopper", src: soundAsset+"Inve/grasshopper.ogg"},
            {id: "sound_ladybug", src: soundAsset+"Inve/ladybug.ogg"},
            {id: "sound_leech", src: soundAsset+"Inve/leech.ogg"},
            {id: "sound_mosquito", src: soundAsset+"Inve/mosquito.ogg"},
            {id: "sound_snail", src: soundAsset+"Inve/snail.ogg"},
            {id: "sound_4_1", src: soundAsset+"p3_s4_1.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

    /*===============================================
    =            data highlight function            =
    ===============================================*/
    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;bounceOnce
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function (index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

    /*=====  End of data highlight function  ======*/


    /*======================================================
     =            Navigation Controller Function            =
     ======================================================*/

    function navigationcontroller(islastpageflag) {
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean' ?
                alert("NavigationController : Hi Master, please provide a boolean parameter") :
                null;

        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
            $prevBtn.css('display', 'none');
        }
        else if ($total_page == 1) {
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');

            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if (countNext > 0 && countNext < $total_page - 1) {
            $nextBtn.show(0);
            $prevBtn.show(0);
        }
        else if (countNext == $total_page - 1) {
            $nextBtn.css('display', 'none');
            $prevBtn.show(0);

            // if lastpageflag is true
            ole.footerNotificationHandler.pageEndSetNotification();
        }

    }

    /*=====  End of user navigation controller function  ======*/


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);


        // highlight any text inside board div with datahighlightflag set true
        // texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext);
        switch(countNext){
            case 0:
                sound_player("sound_0",true);
                break;
            case 1:
                animateInvertebrates();
                sound_player("sound_1",true);
                break;
            case 2:
                sound_player("sound_2",true);
                break;
            case 3:
                sound_player("sound_3",true);
                break;
            case 4:
                sound_player("sound_4",false);
                showAnimalNames();
                break;
            default:
                navigationcontroller();
                break;
        }
    }

    function nav_button_controls(delay_ms) {
        timeoutvar = setTimeout(function () {
            if (countNext == 0) {
                $nextBtn.show(0);
            } else if (countNext > 0 && countNext == $total_page - 1) {
                $prevBtn.show(0);
                ole.footerNotificationHandler.pageEndSetNotification();
            } else {
                $prevBtn.show(0);
                $nextBtn.show(0);
            }
        }, delay_ms);
    }

    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
          navigate?navigationcontroller():'';
        });
    }


    function put_image(content, count) {
        var contentCount=content[count];
        var imageblockcontent=contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent,contentCount)
    }


    function dynamicimageload(imageblockcontent,contentCount){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }



    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function animateDiv(){
        $(".bone,.backbone").hide(0);
        setTimeout(function(){
            $(".muscle").addClass("grayout");
            $(".bone").show(0).addClass("slideL");
            setTimeout(function () {
                $(".bone").addClass("grayout");
                $(".backbone").show(0).addClass("slideL");
                navigationcontroller();
            },3000);
        },3000);
    }

    function animateInvertebrates(){
        $(".earthworm1,.butterfly,.slug").hide(0);
            setTimeout(function(){
                $(".butterfly").show(0);
                setTimeout(function(){
                    $(".earthworm1").show(0);
                },2000);
            },2000);

    }
    function showAnimalNames(){
        $(".secondText").hide(0);
        $(".animaldiv").each(function () {
            $(this).find('p').hide(0);
        });
        $(".animaldiv").click(function(){
            $(this).find("p").addClass("zoomInEffect").show(0);
            var classStr = $(this).attr('class');
            var lastClass = classStr.substr( classStr.lastIndexOf(' ') + 1);
            sound_player(lastClass,false);
            if(!($(".animaldiv p").is(":hidden"))){
                $(".firstText").hide(0);
                $(".middlebox").addClass("flipOnce");
                $(".secondText").show(0);
                setTimeout(function () {
                    sound_player("sound_4_1",true);
                },1000);
            }
        });

    }

});

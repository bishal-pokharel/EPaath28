var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var soundAsset1 = $ref+"/sounds/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: " commontext diy",
                textdata: data.string.diy
            }
        ]
    },
    //slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        imgwithdiv: [{
            divcls: "quesdiv",
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "animal giraffediv",
                        imgclass: "relativecls giraffeimg",
                        imgid: 'giraffeImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "animal elephantdiv",
                        imgclass: "relativecls elephantimg",
                        imgid: 'elephantImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "snaildiv",
                        imgclass: "relativecls snailimg",
                        imgid: 'snailImg',
                        imgsrc: "",
                    },

                    {
                        imgdiv: "animal pandadiv",
                        imgclass: "relativecls pandaimg",
                        imgid: 'pandaImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "animal cheetadiv",
                        imgclass: "relativecls cheetaimg",
                        imgid: 'cheetaImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv:"animal peacockdiv",
                        imgclass: "relativecls peacockimg",
                        imgid : 'peacockImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "earthwormdiv",
                        imgclass: "relativecls earthwormimg",
                        imgid: 'earthwormImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "butterflydiv",
                        imgclass: "relativecls butterflyimg",
                        imgid: 'butterflyImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "grasshopperdiv",
                        imgclass: "relativecls grasshopperimg",
                        imgid: 'grasshopperImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "crabdiv",
                        imgdiv: "crabdiv",
                        imgclass: "relativecls crabimg",
                        imgid: 'crabImg',
                        imgsrc: "",
                    }
                ]
            }]
        },
            {
                extradiv:[{
                    divcls:"outerdiv",
                    divtxt:data.string.p4text1,
                    imageblock: [{
                        imagestoshow: [
                            {
                                imgdiv: "cameradiv",
                                imgclass: "relativecls cameraimg",
                                imgid: 'cameraImg',
                                imgsrc: "",
                            },
                        ]
                    }],
                }],
                divcls: "ansdiv",
                imageblock: [{
                    imagestoshow: [
                        {
                            imgdiv: "giraffeansdiv",
                            imgclass: "relativecls giraffeimg",
                            imgid: 'giraffeImg',
                            imgsrc: "",
                            imgtxtclass:"",
                            imgtxt:data.string.giraffe
                        },
                        {
                            imgdiv:"peacockansdiv",
                            imgclass: "relativecls peacockimg",
                            imgid : 'peacockImg',
                            imgsrc: "",
                            imgtxtclass:"",
                            imgtxt:data.string.peacock
                        },
                        {
                            imgdiv: "elephantansdiv",
                            imgclass: "relativecls elephantimg",
                            imgid: 'elephantImg',
                            imgsrc: "",
                            imgtxtclass:"",
                            imgtxt:data.string.elephant
                        },

                        {
                            imgdiv: "pandaansdiv",
                            imgclass: "relativecls pandaimg",
                            imgid: 'pandaImg',
                            imgsrc: "",
                            imgtxtclass:"",
                            imgtxt:data.string.panda
                        },
                        {
                            imgdiv: "cheetaansdiv",
                            imgclass: "relativecls cheetaimg",
                            imgid: 'cheetaImg',
                            imgsrc: "",
                            imgtxtclass:"",
                            imgtxt:data.string.cheetah
                        }

                    ]
                }]
            }
        ],
        textblock:[{
            textdiv:"congrats",
            textdata:data.string.congratulationvert
        }]
    },
    // slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        imgwithdiv: [{
            divcls: "quesdiv",
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "giraffediv",
                        imgclass: "relativecls giraffeimg",
                        imgid: 'giraffeImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "elephantdiv",
                        imgclass: "relativecls elephantimg",
                        imgid: 'elephantImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "animal snaildiv",
                        imgclass: "relativecls snailimg",
                        imgid: 'snailImg',
                        imgsrc: "",
                    },

                    {
                        imgdiv: "pandadiv",
                        imgclass: "relativecls pandaimg",
                        imgid: 'pandaImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "cheetadiv",
                        imgclass: "relativecls cheetaimg",
                        imgid: 'cheetaImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv:"peacockdiv",
                        imgclass: "relativecls peacockimg",
                        imgid : 'peacockImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "animal earthwormdiv",
                        imgclass: "relativecls earthwormimg",
                        imgid: 'earthwormImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "animal butterflydiv",
                        imgclass: "relativecls butterflyimg",
                        imgid: 'butterflyImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "animal grasshopperdiv",
                        imgclass: "relativecls grasshopperimg",
                        imgid: 'grasshopperImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "animal crabdiv",
                        imgclass: "relativecls crabimg",
                        imgid: 'crabImg',
                        imgsrc: "",
                    }
                ]
            }]
        },
            {
                extradiv:[{
                    divcls:"outerdiv",
                    divtxt:data.string.p4text2,
                    imageblock: [{
                        imagestoshow: [
                            {
                                imgdiv: "cameradiv",
                                imgclass: "relativecls cameraimg",
                                imgid: 'cameraImg',
                                imgsrc: "",
                            },
                        ]
                    }],
                }],
                divcls: "ansdiv",
                imageblock: [{
                    imagestoshow: [
                        {
                            imgdiv: "crabansdiv",
                            imgclass: "relativecls crabimg",
                            imgid: 'crabImg',
                            imgsrc: "",
                            imgtxtclass:"",
                            imgtxt:data.string.crab
                        },
                        {
                            imgdiv:"snailansdiv",
                            imgclass: "relativecls snailimg",
                            imgid : 'snailImg',
                            imgsrc: "",
                            imgtxtclass:"",
                            imgtxt:data.string.snail
                        },
                        {
                            imgdiv: "earthwormansdiv",
                            imgclass: "relativecls earthwormimg",
                            imgid: 'earthwormImg',
                            imgsrc: "",
                            imgtxtclass:"",
                            imgtxt:data.string.earthworm
                        },

                        {
                            imgdiv: "grasshopperansdiv",
                            imgclass: "relativecls grasshopperimg",
                            imgid: 'grasshopperImg',
                            imgsrc: "",
                            imgtxtclass:"",
                            imgtxt:data.string.grasshopper
                        },
                        {
                            imgdiv: "butterflyansdiv",
                            imgclass: "relativecls butterflyimg",
                            imgid: 'butterflyImg',
                            imgsrc: "",
                            imgtxtclass:"",
                            imgtxt:data.string.butterfly
                        }

                    ]
                }]
            }
        ],
        textblock:[{
            textdiv:"congrats",
            textdata:data.string.congratulationinvert
        }]
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "common-css", src: $ref + "css/common.css", type: createjs.AbstractLoader.CSS},
            {id: "page1-css", src: $ref + "css/page1.css", type: createjs.AbstractLoader.CSS},

            {id: "giraffeImg", src: imgpath+"Page01/girafee.png", type: createjs.AbstractLoader.IMAGE},
            {id: "elephantImg", src: imgpath+"Page02/elephant.png", type: createjs.AbstractLoader.IMAGE},
            {id: "deerImg", src: imgpath+"Page01/deer.png", type: createjs.AbstractLoader.IMAGE},
            {id: "peacockImg", src: imgpath+"Page01/pickcock.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pandaImg", src: imgpath+"Page01/panda.png", type: createjs.AbstractLoader.IMAGE},
            {id: "cheetaImg", src: imgpath+"Page04/cheeta.png", type: createjs.AbstractLoader.IMAGE},
            {id: "penguinImg", src: imgpath+"Page01/pinguin.png", type: createjs.AbstractLoader.IMAGE},
            {id: "earthwormImg", src: imgpath+"Page01/earthworm.png", type: createjs.AbstractLoader.IMAGE},
            {id: "butterflyImg", src: imgpath+"Page01/butterfly.png", type: createjs.AbstractLoader.IMAGE},
            {id: "grasshopperImg", src: imgpath+"Page01/grasshooper.png", type: createjs.AbstractLoader.IMAGE},
            {id: "crabImg", src: imgpath+"Page01/crab.png", type: createjs.AbstractLoader.IMAGE},
            {id: "beeImg", src: imgpath+"Page01/bee.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ladybugImg", src: imgpath+"Page01/ladybug.png", type: createjs.AbstractLoader.IMAGE},
            {id: "snailImg", src: imgpath+"Page01/snail.png", type: createjs.AbstractLoader.IMAGE},
            {id: "subrinaImg", src: imgpath+"Page01/subrina.png", type: createjs.AbstractLoader.IMAGE},
            {id: "caterpilarImg", src: imgpath+"Page03/caterpillar.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "butterflygifImg", src: imgpath+"Page01/butterfly-animated-gif-45.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "monkeyImg", src: imgpath+"Page01/monkey.png", type: createjs.AbstractLoader.IMAGE},
            {id: "cameraImg", src: imgpath+"camera.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset+"p4_s0.ogg"},
            {id: "sound_1", src: soundAsset+"p4_s1.ogg"},
            {id: "sound_1_1", src: soundAsset+"p4_s1_1.ogg"},
            {id: "sound_2", src: soundAsset+"p4_s2.ogg"},
            {id: "sound_2_1", src: soundAsset+"p4_s2_1.ogg"},
            {id: "camera", src: soundAsset1+"camera.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

    /*===============================================
    =            data highlight function            =
    ===============================================*/
    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        bounceOnce
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function (index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

    /*=====  End of data highlight function  ======*/


    /*======================================================
     =            Navigation Controller Function            =
     ======================================================*/

    function navigationcontroller(islastpageflag) {
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean' ?
                alert("NavigationController : Hi Master, please provide a boolean parameter") :
                null;

        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
            $prevBtn.css('display', 'none');
        }
        else if ($total_page == 1) {
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');

            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if (countNext > 0 && countNext < $total_page - 1) {
            $nextBtn.show(0);
            $prevBtn.show(0);
        }
        else if (countNext == $total_page - 1) {
            $nextBtn.css('display', 'none');
            $prevBtn.show(0);

            // if lastpageflag is true
            ole.footerNotificationHandler.pageEndSetNotification();
        }

    }

    /*=====  End of user navigation controller function  ======*/


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);


        // highlight any text inside board div with datahighlightflag set true
        // texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext);
        switch (countNext) {
            case 0:
                sound_player("sound_0",true);
                break;
            case 1:
                sound_player("sound_1",false);
                put_image2(content, countNext);
                checkans();
                break;
            case 2:
                sound_player("sound_2",false);
                put_image2(content, countNext);
                checkans2();
                break;
            // case 1:
            //     sound_player("sound_2");
            //     var clickcounter = 0;
            //     timeoutvar = setTimeout(function(){
            //         sound_player("sound_1");
            //         $('#present-flipper').on('click', function() {
            //             if(clickcounter%2==0){
            //                 sound_player("sound_2");
            //             } else {
            //                 sound_player("sound_1");
            //             }
            //             clickcounter++;
            //             $('#present-flipper').toggleClass('doFlip');
            //         });
            //         nav_button_controls(2000);
            //     }, 1000);
            //     break;
            // case 2:
            //     sound_player("sound_2");
            //     var clickcounter = 0;
            //     timeoutvar = setTimeout(function(){
            //         sound_player("sound_1");
            //         $('#present-flipper').on('click', function() {
            //             if(clickcounter%2==0){
            //                 sound_player("sound_2");
            //             } else {
            //                 sound_player("sound_1");
            //             }
            //             clickcounter++;
            //             $('#present-flipper').toggleClass('doFlip');
            //         });
            //         nav_button_controls(2000);
            //     }, 1000);
            //     break;
            // case 3:
            //     sound_player("sound_4");
            //     break;
            // case 4:
            //     sound_player("sound_5");
            //     break;
            // case 5:
            //     sound_player("sound_5");
            //     break;
            // case 6:
            //     sound_player("sound_5");
            //     break;
            // case 7:
            //     sound_player("sound_5");
            //     break;
            // case 8:
            //     sound_player("sound_5");
            //     break;
            default:
                navigationcontroller();
                break;
        }
    }

    function nav_button_controls(delay_ms) {
        timeoutvar = setTimeout(function () {
            if (countNext == 0) {
                $nextBtn.show(0);
            } else if (countNext > 0 && countNext == $total_page - 1) {
                $prevBtn.show(0);
                ole.footerNotificationHandler.pageEndSetNotification();
            } else {
                $prevBtn.show(0);
                $nextBtn.show(0);
            }
        }, delay_ms);
    }

    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller():'';
        });
    }


    function put_image(content, count) {
        var contentCount = content[count];
        var imageblockcontent = contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent, contentCount)
    }

    function put_image2(content, count) {
        if (content[count].hasOwnProperty("imgwithdiv")) {
            console.log("Hello I am here");
            var contentCount = content[count].imgwithdiv[0];
            var imageblockcontent = contentCount.hasOwnProperty('imageblock');
            dynamicimageload(imageblockcontent, contentCount);

            var contentCount1 = content[count].imgwithdiv[1];
            var imageblockcontent1 = contentCount1.hasOwnProperty('imageblock');
            console.log("image " + imageblockcontent1);
            dynamicimageload(imageblockcontent1, contentCount1);
            if(contentCount1.hasOwnProperty("extradiv")){
                console.log("This is test");
                var contentCount2 = contentCount1.extradiv[0];
                var imageblockcontent2 = contentCount2.hasOwnProperty('imageblock');
                dynamicimageload(imageblockcontent2, contentCount2);

            }
        }
    }

    function dynamicimageload(imageblockcontent, contentCount) {
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

function  checkans(){
    $(".giraffeansdiv,.peacockansdiv,.pandaansdiv,.elephantansdiv,.cheetaansdiv,.congrats").hide(0);

    $(".giraffediv").click(function(){
        $(".giraffeansdiv").show(0).addClass("done");
        showNav("vert");
    });
    $(".peacockdiv").click(function(){
        $(".peacockansdiv").show(0).addClass("done");
        showNav("vert");
    });
    $(".pandadiv").click(function(){
        $(".pandaansdiv").show(0).addClass("done");
        showNav("vert");
    });
    $(".elephantdiv").click(function(){
        $(".elephantansdiv").show(0).addClass("done");
        showNav("vert");
    });
    $(".cheetadiv").click(function(){
        $(".cheetaansdiv").show(0).addClass("done");
        showNav("vert");
    });


}
function checkans2(){
    $(".crabansdiv,.snailansdiv,.earthwormansdiv,.grasshopperansdiv,.butterflyansdiv,.congrats").hide(0);

    $(".crabdiv").click(function(){
        $(".crabansdiv").show(0).addClass("done");
        showNav("invert");
    });
    $(".snaildiv").click(function(){
        $(".snailansdiv").show(0).addClass("done");
        showNav("invert");
    });
    $(".earthwormdiv").click(function(){
        $(".earthwormansdiv").show(0).addClass("done");
        showNav("invert");
    });
    $(".grasshopperdiv").click(function(){
        $(".grasshopperansdiv").show(0).addClass("done");
        showNav("invert");
    });
    $(".butterflydiv").click(function(){
        $(".butterflyansdiv").show(0).addClass("done");
        showNav("invert");
    });
}
 function showNav(animalType){
     $('.animal').css("opacity","0");
     setTimeout( function(){
         $('.animal').css("opacity","1");
     }  , 100 );
     sound_player("camera",false);
     if($(".done").size()==5) {
         $(".congrats").show(0);
         $(".quesdiv,.animal").addClass("settodefault");
         setTimeout(function(){
             animalType=="vert"?sound_player("sound_1_1",true):
                 sound_player("sound_2_1",true);
         },1000);

     }
 }


});
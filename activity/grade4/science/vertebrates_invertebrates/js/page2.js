var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: " commontext text1",
                textdata: data.string.vertebratesanim
            },
            {
                textclass: "commontext text2",
                textdata: data.string.p2text1
            }
        ],
        imageblock:[{
            imagestoshow:[
                {
                    imgdiv:"muscle",
                    imgclass: "relativecls muscleimg",
                    imgid : 'muscleImg',
                    imgsrc: "",
                },
                {
                    imgdiv:"bone",
                    imgclass: "relativecls boneimg",
                    imgid : 'boneImg',
                    imgsrc: "",
                },
                {
                    imgdiv:"backbone",
                    imgclass: "relativecls backboneimg",
                    imgid : 'backboneImg',
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: " commontext text1",
                textdata: data.string.vertebratesanim
            },
            {
                textclass: "commontext text2",
                textdata: data.string.p2text2
            }
        ],
        imageblock:[{
            imagestoshow:[
                {
                    imgdiv:"birdvdiv",
                    imgclass: "relativecls birdvimg",
                    imgid : 'birdvImg',
                    imgsrc: "",
                },
                {
                    imgdiv:"fishvdiv",
                    imgclass: "relativecls fishvimg",
                    imgid : 'fishvImg',
                    imgsrc: "",
                },
                {
                    imgdiv:"lionvdiv",
                    imgclass: "relativecls lionvimg",
                    imgid : 'lionvImg',
                    imgsrc: "",
                },
                {
                    imgdiv:"snakevdiv",
                    imgclass: "relativecls snakevimg",
                    imgid : 'snakevImg',
                    imgsrc: "",
                },
                {
                    imgdiv:"tortlevdiv",
                    imgclass: "relativecls tortlevimg",
                    imgid : 'tortlevImg',
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: " commontext text1",
                textdata: data.string.vertebratesanim
            },
            {
                textclass: "commontext text2",
                textdata: data.string.p2text3
            }
        ],
        imageblock:[{
            imagestoshow:[
                {
                    imgdiv:"girrafeebone",
                    imgclass: "relativecls girrafeeboneimg",
                    imgid : 'girrafeeboneImg',
                    imgsrc: "",
                },
                {
                    imgdiv:"frogbone",
                    imgclass: "relativecls frogboneimg",
                    imgid : 'frogboneImg',
                    imgsrc: "",
                },
                {
                    imgdiv:"chickenbone",
                    imgclass: "relativecls chickenboneimg",
                    imgid : 'chickenboneImg',
                    imgsrc: "",
                },
                {
                    imgdiv:"crocbone",
                    imgclass: "relativecls crocboneimg",
                    imgid : 'crocboneImg',
                    imgsrc: "",
                },
                {
                    imgdiv:"dinobone",
                    imgclass: "relativecls dinoboneimg",
                    imgid : 'dinoboneImg',
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: " commontext text1",
                textdata: data.string.vertebratesanim
            }
        ],
        middletext:[
            {
                textdiv:"middlebox",
                textclass: "clicktext",
                textdata: data.string.clickpic,
                textchng:data.string.vertanimals
            }
        ],
        imageblock:[{
            imagestoshow:[
                {
                    imgdiv:"animaldiv cranerealdiv sound_crane",
                    imgclass: "relativecls cranerealimg ",
                    imgid : 'crane',
                    imgsrc: "",
                    imgtxt:data.string.crane,
                },
                {
                    imgdiv:"animaldiv dolphinrealdiv sound_dolphin",
                    imgclass: "relativecls dolphinrealimg ",
                    imgid : 'dolphin',
                    imgsrc: "",
                    imgtxt:data.string.dolphin,
                },
                {
                    imgdiv:"animaldiv elephantrealdiv sound_elephant",
                    imgclass: "relativecls elephantrrealimg",
                    imgid : 'elephant',
                    imgsrc: "",
                    imgtxt:data.string.elephant,
                },
                {
                    imgdiv:"animaldiv tigerrealdiv sound_tiger",
                    imgclass: "relativecls tigerrealimg",
                    imgid : 'tiger',
                    imgsrc: "",
                    imgtxt:data.string.tiger,
                },
                {
                    imgdiv:"animaldiv pandarealdiv sound_panda",
                    imgclass: "relativecls pandarealimg",
                    imgid : 'panda',
                    imgsrc: "",
                    imgtxt:data.string.panda,
                },
                {
                    imgdiv:"animaldiv parotrealdiv sound_parrot",
                    imgclass: "relativecls parotrealimg",
                    imgid : 'parot',
                    imgsrc: "",
                    imgtxt:data.string.parrot,
                },
                {
                    imgdiv:"animaldiv peacockrealdiv sound_peacock",
                    imgclass: "relativecls peacockrealimg",
                    imgid : 'peacock',
                    imgsrc: "",
                    imgtxt:data.string.peacock,
                },
                {
                    imgdiv:"animaldiv penguinrealdiv sound_penguin",
                    imgclass: "relativecls penguinrealimg",
                    imgid : 'penguin',
                    imgsrc: "",
                    imgtxt:data.string.penguin,
                },
                {
                    imgdiv:"animaldiv rhinorealdiv sound_rhino",
                    imgclass: "relativecls rhinorealimg",
                    imgid : 'rhino',
                    imgsrc: "",
                    imgtxt:data.string.rhino,
                },
                {
                    imgdiv:"animaldiv deerrealdiv sound_deer",
                    imgclass: "relativecls deerrealimg",
                    imgid : 'deer',
                    imgsrc: "",
                    imgtxt:data.string.deer,
                }
            ]
        }]
    }
];
$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    var interval;
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "common-css", src: $ref + "css/common.css", type: createjs.AbstractLoader.CSS},
            {id: "page1-css", src: $ref + "css/page1.css", type: createjs.AbstractLoader.CSS},

            {id: "muscleImg", src: imgpath+"Page01/human01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "boneImg", src: imgpath+"Page01/human02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "backboneImg", src: imgpath+"Page01/human03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pigImg", src: imgpath+"Page01/human03.png", type: createjs.AbstractLoader.IMAGE},//no pig img
            {id: "elephantImg", src: imgpath+"Page02/elephant.png", type: createjs.AbstractLoader.IMAGE},
            {id: "henImg", src: imgpath+"Page01/hen.png", type: createjs.AbstractLoader.IMAGE},
            {id: "deerImg", src: imgpath+"Page01/deer.png", type: createjs.AbstractLoader.IMAGE},
            {id: "monkeyImg", src: imgpath+"Page01/monkey.png", type: createjs.AbstractLoader.IMAGE},

            {id: "girrafeeboneImg", src: imgpath+"Page02/skeleton/giraffe.png", type: createjs.AbstractLoader.IMAGE},//no pig img
            {id: "frogboneImg", src: imgpath+"Page02/skeleton/frog.png", type: createjs.AbstractLoader.IMAGE},
            {id: "chickenboneImg", src: imgpath+"Page02/skeleton/bird.png", type: createjs.AbstractLoader.IMAGE},
            {id: "crocboneImg", src: imgpath+"Page02/skeleton/cor.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dinoboneImg", src: imgpath+"Page02/skeleton/dog.png", type: createjs.AbstractLoader.IMAGE},

            {id: "crane", src: imgpath+"real_images/crane.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dolphin", src: imgpath+"real_images/dolphin.png", type: createjs.AbstractLoader.IMAGE},
            {id: "elephant", src: imgpath+"real_images/elephant.png", type: createjs.AbstractLoader.IMAGE},
            {id: "tiger", src: imgpath+"real_images/tiger.png", type: createjs.AbstractLoader.IMAGE},
            {id: "panda", src: imgpath+"real_images/panda.png", type: createjs.AbstractLoader.IMAGE},
            {id: "parot", src: imgpath+"real_images/parot.png", type: createjs.AbstractLoader.IMAGE},
            {id: "peacock", src: imgpath+"real_images/peacock.png", type: createjs.AbstractLoader.IMAGE},
            {id: "penguin", src: imgpath+"real_images/Penguin-Dreams.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rhino", src: imgpath+"real_images/Rhino_gaida.png", type: createjs.AbstractLoader.IMAGE},
            {id: "deer", src: imgpath+"real_images/spotted-deer.png", type: createjs.AbstractLoader.IMAGE},

            {id: "birdvImg", src: imgpath+"images_14/bird.png", type: createjs.AbstractLoader.IMAGE},
            {id: "fishvImg", src: imgpath+"images_14/fish.png", type: createjs.AbstractLoader.IMAGE},
            {id: "lionvImg", src: imgpath+"images_14/lion.png", type: createjs.AbstractLoader.IMAGE},
            {id: "snakevImg", src: imgpath+"images_14/snake.png", type: createjs.AbstractLoader.IMAGE},
            {id: "tortlevImg", src: imgpath+"images_14/tortle.png", type: createjs.AbstractLoader.IMAGE},


            // sounds
            {id: "sound_0", src: soundAsset+"p2_s0.ogg"},
            {id: "sound_1", src: soundAsset+"p2_s1.ogg"},
            {id: "sound_2", src: soundAsset+"p2_s2.ogg"},
            {id: "sound_3", src: soundAsset+"p2_s3.ogg"},


            {id: "sound_crane", src: soundAsset+"Ver/crane.ogg"},
            {id: "sound_deer", src: soundAsset+"Ver/deer.ogg"},
            {id: "sound_dolphin", src: soundAsset+"Ver/dolphin.ogg"},
            {id: "sound_elephant", src: soundAsset+"Ver/elephant.ogg"},
            {id: "sound_panda", src: soundAsset+"Ver/panda.ogg"},
            {id: "sound_parrot", src: soundAsset+"Ver/parrot.ogg"},
            {id: "sound_peacock", src: soundAsset+"Ver/peacock.ogg"},
            {id: "sound_penguin", src: soundAsset+"Ver/penguin.ogg"},
            {id: "sound_rhino", src: soundAsset+"Ver/rhino.ogg"},
            {id: "sound_tiger", src: soundAsset+"Ver/tiger.ogg"},
            {id: "sound_3_1", src: soundAsset+"p2_s3_1.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

    /*===============================================
    =            data highlight function            =
    ===============================================*/
    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function (index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

    /*=====  End of data highlight function  ======*/


    /*======================================================
     =            Navigation Controller Function            =
     ======================================================*/

    function navigationcontroller(islastpageflag) {
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean' ?
                alert("NavigationController : Hi Master, please provide a boolean parameter") :
                null;

          if (countNext == 0 && $total_page != 1) {
              $nextBtn.show(0);
              $prevBtn.css('display', 'none');
          }
          else if ($total_page == 1) {
              $prevBtn.css('display', 'none');
              $nextBtn.css('display', 'none');

              ole.footerNotificationHandler.lessonEndSetNotification();
          }
          else if (countNext > 0 && countNext < $total_page - 1) {
              $nextBtn.show(0);
              $prevBtn.show(0);
          }
          else if (countNext == $total_page - 1) {
              $nextBtn.css('display', 'none');
              $prevBtn.show(0);

              // if lastpageflag is true
              ole.footerNotificationHandler.pageEndSetNotification();
          }
    }

    /*=====  End of user navigation controller function  ======*/


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);


        // highlight any text inside board div with datahighlightflag set true
        // texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext);
        switch(countNext){
            case 0:
                animateDiv();
                interval = setInterval(function () {
                    animateDiv();
                },10000)
                sound_player("sound_0",true);
                break;
            case 1:
                clearInterval(interval);
                animateBackbone();
                sound_player("sound_1",true);
                break;
            case 2:
                animateSkeleton();
                sound_player("sound_2",false);
                break;
            case 3:

                sound_player("sound_3",false);
                showAnimalNames();
                break;
                default:
                    navigationcontroller();
                    break;
        }
    }
    function nav_button_controls(delay_ms){
      timeoutvar = setTimeout(function(){
        if(countNext==0){
          $nextBtn.show(0);
        } else if( countNext>0 && countNext == $total_page-1){
          $prevBtn.show(0);
          ole.footerNotificationHandler.pageEndSetNotification();
        } else{
          $prevBtn.show(0);
          $nextBtn.show(0);
        }
      },delay_ms);
    }

    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
          navigate?navigationcontroller():'';
        });
    }


    function put_image(content, count) {
        var contentCount=content[count];
        var imageblockcontent=contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent,contentCount)
    }


    function put_image2(content, count) {
        console.log("inside image2" + content[count].hasOwnProperty("imgwithdiv"));
        if (content[count].hasOwnProperty("imgwithdiv")) {
            console.log("Hello I am here");
            var contentCount=content[count].imgwithdiv[0];
            var imageblockcontent=contentCount.hasOwnProperty('imageblock');
            dynamicimageload(imageblockcontent,contentCount);

            var contentCount1=content[count].imgwithdiv[1];
            var imageblockcontent1=contentCount1.hasOwnProperty('imageblock');
            console.log("image "+imageblockcontent1);
            dynamicimageload(imageblockcontent1,contentCount1);
        }
    }
    function dynamicimageload(imageblockcontent,contentCount){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }



    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function animateSkeleton(){
        $(".girrafeebone,.frogbone,.chickenbone,.crocbone,.dinobone").hide(0);
        setTimeout(function(){
            $(".girrafeebone").show(0).addClass("zoomInEffect");
            setTimeout(function(){
                $(".frogbone").show(0).addClass("zoomInEffect");
                setTimeout(function(){
                    $(".chickenbone").show(0).addClass("zoomInEffect");
                    setTimeout(function(){
                        $(".crocbone").show(0).addClass("zoomInEffect");
                        setTimeout(function(){
                            $(".dinobone").show(0).addClass("zoomInEffect");
                            $(".girrafeebone,.frogbone,.chickenbone,.crocbone,.dinobone").removeClass("zoomOutEffect");
                            navigationcontroller();
                            },2000);
                    },2000);
                },2000);
            },2000);
        },2000);

    }
    function showAnimalNames(){
        $(".secondText").hide(0);
        $(".animaldiv").each(function () {
            $(this).find('p').hide(0);
        });
        $(".animaldiv").click(function(){
           $(this).find("p").addClass("zoomInEffect").show(0);
            var classStr = $(this).attr('class');
            var lastClass = classStr.substr( classStr.lastIndexOf(' ') + 1);
            sound_player(lastClass,false);
            if(!($(".animaldiv p").is(":hidden"))){
                $(".firstText").hide(0);
                $(".middlebox").addClass("flipOnce");
                $(".secondText").addClass().show(0);
                setTimeout(function () {
                    sound_player("sound_3_1",true);
                },1000);
            }
        });
    }

    function animateBackbone(){
        setTimeout(function(){
            $(".birdvImg").addClass("scaleEffect").attr("src",imgpath+"images_14/bird01.png");
            setTimeout(function(){
                $(".fishvImg").addClass("scaleEffect").attr("src",imgpath+"images_14/fish01.png");
                setTimeout(function(){
                    $(".lionvImg").addClass("scaleEffect").attr("src",imgpath+"images_14/lion01.png");
                    setTimeout(function(){
                        $(".snakevImg").addClass("scaleEffect").attr("src",imgpath+"images_14/snake01.png");
                        setTimeout(function(){
                            $(".tortlevImg").addClass("scaleEffect").attr("src",imgpath+"images_14/tortle01.png");
                        },1000)
                    },1000)
                },1000)
            },1000)
        },2000)
    }
    function animateDiv(){
        $(".muscle,.bone").removeClass("grayout");
        $(".bone,.backbone").hide(0);
        setTimeout(function(){
            $(".muscle").addClass("grayout");
            $(".bone").show(0).addClass("slideL");
            setTimeout(function () {
                $(".bone").addClass("grayout");
                $(".backbone").show(0).addClass("slideL");
            },3000);
        },3000);
    }
});

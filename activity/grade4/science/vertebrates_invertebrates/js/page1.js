var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock:[{
            imagestoshow:[
                {
                    imgdiv:"giraffediv",
                    imgclass: "relativecls giraffeimg",
                    imgid : 'giraffeImg',
                    imgsrc: "",
                },
                {
                    imgdiv:"dolphindiv",
                    imgclass: "relativecls dolphinimg",
                    imgid : 'dolphinImg',
                    imgsrc: "",
                },
                {
                    imgdiv:"ladybugdiv",
                    imgclass: "relativecls ladybugimg",
                    imgid : 'ladybugImg',
                    imgsrc: "",
                },
                {
                    imgdiv:"snaildiv",
                    imgclass: "relativecls snailimg",
                    imgid : 'snailImg',
                    imgsrc: "",
                },

                {
                    imgdiv:"pandadiv",
                    imgclass: "relativecls pandaimg",
                    imgid : 'pandaImg',
                    imgsrc: "",
                },
                {
                    imgdiv:"tigerdiv",
                    imgclass: "relativecls tigerimg",
                    imgid : 'tigerImg',
                    imgsrc: "",
                },
                {
                    imgdiv:"penguindiv",
                    imgclass: "relativecls penguinimg",
                    imgid : 'penguinImg',
                    imgsrc: "",
                },
                {
                    imgdiv:"earthwormdiv",
                    imgclass: "relativecls earthwormimg",
                    imgid : 'earthwormImg',
                    imgsrc: "",
                },
                {
                    imgdiv:"butterflydiv",
                    imgclass: "relativecls butterflyimg",
                    imgid : 'butterflyImg',
                    imgsrc: "",
                },
                {
                    imgdiv:"grasshopperdiv",
                    imgclass: "relativecls grasshopperimg",
                    imgid : 'grasshopperImg',
                    imgsrc: "",
                },
                {
                    imgdiv:"crabdiv",
                    imgclass: "relativecls crabimg",
                    imgid : 'crabImg',
                    imgsrc: "",
                },
                {
                    imgdiv:"beediv",
                    imgclass: "relativecls beeimg",
                    imgid : 'beeImg',
                    imgsrc: "",
                },
                {
                    imgdiv:"deerdiv",
                    imgclass: "relativecls deerimg",
                    imgid : 'deerImg',
                    imgsrc: "",
                },
                {
                    imgdiv:"peacockdiv",
                    imgclass: "relativecls peacockimg",
                    imgid : 'peacockImg',
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imgwithdiv: [{
            divcls: "upperdiv animatedivmovetop",
            textblock: [{
                textdiv: "slideL vert",
                textclass: "vertebrates",
                textdata: data.string.vertebrate
            }],
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "giraffediv1",
                        imgclass: "relativecls giraffeimg",
                        imgid: 'giraffeImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "dolphindiv",
                        imgclass: "relativecls dolphinimg",
                        imgid: 'dolphinImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "deerdiv",
                        imgclass: "relativecls deerimg",
                        imgid: 'deerImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "peacockdiv1",
                        imgclass: "relativecls peacockimg",
                        imgid: 'peacockImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "pandadiv1",
                        imgclass: "relativecls pandaimg",
                        imgid: 'pandaImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "tigerdiv1",
                        imgclass: "relativecls tigerimg",
                        imgid: 'tigerImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "penguindiv1",
                        imgclass: "relativecls penguinimg",
                        imgid: 'penguinImg',
                        imgsrc: "",
                    }
                ]
              }]
          },
          {
                divcls: "lowerdiv animatedivmovedown",
                textblock: [{
                    textdiv: "slideR invert",
                    textclass: "invertebrates",
                    textdata: data.string.invertebratesanim
                    },
                    {
                        textdiv: "zoomInEffect and",
                        textclass: "and",
                        textdata: data.string.and
                    }
                ],
                imageblock: [{
                    imagestoshow: [
                        {
                            imgdiv: "earthwormdiv",
                            imgclass: "relativecls earthwormimg",
                            imgid: 'earthwormImg',
                            imgsrc: "",
                        },
                        {
                            imgdiv: "butterflydiv",
                            imgclass: "relativecls butterflyimg",
                            imgid: 'butterflyImg',
                            imgsrc: "",
                        },
                        {
                            imgdiv: "grasshopperdiv",
                            imgclass: "relativecls grasshopperimg",
                            imgid: 'grasshopperImg',
                            imgsrc: "",
                        },
                        {
                            imgdiv: "crabdiv",
                            imgclass: "relativecls crabimg",
                            imgid: 'crabImg',
                            imgsrc: "",
                        },
                        {
                            imgdiv: "ladybugdiv1",
                            imgclass: "relativecls ladybugimg",
                            imgid: 'ladybugImg',
                            imgsrc: "",
                        },
                        {
                            imgdiv: "snaildiv1",
                            imgclass: "relativecls snailimg",
                            imgid: 'snailImg',
                            imgsrc: "",
                        }
                    ]
                }]
            }

        ],

    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        uppertextblock: [
            {
                textclass: " commontext fadeIn text2",
                textdata: data.string.p1text1
            }
        ],
        imgwithdiv: [{
            divcls: "leftdiv",
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "subrinadiv",
                        imgclass: "relativecls subrinaimg",
                        imgid: 'subrinaImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "caterpilardiv",
                        imgclass: "relativecls caterpilarimg",
                        imgid: 'caterpilarImg',
                        imgsrc: "",
                    }
                ]
            }]
        },
        {
                divcls: "rightdiv",
                imageblock: [{
                    imagestoshow: [
                        {
                            imgdiv: "butterflygifdiv",
                            imgclass: "relativecls butterflygifimg",
                            imgid: 'butterflygifImg',
                            imgsrc: "",
                        },
                        {
                            imgdiv: "monkeydiv",
                            imgclass: "relativecls monkeyimg",
                            imgid: 'monkeyImg',
                            imgsrc: "",
                        }
                    ]
                }]
        }
        ]
    },
//slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        uppertextblock: [
            {
                textclass: "commontext slideR text2",
                textdata: data.string.p1text2
            }
        ],
        imgwithdiv: [{
            divcls: "leftdiv",
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "subrinadiv",
                        imgclass: "relativecls subrinaimg",
                        imgid: 'subrinaImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "caterpilardiv",
                        imgclass: "relativecls caterpilarimg",
                        imgid: 'caterpilarImg',
                        imgsrc: "",
                    }
                ]
            }]
        },
            {
                divcls: "rightdiv",
                imageblock: [{
                    imagestoshow: [
                        {
                            imgdiv: "butterflygifdiv",
                            imgclass: "relativecls butterflygifimg",
                            imgid: 'butterflygifImg',
                            imgsrc: "",
                        },
                        {
                            imgdiv: "monkeydiv",
                            imgclass: "relativecls monkeyimg",
                            imgid: 'monkeyImg',
                            imgsrc: "",
                        }
                    ]
                }]
            }
        ]
    },
    //slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        uppertextblock: [
            {
                textclass: " commontext title1",
                textdata: data.string.vertebratesanim
            },
            {
                textclass: "commontext title2",
                textdata: data.string.invertebratesanim
            }
        ],
        imgwithdiv: [{
            divcls: "leftdiv",
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "subrinadiv",
                        imgclass: "relativecls subrinaimg",
                        imgid: 'subrinaImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "caterpilardiv",
                        imgclass: "relativecls  caterpilarimg",
                        imgid: 'caterpilarImg',
                        imgsrc: "",
                    }
                ]
            }]
        },
            {
                divcls: "rightdiv",
                imageblock: [{
                    imagestoshow: [
                        {
                            imgdiv: "butterflygifdiv",
                            imgclass: "relativecls butterflygifimg",
                            imgid: 'butterflygifImg',
                            imgsrc: "",
                        },
                        {
                            imgdiv: "monkeydiv",
                            imgclass: "relativecls monkeyimg",
                            imgid: 'monkeyImg',
                            imgsrc: "",
                        }
                    ]
                }]
            }
        ]
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "common-css", src: $ref + "css/common.css", type: createjs.AbstractLoader.CSS},
            {id: "page1-css", src: $ref + "css/page1.css", type: createjs.AbstractLoader.CSS},

            {id: "giraffeImg", src: imgpath+"Page01/girafee.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dolphinImg", src: imgpath+"Page01/dolphin.png", type: createjs.AbstractLoader.IMAGE},
            {id: "deerImg", src: imgpath+"Page01/deer.png", type: createjs.AbstractLoader.IMAGE},
            {id: "peacockImg", src: imgpath+"Page01/pickcock.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pandaImg", src: imgpath+"Page01/panda.png", type: createjs.AbstractLoader.IMAGE},
            {id: "tigerImg", src: imgpath+"Page01/tiger.png", type: createjs.AbstractLoader.IMAGE},
            {id: "penguinImg", src: imgpath+"Page01/pinguin.png", type: createjs.AbstractLoader.IMAGE},
            {id: "earthwormImg", src: imgpath+"Page01/earthworm.png", type: createjs.AbstractLoader.IMAGE},
            {id: "butterflyImg", src: imgpath+"Page01/butterfly.png", type: createjs.AbstractLoader.IMAGE},
            {id: "grasshopperImg", src: imgpath+"Page01/grasshooper.png", type: createjs.AbstractLoader.IMAGE},
            {id: "crabImg", src: imgpath+"Page01/crab.png", type: createjs.AbstractLoader.IMAGE},
            {id: "beeImg", src: imgpath+"Page01/bee.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ladybugImg", src: imgpath+"Page01/ladybug.png", type: createjs.AbstractLoader.IMAGE},
            {id: "snailImg", src: imgpath+"Page01/snail.png", type: createjs.AbstractLoader.IMAGE},
            {id: "subrinaImg", src: imgpath+"Page01/subrina.png", type: createjs.AbstractLoader.IMAGE},
            {id: "caterpilarImg", src: imgpath+"Page03/caterpillar.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "butterflygifImg", src: imgpath+"Page01/butterfly-animated-gif-45.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "monkeyImg", src: imgpath+"Page01/monkey.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_1", src: soundAsset+"p1_s0.ogg"},
            {id: "sound_2", src: soundAsset+"p1_s1.ogg"},
            {id: "sound_3", src: soundAsset+"p1_s2.ogg"},
            {id: "sound_4", src: soundAsset+"p1_s3.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

    /*===============================================
    =            data highlight function            =
    ===============================================*/
    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function (index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

    /*=====  End of data highlight function  ======*/


    /*======================================================
     =            Navigation Controller Function            =
     ======================================================*/

    function navigationcontroller(islastpageflag) {
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean' ?
                alert("NavigationController : Hi Master, please provide a boolean parameter") :
                null;

        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
            $prevBtn.css('display', 'none');
        }
        else if ($total_page == 1) {
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');

            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if (countNext > 0 && countNext < $total_page - 1) {
            $nextBtn.show(0);
            $prevBtn.show(0);
        }
        else if (countNext == $total_page - 1) {
            $nextBtn.css('display', 'none');
            $prevBtn.show(0);

            // if lastpageflag is true
            ole.footerNotificationHandler.pageEndSetNotification();
        }

    }

    /*=====  End of user navigation controller function  ======*/


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);


        // highlight any text inside board div with datahighlightflag set true
        // texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext);
        switch(countNext){
            case 1:
                put_image2(content,countNext);
                animateDiv();
                sound_player("sound_1");
                break;
            case 2:
                put_image2(content,countNext);
                $(".subrinadiv,.caterpilardiv,.butterflygifdiv,.monkeydiv").hide(0);
                animateFadeIn();
                sound_player("sound_2");
                break;
            case 3:
                put_image2(content,countNext);
                sound_player("sound_3");
                break;
            case 4:
                put_image2(content,countNext);
                animateTranslation();
                break;
            default:
                navigationcontroller();
                break;
        }
    }

    function nav_button_controls(delay_ms) {
        timeoutvar = setTimeout(function () {
            if (countNext == 0) {
                $nextBtn.show(0);
            } else if (countNext > 0 && countNext == $total_page - 1) {
                $prevBtn.show(0);
                ole.footerNotificationHandler.pageEndSetNotification();
            } else {
                $prevBtn.show(0);
                $nextBtn.show(0);
            }
        }, delay_ms);
    }

    function sound_player(sound_id) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigationcontroller();
        });
    }


    function put_image(content, count) {
        var contentCount=content[count];
        var imageblockcontent=contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent,contentCount)
    }


    function put_image2(content, count) {
        console.log("inside image2" + content[count].hasOwnProperty("imgwithdiv"));
        if (content[count].hasOwnProperty("imgwithdiv")) {
            console.log("Hello I am here");
            var contentCount=content[count].imgwithdiv[0];
            var imageblockcontent=contentCount.hasOwnProperty('imageblock');
            dynamicimageload(imageblockcontent,contentCount);

            var contentCount1=content[count].imgwithdiv[1];
            var imageblockcontent1=contentCount1.hasOwnProperty('imageblock');
            console.log("image "+imageblockcontent1);
            dynamicimageload(imageblockcontent1,contentCount1);
        }
    }
    function dynamicimageload(imageblockcontent,contentCount){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }



    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function animateDiv(){
        $(".and").hide(0);
         setTimeout(function(){
             $(".and").show(0);
         },500);

    }

    function animateFadeIn(){
     setTimeout(function(){
         $(".subrinadiv").show(0).addClass("zoomInEffect");
         setTimeout(function(){
             $(".caterpilardiv").show(0).addClass("zoomInEffect");
             setTimeout(function(){
                 $(".butterflygifdiv").show(0).addClass("zoomInEffect");
                 setTimeout(function(){
                     $(".monkeydiv").show(0).addClass("zoomInEffect");
                 },1000);
             },1000);
         },1000);
     },1000);
    }

    function animateTranslation(){
        $(".title1,.title2").hide(0);
        setTimeout(function(){
            $(".monkeydiv").addClass("translateright");
                $(".caterpilardiv ").addClass("translateleft");
                setTimeout(function(){
                    $(".title1,.title2").show(0).addClass("zoomInEffect");
                    $(".leftdiv").addClass("leftborder");
                    sound_player("sound_4");
                },2000);
        },2000);
    }
});

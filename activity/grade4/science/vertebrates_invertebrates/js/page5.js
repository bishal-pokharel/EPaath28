var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"inner",
        uppertextblock: [
            {
                textclass: "topic",
                textdata: data.string.p5text1
            }
        ]
    },
    //slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                maindiv: "topic1",
                mainclass:"title",
                textdata: data.string.vertsymp,
                text:[
                    {
                        textclass: "slideR title1",
                        textdata: data.string.p5text2
                    }
                ]
            }
        ],
        imageblock: [{
                        imagestoshow: [
                            {
                                imgdiv: "catdiv",
                                imgclass: "relativecls catimg",
                                imgid: 'catImg',
                                imgsrc: "",
                            },
                            {
                                imgdiv: "duckdiv",
                                imgclass: "relativecls duckimg",
                                imgid: 'duckImg',
                                imgsrc: "",
                            },
                            {
                                imgdiv: "dolphindiv",
                                imgclass: "relativecls dolphinimg",
                                imgid: 'dolphinImg',
                                imgsrc: "",
                            },

                            {
                                imgdiv: "beerdiv",
                                imgclass: "relativecls beerimg",
                                imgid: 'beerImg',
                                imgsrc: "",
                            },
                            {
                                imgdiv: "monkeydiv",
                                imgclass: "relativecls monkeyimg",
                                imgid: 'monkeyImg',
                                imgsrc: "",
                            }
                        ]
                    }]

    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                maindiv: "topic1",
                mainclass:"title",
                textdata: data.string.vertsymp,
                text:[
                    {
                        textclass: "title1 fontcolor",
                        textdata: data.string.p5text2
                    },
                    {
                        textclass: "slideR title2",
                        textdata: data.string.p5text3
                    }
                ]
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "penguindiv",
                    imgclass: "relativecls penguinimg",
                    imgid: 'penguinImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "sagardiv",
                    imgclass: "relativecls sagarimg",
                    imgid: 'sagarImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "giraffediv",
                    imgclass: "relativecls giraffeimg",
                    imgid: 'giraffeImg',
                    imgsrc: "",
                }
            ]
        }]

    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                maindiv: "topic1",
                mainclass:"title",
                textdata: data.string.vertsymp,
                text:[
                    {
                        textclass: "fontcolor title1",
                        textdata: data.string.p5text2
                    },
                    {
                        textclass: "fontcolor title2",
                        textdata: data.string.p5text3
                    },
                    {
                        textclass: "slideR title2",
                        textdata: data.string.p5text4
                    }

                ]
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "dogdiv",
                    imgclass: "relativecls giraffeimg",
                    imgid: 'dogImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "sagar1div",
                    imgclass: "relativecls sagarimg",
                    imgid: 'sagarImg',
                    imgsrc: "",
                }

            ]
        }]

    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                maindiv: "topic1",
                mainclass:"title",
                textdata: data.string.vertsymp,
                text:[
                    {
                        textclass: "fontcolor title1",
                        textdata: data.string.p5text2
                    },
                    {
                        textclass: "fontcolor title2",
                        textdata: data.string.p5text3
                    },
                    {
                        textclass: "fontcolor title2",
                        textdata: data.string.p5text4
                    },
                    {
                        textclass: "slideR title2",
                        textdata: data.string.p5text5
                    }

                ]
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "fadeInEffect cheetadiv",
                    imgclass: "relativecls cheetaimg",
                    imgid: 'cheetaImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "fadeInEffect horsediv",
                    imgclass: "relativecls horseimg",
                    imgid: 'horseImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "fadeInEffect elephantdiv",
                    imgclass: "relativecls elephantimg",
                    imgid: 'elephantImg',
                    imgsrc: "",
                }

            ]
        }]

    },
    //slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                maindiv: "topic1",
                mainclass:"title",
                textdata: data.string.vertsymp,
                text:[
                    {
                        textclass: "fontcolor title1",
                        textdata: data.string.p5text2
                    },
                    {
                        textclass: "fontcolor title2",
                        textdata: data.string.p5text3
                    },
                    {
                        textclass: "fontcolor title2",
                        textdata: data.string.p5text4
                    },
                    {
                        textclass: "fontcolor title2",
                        textdata: data.string.p5text5
                    },
                    {
                        textclass: "slideR  title2",
                        textdata: data.string.p5text6
                    }

                ]
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "birddiv",
                    imgclass: "relativecls birdimg",
                    imgid: 'birdImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "chickendiv",
                    imgclass: "relativecls chickenimg",
                    imgid: 'chickenImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "snakediv",
                    imgclass: "relativecls snakeimg",
                    imgid: 'snakeImg',
                    imgsrc: "",
                }

            ]
        }]

    },
    //slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"inner",
        uppertextblock: [
            {
                textclass: "topic2",
                textdata: data.string.p5text7
            }
        ]
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "common-css", src: $ref + "css/common.css", type: createjs.AbstractLoader.CSS},
            {id: "page1-css", src: $ref + "css/page1.css", type: createjs.AbstractLoader.CSS},

            {id: "catImg", src: imgpath+"Page04/cat02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "duckImg", src: imgpath+"Page04/duck02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dolphinImg", src: imgpath+"Page04/dolphin02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "beerImg", src: imgpath+"Page04/beer02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "monkeyImg", src: imgpath+"Page04/monkey02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "penguinImg", src: imgpath+"Page04/pengiun.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sagarImg", src: imgpath+"Page04/sagar-08.png", type: createjs.AbstractLoader.IMAGE},
            {id: "giraffeImg", src: imgpath+"Page01/girafee.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dogImg", src: imgpath+"Page04/doggy-barking-01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "horseImg", src: imgpath+"Page04/horse.png", type: createjs.AbstractLoader.IMAGE},
            {id: "cheetaImg", src: imgpath+"Page04/cheeta.png", type: createjs.AbstractLoader.IMAGE},
            {id: "elephantImg", src: imgpath+"Page04/elephant_new.png", type: createjs.AbstractLoader.IMAGE},
            {id: "birdImg", src: imgpath+"Page01/bird_flying.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "chickenImg", src: imgpath+"Page04/chicken-rooster.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "snakeImg", src: imgpath+"Page03/snake.gif", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset+"p5_s0.ogg"},
            {id: "sound_1", src: soundAsset+"p5_s1.ogg"},
            {id: "sound_2", src: soundAsset+"p5_s2.ogg"},
            {id: "sound_3", src: soundAsset+"p5_s3.ogg"},
            {id: "sound_4", src: soundAsset+"p5_s4.ogg"},
            {id: "sound_5", src: soundAsset+"p5_s5.ogg"},
            {id: "sound_6", src: soundAsset+"p5_s6.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

    /*===============================================
    =            data highlight function            =
    ===============================================*/
    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function (index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

    /*=====  End of data highlight function  ======*/


    /*======================================================
     =            Navigation Controller Function            =
     ======================================================*/

    function navigationcontroller(islastpageflag) {
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean' ?
                alert("NavigationController : Hi Master, please provide a boolean parameter") :
                null;

        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
            $prevBtn.css('display', 'none');
        }
        else if ($total_page == 1) {
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');

            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if (countNext > 0 && countNext < $total_page - 1) {
            $nextBtn.show(0);
            $prevBtn.show(0);
        }
        else if (countNext == $total_page - 1) {
            $nextBtn.css('display', 'none');
            $prevBtn.show(0);

            // if lastpageflag is true
            ole.footerNotificationHandler.pageEndSetNotification();
        }

    }

    /*=====  End of user navigation controller function  ======*/


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);


        // highlight any text inside board div with datahighlightflag set true
        // texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext);
        switch (countNext) {
            case 0:
                sound_player("sound_0",true);
                break;
            case 1:
                sound_player("sound_1",true);
                animateShape();
                break;
            case 2:
                sound_player("sound_2",true);
                animateImg();
                break;
            case 3:
                sound_player("sound_3",true);
                animatePic();
                break;
            case 4:
                sound_player("sound_4",true);
                break;
            case 5:
                sound_player("sound_5",true);
                break;
            case 6:
                sound_player("sound_6",true);
                break;
            default:
                navigationcontroller();
                break;
        }
    }

    function nav_button_controls(delay_ms) {
        timeoutvar = setTimeout(function () {
            if (countNext == 0) {
                $nextBtn.show(0);
            } else if (countNext > 0 && countNext == $total_page - 1) {
                $prevBtn.show(0);
                ole.footerNotificationHandler.pageEndSetNotification();
            } else {
                $prevBtn.show(0);
                $nextBtn.show(0);
            }
        }, delay_ms);
    }

    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller():'';
        });
    }


    function put_image(content, count) {
        var contentCount = content[count];
        var imageblockcontent = contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent, contentCount)
    }

    // function put_image2(content, count) {
    //     if (content[count].hasOwnProperty("imgwithdiv")) {
    //         console.log("Hello I am here");
    //         var contentCount = content[count].imgwithdiv[0];
    //         var imageblockcontent = contentCount.hasOwnProperty('imageblock');
    //         dynamicimageload(imageblockcontent, contentCount);
    //
    //         var contentCount1 = content[count].imgwithdiv[1];
    //         var imageblockcontent1 = contentCount1.hasOwnProperty('imageblock');
    //         console.log("image " + imageblockcontent1);
    //         dynamicimageload(imageblockcontent1, contentCount1);
    //         if(contentCount1.hasOwnProperty("extradiv")){
    //             console.log("This is test");
    //             var contentCount2 = contentCount1.extradiv[0];
    //             var imageblockcontent2 = contentCount2.hasOwnProperty('imageblock');
    //             dynamicimageload(imageblockcontent2, contentCount2);
    //
    //         }
    //     }
    // }

    function dynamicimageload(imageblockcontent, contentCount) {
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

  function animateShape(){
      setTimeout(function(){
          $(".catimg").addClass("scaleEffect").attr("src",imgpath+"Page04/cat01.png");
          setTimeout(function(){
              $(".duckimg").addClass("scaleEffect").attr("src",imgpath+"Page04/duck01.png");
              setTimeout(function(){
                  $(".dolphinimg").addClass("scaleEffect").attr("src",imgpath+"Page04/dolphin01.png");
                  setTimeout(function(){
                      $(".beerimg").addClass("scaleEffect").attr("src",imgpath+"Page04/beer01.png");
                      setTimeout(function(){
                          $(".monkeyimg").addClass("scaleEffect").attr("src",imgpath+"Page04/monkey01.png");
                          },1000);
                  },1000);
              },1000);
          },1000);
      },2000);
  }
  function animateImg(){
      $(".penguindiv,.sagardiv,.giraffediv").hide(0);
      setTimeout(function(){
          $(".penguindiv").show(0).addClass("zoomInEffect");
          setTimeout(function(){
              $(".sagardiv").show(0).addClass("zoomInEffect");
              setTimeout(function(){
                  $(".giraffediv").show(0).addClass("zoomInEffect");
              },1000);
          },1000);
      },1000);
  }
  function animatePic(){
      $(".dogdiv,.sagar1div").hide(0);
      setTimeout(function(){
          $(".dogdiv").show(0).addClass("slideL");
          setTimeout(function(){
              $(".sagar1div").show(0).addClass("slideR");
          },1000);
      },1000);
  }

});
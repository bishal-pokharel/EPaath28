var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var content = [
    //slide 0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "commonques qtitle",
                textdata: data.string.query
            },
            {
                textclass: "commonques qtitle1",
                textdata: data.string.q1
            }
        ],
        imageblock:[{
            imagestoshow:[
                {
                    imgdiv:"img1div ",
                    imgclass: "relativecls wrong img1",
                    imgid : 'slug1img',
                    imgsrc: "",
                },
                {
                    imgdiv:"img2div",
                    imgclass: "relativecls right img2 ",
                    imgid : 'dolphin1img',
                    imgsrc: "",
                }
            ]
        }]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "commonques qtitle",
                textdata: data.string.query
            },
            {
                textclass: "commonques qtitle1",
                textdata: data.string.q2
            }
        ],
        imageblock:[{
            imagestoshow:[
                {
                    imgdiv:"img1div ",
                    imgclass: "relativecls right img1",
                    imgid : 'spider1img',
                    imgsrc: "",
                },
                {
                    imgdiv:"img2div",
                    imgclass: "relativecls wrong img2 ",
                    imgid : 'coc1img',
                    imgsrc: "",
                }
            ]
        }]
    },
//     //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "commonques qtitle",
                textdata: data.string.query
            },
            {
                textclass: "commonques qtitle1",
                textdata: data.string.q3
            }
        ],
        imageblock:[{
            imagestoshow:[
                {
                    imgdiv:"img1div ",
                    imgclass: "relativecls wrong img1",
                    imgid : 'dragon1img',
                    imgsrc: "",
                },
                {
                    imgdiv:"img2div",
                    imgclass: "relativecls right img2 ",
                    imgid : 'dog1img',
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "commonques qtitle",
                textdata: data.string.query
            },
            {
                textclass: "commonques qtitle1",
                textdata: data.string.q4
            }
        ],
        imageblock:[{
            imagestoshow:[
                {
                    imgdiv:"img1div ",
                    imgclass: "relativecls wrong img1",
                    imgid : 'elephant1img',
                    imgsrc: "",
                },
                {
                    imgdiv:"img2div",
                    imgclass: "relativecls right img2 ",
                    imgid : 'leach1img',
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "commonques qtitle",
                textdata: data.string.query
            },
            {
                textclass: "commonques qtitle1",
                textdata: data.string.q5
            }
        ],
        imageblock:[{
            imagestoshow:[
                {
                    imgdiv:"img1div ",
                    imgclass: "relativecls right img1",
                    imgid : 'worm1img',
                    imgsrc: "",
                },
                {
                    imgdiv:"img2div",
                    imgclass: "relativecls wrong img2 ",
                    imgid : 'tiger1img',
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "commonques qtitle",
                textdata: data.string.query
            },
            {
                textclass: "commonques qtitle1",
                textdata: data.string.q6
            }
        ],
        imageblock:[{
            imagestoshow:[
                {
                    imgdiv:"img1div ",
                    imgclass: "relativecls right img1",
                    imgid : 'slug1img',
                    imgsrc: "",
                },
                {
                    imgdiv:"img2div",
                    imgclass: "relativecls wrong img2 ",
                    imgid : 'sheep1img',
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "commonques qtitle",
                textdata: data.string.query
            },
            {
                textclass: "commonques qtitle1",
                textdata: data.string.q7
            }
        ],
        imageblock:[{
            imagestoshow:[
                {
                    imgdiv:"img1div ",
                    imgclass: "relativecls right img1",
                    imgid : 'cheetah1img',
                    imgsrc: "",
                },
                {
                    imgdiv:"img2div",
                    imgclass: "relativecls wrong img2 ",
                    imgid : 'butterfly1img',
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "commonques qtitle",
                textdata: data.string.query
            },
            {
                textclass: "commonques qtitle1",
                textdata: data.string.q8
            }
        ],
        imageblock:[{
            imagestoshow:[
                {
                    imgdiv:"img1div ",
                    imgclass: "relativecls wrong img1",
                    imgid : 'worm1img',
                    imgsrc: "",
                },
                {
                    imgdiv:"img2div",
                    imgclass: "relativecls right img2 ",
                    imgid : 'dolphin1img',
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "commonques qtitle",
                textdata: data.string.query
            },
            {
                textclass: "commonques qtitle2",
                textdata: data.string.q9
            }
        ],
        imageblock:[{
            imagestoshow:[
                {
                    imgdiv:"img1div ",
                    imgclass: "relativecls right img1",
                    imgid : 'snail1img',
                    imgsrc: "",
                },
                {
                    imgdiv:"img2div",
                    imgclass: "relativecls wrong img2 ",
                    imgid : 'elephant1img',
                    imgsrc: "",
                }
            ]
        }]
    },
    // slide 9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "commonques qtitle",
                textdata: data.string.query
            },
            {
                textclass: "commonques qtitle2",
                textdata: data.string.q10
            }
        ],
        imageblock:[{
            imagestoshow:[
                {
                    imgdiv:"img1div ",
                    imgclass: "relativecls right img1",
                    imgid : 'snail1img',
                    imgsrc: "",
                },
                {
                    imgdiv:"img2div",
                    imgclass: "relativecls wrong img2 peng ",
                    imgid : 'penguinimg',
                    imgsrc: "",
                }
            ]
        }]
    },
    // slide10
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var countNext = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;

    var lampTemplate = new LampTemplate();
    lampTemplate.init($total_page-1);


    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "ex", src: $ref + "css/exercisetemplate.css", type: createjs.AbstractLoader.CSS},
            {id: "ex1", src: $ref + "css/exercise1.css", type: createjs.AbstractLoader.CSS},

            {id: "slug1img", src: imgpath+"snail.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dolphin1img", src: imgpath+"dolpin.png", type: createjs.AbstractLoader.IMAGE},
            {id: "spider1img", src: imgpath+"spider.png", type: createjs.AbstractLoader.IMAGE},
            {id: "coc1img", src: imgpath+"cocroach.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dragon1img", src: imgpath+"dragon_fly.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dog1img", src: imgpath+"dog.png", type: createjs.AbstractLoader.IMAGE},
            {id: "elephant1img", src: imgpath+"elephant.png", type: createjs.AbstractLoader.IMAGE},
            {id: "leach1img", src: imgpath+"leach.png", type: createjs.AbstractLoader.IMAGE},
            {id: "worm1img", src: imgpath+"worm.png", type: createjs.AbstractLoader.IMAGE},
            {id: "tiger1img", src: imgpath+"tiger.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sheep1img", src: imgpath+"sheep.png", type: createjs.AbstractLoader.IMAGE},
            {id: "cheetah1img", src: imgpath+"cheetah.png", type: createjs.AbstractLoader.IMAGE},
            {id: "butterfly1img", src: imgpath+"butterfly.png", type: createjs.AbstractLoader.IMAGE},
            {id: "snail1img", src: imgpath+"snail01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "penguinimg", src: imgpath+"penguin.png", type: createjs.AbstractLoader.IMAGE},
            //sound
              {id: "sound_1", src: soundAsset+"ex.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());



    /*======================================================
     =            Navigation Controller Function            =
     ======================================================*/

    function navigationcontroller(islastpageflag) {
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean' ?
                alert("NavigationController : Hi Master, please provide a boolean parameter") :
                null;

        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
        }
        else if ($total_page == 1) {
            $nextBtn.css('display', 'none');

            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if (countNext > 0 && countNext < $total_page - 1) {

            $nextBtn.show(0);
        }
        else if (countNext == $total_page - 2) {

            $nextBtn.css('display', 'none');
            // if lastpageflag is true
            ole.footerNotificationHandler.pageEndSetNotification();
        }

    }

    /*=====  End of user navigation controller function  ======*/


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);

        put_image(content, countNext);
        shuffleImage();
        checkans();
        if(countNext==0){
          sound_player("sound_1");
        }
    }


    function sound_player(sound_id) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
    }


    function put_image(content, count) {
        if (content[count].hasOwnProperty('imageblock')) {
            var imageblock = content[count].imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }



    function templateCaller() {
        $nextBtn.css('display', 'none');
        generaltemplate();
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                lampTemplate.gotoNext();
                templateCaller();
                break;
        }
    });


    function shuffleImage(){
        var imgdiv = $('#encapsulateddiv');
        for (var i = imgdiv.children.length; i >= 0; i--) {
            console.log("I am here"+imgdiv.children().eq(Math.random() * i | 0));
            imgdiv.append(imgdiv.children().eq(Math.random() * i | 0));
        }
        $(".encapsulated :first-child").not("img").attr("class","").addClass("img1div");
        $(".encapsulated :first-child").not("img").next().attr("class","").addClass("img2div");
    }

    function checkans(){
        $(".right").click(function(){
            play_correct_incorrect_sound(1);
            $(this).parent().prepend("<img class='correctWrongImg' src='images/right.png'/>")
            $(this).parent().addClass("avoid-clicks");
            $(".wrong").parent().addClass("avoid-clicks grayout");
            lampTemplate.update(true);
            navigationcontroller();
        });
        $(".wrong").click(function(){
            play_correct_incorrect_sound(0);
            $(this).parent().prepend("<img class='correctWrongImg' src='images/wrong.png'/>")
            $(this).parent().addClass("avoid-clicks grayout");
            lampTemplate.update(false);
        });

    }
});

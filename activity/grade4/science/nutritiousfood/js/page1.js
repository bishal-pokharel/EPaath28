var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
{
	uppertextblock : [{
			textdata : data.lesson.chapter,
			textclass : 'lesson-title'
			}],
	imageblock: [
	{
		imagetoshow: [


		{
			imgclass: "bg_full",
			imgsrc : imgpath + "cover_page.png"
		}

		]
	}]
},

{
	contentblockadditionalclass:'ole-background-gradient-blunatic',
	uppertextblock : [{
			textdata : data.string.p1text1,
			textclass : 'upptertext'
		},
		{
				textdata : data.string.p1text2,
				textclass : 'middletext'
			}],
	imageblock: [
	{
		imagetoshow: [


		{
			imgclass: "bottomimage",
			imgsrc : imgpath + "lokharke02.png"
		}

		]
	}]
},
{
	contentblockadditionalclass:'ole-background-gradient-blunatic',
	uppertextblock : [{
			textdata : data.string.p1text3,
			textclass : 'upptertext'
		},
		{
				textdata : data.string.p1text4,
				textclass : 'middletext'
			}],
},
{
	contentblockadditionalclass:'ole-background-gradient-blunatic',
	uppertextblock : [{
			textdata : data.string.p1text5,
			textclass : 'middletext'
		}],
},
{
	contentblockadditionalclass:'ole-background-gradient-blunatic',
	uppertextblock : [{
			textdata : data.string.p1text6,
			textclass : 'upptertext'
		},
		{
				textdata : data.string.p1text7,
				textclass : 'middletext'
		},
		{
				textdata : data.string.p1text8,
				textclass : 'bottomtext'
		}],

		imageblock: [
			{
				imagetoshow: [
				{
					imgclass: "leftbottomimage",
					imgsrc : imgpath + "rice01.png"
				},
				{
					imgclass: "rightbottomimage",
					imgsrc : imgpath + "carrot.png"
				}
				]
			}]
},

//slide5

{
	contentblockadditionalclass:'ole-background-gradient-blunatic',
	uppertextblock : [{
			textdata : data.string.p1text11,
			textclass : 'upptertext1'
		}],

		imageblock: [
			{
				imagetoshow: [
				{
					imgclass: "rightbottomimage1",
					imgsrc : imgpath + "lokharke01.png"
				}
				]
			}]
},

//slide6

{
	contentblockadditionalclass:'ole-background-gradient-blunatic',
	uppertextblock : [{
			textdata : data.string.p1text12,
			textclass : 'topic'
		},
		{
				textdata : data.string.p1text13,
				textclass : 'definition'
		},
		{
				textdata : data.string.p1text14,
				textclass : 'belowtext'
		},
		{
				textdata : data.string.p1text15,
				textclass : 'points'
		}],

		imageblock: [
			{
				imagetoshow: [
				{
					imgclass: "featuredimage",
					imgsrc : imgpath + "rice.png"
				}
				]
			}]
},

//slide7

{
	contentblockadditionalclass:'ole-background-gradient-blunatic',
	uppertextblock : [{
			textdata : data.string.p1text16,
			textclass : 'topic'
		},
		{
				textdata : data.string.p1text17,
				textclass : 'definition'
		},
		{
				textdata : data.string.p1text18,
				textclass : 'belowtext'
		},
		{
				textdata : data.string.p1text19,
				textclass : 'points'
		}],

		imageblock: [
			{
				imagetoshow: [
				{
					imgclass: "featuredimage",
					imgsrc : imgpath + "vegetable.png"
				}
				]
			}]
},

//slide8

{
	contentblockadditionalclass:'ole-background-gradient-blunatic',
	uppertextblock : [{
			textdata : data.string.p1text20,
			textclass : 'topic'
		},
		{
				textdata : data.string.p1text21,
				textclass : 'definition'
		},
		{
				textdata : data.string.p1text22,
				textclass : 'belowtext'
		},
		{
				textdata : data.string.p1text23,
				textclass : 'points'
		}],

		imageblock: [
			{
				imagetoshow: [
				{
					imgclass: "featuredimage",
					imgsrc : imgpath + "mixedfood.png"
				}
				]
			}]
},

//slide9

{
	contentblockadditionalclass:'ole-background-gradient-blunatic',
	uppertextblock : [{
			textdata : data.string.p1text20,
			textclass : 'desc1'
		},
		{
				textdata : data.string.p1text16,
				textclass : 'desc2'
		},
		{
				textdata : data.string.p1text12,
				textclass : 'desc3'
		},
		{
				textdata : data.string.p1text24,
				textclass : 'topic1'
		}],

		imageblock: [
			{
				imagetoshow: [
				{
					imgclass: "featuredimage1",
					imgsrc : imgpath + "mixedfood.png"
				},
				{
					imgclass: "featuredimage2",
					imgsrc : imgpath + "vegetable.png"
				},
				{
					imgclass: "featuredimage3",
					imgsrc : imgpath + "rice.png"
				}
				]
			}]
},
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);

	var preload;
	var timeoutvar = null;
	var current_sound;


	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
      {id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
      {id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
      {id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
      {id: "s1_p4", src: soundAsset+"s1_p4.ogg"},
			{id: "s1_p5", src: soundAsset+"s1_p5.ogg"},
			{id: "s1_p6", src: soundAsset+"s1_p6.ogg"},
			{id: "s1_p7", src: soundAsset+"s1_p7.ogg"},
			{id: "s1_p8", src: soundAsset+"s1_p8.ogg"},
			{id: "s1_p9", src: soundAsset+"s1_p9.ogg"},
			{id: "s1_p10", src: soundAsset+"s1_p10.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


      function navigationcontroller(islastpageflag){
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		vocabcontroller.findwords(countNext);
		texthighlight($board);
		switch(countNext){
			case 0:
			sound_player_nav("s1_p1");
			break;
			case 1:
			sound_player_nav("s1_p2");
			$('.upptertext').addClass('fadein');
			setTimeout(function(){
				$('.middletext').delay(1000).addClass('fadein');
				$('.bottomimage').delay(1000).addClass('fadein');
			}, 1000);
			break;
			case 2:
			sound_player_nav("s1_p3");
			$('.upptertext').addClass('fadein');
			setTimeout(function(){
				$('.middletext').delay(1000).addClass('fadein');
			}, 1000);
			break;
			case 3:
			sound_player_nav("s1_p4");
			$('.middletext').addClass('fadein');
			break;
			case 4:
			sound_player_nav("s1_p5");
			$('.upptertext').addClass('fadein');
			setTimeout(function(){
				$('.leftbottomimage').delay(1000).addClass('fadein');
				$('.middletext').delay(1000).addClass('fadein');
			}, 1000);
			setTimeout(function(){
				$('.rightbottomimage').delay(2000).addClass('fadein');
				$('.bottomtext').delay(2000).addClass('fadein');
			}, 2000);
			break;
			case 5:
			sound_player_nav("s1_p6");
			$('.upptertext1').addClass('fadein');
			$('.rightbottomimage1').delay(1000).addClass('fadein');
			break;
			case 6:
			case 7:
			case 8:
			sound_player_nav("s1_p"+(countNext+1));
			$(".definition,.belowtext,.points,ul").hide();
			$(".definition").delay(1000).fadeIn(1000);
			$('.featuredimage').animate({width:'29%'},800);
			$(".belowtext").delay(2000).fadeIn(1000);
			$("ul").delay(3000).fadeIn(1000);
			break;
			case 9:
			sound_player_nav("s1_p"+(countNext+1));
			$('.featuredimage1,.featuredimage2,.featuredimage3').animate({width:'25%'},800);
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls();
		});
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});


 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
{

	contentblockadditionalclass:"ole-background-gradient-wheat",
	uppertextblock : [{
			textdata : data.string.p4text1,
			textclass : 'rabitaddtalks'
		}],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "angel",
				imgsrc : imgpath + "flying-chibi-fairy-animation.gif"
			}
			]
		}]
},

// slide1
{

contentblockadditionalclass:"ole-background-gradient-wheat",
uppertextblock : [{
		textdata : data.string.p4text2,
		textclass : 'boytext'
	}],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "angel1",
			imgsrc : imgpath + "flying-chibi-fairy-animation.gif"
		},
		{
			imgclass:"boyeats",
			imgsrc : imgpath + "homemadefood.png"
		}
		]
	}]
},

// slide2
{

contentblockadditionalclass:"ole-background-gradient-wheat",
uppertextblock : [{
		textdata : data.string.p4text3,
		textclass : 'boytext'
	}],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "angel1",
			imgsrc : imgpath + "flying-chibi-fairy-animation.gif"
		},
		{
			imgclass:"foodcentered",
			imgsrc : imgpath + "food.png"
		}
		]
	}]
},

// slide3
{

contentblockadditionalclass:"ole-background-gradient-wheat",
uppertextblock : [{
		textdata : data.string.p4text4,
		textclass : 'boytext'
	},
	{
			textdata : data.string.p4text5,
			textclass : 'iodinetext1'
		},
		{
				textdata : data.string.p4text6,
				textclass : 'iodinetext2'
			}
			],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "angel1",
			imgsrc : imgpath + "flying-chibi-fairy-animation.gif"
		},
		{
			imgclass:"saltimage",
			imgsrc : imgpath + "salt.png"
		}
		]
	}]
},

// slide4
{

contentblockadditionalclass:"ole-background-gradient-wheat",
uppertextblock : [{
		textdata : data.string.p4text7,
		textclass : 'boytext'
	}],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "angel1",
			imgsrc : imgpath + "flying-chibi-fairy-animation.gif"
		},
		{
			imgclass:"girl",
			imgsrc : imgpath + "girl.png"
		},
		{
			imgclass:"salt",
			imgsrc : imgpath + "salt.png"
		}
		]
	}]
},

// slide5
{

contentblockadditionalclass:"ole-background-gradient-wheat",
uppertextblock : [{
		textdata : data.string.p4text8,
		textclass : 'textop'
	},
	{
			textdata : data.string.p4text9,
			textclass : 'textbot'
		}],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "angel1",
			imgsrc : imgpath + "flying-chibi-fairy-animation.gif"
		},
		{
			imgclass:"foodcentered",
			imgsrc : imgpath + "girl.png"
		}
		]
	}]
},


];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;
	var clicks = 0;

	var  inputname = '';


	var $total_page = content.length;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);

	var preload;
	var timeoutvar = null;
	var current_sound;


	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
      {id: "s1_p1", src: soundAsset+"s4_p1.ogg"},
      {id: "s1_p2", src: soundAsset+"s4_p2.ogg"},
      {id: "s1_p3", src: soundAsset+"s4_p3.ogg"},
      {id: "s1_p4", src: soundAsset+"s4_p4.ogg"},
			{id: "s1_p5", src: soundAsset+"s4_p5.ogg"},
			{id: "s1_p6", src: soundAsset+"s4_p6.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


      function navigationcontroller(islastpageflag){
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		vocabcontroller.findwords(countNext);
		texthighlight($board);
		switch (countNext) {
			case 0:
			sound_player_nav("s1_p"+(countNext+1));
			break;
			case 1:
			sound_player_nav("s1_p"+(countNext+1));
			$(".angel1").css("top","33%");
			$('.boyeats').animate({width:"80%"},1000);
			break;
			case 2:
			sound_player_nav("s1_p"+(countNext+1));
			$(".angel1").css({
				"transform":"scaleX(-1)",
				"width":"17%",
				"left":"79%",
				"top":"16%"
			});
			$('.foodcentered').animate({width:"40%"},1000);
			break;
			case 3:
			sound_player_nav("s1_p"+(countNext+1));
      $(".angel1").css({
          "top":"28%",
          "width":"15%",
      });
			$('.saltimage').animate({width:"40%"},1000);
			break;
			case 4:
			sound_player_nav("s1_p"+(countNext+1));
      $(".angel1").css({
          "top":"22%",
      });
			$('.girl,.salt').animate({width:"35%"},1000);
			break;
			case 5:
			sound_player_nav("s1_p"+(countNext+1));
      $(".angel1").css({
          "width":"16%",
      });
			$('.foodcentered').animate({width:"40%"},1000);
			break;
		}
	}




// for entering the value in input box
	function input_box(input_class, button_class) {
		$(input_class).keydown(function(event){
				var charCode = (event.which) ? event.which : event.keyCode;
				/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
				if(charCode === 13 && button_class!=null) {
						$(button_class).trigger("click");
			}
			var condition = charCode != 8 && charCode != 16 && charCode != 20 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, shift, caps , backspace or arrow keys
				if (!condition) {
					return true;
				}
				//check if user inputs more than one '.'
			if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
						return false;
				}
				//check . and 0-9 separately after checking arrow and other keys
				if((charCode < 65 || charCode > 90)){
					return false;
				}
				return true;
		});
		$(input_class).keyup(function(event){
    		if (String(event.target.value).length >= 1) {
    			$(".sbmtbtn").show(0);
    			$(button_class).show(0);
    			global_save_val = String(event.target.value);
    		}
    		else{
    			$(".sbmtbtn").hide(0);
    			$(button_class).hide(0);
    		}
  			return true;
		});
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls();
		});
	}
	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});


 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

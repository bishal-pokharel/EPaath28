var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
{
	contentblockadditionalclass:'ole-background-gradient-rose_glow',
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "angel",
			imgsrc : imgpath + "flying-chibi-fairy-animation.gif"
		},{
			imgclass:"image1",
			imgsrc: imgpath + "chips-39.png"
		},{
			imgclass:"image2",
			imgsrc: imgpath + "rotten-food.png"
		}
		]
	}],
	uppertextblock:[{
		textdata:data.string.p5text4,
		textclass:'toptext '
	},
		{
            textdata:data.string.p5text1,
            textclass:'toptext5_1'
		}]
},
{
	contentblockadditionalclass:'green',
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "angel1",
			imgsrc : imgpath + "flying-chibi-fairy-animation.gif"
		},{
			imgclass:"disease",
			imgsrc: imgpath + "disease.png"
		}
		]
	}],
	uppertextblock:[{
		textdata:data.string.p5text2,
		textclass:'toptext white padding'
	}],
	imagecapblock:[{
		imagecapclass:"diseaseimg1",
		imagecapdata:data.string.vomit
	},{
		imagecapclass:"diseaseimg2",
		imagecapdata:data.string.stom
	},
	{
		imagecapclass:"diseaseimg3",
		imagecapdata:data.string.fever
	},{
		imagecapclass:"diseaseimg4",
		imagecapdata:data.string.diarrhoea
	},
	{
		imagecapclass:"diseaseimg5",
		imagecapdata:data.string.dental
	},{
		imagecapclass:"diseaseimg6",
		imagecapdata:data.string.head
	},
	{
		imagecapclass:"diseaseimg7",
		imagecapdata:data.string.cough
	},{
		imagecapclass:"diseaseimg8",
		imagecapdata:data.string.appe
	}
	]

},
{
	contentblockadditionalclass:'changingbackgrounds',
	imageblock: [
	{
        imagetoshow: [
            {
                imgclass: "bg1 img1",
                imgsrc: imgpath + "healthyfood01.jpg"
            },
            {
                imgclass: "bg1 img2",
                imgsrc: imgpath + "healthyfood02.jpg"
            },
			{
				imgclass: "angel2",
				imgsrc : imgpath + "flying-chibi-fairy-animation.gif"
			},
		]
	}],
	uppertextblock:[{
		textdata:data.string.p5text3,
		textclass:'template-dialougebox2-top-yellow dialogue'
	}]
}
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);

	var preload;
	var timeoutvar = null;
	var current_sound;


	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
      {id: "s1_p1", src: soundAsset+"s5_p1.ogg"},
      {id: "s1_p2", src: soundAsset+"s5_p2.ogg"},
      {id: "s1_p3", src: soundAsset+"s5_p3.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


      function navigationcontroller(islastpageflag){
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		vocabcontroller.findwords(countNext);
		texthighlight($board);
		switch(countNext){
			case 0:
			sound_player_nav("s1_p"+(countNext+1));
			break;
			case 1:
			sound_player_nav("s1_p"+(countNext+1));
			$('.disease').animate({width:"100%" },1500);
			break;
			case 2:
			sound_player_nav("s1_p"+(countNext+1));
			var interval =setInterval(function(){
      $(".img2").delay(1000).fadeOut(1000);
      $(".img1").delay(1000).fadeIn(1000);
      $(".img2").delay(1500).fadeIn(1000);
      $(".img1").delay(2000).fadeOut(1000);
    },3500);
			break;

		}
	}
    function swapC() {
		$(".img1").
        window.setTimeout(function() { swapC() }, 500)
    }
		function nav_button_controls(delay_ms){
			timeoutvar = setTimeout(function(){
				if(countNext==0){
					$nextBtn.show(0);
				} else if( countNext>0 && countNext == $total_page-1){
					$prevBtn.show(0);
					ole.footerNotificationHandler.lessonEndSetNotification();
				} else{
					$prevBtn.show(0);
					$nextBtn.show(0);
				}
			},delay_ms);
		}

		function sound_player(sound_id){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
		}

		function sound_player_nav(sound_id){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
			current_sound.on('complete', function(){
				nav_button_controls();
			});
		}
	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler lessonEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});


 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

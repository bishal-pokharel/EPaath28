var imgpath = $ref+"/images/exercise/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var imageArray = ["01","02","03","04","05","06","07","08","09","10"];
var no_of_draggable = 4; //no of draggable to display at a time
Array.prototype.shufflearray = function(){
  	var i = this.length, j, temp;
    while(--i > 0){
        j = Math.floor(Math.random() * (i+1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
	return this;
};

var content=[
	//2th slide
	{
		// contentblockadditionalclass: "simplebg",
  		contentnocenteradjust: true,
  		textblockadditionalclass: 'instruction',
	  	textblock: [{
	  		textdata: data.string.et4,
	  		textclass: 'head-title'
  		}],

  		subtextblock: [{
  			subtextdata: data.string.s1,
  			subtextclass: "showtext show1"
  		},{
  			subtextdata: data.string.s2,
  			subtextclass: "showtext show2"
  		},{
  			subtextdata: data.string.s3,
  			subtextclass: "showtext show3"
  		},{
  			subtextdata: data.string.l1,
  			subtextclass: "showtext show4"
  		},{
  			subtextdata: data.string.l2,
  			subtextclass: "showtext show5"
  		},{
  			subtextdata: data.string.l3,
  			subtextclass: "showtext show6"
  		},{
  			subtextdata: data.string.l4,
  			subtextclass: "showtext show7"
  		},{
  			subtextdata: data.string.g1,
  			subtextclass: "showtext show8"
  		},{
  			subtextdata: data.string.g2,
  			subtextclass: "showtext show9"
  		},{
  			subtextdata: data.string.g3,
  			subtextclass: "showtext show10"
  		}],
	  	// draggableblockadditionalclass: 'frac_ques',
	  	draggableblock:[{
	  		draggables:[{
	  			draggableclass:"class_1 sliding hidden",
	  			has_been_dropped : false,
				imgclass: "chocolate",
				imgsrc: imgpath + "banana.png",
                labeldata:data.string.banana
			},{
	  			draggableclass:"class_2 sliding hidden",
	  			has_been_dropped : false,
				imgclass: "spill-water",
				imgsrc: imgpath + "chese.png",
                labeldata:data.string.cheese

            },{
	  			draggableclass:"class_3 sliding hidden",
	  			has_been_dropped : false,
				imgclass: "smoke",
				imgsrc: imgpath + "corn.png",
                labeldata:data.string.corn
            },{
	  			draggableclass:"class_2 sliding hidden",
	  			has_been_dropped : false,
				imgclass: "rain",
				imgsrc: imgpath + "curd.png",
                labeldata:data.string.curd
            },{
	  			draggableclass:"class_1 sliding hidden",
	  			has_been_dropped : false,
				imgclass: "mobile",
				imgsrc: imgpath + "brocauli.png",
                labeldata:data.string.broccoli
            },{
	  			draggableclass:"class_3 sliding hidden",
	  			has_been_dropped : false,
				imgclass: "balloon",
				imgsrc: imgpath + "rice02.png",
                labeldata:data.string.rice

            },{
	  			draggableclass:"class_2 sliding hidden",
	  			has_been_dropped : false,
				imgclass: "tea",
				imgsrc: imgpath + "milk.png",
                labeldata:data.string.milk
            },{
	  			draggableclass:"class_1 sliding hidden",
	  			has_been_dropped : false,
				imgclass: "computer",
				imgsrc: imgpath + "carrot.png",
                labeldata:data.string.carrot

            },{
	  			draggableclass:"class_3 sliding hidden",
	  			has_been_dropped : false,
				imgclass: "steam",
				imgsrc: imgpath + "sugercane.png",
                labeldata:data.string.sugarcane

            },{
	  			draggableclass:"class_2 sliding hidden",
	  			has_been_dropped : false,
				imgclass: "juice",
				imgsrc: imgpath + "fish.png",
                labeldata:data.string.fish
            },]
	  	}],
		droppableblock:[{
	  		droppables:[{
	  			headerdata:data.string.et1,
	  			headerclass: "identity",
	  			droppablecontainerclass:"",
	  			droppableclass:"drop_class_1",
					imgclass: "",
			},{
	  			headerdata:data.string.et2,
	  			headerclass: "identity",
	  			droppablecontainerclass:"",
	  			droppableclass:"drop_class_2",
					imgclass: "",
			},{
	  			headerdata:data.string.et3,
	  			headerclass: "identity",
	  			droppablecontainerclass:"",
	  			droppableclass:"drop_class_3",
					imgclass: "",
			}]
	  }]
	}
];

var dummy_class = {
	draggableclass:"dummy_class hidden",
	imgclass: "",
	imgsrc: ""
};

/* Suffle content elements for draggable
 * add some dummy class so that flex behaves correctly
 * add position for first fixed no of draggables
 */
content[0].draggableblock[0].draggables.shufflearray();
for( var i = 1; i<no_of_draggable+1; i++){
	var asd = content[0].draggableblock[0].draggables[i-1].draggableclass.split('"')[0].split('hidden');
	content[0].draggableblock[0].draggables[i-1].draggableclass = asd[0]+'position_'+i;
	content[0].draggableblock[0].draggables.push(dummy_class);
}


$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 10;

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
      {id: "exer", src: soundAsset+"ex.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
	Handlebars.registerPartial("draggablecontent", $("#draggablecontent-partial").html());
	Handlebars.registerPartial("droppablecontent", $("#droppablecontent-partial").html());
	Handlebars.registerPartial("uppertextcontent", $("#uppertextcontent-partial").html());

	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	var score = 0;
	var drop = 0;
	/*random scoreboard eggs*/
	imageArray.shufflearray();
	var wrngClicked = [false, false, false, false, false, false, false, false, false, false];
	var eggtemplatecontroller = new EggTemplate();

	eggtemplatecontroller.init(10);

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[0]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);
		$('.congratulation').hide(0);
		$('.exefin').hide(0);

		/*generate question no at the beginning of question*/
		switch(countNext) {
			case 0:
      sound_player("exer");
                $("label").hide();
                $(".draggable").hover(function(){
                	$(this).find("label").show();
				},function(){
                    $(this).find("label").hide();
                });
				// $(".chocolate").hover(function(){
				// 	$(this).parent().find("label").show();
				// }, function(){
                 //    $(this).parent().find("label").hide();
                //
                // });
                //
				// $(".mobile").hover(function(){
				// 	$(".show2").addClass("show-on-hover");
				// }, function(){
				// 	$(".show2").removeClass("show-on-hover");
				// });
                //
				// $(".computer").hover(function(){
				// 	$(".show3").addClass("show-on-hover");
				// }, function(){
				// 	$(".show3").removeClass("show-on-hover");
				// });
                //
				// $(".spill-water").hover(function(){
				// 	$(".show4").addClass("show-on-hover");
				// }, function(){
				// 	$(".show4").removeClass("show-on-hover");
				// });
                //
				// $(".rain").hover(function(){
				// 	$(".show5").addClass("show-on-hover");
				// }, function(){
				// 	$(".show5").removeClass("show-on-hover");
				// });
                //
				// $(".tea").hover(function(){
				// 	$(".show6").addClass("show-on-hover");
				// }, function(){
				// 	$(".show6").removeClass("show-on-hover");
				// });
                //
				// $(".juice").hover(function(){
				// 	$(".show7").addClass("show-on-hover");
				// }, function(){
				// 	$(".show7").removeClass("show-on-hover");
				// });
                //
				// $(".smoke").hover(function(){
				// 	$(".show8").addClass("show-on-hover");
				// }, function(){
				// 	$(".show8").removeClass("show-on-hover");
				// });
                //
				// $(".balloon").hover(function(){
				// 	$(".show9").addClass("show-on-hover");
				// }, function(){
				// 	$(".show9").removeClass("show-on-hover");
				// });
                //
				// $(".steam").hover(function(){
				// 	$(".show10").addClass("show-on-hover");
				// }, function(){
				// 	$(".show10").removeClass("show-on-hover");
				// });


				$(".draggable").draggable({
					containment : "body",
					revert : "invalid",
					appendTo : "body",
					helper : "clone",
					zindex : 1000,
					start: function(event, ui){
        		        $(ui.helper).addClass("disableanimation");
        		        $(".showtext").removeClass("show-on-hover");
						$(this).css({"opacity": "0.5"});
						$(ui.helper).addClass("ui-draggable-helper");
						$(ui.helper).removeClass("sliding");
					},
					stop: function(event, ui){
		                $(ui.helper).removeClass("disableanimation");
						$(this).css({"opacity": "1"});
					}
				});

        $('.drop_class_1').droppable({
					hoverClass : "hovered",
					drop:function(event, ui) {
						if (ui.draggable.hasClass("class_1")){
							if(wrngClicked[countNext] == false){
								if(($(ui.draggable).data("dropped"))==false){
									// $("#egg" + countNext).attr("src", "images/eggs/" + imageArray[0] +".png").removeClass('eggmove').attr("select","yes");
									// $(".exefin").append("<img class='eggs' src = 'images/eggs/" + imageArray[0] + ".png'> </img>");
									eggtemplatecontroller.update(true);
                  // $nextBtn.show(0);
                  // eggtemplatecontroller.gotoNext();
								}
							}else{
                 if(($(ui.draggable).data("dropped"))==false){
                      // countNext++;
                      $(ui.draggable).data("dropped",true);
                      // eggtemplatecontroller.gotoNext();
                      // $('#egg' + countNext).addClass('eggmove');
                  }
              }
							drop++;
              gotoscorepage(drop);
							// console.log(drop);
							handleCardDrop(event, ui, ".class_1" , ".drop_class_1");
						}else{
              play_correct_incorrect_sound(0);
							if(($(ui.draggable).data("dropped"))==false){
								wrngClicked[countNext] = true;
								eggtemplatecontroller.update(false);
							}
						}

            if(($(ui.draggable).data("dropped"))==false){
                countNext++;
                $(ui.draggable).data("dropped",true);
          	}
					}
				});

				$('.drop_class_2').droppable({
					hoverClass : "hovered",
					drop:function(event, ui) {
						if (ui.draggable.hasClass("class_2")){
							if(wrngClicked[countNext] == false){
								if(($(ui.draggable).data("dropped"))==false){
									// $("#egg" + countNext).attr("src", "images/eggs/" + imageArray[0] +".png").removeClass('eggmove').attr("select","yes");
									// $(".exefin").append("<img class='eggs' src = 'images/eggs/" + imageArray[0] + ".png'> </img>");
									eggtemplatecontroller.update(true);
                  // eggtemplatecontroller.gotoNext();
								}else{
                  // $nextBtn.show(0);
                  if(($(ui.draggable).data("dropped"))==false){
      			                // countNext++;
      			                $(ui.draggable).data("dropped",true);
      			              	// $('#egg' + countNext).addClass('eggmove');
                            // eggtemplatecontroller.gotoNext();
      			          	}
                }
							}
							drop++;
              gotoscorepage(drop);
							console.log(drop);
							handleCardDrop(event, ui, ".class_2" , ".drop_class_2");
						}else{
              play_correct_incorrect_sound(0);
							if(($(ui.draggable).data("dropped"))==false){

								wrngClicked[countNext] = true;
								// $("#egg"+countNext).attr("src", "images/eggs/egg_wrong.png").removeClass('eggmove');
								eggtemplatecontroller.update(false);
							}
						}

            if(($(ui.draggable).data("dropped"))==false){
                countNext++;
                $(ui.draggable).data("dropped",true);
          	}
					}
				});

        $('.drop_class_3').droppable({
					hoverClass : "hovered",
					drop:function(event, ui) {
						if (ui.draggable.hasClass("class_3")){
							if(wrngClicked[countNext] == false){
								if(($(ui.draggable).data("dropped"))==false){
									// $("#egg" + countNext).attr("src", "images/eggs/" + imageArray[0] +".png").removeClass('eggmove').attr("select","yes");
									// $(".exefin").append("<img class='eggs' src = 'images/eggs/" + imageArray[0] + ".png'> </img>");
									eggtemplatecontroller.update(true);
                  // eggtemplatecontroller.gotoNext();
                  // $nextBtn.show(0);
								}
							}else{

                if(($(ui.draggable).data("dropped"))==false){
                          // countNext++;
                          $(ui.draggable).data("dropped",true);
                          // $('#egg' + countNext).addClass('eggmove');
                          // eggtemplatecontroller.gotoNext();
                      }
              }
							drop++;
              gotoscorepage(drop);
							console.log(drop);
							handleCardDrop(event, ui, ".class_3" , ".drop_class_3");
						}else{
              play_correct_incorrect_sound(0);
							if(($(ui.draggable).data("dropped"))==false){

								wrngClicked[countNext] = true;
								// $("#egg"+countNext).attr("src", "images/eggs/egg_wrong.png").removeClass('eggmove');
								eggtemplatecontroller.update(false);
							}
						}

            if(($(ui.draggable).data("dropped"))==false){
                countNext++;
                $(ui.draggable).data("dropped",true);
          	}
					}
				});

				function handleCardDrop(event, ui, classname, droppedOn) {
					ui.draggable.draggable('disable');
					var dropped = ui.draggable;
					// var to count no. of divs in the droppable div
					var drop_index = $(droppedOn+">div").length;
					var top_position = 10*drop_index;
					var left_position = 18*drop_index;
					$(ui.draggable).removeClass("sliding");
          createjs.Sound.stop();
          play_correct_incorrect_sound(1);
					$(ui.draggable).detach().css({
						"cursor": 'pointer',
						"width": "20%",
						"max-height": "45%",
						"flex": "0 0 40%"
					}).appendTo(droppedOn);
					var $newEntry = $(".draggableblock> .hidden").eq(0);

					var $draggable3;
					var $draggable2;
					var $draggable1;
					if(dropped.hasClass("position_4")){
						dropped.removeClass("position_4");
						$draggable3 = $(".position_3");
						$draggable2 = $(".position_2");
						$draggable1 = $(".position_1");
					}else if(dropped.hasClass("position_3")){
						dropped.removeClass("position_3");
						$draggable2 = $(".position_2");
						$draggable1 = $(".position_1");
					}else if(dropped.hasClass("position_2")){
						dropped.removeClass("position_2");
						$draggable1 = $(".position_1");
					}else if(dropped.hasClass("position_1")){
						dropped.removeClass("position_1");
					}

					if($draggable3 != null){
						 $draggable3.removeClass("position_3").addClass("position_4");
						 $draggable3.removeClass('sliding');
						 setTimeout(function() {
							    $draggable3.addClass('sliding');
						},1);
					}
					if($draggable2 != null){
						 $draggable2.removeClass("position_2").addClass("position_3");
						 $draggable2.removeClass('sliding');
						 setTimeout(function() {
							    $draggable2.addClass('sliding');
						},1);
					}
					if($draggable1 != null){
						 $draggable1.removeClass("position_1").addClass("position_2");
						 $draggable1.removeClass('sliding');
						 setTimeout(function() {
							    $draggable1.addClass('sliding');
						},1);
					}
					if($newEntry != null){
						 $newEntry.removeClass("hidden").addClass("position_1");
					}
					if(drop == 10) {
					}
				}
			break;
		}
	}

  function gotoscorepage(drop){
        if(drop==10){
            $nextBtn.show(0);
            $nextBtn.click(function(){
                eggtemplatecontroller.gotoNext();
            });
        }
        else{
            eggtemplatecontroller.gotoNext();
        }
  }

  function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

  $nextBtn.on("click", function(){
		countNext++;
		if(drop == 10) {
			$('#score').html(score);
			$('[select=yes]').fadeTo(1000,0).hide(0);
			$('.exefin').show(0);
			$('.contentblock').hide(0);
			$('.congratulation').show(0);
			$('.exenextbtn').show(0);
			$nextBtn.hide(0);
		}
	});

	// $refreshBtn.click(function(){
	// 	templateCaller();
	// });

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

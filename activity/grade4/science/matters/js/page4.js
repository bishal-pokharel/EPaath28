var imgpath = $ref+"/images/page4/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

content = [

  //1st slide
  {
    uppertextblock: [{
      textdata: data.string.rev,
      textclass: "text"
    }],

    imageblock: [{
      mainimage: [{
        imgsrc: "images/lokharke/important_content.svg",
        imgclass: "lokharke"
      }]
    }]  
  }, 

  //2nd slide
  {
    definitionblock: [
      {
        definitiondata: data.string.rev2,
        definitionclass: "normal-text rev1"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "rock.png",
            imgclass: "solidimg"
          }
        ]
      }
    ]
  },
  
  //3rd slide
  {
    definitionblock: [
      {
        definitiondata: data.string.rev2,
        definitionclass: "normal-text rev1"
      },
      {
        definitiondata: data.string.rev3,
        definitionclass: "normal-text rev3"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "rock.png",
            imgclass: "solidimg"
          },
          {
            imgsrc: imgpath + "liquid-have-different-shapes.png",
            imgclass: "liquidimg"
          }
        ]
      }
    ]
  },

  //4th slide
  {
    definitionblock: [
      {
        definitiondata: data.string.rev2,
        definitionclass: "normal-text rev1"
      },
      {
        definitiondata: data.string.rev3,
        definitionclass: "normal-text rev3"
      },
      {
        definitiondata: data.string.rev4,
        definitionclass: "normal-text rev4"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "rock.png",
            imgclass: "solidimg"
          },
          {
            imgsrc: imgpath + "liquid-have-different-shapes.png",
            imgclass: "liquidimg"
          },
          {
            imgsrc: imgpath + "gas.png",
            imgclass: "gasimg"
          }
        ]
      }
    ]
  },

];



$(function() {
    $(window).resize(function() {
        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   
    });

    var $board = $(".board");
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var total_page = 0;
    var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
    var $total_page = content.length;
    loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			// {id: "cover", src: imgpath+"cover_simpleinterest.png", type: createjs.AbstractLoader.IMAGE},
			
			// sounds
			{id: "sound_0", src: soundAsset+"p4_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p4_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p4_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p4_s3.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
    
    
    /*
      inorder to use the handlebar partials we need to register them
      to their respective handlebar partial pointer first
    */
    Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    
    // Handlebars.registerPartial("tablecontent", $("#tablecontent-partial").html());

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationController(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/


    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        var requestAnimationFrame = window.requestAnimationFrame ; 
        
        loadTimelineProgress($total_page,countNext+1);
        $board.html(html);        

       vocabcontroller.findwords(countNext);

        switch (countNext) {
          case 0:
          sound_player('sound_'+countNext);
          break;
          case 1:
          sound_player('sound_'+countNext);
          break;
          case 2:
          sound_player('sound_'+countNext);
          break;
          case 3:
          sound_player('sound_'+countNext);
            ole.footerNotificationHandler.pageEndSetNotification();
          break;
        }   
    };

    function templateCaller() {
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

        //navigationController();

        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   

    }
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();		
		current_sound.on('complete', function(){
			navigationController();
		});
	}

    $nextBtn.on("click", function() {
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
        countNext--;
        templateCaller();
    });

    // document.addEventListener('xmlLoad', function(e) {
        total_page = content.length;
        // templateCaller();
    // });


});
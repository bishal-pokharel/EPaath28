var imgpath = $ref+"/images/all/";
var imgpath1 = $ref+"/images/page1/";
var imgpath2 = $ref+"/images/page2/";
var imgpath3 = $ref+"/images/page3/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

content = [

  //1st slide
  {
    uppertextblock: [{
      textdata: data.string.diy,
      textclass: "text matter-head"
    }],

    imageblock: [{
      mainimage: [{
        imgsrc: "images/lokharke/2.png",
        imgclass: "lokharke"
      }]
    }]
  },

  //2nd slide
  {
    imageblockadditionalclass: "upperimageblockholder",
    contentblockadditionalclass: "simplebg",
    contentnocenteradjust: true,
    imageblockadditionalclass: "upperimageblockholder",
     uppertextblock: [
      {
        textdata: data.string.diytext1,
        textclass: "matter-head"
      }
    ],
    imageblock: [{
      imagestoshow: [{
        //1
        imgclass: "draggable soliddrag solid draggableposition1",
        imgsrc: imgpath1+ "solid.png"
      },{
        //2
        imgclass: "draggable gasdrag gas draggableposition2",
        imgsrc: imgpath3+ "gas.png"
      },{
        //3
        imgclass: "draggable liquiddrag lqd  draggableposition3",
        imgsrc: imgpath2+ "water_glass.png"
      },{
        //4
        imgclass: "draggable gasdrag gas draggableposition4",
        imgsrc: imgpath3+ "gas01.png"
      },{
        //2_1
        imgclass: "draggable soliddrag solid hide",
        imgsrc: imgpath1+ "pencil.png"
      },{
        //2_2
        imgclass: "draggable liquiddrag lqd hide",
        imgsrc: imgpath2+ "water_drop.png"
      },{
        //2_3
        imgclass: "draggable soliddrag solid hide",
        imgsrc: imgpath1+ "cube.png"
      },{
        //2_4
        imgclass: "draggable gasdrag gas hide",
        imgsrc: imgpath3+ "gas02.png"
      },{
        //3_1
        imgclass: "draggable liquiddrag lqd hide",
        imgsrc: imgpath2+ "juice.png"
      },{
        //3_2
        imgclass: "draggable soliddrag solid hide",
        imgsrc: imgpath1+ "nail.png"
      },{
        //3_3
        imgclass: "draggable liquiddrag lqd hide",
        imgsrc: imgpath2+ "milk.png"
      },{
        //3_4
        imgclass: "draggable gasdrag gas hide",
        imgsrc: imgpath3+ "smoke.png"
      }],
      imagelabels: [{
        imagelabelclass: "draganddropinst",
        imagelabeldata: data.string.p3_s2
      }]
    }],
    specialdropdiv:[{
      droplabelclass: "solidlabel",
      droplabeldata: data.string.solid,
      dropdiv: "soliddrop"
    },{
      droplabelclass: "liquidlabel",
      droplabeldata: data.string.liquid,
      dropdiv: "liquiddrop"
    },{
      droplabelclass: "gaslabel",
      droplabeldata: data.string.gas,
      dropdiv: "gasdrop"
    }]
  },

  //3rd slide
  {
    uppertextblock: [
      {
        textdata: data.string.diytext2,
        textclass: "text text1"
      }
    ],
  },

  //4th slide
  {
    uppertextblock: [
      {
        textdata: data.string.diytext2,
        textclass: "text text1"
      },
      {
        textdata: data.string.diytext3,
        textclass: "text text2"
      },
    ],
  },

  //5th slide
  {
    uppertextblock: [
      {
        textdata: data.string.diytext4,
        textclass: "text"
      }
    ],

    definitionblock: [
      {
        definitiondata: data.string.solid,
        definitionclass: "solid-text solid-text2"
      },
      {
        definitiondata: data.string.liquid,
        definitionclass: "liquid-text liquid-text2"
      },
      {
        definitiondata: data.string.gas,
        definitionclass: "gas-text gas-text2"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "arrow.png",
            imgclass: "arrow1"
          },
          {
            imgsrc: imgpath + "arrow.png",
            imgclass: "arrow2"
          },
          {
            imgsrc: imgpath + "arrow.png",
            imgclass: "arrow3"
          }
       ]
     }
    ]
  },

  //6th slide
  {
    uppertextblock: [
      {
        textdata: data.string.diytext5,
        textclass: "text"
      }
    ],

    imageblock: [
      {
        spriteimage: [
          {
            imgclass: "spriteimg1"
          },
        ]
      }
    ]
  },

  //7th slide
  {
    uppertextblock: [
      {
        textdata: data.string.diytext6,
        textclass: "text"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "vapour01.png",
            imgclass: "changesForm"
          }
        ]
      }
    ]
  },

  //8th slide
  {
    uppertextblock: [
      {
        textdata: data.string.diytext7,
        textclass: "text"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "liquid-to-solid.png",
            imgclass: "changesForm"
          }
        ]
      }
    ]
  },

  //9th slide
  {
    uppertextblock: [
      {
        textdata: data.string.diytext8,
        textclass: "text"
      }
    ],

    definitionblock: [
      {
        definitiondata: data.string.solid,
        definitionclass: "solid-text solid-text3"
      },
      {
        definitiondata: data.string.liquid,
        definitionclass: "liquid-text liquid-text3"
      },
      {
        definitiondata: data.string.gas,
        definitionclass: "gas-text gas-text3"
      },
      {
        definitiondata: data.string.heating,
        definitionclass: "normal-text heating1"
      },
      {
        definitiondata: data.string.heating,
        definitionclass: "normal-text heating2"
      },
      {
        definitiondata: data.string.cooling,
        definitionclass: "normal-text cooling1"
      },
      {
        definitiondata: data.string.cooling,
        definitionclass: "normal-text cooling2"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "orangearrow.png",
            imgclass: "arrow4"
          },
          {
            imgsrc: imgpath + "orangearrow.png",
            imgclass: "arrow5"
          },
          {
            imgsrc: imgpath + "bluearrow.png",
            imgclass: "arrow6"
          },
          {
            imgsrc: imgpath + "bluearrow.png",
            imgclass: "arrow7"
          },
          {
            imgsrc: imgpath1 + "cube.png",
            imgclass: "cube"
          },
          {
            imgsrc: imgpath2 + "water_glass.png",
            imgclass: "liquid"
          },
          {
            imgsrc: imgpath + "watervapour.png",
            imgclass: "watervapour"
          },
       ]
     }
    ]
  },

  //10th slide
  {
    uppertextblock: [
      {
        textdata: data.string.diytext9,
        textclass: "text"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "meltingchocolate.png",
            imgclass: "sol-liq-gas"
          },
          {
            imgsrc: imgpath + "icecream.png",
            imgclass: "sol-liq-gas"
          },
          {
            imgsrc: imgpath + "candle01.png",
            imgclass: "sol-liq-gas"
          }
       ]
     }
    ]
  },

  //11th slide
  {
    uppertextblock: [
      {
        textdata: data.string.diytext10,
        textclass: "text"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "vapour01.png",
            imgclass: "sol-liq-gas"
          },
          {
            imgsrc: imgpath + "vapour02.png",
            imgclass: "sol-liq-gas"
          },
          {
            imgsrc: imgpath + "vapour03.png",
            imgclass: "sol-liq-gas"
          }
       ]
     }
    ]
  },

  //12th slide
  {
    uppertextblock: [
      {
        textdata: data.string.diytext11,
        textclass: "text"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "ice_cream.png",
            imgclass: "sol-liq-gas"
          },
          {
            imgsrc: imgpath + "wax.png",
            imgclass: "sol-liq-gas"
          },
          {
            imgsrc: imgpath + "chocolate.png",
            imgclass: "sol-liq-gas"
          }
       ]
     }
    ]
  },

  //13th slide
  {
    additionaldivblocks: true,

    uppertextblock: [
      {
        textdata: data.string.diytext12,
        textclass: "text"
      }
    ],

    definitionblock: [
      {
        definitiondata: data.string.diytext5,
        definitionclass: "subtext"
      },
      {
        definitiondata: data.string.diytext6,
        definitionclass: "subtext"
      },
      {
        definitiondata: data.string.diytext7,
        definitionclass: "subtext"
      }
    ],

    imageblock: [
      {
        spriteimage: [
          {
            imgclass: "area"
          },
          {
            imgclass: "area"
          },
          {
            imgclass: "area"
          }
        ],
        normalimage: [
          {
            imgsrc: imgpath + "icecream.png",
            imgclass: "solid-items item1"
          },
          {
            imgsrc: imgpath + "meltingchocolate.png",
            imgclass: "solid-items item2"
          },
          {
            imgsrc: imgpath + "ice_cream.png",
            imgclass: "liquid-items item3"
          },
          {
            imgsrc: imgpath1 + "cube.png",
            imgclass: "liquid-items item4"
          },
          {
            imgsrc: imgpath + "vapour01.png",
            imgclass: "gas-items item5"
          },
          {
            imgsrc: imgpath + "vapour02.png",
            imgclass: "gas-items item6"
          },
        ]
      }
    ]
  },

];



$(function() {
    $(window).resize(function() {
        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

    });

    var $board = $(".board");
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var total_page = 0;
    var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
    var $total_page = content.length;
    loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			// {id: "cover", src: imgpath+"cover_simpleinterest.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0", src: soundAsset+"p5_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p5_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p5_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p5_s3.ogg"},
			{id: "sound_4", src: soundAsset+"p5_s4.ogg"},
			{id: "sound_5", src: soundAsset+"p5_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p5_s6.ogg"},
			{id: "sound_7", src: soundAsset+"p5_s7.ogg"},
			{id: "sound_8", src: soundAsset+"p5_s8.ogg"},
			{id: "sound_9", src: soundAsset+"p5_s9.ogg"},
			{id: "sound_10", src: soundAsset+"p5_s10.ogg"},
			{id: "sound_11", src: soundAsset+"p5_s11.ogg"},
      {id: "sound_12-01", src: soundAsset+"p5_s12-01.ogg"},
      {id: "sound_12-02", src: soundAsset+"p5_s12-02.ogg"},
      {id: "sound_12-03", src: soundAsset+"p5_s12-03.ogg"},
			{id: "sound_12-04", src: soundAsset+"p5_s12-04.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();


    /*
      inorder to use the handlebar partials we need to register them
      to their respective handlebar partial pointer first
    */
    Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

    // Handlebars.registerPartial("tablecontent", $("#tablecontent-partial").html());

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationController(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/


    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        var requestAnimationFrame = window.requestAnimationFrame ;
        var droppedInSolid = 0;
        var droppedInLiquid = 0;
        var droppedInGas = 0;

        loadTimelineProgress($total_page,countNext+1);
        $board.html(html);
		vocabcontroller.findwords(countNext);
        switch (countNext) {
          case 0:
          sound_player('sound_'+countNext);
            $(".matter-head").css({ color: "#00BDB3", textShadow: "1px 1px 1px #777" });
            $(".text").css({ top: "26%" });
          break;
          case 1:
          sound_player1('sound_'+countNext);
            $(".draggable").draggable({
              containment : "body",
              cursor : "grab",
              revert : "invalid",
              appendTo : "body",
              helper : "clone",
              zindex : 1000,
              start: function(event, ui){
                $(ui.helper).addClass("disableanimation");
                $(ui.helper).css({"max-width": "7%",
                          "max-height": "15%"});

              },
              stop: function(event, ui){
                $(ui.helper).removeClass("disableanimation");
                $(ui.helper).css({"max-width": "10%",
                          "max-height": "15%"});
              }
            });

            $('.soliddrop').droppable({
              // accept : ".soliddrag",
              hoverClass : "hovered",
              drop : function upondrop(event, ui){
                handleCardDrop(event, ui, "solid" , "soliddrop", ui.draggable);
              }
            });

            $('.liquiddrop').droppable({
              // accept : ".liquiddrag",
              hoverClass : "hovered",
              drop : function upondrop(event, ui){
                handleCardDrop(event, ui, "lqd" , "liquiddrop", ui.draggable);
              }
            });

            $('.gasdrop').droppable({
              // accept : ".gasdrag",
              hoverClass : "hovered",
              drop : function upondrop(event, ui){
              handleCardDrop(event, ui, "gas" , "gasdrop", ui.draggable);
            }
          });

        var divsdroppedcount = 0;
        function handleCardDrop(event, ui, classname, droppedon, dropped) {
          if(dropped.hasClass(classname)){
            play_correct_incorrect_sound(1);
            ui.draggable.draggable('disable');
            var dropped = ui.draggable;
            var droppedOn = $("."+droppedon);
            var count = 0;
            var top = 0;

            count = $("."+droppedon+"> ."+classname).length;
            top = (count > 0)? 35 + 30 * (count-1) : 5;

            $(dropped).detach().css({
              width: "30%",
              maxHeight: "40%",
              flex: "0 0 35%"
            }).removeClass('draggable').appendTo(droppedOn);

            // var $classtoshow;
            var $newEntry = $(".upperimageblockholder> .hide").eq(0);
            var $draggable3;
            var $draggable2;
            var $draggable1;
            if(dropped.hasClass("draggableposition4")){
              dropped.toggleClass("draggableposition4");
              $draggable3 = $(".draggableposition3");
              $draggable2 = $(".draggableposition2");
              $draggable1 = $(".draggableposition1");
              // $classtoshow = $(".draggableposition4").eq(0);
            }else if(dropped.hasClass("draggableposition3")){
              dropped.toggleClass("draggableposition3");
              $draggable2 = $(".draggableposition2");
              $draggable1 = $(".draggableposition1");
              // $classtoshow = $(".draggableposition3").eq(0);
            }else if(dropped.hasClass("draggableposition2")){
              dropped.toggleClass("draggableposition2");
              $draggable1 = $(".draggableposition1");
              // $classtoshow = $(".draggableposition2").eq(0);
            }else if(dropped.hasClass("draggableposition1")){
              dropped.toggleClass("draggableposition1");
              // $classtoshow = $(".draggableposition1").eq(0);
            }

            if($draggable3 != null){
               $draggable3.removeClass("draggableposition3").addClass("draggableposition4");
            }
            if($draggable2 != null){
               $draggable2.removeClass("draggableposition2").addClass("draggableposition3");
            }
            if($draggable1 != null){
               $draggable1.removeClass("draggableposition1").addClass("draggableposition2");
            }
            if($newEntry != null){
               $newEntry.removeClass("hide").addClass("draggableposition1");
            }

            divsdroppedcount++;
            console.log(divsdroppedcount);
            if(divsdroppedcount == 12){
              $nextBtn.show(0);
              $prevBtn.show(0);
            }
        }else{
          play_correct_incorrect_sound(0);
        }
        }
        break;
        case 2:
          sound_player('sound_'+countNext);
        break;
        case 3:
          sound_player('sound_'+countNext);
        break;
        case 4:
          sound_player('sound_'+countNext);
        break;
        case 5:
          sound_player('sound_'+countNext);
        break;
        case 6:
          sound_player('sound_'+countNext);
        break;
        case 7:
          sound_player('sound_'+countNext);
        break;
        case 8:
          sound_player('sound_'+countNext);
        break;
        case 9:
          sound_player('sound_'+countNext);
          $(".liquid").css({
            width: "15%",
            left: "42.5"
          })
        break;
        case 10:
          sound_player('sound_'+countNext);
        break;
        case 11:
          sound_player('sound_'+countNext);
        break;

        case 12:
          $(".subtext, .area, .solid-items, .liquid-items, .gas-items").css("opacity","0");
        		createjs.Sound.stop();
        		current_sound = createjs.Sound.play("sound_12-01");
        		current_sound.play();
        		current_sound.on('complete', function(){
                $(".subtext:eq(0), .area:eq(0),.solid-items").css("opacity","1");
            		createjs.Sound.stop();
            		current_sound = createjs.Sound.play("sound_12-02");
            		current_sound.play();
            		current_sound.on('complete', function(){
                    $(".subtext:eq(1), .area:eq(1),.gas-items").css("opacity","1");
                    // $(".subtext:eq(1)").css("margin-top","17%");
              		createjs.Sound.stop();
              		current_sound = createjs.Sound.play("sound_12-03");
              		current_sound.play();
              		current_sound.on('complete', function(){
                      $(".subtext:eq(2), .area:eq(2),.liquid-items").css("opacity","1");
                		createjs.Sound.stop();
                		current_sound = createjs.Sound.play("sound_12-04");
                		current_sound.play();
                		current_sound.on('complete', function(){
                      ole.footerNotificationHandler.lessonEndSetNotification();
                		});
              		});
            		});
        		});
          // $(".text").css({
          //   padding: "0 5%"
          // })
        break;
        }
    };
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			navigationController();
		});
	}
	function sound_player1(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

    function templateCaller() {
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

        //navigationController();

        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


    }

    $nextBtn.on("click", function() {
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
        countNext--;
        templateCaller();
    });

    // document.addEventListener('xmlLoad', function(e) {
        total_page = content.length;
        // templateCaller();
    // });


});

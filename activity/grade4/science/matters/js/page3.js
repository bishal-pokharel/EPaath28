var imgpath = $ref+"/images/page3/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

content = [

  //1st slide
  {
    additionalclasscontentblock: "opening-bg",
    uppertextblock: [
      {
        textdata: data.string.gas,
        textclass: "text matter-head textSize9"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "air.png",
            imgclass: "air"
          },
          {
            imgsrc: imgpath + "teapot.png",
            imgclass: "teapot"
          }
        ]
      }
    ]
  },

  //2nd slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.gas,
        textclass: "text"
      }
    ],

    definitionblock: [
      {
        definitiondata: data.string.gastext1,
        definitionclass: "normal-text matter-text1"
      }
    ],

    imageblock: [
      {
        spriteimage: [
          {
            imgclass: "sprite-smoke"
          },
        ]
      }
    ]
  },

  //3rd slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.gas,
        textclass: "text"
      }
    ],

    definitionblock: [
      {
        definitiondata: data.string.gastext1,
        definitionclass: "normal-text matter-text1"
      },
      {
        definitiondata: data.string.gastext2,
        definitionclass: "normal-text matter-text2"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "air.png",
            imgclass: "air"
          },
          {
            imgsrc: imgpath + "teapot.png",
            imgclass: "teapot"
          }
        ]
      }
    ]
  },

  //4th slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.gas,
        textclass: "text"
      }
    ],

    definitionblock: [
      {
        definitiondata: data.string.gastext3,
        definitionclass: "normal-text matter-text1"
      }
    ],

    imageblock: [
      {
        spriteimage: [
          {
            imgclass: "sprite-candle"
          },
          {
            imgclass: "sprite-candle-smoke"
          }
        ],
        mainimage: [
          {
            imgsrc: imgpath + "bgforgas.png",
            imgclass: "gas-bg"
          }
        ]
      }
    ]
  },

  //5th slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.gas,
        textclass: "text"
      }
    ],

    definitionblock: [
      {
        definitiondata: data.string.gastext4,
        definitionclass: "normal-text matter-text1"
      }
    ],

    imageblock: [
      {
        spriteimage: [
          {
            imgclass: "sprite-candle2"
          },
          {
            imgclass: "sprite-candle-smoke2"
          },
          {
            imgclass: "sprite-container"
          }
        ],
        mainimage: [
          {
            imgsrc: imgpath + "bgforgas.png",
            imgclass: "gas-bg"
          }
        ]
      }
    ]
  },

  //6th slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.gas,
        textclass: "text"
      }
    ],
    definitionblock: [
      {
        definitiondata: data.string.gasobject0,
        definitionclass: "normal-text object"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "balloon.png",
            imgclass: "objectimg"
          }
        ]
      }
    ]
  },

  //7th slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.gas,
        textclass: "text"
      }
    ],
    definitionblock: [
      {
        definitiondata: data.string.gasobject1,
        definitionclass: "normal-text object"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "firewood.png",
            imgclass: "objectimg"
          }
        ]
      }
    ]
  },

  //8th slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.gas,
        textclass: "text"
      }
    ],
    definitionblock: [
      {
        definitiondata: data.string.gasobject2,
        definitionclass: "normal-text object"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "bubble-in-liquids.png",
            imgclass: "objectimg"
          }
        ]
      }
    ]
  },

  //9th slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.gas,
        textclass: "text"
      }
    ],
    definitionblock: [
      {
        definitiondata: data.string.gasobject3,
        definitionclass: "normal-text object"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "gas-cylinder.png",
            imgclass: "objectimg"
          }
        ]
      }
    ]
  },

  //10th slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.gas,
        textclass: "text"
      }
    ],
    definitionblock: [
      {
        definitiondata: data.string.gasobject4,
        definitionclass: "normal-text object"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "cloud-and-vapour.png",
            imgclass: "objectimg"
          }
        ]
      }
    ]
  },

  //11th slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.gas,
        textclass: "text"
      }
    ],
    definitionblock: [
      {
        definitiondata: data.string.gasobject5,
        definitionclass: "normal-text object"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "fumes-from-colcanos.png",
            imgclass: "objectimg"
          }
        ]
      }
    ]
  },

  //12th slide
  {
    uppertextblock: [
      {
        textdata: data.string.gas,
        textclass: "text"
      }
    ],
    definitionblock: [
      {
        definitiondata: data.string.gasform1,
        definitionclass: "normal-text gasform-text1"
      },
      {
        definitiondata: data.string.gasform2,
        definitionclass: "normal-text gasform-text2"
      }
    ],

    imageblock: [
      {
        spriteimage: [
          {
            imgclass: "spriteimg1"
          },
          {
            imgclass: "spriteimg2"
          }
        ]
      }
    ]
  },

  //13th slide
  {
    uppertextblock: [
      {
        textdata: data.string.gas,
        textclass: "text"
      }
    ],
    definitionblock: [
      {
        definitiondata: data.string.gasform3,
        definitionclass: "normal-text gasform-text1"
      },
      {
        definitiondata: data.string.gasform4,
        definitionclass: "normal-text gasform-text2"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "smoke.png",
            imgclass: "objectimg objectimg1"
          },
          {
            imgsrc: imgpath + "tyre.png",
            imgclass: "objectimg objectimg2"
          }
        ]
      }
    ]
  },

  //14th slide
  {
    uppertextblock: [
      {
        textdata: data.string.gas,
        textclass: "text"
      }
    ],
    definitionblock: [
      {
        definitiondata: data.string.gasfeature,
        definitionclass: "normal-text gasfeature"
      }
    ],



    imageblock: [
      {
        fleximage: [
          {
            imgsrc: imgpath + "smoke.png",
          },
          {
            imgsrc: imgpath + "balloon.png",
          },
          {
            imgsrc: imgpath + "bubble-in-liquids.png",
          },
          {
            imgsrc: imgpath + "gas01.png",
          },
          {
            imgsrc: imgpath + "gas02.png",
          },
          {
            imgsrc: imgpath + "fumes-from-colcanos.png",
          },
          {
            imgsrc: imgpath + "firewood.png",
          },
          {
            imgsrc: imgpath + "cloud-and-vapour.png",
          },
          {
            imgsrc: imgpath + "gas-cylinder.png",
          },
          {
            imgsrc: imgpath + "liquid-to-gas.png",
          }
        ]
      }
    ]
  },

  //15th slide
  {
    uppertextblock: [
      {
        textdata: data.string.gasthink,
        textclass: "text matter-head"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "thinking.png",
            imgclass: "thinking"
          }
        ],
        fleximgaddnalclass:"flexImgSec",
        fleximage: [
          {
            imgsrc: imgpath + "smoke.png",
          },
          {
            imgsrc: imgpath + "balloon.png",
          },
          {
            imgsrc: imgpath + "bubble-in-liquids.png",
          },
          {
            imgsrc: imgpath + "gas01.png",
          },
          {
            imgsrc: imgpath + "gas02.png",
          },
          {
            imgsrc: imgpath + "fumes-from-colcanos.png",
          },
          {
            imgsrc: imgpath + "firewood.png",
          },
          {
            imgsrc: imgpath + "cloud-and-vapour.png",
          },
          {
            imgsrc: imgpath + "gas-cylinder.png",
          },
          {
            imgsrc: imgpath + "liquid-to-gas.png",
          }
        ]
      }
    ]
  }
];



$(function() {
    $(window).resize(function() {
        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

    });

    var $board = $(".board");
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var total_page = 0;
    var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
    var $total_page = content.length;
    loadTimelineProgress($total_page,countNext+1);
	var preload;
	var timeoutvar = null;
	var current_sound;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			// {id: "cover", src: imgpath+"cover_simpleinterest.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0", src: soundAsset+"p3_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p3_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p3_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p3_s3.ogg"},
			{id: "sound_4", src: soundAsset+"p3_s4.ogg"},
			{id: "sound_5", src: soundAsset+"p3_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p3_s6.ogg"},
			{id: "sound_7", src: soundAsset+"p3_s7.ogg"},
			{id: "sound_8", src: soundAsset+"p3_s8.ogg"},
			{id: "sound_9", src: soundAsset+"p3_s9.ogg"},
			{id: "sound_10", src: soundAsset+"p3_s10.ogg"},
      {id: "sound_11", src: soundAsset+"p3_s11.ogg"},
			{id: "sound_11_1", src: soundAsset+"p3_s11_1.ogg"},
			{id: "sound_12", src: soundAsset+"p3_s12.ogg"},
			{id: "sound_12_1", src: soundAsset+"p3_s12_1.ogg"},
			{id: "sound_13", src: soundAsset+"p3_s13.ogg"},
			{id: "sound_14", src: soundAsset+"p3_s14.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();


    /*
      inorder to use the handlebar partials we need to register them
      to their respective handlebar partial pointer first
    */
    Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

    // Handlebars.registerPartial("tablecontent", $("#tablecontent-partial").html());

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationController(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/


    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        var requestAnimationFrame = window.requestAnimationFrame ;

        loadTimelineProgress($total_page,countNext+1);
        $board.html(html);

		vocabcontroller.findwords(countNext);

        switch (countNext) {
        	case 0:
        		sound_player('sound_'+countNext);
        	break;
          case 2:
              setTimeout(function(){
                  sound_player('sound_'+countNext);
              },1500);
            $(".matter-text1").animate({
              animation: "none", opacity: "1"
            });
            $('.board .text').css({
              color: "#FFE4A0"
            });

          break;
          case 1:
          case 3:
          case 4:
          case 6:
          case 7:
          case 8:
          case 9:
          case 10:
              setTimeout(function(){
                  sound_player('sound_'+countNext);
              },1500);
            $('.board .text').css({
              color: "#FFE4A0"
            });
          break;
          case 5:
              setTimeout(function(){
                  sound_player('sound_'+countNext);
              },1500);
            $(".objectimg").css({
              width: "38%",
              top:"33%"
            });
            $('.board .text').css({
              color: "#FFE4A0"
            });
          break;
          case 11:
            setTimeout(function(){
                createjs.Sound.stop();
                current_sound = createjs.Sound.play('sound_'+countNext);
                current_sound.play();
                current_sound.on('complete', function(){
                  navigationController();
                });
            },1500);

            $('.board .text').css({
              color: "white", width: "18%", background: "#F1898A",
              padding: "0.3%", borderRadius: "10px",
              left: "0", right: "0", margin: "0 auto"
            });
            break;
        case 12:
            setTimeout(function(){
                createjs.Sound.stop();
                current_sound = createjs.Sound.play('sound_'+countNext);
                current_sound.play();
                current_sound.on('complete', function(){
                    createjs.Sound.stop();
                    current_sound = createjs.Sound.play('sound_12_1');
                    current_sound.play();
                    current_sound.on('complete', function(){
                        navigationController();
                    });
                });
            },1500);
           $(".spriteimg1").css({
              width: "20%",
              height: "25%",
              top: "25%",
              left: "0%",
              opacity: "0",
              transition: "1s"
            }).delay(1000).animate({
              left: "7%",
              opacity: "1"
            });
            $(".gasform-text2").css({
              animationDelay: "5s"
            });
            $('.board .text').css({
              color: "white", width: "18%", background: "#F1898A",
              padding: "0.3%", borderRadius: "10px",
              left: "0", right: "0", margin: "0 auto"
            });
          break;
          case 13:
              setTimeout(function(){
                  sound_player('sound_'+countNext);
              },1500);            $(".matter-head").animate({
              animation: "none", opacity: "1"
            });
            $(".objectimg").css({width: "12%"});
            $('.board .text').css({
              color: "white", width: "18%", background: "#F1898A",
              padding: "0.3%", borderRadius: "10px",
              left: "0", right: "0", margin: "0 auto"
            });
            $(".gasfeature").css({padding: "0 5%"});
          break;
          case 14:
            $(".flexImgSec").removeClass("flex");
            $(".matter-head").animate({
              animation: "none", opacity: "1"
            });
            $(".flex").css({marginTop: "13%"});
            $(".matter-head").css({top: "5%"});
          		createjs.Sound.stop();
          		current_sound = createjs.Sound.play('sound_'+countNext);
          		current_sound.play();
          		current_sound.on('complete', function(){
                ole.footerNotificationHandler.pageEndSetNotification();
          		});
          break;
        }
    };

    function templateCaller() {
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

        //navigationController();

        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


    }
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			navigationController();
		});
	}

    $nextBtn.on("click", function() {
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
        countNext--;
        templateCaller();
    });

    // document.addEventListener('xmlLoad', function(e) {
        total_page = content.length;
        // templateCaller();
    // });


});

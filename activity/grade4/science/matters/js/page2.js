var imgpath = $ref+"/images/page2/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

content = [

  //1st slide
  {
  	additionalclasscontentblock: "opening-bg",
    uppertextblock: [
      {
        textdata: data.string.liquid,
        textclass: "text matter-head fntsz9"
      }
    ],
    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "liquid-have-different-shapes.png",
            imgclass: "fstImg"
          }
        ]
      }
    ]
  },

  //2nd slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.liquid,
        textclass: "text"
      }
    ],

    definitionblock: [
      {
        definitiondata: data.string.liquidtext1,
        definitionclass: "normal-text matter-text1"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "spilled-water.png",
            imgclass: "spilled-water"
          }
        ]
      }
    ]
  },

  //3rd slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.liquid,
        textclass: "text"
      }
    ],

    definitionblock: [
      {
        definitiondata: data.string.liquidtext1,
        definitionclass: "normal-text matter-text1"
      },
      {
        definitiondata: data.string.liquidtext2,
        definitionclass: "normal-text matter-text2"
      }
    ],

    imageblock: [
      {
        spriteimage: [
          {
            imgclass: "spriteimg1"
          }
        ]
      }
    ]
  },

  //4th slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.liquid,
        textclass: "text"
      }
    ],

    definitionblock: [
      {
        definitiondata: data.string.liquidtext3,
        definitionclass: "normal-text matter-text1"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "liquid-have-different-shapes.png",
            imgclass: "diff-shapes"
          }
        ]
      }
    ]
  },

  //5th slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.liquid,
        textclass: "text"
      }
    ],

    definitionblock: [
      {
        definitiondata: data.string.liquidtext4,
        definitionclass: "normal-text matter-text1"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "liquid-have-different-shapes.png",
            imgclass: "diff-shapes"
          }
        ]
      }
    ]
  },

  //6th slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.liquid,
        textclass: "text"
      }
    ],
    definitionblock: [
      {
        definitiondata: data.string.liquidobject0,
        definitionclass: "normal-text object"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "waterinjug.jpg",
            imgclass: "objectimg"
          }
        ]
      }
    ]
  },

  //7th slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.liquid,
        textclass: "text"
      }
    ],
    definitionblock: [
      {
        definitiondata: data.string.liquidobject1,
        definitionclass: "normal-text object"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "milk.jpg",
            imgclass: "objectimg"
          }
        ]
      }
    ]
  },

  //8th slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.liquid,
        textclass: "text"
      }
    ],
    definitionblock: [
      {
        definitiondata: data.string.liquidobject2,
        definitionclass: "normal-text object"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "juice.jpg",
            imgclass: "objectimg"
          }
        ]
      }
    ]
  },

  //9th slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.liquid,
        textclass: "text"
      }
    ],
    definitionblock: [
      {
        definitiondata: data.string.liquidobject3,
        definitionclass: "normal-text object"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "oil-bottle.jpg",
            imgclass: "objectimg"
          }
        ]
      }
    ]
  },

  //10th slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.liquid,
        textclass: "text"
      }
    ],
    definitionblock: [
      {
        definitiondata: data.string.liquidobject4,
        definitionclass: "normal-text object"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "kerosine.png",
            imgclass: "objectimg"
          }
        ]
      }
    ]
  },

  //11th slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.liquid,
        textclass: "text"
      }
    ],
    definitionblock: [
      {
        definitiondata: data.string.liquidobject5,
        definitionclass: "normal-text object"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "glue.png",
            imgclass: "objectimg"
          }
        ]
      }
    ]
  },

  //12th slide
  {
    uppertextblock: [
      {
        textdata: data.string.liquid,
        textclass: "text"
      }
    ],
    definitionblock: [
      {
        definitiondata: data.string.liquidform1,
        definitionclass: "normal-text liquidform-text1"
      },
      {
        definitiondata: data.string.liquidform2,
        definitionclass: "normal-text liquidform-text2"
      }
    ],

    imageblock: [
      {
      	spriteimage: [
          {
            imgclass: "spriteimg2"
          },
          {
            imgclass: "spriteimg1"
          }
        ]
      }
    ]
  },

  //13th slide
  {
    uppertextblock: [
      {
        textdata: data.string.liquid,
        textclass: "text"
      }
    ],
    definitionblock: [
      {
        definitiondata: data.string.liquidform3,
        definitionclass: "normal-text liquidform-text1"
      },
      {
        definitiondata: data.string.liquidform4,
        definitionclass: "normal-text liquidform-text2"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "large-container.png",
            imgclass: "objectimg objectimg1"
          },
          {
            imgsrc: imgpath + "small-container.png",
            imgclass: "objectimg objectimg2"
          }
        ]
      }
    ]
  },

  //14th slide
  {
    uppertextblock: [
      {
        textdata: data.string.liquid,
        textclass: "text"
      }
    ],
    definitionblock: [
      {
        definitiondata: data.string.liquidfeature,
        definitionclass: "normal-text liquidfeature"
      }
    ],

    imageblock: [
      {
        fleximage: [
          {
            imgsrc: imgpath + "juice.jpg",
          },
          {
            imgsrc: imgpath + "glue.png",
          },
          {
            imgsrc: imgpath + "water_glass.png",
          },
          {
            imgsrc: imgpath + "water_drop.png",
          },
          {
            imgsrc: imgpath + "milk.jpg",
          },
          {
            imgsrc: imgpath + "oil-bottle.jpg",
          },
          {
            imgsrc: imgpath + "spilled-water.png",
          },
          {
            imgsrc: imgpath + "waterinjug.jpg",
          },
          {
            imgsrc: imgpath + "large-container.png",
          },
          {
            imgsrc: imgpath + "kerosine.png",
          }
        ]
      }
    ]
  },

  //15th slide
  {
    uppertextblock: [
      {
        textdata: data.string.liquidthink,
        textclass: "text matter-head"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath + "thinking.png",
            imgclass: "thinking"
          }
        ],
        fleximage: [
          {
            imgsrc: imgpath + "fevicol.jpg",
          },
          {
            imgsrc: imgpath + "honey.jpg",
          },
          {
            imgsrc: imgpath + "glue.png",
          },
          {
            imgsrc: imgpath + "inkpot.jpg",
          },
          {
            imgsrc: imgpath + "dettol.jpg",
          },
          {
            imgsrc: imgpath + "tea.jpg",
          },
          {
            imgsrc: imgpath + "soup.jpg",
          },
          {
            imgsrc: imgpath + "nailpolish.jpg",
          },
          {
            imgsrc: imgpath + "shampoo.jpg",
          },
          {
            imgsrc: imgpath + "eye-drop-bottle.png",
          }
        ]
      }
    ]
  }
];



$(function() {
    $(window).resize(function() {
        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

    });

    var $board = $(".board");
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var total_page = 0;
    var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
    var $total_page = content.length;
    loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			// {id: "cover", src: imgpath+"cover_simpleinterest.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0", src: soundAsset+"p2_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p2_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p2_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p2_s3.ogg"},
			{id: "sound_4", src: soundAsset+"p2_s4.ogg"},
			{id: "sound_5", src: soundAsset+"p2_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p2_s6.ogg"},
			{id: "sound_7", src: soundAsset+"p2_s7.ogg"},
			{id: "sound_8", src: soundAsset+"p2_s8.ogg"},
			{id: "sound_9", src: soundAsset+"p2_s9.ogg"},
			{id: "sound_10", src: soundAsset+"p2_s10.ogg"},
      {id: "sound_11", src: soundAsset+"p2_s11.ogg"},
			{id: "sound_11_1", src: soundAsset+"p2_s11_1.ogg"},
			{id: "sound_12", src: soundAsset+"p2_s12.ogg"},
			{id: "sound_13", src: soundAsset+"p2_s13.ogg"},
			{id: "sound_14", src: soundAsset+"p2_s14.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();


    /*
      inorder to use the handlebar partials we need to register them
      to their respective handlebar partial pointer first
    */
    Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

    // Handlebars.registerPartial("tablecontent", $("#tablecontent-partial").html());

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationController(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/


    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        var requestAnimationFrame = window.requestAnimationFrame ;

        loadTimelineProgress($total_page,countNext+1);
        $board.html(html);

       vocabcontroller.findwords(countNext);

        switch (countNext) {
    	  case 0:
          	sound_player('sound_'+countNext);
    	  break;
          case 1:
              setTimeout(function(){
                  sound_player('sound_'+countNext);
              },1500);
            $('.board .text').css({
              color: "#FFE4A0"
            });
          break;
          case 2:
              setTimeout(function(){
                  sound_player('sound_'+countNext);
              },1500);              $(".matter-text1").animate({
              animation: "none", opacity: "1"
            });
            $('.board .text').css({
              color: "#FFE4A0"
            });
            $(".spriteimg1").css({
              left: "0",
              right: "0",
              margin: "0 auto"
            });

          break;
          case 3:
          case 4:
          case 5:
          case 6:
          case 7:
          case 8:
          case 9:
          case 10:
              setTimeout(function(){
                  sound_player('sound_'+countNext);
              },1500);
              $('.board .text').css({
              color: "#FFE4A0"
            });
          break;
          case 11:
            setTimeout(function(){
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("sound_11");
                current_sound.play();
                current_sound.on('complete', function(){
                    createjs.Sound.stop();
                    current_sound = createjs.Sound.play("sound_11_1");
                    current_sound.play();
                    current_sound.on('complete', function(){
                        navigationController();
                    });
                });
            },1500)

          	$(".spriteimg2").css({
          		top: "30%",
          		left: "0%",
          		opacity: "0"
          	}).delay(1000).animate({
          		left: "6%",
          		opacity: "1"
          	});

            $(".spriteimg1").css({
              width: "25%",
              height: "30%",
              top: "60%",
              right: "3%",
              opacity: "0",
              animationDelay: "2s"
            }).delay(3300).animate({
              right: "10%",
              opacity:"1"
            });

            $('.board .text').css({
              width: "18%",
              color: "#30C1B9",
              padding: "0.3%", borderRadius: "10px",
              left: "0", right: "0", margin: "0 auto"
            });
          break;
          case 12:
              setTimeout(function(){
                  sound_player('sound_'+countNext);
              },1500);             $('.board .text').css({
              color: "white", width: "18%", background: "#30C1B9",
              padding: "0.3%", borderRadius: "10px",
              left: "0", right: "0", margin: "0 auto"
            });
          break;
          case 13:
              setTimeout(function(){
                  sound_player('sound_'+countNext);
              },1500);             $(".text").animate({
              animation: "none", opacity: "1"
            });
            $(".objectimg").css({width: "12%"});
            $('.board .text').css({
              color: "white", width: "18%", background: "#30C1B9",
              padding: "0.3%", borderRadius: "10px",
              left: "0", right: "0", margin: "0 auto"
            });
            $(".liquidfeature").css({
              padding: "0 5%"
            });
          break;
          case 14:
              setTimeout(function(){
                  sound_player('sound_'+countNext);
              },1500);           	$(".matter-head").animate({
              animation: "none", opacity: "1"
            });
            $(".flex").css({marginTop: "13%"});
            $(".matter-head").css({top: "5%"});
            ole.footerNotificationHandler.pageEndSetNotification();
          break;
        }
    };

    function templateCaller() {
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

        //navigationController();

        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


    }
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			navigationController();
		});
	}
    $nextBtn.on("click", function() {
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
        countNext--;
        templateCaller();
    });

    // document.addEventListener('xmlLoad', function(e) {
        total_page = content.length;
        templateCaller();
    // });


});

var imgpath1 = $ref+"/images/page1/";
var imgpath2 = $ref+"/images/page2/";
var imgpath3 = $ref+"/images/page3/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

content = [

    {
        additionalclasscontentblock: 'opening-bg',
        uppertextblock: [
            {
                textdata: data.lesson.chapter,
                textclass: 'chapter-title'
            }
        ]
    },

  //1st slide
  {
    uppertextblock: [
      {
        textdata: data.string.text1,
        textclass: 'text text1'
      }
    ],

    imageblock: [
      {
        mainimage: [
          {
            imgsrc: imgpath3 + "balloon.png",
            imgclass: "obj img1"
          },
          {
            imgsrc: imgpath2 + "water_glass.png",
            imgclass: "obj img2"
          },
          {
            imgsrc: imgpath1 + "nail.png",
            imgclass: "obj img3"
          },
          {
            imgsrc: imgpath3 + "gas02.png",
            imgclass: "obj img4"
          },
          {
            imgsrc: imgpath3 + "gas.png",
            imgclass: "obj img5"
          },
          {
            imgsrc: imgpath3 + "gas01.png",
            imgclass: "obj img6"
          },
          {
            imgsrc: imgpath2 + "water_drop.png",
            imgclass: "obj img7"
          },
          {
            imgsrc: imgpath1 + "tyre.png",
            imgclass: "obj img8"
          },
          {
            imgsrc: imgpath2 + "glue.png",
            imgclass: "obj img9"
          },
          {
            imgsrc: imgpath1 + "spoon.png",
            imgclass: "obj img10"
          }
        ]
      }
    ]
  },

  //2st slide
  {
    uppertextblock: [
      {
        textdata: data.string.text1,
        textclass: 'text text1'
      },
      {
        textdata: data.string.text2,
        textclass: 'text text2'
      }
    ],

    imageblock: [
      {
        mainimage: [
          {
            imgsrc: imgpath3 + "balloon.png",
            imgclass: "obj img1"
          },
          {
            imgsrc: imgpath2 + "water_glass.png",
            imgclass: "obj img2"
          },
          {
            imgsrc: imgpath1 + "nail.png",
            imgclass: "obj img3"
          },
          {
            imgsrc: imgpath3 + "gas02.png",
            imgclass: "obj img4"
          },
          {
            imgsrc: imgpath3 + "gas.png",
            imgclass: "obj img5"
          },
          {
            imgsrc: imgpath3 + "gas01.png",
            imgclass: "obj img6"
          },
          {
            imgsrc: imgpath2 + "water_drop.png",
            imgclass: "obj img7"
          },
          {
            imgsrc: imgpath1 + "tyre.png",
            imgclass: "obj img8"
          },
          {
            imgsrc: imgpath2 + "glue.png",
            imgclass: "obj img9"
          },
          {
            imgsrc: imgpath1 + "spoon.png",
            imgclass: "obj img10"
          }
        ]
      }
    ]
  },

  //3rd slide
  {
    uppertextblock: [
      {
        textdata: data.string.text3,
        textclass: "text text3"
      },
      {
        textdata: data.string.solid,
        textclass: "text solid"
      },
      {
        textdata: data.string.liquid,
        textclass: "text liquid"
      },
      {
        textdata: data.string.gas,
        textclass: "text gas"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath1 + "solid.png",
            imgclass: "img-types solidimg"
          },
          {
            imgsrc: imgpath1 + "water_drop.png",
            imgclass: "img-types liquidimg"
          },
          {
            imgsrc: imgpath1 + "gas.png",
            imgclass: "img-types gasimg"
          }
        ]
      }
    ]
  },

  //4th slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.solid,
        textclass: "text matter-head"
      }
    ],

    definitionblock: [
      {
        definitiondata: data.string.solidtext1,
        definitionclass: "normal-text matter-text1"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath1 + "solid.png",
            imgclass: "matterimg"
          }
        ]
      }
    ]
  },

  //5th slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.solid,
        textclass: "text matter-head"
      }
    ],

    definitionblock: [
      {
        definitiondata: data.string.solidtext1,
        definitionclass: "normal-text matter-text1"
      },
      {
        definitiondata: data.string.solidtext2,
        definitionclass: "normal-text matter-text2"
      }
    ],

    imageblock: [
      {
        spriteimage: [
          {
            imgclass: "spriteimg1"
          }
        ]
      }
    ]
  },

  //6th slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.solid,
        textclass: "text matter-head"
      }
    ],

    definitionblock: [
      {
        definitiondata: data.string.solidtext3,
        definitionclass: "normal-text matter-text1"
      }
    ],

    imageblock: [
      {
        mainimage: [
          {
            imgsrc: imgpath1 + "cube.png",
            imgclass: "imag imag1"
          },
          {
            imgsrc: imgpath1 + "book.png",
            imgclass: "imag imag2"
          },
          {
            imgsrc: imgpath1 + "chair.png",
            imgclass: "imag imag3"
          },
          {
            imgsrc: imgpath1 + "cloth.png",
            imgclass: "imag imag4"
          }
        ]
      }
    ]
  },

  //7th slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.solid,
        textclass: "text matter-head"
      }
    ],
    definitionblock: [
      {
        definitiondata: data.string.solidobject0,
        definitionclass: "normal-text object"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath1 + "rock.png",
            imgclass: "objectimg"
          }
        ]
      }
    ]
  },

  //8th slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.solid,
        textclass: "text matter-head"
      }
    ],
    definitionblock: [
      {
        definitiondata: data.string.solidobject1,
        definitionclass: "normal-text object"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath1 + "cube.png",
            imgclass: "objectimg"
          }
        ]
      }
    ]
  },

  //9th slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.solid,
        textclass: "text matter-head"
      }
    ],
    definitionblock: [
      {
        definitiondata: data.string.solidobject2,
        definitionclass: "normal-text object"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath1 + "chair.png",
            imgclass: "objectimg"
          }
        ]
      }
    ]
  },

  //10th slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.solid,
        textclass: "text matter-head"
      }
    ],
    definitionblock: [
      {
        definitiondata: data.string.solidobject3,
        definitionclass: "normal-text object"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath1 + "shirt.png",
            imgclass: "objectimg"
          }
        ]
      }
    ]
  },
  //11th slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.solid,
        textclass: "text matter-head"
      }
    ],
    definitionblock: [
      {
        definitiondata: data.string.solidobject4,
        definitionclass: "normal-text object"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath1 + "skirt.png",
            imgclass: "objectimg"
          }
        ]
      }
    ]
  },

  //12th slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.solid,
        textclass: "text matter-head"
      }
    ],
    definitionblock: [
      {
        definitiondata: data.string.solidobject5,
        definitionclass: "normal-text object"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath1 + "pencil.png",
            imgclass: "objectimg"
          }
        ]
      }
    ]
  },

  //13th slide
  {
    contentbox: true,
    uppertextblock: [
      {
        textdata: data.string.solid,
        textclass: "text matter-head"
      }
    ],
    definitionblock: [
      {
        definitiondata: data.string.solidobject6,
        definitionclass: "normal-text object"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath1 + "tyre.png",
            imgclass: "objectimg"
          }
        ]
      }
    ]
  },

  //14th slide
  {
    uppertextblock: [
      {
        textdata: data.string.solid,
        textclass: "text matter-head"
      }
    ],
    definitionblock: [
      {
        definitiondata: data.string.solidform1,
        definitionclass: "normal-text solidform-text1"
      },
      {
        definitiondata: data.string.solidform2,
        definitionclass: "normal-text solidform-text2"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath1 + "rock.png",
            imgclass: "objectimg objectimg1"
          },
          {
            imgsrc: imgpath1 + "fur.png",
            imgclass: "objectimg objectimg2"
          }
        ]
      }
    ]
  },

  //15th slide
  {
    uppertextblock: [
      {
        textdata: data.string.solid,
        textclass: "text matter-head"
      }
    ],
    definitionblock: [
      {
        definitiondata: data.string.solidform3,
        definitionclass: "normal-text solidform-text1"
      },
      {
        definitiondata: data.string.solidform4,
        definitionclass: "normal-text solidform-text2"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath1 + "helicopter.png",
            imgclass: "objectimg objectimg1"
          },
          {
            imgsrc: imgpath1 + "nail.png",
            imgclass: "objectimg objectimg2"
          }
        ]
      }
    ]
  },

  //15th slide
  {
    uppertextblock: [
      {
        textdata: data.string.solid,
        textclass: "text matter-head"
      }
    ],
    definitionblock: [
      {
        definitiondata: data.string.solidfeature,
        definitionclass: "normal-text solidfeature"
      }
    ],



    imageblock: [
      {
        fleximage: [
          {
            imgsrc: imgpath1 + "chair.png",
          },
          {
            imgsrc: imgpath1 + "cube.png",
          },
          {
            imgsrc: imgpath1 + "helicopter.png",
          },
          {
            imgsrc: imgpath1 + "shirt.png",
          },
          {
            imgsrc: imgpath1 + "marble1.png",
          },
          {
            imgsrc: imgpath1 + "pencil.png",
          },
          {
            imgsrc: imgpath1 + "spoon.png",
          },
          {
            imgsrc: imgpath1 + "marble2.png",
          },
          {
            imgsrc: imgpath1 + "tyre.png",
          },
          {
            imgsrc: imgpath1 + "rock.png",
          }
        ]
      }
    ]
  },

  //16th slide
  {
    uppertextblock: [
      {
        textdata: data.string.solidthink,
        textclass: "text matter-head"
      }
    ],

    imageblock: [
      {
        normalimage: [
          {
            imgsrc: imgpath2 + "thinking.png",
            imgclass: "thinking"
          }
        ],
        fleximage: [
          {
            imgsrc: imgpath1 + "cloth.png",
          },
          {
            imgsrc: imgpath1 + "toothbrush.png",
          },
          {
            imgsrc: imgpath1 + "basketball.png",
          },
          {
            imgsrc: imgpath1 + "stapler.png",
          },
          {
            imgsrc: imgpath1 + "marble2.png",
          },
          {
            imgsrc: imgpath1 + "book.png",
          },
          {
            imgsrc: imgpath1 + "hairclip.png",
          },
          {
            imgsrc: imgpath1 + "house.png",
          },
          {
            imgsrc: imgpath1 + "tv.png",
          },
          {
            imgsrc: imgpath1 + "xo.png",
          }
        ]
      }
    ]
  }
];



$(function() {
    $(window).resize(function() {
        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

    });

    var $board = $(".board");
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var total_page = 0;
    var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
    var $total_page = content.length;
    loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			// {id: "cover", src: imgpath+"cover_simpleinterest.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0", src: soundAsset+"p1_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p1_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p1_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p1_s3.ogg"},
      {id: "sound_4", src: soundAsset+"p1_s4.ogg"},
			{id: "sound_5", src: soundAsset+"new/p1_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p1_s6.ogg"},
			{id: "sound_7", src: soundAsset+"p1_s7.ogg"},
			{id: "sound_8", src: soundAsset+"p1_s8.ogg"},
			{id: "sound_9", src: soundAsset+"p1_s9.ogg"},
			{id: "sound_10", src: soundAsset+"p1_s10.ogg"},
			{id: "sound_11", src: soundAsset+"p1_s11.ogg"},
			{id: "sound_12", src: soundAsset+"p1_s12.ogg"},
			{id: "sound_13", src: soundAsset+"p1_s13.ogg"},
			{id: "sound_14", src: soundAsset+"p1_s14.ogg"},
			{id: "sound_15", src: soundAsset+"p1_s15.ogg"},
			{id: "sound_15_1", src: soundAsset+"p1_s15_1.ogg"},
			{id: "sound_16", src: soundAsset+"p1_s16.ogg"},
			{id: "sound_17", src: soundAsset+"p1_s17.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();



    /*
      inorder to use the handlebar partials we need to register them
      to their respective handlebar partial pointer first
    */
    Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

    // Handlebars.registerPartial("tablecontent", $("#tablecontent-partial").html());

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationController(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/


    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        var requestAnimationFrame = window.requestAnimationFrame ;

        loadTimelineProgress($total_page,countNext+1);
        $board.html(html);

       vocabcontroller.findwords(countNext);

        switch (countNext) {
          case 0:
          	sound_player('sound_0');
          break;
          case 1:
          	sound_player('sound_1');
          break;
          case 2:
            $(".obj, .text1").css({opacity: "1"}).animate({
              animation: "none"
            });
            $(".obj").css({opacity: 1});
          	sound_player('sound_2');
          break;
          case 3:
            sound_player('sound_3');
            $(".box").css({
              height: "60%",
              width: "25%",
              display: "inline-block",
              margin: "14% 1% 0",
              borderRadius: "15px"
            });

            $(".box:first-child").css({
              backgroundColor: "#7E9CD7"
            });
            $(".box:nth-child(2)").css({
              backgroundColor: "#00B7AE"
            });
            $(".box:nth-child(3)").css({
              backgroundColor: "#FF7F87"
            });
          break;
          case 4:
            sound_player('sound_4');
            $('.board .matter-head').css({
              color: "#FFE4A0"
            });
          break;
          case 5:
           	// $nextBtn.show(0);
		        // $prevBtn.show(0);
              setTimeout(function(){
                  sound_player('sound_5');
              },1500);
            $(".matter-head, .matter-text1").css({opacity: "1"}).animate({
              animation: "none"
            });
            $('.board .matter-head').css({
              color: "#FFE4A0"
            });
          break;
          case 7:
          case 8:
          case 9:
          case 10:
          case 11:
          case 12:
          case 13:
              setTimeout(function(){
                  sound_player("sound_"+countNext);
              },1500);
            $(".matter-head").css({opacity: "1"}).animate({
              animation: "none"
            });
            $('.board .matter-head').css({
              color: "#FFE4A0"
            });
          break;
          case 6:
              setTimeout(function(){
                  sound_player('sound_6');
              },1500);
            $(".matter-head").css({opacity: "1"}).animate({
              animation: "none"
            });
            $('.board .matter-head').css({
              color: "#FFE4A0"
            });
            $(".flex").css({
              marginTop : "35%",
              height: "55%"
            });
            $(".flex img").css({
              width: "25%"
            });
          break;
          case 14:
              setTimeout(function(){
                  sound_player("sound_"+countNext);
              },1500);
            $(".matter-head").animate({
              animation: "none", opacity: "1"
            });
            $('.board .matter-head').css({
              color: "white", width: "18%", background: "#7F9AD2",
              padding: "0.3%", borderRadius: "10px",
              boxShadow: "1px 1px 5px 1px #555555",
              left: "0", right: "0", margin: "0 auto"
            });
          break;
          case 15:
              setTimeout(function(){
                  sound_player("sound_15");
              },1500);
            $(".matter-head").css({ opacity: "1" }).animate({ animation: "none" });
            $(".objectimg2").css({
              top: "64%",
              width: "15%"
            });
            $('.board .matter-head').css({
              color: "white", width: "18%", background: "#7F9AD2",
              padding: "0.3%", borderRadius: "10px",
              boxShadow: "1px 1px 5px 1px #555555",
              left: "0", right: "0", margin: "0 auto"
            });
          break;
          case 16:
              setTimeout(function(){
                  sound_player("sound_"+countNext);
              },1500);
              $(".matter-head").css({ opacity: "1" }).animate({ animation: "none" });
            $(".objectimg").css({width: "12%"});
            $('.board .matter-head').css({
              color: "white", width: "18%", background: "#7F9AD2",
              padding: "0.3%", borderRadius: "10px",
              boxShadow: "1px 1px 5px 1px #555555",
              left: "0", right: "0", margin: "0 auto"
            });
          break;
          case 17:
              setTimeout(function(){
                  sound_player("sound_"+countNext);
              },1500);
            $(".flex").css({marginTop: "13%"});
            ole.footerNotificationHandler.pageEndSetNotification();
          break;
        }
    };

    function templateCaller() {
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

        // navigationController();

        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


    }

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			navigationController();
		})
	}

	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			sound_player(sound_id_2);
		});

	}

    $nextBtn.on("click", function() {
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
        countNext--;
        templateCaller();
    });

    // document.addEventListener('xmlLoad', function(e) {
        total_page = content.length;
        //templateCaller();
    // });


});

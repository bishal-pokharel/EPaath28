var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[

	//ex1
	{
		contentblockadditionalclass: 'ole-background-gradient-turqiose',
		exerciseblock: [
			{
				textdata: data.string.e2_ins,
				sentdata: data.string.sent1,
				btndata: data.string.e2_submit,
				exeoptions: [
					{
						optid: "1",
						imgsrc: imgpath + "q1a.jpg",
					},
					{
						optid: "2",
						imgsrc: imgpath + "q1b.jpg",
					},
					{
						optid: "3",
						imgsrc: imgpath + "q1c.jpg",
					},
					{
						optid: "4",
						imgsrc: imgpath + "q1d.jpg",
					}
				]
			}
		 ],
	},
	//ex2
	{
		contentblockadditionalclass: 'ole-background-gradient-turqiose',
		exerciseblock: [
			{
				textdata: data.string.e2_ins,
				sentdata: data.string.sent2,
				btndata: data.string.e2_submit,
				exeoptions: [
					{
						optid: "1",
						imgsrc: imgpath + "q2a.jpg",
					},
					{
						optid: "2",
						imgsrc: imgpath + "q2b.jpg",
					},
					{
						optid: "3",
						imgsrc: imgpath + "q2c.jpg",
					},
					{
						optid: "4",
						imgsrc: imgpath + "q2d.jpg",
					}
				]
			}
		],
	}
	,
	//ex3
	{
		contentblockadditionalclass: 'ole-background-gradient-turqiose',
		exerciseblock: [
			{
				textdata: data.string.e2_ins,
				sentdata: data.string.sent3,
				btndata: data.string.e2_submit,
				exeoptions: [
					{
						optid: "1",
						imgsrc: imgpath + "q3a.jpg",
					},
					{
						optid: "2",
						imgsrc: imgpath + "q3b.jpg",
					},
					{
						optid: "3",
						imgsrc: imgpath + "q3c.jpg",
					},
					{
						optid: "4",
						imgsrc: imgpath + "q3d.jpg",
					}
				]
			}
		],
	},
	//ex4
	{
		contentblockadditionalclass: 'ole-background-gradient-turqiose',
		exerciseblock: [
			{
				textdata: data.string.e2_ins,
				sentdata: data.string.sent4,
				btndata: data.string.e2_submit,
				exeoptions: [
					{
						optid: "1",
						imgsrc: imgpath + "q4a.jpg",
					},
					{
						optid: "2",
						imgsrc: imgpath + "q4b.jpg",
					},
					{
						optid: "3",
						imgsrc: imgpath + "q4c.jpg",
					},
					{
						optid: "4",
						imgsrc: imgpath + "q4d.jpg",
					}
				]
			}
		],
	},
	//ex5
	{
		contentblockadditionalclass: 'ole-background-gradient-turqiose',
		exerciseblock: [
			{
				textdata: data.string.e2_ins,
				sentdata: data.string.sent5,
				btndata: data.string.e2_submit,
				exeoptions: [
					{
						optid: "1",
						imgsrc: imgpath + "q5a.jpg",
					},
					{
						optid: "2",
						imgsrc: imgpath + "q5b.jpg",
					},
					{
						optid: "3",
						imgsrc: imgpath + "q5c.jpg",
					},
					{
						optid: "4",
						imgsrc: imgpath + "q5d.jpg",
					}
				]
			}
		],
	},
	//ex6
	{
		contentblockadditionalclass: 'ole-background-gradient-turqiose',
		exerciseblock: [
			{
				textdata: data.string.e2_ins,
				sentdata: data.string.sent6,
				btndata: data.string.e2_submit,
				exeoptions: [
					{
						optid: "1",
						imgsrc: imgpath + "q6a.jpg",
					},
					{
						optid: "2",
						imgsrc: imgpath + "q6b.jpg",
					},
					{
						optid: "3",
						imgsrc: imgpath + "q6c.jpg",
					},
					{
						optid: "4",
						imgsrc: imgpath + "q6d.jpg",
					}
				]
			}
		],
	},
	//ex7
	{
		contentblockadditionalclass: 'ole-background-gradient-turqiose',
		exerciseblock: [
			{
				textdata: data.string.e2_ins,
				sentdata: data.string.sent7,
				btndata: data.string.e2_submit,
				exeoptions: [
					{
						optid: "1",
						imgsrc: imgpath + "q7a.jpg",
					},
					{
						optid: "2",
						imgsrc: imgpath + "q7b.jpg",
					},
					{
						optid: "3",
						imgsrc: imgpath + "q7c.jpg",
					},
					{
						optid: "4",
						imgsrc: imgpath + "q7d.jpg",
					}
				]
			}
		],
	},
	//ex8
	{
		contentblockadditionalclass: 'ole-background-gradient-turqiose',
		exerciseblock: [
			{
				textdata: data.string.e2_ins,
				sentdata: data.string.sent8,
				btndata: data.string.e2_submit,
				exeoptions: [
					{
						optid: "1",
						imgsrc: imgpath + "q8a.jpg",
					},
					{
						optid: "2",
						imgsrc: imgpath + "q8b.jpg",
					},
					{
						optid: "3",
						imgsrc: imgpath + "q8c.jpg",
					},
					{
						optid: "4",
						imgsrc: imgpath + "q8d.jpg",
					}
				]
			}
		],
	},
	//ex9
	{
		contentblockadditionalclass: 'ole-background-gradient-turqiose',
		exerciseblock: [
			{
				textdata: data.string.e2_ins,
				sentdata: data.string.sent9,
				btndata: data.string.e2_submit,
				exeoptions: [
					{
						optid: "1",
						imgsrc: imgpath + "q9a.jpg",
					},
					{
						optid: "2",
						imgsrc: imgpath + "q9b.jpg",
					},
					{
						optid: "3",
						imgsrc: imgpath + "q9c.jpg",
					},
					{
						optid: "4",
						imgsrc: imgpath + "q9d.jpg",
					}
				]
			}
		],
	},
	//ex10
	{
		contentblockadditionalclass: 'ole-background-gradient-turqiose',
		exerciseblock: [
			{
				textdata: data.string.e2_ins,
				sentdata: data.string.sent10,
				btndata: data.string.e2_submit,
				exeoptions: [
					{
						optid: "1",
						imgsrc: imgpath + "q10a.jpg",
					},
					{
						optid: "2",
						imgsrc: imgpath + "q10b.jpg",
					},
					{
						optid: "3",
						imgsrc: imgpath + "q10c.jpg",
					},
					{
						optid: "4",
						imgsrc: imgpath + "q10d.jpg",
					}
				]
			}
		],
	}
];

/*remove this for non random questions*/
content.shufflearray();

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var correctans = 0;
	var countNext = 0;
	var scoreupdate = 0;
	
	/*for limiting the questions to 10*/
	var $total_page = content.length;
	/*var $total_page = content.length;*/
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images

		// sounds
			{id: "exer", src: soundAsset+"ex.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	var score = 0;

	var testin = new NumberTemplate();

	testin.init(10);

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);
if(countNext==0){
	sound_player("exer");
}

		/*generate question no at the beginning of question*/
		testin.numberOfQuestions();
		/*for randomizing the options*/
		var parent = $(".optionsdiv");
		var divs = parent.children();
			while (divs.length) {
				parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}

		var ansClicked = false;
		var wrngClicked = false;
		var functionreturn = 0;
		var ansorder = [];
		$(".optionsdiv").sortable({
			placeholder: "drop-highlight",
			stop: function(event, ui) {
				ansorder = $(this).sortable('toArray');
				$(".submitbtn").show(0);
				// $(".instruction").html(productOrder);
			}
		});
		$(".submitbtn").click(function(){
			functionreturn = check_order(ansorder);
			correctans= correctans + functionreturn;
		});
		function check_order(array){
			var correctones = 0;
			for(i=1; i<=array.length; i++){
				if(parseInt(array[i-1])==i){
					correctones++;
					$('#'+i).removeClass('incorrect').addClass('correct');
				} else{
					$('#'+i).removeClass('correct').addClass('incorrect');
				}
			}

			if(correctones==array.length){
				$( ".optionsdiv" ).sortable( "disable" );
				$('.optionscontainer').css('pointer-events', 'none');
				play_correct_incorrect_sound(1);
				$(".submitbtn").hide(0);
				$nextBtn.show(0);
			}
			else{
				play_correct_incorrect_sound(0);
				wrngClicked = true;
			}
			if(!ansClicked && !wrngClicked){
				scoreupdate++;
				testin.updatescore(scoreupdate);
				ansClicked = true;
			}
		}
		/*======= SCOREBOARD SECTION ==============*/
	}

	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators
	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
			testin.gotoNext();
		templateCaller();

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

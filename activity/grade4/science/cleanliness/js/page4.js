var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
{
	startdivs:[
	{
		partimg: imgpath + "monkey01.png",
		partimgclass: "object-one",
		partcontainer: "top-one",
		startdata: data.string.top1
	},
	{
		partimg: imgpath + "teachericon.png",
		partimgclass: "object-two",
		partcontainer: "top-two",
		startdata: data.string.top2
	},
	{
		partimg: imgpath + "dogicon.png",
		partimgclass: "object-three",
		partcontainer: "top-three",
		startdata: data.string.top3
	},
	{
		partimg: imgpath + "boyicon.png",
		partimgclass: "object-four",
		partcontainer: "top-four",
		startdata: data.string.top4
	},
	{
		partimg: imgpath + "comb.png",
		partimgclass: "object-five",
		partcontainer: "top-five",
		startdata: data.string.top5
	},
	]
},
{
	startdivs:[
	{
		partimg: imgpath + "monkey01.png",
		partimgclass: "object-one removethisimg",
		partcontainer: "top-one removethis",
		startdata: data.string.top1
	},
	{
		partimg: imgpath + "teachericon.png",
		partimgclass: "object-two removethisimg",
		partcontainer: "top-two removethis",
		startdata: data.string.top2
	},
	{
		partimg: imgpath + "dogicon.png",
		partimgclass: "object-three removethisimg",
		partcontainer: "top-three removethis",
		startdata: data.string.top3
	},
	{
		partimg: imgpath + "boyicon.png",
		partimgclass: "object-four selectimg",
		partcontainer: "top-four selectthis",
		startdata: data.string.top4
	},
	{
		partimg: imgpath + "comb.png",
		partimgclass: "object-five removethisimg",
		partcontainer: "top-five removethis",
		startdata: data.string.top5
	}
	]
},
{
	contentblockadditionalclass: "background1",
	extratextblock : [{
		textdata : data.string.p4text1,
		textclass : 'template-dialougebox-right-orange dg-1'
	}],
},
{
	contentblockadditionalclass: "background2",
	extratextblock : [{
		textdata : data.string.p4text2,
		textclass : 'template-dialougebox-left-orange dg-2'
	}],
},
{
	contentblockadditionalclass: "background3",
	extratextblock : [{
		textdata : data.string.p4text3,
		textclass : 'template-dialougebox-top-orange dg-3'
	},{
		textclass : 'kira-1'
	}],
},
{
	contentblockadditionalclass: "background4",
	extratextblock : [{
		textdata : data.string.p4text4,
		textclass : 'template-dialougebox-top-orange dg-4'
	},{
		textclass : 'kira-2'
	}],
},
{
	contentblockadditionalclass: "background5",
	extratextblock : [{
		textdata : data.string.p4text5,
		textclass : 'template-dialougebox-right-orange dg-5'
	}],
},
{
	contentblockadditionalclass: "background6",
	extratextblock : [{
		textdata : data.string.p4text6,
		textclass : 'template-dialougebox-top-orange dg-6'
	}],
},
{
	contentblockadditionalclass: "background7",
	uppertextblock : [{
		textdata : data.string.p4text7,
		textclass : 'laterthatday'
	}],
},
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images

		// sounds
			{id: "sound_1", src: soundAsset+"s1_p2_4.ogg"},
			{id: "sound_2", src: soundAsset+"s4_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s4_p4.ogg"},
			{id: "sound_4", src: soundAsset+"s4_p5.ogg"},
      {id: "sound_5", src: soundAsset+"s4_p6.ogg"},
      {id: "sound_6", src: soundAsset+"s4_p7.ogg"},
			{id: "sound_7", src: soundAsset+"s4_p8.ogg"},
			{id: "sound_8", src: soundAsset+"s4_p9.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


      function navigationcontroller(islastpageflag){
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch(countNext){
			case 0:
			nav_button_controls(200);
			break;
			default:
			sound_player("sound_"+countNext);
			break;
		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(0);
		});
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});


 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

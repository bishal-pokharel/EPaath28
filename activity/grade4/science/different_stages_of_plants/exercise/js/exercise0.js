var imgpath = $ref + "/exercise/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
// var content;

// function getContent(data){
var content=[
	{
		additionalclasscontentblock: "contentwithbg",
		uppertextblock:[
        {
                textclass : "exeques",
                textdata : data.string.exe,
        },
		{
			textclass : "fintxt",
			textdata : data.string.fintxt,
		},
		{
			textclass : "check",
			textdata : data.string.check,
		},
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "draggableitem drag1",
				imgsrc : imgpath + "images/01.png"
			},
			{
				imgclass: "draggableitem drag2",
				imgsrc : imgpath + "images/02.png"
			},
			{
				imgclass: "draggableitem drag3",
				imgsrc : imgpath + "images/03.png"
			},
			{
				imgclass: "draggableitem drag4",
				imgsrc : imgpath + "images/04.png"
			},
			{
				imgclass: "draggableitem drag5",
				imgsrc : imgpath + "images/05.png"
			},
			{
				imgclass: "draggableitem drag6",
				imgsrc : imgpath + "images/06.png"
			},
			],
		}
		],
		extradivarray:[
		{
			extradiv: "droppableitem drop1 incorrect",
		},
		{
			extradiv: "droppableitem drop2 incorrect",
		},
		{
			extradiv: "droppableitem drop3 incorrect",
		},
		{
			extradiv: "droppableitem drop4 incorrect",
		},
		{
			extradiv: "droppableitem drop5 incorrect",
		},
		{
			extradiv: "droppableitem drop6 incorrect",
		},
		],
	}

];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;
    loadTimelineProgress($total_page,countNext+1);
		var preload;
		var timeoutvar = null;
		var current_sound;

		function init() {
			//specify type otherwise it will load assests as XHR
			manifest = [
				// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
				// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
				//   ,
				//images

			// sounds
				{id: "sound_0", src: soundAsset+"exe_1.ogg"},

			];
			preload = new createjs.LoadQueue(false);
			preload.installPlugin(createjs.Sound);//for registering sounds
			preload.on("progress", handleProgress);
			preload.on("complete", handleComplete);
			preload.on("fileload", handleFileLoad);
			preload.loadManifest(manifest, true);
		}
		function handleFileLoad(event) {
			// console.log(event.item);
		}
		function handleProgress(event) {
			$('#loading-text').html(parseInt(event.loaded*100)+'%');
		}
		function handleComplete(event) {
			$('#loading-wrapper').hide(0);
			//initialize varibales
			current_sound = createjs.Sound.play('sound_1');
			current_sound.stop();
			// call main function
			templateCaller();
		}
		//initialize
		init();
	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
	*/
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		switch(countNext){
		// 	case 0:
		// 	$nextBtn.show(0);
		// 	 $nextBtn.on("click", function() {
    //     countNext++;
    //     generalTemplate();
  	// 	/*
		//   for (var i = 0; i < content.length; i++) {
		//     slides(i);
		//     $($('.totalsequence')[i]).html(i);
		//     $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		//   "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		//   }
		//   function slides(i){
		//       $($('.totalsequence')[i]).click(function(){
		//         countNext = i;
		//         templateCaller();
		//       });
		//     }
		// */
    //
    //
    // });
		// 	break;
			case 0:
			sound_player1("sound_0");
			$nextBtn.hide(0);
			$('.fintxt').hide(0);

		$(".draggableitem").draggable({
			 //translate is used to place it while dropping. removing this when dragging again.
			 revert: 'invalid',
			 drag: function(event, ui){
			 	$(this).css("transform","translate(0,0)");
			 }
		});

		$(".droppableitem").droppable({
			accept: ".draggableitem",
			drop: handledrop
		});

		function handledrop(event, ui){
			var dropped = ui.draggable;
			var droppedOn = $(this);
			 droppedOn.find('ul').append(dropped);
			 console.log(droppedOn.find('ul').children())
			 var lee = droppedOn.position().left + droppedOn.width()/2;
			 var rii = droppedOn.position().top + droppedOn.height()/2;
			//get the second class and select the forth letter of that class (eg: get 4 from drop4)
			if((dropped.attr('class').split(' ')[1].split('')[4]) == droppedOn.attr('class').split(' ')[1].split('')[4]){
				dropped.addClass('done');
				$('.done').draggable('disable');
				droppedOn.addClass('correct');
				droppedOn.removeClass('incorrect');
			}else{
				let dropNum = droppedOn.attr('class').split(' ')[1].split('')[4] ;
				if(!$(".drag"+dropNum).hasClass("done")){
					droppedOn.addClass('incorrect');
					droppedOn.removeClass('correct');
				}
			}

			$(dropped.context).css({
				"transform": "translate(-50%,-50%)",
				"left": ""+lee+"px",
				"top": ""+rii+"px",
			});
		}

		$('.check').click(function(){
			var correctCount = 0;
			//disable correct one
			// $('.done').draggable('disable');

			$('.correct').each(function(){
				correctCount++;
			});
			if(correctCount == 6){
				$('.fintxt').show(0);
				ole.footerNotificationHandler.pageEndSetNotification() ;
			}

			$('.incorrect').css("border","3px solid #FA2E1D");
			$('.correct').css("border","3px solid #72D400");
		});
			break;
		}


	}
	function sound_player1(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}
});


		function navigationcontroller(islastpageflag){
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;

		}
 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span>";


					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

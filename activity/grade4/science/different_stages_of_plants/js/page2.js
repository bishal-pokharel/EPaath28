var imgpath = $ref+"/images/";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
  {
  	//start
  	contentblockadditionalclass: "simplegreenbg",
  	contentnocenteradjust: true,
  	uppertextblockadditionalclass: "firsttitle",
  	uppertextblock: [{
  		textclass: "title",
  		textdata: data.string.p3_s8
  	}]
  },{
  	//page 1
  		contentblockadditionalclass: "simplegreenbg",
  		contentnocenteradjust: true,
	  	specialimagediv :[{
	  		specialimagecontainerclass: "container1",
	  		imagestoshow :[{
	  			imgsrc : imgpath + "wheat04.png",
				imgclass : "plant1"
	  		},{
	  			imgsrc : imgpath + "paddy04.png",
				imgclass : "plant2"
	  		},{
	  			imgsrc : imgpath + "corn04.png",
				imgclass : "plant3"
	  		}],
	  		imagelabels: [{
	  			imagelabelclass: "label1_1",
				imagelabeldata: data.string.p3_s10
	  		},{
	  			imagelabelclass: "label1_2",
				imagelabeldata: data.string.p3_s9
	  		},{
	  			imagelabelclass: "label1_3",
				imagelabeldata: data.string.p3_s11
	  		}]
	  	},{
	  		specialimagecontainerclass: "container2",
	  		imagestoshow :[{
	  			imgsrc : imgpath + "potato04.png",
				imgclass : "plant2_1"
	  		},{
	  			imgsrc : imgpath + "pedalu04.png",
				imgclass : "plant2_2"
	  		}],
	  		imagelabels: [{
	  			imagelabelclass: "label2_1",
				imagelabeldata: data.string.p3_s12
	  		},{
	  			imagelabelclass: "label2_2",
				imagelabeldata: data.string.p3_s13
	  		}]

	  	},{
	  		specialimagecontainerclass: "container3",
			imagestoshow :[{
	  			imgsrc : imgpath + "sugarcane04.png",
				imgclass : "plant3_1"
	  		},{
	  			imgsrc : imgpath + "rose04.png",
				imgclass : "plant3_2"
	  		}],
	  		imagelabels: [{
	  			imagelabelclass: "label3_1",
				imagelabeldata: data.string.p3_s14
	  		},{
	  			imagelabelclass: "label3_2",
				imagelabeldata: data.string.p3_s15
	  		}]
	  	}]
  },{
  	//page 2
  		contentblockadditionalclass: "simplegreenbg",
  		contentnocenteradjust: true,
	  	specialimagediv :[{
	  		specialimagecontainerclass: "container1",
	  		imagestoshow :[{
	  			imgsrc : imgpath + "wheat04.png",
				imgclass : "plant1"
	  		},{
	  			imgsrc : imgpath + "paddy04.png",
				imgclass : "plant2"
	  		},{
	  			imgsrc : imgpath + "corn04.png",
				imgclass : "plant3"
	  		}],
	  		imagelabels: [{
	  			imagelabelclass: "label1_1",
				imagelabeldata: data.string.p3_s10
	  		},{
	  			imagelabelclass: "label1_2",
				imagelabeldata: data.string.p3_s9
	  		},{
	  			imagelabelclass: "label1_3",
				imagelabeldata: data.string.p3_s11
	  		},{
	  			imagelabelclass: "information1",
				imagelabeldata: data.string.p3_s17_1
	  		}]
	  	}]
  },{
  	//page 3

	contentblockadditionalclass: "simplegreenbg",
	imageblockadditionalclass: "additionalimageblock",
	contentnocenteradjust: true,
	uppertextblockadditionalclass : "mockheadercontainer",
	uppertextblock : [{
		textdata : data.string.p3_s17
	},{
		textclass: "clickhint",
		textdata : data.string.p3_s20
	}],
	lowertextblockadditionalclass: "spritecontainer1",
	lowertextblock: [{
		textclass: "wheatsprite"
	},{
		textclass: "paddysprite"
	},{
		textclass: "cornsprite"
	}],

	imageblock:[{
		imagestoshow : [{
			imgsrc : imgpath + "arrow.png",
			imgclass : "arrow1"
		}, {
			imgsrc : imgpath + "arrow.png",
			imgclass : "arrow2"
		}, {
			imgsrc : imgpath + "arrow.png",
			imgclass : "arrow3"
		}, {
			imgsrc : imgpath + "wheat04.png",
			imgclass : "clickableplant1 clickanimatefilerdiv"
		},{
			imgsrc : imgpath + "paddy04.png",
			imgclass : "clickableplant2 clickanimatefilerdiv"
		},
		{
			imgsrc : imgpath + "corn04.png",
			imgclass : "clickableplant3 clickanimatefilerdiv"
		},{
			imgsrc : imgpath + "wheat01.png",
			imgclass : "lifecycle01_long wheat"
		},{
			imgsrc : imgpath + "wheat02.png",
			imgclass : "lifecycle02_long wheat"
		},{
			imgsrc : imgpath + "wheat03.png",
			imgclass : "lifecycle03_long wheat"
		},{
			imgsrc : imgpath + "wheat04.png",
			imgclass : "lifecycle04_long wheat"
		},{
			imgsrc : imgpath + "paddy01.png",
			imgclass : "lifecycle01_long paddy"
		},{
			imgsrc : imgpath + "paddy02.png",
			imgclass : "lifecycle02_long paddy"
		},{
			imgsrc : imgpath + "paddy03.png",
			imgclass : "lifecycle03_long paddy"
		},{
			imgsrc : imgpath + "paddy04.png",
			imgclass : "lifecycle04_long paddy"
		},{
			imgsrc : imgpath + "corn01.png",
			imgclass : "lifecycle01_long_corn corn"
		},{
			imgsrc : imgpath + "corn02.png",
			imgclass : "lifecycle02_long corn"
		},{
			imgsrc : imgpath + "corn03.png",
			imgclass : "lifecycle03_long corn"
		},{
			imgsrc : imgpath + "corn04.png",
			imgclass : "lifecycle04_long corn"
		},{
			imgsrc : imgpath + "hand-icon.gif",
			imgclass : "clickme1"
		},{
			imgsrc : imgpath + "hand-icon.gif",
			imgclass : "clickme2"
		},{
			imgsrc : imgpath + "hand-icon.gif",
			imgclass : "clickme3"
		}
		],
		imagelabels : [{
			imagelabelclass: "stage1",
			imagelabeldata: data.string.p3_s1_seed
		},{
			imagelabelclass: "stage2",
			imagelabeldata: data.string.p3_s2_seed
		},{
			imagelabelclass: "stage3",
			imagelabeldata: data.string.p3_s3
		},{
			imagelabelclass: "stage4",
			imagelabeldata: data.string.p3_s4_nonflowering
		},{
			imagelabelclass: "clicklabel1",
			imagelabeldata: data.string.p3_s10
		},{
			imagelabelclass: "clicklabel2",
			imagelabeldata: data.string.p3_s9
		},{
			imagelabelclass: "clicklabel3",
			imagelabeldata: data.string.p3_s11
		}]
	}]

  },{
  	//page 4
  		contentblockadditionalclass: "simplegreenbg",
  		contentnocenteradjust: true,
	  	specialimagediv :[{
	  		specialimagecontainerclass: "container1",
	  		imagestoshow :[{
	  			imgsrc : imgpath + "wheat04.png",
				imgclass : "plant1"
	  		},{
	  			imgsrc : imgpath + "paddy04.png",
				imgclass : "plant2"
	  		},{
	  			imgsrc : imgpath + "corn04.png",
				imgclass : "plant3"
	  		}],
	  		imagelabels: [{
	  			imagelabelclass: "label1_1",
				imagelabeldata: data.string.p3_s10
	  		},{
	  			imagelabelclass: "label1_2",
				imagelabeldata: data.string.p3_s9
	  		},{
	  			imagelabelclass: "label1_3",
				imagelabeldata: data.string.p3_s11
	  		}]
	  	},{
	  		specialimagecontainerclass: "container2",
	  		imagestoshow :[{
	  			imgsrc : imgpath + "potato04.png",
				imgclass : "plant2_1"
	  		},{
	  			imgsrc : imgpath + "pedalu04.png",
				imgclass : "plant2_2"
	  		}],
	  		imagelabels: [{
	  			imagelabelclass: "label2_1",
				imagelabeldata: data.string.p3_s12
	  		},{
	  			imagelabelclass: "label2_2",
				imagelabeldata: data.string.p3_s13
	  		}]

	  	},{
	  		specialimagecontainerclass: "container3",
			imagestoshow :[{
	  			imgsrc : imgpath + "sugarcane04.png",
				imgclass : "plant3_1"
	  		},{
	  			imgsrc : imgpath + "rose04.png",
				imgclass : "plant3_2"
	  		}],
	  		imagelabels: [{
	  			imagelabelclass: "label3_1",
				imagelabeldata: data.string.p3_s14
	  		},{
	  			imagelabelclass: "label3_2",
				imagelabeldata: data.string.p3_s15
	  		}]
	  	}]
  }, {
  	//page 5
  		contentblockadditionalclass: "simplegreenbg",
  		contentnocenteradjust: true,
	  	specialimagediv :[{
	  		specialimagecontainerclass: "container2",
	  		imagestoshow :[{
	  			imgsrc : imgpath + "potato04.png",
				imgclass : "plant2_1"
	  		},{
	  			imgsrc : imgpath + "pedalu04.png",
				imgclass : "plant2_2"
	  		}],
	  		imagelabels: [{
	  			imagelabelclass: "label2_1",
				imagelabeldata: data.string.p3_s12
	  		},{
	  			imagelabelclass: "label2_2",
				imagelabeldata: data.string.p3_s13
	  		},{
	  			imagelabelclass: "information2",
				imagelabeldata: data.string.p3_s18_1
	  		}]

	  	}]
  },
  {
  	//page 6
  	contentblockadditionalclass: "simplegreenbg",
	imageblockadditionalclass: "additionalimageblock",
	contentnocenteradjust: true,
	uppertextblockadditionalclass : "mockheadercontainer",
	uppertextblock : [{
		// textclass: "additionalheader",
		textdata : data.string.p3_s18
	},{
		textclass: "clickhint",
		textdata : data.string.p3_s20
	}],
	lowertextblockadditionalclass: "spritecontainer2",
	lowertextblock: [{
		textclass: "potatosprite"
	},{
		textclass: "pedalusprite"
	}],

	imageblock:[{
		imagestoshow : [{
			imgsrc : imgpath + "arrow.png",
			imgclass : "arrow1"
		}, {
			imgsrc : imgpath + "arrow.png",
			imgclass : "arrow2"
		}, {
			imgsrc : imgpath + "arrow.png",
			imgclass : "arrow3"
		}, {
			imgsrc : imgpath + "potato04.png",
			imgclass : "clickableplant1_1 clickanimatefilerdiv"
		},{
			imgsrc : imgpath + "pedalu04.png",
			imgclass : "clickableplant2_1 clickanimatefilerdiv"
		},{
			imgsrc : imgpath + "potato01.png",
			imgclass : "lifecycle01_short wheat"
		},{
			imgsrc : imgpath + "potato02.png",
			imgclass : "lifecycle02_short wheat"
		},{
			imgsrc : imgpath + "potato03.png",
			imgclass : "lifecycle03_short wheat"
		},{
			imgsrc : imgpath + "potato04.png",
			imgclass : "lifecycle04_short wheat"
		},{
			imgsrc : imgpath + "pedalu01.png",
			imgclass : "lifecycle01_short paddy"
		},{
			imgsrc : imgpath + "pedalu02.png",
			imgclass : "lifecycle02_short paddy"
		},{
			imgsrc : imgpath + "pedalu03.png",
			imgclass : "lifecycle03_short paddy"
		},{
			imgsrc : imgpath + "pedalu04.png",
			imgclass : "lifecycle04_short paddy"
		},{
			imgsrc : imgpath + "hand-icon.gif",
			imgclass : "clickme2_1"
		},{
			imgsrc : imgpath + "hand-icon.gif",
			imgclass : "clickme2_2"
		}
		],
		imagelabels : [{
			imagelabelclass: "stage1",
			imagelabeldata: ""
		},{
			imagelabelclass: "stage2",
			imagelabeldata: ""
		},{
			imagelabelclass: "stage3",
			imagelabeldata: data.string.p3_s3
		},{
			imagelabelclass: "stage4",
			imagelabeldata: data.string.p3_s4_nonflowering
		},{
			imagelabelclass: "clicklabel1_1",
			imagelabeldata: data.string.p3_s12
		},{
			imagelabelclass: "clicklabel2_1",
			imagelabeldata: data.string.p3_s13
		}]
	}]
  },{
  	//page 7
  		contentblockadditionalclass: "simplegreenbg",
  		contentnocenteradjust: true,
	  	specialimagediv :[{
	  		specialimagecontainerclass: "container1",
	  		imagestoshow :[{
	  			imgsrc : imgpath + "wheat04.png",
				imgclass : "plant1"
	  		},{
	  			imgsrc : imgpath + "paddy04.png",
				imgclass : "plant2"
	  		},{
	  			imgsrc : imgpath + "corn04.png",
				imgclass : "plant3"
	  		}],
	  		imagelabels: [{
	  			imagelabelclass: "label1_1",
				imagelabeldata: data.string.p3_s10
	  		},{
	  			imagelabelclass: "label1_2",
				imagelabeldata: data.string.p3_s9
	  		},{
	  			imagelabelclass: "label1_3",
				imagelabeldata: data.string.p3_s11
	  		}]
	  	},{
	  		specialimagecontainerclass: "container2",
	  		imagestoshow :[{
	  			imgsrc : imgpath + "potato04.png",
				imgclass : "plant2_1"
	  		},{
	  			imgsrc : imgpath + "pedalu04.png",
				imgclass : "plant2_2"
	  		}],
	  		imagelabels: [{
	  			imagelabelclass: "label2_1",
				imagelabeldata: data.string.p3_s12
	  		},{
	  			imagelabelclass: "label2_2",
				imagelabeldata: data.string.p3_s13
	  		}]

	  	},{
	  		specialimagecontainerclass: "container3",
			imagestoshow :[{
	  			imgsrc : imgpath + "sugarcane04.png",
				imgclass : "plant3_1"
	  		},{
	  			imgsrc : imgpath + "rose04.png",
				imgclass : "plant3_2"
	  		}],
	  		imagelabels: [{
	  			imagelabelclass: "label3_1",
				imagelabeldata: data.string.p3_s14
	  		},{
	  			imagelabelclass: "label3_2",
				imagelabeldata: data.string.p3_s15
	  		}]
	  	}]
  },{
  	//page 8
  		contentblockadditionalclass: "simplegreenbg",
  		contentnocenteradjust: true,
	  	specialimagediv :[{
	  		specialimagecontainerclass: "container3",
			imagestoshow :[{
	  			imgsrc : imgpath + "sugarcane04.png",
				imgclass : "plant3_1"
	  		},{
	  			imgsrc : imgpath + "rose04.png",
				imgclass : "plant3_2"
	  		}],
	  		imagelabels: [{
	  			imagelabelclass: "label3_1",
				imagelabeldata: data.string.p3_s14
	  		},{
	  			imagelabelclass: "label3_2",
				imagelabeldata: data.string.p3_s15
	  		},{
	  			imagelabelclass: "information3",
				imagelabeldata: data.string.p3_s19_1
	  		}]
	  	}]
  },
   {
   	//page 9
  	contentblockadditionalclass: "simplegreenbg",
	imageblockadditionalclass: "additionalimageblock",
	contentnocenteradjust: true,
	uppertextblockadditionalclass : "mockheadercontainer",
	uppertextblock : [{
		// textclass: "additionalheader",
		textdata : data.string.p3_s19
	},{
		textclass: "clickhint",
		textdata : data.string.p3_s20
	}],
	lowertextblockadditionalclass: "spritecontainer2",
	lowertextblock: [{
		textclass: "sugarcanesprite"
	},{
		textclass: "rosesprite"
	}],

	imageblock:[{
		imagestoshow : [{
			imgsrc : imgpath + "arrow.png",
			imgclass : "arrow1"
		}, {
			imgsrc : imgpath + "arrow.png",
			imgclass : "arrow2"
		}, {
			imgsrc : imgpath + "arrow.png",
			imgclass : "arrow3"
		}, {
			imgsrc : imgpath + "sugarcane04.png",
			imgclass : "clickableplant1_sugarcane clickanimatefilerdiv"
		},{
			imgsrc : imgpath + "rose04.png",
			imgclass : "clickableplant2_1 clickanimatefilerdiv"
		},{
			imgsrc : imgpath + "sugarcane01.png",
			imgclass : "lifecycle01_sugarcane wheat"
		},{
			imgsrc : imgpath + "sugarcane02.png",
			imgclass : "lifecycle02_sugarcane wheat"
		},{
			imgsrc : imgpath + "sugarcane03.png",
			imgclass : "lifecycle03_sugarcane wheat"
		},{
			imgsrc : imgpath + "sugarcane04.png",
			imgclass : "lifecycle04_sugarcane wheat"
		},{
			imgsrc : imgpath + "rose01.png",
			imgclass : "lifecycle01_short paddy"
		},{
			imgsrc : imgpath + "rose02.png",
			imgclass : "lifecycle02_short paddy"
		},{
			imgsrc : imgpath + "rose03.png",
			imgclass : "lifecycle03_short paddy"
		},{
			imgsrc : imgpath + "rose04.png",
			imgclass : "lifecycle04_short paddy"
		},{
			imgsrc : imgpath + "hand-icon.gif",
			imgclass : "clickme3_1"
		},{
			imgsrc : imgpath + "hand-icon.gif",
			imgclass : "clickme3_2"
		}
		],
		imagelabels : [{
			imagelabelclass: "stage1",
			imagelabeldata: ""
		},{
			imagelabelclass: "stage2",
			imagelabeldata: ""
		},{
			imagelabelclass: "stage3",
			imagelabeldata: data.string.p3_s3
		},{
			imagelabelclass: "stage4",
			imagelabeldata: ""
		},{
			imagelabelclass: "clicklabel1_1",
			imagelabeldata: data.string.p3_s14
		},{
			imagelabelclass: "clicklabel2_1",
			imagelabeldata: data.string.p3_s15
		}]
	}]
  },{
  	//page 10
  	contentblockadditionalclass: "simplegreenbg",
	// contentnocenteradjust: true,
	uppertextblockadditionalclass : "conclusioncontainer newtxt",
	uppertextblock : [{
		textclass: "additionalheader",
		textdata : data.string.p4_s1
	}],
	conclusionlist: [
		{
			textdata : data.string.p4_s2
		}
	],
  imageblock:[{
  imagestoshow :[{
      imgsrc : imgpath + "potato04.png",
    imgclass : "newsugarcan"
    },{
      imgsrc : imgpath + "rose04.png",
    imgclass : "newrose"
  },{
    imgsrc : imgpath + "paddy04.png",
    imgclass : "newpaddy"
  }]
}]
  }
];


$(function() {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;

	loadTimelineProgress($total_page, countNext + 1);

  	var vocabcontroller =  new Vocabulary();
  	vocabcontroller.init();
    var preload;
  	var timeoutvar = null;
  	var current_sound;

  	function init() {
  		//specify type otherwise it will load assests as XHR
  		manifest = [
  			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
  			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
  			//   ,
  			//images

  		// sounds
  			{id: "sound_0", src: soundAsset+"s2_p1.ogg"},
  			{id: "sound_2", src: soundAsset+"s2_p3.ogg"},
  			{id: "sound_3", src: soundAsset+"s2_p4.ogg"},
  			{id: "sound_4", src: soundAsset+"s2_p6.ogg"},
  			{id: "sound_5", src: soundAsset+"s2_p9.ogg"},
  			{id: "sound_6", src: soundAsset+"s2_p11.ogg"},

  		];
  		preload = new createjs.LoadQueue(false);
  		preload.installPlugin(createjs.Sound);//for registering sounds
  		preload.on("progress", handleProgress);
  		preload.on("complete", handleComplete);
  		preload.on("fileload", handleFileLoad);
  		preload.loadManifest(manifest, true);
  	}
  	function handleFileLoad(event) {
  		// console.log(event.item);
  	}
  	function handleProgress(event) {
  		$('#loading-text').html(parseInt(event.loaded*100)+'%');
  	}
  	function handleComplete(event) {
  		$('#loading-wrapper').hide(0);
  		//initialize varibales
  		current_sound = createjs.Sound.play('sound_1');
  		current_sound.stop();
  		// call main function
  		templateCaller();
  	}
  	//initialize
  	init();

	/*==================================================
	 =            Handlers and helpers Block            =
	 ==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	/*=====  End of data highlight function  ======*/

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templateCaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templateCaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

	}

	/*=====  End of user navigation controller function  ======*/

	/*=====  End of Handlers and helpers Block  ======*/

	/*=======================================
	 =            Templates Block            =
	 =======================================*/
	/*=================================================
	 =            general template function            =
	 =================================================*/
	var source = $("#general-template").html();
	var template = Handlebars.compile(source);

	function generalTemplate() {
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		//call notifyuser
		// notifyuser($anydiv);

		switch(countNext){
      case 0:
      sound_player("sound_0");
      break;
      case 1:
      nav_button_controls(1000);
      break;
      case 2:
      sound_player("sound_2");
      break;
			case 3:
      sound_player1("sound_3");
				var clickedplant1 = false;
				var clickedplant2 = false;
				var clickedplant3 = false;

				var $wheat = $(".wheat");
				var $paddy = $(".paddy");
				var $corn = $(".corn");
				var $stage1 = $(".stage1");
				var $stage2 = $(".stage2");
				var $stage3 = $(".stage3");
				var $stage4 = $(".stage4");
				var $clickme1 = $(".clickme1");
				var $clickme2 = $(".clickme2");
				var $clickme3 = $(".clickme3");

				var spriteinprogress = false;
				var clicked = false;

				var click1 = false;
				var click2 = false;
				var click3 = false;

				 $(".clickableplant1, .clicklabel1, .clickme1").click(function(){
				 	if(!clicked){
				 		$(".clickhint").hide(0);
				 		clicked = true;
				 	}
					if (spriteinprogress)
						return false;

					if($(".clickableplant1").hasClass("clickanimatefilerdiv")){
						$(".clickableplant1").toggleClass("clickanimatefilerdiv").css("cursor", "auto");
					}
					clickedplant1 = true;
					click1 = true;
					$clickme1.hide(0);

					spriteinprogress = true;
					$(".spritecontainer1").show(0);
					$(".spritecontainer1").delay(5000).hide(0);
					$(".paddysprite").hide(0);
					$(".cornsprite").hide(0);
					$paddy.hide(0);
					$corn.hide(0);
					$wheat.hide(0);
					$stage1.hide(0);
					$stage2.hide(0);
					$stage3.hide(0);
					$stage4.hide(0);
					$(".wheatsprite").show(0);
					if(!click2){
						$(".clickme2").hide(0);
						$(".clickableplant2").toggleClass("clickanimatefilerdiv").css("cursor", "auto");
					}
					if(!click3){
						$(".clickme3").hide(0);
						$(".clickableplant3").toggleClass("clickanimatefilerdiv").css("cursor", "auto");
					}
					setTimeout(function(){
						$wheat.show(0);
						$stage1.delay(1000).show(0);
						$stage2.delay(2800).show(0);
						$stage3.delay(4600).show(0);
						$stage4.delay(6400).show(0);
					}, 4000);
					setTimeout(function(){
						// $(".wheatsprite").hide(0);
						spriteinprogress = false;
						if(!click2){
							$(".clickme2").show(0);
							$(".clickableplant2").toggleClass("clickanimatefilerdiv").css("cursor", "pointer");
						}
						if(!click3){
							$(".clickme3").show(0);
							$(".clickableplant3").toggleClass("clickanimatefilerdiv").css("cursor", "pointer");
						}
						if(clickedplant1 == true&& clickedplant2 == true && clickedplant3 == true){
			           nav_button_controls(2000);
						}
					}, 10000);
				});

				$(".clickableplant2, .clicklabel2, .clickme2").click(function(){
					if(!clicked){
				 		$(".clickhint").hide(0);
				 		clicked = true;
				 	}
					if (spriteinprogress)
						return false;

					if($(".clickableplant2").hasClass("clickanimatefilerdiv")){
						$(".clickableplant2").toggleClass("clickanimatefilerdiv").css("cursor", "auto");
					}


					clickedplant2 = true;
					click2 = true;
					$clickme2.hide(0);
					spriteinprogress = true;
					$(".spritecontainer1").show(0);
					$(".spritecontainer1").delay(5000).hide(0);
					$(".wheatsprite").hide(0);
					$(".cornsprite").hide(0);
					$wheat.hide(0);
					$paddy.hide(0);
					$corn.hide(0);
					$stage1.hide(0);
					$stage2.hide(0);
					$stage3.hide(0);
					$stage4.hide(0);
					$(".paddysprite").show(0);
					if(!click1){
						$(".clickme1").hide(0);
						$(".clickableplant1").toggleClass("clickanimatefilerdiv").css("cursor", "auto");
					}
					if(!click3){
						$(".clickme3").hide(0);
						$(".clickableplant3").toggleClass("clickanimatefilerdiv").css("cursor", "auto");
					}
					setTimeout(function(){
						$paddy.show(0);
						$stage1.delay(1000).show(0);
						$stage2.delay(2800).show(0);
						$stage3.delay(4600).show(0);
						$stage4.delay(6400).show(0);
					}, 4000);
					setTimeout(function(){
						// $(".paddysprite").hide(0);
						spriteinprogress = false;
						if(!click1){
							$(".clickme1").show(0);
							$(".clickableplant1").toggleClass("clickanimatefilerdiv").css("cursor", "pointer");
						}
						if(!click3){
							$(".clickme3").show(0);
							$(".clickableplant3").toggleClass("clickanimatefilerdiv").css("cursor", "pointer");
						}
						if(clickedplant1 == true&& clickedplant2 == true && clickedplant3 == true){
							$nextBtn.delay(2000).show(0);
						}
					}, 10000);
				});

				$(".clickableplant3, .clicklabel3, .clickme3").click(function(){
					if(!clicked){
				 		$(".clickhint").hide(0);
				 		clicked = true;
				 	}
					if (spriteinprogress)
						return false;

					if($(".clickableplant3").hasClass("clickanimatefilerdiv")){
						$(".clickableplant3").toggleClass("clickanimatefilerdiv").css("cursor", "auto");
					}

					clickedplant3 = true;
					click3 = true;
					$clickme3.hide(0);
					spriteinprogress = true;
					$(".spritecontainer1").show(0);
					$(".spritecontainer1").delay(5000).hide(0);
					$(".paddysprite").hide(0);
					$(".wheatsprite").hide(0);
					$wheat.hide(0);
					$paddy.hide(0);
					$corn.hide(0);
					$stage1.hide(0);
					$stage2.hide(0);
					$stage3.hide(0);
					$stage4.hide(0);
					$(".cornsprite").show(0);
					if(!click1){
						$(".clickme1").hide(0);
						$(".clickableplant1").toggleClass("clickanimatefilerdiv").css("cursor", "auto");
					}
					if(!click2){
						$(".clickme2").hide(0);
						$(".clickableplant2").toggleClass("clickanimatefilerdiv").css("cursor", "auto");
					}
					setTimeout(function(){
						$corn.show(0);
						$stage1.delay(1000).show(0);
						$stage2.delay(2800).show(0);
						$stage3.delay(4600).show(0);
						$stage4.delay(6400).show(0);
					}, 4000);
					setTimeout(function(){
						// $(".cornsprite").hide(0);
						if(!click1){
							$(".clickme1").show(0);
							$(".clickableplant1").toggleClass("clickanimatefilerdiv").css("cursor", "pointer");
						}
						if(!click2){
							$(".clickme2").show(0);
							$(".clickableplant2").toggleClass("clickanimatefilerdiv").css("cursor", "pointer");
						}
						if(clickedplant1 == true&& clickedplant2 == true && clickedplant3 == true){
							$nextBtn.delay(2000).show(0);
						}
						spriteinprogress = false;
					}, 10000);
				});

				break;
      case 5:
      sound_player("sound_4");
      break;
			case 6:
		    sound_player1("sound_3");
				var clickedplant1 = false;
				var clickedplant2 = false;
				var clickedplant3 = false;

				var $wheat = $(".wheat");
				var $paddy = $(".paddy");
				var $stage1 = $(".stage1");
				var $stage2 = $(".stage2");
				var $stage3 = $(".stage3");
				var $stage4 = $(".stage4");
				var clicked = false;
				var $clickme1 = $(".clickme2_1");
				var $clickme2 = $(".clickme2_2");
				var potatotextstage1 = data.string.p3_s1_potato;
				var yamstage1 = data.string.p3_s1_bulb;
				var potatostage2 = data.string.p3_s2_seed;
				var yamstage2 = data.string.p3_s2_bulb;

				var click2_1 = false;
				var click2_2 = false;

				$(".clickableplant1_1, .clicklabel1_1, .clickme2_1").click(function(){
					if(!clicked){
				 		$(".clickhint").hide(0);
				 		clicked = true;
				 	}
					if (spriteinprogress)
						return false;

					if($(".clickableplant1_1").hasClass("clickanimatefilerdiv")){
						$(".clickableplant1_1").toggleClass("clickanimatefilerdiv").css("cursor", "auto");
					}

					clickedplant1 = true;
					click2_1 = true;
					$clickme1.hide(0);
					$stage1.html(potatotextstage1);
					$stage2.html(potatostage2);
					spriteinprogress = true;
					$(".spritecontainer2").show(0);
					$(".spritecontainer2").delay(5000).hide(0);
					$(".pedalusprite").hide(0);
					$paddy.hide(0);
					// $corn.hide(0);
					$wheat.hide(0);
					$stage1.hide(0);
					$stage2.hide(0);
					$stage3.hide(0);
					$stage4.hide(0);
					$(".potatosprite").show(0);
					if(!click2_2){
						$(".clickme2_2").hide(0);
						$(".clickableplant2_1").toggleClass("clickanimatefilerdiv").css("cursor", "auto");
					}
					setTimeout(function(){
						$wheat.show(0);
						$stage1.delay(1000).show(0);
						$stage2.delay(2800).show(0);
						$stage3.delay(4600).show(0);
						$stage4.delay(6400).show(0);
					}, 4000);
					setTimeout(function(){
						spriteinprogress = false;
						if(!click2_2){
							$(".clickme2_2").show(0);
							$(".clickableplant2_1").toggleClass("clickanimatefilerdiv").css("cursor", "pointer");
						}
						if(clickedplant1 == true&& clickedplant2 == true){
							nav_button_controls(1000);
						}
					}, 10000);
				});

				 $(".clickableplant2_1, .clicklabel2_1, .clickme2_2").click(function(){
				 	if(!clicked){
				 		$(".clickhint").hide(0);
				 		clicked = true;
				 	}
					if (spriteinprogress)
						return false;

					if($(".clickableplant2_1").hasClass("clickanimatefilerdiv")){
						$(".clickableplant2_1").toggleClass("clickanimatefilerdiv").css("cursor", "auto");
					}

					clickedplant2 = true;
					click2_2 = true;
					$clickme2.hide(0);
					$stage1.html(yamstage1);
					$stage2.html(yamstage2);
					spriteinprogress = true;
					$(".spritecontainer2").show(0);
					$(".spritecontainer2").delay(5000).hide(0);
					$(".potatosprite").hide(0);
					$wheat.hide(0);
					$paddy.hide(0);
					// $corn.hide(0);
					$stage1.hide(0);
					$stage2.hide(0);
					$stage3.hide(0);
					$stage4.hide(0);
					$(".pedalusprite").show(0);
					if(!click2_1){
						$(".clickme2_1").hide(0);
						$(".clickableplant1_1").toggleClass("clickanimatefilerdiv").css("cursor", "auto");
					}
					setTimeout(function(){
						$paddy.show(0);
						$stage1.delay(1000).show(0);
						$stage2.delay(2800).show(0);
						$stage3.delay(4600).show(0);
						$stage4.delay(6400).show(0);
					}, 4000);
					setTimeout(function(){
						spriteinprogress = false;
						if(!click2_1){
							$(".clickme2_1").show(0);
							$(".clickableplant1_1").toggleClass("clickanimatefilerdiv").css("cursor", "pointer");
						}
						if(clickedplant1 == true&& clickedplant2 == true){
						nav_button_controls(1000);
						}
					}, 10000);
				});
				break;
        case 8:
        sound_player("sound_5");
        break;
			case 9:
				sound_player1("sound_3");
				var clickedplant1 = false;
				var clickedplant2 = false;
				var clickedplant3 = false;

				var $wheat = $(".wheat");
				var $paddy = $(".paddy");
				var $stage1 = $(".stage1");
				var $stage2 = $(".stage2");
				var $stage3 = $(".stage3");
				var $stage4 = $(".stage4");
				var clicked = false;
				var $clickme1 = $(".clickme3_1");
				var $clickme2 = $(".clickme3_2");
				var sugarcanestage1 = data.string.p3_s1_sugarcane;
				var rosestage1 = data.string.p3_s1_rose;
				var sugarcanestage2 = data.string.p3_s2_sugarcane;
				var rosestage2 = data.string.p3_s2_rose;
				var sugarcanestage4 = data.string.p3_s4_nonflowering;
				var rosestage4 = data.string.p3_s4_rose;

				var click3_1 = false;
				var click3_2 = false;

				$(".clickableplant1_sugarcane, .clicklabel1_1, .clickme3_1").click(function(){
					if(!clicked){
				 		$(".clickhint").hide(0);
				 		clicked = true;
				 	}
					if (spriteinprogress)
						return false;

					if($(".clickableplant1_sugarcane").hasClass("clickanimatefilerdiv")){
						$(".clickableplant1_sugarcane").toggleClass("clickanimatefilerdiv").css("cursor", "auto");
					}

					clickedplant1 = true;
					$clickme1.hide(0);
					$stage1.html(sugarcanestage1);
					$stage2.html(sugarcanestage2);
					$stage4.html(sugarcanestage4);
					spriteinprogress = true;
					$(".spritecontainer2").css("top", "30%");
					$(".spritecontainer2").show(0);
					setTimeout(function(){
						$(".spritecontainer2").css("top", "36%");
						$(".spritecontainer2").hide(0);
					}, 5000);
					$(".rosesprite").hide(0);
					$paddy.hide(0);
					// $corn.hide(0);
					$wheat.hide(0);
					$stage1.hide(0);
					$stage2.hide(0);
					$stage3.hide(0);
					$stage4.hide(0);
					$(".sugarcanesprite").show(0);
					click3_1 = true;
					if(!click3_2){
						$(".clickme3_2").hide(0);
						$(".clickableplant2_1").toggleClass("clickanimatefilerdiv").css("cursor", "auto");
					}
					setTimeout(function(){
						$wheat.show(0);
						$stage1.delay(1000).show(0);
						$stage2.delay(2800).show(0);
						$stage3.delay(4600).show(0);
						$stage4.delay(6400).show(0);
					}, 4000);
					setTimeout(function(){
						spriteinprogress = false;
						if(!click3_2){
							$(".clickme3_2").show(0);
							$(".clickableplant2_1").toggleClass("clickanimatefilerdiv").css("cursor", "pointer");
						}
						if(clickedplant1 == true&& clickedplant2 == true){
							$nextBtn.delay(2000).show(0);
						}
					}, 10000);
				});

				$(".clickableplant2_1, .clicklabel2_1, .clickme3_2").click(function(){
					if(!clicked){
				 		$(".clickhint").hide(0);
				 		clicked = true;
				 	}
					if (spriteinprogress)
						return false;

					if($(".clickableplant2_1").hasClass("clickanimatefilerdiv")){
						$(".clickableplant2_1").toggleClass("clickanimatefilerdiv").css("cursor", "auto");
					}

					clickedplant2 = true;
					$clickme2.hide(0);
					$stage1.html(rosestage1);
					$stage2.html(rosestage2);
					$stage4.html(rosestage4);
					spriteinprogress = true;
					$(".spritecontainer2").show(0);
					$(".spritecontainer2").delay(5000).hide(0);
					$(".sugarcanesprite").hide(0);
					$wheat.hide(0);
					$paddy.hide(0);
					// $corn.hide(0);
					$stage1.hide(0);
					$stage2.hide(0);
					$stage3.hide(0);
					$stage4.hide(0);
					$(".rosesprite").show(0);
					click3_2 = true;
					if(!click3_1){
						$(".clickme3_1").hide(0);
						$(".clickableplant1_sugarcane").toggleClass("clickanimatefilerdiv").css("cursor", "auto");
					}
					setTimeout(function(){
						$paddy.show(0);
						$stage1.delay(1000).show(0);
						$stage2.delay(2800).show(0);
						$stage3.delay(4600).show(0);
						$stage4.delay(6400).show(0);
					}, 4000);
					setTimeout(function(){
						spriteinprogress = false;
						if(!click3_1){
							$(".clickme3_1").show(0);
							$(".clickableplant1_sugarcane").toggleClass("clickanimatefilerdiv").css("cursor", "pointer");
						}
						if(clickedplant1 == true&& clickedplant2 == true){
						nav_button_controls(2000);
						}
					}, 10000);
				});
				break;
        case 10:
        sound_player("sound_6");
        break;

			default:
      nav_button_controls(1000);
				break;
		}

	}
  function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(0);
		});
	}
	function sound_player1(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	/*=====  End of Templates Block  ======*/

	/*==================================================
	=            Templates Controller Block            =
	==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
	 Motivation :
	 - Make a single function call that handles all the
	 template load easier

	 How To:
	 - Update the template caller with the required templates
	 - Call template caller

	 What it does:
	 - According to value of the Global Variable countNext
	 the slide templates are updated
	 */

	function templateCaller() {
			/*always hide next and previous navigation button unless
			 explicitly called from inside a template*/
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// call navigation controller
			navigationcontroller(true);

			// call the template
			generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


			//call the slide indication bar handler for pink indicators
			loadTimelineProgress($total_page, countNext + 1);

			// just for development purpose to see total slide vs current slide number
			// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click', function() {
		switch(countNext) {
		case 1:
			$nextBtn.hide(0);
			$(".container2, .container3").hide(0);
			// $(".specialimagediv").toggleClass("classfadeout");
			// $(".plant1").toggleClass("plant1animate");
			// $(".plant2").toggleClass("plant2animate");
			// $(".plant3").toggleClass("plant3animate");
			setTimeout(function(){
				countNext++;
				templateCaller();
			}, 500);
			break;
		case 2:
			$nextBtn.hide(0);
			// $(".container2, .container3").hide(0);
			$(".specialimagediv").toggleClass("classfadeout");
			$(".plant1").toggleClass("plant1animate");
			$(".plant2").toggleClass("plant2animate");
			$(".plant3").toggleClass("plant3animate");
			setTimeout(function(){
				countNext++;
				templateCaller();
			}, 3180);
			break;
		case 4:
			$nextBtn.hide(0);
			$(".container3, .container1").hide(0);
			// $(".specialimagediv").toggleClass("classfadeout");
			// $(".plant2_1").toggleClass("plant2_1animate");
			// $(".plant2_2").toggleClass("plant2_2animate");
			setTimeout(function(){
				countNext++;
				templateCaller();
			}, 500);
			break;
		case 5:
			$nextBtn.hide(0);
			// $(".container3, .container1").hide(0);
			$(".specialimagediv").toggleClass("classfadeout");
			$(".plant2_1").toggleClass("plant2_1animate");
			$(".plant2_2").toggleClass("plant2_2animate");
			setTimeout(function(){
				countNext++;
				templateCaller();
			}, 3180);
			break;
		case 7:
			$nextBtn.hide(0);
			$(".container2, .container1").hide(0);
			// $(".specialimagediv").toggleClass("classfadeout");
			// $(".plant3_1").toggleClass("plant3_1animate");
			// $(".plant3_2").toggleClass("plant3_2animate");
			setTimeout(function(){
				countNext++;
				templateCaller();
			}, 500);
			break;
		case 8:
			$nextBtn.hide(0);
			// $(".container2, .container1").hide(0);
			$(".specialimagediv").toggleClass("classfadeout");
			$(".plant3_1").toggleClass("plant3_1animate");
			$(".plant3_2").toggleClass("plant3_2animate");
			setTimeout(function(){
				countNext++;
				templateCaller();
			}, 3180);
			break;
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/*=====  End of Templates Controller Block  ======*/
});

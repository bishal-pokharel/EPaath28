var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var content=[
	//slide 1
	{
			contentnocenteradjust: true,
			contentblockadditionalclass: "bg2",
			extratextblock:[
			{
				textclass: "toptxtFst red",
				textdata: data.string.dyk
			},{
				textclass: "midtxt hidden fntlaila green_op",
				textdata: data.string.p2s5txt
			}],
			imageblock:[{
				imagestoshow:[
					{
						imgclass: "background",
						imgid : 'bgfrogjump',
						imgsrc: ""
					},{
						imgclass: "turtle_l",
						imgid : 'tortle',
						imgsrc: ""
					},{
						imgclass: "crock_l crfst",
						imgid : 'crock',
						imgsrc: ""
					},{
						imgclass: "jumping frogJmp",
						imgid : 'jumping',
						imgsrc: ""
					}]
			}]
		},
	//slide 2
	{
			contentnocenteradjust: true,
			contentblockadditionalclass: "bg2",
			extratextblock:[
			{
				textclass: "midtxt fntlaila whiteTrs",
				textdata: data.string.p3s2txt
			}],
			imageblock:[{
				imagestoshow:[
					{
						imgclass: "background",
						imgid : 'bgfrogjump',
						imgsrc: ""
					},{
						imgclass: "turtle_l",
						imgid : 'tortle',
						imgsrc: ""
					},{
						imgclass: "crock_l crfst",
						imgid : 'crock',
						imgsrc: ""
					},{
						imgclass: "jumping frog_2",
						imgid : 'frog',
						imgsrc: ""
					}]
			}]
		},
	//slide 3
	{
		contentnocenteradjust: true,
		extratextblock:[
		{
			textclass: "midtxt_1 green_op",
			textdata: data.string.aqtc
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'underwatebg',
					imgsrc: ""
				}]
		}]
	},
	// slide 4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg",
		extratextblock:[
		{
			textclass: "toptxt green",
			textdata: data.string.p2s0
		},{
			textclass: "anmNm animal-1",
			textdata: data.string.fish
		},{
			textclass: "anmNm animal-2",
			textdata: data.string.shark
		},{
			textclass: "anmNm animal-3",
			textdata: data.string.whale
		},{
			textclass: "anmNm animal-4",
			textdata: data.string.octopus
		},{
			textclass: "lstSldTxt fntlaila",
			textdata: data.string.lstsldtxt
		}],
		selectorbox:[{
					selectorboxclass:'jama-box',
					define_next_catagory: 'jama-next_button',
					define_prev_catagory: 'jama-prev_button',
					textdata:data.string.diytext2,
					imageinsideclass:'jama-image',
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass:'background',
				imgid:'underwatebg01'
			}]
		}]
	},
	//slide 5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg2",
		extratextblock:[
		{
			textclass: "toptxt green",
			textdata: data.string.p2s1
		},{
			textclass: "lefthalf lftxt fntlaila",
			textdata: data.string.p2s1txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "righthalf",
					imgid : 'underwatebg02',
					imgsrc: ""
				},{
					imgclass: "eel",
					imgid : 'eel',
					imgsrc: ""
				},{
					imgclass: "frog",
					imgid : 'frog',
					imgsrc: ""
				},{
					imgclass: "fish",
					imgid : 'fish_unline',
					imgsrc: ""
				},{
					imgclass: "frog_line",
					imgid : 'frog_line',
					imgsrc: ""
				},{
					imgclass: "fish_line",
					imgid : 'fish_line',
					imgsrc: ""
				},{
					imgclass: "eil_line",
					imgid : 'eil_line',
					imgsrc: ""
				}]
		}]
	},
	//slide 6
	{
			contentnocenteradjust: true,
			contentblockadditionalclass: "bg2",
			extratextblock:[
			{
				textclass: "toptxt green",
				textdata: data.string.p2s1
			},{
				textclass: "lefthalf lftxt fntlaila",
				textdata: data.string.p2s2txt
			},{
				textclass: "fins",
				textdata: data.string.fins
			}],
			imageblock:[{
				imagestoshow:[
					{
						imgclass: "righthalf",
						imgid : 'sea_background1',
						imgsrc: ""
					},{
						imgclass: "dolphin01",
						imgid : 'dolphin01',
						imgsrc: ""
					},{
						imgclass: "arrow1 ar1_2",
						imgid : 'arrow02',
						imgsrc: ""
					},{
						imgclass: "arrow2 ar2_2",
						imgid : 'arrow02',
						imgsrc: ""
					}]
			}]
		},
	//slide 7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg2",
		extratextblock:[
		{
			textclass: "toptxt green",
			textdata: data.string.p2s1
		},{
			textclass: "lefthalf lftxt fntlaila",
			textdata: data.string.p2s3txt
		},{
			textclass: "webft",
			textdata: data.string.webft
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "righthalf",
					imgid : 'frog_bg',
					imgsrc: ""
				},{
					imgclass: "frog_3",
					imgid : 'frog',
					imgsrc: ""
				},{
					imgclass: "arrow2 ar3_2",
					imgid : 'arrow02',
					imgsrc: ""
				}]
		}],
	},
	//slide 8
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg2",
		extratextblock:[
		{
			textclass: "toptxt green",
			textdata: data.string.p2s1
		},{
			textclass: "lefthalf lftxt fntlaila",
			textdata: data.string.p2s4txt
		},{
			textclass: "fins",
			textdata: data.string.gill
		},{
			textclass: "righthalf white",
			textdata: data.string.blank
		}],
		imageblock:[{
		imagestoshow:[
			{
					imgclass: "fish_gill",
					imgid : 'fish_gill',
					imgsrc: ""
			},{
					imgclass: "arrow2 ar4_2",
					imgid : 'arrow02',
					imgsrc: ""
			}]
		}],
		},
	// //slide 9
	// {
	// 	contentnocenteradjust: true,
	// 	contentblockadditionalclass: "bg2",
	// 	extratextblock:[
	// 	{
	// 		textclass: "toptxt red",
	// 		textdata: data.string.dyk
	// 	},{
	// 		textclass: "midtxt fntlaila green_op",
	// 		textdata: data.string.p2s5txt
	// 	}],
	// 	imageblock:[{
	// 		imagestoshow:[
	// 			{
	// 				imgclass: "background",
	// 				imgid : 'bg_crock',
	// 				imgsrc: ""
	// 			},{
	// 				imgclass: "turtle_l",
	// 				imgid : 'tortle',
	// 				imgsrc: ""
	// 			},{
	// 				imgclass: "frog_l",
	// 				imgid : 'frog',
	// 				imgsrc: ""
	// 			},{
	// 				imgclass: "crock_l",
	// 				imgid : 'crock01',
	// 				imgsrc: ""
	// 			}]
	// 	}]
	// },
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var timeoutvar2 = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images

			{id: "crock01", src: imgpath+"crock01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "crock", src: imgpath+"crock.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_crock", src: imgpath+"bg_crock.png", type: createjs.AbstractLoader.IMAGE},
			{id: "underwatebg", src: imgpath+"underwaterbg.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "underwatebg01", src: imgpath+"underwatebg01.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "underwatebg02", src: imgpath+"underwaterbg02.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "frog", src: imgpath+"frog.png", type: createjs.AbstractLoader.IMAGE},
			{id: "frog_line", src: imgpath+"frog_line.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fish_line", src: imgpath+"fish_line.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img1", src: imgpath+"fish.png", type: createjs.AbstractLoader.IMAGE},{id: "img2", src: imgpath+"shark.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img4", src: imgpath+"octopus.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img3", src: imgpath+"epaulard.png", type: createjs.AbstractLoader.IMAGE},
			{id: "epulard_01", src: imgpath+"epaulard_01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eel", src: imgpath+"eel-png.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dolphin", src: imgpath+"dolphin.png", type: createjs.AbstractLoader.IMAGE},
			{id: "btn01", src: imgpath+"bigarrow02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "btn02", src: imgpath+"bigarrow01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dolphin", src: imgpath+"dolphin.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tortle", src: imgpath+"tortle.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rabbit", src: imgpath+"rabbit.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-3", src: imgpath+"cow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow01", src: imgpath+"arrow01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow02", src: imgpath+"arrow02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "background", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fish_gill", src: imgpath+"fish_gill_01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dolphin01", src: imgpath+"dolphin01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "frog_bg", src: imgpath+"frog_bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "crock", src: imgpath+"crock.png", type: createjs.AbstractLoader.IMAGE},
			{id: "frog_line", src: imgpath+"frog_line.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fish_line", src: imgpath+"fish_line.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eil_line", src: imgpath+"eil_line.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fish_unline", src: imgpath+"fish_gill.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sea_background1", src: imgpath+"Sea-background1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg01", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "jumping", src: imgpath+"new/jumping.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "bgfrogjump", src: imgpath+"new/bg12a.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "frog", src: imgpath+"frog_new.png", type: createjs.AbstractLoader.IMAGE},
			{id: "next", src: imgpath+"new/next.png", type: createjs.AbstractLoader.IMAGE},
			{id: "previous", src: imgpath+"new/previous.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s3_p1", src: soundAsset+"s3_p1.ogg"},
			{id: "s3_p1_1", src: soundAsset+"s3_p1_1.ogg"},
			{id: "s3_p2", src: soundAsset+"s3_p2.ogg"},
			{id: "s3_p3", src: soundAsset+"s3_p3.ogg"},
			{id: "s3_p4", src: soundAsset+"s3_p4.ogg"},
			{id: "sound_1", src: soundAsset+"s3_p4_fish.ogg"},
			{id: "sound_4", src: soundAsset+"s3_p4_octopus.ogg"},
			{id: "sound_2", src: soundAsset+"s3_p4_sark.ogg"},
			{id: "sound_3", src: soundAsset+"s3_p4_whale.ogg"},
			{id: "s3_p4_last", src: soundAsset+"s3_p4_last.ogg"},
			{id: "s3_p5", src: soundAsset+"s3_p5.ogg"},
			{id: "s3_p5_1", src: soundAsset+"s3_p5_1.ogg"},
			{id: "s3_p6", src: soundAsset+"s3_p6.ogg"},
			{id: "s3_p7", src: soundAsset+"s3_p7.ogg"},
			{id: "s3_p8", src: soundAsset+"s3_p8.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		var currentSound;
		var showImage;
		var index = 1;
		$(".animal-1, .animal-2, .animal-3, .animal-4").css({display:'none'});
		$('.lstSldTxt').hide(0);
		$('.prev_button').attr("src",preload.getResult('previous').src);
		$('.next_button').attr("src",preload.getResult('next').src);
		function showImage(img_name, index){
			$('.image_inside').attr("src",preload.getResult(img_name+index).src);
			$(".animal-"+index).css({display:'block'});
			if(index == 1){
				$('.prev_button').hide(0);
				$('.next_button').addClass("blink");
			}
			else if(index == 4){
				$('.lstSldTxt').addClass("show");
				$('.next_button').hide(0);
				setTimeout(function(){
					sound_player("s3_p4_last",1);
				},1300);
			}
			else{
				$('.next_button').show(0);
				$('.prev_button').show(0);
				$('.next_button').removeClass("blink");
			}
		}
		$('.next_button').click(function(){
				index = index+1;
				showImage("img",index);
				$(".animal-"+(index-1)).css({display:'none'});
				sound_player("sound_"+index,0);
		});
		$('.prev_button').click(function(){
				index = index-1;
					navigationcontroller();
				showImage("img",index);
				$(".animal-"+(index+1)).css({display:'none'});
				sound_player("sound_"+index,0);
		});
		switch(countNext){
			case 0:
				$(".jumping").animate({
					left: "44%",
					top: "77%",
					height: "20%"
				},1000, function(){
					$(".toptxtFst").addClass("slideDwn");
					setTimeout(function(){
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("s3_p1");
						current_sound.play();
						current_sound.on('complete', function(){
							$(".hidden").removeClass("hidden");
							current_sound1 = createjs.Sound.play("s3_p1_1");
							current_sound1.on('complete', function(){
								navigationcontroller();});
						});
					},500);
				});
			break;
			case 3:
				$(".next_button, .prev_button").css("pointer-events","none");
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s3_p4");
				current_sound.play();
				current_sound.on('complete', function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("sound_1");
					current_sound.play();
					current_sound.on('complete', function(){
						$(".next_button, .prev_button").css("pointer-events","auto");
					});
				});
				showImage("img",1);
			break;
			case 4:
				$(".toptxt").css("top", "-15%");
				$(".toptxt").animate({
					top:"0%"},500,function(){
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("s3_p5");
						current_sound.play();
						current_sound.on('complete', function(){
							$(".lftxt").addClass("fadein");
							sound_player1("s3_p5_1");
						});
					}
				);
			break;
			case 5:
					$(".lftxt").addClass("fadein");
						createjs.Sound.stop();
						current_sound_1 = createjs.Sound.play("s3_p6");
						current_sound_1.play();
						current_sound_1.on('complete', function(){
							$(".ar1_2, .ar2_2, .fins").addClass("fadein");
							nav_button_controls(0);
						});
			break;
			case 6:
					$(".lftxt").addClass("fadein");
						createjs.Sound.stop();
						current_sound_1 = createjs.Sound.play("s3_p7");
						current_sound_1.play();
						current_sound_1.on('complete', function(){
							$(".ar3_2, .webft").addClass("fadein");
							nav_button_controls(0);
						});
			break;
			case 7:
					$(".lftxt").addClass("fadein");
						createjs.Sound.stop();
						current_sound_1 = createjs.Sound.play("s3_p8");
						current_sound_1.play();
						current_sound_1.on('complete', function(){
							$(".ar4_2, .fins").addClass("fadein");
							nav_button_controls(0);
						});
			break;
			default:
				sound_player("s3_p"+(countNext+1),1);
			break;

		}
	}

	function sound_player(sound_id, next){
		$(".next_button, .prev_button").css("pointer-events","none");
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".next_button, .prev_button").css("pointer-events","auto");
			next?nav_button_controls():'';
		});
	}
	function sound_player1(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete',function(){
				navigationcontroller();

		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2);
		});

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// if(countNext == 0)
		// navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentblockadditionalclass: "greenbg",
		singletext:[
			{
				textclass: "toptxt",
				textdata: data.string.p2sfst
			}
		],
		imageblock:[{
			imagestoshow:[{
				imgid:"night_sky",
				imgclass:"background",
				imgsrc:'',
			}]
		}],
		svgblock:[{
			svgblock:"earthSvg"
		}]
	},
	// slide1
	{
		contentblockadditionalclass: "greenbg",
		headerblock:[
			{
				textclass: "toptxt",
				textdata: data.string.p2s2toptxt
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgid:"night_sky",
					imgclass:"background",
					imgsrc:'',
				},{
					imgid:"coldbg",
					imgclass:"p2Img img1",
					imgsrc:'',
				},{
					imgid:"desertbg",
					imgclass:"p2Img img2",
					imgsrc:'',
				},{
					imgid:"petbg",
					imgclass:"p2Img img3",
					imgsrc:'',
				},{
					imgid:"wildbg",
					imgclass:"p2Img img4",
					imgsrc:'',
				},{
					imgid:"birdsbg",
					imgclass:"p2Img img5",
					imgsrc:'',
				}
			]
		}]
	},
	// slide2
	{
		contentblockadditionalclass: "greenbg",
		headerblock:[
		{
			textclass: "toptxt",
			textdata: data.string.clktxt
		}
	],
	imageblock:[{
		imagestoshow:[{
			imgid:"night_sky",
			imgclass:"background",
			imgsrc:'',
		}]
	}],
		imgandtext:[
		{
			imgtextadditionalclass: "firstblock forhover",
			imgclass: "aganim-01",
			imgid : 'coldbg',
			imgsrc: "",
			textdata: data.string.proto
		},
		{
			imgtextadditionalclass: "secondblock forhover",
			imgclass: "aganim-02",
			imgid : 'desertbg',
			imgsrc: "",
			textdata: data.string.porif
		},
		{
			imgtextadditionalclass: "thirdblock forhover",
			imgclass: "aganim-03",
			imgid : 'petbg',
			imgsrc: "",
			textdata: data.string.coele
		},
		{
			imgtextadditionalclass: "fourblock forhover",
			imgclass: "aganim-04",
			imgid : 'wildbg',
			imgsrc: "",
			textdata: data.string.platy
		},
		{
			imgtextadditionalclass: "fifthblock forhover",
			imgclass: "aganim-05",
			imgid : 'birdsbg',
			imgsrc: "",
			textdata: data.string.nemat
		},
	]
	}
];

$(function () {
	var $board = $('.board');
	var $board2 = $(".storydiv");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "corrimg", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrimg", src: "images/wrongicon.png", type: createjs.AbstractLoader.IMAGE},
			{id: "coldbg", src: imgpath+"new/animals04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "desertbg", src: imgpath+"new/animals02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "petbg", src: imgpath+"new/animals01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wildbg", src: imgpath+"new/animals03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "birdsbg", src: imgpath+"new/birds.png", type: createjs.AbstractLoader.IMAGE},
			{id: "night_sky", src: imgpath+"new/night_sky.jpg", type: createjs.AbstractLoader.IMAGE},

			{id: "snowfall_bg", src: imgpath+"bg_now_fall.png", type: createjs.AbstractLoader.IMAGE},
			{id: "snow_leopard", src: imgpath+"snow_leopard.png", type: createjs.AbstractLoader.IMAGE},
			{id: "yak", src: imgpath+"yak.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arcticfox", src: imgpath+"new/arcticfox.png", type: createjs.AbstractLoader.IMAGE},

			{id: "desert", src: imgpath+"new/bg10.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "camel", src: imgpath+"camel.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rat", src: imgpath+"new/rat.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rat_01", src: imgpath+"new/rat_01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow_black", src: imgpath+"arrow_black.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fox_half", src: imgpath+"new/fox_half.png", type: createjs.AbstractLoader.IMAGE},
			{id: "camel_head", src: imgpath+"new/camel_head.png", type: createjs.AbstractLoader.IMAGE},

			{id: "bg_grass_land", src: imgpath+"new/bg09.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "buffalo", src: imgpath+"new/buffalo.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bull", src: imgpath+"new/bull.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cow", src: imgpath+"new/cow01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rabbit", src: imgpath+"new/rabbit.png", type: createjs.AbstractLoader.IMAGE},
			{id: "goat", src: imgpath+"new/goat.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pig", src: imgpath+"new/pig.png", type: createjs.AbstractLoader.IMAGE},
			{id: "horse", src: imgpath+"new/horse.png", type: createjs.AbstractLoader.IMAGE},

			{id: "bg_jungle", src: imgpath+"new/bg11.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "bear", src: imgpath+"new/bear.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fox", src: imgpath+"new/fox01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "elephant", src: imgpath+"new/elephant.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tiger", src: imgpath+"new/tiger.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tiger_half", src: imgpath+"new/tiger01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow_white", src: imgpath+"new/arrow_white.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rhino", src: imgpath+"new/rhino.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spider", src: imgpath+"new/spider.png", type: createjs.AbstractLoader.IMAGE},
			{id: "lizard", src: imgpath+"new/lizards.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hanging_monkey", src: imgpath+"new/hanging_monkey.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fox", src: imgpath+"new/fox01.png", type: createjs.AbstractLoader.IMAGE},


			{id: "bg_birds", src: imgpath+"new/bg15.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "peacock", src: imgpath+"new/peacock.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pigeon", src: imgpath+"new/pigeon.png", type: createjs.AbstractLoader.IMAGE},
			{id: "grasshopper", src: imgpath+"new/grasshooper.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mosquito", src: imgpath+"new/mosquito.png", type: createjs.AbstractLoader.IMAGE},
			{id: "housefly", src: imgpath+"new/housefly.png", type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src:"images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wrong", src:"images/wrong.png", type: createjs.AbstractLoader.IMAGE},

			{id: "globe", src: imgpath+"new/globe.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "spider_gif", src: imgpath+"new/spider.gif", type: createjs.AbstractLoader.IMAGE},



			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s2_p1", src: soundAsset+"s2_p1.ogg"},
			{id: "s2_p2", src: soundAsset+"s2_p2.ogg"},
			{id: "s2_p3", src: soundAsset+"s2_p3.ogg"},
			{id: "s2_p3_cold region_1", src: soundAsset+"s2_p3_cold region_1.ogg"},
			{id: "s2_p3_cold region_2", src: soundAsset+"s2_p3_cold region_2.ogg"},
			{id: "s2_p3_cold region_3", src: soundAsset+"s2_p3_cold region_3.ogg"},
			{id: "s2_p3_cold region_4", src: soundAsset+"s2_p3_cold region_4.ogg"},
			{id: "s2_p3_deserts_1", src: soundAsset+"s2_p3_deserts_1.ogg"},
			{id: "s2_p3_deserts_2", src: soundAsset+"s2_p3_deserts_2.ogg"},
			{id: "s2_p3_deserts_3", src: soundAsset+"s2_p3_deserts_3.ogg"},
			{id: "s2_p3_deserts_4", src: soundAsset+"s2_p3_deserts_4.ogg"},
			{id: "s2_p3_fly_1", src: soundAsset+"s2_p3_fly_1.ogg"},
			{id: "s2_p3_fly_2", src: soundAsset+"s2_p3_fly_2.ogg"},
			{id: "s2_p3_fly_3", src: soundAsset+"s2_p3_fly_3.ogg"},
			{id: "s2_p3_jungles_1", src: soundAsset+"s2_p3_jungles_1.ogg"},
			{id: "s2_p3_jungles_2", src: soundAsset+"s2_p3_jungles_2.ogg"},
			{id: "s2_p3_jungles_3", src: soundAsset+"s2_p3_jungles_3.ogg"},
			{id: "s2_p3_jungles_4", src: soundAsset+"s2_p3_jungles_4.ogg"},
			{id: "s2_p3_jungles_5", src: soundAsset+"s2_p3_jungles_5.ogg"},
			{id: "s2_p3_jungles_6", src: soundAsset+"s2_p3_jungles_6.ogg"},
			{id: "s2_p3_wekeep_1", src: soundAsset+"s2_p3_wekeep_1.ogg"},
			{id: "s2_p3_wekeep_2", src: soundAsset+"s2_p3_wekeep_2.ogg"},
			{id: "s2_p3_wekeep_3", src: soundAsset+"s2_p3_wekeep_3.ogg"},
			{id: "s2_p3_wekeep_4", src: soundAsset+"s2_p3_wekeep_4.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	var storyCounter = 0;
	function generaltemplate() {
		var myCurrClass;
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);

		$(".imgtextcontainer").click(function(){
			$myStory = $(this);
			myCurrClass = $myStory.attr('class').split(' ')[1];
			if($myStory.hasClass("forhover")){
				storyNext = 0;
				//$myStory.addClass("disabled").removeClass("forhover");

			if($myStory.hasClass("firstblock")){
				generalStoryTemplate(coldAnimals);
			}
			else if($myStory.hasClass("secondblock")){
				generalStoryTemplate(desertAnimals);
			}
			else if($myStory.hasClass("thirdblock")){
				generalStoryTemplate(grassAnimals);
			}
			else if($myStory.hasClass("fourblock")){
				generalStoryTemplate(jungleAnimals);
			}
			else if($myStory.hasClass("fifthblock")){
				generalStoryTemplate(birds);
			}
			}
		});

    // remove ;ater
		// $(".firstblock").trigger("click");

		function generalStoryTemplate(currentstory){
			$(".storydiv").show(0);
			var source2 = $("#story-template").html();
			var template2 = Handlebars.compile(source2);
			var html2 = template2(currentstory[storyNext]);
			//$board2.html(html2);

			$(".storydiv").html(html2);
			texthighlight($board);
			storynavigation();
			put_image3(currentstory, storyNext);

			switch (currentstory) {
				case coldAnimals:
					switch (storyNext) {
						case 0:
							$(".storynxt").hide(0);
							sound_player("s2_p3_cold region_1");
							$('.image').css({"cursor":"auto"});
						break;
						case 1:
							$(".storynxt").hide(0);
							sound_player("s2_p3_cold region_2");
							$('.image').css({"cursor":"auto"});

						break;
						case 2:
							$(".storynxt").hide(0);
							sound_player("s2_p3_cold region_3");
							$('.image').css({"cursor":"auto"});

						break;
						case 3:
						$(".closebtn").hide(0);
							$(".intFact").addClass("slideRight");
							$('.image').css({"cursor":"auto"});
							$(".intFactTxt").addClass("fadeIn");
							$(".intFactTxt").css("opacity","0");
							setTimeout(function(){
								// createjs.Sound.stop();
								// current_sound = createjs.Sound.play("sound_1");
								// current_sound.play();
								// current_sound.on('complete', function(){

										createjs.Sound.stop();
										current_sound_1 = createjs.Sound.play("s2_p3_cold region_4");
										current_sound_1.play();
										current_sound_1.on('complete', function(){
											$(".closebtn").show(0);
										});
								// });
							},4000);
						break;
						default:

					}
				break;
				case desertAnimals:
					switch (storyNext) {
						case 0:
							$(".storynxt").hide(0);
							sound_player("s2_p3_deserts_1");
							$('.image').css({'cursor':'auto'});
						break;
						case 1:
							$(".storynxt").hide(0);
							$(".intFactTxt").css("opacity","0");
									createjs.Sound.stop();
									current_sound = createjs.Sound.play("s2_p3_deserts_2");
									current_sound.play();
									current_sound.on('complete', function(){
										$(".clickArea").click(function(){
												$(".hidden").removeClass("hidden");
												$(".intFactTxt").addClass("fadeIn");

												$(".intFact").addClass("slideRight");
												// sound_player("s2_p3_deserts_1");
												setTimeout(function(){
														createjs.Sound.stop();
														current_sound = createjs.Sound.play("s2_p3_deserts_3");
														current_sound.play();
														current_sound.on('complete', function(){
																$(".storynxt").show(0);
														});
												},4000);
										});

									});
						break;
						case 2:
							$(".fontTrVw").css("opacity", "0");
							$(".closebtn").hide(0);
							$(".fontTrVw").addClass("fadeIn");
								$(".hidden").removeClass("hidden");
								createjs.Sound.stop();
								current_sound = createjs.Sound.play("s2_p3_deserts_4");
								current_sound.play();
								setTimeout(function(){
									$(".btmtxt, .rat_01").addClass("slideup");
								},11000);
								current_sound.on('complete', function(){
									$(".closebtn").show(0);
								});
						break;
						default:

					}
				break;
				case grassAnimals:
					switch (storyNext) {
						case 0:
							$(".storynxt").hide(0);
							$(".image").css({"cursor":"none"});
							sound_player("s2_p3_wekeep_1");
						break;
						case 1:
								createjs.Sound.stop();
								current_sound = createjs.Sound.play("s2_p3_wekeep_2");
								current_sound.play();
							$(".bflclk").click(function(){
								$(".bufCorrect").removeClass("hidden");
								play_correct_incorrect_sound(true);
								});
							$(".cwclk").click(function(){
								$(".cowCorrect").removeClass("hidden");
								play_correct_incorrect_sound(true);
								});
							$(".gtclk").click(function(){
								$(".goatCorrect").removeClass("hidden");
								play_correct_incorrect_sound(true);
								});
							$(".pgclk").click(function(){
								$(".pigWrong").removeClass("hidden");
								play_correct_incorrect_sound(false);
								});
							$(".rbtclk").click(function(){
								$(".rabitWrong").removeClass("hidden");
								play_correct_incorrect_sound(false);
								});
						break;
						case 2:
							$(".storynxt").hide(0);
							$(".image").css({"cursor":"none"});
							sound_player("s2_p3_wekeep_3");
						break;
						case 3:
							$(".closebtn").hide(0);
							$(".image").css({"cursor":"none"});
							$(".storynxt").hide(0);
							sound_player_sec("s2_p3_wekeep_4");
						break;
						default:

					}
				break;
				case jungleAnimals:
					switch (storyNext) {
						case 0:
							$(".storynxt").hide(0);
							sound_player("s2_p3_jungles_1");
							$('.image').css({"cursor":"auto"});

						break;
						case 1:
							$(".storynxt").hide(0);
							// sound_player("s2_p3_jungles_2");
							createjs.Sound.stop();
							current_sound = createjs.Sound.play("s2_p3_jungles_2");
							current_sound.play();
							current_sound.on('complete', function(){
								$(".tiger").click(function(){
									storyNext++;
									generalStoryTemplate(currentstory);
								});
							});
						break;
						case 2:
							$(".storynxt").hide(0);
							$('.image').css({"cursor":"auto"});
							setTimeout(function(){$(".storynxt").show(0);},7000);
							$(".tiger_half").addClass("slideFromLeft");
						break;
						case 3:
							$(".storynxt").hide(0);
						$('.image').css({"cursor":"auto"});
						sound_player("s2_p3_jungles_3");
						break;
						case 4:
							$(".storynxt").hide(0);
						$('.image').css({"cursor":"auto"});
						sound_player("s2_p3_jungles_4");
						break;
						case 5:
							$(".storynxt").hide(0);
						$('.image').css({"cursor":"auto"});
							$(".storynxt").hide(0);
							$(".hanging_monkey").css("top", "-28%");
								createjs.Sound.stop();
								current_sound = createjs.Sound.play("s2_p3_jungles_5");
								current_sound.play();
								current_sound.on('complete', function(){
									$(".hanging_monkey").animate({
										top: "28%"
									},2000);
									setTimeout(function(){
										$(".storynxt").show(0);
									},2000);
								});
						break;
						case 6:
							$(".closebtn").hide(0);
							createjs.Sound.stop();
							current_sound = createjs.Sound.play("s2_p3_jungles_6");
							current_sound.play();
							current_sound.on('complete', function(){
								$(".closebtn").show(0);
								$(".spider").addClass("hide");
								$(".spd_gf").removeClass("hide");
							});
						break;
						default:
					}
					break;
				case birds:
					switch (storyNext) {
						case 0:
							$(".storynxt").hide(0);
							$('.image').css({"cursor":"auto"});
								createjs.Sound.stop();
								current_sound = createjs.Sound.play("s2_p3_fly_1");
								current_sound.play();
								current_sound.on('complete', function(){
									$(".arw_nest, .nest").addClass("fadeIn");
									$(".storynxt").show(0);
								});
						break;
						case 1:
							$(".storynxt").hide(0);
							sound_player("s2_p3_fly_2");
							$(".image").css({"cursor":"none"});

						break;
						case 2:
						$(".image").css({"cursor":"none"});
							$(".closebtn").hide(0);
							sound_player_sec("s2_p3_fly_3");
						break;
						default:

					}
					break;
				default:

			}


			$(".closebtn").click(function(){
				$("."+myCurrClass).addClass("disabled");
				if(($("."+myCurrClass)).hasClass("read")){
				storyCounter;
				}
				else{
					storyCounter++;
				}
				$("."+myCurrClass).addClass("read");
				if(storyCounter == 5)
					navigationcontroller();
				$(this).parent().fadeOut();
			});

			$(".storynxt").on("click", function(){
				storyNext++;
				generalStoryTemplate(currentstory);
			});

			$(".storyprev").on("click", function(){
				storyNext--;
				generalStoryTemplate(currentstory);
			});

			function storynavigation(){
					if(storyNext == 0){
						$(".storyprev").hide(0);
						$(".storynxt").show(0);
					}
					else if(storyNext == currentstory.length - 1){
						$(".storynxt").hide(0);
						$(".storyprev").show(0);
						$(".closebtn").show(0);
					}
					else{
						$(".storyprev").show(0);
						$(".storynxt").show(0);
					}
			}
		}

		switch(countNext){
			case 0:
				$nextBtn.hide(0);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s2_p1");
				current_sound.play();
				current_sound.on('complete', function(){
					// $nextBtn.show(0);
				});
			var s = Snap.select("#earthSvg");
			var svg = Snap.load(preload.getResult('globe').src, function(loadedFragment){
				s.append(loadedFragment);
				var $land = $("#land");
				$("#land").on('mouseover', function(){
					$("#land").css("cursor", 'pointer');
				});
				$land.click(function(){
					countNext++;
					templatecaller();
				});
			});
			break;
			case 1:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s2_p2");
				current_sound.play();
				current_sound.on('complete', function(){
					$nextBtn.show(0);
				});
			break;
			case 2:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s2_p3");
				current_sound.play();
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".storynxt").show(0);
		});
	}
	function sound_player_sec(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".closebtn").show(0);
		});
	}
	function sound_player_duo(sound_id_1, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id_1);
		current_sound.play();
		current_sound.on('complete', function(){
			sound_player(sound_id_2);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('imgandtext')){
			var imgandtext = content[count].imgandtext;
			for(var i=0; i<imgandtext.length; i++){
				var image_src = preload.getResult(imgandtext[i].imgid).src;
				//get list of classes
				var classes_list = imgandtext[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}

	function put_image3(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[

	//slide1
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: "bg",
			extratextblock:[
			{
				textclass: "p1s0 green_3",
				textdata:data.string.chaptername
			}],
			imageblock:[{
				imagestoshow:[
					{
						imgclass: "background",
						imgid : 'cover',
						imgsrc: ""
					}
				]
			}]
		},
	// slide2
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: "bg2",
			extratextblock:[
			{
				textclass: "toptxt red slideDown",
				textdata: data.string.p1s0top
			},{
				textclass: "btmtxt txt-0",
				textdata: data.string.p1s0btm
			}],
			imageblock:[{
				imagestoshow:[
					{
						imgclass: "image img0",
						imgid : 'cow',
						imgsrc: ""
					},{
						imgclass: "image img2",
						imgid : 'dog',
						imgsrc: ""
					},{
						imgclass: "image img1",
						imgid : 'tortoise',
						imgsrc: ""
					},{
						imgclass: "image img3",
						imgid : 'dolphin',
						imgsrc: ""
					}]
			}]
		},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg2",
		extratextblock:[
		{
			textclass: "toptxt red slideDown",
			textdata: data.string.p1s1top
		},{
			textclass: "btntext trst",
			textdata: data.string.trst
		},{
			textclass: "btntext aqtc",
			textdata: data.string.aqtc
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "image img0",
					imgid : 'cow',
					imgsrc: ""
				},{
					imgclass: "image img1",
					imgid : 'tortoise',
					imgsrc: ""
				},{
					imgclass: "image img2",
					imgid : 'dog',
					imgsrc: ""
				},{
					imgclass: "image img3",
					imgid : 'dolphin',
					imgsrc: ""
				}]
			}]
		},
	//slide 4
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: "bg2",
			extratextblock:[
			{
				textclass: "toptxt green slideDown",
				textdata: data.string.trst
			},{
				textclass: "midtxt",
				textdata: data.string.p1s4
			}],
			imageblock:[{
				imagestoshow:[
					{
						imgclass: "background",
						imgid : 'background_1',
						imgsrc: ""
					},
					{
						imgclass: "img cow",
						imgid : 'cow',
						imgsrc: ""
					},
					{
						imgclass: "img dog",
						imgid : 'dog',
						imgsrc: ""
					}]
			}]
		},
	//slide 5
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: "bg2",
			extratextblock:[
			{
				textclass: "toptxt blue slideDown",
				textdata: data.string.aqtc
			},{
				textclass: "midtxt",
				textdata: data.string.p1s5
			}],
			imageblock:[{
				imagestoshow:[
					{
						imgclass: "background top",
						imgid : 'sea_bg',
						imgsrc: ""
					},
					{
						imgclass: "img dolphin",
						imgid : 'dolphin',
						imgsrc: ""
					},
					{
						imgclass: "img tortoise",
						imgid : 'tortoise',
						imgsrc: ""
					}
				]
			}]
		},
	//slide 6
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: "bg2",
			extratextblock:[
				{
					textclass: "fd3 blktxt rightBlock",
					textdata: data.string.aqtc
				},{
					textclass: "fd1 blktxt leftBlock green",
					textdata: data.string.trst
				},{
					textclass: "fd4 btmBlk livWtr",
					textdata: data.string.p1s6_2
				},{
					textclass: "fd2 btmBlk livLand",
					textdata: data.string.p1s6_1
				}
			],
			imageblock:[{
				imagestoshow:[
					{
						imgclass: "blkImg rightBlock",
						imgid : 'sea_bg01',
						imgsrc: ""
					},{
						imgclass: "blkImg leftBlock",
						imgid : 'cow_dog_01',
						imgsrc: ""
					},
				]
			}]
		},
	//slide 7
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: "bg2",
			extratextblock:[
				{
					textclass: "midtxt_1 orange",
					textdata: data.string.p1s7
				}
			],
			imageblock:[{
				imagestoshow:[
					{
						imgclass: "background",
						imgid : 'bg12',
						imgsrc: ""
					}
				]
			}]
	},
 	];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "cover", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow_black", src: imgpath+"arrow_black02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dolphin", src: imgpath+"dolphin.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tortoise", src: imgpath+"tortle.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cow", src: imgpath+"cow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cow_01", src: imgpath+"cow01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dog", src: imgpath+"dog.png", type: createjs.AbstractLoader.IMAGE},
			{id: "background_1", src: imgpath+"new/bg07.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "sea_bg", src: imgpath+"new/bg06.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "sea_bg01", src: imgpath+"new/see_bg01.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "cow_dog_01", src: imgpath+"new/cow_dog_01.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "bg12", src: imgpath+"new/bg12.jpg", type: createjs.AbstractLoader.IMAGE},

			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
			{id: "s1_p2_1", src: soundAsset+"s1_p2_1.ogg"},
			{id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
			{id: "s1_p3_1", src: soundAsset+"s1_p3_1.ogg"},
			{id: "s1_p3_2", src: soundAsset+"s1_p3_2.ogg"},
			{id: "s1_p4", src: soundAsset+"s1_p4.ogg"},
			{id: "s1_p4_1", src: soundAsset+"s1_p4_1.ogg"},
			{id: "s1_p5", src: soundAsset+"s1_p5.ogg"},
			{id: "s1_p5_1", src: soundAsset+"s1_p5_2.ogg"},
			{id: "s1_p6", src: soundAsset+"s1_p6.ogg"},
			{id: "s1_p6_1", src: soundAsset+"s1_p6_1.ogg"},
			{id: "s1_p6_2", src: soundAsset+"s1_p6_2.ogg"},
			{id: "s1_p6_3", src: soundAsset+"s1_p6_3.ogg"},
			{id: "s1_p7", src: soundAsset+"s1_p7.ogg"},
			{id: "s1_p7_2", src: soundAsset+"s1_p7_2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	// $nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		var count;

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);

		switch(countNext){
			case 0:
				sound_player("s1_p1",1);
			break;
			case 1:
				createjs.Sound.stop();
			 	current_sound = createjs.Sound.play("s1_p2");
				current_sound.play();
				current_sound.on('complete',function(){
					$(".btmtxt").addClass("slideUp");
					$(".img0").addClass("fadein_1");
					$(".img1").addClass("fadein_3");
					$(".img2").addClass("fadein_2");
					$(".img3").addClass("fadein_4");
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("s1_p2_1");
						current_sound.play();
						current_sound.on('complete', function(){
							nav_button_controls(300);
						});
				});
			break;
			case 2:
			setTimeout(function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s1_p3");
				current_sound.play();
				current_sound.on('complete', function(){
						$(".img0, .img2").addClass("fadein_0");
						$(".trst").addClass("fadein_0");
							createjs.Sound.stop();
							 current_sound = createjs.Sound.play("s1_p3_1");
							 	current_sound.play();
							 	current_sound.on('complete', function(){
									$(".img1, .img3").addClass("fadein_0");
									$(".aqtc").addClass("fadein_0");
									createjs.Sound.stop();
									 current_sound = createjs.Sound.play("s1_p3_2");
									current_sound.on('complete', function(){
										nav_button_controls(300);
									});
							});
				});
			},500);
			break;
			case 3:
			setTimeout(function(){
				createjs.Sound.stop();
				 current_sound = createjs.Sound.play("s1_p4");
				current_sound.play();
				current_sound.on('complete', function(){
						$(".midtxt").addClass("fadein_0");
							createjs.Sound.stop();
						 current_sound = createjs.Sound.play("s1_p4_1");
							current_sound.play();
							current_sound.on('complete', function(){
								nav_button_controls(300);
							});
				});
			},500);
			break;
			case 4:
			setTimeout(function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s1_p5");
				current_sound.play();
				current_sound.on('complete', function(){
						$(".midtxt").addClass("fadein_0");
							createjs.Sound.stop();
							current_sound = createjs.Sound.play("s1_p5_1");
							current_sound.play();
							current_sound.on('complete', function(){
								nav_button_controls(300);
							});
						});
			},500);
			break;
			case 5:
				$(".fd1").addClass("fadein_0");
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s1_p6");
				current_sound.play();
				current_sound.on('complete', function(){
					$(".fd2").addClass("fadein_0");
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s1_p6_1");
					current_sound.play();
					current_sound.on('complete', function(){
						$(".fd3").addClass("fadein_0");
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("s1_p6_2");
						current_sound.play();
						current_sound.on('complete', function(){
							$(".fd4").addClass("fadein_0");
							createjs.Sound.stop();
							current_sound = createjs.Sound.play("s1_p6_3");
							current_sound.play();
							current_sound.on('complete', function(){
								nav_button_controls(300);
							});
						});
					});
				});
			break;
			case 6:
			setTimeout(function(){
				sound_player("s1_p7",1);
			},500);
			break;

			default:
			sound_player("s1_p"+(countNext+1),1);
			break;
		}
	}


	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls(300):"";
		});
	}

	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

//TO DO unused function
	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//console.log("imgsrc---"+image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

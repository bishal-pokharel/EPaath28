var coldAnimals=[
	// slide1
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.cold_top_txt
			},
			{
			textclass: "anmlDesc",
			textdata: data.string.cold_midtxt_1
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "imgbg",
						imgid : 'snowfall_bg',
						imgsrc: "",
					},
					{
						imgclass: "image leopard",
						imgid : 'snow_leopard',
						imgsrc: "",
					},
					{
						imgclass: "image yak",
						imgid : 'yak',
						imgsrc: "",
					}
				]
			}
		],
	},
	// slide2
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.cold_top_txt
			},
			{
			textclass: "anmlDesc",
			textdata: data.string.cold_midtxt_2
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "imgbg",
						imgid : 'snowfall_bg',
						imgsrc: "",
					},
					{
						imgclass: "image leopard",
						imgid : 'snow_leopard',
						imgsrc: "",
					},
					{
						imgclass: "image yak",
						imgid : 'yak',
						imgsrc: "",
					}
				]
			}
		],
	},
	// slide3
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.cold_top_txt
			},
			{
			textclass: "anmlDesc",
			textdata: data.string.cold_midtxt_3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "imgbg",
						imgid : 'snowfall_bg',
						imgsrc: "",
					},
					{
						imgclass: "image leopard",
						imgid : 'snow_leopard',
						imgsrc: "",
					},
					{
						imgclass: "image yak",
						imgid : 'yak',
						imgsrc: "",
					}
				]
			}
		],
	},
	// slide4
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.cold_top_txt
			},
			{
				datahighlightflag:'true',
				datahighlightcustomclass:"intFactTxt",
				textclass: "intFact",
				textdata: data.string.cold_int_fact
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "imgbg topTen",
						imgid : 'snowfall_bg',
						imgsrc: "",
					},
					{
						imgclass: "image leopard arcticfox",
						imgid : 'arcticfox',
						imgsrc: "",
					}
				]
			}
		],
	},
]

var desertAnimals = [
	// slide1
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.desert_top_txt
			},
			{
			textclass: "anmlDesc",
			textdata: data.string.desert_midtxt_1
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "imgbg",
						imgid : 'desert',
						imgsrc: "",
					},
					{
						imgclass: "image camel",
						imgid : 'camel',
						imgsrc: "",
					},
					{
						imgclass: "image rat",
						imgid : 'rat',
						imgsrc: "",
					}
				]
			}
		],
	},
	// slide2
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.desert_top_txt
			},
			{
				textclass: "anmlDesc fontTrVw silverBg",
				textdata: data.string.des_clk_txt
			},
			{
				textclass: "clickArea",
			},
			{
				textclass: "hidden humpTxt",
				textdata: data.string.hump
			},
			{
				datahighlightflag:'true',
				datahighlightcustomclass:"intFactTxt",
				textclass: "intFact topThrty",
				textdata: data.string.desert_int_fact
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "imgbg",
						imgid : 'desert',
						imgsrc: "",
					},
					{
						imgclass: "image camel",
						imgid : 'camel',
						imgsrc: "",
					},
					{
						imgclass: "image rat",
						imgid : 'rat',
						imgsrc: "",
					},
					{
						imgclass: "hidden arrow",
						imgid : 'arrow_black',
						imgsrc: "",
					}
				]
			}
		],
	},
	// slide3
	{
		singletext:[
			{
				textclass: "background black",
			},
			{
				textclass: "strytextclass",
				textdata: data.string.desert_top_txt
			},
			{
				textclass: "btmtxt",
				textdata: data.string.desert_lslide_1
			},
			{
				datahighlightflag:'true',
				datahighlightcustomclass:'fontTrVw',
				textclass: "hidden if_sec",
				textdata: data.string.des_if_sec
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "halfimg foxHalf",
						imgid : 'fox_half',
						imgsrc: "",
					},
					{
						imgclass: "halfimg camelHalf",
						imgid : 'camel_head',
						imgsrc: "",
					},
					{
						imgclass: "rat_01",
						imgid : 'rat_01',
						imgsrc: "",
					}
				]
			}
		],
	},
]

var grassAnimals = [
	// slide1
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.pet_top_txt
			},
			{
			textclass: "anmlDesc",
			textdata: data.string.pet_midtxt_1
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "imgbg",
						imgid : 'bg_grass_land',
						imgsrc: "",
					},
					{
						imgclass: "image buffalo",
						imgid : 'buffalo',
						imgsrc: "",
					},
					{
						imgclass: "image cow",
						imgid : 'cow',
						imgsrc: "",
					},
					{
						imgclass: "image pig",
						imgid : 'pig',
						imgsrc: "",
					},
					{
						imgclass: "image goat",
						imgid : 'goat',
						imgsrc: "",
					},
					{
						imgclass: "image rabbit",
						imgid : 'rabbit',
						imgsrc: "",
					}
				]
			}
		],
	},
	// slide2
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.pet_top_txt
			},
			{
			textclass: "anmlDesc silverBg",
			textdata: data.string.pet_midtxt_2
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "imgbg",
						imgid : 'bg_grass_land',
						imgsrc: "",
					},
					{
						imgclass: "image buffalo bflclk",
						imgid : 'buffalo',
						imgsrc: "",
					},
					{
						imgclass: "image cow cwclk",
						imgid : 'cow',
						imgsrc: "",
					},
					{
						imgclass: "image pig pgclk",
						imgid : 'pig',
						imgsrc: "",
					},
					{
						imgclass: "image goat gtclk",
						imgid : 'goat',
						imgsrc: "",
					},
					{
						imgclass: "image rabbit rbtclk",
						imgid : 'rabbit',
						imgsrc: "",
					},
					{
						imgclass: "correct hidden bufCorrect",
						imgid : 'correct',
						imgsrc: "",
					},
					{
						imgclass: "correct hidden cowCorrect",
						imgid : 'correct',
						imgsrc: "",
					},
					{
						imgclass: "correct hidden goatCorrect",
						imgid : 'correct',
						imgsrc: "",
					},
					{
						imgclass: "correct hidden pigWrong",
						imgid : 'wrong',
						imgsrc: "",
					},
					{
						imgclass: "correct hidden rabitWrong",
						imgid : 'wrong',
						imgsrc: "",
					}
				]
			}
		],
	},
	// slide3
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.pet_top_txt
			},
			{
			textclass: "anmlDesc",
			textdata: data.string.pet_midtxt_3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "imgbg",
						imgid : 'bg_grass_land',
						imgsrc: "",
					},
					{
						imgclass: "image buffalo",
						imgid : 'buffalo',
						imgsrc: "",
					},
					{
						imgclass: "image cow",
						imgid : 'cow',
						imgsrc: "",
					},
					{
						imgclass: "image pig",
						imgid : 'pig',
						imgsrc: "",
					},
					{
						imgclass: "image goat",
						imgid : 'goat',
						imgsrc: "",
					},
					{
						imgclass: "image rabbit",
						imgid : 'rabbit',
						imgsrc: "",
					}
				]
			}
		],
	},
	// slide4
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.pet_top_txt
			},
			{
			textclass: "anmlDesc",
			textdata: data.string.pet_midtxt_4
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "imgbg",
						imgid : 'bg_grass_land',
						imgsrc: "",
					},
					{
						imgclass: "image buffalo",
						imgid : 'horse',
						imgsrc: "",
					},
					{
						imgclass: "image cow",
						imgid : 'bull',
						imgsrc: "",
					}
				]
			}
		],
	},
]

var jungleAnimals = [
	// slide1
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.wild_top_txt
			},
			{
			textclass: "anmlDesc",
			textdata: data.string.wild_midtxt_1
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "imgbg",
						imgid : 'bg_jungle',
						imgsrc: "",
					},
					{
						imgclass: "image tiger",
						imgid : 'tiger',
						imgsrc: "",
					},
					{
						imgclass: "image bear",
						imgid : 'bear',
						imgsrc: "",
					},
					{
						imgclass: "image fox",
						imgid : 'fox',
						imgsrc: "",
					}
				]
			}
		],
	},
	// slide2
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.wild_top_txt
			},
			{
			textclass: "anmlDesc silverBg",
			textdata: data.string.wild_midtxt_2
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "imgbg",
						imgid : 'bg_jungle',
						imgsrc: "",
					},
					{
						imgclass: "image tiger",
						imgid : 'tiger',
						imgsrc: "",
					},
					{
						imgclass: "image bear",
						imgid : 'bear',
						imgsrc: "",
					},
					{
						imgclass: "image fox",
						imgid : 'fox',
						imgsrc: "",
					}
				]
			}
		],
	},
	// slide3
	{
		singletext:[
			{
				textclass: "background black"
			},
			{
				textclass: "strytextclass",
				textdata: data.string.wild_top_txt
			},
			{
				textclass: "bodyPart teeth",
				textdata: data.string.teeth
			},
			{
				textclass: "bodyPart claw",
				textdata: data.string.claw
			},
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "image tiger_half",
						imgid : 'tiger_half',
						imgsrc: "",
					},{
						imgclass: "arrow arwtth",
						imgid : 'arrow_white',
						imgsrc: "",
					},{
						imgclass: "arrow arwclw",
						imgid : 'arrow_white',
						imgsrc: "",
					}
				]
			}
		],
	},
	// slide4
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.wild_top_txt
			},
			{
			textclass: "anmlDesc",
			textdata: data.string.wild_midtxt_4
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "imgbg",
						imgid : 'bg_jungle',
						imgsrc: "",
					},
					{
						imgclass: "image tiger",
						imgid : 'tiger',
						imgsrc: "",
					},
					{
						imgclass: "image bear",
						imgid : 'bear',
						imgsrc: "",
					},
					{
						imgclass: "image fox",
						imgid : 'fox',
						imgsrc: "",
					}
				]
			}
		],
	},
	// slide5
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.wild_top_txt
			},
			{
			textclass: "anmlDesc",
			textdata: data.string.wild_midtxt_5
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "imgbg",
						imgid : 'bg_jungle',
						imgsrc: "",
					},
					{
						imgclass: "image elephant",
						imgid : 'elephant',
						imgsrc: "",
					},
					{
						imgclass: "image rhino",
						imgid : 'rhino',
						imgsrc: "",
					}
				]
			}
		],
	},
	// slide6
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.wild_top_txt
			},
			{
			textclass: "anmlDesc",
			textdata: data.string.wild_midtxt_6
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "imgbg",
						imgid : 'bg_jungle',
						imgsrc: "",
					},
					{
						imgclass: "image hanging_monkey",
						imgid : 'hanging_monkey',
						imgsrc: "",
					}
				]
			}
		],
	},
	// slide7
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.wild_top_txt
			},
			{
			textclass: "anmlDesc",
			textdata: data.string.wild_midtxt_7
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "imgbg",
						imgid : 'bg_jungle',
						imgsrc: "",
					},
					{
						imgclass: "image lizard",
						imgid : 'lizard',
						imgsrc: "",
					},
					{
						imgclass: "image spider",
						imgid : 'spider',
						imgsrc: "",
					},
					{
						imgclass: "image spd_gf hide",
						imgid : 'spider_gif',
						imgsrc: "",
					}
				]
			}
		],
	},
]

var birds = [
	// slide1
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.birds_top_txt
			},
			{
			textclass: "anmlDesc",
			textdata: data.string.birds_midtxt_1
			},
			{
			textclass: "nest",
			textdata: data.string.nest
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "imgbg",
						imgid : 'bg_birds',
						imgsrc: "",
					},
					{
						imgclass: "image peacock",
						imgid : 'peacock',
						imgsrc: "",
					},
					{
						imgclass: "arrow arw_nest",
						imgid : 'arrow_white',
						imgsrc: "",
					}
				]
			}
		],
	},
	// slide2
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.birds_top_txt
			},
			{
			textclass: "anmlDesc",
			textdata: data.string.birds_midtxt_2
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "imgbg",
						imgid : 'bg_birds',
						imgsrc: "",
					},
					{
						imgclass: "image pigeon",
						imgid : 'pigeon',
						imgsrc: "",
					},
				]
			}
		],
	},
	// slide3
	{
		singletext:[
			{
			textclass: "background gray",
			},
			{
			textclass: "strytextclass",
			textdata: data.string.birds_top_txt
			},
			{
			textclass: "anmlDesc heightThty",
			textdata: data.string.birds_midtxt_3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "image housefly",
						imgid : 'housefly',
						imgsrc: "",
					},{
						imgclass: "image grasshopper",
						imgid : 'grasshopper',
						imgsrc: "",
					},{
						imgclass: "image mosquito",
						imgid : 'mosquito',
						imgsrc: "",
					},
				]
			}
		],
	},
]

var AnnelStory = [
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.annel
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "intypcenimg",
						imgid : 'leech_new',
						imgsrc: "",
					}
				]
			}
		],
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.annel
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp6text1
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'anim06',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.annel
			},
			{
				textclass: "sidetext_sec",
				textdata: data.string.intyp6text2
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'leech_sucking_blood',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.annel
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp6text3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'leech_gif',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.annel
			},
			{
				textclass: "inner-mid-text",
				textdata: data.string.intyp6text4
			},
			{
				textclass: "threetext-1",
				textdata: data.string.intyp6text5
			},
			{
				textclass: "threetext-2",
				textdata: data.string.intyp6text6
			},
			{
				textclass: "threetext-3",
				textdata: data.string.intyp6text7
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "threeimg-1",
						imgid : 'earthworm',
						imgsrc: "",
					},
					{
						imgclass: "threeimg-2",
						imgid : 'anim06',
						imgsrc: "",
					},
					{
						imgclass: "threeimg-3",
						imgid : 'polychaete',
						imgsrc: "",
					}
				]
			}
		]
	},
//  quicktest
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.annel
			},
			{
				textclass: "qtStr",
				textdata: data.string.quicktest
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "bgQc",
						imgid : 'bg_quick_check',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.annel
			},
			{
				textclass: "inner-mid-text",
				textdata: data.string.intyp6text8
			},
			{
				textclass: "buttonsel hoverdiy diybtns-1",
				textdata: data.string.intyp6opt1,
				ans:"correct"
			},
			{
				textclass: "buttonsel hoverdiy diybtns-2",
				textdata: data.string.intyp6opt2
			},
			{
				textclass: "buttonsel hoverdiy diybtns-3",
				textdata: data.string.intyp6opt3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "diyimg",
						imgid : 'earthworm',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "summary",
				textdata: data.string.summary
			},
			{
				textclass: "pohead",
				textdata: data.string.intyp6text9
			}
		]
	}
]

var ArthrStory = [
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.arthr
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "intypcenimg",
						imgid : 'anim07',
						imgsrc: "",
					}
				]
			}
		],
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.arthr
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp7text1
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'anim07',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.arthr
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp7text2
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'anim07',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.arthr
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp7text3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'anim07',
						imgsrc: "",
					}
				]
			}
		]
	},
	//added
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.arthr
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp7text10
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'anim07',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.arthr
			},
			{
				textclass: "inner-mid-text",
				textdata: data.string.intyp7text4
			},
			{
				textclass: "threetext-1",
				textdata: data.string.intyp7text5
			},
			{
				textclass: "threetext-2",
				textdata: data.string.intyp7text6
			},
			{
				textclass: "threetext-3",
				textdata: data.string.intyp7text7
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "threeimg-1",
						imgid : 'anim07',
						imgsrc: "",
					},
					{
						imgclass: "threeimg-2",
						imgid : 'spider',
						imgsrc: "",
					},
					{
						imgclass: "threeimg-3",
						imgid : 'crab',
						imgsrc: "",
					}
				]
			}
		]
	},
	// quicktest
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.arthr
			},
			{
				textclass: "qtStr",
				textdata: data.string.quicktest
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "bgQc",
						imgid : 'bg_quick_check',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.arthr
			},
			{
				textclass: "inner-mid-text",
				textdata: data.string.intyp7text8
			},
			{
				textclass: "buttonsel hoverdiy diybtns-1",
				textdata: data.string.intyp7opt1
			},
			{
				textclass: "buttonsel hoverdiy diybtns-2",
				textdata: data.string.intyp7opt2
			},
			{
				textclass: "buttonsel hoverdiy diybtns-3",
				textdata: data.string.intyp7opt3,
				ans:"correct"
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "diyimg",
						imgid : 'anim07',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "summary",
				textdata: data.string.summary
			},
			{
				textclass: "pohead",
				textdata: data.string.intyp7text9
			}
		]
	}
]

var MolluStory = [
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.mollu
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "intypcenimg",
						imgid : 'anim08',
						imgsrc: "",
					}
				]
			}
		],
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.mollu
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp8text1
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg zoomin",
						imgid : 'anim08',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.mollu
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp8text10
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'anim08',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.mollu
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp8text2
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'anim08',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.mollu
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp8text3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'anim08',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.mollu
			},
			{
				textclass: "inner-mid-text",
				textdata: data.string.intyp8text4
			},
			{
				textclass: "threetext-1",
				textdata: data.string.intyp8text5
			},
			{
				textclass: "threetext-2",
				textdata: data.string.intyp8text6
			},
			{
				textclass: "threetext-3",
				textdata: data.string.intyp8text7
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "threeimg-1",
						imgid : 'anim08',
						imgsrc: "",
					},
					{
						imgclass: "threeimg-2",
						imgid : 'octopus',
						imgsrc: "",
					},
					{
						imgclass: "threeimg-3",
						imgid : 'slug',
						imgsrc: "",
					}
				]
			}
		]
	},
// quicktest
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.mollu
			},
			{
				textclass: "qtStr",
				textdata: data.string.quicktest
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "bgQc",
						imgid : 'bg_quick_check',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.mollu
			},
			{
				textclass: "inner-mid-text",
				textdata: data.string.intyp8text8
			},
			{
				textclass: "buttonsel hoverdiy diybtns-1",
				textdata: data.string.intyp8opt1
			},
			{
				textclass: "buttonsel hoverdiy diybtns-2",
				textdata: data.string.intyp8opt2,
				ans:"correct"
			},
			{
				textclass: "buttonsel hoverdiy diybtns-3",
				textdata: data.string.intyp8opt3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "diyimg",
						imgid : 'slug_gif',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "summary",
				textdata: data.string.summary
			},
			{
				textclass: "pohead",
				textdata: data.string.intyp8text9
			}
		]
	}
]

var EchinStory = [
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.echin
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "intypcenimg",
						imgid : 'anim09',
						imgsrc: "",
					}
				]
			}
		],
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.echin
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp9text1
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg zoominLarge",
						imgid : 'anim09',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.echin
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp9text2
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'anim09',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.echin
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp9text3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'stargif',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.echin
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp9text4
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'starFish_gif',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.echin
			},
			{
				textclass: "inner-mid-text",
				textdata: data.string.intyp9text5
			},
			{
				textclass: "threetext-1",
				textdata: data.string.intyp9text6
			},
			{
				textclass: "threetext-2",
				textdata: data.string.intyp9text7
			},
			{
				textclass: "threetext-3",
				textdata: data.string.intyp9text8
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "threeimg-1",
						imgid : 'anim09',
						imgsrc: "",
					},
					{
						imgclass: "threeimg-2",
						imgid : 'seaurchin',
						imgsrc: "",
					},
					{
						imgclass: "threeimg-3",
						imgid : 'seacucumber',
						imgsrc: "",
					}
				]
			}
		]
	},
// quicktest
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.echin
			},
			{
				textclass: "qtStr",
				textdata: data.string.quicktest
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "bgQc",
						imgid : 'bg_quick_check',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.echin
			},
			{
				textclass: "inner-mid-text",
				textdata: data.string.intyp9text9
			},
			{
				textclass: "buttonsel hoverdiy diybtns-1",
				textdata: data.string.intyp9opt1,
				ans:"correct"
			},
			{
				textclass: "buttonsel hoverdiy diybtns-2",
				textdata: data.string.intyp9opt2
			},
			{
				textclass: "buttonsel hoverdiy diybtns-3",
				textdata: data.string.intyp9opt3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "diyimg",
						imgid : 'anim09',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "summary",
				textdata: data.string.summary
			},
			{
				textclass: "pohead",
				textdata: data.string.intyp9text10
			}
		]
	}
]

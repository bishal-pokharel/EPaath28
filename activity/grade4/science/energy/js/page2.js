var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";


var content=[
	// slide0
	{
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "template-dialougebox2-right-white doctor_dialogue1b",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'lab1'
				},{
					imgclass : "profesor_1 p_png",
					imgsrc : '',
					imgid : 'profesor_1_png'
				},{
					imgclass : "profesor_1 p_gif",
					imgsrc : '',
					imgid : 'profesor_1_gif'
				}
			]
		}]

	},
	// slide1
	{
		uppertextblock:[{
			textdata: data.string.p2text2,
			textclass: "boardtext",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'lab1'
				},{
					imgclass : "profesor_1 p_png",
					imgsrc : '',
					imgid : 'profesor_1_png'
				},{
					imgclass : "profesor_1 p_gif",
					imgsrc : '',
					imgid : 'profesor_1_gif'
				}
			]
		}]

	},
	// slide2
	{
		uppertextblock:[{
			textdata: data.string.p2text2,
			textclass: "boardtext",
		},
		{
			textdata: data.string.p2text3,
			textclass: "template-dialougebox2-top-white doctor_dialogue1a",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'lab1'
				},{
					imgclass : "profesor_1 p_png",
					imgsrc : '',
					imgid : 'profesor_1_png'
				},{
					imgclass : "profesor_1 p_gif",
					imgsrc : '',
					imgid : 'profesor_1_gif'
				}
			]
		}]

	},
	// slide3
	{
		uppertextblock:[
		{
			textdata: data.string.p2text4,
			textclass: "template-dialougebox2-right-white doctor_dialogue1",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'lab2'
				},{
					imgclass : "profesor_2 p_png",
					imgsrc : '',
					imgid : 'profesor_2_png'
				},{
					imgclass : "profesor_2 p_gif",
					imgsrc : '',
					imgid : 'profesor_2_gif'
				}
			]
		}]

	},
	// slide4
	{
		uppertextblock:[
		{
			textdata: data.string.p2text4,
			textclass: "template-dialougebox2-right-white doctor_dialogue1",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'lab2'
				},
				{
					imgclass : "boardclass",
					imgsrc : '',
					imgid : 'board'
				}
			]
		}],

		imageandlabelblock:[{
			blockclass: "a1",
			imgsrc :imgpath+'bulb.png',
			imgclass:'imgclass',
			textclass:'textclass',
			textdata: data.string.p2text5
		},{
			blockclass: "a2",
			imgsrc :imgpath+'pole.png',
			imgclass:'imgclass',
			textclass:'textclass',
			textdata: data.string.p2text6
		},{
			blockclass: "a3",
			imgsrc :imgpath+'candel.png',
			imgclass:'imgclass',
			textclass:'textclass',
			textdata: data.string.p2text7
		},{
			blockclass: "a4",
			imgsrc :imgpath+'nuclearenergy.png',
			imgclass:'imgclass diff',
			textclass:'textclass',
			textdata: data.string.p2text8
		},{
			blockclass: "b1",
			imgsrc :imgpath+'mechanicalenergy.png',
			imgclass:'imgclass',
			textclass:'textclass',
			textdata: data.string.p2text9
		},{
			blockclass: "b2",
			imgsrc :imgpath+'heatenergy.png',
			imgclass:'imgclass',
			textclass:'textclass',
			textdata: data.string.p2text10
		},{
			blockclass: "b3",
			imgsrc :imgpath+'magnetenergy.png',
			imgclass:'imgclass',
			textclass:'textclass',
			textdata: data.string.p2text11
		},{
			blockclass: "b4",
			imgsrc :imgpath+'soundenergy.png',
			imgclass:'imgclass',
			textclass:'textclass',
			textdata: data.string.p2text12
		}]

	},
	// slide5
	{
		uppertextblock:[
		{
			textdata: data.string.p2text13,
			textclass: "bottomtext",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'board'
				}
			]
		}],

		imageandlabelblock:[{
			blockclass: "a1",
			imgsrc :imgpath+'bulb.png',
			imgclass:'imgclass',
			textclass:'textclass',
			textdata: data.string.p2text5
		},{
			blockclass: "a2",
			imgsrc :imgpath+'pole.png',
			imgclass:'imgclass',
			textclass:'textclass',
			textdata: data.string.p2text6
		},{
			blockclass: "a3",
			imgsrc :imgpath+'candel.png',
			imgclass:'imgclass',
			textclass:'textclass',
			textdata: data.string.p2text7
		},{
			blockclass: "a4",
			imgsrc :imgpath+'nuclearenergy.png',
			imgclass:'imgclass diff',
			textclass:'textclass',
			textdata: data.string.p2text8
		},{
			blockclass: "b1",
			imgsrc :imgpath+'mechanicalenergy.png',
			imgclass:'imgclass',
			textclass:'textclass',
			textdata: data.string.p2text9
		},{
			blockclass: "b2",
			imgsrc :imgpath+'heatenergy.png',
			imgclass:'imgclass',
			textclass:'textclass',
			textdata: data.string.p2text10
		},{
			blockclass: "b3",
			imgsrc :imgpath+'magnetenergy.png',
			imgclass:'imgclass',
			textclass:'textclass',
			textdata: data.string.p2text11
		},{
			blockclass: "b4",
			imgsrc :imgpath+'soundenergy.png',
			imgclass:'imgclass',
			textclass:'textclass',
			textdata: data.string.p2text12
		}]

	},
	// slide6
	{
		uppertextblock:[
		{
			textdata: data.string.p2text14,
			textclass: "topttext",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'board'
				}
			]
		}],

		imageandlabelblock:[{
			blockclass: "c1",
			imgsrc :imgpath+'fire.png',
			imgclass:'imgclass',
			textclass:'textclass',
			textdata: data.string.p2text15
		},{
			blockclass: "c2",
			imgsrc :imgpath+'fire01.png',
			imgclass:'imgclass',
			textclass:'textclass',
			textdata: data.string.p2text16
		},{
			blockclass: "c3",
			imgsrc :imgpath+'drying-clothers.png',
			imgclass:'imgclass',
			textclass:'textclass',
			textdata: data.string.p2text17
		},{
			blockclass: "c4",
			imgsrc :imgpath+'boiling-water.png',
			imgclass:'imgclass',
			textclass:'textclass',
			textdata: data.string.p2text18
		}]

	},


];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "lab1", src: imgpath+"lab01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "lab2", src: imgpath+"lab03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "board", src: imgpath+"board.png", type: createjs.AbstractLoader.IMAGE},
			{id: "profesor_1_gif", src: imgpath+"professor01.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "profesor_1_png", src: imgpath+"professor01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "profesor_2_gif", src: imgpath+"professor02.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "profesor_2_png", src: imgpath+"professor02.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"p2_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p2_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p2_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p2_s3.ogg"},
			{id: "sound_4_0", src: soundAsset+"p2_s4_0.ogg"},
			{id: "sound_4_1", src: soundAsset+"p2_s4_1.ogg"},
			{id: "sound_4_2", src: soundAsset+"p2_s4_2.ogg"},
			{id: "sound_4_3", src: soundAsset+"p2_s4_3.ogg"},
			{id: "sound_4_4", src: soundAsset+"p2_s4_4.ogg"},
			{id: "sound_4_5", src: soundAsset+"p2_s4_5.ogg"},
			{id: "sound_4_6", src: soundAsset+"p2_s4_6.ogg"},
			{id: "sound_4_7", src: soundAsset+"p2_s4_7.ogg"},
			{id: "sound_5", src: soundAsset+"p2_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p2_s6.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext) {
			case 0:
				sound_play_click('sound_0',".doctor_dialogue1");
				break;
			case 1:
				sound_play_click('sound_1',".doctor_dialogue1");
				break;
			case 2:
				sound_play_click('sound_2',".doctor_dialogue1");
				$('.template-dialougebox2-top-white').css({"top":"4%"});
				break;
			case 3:
				sound_play_click('sound_3',".doctor_dialogue1");
				break;
			case 4:
					setTimeout(function(){
						sound_play_click('sound_4');
					},800);
					$('.boardclass').animate({"width": "100%","height": "100%","left":"0","top":"0"},700);
					$('.doctor_dialogue1').animate({"opacity":"0"},500).css({"display":"none"});
					$('.a1, .a2, .a3, .a4, .b1, .b2, .b3, .b4').css({"opacity":"0"});
					setTimeout(function(){
					createjs.Sound.stop();
				 	current_sound_0 = createjs.Sound.play("sound_4_0");
					$(".a1").addClass("fade_In");
					current_sound_0.on("complete",function(){
						setTimeout(function(){
                            current_sound_1 = createjs.Sound.play("sound_4_1");
                            $(".a2").addClass("fade_In");
						current_sound_1.on("complete",function(){
                            setTimeout(function(){
						 	var current_sound_2 = createjs.Sound.play("sound_4_2");
							$(".a3").addClass("fade_In");
							current_sound_2.on("complete",function(){
                                setTimeout(function(){
							 	var current_sound_3 = createjs.Sound.play("sound_4_3");
								$(".a4").addClass("fade_In");
								current_sound_3.on("complete",function(){
                                    setTimeout(function(){
								 	var current_sound_4 = createjs.Sound.play("sound_4_4");
									$(".b1").addClass("fade_In");
									current_sound_4.on("complete",function(){
                                        setTimeout(function(){
									 	var current_sound_5 = createjs.Sound.play("sound_4_5");
										$(".b2").addClass("fade_In");
										current_sound_5.on("complete",function(){
                                            setTimeout(function(){
										 	var current_sound_5 = createjs.Sound.play("sound_4_6");
											$(".b3").addClass("fade_In");
											current_sound_5.on("complete",function(){
                                                setTimeout(function(){
											 	var current_sound_6 = createjs.Sound.play("sound_4_7");
												$(".b4").addClass("fade_In");
												current_sound_6.on("complete",function(){
												nav_button_controls(0);
												})
                                            },1000);
											})
                                        },1000);
										})
                                    },1000);
									})
                                },1000);
								})
                            },1000);
							})
                            },1000);
						})
                        },1000);
					})
					},1000);
					break;
			case 5:
					setTimeout(function(){
						sound_play_click('sound_5');
					},800);
					$('.b2').animate({"left":"35%","width":"30%","height":"50%","top":"25%"},700);
					$('.a1, .a2, .a3, .a4, .b1, .b3, .b4').animate({"opacity":"0"},500).css({"display":"none"});
					$('.bottomtext').css({"bottom":"-25%"}).delay(500).animate({"bottom":"0%"},500);

					break;
			case 6:
				sound_play_click('sound_6');
				break;
			default:
				$prevBtn.show(0);
				sound_player('sound_2');
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_play_click(sound_id, click_class){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			$(".p_gif").hide(0);
			$(".p_png").show(0);
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					var currrent_sound_1=createjs.Sound.play(sound_id);
					$(".p_gif").show(0);
					$(".p_png").hide(0);
						currrent_sound_1.on("complete", function(){
							$(".p_gif").hide(0);
							$(".p_png").show(0);
					});
				});
			}
			nav_button_controls(0);
		});
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

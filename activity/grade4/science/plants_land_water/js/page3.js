var imgpath = $ref + "/images/";
if($lang == "en"){
	soundAsset = $ref+"/sounds/en/";
}
else if($lang == "np")
{
	soundAsset = $ref+"/sounds/np/";
}

var sound_dg1 = new buzz.sound((soundAsset + "p3_s1.ogg"));
var sound_dg2 = new buzz.sound((soundAsset + "p3_s3.ogg"));
var sound_dg3 = new buzz.sound((soundAsset + "p3_s4.ogg"));
var sound_dg4 = new buzz.sound((soundAsset + "p3_s5.ogg"));
var sound_dg5 = new buzz.sound((soundAsset + "p3_s6.ogg"));
var sound_dg6 = new buzz.sound((soundAsset + "p3_s7.ogg"));
var sound_dg7 = new buzz.sound((soundAsset + "p3_s8.ogg"));
var content=[
{
	//slide0
	contentblockadditionalclass: "contentwithbg1",
},
{
	//slide1
	contentblockadditionalclass: "contentwithbg1-1",
	uppertextblock:[
	{
		textclass : 'template-dialougebox2-left-yellow dg-2',
		textdata : data.string.p3text0,
	}
	],
	imageblock:[
		{
			imagetoshow:[
			{
				imgclass: "malidai1",
				imgsrc: imgpath + "mali01.png"
			}]
		}
	]
},
{
	//slide2
	contentblockadditionalclass: "contentwithbg1-1 bgmove2",
	imageblock:[
		{
			imagetoshow:[
			{
				imgclass: "malidai2",
				imgsrc: imgpath + "mali03.png"
			},
			{
				imgclass: "kidsmanmoved kidsmove2",
				imgsrc: imgpath + "girl-questioning.png"
			}]
		}
	]
},
{
	//slide3
	contentblockadditionalclass: "contentwithbg3",
	uppertextblock:[
	{
		textclass : 'template-dialougebox2-top-flipped-yellow dg-1',
		textdata : data.string.p3text1,
	}
	],
	imageblock:[
		{
			imagetoshow:[
			{
				imgclass: "malidai2",
				imgsrc: imgpath + "mali01.png"
			},
			{
				imgclass: "kidsmanmoved",
				imgsrc: imgpath + "girl-questioning.png"
			}]
		}
	]
},
{
	//slide4
	contentblockadditionalclass: "contentwithbg2",
	uppertextblock:[
	{
		textclass : 'template-dialougebox2-top-yellow dg-3',
		textdata : data.string.p3text2,
	}
	],
	imageblock:[
		{
			imagetoshow:[
			{
				imgclass: "malidai",
				imgsrc: imgpath + "mali01.png"
			},
			{
				imgclass: "bigimg",
				imgsrc: imgpath + "cactus04.png"
			}]
		}
	]
},
{
	//slide5
	contentblockadditionalclass: "contentwithbg2",
	uppertextblock:[
	{
		textclass : 'template-dialougebox2-top-yellow dg-3',
		textdata : data.string.p3text3,
	}
	],
	imageblock:[
		{
			imagetoshow:[
			{
				imgclass: "malidai",
				imgsrc: imgpath + "mali01.png"
			},
			{
				imgclass: "bigimg",
				imgsrc: imgpath + "cactusplant02.png"
			}]
		}
	]
},
{
	//slide6
	contentblockadditionalclass: "contentwithbg2",
	uppertextblock:[
	{
		textclass : 'template-dialougebox2-top-yellow dg-4',
		textdata : data.string.p3text4,
	}
	],
	imageblock:[
		{
			imagetoshow:[
			{
				imgclass: "malidaicenter",
				imgsrc: imgpath + "mali02.png"
			},
			{
				imgclass: "bigimgleft",
				imgsrc: imgpath + "treewithroot.png"
			},
			{
				imgclass: "bigimgright",
				imgsrc: imgpath + "cactusplant03.png"
			}]
		}
	]
},
{
	//slide7
	contentblockadditionalclass: "contentwithbg3",
	uppertextblock:[
	{
		textclass : 'template-dialougebox2-top-flipped-yellow dg-1',
		textdata : data.string.p3text5,
	}
	],
	imageblock:[
		{
			imagetoshow:[
			{
				imgclass: "malidai2",
				imgsrc: imgpath + "mali03.png"
			},
			{
				imgclass: "kidsmanmoved kidsmove2",
				imgsrc: imgpath + "girl-questioning.png"
			}]
		}
	]
},
{
	//slide8
	contentblockadditionalclass: "contentwithbg3",
	uppertextblock:[
	{
		textclass : 'template-dialougebox2-left-yellow dg-5',
		textdata : data.string.p3text6,
	}
	],
	imageblock:[
		{
			imagetoshow:[
			{
				imgclass: "malidai2",
				imgsrc: imgpath + "mali03.png"
			},
			{
				imgclass: "kidsmanmoved kidsmove2",
				imgsrc: imgpath + "girl-questioning.png"
			}]
		}
	]
},
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	loadTimelineProgress($total_page, countNext + 1);

		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


      function navigationcontroller(islastpageflag){
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch(countNext){
			case 1:
				playaudio(sound_dg1, $(".dg-2"));
			break;
			case 2:
				$nextBtn.hide(0);
				setTimeout(function(){
					$nextBtn.trigger("click");
				}, 2000);
				case 3:
				$nextBtn.hide(0);
				setTimeout(function(){
					playaudio(sound_dg2, $(".dg-1"));
				}, 2000);
				break;
				case 4:
					playaudio(sound_dg3, $(".dg-3"));
				break;
				case 5:
					playaudio(sound_dg4, $(".dg-3"));
				break;
				case 6:
					playaudio(sound_dg5, $(".dg-4"));
				break;
				case 7:
					playaudio(sound_dg6, $(".dg-1"));
				break;
				case 8:
					playaudio(sound_dg7, $(".dg-5"));
				break;
			break;
		}
	}
	function playaudio(sound_data, $dialog_container){
			var playing = true;
			$dialog_container.removeClass("playable");
			$dialog_container.click(function(){
				if(!playing){
					playaudio(sound_data, $dialog_container);
				}
				return false;
			});
			$prevBtn.hide(0);
			if((countNext+1) == content.length){
				ole.footerNotificationHandler.hideNotification();
			}else{
				$nextBtn.hide(0);
			}
			sound_data.play();
			sound_data.bind('ended', function(){
				setTimeout(function(){
					if(countNext != 0)
					$prevBtn.show(0);
					$dialog_container.addClass("playable");
					playing = false;
					sound_data.unbind('ended');
					if((countNext+1) == content.length){
						ole.footerNotificationHandler.pageEndSetNotification();
					}else{
						$nextBtn.show(0);
					}
				}, 1000);
			});
		}
	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});


 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

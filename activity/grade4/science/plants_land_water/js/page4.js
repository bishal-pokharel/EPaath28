var imgpath = $ref + "/images/";
if($lang == "en"){
	soundAsset = $ref+"/sounds/en/";
}
else if($lang == "np")
{
	soundAsset = $ref+"/sounds/np/";
}

var sound_dg1 = new buzz.sound((soundAsset + "p4_s2.ogg"));
var sound_dg2 = new buzz.sound((soundAsset + "p4_s3.ogg"));
var sound_dg3 = new buzz.sound((soundAsset + "p4_s4.ogg"));
var sound_dg4 = new buzz.sound((soundAsset + "p4_s5.ogg"));
var sound_dg5 = new buzz.sound((soundAsset + "p4_s6.ogg"));
var sound_dg6 = new buzz.sound((soundAsset + "p4_s7.ogg"));
var sound_dg7 = new buzz.sound((soundAsset + "p4_s8.ogg"));
var sound_dg8 = new buzz.sound((soundAsset + "p4_s9.ogg"));
var sound_dg9 = new buzz.sound((soundAsset + "p4_s10.ogg"));
var sound_dg10 = new buzz.sound((soundAsset + "p4_s11.ogg"));
var sound_dg11 = new buzz.sound((soundAsset + "p4_s12.ogg"));
var sound_dg12 = new buzz.sound((soundAsset + "p4_s13_1.ogg"));
var sound_dg13 = new buzz.sound((soundAsset + "p4_s13_2.ogg"));
var sound_dg14 = new buzz.sound((soundAsset + "p4_s14.ogg"));
var content=[
{
	//slide0
	contentblockadditionalclass: "contentwithbg1",
	imageblock:[
		{
			imagetoshow:[
			{
				imgclass: "malidai",
				imgsrc: imgpath + "mali03.png"
			},]
		}
	]
},
{
	//slide1
	contentblockadditionalclass: "contentwithbg1 bgmove",
	imageblock:[
		{
			imagetoshow:[
			{
				imgclass: "malidai",
				imgsrc: imgpath + "mali03.png"
			},]
		}
	]
},
{
	//slide2
	contentblockadditionalclass: "contentwithbg1-1",
	uppertextblock:[
	{
		textclass : 'template-dialougebox2-top-yellow dg-1',
		textdata : data.string.p4text1,
	}
	],
	imageblock:[
		{
			imagetoshow:[
			{
				imgclass: "malidai",
				imgsrc: imgpath + "mali03.png"
			}]
		}
	]
},
{
	//slide3
	contentblockadditionalclass: "contentwithbg1-1",
	uppertextblock:[
	{
		textclass : 'template-dialougebox2-top-yellow dg-1',
		textdata : data.string.p4text2,
	}
	],
	imageblock:[
		{
			imagetoshow:[
			{
				imgclass: "malidai",
				imgsrc: imgpath + "mali03.png"
			}]
		}
	]
},
{
	//slide4
	contentblockadditionalclass: "contentwithbg1-1",
	uppertextblock:[
	{
		textclass : 'template-dialougebox2-top-yellow dg-1',
		textdata : data.string.p4text3,
	}
	],
	imageblock:[
		{
			imagetoshow:[
			{
				imgclass: "malidai",
				imgsrc: imgpath + "mali03.png"
			}]
		}
	]
},
{
	//slide5
	contentblockadditionalclass: "contentwithbg2",
	uppertextblock:[
	{
		textclass : 'template-dialougebox2-top-yellow dg-3',
		textdata : data.string.p4text4,
	}
	],
	imageblock:[
		{
			imagetoshow:[
			{
				imgclass: "malidai2",
				imgsrc: imgpath + "mali01.png"
			},
			{
				imgclass: "bigimg",
				imgsrc: imgpath + "hydrillaflowers.jpg"
			}]
		}
	]
},
{
	//slide6
	contentblockadditionalclass: "contentwithbg2",
	uppertextblock:[
	{
		textclass : 'template-dialougebox2-top-yellow dg-3',
		textdata : data.string.p4text5,
	}
	],
	imageblock:[
		{
			imagetoshow:[
			{
				imgclass: "malidai2",
				imgsrc: imgpath + "mali01.png"
			},
			{
				imgclass: "bigimg",
				imgsrc: imgpath + "vallisneria.jpg"
			}]
		}
	]
},
{
	//slide7
	contentblockadditionalclass: "contentwithbg3",
	uppertextblock:[
	{
		textclass : 'template-dialougebox2-top-yellow dg-4',
		textdata : data.string.p4text6,
	}
	],
	imageblock:[
		{
			imagetoshow:[
			{
				imgclass: "malidai3",
				imgsrc: imgpath + "mali03.png"
			},
			{
				imgclass: "kidsman",
				imgsrc: imgpath + "kids_and_maam.png"
			}]
		}
	]
},
{
	//slide8
	contentblockadditionalclass: "contentwithbg3",
	uppertextblock:[
	{
		textclass : 'template-dialougebox2-top-flipped-yellow dg-8',
		textdata : data.string.p4text7,
	}
	],
	imageblock:[
		{
			imagetoshow:[
			{
				imgclass: "malidai3",
				imgsrc: imgpath + "mali01.png"
			},
			{
				imgclass: "kidsman",
				imgsrc: imgpath + "kids03.png"
			}]
		}
	]
},
{
	//slide9
	contentblockadditionalclass: "contentwithbg3",
	uppertextblock:[
	{
		textclass : 'template-dialougebox2-top-yellow dg-4',
		textdata : data.string.p4text8,
	}
	],
	imageblock:[
		{
			imagetoshow:[
			{
				imgclass: "malidai3",
				imgsrc: imgpath + "mali04.png"
			},
			{
				imgclass: "kidsman",
				imgsrc: imgpath + "kids_and_maam.png"
			}]
		}
	]
},
{
	//slide10
	contentblockadditionalclass: "contentwithbg3",
	uppertextblock:[
	{
		textclass : 'template-dialougebox2-right-yellow dg-6',
		textdata : data.string.p4text9,
	}
	],
	imageblock:[
		{
			imagetoshow:[
			{
				imgclass: "malidai3",
				imgsrc: imgpath + "mali03.png"
			},
			{
				imgclass: "kidsman",
				imgsrc: imgpath + "kids_and_maam.png"
			}]
		}
	]
},
{
	//slide11
	contentblockadditionalclass: "contentwithbg3",
	uppertextblock:[
	{
		textclass : 'template-dialougebox2-top-yellow dg-7',
		textdata : data.string.p4text10,
	}
	],
	imageblock:[
		{
			imagetoshow:[
			{
				imgclass: "malidai3",
				imgsrc: imgpath + "mali04.png"
			},
			{
				imgclass: "kidsman",
				imgsrc: imgpath + "kids06.png"
			}]
		}
	]
},
{
	//slide12
	contentblockadditionalclass: "contentwithbg3",
	uppertextblock:[
	{
		textclass : 'template-dialougebox2-top-flipped-yellow dg-8',
		textdata : data.string.p4text11,
	}
	],
	imageblock:[
		{
			imagetoshow:[
			{
				imgclass: "malidai3",
				imgsrc: imgpath + "mali05.png"
			},
			{
				imgclass: "kidsman",
				imgsrc: imgpath + "kids03.png"
			}]
		}
	]
},
{
	//slide13
	contentblockadditionalclass: "contentwithbg3",
	uppertextblock:[
	{
		textclass : 'template-dialougebox2-top-flipped-yellow dg-10',
		textdata : data.string.p4text12,
	},
	],
	imageblock:[
		{
			imagetoshow:[
			{
				imgclass: "malidai3",
				imgsrc: imgpath + "mali03.png"
			},
			{
				imgclass: "kidsman",
				imgsrc: imgpath + "kids04.png"
			}]
		}
	]
},
{
	//slide14
	contentblockadditionalclass: "contentwithbg3",
	uppertextblock:[
	{
		textclass : 'template-dialougebox2-top-flipped-yellow dg-9',
		textdata : data.string.p4text13,
	}
	],
	imageblock:[
		{
			imagetoshow:[
			{
				imgclass: "malidai3",
				imgsrc: imgpath + "mali03.png"
			},
			{
				imgclass: "kidsman",
				imgsrc: imgpath + "kids04.png"
			}]
		}
	]
},
{
	//slide15
	contentblockadditionalclass: "contentwithbg3",
	uppertextblock:[
	{
		textclass : 'template-dialougebox2-top-yellow dg-11',
		textdata : data.string.p4text14,
	}
	],
	imageblock:[
		{
			imagetoshow:[
			{
				imgclass: "malidai3",
				imgsrc: imgpath + "mali01.png"
			},
			{
				imgclass: "kidsman",
				imgsrc: imgpath + "kids05.png"
			}]
		}
	]
},
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	loadTimelineProgress($total_page, countNext + 1);

		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


      function navigationcontroller(islastpageflag){
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch(countNext){
			case 1:
				$nextBtn.hide(0);
				setTimeout(function(){
					$nextBtn.trigger("click");
				}, 2000);
			break;
			case 2:
				playaudio(sound_dg1, $(".dg-1"));
			break;
			case 3:
				playaudio(sound_dg2, $(".dg-1"));
			break;
			case 4:
				playaudio(sound_dg3, $(".dg-1"));
			break;
			case 5:
				playaudio(sound_dg4, $(".dg-3"));
			break;
			case 6:
				playaudio(sound_dg5, $(".dg-3"));
			break;
			case 7:
				playaudio(sound_dg6, $(".dg-4"));
			break;
			case 8:
				playaudio(sound_dg7, $(".dg-8"));
			break;
			case 9:
				playaudio(sound_dg8, $(".dg-4"));
			break;
			case 10:
				playaudio(sound_dg9, $(".dg-6"));
			break;
			case 11:
				playaudio(sound_dg10, $(".dg-7"));
			break;
			case 12:
				playaudio(sound_dg11, $(".dg-8"));
			break;
			case 13:
				playaudio(sound_dg12, $(".dg-10"));
			break;
			case 14:
				playaudio(sound_dg13, $(".dg-9"));
			break;
			case 15:
				playaudio(sound_dg14, $(".dg-11"));
				setTimeout(function(){
					ole.footerNotificationHandler.lessonEndSetNotification() ;
				},4000);
			break;
		}
	}

	function playaudio(sound_data, $dialog_container){
			var playing = true;
			$dialog_container.removeClass("playable");
			$dialog_container.click(function(){
				if(!playing){
					playaudio(sound_data, $dialog_container);
				}
				return false;
			});
			$prevBtn.hide(0);
			if((countNext+1) == content.length){
				ole.footerNotificationHandler.hideNotification();
			}else{
				$nextBtn.hide(0);
			}
			sound_data.play();
			sound_data.bind('ended', function(){
				setTimeout(function(){
					if(countNext != 0)
					$prevBtn.show(0);
					$dialog_container.addClass("playable");
					playing = false;
					sound_data.unbind('ended');
					if((countNext+1) == content.length){
						ole.footerNotificationHandler.pageEndSetNotification();
					}else{
						$nextBtn.show(0);
					}
				}, 1000);
			});
		}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});


 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
//ex 1
	{
		contentblockadditionalclass:"creamBg",
			extratxtdiv:[
			{
				textclass: "toptxt",
				textdata: data.string.p3_instrn_1
			}
		],
		speechbox:true,
		spbxcontainer:"spcorincorCont",
		speechbox:[{
			speechbox:"sp-1",
			imgclass:"spBubble",
			sptxt:[{
				textclass:"sptxt corct_txt crHd",
				textdata:data.string.correct
			},{
				textclass:"sptxt corct_txt info",
				textdata:data.string.excq1_cortxt
			},{
				textclass:"sptxt incorct_txt",
				textdata:data.string.incorrect
			}]
		}],
		extratext:[{
			textclass:"clickTxt",
			textdata:data.string.clktxt
		}],
		exetype1: [{
			optionsdivclass:"optionsdiv",
	    datahighlightflag: "true",
	    datahighlightcustomclass: "qn",
			ques_class:"oneLiner",
			textdata:data.string.excq1,
	    exeoptions:[
	      {
	        optaddclass: "correct",
					optdata:data.string.excq1_op1

	      },
	      {
					optdata:data.string.excq1_op2
	    	},
	      {
					optdata:data.string.excq1_op3
	    	},
	      {
					optdata:data.string.excq1_op4
	    	}
			]
		}]

	},
//ex 2
	{
		contentblockadditionalclass:"creamBg",
			extratxtdiv:[
			{
				textclass: "toptxt",
				textdata: data.string.p3_instrn_1
			}
		],
		speechbox:true,
		spbxcontainer:"spcorincorCont",
		speechbox:[{
			speechbox:"sp-1",
			imgclass:"spBubble",
			sptxt:[{
				textclass:"sptxt corct_txt crHd",
				textdata:data.string.correct
			},{
				textclass:"sptxt corct_txt info",
				textdata:data.string.excq2_cortxt
			},{
				textclass:"sptxt incorct_txt",
				textdata:data.string.incorrect
			}]
		}],
		extratext:[{
			textclass:"clickTxt",
			textdata:data.string.clktxt
		}],
		exetype1: [{
			optionsdivclass:"optionsdiv",
	    datahighlightflag: "true",
	    datahighlightcustomclass: "qn",
			ques_class:"oneLiner",
			textdata:data.string.excq2,
	    exeoptions:[
	      {
	        optaddclass: "correct",
					optdata:data.string.excq2_op1

	      },
	      {
					optdata:data.string.excq2_op2
	    	},
	      {
					optdata:data.string.excq2_op3
	    	},
	      {
					optdata:data.string.excq2_op4
	    	}
			]
		}]

	},
//ex 3
	{
		contentblockadditionalclass:"creamBg",
			extratxtdiv:[
			{
				textclass: "toptxt",
				textdata: data.string.p3_instrn_1
			}
		],
		speechbox:true,
		spbxcontainer:"spcorincorCont",
		speechbox:[{
			speechbox:"sp-1",
			imgclass:"spBubble",
			sptxt:[{
				textclass:"sptxt corct_txt crHd",
				textdata:data.string.correct
			},{
				textclass:"sptxt corct_txt info",
				textdata:data.string.excq3_cortxt
			},{
				textclass:"sptxt incorct_txt",
				textdata:data.string.incorrect
			}]
		}],
		extratext:[{
			textclass:"clickTxt",
			textdata:data.string.clktxt
		}],
		exetype1: [{
			optionsdivclass:"optionsdiv",
	    datahighlightflag: "true",
	    datahighlightcustomclass: "qn",
			ques_class:"oneLiner",
			textdata:data.string.excq3,
	    exeoptions:[
	      {
	        optaddclass: "smallFnt correct",
					optdata:data.string.excq3_op1

	      },
	      {
	        optaddclass: "smallFnt sml1",
					optdata:data.string.excq3_op2
	    	},
	      {
	        optaddclass: "smallFnt sml2",
					optdata:data.string.excq3_op3
	    	},
	      {
	        optaddclass: "smallFnt sml3",
					optdata:data.string.excq3_op4
	    	}
			]
		}]

	},
//ex 4
	{
		contentblockadditionalclass:"creamBg",
			extratxtdiv:[
			{
				textclass: "toptxt",
				textdata: data.string.p3_instrn_1
			}
		],
		speechbox:true,
		spbxcontainer:"spcorincorCont",
		speechbox:[{
			speechbox:"sp-1",
			imgclass:"spBubble",
			sptxt:[{
				textclass:"sptxt corct_txt crHd",
				textdata:data.string.correct
			},{
				textclass:"sptxt corct_txt info",
				textdata:data.string.excq4_cortxt
			},{
				textclass:"sptxt incorct_txt",
				textdata:data.string.incorrect
			}]
		}],
		extratext:[{
			textclass:"clickTxt",
			textdata:data.string.clktxt
		}],
		exetype1: [{
			optionsdivclass:"optionsdiv",
	    datahighlightflag: "true",
	    datahighlightcustomclass: "qn",
			ques_class:"oneLiner",
			textdata:data.string.excq4,
	    exeoptions:[
	      {
	        optaddclass: "smallFnt correct",
					optdata:data.string.excq4_op1

	      },
	      {
	        optaddclass: "smallFnt sml1",
					optdata:data.string.excq4_op2
	    	},
	      {
	        optaddclass: "smallFnt sml2",
					optdata:data.string.excq4_op3
	    	},
	      {
	        optaddclass: "smallFnt sml3",
					optdata:data.string.excq4_op4
	    	}
			]
		}]

	},
//ex 5
	{
		contentblockadditionalclass:"creamBg",
			extratxtdiv:[
			{
				textclass: "toptxt",
				textdata: data.string.p3_instrn_1
			}
		],
		speechbox:true,
		spbxcontainer:"spcorincorCont",
		speechbox:[{
			speechbox:"sp-1",
			imgclass:"spBubble",
			sptxt:[{
				textclass:"sptxt corct_txt crHd",
				textdata:data.string.correct
			},{
				textclass:"sptxt corct_txt info",
				textdata:data.string.excq5_cortxt
			},{
				textclass:"sptxt incorct_txt",
				textdata:data.string.incorrect
			}]
		}],
		extratext:[{
			textclass:"clickTxt",
			textdata:data.string.clktxt
		}],
		exetype1: [{
			optionsdivclass:"optionsdiv",
	    datahighlightflag: "true",
	    datahighlightcustomclass: "qn",
			ques_class:"oneLiner",
			textdata:data.string.excq5,
	    exeoptions:[
	      {
	        optaddclass: "correct",
					optdata:data.string.excq5_op1

	      },
	      {
					optdata:data.string.excq5_op2
	    	},
	      {
					optdata:data.string.excq5_op3
	    	},
	      {
					optdata:data.string.excq5_op4
	    	}
			]
		}]

	},
//ex 6
	{
		contentblockadditionalclass:"creamBg",
			extratxtdiv:[
			{
				textclass: "toptxt",
				textdata: data.string.p3_instrn_1
			}
		],
		speechbox:true,
		spbxcontainer:"spcorincorCont",
		speechbox:[{
			speechbox:"sp-1",
			imgclass:"spBubble",
			sptxt:[{
				textclass:"sptxt corct_txt crHd",
				textdata:data.string.correct
			},{
				textclass:"sptxt corct_txt info",
				textdata:data.string.excq6_cortxt
			},{
				textclass:"sptxt incorct_txt",
				textdata:data.string.incorrect
			}]
		}],
		extratext:[{
			textclass:"clickTxt",
			textdata:data.string.clktxt
		}],
		exetype1: [{
			optionsdivclass:"optionsdiv",
	    datahighlightflag: "true",
	    datahighlightcustomclass: "qn",
			ques_class:"oneLiner",
			textdata:data.string.excq6,
	    exeoptions:[
	      {
	        optaddclass: "correct",
					optdata:data.string.excq6_op1

	      },
	      {
					optdata:data.string.excq6_op2
	    	},
	      {
					optdata:data.string.excq6_op3
	    	},
	      {
					optdata:data.string.excq6_op4
	    	}
			]
		}]

	},
//ex 7
	{
		contentblockadditionalclass:"creamBg",
			extratxtdiv:[
			{
				textclass: "toptxt",
				textdata: data.string.p3_instrn_1
			}
		],
		speechbox:true,
		spbxcontainer:"spcorincorCont",
		speechbox:[{
			speechbox:"sp-1",
			imgclass:"spBubble",
			sptxt:[{
				textclass:"sptxt corct_txt crHd",
				textdata:data.string.correct
			},{
				textclass:"sptxt corct_txt info",
				textdata:data.string.excq7_cortxt
			},{
				textclass:"sptxt incorct_txt",
				textdata:data.string.incorrect
			}]
		}],
		extratext:[{
			textclass:"clickTxt",
			textdata:data.string.clktxt
		}],
		exetype1: [{
			optionsdivclass:"optionsdiv",
	    datahighlightflag: "true",
	    datahighlightcustomclass: "qn",
			ques_class:"oneLiner",
			textdata:data.string.excq7,
	    exeoptions:[
	      {
	        optaddclass: "smallFnt correct",
					optdata:data.string.excq7_op1

	      },
	      {
	        optaddclass: "smallFnt sml1",
					optdata:data.string.excq7_op2
	    	},
	      {
	        optaddclass: "smallFnt sml2",
					optdata:data.string.excq7_op3
	    	},
	      {
	        optaddclass: "smallFnt sml3",
					optdata:data.string.excq7_op4
	    	}
			]
		}]

	},
//ex 8
	{
		contentblockadditionalclass:"creamBg",
			extratxtdiv:[
			{
				textclass: "toptxt",
				textdata: data.string.p3_instrn_1
			}
		],
		speechbox:true,
		spbxcontainer:"spcorincorCont",
		speechbox:[{
			speechbox:"sp-1",
			imgclass:"spBubble",
			sptxt:[{
				textclass:"sptxt corct_txt crHd",
				textdata:data.string.correct
			},{
				textclass:"sptxt corct_txt info",
				textdata:data.string.excq8_cortxt
			},{
				textclass:"sptxt incorct_txt",
				textdata:data.string.incorrect
			}]
		}],
		extratext:[{
			textclass:"clickTxt",
			textdata:data.string.clktxt
		}],
		exetype1: [{
			optionsdivclass:"optionsdiv",
	    datahighlightflag: "true",
	    datahighlightcustomclass: "qn",
			ques_class:"oneLiner",
			textdata:data.string.excq8,
	    exeoptions:[
	      {
	        optaddclass: "correct",
					optdata:data.string.excq8_op1

	      },
	      {
					optdata:data.string.excq8_op2
	    	},
	      {
					optdata:data.string.excq8_op3
	    	},
	      {
					optdata:data.string.excq8_op4
	    	}
			]
		}]

	},
//ex 9
	{
		contentblockadditionalclass:"creamBg",
			extratxtdiv:[
			{
				textclass: "toptxt",
				textdata: data.string.p3_instrn_1
			}
		],
		speechbox:true,
		spbxcontainer:"spcorincorCont",
		speechbox:[{
			speechbox:"sp-1",
			imgclass:"spBubble",
			sptxt:[{
				textclass:"sptxt corct_txt crHd",
				textdata:data.string.correct
			},{
				textclass:"sptxt corct_txt info",
				textdata:data.string.excq9_cortxt
			},{
				textclass:"sptxt incorct_txt",
				textdata:data.string.incorrect
			}]
		}],
		extratext:[{
			textclass:"clickTxt",
			textdata:data.string.clktxt
		}],
		exetype1: [{
			optionsdivclass:"optionsdiv",
	    datahighlightflag: "true",
	    datahighlightcustomclass: "qn",
			ques_class:"oneLiner",
			textdata:data.string.excq9,
	    exeoptions:[
	      {
	        optaddclass: "correct",
					optdata:data.string.excq9_op1

	      },
	      {
					optdata:data.string.excq9_op2
	    	},
	      {
					optdata:data.string.excq9_op3
	    	},
	      {
					optdata:data.string.excq9_op4
	    	}
			]
		}]

	},
//ex 10
	{
		contentblockadditionalclass:"creamBg",
			extratxtdiv:[
			{
				textclass: "toptxt",
				textdata: data.string.p3_instrn_1
			}
		],
		speechbox:true,
		spbxcontainer:"spcorincorCont",
		speechbox:[{
			speechbox:"sp-1",
			imgclass:"spBubble",
			sptxt:[{
				textclass:"sptxt corct_txt crHd",
				textdata:data.string.correct
			},{
				textclass:"sptxt corct_txt info",
				textdata:data.string.excq10_cortxt
			},{
				textclass:"sptxt incorct_txt",
				textdata:data.string.incorrect
			}]
		}],
		extratext:[{
			textclass:"clickTxt",
			textdata:data.string.clktxt
		}],
		exetype1: [{
			optionsdivclass:"optionsdiv",
	    datahighlightflag: "true",
	    datahighlightcustomclass: "qn",
			ques_class:"oneLiner",
			textdata:data.string.excq10,
	    exeoptions:[
	      {
	        optaddclass: "smallFnt correct",
					optdata:data.string.excq10_op1

	      },
	      {
	        optaddclass: "smallFnt sml1",
					optdata:data.string.excq10_op2
	    	},
	      {
	        optaddclass: "smallFnt sml2",
					optdata:data.string.excq10_op3
	    	},
	      {
	        optaddclass: "smallFnt sml3",
					optdata:data.string.excq10_op4
	    	}
			]
		}]

	},
//ex 11
	{
		contentblockadditionalclass:"creamBg",
			extratxtdiv:[
			{
				textclass: "toptxt",
				textdata: data.string.p3_instrn_1
			}
		],
		speechbox:true,
		spbxcontainer:"spcorincorCont",
		speechbox:[{
			speechbox:"sp-1",
			imgclass:"spBubble",
			sptxt:[{
				textclass:"sptxt corct_txt crHd",
				textdata:data.string.correct
			},{
				textclass:"sptxt corct_txt info",
				textdata:data.string.excq11_cortxt
			},{
				textclass:"sptxt incorct_txt",
				textdata:data.string.incorrect
			}]
		}],
		extratext:[{
			textclass:"clickTxt",
			textdata:data.string.clktxt
		}],
		exetype1: [{
			optionsdivclass:"optionsdiv",
	    datahighlightflag: "true",
	    datahighlightcustomclass: "qn",
			ques_class:"oneLiner",
			textdata:data.string.excq11,
	    exeoptions:[
	      {
	        optaddclass: "correct",
					optdata:data.string.excq11_op1

	      },
	      {
					optdata:data.string.excq11_op2
	    	},
	      {
					optdata:data.string.excq11_op3
	    	},
	      {
					optdata:data.string.excq11_op4
	    	}
			]
		}]

	},
//ex 12
	{
		contentblockadditionalclass:"creamBg",
			extratxtdiv:[
			{
				textclass: "toptxt",
				textdata: data.string.p3_instrn_1
			}
		],
		speechbox:true,
		spbxcontainer:"spcorincorCont",
		speechbox:[{
			speechbox:"sp-1",
			imgclass:"spBubble",
			sptxt:[{
				textclass:"sptxt corct_txt crHd",
				textdata:data.string.correct
			},{
				textclass:"sptxt corct_txt info",
				textdata:data.string.excq12_cortxt
			},{
				textclass:"sptxt incorct_txt",
				textdata:data.string.incorrect
			}]
		}],
		extratext:[{
			textclass:"clickTxt",
			textdata:data.string.clktxt
		}],
		exetype1: [{
			optionsdivclass:"optionsdiv",
	    datahighlightflag: "true",
	    datahighlightcustomclass: "qn",
			ques_class:"oneLiner",
			textdata:data.string.excq12,
	    exeoptions:[
	      {
	        optaddclass: "correct",
					optdata:data.string.excq12_op1

	      },
	      {
					optdata:data.string.excq12_op2
	    	},
	      {
					optdata:data.string.excq12_op3
	    	},
	      {
					optdata:data.string.excq12_op4
	    	}
			]
		}]

	},
];
content.shufflearray();
$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	/*for limiting the questions to 10*/
	var $total_page = 12;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bubble01", src: imgpath+"bubble01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "correct-1", src: imgpath+"correct-1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wrong01", src: imgpath+"wrong01.png", type: createjs.AbstractLoader.IMAGE},

//textboxes


			// sounds
            {id: "sound_1", src: soundAsset+"ex.ogg"},

        ];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		//scoring.init(10);
		templateCaller();
	}
	//initialize
	init();

	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
 	}

	 /*values in this array is same as the name of images of eggs in image folder*/

	 //create eggs
	 var testin = new EggTemplate();

	 	//eggTemplate.eggMove(countNext);
 		testin.init(12);
    function sound_player(sound_id){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
    }
	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);
		texthighlight($board);
		put_image(content, countNext);
		var updateScore = 0;
		// put_image_sec(content, countNext);
	 	var testcount = 0;

	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);

	 	/*generate question no at the beginning of question*/
	 	testin.numberOfQuestions();
	 	countNext==0?sound_player("sound_1"):"";

	 	/*for randomizing the options*/
		function randomize(parent){
			// alert(parent);
			var parent = $(parent);
			var divs = parent.children();
			while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}
}

	 	/*======= SCOREBOARD SECTION ==============*/
	 	/*random scoreboard eggs*/
	 	//var i = Math.floor(Math.random() * imageArray.length);
	 	//var randImg = imageArray[i];
		$(".spBubble").attr('src', preload.getResult("bubble01").src);
	 	var ansClicked = false;
	 	var wrngClicked = false;
		$(".question").prepend((countNext+1)+". ");
		$(".info").hide(0);
	 	switch(countNext){
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
			case 10:
			case 11:
				randomize(".optionsdiv");
				btnslClk();
		 	break;
	 	}
	function randomize(parent){
		var parent = $(parent);
		var divs = parent.children();
		while (divs.length) {
 			parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
	}
	//handle mcq
	function btnslClk(){
	$(".buttonsel").click(function(){
		$(this).removeClass('forhover');
			if(ansClicked == false){
				/*class 1 is always for the right answer. updates scoreboard and disables other click if
				right answer is clicked*/
				if($(this).hasClass("correct")){
					if(wrngClicked == false){
						testin.update(true);
					}
					play_correct_incorrect_sound(1);
					$(this).css({
						"background": "#bed62fff",
						"border":"5px solid #deef3c",
						"color":"#000",
						'pointer-events': 'none'
					});
					$(this).siblings(".corctopt").show(0);
					$('.buttonsel').removeClass('forhover forhoverimg');
					$('.buttonsel').removeClass('clock-hover');
					$(".monkeyexp, .sptxt").css("opacity","0");
					$(".sp-1, .mnkcor, .corct_txt").css("opacity","1");
					$(".crHd").delay(800).fadeOut(200);
					$(".info").delay(1000).fadeIn(200);
					$nextBtn.show(0);
					ansClicked = true;

				}
				else{
					testin.update(false);
					play_correct_incorrect_sound(0);
					$(this).css({
						"background":"#FF0000",
						"border":"5px solid #980000",
						"color":"#000",
						'pointer-events': 'none'
					});
					$(".sp-1, .mnkincor, .incorct_txt").css("opacity","1");
					$(this).siblings(".wrngopt").show(0);
					wrngClicked = true;
				}
			}
		});
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					// console.log(selector);
					$(selector).attr('src',image_src);
				}
			}
		}
	}
	 	/*======= SCOREBOARD SECTION ==============*/
	 }

	 function templateCaller(){
		/*always hide next and previousloadimage navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators


	}

	// first call to template caller
	// templateCaller(); loadimage($(".myimg3").find("img"),$(".myimg3").find("p"),preload.getResult('ex1typ3qn'+quesNo).src,eval('data.string.ex2typ1op'+quesNo));


	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
 	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		singletext:[
			{
				textclass: "covTxt",
				textdata: data.lesson.chapter
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "fullbg",
					imgid : 'cover_page',
					imgsrc: ""
				}
			]
		}]
	},
	// slide1
	{
		singletext:[
			{
				textclass: "whitebg",
			},
			{
				textclass: "whitetext",
				textdata: data.string.p1text1
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "fullbg",
					imgid : 'bg01',
					imgsrc: ""
				},
				{
					imgclass: "ant",
					imgid : 'ant',
					imgsrc: ""
				},
				{
					imgclass: "bird",
					imgid : 'bird',
					imgsrc: ""
				}
			]
		}]
	},
	// slide2
	{
		singletext:[
			{
				textclass: "whitebg",
			},
			{
				textclass: "whitetext",
				textdata: data.string.p1text2
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "fullbg",
					imgid : 'bg01',
					imgsrc: ""
				},
				{
					imgclass: "ant",
					imgid : 'ant',
					imgsrc: ""
				},
				{
					imgclass: "bird",
					imgid : 'bird',
					imgsrc: ""
				}
			]
		}]
	},
	// slide3
	{
		singletext:[
			{
				textclass: "whitebg",
			},
			{
				textclass: "whitetext",
				textdata: data.string.p1text3
			}
		],
		uppertextblockadditionalclass: "lifeprocont hideforslide",
		uppertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "ignore",
				textclass: "ss1",
				textdata: data.string.lp1
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "ignore",
				textclass: "ss2",
				textdata: data.string.lp2
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "ignore",
				textclass: "ss3",
				textdata: data.string.lp3
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "ignore",
				textclass: "ss4",
				textdata: data.string.lp4
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "ignore",
				textclass: "ss5",
				textdata: data.string.lp5
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "ignore",
				textclass: "ss6",
				textdata: data.string.lp6
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "ignore",
				textclass: "ss7",
				textdata: data.string.lp7
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "fullbg",
					imgid : 'bg01',
					imgsrc: ""
				},
				{
					imgclass: "ant",
					imgid : 'ant',
					imgsrc: ""
				},
				{
					imgclass: "bird",
					imgid : 'bird',
					imgsrc: ""
				}
			]
		}]
	},
	// slide4
	{
		singletext:[
			{
				textclass: "whitebg",
			},
			{
				textclass: "whitetext",
				textdata: data.string.p1text4
			}
		],
		uppertextblockadditionalclass: "lifeprocont",
		uppertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "ignore",
				textclass: "ss1",
				textdata: data.string.lp1
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "ignore",
				textclass: "ss2",
				textdata: data.string.lp2
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "ignore",
				textclass: "ss3",
				textdata: data.string.lp3
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "ignore",
				textclass: "ss4",
				textdata: data.string.lp4
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "ignore",
				textclass: "ss5",
				textdata: data.string.lp5
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "ignore",
				textclass: "ss6",
				textdata: data.string.lp6
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "ignore",
				textclass: "ss7",
				textdata: data.string.lp7
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "fullbg",
					imgid : 'bg01',
					imgsrc: ""
				},
				{
					imgclass: "ant",
					imgid : 'ant',
					imgsrc: ""
				},
				{
					imgclass: "bird",
					imgid : 'bird',
					imgsrc: ""
				}
			]
		}]
	},
	// slide5
	{
		singletext:[
			{
				textclass: "whitebg",
			},
			{
				datahighlightflag:'true',
				datahighlightcustomclass:"p5Txt",
				textclass: "whitetext",
				textdata: data.string.p1text5
			}
		],
		uppertextblockadditionalclass: "lifeprocont",
		uppertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "ignore",
				textclass: "ss1",
				textdata: data.string.lp1
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "ignore",
				textclass: "ss2",
				textdata: data.string.lp2
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "ignore",
				textclass: "ss3",
				textdata: data.string.lp3
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "ignore",
				textclass: "ss4",
				textdata: data.string.lp4
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "ignore",
				textclass: "ss5",
				textdata: data.string.lp5
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "ignore",
				textclass: "ss6",
				textdata: data.string.lp6
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "ignore",
				textclass: "ss7",
				textdata: data.string.lp7
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "fullbg",
					imgid : 'bg01',
					imgsrc: ""
				},
				{
					imgclass: "ant",
					imgid : 'ant',
					imgsrc: ""
				},
				{
					imgclass: "bird",
					imgid : 'bird',
					imgsrc: ""
				}
			]
		}]
	},
	// // slide6
	// {
	// 	singletext:[
	// 		{
	// 			textclass: "whitebg",
	// 		},
	// 		{
	// 			textclass: "whitetext",
	// 			textdata: data.string.p1text6
	// 		}
	// 	],
	// 	uppertextblockadditionalclass: "lifeprocont",
	// 	uppertextblock:[
	// 		{
	// 			datahighlightflag: true,
	// 			datahighlightcustomclass: "ignore",
	// 			textclass: "ss1",
	// 			textdata: data.string.lp1
	// 		},
	// 		{
	// 			datahighlightflag: true,
	// 			datahighlightcustomclass: "ignore",
	// 			textclass: "ss2",
	// 			textdata: data.string.lp2
	// 		},
	// 		{
	// 			datahighlightflag: true,
	// 			datahighlightcustomclass: "ignore",
	// 			textclass: "ss3",
	// 			textdata: data.string.lp3
	// 		},
	// 		{
	// 			datahighlightflag: true,
	// 			datahighlightcustomclass: "ignore",
	// 			textclass: "ss4",
	// 			textdata: data.string.lp4
	// 		},
	// 		{
	// 			datahighlightflag: true,
	// 			datahighlightcustomclass: "ignore",
	// 			textclass: "ss5",
	// 			textdata: data.string.lp5
	// 		},
	// 		{
	// 			datahighlightflag: true,
	// 			datahighlightcustomclass: "ignore",
	// 			textclass: "ss6",
	// 			textdata: data.string.lp6
	// 		},
	// 		{
	// 			datahighlightflag: true,
	// 			datahighlightcustomclass: "ignore",
	// 			textclass: "ss7",
	// 			textdata: data.string.lp7
	// 		}
	// 	],
	// 	imageblock:[{
	// 		imagestoshow:[
	// 			{
	// 				imgclass: "fullbg",
	// 				imgid : 'bg01',
	// 				imgsrc: ""
	// 			},
	// 			{
	// 				imgclass: "ant",
	// 				imgid : 'ant',
	// 				imgsrc: ""
	// 			},
	// 			{
	// 				imgclass: "bird",
	// 				imgid : 'bird',
	// 				imgsrc: ""
	// 			}
	// 		]
	// 	}]
	// },
	// slide7
	{
		singletext:[
			{
				textclass: "whitebg",
			},
			{
				textclass: "whitetext",
				textdata: data.string.p1text6
			},
			{
				textclass: "let-1",
				textdata: data.string.let1
			},
			{
				textclass: "let-2",
				textdata: data.string.let2
			},
			{
				textclass: "let-3",
				textdata: data.string.let3
			},
			{
				textclass: "let-4",
				textdata: data.string.let4
			},
			{
				textclass: "let-5",
				textdata: data.string.let5
			},
			{
				textclass: "let-6",
				textdata: data.string.let6
			},
			{
				textclass: "let-7",
				textdata: data.string.let7
			}
		],
		uppertextblockadditionalclass: "lifeprocont",
		uppertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "ignore",
				textclass: "ss1",
				textdata: data.string.lp1
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "ignore",
				textclass: "ss2",
				textdata: data.string.lp2
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "ignore",
				textclass: "ss3",
				textdata: data.string.lp3
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "ignore",
				textclass: "ss4",
				textdata: data.string.lp4
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "ignore",
				textclass: "ss5",
				textdata: data.string.lp5
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "ignore",
				textclass: "ss6",
				textdata: data.string.lp6
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "ignore",
				textclass: "ss7",
				textdata: data.string.lp7
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "fullbg",
					imgid : 'bg01',
					imgsrc: ""
				},
				{
					imgclass: "ant",
					imgid : 'ant',
					imgsrc: ""
				},
				{
					imgclass: "bird",
					imgid : 'bird',
					imgsrc: ""
				}
			]
		}]
	},
	// slide8
	{
		contentblockadditionalclass: "thebg1",
		singletext:[
			{
				textclass: "grentext",
				textdata: data.string.p1text7
			},
			{
				textclass: "grenlabel",
				textdata: data.string.p1text8
			}
		],
		uppertextblockadditionalclass: "lifeprocont",
		uppertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "yelback",
				textclass: "ss1",
				textdata: data.string.lp1
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "yelback",
				textclass: "ss2",
				textdata: data.string.lp2
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "yelback",
				textclass: "ss3",
				textdata: data.string.lp3
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "yelback",
				textclass: "ss4",
				textdata: data.string.lp4
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "yelback",
				textclass: "ss5",
				textdata: data.string.lp5
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "yelback",
				textclass: "ss6",
				textdata: data.string.lp6
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "yelback",
				textclass: "ss7",
				textdata: data.string.lp7
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "mrsgren",
					imgid : 'mrsgren',
					imgsrc: ""
				}
			]
		}]
	}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bg01", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ant", src: imgpath+"ant.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "bird", src: imgpath+"bird_fly.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "mrsgren", src: imgpath+"mrs_gren03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cover_page", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},

			{id: "corrimg", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrimg", src: "images/wrongicon.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: imgpath+"cloud.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"s1_p1.ogg"},
			{id: "sound_2", src: soundAsset+"s1_p2.ogg"},
			{id: "sound_3", src: soundAsset+"s1_p3.ogg"},
			{id: "sound_4", src: soundAsset+"s1_p4.ogg"},
			{id: "sound_5", src: soundAsset+"s1_p5.ogg"},
			{id: "sound_6", src: soundAsset+"s1_p6.ogg"},
			{id: "sound_7", src: soundAsset+"s1_p7.ogg"},
			{id: "sound_8", src: soundAsset+"s1_p8.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside){
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ?
		alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		null ;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightstarttag2;
		var texthighlightstarttag3;
		var texthighlightendtag   = "</span>";
		if($alltextpara.length > 0){
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				(stylerulename = $(this).attr("data-highlightcustomclass")) :
				(stylerulename = "parsedstring") ;

				$(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				(stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
				(stylerulename2 = "parsedstring2") ;

				$(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				(stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
				(stylerulename3 = "parsedstring3") ;

				texthighlightstarttag = "<span class='"+stylerulename+"'>";
				texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
				texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
				replaceinstring       = $(this).html();
				replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
				replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


				replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
				replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


				replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
				replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	function navigationcontroller(islastpageflag){
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;

		if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext){
			case 0:
				sound_player("sound_1", 1);
			break;
			case 1:
				sound_player("sound_2", 1);
			break;
			case 2:
				sound_player("sound_3", 1);
			break;
			case 3:
                sound_player("sound_4", 1);
             setTimeout( function(){
				$(".hideforslide").animate({
					right: "2%",
				},2000, function(){
					loop_player(8);
				});
			},3000);

			var loopvar = 1;

			function loop_player(lastval){
				var delaytime = 0;
				setTimeout(function(){
                    $(".ss"+loopvar).addClass("highme");
                    $(".ss"+(loopvar-1)).removeClass("highme");
                    loopvar++;
                    if(loopvar <= lastval){
                    	loop_player(8);
                    }
                    else{
                    	$(".ss"+(loopvar-1)).removeClass("highme");
                    }
				},delaytime+1200);
			}
			break;
			case 4:
				sound_player("sound_5", 1);
			break;
			case 5:
				$(".p5Txt:eq(0)").css("opacity","0");
                sound_player("sound_6", 1);
			break;
			case 6:
                sound_player("sound_7", 1);
			break;
			case 7:
                sound_player("sound_8", 1);
			break;
		}
	}

	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				// console.log(selector);
				$(selector).attr('src', image_src);
			}
		}
	}

	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');


		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
			default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

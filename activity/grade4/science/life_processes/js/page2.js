var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide1-->clktxt
	{
		contentblockadditionalclass: "grenBg",
		uppertextblockadditionalclass: "lifeprocont",
		uppertextblock:[
			{
				textclass: "ss1",
				textdata: data.string.lp1_1
			},
			{
				textclass: "ss2",
				textdata: data.string.lp2_1
			},
			{
				textclass: "ss3",
				textdata: data.string.lp3_1
			},
			{
				textclass: "ss4",
				textdata: data.string.lp4_1
			},
			{
				textclass: "ss5",
				textdata: data.string.lp5_1
			},
			{
				textclass: "ss6",
				textdata: data.string.lp6_1
			},
			{
				textclass: "ss7",
				textdata: data.string.lp7_1
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "mrsGren",
					imgid : 'mrs_gren',
					imgsrc: ""
				},
				{
					imgclass: "hand-s1",
					imgid : 'hand-icon',
					imgsrc: ""
				}
			]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:"spImage",
			imgid:'tb1',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p2s1_sb_txt,
		}]
	},
	// slide2
	{
		contentblockadditionalclass: "dblue_bg",
		imageload:true,
		sideboxes:[{
			sideboxclass:"leftBox",
			sideboxtxt:[{
				textclass:"boxToptext",
				textdata:data.string.lp1_1
			},{
				datahighlightflag:"true",
				datahighlightcustomclass:"lhs_oboTxt",
				textclass:"boxMidTxt",
				textdata:data.string.p2s2_lhs_txt
			}]
		},
		{
			sideboxclass:"rightBox lightBg",
			sideboxtxt:[{
				textclass:"boxToptext s2LongBox",
				textdata:data.string.p2s2_rhs_clktxt
			}],
			imageblock:[{
				imagestoshow:[
				{
					imgclass: "fullbg",
					imgid : 'bg02',
					imgsrc: ""
				},
				{
					imgclass: "budi_s2",
					imgid : 'budi_pp',
					imgsrc: ""
				},
				{
					imgclass: "cheetah",
					imgid : 'cheetah_animated',
					imgsrc: ""
				}
				]
			}]
		}],
		intfactdiv:[{
			ifdivboxclass:"intFactCont",
			boxcontent:[
				{
					boxcontentclass:"boxcontents bc1",
					midimgclass:"intFactImg ifImg1",
					textclass:"mov_if_s1txt",
					textdata:data.string.mov_s1txt
				},{
					boxcontentclass:"boxcontents bc2",
					midimgclass:"intFactImg ifImg2",
					textclass:"mov_if_s1txt",
					textdata:data.string.mov_s2txt
				},{
					boxcontentclass:"boxcontents bc3",
					midimgclass:"intFactImg ifImg3",
					textclass:"mov_if_s1txt",
					textdata:data.string.mov_s3txt
				}
			]

		}]
	},
	// slide3-->clktxt
	{
		contentblockadditionalclass: "grenBg",
		uppertextblockadditionalclass: "lifeprocont",
		uppertextblock:[
			{
				textclass: "ss1",
				textdata: data.string.lp1_1
			},
			{
				textclass: "ss2",
				textdata: data.string.lp2_1
			},
			{
				textclass: "ss3",
				textdata: data.string.lp3_1
			},
			{
				textclass: "ss4",
				textdata: data.string.lp4_1
			},
			{
				textclass: "ss5",
				textdata: data.string.lp5_1
			},
			{
				textclass: "ss6",
				textdata: data.string.lp6_1
			},
			{
				textclass: "ss7",
				textdata: data.string.lp7_1
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "mrsGren",
					imgid : 'mrs_gren',
					imgsrc: ""
				},
				{
					imgclass: "hand-s3",
					imgid : 'hand-icon',
					imgsrc: ""
				}
			]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:"spImage",
			imgid:'tb1',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p2s1_sb_txt,
		}]
	},
	// slide4
	{
		contentblockadditionalclass: "dblue_bg",
		imageload:true,
		sideboxes:[{
			sideboxclass:"leftBox",
			sideboxtxt:[{
				textclass:"boxToptext",
				textdata:data.string.respiration
			},{
				textclass:"boxMidTxt",
				textdata:data.string.p2s4_lhs_txt
			}]
		},
		{
			sideboxclass:"rightBox whiteBg",
			sideboxtxt:[{
				datahighlightflag:"true",
				datahighlightcustomclass:"blkTxt",
				textclass:"boxBtmText",
				textdata:data.string.resp_desc,
			},{
				datahighlightflag:"true",
				datahighlightcustomclass:"subTxt",
				textclass:"gasNameText",
				textdata:data.string.gas_names,
			}],
			imageblock:[{
				imagestoshow:[
					{
						imgclass: "respiration",
						imgid : 'respiration',
						imgsrc: ""
					}
				]
			}]
		}]
	},
	// slide5
	{
		contentblockadditionalclass: "dblue_bg",
		imageload:true,
		sideboxes:[{
			sideboxclass:"leftBox",
			sideboxtxt:[{
				textclass:"boxToptext",
				textdata:data.string.respiration
			},{
				textclass:"boxMidTxt",
				textdata:data.string.p2s4_lhs_txt
			}]
		},
		{
			sideboxclass:"rightBox whiteBg",
			sideboxtxt:[{
				datahighlightflag:"true",
				datahighlightcustomclass:"blkTxt",
				textclass:"boxBtmText",
				textdata:data.string.resp_desc,
			},{
				datahighlightflag:"true",
				datahighlightcustomclass:"subTxt",
				textclass:"gasNameText",
				textdata:data.string.gas_names,
			}],
			imageblock:[{
				imagestoshow:[
					{
						imgclass: "respiration",
						imgid : 'respiration',
						imgsrc: ""
					}
				]
			}]
		}],
		exetype1: [{
			optionsdivclass:"optionsdiv",
			ques_class:"oneLiner",
			textdata:data.string.p2s5qn,
	    exeoptions:[
	      {
	        optaddclass: "correct",
					optdata:data.string.no
	      },
	      {
					optdata:data.string.yes
	    	}
			]
		}]
	},
	// slide6
	{
		contentblockadditionalclass: "dblue_bg",
		imageload:true,
		sideboxes:[{
			sideboxclass:"leftBox",
			sideboxtxt:[{
				textclass:"boxToptext",
				textdata:data.string.breathing
			},{
				textclass:"btm_wbg",
				textdata:data.string.breathing_exp
			}],
			imageblock:[{
				imagestoshow:[
					{
						imgclass: "midImg respiration",
						imgid : 'breathing',
						imgsrc: ""
					}
				]
			}]
		},
		{
			sideboxclass:"rightBox lightBg",
			sideboxtxt:[{
				textclass:"boxToptext ",
				textdata:data.string.respiration
			},{
				textclass:"btm_wbg resp",
				textdata:data.string.resp_exp
			},{
				textclass:"respNames celresp",
				textdata:data.string.cel_resp
			},{
				textclass:"respNames blood",
				textdata:data.string.blod
			}],
			imageblock:[{
				imagestoshow:[
					{
						imgclass: "midImg breathing",
						imgid : 'resp_human',
						imgsrc: ""
					}
				]
			}]
		}]
	},
	// slide7
	{
		contentblockadditionalclass: "dblue_bg",
		imageload:true,
		sideboxes:[{
			sideboxclass:"leftBox",
			sideboxtxt:[{
				textclass:"boxToptext",
				textdata:data.string.respiration
			},{
				textclass:"boxMidTxt",
				textdata:data.string.p2s7_lhstxt
			}],
			imageblock:[{
				imagestoshow:[
					{
						imgclass: "midImg s7Img",
						imgid : 'respiration_of_plants',
						imgsrc: ""
					}
				]
			}]
		},
		{
			sideboxclass:"rightBox whiteBg",
			sideboxtxt:[{
				textclass:"btm_wbg resp",
				textdata:data.string.p2s7_rhstxt
			}],
			imageblock:[{
				imagestoshow:[
					{
						imgclass: "midImg breathing",
						imgid : 'photosynthesis-&-respiration',
						imgsrc: ""
					}
				]
			}]
		}]
	},
	// slide8-->clktxt
	{
		contentblockadditionalclass: "grenBg",
		uppertextblockadditionalclass: "lifeprocont",
		uppertextblock:[
			{
				textclass: "ss1",
				textdata: data.string.lp1_1
			},
			{
				textclass: "ss2",
				textdata: data.string.lp2_1
			},
			{
				textclass: "ss3",
				textdata: data.string.lp3_1
			},
			{
				textclass: "ss4",
				textdata: data.string.lp4_1
			},
			{
				textclass: "ss5",
				textdata: data.string.lp5_1
			},
			{
				textclass: "ss6",
				textdata: data.string.lp6_1
			},
			{
				textclass: "ss7",
				textdata: data.string.lp7_1
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "mrsGren",
					imgid : 'mrs_gren',
					imgsrc: ""
				},
				{
					imgclass: "hand-s8",
					imgid : 'hand-icon',
					imgsrc: ""
				}
			]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:"spImage",
			imgid:'tb1',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p2s1_sb_txt,
		}]
	},
	// slide9
	{
		contentblockadditionalclass: "dblue_bg",
		qnflip:[{
			flipclass:"greenBoxTxt",
			fliptxt:[{
				textclass:"bxtxt",
				textdata:data.string.s9_clk_txt_sec
			}]
		}],
		imageload:true,
		sideboxes:[{
			sideboxclass:"leftBox",
			sideboxtxt:[{
				textclass:"boxToptext",
				textdata:data.string.sensitivity
			},{
				textclass:"boxMidTxt",
				textdata:data.string.sensitivity_expln
			}]
		},
		{
			sideboxclass:"rightBox whiteBg",
			sideboxtxt:[{
				textclass:"topTxt drkGrayBg",
				textdata:data.string.s10_clk_txt
			}
		],
			imageblock:[{
				imagestoshow:[
					{
						imgclass: "touchPlant tchPlnt",
						imgid : 'feeling',
						imgsrc: ""
					},{
						imgclass: "touchPlant tchGif",
						imgid : 'feeling_01',
						imgsrc: ""
					}
				]
			}]
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "hic-hI9 smlLft dragable",
					imgid : 'pointing_hand',
					imgsrc: ""
				},{
					imgclass: "tipS9",
					imgid : 'tipicon4',
					imgsrc: ""
				}
			]
		}],
		ifsecdiv:[{
			ifsecdivclass:"s9InfDiv",
			txtofintfact:[{
				textclass:"leftBox",
			},{
				textclass:"leftBoxTxt",
				textdata: data.string.p2s10_lhs
			}],
			imageofintfact:[{
				ifimgclass:"s9intFactImg",
			}]
		}]
	},
	// slide11-->clktxt
	{
		contentblockadditionalclass: "grenBg",
		uppertextblockadditionalclass: "lifeprocont",
		uppertextblock:[
			{
				textclass: "ss1",
				textdata: data.string.lp1_1
			},
			{
				textclass: "ss2",
				textdata: data.string.lp2_1
			},
			{
				textclass: "ss3",
				textdata: data.string.lp3_1
			},
			{
				textclass: "ss4",
				textdata: data.string.lp4_1
			},
			{
				textclass: "ss5",
				textdata: data.string.lp5_1
			},
			{
				textclass: "ss6",
				textdata: data.string.lp6_1
			},
			{
				textclass: "ss7",
				textdata: data.string.lp7_1
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "mrsGren",
					imgid : 'mrs_gren',
					imgsrc: ""
				},
				{
					imgclass: "hand-s11",
					imgid : 'hand-icon',
					imgsrc: ""
				}
			]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:"spImage",
			imgid:'tb1',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p2s1_sb_txt,
		}]
	},
	// slide12
	{
		contentblockadditionalclass: "dblue_bg",
		imageload:true,
		sideboxes:[{
			sideboxclass:"leftBox",
			sideboxtxt:[{
				textclass:"boxToptext",
				textdata:data.string.lp4_1
			},{
				textclass:"boxMidTxt",
				textdata:data.string.p2s12_lftxt
			}],
			imageblock:[{
				imagestoshow:[
					{
						imgclass: "btmLeftImg",
						imgid : 'budi_pp',
						imgsrc: ""
					}
				]
			}]
		},
		{
			sideboxclass:"rightBox whiteBg",
			sideboxtxt:[{
				textclass:"topTxt drkGrayBg",
				textdata:data.string.s12_clktxt
			},{
				textclass:"yearsTab yr_9",
				textdata:data.string.nine_yrs
			},{
				textclass:"yearsTab yr_3",
				textdata:data.string.three_yrs
			},{
				textclass:"yearsTab yr_1",
				textdata:data.string.one_yrs
			}],
			imageblock:[{
				imagestoshow:[
					{
						imgclass: "fullbg hwsml",
						imgid : 'box',
						imgsrc: ""
					},
					{
						imgclass: "pasang psngFst",
						imgid : 'pasang01',
						imgsrc: ""
					},
					{
						imgclass: "pasang psngSec",
						imgid : 'pasang02',
						imgsrc: ""
					},
					{
						imgclass: "pasang psngThird",
						imgid : 'pasang03',
						imgsrc: ""
					},
					{
						imgclass: "doted_line line_1",
						imgid : 'doted_line',
						imgsrc: ""
					},
					{
						imgclass: "doted_line line_2",
						imgid : 'doted_line',
						imgsrc: ""
					},
					{
						imgclass: "doted_line line_3",
						imgid : 'doted_line',
						imgsrc: ""
					}
				]
			}]
		}]
	},
	// slide13-->clktxt
	{
		contentblockadditionalclass: "grenBg",
		uppertextblockadditionalclass: "lifeprocont",
		uppertextblock:[
			{
				textclass: "ss1",
				textdata: data.string.lp1_1
			},
			{
				textclass: "ss2",
				textdata: data.string.lp2_1
			},
			{
				textclass: "ss3",
				textdata: data.string.lp3_1
			},
			{
				textclass: "ss4",
				textdata: data.string.lp4_1
			},
			{
				textclass: "ss5",
				textdata: data.string.lp5_1
			},
			{
				textclass: "ss6",
				textdata: data.string.lp6_1
			},
			{
				textclass: "ss7",
				textdata: data.string.lp7_1
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "mrsGren",
					imgid : 'mrs_gren',
					imgsrc: ""
				},
				{
					imgclass: "hand-s13",
					imgid : 'hand-icon',
					imgsrc: ""
				}
			]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:"spImage",
			imgid:'tb1',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p2s1_sb_txt,
		}]
	},
	// slide14
	{
		contentblockadditionalclass: "dblue_bg",
		imageload:true,
		sideboxes:[{
			sideboxclass:"leftBox",
			sideboxtxt:[{
				textclass:"boxToptext",
				textdata:data.string.reprod
			},{
				textclass:"boxMidTxt",
				textdata:data.string.p2s13_lftxt
			}]
		},
		{
			sideboxclass:"rightBox",
			imageblock:[{
				imagestoshow:[{
					imgclass: "fullbg",
					imgid : 'bg02',
					imgsrc: ""
				},
				{
					imgclass: "shepGif",
					imgid : 'sheep',
					imgsrc: ""
				},
				{
					imgclass: "lambGif",
					imgid : 'lamb',
					imgsrc: ""
				}
				]
			}]
		}]
	},
	// slide15
	{
		contentblockadditionalclass: "dblue_bg",
		extratextblock:[{
			textclass:"topinstrn",
			textdata:data.string.p2s15_top
		}],
		qnflip:[{
			flipclass:"leftFlip",
			flipintclass:"intFlipLft",
			fliptxt:[{
				textclass:"fstTxt befflp",
				textdata:data.string.p2s15_qn1
			},{
				textclass:"fstTxt aftFlip hde",
				textdata:data.string.p2s15_ans1
			}]
		},{
			flipclass:"rightFlip",
			fliptxt:[{
				textclass:"fstTxt flpSecBef",
				textdata:data.string.p2s15_qn2
			},{
				textclass:"fstTxt flpSecAft hde",
				textdata:data.string.p2s15_ans2
			}]
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "s15Budi",
				imgid : 'budi_pp',
				imgsrc: ""
			}],
			imagelabels:[{
				imagelabelclass:"clkInstrn",
				imagelabeldata:data.string.p2s15_clk
			}]
		}]
	},
	// slide16-->clktxt
	{
		contentblockadditionalclass: "grenBg",
		uppertextblockadditionalclass: "lifeprocont",
		uppertextblock:[
			{
				textclass: "ss1",
				textdata: data.string.lp1_1
			},
			{
				textclass: "ss2",
				textdata: data.string.lp2_1
			},
			{
				textclass: "ss3",
				textdata: data.string.lp3_1
			},
			{
				textclass: "ss4",
				textdata: data.string.lp4_1
			},
			{
				textclass: "ss5",
				textdata: data.string.lp5_1
			},
			{
				textclass: "ss6",
				textdata: data.string.lp6_1
			},
			{
				textclass: "ss7",
				textdata: data.string.lp7_1
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "mrsGren",
					imgid : 'mrs_gren',
					imgsrc: ""
				},
				{
					imgclass: "hand-s16",
					imgid : 'hand-icon',
					imgsrc: ""
				}
			]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:"spImage",
			imgid:'tb1',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p2s1_sb_txt,
		}]
	},
	// slide17
	{
		contentblockadditionalclass: "dblue_bg",
		imageload:true,
		sideboxes:[{
			sideboxclass:"leftBox",
			sideboxtxt:[{
				textclass:"boxToptext",
				textdata:data.string.excret
			},{
				textclass:"boxMidTxt fnt3vh",
				textdata:data.string.p2s17_lftxt
			}]
		},
		{
			sideboxclass:"rightBox creamBg",
			imageblock:[{
				imagestoshow:[{
					imgclass: "fullbg",
					imgid : 'lung-and-kidney',
					imgsrc: ""
				}
				]
			}]
		}]
	},
	// slide18-->clktxt
	{
		contentblockadditionalclass: "grenBg",
		uppertextblockadditionalclass: "lifeprocont",
		uppertextblock:[
			{
				textclass: "ss1",
				textdata: data.string.lp1_1
			},
			{
				textclass: "ss2",
				textdata: data.string.lp2_1
			},
			{
				textclass: "ss3",
				textdata: data.string.lp3_1
			},
			{
				textclass: "ss4",
				textdata: data.string.lp4_1
			},
			{
				textclass: "ss5",
				textdata: data.string.lp5_1
			},
			{
				textclass: "ss6",
				textdata: data.string.lp6_1
			},
			{
				textclass: "ss7",
				textdata: data.string.lp7_1
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "mrsGren",
					imgid : 'mrs_gren',
					imgsrc: ""
				},
				{
					imgclass: "hand-s18",
					imgid : 'hand-icon',
					imgsrc: ""
				}
			]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:"spImage",
			imgid:'tb1',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p2s1_sb_txt,
		}]
	},
	// slide19
	{
		contentblockadditionalclass: "dblue_bg",
		imageload:true,
		sideboxes:[{
			sideboxclass:"leftBox",
			sideboxtxt:[{
				textclass:"boxToptext",
				textdata:data.string.nutrit
			},{
				datahighlightflag:"true",
				datahighlightcustomclass:"ylwTxt",
				textclass:"boxMidTxt fnt3vh",
				textdata:data.string.p2s19_lftxt
			}]
		},
		{
			sideboxclass:"rightBox creamBg",
			imageblock:[{
				imagestoshow:[{
					imgclass: "fullbg",
					imgid : 'nutrition',
					imgsrc: ""
				}
				]
			}]
		}]
	},
	// slide20
	{
		contentblockadditionalclass: "dblue_bg",
		imageload:true,
		sideboxes:[{
			sideboxclass:"leftBox",
			sideboxtxt:[{
				textclass:"boxToptext",
				textdata:data.string.nutrit
			},{
				datahighlightflag:"true",
				datahighlightcustomclass:"ylwTxt",
				textclass:"boxMidTxt fnt3vh",
				textdata:data.string.p2s20_lftxt
			}]
		},
		{
			sideboxclass:"rightBox creamBg",
			imageblock:[{
				imagestoshow:[{
					imgclass: "fullbg",
					imgid : 'photosynthesis',
					imgsrc: ""
				}
				]
			}]
		}]
	},
	// slide21
	{
		contentblockadditionalclass: "dblue_bg",
		imageload:true,
		sideboxes:[{
			sideboxclass:"leftBox",
			sideboxtxt:[{
				textclass:"boxToptext",
				textdata:data.string.nutrit
			},{
				datahighlightflag:"true",
				datahighlightcustomclass:"fadeInLtr",
				textclass:"boxMidTxt fnt3vh",
				textdata:data.string.p2s21_lftxt
			}]
		},
		{
			sideboxclass:"rightBox creamBg",
			imageblock:[{
				imagestoshow:[{
					imgclass: "fullbg show1",
					imgid : 'nutrition1',
					imgsrc: ""
				},
                    {
                        imgclass: "fullbg show2",
                        imgid : 'nutrition2',
                        imgsrc: ""
                    },
                    {
                        imgclass: "fullbg show3",
                        imgid : 'nutrition',
                        imgsrc: ""
                    }
				]
			}]
		}]
	},
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bg01", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ant", src: imgpath+"ant.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "mrs_gren", src: imgpath+"mrs_gren03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tb1", src: imgpath+"textbox01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cheetah_animated", src: imgpath+"cheetah_animated.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "respiration", src: imgpath+"respiration.png", type: createjs.AbstractLoader.IMAGE},
			{id: "breathing", src: imgpath+"breathing.png", type: createjs.AbstractLoader.IMAGE},
			{id: "resp_human", src: imgpath+"respiration01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "photosynthesis", src: imgpath+"nutrition-plant.png", type: createjs.AbstractLoader.IMAGE},
			{id: "feeling", src: imgpath+"feeling.png", type: createjs.AbstractLoader.IMAGE},
			{id: "feeling_gif", src: imgpath+"feeling01.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "budi_pp", src: imgpath+"mrs_gren04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box", src: imgpath+"box_scale.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pasang01", src: imgpath+"pasang01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pasang02", src: imgpath+"pasang02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pasang03", src: imgpath+"pasang03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sheep", src: imgpath+"sheep.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "lamb", src: imgpath+"lamb.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "bg02", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "lung-and-kidney", src: imgpath+"lung-and-kidney.png", type: createjs.AbstractLoader.IMAGE},
			{id: "nutrition", src: imgpath+"nutrition.png", type: createjs.AbstractLoader.IMAGE},
			{id: "nutrition1", src: imgpath+"nutrition01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "nutrition2", src: imgpath+"nutrition02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "close", src: imgpath+"close.png", type: createjs.AbstractLoader.IMAGE},
			{id: "previous", src: imgpath+"previous.png", type: createjs.AbstractLoader.IMAGE},
			{id: "next", src: imgpath+"next.png", type: createjs.AbstractLoader.IMAGE},
			{id: "man_running", src: imgpath+"rhino.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "dolphin", src: imgpath+"dolphin.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "tiger_carrying_cup", src: imgpath+"tiger_carrying_cup.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "hand-icon", src: imgpath+"hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "tipicon4", src: imgpath+"information-icon.png", type: createjs.AbstractLoader.IMAGE},
			{id: "feeling_01", src: imgpath+"feeling_01.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "pointing_hand", src: imgpath+"pointing_hand.png", type: createjs.AbstractLoader.IMAGE},
			{id: "doted_line", src: imgpath+"doted_line.png", type: createjs.AbstractLoader.IMAGE},
			{id: "respiration_of_plants", src: imgpath+"respiration_of_plants.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "photosynthesis-&-respiration", src: imgpath+"photosynthesis-&-respiration.png", type: createjs.AbstractLoader.IMAGE},

			{id: "corrimg", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrimg", src: "images/wrongicon.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: imgpath+"cloud.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"s2_p1.ogg"},
			{id: "sound_2", src: soundAsset+"s2_p2.ogg"},
			{id: "sound_2_1", src: soundAsset+"s2_p2_1.ogg"},
			{id: "sound_2_2", src: soundAsset+"s2_p2_2.ogg"},
			{id: "sound_2_3", src: soundAsset+"s2_p2_3.ogg"},
			{id: "sldSnd_1", src: soundAsset+"s2_p2_3.ogg"},
			{id: "sldSnd_2", src: soundAsset+"s2_p2_4.ogg"},
			{id: "sldSnd_3", src: soundAsset+"s2_p2_5.ogg"},
      {id: "sound_4", src: soundAsset+"s2_p4.ogg"},
      {id: "sound_5", src: soundAsset+"s2_p5.ogg"},
      {id: "sound_6", src: soundAsset+"s2_p6.ogg"},
      {id: "sound_7", src: soundAsset+"s2_p7.ogg"},
      {id: "sound_9", src: soundAsset+"s2_p9.ogg"},
      {id: "sound_9_1", src: soundAsset+"s2_p9_1.ogg"},
      {id: "sound_9_2", src: soundAsset+"s2_p9_2.ogg"},
      {id: "sound_11", src: soundAsset+"s2_p11.ogg"},
      {id: "sound_13", src: soundAsset+"s2_p13.ogg"},
      {id: "sound_14", src: soundAsset+"s2_p14.ogg"},
      {id: "sound_14_1", src: soundAsset+"s2_p14_1.ogg"},
      {id: "sound_14_2", src: soundAsset+"s2_p14_2.ogg"},
      {id: "sound_16", src: soundAsset+"s2_p16.ogg"},
      {id: "sound_18", src: soundAsset+"s2_p18.ogg"},
      {id: "sound_19", src: soundAsset+"s2_p19.ogg"},
      {id: "sound_20", src: soundAsset+"s2_p20.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside){
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ?
		alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		null ;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightstarttag2;
		var texthighlightstarttag3;
		var texthighlightendtag   = "</span>";
		if($alltextpara.length > 0){
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				(stylerulename = $(this).attr("data-highlightcustomclass")) :
				(stylerulename = "parsedstring") ;

				$(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				(stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
				(stylerulename2 = "parsedstring2") ;

				$(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				(stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
				(stylerulename3 = "parsedstring3") ;

				texthighlightstarttag = "<span class='"+stylerulename+"'>";
				texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
				texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
				replaceinstring       = $(this).html();
				replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
				replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


				replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
				replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


				replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
				replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	function navigationcontroller(islastpageflag){
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;

		if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}
    function goProcess(){
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch(countNext) {
            default:
                countNext++;
                templatecaller();
                break;
        }
    }
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		content[countNext].imageload?put_image_sec(content, countNext):"";
		put_speechbox_image(content, countNext);
		$(".cross-btn").attr('src', preload.getResult("close").src);
		$(".previous-btn").attr('src', preload.getResult("previous").src);
		$(".nex-btn").attr('src', preload.getResult("next").src);
		var slidesCounter = 1;
		var clickCounter = 0;
		var flipFlag_1 = true, flipFlag_2=true;
         $(".leftBox").css("z-index","20");
		function intfact(slidesCounter,totalSldCount){
            $(".previous-btn").css({"pointer-events":"none","background":"#717180"});
			$(".nex-btn").click(function(){
				$(".previous-btn").css({"pointer-events":"auto","background":"#1155cc"});
				if(slidesCounter<totalSldCount){
					if(slidesCounter==2){
						slidesCounter+=1;
						$(".bc"+slidesCounter).show(0);
						$(".bc"+(slidesCounter-1)).hide(0);
						$(".nex-btn").css({"pointer-events":"none","background":"#717180"});
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("sldSnd_"+slidesCounter);
						current_sound.play();
						current_sound.on('complete', function(){
							$(".cross-btn").show(0);
							$(".cross-btn").css("opacity","1");
						});
					}else{
						slidesCounter+=1;
						sound_player("sldSnd_"+slidesCounter);
						$(".bc"+slidesCounter).show(0);
						$(".bc"+(slidesCounter-1)).hide(0);
						$(".nex-btn, .previous-btn").css("pointer-events","auto");
					}
				}
			});
			$(".previous-btn").click(function(){
				$(".nex-btn").css("background","#1155cc");
				if(slidesCounter>1){
					slidesCounter-=1;
					sound_player("sldSnd_"+slidesCounter);
					$(".bc"+slidesCounter).show(0);
					$(".bc"+(slidesCounter+1)).hide(0);
					$(".nex-btn, .previous-btn").css("pointer-events","auto");
				}else{
					$(".previous-btn").css({"pointer-events":"none","background":"#717180"});
				}
			});
			$(".cross-btn").click(function(){
                $(".leftBox").css("z-index","20");
                $(".iffulldiv").css("opacity","0");
				$(".s2LongBox, .budi_s2").show();
				createjs.Sound.stop();
				navigationcontroller();
			});
		}
		switch(countNext){
			case 0:
                sound_player("sound_1");
                $nextBtn.css('display', 'none');
				$(".hand-s1,.ss1").css("cursor","pointer").click(function(){
					goProcess();
				});
			break;
			case 2:
                $(".hand-s3,.ss2").css("cursor","pointer").click(function(){
                    $prevBtn.css('display', 'block');
                    goProcess();
                });
                break;
			case 7:
                $(".hand-s8,.ss3").css("cursor","pointer").click(function(){
                    $prevBtn.css('display', 'block');
                    goProcess();
                });
                break;
			case 9:
                $(".hand-s11,.ss4").css("cursor","pointer").click(function(){
                    $prevBtn.css('display', 'block');
                    goProcess();
                });
                break;
			case 11:
                $(".hand-s13,.ss5").css("cursor","pointer").click(function(){
                    goProcess();
                });
                break;
			case 14:
                texthighlight($board)
                $(".hand-s16,.ss6").css("cursor","pointer").click(function(){
                    $prevBtn.css('display', 'block');
                    goProcess();
                });
                break;
			case 16:
                $(".hand-s18,.ss7").css("cursor","pointer").click(function(){
                    $prevBtn.css('display', 'block');
                    goProcess();
                });
			break;
			case 1:
				sound_player("sound_2");
				$(".ifImg1").attr('src', preload.getResult("man_running").src);
				$(".ifImg2").attr('src', preload.getResult("dolphin").src);
				$(".ifImg3").attr('src', preload.getResult("tiger_carrying_cup").src);
				// $(".nex-btn, .previous-btn").hide(0);
				$(".bc2, .bc3, .budi_s2, .s2LongBox, .cross-btn").hide(0);
				$(".lhs_oboTxt").eq(1).css("opacity","0");
					current_sound.on('complete', function(){
						$(".lhs_oboTxt:eq(1)").animate({opacity:"1"});
                        sound_player("sound_2_1");
						current_sound.on('complete',function(){
                            sound_player("sound_2_2");
                            $(".budi_s2").show(0);
							$(".s2LongBox").show(0).addClass("highLight");
						});
					});
				$(".s2LongBox").click(function(){
					$(".iffulldiv").css("opacity","1");
					$(".s2LongBox, .budi_s2").hide(0);
					sound_player("sound_2_3");
                    $(".leftBox").css("z-index","0");
				});
				var intFact = content[countNext].intfactdiv[0];
				var slides = intFact.boxcontent;
				var totalSldCount = slides.length;
				intfact(slidesCounter,totalSldCount);
			break;
			case 3:
                sound_player("sound_"+(countNext+1),1);
                $(".subTxt").append("<sub>"+2+"</sub>");
				// navigationcontroller(true);
			break;
			case 4:
                sound_player("sound_"+(countNext+1));
                $(".subTxt").append("<sub>"+2+"</sub>");
				// navigationcontroller(true);
			break;
			case 8:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play('sound_9');
			current_sound.play();
			$(".s9intFactImg").attr("src", preload.getResult("feeling_gif").src);
			$(".tchGif").hide(0);
			current_sound.on('complete', function(){
				$(".dragable").draggable({
					revert:true,
					cursor: "move"
				});
				$(".tchPlnt").droppable({
					accept:".dragable",
					drop: function(event, ui){
						$(".tchPlnt").hide(0);
						$(".tchGif").show(0);
						$(".greenBoxTxt, .tipS9").delay(1000).show(100);
						setTimeout(function(){
							$(".tchGif").hide(0);
							$(".tchPlnt").show(0);
                            sound_player("sound_"+(countNext+1)+"_1");
                        },2000);
					}
				});
				$(".greenBoxTxt, .tipS9").click(function(){
                    $(".leftBox").css("z-index","0");
                    sound_player("sound_"+(countNext+1)+"_2");
                    $(".s9InfDiv").show(500);
					$(".cross-btn").css("opacity","1");
				});
				$(".cross-btn").click(function(){
                    $(".leftBox").css("z-index","20");
					createjs.Sound.stop();
					navigationcontroller();
					$(".s9InfDiv").hide(500);
					$(".cross-btn").css("opacity","0");
				});
			});
			break;
			case 10:
                sound_player("sound_"+(countNext+1),true);
                $(".psngSec, .psngThird, .line_2, .line_3").hide(0);
				$(".yr_1").css("pointer-events","none").addClass("chngclr");
				$(".yr_1").click(function(){
					$(".yearsTab").removeClass("chngclr");
					$(this).addClass("chngclr");
					$(".yearsTab").css("pointer-events","auto");
					$(".pasang, .doted_line").fadeOut(500);
					$(".psngFst, .line_1").delay(400).fadeIn(500);
					$(".yr_1").css("pointer-events","none");
				});
				$(".yr_3").click(function(){
                    $(".yearsTab").removeClass("chngclr");
                    $(this).addClass("chngclr");
                    $(".yearsTab").css("pointer-events","auto");
					$(".pasang, .doted_line").fadeOut(500);
					$(".psngSec, .line_2").delay(400).fadeIn(500);
					$(".yr_3").css("pointer-events","none");
				});
				$(".yr_9").click(function(){
                    $(".yearsTab").removeClass("chngclr");
                    $(this).addClass("chngclr");
                    $(".yearsTab").css("pointer-events","auto");
					$(".pasang, .doted_line").fadeOut(500);
					$(".psngThird, .line_3").delay(400).fadeIn(500);
					$(".yr_9").css("pointer-events","none");
				});
			break;
			case 13:
                sound_player("sound_"+(countNext+1));
                $(".leftFlip").click(function(){
                    flipFlag_1=false;
					flipFlag_1==false&&flipFlag_2==false?navigationcontroller():'';
					if($(".aftFlip").hasClass("hde")){
                        sound_player("sound_"+(countNext+1)+"_1");
                        if($(".leftFlip").hasClass("flipEfctSec")){
							$(".leftFlip").removeClass("flipEfctSec").addClass("flipEfct");
						}
						$(".leftFlip").addClass("flipEfct");
						$(".befflp, .aftFlip").removeClass("unrotate");
						$(".befflp, .aftFlip").addClass("rotatedTxt");
						$(".befflp").addClass("hde");
						$(".aftFlip").removeClass("hde");
					}else{
						$(".leftFlip").removeClass("flipEfct").addClass("flipEfctSec");
						$(".befflp, .aftFlip").removeClass("rotatedTxt");
						$(".befflp, .aftFlip").addClass("unrotate");
						$(".aftFlip").addClass("hde");
						$(".befflp").removeClass("hde");
					}
				});
				$(".rightFlip").click(function(){
                    flipFlag_2=false;
					flipFlag_1==false&&flipFlag_2==false?navigationcontroller():'';
					if($(".flpSecAft").hasClass("hde")){
                        sound_player("sound_"+(countNext+1)+"_2");

                        if($(".rightFlip").hasClass("flipEfctSec_1")){
							$(".rightFlip").removeClass("flipEfctSec_1").addClass("flipEfct_1");
						}
						$(".rightFlip").addClass("flipEfct_1");
						$(".flpSecBef, .flpSecAft").removeClass("unrotate");
						$(".flpSecBef, .flpSecAft").addClass("rotatedTxt");
						$(".flpSecBef").addClass("hde");
						$(".flpSecAft").removeClass("hde");
					}else{
						$(".rightFlip").removeClass("flipEfct_1").addClass("flipEfctSec_1");
						$(".flpSecBef, .flpSecAft").removeClass("rotatedTxt");
						$(".flpSecBef, .flpSecAft").addClass("unrotate");
						$(".flpSecAft").addClass("hde");
						$(".flpSecBef").removeClass("hde");
					}
				});
			break;
			case 19:
                sound_player("sound_"+(countNext+1),true);
                $(".show2,.show3").hide();
                $(".fadeInLtr").hide(0);
					$(".fadeInLtr:eq(0)").delay(7000).fadeIn(500);
					$(".show2").delay(7000).fadeIn(500);
						$(".fadeInLtr:eq(1)").delay(17000).fadeIn(500);
						$(".show3").delay(17000).fadeIn(500);
			break;
			default:
                sound_player("sound_"+(countNext+1),1);
                // navigationcontroller();
			break;
		}
		$(".buttonsel").click(function(){
			if($(this).hasClass("correct")){
				$(this).siblings(".corctopt").show(0);
				$(this).css({
					"background":"#98c02e",
					"border":"4px solid #ff0"
				});
				$(".buttonsel").css("pointer-events","none");
				current_sound.stop();
				play_correct_incorrect_sound(1)
				navigationcontroller();
			}else{
				$(this).siblings(".wrngopt").show(0);
				$(this).css({
					"background":"#f00",
					"border":"4px solid #980000"
				});
				$(this).css("pointer-events","none");
                current_sound.stop();
                play_correct_incorrect_sound(0)
			}
		});
	}

    function sound_player_seq(soundarray,navflag){
        createjs.Sound.stop();
        var current_sound = createjs.Sound.play(soundarray[0]);
        soundarray.splice( 0, 1);
        current_sound.on("complete", function(){
            if(soundarray.length > 0){
                sound_player_seq(soundarray,textarray, navflag);
            }else{
                if(navflag)
                    nav_button_controls(0);
            }

        });
    }

    function sound_player(sound_id,navigate){
		// $(".nex-btn, .previous-btn").hide(0);
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			navigate?navigationcontroller(false):"";
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image_sec(content, count){
		if(content[count].hasOwnProperty('sideboxes')){
			for(var i=0; i<content[count].sideboxes.length;i++){
				if(content[count].sideboxes[i].hasOwnProperty('imageblock'))
				{
					var imageblock = content[count].sideboxes[i].imageblock[0];
					if(imageblock.hasOwnProperty('imagestoshow')){
						var imageClass = imageblock.imagestoshow;
						for(var j=0; j<imageClass.length; j++){
							var image_src = preload.getResult(imageClass[j].imgid).src;
							//get list of classes
							var classes_list = imageClass[j].imgclass.match(/\S+/g) || [];
							var selector = ('.'+classes_list[classes_list.length-1]);
							$(selector).attr('src', image_src);
								// alert(i);
						}
					}
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				// console.log(selector);
				$(selector).attr('src', image_src);
			}
		}
	}

	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
			default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

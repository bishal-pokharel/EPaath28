var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide1
	{
		contentblockadditionalclass: "creamBg",
		extratextblock:[{
			textclass:"diytxt",
			textdata:data.string.diy
		}],
    imageblock:[{
    imagestoshow:[{
        imgclass:"diyimage",
        imgsrc:'',
        imgid:"diy"
    }]
    }]

	},
	// slide2
	{
		contentblockadditionalclass: "creamBg",
		extratextblock:[{
			textclass:"s2txt",
			textdata:data.string.p3s2txt
		}]
	},
	// slide3
	{
		contentblockadditionalclass: "creamBg",
		exetype1: [{
			optionsdivclass:"optionsdiv",
			ques_class:"oneLiner",
			textdata:data.string.qn_1,
	    exeoptions:[
	      {
					optdata:data.string.qn1_op1
	      },
	      {
					optdata:data.string.qn1_op2
	    	},
	      {
	        optaddclass: "correct",
					optdata:data.string.qn1_op3
	    	}
			]
		}],
		intfactdiv:[{
			ifdivboxclass:"intFactCont",
			boxcontent:[{
				boxcontentclass:"boxcontents",
				textclass:"funTitle",
				textdata:data.string.funfct,
				midimgclass:"sideImg hen"
			},{
				boxcontentclass:"boxcontents",
				textclass:"funTxt",
				textdata:data.string.hen_fact,
				midimgclass:"sideImg chick"
			}]
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"qnSideImg henImg",
				imgsrc:'',
				imgid:"hen"
			},{
				imgclass:"qnSideImg chickImg",
				imgsrc:'',
				imgid:"chick"
			},{
				imgclass:"tip tdark",
				imgsrc:'',
				imgid:"tip_icon_sec"
			},{
				imgclass:"tip tlight",
				imgsrc:'',
				imgid:"tip"
			}]
		}]
	},
	// slide4
	{
		contentblockadditionalclass: "creamBg",
		exetype1: [{
			optionsdivclass:"optionsdiv",
			ques_class:"oneLiner",
			textdata:data.string.qn_2,
	    exeoptions:[
	      {
					optdata:data.string.qn2_op1
	      },
	      {
					optdata:data.string.qn2_op2
	    	},
	      {
	        optaddclass: "correct",
					optdata:data.string.qn2_op3
	    	}
			]
		}],
		intfactdiv:[{
			ifdivboxclass:"intFactCont",
			boxcontent:[{
				boxcontentclass:"boxcontents",
				textclass:"funTitle",
				textdata:data.string.funfct,
				midimgclass:"funfactImg"
			},{
				boxcontentclass:"boxcontents",
				textclass:"funTxt",
				textdata:data.string.growth_fact,
			}]
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"qnSideImg henImg",
				imgsrc:'',
				imgid:"butterfly"
			},{
				imgclass:"tip",
				imgsrc:'',
				imgid:"tip"
			},{
				imgclass:"tip tdark",
				imgsrc:'',
				imgid:"tip_icon_sec"
			},{
				imgclass:"tip tlight",
				imgsrc:'',
				imgid:"tip"
			}]
		}]
	},
	// slide5
	{
		contentblockadditionalclass: "creamBg",
		exetype1: [{
			optionsdivclass:"optionsdiv",
			ques_class:"oneLiner",
			textdata:data.string.qn_3,
	    exeoptions:[
	      {
					optdata:data.string.qn3_op1
	      },
	      {
					optdata:data.string.qn3_op2
	    	},
	      {
	        optaddclass: "correct",
					optdata:data.string.qn3_op3
	    	}
			]
		}],
		intfactdiv:[{
			ifdivboxclass:"intFactCont",
			boxcontent:[{
				boxcontentclass:"boxcontents",
				textclass:"funTitle",
				textdata:data.string.funfct,
				midimgclass:"funfactImg whale"
			},{
				boxcontentclass:"boxcontents",
				textclass:"funTxt",
				textdata:data.string.nutrition_fact,
			}]
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"qnSideImg fodChain",
				imgsrc:'',
				imgid:"nutrition_animals"
			},{
				imgclass:"tip tdark",
				imgsrc:'',
				imgid:"tip_icon_sec"
			},{
				imgclass:"tip tlight",
				imgsrc:'',
				imgid:"tip"
			}]
		}]
	},
	// slide6
	{
		contentblockadditionalclass: "creamBg",
		exetype1: [{
			optionsdivclass:"optionsdiv",
			ques_class:"oneLiner",
			textdata:data.string.qn_4,
	    exeoptions:[
	      {
					optdata:data.string.qn4_op1
	      },
	      {
					optdata:data.string.qn4_op2
	    	},
	      {
	        optaddclass: "correct",
					optdata:data.string.qn4_op3
	    	}
			]
		}],
		intfactdiv:[{
			ifdivboxclass:"intFactCont",
			boxcontent:[{
				boxcontentclass:"boxcontents",
				textclass:"funTitle",
				textdata:data.string.funfct,
				midimgclass:"funfactImg ballBoy"
			},{
				boxcontentclass:"boxcontents",
				textclass:"funTxt",
				textdata:data.string.Sensitivity_fact,
			}]
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"qnSideImg sensImg",
				imgsrc:'',
				imgid:"sensitivity"
			},{
				imgclass:"tip tdark",
				imgsrc:'',
				imgid:"tip_icon_sec"
			},{
				imgclass:"tip tlight",
				imgsrc:'',
				imgid:"tip"
			}]
		}]
	},
	// slide7
	{
		contentblockadditionalclass: "creamBg",
		exetype1: [{
			optionsdivclass:"optionsdiv",
			ques_class:"oneLiner",
			textdata:data.string.qn_5,
	    exeoptions:[
	      {
					optdata:data.string.qn5_op1
	      },
	      {
					optdata:data.string.qn5_op2
	    	},
	      {
	        optaddclass: "correct",
					optdata:data.string.qn5_op3
	    	}
			]
		}],
		intfactdiv:[{
			ifdivboxclass:"intFactCont",
			boxcontent:[{
				boxcontentclass:"boxcontents",
				textclass:"funTitle",
				textdata:data.string.funfct,
				midimgclass:"funfactImg"
			},{
				boxcontentclass:"boxcontents",
				textclass:"funTxt",
				textdata:data.string.Respiration_fact,
			}]
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"qnSideImg resp",
				imgsrc:'',
				imgid:"respiration"
			},{
				imgclass:"tip tdark",
				imgsrc:'',
				imgid:"tip_icon_sec"
			},{
				imgclass:"tip tlight",
				imgsrc:'',
				imgid:"tip"
			}]
		}]
	},
	// slide8
	{
		contentblockadditionalclass: "creamBg",
		exetype1: [{
			optionsdivclass:"optionsdiv",
			ques_class:"oneLiner",
			textdata:data.string.qn_6,
	    exeoptions:[
	      {
					optdata:data.string.qn6_op1
	      },
	      {
					optdata:data.string.qn6_op2
	    	},
	      {
	        optaddclass: "correct",
					optdata:data.string.qn6_op3
	    	}
			]
		}],
		intfactdiv:[{
			ifdivboxclass:"intFactCont",
			boxcontent:[{
				boxcontentclass:"boxcontents",
				textclass:"funTitle",
				textdata:data.string.funfct,
				midimgclass:"funfactImg eagle"
			},{
				boxcontentclass:"boxcontents",
				textclass:"funTxt egtxt",
				textdata:data.string.Movement_fact,
			}]
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"qnSideImg foxwlk",
				imgsrc:'',
				imgid:"movement_animal"
			},{
				imgclass:"tip tdark",
				imgsrc:'',
				imgid:"tip_icon_sec"
			},{
				imgclass:"tip tlight",
				imgsrc:'',
				imgid:"tip"
			}]
		}]
	},
	// slide9
	{
		contentblockadditionalclass: "creamBg",
		exetype1: [{
			optionsdivclass:"optionsdiv",
			ques_class:"oneLiner",
			textdata:data.string.qn_7,
	    exeoptions:[
	      {
					optdata:data.string.qn7_op1
	      },
	      {
					optdata:data.string.qn7_op2
	    	},
	      {
	        optaddclass: "correct",
					optdata:data.string.qn7_op3
	    	}
			]
		}],
		intfactdiv:[{
			ifdivboxclass:"intFactCont",
			boxcontent:[{
				boxcontentclass:"boxcontents",
				textclass:"funTitle",
				textdata:data.string.funfct,
				midimgclass:"funfactImg tlt"
			},{
				boxcontentclass:"boxcontents",
				textclass:"funTxt",
				textdata:data.string.breathing_fact,
			}]
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"qnSideImg resp",
				imgsrc:'',
				imgid:"lung-and-kidney"
			},{
				imgclass:"tip tdark",
				imgsrc:'',
				imgid:"tip_icon_sec"
			},{
				imgclass:"tip tlight",
				imgsrc:'',
				imgid:"tip"
			}]
		}]
	},
	// slide10
	{
		contentblockadditionalclass: "creamBg",
		imageblock:[{
			imagestoshow:[{
				imgclass:"squirrel",
				imgsrc:'',
				imgid:"squirrel01"
			}]
		}],
		extratextblock:[{
			textclass:"p3s10Txt",
			textdata:data.string.p3s10txt
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:"spImage",
			imgid:'bubble',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.p3s10txtbox,
		}]
	},
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "diy", src: "images/diy_bg/a_07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg01", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hen", src: imgpath+"hen.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "chick", src: imgpath+"chick.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "close", src: imgpath+"close.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tip", src: imgpath+"information-icon.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bamboo", src: imgpath+"bamboo.png", type: createjs.AbstractLoader.IMAGE},
			{id: "peregrine", src: imgpath+"peregrine.png", type: createjs.AbstractLoader.IMAGE},
			{id: "person-breathing-on-windowpanel01", src: imgpath+"person-breathing-on-windowpanel01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "person-breathing-on-windowpanel02", src: imgpath+"person-breathing-on-windowpanel02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "toilet", src: imgpath+"toilet.png", type: createjs.AbstractLoader.IMAGE},
			{id: "whale", src: imgpath+"whale.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy_holding_ball", src: imgpath+"boy_holding_ball.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "butterfly", src: imgpath+"butterfly.png", type: createjs.AbstractLoader.IMAGE},
			{id: "nutrition_animals", src: imgpath+"nutrition_animals.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sensitivity", src: imgpath+"sensitivity.png", type: createjs.AbstractLoader.IMAGE},
			{id: "respiration", src: imgpath+"respiration.png", type: createjs.AbstractLoader.IMAGE},
			{id: "movement_animal", src: imgpath+"movement_animal.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "lung-and-kidney", src: imgpath+"lung-and-kidney.png", type: createjs.AbstractLoader.IMAGE},
			{id: "squirrel01", src: imgpath+"squirrel01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bubble", src: imgpath+"bubble.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tip_icon_sec", src: imgpath+"information-icon.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hand-icon", src: imgpath+"hand-icon.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "corrimg", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrimg", src: "images/wrongicon.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: imgpath+"cloud.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_2", src: soundAsset+"s3_p2.ogg"},
			{id: "sound_3", src: soundAsset+"s3_p3.ogg"},
			{id: "sound_3_1", src: soundAsset+"s3_p3_1.ogg"},
			{id: "sound_4", src: soundAsset+"s3_p4.ogg"},
			{id: "sound_4_1", src: soundAsset+"s3_p4_1.ogg"},
			{id: "sound_5", src: soundAsset+"s3_p5.ogg"},
			{id: "sound_5_1", src: soundAsset+"s3_p5_1.ogg"},
			{id: "sound_6", src: soundAsset+"s3_p6.ogg"},
			{id: "sound_6_1", src: soundAsset+"s3_p6_1.ogg"},
			{id: "sound_7", src: soundAsset+"s3_p7.ogg"},
			{id: "sound_7_1", src: soundAsset+"s3_p7_1.ogg"},
			{id: "sound_8", src: soundAsset+"s3_p8.ogg"},
			{id: "sound_8_1", src: soundAsset+"s3_p8_1.ogg"},
			{id: "sound_9", src: soundAsset+"s3_p9.ogg"},
			{id: "sound_9_1", src: soundAsset+"s3_p9_1.ogg"},
			{id: "sound_10", src: soundAsset+"s3_p10.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside){
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ?
		alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		null ;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightstarttag2;
		var texthighlightstarttag3;
		var texthighlightendtag   = "</span>";
		if($alltextpara.length > 0){
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				(stylerulename = $(this).attr("data-highlightcustomclass")) :
				(stylerulename = "parsedstring") ;

				$(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				(stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
				(stylerulename2 = "parsedstring2") ;

				$(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				(stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
				(stylerulename3 = "parsedstring3") ;

				texthighlightstarttag = "<span class='"+stylerulename+"'>";
				texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
				texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
				replaceinstring       = $(this).html();
				replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
				replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


				replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
				replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


				replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
				replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	function navigationcontroller(islastpageflag){
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;

		if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.pageEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.pageEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
	}

	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		content[countNext].imageload?put_image_sec(content, countNext):"";
		put_speechbox_image(content, countNext);
		$(".cross-btn").attr('src', preload.getResult("close").src);
	 	/*for randomizing the options*/
		// function randomize(parent){
			// alert(parent);
			var parent = $(".optionsdiv");
			var divs = parent.children();
			while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}
		// }
		function popup(){
			$(".tip,.handIcon").click(function(){
				sound_player("sound_"+(countNext+1)+"_1");
				$(".iffulldiv").show(0);
			});
			$(".cross-btn").click(function(){
				current_sound.stop();
				$(".iffulldiv").hide(0);
				navigationcontroller(true);
			});
		}
		$(".tip,.handIcon").css("pointer-events","none");
		$(".tlight").hide(0);
		current_sound.stop();
		countNext!=0?sound_player("sound_"+(countNext+1)):play_diy_audio();
		switch(countNext){
			case 0:
				navigationcontroller();
				break;
			case 2:
				$(".hen").attr("src",preload.getResult("hen").src);
				$(".chick").attr("src",preload.getResult("chick").src);
				$(".iffulldiv").hide(0);
				popup();
			break;
			case 3:
				$(".funfactImg").attr("src",preload.getResult("bamboo").src);
				$(".iffulldiv").hide(0);
				popup();
			break;
			case 4:
				$(".funfactImg").attr("src",preload.getResult("whale").src);
				$(".iffulldiv").hide(0);
				popup();
			break;
			case 5:
				$(".funfactImg").attr("src",preload.getResult("boy_holding_ball").src);
				$(".iffulldiv").hide(0);
				popup();
			break;
			case 6:
				$(".funfactImg").attr("src",preload.getResult("person-breathing-on-windowpanel01").src);
				$(".iffulldiv").hide(0);
				popup();
			break;
			case 7:
				$(".funfactImg").attr("src",preload.getResult("peregrine").src);
				$(".iffulldiv").hide(0);
				popup();
			break;
			case 8:
				$(".funfactImg").attr("src",preload.getResult("toilet").src);
				$(".iffulldiv").hide(0);
				popup();
			break;
		}
		$(".buttonsel").click(function(){
			if($(this).hasClass("correct")){
				$(".creamBg").removeClass("creamBg").addClass("creamBglight");
				$(".tip,.handIcon").css("pointer-events","auto");
				$(this).siblings(".corctopt").show(0);
				$(this).css({
					"background":"#98c02e",
					"border":"4px solid #ff0"
				});
				current_sound.stop();
				play_correct_incorrect_sound(1);
				$(".tlight").show(0).addClass("bounceIn");
				$(".tdark").hide(0);
				// $(".handIcon").css("display","block");
				$(".buttonsel").css("pointer-events","none");
				// navigationcontroller(true);
			}else{
                current_sound.stop();
                play_correct_incorrect_sound(0);
				$(this).siblings(".wrngopt").show(0);
				$(this).css({
					"background":"#f00",
					"border":"4px solid #980000"
				});
				$(this).css("pointer-events","none");
			}
		});
	}

	function sound_player(sound_id){
		$(".nex-btn, .previous-btn").hide(0);
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			navigationcontroller()
		});
	}
	function sound_sec(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image_sec(content, count){
		if(content[count].hasOwnProperty('sideboxes')){
			for(var i=0; i<content[count].sideboxes.length;i++){
				if(content[count].sideboxes[i].hasOwnProperty('imageblock'))
				{
					var imageblock = content[count].sideboxes[i].imageblock[0];
					if(imageblock.hasOwnProperty('imagestoshow')){
						var imageClass = imageblock.imagestoshow;
						for(var j=0; j<imageClass.length; j++){
							var image_src = preload.getResult(imageClass[j].imgid).src;
							//get list of classes
							var classes_list = imageClass[j].imgclass.match(/\S+/g) || [];
							var selector = ('.'+classes_list[classes_list.length-1]);
							$(selector).attr('src', image_src);
								// alert(i);
						}
					}
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				// console.log(selector);
				$(selector).attr('src', image_src);
			}
		}
	}

	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');


		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
			default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

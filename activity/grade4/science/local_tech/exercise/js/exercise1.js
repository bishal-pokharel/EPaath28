var imgpath = $ref+"/images/";

Array.prototype.shufflearray = function() {
    var i = this.length,
        j, temp;
    while (--i > 0) {
        j = Math.floor(Math.random() * (i + 1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
    return this;
};
var audioPath;
if($lang == "en")
 audioPath = $ref + '/sound/en/';
else if($lang == "np")
 audioPath = $ref + '/sound/nep/';

var sound1 = new buzz.sound(audioPath + "click.ogg");
var content=[
	{
	//slide 0
    lowertextblock:[
      {
  			textdata : data.string.et1,
  			textclass : 'maininstruction',
  		},
    ],
		exerciseblock: [
		{
			textdata: data.string.ques1,

			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "rice-mill-main.png",
				labeltext: data.string.q1opt1
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "turbine.png",
				labeltext: data.string.q1opt2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "handpumpmain.png",
				labeltext: data.string.q1opt3
			},

			]
		}
		]
	},
	{
	//slide 1
    lowertextblock:[
      {
        textdata : data.string.et1,
        textclass : 'maininstruction',
      },
    ],
		exerciseblock: [
		{
			textdata: data.string.ques2,

			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "forexe/centrifuging.png",
				labeltext: data.string.q2opt1
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "rice-mill-main.png",
				labeltext: data.string.q2opt2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "handpumpmain.png",
				labeltext: data.string.q2opt3
			},

			]
		}
		]
	},
	{
	//slide 2
    lowertextblock:[
      {
        textdata : data.string.et1,
        textclass : 'maininstruction',
      },
    ],
		exerciseblock: [
		{
			textdata: data.string.ques3,

			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "handpumpmain.png",
				labeltext: data.string.q3opt1
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "rice-mill-main.png",
				labeltext: data.string.q3opt2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "turbine.png",
				labeltext: data.string.q3opt3
			},

			]
		}
		]
	},
	{
	//slide 3
    lowertextblock:[
      {
        textdata : data.string.et1,
        textclass : 'maininstruction',
      },
    ],
		exerciseblock: [
		{
			textdata: data.string.ques4,

			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "forexe/pounding-tool.png",
				labeltext: data.string.q4opt1
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "forexe/centrifuging.png",
				labeltext: data.string.q4opt2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "handpumpmain.png",
				labeltext: data.string.q4opt3
			},

			]
		}
		]
	},
	{
	//slide 4
    lowertextblock:[
      {
        textdata : data.string.et1,
        textclass : 'maininstruction',
      },
    ],
		exerciseblock: [
		{
			textdata: data.string.ques5,

			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "turbine.png",
				labeltext: data.string.q5opt1
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "rice-mill-main.png",
				labeltext: data.string.q5opt2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "handpumpmain.png",
				labeltext: data.string.q5opt3
			},

			]
		}
		]
	},
	{
	//slide 5
    lowertextblock:[
      {
        textdata : data.string.et1,
        textclass : 'maininstruction',
      },
    ],
		exerciseblock: [
		{
			textdata: data.string.ques6,

			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "forexe/pottery-wheel.png",
				labeltext: data.string.q6opt1
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "forexe/pounding-tool.png",
				labeltext: data.string.q6opt2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "forexe/centrifuging.png",
				labeltext: data.string.q6opt3
			},

			]
		}
		]
	},
	{
	//slide 6
    lowertextblock:[
      {
        textdata : data.string.et1,
        textclass : 'maininstruction',
      },
    ],
		exerciseblock: [
		{
			textdata: data.string.ques7,

			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "handpumpmain.png",
				labeltext: data.string.q7opt1
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "rice-mill-main.png",
				labeltext: data.string.q7opt2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "turbine.png",
				labeltext: data.string.q7opt3
			},

			]
		}
		]
	},
	{
	//slide 7
    lowertextblock:[
      {
        textdata : data.string.et1,
        textclass : 'maininstruction',
      },
    ],
		exerciseblock: [
		{
			textdata: data.string.ques8,

			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "forexe/loom.png",
				labeltext: data.string.q8opt1
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "forexe/pottery-wheel.png",
				labeltext: data.string.q8opt2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "forexe/centrifuging.png",
				labeltext: data.string.q8opt3
			},

			]
		}
		]
	},
	{
	//slide 8
    lowertextblock:[
      {
        textdata : data.string.et1,
        textclass : 'maininstruction',
      },
    ],
		exerciseblock: [
		{
			textdata: data.string.ques9,

			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "forexe/plough.png",
				labeltext: data.string.q9opt1
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "forexe/pottery-wheel.png",
				labeltext: data.string.q9opt2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "forexe/loom.png",
				labeltext: data.string.q9opt3
			},

			]
		}
		]
	},
	{
	//slide 9
    lowertextblock:[
      {
        textdata : data.string.et1,
        textclass : 'maininstruction',
      },
    ],
		exerciseblock: [
		{
			textdata: data.string.ques10,

			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "rice-mill-main.png",
				labeltext: data.string.q10opt1
			},
			{
				forshuffle: "options_sign incorrecttwo",
				imgsrc: imgpath + "turbine.png",
				labeltext: data.string.q10opt2
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "handpumpmain.png",
				labeltext: data.string.q10opt3
			},

			]
		}
		]
	}
	];

/*remove this for non random questions*/
content.shufflearray();

$(function (){
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	 /*values in this array is same as the name of images of eggs in image folder*/

	 //create eggs
	 var testin = new EggTemplate();

	 	//eggTemplate.eggMove(countNext);
 		testin.init(10);

	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);
	 	var testcount = 0;

	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);
	 	$('.congratulation').hide(0);
	 	$('.exefin').hide(0);

	 	/*generate question no at the beginning of question*/
	 	testin.numberOfQuestions();

	 	/*for randomizing the options*/
	 	var parent = $(".optionsdiv");
	 	var divs = parent.children();
	 	while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
	 	}
    if(countNext==0){
      sound1.play();
    }

	 	/*======= SCOREBOARD SECTION ==============*/
	 	/*random scoreboard eggs*/
	 	//var i = Math.floor(Math.random() * imageArray.length);
	 	//var randImg = imageArray[i];
	 	var ansClicked = false;
	 	var wrngClicked = false;

	 	$(".correctOne").click(function(){
			correct_btn(this);
	 		testin.update(true);
      play_correct_incorrect_sound(1);
	 		$nextBtn.show(0);
	 	});

	 	$(".incorrectOne").click(function(){
			incorrect_btn(this);
      play_correct_incorrect_sound(0);
	 		testin.update(false);
	 	});

	 	$(".incorrectTwo").click(function(){
			incorrect_btn(this);
      play_correct_incorrect_sound(0);
	 		testin.update(false);
	 	});

	 	function correct_btn(current_btn){
	 		$('.options_sign').addClass('disabled');
	 		$('.hidden_sign').html($(current_btn).html());
	 		$('.hidden_sign').addClass('fade_in');
	 		$('.optionscontainer').removeClass('forHover');
	 		$(current_btn).addClass('option_true');
	 	}

	 	function incorrect_btn(current_btn){
	 		$(current_btn).addClass('disabled');
	 		$(current_btn).addClass('option_false');
	 	}

	 	/*======= SCOREBOARD SECTION ==============*/
	 }

	 function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
		testin.gotoNext();

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

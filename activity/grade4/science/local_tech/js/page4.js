var imgpath = $ref + "/images/";
var audioPath = $ref + '/sound/eng/'

var audioPath;
if($lang == "en")
 audioPath = $ref + '/sound/en/p4/';
else if($lang == "np")
 audioPath = $ref + '/sound/nep/p4/';

var sound_c_1 = new buzz.sound((audioPath + "1.ogg"));
var sound_c_2 = new buzz.sound((audioPath + "2.ogg"));
var sound_c_3 = new buzz.sound((audioPath + "3.ogg"));
var sound_c_4 = new buzz.sound((audioPath + "4.ogg"));
var sound_c_5 = new buzz.sound((audioPath + "5.ogg"));
var sound_c_6 = new buzz.sound((audioPath + "6.ogg"));
var sound_c_7 = new buzz.sound((audioPath + "7.ogg"));
var sound_c_8 = new buzz.sound((audioPath + "8.ogg"));

var content=[
{
	//page 1
  headerblock : [
	{
		textclass : "header",
		textdata : data.string.header3
	},
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "pumpback",
			imgsrc : imgpath + "handpump.gif"
		},
		],
	}
	],
},
{
	//page 2
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "pumpback",
			imgsrc : imgpath + "handpump.gif"
		},
		{
			imgclass : "rightchar",
			imgsrc : imgpath + "dadicon.png"
		},
		],
		imagelabels : [
		{
			imagelabelclass: "chattext",
			imagelabeldata: data.string.p4text1_1
		}
		]
	}
	],
},
{
	//page 3
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "pumpback",
			imgsrc : imgpath + "handpump.gif"
		},
		{
			imgclass : "rightchar",
			imgsrc : imgpath + "dadicon.png"
		},
		],
		imagelabels : [
		{
			imagelabelclass: "chattext",
			imagelabeldata: data.string.p4text1
		}
		]
	}
	],
},
{
	//page 4
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "pumpback",
			imgsrc : imgpath + "handpumpmain.png"
		},
		{
			imgclass : "rightcharup",
			imgsrc : imgpath + "dadicon.png"
		},
		{
			imgclass : "ironpipe",
			imgsrc : imgpath + "ironpipe.png"
		},
		],
		imagelabels : [
		{
			imagelabelclass: "chattextup",
			imagelabeldata: data.string.p4text2
		}
		]
	}
	],
},
{
	//page 5
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "pumpback",
			imgsrc : imgpath + "handpumpmain.png"
		},
		{
			imgclass : "ironpipe",
			imgsrc : imgpath + "handel.png"
		},
		{
			imgclass : "rightchar",
			imgsrc : imgpath + "dadicon.png"
		},
		],
		imagelabels : [
		{
			imagelabelclass: "chattext",
			imagelabeldata: data.string.p4text3
		}
		]
	}
	],
},
{
	//page 6
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "pumpback",
			imgsrc : imgpath + "handpumpmain.png"
		},
		{
			imgclass : "ironpipe",
			imgsrc : imgpath + "handel.png"
		},
		{
			imgclass : "rightchar",
			imgsrc : imgpath + "dadicon.png"
		},
		],
		imagelabels : [
		{
			imagelabelclass: "chattext",
			imagelabeldata: data.string.p4text4
		}
		]
	}
	],
},
{
	//page 7
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "pumpback",
			imgsrc : imgpath + "handpump.gif"
		},
		{
			imgclass : "rightchar",
			imgsrc : imgpath + "dadicon.png"
		},
		],
		imagelabels : [
		{
			imagelabelclass: "chattext",
			imagelabeldata: data.string.p4text5
		}
		]
	}
	],
},
{
	//page 7
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "pumpback",
			imgsrc : imgpath + "handpump.gif"
		},
		{
			imgclass : "leftchar",
			imgsrc : imgpath + "girlicon.png"
		},
		],
		imagelabels : [
		{
			imagelabelclass: "chattext",
			imagelabeldata: data.string.p4text6
		}
		]
	}
	],
},
{
	headerblock : [
	{
		textclass : "header",
		textdata : data.string.p4text7
	},
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "ltimg lt1",
			imgsrc : imgpath + "lt02.jpg"
		},
		{
			imgclass : "ltimg lt2",
			imgsrc : imgpath + "lt03.jpg"
		},
		{
			imgclass : "ltimg lt3",
			imgsrc : imgpath + "lt04.jpg"
		},
		{
			imgclass : "ltimg lt4",
			imgsrc : imgpath + "lt05.jpg"
		},
		{
			imgclass : "ltimg lt5",
			imgsrc : imgpath + "lt06.jpg"
		},
		{
			imgclass : "ltimg lt6",
			imgsrc : imgpath + "lt07.jpg"
		},
		],
	}
	]
}
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
	*/
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("speechboxcontent", $("#speechboxcontent-partial").html());

	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */
 	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch (countNext){

			case 1:
				play_audio(sound_c_1);
			break;
			case 2:
				play_audio(sound_c_2);
			break;
			case 3:
				play_audio(sound_c_3);
			break;
			case 4:
				play_audio(sound_c_4);
			break;
			case 5:
				play_audio(sound_c_5);
			break;
			case 6:
				play_audio(sound_c_6);
			break;
			case 7:
				play_audio(sound_c_7);
			break;
			case 8:
				play_audio(sound_c_8);
			break;

			function play_audio(audio){
				audio.play();
				$prevBtn.hide(0);
				$nextBtn.hide(0);
				audio.bindOnce('ended', function(){
					navigationcontroller();
				});
			}
		}

	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

	loadTimelineProgress($total_page, countNext + 1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
		total_page = content.length;
		templateCaller();
	// });

});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span>";


					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

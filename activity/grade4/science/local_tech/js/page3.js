var imgpath = $ref + "/images/";
var audioPath;
if($lang == "en")
 audioPath = $ref + '/sound/en/p3/';
else if($lang == "np")
 audioPath = $ref + '/sound/nep/p3/';

var sound_c_1 = new buzz.sound((audioPath + "1.ogg"));
var sound_c_2 = new buzz.sound((audioPath + "2.ogg"));
var sound_c_3 = new buzz.sound((audioPath + "3.ogg"));
var sound_c_4 = new buzz.sound((audioPath + "4.ogg"));
var sound_c_5 = new buzz.sound((audioPath + "5.ogg"));
var sound_c_6 = new buzz.sound((audioPath + "6.ogg"));
var instrn = new buzz.sound((audioPath + "s3_p4.ogg"));

var content=[
{
	//starting page
	contentblockadditionalclass : "homeback",
  headerblock : [
	{
		textclass : "header",
		textdata : data.string.header2
	},
	],
},
{
	//page 1
	contentblockadditionalclass : "homeback",
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "rightchar",
			imgsrc : imgpath + "dadicon.png"
		},
		],
		imagelabels : [
		{
			imagelabelclass: "chattext",
			imagelabeldata: data.string.p3text1
		}
		]
	}
	],
},
{
	//page 2
	contentblockadditionalclass : "homeback",
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "rightchar",
			imgsrc : imgpath + "dadicon.png"
		},
		],
		imagelabels : [
		{
			imagelabelclass: "chattext",
			imagelabeldata: data.string.p3text2
		}
		]
	}
	],

},
{
	//page 3
	contentblockadditionalclass : "homeback",
	uppertextblock:[
		{
			textclass : "captionbox caption2",
			textdata : data.string.p3text3,
		}
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "wood",
			imgsrc : imgpath + "woodblock.png"
		},
		],
	}
	],

},
{
	//page 4
	contentblockadditionalclass : "gifback",
	draggableblock: [
	{
		draggabletextstyle: "draggableitem water",
		draggabledata: data.string.water
	},
	{
		draggabletextstyle: "draggableitem generator",
		draggabledata: data.string.generator
	},
	{
		draggabletextstyle: "draggableitem light",
		draggabledata: data.string.light
	},
	{
		draggabletextstyle: "draggableitem turbine",
		draggabledata: data.string.turbine
	},
	{
		draggabletextstyle: "draggableitem wire",
		draggabledata: data.string.wire
	},
	],
	uppertextblock:[
	{
			textclass : "captionbox caption2",
			textdata : data.string.p3text4,
	},
	{
		textclass : "watertext labelbox",
	},
	{
		textclass : "generatortext labelbox",
	},
	{
		textclass : "lighttext labelbox",
	},
	{
		textclass : "turbinetext labelbox",
	},
	{
		textclass : "wiretext labelbox",
	},
	],

},
{
	//page 5
	contentblockadditionalclass : "gifback",
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "rightcharup",
			imgsrc : imgpath + "dadicon.png"
		},
		],
		imagelabels : [
		{
			imagelabelclass: "chattextup",
			imagelabeldata: data.string.p3text5
		}
		]
	}
	],

},
{
	//page 6
	contentblockadditionalclass : "gifback",
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "leftcharup",
			imgsrc : imgpath + "girlicon.png"
		},
		],
		imagelabels : [
		{
			imagelabelclass: "chattextup",
			imagelabeldata: data.string.p3text6
		}
		]
	}
	],

},
{
	//page 7
	contentblockadditionalclass : "gifback",
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "leftcharup",
			imgsrc : imgpath + "girlicon.png"
		},
		],
		imagelabels : [
		{
			imagelabelclass: "chattextup",
			imagelabeldata: data.string.p3text7
		}
		]
	}
	],

},

];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
	*/
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("speechboxcontent", $("#speechboxcontent-partial").html());
	Handlebars.registerPartial("draggablecontent", $("#draggablecontent-partial").html());

	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */
 	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			!islastpageflag ?
			ole.footerNotificationHandler.pageEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			!islastpageflag ?
			ole.footerNotificationHandler.pageEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch (countNext){
			case 1:
				play_audio(sound_c_1);
			break;
			case 2:
				play_audio(sound_c_2);
			break;
			case 3:
				$nextBtn.hide(0);
        instrn.play();
				$('.wood').click(function(){
					$nextBtn.show(0);
					$(this).hide(0);
          instrn.stop();
					$('.homeback').css({
						"background":"url("+ imgpath + "turbine.gif)",
						"background-repeat": "no-repeat",
						"background-size": "100% 100%",
						});
				});
			break;
			case 4:
      play_audio(sound_c_6);
				var finCount = 0;
				$nextBtn.hide(0);
				$('.draggableitem').draggable({
					containment : ".generalTemplateblock",
					revert : "invalid",
					cursor : "move",
					zIndex: 100000,
				});

				$(".watertext").droppable({
				accept: ".water",
				drop: function (event, ui){
					$this = $(this);
					dropfunc(event, ui, $this);
				}
				});

				$(".generatortext").droppable({
				accept: ".generator",
				drop: function (event, ui){
					$this = $(this);
					dropfunc(event, ui, $this);
				}
				});

				$(".lighttext").droppable({
				accept: ".light",
				drop: function (event, ui){
					$this = $(this);
					dropfunc(event, ui, $this);
				}
				});

				$(".turbinetext").droppable({
				accept: ".turbine",
				drop: function (event, ui){
					$this = $(this);
					dropfunc(event, ui, $this);
				}
				});

				$(".wiretext").droppable({
				accept: ".wire",
				drop: function (event, ui){
					$this = $(this);
					dropfunc(event, ui, $this);
				}
				});

			function dropfunc(event, ui, $droppedOn){
				var classText = ui.draggable.text();
				ui.draggable.draggable('disable');
				ui.draggable.hide(0);
				/*var droppedOn = $(this);*/
				$droppedOn.html(classText);
				$droppedOn.removeClass('labelbox');
				$droppedOn.addClass('labelboxdropped');
				finCount++;
				if(finCount == 5)
					$nextBtn.show(0);
			}
			break;
			case 5:
				play_audio(sound_c_3);
			break;
			case 6:
				play_audio(sound_c_4);
			break;
			case 7:
				play_audio(sound_c_5);
			break;

			function play_audio(audio){
				audio.play();
				$prevBtn.hide(0);
				$nextBtn.hide(0);
				audio.bindOnce('ended', function(){
					navigationcontroller();
				});
			}
		}
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

	loadTimelineProgress($total_page, countNext + 1);

		if(countNext == 0)
		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
		total_page = content.length;
		templateCaller();
	// });

});

 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span>";


					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

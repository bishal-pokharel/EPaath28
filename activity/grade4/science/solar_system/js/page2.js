$(function(){

  var body = $("#mysample"),
      universe = $("#universe"),
      solarsys = $("#solar-system");
  var countNext = 0;
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var clickCountMyPlanet = 0;
  var audioPath;
  var $total_page = 3;
  if($lang == "en")
   audioPath = $ref + '/sounds/eng/';
  else if($lang == "np")
   audioPath = $ref + '/sounds/nep/';

  var sound_c_1 = new buzz.sound((audioPath + "p2_s0.ogg"));
  var sound_c_2 = new buzz.sound((audioPath + "p2_s1.ogg"));
  var sound_c_3 = new buzz.sound((audioPath + "p2_s2.ogg"));

    //Texts to appear
    $(".myCaptionBox").html(data.string.p1text8);
    $(".myCaptionBox2").html(data.string.p1text9);
    $(".myCaptionBox3").html(data.string.p1text10);

    generalTemplate();

      function generalTemplate(){
      loadTimelineProgress(3, countNext + 1);

      switch (countNext) {
      case 0:
            $('.myCaptionBox2').removeClass('cssfadein');
            $('.myCaptionBox3').removeClass('cssfadein');
            $('.myCaptionBox').addClass('cssfadein');
      $prevBtn.hide(0);
      $nextBtn.hide(0);
      play_audio(sound_c_1);
      break;

      case 1:
          $('.myCaptionBox').removeClass('cssfadein');
          $('.myCaptionBox3').removeClass('cssfadein');
          $('.myCaptionBox2').addClass('cssfadein');
      $prevBtn.hide(0);
      $nextBtn.hide(0);
      play_audio(sound_c_2);
      break;

       case 2:
           $('.myCaptionBox2').removeClass('cssfadein');
             $('.myCaptionBox1').removeClass('cssfadein');
             $('.myCaptionBox3').addClass('cssfadein');
       $prevBtn.hide(0);
       $nextBtn.hide(0);
       play_audio(sound_c_3);
      break;

    }

    function play_audio(audio){
      audio.play();
      console.log(countNext);
      audio.bindOnce('ended', function(){
         navigationcontroller();
      });
    }
  }



 /*     setTimeout(function(){
        ole.footerNotificationHandler.pageEndSetNotification();
      }, 12000);*/


    body.removeClass('opening').addClass("view-3D").delay(2000).queue(function() {
      $(this).removeClass('hide-UI').addClass("set-speed");
      $(this).dequeue();
    });

    $( ".planet" ).each(function( index ) {
      var planetName= $( this ).parent().parent('.orbit').attr('id');
          $(this).find('dt').html(data.string["p7_1_"+(index+1)]);
    });

    $("#sun").find('dt').html(data.string["p7_1_0"]);



  /**click moon**/
  $( "#earth .orbit").click(function(e)
   {
      e.stopPropagation();
	  clickCountMyPlanet++;
      whatClicked="moon";

      e.preventDefault();

      getMydata=getData(whatClicked);
      var source = $("#first-template").html();
      var template = Handlebars.compile(source);
      var html = template(getMydata);
      $(".myInfoBox").html(html).fadeIn(500,function()
      {
         // $(".absolutecls").hide(0);
      });

	  if(clickCountMyPlanet==4)
	  {
		  ole.footerNotificationHandler.pageEndSetNotification();
	  }
   });



  $(".orbit, #sun").click(function(e)
  {
      var ref = $(this).attr('id');
      whatClicked=ref;
	  clickCountMyPlanet++;
      solarsys.removeClass().addClass(ref);
      e.preventDefault();

      getMydata=getData(ref);
      var source = $("#first-template").html();
      var template = Handlebars.compile(source);
      var html = template(getMydata);

      $(".myInfoBox").html(html).fadeIn(500,function()
      {
          //$(".absolutecls").hide(0);
      });

	  if(clickCountMyPlanet==5)
	  {
		  ole.footerNotificationHandler.pageEndSetNotification();
	  }

  });

  $(".planet").click(function(e)
  {
      var ref = $(this).parent().parent('.orbit').attr('id');
      whatClicked=ref;
	  clickCountMyPlanet++;
      solarsys.removeClass().addClass(ref);

      e.preventDefault();

      getMydata=getData(ref);
      var source = $("#first-template").html();
      var template = Handlebars.compile(source);
      var html = template(getMydata);
      $(".myInfoBox").html(html).fadeIn(500,function()
      {
         // $(".absolutecls").hide(0);
      });

  });

  /****   datatable click    ***/
  $(".myInfoBox").on('click','#spaceNext',function(){
    $("#spacePrev").removeClass('activePrevNext').addClass('noprevnext');
    $("#spaceNext").addClass('activePrevNext').removeClass('noprevnext');

    $(".myInfoBox").find(".solarDefinition").hide(0);
    $(".myInfoBox").find('.relativeBox').show(0);
    $(".myInfoBox").find('.explanationMe').hide(0);
    $(".myInfoBox").find('.explanation2Me').hide(0);
     $(".myInfoBox").find(".whatIconVal").hide(0);

    $(".whatIconVal").fadeOut(10);
  });

   /****   basics click    ***/
  $(".myInfoBox").on('click','#spacePrev',function(){
    $("#spaceNext").removeClass('activePrevNext').addClass('noprevnext');
    $("#spacePrev").addClass('activePrevNext').removeClass('noprevnext');
    $(".myInfoBox").find(".solarDefinition").show(0);
    $(".myInfoBox").find('.relativeBox').hide(0);
    $(".myInfoBox").find('.explanationMe').show(0);
    $(".myInfoBox").find('.explanation2Me').hide(0);
     $(".myInfoBox").find(".whatIconVal").hide(0);

  });
    $nextBtn.on('click', function() {
                countNext++;
                templateCaller();
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function() {
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });
    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');

        if(countNext == 0)
            navigationcontroller();

        generalTemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }
    function navigationcontroller(islastpageflag){
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean'?
                alert("NavigationController : Hi Master, please provide a boolean parameter") :
                null;

        if(countNext == 0 && $total_page!=1){
            $nextBtn.show(0);
            $prevBtn.css('display', 'none');
        }
        else if($total_page == 1){
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');

            // if lastpageflag is true
            islastpageflag ?
                ole.footerNotificationHandler.lessonEndSetNotification() :
                ole.footerNotificationHandler.lessonEndSetNotification() ;
        }
        else if(countNext > 0 && countNext < $total_page-1){
            $nextBtn.show(0);
            $prevBtn.show(0);
        }
        else if(countNext == $total_page-1){
            $nextBtn.css('display', 'none');
            $prevBtn.show(0);

            // if lastpageflag is true
            islastpageflag ?
                ole.footerNotificationHandler.lessonEndSetNotification() :
                ole.footerNotificationHandler.pageEndSetNotification() ;
        }
    }
});

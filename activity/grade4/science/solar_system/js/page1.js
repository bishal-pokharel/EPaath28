var imgpath = $ref + "/";
var audioPath;
if($lang == "en")
 audioPath = $ref + '/sounds/eng/';
else if($lang == "np")
 audioPath = $ref + '/sounds/nep/';

var sound_c_1 = new buzz.sound((audioPath + "p1_s0.ogg"));
var sound_c_2 = new buzz.sound((audioPath + "p1_s1.ogg"));
var sound_c_3 = new buzz.sound((audioPath + "p1_s2.ogg"));
var sound_c_4 = new buzz.sound((audioPath + "p1_s3.ogg"));
var sound_c_5 = new buzz.sound((audioPath + "p1_s4.ogg"));
var sound_c_6 = new buzz.sound((audioPath + "p1_s5.ogg"));
var sound_c_7 = new buzz.sound((audioPath + "p1_s6.ogg"));
	var content=[
	//slide 0
	{

	hasheaderblock : false,
	contentblocknocenteradjust : true,
	additionalclasscontentblock : 'backgroundss',
	uppertextblock : [{
		textdata : data.lesson.chapter,
		textclass : 'lesson-title vertical-horizontal-center'
		}]
	},
	{
		uppertextblock:[
		{
			textclass : "firstheading caption1 cssfadein",
			textdata : data.string.p1text1,
		}
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "bgfront",
				imgsrc : imgpath + "images/ground.png"
			},
			{
				imgclass: "bgback2",
				imgsrc : imgpath + "images/sky.png"
			},

			{
				imgclass: "cloud1",
				imgsrc : imgpath + "images/whitecloud01.png"
			},
			{
				imgclass: "cloud2",
				imgsrc : imgpath + "images/whitecloud01.png"
			},

			],

		}
		],

		curvedanim: [
		{
			imgclass: "sunstill opa",
			imgsrc : imgpath + "images/sun.png"
		},

		]
	},


		//slide 1
		{
			uppertextblock:[
			{
				textclass : "firstheading caption1 cssfadeinmoon",
				textdata : data.string.p1text2,
			}
			],
			imageblock: [
			{
				imagetoshow: [
				{
					imgclass: "bgfront",
					imgsrc : imgpath + "images/ground.png"
				},
				{
					imgclass: "bgback",
					imgsrc : imgpath + "images/sky.png"
				},
				{
					imgclass: "bgback2",
					imgsrc : imgpath + "images/nightbg.png"
				},

				{
					imgclass: "cloud1",
					imgsrc : imgpath + "images/greycloud01.png"
				},
				{
					imgclass: "cloud2",
					imgsrc : imgpath + "images/greycloud01.png"
				},
				{
					imgclass: "cloud1 dayfadeout",
					imgsrc : imgpath + "images/whitecloud01.png"
				},
				{
					imgclass: "cloud2 dayfadeout",
					imgsrc : imgpath + "images/whitecloud01.png"
				},
				],
			}
			],
			curvedanim: [
			{
				animouter: "sunouter",
				imgclass: "sunrise sunanimation opa",
				imgsrc : imgpath + "images/sun.png"
			},
			{
				animouter: "moonouter",
				imgclass: "moonrise moonanimation opa",
				imgsrc : imgpath + "images/moon.png"
			},
			]
		},
	//slide 2
	{
		uppertextblock:[
		{
			textclass : "firstheading caption1 cssfadein",
			textdata : data.string.p1text3,
		}
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "bgfront",
				imgsrc : imgpath + "images/ground.png"
			},
			{
				imgclass: "bgback2",
				imgsrc : imgpath + "images/nightbg.png"
			},
			{
				imgclass: "cloud1",
				imgsrc : imgpath + "images/greycloud01.png"
			},
			{
				imgclass: "cloud2",
				imgsrc : imgpath + "images/greycloud01.png"
			},
			],
		}
		],
		curvedanim: [
		{
			imgclass: "moonstill opa",
			imgsrc : imgpath + "images/moon.png"
		},
		]
	},
	//slide 3
	{
		uppertextblock:[
		{
			textclass : "secheading caption2 cssfadein",
			textdata : data.string.p1text5,
		}
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "bgback2",
				imgsrc : imgpath + "images/bg.png"
			},

			{
				imgclass: "sun1",
				imgsrc : imgpath + "images/sun400x400.png"
			},
			],
		}
		],
	},
		//slide 4
		{
			uppertextblock:[
			{
				textclass : "secheading caption2 cssfadein",
				textdata : data.string.p1text55,
			}
			],

			imageblock: [
			{
				imagetoshow: [
				{
					imgclass: "bgback2 bganimation",
					imgsrc : imgpath + "images/bg.png"
				},
				{
					imgclass: "sun1 sunanimation1",
					imgsrc : imgpath + "images/sun400x400.png"
				},
				],
			}
			],
		},
			//slide 5
			{
				uppertextblock:[
				{
					textclass : "secheading caption3",
					textdata : data.string.p1text6,
				},
				],
				extraimagearray:[
				{
					extraimage: "orbitdraw",
				},
				],
				imageblock: [
				{
					imagetoshow: [
					{
						imgclass: "bgback3",
						imgsrc : imgpath + "images/bg.png"
					},
					{
						imgclass: "sun2",
						imgsrc : imgpath + "images/sun400x400.png"
					},
					{
						imgclass: "earthrotation",
						imgsrc : imgpath + "images/earth400x400.png"
					},
					],
				}
				],
			},
	];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;
	var $total_page = content.length;

	/*recalculateHeightWidth();*/
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var total_page = 0;
	loadTimelineProgress($total_page, countNext + 1);


	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


	//controls the navigational state of the program
	function navigationController(islastpageflag) {
  	typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

  	if (countNext == 0 && $total_page != 1) {
  		$nextBtn.delay(800).show(0);
  		$prevBtn.css('display', 'none');
  	} else if ($total_page == 1) {
  		$prevBtn.css('display', 'none');
  		$nextBtn.css('display', 'none');

		// if lastpageflag is true
		islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	} else if (countNext > 0 && countNext < $total_page - 1) {
		$nextBtn.show(0);
		$prevBtn.show(0);
	} else if (countNext == $total_page - 1) {
		$nextBtn.css('display', 'none');
		$prevBtn.show(0);

		// if lastpageflag is true
		islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}
}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		vocabcontroller.findwords(countNext);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		switch (countNext) {
			case 0:
				play_audio(sound_c_1);
			break;
			case 1:
				play_audio(sound_c_2);
			break;
			case 2:
			$nextBtn.hide(0);
			setTimeout(function(){
				play_audio(sound_c_3);
			}, 14000);
			break;
			case 3:
				play_audio(sound_c_4);
			break;
			case 4:
				play_audio(sound_c_5);
			break;
			case 5:
				play_audio(sound_c_6);
			break;
			case 6:
				play_audio(sound_c_7);
			break;
		}
	}

	function play_audio(audio){
		audio.play();
		audio.bindOnce('ended', function(){
				navigationController();
				//$("#audiorep").addClass("enableclick");
		});
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		loadTimelineProgress($total_page, countNext + 1);


		//navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}



	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

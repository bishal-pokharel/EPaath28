var imgpath = $ref + "/";
var audioPath;
if($lang == "en")
 audioPath = $ref + '/sounds/eng/';
else if($lang == "np")
 audioPath = $ref + '/sounds/nep/';

 var draganddrop = new buzz.sound((audioPath + "draganddrop.ogg"));
var sound_c_1 = new buzz.sound((audioPath + "p4_s0.ogg"));
var sound_c_2 = new buzz.sound((audioPath + "p4_s1.ogg"));
var sound_c_3 = new buzz.sound((audioPath + "p4_s2.ogg"));
var sound_c_4 = new buzz.sound((audioPath + "p4_s3.ogg"));
var sound_c_5 = new buzz.sound((audioPath + "p4_s4.ogg"));
var sound_c_6 = new buzz.sound((audioPath + "p4_s5.ogg"));
var sound_c_7 = new buzz.sound((audioPath + "p4_s6.ogg"));
var sound_c_8 = new buzz.sound((audioPath + "p4_s7.ogg"));
var sound_c_9 = new buzz.sound((audioPath + "p4_s8.ogg"));
var sound_c_10 = new buzz.sound((audioPath + "p4_s9.ogg"));
var sound_c_11 = new buzz.sound((audioPath + "p4_s10.ogg"));
// var content;

// function getContent(data){
	var content=[
	//slide 0
	{
		uppertextblock:[
		{
			textclass : "doityou",
			textdata : data.string.doityourself
		}],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "bgback",
				imgsrc : imgpath + "images/bg.png"
			},
			{
				imgclass: "squirrel",
				imgsrc : imgpath + "images/Squirrel_withboard04.png"
			},

			],

		}
		],
	},
	//slide 1
	{
		uppertextblock:[
		{
			textclass : "secheading caption3 cssfadein",
			textdata : data.string.p1text4,
		},

		{
			textclass : "planetcaption mercuryloc",
			textdata : data.string.p2text1,
		},
		{
			textclass : "planetcaption venusloc",
			textdata : data.string.p2text2,
		},
		{
			textclass : "planetcaption earthloc",
			textdata : data.string.p2text3,
		},
		{
			textclass : "planetcaption marsloc",
			textdata : data.string.p2text4,
		},
		{
			textclass : "planetcaption jupiterloc",
			textdata : data.string.p2text5,
		},
		{
			textclass : "planetcaption saturnloc",
			textdata : data.string.p2text6,
		},
		{
			textclass : "planetcaption uranusloc",
			textdata : data.string.p2text7,
		},
		{
			textclass : "planetcaption neptuneloc",
			textdata : data.string.p2text8,
		},
		],

		extradivarray:[
		{
			extradiv: "mercurydrop",
		},
		{
			extradiv: "venusdrop",
		},
		{
			extradiv: "earthdrop",
		},
		{
			extradiv: "marsdrop",
		},
		{
			extradiv: "jupiterdrop",
		},
		{
			extradiv: "satrundrop",
		},
		{
			extradiv: "uranusdrop",
		},
		{
			extradiv: "neptunedrop",
		},

		],

		imageblock: [
		{
			imagetoshow: [



			{
				imgclass: "bgback2",
				imgsrc : imgpath + "images/dropanddrag.png"
			},

			{
				imgclass: "planet mercurydrag",
				imgsrc : imgpath + "images/page13/mercury.png"
			},
			{
				imgclass: "planet venusdrag",
				imgsrc : imgpath + "images/page13/venus.png"
			},
			{
				imgclass: "planet earthdrag",
				imgsrc : imgpath + "images/page13/earth.png"
			},
			{
				imgclass: "planet marsdrag",
				imgsrc : imgpath + "images/page13/mars.png"
			},
			{
				imgclass: "planet jupiterdrag",
				imgsrc : imgpath + "images/page13/jupiter.png"
			},
			{
				imgclass: "planet satrundrag",
				imgsrc : imgpath + "images/page13/planet-saturn.png"
			},
			{
				imgclass: "planet uranusdrag",
				imgsrc : imgpath + "images/page13/uranus.png"
			},
			{
				imgclass: "planet neptunedrag",
				imgsrc : imgpath + "images/page13/neptune.png"
			},



			],

		}
		],
	},

//slide 2
{
	uppertextblock:[
	{
		textclass : "secheading caption5",
		textdata : data.string.p2text12,
	},
	{
		textclass : "secheading caption6",
		textdata : data.string.p2text13,
	},
	{
		textclass : "secheading caption7",
		textdata : data.string.p2text14,
	},
	],

	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "bgback",
			imgsrc : imgpath + "images/bg.png"
		},
		],

	}
	],
},

//slide 3
{
	uppertextblock:[
	{
		textclass : "secheading caption3 cssfadein",
		textdata : data.string.p2text15,
	},
	{
		textclass : "secheading caption3-3 cssfadein",
		textdata : data.string.p2text16,
	},
	],

	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "bgback",
			imgsrc : imgpath + "images/bg.png"
		},
		{
			imgclass: "squirelimg",
			imgsrc : imgpath + "images/squirrel_what_animated.svg"
		},
		],

	}
	],
},



//slide 4
{
	uppertextblock:[
	{
		textclass : "secheading caption4",
		textdata : data.string.p2text10,
	},
	],
	extradivarray:[
	{
		extradiv: "orbitdraw",
	},
	],

	imageblock: [
	{
		imagetoshow: [



		{
			imgclass: "bgback2",
			imgsrc : imgpath + "images/bg.png"
		},

		{
			imgclass: "sun2",
			imgsrc : imgpath + "images/earth400x400.png"
		},
		{
			imgclass: "earthrotation",
			imgsrc : imgpath + "images/page13/moon.png"
		},

		],
	}
	],

},

	//slide 5
	{
		uppertextblock:[
		{
			textclass : "secheading caption4 cssfadein",
			textdata : data.string.p2text11,
		},
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "bgback",
				imgsrc : imgpath + "images/page13/solarsystem.jpg"
			},
			],

		}
		],
	},

	//slide 6
	{
		uppertextblock:[
		{
			textclass : "secheading caption3-2 cssfadein",
			textdata : data.string.p2text17,
		},
		],

		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "bgback",
				imgsrc : imgpath + "images/masses-of-ice.jpg"
			},
			],

		}
		],
	},

	//slide 7
	{
		uppertextblock:[

		{
			textclass : "secheading caption3-2 cssfadein",
			textdata : data.string.p2text18,
		},
		],

		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "bgback",
				imgsrc : imgpath + "images/masses-of-ice.jpg"
			},
			],

		}
		],
	},

	];
// 	return content;
// }


$(function(){


	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;
	var $total_page = content.length;

	/*recalculateHeightWidth();*/
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var total_page = 0;
	loadTimelineProgress($total_page, countNext + 1);


	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


	//controls the navigational state of the program
	function navigationController(islastpageflag) {
  	typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

  	if (countNext == 0 && $total_page != 1) {
  		$nextBtn.delay(800).show(0);
  		$prevBtn.css('display', 'none');
  	} else if ($total_page == 1) {
  		$prevBtn.css('display', 'none');
  		$nextBtn.css('display', 'none');

		// if lastpageflag is true
		islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	} else if (countNext > 0 && countNext < $total_page - 1) {
		$nextBtn.show(0);
		$prevBtn.show(0);
	} else if (countNext == $total_page - 1) {
		$nextBtn.css('display', 'none');
		$prevBtn.show(0);

		// if lastpageflag is true
		islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.lessonEndSetNotification();
	}
}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		vocabcontroller.findwords(countNext);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		switch (countNext) {
			case 0:
				// play_audio(sound_c_1);
        play_diy_audio();
        setTimeout(function(){
      		$nextBtn.show(0);
        },2000);
			break;
			case 1:
			play_audio(draganddrop, true);
			$nextBtn.hide(0);
			var p1 = false;
			var p2 = false;
			var p3 = false;
			var p4 = false;
			var p5 = false;
			var p6 = false;
			var p7 = false;
			var p8 = false;

			function nextcheck(){
				if(p1 && p2 && p3 && p4 && p5 && p6 && p7 && p8){
					$nextBtn.show(0);
				}
			}

			$(".mercurydrag").draggable({
				cursor : "move",
				revert : "invalid",
				zIndex: 1000,
			});

			$(".venusdrag").draggable({
				cursor : "move",
				revert : "invalid",
				zIndex: 1000,
			});

			$(".earthdrag").draggable({
				cursor : "move",
				revert : "invalid",
				zIndex: 1000,
			});

			$(".marsdrag").draggable({
				cursor : "move",
				revert : "invalid",
				zIndex: 1000,
			});

			$(".jupiterdrag").draggable({
				cursor : "move",
				revert : "invalid",
				zIndex: 1000,
			});

			$(".satrundrag").draggable({
				cursor : "move",
				revert : "invalid",
				zIndex: 1000,
			});

			$(".uranusdrag").draggable({
				cursor : "move",
				revert : "invalid",
				zIndex: 1000,
			});

			$(".neptunedrag").draggable({
				cursor : "move",
				revert : "invalid",
				zIndex: 1000,
			});


			$(".mercurydrop").droppable({
				accept: ".mercurydrag",
				drop: function(event, ui){
					$(".mercurydrag").css({
						"z-index": "auto",
						"left": "15.9%",
						"top": "43.2%"
					});
					$(".mercuryloc").css({
						"left": "12%",
						"top": "38%"
					});
					p1 = true;
					nextcheck();
				}
			});

			$(".venusdrop").droppable({
				accept: ".venusdrag",
				drop: function(event, ui){
					$(".venusdrag").css({
						"z-index": "auto",
						"left": "21%",
						"top": "42%"
					});
					$(".venusloc").css({
						"left": "20%",
						"top": "37%"
					});
					p2 = true;
					nextcheck();
				}
			});

			$(".earthdrop").droppable({
				accept: ".earthdrag",
				drop: function(event, ui){
					$(".earthdrag").css({
						"z-index": "auto",
						"left": "27%",
						"top": "42%"
					});
					$(".earthloc").css({
						"left": "26%",
						"top": "37%"
					});
					p3 = true;
					nextcheck();
				}
			});

			$(".marsdrop").droppable({
				accept: ".marsdrag",
				drop: function(event, ui){
					$(".marsdrag").css({
						"z-index": "auto",
						"left": "34%",
						"top": "43%"
					});
					$(".marsloc").css({
						"left": "33%",
						"top": "38%"
					});
					p4 = true;
					nextcheck();
				}
			});

			$(".jupiterdrop").droppable({
				accept: ".jupiterdrag",
				drop: function(event, ui){
					$(".jupiterdrag").css({
						"z-index": "auto",
						"left": "44%",
						"top": "37%"
					});
					$(".jupiterloc").css({
						"left": "46%",
						"top": "32%"
					});
					p5 = true;
					nextcheck();
				}
			});

			$(".satrundrop").droppable({
				accept: ".satrundrag",
				drop: function(event, ui){
					$(".satrundrag").css({
						"z-index": "auto",
						"left": "53%",
						"top": "37%"
					});
					$(".saturnloc").css({
						"left": "61%",
						"top": "33%"
					});
					p6 = true;
					nextcheck();
				}
			});

			$(".uranusdrop").droppable({
				accept: ".uranusdrag",
				drop: function(event, ui){
					$(".uranusdrag").css({
						"z-index": "auto",
						"left": "74%",
						"top": "40%"
					});
					$(".uranusloc").css({
						"left": "74%",
						"top": "35%"
					});
					p7 = true;
					nextcheck();
				}
			});

			$(".neptunedrop").droppable({
				accept: ".neptunedrag",
				drop: function(event, ui){
					$(".neptunedrag").css({
						"z-index": "auto",
						"left": "90%",
						"top": "40%"
					});
					$(".neptuneloc").css({
						"left": "89%",
						"top": "35%"
					});
					p8 = true;
					nextcheck();
				}
			});



			break;

			case 2:
			$nextBtn.hide(0);
			play_audio(sound_c_3);
			break;

			case 3:
			play_audio(sound_c_4);
			break;

			case 4:
			play_audio(sound_c_5);
			break;

			case 5:
			play_audio(sound_c_6);
			break;

			case 6:
			play_audio(sound_c_7);
			break;

			case 7:
			play_audio(sound_c_8);
			break;

			case 8:
			play_audio(sound_c_9);
			break;

			case 9:
			play_audio(sound_c_10);
			break;

		}
		function play_audio(audio, next){
			audio.play();
			audio.bindOnce('ended', function(){
				if(next == null)
					navigationController();
					//$("#audiorep").addClass("enableclick");
			});
		}


	}

	// Handlebars.registerHelper('listItem', function (from, to, context, options){
	    // var item = "";
	    // for (var i = from, j = to; i <= j; i++) {
	        // item = item + options.fn(context[i]);
	    // }
	    // return item;
	// });

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		loadTimelineProgress($total_page, countNext + 1);


		//navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}



	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

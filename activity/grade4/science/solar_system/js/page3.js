var imgpath = $ref + "/";
var audioPath;
if($lang == "en")
 audioPath = $ref + '/sounds/eng/';
else if($lang == "np")
 audioPath = $ref + '/sounds/nep/';

var sound_c_1 = new buzz.sound((audioPath + "p3_s0.ogg"));
var sound_c_2 = new buzz.sound((audioPath + "p3_s1.ogg"));
var sound_c_3 = new buzz.sound((audioPath + "p3_s2.ogg"));
var sound_c_4 = new buzz.sound((audioPath + "p3_s3.ogg"));
var sound_c_5 = new buzz.sound((audioPath + "p3_s4.ogg"));
var sound_c_6 = new buzz.sound((audioPath + "p3_s5.ogg"));
var sound_c_7 = new buzz.sound((audioPath + "p3_s6.ogg"));
var sound_c_8 = new buzz.sound((audioPath + "p3_s7.ogg"));
var sound_c_9 = new buzz.sound((audioPath + "p3_s8.ogg"));
var sound_c_10 = new buzz.sound((audioPath + "p3_s9.ogg"));
var sound_c_11 = new buzz.sound((audioPath + "p3_s10.ogg"));
	var content=[

		{
			uppertextblock:[
			{
				textclass : "myCaptionBox0 cssfadein",
				textdata : data.string.p3text2,
			}
			],
			imageblock: [
			{
				imagetoshow: [
				{
					imgclass: "bgback",
					imgsrc : imgpath + "images/page13/solarsystem.jpg"
				},
				],
			}
			],
		},
	//2nd slide
	{
		uppertextblock:[
		{
			textclass : "myCaptionBox",
			textdata : data.string.p3text3,
		},
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "bgback2",
				imgsrc : imgpath + "images/bg.png"
			},

			{
				imgclass: "sun1 pla-anim",
				imgsrc : imgpath + "images/sun400x400.png"
			},
			],
		}
		],
	},
		//3rd slide
		{
			uppertextblock:[
			{
				textclass : "myCaptionBox",
				textdata : data.string.p3text4,
			},
			],
			imageblock: [
			{
				imagetoshow: [
				{
					imgclass: "bgback2",
					imgsrc : imgpath + "images/bg.png"
				},
				{
					imgclass: "sungo",
					imgsrc : imgpath + "images/sun400x400.png"
				},
				{
					imgclass: "planets pla-mercury",
					imgsrc : imgpath + "images/page13/mercury.png"
				},
				],
			}
			],
		},
		//3rd slide
		{
			uppertextblock:[
			{
				textclass : "myCaptionBox",
				textdata : data.string.p3text5,
			},
			],
			imageblock: [
			{
				imagetoshow: [
				{
					imgclass: "bgback2",
					imgsrc : imgpath + "images/bg.png"
				},
				{
					imgclass: "planetsgo pla-mercury",
					imgsrc : imgpath + "images/page13/mercury.png"
				},

				{
					imgclass: "planets pla-venus",
					imgsrc : imgpath + "images/page13/venus.png"
				},
				],
			}
			],
		},
			//3rd slide
			{
				uppertextblock:[
				{
					textclass : "myCaptionBox",
					textdata : data.string.p3text6,
				},
				],
				imageblock: [
				{
					imagetoshow: [
					{
						imgclass: "planetsgo pla-venus",
						imgsrc : imgpath + "images/page13/venus.png"
					},
					{
						imgclass: "bgback2",
						imgsrc : imgpath + "images/bg.png"
					},
					{
						imgclass: "planets pla-earth",
						imgsrc : imgpath + "images/page13/earth.png"
					},
					],
				}
				],
			},
			//3rd slide
			{
				uppertextblock:[
				{
					textclass : "myCaptionBox",
					textdata : data.string.p3text7,
				},
				],
				imageblock: [
				{
					imagetoshow: [
					{
						imgclass: "planetsgo pla-earth",
						imgsrc : imgpath + "images/page13/earth.png"
					},
					{
						imgclass: "bgback2",
						imgsrc : imgpath + "images/bg.png"
					},
					{
						imgclass: "planets pla-mars",
						imgsrc : imgpath + "images/page13/mars.png"
					},
					],

				}
				],
			},
			//3rd slide
			{
				uppertextblock:[
				{
					textclass : "myCaptionBox",
					textdata : data.string.p3text8,
				},
				],
				imageblock: [
				{
					imagetoshow: [
					{
						imgclass: "planetsgo pla-mars",
						imgsrc : imgpath + "images/page13/mars.png"
					},
					{
						imgclass: "bgback2",
						imgsrc : imgpath + "images/bg.png"
					},
					{
						imgclass: "planets pla-jupiter",
						imgsrc : imgpath + "images/page13/jupiter.png"
					},
					],
				}
				],
			},
			//3rd slide
			{
				uppertextblock:[
				{
					textclass : "myCaptionBox",
					textdata : data.string.p3text9,
				},
				],
				imageblock: [
				{
					imagetoshow: [
					{
						imgclass: "planetsgo pla-jupiter",
						imgsrc : imgpath + "images/page13/jupiter.png"
					},
					{
						imgclass: "bgback2",
						imgsrc : imgpath + "images/bg.png"
					},
					{
						imgclass: "planets pla-saturntoround",
						imgsrc : imgpath + "images/page13/earth.png"
					},
					{
						imgclass: "planets pla-saturn",
						imgsrc : imgpath + "images/page13/planet-saturn.png"
					},
					],
				}
				],
			},
			//3rd slide
			{
				uppertextblock:[
				{
					textclass : "myCaptionBox",
					textdata : data.string.p3text10,
				},
				],
				imageblock: [
				{
					imagetoshow: [

					{
						imgclass: "planetsgo pla-saturn",
						imgsrc : imgpath + "images/page13/planet-saturn.png"
					},
					{
						imgclass: "bgback2",
						imgsrc : imgpath + "images/bg.png"
					},
					{
						imgclass: "planets pla-uranus",
						imgsrc : imgpath + "images/page13/uranus.png"
					},
					],
				}
				],
			},
			//3rd slide
			{
				uppertextblock:[
				{
					textclass : "myCaptionBox",
					textdata : data.string.p3text11,
				},
				],
				imageblock: [
				{
					imagetoshow: [
					{
						imgclass: "planetsgo pla-uranus",
						imgsrc : imgpath + "images/page13/uranus.png"
					},
					{
						imgclass: "bgback2",
						imgsrc : imgpath + "images/bg.png"
					},
					{
						imgclass: "planets pla-neptune",
						imgsrc : imgpath + "images/page13/neptune.png"
					},
					],
				}
				],
			},
	//1st slide
	{
		uppertextblock:[
		{
			textclass : "myCaptionBox0 cssfadein",
			textdata : data.string.p3text12,
		},
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "bgback",
				imgsrc : imgpath + "images/page13/solarsystem.jpg"
			},
			],
		}
		],
	},
	];

$(function(){


	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;
	var $total_page = content.length;

	/*recalculateHeightWidth();*/
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var total_page = 0;
	loadTimelineProgress($total_page, countNext + 1);


	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


	//controls the navigational state of the program
	function navigationController(islastpageflag) {
  	typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

  	if (countNext == 0 && $total_page != 1) {
  		$nextBtn.delay(800).show(0);
  		$prevBtn.css('display', 'none');
  	} else if ($total_page == 1) {
  		$prevBtn.css('display', 'none');
  		$nextBtn.css('display', 'none');

		// if lastpageflag is true
		islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	} else if (countNext > 0 && countNext < $total_page - 1) {
		$nextBtn.show(0);
		$prevBtn.show(0);
	} else if (countNext == $total_page - 1) {
		$nextBtn.css('display', 'none');
		$prevBtn.show(0);

		// if lastpageflag is true
		islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}
}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		vocabcontroller.findwords(countNext);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		switch (countNext) {
			case 0:
				play_audio(sound_c_1);
			break;
			case 1:
			$nextBtn.hide(0);
			setTimeout(function(){
				play_audio(sound_c_2);
			}, 3000);
			break;
			case 2:
			$nextBtn.hide(0);
			setTimeout(function(){
				play_audio(sound_c_3);
			}, 3000);
			break;
			case 3:
			$nextBtn.hide(0);
			setTimeout(function(){
				play_audio(sound_c_4);
			}, 3000);
			break;
			case 4:
			$nextBtn.hide(0);
			setTimeout(function(){
				play_audio(sound_c_5);
			}, 3000);
			break;
			case 5:
			$nextBtn.hide(0);
			setTimeout(function(){
				play_audio(sound_c_6);
			}, 3000);
			break;
			case 6:
			$nextBtn.hide(0);
			setTimeout(function(){
				play_audio(sound_c_7);
			}, 3000);
			break;
			case 7:
			$nextBtn.hide(0);
			setTimeout(function(){
				play_audio(sound_c_8);
			}, 3000);
			break;
			case 8:
			$nextBtn.hide(0);
			setTimeout(function(){
				play_audio(sound_c_9);
			}, 3000);
			break;
			case 9:
			$nextBtn.hide(0);
			setTimeout(function(){
				play_audio(sound_c_10);
			}, 3000);
			break;
			case 10:
			$nextBtn.hide(0);
				play_audio(sound_c_11);
			break;

		}

		function play_audio(audio){
			audio.play();
			audio.bindOnce('ended', function(){
					navigationController();
					//$("#audiorep").addClass("enableclick");
			});
		}
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		loadTimelineProgress($total_page, countNext + 1);


		//navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}



	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

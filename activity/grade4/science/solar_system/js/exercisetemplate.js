var imgpath = $ref+"/exercise/images/";

var content=[
 	/*{
 		exercisedraggable: [
 			{
 				draggabledivs: [
 			 				{
 			 					dragclass: "drag1"
 			 					
 			 				},
 			 				{
 			 					
 			 				}
 			 			],
 			 	droppabledivs: [
 			 				{
 			 					dropclass: "drop1"
 			 					
 			 				},
 			 				{
 			 					dropclass: "drop2"
 			 					
 			 				}
 			 			]
 			}
 		],
 	},*/

 	//slide 0
	{
		exerciseblock: [
			{	
				textdata: '',
				
				exeoptions: [
					{
						check_corr: "correct",
						optdata: questions[0][1],
					},
					{
						check_corr: "incorrect",
						optdata: questions[0][2],
					},
					{
						check_corr: "incorrect",
						optdata: questions[0][3],
					},
					{
						check_corr: "incorrect",
						optdata: questions[0][4],
					}
				]
			}
		]

	},
	//slide 1
	{
		exerciseblock: [
			{
				quesimg: true,
				ques_img: "images/deer.png",
				textdata: data.string.ques2,
				
				exeoptions: [
					{
						check_corr: "incorrect",
						optdata: data.string.q1opt1,
					},
					{
						check_corr: "correct",
						optdata: data.string.q2opt2,
					},
					{
						check_corr: "incorrect",
						optdata: data.string.q2opt3,
					},
					{
						check_corr: "incorrect",
						optdata: data.string.q2opt4,
					}
				]
			}
		]

	},
	//slide 2
	{
		exerciseblock: [
			{
				quesimg: true,
				ques_img: "images/tiger01.png",
				textdata: data.string.ques3,
				
				exeoptions: [
					{
						check_corr: "incorrect",
						optdata: data.string.q2opt1,
					},
					{
						check_corr: "correct",
						optdata: data.string.q2opt2,
					},
					{
						check_corr: "incorrect",
						optdata: data.string.q2opt4,
					}
				]
			}
		]

	},
	//slide 3
	{
		exerciseblock: [
			{
				quesimg: true,
				ques_img: "images/lion.png",
				textdata: data.string.ques4,
				
				exeoptions: [
					{
						check_corr: "incorrect",
						optdata: data.string.q2opt2,
					},
					{
						check_corr: "correct",
						optdata: data.string.q2opt3,
					},
					{
						check_corr: "incorrect",
						optdata: data.string.q2opt4,
					}
				]
			}
		]

	},

	//slide 4
	{
		exerciseblock: [
			{
				textdata: data.string.ques4,
				
				imageoptions: [
					{
						check_corr: "incorrect",
						imgsrc: "images/deer.png"
					},
					{
						check_corr: "incorrect",
						imgsrc: "images/elephant.png"
					},
					{
						check_corr: "correct",
						imgsrc: "images/giraffee.png"
					}
					
				]
			}
		]

	},
	 //slide 5
	{
		exerciseblock: [
			{
				textdata: data.string.ques4,
				
				imageoptions: [
					{
						check_corr: "incorrect",
						imgsrc: "images/giraffee.png"
					},
					{
						check_corr: "incorrect",
						imgsrc: "images/deer.png"
					},
					{
						check_corr: "incorrect",
						imgsrc: "images/elephant.png"
					},
					{
						check_corr: "correct",
						imgsrc: "images/lion.png"
					}
					
				]
			}
		]

	},
	//slide 6
	{
		exerciseblock: [
			{
				textdata: data.string.ques4,
				
				exeoptions: [
					{
						check_corr: "incorrect",
						optdata: data.string.q2opt1,
					},
					{
						check_corr: "incorrect",
						optdata: data.string.q2opt2,
					},
					{
						check_corr: "correct",
						optdata: data.string.q2opt3,
					},
					{
						check_corr: "incorrect",
						optdata: data.string.q2opt4,
					}
				]
			}
		]

	},
		//slide 7
	{
		exerciseblock: [
			{
				textdata: data.string.ques4,
				
				exeoptions: [
					{
						check_corr: "incorrect",
						optdata: data.string.q2opt1,
					},
					{
						check_corr: "incorrect",
						optdata: data.string.q2opt2,
					},
					{
						check_corr: "correct",
						optdata: data.string.q2opt3,
					},
					{
						check_corr: "incorrect",
						optdata: data.string.q2opt4,
					}
				]
			}
		]

	},


];
	/*return content.shufflearray();*/

$(function(){
	$(window).resize(function(){
		recalculateHeightWidth();
	});
	function recalculateHeightWidth(){
		var heightresized = $(window).height();
		var widthresized = $(window). width();
		var factor = 960/580;
		var equivalentwidthtoheight = widthresized/factor;
		
		if(heightresized >= equivalentwidthtoheight){
			$(".shapes_activity").css({"width": widthresized, "height": equivalentwidthtoheight});
		}else{
			$(".shapes_activity").css({"height": heightresized, "width": heightresized*960/580});
		}
		// $(".shapes_activity").css({"left": "50%" , 
									// "height": "50%" ,
									// "-webkit-transform" : "translate(-50%, - 50%)",
									// "transform" : "translate(-50%, - 50%)"});
	}
	
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;
	
	recalculateHeightWidth();
	
	var total_page = 0;
	
	
	/*
		inorder to use the handlebar partials we need to register them 
		to their respective handlebar partial pointer first
	*/
/*	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
*/		
	//controls the navigational state of the program
	function navigationController(){
		if(countNext == 0 && total_page != 1){
			$nextBtn.show(0);
		}else if(countNext > 0 && countNext < (total_page-1)){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}else if(countNext == total_page-1){
			$prevBtn.show(0);
		}
	}
	
	var imageArray = ["01","02","03","04","05","06","07","08","09","10"];
	
	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		$('.congratulation').hide(0);
		$board.html(html);
		
		testin.numberOfQuestions();
		$('#egg' + countNext).addClass('eggmove');
		
		rand_question();
		function rand_question(){
			$('.question').html("hello");
		}
		/*======= SCOREBOARD SECTION ==============*/
		
		var i = Math.floor(Math.random() * imageArray.length);
		var randImg = imageArray[i];
		console.log(i);
		imageArray.splice(i,1);
		var ansClicked = false;

		console.log(ansClicked);
		$(".buttonsel").click(function(){
			$(this).removeClass('forhover');
				if(ansClicked == false){
					if($(this).attr('id')=="correct"){
						$("#egg"+countNext).attr("src","images/eggs/"+ randImg +".png").removeClass('eggmove').attr("select","yes");
						$(this).css("background","#6AA84F");
						$(this).css("border","5px solid #B6D7A8");
						$(this).siblings(".corctopt").css("visibility","visible");
						$('.buttonsel').removeClass('forhover forhoverimg');
						ansClicked = true;

						if(countNext != total_page)
						$nextBtn.show(0);
					}
					else{
						$("#egg"+countNext).attr("src","images/eggs/egg_wrong.png").removeClass('eggmove');
						$(this).css("background","#EA9999");
						$(this).css("border","5px solid #efb3b3");
						$(this).css("color","#F66E20");
						$(this).siblings(".wrngopt").css("visibility","visible");
					}
			}
				}); 
			/*$("#correct").click(function(){
						$("#egg"+countNext).attr("src","images/eggs/"+ randImg +".png");
					}); */

			
		
		/*======= SCOREBOARD SECTION ==============*/



		$('.draggable').draggable({
			containment : "contentblock",
			cursor : "crosshair",
			revert : "invalid",
			appendTo : "body",
			zindex : 10000
		});

		$('.drop1').droppable({
			accept : ".drag1",
			drop : dropped

		});

		function dropped(event, ui){
			var dropped = ui.draggable;
			$(this).css("background","red");
				$nextBtn.show(0);
			
			if(dropped.hasClass("drop1")){
				$("#egg"+countNext).attr("src","images/eggs/"+ randImg +".png").removeClass('eggmove');
			}
		}
		
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);	
		switch (countNext) {
			case 0:
				
			break;
		} 
	}
	
	function templateCaller(){
		//convention is to always hide the prev and next button and show them based 
		//on the convention or page index		
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   
	}
	
	$nextBtn.on("click", function(){
		countNext++;
		if (countNext == content.length){
			$('.scoreboard').addClass('exefin').removeClass('scoreboard');
			$('.contentblock').hide(0);
			$('.congratulation').show(0);
		}
		else
		templateCaller();
		
	});
	
	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
	});
	
});

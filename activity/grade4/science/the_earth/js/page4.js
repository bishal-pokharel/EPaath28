var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+ "/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "dark",
		uppertextblock:[
			{
				textclass: "uptext",
				textdata: data.string.p4text1
			}
		],
		svgblock: [
		{
			svgname: "svgcontainer",
		},
		]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "dark",
		uppertextblock:[
			{
				textclass: "uptext",
				textdata: data.string.p4text8
			}
		],
		svgblock: [
		{
			svgname: "svgcontainer",
		},
	],
	livinnonlivin:[
		{
			//lbadditionalclass: "theliving",
			uppertextblock:[
				{
					textclass: "midtext",
					textdata: data.string.p4text2
				}
			]
		},
	]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "dark",
		svgblock: [
		{
			svgname: "svgcontainer",
		},
	],
	livinnonlivin:[
		{
			lbadditionalclass: "theliving",
			uppertextblock:[
				{
					textclass: "midtext",
					textdata: data.string.p4text3
				}
			]
		},
	]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "dark",
		uppertextblock:[
			{
				textclass: "uptext",
				textdata: data.string.p4text8
			}
		],
		svgblock: [
		{
			svgname: "svgcontainer",
		},
	],
	livinnonlivin:[
		{
			//lbadditionalclass: "theliving",
			uppertextblock:[
				{
					textclass: "uptext",
					textdata: data.string.p4text4
				},
				{
					textclass: "hiltext",
					textdata: data.string.p4text5
				},
				{
					textclass: "mountext",
					textdata: data.string.p4text6
				}
			],
			imageblock:[{
				imagestoshow:[
					{
						imgclass: "hills",
						imgid : 'owl',
						imgsrc: ""
					},
					{
						imgclass: "mounts",
						imgid : 'plant',
						imgsrc: ""
					}
				]
			}]
		},
	]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "dark",
		uppertextblock:[
			{
				textclass: "uptext",
				textdata: data.string.p4text8
			}
		],
		svgblock: [
		{
			svgname: "svgcontainer",
		},
	],
	livinnonlivin:[
		{
			//lbadditionalclass: "theliving",
			uppertextblock:[
				{
					textclass: "midtext",
					textdata: data.string.p4text7
				}
			],
		},
	]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			/**
			 * TODO
			 * Some unnecessary resources being loaded
			 */
			{id: "rocket", src: imgpath+"rockets.png", type: createjs.AbstractLoader.IMAGE},
			{id: "white", src: imgpath+"cloud_white.png", type: createjs.AbstractLoader.IMAGE},
			{id: "black", src: imgpath+"cloud_grey.png", type: createjs.AbstractLoader.IMAGE},
			{id: "earth", src: imgpath+"earth.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow1", src: imgpath+"arrow01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow2", src: imgpath+"arrow02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "earth-real", src: imgpath+"earth.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mountains", src: imgpath+"hill-and-mountain.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hills", src: imgpath+"earth.png", type: createjs.AbstractLoader.IMAGE},
			{id: "owl", src: imgpath+"owl.png", type: createjs.AbstractLoader.IMAGE},
			{id: "plant", src: imgpath+"plant.png", type: createjs.AbstractLoader.IMAGE},

			{id: "cloud-1", src: imgpath+"diy/cloudhalf.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-2", src: imgpath+"diy/cloudfull.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-3", src: imgpath+"cloud-and-without-sun.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-4", src: imgpath+"cloud_and_sun.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wind", src: imgpath+"wind.png", type: createjs.AbstractLoader.IMAGE},
			{id: "vapour", src: imgpath+"waves.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rain", src: imgpath+"waterdrop.png", type: createjs.AbstractLoader.IMAGE},
			{id: "thunder-1", src: imgpath+"thunder.png", type: createjs.AbstractLoader.IMAGE},
			{id: "thunder-2", src: imgpath+"thunder.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ground", src: imgpath+"diy/wave02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "well", src: imgpath+"wind.png", type: createjs.AbstractLoader.IMAGE},

			{id: "wave-1", src: imgpath+"diy/wave01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wave-2", src: imgpath+"diy/wave02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wave-3", src: imgpath+"diy/wave03.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"p4_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p4_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p4_s1_1.ogg"},
			{id: "sound_3", src: soundAsset+"p4_s2.ogg"},
			{id: "sound_4", src: soundAsset+"p4_s3.ogg"},
			{id: "sound_5", src: soundAsset+"p4_s3_1.ogg"},
			{id: "sound_6", src: soundAsset+"p4_s4.ogg"},
			{id: "sound_7", src: soundAsset+"p4_s4_1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		/**
		 * TODO
		 * Same as page 1
		 */
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);



		switch(countNext){
			case 0:
			sound_player("sound_0");
			var s = Snap(".svgcontainer");
			Snap.load(imgpath+"globe01.svg", function (f) {
				s.append(f);
			});
			break;
			case 1:
			$nextBtn.hide(0);
			sound_player("sound_1", "no");
			var s = Snap(".svgcontainer");
			Snap.load(imgpath+"globe01.svg", function (f) {
				s.append(f);
				$('.svgcontainer').find(".atmos").attr('class', 'highlight2');
				$(".highlight2").click(function(){
					$('.svgcontainer').find(".highlight2").attr('class', '');
					$(".livinnonlivinbox").addClass("theliving");
					sound_player("sound_2");
					//$('.svgcontainer').find("#Layer_1").attr('class', 'zoomwater');
					//$nextBtn.show(0);
				});
			});
			break;
			case 2:
			sound_player("sound_3");
			break;
			case 3:
			$nextBtn.hide(0);
			sound_player("sound_4", "no");
			var s = Snap(".svgcontainer");
			Snap.load(imgpath+"earth_new.svg", function (f) {
				s.append(f);
				$('.svgcontainer').find(".atmos").attr('class', 'highlight2');
				$(".highlight2").click(function(){
					$('.svgcontainer').find(".highlight2").attr('class', '');
					$(".livinnonlivinbox").addClass("theliving");
					sound_player("sound_5");
					//$('.svgcontainer').find("#Layer_1").attr('class', 'zoomwater');
					//$nextBtn.show(0);
				});
			});
			break;
			case 4:
			$nextBtn.hide(0);
			sound_player("sound_6", "no");
			var s = Snap(".svgcontainer");
			Snap.load(imgpath+"globe02.svg", function (f) {
				s.append(f);
				$('.svgcontainer').find(".atmos").attr('class', 'highlight2');
				$(".highlight2").click(function(){
					$('.svgcontainer').find(".highlight2").attr('class', '');
					$(".livinnonlivinbox").addClass("theliving");
					sound_player("sound_7");
					//$('.svgcontainer').find("#Layer_1").attr('class', 'zoomwater');
					//$nextBtn.show(0);
				});
			});
			break;

		}
	}
	/**
	 * TODO
	 * Same as page 1
	 */
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next == null)
			navigationcontroller();
		});
	}

	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2);
		});

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	/**
	 * TODO
	 * Same as page 1
	 */
	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}

	/**
	 * TODO
	 * Same as page 1
	 */
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');


		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	var windflag = true;
	$nextBtn.on('click', function() {

			createjs.Sound.stop();
			clearTimeout(timeoutvar);
			switch(countNext) {
			default:
				countNext++;
				templateCaller();
				break;
			}


	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

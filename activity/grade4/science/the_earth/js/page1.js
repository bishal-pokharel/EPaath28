var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+ "/";

/**
 * TODO
 * duplicate imgpath, remove one
 */
var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "dark",
		uppertextblock:[
			{
				textclass: "uptext",
				textdata: data.lesson.chapter
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "earth",
					imgid : 'earth',
					imgsrc: ""
				}
			]
		}]
	},
// slide0
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "sky",
	uppertextblock:[
	{
		textclass: "clouds moving"
	}
],

imageblock:[{
	imagestoshow:[
		{
			imgclass: "rocket",
			imgid : 'rocket',
			imgsrc: ""
		}
	]
}]
},
// slide1
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "dark",
	uppertextblock:[
		{
			textclass: "uptext",
			textdata: data.string.p1text1
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "earth",
				imgid : 'earth',
				imgsrc: ""
			},
			{
				imgclass: "cloblack-b1",
				imgid : 'black',
				imgsrc: ""
			},
			{
				imgclass: "cloblack-b2",
				imgid : 'black',
				imgsrc: ""
			},
			{
				imgclass: "cloblack-b3",
				imgid : 'black',
				imgsrc: ""
			},
			{
				imgclass: "cloblack-b4",
				imgid : 'black',
				imgsrc: ""
			},
			{
				imgclass: "clowhite-w1",
				imgid : 'white',
				imgsrc: ""
			},
			{
				imgclass: "clowhite-w2",
				imgid : 'white',
				imgsrc: ""
			},
			{
				imgclass: "clowhite-w3",
				imgid : 'white',
				imgsrc: ""
			},
			{
				imgclass: "clowhite-w4",
				imgid : 'white',
				imgsrc: ""
			}
		]
	}]
},
// slide2
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "dark",
	uppertextblock:[
		{
			textclass: "uptext",
			textdata: data.string.p1text2
		},
		{
			textclass: "dotext",
			textdata: data.string.p1text3
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "earth-still",
				imgid : 'earth',
				imgsrc: ""
			}
		]
	}]
},
// slide3
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "dark",
	uppertextblock:[
		{
			textclass: "uptext",
			textdata: data.string.p1text4
		},
		{
			textclass: "dotext",
			textdata: data.string.p1text5
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "earth-still squish",
				imgid : 'earth',
				imgsrc: ""
			}
		]
	}]
},
// slide4
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "dark",
	uppertextblock:[
		{
			textclass: "uptext",
			textdata: data.string.p1text6
		},
		{
			textclass: "ntext",
			textdata: data.string.p1text13
		},
		{
			textclass: "stext",
			textdata: data.string.p1text14
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "earth-squi",
				imgid : 'earth',
				imgsrc: ""
			},
			{
				imgclass: "earth-squi arr",
				imgid : 'arrow1',
				imgsrc: ""
			}
		]
	}]
},
// slide5
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "dark",
	uppertextblock:[
		{
			textclass: "uptext",
			textdata: data.string.p1text7
		},
		{
			textclass: "etext",
			textdata: data.string.p1text15
		},
		{
			textclass: "wtext",
			textdata: data.string.p1text16
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "earth-squi",
				imgid : 'earth',
				imgsrc: ""
			},
			{
				imgclass: "earth-squi arr",
				imgid : 'arrow2',
				imgsrc: ""
			}
		]
	}]
},
// slide6
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "dark",
	uppertextblock:[
		{
			textclass: "uptext",
			textdata: data.string.p1text8
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "earth-squi",
				imgid : 'earth',
				imgsrc: ""
			}
		]
	}]
},
// slide7
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "dark",
	uppertextblock:[
		{
			textclass: "uptext",
			textdata: data.string.p1text9
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "earth-still",
				imgid : 'earth-hot',
				imgsrc: ""
			}
		]
	}]
},
// slide8
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "dark",
	uppertextblock:[
		{
			textclass: "uptext",
			textdata: data.string.p1text10
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "earth-still",
				imgid : 'earth',
				imgsrc: ""
			}
		]
	}]
},
// slide9
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "dark",
	uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "yellow",
			textclass: "midtext",
			textdata: data.string.p1text11
		}
	]
},
// slide10
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "dark",
	uppertextblock:[
		{
			textclass: "midtext",
			textdata: data.string.p1text12
		}
	]
}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "rocket", src: imgpath+"rockets.png", type: createjs.AbstractLoader.IMAGE},
			{id: "white", src: imgpath+"cloud_white.png", type: createjs.AbstractLoader.IMAGE},
			{id: "black", src: imgpath+"cloud_grey.png", type: createjs.AbstractLoader.IMAGE},
			{id: "earth", src: imgpath+"earth.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow1", src: imgpath+"arrow01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow2", src: imgpath+"arrow02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "earth-hot", src: imgpath+"moonwithoutlight.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			/**
			 * TODO
			 * Unnecessary resources being loaded
			 */
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "soundtitle", src: soundAsset+"title.ogg"},
			// {id: "sound_0", src: soundAsset+"p1_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p1_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p1_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p1_s2_1.ogg"},
			{id: "sound_4", src: soundAsset+"p1_s3.ogg"},
			{id: "sound_5", src: soundAsset+"p1_s4.ogg"},
			{id: "sound_6", src: soundAsset+"p1_s5.ogg"},
			{id: "sound_7", src: soundAsset+"p1_s6.ogg"},
			{id: "sound_8", src: soundAsset+"p1_s7.ogg"},
			{id: "sound_9", src: soundAsset+"p1_s8.ogg"},
			{id: "sound_10", src: soundAsset+"p1_s9.ogg"},
			{id: "sound_11", src: soundAsset+"p1_s10.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		/**
	 	 * TODO
		 * This part is not needed so better to remove it
		 * I used this for testing soundjs initially and forgot to remove it later
		 */
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;


 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		/**
		 * TODO
		 * no need for  put_speechbox_image() and put_image2()
		 */
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext){
			case 0:
				sound_player("soundtitle");
				nav_button_controls(3000);
				break;
			case 1:
			
			nav_button_controls(5000);
			break;
			case 2:
			$(".uptext").hide(0);

				setTimeout(function(){
					$(".uptext").show(0);
					sound_player("sound_1");
				}, 1200);
			break;
			case 3:
			sound_player_duo("sound_2", "sound_3");
			break;
			case 4:
			sound_player("sound_4");
			break;
			case 5:
			sound_player("sound_5");
			break;
			case 6:
			sound_player("sound_6");
			break;
			case 7:
			sound_player("sound_7");
			break;
			case 8:
			sound_player("sound_8");
			break;
			case 9:
			sound_player("sound_9");
			break;
			case 10:
			sound_player("sound_10");
			break;
			case 11:
			sound_player("sound_11");
			break;
		}
	}
	/**
	 * TODO
	 * This function is being used
	 */
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(countNext==0){
				$nextBtn.hide(0);
			}else
			nav_button_controls(0);
		});
	}

	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2);
		});

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	/**
	 * TODO
	 * This function is not needed
	 */
	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	/**
	 * TODO
	 * This function is not needed
	 */
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentblockadditionalclass: 'purplebg',
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "communicationtext",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "a",
					imgsrc : '',
					imgid : 'newspaper'
				},
				{
					imgclass : "b",
					imgsrc : '',
					imgid : 'mobile'
				},
				{
					imgclass : "c",
					imgsrc : '',
					imgid : 'radio'
				},
				{
					imgclass : "d",
					imgsrc : '',
					imgid : 'letter'
				},
				{
					imgclass : "e",
					imgsrc : '',
					imgid : 'cordless'
				},
				{
					imgclass : "f",
					imgsrc : '',
					imgid : 'telephone'
				},
				{
					imgclass : "g",
					imgsrc : '',
					imgid : 'computer'
				},
				{
					imgclass : "h",
					imgsrc : '',
					imgid : 'tvlcd'
				},
			]
		}]

	},
	// slide1
	{
		uppertextblock:[{
			textdata: data.string.p2text2,
			textclass: "left_top_text",
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "sundar01",
					imgsrc : '',
					imgid : 'sunder04'
				},
				{
					imgclass : "radiowave",
					imgsrc : '',
					imgid : 'radiowave'
				}
			]
		}]
	},
	// slide3
	{
		uppertextblock:[{
			textdata: data.string.p2text3,
			textclass: "left_top_text",
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "sundar01",
					imgsrc : '',
					imgid : 'sunder04'
				},
				{
					imgclass : "radiowave",
					imgsrc : '',
					imgid : 'radiowave'
				}
			]
		}]
	},
	// slide4
	{
		uppertextblock:[{
			textdata: data.string.p2text4,
			textclass: "template-dialougebox2-top-white",
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "sundar01",
					imgsrc : '',
					imgid : 'sunder02'
				}
			]
		}]
	},
	// slide5
	{
		uppertextblock:[{
			textdata: data.string.p2text5,
			textclass: "template-dialougebox2-top-white",
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "sundar01",
					imgsrc : '',
					imgid : 'sunder02'
				}
			]
		}]
	},
	// slide6
	{
		uppertextblock:[{
			textdata: data.string.p2text5a,
			textclass: "template-dialougebox2-top-white",
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "sundar01",
					imgsrc : '',
					imgid : 'sunder03'
				}
			]
		}]
	},
	// slide7
	{
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "sundar01",
					imgsrc : '',
					imgid : 'sunder01'
				},
				{
					imgclass : "dotmove",
					imgsrc : '',
					imgid : 'dotmove'
				}
			]
		}]
	},
	// slide8
	{
		uppertextblock:[{
			textdata: data.string.p2text6,
			textclass: "left_top_text",
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "sundar01",
					imgsrc : '',
					imgid : 'sunder02'
				}
			]
		}]
	},
	// slide9
	{
		uppertextblock:[{
			textdata: data.string.p2text7,
			textclass: "left_top_text",
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "sundar01",
					imgsrc : '',
					imgid : 'sunder02'
				}
			]
		}]
	},
	// slide10
	{
		contentblockadditionalclass: 'greenish',
		uppertextblock:[{
			textdata: data.string.p2text8,
			textclass: "center_text",
			datahighlightflag:true,
			datahighlightcustomclass:'brown'
		}]
	},
	// slide11
	{
		contentblockadditionalclass: 'greenish',
		uppertextblock:[{
			textdata: data.string.p2text9,
			textclass: "imgdesc",
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "center_image",
					imgsrc : '',
					imgid : 'girlwriting'
				}
			]
		}]
	},
	// slide12
	{
		contentblockadditionalclass: 'greenish',
		uppertextblock:[{
			textdata: data.string.p2text10,
			textclass: "imgdesc",
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "center_image",
					imgsrc : '',
					imgid : 'reading'
				}
			]
		}]
	},
	// slide13
	{
		contentblockadditionalclass: 'greenish',
		uppertextblock:[{
			textdata: data.string.p2text11,
			textclass: "imgdesc",
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "center_image",
					imgsrc : '',
					imgid : 'watchingnews'
				},
				{
					imgclass : "tv",
					imgsrc : '',
					imgid : 'tv'
				}
			]
		}]
	},
	// slide14
	{
		contentblockadditionalclass: 'greenish',
		uppertextblock:[{
			textdata: data.string.p2text12,
			textclass: "bottom_text",
			datahighlightflag:true,
			datahighlightcustomclass:'brown'
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "thirda",
					imgsrc : '',
					imgid : 'watchingnews'
				},
				{
					imgclass : "thirdb",
					imgsrc : '',
					imgid : 'tv'
				},
				{
					imgclass : "second",
					imgsrc : '',
					imgid : 'reading'
				},
				{
					imgclass : "first",
					imgsrc : '',
					imgid : 'girlwriting'
				}
			]
		}]
	},
	// slide15
	{
		contentblockadditionalclass: 'greenish',
		uppertextblock:[{
			textdata: data.string.p2text13,
			textclass: "bottom_text",
			datahighlightflag:true,
			datahighlightcustomclass:'brown'
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "thirda",
					imgsrc : '',
					imgid : 'watchingnews'
				},
				{
					imgclass : "thirdb",
					imgsrc : '',
					imgid : 'tv'
				},
				{
					imgclass : "second",
					imgsrc : '',
					imgid : 'reading'
				},
				{
					imgclass : "first",
					imgsrc : '',
					imgid : 'girlwriting'
				}
			]
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bookrack", src: imgpath+"book-rack.png", type: createjs.AbstractLoader.IMAGE},
			{id: "computer", src: imgpath+"computer.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cordless", src: imgpath+"cordless.png", type: createjs.AbstractLoader.IMAGE},
			{id: "flowerpot", src: imgpath+"flowerpot.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girlwriting", src: imgpath+"girl-writing.png", type: createjs.AbstractLoader.IMAGE},
			{id: "letter", src: imgpath+"letter.png", type: createjs.AbstractLoader.IMAGE},
			{id: "reading", src: imgpath+"reading.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mobile", src: imgpath+"mobile.png", type: createjs.AbstractLoader.IMAGE},
			{id: "newspaper", src: imgpath+"newspaper.png", type: createjs.AbstractLoader.IMAGE},
			{id: "radio", src: imgpath+"radio.png", type: createjs.AbstractLoader.IMAGE},
			{id: "stand", src: imgpath+"stand.png", type: createjs.AbstractLoader.IMAGE},
			{id: "table", src: imgpath+"table.png", type: createjs.AbstractLoader.IMAGE},
			{id: "telephone", src: imgpath+"telephone.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tv", src: imgpath+"tv.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tvlcd", src: imgpath+"tv_lcd.png", type: createjs.AbstractLoader.IMAGE},
			{id: "watchingnews", src: imgpath+"watching-news.png", type: createjs.AbstractLoader.IMAGE},
			{id: "monkey", src: imgpath+"welldone02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg", src: imgpath+"sunderhouse.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sunder01", src: imgpath+"sunder01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sunder02", src: imgpath+"sunder02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sunder03", src: imgpath+"sunder03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sunder04", src: imgpath+"sunder04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "radiowave", src: imgpath+"radio-wave.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "dotmove", src: imgpath+"dot-move.gif", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"p2_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p2_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p2_s3.ogg"},
			{id: "sound_4", src: soundAsset+"p2_s4.ogg"},
			{id: "sound_5", src: soundAsset+"p2_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p2_s6.ogg"},
			// {id: "sound_7", src: soundAsset+"p2_s7.ogg"},
			{id: "sound_8", src: soundAsset+"p2_s8.ogg"},
			{id: "sound_9", src: soundAsset+"p2_s9.ogg"},
			{id: "sound_10", src: soundAsset+"p2_s10.ogg"},
			{id: "sound_11", src: soundAsset+"p2_s11.ogg"},
			{id: "sound_12", src: soundAsset+"p2_s12.ogg"},
			{id: "sound_13", src: soundAsset+"p2_s13.ogg"},
			{id: "sound_14", src: soundAsset+"p2_s14.ogg"},
			{id: "sound_15", src: soundAsset+"p2_s15.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext) {
			case 0:
						sound_player('sound_1');
						break;
			case 1:
						sound_player('sound_2');
						break;
			case 2:
				sound_player('sound_3');
				$nextBtn.hide(0);
				$('.left_top_text').css({"width":"90%"});
				break;
			case 3:
				sound_player('sound_4');
				break;
			case 4:
			sound_player('sound_5');
					$('.template-dialougebox2-top-white').css({"left": "50%",
																"width": "33%",
																"height": "18%",
																 "top": "13%"});
					break;
			case 5:
			sound_player('sound_6');
				$('.template-dialougebox2-top-white').css({"left": "49%",
																"width": "30%",
																"height": "16%",
																 "top": "16%"});
					break;
			case 6:
						createjs.Sound.stop();
						nav_button_controls(100);
							break;
			case 7:
				sound_player('sound_8');
				break;
			case 8:
			sound_player('sound_9');
				$('.left_top_text').css({"width":"90%"});
				break;
			case 9:
			sound_player('sound_10');
				break;
			case 10:
			sound_player('sound_11');
				break;
			case 11:
			$('.center_image').css({"width":"23%"});
			sound_player('sound_12');
				break;
				case 12:
				sound_player('sound_13');
					break;
			case 13:
			sound_player('sound_14');
				break;
			case 14:
			sound_player('sound_15');
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,delay_ms){
		createjs.Sound.stop();
		timeoutvar = setTimeout(function(){
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(1000);
		});
	},delay_ms);
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentblockadditionalclass: 'purplebg',
		uppertextblock:[{
			textdata: data.lesson.chapter,
			textclass: "chaptertitle",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "img-center",
					imgsrc : '',
					imgid : 'kids'
				},

			]
		}]
	},
	// slide1
	{
		contentblockadditionalclass: 'purplebg',
		uppertextblock:[{
			textdata: data.string.p1text1a,
			textclass: "communicationtext",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "a",
					imgsrc : '',
					imgid : 'newspaper'
				},
				{
					imgclass : "b",
					imgsrc : '',
					imgid : 'mobile'
				},
				{
					imgclass : "c",
					imgsrc : '',
					imgid : 'radio'
				},
				{
					imgclass : "d",
					imgsrc : '',
					imgid : 'letter'
				},
				{
					imgclass : "e",
					imgsrc : '',
					imgid : 'cordless'
				},
				{
					imgclass : "f",
					imgsrc : '',
					imgid : 'telephone'
				},
				{
					imgclass : "g",
					imgsrc : '',
					imgid : 'computer'
				},
				{
					imgclass : "h",
					imgsrc : '',
					imgid : 'tvlcd'
				},
			]
		}]

	},
	// slide2
	{
		contentblockadditionalclass: 'purplebg',
		uppertextblock:[{
			textdata: data.string.p1text5a,
			textclass: "topttext",
		},
		{
			textdata: data.string.p1text1,
			textclass: "text1",
		},
		{
			textdata: data.string.p1text2,
			textclass: "text2",
		},
		{
			textdata: data.string.p1text3,
			textclass: "text3",
		},
		{
			textdata: data.string.p1text4,
			textclass: "text4",
		},
		{
			textdata: data.string.p1text5,
			textclass: "text5",
		},
		{
			textdata: data.string.p1text6,
			textclass: "text6",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "mobile",
					imgsrc : '',
					imgid : 'mobile'
				},
				{
					imgclass : "computer",
					imgsrc : '',
					imgid : 'computer'
				},
				{
					imgclass : "tv",
					imgsrc : '',
					imgid : 'tv'
				},
				{
					imgclass : "radio",
					imgsrc : '',
					imgid : 'radio'
				},
				{
					imgclass : "letter",
					imgsrc : '',
					imgid : 'letter'
				},
				{
					imgclass : "newspaper",
					imgsrc : '',
					imgid : 'newspaper'
				}
			]
		}]
	},
	// slide3
	{
		contentblockadditionalclass: 'purplebg',
		uppertextblock:[{
			textdata: data.string.p1text7,
			textclass: "lefttext",
			datahighlightflag: true,
			datahighlightcustomclass: 'yellow'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "mobile",
					imgsrc : '',
					imgid : 'mobile'
				},
				{
					imgclass : "computer",
					imgsrc : '',
					imgid : 'computer'
				},
				{
					imgclass : "tv",
					imgsrc : '',
					imgid : 'tv'
				},
				{
					imgclass : "radio",
					imgsrc : '',
					imgid : 'radio'
				},
				{
					imgclass : "letter",
					imgsrc : '',
					imgid : 'letter'
				},
				{
					imgclass : "newspaper",
					imgsrc : '',
					imgid : 'newspaper'
				}
			]
		}],
		},
		//slide4
		{
		contentblockadditionalclass: 'purplebg',
		uppertextblock:[{
			textdata: data.string.p1text8a,
			textclass: "topttext",
		},
		{
			textdata: data.string.p1text1,
			textclass: "text1",
		},
		{
			textdata: data.string.p1text2,
			textclass: "text2",
		},
		{
			textdata: data.string.p1text3,
			textclass: "text3",
		},
		{
			textdata: data.string.p1text4,
			textclass: "text4",
		},
		{
			textdata: data.string.p1text5,
			textclass: "text5",
		},
		{
			textdata: data.string.p1text6,
			textclass: "text6",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "mobile",
					imgsrc : '',
					imgid : 'mobile'
				},
				{
					imgclass : "computer",
					imgsrc : '',
					imgid : 'computer'
				},
				{
					imgclass : "tv",
					imgsrc : '',
					imgid : 'tv'
				},
				{
					imgclass : "radio",
					imgsrc : '',
					imgid : 'radio'
				},
				{
					imgclass : "letter",
					imgsrc : '',
					imgid : 'letter'
				},
				{
					imgclass : "newspaper",
					imgsrc : '',
					imgid : 'newspaper'
				}
			]
		}]

	},
	//slide 5
	{
		contentblockadditionalclass: 'purplebg',
		uppertextblock:[{
			textdata: data.string.p1text8,
			textclass: "centertext",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "monkey",
					imgsrc : '',
					imgid : 'monkey'
				}
			]
		}],
		},
	//slide 6
	{
		contentblockadditionalclass: 'purplebg',
		uppertextblock:[{
			textdata: data.string.p1text10,
			textclass: "desc",
		},
		{
			textdata: data.string.p1text11,
			textclass: "des1",
		},
		{
			textdata: data.string.p1text12,
			textclass: "des2",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "arrow1",
					imgsrc : '',
					imgid : 'arrow'
				},
				{
					imgclass : "arrow2",
					imgsrc : '',
					imgid : 'arrow'
				}
			]
		}],
		},
		//slide 7
		{
		contentblockadditionalclass: 'purplebg',
		uppertextblock:[{
			textdata: data.string.p1text13,
			textclass: "centertext",
		}]
		},


];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bookrack", src: imgpath+"book-rack.png", type: createjs.AbstractLoader.IMAGE},
			{id: "computer", src: imgpath+"computer.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cordless", src: imgpath+"cordless.png", type: createjs.AbstractLoader.IMAGE},
			{id: "flowerpot", src: imgpath+"flowerpot.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girlwriting", src: imgpath+"girl-writing.png", type: createjs.AbstractLoader.IMAGE},
			{id: "letter", src: imgpath+"letter.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mobile", src: imgpath+"mobile.png", type: createjs.AbstractLoader.IMAGE},
			{id: "newspaper", src: imgpath+"newspaper.png", type: createjs.AbstractLoader.IMAGE},
			{id: "radio", src: imgpath+"radio.png", type: createjs.AbstractLoader.IMAGE},
			{id: "stand", src: imgpath+"stand.png", type: createjs.AbstractLoader.IMAGE},
			{id: "table", src: imgpath+"table.png", type: createjs.AbstractLoader.IMAGE},
			{id: "telephone", src: imgpath+"telephone.png", type: createjs.AbstractLoader.IMAGE},
			{id: "kids", src: imgpath+"01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tv", src: imgpath+"tv.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tvlcd", src: imgpath+"tv_lcd.png", type: createjs.AbstractLoader.IMAGE},
			{id: "watchingnews", src: imgpath+"watching-news.png", type: createjs.AbstractLoader.IMAGE},
			{id: "monkey", src: imgpath+"welldone02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src: imgpath+"arrow.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"p1_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p1_s1.ogg"},
			{id: "sound_3", src: soundAsset+"p1_s2.ogg"},
			{id: "sound_10", src: soundAsset+"p1_s4.ogg"},
			{id: "sound_11", src: soundAsset+"p1_s5.ogg"},
			{id: "sound_12", src: soundAsset+"p1_s6.ogg"},
			{id: "sound_13", src: soundAsset+"p1_s7.ogg"},
			{id: "sound_14", src: soundAsset+"p1_s3.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext) {
			case 0:
				$prevBtn.hide(0);
				sound_player('sound_1');
				break;
			case 1:
				sound_player('sound_2',2000);
				break;
			case 2:
			if($lang=='en'){
				sound_player('sound_3');
				$('.newspaper, .letter, .radio, .tv, .computer, .mobile,.text1, .text2, .text3, .text4, .text5, .text6 ').hide(0);
				setTimeout(function() {
					$('.mobile,.text1').fadeIn(500);
				},4700);
				setTimeout(function() {
					$('.computer,.text2').fadeIn(500);
				},6000);
				setTimeout(function() {
					$('.tv,.text3').fadeIn(500);
				},7300);
				setTimeout(function() {
					$('.radio,.text4').fadeIn(500);
				},8900);
				setTimeout(function() {
					$('.letter,.text5').fadeIn(500);
				},11200);
				setTimeout(function() {
					$('.newspaper,.text6').fadeIn(500);
				},12600);
			}
			if($lang=='np'){
				sound_player('sound_3');
				$('.newspaper, .letter, .radio, .tv, .computer, .mobile,.text1, .text2, .text3, .text4, .text5, .text6 ').hide(0);
				setTimeout(function() {
					$('.mobile,.text1').fadeIn(500);
				},7500);
				setTimeout(function() {
					$('.computer,.text2').fadeIn(500);
				},9000);
				setTimeout(function() {
					$('.tv,.text3').fadeIn(500);
				},11500);
				setTimeout(function() {
					$('.radio,.text4').fadeIn(500);
				},13000);
				setTimeout(function() {
					$('.letter,.text5').fadeIn(500);
				},14500);
				setTimeout(function() {
					$('.newspaper,.text6').fadeIn(500);
				},16000);
			}

				break;
			case 3:
				sound_player('sound_14');
				$('.mobile,.radio').css({"left":"3%"});
				$('.computer,.letter').css({"left":"21%"});
				$('.tv,.newspaper').css({"left":"39%"});
				$prevBtn.show(0);
				break;
			case 4:
				$nextBtn.hide(0);
				sound_player('sound_10');
				countNext=countNext+1;
				var c=0;
				$('.mobile,.radio,.computer,.letter,.tv,.newspaper').css({"cursor":"pointer","z-index":"999"});
				$('.mobile,.radio,.computer,.letter,.tv,.newspaper').click(
					function(){
						c++;
						if(c==1){
							countNext=countNext-1;
						}
						$(this).css({"border":".2em solid white","border-radius":"1em"});
					}
				);
				break;
			case 5:
			sound_player('sound_11');
			break;
			case 6:
			sound_player('sound_12');
			$('.des1,.des2').hide(0);
				setTimeout(function() {
					$('.des1').fadeIn(500);
				},5000);
				setTimeout(function() {
					$('.des2').fadeIn(500);
				},7000);

			break;
			case 7:
			sound_player('sound_13');
				$(".centertext").css({"top": "43%"});
				break;
			default:
				$prevBtn.show(0);
				sound_player('sound_2');
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,delay_ms){
		timeoutvar = setTimeout(function(){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(1000);
		});
	},delay_ms);
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

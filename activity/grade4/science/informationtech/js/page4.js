var imgpath = $ref + "/images/diy/";
var soundAsset = $ref+"/sounds/"+$lang+"/";


var content=[
	// slide0
	{
		contentblockadditionalclass:'bg',
		uppertextblock:[{
			textdata: data.string.diytext,
			textclass: "diytitle",
		}]
	},
	// slide1
	{
		contentblockadditionalclass:'bg',
		uppertextblock:[{
			textdata: data.string.p4text1,
			textclass: "instruction",
		}]
	},

	// slide2
	{
		contentblockadditionalclass:'bg',
		uppertextblock:[{
			textdata: data.string.p4text2,
			textclass: "instuction_top",
		},{
			textdata: data.string.p4text4,
			textclass: "left",
		},{
			textdata: data.string.p4text5,
			textclass: "right",
		},{
			textdata: data.string.p4text5a,
			textclass: "about",
		},{
			textclass: "onewayblock",
		},{
			textclass: "twowayblock",
		},],
		defineclass :'class1',
		draggableimagesource:imgpath + 'cell_phone.png',
		draggableblock:[{}]
	},
	// slide3
	{
		contentblockadditionalclass:'bg',
		uppertextblock:[{
			textdata: data.string.p4text2,
			textclass: "instuction_top",
		},{
			textdata: data.string.p4text4,
			textclass: "left",
		},{
			textdata: data.string.p4text5,
			textclass: "right",
		},{
			textdata: data.string.p4text5b,
			textclass: "about",
		},{
			textclass: "onewayblock",
		},{
			textclass: "twowayblock",
		},],
		defineclass :'class2',
		draggableimagesource:imgpath + 'radio_fm.png',
		draggableblock:[{}]
	},
	// slide3
	{
		contentblockadditionalclass:'bg',
		uppertextblock:[{
			textdata: data.string.p4text2,
			textclass: "instuction_top",
		},{
			textdata: data.string.p4text4,
			textclass: "left",
		},{
			textdata: data.string.p4text5,
			textclass: "right",
		},{
			textdata: data.string.p4text6,
			textclass: "about",
		},{
			textclass: "onewayblock",
		},{
			textclass: "twowayblock",
		},],
		defineclass :'class2',
		draggableimagesource:imgpath + 'reading-newspaper.png',
		draggableblock:[{}]
	},
	// slide4
	{
		contentblockadditionalclass:'bg',
		uppertextblock:[{
			textdata: data.string.p4text2,
			textclass: "instuction_top",
		},{
			textdata: data.string.p4text4,
			textclass: "left",
		},{
			textdata: data.string.p4text5,
			textclass: "right",
		},{
			textdata: data.string.p4text7,
			textclass: "about",
		},{
			textclass: "onewayblock",
		},{
			textclass: "twowayblock",
		},],
		defineclass :'class1',
		draggableimagesource:imgpath + 'fax_machine.png',
		draggableblock:[{}]
	},
	// slide5
	{
		contentblockadditionalclass:'bg',
		uppertextblock:[{
			textdata: data.string.p4text2,
			textclass: "instuction_top",
		},{
			textdata: data.string.p4text4,
			textclass: "left",
		},{
			textdata: data.string.p4text5,
			textclass: "right",
		},{
			textdata: data.string.p4text8,
			textclass: "about",
		},{
			textclass: "onewayblock",
		},{
			textclass: "twowayblock",
		},],
		defineclass :'class2',
		draggableimagesource:imgpath + 'letter.png',
		draggableblock:[{}]
	},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			{id: "niti", src: imgpath+"niti01.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// sounds
			{id: "sound_1", src: soundAsset+"p4_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p4_s1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_option_image(content, countNext);
		put_speechbox_image(content, countNext);

		$('.draggableblock').draggable({
                containment : ".generalTemplateblock",
                revert : true,
                cursor : "move",
                zIndex: 100000,
            });
		$(".twowayblock").droppable({
                accept: ".class1,.class2",
								hoverClass : "hovered",
                drop: function (event, ui){
                    $this = $(this);
                    dropfunc1(event, ui, $this);
                }
            });
		$(".onewayblock").droppable({
                accept: ".class1,.class2",
								hoverClass : "hovered",
                drop: function (event, ui){
                    $this = $(this);
                    dropfunc2(event, ui, $this);
                }
            });
		function dropfunc1(event, ui, $droppedOn){
								if(ui.draggable.hasClass('class1')){
													ui.draggable.draggable('option', 'revert', false);
														play_correct_incorrect_sound(1);
						                ui.draggable.draggable('disable');
						                $('.class1').css({"left": "75%",
														"top": "71%",
													"z-index": "99",
												"transform": "translate(-50%,-50%)",
											"height": "27%",
										"width": "auto !important"});

						                $('.class2').css({"left": "30%",
														"top": "71%",
													"transform": "translate(-50%,-50%)",
												"z-index": "99",
											"height": "27%",
										"width": "auto !important"});

								 		nav_button_controls(500);
								}
								else{
									play_correct_incorrect_sound(0);
								}
            }

						function dropfunc2(event, ui, $droppedOn){
							if(ui.draggable.hasClass('class2')){

								ui.draggable.draggable('option', 'revert', false);
								play_correct_incorrect_sound(1);
														ui.draggable.draggable('disable');
														$('.class1').css({"left": "75%",
														"top": "71%",
														"z-index": "99",
														"transform": "translate(-50%,-50%)",
														"height": "27%",
														"width": "auto !important"});

														$('.class2').css({"left": "30%",
														"top": "71%",
														"transform": "translate(-50%,-50%)",
														"z-index": "99",
														"height": "27%",
														"width": "auto !important"});

										nav_button_controls(500);
									}
									else{
										play_correct_incorrect_sound(0);
										}
								}
		 switch(countNext){
		 		case 0:
				sound_player('sound_1');
				break;
		 		case 1:
				sound_player('sound_2');
		 		break;

		 }

	}
	//change_src-> put 1 for girl speak first else 2 for boy-speak first, 3 for boy-3
	function conversation(class1, sound_data1, class2, sound_data2, change_src){
		$(class1).fadeIn(500, function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_data1);
			current_sound.play();
			current_sound.on("complete", function(){
				if(change_src==1){
					$('.boy').attr('src', preload.getResult('boy-1').src);
					$('.girl').attr('src', preload.getResult('girl-1').src);
				} else if(change_src==2){
					$('.boy').attr('src', preload.getResult('boy-2').src);
					$('.girl').attr('src', preload.getResult('girl-2').src);
				} else if(change_src==3){
					$('.boy').attr('src', preload.getResult('boy-3').src);
					$('.girl').attr('src', preload.getResult('girl-1').src);
				}
				$(class2).fadeIn(500, function(){
					current_sound = createjs.Sound.play(sound_data2);
					current_sound.play();
					current_sound.on("complete", function(){
						nav_button_controls(0);
						$(class1).click(function(){
							sound_player(sound_data1);
						});
						$(class2).click(function(){
							sound_player(sound_data2);
						});
					});
				});
			});
		});
	}
	function sound_player(sound_id,delay_ms){
		createjs.Sound.stop();
		timeoutvar = setTimeout(function(){
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(1000);
		});
	},delay_ms);
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/

	 /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		CODE MODIFIED FOR THREE DIFFERENT HIGHTLIGHTS
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
		function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightstarttag2;
        var texthighlightstarttag3;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
              $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

              $(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
              (stylerulename2 = "parsedstring2") ;

              $(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
              (stylerulename3 = "parsedstring3") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
            texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
		/*=====  End of data highlight function  ======*/
	function sound_play_click(sound_id, click_class){
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_option_image(content, count){
		if(content[count].hasOwnProperty('option')){
			var imageClass = content[count].option;
			console.log(imageClass);
			for(var i=0; i<imageClass.length; i++){
				var image_src = preload.getResult(imageClass[i].imgid).src;
				//get list of classes
				var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				// console.log(selector);
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
			/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

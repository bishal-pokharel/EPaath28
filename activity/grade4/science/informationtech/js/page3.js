var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var imgpath = $ref+"/images/";

var content=[
// slide0
	{
		contentblockadditionalclass: 'bluebg',
		uppertextblock:[{
			textdata: data.string.p3text1,
			textclass: "communicationtext",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "a",
					imgsrc : '',
					imgid : 'newspaper'
				},
				{
					imgclass : "b",
					imgsrc : '',
					imgid : 'mobile'
				},
				{
					imgclass : "c",
					imgsrc : '',
					imgid : 'radio'
				},
				{
					imgclass : "d",
					imgsrc : '',
					imgid : 'letter'
				},
				{
					imgclass : "e",
					imgsrc : '',
					imgid : 'cordless'
				},
				{
					imgclass : "f",
					imgsrc : '',
					imgid : 'telephone'
				},
				{
					imgclass : "g",
					imgsrc : '',
					imgid : 'computer'
				},
				{
					imgclass : "h",
					imgsrc : '',
					imgid : 'tvlcd'
				},
			]
		}]
	},
	// slide1
	{
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "sundar01",
					imgsrc : '',
					imgid : 'sunder04'
				},
				{
					imgclass : "phone",
					imgsrc : '',
					imgid : 'phone'
				}
			]
		}]
	},
	// slide2
	{
		uppertextblock:[{
			textdata: data.string.p3text2,
			textclass: "template-dialougebox2-top-flipped-white",
		},
		{
			textdata: data.string.p3text3,
			textclass: "template-dialougebox2-top-white",
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg1'
				},
				{
					imgclass : "sunder",
					imgsrc : '',
					imgid : 'sundartalking1'
				},
				{
					imgclass : "ramesh",
					imgsrc : '',
					imgid : 'rameshtalking1'
				}
			]
		}]
	},
	// slide3
	{
		uppertextblock:[{
			textdata: data.string.p3text4,
			textclass: "template-dialougebox2-top-flipped-white",
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg1'
				},
				{
					imgclass : "sunder",
					imgsrc : '',
					imgid : 'sundartalking1'
				},
				{
					imgclass : "ramesh",
					imgsrc : '',
					imgid : 'rameshtalking2'
				}
			]
		}]
	},
	// slide4
	{
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg1'
				},
				{
					imgclass : "sunder",
					imgsrc : '',
					imgid : 'sundartalking2'
				},
				{
					imgclass : "ramesh",
					imgsrc : '',
					imgid : 'rameshtalking2'
				},
				{
					imgclass : "dots",
					imgsrc : '',
					imgid : 'dotmove'
				}
			]
		}]
	},
	// slide5
	{
		uppertextblock:[
		{
			textdata: data.string.p3text5,
			textclass: "template-dialougebox2-top-white",
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg1'
				},
				{
					imgclass : "sunder",
					imgsrc : '',
					imgid : 'sundartalking2'
				},
				{
					imgclass : "ramesh",
					imgsrc : '',
					imgid : 'rameshtalking1'
				}
			]
		}]
	},
	// slide6
	{
		uppertextblock:[
		{
			textdata: data.string.p3text6,
			textclass: "template-dialougebox2-top-flipped-white",
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg1'
				},
				{
					imgclass : "sunder",
					imgsrc : '',
					imgid : 'sundartalking3'
				},
				{
					imgclass : "ramesh",
					imgsrc : '',
					imgid : 'rameshtalking2'
				}
			]
		}]
	},
	// slide7
	{
		uppertextblock:[
		{
			textdata: data.string.p3text7,
			textclass: "left_top_text",
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg1'
				},
				{
					imgclass : "sunder",
					imgsrc : '',
					imgid : 'sundartalking2'
				},
				{
					imgclass : "ramesh",
					imgsrc : '',
					imgid : 'rameshtalking2'
				}
			]
		}]
	},
	// slide7
	{
		contentblockadditionalclass: 'darkblue',
		uppertextblock:[
		{
			textdata: data.string.p3text8,
			textclass: "template-dialougebox2-top-flipped-white left_dial",
		},
		{
			textdata: data.string.p3text8a,
			textclass: "template-dialougebox2-top-white right_dial",
		},
		{
			textdata: data.string.p3text9,
			textclass: "bottom_text",
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "boy",
					imgsrc : '',
					imgid : 'suraj'
				},
				{
					imgclass : "girl",
					imgsrc : '',
					imgid : 'asha'
				}
			]
		}]
	},
	// slide8
	{
		contentblockadditionalclass: 'bluebg',
		uppertextblock:[
		{
			textdata: data.string.p3text10,
			textclass: "bottom_text",
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "computer1",
					imgsrc : '',
					imgid : 'computer1'
				},
				{
					imgclass : "hand",
					imgsrc : '',
					imgid : 'hand'
				},
				{
					imgclass : "messagebox01",
					imgsrc : '',
					imgid : 'messagebox01'
				},
				{
					imgclass : "messagebox02",
					imgsrc : '',
					imgid : 'messagebox02'
				},
				{
					imgclass : "messagebox03",
					imgsrc : '',
					imgid : 'messagebox03'
				},
				{
					imgclass : "messagebox04",
					imgsrc : '',
					imgid : 'messagebox04'
				},
				{
					imgclass : "messagebox05",
					imgsrc : '',
					imgid : 'messagebox05'
				},
				{
					imgclass : "messagebox06",
					imgsrc : '',
					imgid : 'messagebox06'
				}
			]
		}]
	},
	// slide9
	{
		contentblockadditionalclass: 'darkblue',
		uppertextblock:[
		{
			textdata: data.string.p3text11,
			textclass: "bottom_text",
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "f1back",
					imgsrc : '',
					imgid : 'fax01_back'
				},
				{
					imgclass : "f1front",
					imgsrc : '',
					imgid : 'fax01_front'
				},
				{
					imgclass : "f2back",
					imgsrc : '',
					imgid : 'fax02_back'
				},
				{
					imgclass : "f2front",
					imgsrc : '',
					imgid : 'fax02_front'
				},
				{
					imgclass : "paper01",
					imgsrc : '',
					imgid : 'paper01'
				},
				{
					imgclass : "paper02",
					imgsrc : '',
					imgid : 'paper01'
				}
			]
		}]
	},
	// slide10
	{
		contentblockadditionalclass: 'darkblue',
		uppertextblock:[
		{
			textdata: data.string.p3text12,
			textclass: "bottom_text1",
			datahighlightflag:true,
			datahighlightcustomclass:'brown'
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "f1back",
					imgsrc : '',
					imgid : 'fax01_back'
				},
				{
					imgclass : "f1front",
					imgsrc : '',
					imgid : 'fax01_front'
				},
				{
					imgclass : "f2back",
					imgsrc : '',
					imgid : 'fax02_back'
				},
				{
					imgclass : "f2front",
					imgsrc : '',
					imgid : 'fax02_front'
				},
				{
					imgclass : "a1",
					imgsrc : '',
					imgid : 'asha'
				},
				{
					imgclass : "s1",
					imgsrc : '',
					imgid : 'suraj'
				},
				{
					imgclass : "c1",
					imgsrc : '',
					imgid : 'computer1'
				}
			]
		}]
	},


];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			{id: "data-xml", src: "data.xml", type: createjs.AbstractLoader.XML},
			//images
			{id: "newspaper", src: imgpath+"newspaper.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mobile", src: imgpath+"mobile.png", type: createjs.AbstractLoader.IMAGE},
			{id: "radio", src: imgpath+"radio.png", type: createjs.AbstractLoader.IMAGE},
			{id: "letter", src: imgpath+"letter.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cordless", src: imgpath+"cordless.png", type: createjs.AbstractLoader.IMAGE},
			{id: "computer", src: imgpath+"computer.png", type: createjs.AbstractLoader.IMAGE},
			{id: "computer1", src: imgpath+"computer1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "telephone", src: imgpath+"telephone.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tvlcd", src: imgpath+"tv_lcd.png", type: createjs.AbstractLoader.IMAGE},
			{id: "phone", src: imgpath+"sunder_talking05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sunder04", src: imgpath+"sunder04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg", src: imgpath+"bg_talking_on_phone01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg1", src: imgpath+"bg_talking_on_phone.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sundartalking1", src: imgpath+"sunder_talking02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sundartalking2", src: imgpath+"sunder_talking03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sundartalking3", src: imgpath+"sunder_talking04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rameshtalking1", src: imgpath+"ramesh_talking01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rameshtalking2", src: imgpath+"ramesh_talking02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha", src: imgpath+"asha.png", type: createjs.AbstractLoader.IMAGE},
			{id: "suraj", src: imgpath+"suraj.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dotmove", src: imgpath+"dot-move.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "messagebox01", src: imgpath+"messagebox01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "messagebox02", src: imgpath+"messagebox02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "messagebox03", src: imgpath+"messagebox03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "messagebox04", src: imgpath+"messagebox04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "messagebox05", src: imgpath+"messagebox05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "messagebox06", src: imgpath+"messagebox06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hand", src: imgpath+"hand.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fax01_back", src: imgpath+"fax01_back.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fax01_front", src: imgpath+"fax01_front.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fax02_back", src: imgpath+"fax02_back.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fax02_front", src: imgpath+"fax02_front.png", type: createjs.AbstractLoader.IMAGE},
			{id: "paper01", src: imgpath+"paper01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "paper02", src: imgpath+"paper02.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_1", src: soundAsset+"p3_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p3_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p3_s3.ogg"},
			// {id: "sound_4", src: soundAsset+"p3_s4.ogg"},
			{id: "sound_5", src: soundAsset+"p3_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p3_s6.ogg"},
			{id: "sound_7", src: soundAsset+"p3_s7.ogg"},
			{id: "sound_8", src: soundAsset+"p3_s8.ogg"},
			{id: "sound_9", src: soundAsset+"p3_s9.ogg"},
			{id: "sound_10", src: soundAsset+"p3_s10.ogg"},
			{id: "sound_11", src: soundAsset+"p3_s11.ogg"},
			{id: "ring", src: soundAsset+"ring.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	  	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */
     function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightstarttag2;
        var texthighlightstarttag3;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
              $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

              $(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
              (stylerulename2 = "parsedstring2") ;

              $(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
              (stylerulename3 = "parsedstring3") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
            texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext) {
			case 0:
						sound_player('sound_1');
						break;
			case 1:
						sound_player('ring');
						break;
			case 2:
						sound_player('sound_2');
						break;

			case 3:
					sound_player('sound_3');
				$('.template-dialougebox2-top-flipped-white').css({"left": "9%",
																"width": "41%",
																 "top": "3%"});
					break;
			case 4:
			nav_button_controls(500);
					break;

			case 5:
				sound_player('sound_5');
				$('.template-dialougebox2-top-white').css({"left": "55%",
																"width": "41%",
																 "top": "7%",
																 "height":"20%"});
																 $( '.template-dialougebox2-top-white' ).each(function () {
												     			this.style.setProperty( 'padding-top', '0%', 'important' );
												 				});
					break;
			case 6:
				sound_player('sound_6');
				$('.template-dialougebox2-top-flipped-white').css({"left": "19%",
																"width": "30%",
																 "top": "3%"});
					break;
			case 7:
				sound_player('sound_7');
				break;
			case 8:
				sound_player('sound_8');
				break;
			case 9:
				sound_player('sound_9');
				break;
			case 10:
				sound_player('sound_10');
				break;
			case 11:
				sound_player('sound_11');
				$('.f1front, .f1back').css({"left": "60%",
											"width": "15%",
											 "top": "44%"});
				$('.f2front, .f2back').css({"left": "77%",
											"width": "15%",
											 "top": "44%"});
					break;
			default:
				$prevBtn.show(0);
				nav_button_controls(500);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,delay_ms){
		createjs.Sound.stop();
		timeoutvar = setTimeout(function(){
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(1000);
		});
	},delay_ms);
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				// console.log(selector);
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentblockadditionalclass: 'default-bg',
		exerciseblock: [
			{					
				ques_img: imgpath+'im-1.png',
				instructiondata: data.string.exins,
				option: [{
					option_class: "class1",
					optiondata: data.string.exopt1a,
				},
				{
						option_class: "class2",
						optiondata: data.string.exopt1b,
				}],
			} 
		] 
	},
	// slide1
	{
		contentblockadditionalclass: 'default-bg',
		exerciseblock: [
			{					
				ques_img: imgpath+'im-2.png',
				instructiondata: data.string.exins,
				option: [{
					option_class: "class2",
					optiondata: data.string.exopt1a,
				},
				{
						option_class: "class1",
						optiondata: data.string.exopt1b,
				}],
			} 
		] 
	},
	// slide2
	{
		contentblockadditionalclass: 'default-bg',
		exerciseblock: [
			{					
				ques_img: imgpath+'im-3.png',
				instructiondata: data.string.exins,
				option: [{
					option_class: "class1",
					optiondata: data.string.exopt1a,
				},
				{
						option_class: "class2",
						optiondata: data.string.exopt1b,
				}],
			} 
		] 
	},
	
	// slide3
	{
		contentblockadditionalclass: 'default-bg',
		exerciseblock: [
			{					
				ques_img: imgpath+'im-4.png',
				instructiondata: data.string.exins,
				option: [{
					option_class: "class2",
					optiondata: data.string.exopt1a,
				},
				{
						option_class: "class1",
						optiondata: data.string.exopt1b,
				}],
			} 
		] 
	},
	// slide4
	{
		contentblockadditionalclass: 'default-bg',
		exerciseblock: [
			{					
				ques_img: imgpath+'im-5.png',
				instructiondata: data.string.exins,
				option: [{
					option_class: "class1",
					optiondata: data.string.exopt1a,
				},
				{
						option_class: "class2",
						optiondata: data.string.exopt1b,
				}],
			} 
		] 
	},
	// slide5
	{
		contentblockadditionalclass: 'default-bg',
		exerciseblock: [
			{					
				ques_img: imgpath+'im-6.png',
				instructiondata: data.string.exins,
				option: [{
					option_class: "class2",
					optiondata: data.string.exopt1a,
				},
				{
						option_class: "class1",
						optiondata: data.string.exopt1b,
				}],
			} 
		] 
	},

	// slide6
	{
		contentblockadditionalclass: 'default-bg',
		exerciseblock: [
			{					
				ques_img: imgpath+'im-7.png',
				instructiondata: data.string.exins,
				option: [{
					option_class: "class1",
					optiondata: data.string.exopt1a,
				},
				{
						option_class: "class2",
						optiondata: data.string.exopt1b,
				}],
			} 
		] 
	},
	// slide7
	{
		contentblockadditionalclass: 'default-bg',
		exerciseblock: [
			{					
				ques_img: imgpath+'im-8.png',
				instructiondata: data.string.exins,
				option: [{
					option_class: "class2",
					optiondata: data.string.exopt1a,
				},
				{
						option_class: "class1",
						optiondata: data.string.exopt1b,
				}],
			} 
		] 
	},
	// slide8
	{
		contentblockadditionalclass: 'default-bg',
		exerciseblock: [
			{					
				ques_img: imgpath+'im-9.png',
				instructiondata: data.string.exins,
				option: [{
					option_class: "class1",
					optiondata: data.string.exopt1a,
				},
				{
						option_class: "class2",
						optiondata: data.string.exopt1b,
				}],
			} 
		] 
	},
	// slide9
	{
		contentblockadditionalclass: 'default-bg',
		exerciseblock: [
			{					
				ques_img: imgpath+'im-10.png',
				instructiondata: data.string.exins,
				option: [{
					option_class: "class2",
					optiondata: data.string.exopt1a,
				},
				{
						option_class: "class1",
						optiondata: data.string.exopt1b,
				}],
			} 
		] 
	},
];
content.shufflearray();

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
    var current_sound;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
    var scoring = new LampTemplate();
    scoring.init($total_page);
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            //    sounds
            {id: "sound_1", src: soundAsset + "ex.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }



	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templatecaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templatecaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
	/**
	 How to:
	 - Just call instructionblockcontroller() from the template
	 */


	/**
	 What it does:
	 - It inserts and handles closing and opening of instruction block
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	var wrongclick = false;
	var ques_count = ['१) ', '२) ', '३) ', '४) ', '५) ', '६) ', '७) ', '८) ', '९) ', '१०) '];
	
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		
		$board.html(html);
		countNext==0?sound_player("sound_1"):"";
		var option_position = [1,2];
		// option_position.shufflearray();
		for(var op=0; op<2; op++){
			$('.main-container').eq(op).addClass('option-pos-'+option_position[op]);
		}

		
		var wrong_clicked = 0;
		$(".option-container").click(function(){
			if($(this).hasClass("class1")){
				if(wrong_clicked<1){
					scoring.update(true);
				}
				$(".option-container").css('pointer-events','none');
				current_sound.stop();
	 			play_correct_incorrect_sound(1);
				$(this).addClass('correct-ans');
				$(this).parent().children('.correct-icon').show(0);
				wrong_clicked = 0;
				if(countNext != $total_page)
					$nextBtn.show(0);
			}
			else{
                current_sound.stop();
				scoring.update(false);
	 			play_correct_incorrect_sound(0);
				$(this).addClass('incorrect-ans');
				$(this).parent().children('.incorrect-icon').show(0);
				wrong_clicked++;
			}
		}); 
		
		$prevBtn.hide(0);
	}
	
	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		generalTemplate();
	}

	$nextBtn.on('click', function() {
		countNext++;
		scoring.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
});

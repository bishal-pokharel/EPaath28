var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";


var content=[
	// slide0
{
	contentblockadditionalclass:'blue',
	uppertextblock : [{
			textdata : data.string.p2title,
			textclass : 'lesson-title diyfont'
			}],
    imageblock: [
        {
            imagetoshow: [
                {
                    imgclass: "img1",
                    imgsrc : imgpath + "a_10.png"
                }

            ]
        }]
},

// slide1

{
	contentblockadditionalclass:'skyblue',
	uppertextblock : [
			{
				textdata : data.string.p2text1,
				textclass : 'question'
			},
			{
				textdata : data.string.p2text2,
				textclass : 'wrongclick'
			},
			{
				textdata : data.string.p2text3,
				textclass : 'rightclick'
			}],

	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "centerimage",
			imgsrc : imgpath + "human.png"
		}

		]
	}]
},
// slide2

{
	contentblockadditionalclass:'skyblue',
	uppertextblock : [
			{
				textdata : data.string.p2text4,
				textclass : 'question'
			}],

	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "cowcycle1",
			imgsrc : imgpath + "woman01.png"
		},
		{
			imgclass: "arrow1",
			imgsrc : imgpath + "arrow01.png"
		},
		{
			imgclass: "cowcycle2",
			imgsrc : imgpath + "woman02.png"
		},
		{
			imgclass: "arrow2",
			imgsrc : imgpath + "arrow02.png"
		},
		{
			imgclass: "cowcycle3",
			imgsrc : imgpath + "woman03.png"
		},
		{
			imgclass: "arrow3",
			imgsrc : imgpath + "arrow03.png"
		},
		{
			imgclass: "cowcycle4",
			imgsrc : imgpath + "woman04.png"
		},
		{
			imgclass: "arrow4",
			imgsrc : imgpath + "arrow04.png"
		},

		]
	}]
},
// slide3

{
	contentblockadditionalclass:'skyblue',
	uppertextblock : [
			{
				textdata : data.string.p2text5,
				textclass : 'question'
			},
			{
				textdata : data.string.p2text2,
				textclass : 'rightclick'
			},
			{
				textdata : data.string.p2text3,
				textclass : 'wrongclick'
			}],

	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "centerimage",
			imgsrc : imgpath + "gohi.png"
		}

		]
	}]
},
// slide4

{
	contentblockadditionalclass:'skyblue',
	uppertextblock : [
			{
				textdata : data.string.p2text6,
				textclass : 'question'
			}],

	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "gohicycle1",
			imgsrc : imgpath + "gohi01.png"
		},
		{
			imgclass: "a1",
			imgsrc : imgpath + "arrow01.png"
		},
		{
			imgclass: "gohicycle2",
			imgsrc : imgpath + "gohi02.png"
		},
		{
			imgclass: "a2",
			imgsrc : imgpath + "arrow02.png"
		},
		{
			imgclass: "gohicycle3",
			imgsrc : imgpath + "gohi03.png"
		},
		{
			imgclass: "a3",
			imgsrc : imgpath + "arrow03.png"
		},
		{
			imgclass: "gohicycle4",
			imgsrc : imgpath + "gohi04.png"
		},
		{
			imgclass: "a4",
			imgsrc : imgpath + "arrow04.png"
		},
		{
			imgclass: "gohicycle5",
			imgsrc : imgpath + "gohi05.png"
		},
		{
			imgclass: "a5",
			imgsrc : imgpath + "arrow02.png"
		},

		]
	}]
},
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;
	var clicks = 0;
    var current_sound;
	var  inputname = '';


	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            //    sounds
            {id: "sound_2", src: soundAsset + "S2_P2.ogg"},
            {id: "sound_3", src: soundAsset + "S2_P3.ogg"},
            {id: "sound_4", src: soundAsset + "S2_P4.ogg"},
            {id: "sound_5", src: soundAsset + "S2_P5.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();
    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }

    function sound_player_seq(sound1,sound2){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound1);
        current_sound.play();
        current_sound.on('complete', function () {
            current_sound = createjs.Sound.play(sound2);
            current_sound.play();
            current_sound.on('complete', function () {
                navigationcontroller(countNext, $total_page);
            });
        });
    }

		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


      function navigationcontroller(islastpageflag){
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
                ole.footerNotificationHandler.pageEndSetNotification();

        }
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		vocabcontroller.findwords(countNext);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		switch(countNext){
			case 0:
				play_diy_audio();
				navigationcontroller();
				break;
						case 1:
							sound_player("sound_"+(countNext+1),false);
						$nextBtn.hide(0);
						$('.rightclick').click(function(){
							$('.rightclick').css({"background":"green"});
							$('.wrongclick').css({"pointer-events":"none"});
							current_sound.stop();
							play_correct_incorrect_sound(1);
							$nextBtn.show(0);
						})
						$('.wrongclick').click(function(){
                            current_sound.stop();
							$('.wrongclick').css({"background":"#d44b4b none repeat scroll 0% 0%"});
							play_correct_incorrect_sound(0);
						});
						break;

						case 2:
                            sound_player("sound_"+(countNext+1),false);
                            $nextBtn.hide(0);
						$('.imageblock').css({"top":"12%"});
						$('.cowcycle1').animate({opacity:1},500);
						$('.arrow2').delay(500).animate({opacity:1},500);
						$('.cowcycle2').delay(1000).animate({opacity:1},500);
						$('.arrow3').delay(1500).animate({opacity:1},500);
						$('.cowcycle3').delay(2000).animate({opacity:1},500);
						$('.arrow4').delay(2500).animate({opacity:1},500);
						$('.cowcycle4').delay(3000).animate({opacity:1},500);
						$('.arrow1').delay(3500).animate({opacity:1},500);
						setTimeout(function(){
							$nextBtn.show(0);
						},4000);
						break;

						case 3:
                            sound_player("sound_"+(countNext+1),false);
                            $nextBtn.hide(0);
						$('.rightclick').click(function(){
							$('.rightclick').css({"background":"green"});
							$('.wrongclick').css({"pointer-events":"none"});
                            current_sound.stop();
                            play_correct_incorrect_sound(1);
							$nextBtn.show(0);
						})
						$('.wrongclick').click(function(){
                            current_sound.stop();
                            $('.wrongclick').css({"background":"#d44b4b none repeat scroll 0% 0%"});
							play_correct_incorrect_sound(0);
						})
						break;

						case 4:
                            sound_player("sound_"+(countNext+1),false);
                            $nextBtn.hide(0);
						ole.footerNotificationHandler.hideNotification();
						$('.imageblock').css({"top":"12%"});
						$('.gohicycle1').animate({opacity:1},500);
						$('.a2').delay(500).animate({opacity:1},500);
						$('.gohicycle2').delay(1000).animate({opacity:1},500);
						$('.a3').delay(1500).animate({opacity:1},500);
						$('.gohicycle3').delay(2000).animate({opacity:1},500);
						$('.a4').delay(2500).animate({opacity:1},500);
						$('.gohicycle4').delay(3000).animate({opacity:1},500);
						$('.a5').delay(3500).animate({opacity:1},500);
						$('.gohicycle5').delay(4000).animate({opacity:1},500);
						$('.a1').delay(4500).animate({opacity:1},500);
						setTimeout(function(){
							ole.footerNotificationHandler.pageEndSetNotification();
						},5000);
						break;

                            break;
					}
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);


		generalTemplate();


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	// });

});


 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

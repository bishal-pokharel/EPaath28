var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
//slide1
{
	uppertextblock : [{
			textdata : data.lesson.chapter,
			textclass : 'lesson-title chapter'
			}],
    imageblock: [
        {
            imagetoshow: [


                {
                    imgclass: "coverpage img1",
                    imgsrc : imgpath + "coverpage_oviparous_and_viviparous.jpg"
                }

            ]
        }]
},
//slide2

{
	contentblockadditionalclass:'tree-bg',
	uppertextblock : [{
			textdata : data.string.p1text1,
			textclass : 'top_text'
		}],
	imageblock: [
	{
		imagetoshow: [


		{
			imgclass: "bottomimage",
			imgsrc : imgpath + "duck_01.png"
		}

		]
	}]
},
//slide3

{
	contentblockadditionalclass:'tree-bg',
	uppertextblock : [{
			textdata : data.string.p1text2,
			textclass : 'top_text'
		}],
	imageblock: [
	{
		imagetoshow: [


		{
			imgclass: "ducks",
			imgsrc : imgpath + "duck_02.png"
		},
		{
			imgclass: "egg",
			imgsrc : imgpath + "egg01.png"
		}

		]
	}]
},
//slide4

{
	contentblockadditionalclass:'tree-bg1',
	uppertextblock : [{
			textdata : data.string.p1text3,
			textclass : 'bottom_text'
		}],
	imageblock: [
	{
		imagetoshow: [


		{
			imgclass: "ducks1",
			imgsrc : imgpath + "duck_02.png"
		},
		{
			imgclass: "egg1",
			imgsrc : imgpath + "egg01.png"
		}

		]
	}]
},
//slide5

{
	contentblockadditionalclass:'stablebg1',
	uppertextblock : [{
			textdata : data.string.p1text4,
			textclass : 'top_text'
		}],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "egg2",
			imgsrc : imgpath + "egg01.png"
		}

		]
	}]
},
//slide6

{
	contentblockadditionalclass:'stablebg',
	uppertextblock : [{
			textdata : data.string.p1text5,
			textclass : 'dial1 hide1'
		},
		{
            textdata : data.string.p1text5_1,
            textclass : 'dial2 hide2'
		}
		],
	imageblock: [
	{
		imagetoshow: [
			{
                imgclass: "box1 hide1",
                imgsrc : imgpath + "dialoguebox01.png"
			},
			{
                imgclass: "box2 hide2",
                imgsrc : imgpath + "dialoguebox01.png"
			},

		{
			imgclass: "cow",
			imgsrc : imgpath + "cow.gif"
		},
		{
			imgclass: "egg3",
			imgsrc : imgpath + "egg02.png"
		}

		]
	}]
},
//slide7

{
	contentblockadditionalclass:'greenbg',
	uppertextblock : [{
			textdata : data.string.p1text6,
			textclass : 'extremebottom_text typingefftext'
		}],
	imageblock: [
	{
		imagetoshow: [


		{
			imgclass: "cowcycle1",
			imgsrc : imgpath + "cow01.png"
		},
		{
			imgclass: "arrow1",
			imgsrc : imgpath + "arrow01.png"
		},
		{
			imgclass: "cowcycle2",
			imgsrc : imgpath + "cow02.png"
		},
		{
			imgclass: "arrow2",
			imgsrc : imgpath + "arrow02.png"
		},
		{
			imgclass: "cowcycle3",
			imgsrc : imgpath + "cow03.png"
		},
		{
			imgclass: "arrow3",
			imgsrc : imgpath + "arrow03.png"
		},
		{
			imgclass: "cowcycle4",
			imgsrc : imgpath + "cow04.png"
		},
		{
			imgclass: "arrow4",
			imgsrc : imgpath + "arrow04.png"
		},
		{
			imgclass: "egg4",
			imgsrc : imgpath + "feelbad.png"
		}
		// {
		// 	imgclass: "fairy",
		// 	imgsrc : imgpath + "pari.gif"
		// }

		]
	}]
},
//slide8

{
	contentblockadditionalclass:'sheepstablebg2',
	uppertextblock : [{
			textdata : data.string.p1text7,
			textclass : 'top_text'
		}],
	imageblock: [
	{
		imagetoshow: [


		{
			imgclass: "egg2",
			imgsrc : imgpath + "egg02.png"
		}

		]
	}]
},
//slide9

{
	contentblockadditionalclass:'sheepstablebg',
	uppertextblock : [{
			textdata : data.string.p1text8,
			textclass : 'dial1 hide1'
		},
        {
            textdata : data.string.p1text8_1,
            textclass : 'dial3 hide2'
        }
		],
	imageblock: [
	{
		imagetoshow: [
            {
                imgclass: "box1 hide1",
                imgsrc : imgpath + "dialoguebox01.png"
            },
            {
                imgclass: "box2 hide2",
                imgsrc : imgpath + "dialoguebox01.png"
            },

		{
			imgclass: "ducklineinstable",
			imgsrc : imgpath + "egg02.png"
		},
		{
			imgclass: "sheep",
			imgsrc : imgpath + "sheep.gif"
		}

		]
	}]
},
//slide10

{
	contentblockadditionalclass:'bluebg',
	uppertextblock : [{
			textdata : data.string.p1text9,
			textclass : 'extremebottom_text typingefftext smallfont1'
		}],
	imageblock: [
	{
		imagetoshow: [


		{
			imgclass: "cowcycle1",
			imgsrc : imgpath + "sheep01.png"
		},
		{
			imgclass: "arrow1",
			imgsrc : imgpath + "arrow01.png"
		},
		{
			imgclass: "cowcycle2",
			imgsrc : imgpath + "sheep02.png"
		},
		{
			imgclass: "arrow2",
			imgsrc : imgpath + "arrow02.png"
		},
		{
			imgclass: "cowcycle3",
			imgsrc : imgpath + "sheep03.png"
		},
		{
			imgclass: "arrow3",
			imgsrc : imgpath + "arrow03.png"
		},
		{
			imgclass: "cowcycle4",
			imgsrc : imgpath + "sheep04.png"
		},
		{
			imgclass: "arrow4",
			imgsrc : imgpath + "arrow04.png"
		},
		{
			imgclass: "egg4",
			imgsrc : imgpath + "feelbad.png"
		}
		// {
		// 	imgclass: "fairy",
		// 	imgsrc : imgpath + "pari.gif"
		// }

		]
	}]
},
//slide11
{
	contentblockadditionalclass:'pigbg1',
	uppertextblock : [{
			textdata : data.string.p1text10,
			textclass : 'top_text'
		}],
	imageblock: [
	{
		imagetoshow: [


		{
			imgclass: "egg2",
			imgsrc : imgpath + "egg02.png"
		}

		]
	}]
},
//slide12
{
	contentblockadditionalclass:'pigbg2',
	uppertextblock : [{
			textdata : data.string.p1text11_1,
			textclass : 'dial4 hide2'
		},
        {
            textdata : data.string.p1text11,
            textclass : 'dial3 hide1'
        }
		],
	imageblock: [
	{
		imagetoshow: [
            {
                imgclass: "box3 hide2",
                imgsrc : imgpath + "dialoguebox01.png"
            },
            {
                imgclass: "box2 hide1",
                imgsrc : imgpath + "dialoguebox01.png"
            },

		{
			imgclass: "eggchickpig",
			imgsrc : imgpath + "egg02.png"
		},
        {
            imgclass: "pigimg",
            imgsrc : imgpath + "pig.gif"
        }

		]
	}]
},
//slide13

{
	contentblockadditionalclass:'pinkbg',
	uppertextblock : [{
			textdata : data.string.p1text12,
			textclass : 'bottom_text typingefftext smallfont'
		}],
	imageblock: [
	{
		imagetoshow: [


		{
			imgclass: "cowcycle1",
			imgsrc : imgpath + "pig01.png"
		},
		{
			imgclass: "arrow1",
			imgsrc : imgpath + "arrow01.png"
		},
		{
			imgclass: "cowcycle2",
			imgsrc : imgpath + "pig02.png"
		},
		{
			imgclass: "arrow2",
			imgsrc : imgpath + "arrow02.png"
		},
		{
			imgclass: "cowcycle3",
			imgsrc : imgpath + "pig03.png"
		},
		{
			imgclass: "arrow3",
			imgsrc : imgpath + "arrow03.png"
		},
		{
			imgclass: "cowcycle4",
			imgsrc : imgpath + "pig04.png"
		},
		{
			imgclass: "arrow4",
			imgsrc : imgpath + "arrow04.png"
		},
		{
			imgclass: "egg4",
			imgsrc : imgpath + "feelbad.png"
		}
		// {
		// 	imgclass: "fairy",
		// 	imgsrc : imgpath + "pari.gif"
		// }

		]
	}]
},
//slide14
{
	contentblockadditionalclass:'horsestablebg1',
	uppertextblock : [{
			textdata : data.string.p1text13,
			textclass : 'top_text'
		}],
	imageblock: [
	{
		imagetoshow: [


		{
			imgclass: "egg2",
			imgsrc : imgpath + "egg02.png"
		}

		]
	}]
},
//slide15
{
	contentblockadditionalclass:'horsestablebg2',
	uppertextblock : [
		{
			textdata : data.string.p1text14,
			textclass : 'dial6 hide1'
		},
        {
            textdata : data.string.p1text14_1,
            textclass : 'dial5 hide2'
        }
	],
	imageblock: [
	{
		imagetoshow: [
            {
                imgclass: "box5 hide1",
                imgsrc : imgpath + "dialoguebox01.png"
            },
            {
                imgclass: "box4 hide2",
                imgsrc : imgpath + "dialoguebox01.png"
            },

		{
			imgclass: "eggchickhorse",
			imgsrc : imgpath + "egg02.png"
		},
		{
			imgclass: "horse",
			imgsrc : imgpath + "horse.gif"
		}

		]
	}]
},
//slide16

{
	contentblockadditionalclass:'creambg',
	uppertextblock : [{
			textdata : data.string.p1text15,
			textclass : 'bottom_text typingefftext'
		}],
	imageblock: [
	{
		imagetoshow: [


		{
			imgclass: "cowcycle1",
			imgsrc : imgpath + "horse01.png"
		},
		{
			imgclass: "arrow1",
			imgsrc : imgpath + "arrow01.png"
		},
		{
			imgclass: "cowcycle2",
			imgsrc : imgpath + "horse02.png"
		},
		{
			imgclass: "arrow2",
			imgsrc : imgpath + "arrow02.png"
		},
		{
			imgclass: "cowcycle3",
			imgsrc : imgpath + "horse03.png"
		},
		{
			imgclass: "arrow3",
			imgsrc : imgpath + "arrow03.png"
		},
		{
			imgclass: "cowcycle4",
			imgsrc : imgpath + "horse04.png"
		},
		{
			imgclass: "arrow4",
			imgsrc : imgpath + "arrow04.png"
		},
		{
			imgclass: "egg4",
			imgsrc : imgpath + "feelbad.png"
		}
		// {
		// 	imgclass: "fairy",
		// 	imgsrc : imgpath + "pari.gif"
		// }

		]
	}]
},
//slide17
{
	contentblockadditionalclass:'ole-backrgound-gradient-weepysky',
	uppertextblock : [{
			textdata : data.string.p1text17,
			textclass : 'info_title'
		},
		{
			textdata : data.string.p1text18,
			textclass : 'info'
		}]
	// imageblock: [
	// {
	// 	imagetoshow: [
	// 	{
	// 		imgclass: "mid_fairy",
	// 		imgsrc : imgpath + "pari.gif"
	// 	}
	// 	]
	// }]
},
//slide18
{
	contentblockadditionalclass:'ole-background-gradient-blunatic',
	uppertextblock : [{
			textdata : data.string.p1text19,
			textclass : 'toptext'
		},
		{
			textdata : data.string.p1text20,
			textclass : 'bottext'
		}],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "centerimage",
			imgsrc : imgpath + "shark.png"
		}
		]
	}]
},
//slide19
{
	contentblockadditionalclass:'ole-background-gradient-blunatic',
	uppertextblock : [{
			textdata : data.string.p1text19,
			textclass : 'toptext'
		},
		{
			textdata : data.string.p1text21,
			textclass : 'bottext'
		}],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "centerimage",
			imgsrc : imgpath + "bat.png"
		}
		]
	}]
},
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;
    var isFirefox = typeof InstallTrigger !== 'undefined';
	var current_sound;
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            //    sounds
            {id: "sound_1", src: soundAsset + "S1_P1.ogg"},
            {id: "sound_2", src: soundAsset + "S1_P2.ogg"},
            {id: "sound_3", src: soundAsset + "S1_P3.ogg"},
            {id: "sound_4", src: soundAsset + "S1_P4.ogg"},
            {id: "sound_5", src: soundAsset + "S1_P5.ogg"},
            {id: "sound_6_1", src: soundAsset + "S1_P6_1.ogg"},
            {id: "sound_6_2", src: soundAsset + "S1_P6_2.ogg"},
            {id: "sound_7", src: soundAsset + "S1_P7.ogg"},
            {id: "sound_8", src: soundAsset + "S1_P8.ogg"},
            {id: "sound_9_1", src: soundAsset + "S1_P9_1.ogg"},
            {id: "sound_9_2", src: soundAsset + "S1_P9_2.ogg"},
            {id: "sound_10", src: soundAsset + "S1_P10.ogg"},
            {id: "sound_11", src: soundAsset + "S1_P11.ogg"},
            {id: "sound_12_1", src: soundAsset + "S1_P12_1.ogg"},
            {id: "sound_12_2", src: soundAsset + "S1_P12_2.ogg"},
            {id: "sound_13", src: soundAsset + "S1_P13.ogg"},
            {id: "sound_14", src: soundAsset + "S1_P14.ogg"},
            {id: "sound_15_1", src: soundAsset + "S1_P15_1.ogg"},
            {id: "sound_15_2", src: soundAsset + "S1_P15_2.ogg"},
            {id: "sound_16", src: soundAsset + "S1_P16.ogg"},
            {id: "sound_17", src: soundAsset + "S1_P17.ogg"},
            {id: "sound_18", src: soundAsset + "S1_P18.ogg"},
            {id: "sound_19", src: soundAsset + "S1_P19.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();
    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }

    function sound_player_seq(sound1,sound2){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound1);
        current_sound.play();
        current_sound.on('complete', function () {
            current_sound = createjs.Sound.play(sound2);
            current_sound.play();
            current_sound.on('complete', function () {
                navigationcontroller(countNext, $total_page);
            });
        });
	}

		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


      function navigationcontroller(islastpageflag){

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		vocabcontroller.findwords(countNext);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
        switch(countNext){
			case 5:
			case 8:
			case 11:
			case 14:
				sound_player_seq("sound_"+(countNext+1)+"_1","sound_"+(countNext+1)+"_2");
				$(".hide1").hide().fadeIn(1000);
				$(".hide2").hide().delay(3000).fadeIn(1000);
                break;
			case 6:
			case 9:
			case 12:
			case 15:
                playtextaudio("typingefftext", "p1text1", false);
                $nextBtn.hide(0);
						$('.cowcycle1').animate({opacity:1},500);
						$('.arrow2').delay(500).animate({opacity:1},500);
						$('.cowcycle2').delay(1000).animate({opacity:1},500);
						$('.arrow3').delay(1500).animate({opacity:1},500);
						$('.cowcycle3').delay(2000).animate({opacity:1},500);
						$('.arrow4').delay(2500).animate({opacity:1},500);
						$('.cowcycle4').delay(3000).animate({opacity:1},500);
						$('.arrow1').delay(3500).animate({opacity:1},500);
						break;
			default:
                sound_player("sound_"+(countNext+1),true);
                break;
        }
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);


		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	// });

    function playtextaudio(text_class, my_sound_data, last_page_flag){
        var $textblack = $("."+text_class);
        var current_text = $textblack.html();
        // current_text.replace(/<.*>/, '');
        play_text($textblack, current_text);
        sound_player("sound_"+(countNext+1),true);
        // current_sound = createjs.Sound.play(my_sound_data);
        // current_sound.play();
        // current_sound.on('complete', function(){
        //     textanimatecomplete = false;
        //     if(!last_page_flag)
        //         firstline_fin();
        //     else{
        //         vocabcontroller.findwords(countNext);
        //         navigationcontroller();
        //     }
        // });
    }
    var textanimatecomplete = false;

    function play_text($this, text){
        original_text =  text;
        if(isFirefox){
            $this.html("<span id='span_speec_text1'></span>"+text);
            $prevBtn.hide(0);
            var $span_speec_text = $("#span_speec_text1");
            // $this.css("background-color", "#faf");
            show_text($this, $span_speec_text,text, 105);	// 65 ms is the interval found out by hit and trial
        } else {
            $this.html("<span id='span_speec_text1'>"+original_text+"</span>");
        }
    }

    function show_text($this,  $span_speec_text, message, interval) {
        if (0 < message.length && !textanimatecomplete) {
            var nextText = message.substring(0, 1);
            var additionalinterval = 0;
            if (nextText == "<") {
                additionalinterval = 800;
                message = message.substring(4, message.length);
            } else {
                $span_speec_text.append(nextText);
                message = message.substring(1, message.length);
            }
            $this.html($span_speec_text);
            $this.find("span").css("color", "black");
            $this.append(message);
            setTimeout(function() {
                show_text($this, $span_speec_text, message, interval);
            }, (interval + additionalinterval));
        } else{
            textanimatecomplete = true;
        }
    }

});

/*===============================================
	 =            data highlight function            =
	 ===============================================*/

	 /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		CODE MODIFIED FOR THREE DIFFERENT HIGHTLIGHTS
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
		function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightstarttag2;
        var texthighlightstarttag3;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
              $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

              $(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
              (stylerulename2 = "parsedstring2") ;

              $(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
              (stylerulename3 = "parsedstring3") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
            texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
		/*=====  End of data highlight function  ======*/

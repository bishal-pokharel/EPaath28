var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
//slide0

{
	contentblockadditionalclass:'stablebg1',
	uppertextblock : [{
			textdata : data.string.p3text1,
			textclass : 'top_text'
		}]
},
//slide1

{
	contentblockadditionalclass:'stablebg2',
	uppertextblock : [{
			textdata : data.string.p3text2,
			textclass : 'top_text'
		}],

	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "centerimage",
			imgsrc : imgpath + "egg02.png"
		}

		]
	}]
},
//slide2

{
	contentblockadditionalclass:'stablebg3',
	uppertextblock : [
		{
			textdata : data.string.p3text3,
			textclass : 'dial1 hide1 langdial'
		},
        {
            textdata : data.string.p3text3_1,
            textclass : 'dial2 hide2 langdial'
        }],

	imageblock: [
	{
		imagetoshow: [
            {
                imgclass: "box1 hide1",
                imgsrc : imgpath + "dialoguebox01.png"
            },
            {
                imgclass: "box2 hide2",
                imgsrc : imgpath + "dialoguebox01.png"
            },
		{
			imgclass: "centerimage1",
			imgsrc : imgpath + "egg02.png"
		}

		]
	}]
},
// slide3
    {
        contentblockadditionalclass:'stablebg3',
        uppertextblock : [
        	{
            textdata : data.string.p3text3_2,
            textclass : 'dial1 hide1 langdial'
        },
            {
                textdata : data.string.p3text3_3,
                textclass : 'dial2 hide2 langdial'
            }],

        imageblock: [
            {
                imagetoshow: [
                    {
                        imgclass: "box1 hide1",
                        imgsrc : imgpath + "dialoguebox01.png"
                    },
                    {
                        imgclass: "box2 hide2",
                        imgsrc : imgpath + "dialoguebox01.png"
                    },
                    {
                        imgclass: "centerimage1",
                        imgsrc : imgpath + "egg02.png"
                    }

                ]
            }]
    },
//	slide4

{
	contentblockadditionalclass:'blue',
	uppertextblock : [
			{
				textdata : data.string.p3text4,
				textclass : 'extremebottom_text typingefftext'
			}],

	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "gohicycle1",
			imgsrc : imgpath + "tortile01.png"
		},
		{
			imgclass: "a1",
			imgsrc : imgpath + "arrow01.png"
		},
		{
			imgclass: "gohicycle2",
			imgsrc : imgpath + "tortile02.png"
		},
		{
			imgclass: "a2",
			imgsrc : imgpath + "arrow02.png"
		},
		{
			imgclass: "gohicycle3",
			imgsrc : imgpath + "tortile03.png"
		},
		{
			imgclass: "a3",
			imgsrc : imgpath + "arrow03.png"
		},
		{
			imgclass: "gohicycle4",
			imgsrc : imgpath + "tortile04.png"
		},
		{
			imgclass: "a4",
			imgsrc : imgpath + "arrow04.png"
		},
		{
			imgclass: "gohicycle5",
			imgsrc : imgpath + "tortile05.png"
		},
		{
			imgclass: "a5",
			imgsrc : imgpath + "arrow02.png"
		},
		// {
		// 	imgclass: "fairy",
		// 	imgsrc : imgpath + "pari.gif"
		// },
		{
			imgclass: "feelingbadbird",
			imgsrc : imgpath + "feelbad.png"
		}

		]
	}]
},
//slide5

{
	contentblockadditionalclass:'birdbg',
	uppertextblock : [{
			textdata : data.string.p3text5,
			textclass : 'top_text'
		}],

	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "centerimage",
			imgsrc : imgpath + "egg02.png"
		}

		]
	}]
},
//slide6

{
	contentblockadditionalclass:'birdbg1',
	uppertextblock : [{
			textdata : data.string.p3text6,
			textclass : 'dial1 hide1 langdial'
		},
        {
            textdata : data.string.p3text6_1,
            textclass : 'dial2 hide2 langdial'
        }
	],

	imageblock: [
	{
		imagetoshow: [
            {
                imgclass: "box1 hide1",
                imgsrc : imgpath + "dialoguebox01.png"
            },
            {
                imgclass: "box2 hide2",
                imgsrc : imgpath + "dialoguebox01.png"
            },
		{
			imgclass: "centerimage1",
			imgsrc : imgpath + "egg02.png"
		},
		{
			imgclass: "bird",
			imgsrc : imgpath + "bird.png"
		}

		]
	}]
},
// slide7

{
	contentblockadditionalclass:'nextblue',
	uppertextblock : [
			{
				textdata : data.string.p3text7,
				textclass : 'extremebottom_text typingefftext'
			}],

	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "gohicycle1",
			imgsrc : imgpath + "bird01.png"
		},
		{
			imgclass: "a1",
			imgsrc : imgpath + "arrow01.png"
		},
		{
			imgclass: "gohicycle2",
			imgsrc : imgpath + "bird02.png"
		},
		{
			imgclass: "a2",
			imgsrc : imgpath + "arrow02.png"
		},
		{
			imgclass: "gohicycle3",
			imgsrc : imgpath + "bird03.png"
		},
		{
			imgclass: "a3",
			imgsrc : imgpath + "arrow03.png"
		},
		{
			imgclass: "gohicycle4",
			imgsrc : imgpath + "bird04.png"
		},
		{
			imgclass: "a4",
			imgsrc : imgpath + "arrow04.png"
		},
		{
			imgclass: "gohicycle5",
			imgsrc : imgpath + "bird05.png"
		},
		{
			imgclass: "a5",
			imgsrc : imgpath + "arrow02.png"
		},
		// {
		// 	imgclass: "fairy",
		// 	imgsrc : imgpath + "pari.gif"
		// },
		{
			imgclass: "feelingbadbird",
			imgsrc : imgpath + "feelbad.png"
		}
		]
	}]
},
//slide8

{
	contentblockadditionalclass:'henbg',
	uppertextblock : [{
			textdata : data.string.p3text8,
			textclass : 'top_text'
		}],

	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "centerimage",
			imgsrc : imgpath + "egg02.png"
		}

		]
	}]
},
//slide9

{
	contentblockadditionalclass:'henbg1',
	uppertextblock : [{
			textdata : data.string.p3text9,
			textclass : 'dial1 hide1 langdial'
		},
        {
            textdata : data.string.p3text9_1,
            textclass : 'dial2 hide2 langdial'
        }],

	imageblock: [
	{
		imagetoshow: [
            {
                imgclass: "box1 hide1",
                imgsrc : imgpath + "dialoguebox01.png"
            },
            {
                imgclass: "box2 hide2",
                imgsrc : imgpath + "dialoguebox01.png"
            },
		{
			imgclass: "centerimage2",
			imgsrc : imgpath + "egg02.png"
		}

		]
	}]
},
// slide10

{
	contentblockadditionalclass:'nextblue1',
	uppertextblock : [
			{
				textdata : data.string.p3text10,
				textclass : 'extremebottom_text typingefftext'
			}],

	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "gohicycle1",
			imgsrc : imgpath + "hen01.png"
		},
		{
			imgclass: "a1",
			imgsrc : imgpath + "arrow01.png"
		},
		{
			imgclass: "gohicycle2",
			imgsrc : imgpath + "hen02.png"
		},
		{
			imgclass: "a2",
			imgsrc : imgpath + "arrow02.png"
		},
		{
			imgclass: "gohicycle3",
			imgsrc : imgpath + "hen03.png"
		},
		{
			imgclass: "a3",
			imgsrc : imgpath + "arrow03.png"
		},
		{
			imgclass: "gohicycle4",
			imgsrc : imgpath + "hen04.png"
		},
		{
			imgclass: "a4",
			imgsrc : imgpath + "arrow04.png"
		},
		{
			imgclass: "gohicycle5",
			imgsrc : imgpath + "hen05.png"
		},
		{
			imgclass: "a5",
			imgsrc : imgpath + "arrow02.png"
		},
		// {
		// 	imgclass: "fairy",
		// 	imgsrc : imgpath + "pari.gif"
		// },
		{
			imgclass: "feelingbadbird",
			imgsrc : imgpath + "feelbad.png"
		}
		]
	}]
},
//slide11

{
	contentblockadditionalclass:'frogbg',
	uppertextblock : [{
			textdata : data.string.p3text11,
			textclass : 'top_text'
		}],

	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "centerimage5",
			imgsrc : imgpath + "egg02.png"
		}

		]
	}]
},
//slide12

{
	contentblockadditionalclass:'frogbg1',
	uppertextblock : [{
			textdata : data.string.p3text12,
			textclass : 'dial1 hide1 langdial'
		},
        {
            textdata : data.string.p3text12_1,
            textclass : 'dial2 hide2 langdial'
        }],

	imageblock: [
	{
		imagetoshow: [
            {
                imgclass: "box1 hide1",
                imgsrc : imgpath + "dialoguebox01.png"
            },
            {
                imgclass: "box2 hide2",
                imgsrc : imgpath + "dialoguebox01.png"
            },
		{
			imgclass: "centerimage4",
			imgsrc : imgpath + "egg02.png"
		}

		]
	}]
},
// slide13

{
	contentblockadditionalclass:'greenbg',
	uppertextblock : [
			{
				textdata : data.string.p3text13,
                textclass : 'extremebottom_text typingefftext'
			}],

	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "gohicycle1",
			imgsrc : imgpath + "frog01.png"
		},
		{
			imgclass: "a1",
			imgsrc : imgpath + "arrow01.png"
		},
		{
			imgclass: "gohicycle2",
			imgsrc : imgpath + "frog02.png"
		},
		{
			imgclass: "a2",
			imgsrc : imgpath + "arrow02.png"
		},
		{
			imgclass: "gohicycle3",
			imgsrc : imgpath + "frog03.png"
		},
		{
			imgclass: "a3",
			imgsrc : imgpath + "arrow03.png"
		},
		{
			imgclass: "gohicycle4",
			imgsrc : imgpath + "frog04.png"
		},
		{
			imgclass: "a4",
			imgsrc : imgpath + "arrow04.png"
		},
		{
			imgclass: "gohicycle5",
			imgsrc : imgpath + "frog05.png"
		},
		{
			imgclass: "a5",
			imgsrc : imgpath + "arrow02.png"
		},
		// {
		// 	imgclass: "fairy",
		// 	imgsrc : imgpath + "pari.gif"
		// },
		{
			imgclass: "feelingbadbird",
			imgsrc : imgpath + "feelbad.png"
		}
		]
	}]
},
//slide14
{
	contentblockadditionalclass:'ole-backrgound-gradient-weepysky',
	uppertextblock : [{
			textdata : data.string.p1text17,
			textclass : 'info_title'
		},
		{
			textdata : data.string.p3text14,
			textclass : 'info'
		}]
	// imageblock: [
	// {
	// 	imagetoshow: [
	// 	{
	// 		imgclass: "mid_fairy",
	// 		imgsrc : imgpath + "pari.gif"
	// 	}
	// 	]
	// }]
},
//slide15
{
	contentblockadditionalclass:'returnshomebg',
	uppertextblock : [
		{
			textdata : data.string.p3text15,
			textclass : 'top_text'
		}],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "returninghome",
			imgsrc : imgpath + "d7.png"
		}
		]
	}]
},
//slide16
{
	contentblockadditionalclass:'returnshomebg',
	uppertextblock : [
		{
			textdata : data.string.p3text16_1,
			textclass : 'dial1 hide2 langdial'
		},
        {
            textdata : data.string.p3text16,
            textclass : 'dial2 hide1 langdial'
        }],
	imageblock: [
	{
		imagetoshow: [
            {
                imgclass: "box1 hide2",
                imgsrc : imgpath + "dialoguebox01.png"
            },
            {
                imgclass: "box2 hide1",
                imgsrc : imgpath + "dialoguebox01.png"
            },
		{
			imgclass: "returninghome",
			imgsrc : imgpath + "d6.png"
		}
		]
	}]
},
//slide17
{
	contentblockadditionalclass:'returnshomebg',
	uppertextblock : [
		{
			textdata : data.string.p3text17_1,
			textclass : 'dial1 hide2 langdial'
		},
        {
            textdata : data.string.p3text17,
            textclass : 'dial2 hide1 langdial'
        }],
	imageblock: [
	{
		imagetoshow: [
            {
                imgclass: "box1 hide2",
                imgsrc : imgpath + "dialoguebox01.png"
            },
            {
                imgclass: "box2 hide1",
                imgsrc : imgpath + "dialoguebox01.png"
            },
		{
			imgclass: "returninghome",
			imgsrc : imgpath + "d4.png"
		}
		]
	}]
},
//slide18
{
	contentblockadditionalclass:'returnshomebg',
	uppertextblock : [
		{
			textdata : data.string.p3text18,
			textclass : 'dial1 langdial'
		}],
	imageblock: [
	{
		imagetoshow: [
            {
                imgclass: "box2",
                imgsrc : imgpath + "dialoguebox01.png"
            },
		{
			imgclass: "returninghome",
			imgsrc : imgpath + "d5.png"
		}
		]
	}]
},
//slide19
{
	contentblockadditionalclass:'whitishgreen',
	uppertextblock : [
		{
			textdata : data.string.p3text19,
			textclass : 'box_text typingeffect'
		}],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "chick",
			imgsrc : imgpath + "d3.png"
		},
		{
			imgclass: "chick1",
			imgsrc : imgpath + "duck01.png"
		},
		{
			imgclass: "b1",
			imgsrc : imgpath + "arrow01.png"
		},
		{
			imgclass: "chick2",
			imgsrc : imgpath + "duck02.png"
		},
		{
			imgclass: "b2",
			imgsrc : imgpath + "arrow01.png"
		},
		{
			imgclass: "chick3",
			imgsrc : imgpath + "duck03.png"
		},
		{
			imgclass: "b3",
			imgsrc : imgpath + "arrow01.png"
		},
		{
			imgclass: "chick4",
			imgsrc : imgpath + "duck04.png"
		}
		]
	}]
},
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;
	var clicks = 0;
    var current_sound;
	var  inputname = '';
    var isFirefox = typeof InstallTrigger !== 'undefined';


	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            //    sounds
            {id: "sound_1", src: soundAsset + "S3_P1.ogg"},
            {id: "sound_2", src: soundAsset + "S3_P2.ogg"},
            {id: "sound_3_1", src: soundAsset + "S3_P3_1.ogg"},
            {id: "sound_3_2", src: soundAsset + "S3_P3_2.ogg"},
            {id: "sound_4_1", src: soundAsset + "S3_P4_1.ogg"},
            {id: "sound_4_2", src: soundAsset + "S3_P4_2.ogg"},
            {id: "sound_5", src: soundAsset + "S3_P5.ogg"},
            {id: "sound_6", src: soundAsset + "S3_P6.ogg"},
            {id: "sound_7_1", src: soundAsset + "S3_P7_1.ogg"},
            {id: "sound_7_2", src: soundAsset + "S3_P7_2.ogg"},
            {id: "sound_8", src: soundAsset + "S3_P8.ogg"},
            {id: "sound_9", src: soundAsset + "S3_P9.ogg"},
            {id: "sound_10_1", src: soundAsset + "S3_P10_1.ogg"},
            {id: "sound_10_2", src: soundAsset + "S3_P10_2.ogg"},
            {id: "sound_11", src: soundAsset + "S3_P11.ogg"},
            {id: "sound_12", src: soundAsset + "S3_P12.ogg"},
            {id: "sound_13_1", src: soundAsset + "S3_P13_1.ogg"},
            {id: "sound_13_2", src: soundAsset + "S3_P13_2.ogg"},
            {id: "sound_14", src: soundAsset + "S3_P14.ogg"},
            {id: "sound_15", src: soundAsset + "S3_P15.ogg"},
            {id: "sound_16", src: soundAsset + "S3_P16.ogg"},
            {id: "sound_17_1", src: soundAsset + "S3_P17_1.ogg"},
            {id: "sound_17_2", src: soundAsset + "S3_P17_2.ogg"},
            {id: "sound_18_1", src: soundAsset + "S3_P18_1.ogg"},
            {id: "sound_18_2", src: soundAsset + "S3_P18_2.ogg"},
            {id: "sound_19", src: soundAsset + "S3_P19_1.ogg"},
            {id: "sound_20", src: soundAsset + "S3_P20.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();
    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }

    function sound_player_seq(sound1,sound2){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound1);
        current_sound.play();
        current_sound.on('complete', function () {
            current_sound = createjs.Sound.play(sound2);
            current_sound.play();
            current_sound.on('complete', function () {
                navigationcontroller(countNext, $total_page);
            });
        });
    }
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


      function navigationcontroller(islastpageflag){

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		vocabcontroller.findwords(countNext);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		switch (countNext) {
			case 2:
                sound_player_seq("sound_"+(countNext+1)+"_1","sound_"+(countNext+1)+"_2");
                showhidedial();
                break;
			case 3:
                sound_player_seq("sound_"+(countNext+1)+"_1","sound_"+(countNext+1)+"_2");
                showhidedial();
				$(".dial1").css({"top":"35%","width":"26%","left":"22%","height":"12%"});
				$(".dial2").css({"top":"21%"});
                break;
			case 6:
                sound_player_seq("sound_"+(countNext+1)+"_1","sound_"+(countNext+1)+"_2");
                showhidedial();
                $(".dial1").css({"top":"35%"});
                $(".dial2").css({"top":"40%"});

                $(".box1").css({"bottom":"35%"});
                $(".box2").css({"bottom":"28%"});
                break;
            case 9:
                sound_player_seq("sound_"+(countNext+1)+"_1","sound_"+(countNext+1)+"_2");
                showhidedial();
                $(".dial1").css({"top":"38%","left":"5%"});
                $(".dial2").css({"top":"10%"});

                $(".box1").css({"bottom":"35%","left":"4%"});
                $(".box2").css({"bottom":"57%"});
                break;
			case 12:
                sound_player_seq("sound_"+(countNext+1)+"_1","sound_"+(countNext+1)+"_2");
                showhidedial();
                $(".dial1").css({"width":"31%","top":"10%","left":"4%"});
                $(".dial2").css({"top":"5%","left":"51%"});
                $(".box1").css({"width":"34%","transform":"scaleX(-1)","top":"9%","left":"3%"});
                $(".box2").css({"bottom":"62%","left":"49%"});
                break;
			case 4:
			case 7:
			case 13:
                playtextaudio("typingefftext", "p1text1", false);

                $nextBtn.hide(0);
						ole.footerNotificationHandler.hideNotification();
						$('.imageblock').css({"top":"-1.5%"});
						$('.gohicycle1').animate({opacity:1},500);
						$('.a2').delay(500).animate({opacity:1},500);
						$('.gohicycle2').delay(1000).animate({opacity:1},500);
						$('.a3').delay(1500).animate({opacity:1},500);
						$('.gohicycle3').delay(2000).animate({opacity:1},500);
						$('.a4').delay(2500).animate({opacity:1},500);
						$('.gohicycle4').delay(3000).animate({opacity:1},500);
						$('.a5').delay(3500).animate({opacity:1},500);
						$('.gohicycle5').delay(4000).animate({opacity:1},500);
						$('.a1').delay(4500).animate({opacity:1},500);
						break;
			case 10:
                playtextaudio("typingefftext", "p1text1", false);

                $nextBtn.hide(0);
						ole.footerNotificationHandler.hideNotification();
						$('.imageblock').css({"top":"-2.5%","width":"70%","left":"15%"});
						$('.fairy').css({"left": "-19%","top": "61%"});
						$('.gohicycle1').animate({opacity:1},500);
						$('.a2').delay(500).animate({opacity:1},500);
						$('.gohicycle2').delay(1000).animate({opacity:1},500);
						$('.a3').delay(1500).animate({opacity:1},500);
						$('.gohicycle3').delay(2000).animate({opacity:1},500);
						$('.a4').delay(2500).animate({opacity:1},500);
						$('.gohicycle4').delay(3000).animate({opacity:1},500);
						$('.a5').delay(3500).animate({opacity:1},500);
						$('.gohicycle5').delay(4000).animate({opacity:1},500);
						$('.a1').delay(4500).animate({opacity:1},500);
						break;
			case 16:
                sound_player_seq("sound_"+(countNext+1)+"_1","sound_"+(countNext+1)+"_2");
                showhidedial();
                $(".dial1").css({"top":"35%","left":"1%"});
                $(".dial2").css({"top":"9%","left":"42%"});
                $(".box1").css({"left":"1%","top":"35%"});
                $(".box2").css({"bottom":"60%","left":"40%"});
                break;
            case 17:
                sound_player_seq("sound_"+(countNext+1)+"_1","sound_"+(countNext+1)+"_2");
                showhidedial();
                $(".dial1").css({"top":"44%","left":"1%"});
                $(".dial2").css({"top":"19%","left":"38%"});
                $(".box1").css({"left":"4%","top":"42%","width":"23%"});
                $(".box2").css({"bottom":"59%","left":"39%","width":"32%"});
                break;
			case 18:
                sound_player("sound_"+(countNext+1),true);
                $(".dial1").css({"top":"11%","left":"43%"});
                $(".box2").css({"bottom":"57%","left":"40%"});
                break;

            default:
                sound_player("sound_"+(countNext+1),true);
                break;

		}

	}
    function playtextaudio(text_class, my_sound_data, last_page_flag){
        var $textblack = $("."+text_class);
        var current_text = $textblack.html();
        // current_text.replace(/<.*>/, '');
        play_text($textblack, current_text);
        sound_player("sound_"+(countNext+1),true);
    }
    var textanimatecomplete = false;

    function play_text($this, text){
        original_text =  text;
        if(isFirefox){
            $this.html("<span id='span_speec_text'></span>"+text);
            $prevBtn.hide(0);
            var $span_speec_text = $("#span_speec_text");
            // $this.css("background-color", "#faf");
            show_text($this, $span_speec_text,text, 105);	// 65 ms is the interval found out by hit and trial
        } else {
            $this.html("<span id='span_speec_text'>"+original_text+"</span>");
        }
    }

    function show_text($this,  $span_speec_text, message, interval) {
        if (0 < message.length && !textanimatecomplete) {
            var nextText = message.substring(0, 1);
            var additionalinterval = 0;
            if (nextText == "<") {
                additionalinterval = 800;
                message = message.substring(4, message.length);
            } else {
                $span_speec_text.append(nextText);
                message = message.substring(1, message.length);
            }
            $this.html($span_speec_text);
            $this.find("span").css("color", "black");
            $this.append(message);
            setTimeout(function() {
                show_text($this, $span_speec_text, message, interval);
            }, (interval + additionalinterval));
        } else{
            textanimatecomplete = true;
        }
    }

// for entering the value in input box
	function input_box(input_class, button_class) {
		$(input_class).keydown(function(event){
				var charCode = (event.which) ? event.which : event.keyCode;
				/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
				if(charCode === 13 && button_class!=null) {
						$(button_class).trigger("click");
			}
			var condition = charCode != 8 && charCode != 16 && charCode != 20 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, shift, caps , backspace or arrow keys
				if (!condition) {
					return true;
				}
				//check if user inputs more than one '.'
			if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
						return false;
				}
				//check . and 0-9 separately after checking arrow and other keys
				if((charCode < 65 || charCode > 90)){
					return false;
				}
				return true;
		});
		$(input_class).keyup(function(event){
    		if (String(event.target.value).length >= 1) {
    			$(".sbmtbtn").show(0);
    			$(button_class).show(0);
    			global_save_val = String(event.target.value);
    		}
    		else{
    			$(".sbmtbtn").hide(0);
    			$(button_class).hide(0);
    		}
  			return true;
		});
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);


		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	// });
    function showhidedial(){
        $(".hide1").hide().delay(100).fadeIn(1000);
        $(".hide2").hide().delay(2000).fadeIn(1000);
    }
});


/*===============================================
	 =            data highlight function            =
	 ===============================================*/

	 /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		CODE MODIFIED FOR THREE DIFFERENT HIGHTLIGHTS
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
		function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightstarttag2;
        var texthighlightstarttag3;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
              $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

              $(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
              (stylerulename2 = "parsedstring2") ;

              $(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
              (stylerulename3 = "parsedstring3") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
            texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            // replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
            // replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
		/*=====  End of data highlight function  ======*/

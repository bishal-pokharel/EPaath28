var imgpath = $ref + "/images/DIY/";
var soundAsset = $ref+"/sounds/"+$lang+'/';
var no_of_draggable = 4; //no of draggable to display at a time1
Array.prototype.shufflearray = function() {
    var i = this.length,
        j, temp;
    while (--i > 0) {
        j = Math.floor(Math.random() * (i + 1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
    return this;
};
var sound_dg1 = new buzz.sound(soundAsset + "champion.ogg");
var sound_ins = new buzz.sound(soundAsset + "S4_P1.ogg");
var content = [
    //slide 0
    {
        textblock : [{
            textdata : data.string.diy,
            textclass : 'diytext'
        }],
        imageblock: [
            {
                imagetoshow: [


                    {
                        imgclass: "coverpage img1",
                        imgsrc : "images/diy_bg/a_07.png"
                    }

                ]
            }]
    },
    //slide 1
    {
        contentblockadditionalclass: "ole-background-gradient-morningmist",
        contentnocenteradjust: true,
        textblockadditionalclass: 'instruction',
        textblock: [{
            textdata: data.string.p4text1,
            textclass: 'ques'
        }],
        // draggableblockadditionalclass: 'frac_ques',
        draggableblock: [{
            draggables: [{
                draggableclass: "class_1 sliding hidden",
                has_been_dropped : false,
                imgclass: "",
                imgsrc: imgpath + "bee.png"
            }, {
                draggableclass: "class_1 sliding hidden",
                has_been_dropped : false,
                imgclass: "",
                imgsrc: imgpath + "corcodile.png"
            }, {
                draggableclass: "class_1 sliding hidden",
                has_been_dropped : false,
                imgclass: "",
                imgsrc: imgpath + "duck.png"
            }, {
                draggableclass: "class_1 sliding hidden",
                has_been_dropped : false,
                imgclass: "",
                imgsrc: imgpath + "hen.png"
            }, {
                draggableclass: "class_1 sliding hidden",
                has_been_dropped : false,
                imgclass: "",
                imgsrc: imgpath + "shake.png"
            }, {
                draggableclass: "class_1 sliding hidden",
                has_been_dropped : false,
                imgclass: "",
                imgsrc: imgpath + "spider.png"
            }, {
                draggableclass: "class_2 sliding hidden",
                has_been_dropped : false,
                imgclass: "",
                imgsrc: imgpath + "bat.png"
            }, {
                draggableclass: "class_2 sliding hidden",
                has_been_dropped : false,
                imgclass: "",
                imgsrc: imgpath + "cow.png"
            }, {
                draggableclass: "class_2 sliding hidden",
                has_been_dropped : false,
                imgclass: "",
                imgsrc: imgpath + "elephant.png"
            }, {
                draggableclass: "class_2 sliding hidden",
                has_been_dropped : false,
                imgclass: "allimg",
                imgsrc: imgpath + "whale.png"
            }, {
                draggableclass: "class_2 sliding hidden",
                has_been_dropped : false,
                imgclass: "",
                imgsrc: imgpath + "sheep.png"
            }, {
                draggableclass: "class_2 sliding hidden",
                has_been_dropped : false,
                imgclass: "",
                imgsrc: imgpath + "woman.png"
            }]
        }],
        droppableblock: [{
            droppables: [{
                headerdata: data.string.p4text2,
                droppablecontainerclass: "",
                droppableclass: "drop_class_1",
                imgclass: "",

            }, {
                headerdata: data.string.p4text3,
                droppablecontainerclass: "",
                droppableclass: "drop_class_2",
                imgclass: "",

            }]
        }],
        imageblock: [
            {
                imagetoshow: [
                    {
                        imgclass: "monkey",
                        imgid: "img1",
                        imgsrc: imgpath + "welldone02.png"
                    },
                ]
            }
        ]
    }
];

var dummy_class = {
    draggableclass: "dummy_class hidden",
    imgclass: "",
    imgsrc: ""
};

/* Suffle content elements for draggable
 * add some dummy class so that flex behaves correctly
 * add position for first fixed no of draggables
 */
content[1].draggableblock[0].draggables.shufflearray();
for (var i = 1; i < no_of_draggable + 1; i++) {
    var asd = content[1].draggableblock[0].draggables[i - 1].draggableclass.split('"')[0].split('hidden');
    content[1].draggableblock[0].draggables[i - 1].draggableclass = asd[0] + 'position_' + i;
    content[1].draggableblock[0].draggables.push(dummy_class);
}


$(function() {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var no_egg = 0;
    var egg_count = false;
    var current_sound;

    var $total_page = content.length;
    console.log($total_page);
    var vocabcontroller =  new Vocabulary();
  	vocabcontroller.init();
    Handlebars.registerPartial("draggablecontent", $("#draggablecontent-partial").html());
    Handlebars.registerPartial("droppablecontent", $("#droppablecontent-partial").html());

    function navigationcontroller(islastpageflag){

        if(countNext == 0 && $total_page!=1){
            $nextBtn.show(0);
            $prevBtn.css('display', 'none');
        }
        else if($total_page == 1){
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');

            // if lastpageflag is true
            ole.footerNotificationHandler.lessonEndSetNotification() ;
        }
        else if(countNext > 0 && countNext < $total_page-1){
            $nextBtn.show(0);
            $prevBtn.show(0);
        }
        else if(countNext == $total_page-1){
            $nextBtn.css('display', 'none');
            $prevBtn.show(0);

            // if lastpageflag is true
            ole.footerNotificationHandler.lessonEndSetNotification() ;
        }
    }

    var score = 0;
    /*random scoreboard eggs*/
    var wrngClicked = [false, false, false, false, false, false, false, false, false, false, false, false];


    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);

        $nextBtn.hide(0);
        $prevBtn.hide(0);
        var index = 0;
        var flag = 0;

        /*generate question no at the beginning of question*/
        switch (countNext) {
            case 0:
                play_diy_audio();
                navigationcontroller();
                break;
            default:
                soundplayer(sound_ins,false);

                $nextBtn.hide(0);
                $(".draggable").draggable({
                containment: "body",
                revert: "true",
                appendTo: "body",
                helper: "clone",
                zindex: 1000,
                start: function(event, ui) {
                    $(this).css({
                        "opacity": "0"
                    });
                    $(ui.helper).addClass("ui-draggable-helper");
                    $(ui.helper).removeClass("sliding");
                },
                stop: function(event, ui) {
                    $(this).css({
                        "opacity": "1"
                    });
                }
            });
            $('.drop_class_1').droppable({
                // accept : ".class_1",
                hoverClass: "hovered",
                drop: function(event, ui) {
                    if (ui.draggable.hasClass("class_1")) {
                        play_correct_incorrect_sound(1);
                        if (wrngClicked[countNext] == false) {
                            if(($(ui.draggable).data("dropped"))==false){
                    		}
                        }
                        handleCardDrop(event, ui, ".class_1", ".drop_class_1");
                        index++;
                    } else {
                        play_correct_incorrect_sound(0);
                        if(($(ui.draggable).data("dropped"))==false){
                            wrngClicked[countNext] = true;
	                        $("#egg" + countNext).attr("src", "images/eggs/egg_wrong.png").removeClass('eggmove');
	                	}
                    }
                    if(($(ui.draggable).data("dropped"))==false){
                        countNext++;
                        $(ui.draggable).data("dropped",true);
                    }
                    if (index == 12){
            						ole.footerNotificationHandler.lessonEndSetNotification();
            						$(".ques").text(data.string.p4text4);
                        $(".monkey").animate({"opacity":"1"},1000);
                        soundplayer(sound_dg1,true);

                    }
                }
            });

            $('.drop_class_2').droppable({
                hoverClass: "hovered",
                drop: function(event, ui) {
                    if (ui.draggable.hasClass("class_2")) {
                        play_correct_incorrect_sound(1);
                        if (wrngClicked[countNext] == false) {
                        	if(($(ui.draggable).data("dropped"))==false){
		                    }
                        }
                        handleCardDrop(event, ui, ".class_2", ".drop_class_2");
                        index++;
                    } else {
                        play_correct_incorrect_sound(0);
                        if(($(ui.draggable).data("dropped"))==false){
	                        wrngClicked[countNext] = true;
	                	}
                    }
                    if(($(ui.draggable).data("dropped"))==false){
                        countNext++;
                        $(ui.draggable).data("dropped",true);
                    }
                    if (index == 12){
		                    ole.footerNotificationHandler.lessonEndSetNotification();
                        $(".ques").text(data.string.p4text4);
                        $(".monkey").animate({"opacity":"1"},1000);
                    }
                }
            });

            function topLeftCalculator(count) {
                var top = count % 3;
                var factor = Math.floor(count / 3);
                var height = 0;
                var left = 0;
                switch (top) {
                    case 0:
                        height = 20;
                        left = (factor > 0) ? ((factor * 5) + 20) : 40;
                        break;
                    case 1:
                        height = 30;
                        left = (factor > 0) ? ((factor * 7) + 30) : 50;
                        break;
                    case 2:
                        height = 40;
                        left = (factor > 0) ? ((factor * 9) + 40) : 60;
                        break;
                }

                var returnNumber = [height, left];
                return returnNumber;

            }

            function handleCardDrop(event, ui, classname, droppedOn) {
                ui.draggable.draggable('disable');
                var dropped = ui.draggable;
                // var to count no. of divs in the droppable div
                var drop_index = $(droppedOn + ">div").length;
                var top_position = drop_index;
                // var lef_position = drop_index * 32;
                $(ui.draggable).removeClass("sliding");
                $(ui.draggable).detach().css({
                    "position": "relative",
                    "cursor": 'auto',
                    "flex": "0 0 30%",
                    "height": "45%",
                    "border":"none"
                }).appendTo(droppedOn);
                var $newEntry = $(".draggableblock> .hidden").eq(0);

                var $draggable3;
                var $draggable2;
                var $draggable1;
                if (dropped.hasClass("position_4")) {
                    dropped.removeClass("position_4");
                    $draggable3 = $(".position_3");
                    $draggable2 = $(".position_2");
                    $draggable1 = $(".position_1");
                } else if (dropped.hasClass("position_3")) {
                    dropped.removeClass("position_3");
                    $draggable2 = $(".position_2");
                    $draggable1 = $(".position_1");
                } else if (dropped.hasClass("position_2")) {
                    dropped.removeClass("position_2");
                    $draggable1 = $(".position_1");
                } else if (dropped.hasClass("position_1")) {
                    dropped.removeClass("position_1");
                }

                if ($draggable3 != null) {
                    $draggable3.removeClass("position_3").addClass("position_4");
                    $draggable3.removeClass('sliding');
                    setTimeout(function() {
                        $draggable3.addClass('sliding');
                    }, 1);
                }
                if ($draggable2 != null) {
                    $draggable2.removeClass("position_2").addClass("position_3");
                    $draggable2.removeClass('sliding');
                    setTimeout(function() {
                        $draggable2.addClass('sliding');
                    }, 1);
                }
                if ($draggable1 != null) {
                    $draggable1.removeClass("position_1").addClass("position_2");
                    $draggable1.removeClass('sliding');
                    setTimeout(function() {
                        $draggable1.addClass('sliding');
                    }, 1);
                }
                if ($newEntry != null) {
                    $newEntry.removeClass("hidden").addClass("position_1");
                }
                 if (no_egg == 12) {
                }
                egg_count = false;
            }
            break;
        }
    }
    function soundplayer(i,navigate){
      buzz.all().stop();
      i.play().bind("ended",function(){
          navigate?navigationcontroller():"false";
      });
    }

    function templateCaller() {
        /*always hide next and previous navigation button unless
        explicitly called from inside a template*/
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        loadTimelineProgress($total_page, countNext + 1);


        // call the template
        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

    }

    // first call to template caller
    templateCaller();

    /* navigation buttons event handlers */
	function navigationcontroller(islastpageflag){
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
	/*		islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		*/
    }
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}
    $nextBtn.on("click", function(){
        countNext++;
        templateCaller();
    });


	$prevBtn.on('click', function() {
        countNext--;
        templateCaller();

        /* if footerNotificationHandler pageEndSetNotification was called then on click of
        	previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    /*=====  End of Templates Controller Block  ======*/
});

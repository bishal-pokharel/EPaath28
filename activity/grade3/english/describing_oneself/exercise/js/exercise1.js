var imgpath = $ref+"/images/exercise/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide1
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "title",
			textdata: data.string.eins
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "im-1",
					imgsrc : '',
					imgid : 'im-01'
				},
			]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textclass: "answer",
			textdata: data.string.eq_1,
			datahighlightflag: true,
			datahighlightcustomclass: 'blank-space',
			imgclass: 'flipped-v',
			imgid : 't-2',
			imgsrc: '',
			// audioicon: true,
		}],
	},
	// slide2
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "title",
			textdata: data.string.eins
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "im-2",
					imgsrc : '',
					imgid : 'im-02'
				},
			]
		}],
		speechbox:[{
			speechbox: 'sp-2',
			textclass: "answer",
			textdata: data.string.eq_2,
			datahighlightflag: true,
			datahighlightcustomclass: 'blank-space',
			imgclass: '',
			imgid : 't-1',
			imgsrc: '',
			// audioicon: true,
		}],
	},
	// slide3
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "title",
			textdata: data.string.eins
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "im-3",
					imgsrc : '',
					imgid : 'im-03'
				},
			]
		}],
		speechbox:[{
			speechbox: 'sp-3',
			textclass: "answer",
			textdata: data.string.eq_3,
			datahighlightflag: true,
			datahighlightcustomclass: 'blank-space',
			imgclass: '',
			imgid : 't-1',
			imgsrc: '',
			// audioicon: true,
		}],
	},
	// slide4
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "title",
			textdata: data.string.eins
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "im-4",
					imgsrc : '',
					imgid : 'im-04'
				},
			]
		}],
		speechbox:[{
			speechbox: 'sp-4',
			textclass: "answer",
			textdata: data.string.eq_4,
			datahighlightflag: true,
			datahighlightcustomclass: 'blank-space',
			imgclass: 'flipped-v',
			imgid : 't-2',
			imgsrc: '',
			// audioicon: true,
		}],
	},
	// slide5
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "title",
			textdata: data.string.eins
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "im-5",
					imgsrc : '',
					imgid : 'im-05'
				},
			]
		}],
		speechbox:[{
			speechbox: 'sp-5',
			textclass: "answer",
			textdata: data.string.eq_5,
			datahighlightflag: true,
			datahighlightcustomclass: 'blank-space',
			imgclass: '',
			imgid : 't-1',
			imgsrc: '',
			// audioicon: true,
		}],
	},
	// slide6
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "title",
			textdata: data.string.eins
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "im-6",
					imgsrc : '',
					imgid : 'im-06'
				},
			]
		}],
		speechbox:[{
			speechbox: 'sp-6',
			textclass: "answer",
			textdata: data.string.eq_6,
			datahighlightflag: true,
			datahighlightcustomclass: 'blank-space',
			imgclass: '',
			imgid : 't-1',
			imgsrc: '',
			// audioicon: true,
		}],
	},
	// slide7
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "title",
			textdata: data.string.eins
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "im-7",
					imgsrc : '',
					imgid : 'im-07'
				},
			]
		}],
		speechbox:[{
			speechbox: 'sp-7',
			textclass: "answer",
			textdata: data.string.eq_7,
			datahighlightflag: true,
			datahighlightcustomclass: 'blank-space',
			imgclass: '',
			imgid : 't-1',
			imgsrc: '',
			// audioicon: true,
		}],
	},
	// slide8
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "title",
			textdata: data.string.eins
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "im-8",
					imgsrc : '',
					imgid : 'im-08'
				},
			]
		}],
		speechbox:[{
			speechbox: 'sp-8',
			textclass: "answer",
			textdata: data.string.eq_8,
			datahighlightflag: true,
			datahighlightcustomclass: 'blank-space',
			imgclass: '',
			imgid : 't-1',
			imgsrc: '',
			// audioicon: true,
		}],
	},
	// slide9
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "title",
			textdata: data.string.eins
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "im-9",
					imgsrc : '',
					imgid : 'im-09'
				},
			]
		}],
		speechbox:[{
			speechbox: 'sp-9',
			textclass: "answer",
			textdata: data.string.eq_9,
			datahighlightflag: true,
			datahighlightcustomclass: 'blank-space',
			imgclass: 'flipped-v',
			imgid : 't-2',
			imgsrc: '',
			// audioicon: true,
		}],
	},
	// slide10
	{
		contentblockadditionalclass: 'bg-main',
		extratextblock:[{
			textclass: "title",
			textdata: data.string.eins
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "im-10",
					imgsrc : '',
					imgid : 'im-10'
				},
			]
		}],
		speechbox:[{
			speechbox: 'sp-10',
			textclass: "answer",
			textdata: data.string.eq_10,
			datahighlightflag: true,
			datahighlightcustomclass: 'blank-space',
			imgclass: '',
			imgid : 't-1',
			imgsrc: '',
			// audioicon: true,
		}],
	}
];
content.shufflearray();

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;


	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	var scoring = new NumberTemplate();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "im-01", src: imgpath+"01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-02", src: imgpath+"02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-03", src: imgpath+"03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-04", src: imgpath+"04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-05", src: imgpath+"05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-06", src: imgpath+"06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-07", src: imgpath+"07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-08", src: imgpath+"08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-09", src: imgpath+"09.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-10", src: imgpath+"10.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "t-1", src: 'images/textbox/white/tl-2.png', type: createjs.AbstractLoader.IMAGE},
			{id: "t-2", src: 'images/textbox/white/l-1-b.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"exercise.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		scoring.init($total_page);
		templateCaller();
	}
	//initialize
	init();


	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	/*=====  End of data highlight function  ======*/

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templatecaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templatecaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// setTimeout(function(){
			// if(countNext == $total_page - 1)
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
			// }, 1000);
		}
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
	/**
	 How to:
	 - Just call instructionblockcontroller() from the template
	 */


	/**
	 What it does:
	 - It inserts and handles closing and opening of instruction block
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	var wrongclick = false;
	var total_count = 0;
	var no_of_incorr = 0;
	var no_of_select = 0;

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);

		// var parent = $(".option");
		// var divs = parent.children('span');
		// var copydivs = divs;
		// var rand_idx = [0,1,2];
		// rand_idx.shufflearray();
		// for (var m = 0; m<3; m++){
		// 	divs[m].before(copydivs[rand_idx[m]]);
		// }

		$prevBtn.hide(0);
		no_of_incorr = 0;
		no_of_select = 0;
		total_count = $('.blank-space').length;
		switch (countNext) {
			case 0:
				sound_nav("sound_1");
				break;
			default:

		}

		exercise_span_selector2($('.correct'),$('.incorrect'),scoring,$nextBtn);

	}
	function exercise_span_selector2(class1, class2,scoring,nextbutton){
		nextbutton.hide(0);
		class1.click(function(){
			var $current = $(this);
			no_of_select++;
			play_correct_incorrect_sound(true);
			$(this).parent().fadeOut(500, function(){
				$(".blank-space").html($current.html());
				$(".blank-space").css('display', 'inline');
				if(no_of_select == total_count){
					nextbutton.show(0);
					if(no_of_incorr == 0){
						scoring.update(true);
					}
				}
			});
		});
		class2.click(function(){
			if(!wrongclick){
				scoring.update(false);
			}
			no_of_incorr++;
			// no_of_select++;
			play_correct_incorrect_sound(false);
			$(this).addClass("notdone");
			$(this).css('pointer-events', 'none');
			wrongclick = true;
		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){

			if(countNext==0){
				$nextBtn.hide(0);
			}else{
				nav_button_controls(0);
			}
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}

	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();
		generalTemplate();
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({
				'color' : 'red',
				"height" : "4.3vmin",
				"width" : "4.3vmin",
				"cursor" : "pointer",
				"text-align" : "center"
			});
		}
		function slides(i) {
			$($('.totalsequence')[i]).click(function() {
				countNext = i;
				templateCaller();
			});
		}
		*/
		loadTimelineProgress($total_page,countNext+1);
	}

	$nextBtn.on('click', function() {
		countNext++;
		scoring.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
});

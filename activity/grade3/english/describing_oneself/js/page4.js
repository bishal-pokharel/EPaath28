var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "duckling",
					imgsrc : '',
					imgid : 'duck-1'
				},
			]
		}],
		uppertextblockadditionalclass:'diy-text sniglet',
		uppertextblock:[{
			textdata: data.string.p4text1,
			textclass: "front",
		}],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "duckling",
					imgsrc : '',
					imgid : 'duck-1'
				},
			]
		}],
		uppertextblockadditionalclass:'diy-text sniglet flip-v',
		uppertextblock:[{
			textdata: data.string.p4text1,
			textclass: "front",
		},{
			textdata: data.string.p4text2,
			textclass: "back",
		}],
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover bg-zoom-in',
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		svgblock: 'svg-div',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-1'
				},
				// {
					// imgclass : "man-face",
					// imgsrc : '',
					// imgid : 'face-1'
				// },
			]
		}],
		extratextblock:[{
			textdata: data.string.p4text4,
			textclass: "side-notification",
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p4text3,
			datahighlightflag : true,
			datahighlightcustomclass : 'select-opt',
			imgclass: '',
			textclass : '',
			imgid : 't-1',
			imgsrc: '',
			// audioicon: true,
		}],
		optiondivadditionalclass: '',
		option:[{
			imgclass : "correct cl-1",
			imgsrc : '',
			imgid : 'nose-1'
		},{
			imgclass : "incorrect cl-2",
			imgsrc : '',
			imgid : 'nose-2'
		},{
			imgclass : "incorrect cl-3",
			imgsrc : '',
			imgid : 'nose-3'
		}]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover bg-zoom-in',
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		svgblock: 'svg-div',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-1'
				},
				// {
					// imgclass : "man-face",
					// imgsrc : '',
					// imgid : 'face-2'
				// },
			]
		}],
		extratextblock:[{
			textdata: data.string.p4text7,
			textclass: "side-notification",
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p4text6,
			datahighlightflag : true,
			datahighlightcustomclass : 'select-opt',
			imgclass: '',
			textclass : '',
			imgid : 't-1',
			imgsrc: '',
			// audioicon: true,
		}],
		optiondivadditionalclass: 'eyes-div',
		option:[{
			imgclass : "incorrect cl-1",
			imgsrc : '',
			imgid : 'eye-1'
		},{
			imgclass : "incorrect cl-2",
			imgsrc : '',
			imgid : 'eye-2'
		},{
			imgclass : "correct cl-3",
			imgsrc : '',
			imgid : 'eye-3'
		}]
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover bg-zoom-in',
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		svgblock: 'svg-div',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-1'
				},
				// {
					// imgclass : "man-face",
					// imgsrc : '',
					// imgid : 'face-3'
				// },
			]
		}],
		extratextblock:[{
			textdata: data.string.p4text10,
			textclass: "side-notification",
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p4text9,
			datahighlightflag : true,
			datahighlightcustomclass : 'select-opt',
			imgclass: '',
			textclass : '',
			imgid : 't-1',
			imgsrc: '',
			// audioicon: true,
		}],
		optiondivadditionalclass: 'hair-div',
		option:[{
			imgclass : "incorrect cl-1",
			imgsrc : '',
			imgid : 'hair-1'
		},{
			imgclass : "correct cl-2",
			imgsrc : '',
			imgid : 'hair-2'
		},{
			imgclass : "incorrect cl-3",
			imgsrc : '',
			imgid : 'hair-3'
		}]
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover bg-zoom-in',
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		svgblock: 'svg-div2',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-1'
				},
				// {
					// imgclass : "man-body",
					// imgsrc : '',
					// imgid : 'body-1'
				// },
				{
					imgclass : "curtain",
					imgsrc : '',
					imgid : 'curtain'
				},
			]
		}],
		extratextblock:[{
			textdata: data.string.p4text13,
			textclass: "side-notification",
		},{
			textdata: '',
			textclass: "curtain-bar",
		}],
		speechbox:[{
			speechbox: 'sp-2',
			textdata : data.string.p4text12,
			datahighlightflag : true,
			datahighlightcustomclass : 'select-opt',
			imgclass: '',
			textclass : '',
			imgid : 't-1',
			imgsrc: '',
			// audioicon: true,
		}],
		optiondivadditionalclass: '',
		option:[{
			imgclass : "correct cl-1",
			imgsrc : '',
			imgid : 'cloth-1'
		},{
			imgclass : "incorrect cl-2",
			imgsrc : '',
			imgid : 'cloth-2'
		},{
			imgclass : "incorrect cl-3",
			imgsrc : '',
			imgid : 'cloth-3'
		}]
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		svgblock: 'svg-div3',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "star star-1",
					imgsrc : '',
					imgid : 'star'
				},
				{
					imgclass : "star star-2",
					imgsrc : '',
					imgid : 'star'
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-3',
			textdata : data.string.p4text15,
			imgclass: '',
			textclass : '',
			imgid : 't-1',
			imgsrc: '',
			// audioicon: true,
		}],
	},
	// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		svgblock: 'svg-div4',
		speechbox:[{
			speechbox: ' spa-0',
			textdata : data.string.p4text16,
			imgclass: '',
			textclass : '',
			imgid : 't-1',
			imgsrc: '',
			// audioicon: true,
		},{
			speechbox: ' spa-1',
			textdata : data.string.p4text3,
			datahighlightflag : true,
			datahighlightcustomclass : 'select-opt1',
			imgclass: 'flipped-h',
			textclass : '',
			imgid : 'tb-2',
			imgsrc: '',
			// audioicon: true,
		},{
			speechbox: ' spa-2',
			textdata : data.string.p4text6,
			datahighlightflag : true,
			datahighlightcustomclass : 'select-opt2',
			imgclass: '',
			textclass : '',
			imgid : 'tb-2',
			imgsrc: '',
			// audioicon: true,
		},{
			speechbox: ' spa-3',
			textdata : data.string.p4text9,
			datahighlightflag : true,
			datahighlightcustomclass : 'select-opt3',
			imgclass: '',
			textclass : 'offset-top',
			imgid : 'tb-1',
			imgsrc: '',
			// audioicon: true,
		},{
			speechbox: ' spa-4',
			textdata : data.string.p4text12,
			datahighlightflag : true,
			datahighlightcustomclass : 'select-opt4',
			imgclass: 'flipped-h',
			textclass : 'offset-top',
			imgid : 'tb-1',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-1'
				},
			]
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;

	var cap, eyes1, eyes2, eyes3, hand, nose1, nose2, nose3, hair;

	var hairarr = [ 'brown', 'black', 'gray'];
	var haircolors = ['#A57555', '#1C1916', '#BAB09D'];

	var eye = [ 'green', 'brown', 'black'];
	var eyecolors = ['#43895C', '#8C6239', '#190D05'];

	var nose = ['sharp', 'round', 'crooked'];
	//array to update nose initialize in inti_svg function as well
	var nosecolors = [nose1, nose2, nose3];

	var shirt = ['green', 'yellow', 'pink'];
	var shirt1, shirt2, shirt3;
	var shirtcolor = ['#65A845', '#FFC905', '#FF54CB'];

	var selectors = [0,0,0,0];
	for(var i=0; i<4; i++){
		selectors[i]= Math.floor(Math.random()*3);
	}

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "mface", src: imgpath+"diy/face_new.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "mbody", src: imgpath+"diy/face_new2.svg", type: createjs.AbstractLoader.IMAGE},

			{id: "bg-1", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "duck-1", src: imgpath+"duckling-20.png", type: createjs.AbstractLoader.IMAGE},
			{id: "duck-2", src: imgpath+"duckling-20.png", type: createjs.AbstractLoader.IMAGE},

			{id: "face-1", src: imgpath+"diy/nose_main.png", type: createjs.AbstractLoader.IMAGE},
			{id: "face-2", src: imgpath+"diy/closeeye.png", type: createjs.AbstractLoader.IMAGE},
			{id: "face-3", src: imgpath+"diy/openeye.png", type: createjs.AbstractLoader.IMAGE},
			{id: "face-4", src: imgpath+"diy/withhair.png", type: createjs.AbstractLoader.IMAGE},

			{id: "nose-1", src: imgpath+"diy/nose03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "nose-2", src: imgpath+"diy/nose01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "nose-3", src: imgpath+"diy/nose02.png", type: createjs.AbstractLoader.IMAGE},

			{id: "eye-1", src: imgpath+"diy/eye01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eye-2", src: imgpath+"diy/eye02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eye-3", src: imgpath+"diy/eye03.png", type: createjs.AbstractLoader.IMAGE},

			{id: "hair-1", src: imgpath+"diy/hair02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hair-2", src: imgpath+"diy/hair01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hair-3", src: imgpath+"diy/hair03.png", type: createjs.AbstractLoader.IMAGE},

			{id: "body-1", src: imgpath+"diy/withouttshirt.png", type: createjs.AbstractLoader.IMAGE},
			{id: "body-2", src: imgpath+"diy/withtshirt.png", type: createjs.AbstractLoader.IMAGE},
			{id: "body-3", src: imgpath+"diy/happy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "curtain", src: imgpath+"diy/curtain.png", type: createjs.AbstractLoader.IMAGE},

			{id: "cloth-1", src: imgpath+"diy/tshirt03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloth-2", src: imgpath+"diy/tshirt02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloth-3", src: imgpath+"diy/tshirt01.png", type: createjs.AbstractLoader.IMAGE},

			{id: "star", src:  imgpath+"diy/star.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "right", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "wrong", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "t-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_curtain", src: soundAsset+"curtain-2.ogg"},
			{id: "sound_1", src: soundAsset+"s4_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s4_p3.ogg"},
			{id: "sound_3", src: soundAsset+"p1/3.ogg"},
			{id: "sound_4", src: soundAsset+"p1/4.ogg"},
			{id: "sound_7", src: soundAsset+"s4_p7.ogg"},
			{id: "correct", src: soundAsset+"correct.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_option_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext) {
			case 0:
				 $nextBtn.show(0);
				play_diy_audio();
				break;
				case 1:
				setTimeout(function(){
					sound_nav("sound_1");
				},1000);

				break;
			case 2:
			setTimeout(function(){
				sound_nav("sound_2");
			},1000);
				$prevBtn.show(0);
				$('.select-opt').html(nose[selectors[0]]);
				init_svg(new_func1);
				function new_func1(){
					eyes1.attr('display', 'block');
					hand.attr('display', 'block');
					cap.attr('display', 'block');
					nosecolors[selectors[0]].attr('display', 'block');
				}
				$('.choice-img').click(function(){
					$('.side-notification').fadeOut(500);
					if($(this).hasClass('cl-'+parseInt(selectors[countNext-2]+1))){
						$('.choice-img').css('pointer-events', 'none');
						play_correct_incorrect_sound(1);
						$(this).siblings('.right_wrong').attr('src', preload.getResult('right').src);
						$('.sp-1>p').fadeOut(500, function(){
							hand.attr('display', 'none');
							$('.sp-1>p').html(data.string.p4text5);
							$('.sp-1>p').fadeIn(500, function(){
								nav_button_controls(100);
							});
						});
					} else{
						$(this).css('pointer-events', 'none');
						play_correct_incorrect_sound(0);
						$(this).siblings('.right_wrong').attr('src', preload.getResult('wrong').src);
					}
				});
				break;
			case 3:
				$prevBtn.show(0);
				$('.select-opt').html(eye[selectors[1]]);
				init_svg(new_func2);
				function new_func2(){
					eyes2.attr('display', 'block');
					cap.attr('display', 'block');
					nosecolors[selectors[0]].attr('display', 'block');
					eyes3.selectAll('.eyeball').forEach(function(e){
						e.attr('fill', eyecolors[selectors[1]]);
					});
				}
				$('.choice-img').click(function(){
					$('.side-notification').fadeOut(500);
					if($(this).hasClass('cl-'+parseInt(selectors[countNext-2]+1))){
						$('.choice-img').css('pointer-events', 'none');
						play_correct_incorrect_sound(1);
						$(this).siblings('.right_wrong').attr('src', preload.getResult('right').src);
						$('.sp-1>p').fadeOut(500, function(){
							eyes2.attr('display', 'none');
							eyes3.attr('display', 'block');
							$('.sp-1>p').html(data.string.p4text8);
							$('.sp-1>p').fadeIn(500, function(){
								nav_button_controls(100);
							});
						});
					} else{
						$(this).css('pointer-events', 'none');
						play_correct_incorrect_sound(0);
						$(this).siblings('.right_wrong').attr('src', preload.getResult('wrong').src);
					}
				});
				break;
			case 4:
				$prevBtn.show(0);
				$('.select-opt').html(hairarr[selectors[2]]);
				init_svg(new_func3);
				function new_func3(){
					eyes3.attr('display', 'block');
					cap.attr('display', 'block');
					nosecolors[selectors[0]].attr('display', 'block');
					eyes3.selectAll('.eyeball').forEach(function(e){
						e.attr('fill', eyecolors[selectors[1]]);
					});
					hair.attr('fill', haircolors[selectors[2]]);
				}
				$('.choice-img').click(function(){
					$('.side-notification').fadeOut(500);
					if($(this).hasClass('cl-'+parseInt(selectors[countNext-2]+1))){
						$('.choice-img').css('pointer-events', 'none');
						play_correct_incorrect_sound(1);
						$(this).siblings('.right_wrong').attr('src', preload.getResult('right').src);
						$('.sp-1>p').fadeOut(500, function(){
							hair.attr('display', 'block');
							cap.attr('display', 'none');
							$('.sp-1>p').html(data.string.p4text11);
							$('.sp-1>p').fadeIn(500, function(){
								nav_button_controls(100);
							});
						});
					} else{
						$(this).css('pointer-events', 'none');
						play_correct_incorrect_sound(0);
						$(this).siblings('.right_wrong').attr('src', preload.getResult('wrong').src);
					}
				});
				break;
			case 5:
				$prevBtn.show(0);
				$('.select-opt').html(shirt[selectors[3]]);
				var s = Snap("#svg-div2");
				var svg = Snap.load(preload.getResult('mbody').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					//to resive whole svg
					Snap.select('svg').attr({
						width: '100%',
						height: '100%',
					});
					cap = Snap.select("#cap");
					eyes1 = Snap.select("#eye-happy");
					eyes2 = Snap.select("#eye-close");
					eyes3 = Snap.select("#eye-main");
					hand = Snap.select("#hand");
					nose1 = Snap.select("#nose-sharp");
					nose2 = Snap.select("#nose-round");
					nose3 = Snap.select("#nose-flat");
					shirt1 = Snap.select("#green-shirt");
					shirt2 = Snap.select("#yellow-shirt");
					shirt3 = Snap.select("#pink-shirt");
					hair = Snap.select("#hair");
					nosecolors = [nose1, nose2, nose3];
					var shirtsarr = [shirt1, shirt2, shirt3];
					shirtsarr[selectors[3]].attr('display', 'block');
					eyes3.attr('display', 'block');
					hair.attr('display', 'block');
					nosecolors[selectors[0]].attr('display', 'block');
					eyes3.selectAll('.eyeball').forEach(function(e){
						e.attr('fill', eyecolors[selectors[1]]);
					});
					hair.attr('fill', haircolors[selectors[2]]);
				} );
				$('.choice-img').click(function(){
					$('.side-notification').fadeOut(500);
					if($(this).hasClass('cl-'+parseInt(selectors[countNext-2]+1))){
						$('.choice-img').css('pointer-events', 'none');
						play_correct_incorrect_sound(1);
						timeoutvar = setTimeout(function(){
							createjs.Sound.play('sound_curtain');
							nav_button_controls(500);
						}, 500);
						$(this).siblings('.right_wrong').attr('src', preload.getResult('right').src);
						$('.sp-2>p').fadeOut(500, function(){
							$('.curtain').animate({
								'width': '5%'
							}, 1000);
							$('.sp-2>p').html(data.string.p4text14);
							$('.sp-2>p').fadeIn(500, function(){
							});
						});
					} else{
						$(this).css('pointer-events', 'none');
						play_correct_incorrect_sound(0);
						$(this).siblings('.right_wrong').attr('src', preload.getResult('wrong').src);
					}
				});
				break;
			case 6:
				var s = Snap("#svg-div3");
				var svg = Snap.load(preload.getResult('mbody').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					//to resive whole svg
					Snap.select('svg').attr({
						width: '100%',
						height: '100%',
					});
					cap = Snap.select("#cap");
					eyes1 = Snap.select("#eye-happy");
					eyes2 = Snap.select("#eye-close");
					eyes3 = Snap.select("#eye-main");
					hand = Snap.select("#hand");
					nose1 = Snap.select("#nose-sharp");
					nose2 = Snap.select("#nose-round");
					nose3 = Snap.select("#nose-flat");
					shirt1 = Snap.select("#green-shirt");
					shirt2 = Snap.select("#yellow-shirt");
					shirt3 = Snap.select("#pink-shirt");
					hair = Snap.select("#hair");
					nosecolors = [nose1, nose2, nose3];
					var shirtsarr = [shirt1, shirt2, shirt3];
					shirtsarr[selectors[3]].attr('display', 'block');
					eyes3.attr('display', 'block');
					hair.attr('display', 'block');
					nosecolors[selectors[0]].attr('display', 'block');
					eyes3.selectAll('.eyeball').forEach(function(e){
						e.attr('fill', eyecolors[selectors[1]]);
					});
					hair.attr('fill', haircolors[selectors[2]]);
				} );
				$('.sp-3').fadeIn(1000, function(){
					sound_play_click('sound_7', '.sp-3');
				});
				break;
			case 7:
				$('.select-opt1').html(nose[selectors[0]]);
				$('.select-opt2').html(eye[selectors[1]]);
				$('.select-opt3').html(hairarr[selectors[2]]);
				$('.select-opt4').html(shirt[selectors[3]]);
				var s = Snap("#svg-div4");
				var svg = Snap.load(preload.getResult('mbody').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					//to resive whole svg
					Snap.select('svg').attr({
						width: '100%',
						height: '100%',
					});
					cap = Snap.select("#cap");
					eyes1 = Snap.select("#eye-happy");
					eyes2 = Snap.select("#eye-close");
					eyes3 = Snap.select("#eye-main");
					hand = Snap.select("#hand");
					nose1 = Snap.select("#nose-sharp");
					nose2 = Snap.select("#nose-round");
					nose3 = Snap.select("#nose-flat");
					var mouth = Snap.select("#mouth");
					var mouth2 = Snap.select("#mouth-2");
					var shirt4 = Snap.select("#shirt-2");
					var shirtcolor2 = Snap.select("#out-shirt");
					hair = Snap.select("#hair");
					shirtcolor2.attr('fill', shirtcolor[selectors[3]]);
					nosecolors = [nose1, nose2, nose3];
					mouth.attr('display', 'none');
					mouth2.attr('display', 'block');
					shirt4.attr('display', 'block');
					eyes3.attr('display', 'block');
					hair.attr('display', 'block');
					nosecolors[selectors[0]].attr('display', 'block');
					eyes3.selectAll('.eyeball').forEach(function(e){
						e.attr('fill', eyecolors[selectors[1]]);
					});
					hair.attr('fill', haircolors[selectors[2]]);
				} );

			createjs.Sound.stop();
			current_sound = createjs.Sound.play("correct");
			current_sound.play();
			$(".spa-0").show(0);
			current_sound.on("complete", function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("correct");
			current_sound.play();
			$(".spa-1").show(0);
			current_sound.on("complete", function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("correct");
			current_sound.play();
			$(".spa-2").show(0);
			current_sound.on("complete", function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("correct");
			current_sound.play();
			$(".spa-3").show(0);
			current_sound.on("complete", function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("correct");
			current_sound.play();
			$(".spa-4").show(0);
			current_sound.on("complete", function(){
				nav_button_controls(300);
				});
			});
		});
	});
});
				break;
			default:
				$prevBtn.show(0);
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function init_svg( func ){
		var s = Snap("#svg-div");
		var svg = Snap.load(preload.getResult('mface').src, function ( loadedFragment ) {
			s.append(loadedFragment);
			//to resive whole svg
			Snap.select('svg').attr({
				width: '100%',
				height: '100%',
			});
			cap = Snap.select("#cap");
			eyes1 = Snap.select("#eye-happy");
			eyes2 = Snap.select("#eye-close");
			eyes3 = Snap.select("#eye-main");
			hand = Snap.select("#hand");
			nose1 = Snap.select("#nose-sharp");
			nose2 = Snap.select("#nose-round");
			nose3 = Snap.select("#nose-flat");
			hair = Snap.select("#hair");
			nosecolors = [nose1, nose2, nose3];
			func();
		} );
		return svg;
	}
	function sound_play_click(sound_id, click_class){
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(0);

		});
	}

		function sound_nav(sound_id){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
			current_sound.on("complete", function(){

				if(countNext==2){
					$nextBtn.hide(0);
				}else {
									nav_button_controls(0);
				}
			});
		}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_option_image(content, count){
		if(content[count].hasOwnProperty('option')){
			var imageClass = content[count].option;
			for(var i=0; i<imageClass.length; i++){
				var image_src = preload.getResult(imageClass[i].imgid).src;
				//get list of classes
				var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

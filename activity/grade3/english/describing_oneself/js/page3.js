var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "main-background",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "boy1",
					imgsrc : '',
					imgid : 'boy1-1'
				},
				{
					imgclass : "boy2",
					imgsrc : '',
					imgid : 'boy2-1'
				},
				{
					imgclass : "dad",
					imgsrc : '',
					imgid : 'dad-1'
				},
				{
					imgclass : "mom",
					imgsrc : '',
					imgid : 'mom-1'
				}
			]
		}],
		uppertextblockadditionalclass:'board-text my_font_medium chelseamarket',
		uppertextblock:[{
			textdata: data.string.p3text1,
			textclass: "",
		}],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover bg-zoom-in-anim',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "main-background",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "boy1",
					imgsrc : '',
					imgid : 'boy1-1'
				},
				{
					imgclass : "boy2",
					imgsrc : '',
					imgid : 'boy2-2'
				},
				{
					imgclass : "dad",
					imgsrc : '',
					imgid : 'dad-1'
				},
				{
					imgclass : "mom",
					imgsrc : '',
					imgid : 'mom-1'
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-1 its_hidden',
			textdata : data.string.p3text2,
			imgclass: '',
			textclass : '',
			imgid : 'tl-1',
			imgsrc: '',
			// audioicon: true,
		}],
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover bg-zoom-in',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "main-background",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "boy1",
					imgsrc : '',
					imgid : 'boy1-2'
				},
				{
					imgclass : "boy2",
					imgsrc : '',
					imgid : 'boy2-1'
				},
				{
					imgclass : "dad",
					imgsrc : '',
					imgid : 'dad-1'
				},
				{
					imgclass : "mom",
					imgsrc : '',
					imgid : 'mom-1'
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-2 its_hidden',
			textdata : data.string.p3text3,
			imgclass: 'flipped-h',
			textclass : '',
			imgid : 'tl-2',
			imgsrc: '',
			// audioicon: true,
		}],
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover bg-zoom-in-anim',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "main-background",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "boy1",
					imgsrc : '',
					imgid : 'boy1-1'
				},
				{
					imgclass : "boy2",
					imgsrc : '',
					imgid : 'boy2-2'
				},
				{
					imgclass : "dad",
					imgsrc : '',
					imgid : 'dad-1'
				},
				{
					imgclass : "mom",
					imgsrc : '',
					imgid : 'mom-1'
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-1 its_hidden',
			textdata : data.string.p3text4,
			imgclass: '',
			textclass : '',
			imgid : 'tl-1',
			imgsrc: '',
			// audioicon: true,
		}],
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover bg-zoom-in',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "main-background",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "boy1",
					imgsrc : '',
					imgid : 'boy1-2'
				},
				{
					imgclass : "boy2",
					imgsrc : '',
					imgid : 'boy2-1'
				},
				{
					imgclass : "dad",
					imgsrc : '',
					imgid : 'dad-1'
				},
				{
					imgclass : "mom",
					imgsrc : '',
					imgid : 'mom-1'
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-2 its_hidden',
			textdata : data.string.p3text5,
			imgclass: 'flipped-h',
			textclass : '',
			imgid : 'tl-2',
			imgsrc: '',
			// audioicon: true,
		}],
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover bg-zoom-in',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "main-background",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "boy1",
					imgsrc : '',
					imgid : 'boy1-2'
				},
				{
					imgclass : "boy2",
					imgsrc : '',
					imgid : 'boy2-1'
				},
				{
					imgclass : "dad",
					imgsrc : '',
					imgid : 'dad-1'
				},
				{
					imgclass : "mom",
					imgsrc : '',
					imgid : 'mom-1'
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-3 its_hidden',
			textdata : data.string.p3text6,
			imgclass: 'flipped-h',
			textclass : '',
			imgid : 'tl-2',
			imgsrc: '',
			// audioicon: true,
		}],
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover bg-zoom-in-anim',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "main-background",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "boy1",
					imgsrc : '',
					imgid : 'boy1-1'
				},
				{
					imgclass : "boy2",
					imgsrc : '',
					imgid : 'boy2-2'
				},
				{
					imgclass : "dad",
					imgsrc : '',
					imgid : 'dad-1'
				},
				{
					imgclass : "mom",
					imgsrc : '',
					imgid : 'mom-1'
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-1 its_hidden',
			textdata : data.string.p3text7,
			imgclass: '',
			textclass : '',
			imgid : 'tl-1',
			imgsrc: '',
			// audioicon: true,
		}],
	},
	// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover bg-zoom-in',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "main-background",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "boy1",
					imgsrc : '',
					imgid : 'boy1-2'
				},
				{
					imgclass : "boy2",
					imgsrc : '',
					imgid : 'boy2-1'
				},
				{
					imgclass : "dad",
					imgsrc : '',
					imgid : 'dad-1'
				},
				{
					imgclass : "mom",
					imgsrc : '',
					imgid : 'mom-1'
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-2 its_hidden',
			textdata : data.string.p3text8,
			imgclass: 'flipped-h',
			textclass : '',
			imgid : 'tl-2',
			imgsrc: '',
			// audioicon: true,
		}],
	},
	// slide8
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover bg-zoom-in-anim',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "main-background",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "boy1",
					imgsrc : '',
					imgid : 'boy1-1'
				},
				{
					imgclass : "boy2",
					imgsrc : '',
					imgid : 'boy2-2'
				},
				{
					imgclass : "dad",
					imgsrc : '',
					imgid : 'dad-1'
				},
				{
					imgclass : "mom",
					imgsrc : '',
					imgid : 'mom-1'
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-5 its_hidden',
			textdata : data.string.p3text9,
			imgclass: '',
			textclass : '',
			imgid : 'tl-1',
			imgsrc: '',
			// audioicon: true,
		}],
	},
	// slide9
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover bg-zoom-in',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "main-background",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "boy1",
					imgsrc : '',
					imgid : 'boy1-2'
				},
				{
					imgclass : "boy2",
					imgsrc : '',
					imgid : 'boy2-1'
				},
				{
					imgclass : "dad",
					imgsrc : '',
					imgid : 'dad-1'
				},
				{
					imgclass : "mom",
					imgsrc : '',
					imgid : 'mom-1'
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-2 its_hidden',
			textdata : data.string.p3text10,
			imgclass: 'flipped-h',
			textclass : '',
			imgid : 'tl-2',
			imgsrc: '',
			// audioicon: true,
		}],
	},
	// slide10
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover bg-zoom-in-anim',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "main-background",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "boy1",
					imgsrc : '',
					imgid : 'boy1-1'
				},
				{
					imgclass : "boy2",
					imgsrc : '',
					imgid : 'boy2-2'
				},
				{
					imgclass : "dad",
					imgsrc : '',
					imgid : 'dad-1'
				},
				{
					imgclass : "mom",
					imgsrc : '',
					imgid : 'mom-1'
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-4 its_hidden',
			textdata : data.string.p3text11,
			imgclass: '',
			textclass : '',
			imgid : 'tl-1',
			imgsrc: '',
			// audioicon: true,
		}],
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			{id: "data-xml", src: "data.xml", type: createjs.AbstractLoader.XML},
			//images
			{id: "bg-1", src: imgpath+"schoolbg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy2-1", src: imgpath+"pream01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy2-2", src: imgpath+"pream02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy1-1", src: imgpath+"suraj02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy1-2", src: imgpath+"suraj01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy1-3", src: imgpath+"suraj03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mom-1", src: imgpath+"woman.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dad-1", src: imgpath+"man.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tl-2", src: 'images/textbox/white/tl-3.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tl-1", src: 'images/textbox/white/tl-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"s3_p1.ogg"},
			{id: "sound_2", src: soundAsset+"s3_p2.ogg"},
			{id: "sound_3", src: soundAsset+"s3_p3.ogg"},
			{id: "sound_4", src: soundAsset+"s3_p4.ogg"},
			{id: "sound_5", src: soundAsset+"s3_p5.ogg"},
			{id: "sound_6", src: soundAsset+"s3_p6.ogg"},
			{id: "sound_7", src: soundAsset+"s3_p7.ogg"},
			{id: "sound_8", src: soundAsset+"s3_p8.ogg"},
			{id: "sound_9", src: soundAsset+"s3_p9.ogg"},
			{id: "sound_10", src: soundAsset+"s3_p10.ogg"},
			{id: "sound_11", src: soundAsset+"s3_p11.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext) {
			case 0:
						sound_nav('sound_1');
				break;
			case 1:
				$('.sp-1').fadeIn(1000, function(){
					sound_play_click('sound_2', '.sp-1');
				});
				break;
			case 2:
				$('.sp-2').fadeIn(1000, function(){
					sound_play_click('sound_3', '.sp-2');
				});
				break;
			case 3:
				$('.sp-1').fadeIn(1000, function(){
					sound_play_click('sound_4', '.sp-1');
				});
				break;
			case 4:
				$('.sp-2').fadeIn(1000, function(){
					sound_play_click('sound_5', '.sp-2');
				});
				break;
			case 5:
				$('.sp-3').fadeIn(1000, function(){
					sound_play_click('sound_6', '.sp-3');
				});
				break;
			case 6:
				$('.sp-1').fadeIn(1000, function(){
					sound_play_click('sound_7', '.sp-1');
				});
				break;
			case 7:
				$('.sp-2').fadeIn(1000, function(){
					sound_play_click('sound_8', '.sp-2');
				});
				break;
			case 8:
				$('.sp-5').fadeIn(1000, function(){
					sound_play_click('sound_9', '.sp-5');
				});
				break;
			case 9:
				$('.sp-2').fadeIn(1000, function(){
					sound_play_click('sound_10', '.sp-2');
				});
				break;
			case 10:
				$('.sp-4').fadeIn(1000, function(){
					sound_play_click('sound_11', '.sp-4');
				});
				break;
			default:
				nav_button_controls(0);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}
	function sound_play_click(sound_id, click_class){
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				// console.log(selector);
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

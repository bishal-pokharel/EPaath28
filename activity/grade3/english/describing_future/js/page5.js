var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg1",
		uppertextblock:[
		{
			textclass: "uptext-1",
			textdata: data.string.p5text1
		},
		{
			textclass: "uptext",
			textdata: data.string.p5text2
		},
		{
			textclass: "uptext-2",
			textdata: data.string.p5text3
		}
	],
	audiobox:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "boldit",
			audiocontaineradditionalclass: "audio1 playable",
			audiotext: data.string.p5text4
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "boldit",
			audiocontaineradditionalclass: "audio2 playable",
			audiotext: data.string.p5text5
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "boldit",
			audiocontaineradditionalclass: "audio3 playable",
			audiotext: data.string.p5text6
		}
	]
},
// slide1
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg1",
	uppertextblock:[
	{
		textclass: "uptext-1",
		textdata: data.string.p5text1
	},
	{
		textclass: "uptext",
		textdata: data.string.p5text7
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "crossit",
		datahighlightcustomclass2: "showit",
		textclass: "uptext-2",
		textdata: data.string.p5text8
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "crossing",
		datahighlightcustomclass2: "showing",
		textclass: "uptext-3 delayDisplay_15",
		textdata: data.string.p5text9
	}
]
},
// slide1
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg1",
	uppertextblock:[
	{
		textclass: "uptext-1",
		textdata: data.string.p5text1
	},
	{
		textclass: "uptext",
		textdata: data.string.p5text7
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "crossit",
		datahighlightcustomclass2: "showit",
		textclass: "uptext-2",
		textdata: data.string.p5text18
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass2: "showing",
		textclass: "uptext-3 delayDisplay_3",
		textdata: data.string.p5text19
	}
]
},
// slide1
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg1",
	uppertextblock:[
	{
		textclass: "uptext-1",
		textdata: data.string.p5text1
	},
	{
		textclass: "uptext",
		textdata: data.string.p5text7
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "crossit",
		datahighlightcustomclass2: "showit",
		textclass: "uptext-2",
		textdata: data.string.p5text20
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "crossing",
		datahighlightcustomclass2: "showing",
		textclass: "uptext-3 delayDisplay_5",
		textdata: data.string.p5text21
	}
]
},
// slide2
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg1",
	uppertextblock:[
	{
		textclass: "uptext-1",
		textdata: data.string.p5text1
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "boldit",
		textclass: "uptext-3",
		textdata: data.string.p5text22
	}
]
},
// slide5
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg1",
	uppertextblock:[
	{
		textclass: "uptext-1",
		textdata: data.string.p5text1
	},
	{
		textclass: "uptext",
		textdata: data.string.p5text11
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "boldit",
		textclass: "uptext-3",
		textdata: data.string.p5text13
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "crossing",
		datahighlightcustomclass2: "showing",
		textclass: "uptext-2 delayDisplay_13",
		textdata: data.string.p5text14
	}
]
},
// slide6
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg1",
	uppertextblock:[
	{
		textclass: "uptext-1",
		textdata: data.string.p5text1
	},
	{
		textclass: "uptext",
		textdata: data.string.p5text11
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "boldit",
		textclass: "uptext-3",
		textdata: data.string.p5text16
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "crossing",
		datahighlightcustomclass2: "showing",
		textclass: "uptext-2 delayDisplay_15",
		textdata: data.string.p5text17
	}
]
}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();


	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "girl1", src: imgpath+"girl01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl2", src: imgpath+"girl02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl3", src: imgpath+"girl03.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"p5_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p5_s1.ogg"},
			{id: "sound_3", src: soundAsset+"p5_s2.ogg"},
			{id: "sound_4", src: soundAsset+"p5_s3.ogg"},
			{id: "sound_5", src: soundAsset+"p5_s4.ogg"},
			{id: "sound_6", src: soundAsset+"p5_s5.ogg"},
			{id: "sound_7", src: soundAsset+"p5_s6.ogg"},
			{id: "swim", src: soundAsset+"p5_iswim.ogg"},
			{id: "ate", src: soundAsset+"p5_sarita.ogg"},
			{id: "holi", src: soundAsset+"p5_holi.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightstarttag2;
			var texthighlightstarttag3;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
						$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

						$(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
						(stylerulename2 = "parsedstring2") ;

						$(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
						(stylerulename3 = "parsedstring3") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
					texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

        /*delay the content according to time of audio */
        var delayDisplayElement = $('[class*="delayDisplay"]');
        delayDisplayElement?displayWithLag(delayDisplayElement):'';

        put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext){
			case 0:
			sound_player_duo2("sound_1", "swim", "ate", "holi");
			$(".audiocontainer").click(function(){
				if($(this).hasClass("audio1") == true){
					sound_player_box("swim", $(this));
				}
				else if($(this).hasClass("audio2") == true){
					sound_player_box("ate", $(this));
				}
				else {
					sound_player_box("holi", $(this));
				}
			});
			break;
			case 1:
			sound_player("sound_2",3000);
			break;
			case 2:
			sound_player("sound_3",3000);
			break;
			case 3:
			sound_player("sound_4",5000);
			break;
			case 4:
			sound_player("sound_5",0);
			break;
			case 5:
			sound_player("sound_6",7000);
			break;
			case 6:
			sound_player("sound_7",12000);
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,delayDisplayElement){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			setTimeout(function(){
                navigationcontroller();
			},delayDisplayElement);
		});
	}

	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2,0);
		});

	}

	function sound_player_box(sound_id, dialogbox){
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		$(".audiocontainer").removeClass("playable");
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".audiocontainer").addClass("playable");
			navigationcontroller();
		});
	}

	function sound_player_duo2(sound_id, sound_id_2, sound_id_3, sound_id_4){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();
		current_sound.on('complete', function(){
			current_sound = createjs.Sound.play(sound_id_2);
			current_sound.play();
			current_sound.on('complete', function(){
				current_sound = createjs.Sound.play(sound_id_3);
				current_sound.play();
				current_sound.on('complete', function(){
					sound_player(sound_id_4,0);
				});
			});
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	function displayWithLag(content)	{
        content.hide(0);
        var className = content.attr("class");
		var delaySec = className?className.toString().split("_")[1]:0;
		setTimeout(function(){
            content.find("span[class='showing']").hide(0);
            content.show(0);
            content.find("span[class='showing']").each(function(index){
                var crossingContent=content.find("span[class='crossing']").eq(index);
                var showingContent=content.find("span[class='showing']").eq(index);
                if(content.find("span[class='showing']").length>1) {
                    setTimeout(function () {
                        animateOneAfterAnother(crossingContent, showingContent);
                    }, (index + 1) * 2000);
                }
                else{
                    animateOneAfterAnother(crossingContent, showingContent);
                }
            });
        },1000*delaySec);
	}

	function animateOneAfterAnother(crossingContent,showingContent){
        setTimeout(function () {
            crossingContent.removeClass("crossing").attr("class", "crossit").show(0);
            setTimeout(function () {
               showingContent.removeClass("showing").attr("class", "showit").show(0);
            }, 2000);
        }, 500);
	}

});

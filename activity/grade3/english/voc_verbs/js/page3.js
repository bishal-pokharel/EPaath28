var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"emptydiv",
            },
            {
                textdiv:"emptydiv1",
            },
            {
                textdiv:"clicktext blinkEffect",
                textclass: "content1 centertext",
                textdata: data.string.click
            },
            {
                textdiv:"question",
                textclass: "content centertext",
                textdata: data.string.diyques
            },
            {
                textdiv:"option opt1",
                textclass: "content3 centertext",
                textdata: data.string.swim
            },
            {
                textdiv:"option opt2",
                textclass: "content3 centertext",
                textdata: data.string.cook
            },
            {
                textdiv:"option opt3",
                textclass: "content3 centertext",
                textdata: data.string.read
            },
            {
                textdiv:"option opt4",
                textclass: "content3 centertext",
                textdata: data.string.sing
            },
            {
                textdiv:"option opt5",
                textclass: "content3 centertext",
                textdata: data.string.wake
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "piechart",
                    imgclass: "relativecls img1",
                    imgid: 'piechart1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow",
                    imgclass: "relativecls img2",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow1",
                    imgclass: "relativecls img3",
                    imgid: 'arrowImg1',
                    imgsrc: ""
                }
            ]
        }]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn = $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var setTime;
    var $total_page = 5;
    loadTimelineProgress($total_page, countNext + 1);
    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "piechart1Img", src: imgpath + "diy_wheel.png", type: createjs.AbstractLoader.IMAGE},
            {id: "arrowImg", src: imgpath + "orange_down_arrow.png", type: createjs.AbstractLoader.IMAGE},
            {id: "arrowImg1", src: imgpath + "arrow_new.png", type: createjs.AbstractLoader.IMAGE},
            {id: "flowerImg", src: imgpath + "talking_sun02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialImg", src: imgpath + "bubble02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "arrow1Img", src: imgpath + "blue_arrow03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "option1Img", src: imgpath + "new_plant_grow.png", type: createjs.AbstractLoader.IMAGE},
            {id: "option2Img", src: imgpath + "no_plant_grow.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset + "actionverb.ogg"},
            {id: "spinning_wheel", src: soundAsset + "spinning_wheel.ogg"},
            // {id: "sound_1", src: soundAsset + "p1_s1.ogg"},
            // {id: "sound_2", src: soundAsset + "p1_s2.ogg"},
            // {id: "sound_3", src: soundAsset + "p1_s3.ogg"},
            // {id: "sound_4", src: soundAsset + "p1_s4.ogg"},
            // {id: "sound_5", src: soundAsset + "p1_s5.ogg"},
            // {id: "sound_6", src: soundAsset + "p1_s6.ogg"},
            // {id: "sound_7", src: soundAsset + "p1_s7.ogg"},
            // {id: "sound_8", src: soundAsset + "p1_s8.ogg"},
            // {id: "sound_9", src: soundAsset + "p1_s9.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }
    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[0]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(0);
        put_image(content, 0, preload);
        shufflehint("option",["option opt1","option opt2","option opt3","option opt4","option opt5"])
        $(".option").addClass("avoid-clicks");
        switch (countNext) {
            case 0:
                rotatethewheel("rotatearrw");
                checkans("WAKE");
                break;
            case 1:
                rotatethewheel("rotatearrs");
                checkans("SWIM");
                break;
            case 2:
                rotatethewheel("rotatearrc");
                checkans("COOK");
                break;
            case 3:
                rotatethewheel("rotatearrsi");
                checkans("SING");
                break;
            case 4:
                rotatethewheel("rotatearrr");
                checkans("READ");
                break;
            default:
                break;
        }
    }

    function rotatethewheel(animationclass){
        $(".rotatingtext1").hide();
        $(".piechart,.clicktext").click(function(){
            createjs.Sound.stop();
            current_sound = createjs.Sound.play("spinning_wheel");
            current_sound.play();
            current_sound.on('complete', function(){
                if(countNext == 0) {
                    sound_player("sound_0");
                    current_sound.on('complete', function () {
                        $(".option").removeClass("avoid-clicks");
                    });
                }
                else{
                    $(".option").removeClass("avoid-clicks");
                }
            });
            $(".clicktext,.piechart").removeClass("blinkEffect").addClass("avoid-clicks");
            $(".rotatingtext").hide();
            $(".arrow1").addClass(animationclass);
            $(".rotatingtext1").delay(500).fadeIn(100);
        });
        // $(".rotatingtext1").hide();
        // $(".piechart,.clicktext").click(function(){
        //     $(".clicktext").removeClass("blinkEffect").addClass("avoid-clicks");
        //     $(".rotatingtext").hide();
        //     $(".piechart").addClass(animationclass);
        //     $(".rotatingtext1").delay(500).fadeIn(100);
        //     setTimeout(function(){
        //         sound_player(soundid,true);
        //         $(".popuptext").animate({"opacity":"1","z-index":"3"},50);
        //     },1000);
        // });
    }

    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function () {
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });


    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function (index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }
    function shufflehint(optionName,a){
        var optiondiv = $(".contentblock");
        for (var i = 5; i >= 0; i--) {
            optiondiv.append(optiondiv.find("."+optionName).eq(Math.random() * i | 0));
        }
        optiondiv.find("."+optionName).removeClass().addClass("current");
        optiondiv.find(".current").each(function (index) {
            var $this = $(this)
            $this.addClass(a[index]);
        });
        optiondiv.find("."+optionName).removeClass("current");
    }

    function checkans(ans){
        $(".option").on("click",function () {
            createjs.Sound.stop();
            if($(this).text().trim()==ans) {
                $(this).addClass("correctans");
                $(".option").addClass("avoid-clicks");
                $(this).append("<img class='correctwrongimg' src='images/right.png'/>")
                play_correct_incorrect_sound(1);
                navigationcontroller(countNext,$total_page);
            }
            else{
                $(this).addClass("wrongans");
                $(this).append("<img class='correctwrongimg' src='images/wrong.png'/>")
                play_correct_incorrect_sound(0);
            }
        });
    }

});

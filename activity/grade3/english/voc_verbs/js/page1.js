var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_1 = new buzz.sound((soundAsset + "s1_p1.ogg"));
var sound_2 = new buzz.sound((soundAsset + "s1_p2.ogg"));
var sound_3 = new buzz.sound((soundAsset + "s1_p3.ogg"));
var sound_l_1 = new buzz.sound((soundAsset + "p1_s3.ogg"));
var sound_l_2 = new buzz.sound((soundAsset + "p1_s4.ogg"));
var sound_l_3 = new buzz.sound((soundAsset + "p1_s5.ogg"));
var sound_l_4 = new buzz.sound((soundAsset + "p1_s6.ogg"));
var sound_l_5 = new buzz.sound((soundAsset + "p1_s7.ogg"));
var sound_l_6 = new buzz.sound((soundAsset + "p1_s8.ogg"));
var sound_l_7 = new buzz.sound((soundAsset + "p1_s9.ogg"));
var sound_l_8 = new buzz.sound((soundAsset + "p1_s10.ogg"));
var sound_l_9 = new buzz.sound((soundAsset + "p1_s11.ogg"));
var sound_l_10 = new buzz.sound((soundAsset + "p1_s12.ogg"));

var sound_group_p1 = [sound_l_1, sound_l_2, sound_l_3, sound_l_4, sound_l_5, sound_l_6, sound_l_7, sound_l_8, sound_l_9, sound_l_10];

var content=[
{
	//slide0
	additionalclasscontentblock: "coverpage",
	uppertextblock : [
	{
		textdata : data.string.chapter,
		textclass : 'lesson-title'
	}
	]
},
{
	//slide1
	uppertextblock:[
	{
		datahighlightflag: true,
		datahighlightcustomclass: "letshighlight",
		textclass : "startingtext",
		textdata : data.string.p1text1,
	},
	{
		textclass : "suchas",
		textdata : data.string.p1text2,
	}
	],
	vocabbox:[
	{
		imgplace: "onediv",
		imgclass: "insidethediv",
		insidethetext: "insidetext1",
		imgsrc : imgpath + "run.gif",
		topicdesc: data.string.p1text3

	},
	{
		imgplace: "twodiv",
		imgclass: "insidethediv",
		insidethetext: "insidetext2",
		imgsrc : imgpath + "dance.gif",
		topicdesc: data.string.p1text4

	},
	{
		imgplace: "threediv",
		imgclass: "insidethediv",
		insidethetext: "insidetext3",
		imgsrc : imgpath + "jump.gif",
		topicdesc: data.string.p1text5

	}
	]
},
{
	//slide2
	uppertextblock : [
	{
		textdata : data.string.p1text6,
		textclass : 'centertext'
	}
	]
},
{
	//slide3
	additionalclasscontentblock: "contentwithbg",
	uppertextblock : [
	{
		textdata : data.string.clapn,
		textclass : 'cloudnep'
	},
	{
		textdata : data.string.clap,
		textclass : 'cloudback'
	},
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "mainimg",
			imgsrc : imgpath + "clap1.gif"
		},
		],
	}
	],
	audiobox:[
	{
		audiocontaineradditionalclass: "playable",
		audiotext: data.string.claptext
	}
	]
},
{
	//slide4
	additionalclasscontentblock: "contentwithbg",
	uppertextblock : [
	{
		textdata : data.string.countn,
		textclass : 'cloudnep'
	},
	{
		textdata : data.string.count,
		textclass : 'cloudback'
	},
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "mainimg",
			imgsrc : imgpath + "count.gif"
		},
		],
	}
	],
	audiobox:[
	{
		audiocontaineradditionalclass: "playable",
		audiotext: data.string.counttext
	}
	]
},
{
	//slide5
	additionalclasscontentblock: "contentwithbg",
	uppertextblock : [
	{
		textdata : data.string.hiden,
		textclass : 'cloudnep'
	},
	{
		textdata : data.string.hide,
		textclass : 'cloudback'
	},
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "mainimg",
			imgsrc : imgpath + "hide.gif"
		},
		],
	}
	],
	audiobox:[
	{
		audiocontaineradditionalclass: "playable",
		audiotext: data.string.hidetext
	}
	]
},
{
	//slide5
	additionalclasscontentblock: "contentwithbg",
	uppertextblock : [
	{
		textdata : data.string.kissn,
		textclass : 'cloudnep'
	},
	{
		textdata : data.string.kiss,
		textclass : 'cloudback'
	},
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "mainimg",
			imgsrc : imgpath + "kiss.gif"
		},
		],
	}
	],
	audiobox:[
	{
		audiocontaineradditionalclass: "playable",
		audiotext: data.string.kisstext
	}
	]
},
{
	//slide5
	additionalclasscontentblock: "contentwithbg",
	uppertextblock : [
	{
		textdata : data.string.laughn,
		textclass : 'cloudnep'
	},
	{
		textdata : data.string.laugh,
		textclass : 'cloudback'
	},
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "mainimg",
			imgsrc : imgpath + "laugh.gif"
		},
		],
	}
	],
	audiobox:[
	{
		audiocontaineradditionalclass: "playable",
		audiotext: data.string.laughtext
	}
	]
},
{
	//slide5
	additionalclasscontentblock: "contentwithbg",
	uppertextblock : [
	{
		textdata : data.string.openn,
		textclass : 'cloudnep'
	},
	{
		textdata : data.string.open,
		textclass : 'cloudback'
	},
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "mainimg",
			imgsrc : imgpath + "opening_door.gif"
		},
		],
	}
	],
	audiobox:[
	{
		audiocontaineradditionalclass: "playable",
		audiotext: data.string.opentext
	}
	]
},
{
	//slide5
	additionalclasscontentblock: "contentwithbg",
	uppertextblock : [
	{
		textdata : data.string.ploughn,
		textclass : 'cloudnep'
	},
	{
		textdata : data.string.plough,
		textclass : 'cloudback'
	},
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "mainimg",
			imgsrc : imgpath + "plug-the-field.gif"
		},
		],
	}
	],
	audiobox:[
	{
		audiocontaineradditionalclass: "playable",
		audiotext: data.string.ploughtext
	}
	]
},
{
	//slide5
	additionalclasscontentblock: "contentwithbg",
	uppertextblock : [
	{
		textdata : data.string.shutn,
		textclass : 'cloudnep'
	},
	{
		textdata : data.string.shut,
		textclass : 'cloudback'
	},
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "mainimg",
			imgsrc : imgpath + "closethewindow.gif"
		},
		],
	}
	],
	audiobox:[
	{
		audiocontaineradditionalclass: "playable",
		audiotext: data.string.shuttext
	}
	]
},
{
	//slide5
	additionalclasscontentblock: "contentwithbg",
	uppertextblock : [
	{
		textdata : data.string.waven,
		textclass : 'cloudnep'
	},
	{
		textdata : data.string.wave,
		textclass : 'cloudback'
	},
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "mainimg",
			imgsrc : imgpath + "wave.gif"
		},
		],
	}
	],
	audiobox:[
	{
		audiocontaineradditionalclass: "playable",
		audiotext: data.string.wavetext
	}
	]
},
{
	//slide5
	additionalclasscontentblock: "contentwithbg",
	uppertextblock : [
	{
		textdata : data.string.weepn,
		textclass : 'cloudnep'
	},
	{
		textdata : data.string.weep,
		textclass : 'cloudback'
	},
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "mainimg",
			imgsrc : imgpath + "giphy.gif"
		},
		],
	}
	],
	audiobox:[
	{
		audiocontaineradditionalclass: "playable",
		audiotext: data.string.weeptext
	}
	]
},
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	/*recalculateHeightWidth();*/

	var total_page = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
      */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
      */
      function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		switch(countNext){
			// case 1:
			// 	$nextBtn.hide(0);
			// 	$nextBtn.delay(6000).show(0);
			// break;
			case 3:
			playaudio(sound_group_p1[0], $(".audiocontainer"));
			break;
			case 4:
			playaudio(sound_group_p1[1], $(".audiocontainer"));
			break;
			case 5:
			playaudio(sound_group_p1[2], $(".audiocontainer"));
			break;
			case 6:
			playaudio(sound_group_p1[3], $(".audiocontainer"));
			break;
			case 7:
			playaudio(sound_group_p1[4], $(".audiocontainer"));
			break;
			case 8:
			playaudio(sound_group_p1[5], $(".audiocontainer"));
			break;
			case 9:
			playaudio(sound_group_p1[6], $(".audiocontainer"));
			break;
			case 10:
			playaudio(sound_group_p1[7], $(".audiocontainer"));
			break;
			case 11:
			playaudio(sound_group_p1[8], $(".audiocontainer"));
			break;
			case 12:
			playaudio(sound_group_p1[9], $(".audiocontainer"));
			break;
			case 13:
			playaudio(sound_group_p1[10], $(".audiocontainer"));
			break;
			default:
                $nextBtn.hide(0);
				eval("sound_"+(countNext+1)).play().bind("ended",function(){
                    $nextBtn.show(0);
                });
			break;
		}
	}


	function playaudio(sound_data, $dialog_container){
		var playing = true;
		$dialog_container.removeClass("playable");
		$dialog_container.click(function(){
			if(!playing){
				playaudio(sound_data, $dialog_container);
			}
			return false;
		});
		$prevBtn.hide(0);
		if((countNext+1) == content.length){
			ole.footerNotificationHandler.hideNotification();
		}else{
			$nextBtn.hide(0);
		}
		sound_data.play();
		sound_data.bind('ended', function(){
			setTimeout(function(){
				$prevBtn.show(0);
				$dialog_container.addClass("playable");
				playing = false;
				sound_data.unbind('ended');
				if((countNext+1) == content.length){
					ole.footerNotificationHandler.pageEndSetNotification();
				}else{
					$nextBtn.show(0);
				}
			}, 1000);
		});
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";


					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_l_1 = new buzz.sound((soundAsset + "p2_s0.ogg"));
var sound_l_2 = new buzz.sound((soundAsset + "p2_s1.ogg"));
var sound_l_3 = new buzz.sound((soundAsset + "p2_s2.ogg"));
var sound_l_4 = new buzz.sound((soundAsset + "p2_s3.ogg"));
var sound_l_5 = new buzz.sound((soundAsset + "p2_s4.ogg"));
var sound_l_6 = new buzz.sound((soundAsset + "p2_s5.ogg"));
var sound_l_7 = new buzz.sound((soundAsset + "p2_s6.ogg"));
var sound_l_8 = new buzz.sound((soundAsset + "p2_s7.ogg"));
var sound_l_9 = new buzz.sound((soundAsset + "p2_s8.ogg"));
var sound_l_10 = new buzz.sound((soundAsset + "p2_s9.ogg"));

var sound_group_p1 = [sound_l_1, sound_l_2, sound_l_3, sound_l_4, sound_l_5, sound_l_6, sound_l_7, sound_l_8, sound_l_9, sound_l_10];

var content=[
{
	//slide3
	additionalclasscontentblock: "contentwithbg",
	uppertextblock : [
	{
		textdata : data.string.listenn,
		textclass : 'cloudnep'
	},
	{
		textdata : data.string.listen,
		textclass : 'cloudback'
	},
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "mainimg",
			imgsrc : imgpath + "listentomusic.gif"	
		},
		],
	}
	],
	audiobox:[
	{
		audiocontaineradditionalclass: "playable",
		audiotext: data.string.listentext
	}
	]
},
{
	//slide4
	additionalclasscontentblock: "contentwithbg",
	uppertextblock : [
	{
		textdata : data.string.parkn,
		textclass : 'cloudnep'
	},
	{
		textdata : data.string.park,
		textclass : 'cloudback'
	},
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "mainimg",
			imgsrc : imgpath + "carparking.gif"
		},
		],
	}
	],
	audiobox:[
	{
		audiocontaineradditionalclass: "playable",
		audiotext: data.string.parktext
	}
	]
},
{
	//slide5
	additionalclasscontentblock: "contentwithbg",
	uppertextblock : [
	{
		textdata : data.string.pickn,
		textclass : 'cloudnep'
	},
	{
		textdata : data.string.pick,
		textclass : 'cloudback'
	},
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "mainimg",
			imgsrc : imgpath + "pick_up.png"	
		},
		],
	}
	],
	audiobox:[
	{
		audiocontaineradditionalclass: "playable",
		audiotext: data.string.picktext
	}
	]
},
{
	//slide5
	additionalclasscontentblock: "contentwithbg",
	uppertextblock : [
	{
		textdata : data.string.pointn,
		textclass : 'cloudnep'
	},
	{
		textdata : data.string.point,
		textclass : 'cloudback'
	},
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "mainimg",
			imgsrc : imgpath + "pointing01.gif"	
		},
		],
	}
	],
	audiobox:[
	{
		audiocontaineradditionalclass: "playable",
		audiotext: data.string.pointtext
	}
	]
},
{
	//slide5
	additionalclasscontentblock: "contentwithbg",
	uppertextblock : [
	{
		textdata : data.string.reachn,
		textclass : 'cloudnep'
	},
	{
		textdata : data.string.reach,
		textclass : 'cloudback'
	},
	],
	imageblock: [
	{
		imagetoshow: [
		{
            imgclass: "mainimg",
            imgsrc : imgpath + "reachhome.gif"
        }
		],
	}
	],
	audiobox:[
	{
		audiocontaineradditionalclass: "playable",
		audiotext: data.string.reachtext
	}
	]
},
{
	//slide5
	additionalclasscontentblock: "contentwithbg",
	uppertextblock : [
	{
		textdata : data.string.risen,
		textclass : 'cloudnep'
	},
	{
		textdata : data.string.rise,
		textclass : 'cloudback'
	},
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "mainimg",
			imgsrc : imgpath + "rise.gif"	
		},
		],
	}
	],
	audiobox:[
	{
		audiocontaineradditionalclass: "playable",
		audiotext: data.string.risetext
	}
	]
},
{
	//slide5
	additionalclasscontentblock: "contentwithbg",
	uppertextblock : [
	{
		textdata : data.string.shinen,
		textclass : 'cloudnep'
	},
	{
		textdata : data.string.shine,
		textclass : 'cloudback'
	},
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "mainimg",
			imgsrc : imgpath + "sunshine.gif"	
		},
		],
	}
	],
	audiobox:[
	{
		audiocontaineradditionalclass: "playable",
		audiotext: data.string.shinetext
	}
	]
},
{
	//slide5
	additionalclasscontentblock: "contentwithbg",
	uppertextblock : [
	{
		textdata : data.string.touchn,
		textclass : 'cloudnep'
	},
	{
		textdata : data.string.touch,
		textclass : 'cloudback'
	},
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "mainimg",
			imgsrc : imgpath + "touchwall.gif"
		},
		],
	}
	],
	audiobox:[
	{
		audiocontaineradditionalclass: "playable",
		audiotext: data.string.touchtext
	}
	]
},
{
	//slide5
	additionalclasscontentblock: "contentwithbg",
	uppertextblock : [
	{
		textdata : data.string.waken,
		textclass : 'cloudnep'
	},
	{
		textdata : data.string.wake,
		textclass : 'cloudback'
	},
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "mainimg",
			imgsrc : imgpath + "wake_up.gif"	
		},
		],
	}
	],
	audiobox:[
	{
		audiocontaineradditionalclass: "playable",
		audiotext: data.string.waketext
	}
	]
},
{
	//slide5
	additionalclasscontentblock: "contentwithbg",
	uppertextblock : [
	{
		textdata : data.string.wearn,
		textclass : 'cloudnep'
	},
	{
		textdata : data.string.wear,
		textclass : 'cloudback'
	},
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "mainimg",
			imgsrc : imgpath + "wearingclothes.png"	
		},
		],
	}
	],
	audiobox:[
	{
		audiocontaineradditionalclass: "playable",
		audiotext: data.string.weartext
	}
	]
},
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;
	
	/*recalculateHeightWidth();*/
	
	var total_page = 0;
	
	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);
	
	/*
		inorder to use the handlebar partials we need to register them 
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
		
	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/    
   /**   
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that 
      footernotification is called for continue button to navigate to exercise
      */

  /**   
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if 
        so should be taken out from the templateCaller function
      - If the total page number is 
      */  
      function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true 
			islastpageflag ? 
			ole.footerNotificationHandler.pageEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;			
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true 
			islastpageflag ? 
			ole.footerNotificationHandler.pageEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
	}
	
	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		
		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);	
		switch(countNext){
			case 0:
			playaudio(sound_group_p1[0], $(".audiocontainer"));
			break;
			case 1:
			playaudio(sound_group_p1[1], $(".audiocontainer"));
			break;
			case 2:
			playaudio(sound_group_p1[2], $(".audiocontainer"));
			break;
			case 3:
			playaudio(sound_group_p1[3], $(".audiocontainer"));
			break;
			case 4:
			playaudio(sound_group_p1[4], $(".audiocontainer"));
			break;
			case 5:
			playaudio(sound_group_p1[5], $(".audiocontainer"));
			break;
			case 6:
			playaudio(sound_group_p1[6], $(".audiocontainer"));
			break;
			case 7:
			playaudio(sound_group_p1[7], $(".audiocontainer"));
			break;
			case 8:
			playaudio(sound_group_p1[8], $(".audiocontainer"));
			break;
			case 9:
			playaudio(sound_group_p1[9], $(".audiocontainer"));
			break;
			case 10:
			playaudio(sound_group_p1[10], $(".audiocontainer"));
			break;
			default:
			break;
		}
	}
	

	function playaudio(sound_data, $dialog_container){
		var playing = true;
		$dialog_container.removeClass("playable");
		$dialog_container.click(function(){
			if(!playing){
				playaudio(sound_data, $dialog_container);				
			}
			return false;
		});
		$prevBtn.hide(0);
		if((countNext+1) == content.length){
			ole.footerNotificationHandler.hideNotification();
		}else{
			$nextBtn.hide(0);					
		}
		sound_data.play();
		sound_data.bind('ended', function(){
			setTimeout(function(){
				$prevBtn.show(0);
				$dialog_container.addClass("playable");
				playing = false;
				sound_data.unbind('ended');
				if((countNext+1) == content.length){
					ole.footerNotificationHandler.pageEndSetNotification();
				}else{
					$nextBtn.show(0);					
				}
			}, 1000);
		});
	}
	
	function templateCaller(){
		//convention is to always hide the prev and next button and show them based 
		//on the convention or page index		
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		
		loadTimelineProgress($total_page, countNext + 1);
		
		navigationcontroller();
		
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   
		
	}
	
	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});
	
	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });
	
});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			
			
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {	
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/				
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;
					
					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					

					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					
					
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/


var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_l_1 = new buzz.sound((soundAsset + "p1_s0.ogg"));
var sound_l_2 = new buzz.sound((soundAsset + "p4_s1.ogg"));
var sound_l_3 = new buzz.sound((soundAsset + "p4_s2.ogg"));
var sound_l_4 = new buzz.sound((soundAsset + "p4_s3.ogg"));
var sound_l_5 = new buzz.sound((soundAsset + "p4_s4.ogg"));
var content=[
{
	//slide0
	additionalclasscontentblock: "contentwithbg",
	uppertextblock : [
	{
		textclass : 'lesson-title',
		textdata : data.lesson.chapter
	}
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "springimg appear",
			imgsrc : imgpath + "springicon.png"
		},
		{
			imgclass: "summerimg appear2",
			imgsrc : imgpath + "summericon.png"
		},
		{
			imgclass: "autumnimg appear3",
			imgsrc : imgpath + "autumnicon.png"
		},
		{
			imgclass: "winterimg appear4",
			imgsrc : imgpath + "wintericon.png"
		},
		],
	}
	],
},
{
	//slide0
	additionalclasscontentblock: "contentwithbg",
	headerblock : [
	{
		textclass : 'appear3',
		textdata : data.string.winter
	}
	],
	uppertextblock : [
	{
		textclass : 'lesson-title dissapear',
		textdata : data.lesson.chapter
	}
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "springimg dissapear",
			imgsrc : imgpath + "springicon.png"
		},
		{
			imgclass: "summerimg dissapear",
			imgsrc : imgpath + "summericon.png"
		},
		{
			imgclass: "autumnimg dissapear",
			imgsrc : imgpath + "autumnicon.png"
		},
		{
			imgclass: "winterimg selectthis",
			imgsrc : imgpath + "wintericon.png"
		},
		],
	}
	],
},
{
	additionalclasscontentblock: "contentwithbg",
	headerblock : [
	{
		textdata : data.string.winter
	}
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "springimg afterselect",
			imgsrc : imgpath + "wintericon.png"
		},
		{
			imgclass: "centerimg1",
			imgsrc : imgpath + "autumn.png"
		},
		{
			imgclass: "centerimg2",
			imgsrc : imgpath + "winter.png"
		},
		],
	}
	],
	uppertextblock : [
	{
		textclass : 'bottomtext',
		textdata : data.string.witext1
	}
	]
},
{
	additionalclasscontentblock: "contentwithbg",
	headerblock : [
	{
		textdata : data.string.winter
	}
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "springimg afterselect",
			imgsrc : imgpath + "wintericon.png"
		},
		{
			imgclass: "threeimg1 slowappear2",
			imgsrc : imgpath + "dry.png"
		},
		{
			imgclass: "threeimg2 slowappear3",
			imgsrc : imgpath + "snowy.png"
		},
		{
			imgclass: "threeimg3 slowappear4",
			imgsrc : imgpath + "foggy.png"
		},
		],
		imagelabels:[
		{
			imagelabelclass: "threelabel1 slowappear2",
			imagelabeldata: data.string.dry
		},
		{
			imagelabelclass: "threelabel2 slowappear3",
			imagelabeldata: data.string.snowy
		},
		{
			imagelabelclass: "threelabel3 slowappear4",
			imagelabeldata: data.string.foggy
		}
		]
	}
	],
	uppertextblock : [
	{
		textclass : 'uppertext slowappear',
		textdata : data.string.sometime
	}
	]
},
{
	additionalclasscontentblock: "contentwithbg",
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "springimg afterselect",
			imgsrc : imgpath + "wintericon.png"
		},
		{
			imgclass: "threeimg1 slowappear2",
			imgsrc : imgpath + "december.png"
		},
		{
			imgclass: "threeimg2 slowappear3",
			imgsrc : imgpath + "january.png"
		},
		{
			imgclass: "threeimg3 slowappear4",
			imgsrc : imgpath + "february.png"
		},
		]
	}
	],
	uppertextblock : [
	{
		datahighlightflag: true,
		datahighlightcustomclass: "letshighlight",
		textclass : 'uppertext slowappear',
		textdata : data.string.wihappens
	}
	]
}
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	loadTimelineProgress($total_page, countNext + 1);

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
      */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
      */
      function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		buzz.all().stop();

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch(countNext){
			case 0:
				$nextBtn.hide(0);
				sound_l_1.play();
				$nextBtn.delay(4000).show(0);
			break;
			case 1:
				$nextBtn.hide(0);
				$nextBtn.delay(3000).show(0);
				setTimeout(function(){
					sound_l_2.play();
				},	2000);
			break;
			case 2:
				$nextBtn.hide(0);
				$nextBtn.delay(6000).show(0);
				sound_l_3.play();
			break;
			case 3:
				$nextBtn.hide(0);
				$nextBtn.delay(7000).show(0);
				sound_l_4.play();
			break;
			case 4:
			sound_l_5.play();
			setTimeout(function(){
				ole.footerNotificationHandler.pageEndSetNotification();
			},	7000);
			break;
		}

	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		if(countNext != 4)
		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   
	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});

 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ?
		alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		null ;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag   = "</span>";


		if($alltextpara.length > 0){
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				(stylerulename = $(this).attr("data-highlightcustomclass")) :
				(stylerulename = "parsedstring") ;

				texthighlightstarttag = "<span class='"+stylerulename+"'>";
				replaceinstring       = $(this).html();
				replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
				replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

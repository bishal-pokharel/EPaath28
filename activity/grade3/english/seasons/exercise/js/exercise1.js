var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/";


var sound_0 = "0";
createjs.Sound.registerSound((soundAsset + "ex_instr.ogg"), sound_0);

var content1=[
	//slide 0
	{
		contentblockadditionalclass: 'instruction_bg',
		uppertextblockadditionalclass: 'ex_main_text',
		uppertextblock : [
		{
			textdata : data.string.einstruction,
			textclass : 'my_font_very_big',
		}],
	},
];
var content2=[
	//slide 0
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext,
			textclass : 'instruction my_font_big',
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "jun.png",
		}],

		optionsblock : [{
			textdata : data.string.eansa,
			textclass : 'class1 options my_font_medium option_1'
		},
		{
			textdata : data.string.eansb,
			textclass : 'class2 options my_font_medium option_2'
		},
		{
			textdata : data.string.eansc,
			textclass : 'class3 options my_font_medium option_3'
		},
		{
			textdata : data.string.eansd,
			textclass : 'class4 options my_font_medium option_4'
		}],
	},
	//slide 1
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext,
			textclass : 'instruction my_font_big',
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "dec.png",
		}],

		optionsblock : [{
			textdata : data.string.eansa,
			textclass : 'class2 options my_font_medium option_1'
		},
		{
			textdata : data.string.eansb,
			textclass : 'class1 options my_font_medium option_2'
		},
		{
			textdata : data.string.eansc,
			textclass : 'class3 options my_font_medium option_3'
		},
		{
			textdata : data.string.eansd,
			textclass : 'class4 options my_font_medium option_4'
		}],
	},
	//slide 2
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext,
			textclass : 'instruction my_font_big',
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "feb.png",
		}],

		optionsblock : [{
			textdata : data.string.eansa,
			textclass : 'class2 options my_font_medium option_1'
		},
		{
			textdata : data.string.eansb,
			textclass : 'class1 options my_font_medium option_2'
		},
		{
			textdata : data.string.eansc,
			textclass : 'class3 options my_font_medium option_3'
		},
		{
			textdata : data.string.eansd,
			textclass : 'class4 options my_font_medium option_4'
		}],
	},
	//slide 3
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext,
			textclass : 'instruction my_font_big',
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "nov.png",
		}],

		optionsblock : [{
			textdata : data.string.eansa,
			textclass : 'class4 options my_font_medium option_1'
		},
		{
			textdata : data.string.eansb,
			textclass : 'class2 options my_font_medium option_2'
		},
		{
			textdata : data.string.eansc,
			textclass : 'class3 options my_font_medium option_3'
		},
		{
			textdata : data.string.eansd,
			textclass : 'class1 options my_font_medium option_4'
		}],
	},
	//slide 4
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext,
			textclass : 'instruction my_font_big',
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "sept.png",
		}],

		optionsblock : [{
			textdata : data.string.eansa,
			textclass : 'class4 options my_font_medium option_1'
		},
		{
			textdata : data.string.eansb,
			textclass : 'class2 options my_font_medium option_2'
		},
		{
			textdata : data.string.eansc,
			textclass : 'class3 options my_font_medium option_3'
		},
		{
			textdata : data.string.eansd,
			textclass : 'class1 options my_font_medium option_4'
		}],
	},
	//slide 5
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext,
			textclass : 'instruction my_font_big',
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "jul.png",
		}],

		optionsblock : [{
			textdata : data.string.eansa,
			textclass : 'class1 options my_font_medium option_1'
		},
		{
			textdata : data.string.eansb,
			textclass : 'class2 options my_font_medium option_2'
		},
		{
			textdata : data.string.eansc,
			textclass : 'class3 options my_font_medium option_3'
		},
		{
			textdata : data.string.eansd,
			textclass : 'class4 options my_font_medium option_4'
		}],
	},
	//slide 6
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext,
			textclass : 'instruction my_font_big',
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "may.png",
		}],

		optionsblock : [{
			textdata : data.string.eansa,
			textclass : 'class3 options my_font_medium option_1'
		},
		{
			textdata : data.string.eansb,
			textclass : 'class2 options my_font_medium option_2'
		},
		{
			textdata : data.string.eansc,
			textclass : 'class1 options my_font_medium option_3'
		},
		{
			textdata : data.string.eansd,
			textclass : 'class4 options my_font_medium option_4'
		}],
	},
	//slide 7
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext,
			textclass : 'instruction my_font_big',
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "jan.png",
		}],

		optionsblock : [{
			textdata : data.string.eansa,
			textclass : 'class2 options my_font_medium option_1'
		},
		{
			textdata : data.string.eansb,
			textclass : 'class1 options my_font_medium option_2'
		},
		{
			textdata : data.string.eansc,
			textclass : 'class3 options my_font_medium option_3'
		},
		{
			textdata : data.string.eansd,
			textclass : 'class4 options my_font_medium option_4'
		}],
	},
	//slide 8
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext,
			textclass : 'instruction my_font_big',
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "mar.png",
		}],

		optionsblock : [{
			textdata : data.string.eansa,
			textclass : 'class3 options my_font_medium option_1'
		},
		{
			textdata : data.string.eansb,
			textclass : 'class2 options my_font_medium option_2'
		},
		{
			textdata : data.string.eansc,
			textclass : 'class1 options my_font_medium option_3'
		},
		{
			textdata : data.string.eansd,
			textclass : 'class4 options my_font_medium option_4'
		}],
	},
	//slide 9
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext,
			textclass : 'instruction my_font_big',
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "aug.png",
		}],

		optionsblock : [{
			textdata : data.string.eansa,
			textclass : 'class1 options my_font_medium option_1'
		},
		{
			textdata : data.string.eansb,
			textclass : 'class2 options my_font_medium option_2'
		},
		{
			textdata : data.string.eansc,
			textclass : 'class3 options my_font_medium option_3'
		},
		{
			textdata : data.string.eansd,
			textclass : 'class4 options my_font_medium option_4'
		}],
	},
];

content2.shufflearray();
// var content = content2;
var content = content1.concat(content2);

$(function ()
{
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	var score = 0;

	var current_sound = createjs.Sound.play(sound_0);
	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

	//create eggs
	var eggs = new EggTemplate();

 	//eggTemplate.eggMove(countNext);
	eggs.init(10);

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);

		if(countNext ==0){
			setTimeout(function(){
				current_sound.stop();
				current_sound = createjs.Sound.play(sound_0);
				current_sound.play();
			},500);
		}
		//randomize options
		if(countNext>0){
			for( var mm =1; mm<5; mm++){
				$('.options').removeClass('option_'+mm);
			}
			var option_position = [1,2,3,4];
			option_position.shufflearray();
			for(var op=0; op<4; op++){
				$('.options').eq(op).addClass('option_'+option_position[op]);
			}
			var wrong_clicked = false;
			$(".options").click(function(){
				if($(this).hasClass("class1")){
					if(!wrong_clicked){
						eggs.update(true);
					}
					$(".options").css('pointer-events', 'none');
					$(this).css({
						'border': '3px solid #FCD172',
						'background-color': '#6EB260',
						'color': 'white'
					});
					play_correct_incorrect_sound(1);
                    $(this).append("<img class='correctwrongImg' src='images/correct.png'/>");
					if(countNext != $total_page)
						$nextBtn.show(0);
				}
				else{
					if(!wrong_clicked){
						eggs.update(false);
					}
					$(this).css({
						'background-color': '#BA6B82',
						'border': 'none'
					});
					wrong_clicked = true;
					play_correct_incorrect_sound(0);
                    $(this).append("<img class='correctwrongImg' src='images/wrong.png'/>");
                    wrong_clicked = true;
				}
			});
		} else{
			$nextBtn.show(0);
		}
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
		if(countNext>1){
			eggs.gotoNext();
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

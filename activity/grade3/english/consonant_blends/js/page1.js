var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[

//startpage
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg1",
		uppertextblock:[
		{
           textclass: "covertext1",
			textdata:data.lesson.chapter
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "s1img",
				imgid : 'coverpage',
				imgsrc: ""
			},
			{
				imgclass: "s4img",
				imgid : 'birds',
				imgsrc: ""
			}
		]
	}]

},
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg1",
		headerblock:[
		{
			textclass: "covertext",
			textdata: data.lesson.chapter
		}
	],
	uppertextblock:[
		{

            datahighlightflag: true,
			datahighlightcustomclass: "p1txt",
			textclass: "box1",
			textdata: data.string.p1text1
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "p1img",
				imgid : 'duck01',
				imgsrc: ""
			}
		]
	}]
},
//slide1
{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg1",
		headerblock:[
		{
			textclass: "covertext",
			textdata: data.lesson.chapter
		}
	],
	uppertextblock:[
		{
			textclass: "box2",
			textdata: data.string.p1s1text2
		}

	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "p1img",
				imgid : 'duck01',
				imgsrc: ""
			}
		]
	}],
exerciseblock: [
	{

		exeoptions: [
			{
				forshuffle: "class2",
				optdata: data.string.p1s1text3,
			},
				{
					forshuffle: "class1",
					optdata: data.string.p1s1text4,
				},
				{
				forshuffle: "class1",
				optdata: data.string.p1s1text5,
			},
			{
				forshuffle: "class3",
				optdata: data.string.p1s1text6,
			},
			{
				forshuffle: "class1",
				optdata: data.string.p1s1text7,
			}
			]
		}
	]

},
//slide2
{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg1",
		headerblock:[
		{
			textclass: "covertext",
			textdata: data.lesson.chapter
		}
	],
	uppertextblock:[
		{
			textclass: "box2",
			textdata: data.string.p1s1text2
		}

	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "p1img",
				imgid : 'duck01',
				imgsrc: ""
			}
		]
	}],
exerciseblock: [
	{

		exeoptions: [
			{
				forshuffle: "class1",
				optdata: data.string.p1s2text3,
			},
				{
					forshuffle: "class1",
					optdata: data.string.p1s2text4,
				},
				{
				forshuffle: "class2",
				optdata: data.string.p1s2text5,
			},
			{
				forshuffle: "class1",
				optdata: data.string.p1s2text6,
			},
			{
				forshuffle: "class3",
				optdata: data.string.p1s2text7,
			}
			]
		}
	]

},
//slide3
{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg1",
		headerblock:[
		{
			textclass: "covertext",
			textdata: data.lesson.chapter
		}
	],
	uppertextblock:[
		{
            datahighlightflag:true,
            datahighlightcustomclass:"p1txt",
   			textclass: "s3box2",
			textdata: data.string.p1s3text1
		}

	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "p1s3img",
				imgid : 'duck02',
				imgsrc: ""
			}
		]
	}
]

},
//slide4
{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg1",
		headerblock:[
		{
			textclass: "covertext",
			textdata: data.lesson.chapter
		}
	],
	uppertextblock:[
		{
			textclass: "s4box2",
			textdata: data.string.p1s4text1
		}


	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "p1s4img",
				imgid : 'duck01',
				imgsrc: ""
			}

		]
	}
]

},
//slide5
{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg1",
		headerblock:[
		{
			textclass: "covertext",
			textdata: data.lesson.chapter
		}
	],
	uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "p1txt",
			textclass: "s5box2",
			textdata: data.string.p1s5text1
		},
		{
            datahighlightflag:true,
            datahighlightcustomclass:"underline",
			textclass: "s4box3",
			textdata: data.string.p1s4text2
		}

	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "p1s5img",
				imgid : 'duck01',
				imgsrc: ""
			},
			{
				imgclass: "p1s4skullimg",
				imgid : 'train',
				imgsrc: ""
			},
				{
				imgclass: "p1s4peakerimg",
				imgid : 'speaker',
				imgsrc: ""
			}
		]
	}
]

},
//slide6
{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg1",
		headerblock:[
		{
			textclass: "covertext",
			textdata: data.lesson.chapter
		}
	],
	uppertextblock:[
		{
			textclass: "s6box2",
			textdata: data.string.p1s6text1
		}

	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "p1s6img",
				imgid : 'duck02a',
				imgsrc: ""
			}
		]
	}
]

},
//slide7
{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg1",
		headerblock:[
		{
			textclass: "covertext",
			textdata: data.lesson.chapter
		}
	],
	uppertextblock:[
		{
			datahighlightflag:true,
			datahighlightcustomclass:"p1txt",
			textclass: "s7box2",
			textdata: data.string.p1s7text1
		},
		{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass: "s7box3",
			textdata: data.string.p1s7text2
		}

	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "p1s7img",
				imgid : 'duck01a',
				imgsrc: ""
			},
			{
				imgclass: "p1s7secimg",
				imgid : 'spring',
				imgsrc: ""
			},
			{
				imgclass: "speaker",
				imgid : 'speaker',
				imgsrc: ""
			}
		]
	}
]

}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "duck01", src: imgpath+"duck01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "duck02", src: imgpath+"duck02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spring", src: imgpath+"spring.png", type: createjs.AbstractLoader.IMAGE},
			{id: "skull", src: imgpath+"skull.png", type: createjs.AbstractLoader.IMAGE},
			{id: "speaker", src: imgpath+"speaker.png", type: createjs.AbstractLoader.IMAGE},
			{id: "duck01a", src: imgpath+"duck01a.png", type: createjs.AbstractLoader.IMAGE},
			{id: "duck02a", src: imgpath+"duck02a.png", type: createjs.AbstractLoader.IMAGE},
			{id: "train", src: imgpath+"train04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tree", src: imgpath+"tree.png", type: createjs.AbstractLoader.IMAGE},
			{id: "frog", src: imgpath+"frog.png", type: createjs.AbstractLoader.IMAGE},
			{id: "clouds", src: imgpath+"cloud03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "birds", src: imgpath+"flying-birds.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "prince", src: imgpath+"prince.png", type: createjs.AbstractLoader.IMAGE},
			{id: "coverpage", src: imgpath+"coverpage.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"p1_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p1_s1.ogg"},
			{id: "sound_3", src: soundAsset+"p1_s2.ogg"},
			// {id: "sound_4", src: soundAsset+"p1_s3.ogg"},
			{id: "sound_5", src: soundAsset+"p1_s4.ogg"},
			{id: "sound_6", src: soundAsset+"p1_s5.ogg"},
			{id: "sound_7", src: soundAsset+"p1_s6.ogg"},
			{id: "sound_8", src: soundAsset+"p1_s7.ogg"},
			{id: "sound_9", src: soundAsset+"p1_s8.ogg"},
			{id: "sound_10", src: soundAsset+"train.ogg"},
			{id: "sound_11", src: soundAsset+"spring.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);

		//tick qns start

		var wrngClicked = false;
		var corrCounter=0;

		$(".buttonsel").click(function(){

					/*class 1 is always for the right answer. updates scoreboard and disables other click if
					right answer is clicked*/

					if($(this).hasClass("class1") && $(this).hasClass("forhover")){

						$(this).removeClass('forhover');
                        corrCounter++;
						checkCrCount();
						//corr_action();
						play_correct_incorrect_sound(1);
						$(this).css("background","#98c02e");
						$(this).css("border","5px solid #deef3c");
                        $(this).css("color","white");
						$(this).siblings(".corctopt").show(0);
						//ansClicked = true;

						//if(countNext != $total_page)

						//navigationcontroller();
					}
					else{
						if($(this).hasClass("forhover")){
						//testin.update(false);
						play_correct_incorrect_sound(0);
						$(this).css("background","#FF0000");
						$(this).css("border","5px solid #980000");
						$(this).css("color","white");
						$(this).siblings(".wrngopt").show(0);
						wrngClicked = true;

						}
					}
			function checkCrCount(){
				if(corrCounter==3){
						$('.buttonsel').removeClass('forhover forhoverimg');
                        console.log(corrCounter);
					navigationcontroller();
				}
			}

	});

		//tick qns end


		switch(countNext){
			case 0:
			sound_player("sound_1");
			break;
			case 1:
			sound_player("sound_2");
			break;
			case 2:
			/*TO DO
				the next button is showing up after the audio.
				refer to grade 2 math capacity page2.js for modified sound_player function
				it has option for not calling navigationcontroller after the audio
			*/
			sound_player("sound_3",0);
			break;
			case 3:
			sound_player("sound_4");
			break;
			case 4:
			sound_player("sound_5");
			break;
			case 5:
			sound_player("sound_6");
			break;
			case 6:
			// currentSound_1="sound_10";
			// currentSound = "sound_7";
			//lateSound();
			// lateSoundTrSpr();
			 sound_player_duo("sound_10","sound_7")
			break;
			case 7:
			sound_player("sound_8");
			break;
			case 8:
			//currentSound="sound_9";
			// currentSound_1="sound_11";
			// lateSoundTrSpr();
			// lateSound();
			sound_player_duo("sound_11","sound_9")
			
			/*
			TO DO
			The issue with these two function is that the navigationcontroller triggers after 1st audio
			use a function named sound_player_duo instead? it is written below
			*/
			break;
		}
//click and play start

	$('.speaker').click(function(){
		if($(this).hasClass("enable")){
			sound_player("sound_11");
			$nextBtn.hide(0);
			$prevBtn.hide(0);
		}
		else
		$(this).hasClass("disable");
	});

	$('.p1s4peakerimg').click(function(){
		if($(this).hasClass("enable")){
						sound_player("sound_10");
						$nextBtn.hide(0);
						$prevBtn.hide(0);
						}
			else
			$(this).hasClass("disable");			
		});
						
								 


//click and play end
	}




	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
		 $('.speaker').addClass("enable");
		  $('.p1s4peakerimg').addClass("enable");
			if(next == null)
			navigationcontroller();
		});
	}

	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2);
		});

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sound/";

var sound_s0 = new buzz.sound((soundAsset + "s1_p1.ogg"));
var sound_s1 = new buzz.sound((soundAsset + "s1_p2.ogg"));
var sound_s2 = new buzz.sound((soundAsset + "s1_p3.ogg"));
var sound_s3 = new buzz.sound((soundAsset + "s1_p4.ogg"));
var sound_s4 = new buzz.sound((soundAsset + "s1_p5.ogg"));
var sound_s5 = new buzz.sound((soundAsset + "s1_p6.ogg"));
var sound_s6 = new buzz.sound((soundAsset + "s1_p7.ogg"));
var sound_s7 = new buzz.sound((soundAsset + "s1_p8_1.ogg"));
var sound_s8 = new buzz.sound((soundAsset + "s1_p8_2.ogg"));
var sound_s9 = new buzz.sound((soundAsset + "s1_p8_3.ogg"));


var sound_group_p1 = [sound_s0, sound_s1, sound_s2, sound_s3, sound_s4, sound_s5, sound_s6, sound_s7, sound_s8, sound_s9];

var content=[
	{
   	//start
   	contentnocenteradjust: true,
     imageblock: [{
       imagestoshow:[{
         imgclass: "firstpagepunccss",
         imgid: "",
         imgsrc: imgpath+"punc_bg.jpg"
       }]
     }],
   },{
		// slide1
		contentnocenteradjust: true,
		uppertextblock: [{
			textclass: "description_title animate_out",
			textdata: data.string.lesson_title
		}],
		imageblock:[{
			imagelabels:[{
				imagelabelclass: "period2 animate_in",
				imagelabeldata: "&nbsp;"
			}]
		}],
		lowertextblock:[{
			textclass:"period_dialogue dialog_text template-dialougebox2-top-white align_left",
			datahighlightflag: true,
			datahighlightcustomclass: "pointing_highlight",
			textdata: data.string.p1_s0
		}]

	},{
		// slide2
		contentnocenteradjust: true,
		imageblock:[{
			imagelabels:[{
				imagelabelclass: "period2",
				imagelabeldata: "&nbsp;"
			}]
		}],
		lowertextblock:[{
			textclass:"period_dialogue dialog_text template-dialougebox2-top-white align_left",
			textdata: data.string.p1_s1
		}]
	},{
		// slide3
		contentnocenteradjust: true,
		imageblock:[{
			imagelabels:[{
				imagelabelclass: "period1",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "question1 animate_in",
				imagelabeldata: "&nbsp;"
			}]
		}],
		lowertextblock:[{
			textclass:"question_dialogue dialog_text template-dialougebox2-top-flipped-white",
			datahighlightflag: true,
			datahighlightcustomclass: "pointing_highlight",
			textdata: data.string.p1_s2
		}]
	},{
		// slide4
		contentnocenteradjust: true,
		imageblock:[{
			imagelabels:[{
				imagelabelclass: "period1",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "question1",
				imagelabeldata: "&nbsp;"
			}]
		}],
		lowertextblock:[{
			textclass:"question_dialogue dialog_text template-dialougebox2-top-flipped-white",
			textdata: data.string.p1_s3
		}]
	},{
		// slide5
		contentnocenteradjust: true,
		imageblock:[{
			imagelabels:[{
				imagelabelclass: "period1",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "question4",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "exclamation1 animate_in",
				imagelabeldata: "&nbsp;"
			}]
		}],
		lowertextblock:[{
			textclass:"exclamation_dialogue dialog_text template-dialougebox2-top-flipped-white",
			datahighlightflag: true,
			datahighlightcustomclass: "pointing_highlight",
			textdata: data.string.p1_s4
		}]
	},{
		// slide6
		contentnocenteradjust: true,
		imageblock:[{
			imagelabels:[{
				imagelabelclass: "period1",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "question4",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "exclamation1",
				imagelabeldata: "&nbsp;"
			}]
		}],
		lowertextblock:[{
			textclass:"exclamation_dialogue2 dialog_text template-dialougebox2-top-flipped-white",
			textdata: data.string.p1_s5
		}]
	},{
		// slide7
		contentnocenteradjust: true,
		imageblock:[{
			imagestoshow:[{
				imgclass: "sack",
				imgsrc: imgpath+"sack.png"
			}],
			imagelabels:[{
				imagelabelclass: "period3",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "question3 animate_dodge_bag",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "exclamation2",
				imagelabeldata: "&nbsp;"
			}]
		}],
		lowertextblock:[{
			textclass:"period_dialogue2 dialog_text template-dialougebox2-top-white",
			textdata: data.string.p1_s6
		},{
			textclass:"question_dialogue2 dialog_text template-dialougebox2-top-flipped-white",
			textdata: data.string.p1_s7
		},{
			textclass:"exclamation_dialogue3 dialog_text template-dialougebox2-top-flipped-white",
			textdata: data.string.p1_s8
		}]
	}
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

  loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			setTimeout(function(){
				if(countNext == $total_page - 1)
					islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
			}, 10500);
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    vocabcontroller.findwords(countNext);

		function playaudio(audioindex, show_hide_next_prev_btns, length){
			var audio = sound_group_p1[audioindex];

			show_hide_next_prev_btns = (show_hide_next_prev_btns == null)? true: show_hide_next_prev_btns;
			// if(show_hide_next_prev_btns){
			// }
			if(countNext > 6 && countNext < $total_page){
				$(".imageclose").hide(0);
			}

			audio.play();
			audio.bind("ended", function(){
				if(length == 1){
					// if(show_hide_next_prev_btns){
						(countNext < ($total_page-1) ) ? $nextBtn.show(0):  true;
						(countNext > 0) ? $prevBtn.show(0):  true;
					// }

					if( countNext == ($total_page-1)){
						$(".imageclose").show(0);
					}
					$(this).unbind("ended");
				} else {
					$(this).unbind("ended");
					playaudio(audioindex+1, show_hide_next_prev_btns, length-1);
				}
			});
		}
		if( countNext > 0){
			(countNext < $total_page ) ? $nextBtn.hide(0):  true;
			(countNext > 0) ? $prevBtn.hide(0):  true;
		}
		switch(countNext) {
			case 0:

			playaudio(0, true, 1);
			break;
			case 1:
				var $pointing = $(".pointing_highlight");
				$($pointing[0]).append("<img class='pointer' src='"+imgpath+"/arrowpointer.png'/>");
				$($pointing[1]).append("<img class='pointer' src='"+imgpath+"/arrowpointer.png'/>");
				$($pointing[2]).append("<img class='pointer' src='"+imgpath+"/arrowpointer.png'/>");
				$($pointing[3]).append("<img class='pointer' src='"+imgpath+"/arrowpointer.png'/>");
				setTimeout(function(){
					$(".period_dialogue").show(0);
					playaudio(1, true, 1);
				}, 2000);
				break;
			case 2:
				setTimeout(function(){
					$(".period_dialogue").show(0);
					playaudio(2, true, 1);
				}, 500);
				break;
			case 3:
				var $pointing = $(".pointing_highlight");
				$($pointing[0]).append("<img class='pointer2' src='"+imgpath+"/arrowpointer.png'/>");
				setTimeout(function(){
					$(".question_dialogue").show(0);
					playaudio(3, true, 1);
				}, 2000);
				break;
			case 4:
				setTimeout(function(){
					$(".question_dialogue").show(0);
					playaudio(4, true, 1);
				}, 500);
				break;
			case 5:
				var $pointing = $(".pointing_highlight");
				$($pointing[0]).append("<img class='pointer3' src='"+imgpath+"/arrowpointer.png'/>");
				$($pointing[1]).append("<img class='pointer3' src='"+imgpath+"/arrowpointer.png'/>");
				$($pointing[2]).append("<img class='pointer3' src='"+imgpath+"/arrowpointer.png'/>");
				setTimeout(function(){
					$(".exclamation_dialogue").show(0);
					playaudio(5, true, 1);
				}, 2000);
				break;
			case 6:
				setTimeout(function(){
					$(".exclamation_dialogue2").show(0);
					playaudio(6, true, 1);
				}, 500);
				break;
			case 7:
			setTimeout(function(){
					playaudio(7, false, 3);
			},5500);

				setTimeout(function(){
					$(".exclamation_dialogue3").delay(200).show(0);
					$(".question_dialogue2").delay(1000).show(0);
					$(".period_dialogue2").delay(2200).show(0);
				}, 5500);
				break;
			default:
				break;
		}

		// splitintofractions($(".fractionblock"));
  }

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

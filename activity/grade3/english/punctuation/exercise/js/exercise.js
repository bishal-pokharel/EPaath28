var content=[
    //slide 0
    {
        contentblockadditionalclass:'',
        exerciseblock: [
            {
                textclass : 'instruction my_font_big',
                questiondata: data.string.pq1,
                option: [
                    {
                        option_class: "class1",
                        optiondata: data.string.a1,
                    },
                    {
                        option_class: "class2",
                        optiondata: data.string.a2,
                    },
                    {
                        option_class: "class3",
                        optiondata: data.string.a3,
                    },
                    {
                        option_class: "class4",
                        optiondata: data.string.a4,
                    }],
            }
        ]
    },
    //slide 1
    {
        contentblockadditionalclass:'',
        exerciseblock: [
            {
                textclass : 'instruction my_font_big',
                questiondata: data.string.pq1,
                option: [
                    {
                        option_class: "class1",
                        optiondata: data.string.a5,
                    },
                    {
                        option_class: "class2",
                        optiondata: data.string.a6,
                    },
                    {
                        option_class: "class3",
                        optiondata: data.string.a7,
                    },
                    {
                        option_class: "class4",
                        optiondata: data.string.a8,
                    }],
            }
        ]
    },
    //slide 2
    {
        contentblockadditionalclass:'',
        exerciseblock: [
            {
                textclass : 'instruction my_font_big',
                questiondata: data.string.pq1,
                option: [
                    {
                        option_class: "class1",
                        optiondata: data.string.a9,
                    },
                    {
                        option_class: "class2",
                        optiondata: data.string.a10,
                    },
                    {
                        option_class: "class3",
                        optiondata: data.string.a11,
                    },
                    {
                        option_class: "class4",
                        optiondata: data.string.a12,
                    }],
            }
        ]
    },
    //slide 3
    {
        contentblockadditionalclass:'',
        exerciseblock: [
            {
                textclass : 'instruction my_font_big',
                questiondata: data.string.pq1,
                option: [
                    {
                        option_class: "class1",
                        optiondata: data.string.a13,
                    },
                    {
                        option_class: "class2",
                        optiondata: data.string.a14,
                    },
                    {
                        option_class: "class3",
                        optiondata: data.string.a15,
                    },
                    {
                        option_class: "class4",
                        optiondata: data.string.a16,
                    }],
            }
        ]
    },
    //slide 4
    {
        contentblockadditionalclass:'',
        exerciseblock: [
            {
                textclass : 'instruction my_font_big',
                questiondata: data.string.pq1,
                option: [
                    {
                        option_class: "class1",
                        optiondata: data.string.a17,
                    },
                    {
                        option_class: "class2",
                        optiondata: data.string.a18,
                    },
                    {
                        option_class: "class3",
                        optiondata: data.string.a19,
                    },
                    {
                        option_class: "class4",
                        optiondata: data.string.a20,
                    }],
            }
        ]
    },
    //slide 5
    {
        contentblockadditionalclass:'',
        exerciseblock: [
            {
                textclass : 'instruction my_font_big',
                questiondata: data.string.pq1,
                option: [
                    {
                        option_class: "class1",
                        optiondata: data.string.a21,
                    },
                    {
                        option_class: "class2",
                        optiondata: data.string.a22,
                    },
                    {
                        option_class: "class3",
                        optiondata: data.string.a23,
                    },
                    {
                        option_class: "class4",
                        optiondata: data.string.a24,
                    }],
            }
        ]
    },
    //slide 6
    {
        contentblockadditionalclass:'',
        exerciseblock: [
            {
                textclass : 'instruction my_font_big',
                questiondata: data.string.pq1,
                option: [
                    {
                        option_class: "class1",
                        optiondata: data.string.a25,
                    },
                    {
                        option_class: "class2",
                        optiondata: data.string.a26,
                    },
                    {
                        option_class: "class3",
                        optiondata: data.string.a27,
                    },
                    {
                        option_class: "class4",
                        optiondata: data.string.a28,
                    }],
            }
        ]
    },
    //slide 7
    {
        contentblockadditionalclass:'',
        exerciseblock: [
            {
                textclass : 'instruction my_font_big',
                questiondata: data.string.pq1,
                option: [
                    {
                        option_class: "class1",
                        optiondata: data.string.a29,
                    },
                    {
                        option_class: "class2",
                        optiondata: data.string.a30,
                    },
                    {
                        option_class: "class3",
                        optiondata: data.string.a31,
                    },
                    {
                        option_class: "class4",
                        optiondata: data.string.a32,
                    }],
            }
        ]
    },
    //slide 8
    {
        contentblockadditionalclass:'',
        exerciseblock: [
            {
                textclass : 'instruction my_font_big',
                questiondata: data.string.pq1,
                option: [
                    {
                        option_class: "class1",
                        optiondata: data.string.a33,
                    },
                    {
                        option_class: "class2",
                        optiondata: data.string.a34,
                    },
                    {
                        option_class: "class3",
                        optiondata: data.string.a35,
                    },
                    {
                        option_class: "class4",
                        optiondata: data.string.a36,
                    }],
            }
        ]
    },
    //slide 10

];

/*remove this for non random questions*/
$(function () {
    var numbertemp = new EggTemplate();

    var exercise = new template_exercise_mcq_monkey(content, numbertemp, true);
    exercise.create_exercise();
});

var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/";
var balloon_src = [
						"images/ex_balloon/balloon01.png",
						"images/ex_balloon/balloon02.png",
						"images/ex_balloon/balloon03.png",
						"images/ex_balloon/balloon04.png",
						"images/ex_balloon/balloon05.png",
						"images/ex_balloon/balloon06.png",
					];
var pop_balloon_src = "images/ex_balloon/pop.png";
var pop_star_src = "images/star_1.gif";
var pop_sound = new buzz.sound(('sounds/common/balloon-pop.ogg'));
var sound_1 = new buzz.sound((soundAsset + "exercise.ogg"));

var content=[
	//slide 0
	{
		contentblockadditionalclass: 'main_bg',
		exerciseblock: [
			{
				instructiondata : data.string.e1instruction,
				instructionclass : 'my_font_big',

				questionclass: 'my_font_big',
				questiondata: data.string.e1text,
				datahighlightflag: true,
				datahighlightcustomclass: 'blank-space',

				option: [
					{
						mc_class: "class1",
						optiondata: data.string.e1ans1a,
					},
					{
						mc_class: "class2",
						optiondata: data.string.e1ans1b,
					},
					{
						mc_class: "class3",
						optiondata: data.string.e1ans1c,
					},
					{
						mc_class: "class4",
						optiondata: data.string.e1ans1d,
					}],
			}
		]
	},
	//slide 1
	{
		contentblockadditionalclass: 'main_bg',
		exerciseblock: [
			{
				instructiondata : data.string.e1instruction,
				instructionclass : 'my_font_big',

				questionclass: 'my_font_big',
				questiondata: data.string.e2text,
				datahighlightflag: true,
				datahighlightcustomclass: 'blank-space',

				option: [
					{
						mc_class: "class1",
						optiondata: data.string.e2ans1a,
					},
					{
						mc_class: "class2",
						optiondata: data.string.e2ans1b,
					},
					{
						mc_class: "class3",
						optiondata: data.string.e2ans1c,
					},
					{
						mc_class: "class4",
						optiondata: data.string.e2ans1d,
					}],
			}
		]
	},
	//slide 2
	{
		contentblockadditionalclass: 'main_bg',
		exerciseblock: [
			{
				instructiondata : data.string.e1instruction,
				instructionclass : 'my_font_big',

				questionclass: 'my_font_big',
				questiondata: data.string.e3text,
				datahighlightflag: true,
				datahighlightcustomclass: 'blank-space',

				option: [
					{
						mc_class: "class1",
						optiondata: data.string.e3ans1a,
					},
					{
						mc_class: "class2",
						optiondata: data.string.e3ans1b,
					},
					{
						mc_class: "class3",
						optiondata: data.string.e3ans1c,
					},
					{
						mc_class: "class4",
						optiondata: data.string.e3ans1d,
					}],
			}
		]
	},
	//slide 3
	{
		contentblockadditionalclass: 'main_bg',
		exerciseblock: [
			{
				instructiondata : data.string.e1instruction,
				instructionclass : 'my_font_big',

				questionclass: 'my_font_big',
				questiondata: data.string.e4text,
				datahighlightflag: true,
				datahighlightcustomclass: 'blank-space',

				option: [
					{
						mc_class: "class1",
						optiondata: data.string.e4ans1a,
					},
					{
						mc_class: "class2",
						optiondata: data.string.e4ans1b,
					},
					{
						mc_class: "class3",
						optiondata: data.string.e4ans1c,
					},
					{
						mc_class: "class4",
						optiondata: data.string.e4ans1d,
					}],
			}
		]
	},
	//slide 4
	{
		contentblockadditionalclass: 'main_bg',
		exerciseblock: [
			{
				instructiondata : data.string.e1instruction,
				instructionclass : 'my_font_big',

				questionclass: 'my_font_big',
				questiondata: data.string.e5text,
				datahighlightflag: true,
				datahighlightcustomclass: 'blank-space',

				option: [
					{
						mc_class: "class1",
						optiondata: data.string.e5ans1a,
					},
					{
						mc_class: "class2",
						optiondata: data.string.e5ans1b,
					},
					{
						mc_class: "class3",
						optiondata: data.string.e5ans1c,
					},
					{
						mc_class: "class4",
						optiondata: data.string.e5ans1d,
					}],
			}
		]
	},
	//slide 5
	{
		contentblockadditionalclass: 'main_bg',
		exerciseblock: [
			{
				instructiondata : data.string.e1instruction,
				instructionclass : 'my_font_big',

				questionclass: 'my_font_big',
				questiondata: data.string.e6text,
				datahighlightflag: true,
				datahighlightcustomclass: 'blank-space',

				option: [
					{
						mc_class: "class1",
						optiondata: data.string.e6ans1a,
					},
					{
						mc_class: "class2",
						optiondata: data.string.e6ans1b,
					},
					{
						mc_class: "class3",
						optiondata: data.string.e6ans1c,
					},
					{
						mc_class: "class4",
						optiondata: data.string.e6ans1d,
					}],
			}
		]
	},

	//slide 6
	{
		contentblockadditionalclass: 'main_bg',
		exerciseblock: [
			{
				instructiondata : data.string.e1instruction,
				instructionclass : 'my_font_big',

				questionclass: 'my_font_big',
				questiondata: data.string.e7text,
				datahighlightflag: true,
				datahighlightcustomclass: 'blank-space',

				option: [
					{
						mc_class: "class1",
						optiondata: data.string.e7ans1a,
					},
					{
						mc_class: "class2",
						optiondata: data.string.e7ans1b,
					},
					{
						mc_class: "class3",
						optiondata: data.string.e7ans1c,
					},
					{
						mc_class: "class4",
						optiondata: data.string.e7ans1d,
					}],
			}
		]
	},
	//slide 7
	{
		contentblockadditionalclass: 'main_bg',
		exerciseblock: [
			{
				instructiondata : data.string.e1instruction,
				instructionclass : 'my_font_big',

				questionclass: 'my_font_big',
				questiondata: data.string.e8text,
				datahighlightflag: true,
				datahighlightcustomclass: 'blank-space',

				option: [
					{
						mc_class: "class1",
						optiondata: data.string.e8ans1a,
					},
					{
						mc_class: "class2",
						optiondata: data.string.e8ans1b,
					},
					{
						mc_class: "class3",
						optiondata: data.string.e8ans1c,
					},
					{
						mc_class: "class4",
						optiondata: data.string.e8ans1d,
					}],
			}
		]
	},
	//slide 8
	{
		contentblockadditionalclass: 'main_bg',
		exerciseblock: [
			{
				instructiondata : data.string.e1instruction,
				instructionclass : 'my_font_big',

				questionclass: 'my_font_big',
				questiondata: data.string.e9text,
				datahighlightflag: true,
				datahighlightcustomclass: 'blank-space',

				option: [
					{
						mc_class: "class1",
						optiondata: data.string.e9ans1a,
					},
					{
						mc_class: "class2",
						optiondata: data.string.e9ans1b,
					},
					{
						mc_class: "class3",
						optiondata: data.string.e9ans1c,
					},
					{
						mc_class: "class4",
						optiondata: data.string.e9ans1d,
					}],
			}
		]
	},
	//slide 9
	{
		contentblockadditionalclass: 'main_bg',
		exerciseblock: [
			{
				instructiondata : data.string.e1instruction,
				instructionclass : 'my_font_big',

				questionclass: 'my_font_big',
				questiondata: data.string.e10text,
				datahighlightflag: true,
				datahighlightcustomclass: 'blank-space',

				option: [
					{
						mc_class: "class1",
						optiondata: data.string.e10ans1a,
					},
					{
						mc_class: "class2",
						optiondata: data.string.e10ans1b,
					},
					{
						mc_class: "class3",
						optiondata: data.string.e10ans1c,
					},
					{
						mc_class: "class4",
						optiondata: data.string.e10ans1d,
					}],
			}
		]
	},
	//slide 10
	{
		contentblockadditionalclass: 'main_bg',
		exerciseblock: [
			{
				instructiondata : data.string.e1instruction,
				instructionclass : 'my_font_big',

				questionclass: 'my_font_big',
				questiondata: data.string.e11text,
				datahighlightflag: true,
				datahighlightcustomclass: 'blank-space',

				option: [
					{
						mc_class: "class1",
						optiondata: data.string.e11ans1a,
					},
					{
						mc_class: "class2",
						optiondata: data.string.e11ans1b,
					},
					{
						mc_class: "class3",
						optiondata: data.string.e11ans1c,
					},
					{
						mc_class: "class4",
						optiondata: data.string.e11ans1d,
					}],
			}
		]
	}
];

content.shufflearray();
// var content = content2;

$(function ()
{
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	var score = 0;
	var current_sound  = sound_1;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

	//create scoretemplate
	var scoretemplate = new NumberTemplate();

 	//eggTemplate.eggMove(countNext);
	scoretemplate.init(10);

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		balloon_src.shufflearray();


		scoretemplate.numberOfQuestions();
		countNext==0?sound_1.play():'';
		$('.class1>.balloon-icon').attr('src', balloon_src[0]);
		$('.class2>.balloon-icon').attr('src', balloon_src[1]);
		$('.class3>.balloon-icon').attr('src', balloon_src[2]);
		$('.class4>.balloon-icon').attr('src', balloon_src[3]);
		// for( var mm =1; mm<5; mm++){
			// $('.main-container').removeClass('option_'+mm);
		// }
		var option_position = [1,2,3,4];
		option_position.shufflearray();
		for(var op=0; op<4; op++){
			$('.main-container').eq(op).addClass('option_'+option_position[op]);
		}
		var wrong_clicked = false;
		$(".main-container").click(function(){
			current_sound.stop();
			if($(this).hasClass("class1")){
				if(!wrong_clicked){
					scoretemplate.update(true);
				}
				var $textballoon =  $(this).children('p');
				var box_left = ( $('.question').position().left + $('.blank-space').position().left )*100/$board.width() +'%';
				var box_top = ($('.question').position().top + $('.blank-space').position().top + $('.blank-space').height()/2)*100/$board.height() +'%';

				var init_left = ($(this).position().left + $textballoon.position().left)*100/$board.width() +'%';
				var init_top = ($(this).position().top + $textballoon.position().top)*100/$board.height() +'%';

				$(this).children('p').detach().css({
					'left': init_left,
					'top': init_top,
					'z-index': '100',
					'width': '14%',
					'color': '#8E7CC3',
				}).appendTo('.contentblock');

				$textballoon.animate({
					'left': box_left,
					'top': box_top,
				}, 1000, function(){
					$textballoon.hide(0);
					$('.blank-space').html($textballoon.html());
					play_correct_incorrect_sound(1);
					if(countNext != $total_page)
						$nextBtn.show(0);
				});

				$(".main-container").css('pointer-events', 'none');
				pop_sound.play();var $balloon = $(this);
				var dt = new Date();
				$(this).children('.balloon-icon').css({
					'z-index': 100,
					'height': '40%',
					'top': '20%',
					'width': '100%'}).attr('src', pop_star_src+'?' + dt.getTime());
				$(this).fadeOut(600, function(){
				});
			}
			else{
				if(!wrong_clicked){
					scoretemplate.update(false);
				}
				$(this).css('transform', 'scale(0.8)');
				$(this).children('.incorrect-icon').show(0);
				wrong_clicked = true;
				play_correct_incorrect_sound(0);
				wrong_clicked = true;
			}
		});
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
		scoretemplate.gotoNext();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

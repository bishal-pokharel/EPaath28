var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/page5/";

var sound_1 = new buzz.sound((soundAsset + "Recording1.ogg"));
var sound_2 = new buzz.sound((soundAsset + "Recording2.ogg"));
var sound_3 = new buzz.sound((soundAsset + "Recording3.ogg"));
var sound_4 = new buzz.sound((soundAsset + "Recording4.ogg"));
var sound_5 = new buzz.sound((soundAsset + "Recording5.ogg"));
var sound_6 = new buzz.sound((soundAsset + "Recording6.ogg"));
var sound_7 = new buzz.sound((soundAsset + "Recording7.ogg"));

// var ea_ee_gr = [sound_sea,sound_leaf,sound_tea, sound_bee,sound_tree,sound_teeth];

var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
	
		imagetextblock: [
			{
				imagediv: 'simple_img img_1',
				imgclass: 'boy',
				imgsrc: imgpath + "deepak.png",
				textdata : data.string.p5text1,
				textclass : 'my_font_medium sniglet',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_pn'
			},
			{
				imagediv: 'simple_img img_2',
				imgclass: 'girl',
				imgsrc: imgpath + "rita.png",
				textdata : data.string.p5text2,
				textclass : 'my_font_medium sniglet',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_pn'
			},
			{
				imagediv: 'simple_img img_3',
				imgclass: 'dog',
				imgsrc: imgpath + "puppy.png",
				textdata : data.string.p5text3,
				textclass : 'my_font_medium sniglet',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_pn'
			},
		]
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
		
		uppertextblockadditionalclass: 'center-text-2 my_font_medium',
		uppertextblock : [{
			textdata : data.string.p5text4,
			textclass : 'description',
		},
		{
			textdata : data.string.i,
			textclass : 'highlight_pn_1 its_hidden',
		},
		{
			textdata : data.string.p5text5,
			textclass : 'description its_hidden',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text_2'
		}],
	
		imagetextblock: [
			{
				imagediv: 'simple_img img_1',
				imgclass: 'boy',
				imgsrc: imgpath + "deepak.png",
				textdata : data.string.p5text1,
				textclass : 'my_font_medium sniglet',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_pn'
			},
			{
				imagediv: 'simple_img img_2',
				imgclass: 'girl',
				imgsrc: imgpath + "rita.png",
				textdata : data.string.p5text2,
				textclass : 'my_font_medium sniglet',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_pn'
			},
			{
				imagediv: 'simple_img img_3',
				imgclass: 'dog',
				imgsrc: imgpath + "puppy.png",
				textdata : data.string.p5text3,
				textclass : 'my_font_medium sniglet',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_pn'
			},
		],
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
		
		uppertextblockadditionalclass: 'center-text-2 my_font_medium',
		uppertextblock : [{
			textdata : data.string.p5text4,
			textclass : 'description',
		},
		{
			textdata : data.string.i,
			textclass : 'highlight_pn_1',
		},
		{
			textdata : data.string.p5text5,
			textclass : 'description',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text_2'
		}],
	
		imagetextblock: [
			{
				imagediv: 'simple_img img_1',
				imgclass: 'boy',
				imgsrc: imgpath + "deepak.png",
				textdata : data.string.p5text1,
				textclass : 'my_font_medium sniglet',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_pn'
			},
			{
				imagediv: 'simple_img img_2',
				imgclass: 'girl',
				imgsrc: imgpath + "rita.png",
				textdata : data.string.p5text2,
				textclass : 'my_font_medium sniglet',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_pn'
			},
			{
				imagediv: 'simple_img img_3',
				imgclass: 'dog',
				imgsrc: imgpath + "puppy.png",
				textdata : data.string.p5text3,
				textclass : 'my_font_medium sniglet',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_pn'
			},
		],
		lowertextblockadditionalclass: 'cover_board',
		lowertextblock : [{
			textdata : data.string.p5text6,
			textclass : 'speech-bubble bubble_1 my_font_big sniglet',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text_3'
		},
		{
			textdata : data.string.p5text7,
			textclass : 'speech-bubble bubble_2 my_font_big sniglet',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text_3'
		},
		{
			textdata : data.string.p5text8,
			textclass : 'speech-bubble bubble_3 my_font_big sniglet',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text_3'
		}],
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
		
		uppertextblockadditionalclass: 'center-text-2 my_font_medium',
		uppertextblock : [{
			textdata : data.string.p5text4,
			textclass : 'description',
		},
		{
			textdata : data.string.i,
			textclass : 'highlight_pn_1',
		},
		{
			textdata : data.string.p5text5,
			textclass : 'description',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text_2'
		}],
	
		imagetextblock: [
			{
				imagediv: 'simple_img img_1',
				imgclass: 'boy',
				imgsrc: imgpath + "deepak.png",
				textdata : data.string.p5text1,
				textclass : 'my_font_medium sniglet',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_pn'
			},
			{
				imagediv: 'simple_img img_2',
				imgclass: 'girl',
				imgsrc: imgpath + "rita.png",
				textdata : data.string.p5text2,
				textclass : 'my_font_medium sniglet',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_pn'
			},
			{
				imagediv: 'simple_img img_3',
				imgclass: 'dog',
				imgsrc: imgpath + "puppy.png",
				textdata : data.string.p5text3,
				textclass : 'my_font_medium sniglet',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_pn'
			},
		],
		
		lowertextblockadditionalclass: 'cover_board',
		lowertextblock : [{
			textdata : data.string.p5text9,
			textclass : 'speech-bubble bubble_4 my_font_medium sniglet',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text_3'
		},
		{
			textdata : data.string.p5text10,
			textclass : 'speech-bubble bubble_5 my_font_medium sniglet',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text_3'
		},
		{
			textdata : data.string.p5text11,
			textclass : 'speech-bubble bubble_6 my_font_medium sniglet',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text_3'
		}],
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var current_sound = sound_1;
	var my_timeouts = [];
	var timeoutvar = null;

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		
		switch (countNext) {
			case 1:
				$prevBtn.show(0);
				sound_player_new(sound_1);
				my_timeouts.push(setTimeout(function(){
					$('.its_hidden').eq(0).addClass('fade_in');
					my_timeouts.push(setTimeout(function(){
						$('.its_hidden').eq(1).addClass('fade_in');
					}, 4000));
				}, 500));
				// nav_button_controls(2000);
				break;
			case 2:
				$prevBtn.show(0);
				$('.bubble_1').fadeIn(500, function(){
					current_sound.stop();
					current_sound = sound_2;
					current_sound.play();
					current_sound.bindOnce('ended', function(){
						$('.bubble_2').delay(500).fadeIn(500, function(){
							current_sound.stop();
							current_sound = sound_3;
							current_sound.play();
							current_sound.bindOnce('ended', function(){
								$('.bubble_3').delay(500).fadeIn(500);
								current_sound.stop();
								current_sound = sound_4;
								current_sound.play();
								current_sound.bindOnce('ended', function(){
									$('.bubble_1').click(function(){
										sound_player(sound_2);
									});
									$('.bubble_2').click(function(){
										sound_player(sound_3);
									});
									$('.bubble_3').click(function(){
										sound_player(sound_4);
									});
									nav_button_controls(200);
								});
							});
						});
					});
				});
				break;
			case 3:
				$prevBtn.show(0);
				$('.bubble_4').fadeIn(500, function(){
					current_sound.stop();
					current_sound = sound_5;
					current_sound.play();
					current_sound.bindOnce('ended', function(){
						$('.bubble_5').delay(500).fadeIn(500, function(){
							current_sound.stop();
							current_sound = sound_6;
							current_sound.play();
							current_sound.bindOnce('ended', function(){
								$('.bubble_6').delay(500).fadeIn(500);
								current_sound.stop();
								current_sound = sound_7;
								current_sound.play();
								current_sound.bindOnce('ended', function(){
									$('.bubble_4').click(function(){
										sound_player(sound_5);
									});
									$('.bubble_5').click(function(){
										sound_player(sound_6);
									});
									$('.bubble_6').click(function(){
										sound_player(sound_7);
									});
									nav_button_controls(200);
								});
							});
						});
					});
				});
				break;
			default:
				nav_button_controls(500);
				break;
		}
	}
	
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}

	function sound_player_new(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bind('ended',()=>nav_button_controls(100));
	}
	
	

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   
	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				for (var i=0; i<my_timeouts.length; i++) {
					clearTimeout(my_timeouts[i]);
				}
				clearTimeout(timeoutvar);
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		clearTimeout(timeoutvar);
		for (var i=0; i<my_timeouts.length; i++) {
			clearTimeout(my_timeouts[i]);
		}
		current_sound.stop();
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();
	
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

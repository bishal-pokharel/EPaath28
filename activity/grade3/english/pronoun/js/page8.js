var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/page8/";


var sound_i = new buzz.sound((soundAsset + "i-1.ogg"));
var sound_i2 = new buzz.sound((soundAsset + "p8_s0.ogg"));
var sound_i3 = new buzz.sound((soundAsset + "i-2.ogg"));
var sound_5 = new buzz.sound((soundAsset + "p8_s4.ogg"));

var sound_he = new buzz.sound((soundAsset + "he-2.ogg"));
var sound_she = new buzz.sound((soundAsset + "she-2.ogg"));
var sound_it = new buzz.sound((soundAsset + "it-2.ogg"));

var sound_they = new buzz.sound((soundAsset + "they-2.ogg"));
var sound_we = new buzz.sound((soundAsset + "we-2.ogg"));

// var ea_ee_gr = [sound_sea,sound_leaf,sound_tea, sound_bee,sound_tree,sound_teeth];

var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "center_image_1 center_image",
						imgsrc : imgpath + "rita.png",
					},
					{
						imgclass : "center_image_2 center_image",
						imgsrc : imgpath + "deepak.png",
					},
					{
						imgclass : "center_image_3 center_image",
						imgsrc : imgpath + "puppy.png",
					}
				]
			}
		],
		extratextblock:[{
			textdata : data.string.i,
			textclass : 'highlight_pn i_position',
		}]
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "center_image_1 center_image",
						imgsrc : imgpath + "rita.png",
					},
					{
						imgclass : "center_image_2 center_image",
						imgsrc : imgpath + "deepak.png",
					},
					{
						imgclass : "center_image_3 center_image",
						imgsrc : imgpath + "puppy.png",
					}
				]
			}
		],
		extratextblock:[{
			textdata : data.string.we,
			textclass : 'bottom_label'
		},
		{
			textdata : data.string.i,
			textclass : 'highlight_pn i2_position',
		}]
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "center_image_1 center_image",
						imgsrc : imgpath + "rita.png",
					},
					{
						imgclass : "center_image_2 center_image",
						imgsrc : imgpath + "deepak.png",
					},
					{
						imgclass : "center_image_3 center_image",
						imgsrc : imgpath + "puppy.png",
					},
					{
						imgclass : "squirrel",
						imgsrc : imgpath + "lokharke.png",
					},
					{
						imgclass : "hand",
						imgsrc : imgpath + "sq_hand.png",
					}
				]
			}
		],
		extratextblock:[{
			textdata : data.string.we,
			textclass : 'bottom_label'
		},
		{
			textdata : data.string.i,
			textclass : 'highlight_pn i2_position',
		},
		{
			textdata : data.string.he1,
			textclass : 'highlight_pn he_position its_hidden',
		},
		{
			textdata : data.string.she1,
			textclass : 'highlight_pn she_position its_hidden',
		},
		{
			textdata : data.string.it1,
			textclass : 'highlight_pn it_position its_hidden',
		}]
	},

	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "center_image_1 center_image",
						imgsrc : imgpath + "rita.png",
					},
					{
						imgclass : "center_image_2 center_image",
						imgsrc : imgpath + "deepak.png",
					},
					{
						imgclass : "center_image_3 center_image",
						imgsrc : imgpath + "puppy.png",
					},
					{
						imgclass : "squirrel",
						imgsrc : imgpath + "lokharke.png",
					},
					{
						imgclass : "hand hand-02",
						imgsrc : imgpath + "sq_hand.png",
					}
				]
			}
		],
		extratextblock:[{
			textdata : data.string.we,
			textclass : 'bottom_label'
		},
		{
			textdata : data.string.i,
			textclass : 'highlight_pn i2_position',
		},
		{
			textdata : data.string.he1,
			textclass : 'highlight_pn he_position',
		},
		{
			textdata : data.string.she1,
			textclass : 'highlight_pn she_position',
		},
		{
			textdata : data.string.it1,
			textclass : 'highlight_pn it_position',
		},
		{
			textdata : data.string.they1,
			textclass : 'highlight_they its_hidden',
		}]
	},

	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "center_image_1 center_image",
						imgsrc : imgpath + "rita.png",
					},
					{
						imgclass : "center_image_2 center_image",
						imgsrc : imgpath + "deepak.png",
					},
					{
						imgclass : "center_image_3 center_image",
						imgsrc : imgpath + "puppy.png",
					}
				]
			}
		],
		extratextblock:[{
			textdata : data.string.we,
			textclass : 'bottom_label animate_pronoun_inf'
		},
		{
			textdata : data.string.i,
			textclass : 'highlight_pn i2_position animate_pronoun_inf',
		},
		{
			textdata : data.string.he1,
			textclass : 'highlight_pn he_position animate_pronoun_inf',
		},
		{
			textdata : data.string.she1,
			textclass : 'highlight_pn she_position animate_pronoun_inf',
		},
		{
			textdata : data.string.it1,
			textclass : 'highlight_pn it_position animate_pronoun_inf',
		},
		{
			textdata : data.string.they1,
			textclass : 'highlight_they animate_pronoun_inf',
		},
		{
			textdata : data.string.p8text1,
			textclass : 'top_text sniglet my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'top_highlight'
		}]
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var current_sound = sound_i;
	var my_timeout = null;
	var timeouts = [];

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
			case 0:
				$('.center_image_2').addClass('jump');
				$('.highlight_pn').addClass('animate_pronoun');
				timeouts.push(setTimeout(function(){
					$('.highlight_pn').removeClass('animate_pronoun');
				},1500));
				sound_player(sound_i);
				timeouts.push(setTimeout(function(){
					$('.center_image_1').addClass('jump');
					$('.highlight_pn').addClass('animate_pronoun');
					sound_player(sound_i3);
				},2000));
				timeouts.push(setTimeout(function(){
					$('.highlight_pn').removeClass('animate_pronoun');
				},3500));
				timeouts.push(setTimeout(function(){
					$('.center_image_3').addClass('jump');
					$('.highlight_pn').addClass('animate_pronoun');
					sound_player(sound_i2);
				},4000));
				timeouts.push(setTimeout(function(){
					$('.highlight_pn').removeClass('animate_pronoun');
				},5500));
	            nav_button_controls(6000);
				break;
			case 1:
				$prevBtn.show(0);
				$('.center_image').addClass('jump');
				$('.bottom_label').addClass('animate_pronoun');
				sound_player(sound_we);
				nav_button_controls(2000);
				break;
			case 2:
				$prevBtn.show(0);
				$('.squirrel, .hand').addClass('blink');
				$('.hand').addClass('hand-01');
				sound_player(sound_he);
				$('.center_image_2').addClass('jump');
				$('.he_position').delay(1000).fadeIn(500, function(){
					$('.squirrel, .hand').removeClass('blink');
					$('.he_position').addClass('animate_pronoun');
				});
				timeouts.push(setTimeout(function(){
					$('.squirrel, .hand').addClass('blink');
					sound_player(sound_she);
					$('.hand').removeClass('hand-01').addClass('hand-02');
					$('.center_image_1').addClass('jump');
					$('.she_position').delay(1000).fadeIn(500, function(){
						$('.squirrel, .hand').removeClass('blink');
						$('.she_position').addClass('animate_pronoun');
					});
				},2500));
				timeouts.push(setTimeout(function(){
					$('.squirrel, .hand').addClass('blink');
					$('.hand').removeClass('hand-02').addClass('hand-03');
					sound_player(sound_it);
					$('.center_image_3').addClass('jump');
					$('.it_position').delay(1000).fadeIn(500, function(){
						$('.squirrel, .hand').removeClass('blink');
						$('.it_position').addClass('animate_pronoun');
					});
				},5000));
				nav_button_controls(7000);
				break;
			case 3:
				$prevBtn.show(0);
				$('.squirrel, .hand').addClass('blink');
				sound_player(sound_they);
				$('.center_image').addClass('jump');
				$('.highlight_they').delay(1000).fadeIn(500, function(){
					$('.squirrel, .hand').removeClass('blink');
					$('.highlight_they').addClass('animate_pronoun');
					$prevBtn.show(0);
					$nextBtn.show(0);
				});
				break;
				case 4:
					sound_player(sound_5);
					nav_button_controls(4000);
				break;
			default:
	            nav_button_controls(1500);
				break;
		}
	}

	function nav_button_controls(delay_ms){
		my_timeout = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}


	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				clearTimeout(my_timeout);
				for (var i=0; i<timeouts.length; i++) {
					clearTimeout(timeouts[i]);
				}
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		clearTimeout(my_timeout);
		for (var i=0; i<timeouts.length; i++) {
			clearTimeout(timeouts[i]);
		}
		current_sound.stop();
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

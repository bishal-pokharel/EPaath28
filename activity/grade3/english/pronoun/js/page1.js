var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/page1/";

var sound_0 = new buzz.sound(soundAsset + "p1_s0.ogg");
var sound_1 = new buzz.sound(soundAsset + "Recording1.ogg");
var sound_2 = new buzz.sound(soundAsset + "Recording2.ogg");
var sound_3 = new buzz.sound(soundAsset + "Recording3.ogg");
var sound_4 = new buzz.sound(soundAsset + "Recording4.ogg");
var sound_5 = new buzz.sound(soundAsset + "Recording5.ogg");
var sound_6 = new buzz.sound(soundAsset + "Recording6.ogg");
var sound_7 = new buzz.sound(soundAsset + "he.ogg");
// var ea_ee_gr = [sound_sea,sound_leaf,sound_tea, sound_bee,sound_tree,sound_teeth];

var content = [
  //slide0
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "ole-background-gradient-paperlinings",

    uppertextblockadditionalclass: "center-text",
    uppertextblock: [
      {
        textdata: data.lesson.chapter,
        textclass: "lesson-title sniglet"
      }
    ]
  },

  // slide 1
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "main_bg",

    flipperBlock: [
      {
        balloonSrc: imgpath + "balloon.png",
        flippers: [
          {
            flipperID: "present-flipper",
            sides: [
              {
                sideClass: "front my_font_medium sniglet",
                titleText: data.string.pronoun,
                textdata: data.string.p1text1
              },
              {
                sideClass: "back my_font_medium sniglet",
                titleText: data.string.pronoun,
                textdata: data.string.p1text2
              }
            ]
          }
        ]
      }
    ]
  },

  // slide 2
  {
    uaudio: false,
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "",

    uppertextblockadditionalclass: "bottom-text my_font_big",
    uppertextblock: [
      {
        textdata: data.string.p1text3,
        textclass: "description"
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "center_image",
            imgsrc: imgpath + "deepak.png"
          }
        ]
      }
    ]
  },
  // slide 3
  {
    uaudio: false,
    laudio: false,
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "",

    uppertextblockadditionalclass: "bottom-text my_font_big",
    uppertextblock: [
      {
        textdata: data.string.p1text5,
        textclass: "description",
        datahighlightflag: true,
        datahighlightcustomclass: "highlight_name"
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "center_image",
            imgsrc: imgpath + "deepak.png"
          },
          {
            imgclass: "squirrel sq_image_1",
            imgsrc: imgpath + "lokharke.png"
          }
        ]
      }
    ],
    lowertextblockadditionalclass: "speech-bubble bubble_1 my_font_big sniglet",
    lowertextblock: [
      {
        textdata: data.string.p1text4,
        textclass: "",
        datahighlightflag: true,
        datahighlightcustomclass: "highlight_bubble"
      }
    ]
  },
  // slide 4
  {
    uaudio: false,
    laudio: false,
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "",

    uppertextblockadditionalclass: "bottom-text my_font_big",
    uppertextblock: [
      {
        textdata: data.string.p1text5,
        textclass: "description",
        datahighlightflag: true,
        datahighlightcustomclass: "highlight_name"
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "center_image",
            imgsrc: imgpath + "deepak.png"
          },
          {
            imgclass: "squirrel sq_image_2",
            imgsrc: imgpath + "lokharke.png"
          }
        ]
      }
    ],
    lowertextblockadditionalclass:
      "speech-bubble bubble_2 my_font_medium sniglet",
    lowertextblock: [
      {
        textdata: data.string.p1text6,
        textclass: "",
        datahighlightflag: true,
        datahighlightcustomclass: "highlight_bubble"
      }
    ]
  },
  // slide 5
  {
    uaudio: false,
    laudio: false,
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "",

    uppertextblockadditionalclass: "bottom-text my_font_big",
    uppertextblock: [
      {
        textdata: data.string.p1text5,
        textclass: "description",
        datahighlightflag: true,
        datahighlightcustomclass: "highlight_name"
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "center_image",
            imgsrc: imgpath + "deepak.png"
          },
          {
            imgclass: "squirrel sq_image_3",
            imgsrc: imgpath + "lokharke.png"
          }
        ]
      }
    ],
    lowertextblockadditionalclass:
      "speech-bubble bubble_3 my_font_medium sniglet",
    lowertextblock: [
      {
        textdata: data.string.p1text7,
        textclass: "",
        datahighlightflag: true,
        datahighlightcustomclass: "highlight_he_special"
      }
    ]
  }
];

$(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;
  var $total_page = content.length;
  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();
  var current_sound = sound_1;
  var my_timeout = null;
  var timeoutvar = null;

  /*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
  Handlebars.registerPartial(
    "definitioncontent",
    $("#definitioncontent-partial").html()
  );
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

  // controls the navigational state of the program
  // next btn is disabled for this page
  function navigationController(islastpageflag) {
    typeof islastpageflag === "undefined"
      ? (islastpageflag = false)
      : typeof islastpageflag != "boolean"
      ? alert(
          "NavigationController : Hi Master, please provide a boolean parameter"
        )
      : null;
    // if lastpageflag is true
    // islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
  }

  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);

    $board.html(html);
    loadTimelineProgress($total_page, countNext + 1);
    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    vocabcontroller.findwords(countNext);

    switch (countNext) {
      case 0:
        sound_0.play();
        $nextBtn.show(0);
        break;
      case 1:
        // $prevBtn.show(0);
        var clickcounter = 0;
        timeoutvar = setTimeout(function() {
          sound_player(sound_1);
          $("#present-flipper").on("click", function() {
            if (clickcounter % 2 == 0) {
              sound_player(sound_2);
              nav_button_controls(20000);
            } else {
              sound_player(sound_1);
            }
            clickcounter++;
            $("#present-flipper").toggleClass("doFlip");
          });
          // nav_button_controls(2000);
        }, 1000);
        break;
      case 2:
        // sound_player(sound_3);
        current_sound.stop();
        current_sound = sound_3;
        current_sound.play();
        current_sound.bindOnce("ended", function() {
          $(".textblock").click(function() {
            sound_player(sound_3);
          });
          nav_button_controls(0);
          $(".audio_icon").show(0);
        });
        // $prevBtn.show(0);
        break;
      case 3:
        current_sound = sound_4;
        sound_and_nav(sound_4, ".speech-bubble");
        $(".textblock").click(function() {
          sound_player(sound_3);
        });
        $(".speech-bubble").click(function() {
          sound_player(sound_4);
        });
        $prevBtn.hide(0);
        break;
      case 4:
        current_sound = sound_5;
        sound_and_nav(sound_5, ".speech-bubble");
        $(".textblock").click(function() {
          sound_player(sound_3);
        });
        $(".speech-bubble").click(function() {
          sound_player(sound_5);
        });
        $prevBtn.hide(0);
        break;
      case 5:
        sound_player(sound_6);
        $(".highlight_name:first-child").addClass("do_not_change");
        // $('.speech-bubble, .description').css('pointer-events', 'none');
        current_sound.bindOnce("ended", function() {
          //add this audio if needed
          sound_player(sound_7);
          $(".highlight_name:not(.do_not_change)").fadeOut(1000, function() {
            $(".highlight_name:not(.do_not_change)").html("He");
            $(".highlight_name:not(.do_not_change)").addClass(
              "highlight_pronoun"
            );
            $(".highlight_name:not(.do_not_change)").fadeIn(1000);
          });
          current_sound.bindOnce("ended", function() {
            $(".speech-bubble, .description").css("pointer-events", "all");
            $(".audio_icon").show(0);
            nav_button_controls(500);
            $(".description").click(function() {
              sound_player(sound_7);
            });
            $(".speech-bubble").click(function() {
              sound_player(sound_6);
            });
          });
        });
        $prevBtn.hide(0);
        break;
      default:
        nav_button_controls(500);
        break;
    }
  }

  function nav_button_controls(delay_ms) {
    my_timeout = setTimeout(function() {
      if (countNext == 0) {
        $nextBtn.show(0);
      } else if (countNext > 0 && countNext == $total_page - 1) {
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else {
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    }, delay_ms);
  }
  function sound_player(sound_data) {
    current_sound.stop();
    current_sound = sound_data;
    current_sound.play();
  }
  function sound_and_nav(sound_data, divclass) {
    $prevBtn.show(0);
    sound_player(sound_data);
    $(divclass).css("pointer-events", "none");
    current_sound.bindOnce("ended", function() {
      nav_button_controls(0);
      $(divclass).css({ "pointer-events": "all" });
      $(".audio_icon").show(0);
    });
  }

  function templateCaller() {
    //convention is to always hide the prev and next button and show them based
    //on the convention or page index
    $prevBtn.hide(0);
    $nextBtn.hide(0);
    navigationController();

    generalTemplate();
    /*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
  }

  $nextBtn.on("click", function() {
    switch (countNext) {
      default:
        clearTimeout(timeoutvar);
        clearTimeout(my_timeout);
        current_sound.stop();
        countNext++;
        templateCaller();
        break;
    }
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    clearTimeout(timeoutvar);
    clearTimeout(my_timeout);
    current_sound.stop();
    countNext--;
    templateCaller();
  });

  total_page = content.length;
  templateCaller();
});

/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
  //check if $highlightinside is provided
  typeof $highlightinside !== "object"
    ? alert(
        "Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted"
      )
    : null;

  var $alltextpara = $highlightinside.find("*[data-highlight='true']");
  var stylerulename;
  var replaceinstring;
  var texthighlightstarttag;
  var texthighlightendtag = "</span>";

  if ($alltextpara.length > 0) {
    $.each($alltextpara, function(index, val) {
      /*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
      $(this).attr(
        "data-highlightcustomclass"
      ) /*if there is data-highlightcustomclass defined it is true else it is not*/
        ? (stylerulename = $(this).attr("data-highlightcustomclass"))
        : (stylerulename = "parsedstring");

      texthighlightstarttag = "<span class = " + stylerulename + " >";

      replaceinstring = $(this).html();
      replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
      replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

      $(this).html(replaceinstring);
    });
  }
}

/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/page6/";

var sound_1 = new buzz.sound((soundAsset + "Recording1.ogg"));
var sound_2 = new buzz.sound((soundAsset + "Recording2.ogg"));
var sound_3 = new buzz.sound((soundAsset + "Recording3.ogg"));
var sound_4 = new buzz.sound((soundAsset + "Recording4.ogg"));
var sound_5 = new buzz.sound((soundAsset + "Recording5.ogg"));
var sound_6 = new buzz.sound((soundAsset + "Recording6.ogg"));
var sound_7 = new buzz.sound((soundAsset + "they.ogg"));

// var ea_ee_gr = [sound_sea,sound_leaf,sound_tea, sound_bee,sound_tree,sound_teeth];

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
	
		uppertextblockadditionalclass: 'center-text-1 my_font_big sniglet',
		uppertextblock : [{
			textdata : data.string.p6text1,
			textclass : 'description',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text_3'
		}],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "center_image_1 center_image",
						imgsrc : imgpath + "rita.png",
					},
					{
						imgclass : "center_image_2 center_image",
						imgsrc : imgpath + "deepak.png",
					},
					{
						imgclass : "center_image_3 center_image",
						imgsrc : imgpath + "puppy.png",
					}
				]
			}
		],
		lowertextblockadditionalclass: 'bottom_label',
		lowertextblock : [{
			textdata : data.string.qmark,
			textclass : 'highlight_pn'
		}],
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
	
		uppertextblockadditionalclass: 'center-text-1 my_font_big sniglet',
		uppertextblock : [{
			textdata : data.string.p6text1,
			textclass : 'description',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text_3'
		},
		{
			textdata : data.string.p6text2,
			textclass : 'description fade_in_2',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text_3'
		}],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "center_image_1 center_image",
						imgsrc : imgpath + "rita.png",
					},
					{
						imgclass : "center_image_2 center_image",
						imgsrc : imgpath + "deepak.png",
					},
					{
						imgclass : "center_image_3 center_image",
						imgsrc : imgpath + "puppy.png",
					}
				]
			}
		],
		lowertextblockadditionalclass: 'bottom_label',
		lowertextblock : [{
			textdata : data.string.they,
			textclass : 'highlight_pn'
		}],
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
	
		uppertextblockadditionalclass: 'center-text-1 my_font_big sniglet',
		uppertextblock : [{
			textdata : data.string.p6text1,
			textclass : 'description',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text_3'
		},
		{
			textdata : data.string.p6text2,
			textclass : 'description',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text_3'
		}],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "center_image_1 center_image",
						imgsrc : imgpath + "rita.png",
					},
					{
						imgclass : "center_image_2 center_image",
						imgsrc : imgpath + "deepak.png",
					},
					{
						imgclass : "center_image_3 center_image",
						imgsrc : imgpath + "puppy.png",
					}
				]
			}
		],
		lowertextblockadditionalclass: 'bottom_label',
		lowertextblock : [{
			textdata : data.string.they,
			textclass : 'highlight_pn'
		}],
		extratextblock:[{
			textdata : data.string.p6text3,
			textclass : 'they_text my_font_medium its_hidden',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text_4'
		}]
	},
	
	
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
	
		uppertextblockadditionalclass: 'center-text my_font_big sniglet',
		uppertextblock : [{
			textdata : data.string.p6text2,
			textclass : 'description',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_pn'
		}],
		imagetextblock: [
			{
				imagediv: 'image_tags img_tag_1 its_hidden',
				imgclass: 'domestic_img',
				imgsrc: imgpath + "domestic.png",
				textclass : "sniglet my_font_big domestic_p",
				textdata : data.string.p6text4,
			}
		]
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
	
		uppertextblockadditionalclass: 'center-text my_font_big sniglet',
		uppertextblock : [{
			textdata : data.string.p6text2,
			textclass : 'description',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_pn'
		}],
		imagetextblock: [
			{
				imagediv: 'image_tags img_tag_1',
				imgclass: 'domestic_img',
				imgsrc: imgpath + "domestic.png",
				textclass : "sniglet my_font_big domestic_p",
				textdata : data.string.p6text4,
			},
			{
				imagediv: 'image_tags img_tag_2 its_hidden',
				imgclass: '',
				imgsrc: imgpath + "stars.png",
				textclass : "sniglet my_font_big",
				textdata : data.string.p6text5,
			}
		]
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
	
		uppertextblockadditionalclass: 'center-text my_font_big sniglet',
		uppertextblock : [{
			textdata : data.string.p6text2,
			textclass : 'description',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_pn'
		}],
		imagetextblock: [
			{
				imagediv: 'image_tags img_tag_1',
				imgclass: 'domestic_img',
				imgsrc: imgpath + "domestic.png",
				textclass : "sniglet my_font_big domestic_p",
				textdata : data.string.p6text4,
			},
			{
				imagediv: 'image_tags img_tag_2',
				imgclass: '',
				imgsrc: imgpath + "stars.png",
				textclass : "sniglet my_font_big",
				textdata : data.string.p6text5,
			},
			{
				imagediv: 'image_tags img_tag_3 its_hidden',
				imgclass: '',
				imgsrc: imgpath + "happy.png",
				textclass : "sniglet my_font_big",
				textdata : data.string.p6text6,
			}
		]
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var current_sound = sound_1;
	var my_timeout = null;

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		
		switch (countNext) {
			case 0:
				sound_player_new(sound_1);
				// nav_button_controls(3000);
				break;
			case 1:
				$prevBtn.show(0);
				sound_player(sound_7);
				nav_button_controls(500);
				break;
			case 2:
			current_sound.stop();
			current_sound = sound_2;
			current_sound.play();
			current_sound.bind('ended', function(){
				$('.they_text').fadeIn(1000);
				sound_3.play();
				sound_3.bind('ended',()=>nav_button_controls(100));
			});
			break;
			case 3:
				$prevBtn.show(0);
				$('.its_hidden').fadeIn(1000, function(){
					sound_player_new(sound_4);
					// nav_button_controls(1500);
				});
				break;
			case 4:
				$prevBtn.show(0);
				$('.its_hidden').fadeIn(1000, function(){
					sound_player(sound_5);
					nav_button_controls(1500);
				});
				break;
			case 5:
				$prevBtn.show(0);
				$('.its_hidden').fadeIn(1000, function(){
					sound_player(sound_6);
					nav_button_controls(1500);
				});
				break;
			default:
				nav_button_controls(1500);
				break;
		}
	}
	
	function nav_button_controls(delay_ms){
		my_timeout = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}
	function sound_player_new(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bind('ended',()=>nav_button_controls(100));
	}
	
	

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   
	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				clearTimeout(my_timeout);
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		clearTimeout(my_timeout);
		current_sound.stop();
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();
	
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

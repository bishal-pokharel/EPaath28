var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/page2/";

var sound_1 = new buzz.sound((soundAsset + "Recording1.ogg"));
var sound_2 = new buzz.sound((soundAsset + "Recording2.ogg"));
var sound_3 = new buzz.sound((soundAsset + "Recording3.ogg"));
var sound_4 = new buzz.sound((soundAsset + "Recording4.ogg"));

// var ea_ee_gr = [sound_sea,sound_leaf,sound_tea, sound_bee,sound_tree,sound_teeth];

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
	
		uppertextblockadditionalclass: 'center-text my_font_big',
		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : 'description sniglet',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_pn'
		}],
		imagetextblock: [
			{
				imagediv: 'image_tags img_tag_1 its_hidden',
				imgclass: '',
				imgsrc: imgpath + "boy01.png",
				textclass : "sniglet my_font_big",
				textdata : data.string.p2text2,
			},
		]
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
	
		uppertextblockadditionalclass: 'center-text my_font_big',
		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : 'description sniglet',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_pn'
		}],
		imagetextblock: [
			{
				imagediv: 'image_tags img_tag_1',
				imgclass: '',
				imgsrc: imgpath + "boy01.png",
				textclass : "sniglet my_font_big",
				textdata : data.string.p2text2,
			},
			{
				imagediv: 'image_tags img_tag_2 fade_in',
				imgclass: '',
				imgsrc: imgpath + "police.png",
				textclass : "sniglet my_font_big",
				textdata : data.string.p2text3,
			}
		]
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
	
		uppertextblockadditionalclass: 'center-text my_font_big',
		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : 'description sniglet',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_pn'
		}],
		imagetextblock: [
			{
				imagediv: 'image_tags img_tag_1',
				imgclass: '',
				imgsrc: imgpath + "boy01.png",
				textclass : "sniglet my_font_big",
				textdata : data.string.p2text2,
			},
			{
				imagediv: 'image_tags img_tag_2',
				imgclass: '',
				imgsrc: imgpath + "police.png",
				textclass : "sniglet my_font_big",
				textdata : data.string.p2text3,
			},
			{
				imagediv: 'image_tags img_tag_3 fade_in',
				imgclass: '',
				imgsrc: imgpath + "boy02.png",
				textclass : "sniglet my_font_big",
				textdata : data.string.p2text4,
			}
		]
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var current_sound = sound_1;
	var timeoutvar = null;

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		
		switch (countNext) {
			case 0:
				current_sound.stop();
				current_sound = sound_1;
				current_sound.play();
				current_sound.bindOnce('ended', function(){
					$('.img_tag_1').fadeIn(1000);
					nav_button_controls(1500);
					sound_player(sound_2);
				});
				break;
			case 1:
				$prevBtn.show(0);
				sound_player(sound_3);
				nav_button_controls(1500);
				break;
			case 2:
				$prevBtn.show(0);
				sound_player(sound_4);
				nav_button_controls(1500);
				break;
			default:
				$prevBtn.show(0);
				nav_button_controls(1500);
				break;
		}
	}
	
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}
	

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   
	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				clearTimeout(timeoutvar);
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}
		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		clearTimeout(timeoutvar);
		current_sound.stop();
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	total_page = content.length;
	templateCaller();
	
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

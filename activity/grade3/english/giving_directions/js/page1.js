var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";


var sound_1 = new buzz.sound((soundAsset + "page1/recording1.ogg"));


var sound_2 = new buzz.sound((soundAsset + "page1/recording2.ogg"));
var sound_3 = new buzz.sound((soundAsset + "page1/recording3.ogg"));

var sound_4 = new buzz.sound((soundAsset + "page1/recording4.ogg"));
var sound_5 = new buzz.sound((soundAsset + "page1/recording5.ogg"));

var sound_6 = new buzz.sound((soundAsset + "page1/recording6.ogg"));
var sound_7 = new buzz.sound((soundAsset + "page1/recording7.ogg"));

var dir_sounds = [sound_1,sound_2, sound_3, sound_4,sound_5, sound_6, sound_7];

// var ea_ee_gr = [sound_sea,sound_leaf,sound_tea, sound_bee,sound_tree,sound_teeth];

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'grassbg',

		uppertextblock : [{
			textdata : data.lesson.chapter,
			textclass : 'lesson-title sniglet'
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "frontimg",
					imgsrc : imgpath + "map.png",
				}
			],
		}]
	},

	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'main_bg',

		uppertextblockadditionalclass: 'blue_heading sniglet',
		uppertextblock : [{
			textdata : data.string.directions,
			textclass : 'my_font_big'
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "center_img",
					imgsrc : imgpath + "straight.png",
				},
				{
					imgclass : "left_img",
					imgsrc : imgpath + "left.png",
				},
				{
					imgclass : "right_img",
					imgsrc : imgpath + "right.png",
				}
			],
		}]
	},

    // slide 2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'main_bg',

		uppertextblockadditionalclass: 'blue_heading sniglet',
		uppertextblock : [{
			textdata : data.string.directions,
			textclass : 'my_font_big'
		}],

		lowertextblockadditionalclass: 'yellow_banner sniglet',
		lowertextblock : [{
			textdata : data.string.straight,
			textclass : 'my_font_big'
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "center_img",
					imgsrc : imgpath + "straight.png",
				},
				{
					imgclass : "center_bottom",
					imgsrc : imgpath + "c_straight.png",
				}
			],
		}],

	    flipperblock: [
		    {
		    	flipperblockadditionalclass: 'flipper-bottom my_font_medium',
		        flippers: [{
		            flipperID: 'direction-flipper',
		            sides: [{
		                sideClass: 'front',
		                textdata: data.string.p1text1
		            }, {
		                sideClass: 'back',
		                textdata: data.string.p1text2
		            }]
		        }]
		    }
		]
	},
	// slide 3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'main_bg',

		uppertextblockadditionalclass: 'blue_heading sniglet',
		uppertextblock : [{
			textdata : data.string.directions,
			textclass : 'my_font_big'
		}],

		lowertextblockadditionalclass: 'yellow_banner sniglet',
		lowertextblock : [{
			textdata : data.string.left,
			textclass : 'my_font_big'
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "center_img",
					imgsrc : imgpath + "left.png",
				},
				{
					imgclass : "center_bottom",
					imgsrc : imgpath + "c_left.png",
				}
			],
		}],

	    flipperblock: [
		    {
		    	flipperblockadditionalclass: 'flipper-bottom my_font_medium',
		        flippers: [{
		            flipperID: 'direction-flipper',
		            sides: [{
		                sideClass: 'front',
		                textdata: data.string.p1text3
		            }, {
		                sideClass: 'back',
		                textdata: data.string.p1text4
		            }]
		        }]
		    }
		]
	},
	// slide 4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'main_bg',

		uppertextblockadditionalclass: 'blue_heading sniglet',
		uppertextblock : [{
			textdata : data.string.directions,
			textclass : 'my_font_big'
		}],

		lowertextblockadditionalclass: 'yellow_banner sniglet',
		lowertextblock : [{
			textdata : data.string.right,
			textclass : 'my_font_big'
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "center_img",
					imgsrc : imgpath + "right.png",
				},
				{
					imgclass : "center_bottom",
					imgsrc : imgpath + "c_right.png",
				}
			],
		}],

	    flipperblock: [
		    {
		    	flipperblockadditionalclass: 'flipper-bottom my_font_medium',
		        flippers: [{
		            flipperID: 'direction-flipper',
		            sides: [{
		                sideClass: 'front',
		                textdata: data.string.p1text5
		            }, {
		                sideClass: 'back',
		                textdata: data.string.p1text6
		            }]
		        }]
		    }
		]
	},
	// slide 5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'grassbg',

		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "arrows arrows_1",
						imgsrc : imgpath + "arrows.png",
					},
					{
						imgclass : "car car_up",
						imgsrc : imgpath + "yellowcar.png",
					},
				],
				imagelabels : [
					{
						imagelabelclass : "road_horizontal",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "road_vertical",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "label_dir label_up my_font_big",
						imagelabeldata : data.string.straight,
					},
					{
						imagelabelclass : "label_dir label_left my_font_big",
						imagelabeldata : data.string.left,
					},
					{
						imagelabelclass : "label_dir label_right my_font_big",
						imagelabeldata : data.string.right,
					}
				]
			}
		],
	},
	// slide 6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'grassbg',

		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "arrows arrows_2",
						imgsrc : imgpath + "arrows.png",
					},
					{
						imgclass : "car car_down",
						imgsrc : imgpath + "yellowcar.png",
					},
				],
				imagelabels : [
					{
						imagelabelclass : "road_horizontal",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "road_vertical",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "label_dir label_down my_font_big",
						imagelabeldata : data.string.straight,
					},
					{
						imagelabelclass : "label_dir label_left my_font_big",
						imagelabeldata : data.string.right,
					},
					{
						imagelabelclass : "label_dir label_right my_font_big",
						imagelabeldata : data.string.left,
					}
				]
			}
		],
	},
	// slide 7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'grassbg',

		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "arrows arrows_3",
						imgsrc : imgpath + "arrows.png",
					},
					{
						imgclass : "car car_right",
						imgsrc : imgpath + "yellowcar.png",
					},
				],
				imagelabels : [
					{
						imagelabelclass : "road_horizontal",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "road_vertical",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "label_dir label_right my_font_big",
						imagelabeldata : data.string.straight,
					},
					{
						imagelabelclass : "label_dir label_up my_font_big",
						imagelabeldata : data.string.left,
					},
					{
						imagelabelclass : "label_dir label_down my_font_big",
						imagelabeldata : data.string.right,
					}
				]
			}
		],
	},
	// slide 8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'grassbg',

		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "arrows arrows_4",
						imgsrc : imgpath + "arrows.png",
					},
					{
						imgclass : "car car_left",
						imgsrc : imgpath + "yellowcar.png",
					},
				],
				imagelabels : [
					{
						imagelabelclass : "road_horizontal",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "road_vertical",
						imagelabeldata : '',
					},
					{
						imagelabelclass : "label_dir label_left my_font_big",
						imagelabeldata : data.string.straight,
					},
					{
						imagelabelclass : "label_dir label_down my_font_big",
						imagelabeldata : data.string.left,
					},
					{
						imagelabelclass : "label_dir label_up my_font_big",
						imagelabeldata : data.string.right,
					}
				]
			}
		],
	}
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var current_sound = sound_1;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
		case 0:
			sound_player(sound_1);
			nav_button_controls(500);
			break;
		case 2:
			var clickcounter = 0;
	 		sound_player(dir_sounds[1]);
			$('#direction-flipper').addClass('bounce');
			$('#direction-flipper').on('click', function() {
				$('#direction-flipper').removeClass('bounce');
				if(clickcounter%2==0){
		 			sound_player(dir_sounds[2]);
					nav_button_controls(6000);

				} else {
					sound_player(dir_sounds[1]);
				}
				clickcounter++;
				$('#direction-flipper').toggleClass('doFlip');
            });
			break;
		case 3:
			var clickcounter = 0;
	 		sound_player(dir_sounds[3]);
			$('#direction-flipper').on('click', function() {
				if(clickcounter%2==0){
		 			sound_player(dir_sounds[4]);
				} else {
					sound_player(dir_sounds[3]);
				}
				clickcounter++;
				$('#direction-flipper').toggleClass('doFlip');
            });
            nav_button_controls(1500);
			break;
		case 4:
			var clickcounter = 0;
	 		sound_player(dir_sounds[5]);
			$('#direction-flipper').on('click', function() {
				if(clickcounter%2==0){
		 			sound_player(dir_sounds[6]);
				} else {
					sound_player(dir_sounds[5]);
				}
				clickcounter++;
				$('#direction-flipper').toggleClass('doFlip');
            });
            nav_button_controls(1500);
			break;
		default:
            nav_button_controls(500);
			break;
		}
	}

	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}


	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		current_sound.stop();
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

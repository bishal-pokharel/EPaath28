var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_1 = new buzz.sound((soundAsset + "page3/left.ogg"));
var sound_2 = new buzz.sound((soundAsset + "page3/straight.ogg"));
var sound_3 = new buzz.sound((soundAsset + "page3/right.ogg"));

var sound_c = new buzz.sound((soundAsset + "correct.ogg"));
var sound_w = new buzz.sound((soundAsset + "wrong.ogg"));

var s3_p1 = new buzz.sound((soundAsset + "page3/s3_p1.ogg"))
var s3_p2 = new buzz.sound((soundAsset + "page3/s3_p2.ogg"))
var s3_p3 = new buzz.sound((soundAsset + "page3/s3_p3.ogg"))
var s3_p4 = new buzz.sound((soundAsset + "page3/s3_p4.ogg"))
var s3_p5 = new buzz.sound((soundAsset + "page3/s3_p5.ogg"))
var s3_p6 = new buzz.sound((soundAsset + "page3/s3_p6.ogg"))
var s3_p7 = new buzz.sound((soundAsset + "page3/s3_p7.ogg"))
var s3_p8 = new buzz.sound((soundAsset + "page3/s3_p8.ogg"))

var dirsounds = [sound_1, sound_2, sound_3];

var possible_moves = [
						[0, [-1,1,0], [0,-1,1], [0,0,-1] ],
						[1, [1,-1,0], [0,1,-1] ],
						[2, [1,-1,0,1], [0,0,1,0] ],
						[3, [0,0,1] ],
						[4, [0,1,0] ],
					];
possible_moves.shufflearray();

var content = [
	// slide 0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		inputblock:[{
			inputblockadditionalclass: 'block_purple',
			text:[{
				textdata : data.string.p3text1,
				textclass : 'sniglet my_font_big text_1'
			}],
			inputclass: 'name-input sniglet my_font_big',
			button:[{
				buttonclass: 'button-yellow sniglet my_font_big button-1',
				buttondata: data.string.p3text2
			}]
		}],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "bg_map",
						imgsrc : imgpath + "bg.png",
					},
				],
			}
		],
	},
	// slide 1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		inputblock:[{
			inputblockadditionalclass: 'block_image block_image-1',
			text:[{
				textdata : data.string.p3text3,
				textclass : 'sniglet my_font_big text_1',
				datahighlightflag : true,
				datahighlightcustomclass : 'uname',
			},
			{
				textdata : data.string.p3text4,
				textclass : 'sniglet my_font_big text_1',
			}],
			image:[
				{
					imagestoshow : [
						{
							imgclass : "girl-left",
							imgsrc : imgpath + "asha02.png",
						},
					],
				}
			]
		}],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "bg_map",
						imgsrc : imgpath + "bg.png",
					},
				],
			}
		],
	},
	// slide 2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		inputblock:[{
			inputblockadditionalclass: 'block_image block_image-2',
			text:[{
				textdata : data.string.p3text8,
				textclass : 'sniglet my_font_big text_1',
			},
			{
				textdata : data.string.p3text9,
				textclass : 'sniglet my_font_big text_1',
			}],
			image:[
				{
					imagestoshow : [
						{
							imgclass : "girl-left",
							imgsrc : imgpath + "asha01.png",
						},{
							imgclass : "art-top",
							imgsrc : imgpath + "artfair.png",
						},
						{
							imgclass : "group-bottom",
							imgsrc : imgpath + "group-of-kids.png",
						},
					],
				}
			]
		}],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "bg_map",
						imgsrc : imgpath + "bg.png",
					},
				],
			}
		],
	},
	// slide 4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		inputblock:[{
			inputblockadditionalclass: 'block_image block_image-4',
			text:[{
				textdata : data.string.p3text10,
				textclass : 'sniglet my_font_big text_1',
			},
			{
				textdata : data.string.p3text11,
				textclass : 'sniglet my_font_big text_1',
			}],
			image:[
				{
					imagestoshow : [
						{
							imgclass : "girl-left",
							imgsrc : imgpath + "asha01.png",
						},
						{
							imgclass: 'taxi-bot',
							imgsrc : imgpath + "car.png",
						}
					],
				}
			]
		}],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "bg_map",
						imgsrc : imgpath + "bg.png",
					},
				],
			}
		],
	},
	// slide 4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		inputblock:[{
			inputblockadditionalclass: 'block_image block_image-5',
			text:[{
				textdata : data.string.p3text12,
				textclass : 'sniglet my_font_big text_1',
			}],
			image:[
				{
					imagestoshow : [
						{
							imgclass : "girl-left",
							imgsrc : imgpath + "asha01.png",
						},
					],
				}
			],
			button:[{
				buttonclass: 'button-blue sniglet my_font_big button-2',
				buttondata: data.string.p3text17
			}]
		}],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "bg_map",
						imgsrc : imgpath + "bg.png",
					},
				],
			}
		],
	},
	// slide 5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		inputblock:[{
			inputblockadditionalclass: 'miniblock miniblock-1',
			text:[{
				textdata : data.string.p3text18,
				textclass : 'sniglet my_font_medium text_1',
			}],
			button:[{
				buttonclass: 'button-blue sniglet my_font_medium button-3',
				buttondata: data.string.p3text19
			}]
		}],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "bg_map",
						imgsrc : imgpath + "bg.png",
					},
				],
			}
		],
	},
	// slide 6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'main_bg_2',

		uppertextblockadditionalclass: 'slider_text',
		uppertextblock : [{
			textdata : '',
			textclass : 'my_font_medium sniglet'
		}],
		inputblock:[{
			inputblockadditionalclass: 'miniblock start-pick',
			text:[{
				textdata : data.string.p3text20,
				textclass : 'sniglet my_font_medium text_1',
			}],
			button:[{
				buttonclass: 'button-blue sniglet my_font_medium button-3 button-start-pick',
				buttondata: data.string.p3text19
			}]
		},{
			inputblockadditionalclass: 'miniblock go_b',
			text:[{
				textdata : data.string.p3text21,
				textclass : 'sniglet my_font_medium text_1',
			}],
			button:[{
				buttonclass: 'button-blue sniglet my_font_medium button-3',
				buttondata: data.string.p3text19
			}]
		},{
			inputblockadditionalclass: 'miniblock drop-pick',
			text:[{
				textdata : data.string.p3text23,
				textclass : 'sniglet my_font_medium text_1',
			}],
			button:[{
				buttonclass: 'button-blue sniglet my_font_medium button-3 button-drop-pick',
				buttondata: data.string.p3text19
			}]
		},{
			inputblockadditionalclass: 'replay_t',
			text:[{
				textdata : data.string.p3text22,
				textclass : 'sniglet my_font_medium text_1',
			}],
			button:[{
				buttonclass: 'button-blue sniglet my_font_medium button-yes',
				buttondata: data.string.p3yes
			},{
				buttonclass: 'button-blue sniglet my_font_medium button-no',
				buttondata: data.string.p3no
			}]

		}],

		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "car car_up",
						imgsrc : imgpath + "car.png",
					},
					{
						imgclass : "bubble-head",
						imgsrc : imgpath + "deepa2.png",
					},
					{
						imgclass : "bg_map",
						imgsrc : imgpath + "bg.png",
					},
					{
						imgclass : "bottom_arrows central_arrow",
						imgsrc : imgpath + "straight.png",
					},
					{
						imgclass : "bottom_arrows central_arrow_left",
						imgsrc : imgpath + "left.png",
					},
					{
						imgclass : "bottom_arrows central_arrow_right",
						imgsrc : imgpath + "right.png",
					},
					{
						imgclass : "location",
						imgsrc : imgpath + "location.png",
					}
				],
				imagelabels : [
					{
						imagelabelclass : "bottom_ins",
						imagelabeldata : '',
					}
				]
			}
		],
	},
	// slide 7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		inputblock:[{
			inputblockadditionalclass: 'block_image block_image-2',
			text:[{
				textdata : data.string.p3text24,
				textclass : 'sniglet my_font_big text_1',
				datahighlightflag : true,
				datahighlightcustomclass : 'uname',
			},],
			image:[
				{
					imagestoshow : [
						{
							imgclass : "girl-left",
							imgsrc : imgpath + "asha01.png",
						},
						{
							imgclass : "group-bottom",
							imgsrc : imgpath + "group-of-kids.png",
						},
					],
				}
			]
		}],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "bg_map",
						imgsrc : imgpath + "bg.png",
					},
				],
			}
		],
	}
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var current_sound = sound_1;

	var uname = '';
	var current_direction = 0;
	var current_rotation = 0;
	var prev_straight = 1;
	var array_dir = [];
	var return_dir = [];
	var direction_commands = [data.string.p3text7, data.string.p3text5, data.string.p3text6];
	var current_idx = 0;

	var offset_x = 0;
	var offset_y = 0;
	var click_counter = 0;
	var	my_go_back = false;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		is_last_page= false;
		var click_count = 0;
		offset_x = 0;

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
			case 0:
				sound_player_sec(s3_p1, 0);
				$('.button-1').click(function(){
					current_sound.stop();
					uname = $('.name-input').val();
					if(uname.length != 0){
						$nextBtn.show(0);
					} else{
						$nextBtn.hide(0);
					}
				});
				break;
			case 1:
				$prevBtn.show(0);
					sound_player_sec(s3_p2, 1);
				$('.uname').html(uname);
				// nav_button_controls(200);
				break;
			case 2:
				sound_player_sec(s3_p3, 1);
			break;
			case 3:
				sound_player_sec(s3_p4, 1);
			break;
			case 4:
			case 5:
				countNext==4?sound_player_sec(s3_p5, 0):sound_player_sec(s3_p6, 0);
				$prevBtn.show(0);
				$('.button-blue').click(function(){
					current_sound.stop();
					$nextBtn.trigger('click');
				});
				break;
			case 6:
				my_init();
				var my_checker = 0;
				var last_str = 0;

				sound_player_sec(s3_p7, 0);
				$('.location').addClass('location_'+possible_moves[current_idx][0]);
				$('.location').attr('src', imgpath + "head0"+possible_moves[current_idx][0]+".png");
				current_direction = 0;

				var left_div_pos = 2 + ($('.location').position().left + $('.location').width())/$board.width()*100;
				var top_div_pos = 4.5;
				if(possible_moves[current_idx][0]==4){
					top_div_pos = 30.5;
				}
				if(($('.location').position().left)/$board.width()*100 > 30){
					left_div_pos = -2 + ($('.location').position().left - $('.start-pick').width())/$board.width()*100;
				}

				$('.start-pick').css({
					'left': left_div_pos +'%',
					'top' : top_div_pos + '%'
				});

				var rand_from_movesarr = 1 + Math.floor( (possible_moves[current_idx].length-1) *Math.random() );
				// console.log(possible_moves, current_idx, rand_from_movesarr, new_moves);
				var new_moves = possible_moves[current_idx][rand_from_movesarr];
				array_dir = new_moves;
				for(var mm=0; mm<array_dir.length; mm++){
					if(array_dir[mm]!=0){
						array_dir.splice(mm+1,0,0);
					}
				}
				if(array_dir[0]==0){
					return_dir.push(0);
				}

				$('.bottom_arrows').addClass('highlight_image');
				$('.bottom_arrows').mouseenter(function(){
					$('.bottom_arrows').removeClass('highlight_image');
				});

				// for click functions
				$('.central_arrow').click(function(){
					if(click_counter < array_dir.length){
						my_checker = array_dir[click_counter];
					} else{
						my_checker = return_dir[click_counter-array_dir.length];
						if( (click_counter-array_dir.length)== return_dir.length-1 ){
							last_str = 1;
						}
					}
					if(my_checker==0){
						switch(current_direction){
							case -1:
								if(!prev_straight){
									offset_x -=  1050;
								} else{
									offset_x -=  1370;
								}
								break;
							case 0:
								if(!prev_straight){
									offset_y -= 130;
								} else{
									offset_y -= 370;
								}
								break;
							case 1:
								if(!prev_straight){
									offset_x +=  1050;
								} else{
									offset_x +=  1370;
								}
								break;
							default:
								if(!last_str){
									if(!prev_straight){
										offset_y += 130;
									} else{
										offset_y += 370;
									}
								} else {
									offset_y +=240;
								}
								break;
						}
						$('.car').css({'transform': 'translate('+offset_x+'%,'+ offset_y+'%) rotate('+ current_rotation*90 +'deg)'});
						sound_player(sound_c);
						prev_straight = 1;
						click_counter++;
						$('.bottom_arrows').css('pointer-events','none');
						if(click_counter <= array_dir.length){
							return_dir.splice(0,0,0);
							setTimeout(function(){
								give_direction(array_dir[click_counter], true);
							}, 2000);
						} else {
							setTimeout(function(){
								give_direction(return_dir[click_counter-array_dir.length], true);
							}, 2000);
						}
					} else{
						play_correct_incorrect_sound(0);
					}

				});

				$('.central_arrow_left').click(function(){
					if(click_counter < array_dir.length){
						my_checker = array_dir[click_counter];
					} else{
						my_checker = return_dir[click_counter-array_dir.length];
					}
					if(my_checker==-1){
						switch(current_direction){
							case -1:
								offset_x -=  220;
								offset_y += 120;
								break;
							case 0:
								offset_x -=  150;
								offset_y -= 120;
								break;
							case 1:
								offset_x +=  220;
								offset_y -= 120;
								break;
							default:
								offset_x +=  150;
								offset_y += 120;
								break;
						}
						current_rotation--;
						current_direction--;
						if(current_direction<-2){
							current_direction=1;
						}
						$('.car').css({'transform': 'translate('+offset_x+'%,'+ offset_y+'%) rotate('+ current_rotation*90 +'deg)'});
						sound_player(sound_c);
						prev_straight = 0;
						click_counter++;
						$('.bottom_arrows').css('pointer-events','none');
						if(click_counter <= array_dir.length){
							return_dir.splice(0,0,1);
							setTimeout(function(){
								give_direction(array_dir[click_counter], true);
							}, 2000);
						} else {
							setTimeout(function(){
								give_direction(return_dir[click_counter-array_dir.length], true);
							}, 2000);
						}
					} else{
						play_correct_incorrect_sound(0);

					}
				});

				$('.central_arrow_right').click(function(){
					if(click_counter < array_dir.length){
						my_checker = array_dir[click_counter];
					} else{
						my_checker = return_dir[click_counter-array_dir.length];
					}
					if(my_checker==1){
						switch(current_direction){
							case -1:
								offset_x -=  220;
								offset_y -= 120;
								break;
							case 0:
								offset_x +=  150;
								offset_y -= 120;
								break;
							case 1:
								offset_x +=  220;
								offset_y += 120;
								break;
							default:
								offset_x -=  150;
								offset_y += 120;
								break;
						}
						current_rotation++;
						current_direction++;
						if(current_direction>2){
							current_direction=-1;
						}
						$('.car').css({'transform': 'translate('+offset_x+'%,'+ offset_y+'%) rotate('+ current_rotation*90 +'deg)'});
						sound_player(sound_c);
						prev_straight = 0;
						click_counter++;
						$('.bottom_arrows').css('pointer-events','none');
						if(click_counter <= array_dir.length){
							return_dir.splice(0,0,-1);
							setTimeout(function(){
								give_direction(array_dir[click_counter], true);
							}, 2000);
						} else {
							setTimeout(function(){
								give_direction(return_dir[click_counter-array_dir.length], true);
							}, 2000);
						}
					} else{
						play_correct_incorrect_sound(0);

					}
				});
				//start
				$('.bottom_arrows').css('pointer-events','none');
				$('.slider_text').hide(0);
				$('.button-start-pick').click(function(){
					give_direction(array_dir[click_counter], true);
					$('.bottom_arrows').css('pointer-events','all');
					$('.start-pick').hide(0);
					$('.slider_text').show(0);
				});

				//collect ok click
				$('.go_b>button').click(function(){
					my_go_back = true;
					$('.go_b').hide(0);
					give_direction(return_dir[0], true);
					setTimeout(function(){
						$('.bottom_arrows').css('pointer-events','all');
					}, 500);

					//bubblehead-location animations
					$('.car').css('transition','0s');
					$('.car').css({'transform': 'translate('+offset_x+'%,'+ offset_y+'%) rotate('+ current_rotation*90 +'deg)'});

					if( possible_moves[current_idx][0] < 2 ){
						if( rand_from_movesarr < 3 ){
							$('.location').addClass('location-anim-1');
						} else {
							$('.location').addClass('location-anim-2');
						}
					} else if( possible_moves[current_idx][0] < 4 ){
						$('.location').addClass('location-anim-3');
					} else {
						$('.location').addClass('location-anim-4');
					}

					setTimeout(function(){
						$('.car').css('transition','2s linear');
					}, 25);
				});
				//drop after pick click
				$('.button-drop-pick').click(function(){
					$('.bottom_arrows').addClass('disabled_div');
					$('.drop-pick').hide(0);
					$('.location').removeClass('location-anim-1').removeClass('location-anim-2').removeClass('location-anim-3').removeClass('location-anim-4');
					$('.location').addClass('location-anim-end');
					setTimeout(function(){
						$('.replay_t').show(0);
					}, 2000);
				});
				$('.replay_t>.button-yes').click(function(){
					current_idx += 1;
					if(current_idx > 4){
						current_idx = 0;
					}
					current_sound.stop();
					countNext=6;
					templateCaller();
				});
				$('.replay_t>.button-no').click(function(){
					current_sound.stop();
					countNext=7;
					templateCaller();
				});
				break;
			case 7:
				$prevBtn.show(0);
				sound_player_sec(s3_p8, 1);
				$('.uname').html(uname);
				// nav_button_controls(200);
				break;
			default:
				$prevBtn.show(0);
				nav_button_controls(200);
				break;
		}
		function give_direction(direction_index, not_disabled){
			if(click_counter<array_dir.length){
				open_slider(direction_commands[direction_index+1]);
				sound_player(dirsounds[direction_index+1]);
				$('.bottom_arrows').css('pointer-events','none');
			} else if( ((click_counter-array_dir.length)< return_dir.length) && my_go_back){
				sound_player(dirsounds[direction_index+1]);
				open_slider(direction_commands[direction_index+1]);
				$('.bottom_arrows').css('pointer-events','none');
			}
			else if(((click_counter-array_dir.length)>= return_dir.length)){
				$('.drop-pick').show(0);
			}
			if( (click_counter==array_dir.length)&& !my_go_back){

				//for pick the student
				var left_div_pos = 5 + ($('.location').position().left + $('.location').width())/$board.width()*100;
				var top_div_pos = 4.5;
				if(possible_moves[current_idx][0]==4){
					top_div_pos = 30.5;
				}
				if(($('.location').position().left)/$board.width()*100 > 30){
					left_div_pos = -5 + ($('.location').position().left - $('.start-pick').width())/$board.width()*100;
				}
				$('.go_b').css({
					'left': left_div_pos +'%',
					'top' : top_div_pos + '%'
				});
				$('.go_b').show(0);
				//for pick the student end

				current_rotation-=2;
				if(current_direction==0){
					current_direction = 2;
				} else if(current_direction==-1){
					current_direction = 1;
					offset_x-=80;
				} else if(current_direction==1){
					current_direction = -1;
					offset_x+=80;
				}
				prev_straight = 0;
			}
		}
		function open_slider(deepa_text, interval){
			var cur_interval = 1500;
			if(typeof interval === 'number'){
				cur_interval = number;
			}
			if( !(typeof deepa_text == 'undefined' || deepa_text == null) ){
				$('.slider_text>p').html(deepa_text);
			}
			$('.slider_text').addClass('text-animation');
			setTimeout(function(){
				$('.slider_text').removeClass('text-animation');
			}, 1000);
		}
		function my_init(){
			current_direction = 0;
			current_rotation = 0;
			prev_straight = 1;
			array_dir = [];
			return_dir = [];
			direction_commands = [data.string.p3text7, data.string.p3text5, data.string.p3text6];

			offset_x = 0;
			offset_y = 0;
			click_counter = 0;
			my_go_back = false;
		}
	}

	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		$('.bottom_arrows').css('pointer-events','none');
		current_sound.bindOnce('ended', function(){
			$('.bottom_arrows').css('pointer-events','all');
		});
	}

	function sound_player_sec(sound_data, next){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			next?nav_button_controls():'';
		});
	}


	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		current_sound.stop();
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

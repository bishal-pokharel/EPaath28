var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/tellTime/";

var sound_dg1 = new buzz.sound((soundAsset + "s7_p2.ogg"));
var sound_dg2 = new buzz.sound((soundAsset + "s7_p3.ogg"));
var soundsarr = [sound_dg1, sound_dg2];
var current_sound = sound_dg1;

var content = [
	//slide0
	{
		contentnocenteradjust: true,

		extratextblock:[{
			textdata:  data.string.diytext,
			textclass: "title-diy my_font_super_big",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : imgpath + "bg_diy.png",
				}
			]
		}]
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "boy-intro",
					imgsrc : imgpath + "raja.png",
				},
			],
		}],

		diary: [{
			diaryclass: 'diary-intro',
			frontclass: '',
			midclass: '',
			listblockadditionalclass: 'diary-list patrickhand my_font_medium',
			listitem:[
				{
					textdata : data.string.p3text3,
					textclass : ''
				},
				{
					textdata : data.string.p3text4,
					textclass : ''
				},
				{
					textdata : data.string.p3text5,
					textclass : ''
				},
				{
					textdata : data.string.p3text6,
					textclass : ''
				}
			],
			}],
	},
	//slide7
	// {
		// hasheaderblock : false,
		// contentblocknocenteradjust : true,
		// contentblockadditionalclass : 'bg_cloudy',
//
		// extratextblock : [{
			// textdata : data.string.p3text2,
			// textclass : 'review-instruction my_font_big pangolin'
		// }],
//
//
		// diary: [{
			// diaryclass: 'diary-mini',
			// frontclass: '',
			// midclass: '',
			// clickme: '',
			// clickmesrc: 'images/hand-icon.gif',
			// listblockadditionalclass: 'diary-list patrickhand my_font_medium',
			// listitem:[
				// {
					// textdata : data.string.p3text3,
					// textclass : ''
				// },
				// {
					// textdata : data.string.p3text4,
					// textclass : ''
				// },
				// {
					// textdata : data.string.p3text5,
					// textclass : ''
				// },
				// {
					// textdata : data.string.p3text6,
					// textclass : ''
				// }
			// ],
		// }],
//
		// lowertextblockadditionalclass: 'option-block',
		// lowertextblock : [{
			// textdata : data.string.p3text7,
			// divclass: 'option option-1',
			// textclass : 'my_font_medium patrickhand'
		// },{
			// textdata : data.string.p3text8,
			// divclass: 'option option-2',
			// textclass : 'my_font_medium patrickhand'
		// },{
			// textdata : data.string.p3text9,
			// divclass: 'option option-3',
			// textclass : 'my_font_medium patrickhand'
		// },{
			// textdata : data.string.p3text10,
			// divclass: 'option option-4',
			// textclass : 'my_font_medium patrickhand'
		// },
		// ],
//
		// imageblock : [{
			// imagestoshow : [
				// {
					// imgclass : "boy",
					// imgsrc : imgpath + "rahul02.png",
				// },
			// ],
		// }],
		// clockblock : [{
			// clockblockclass: 'clock-diy',
			// cookiesrc: imgpath + 'cookies01.png',
			// clockbodyclass: '',
			// clockbodysrc: imgpath + 'clock.png',
			// hrclass: '',
			// minclass: '',
			// dotclass: '',
		// }],
	// },
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_cloudy',

		extratextblock : [{
			textdata : data.string.p3text2,
			textclass : 'review-instruction-a my_font_big pangolin'
		}],


		diary: [{
			diaryclass: 'diary-mini',
			frontclass: '',
			midclass: '',
			clickme: '',
			clickmesrc: 'images/hand-icon.gif',
			listblockadditionalclass: 'diary-list patrickhand my_font_medium',
			listitem:[
				{
					textdata : data.string.p3text3,
					textclass : ''
				},
				{
					textdata : data.string.p3text4,
					textclass : ''
				},
				{
					textdata : data.string.p3text5,
					textclass : ''
				},
				{
					textdata : data.string.p3text6,
					textclass : ''
				}
			],
		}],

		lowertextblockadditionalclass: 'option-block',
		lowertextblock : [{
			textdata : data.string.p3text7,
			divclass: 'option option-1',
			textclass : 'my_font_medium patrickhand'
		},{
			textdata : data.string.p3text8,
			divclass: 'option option-2',
			textclass : 'my_font_medium patrickhand'
		},{
			textdata : data.string.p3text9,
			divclass: 'option option-3',
			textclass : 'my_font_medium patrickhand'
		},{
			textdata : data.string.p3text10,
			divclass: 'option option-4',
			textclass : 'my_font_medium patrickhand'
		},
		],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "boy",
					imgsrc : imgpath + "rahul02.png",
				},
			],
		}],
		clockblock : [{
			clockblockclass: 'clock-diy',
			cookiesrc: imgpath + 'cookies01.png',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: '',
			minclass: '',
			dotclass: '',
		}],
	},

];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;

	var timeoutvar = null;
	var timeoutvar3 = null;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
			case 0:
				play_diy_audio();
				$nextBtn.delay(3000).show(0);
			break;
			case 1:
				$prevBtn.show(0);
				// $nextBtn.delay(3000).show(0);
				diary_init();

				timeoutvar = setTimeout(function(){
					$('.diary-front').addClass('flipped-front-anim');
					$('.diary-mid').addClass('flipped-mid-anim');
					sound_and_nav(sound_dg1);
				}, 2000);
			break;
			case 2:
			case 3:
				countNext===2?sound_dg2.play():'';
				diary_init();
				$('.diary').click(function(){
					$('.diary').css('pointer-events', 'none');
					timeoutvar = setTimeout(function(){
						$('.diary').css('pointer-events', 'all');
						if(!$('.diary').hasClass('diaryopen')){
							$('.clickins').removeClass('its_hidden');
						}
					}, 3500);
					if($('.diary').hasClass('diaryopen')){
						$('.diary').removeClass('diaryopen').addClass('diaryclose');
						$('.diary-front').removeClass('diary-front-open-2').addClass('flipped-front-anim-close');
						$('.diary-mid').removeClass('diary-mid-open-2').addClass('flipped-mid-anim-close');
					} else {
						$('.clickins').addClass('its_hidden');
						$('.diary').removeClass('diaryclose').addClass('diaryopen');;
						$('.diary-front').removeClass('flipped-front-anim-close').addClass('diary-front-open-2');
						$('.diary-mid').removeClass('flipped-mid-anim-close').addClass('diary-mid-open-2');
					}
				});
				$prevBtn.show(0);
				var possiblecases = [
										['hr-10', 'hr-12', '10', '00', 'option-1'],
										['hr-11-45', 'hr-9', '11', '45', 'option-2'],
										['hr-1-15', 'hr-3', '01', '15', 'option-3'],
										['hr-5-30', 'hr-6', '05', '30', 'option-4']
									];
				var rand_idx = Math.floor( (Math.random()*possiblecases.length) );
				$('.hr-hand').addClass(possiblecases[rand_idx][0]);
				$('.min-hand').addClass(possiblecases[rand_idx][1]);
				$('.digihr>p').html(possiblecases[rand_idx][2]);
				$('.digimin>p').html(possiblecases[rand_idx][3]);
				$('.option').click(function(){
					$(this).addClass('selected');
					$(this).css('pointer-events', 'none');
					if($(this).hasClass(possiblecases[rand_idx][4])){
						$('.option').css('pointer-events', 'none');
						$(this).addClass('correct-opt');
						play_correct_incorrect_sound(1);
						nav_button_controls(0);
					} else{
						play_correct_incorrect_sound(0);
						$(this).addClass('incorrect-opt');
					}
				});
				break;
			default:
				if(countNext>0){
					$prevBtn.show(0);
				}
				$nextBtn.show(0);
				break;
		}
	}
	function diary_init(){
		for(i=1; i<5; i++){
			$('.diary-mid').prepend('<div style="left:'+(i*1)+'%;"></div>');
			$('.diary-back').prepend('<div style="left:'+(i*1)+'%;"></div>');
		}
	}


	function sound_and_nav(sound_data, clickfunction , a){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			if(typeof clickfunction != 'undefined'){
				clickfunction(a, sound_data);
			}
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar3 = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function() {
		current_sound.stop();
		switch(countNext){
			default:
				clearTimeout(timeoutvar);
				clearTimeout(timeoutvar3);
				timeoutvar = null;
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar3);
		timeoutvar = null;
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

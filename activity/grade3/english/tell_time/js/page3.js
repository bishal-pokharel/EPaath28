var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/tellTime/";

var sound_dg1 = new buzz.sound((soundAsset + "milena.ogg"));
var sound_dg2 = new buzz.sound((soundAsset + "s5_p2.ogg"));

var current_sound = sound_dg1;

var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-diy',

		extratextblock : [{
			textdata : data.string.diytext,
			textclass : 'diy-text',
		},
		{
			textdata : '',
			textclass : 'purple-bg-bottom',
		}]
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		extratextblock : [{
			textdata : data.string.p2text20,
			textclass : 'instruction-text luckiestguy my_font_big',
		}],

		draggableblock: [{
			dragclass: 'draggable class-1',
			textclass : 'sniglet my_font_medium',
			imgsrc: imgpath + 'puzzle/bottom01.png',
			textdata: data.string.p2text21,
			ans:"class-1"
		},{
			dragclass: 'draggable class-2',
			textclass : 'sniglet my_font_medium',
			imgsrc: imgpath + 'puzzle/bottom02.png',
			textdata: data.string.p2text22,
            ans:"class-2"
		},{
			dragclass: 'draggable class-3',
			textclass : 'sniglet my_font_medium',
			imgsrc: imgpath + 'puzzle/bottom03.png',
			textdata: data.string.p2text23,
            ans:"class-3"
		},{
			dragclass: 'draggable class-4',
			textclass : 'sniglet my_font_medium',
			imgsrc: imgpath + 'puzzle/bottom04.png',
			textdata: data.string.p2text24,
            ans:"class-4"
		}],

		droppableblock: [{
			dropclass: 'dropclass-1',
			imgsrc: imgpath + 'puzzle/top01.png',
			clockblockclass: '',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-6-45',
			minclass: 'hr-9',
			dotclass: '',
			ans:"class-1"
		},{
			dropclass: 'dropclass-2',
			imgsrc: imgpath + 'puzzle/top02.png',
			clockblockclass: '',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-1-15',
			minclass: 'hr-3',
			dotclass: '',
            ans:"class-2"
		},{
			dropclass: 'dropclass-3',
			imgsrc: imgpath + 'puzzle/top03.png',
			clockblockclass: '',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-4-15',
			minclass: 'hr-3',
			dotclass: '',
            ans:"class-3"
		},{
			dropclass: 'dropclass-4',
			imgsrc: imgpath + 'puzzle/top04.png',
			clockblockclass: '',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-10-45',
			minclass: 'hr-9',
			dotclass: '',
            ans:"class-4"
		}]
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		extratextblock : [{
			textdata : data.string.p2text20,
			textclass : 'instruction-text luckiestguy my_font_big',
		}],

		draggableblock: [{
			dragclass: 'draggable class-1',
			textclass : 'sniglet my_font_medium',
			imgsrc: imgpath + 'puzzle/bottom01.png',
			textdata: data.string.p2text25,
            ans:"class-1"
		},{
			dragclass: 'draggable class-2',
			textclass : 'sniglet my_font_medium',
			imgsrc: imgpath + 'puzzle/bottom02.png',
			textdata: data.string.p2text26,
            ans:"class-2"
		},{
			dragclass: 'draggable class-3',
			textclass : 'sniglet my_font_medium',
			imgsrc: imgpath + 'puzzle/bottom03.png',
			textdata: data.string.p2text27,
            ans:"class-3"
		},{
			dragclass: 'draggable class-4',
			textclass : 'sniglet my_font_medium',
			imgsrc: imgpath + 'puzzle/bottom04.png',
			textdata: data.string.p2text28,
            ans:"class-4"
		}],

		droppableblock: [{
			dropclass: 'dropclass-1',
			imgsrc: imgpath + 'puzzle/top01.png',
			clockblockclass: '',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-11-15',
			minclass: 'hr-3',
			dotclass: '',
            ans:"class-1"
		},{
			dropclass: 'dropclass-2',
			imgsrc: imgpath + 'puzzle/top02.png',
			clockblockclass: '',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-8-15',
			minclass: 'hr-3',
			dotclass: '',
            ans:"class-2"
		},{
			dropclass: 'dropclass-3',
			imgsrc: imgpath + 'puzzle/top03.png',
			clockblockclass: '',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-11-45',
			minclass: 'hr-9',
			dotclass: '',
            ans:"class-3"
		},{
			dropclass: 'dropclass-4',
			imgsrc: imgpath + 'puzzle/top04.png',
			clockblockclass: '',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'hr-1-45',
			minclass: 'hr-9',
			dotclass: '',
            ans:"class-4"
		}]
	},
];

content[1].droppableblock.shufflearray();
content[2].droppableblock.shufflearray();


$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;

	var timeoutvar = null;
	var timeoutvar3 = null;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
			case 0:
				play_diy_audio();
				nav_button_controls(500);
				break;
			case 1:
			case 2:
				countNext===1?sound_player(sound_dg2):'';
        var option_position = [1,2,3,4];
        option_position.shufflearray();
        for(var op=0; op<4; op++){
            $('.dragclass').eq(op).addClass('pos-'+option_position[op]);
        }
        dragdrop();
      break;
			default:
				$prevBtn.show(0);
				nav_button_controls(500);
				break;
		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar3 = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}

	function sound_and_nav(sound_data, clickfunction , a){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			if(typeof clickfunction != 'undefined'){
				clickfunction(a, sound_data);
			}
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		});
	}
	function click_dg(dg_class, audio){
		$(dg_class).click(function(){
			sound_player(audio);
		});
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				current_sound.stop();
				clearTimeout(timeoutvar);
				clearTimeout(timeoutvar3);
				timeoutvar = null;
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		current_sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar3);
		timeoutvar = null;
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();


    function dragdrop(){
        // $(".droppable").attr("disabled","disabled");
		var dropCount = 0;
        $(".draggable").draggable({
            containment: ".board",
            revert: true,
            appendTo: "body",
            zindex: 10,
            start:function(event, ui){
                $(this).addClass('selected');
            },
            stop:function(event,ui){
                $(this).removeClass('selected');

            }

        });
        $('.dropclass').droppable({
            accept : ".draggable",
            hoverClass: "hovered",
            drop: function(event, ui) {
                var draggableans = ui.draggable.attr("data-answer");
                var droppableans = $(this).attr("data-answer");
                console.log("drag"+draggableans)
                console.log("drop"+droppableans)
                console.log(draggableans)
                console.log(droppableans)
                if(draggableans.toString().trim() == droppableans.toString().trim()) {
                    current_sound.stop();
                    play_correct_incorrect_sound(1);
                    ui.draggable.addClass('dropped')
                    ui.draggable.detach().css({
                        'pointer-events': 'none',
                        'z-index': '10',
                        'position': 'absolute',
                        'left': '0%',
                        'width': '100%',
                        'top': '57%',
                        'margin': '0%',
                    }).appendTo($(this));
                    dropCount++;
                    if(dropCount>3){
                    	nav_button_controls(0);
					}
                }
                else {
                    current_sound.stop();
                    play_correct_incorrect_sound(0);
                }
            }
        });
    }

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/tellTime/";

var sound_dg1 = new buzz.sound((soundAsset + "s6_p1.ogg"));
var sound_dg2 = new buzz.sound((soundAsset + "s6_p1(3).ogg"));
var sound_dg3 = new buzz.sound((soundAsset + "s6_p1(4).ogg"));
var sound_dg4 = new buzz.sound((soundAsset + "s6_p1(5).ogg"));
var sound_dg5 = new buzz.sound((soundAsset + "s6_p1.ogg"));

var soundsarr = [
				sound_dg1, sound_dg2, sound_dg3, sound_dg4, sound_dg5
				];
var current_sound = sound_dg1;
var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		extratextblock : [{
			textdata : data.string.p3text1,
			textclass : 'review-text my_font_super_big luckiestguy',
			datahighlightflag: true,
			datahighlightcustomclass: 'ul'
		}],

		clockblock : [{
			clockblockclass: 'clock-review',
			cookiesrc: imgpath + 'cookies01.png',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'its_hidden',
			minclass: 'hr-12',
			dotclass: '',
			q1data: data.string.q0,
			q1class: 'my_font_medium patrickhand'
		}]
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		extratextblock : [{
			textdata : data.string.p3text1,
			textclass : 'review-text my_font_super_big luckiestguy',
			datahighlightflag: true,
			datahighlightcustomclass: 'ul'
		}],

		clockblock : [{
			clockblockclass: 'clock-review',
			cookiesrc: imgpath + 'cookies01.png',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'its_hidden',
			minclass: 'hr-3',
			dotclass: '',
			q2data: data.string.q1,
			q2class: 'my_font_medium patrickhand',
		}]
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		extratextblock : [{
			textdata : data.string.p3text1,
			textclass : 'review-text my_font_super_big luckiestguy',
			datahighlightflag: true,
			datahighlightcustomclass: 'ul'
		}],

		clockblock : [{
			clockblockclass: 'clock-review',
			cookiesrc: imgpath + 'cookies01.png',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'its_hidden',
			minclass: 'hr-6',
			dotclass: '',
			q3data: data.string.q2,
			q3class: 'my_font_medium patrickhand',
		}]
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		extratextblock : [{
			textdata : data.string.p3text1,
			textclass : 'review-text my_font_super_big luckiestguy',
			datahighlightflag: true,
			datahighlightcustomclass: 'ul'
		}],

		clockblock : [{
			clockblockclass: 'clock-review',
			cookiesrc: imgpath + 'cookies01.png',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'its_hidden',
			minclass: 'hr-9',
			dotclass: '',
			q4data: data.string.q3,
			q4class: 'my_font_medium patrickhand',
		}]
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',

		extratextblock : [{
			textdata : data.string.p3text1,
			textclass : 'review-text my_font_super_big luckiestguy',
			datahighlightflag: true,
			datahighlightcustomclass: 'ul'
		}],

		clockblock : [{
			clockblockclass: 'clock-review',
			cookiesrc: imgpath + 'cookies01.png',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clock.png',
			hrclass: 'its_hidden',
			minclass: 'its_hidden',
			dotclass: '',
			q1data: data.string.q0,
			q1class: 'my_font_medium patrickhand',
			q2data: data.string.q1,
			q2class: 'my_font_medium patrickhand',
			q3data: data.string.q2,
			q3class: 'my_font_medium patrickhand',
			q4data: data.string.q3,
			q4class: 'my_font_medium patrickhand',
		}]
	}
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;

	var timeoutvar = null;
	var timeoutvar3 = null;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
			case 4:
				$prevBtn.show(0);
				nav_button_controls(0);
			break;
			default:
				sound_and_nav(soundsarr[countNext]);
				if(countNext>0){
					$prevBtn.show(0);
				}
				// nav_button_controls(0);
				break;
		}
	}
	function diary_init(){
		for(i=1; i<5; i++){
			$('.diary-mid').prepend('<div style="left:'+(i*1)+'%;"></div>');
			$('.diary-back').prepend('<div style="left:'+(i*1)+'%;"></div>');
		}
	}
	function sound_and_nav(sound_data, clickfunction , a){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			if(typeof clickfunction != 'undefined'){
				clickfunction(a, sound_data);
			}
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		});
	}

	function nav_button_controls(delay_ms){
		timeoutvar3 = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				clearTimeout(timeoutvar);
				clearTimeout(timeoutvar3);
				timeoutvar = null;
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar3);
		timeoutvar = null;
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

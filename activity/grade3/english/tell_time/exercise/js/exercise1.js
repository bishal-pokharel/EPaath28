var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";

var content=[

	//ex1
	{
		exerciseblockadditionalclass: "mainbg",
		exerciseblock:[{
			instructionclass: 'my_font_medium',
			instructiondata : data.string.eins,
			submitdata: data.string.esubmit,
			hintdata: data.string.ehint,
			dragblockleft:[{
				dlt1 : data.string.ed1,
				dlt2 : data.string.ed2,
				dlt3 : data.string.ed3,
				dlt4 : data.string.ed4,
				dlt5 : data.string.ed5,
			}],
			drc5: 'correct-drag',
			dragblockright: true,
			clockblock : [{
				clockblockclass: 'clock-ex',
				clockbodyclass: '',
				clockbodysrc: imgpath + 'clock.png',
				hrclass: 'hr-5 hint-c',
				minclass: 'hr-12',
				dotclass: ''
			}],
			option:[{
				textclass: '',
				textdata: data.string.ed3
			}]
		}]
	},
	//ex2
	{
		exerciseblockadditionalclass: "mainbg",
		exerciseblock:[{
			instructionclass: 'my_font_medium',
			instructiondata : data.string.eins,
			submitdata: data.string.esubmit,
			hintdata: data.string.ehint,
			dragblockleft:[{
				dlt1 : data.string.ed1,
				dlt2 : data.string.ed2,
				dlt3 : data.string.ed3,
				dlt4 : data.string.ed4,
				dlt5 : data.string.ed5,
				dlc5: 'correct-drag',
			}],
			// drc5: 'correct-drag',
			dragblockright: true,
			clockblock : [{
				clockblockclass: 'clock-ex',
				clockbodyclass: '',
				clockbodysrc: imgpath + 'clock.png',
				hrclass: 'hr-1-45',
				minclass: 'hr-9 hint-c',
				dotclass: ''
			}],
			option:[{
				textclass: 'order-first',
				textdata: data.string.equater
			},{
				textclass: 'order-last',
				textdata: '2'
			}]
		}]
	},
	//ex3
	{
		exerciseblockadditionalclass: "mainbg",
		exerciseblock:[{
			instructionclass: 'my_font_medium',
			instructiondata : data.string.eins,
			submitdata: data.string.esubmit,
			hintdata: data.string.ehint,
			dragblockleft:[{
				dlt1 : data.string.ed1,
				dlt2 : data.string.ed2,
				dlt3 : data.string.ed3,
				dlt4 : data.string.ed4,
				dlt5 : data.string.ed5,
				dlc2: 'correct-drag',
			}],
			// drc5: 'correct-drag',
			dragblockright: true,
			clockblock : [{
				clockblockclass: 'clock-ex',
				clockbodyclass: '',
				clockbodysrc: imgpath + 'clock.png',
				hrclass: 'hr-7-15',
				minclass: 'hr-3 hint-c',
				dotclass: ''
			}],
			option:[{
				textclass: 'order-first',
				textdata: data.string.equater
			},{
				textclass: 'order-last',
				textdata: '7'
			}]
		}]
	},
	//ex4
	{
		exerciseblockadditionalclass: "mainbg",
		exerciseblock:[{
			instructionclass: 'my_font_medium',
			instructiondata : data.string.eins,
			submitdata: data.string.esubmit,
			hintdata: data.string.ehint,
			dragblockleft:[{
				dlt1 : data.string.ed1,
				dlt2 : data.string.ed2,
				dlt3 : data.string.ed3,
				dlt4 : data.string.ed4,
				dlt5 : data.string.ed5,
				dlc4: 'correct-drag',
			}],
			// drc5: 'correct-drag',
			dragblockright: true,
			clockblock : [{
				clockblockclass: 'clock-ex',
				clockbodyclass: '',
				clockbodysrc: imgpath + 'clock.png',
				hrclass: 'hr-3-30',
				minclass: 'hr-6 hint-c',
				dotclass: ''
			}],
			option:[{
				textclass: '',
				textdata: data.string.epast
			},{
				textclass: 'order-last',
				textdata: '3'
			}]
		}]
	},
	//ex5
	{
		exerciseblockadditionalclass: "mainbg",
		exerciseblock:[{
			instructionclass: 'my_font_medium',
			instructiondata : data.string.eins,
			submitdata: data.string.esubmit,
			hintdata: data.string.ehint,
			dragblockleft:[{
				dlt1 : data.string.ed1,
				dlt2 : data.string.ed2,
				dlt3 : data.string.ed3,
				dlt4 : data.string.ed4,
				dlt5 : data.string.ed5,
				dlc3: 'correct-drag',
			}],
			// drc5: 'correct-drag',
			dragblockright: true,
			clockblock : [{
				clockblockclass: 'clock-ex',
				clockbodyclass: '',
				clockbodysrc: imgpath + 'clock.png',
				hrclass: 'hr-8',
				minclass: 'hr-12 hint-c',
				dotclass: ''
			}],
			option:[{
				textclass: 'order-first',
				textdata: '8'
			}]
		}]
	},
	//ex6
	{
		exerciseblockadditionalclass: "mainbg",
		exerciseblock:[{
			instructionclass: 'my_font_medium',
			instructiondata : data.string.eins,
			submitdata: data.string.esubmit,
			hintdata: data.string.ehint,
			dragblockleft:[{
				dlt1 : data.string.ed1,
				dlt2 : data.string.ed2,
				dlt3 : data.string.ed3,
				dlt4 : data.string.ed4,
				dlt5 : data.string.ed5,
				// dlc3: 'correct-drag',
			}],
			drc7: 'correct-drag',
			dragblockright: true,
			clockblock : [{
				clockblockclass: 'clock-ex',
				clockbodyclass: '',
				clockbodysrc: imgpath + 'clock.png',
				hrclass: 'hr-7-15 hint-c',
				minclass: 'hr-3',
				dotclass: ''
			}],
			option:[{
				textclass: 'order-first',
				textdata: data.string.q1
			}]
		}]
	},
	//ex7
	{
		exerciseblockadditionalclass: "mainbg",
		exerciseblock:[{
			instructionclass: 'my_font_medium',
			instructiondata : data.string.eins,
			submitdata: data.string.esubmit,
			hintdata: data.string.ehint,
			dragblockleft:[{
				dlt1 : data.string.ed1,
				dlt2 : data.string.ed2,
				dlt3 : data.string.ed3,
				dlt4 : data.string.ed4,
				dlt5 : data.string.ed5,
				// dlc3: 'correct-drag',
			}],
			drc6: 'correct-drag',
			dragblockright: true,
			clockblock : [{
				clockblockclass: 'clock-ex',
				clockbodyclass: '',
				clockbodysrc: imgpath + 'clock.png',
				hrclass: 'hr-5-45 hint-c',
				minclass: 'hr-9',
				dotclass: ''
			}],
			option:[{
				textclass: 'order-first',
				textdata: data.string.q3
			}]
		}]
	},
	//ex8
	{
		exerciseblockadditionalclass: "mainbg",
		exerciseblock:[{
			instructionclass: 'my_font_medium',
			instructiondata : data.string.eins,
			submitdata: data.string.esubmit,
			hintdata: data.string.ehint,
			dragblockleft:[{
				dlt1 : data.string.ed1,
				dlt2 : data.string.ed2,
				dlt3 : data.string.ed3,
				dlt4 : data.string.ed4,
				dlt5 : data.string.ed5,
				dlc2: 'correct-drag',
			}],
			// drc5: 'correct-drag',
			dragblockright: true,
			clockblock : [{
				clockblockclass: 'clock-ex',
				clockbodyclass: '',
				clockbodysrc: imgpath + 'clock.png',
				hrclass: 'hr-1-30',
				minclass: 'hr-6 hint-c',
				dotclass: ''
			}],
			option:[{
				textclass: 'order-first',
				textdata: data.string.ehalf
			},{
				textclass: 'order-last',
				textdata: '1'
			}]
		}]
	},
	//ex9
	{
		exerciseblockadditionalclass: "mainbg",
		exerciseblock:[{
			instructionclass: 'my_font_medium',
			instructiondata : data.string.eins,
			submitdata: data.string.esubmit,
			hintdata: data.string.ehint,
			dragblockleft:[{
				dlt1 : data.string.ed1,
				dlt2 : data.string.ed2,
				dlt3 : data.string.ed3,
				dlt4 : data.string.ed4,
				dlt5 : data.string.ed5,
				dlc1: 'correct-drag',
			}],
			// drc5: 'correct-drag',
			dragblockright: true,
			clockblock : [{
				clockblockclass: 'clock-ex',
				clockbodyclass: '',
				clockbodysrc: imgpath + 'clock.png',
				hrclass: 'hr-10-45',
				minclass: 'hr-9 hint-c',
				dotclass: ''
			}],
			option:[{
				textclass: '',
				textdata: 'to 11'
			}]
		}]
	},
	//ex10
	{
		exerciseblockadditionalclass: "mainbg",
		exerciseblock:[{
			instructionclass: 'my_font_medium',
			instructiondata : data.string.eins,
			submitdata: data.string.esubmit,
			hintdata: data.string.ehint,
			dragblockleft:[{
				dlt1 : data.string.ed1,
				dlt2 : data.string.ed2,
				dlt3 : data.string.ed3,
				dlt4 : data.string.ed4,
				dlt5 : data.string.ed5,
				// dlc3: 'correct-drag',
			}],
			drc9: 'correct-drag',
			dragblockright: true,
			clockblock : [{
				clockblockclass: 'clock-ex',
				clockbodyclass: '',
				clockbodysrc: imgpath + 'clock.png',
				hrclass: 'hr-9 hint-c',
				minclass: 'hr-12',
				dotclass: ''
			}],
			option:[{
				textclass: 'order-last',
				textdata: data.string.eoclock
			}]
		}]
	},
];

content.shufflearray();


$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	/*for limiting the questions to 10*/
	var $total_page = content.length;

	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
 	}

	 /*values in this array is same as the name of images of eggs in image folder*/

	 //create eggs
	 var testin = new NumberTemplate();

	 	//eggTemplate.eggMove(countNext);
	testin.init($total_page);

	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);
	 	var testcount = 0;

	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);

	 	/*generate question no at the beginning of question*/
	 	testin.numberOfQuestions();

	 	var wrngClicked = false;
	 	var $current_par = null;

	 	$('.dl-text, .drag-number').draggable({
			containment: ".board",
			cursor: "move",
			revert: true,
			appendTo: "body",
			zIndex: 50,
			start: function( event, ui ){
				// $(this).addClass('selected');
			},
			stop: function( event, ui ){
				// $(this).removeClass('selected');
			}
		});
		$('.blank-space').droppable({
			drop:function(event, ui) {
				dropper(ui.draggable, $(this));
			}
		});
		function dropper(dropped, droppedOn){
				if(droppedOn.children().length>0){
					droppedOn.children().draggable('enable');
					droppedOn.children().detach().css({
						'pointer-events': '',
						'top': '',
						'left': '',
						'background-color': '',
						'padding': '',
						'border': '',
						'color':'#f00'
					}).appendTo($current_par);
				}
				dropped.draggable('option', 'revert', false);
				dropped.draggable('disable');
				$current_par = dropped.parent();
				console.log($current_par);
				dropped.detach().css({
					'pointer-events': 'none',
                    'color':'black'
				}).appendTo(droppedOn);
		}
 		$(".submit-btn").click(function(){
 			if($('.blank-space').children().hasClass("correct-drag")){
 				if(!wrngClicked){
		 			testin.update(true);
				}
		 		play_correct_incorrect_sound(1);
				$(".submit-btn").css("background", "gray");
				if(countNext != $total_page)
					$nextBtn.show(0);
				play_correct_incorrect_sound(1);
				$('.corincor-img').attr('src', 'images/correct.png');
				$('.corincor-text').html('Correct!');
				$('.corincor-text').css('color', '#4CAF50');
				$('.exerciseblock').css('pointer-events', 'none');
 				$('.corincor-block').show(0);
			} else{
				if(!wrngClicked){
					testin.update(false);
				}
				wrngClicked = true;
	 			$('.hint-c').addClass('hl-hand');
				play_correct_incorrect_sound(0);
 				$('.corincor-block').show(0);
			}
	 	});
	 	$(".hint-btn").click(function(){
	 		$('.hint-c').addClass('hl-hand');
	 	});
 	}

 	/*======= SCOREBOARD SECTION ==============*/

	 function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
		testin.gotoNext();
	});


	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

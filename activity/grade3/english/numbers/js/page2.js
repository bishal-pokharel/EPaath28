var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'sky_blue',
		extratextblock:[{
			textdata: data.string.diytext,
			textclass: "diy_title",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'cover'
				}
			]
		}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'sky_blue',
		extratextblock:[{
			textclass:'top_instuction',
			textdata:data.string.ins
		},{
			textclass:'question_top',
				datahighlightflag: true,
				datahighlightcustomclass: "makeitlc",
			textdata:data.string.p2ques1
		},{
			textclass:'option correct',
			textdata:data.string.p2q1opt1
		},{
			textclass:'option',
			textdata:data.string.p2q1opt2
		},{
			textclass:'option',
			textdata:data.string.p2q1opt3
		}]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'sky_blue',
		extratextblock:[{
			textclass:'top_instuction',
			textdata:data.string.ins
		},{
			datahighlightflag: true,
			datahighlightcustomclass: "makeitlc",
			textclass:'question_top',
			textdata:data.string.p2ques2
		},{
			textclass:'option correct',
			textdata:data.string.p2q2opt1
		},{
			textclass:'option',
			textdata:data.string.p2q2opt2
		},{
			textclass:'option',
			textdata:data.string.p2q2opt3
		}]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'sky_blue',
		extratextblock:[{
			textclass:'top_instuction',
			textdata:data.string.ins
		},{
			datahighlightflag: true,
			datahighlightcustomclass: "makeitlc",
			textclass:'question_top',
			textdata:data.string.p2ques3
		},{
			textclass:'option correct',
			textdata:data.string.p2q3opt1
		},{
			textclass:'option',
			textdata:data.string.p2q3opt2
		},{
			textclass:'option',
			textdata:data.string.p2q3opt3
		}]
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'sky_blue',
		extratextblock:[{
			textclass:'top_instuction',
			textdata:data.string.ins
		},{
			datahighlightflag: true,
			datahighlightcustomclass: "makeitlc",
			textclass:'question_top',
			textdata:data.string.p2ques4
		},{
			textclass:'option correct',
			textdata:data.string.p2q4opt1
		},{
			textclass:'option',
			textdata:data.string.p2q4opt2
		},{
			textclass:'option',
			textdata:data.string.p2q4opt3
		}]
	}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// images
			{id: "corrimg", src: "images/right.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrimg", src: "images/wrongicon.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cover", src: "images/diy_bg/a_29.png", type: createjs.AbstractLoader.IMAGE},
			// soundsa
			{id: "correct", src: "sounds/common/correct.ogg"},
			{id: "incorrect", src: "sounds/common/incorrect.ogg"},
			{id: "sound_1", src: soundAsset+"diy.ogg"},
			{id: "sound_2", src: soundAsset+"diyins.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	 function navigationcontroller(islastpageflag){
 	 typeof islastpageflag === "undefined" ?
 	 islastpageflag = false :
 	 typeof islastpageflag != 'boolean'?
 	 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 	 null;

 	 if(countNext == 0 && $total_page!=1){
 	 	$nextBtn.show(0);
 	 	$prevBtn.css('display', 'none');
 	 }
 	 else if($total_page == 1){
 	 	$prevBtn.css('display', 'none');
 	 	$nextBtn.css('display', 'none');

 	 	// if lastpageflag is true
 	 	islastpageflag ?
 	 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 	 }
 	 else if(countNext > 0 && countNext < $total_page-1){
 	 	$nextBtn.show(0);
 	 	$prevBtn.show(0);
 	 }
 	 else if(countNext == $total_page-1){
 	 	$nextBtn.css('display', 'none');
 	 	$prevBtn.show(0);

 	 	// if lastpageflag is true
 	 	islastpageflag ?
 	 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	 	ole.footerNotificationHandler.pageEndSetNotification() ;
 	 }
 	 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		switch (countNext) {
			case 0:
				sound_player1("sound_1");
			break;
			case 1:
				sound_player1("sound_2");
			break;
		}

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_label_image(content, countNext);
		put_card_image(content, countNext);
		vocabcontroller.findwords(countNext);
		ole.footerNotificationHandler.hideNotification();
		var pos_array=['pos1','pos2','pos3'];
		pos_array.shufflearray();
		for (var i = 0; i < 3; i++) {
			$('.option').eq(i).addClass(pos_array[i]);
		}

		$('.option').click(function(){
			if($(this).hasClass('correct')){
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$('.coverboardfull ').width()+'%';
				var centerY = ((position.top + height)*100)/$('.coverboardfull ').height()+'%';
				$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:4%;transform:translate(-8%,-221%)" src="'+preload.getResult('corrimg').src +'" />').insertAfter(this);
				$(this).css({"background": "#98C02E",
											"border-color": "#DEEF3C",
												"transition":'.1s'});
				$('.option').css('pointer-events','none');
				sound_player1('correct');
				navigationcontroller();
			}
			else{
				$(this).css({"background": "#FF0000",
											"border-color": "#980000",
												"transition":'.1s',
												"pointer-events":"none"});
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$('.coverboardfull ').width()+'%';
				var centerY = ((position.top + height)*100)/$('.coverboardfull ').height()+'%';
				$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:4%;transform:translate(-8%,-221%)" src="'+preload.getResult('incorrimg').src +'"/>').insertAfter(this);
				sound_player1('incorrect');
			}
		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player1(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
		if(content[count].hasOwnProperty("containsdialog")){
			for(var j = 0; j < content[count].containsdialog.length; j++){
				var containsdialog = content[count].containsdialog[j];
				console.log("imageblock", imageblock);
				if(containsdialog.hasOwnProperty('dialogueimgid')){
						var image_src = preload.getResult(containsdialog.dialogueimgid).src;
						//get list of classes
						var classes_list = containsdialog.dialogimageclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_label_image(content, count){
		if(content[count].hasOwnProperty('imageandlabel')){
			var imageandlabel = content[count].imageandlabel;
			for(var i=0; i<imageandlabel.length; i++){
				var image_src = preload.getResult(imageandlabel[i].imgid).src;
				console.log(image_src);
				var classes_list = imageandlabel[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function put_card_image(content, count){
		if(content[count].hasOwnProperty('card')){
			var card = content[count].card;
			for(var i=0; i<card.length; i++){
				var image_src = preload.getResult(card[i].imgid).src;
				console.log(image_src);
				var classes_list = card[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		console.log(countNext);
		if(countNext == 0)
		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}
	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});
	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

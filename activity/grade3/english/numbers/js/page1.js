var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		singletext:[
			{
				textclass: "maintext",
				textdata: data.lesson.chapter
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "cover",
					imgid : 'cover',
					imgsrc: ""
				}
			]
		}]
	},
	// slide1
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		headerblock:[
			{
				textdata: data.string.p1text1
			}
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "1"
							},
							{
								textclass: "chocclass",
								textdata: "1"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: "2"
							},
							{
								textclass: "chocclass",
								textdata: "1"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: "3"
							},
							{
								textclass: "chocclass",
								textdata: "1"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow4",
								textdata: "4"
							},
							{
								textclass: "chocclass",
								textdata: "1"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow5",
								textdata: "5"
							},
							{
								textclass: "chocclass",
								textdata: "1"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow6",
								textdata: "6"
							},
							{
								textclass: "chocclass",
								textdata: "1"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow7",
								textdata: "7"
							},
							{
								textclass: "chocclass",
								textdata: "1"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow8",
								textdata: "8"
							},
							{
								textclass: "chocclass",
								textdata: "1"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow9",
								textdata: "9"
							},
							{
								textclass: "chocclass",
								textdata: "1"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow10",
								textdata: "10"
							},
							{
								textclass: "chocclass",
								textdata: "1"
							}
						]
					}

				]
			}
		]
	},
	//slide2
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		headerblock:[
			{
				textdata: data.string.p3text3
			}
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",
				flexblock:[]
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "box",
						imgid: "bag",
						imgscr: "",
					}
				]
			}
		],
		singletext:[
			{
				textclass: "ltb",
				textdata: data.string.p1text3
			}
		],
	},
	//slide3
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		singletext:[
			{
				textclass: "middletext",
				textdata: data.string.p1text4
			}
		]
	},
	// slide4
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock200",
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "10"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: "20"
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: "30"
							},
							{
								flexboxrowclass :"tabletext tbrow4",
								textdata: "40"
							},
							{
								flexboxrowclass :"tabletext tbrow5",
								textdata: "50"
							},
							{
								flexboxrowclass :"tabletext tbrow6",
								textdata: "60"
							},
							{
								flexboxrowclass :"tabletext tbrow7",
								textdata: "70"
							},
							{
								flexboxrowclass :"tabletext tbrow8",
								textdata: "80"
							},
							{
								flexboxrowclass :"tabletext tbrow9",
								textdata: "90"
							},
							{
								flexboxrowclass :"tabletext tbrow10",
								textdata: "100"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "TEN"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: "TWENTY"
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: "THIRTY"
							},
							{
								flexboxrowclass :"tabletext tbrow4",
								textdata: "FORTY"
							},
							{
								flexboxrowclass :"tabletext tbrow5",
								textdata: "FIFTY"
							},
							{
								flexboxrowclass :"tabletext tbrow6",
								textdata: "SIXTY"
							},
							{
								flexboxrowclass :"tabletext tbrow7",
								textdata: "SEVENTY"
							},
							{
								flexboxrowclass :"tabletext tbrow8",
								textdata: "EIGHTY"
							},
							{
								flexboxrowclass :"tabletext tbrow9",
								textdata: "NINETY"
							},
							{
								flexboxrowclass :"tabletext tbrow10",
								textdata: "HUNDRED"
							}
						]
					}

				]
			}
		]
	},
	//slide 5
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		headerblock:[
			{
				textdata: data.string.p1text5
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "threeimg-1",
						imgid: "bag",
						imgscr: "",
					},
					{
						imgclass: "threeimg-2",
						imgid: "bag",
						imgscr: "",
					},
					{
						imgclass: "threeimg-3",
						imgid: "bag",
						imgscr: "",
					}
				]
			}
		],
		singletext:[
			{
				textclass: "iclabel-1",
				textdata: "+"
			},
			{
				textclass: "iclabel-2",
				textdata: "+"
			},
			{
				textclass: "iclabel-3",
				textdata: "= 30"
			}
		]
	},
	//slide6
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		singletext:[
			{
				textclass: "middletext",
				textdata: data.string.p3text7
			}
		]
	},
	//slide7
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		headerblock:[
			{
				textdata: data.string.p3text8
			}
		],
		fleximages: [
			{
				flexblockadditionalclass: "flexcandyimages",
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								imgclass: "bagimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx2",
								imgid: "box",
								imgscr: "",
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								imgclass: "cdyimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx3",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx4",
								imgid: "box",
								imgscr: "",
							}
						]
					}

				]
			}
		]
	},
	//slide8
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		headerblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "highsize",
				textdata: data.string.p3text10
			}
		],
		fleximages: [
			{
				flexblockadditionalclass: "flexcandyimages",
				flexblock:[
					{
						flexboxcolumnclass: "column col2",
						flexblockcolumn:[
							{
								imgclass: "bagimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx2",
								imgid: "box",
								imgscr: "",
							}
						]
					},
					{
						flexboxcolumnclass: "column col3",
						flexblockcolumn:[
							{
								imgclass: "cdyimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx3",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx4",
								imgid: "box",
								imgscr: "",
							}
						]
					}

				]
			}
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "lowerflex",
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "TENS"
							},
							{
								flexboxrowclass :"tabletext tbrow2 hiddencount",
								textdata: "2"
							},
							{
								flexboxrowclass :"tabletext tbrow3 hiddencount",
								textdata: "2 tens = 20 (twenty)"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "ONES"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: ""
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: ""
							}
						]
					}

				]
			}
		]
	},
	//slide10
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		headerblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "highsize",
				textdata: data.string.p3text11
			}
		],
		fleximages: [
			{
				flexblockadditionalclass: "flexcandyimages",
				flexblock:[
					{
						flexboxcolumnclass: "column col2",
						flexblockcolumn:[
							{
								imgclass: "bagimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx2",
								imgid: "box",
								imgscr: "",
							}
						]
					},
					{
						flexboxcolumnclass: "column col3",
						flexblockcolumn:[
							{
								imgclass: "cdyimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx3",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx4",
								imgid: "box",
								imgscr: "",
							}
						]
					}

				]
			}
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "lowerflex",
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "TENS"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: "2"
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: "2 tens = 20 (twenty)"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "ONES"
							},
							{
								flexboxrowclass :"tabletext tbrow2 hiddencount",
								textdata: "4"
							},
							{
								flexboxrowclass :"tabletext tbrow3 hiddencount",
								textdata: "4 ones = 4 (four)"
							}
						]
					}

				]
			}
		]
	},
	//slide11
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		singletext:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "greenhigh",
				textclass: "conclubox",
				textdata: data.string.p1text10
			}
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "lowerflex lowmoveup",
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "TENS"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: "2"
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: "2 tens = 20 (twenty)"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "ONES"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: "4"
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: "4 ones = 4 (four)"
							}
						]
					}

				]
			}
		]
	},
	//slide12
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		singletext:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "highsize",
				textclass: "ltb",
				textdata: data.string.p1text11
			}
		],
		fleximages: [
			{
				flexblockadditionalclass: "flexcandyimages",
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								imgclass: "bagimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx2",
								imgid: "box",
								imgscr: "",
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								imgclass: "cdyimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx3",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx4",
								imgid: "box",
								imgscr: "",
							}
						]
					}

				]
			}
		]
	},
	//slide13
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		singletext:[
			{
				textclass: "middletext",
				textdata: data.string.p3text7
			}
		]
	},
	//slide14
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		headerblock:[
			{
				textdata: data.string.p3text8
			}
		],
		fleximages: [
			{
				flexblockadditionalclass: "flexcandyimages",
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								imgclass: "bagimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx3",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx4",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx5",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx6",
								imgid: "box",
								imgscr: "",
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								imgclass: "cdyimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx3",
								imgid: "box",
								imgscr: "",
							}
						]
					}

				]
			}
		]
	},
	//slide16
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		headerblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "highsize",
				textdata: data.string.p3text10
			}
		],
		fleximages: [
			{
				flexblockadditionalclass: "flexcandyimages",
				flexblock:[
					{
						flexboxcolumnclass: "column col2",
						flexblockcolumn:[
							{
								imgclass: "bagimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx3",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx4",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx5",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx6",
								imgid: "box",
								imgscr: "",
							}
						]
					},
					{
						flexboxcolumnclass: "column col3",
						flexblockcolumn:[
							{
								imgclass: "cdyimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx3",
								imgid: "box",
								imgscr: "",
							}
						]
					}

				]
			}
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "lowerflex",
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "TENS"
							},
							{
								flexboxrowclass :"tabletext tbrow2 hiddencount",
								textdata: "6"
							},
							{
								flexboxrowclass :"tabletext tbrow3 hiddencount",
								textdata: "6 tens = 60 (sixty)"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "ONES"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: ""
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: ""
							}
						]
					}

				]
			}
		]
	},
	//slide17
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		headerblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "highsize",
				textdata: data.string.p3text11
			}
		],
		fleximages: [
			{
				flexblockadditionalclass: "flexcandyimages",
				flexblock:[
					{
						flexboxcolumnclass: "column col2",
						flexblockcolumn:[
							{
								imgclass: "bagimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx3",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx4",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx5",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx6",
								imgid: "box",
								imgscr: "",
							}
						]
					},
					{
						flexboxcolumnclass: "column col3",
						flexblockcolumn:[
							{
								imgclass: "cdyimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx3",
								imgid: "box",
								imgscr: "",
							}
						]
					}

				]
			}
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "lowerflex",
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "TENS"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: "6"
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: "6 tens = 60 (sixty)"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "ONES"
							},
							{
								flexboxrowclass :"tabletext tbrow2 hiddencount",
								textdata: "3"
							},
							{
								flexboxrowclass :"tabletext tbrow3 hiddencount",
								textdata: "3 ones = 3 (three)"
							}
						]
					}

				]
			}
		]
	},
	//slide18
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		singletext:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "greenhigh",
				textclass: "conclubox",
				textdata: data.string.p1text12
			}
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "lowerflex lowmoveup",
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "TENS"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: "6"
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: "6 tens = 60 (sixty)"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "ONES"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: "3"
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: "3 ones = 3 (three)"
							}
						]
					}

				]
			}
		]
	},
	//slide19
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		singletext:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "highsize",
				textclass: "ltb",
				textdata: data.string.p1text13
			}
		],
		fleximages: [
			{
				flexblockadditionalclass: "flexcandyimages",
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								imgclass: "bagimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx3",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx4",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx5",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx6",
								imgid: "box",
								imgscr: "",
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								imgclass: "cdyimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx3",
								imgid: "box",
								imgscr: "",
							}
						]
					}

				]
			}
		]
	}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var flexCopier;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "cover", src: imgpath+"cover.png", type: createjs.AbstractLoader.IMAGE},
			{id: "candy1", src: imgpath+"candy01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "candy2", src: imgpath+"candy02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "candy3", src: imgpath+"candy03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "candy4", src: imgpath+"candy04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "candy5", src: imgpath+"candy05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bag", src: imgpath+"bag.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box", src: imgpath+"box.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"p1_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p1_s1.ogg"},
			{id: "sound_3", src: soundAsset+"p1_s2.ogg"},
			{id: "sound_4", src: soundAsset+"p1_s3.ogg"},
			{id: "sound_5", src: soundAsset+"p1_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p1_s6.ogg"},
			{id: "sound_7", src: soundAsset+"p1_s7.ogg"},
			{id: "sound_8_1", src: soundAsset+"p1_s8_1.ogg"},
			{id: "sound_8_2", src: soundAsset+"p1_s8_2.ogg"},
			{id: "sound_9_1", src: soundAsset+"p1_s9_1.ogg"},
			{id: "sound_9_2", src: soundAsset+"p1_s9_2.ogg"},
			{id: "sound_10", src: soundAsset+"p1_s10.ogg"},
			{id: "sound_11", src: soundAsset+"p1_s11.ogg"},
			{id: "sound_12", src: soundAsset+"p1_s12.ogg"},
			{id: "sound_13", src: soundAsset+"p1_s13.ogg"},
			{id: "sound_14_1", src: soundAsset+"p1_s14_1.ogg"},
			{id: "sound_14_2", src: soundAsset+"p1_s14_2.ogg"},
			{id: "sound_15_1", src: soundAsset+"p1_s15_1.ogg"},
			{id: "sound_15_2", src: soundAsset+"p1_s15_2.ogg"},
			{id: "sound_16", src: soundAsset+"p1_s16.ogg"},
			{id: "sound_17", src: soundAsset+"p1_s17.ogg"},
			{id: "s1", src: soundAsset+"1.ogg"},
			{id: "s2", src: soundAsset+"2.ogg"},
			{id: "s3", src: soundAsset+"3.ogg"},
			{id: "s4", src: soundAsset+"4.ogg"},
			{id: "s5", src: soundAsset+"5.ogg"},
			{id: "s6", src: soundAsset+"6.ogg"},
			{id: "s7", src: soundAsset+"7.ogg"},
			{id: "s8", src: soundAsset+"8.ogg"},
			{id: "s9", src: soundAsset+"9.ogg"},
			{id: "s10", src: soundAsset+"10.ogg"},
			{id: "s21", src: soundAsset+"10.ogg"},
			{id: "s22", src: soundAsset+"20.ogg"},
			{id: "s23", src: soundAsset+"30.ogg"},
			{id: "s24", src: soundAsset+"40.ogg"},
			{id: "s25", src: soundAsset+"50.ogg"},
			{id: "s26", src: soundAsset+"60.ogg"},
			{id: "s27", src: soundAsset+"70.ogg"},
			{id: "s28", src: soundAsset+"80.ogg"},
			{id: "s29", src: soundAsset+"90.ogg"},
			{id: "s210", src: soundAsset+"100.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext){
			case 0:
				sound_player("sound_1", 1);
			break;
			case 1:
				sound_player("sound_2", 0);
				$(".tabletext").css("opacity","0");
				$(".chocclass").empty();
				$(".chocclass").append("<img class='smallcandies' src= '"+ preload.getResult('candy'+rand_gene()).src +"'>");
				setTimeout(function(){
					countTabs(10);
				},3500);
				flexCopier = $(".flexcontainerblock100").html();
			break;
			case 2:
				sound_player("sound_3", 1);
				$(".flexcontainerblock100").empty().html(flexCopier);
				setTimeout(function(){
					$(".flexcontainerblock100").addClass("flexshrink");
				},1000);
			break;
			case 3:
				sound_player("sound_4", 1);
			break;
			case 4:
				$(".tabletext").css("opacity","0");
				countCounter = 1;
				countTabs2(10);
			break;
			case 5:
				sound_player("sound_5", 1);
			break;
			case 6:
				sound_player("sound_6", 1);
			break;
			case 7:
				sound_player("sound_7", 1);
				$(".boximg").attr("src", preload.getResult('box').src);
				$(".bagimg").attr("src", preload.getResult('bag').src);
				$(".cdyimg").attr("src", preload.getResult('candy1').src);
			break;
			case 8:
				diy_sound("sound_8_1", $('.col2'), "sound_8_2");
				$(".boximg").attr("src", preload.getResult('box').src);
				$(".bagimg").attr("src", preload.getResult('bag').src);
				$(".cdyimg").attr("src", preload.getResult('candy1').src);
			break;
			case 9:
				diy_sound("sound_9_1", $('.col3'), "sound_9_2");
				$(".boximg").attr("src", preload.getResult('box').src);
				$(".bagimg").attr("src", preload.getResult('bag').src);
				$(".cdyimg").attr("src", preload.getResult('candy1').src);
			break;
			case 10:
				sound_player("sound_10", 1);
			break;
			case 11:
				sound_player("sound_11", 1);
				$(".boximg").attr("src", preload.getResult('box').src);
				$(".bagimg").attr("src", preload.getResult('bag').src);
				$(".cdyimg").attr("src", preload.getResult('candy1').src);
			break;
			case 12:
				sound_player("sound_12", 1);
			break;
			case 13:
				sound_player("sound_13", 1);
				$(".boximg").attr("src", preload.getResult('box').src);
				$(".bagimg").attr("src", preload.getResult('bag').src);
				$(".cdyimg").attr("src", preload.getResult('candy1').src);
			break;
			case 14:
				diy_sound("sound_14_1", $('.col2'), "sound_14_2");
				$(".boximg").attr("src", preload.getResult('box').src);
				$(".bagimg").attr("src", preload.getResult('bag').src);
				$(".cdyimg").attr("src", preload.getResult('candy1').src);
			break;
			case 15:
				diy_sound("sound_15_1", $('.col3'), "sound_15_2");
				$(".boximg").attr("src", preload.getResult('box').src);
				$(".bagimg").attr("src", preload.getResult('bag').src);
				$(".cdyimg").attr("src", preload.getResult('candy1').src);
			break;
			case 16:
				sound_player("sound_16", 1);
			break;
			case 17:
				sound_player("sound_17", 1);
				$(".boximg").attr("src", preload.getResult('box').src);
				$(".bagimg").attr("src", preload.getResult('bag').src);
				$(".cdyimg").attr("src", preload.getResult('candy1').src);
			break;
		}
	}

	function countTabs(count){
		$(".tbrow"+countCounter).css("opacity","1");
		sound_player("s"+countCounter, 0);
		setTimeout(function(){
			countCounter++;
			if(countCounter <= count)
				countTabs(count);
			else {
				countCounter = 1;
				navigationcontroller();
			}
		}, 1500);
	}

	function countTabs2(count){
		$(".tbrow"+countCounter).css("opacity","1");
		sound_player("s2"+countCounter, 0);
		setTimeout(function(){
			countCounter++;
			if(countCounter <= count)
				countTabs2(count);
			else {
				countCounter = 1;
				navigationcontroller();
			}
		}, 1500);
	}

	function rand_gene(){
		var ranNum = Math.floor((Math.random() * 5) + 1);
		return ranNum;
	}

	function diy_sound(sound_id, howmany, last_sound){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			//howmany.hide(0);
			setTimeout(function(){
				countNums(howmany, last_sound);
			}, 1000);
		});
	}

	var countCounter = 1;
	function countNums(theclass, last_sound){
			theclass.children(".bx"+countCounter).addClass("selectthis");
			sound_player("s"+countCounter, 0);
			setTimeout(function(){
				theclass.children(".bx"+(countCounter)).removeClass("selectthis");
				if(countCounter < theclass.children().length){
					countCounter++;
					countNums(theclass, last_sound);
				}
				else {
					finishIt(last_sound);
				}
			}, 2000);
	}

	function finishIt(last_sound){
		countCounter = 1;
		$(".hiddencount p").show(0);
		sound_player(last_sound, 1);
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

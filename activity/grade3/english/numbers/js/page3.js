var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		headerblock:[
			{
				textdata: data.string.p3text1
			}
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",
				flexblock:[
					{
						flexboxcolumnclass: "column1",
						flexblockcolumn:[
							{
								textdata: "1"
							},
							{
								textdata: "11"
							},
							{
								textdata: "21"
							},
							{
								textdata: "31"
							},
							{
								textdata: "41"
							},
							{
								textdata: "51"
							},
							{
								textdata: "61"
							},
							{
								textdata: "71"
							},
							{
								textdata: "81"
							},
							{
								textdata: "91"
							}
						]
					},
					{
						flexboxcolumnclass: "column1",
						flexblockcolumn:[
							{
								textdata: "2"
							},
							{
								textdata: "12"
							},
							{
								textdata: "22"
							},
							{
								textdata: "32"
							},
							{
								textdata: "42"
							},
							{
								textdata: "52"
							},
							{
								textdata: "62"
							},
							{
								textdata: "72"
							},
							{
								textdata: "82"
							},
							{
								textdata: "92"
							}
						]
					},
					{
						flexboxcolumnclass: "column1",
						flexblockcolumn:[
							{
								textdata: "3"
							},
							{
								textdata: "13"
							},
							{
								textdata: "23"
							},
							{
								textdata: "33"
							},
							{
								textdata: "43"
							},
							{
								textdata: "53"
							},
							{
								textdata: "63"
							},
							{
								textdata: "73"
							},
							{
								textdata: "83"
							},
							{
								textdata: "93"
							}
						]
					},
					{
						flexboxcolumnclass: "column1",
						flexblockcolumn:[
							{
								textdata: "4"
							},
							{
								textdata: "14"
							},
							{
								textdata: "24"
							},
							{
								textdata: "34"
							},
							{
								textdata: "44"
							},
							{
								textdata: "54"
							},
							{
								textdata: "64"
							},
							{
								textdata: "74"
							},
							{
								textdata: "84"
							},
							{
								textdata: "94"
							}
						]
					},
					{
						flexboxcolumnclass: "column1",
						flexblockcolumn:[
							{
								textdata: "5"
							},
							{
								textdata: "15"
							},
							{
								textdata: "25"
							},
							{
								textdata: "35"
							},
							{
								textdata: "45"
							},
							{
								textdata: "55"
							},
							{
								textdata: "65"
							},
							{
								textdata: "75"
							},
							{
								textdata: "85"
							},
							{
								textdata: "95"
							}
						]
					},
					{
						flexboxcolumnclass: "column1",
						flexblockcolumn:[
							{
								textdata: "6"
							},
							{
								textdata: "16"
							},
							{
								textdata: "26"
							},
							{
								textdata: "36"
							},
							{
								textdata: "46"
							},
							{
								textdata: "56"
							},
							{
								textdata: "66"
							},
							{
								textdata: "76"
							},
							{
								textdata: "86"
							},
							{
								textdata: "96"
							}
						]
					},
					{
						flexboxcolumnclass: "column1",
						flexblockcolumn:[
							{
								textdata: "7"
							},
							{
								textdata: "17"
							},
							{
								textdata: "27"
							},
							{
								textdata: "37"
							},
							{
								textdata: "47"
							},
							{
								textdata: "57"
							},
							{
								textdata: "67"
							},
							{
								textdata: "77"
							},
							{
								textdata: "87"
							},
							{
								textdata: "97"
							}
						]
					},
					{
						flexboxcolumnclass: "column1",
						flexblockcolumn:[
							{
								textdata: "8"
							},
							{
								textdata: "18"
							},
							{
								textdata: "28"
							},
							{
								textdata: "38"
							},
							{
								textdata: "48"
							},
							{
								textdata: "58"
							},
							{
								textdata: "68"
							},
							{
								textdata: "78"
							},
							{
								textdata: "88"
							},
							{
								textdata: "98"
							}
						]
					},
					{
						flexboxcolumnclass: "column1",
						flexblockcolumn:[
							{
								textdata: "9"
							},
							{
								textdata: "19"
							},
							{
								textdata: "29"
							},
							{
								textdata: "39"
							},
							{
								textdata: "49"
							},
							{
								textdata: "59"
							},
							{
								textdata: "69"
							},
							{
								textdata: "79"
							},
							{
								textdata: "89"
							},
							{
								textdata: "99"
							}
						]
					},
					{
						flexboxcolumnclass: "column1",
						flexblockcolumn:[
							{
								flexboxrowclass :"bext1",
								textdata: "10"
							},
							{
								flexboxrowclass :"bext2",
								textdata: "20"
							},
							{
								flexboxrowclass :"bext3",
								textdata: "30"
							},
							{
								flexboxrowclass :"bext4",
								textdata: "40"
							},
							{
								flexboxrowclass :"bext5",
								textdata: "50"
							},
							{
								flexboxrowclass :"bext6",
								textdata: "60"
							},
							{
								flexboxrowclass :"bext7",
								textdata: "70"
							},
							{
								flexboxrowclass :"bext8",
								textdata: "80"
							},
							{
								flexboxrowclass :"bext9",
								textdata: "90"
							},
							{
								flexboxrowclass :"bext10",
								textdata: "100"
							}
						]
					}

				]
			}
		]
	},
	// slide1
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		headerblock:[
			{
				textdata: data.string.p3text2
			}
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",
				flexblock:[
					{
						flexboxcolumnclass: "column1",
						flexblockcolumn:[
							{
								textdata: "1"
							},
							{
								textdata: "11"
							},
							{
								textdata: "21"
							},
							{
								textdata: "31"
							},
							{
								textdata: "41"
							},
							{
								textdata: "51"
							},
							{
								textdata: "61"
							},
							{
								textdata: "71"
							},
							{
								textdata: "81"
							},
							{
								textdata: "91"
							}
						]
					},
					{
						flexboxcolumnclass: "column1",
						flexblockcolumn:[
							{
								textdata: "2"
							},
							{
								textdata: "12"
							},
							{
								textdata: "22"
							},
							{
								textdata: "32"
							},
							{
								textdata: "42"
							},
							{
								textdata: "52"
							},
							{
								textdata: "62"
							},
							{
								textdata: "72"
							},
							{
								textdata: "82"
							},
							{
								textdata: "92"
							}
						]
					},
					{
						flexboxcolumnclass: "column1",
						flexblockcolumn:[
							{
								textdata: "3"
							},
							{
								textdata: "13"
							},
							{
								textdata: "23"
							},
							{
								textdata: "33"
							},
							{
								textdata: "43"
							},
							{
								textdata: "53"
							},
							{
								textdata: "63"
							},
							{
								textdata: "73"
							},
							{
								textdata: "83"
							},
							{
								textdata: "93"
							}
						]
					},
					{
						flexboxcolumnclass: "column1",
						flexblockcolumn:[
							{
								textdata: "4"
							},
							{
								textdata: "14"
							},
							{
								textdata: "24"
							},
							{
								textdata: "34"
							},
							{
								textdata: "44"
							},
							{
								textdata: "54"
							},
							{
								textdata: "64"
							},
							{
								textdata: "74"
							},
							{
								textdata: "84"
							},
							{
								textdata: "94"
							}
						]
					},
					{
						flexboxcolumnclass: "column1",
						flexblockcolumn:[
							{
								textdata: "5"
							},
							{
								textdata: "15"
							},
							{
								textdata: "25"
							},
							{
								textdata: "35"
							},
							{
								textdata: "45"
							},
							{
								textdata: "55"
							},
							{
								textdata: "65"
							},
							{
								textdata: "75"
							},
							{
								textdata: "85"
							},
							{
								textdata: "95"
							}
						]
					},
					{
						flexboxcolumnclass: "column1",
						flexblockcolumn:[
							{
								textdata: "6"
							},
							{
								textdata: "16"
							},
							{
								textdata: "26"
							},
							{
								textdata: "36"
							},
							{
								textdata: "46"
							},
							{
								textdata: "56"
							},
							{
								textdata: "66"
							},
							{
								textdata: "76"
							},
							{
								textdata: "86"
							},
							{
								textdata: "96"
							}
						]
					},
					{
						flexboxcolumnclass: "column1",
						flexblockcolumn:[
							{
								textdata: "7"
							},
							{
								textdata: "17"
							},
							{
								textdata: "27"
							},
							{
								textdata: "37"
							},
							{
								textdata: "47"
							},
							{
								textdata: "57"
							},
							{
								textdata: "67"
							},
							{
								textdata: "77"
							},
							{
								textdata: "87"
							},
							{
								textdata: "97"
							}
						]
					},
					{
						flexboxcolumnclass: "column1",
						flexblockcolumn:[
							{
								textdata: "8"
							},
							{
								textdata: "18"
							},
							{
								textdata: "28"
							},
							{
								textdata: "38"
							},
							{
								textdata: "48"
							},
							{
								textdata: "58"
							},
							{
								textdata: "68"
							},
							{
								textdata: "78"
							},
							{
								textdata: "88"
							},
							{
								textdata: "98"
							}
						]
					},
					{
						flexboxcolumnclass: "column1",
						flexblockcolumn:[
							{
								textdata: "9"
							},
							{
								textdata: "19"
							},
							{
								textdata: "29"
							},
							{
								textdata: "39"
							},
							{
								textdata: "49"
							},
							{
								textdata: "59"
							},
							{
								textdata: "69"
							},
							{
								textdata: "79"
							},
							{
								textdata: "89"
							},
							{
								textdata: "99"
							}
						]
					},
					{
						flexboxcolumnclass: "column1",
						flexblockcolumn:[
							{
								flexboxrowclass :"bext1",
								textdata: "10"
							},
							{
								flexboxrowclass :"bext2",
								textdata: "20"
							},
							{
								flexboxrowclass :"bext3",
								textdata: "30"
							},
							{
								flexboxrowclass :"bext4",
								textdata: "40"
							},
							{
								flexboxrowclass :"bext5",
								textdata: "50"
							},
							{
								flexboxrowclass :"bext6",
								textdata: "60"
							},
							{
								flexboxrowclass :"bext7",
								textdata: "70"
							},
							{
								flexboxrowclass :"bext8",
								textdata: "80"
							},
							{
								flexboxrowclass :"bext9",
								textdata: "90"
							},
							{
								flexboxrowclass :"bext10",
								textdata: "100"
							}
						]
					}

				]
			}
		]
	},
	//slide2
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		headerblock:[
			{
				textdata: data.string.p3text3
			}
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",
				flexblock:[]
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "box",
						imgid: "box",
						imgscr: "",
					}
				]
			}
		],
		singletext:[
			{
				textclass: "ltb",
				textdata: data.string.p3text4
			}
		],
	},
	//slide3
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		singletext:[
			{
				textclass: "middletext",
				textdata: data.string.p3text5
			}
		]
	},
	// slide4
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock200",
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "100"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: "200"
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: "300"
							},
							{
								flexboxrowclass :"tabletext tbrow4",
								textdata: "400"
							},
							{
								flexboxrowclass :"tabletext tbrow5",
								textdata: "500"
							},
							{
								flexboxrowclass :"tabletext tbrow6",
								textdata: "600"
							},
							{
								flexboxrowclass :"tabletext tbrow7",
								textdata: "700"
							},
							{
								flexboxrowclass :"tabletext tbrow8",
								textdata: "800"
							},
							{
								flexboxrowclass :"tabletext tbrow9",
								textdata: "900"
							},
							{
								flexboxrowclass :"tabletext tbrow10",
								textdata: "1000"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "ONE HUNDRED"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: "TWO HUNDRED"
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: "THREE HUNDRED"
							},
							{
								flexboxrowclass :"tabletext tbrow4",
								textdata: "FOUR HUNDRED"
							},
							{
								flexboxrowclass :"tabletext tbrow5",
								textdata: "FIVE HUNDRED"
							},
							{
								flexboxrowclass :"tabletext tbrow6",
								textdata: "SIX HUNDRED"
							},
							{
								flexboxrowclass :"tabletext tbrow7",
								textdata: "SEVEN HUNDRED"
							},
							{
								flexboxrowclass :"tabletext tbrow8",
								textdata: "EIGHT HUNDRED"
							},
							{
								flexboxrowclass :"tabletext tbrow9",
								textdata: "NINE HUNDRED"
							},
							{
								flexboxrowclass :"tabletext tbrow10",
								textdata: "ONE THOUSAND"
							}
						]
					}

				]
			}
		]
	},
	//slide 5
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		headerblock:[
			{
				textdata: data.string.p3text6
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "threeimg-1",
						imgid: "box",
						imgscr: "",
					},
					{
						imgclass: "threeimg-2",
						imgid: "box",
						imgscr: "",
					},
					{
						imgclass: "threeimg-3",
						imgid: "box",
						imgscr: "",
					}
				]
			}
		],
		singletext:[
			{
				textclass: "iclabel-1",
				textdata: "+"
			},
			{
				textclass: "iclabel-2",
				textdata: "+"
			},
			{
				textclass: "iclabel-3",
				textdata: "= 300"
			}
		]
	},
	//slide6
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		singletext:[
			{
				textclass: "middletext",
				textdata: data.string.p3text7
			}
		]
	},
	//slide7
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		headerblock:[
			{
				textdata: data.string.p3text8
			}
		],
		fleximages: [
			{
				flexblockadditionalclass: "flexcandyimages",
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								imgclass: "boximg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "boximg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "boximg bx3",
								imgid: "box",
								imgscr: "",
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								imgclass: "bagimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx3",
								imgid: "box",
								imgscr: "",
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								imgclass: "cdyimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx3",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx4",
								imgid: "box",
								imgscr: "",
							}
						]
					}

				]
			}
		]
	},
	//slide8
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		headerblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "highsize",
				textdata: data.string.p3text9
			}
		],
		fleximages: [
			{
				flexblockadditionalclass: "flexcandyimages",
				flexblock:[
					{
						flexboxcolumnclass: "column col1",
						flexblockcolumn:[
							{
								imgclass: "boximg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "boximg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "boximg bx3",
								imgid: "box",
								imgscr: "",
							}
						]
					},
					{
						flexboxcolumnclass: "column col2",
						flexblockcolumn:[
							{
								imgclass: "bagimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx3",
								imgid: "box",
								imgscr: "",
							}
						]
					},
					{
						flexboxcolumnclass: "column col3",
						flexblockcolumn:[
							{
								imgclass: "cdyimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx3",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx4",
								imgid: "box",
								imgscr: "",
							}
						]
					}

				]
			}
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "lowerflex",
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "HUNDREDS"
							},
							{
								flexboxrowclass :"tabletext tbrow2 hiddencount",
								textdata: "3"
							},
							{
								flexboxrowclass :"tabletext tbrow3 hiddencount",
								textdata: "3 hundreds = 300 (three hundred)"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "TENS"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: ""
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: ""
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "ONES"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: ""
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: ""
							}
						]
					}

				]
			}
		]
	},
	//slide9
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		headerblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "highsize",
				textdata: data.string.p3text10
			}
		],
		fleximages: [
			{
				flexblockadditionalclass: "flexcandyimages",
				flexblock:[
					{
						flexboxcolumnclass: "column col1",
						flexblockcolumn:[
							{
								imgclass: "boximg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "boximg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "boximg bx3",
								imgid: "box",
								imgscr: "",
							}
						]
					},
					{
						flexboxcolumnclass: "column col2",
						flexblockcolumn:[
							{
								imgclass: "bagimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx3",
								imgid: "box",
								imgscr: "",
							}
						]
					},
					{
						flexboxcolumnclass: "column col3",
						flexblockcolumn:[
							{
								imgclass: "cdyimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx3",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx4",
								imgid: "box",
								imgscr: "",
							}
						]
					}

				]
			}
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "lowerflex",
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "HUNDREDS"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: "3"
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: "3 hundreds = 300 (three hundred)"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "TENS"
							},
							{
								flexboxrowclass :"tabletext tbrow2 hiddencount",
								textdata: "3"
							},
							{
								flexboxrowclass :"tabletext tbrow3 hiddencount",
								textdata: "3 tens = 30 (thirty)"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "ONES"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: ""
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: ""
							}
						]
					}

				]
			}
		]
	},
	//slide10
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		headerblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "highsize",
				textdata: data.string.p3text11
			}
		],
		fleximages: [
			{
				flexblockadditionalclass: "flexcandyimages",
				flexblock:[
					{
						flexboxcolumnclass: "column col1",
						flexblockcolumn:[
							{
								imgclass: "boximg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "boximg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "boximg bx3",
								imgid: "box",
								imgscr: "",
							}
						]
					},
					{
						flexboxcolumnclass: "column col2",
						flexblockcolumn:[
							{
								imgclass: "bagimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx3",
								imgid: "box",
								imgscr: "",
							}
						]
					},
					{
						flexboxcolumnclass: "column col3",
						flexblockcolumn:[
							{
								imgclass: "cdyimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx3",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx4",
								imgid: "box",
								imgscr: "",
							}
						]
					}

				]
			}
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "lowerflex",
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "HUNDREDS"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: "3"
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: "3 hundreds = 300 (three hundred)"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "TENS"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: "3"
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: "3 tens = 30 (thirty)"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "ONES"
							},
							{
								flexboxrowclass :"tabletext tbrow2 hiddencount",
								textdata: "4"
							},
							{
								flexboxrowclass :"tabletext tbrow3 hiddencount",
								textdata: "4 ones = 4 (four)"
							}
						]
					}

				]
			}
		]
	},
	//slide11
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		singletext:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "greenhigh",
				textclass: "conclubox",
				textdata: data.string.p3text12
			}
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "lowerflex lowmoveup",
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "HUNDREDS"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: "3"
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: "3 hundreds = 300 (three hundred)"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "TENS"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: "3"
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: "3 tens = 30 (thirty)"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "ONES"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: "4"
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: "4 ones = 4 (four)"
							}
						]
					}

				]
			}
		]
	},
	//slide12
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		singletext:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "highsize",
				textclass: "ltb",
				textdata: data.string.p3text13
			}
		],
		fleximages: [
			{
				flexblockadditionalclass: "flexcandyimages",
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								imgclass: "boximg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "boximg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "boximg bx3",
								imgid: "box",
								imgscr: "",
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								imgclass: "bagimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx3",
								imgid: "box",
								imgscr: "",
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								imgclass: "cdyimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx3",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx4",
								imgid: "box",
								imgscr: "",
							}
						]
					}

				]
			}
		]
	},
	//slide13
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		singletext:[
			{
				textclass: "middletext",
				textdata: data.string.p3text7
			}
		]
	},
	//slide14
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		headerblock:[
			{
				textdata: data.string.p3text8
			}
		],
		fleximages: [
			{
				flexblockadditionalclass: "flexcandyimages",
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								imgclass: "boximg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "boximg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "boximg bx3",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "boximg bx3",
								imgid: "box",
								imgscr: "",
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								imgclass: "bagimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx2",
								imgid: "box",
								imgscr: "",
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								imgclass: "cdyimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx3",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx4",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx5",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx6",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx7",
								imgid: "box",
								imgscr: "",
							}
						]
					}

				]
			}
		]
	},
	//slide15
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		headerblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "highsize",
				textdata: data.string.p3text9
			}
		],
		fleximages: [
			{
				flexblockadditionalclass: "flexcandyimages",
				flexblock:[
					{
						flexboxcolumnclass: "column col1",
						flexblockcolumn:[
							{
								imgclass: "boximg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "boximg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "boximg bx3",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "boximg bx4",
								imgid: "box",
								imgscr: "",
							}
						]
					},
					{
						flexboxcolumnclass: "column col2",
						flexblockcolumn:[
							{
								imgclass: "bagimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx2",
								imgid: "box",
								imgscr: "",
							}
						]
					},
					{
						flexboxcolumnclass: "column col3",
						flexblockcolumn:[
							{
								imgclass: "cdyimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx3",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx4",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx5",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx6",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx7",
								imgid: "box",
								imgscr: "",
							}
						]
					}

				]
			}
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "lowerflex",
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "HUNDREDS"
							},
							{
								flexboxrowclass :"tabletext tbrow2 hiddencount",
								textdata: "4"
							},
							{
								flexboxrowclass :"tabletext tbrow3 hiddencount",
								textdata: "4 hundreds = 400 (four hundred)"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "TENS"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: ""
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: ""
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "ONES"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: ""
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: ""
							}
						]
					}

				]
			}
		]
	},
	//slide16
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		headerblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "highsize",
				textdata: data.string.p3text10
			}
		],
		fleximages: [
			{
				flexblockadditionalclass: "flexcandyimages",
				flexblock:[
					{
						flexboxcolumnclass: "column col1",
						flexblockcolumn:[
							{
								imgclass: "boximg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "boximg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "boximg bx3",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "boximg bx4",
								imgid: "box",
								imgscr: "",
							}
						]
					},
					{
						flexboxcolumnclass: "column col2",
						flexblockcolumn:[
							{
								imgclass: "bagimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx2",
								imgid: "box",
								imgscr: "",
							}
						]
					},
					{
						flexboxcolumnclass: "column col3",
						flexblockcolumn:[
							{
								imgclass: "cdyimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx3",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx4",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx5",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx6",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx7",
								imgid: "box",
								imgscr: "",
							}
						]
					}

				]
			}
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "lowerflex",
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "HUNDREDS"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: "4"
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: "4 hundreds = 400 (four hundred)"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "TENS"
							},
							{
								flexboxrowclass :"tabletext tbrow2 hiddencount",
								textdata: "2"
							},
							{
								flexboxrowclass :"tabletext tbrow3 hiddencount",
								textdata: "2 tens = 20 (twenty)"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "ONES"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: ""
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: ""
							}
						]
					}

				]
			}
		]
	},
	//slide17
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		headerblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "highsize",
				textdata: data.string.p3text11
			}
		],
		fleximages: [
			{
				flexblockadditionalclass: "flexcandyimages",
				flexblock:[
					{
						flexboxcolumnclass: "column col1",
						flexblockcolumn:[
							{
								imgclass: "boximg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "boximg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "boximg bx3",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "boximg bx4",
								imgid: "box",
								imgscr: "",
							}
						]
					},
					{
						flexboxcolumnclass: "column col2",
						flexblockcolumn:[
							{
								imgclass: "bagimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx2",
								imgid: "box",
								imgscr: "",
							}
						]
					},
					{
						flexboxcolumnclass: "column col3",
						flexblockcolumn:[
							{
								imgclass: "cdyimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx3",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx4",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx5",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx6",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx7",
								imgid: "box",
								imgscr: "",
							}
						]
					}

				]
			}
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "lowerflex",
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "HUNDREDS"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: "4"
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: "4 hundreds = 400 (four hundred)"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "TENS"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: "2"
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: "2 tens = 20 (twenty)"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "ONES"
							},
							{
								flexboxrowclass :"tabletext tbrow2 hiddencount",
								textdata: "7"
							},
							{
								flexboxrowclass :"tabletext tbrow3 hiddencount",
								textdata: "7 ones = 7 (seven)"
							}
						]
					}

				]
			}
		]
	},
	//slide18
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		singletext:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "greenhigh",
				textclass: "conclubox",
				textdata: data.string.p3text14
			}
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "lowerflex lowmoveup",
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "HUNDREDS"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: "4"
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: "4 hundreds = 400 (four hundred)"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "TENS"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: "2"
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: "2 tens = 20 (twenty)"
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								flexboxrowclass :"tabletext tbrow1",
								textdata: "ONES"
							},
							{
								flexboxrowclass :"tabletext tbrow2",
								textdata: "7"
							},
							{
								flexboxrowclass :"tabletext tbrow3",
								textdata: "7 ones = 7 (seven)"
							}
						]
					}

				]
			}
		]
	},
	//slide19
	{
		contentblockadditionalclass: "ole-background-gradient-blunatic",
		singletext:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "highsize",
				textclass: "ltb",
				textdata: data.string.p3text15
			}
		],
		fleximages: [
			{
				flexblockadditionalclass: "flexcandyimages",
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								imgclass: "boximg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "boximg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "boximg bx3",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "boximg bx4",
								imgid: "box",
								imgscr: "",
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								imgclass: "bagimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "bagimg bx2",
								imgid: "box",
								imgscr: "",
							}
						]
					},
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[
							{
								imgclass: "cdyimg bx1",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx2",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx3",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx4",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx5",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx6",
								imgid: "box",
								imgscr: "",
							},
							{
								imgclass: "cdyimg bx7",
								imgid: "box",
								imgscr: "",
							}
						]
					}

				]
			}
		]
	}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var flexCopier;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "candy1", src: imgpath+"candy01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "candy2", src: imgpath+"candy02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "candy3", src: imgpath+"candy03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "candy4", src: imgpath+"candy04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "candy5", src: imgpath+"candy05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bag", src: imgpath+"bag.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box", src: imgpath+"box.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"p3_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p3_s1.ogg"},
			{id: "sound_3", src: soundAsset+"p3_s2.ogg"},
			{id: "sound_4", src: soundAsset+"p3_s3.ogg"},
			{id: "sound_5", src: soundAsset+"p3_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p3_s6.ogg"},
			{id: "sound_7", src: soundAsset+"p3_s7.ogg"},
			{id: "sound_8_1", src: soundAsset+"p3_s8_1.ogg"},
			{id: "sound_8_2", src: soundAsset+"p3_s8_2.ogg"},
			{id: "sound_9_1", src: soundAsset+"p3_s9_1.ogg"},
			{id: "sound_9_2", src: soundAsset+"p3_s9_2.ogg"},
			{id: "sound_10_1", src: soundAsset+"p3_s10_1.ogg"},
			{id: "sound_10_2", src: soundAsset+"p3_s10_2.ogg"},
			{id: "sound_11", src: soundAsset+"p3_s11.ogg"},
			{id: "sound_12", src: soundAsset+"p3_s12.ogg"},
			{id: "sound_13", src: soundAsset+"p3_s13.ogg"},
			{id: "sound_14", src: soundAsset+"p3_s14.ogg"},
			{id: "sound_15_1", src: soundAsset+"p3_s15_1.ogg"},
			{id: "sound_15_2", src: soundAsset+"p3_s15_2.ogg"},
			{id: "sound_16_1", src: soundAsset+"p3_s16_1.ogg"},
			{id: "sound_16_2", src: soundAsset+"p3_s16_2.ogg"},
			{id: "sound_17_1", src: soundAsset+"p3_s17_1.ogg"},
			{id: "sound_17_2", src: soundAsset+"p3_s17_2.ogg"},
			{id: "sound_18", src: soundAsset+"p3_s18.ogg"},
			{id: "sound_19", src: soundAsset+"p3_s19.ogg"},
			{id: "s1", src: soundAsset+"1.ogg"},
			{id: "s2", src: soundAsset+"2.ogg"},
			{id: "s3", src: soundAsset+"3.ogg"},
			{id: "s4", src: soundAsset+"4.ogg"},
			{id: "s5", src: soundAsset+"5.ogg"},
			{id: "s6", src: soundAsset+"6.ogg"},
			{id: "s7", src: soundAsset+"7.ogg"},
			{id: "s8", src: soundAsset+"8.ogg"},
			{id: "s9", src: soundAsset+"9.ogg"},
			{id: "s10", src: soundAsset+"10.ogg"},
			{id: "s21", src: soundAsset+"100.ogg"},
			{id: "s22", src: soundAsset+"200.ogg"},
			{id: "s23", src: soundAsset+"300.ogg"},
			{id: "s24", src: soundAsset+"400.ogg"},
			{id: "s25", src: soundAsset+"500.ogg"},
			{id: "s26", src: soundAsset+"600.ogg"},
			{id: "s27", src: soundAsset+"700.ogg"},
			{id: "s28", src: soundAsset+"800.ogg"},
			{id: "s29", src: soundAsset+"900.ogg"},
			{id: "s210", src: soundAsset+"1000.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext){
			case 0:
				sound_player("sound_1", 1);
			break;
			case 1:
			sound_player("sound_2", 1);
			setTimeout(function(){
				$(".column1 div p").animate({
					"opacity": 0
				},1000, function(){
					$(this).empty();
					$(this).css("opacity","1");
					$(this).append("<img class='smallcandies' src= '"+ preload.getResult('candy'+rand_gene()).src +"'>");
					flexCopier = $(".flexcontainerblock100").html();
					// navigationcontroller();
				});
			},2000);
			break;
			case 2:
			sound_player("sound_3", 1);
			$(".flexcontainerblock100").empty().html(flexCopier);
			setTimeout(function(){
				$(".flexcontainerblock100").addClass("flexshrink");
				// navigationcontroller();
			},1000);
			break;
			case 3:
				sound_player("sound_4", 1);
			break;
			case 4:
				$(".tabletext").css("opacity","0");
				countCounter = 1;
				countTabs(10);
			break;
			case 5:
				sound_player("sound_5", 1);
			break;
			case 6:
				sound_player("sound_6", 1);
			break;
			case 7:
			sound_player("sound_7", 1);
				$(".boximg").attr("src", preload.getResult('box').src);
				$(".bagimg").attr("src", preload.getResult('bag').src);
				$(".cdyimg").attr("src", preload.getResult('candy1').src);
			break;
			case 8:
				diy_sound("sound_15_1", $('.col1'), "sound_8_2");
				$(".boximg").attr("src", preload.getResult('box').src);
				$(".bagimg").attr("src", preload.getResult('bag').src);
				$(".cdyimg").attr("src", preload.getResult('candy1').src);
			break;
			case 9:
			diy_sound("sound_9_1", $('.col2'), "sound_9_2");
				$(".boximg").attr("src", preload.getResult('box').src);
				$(".bagimg").attr("src", preload.getResult('bag').src);
				$(".cdyimg").attr("src", preload.getResult('candy1').src);
			break;
			case 10:
			diy_sound("sound_10_1", $('.col3'), "sound_10_2");
				$(".boximg").attr("src", preload.getResult('box').src);
				$(".bagimg").attr("src", preload.getResult('bag').src);
				$(".cdyimg").attr("src", preload.getResult('candy1').src);
			break;
			case 11:
				sound_player("sound_11", 1);
			break;
			case 12:
			sound_player("sound_12", 1);
				$(".boximg").attr("src", preload.getResult('box').src);
				$(".bagimg").attr("src", preload.getResult('bag').src);
				$(".cdyimg").attr("src", preload.getResult('candy1').src);
			break;
			case 13:
				sound_player("sound_13", 1);
			break;
			case 14:
			sound_player("sound_14", 1);
				$(".boximg").attr("src", preload.getResult('box').src);
				$(".bagimg").attr("src", preload.getResult('bag').src);
				$(".cdyimg").attr("src", preload.getResult('candy1').src);
			break;
			case 15:
			diy_sound("sound_15_1", $('.col1'), "sound_15_2");
				$(".boximg").attr("src", preload.getResult('box').src);
				$(".bagimg").attr("src", preload.getResult('bag').src);
				$(".cdyimg").attr("src", preload.getResult('candy1').src);
			break;
			case 16:
			diy_sound("sound_16_1", $('.col2'), "sound_16_2");
				$(".boximg").attr("src", preload.getResult('box').src);
				$(".bagimg").attr("src", preload.getResult('bag').src);
				$(".cdyimg").attr("src", preload.getResult('candy1').src);
			break;
			case 17:
			diy_sound("sound_17_1", $('.col3'), "sound_17_2");
				$(".boximg").attr("src", preload.getResult('box').src);
				$(".bagimg").attr("src", preload.getResult('bag').src);
				$(".cdyimg").attr("src", preload.getResult('candy1').src);
			break;
			case 18:
				sound_player("sound_18", 1);
			break;
			case 19:
			sound_player("sound_19", 1);
				$(".boximg").attr("src", preload.getResult('box').src);
				$(".bagimg").attr("src", preload.getResult('bag').src);
				$(".cdyimg").attr("src", preload.getResult('candy1').src);
			break;
		}
	}

	function countTabs(count){
		$(".tbrow"+countCounter).css("opacity","1");
		sound_player("s2"+countCounter, 0);
		setTimeout(function(){
			countCounter++;
			if(countCounter <= count)
				countTabs(count);
			else {
				countCounter = 1;
				navigationcontroller();
			}
		}, 1700);
	}

	function rand_gene(){
		var ranNum = Math.floor((Math.random() * 5) + 1);
		return ranNum;
	}

	function diy_sound(sound_id, howmany, last_sound){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			//howmany.hide(0);
			setTimeout(function(){
				countNums(howmany, last_sound);
			}, 1000);
		});
	}

	var countCounter = 1;
	function countNums(theclass, last_sound){
			theclass.children(".bx"+countCounter).addClass("selectthis");
			sound_player("s"+countCounter, 0);
			setTimeout(function(){
				theclass.children(".bx"+(countCounter)).removeClass("selectthis");
				if(countCounter < theclass.children().length){
					countCounter++;
					countNums(theclass, last_sound);
				}
				else {
					finishIt(last_sound);
				}
			}, 2000);
	}

	function finishIt(last_sound){
		countCounter = 1;
		$(".hiddencount p").show(0);
		sound_player(last_sound, 1);
	}


	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

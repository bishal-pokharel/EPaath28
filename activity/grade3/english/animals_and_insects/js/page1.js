var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";



var content=[
	//slide 0
	{
		contentblockadditionalclass: 'firsttwoslides_bg',
		uppertextblockadditionalclass: 'instruction_div1',


		uppertextblock : [{
			textdata : data.string.p1_s0,
			textclass : 'p1_s0',
			datahighlightflag : true,
			datahighlightcustomclass : 'redandblue',
		}
		],
        optionsblockadditionalclass: 'coverpage',
        optionsblock : [{
            imgsrc : imgpath + "cover_page.png",
            containerclass : '',
        },]

	},
	//slide 1
	{
		contentblockadditionalclass: 'firsttwoslides_bg',
		uppertextblockadditionalclass: 'instruction_div title' ,


		uppertextblock : [{
			textdata : data.string.p1_s1,
			textclass : 'p1_s1',
			datahighlightflag : true,
			datahighlightcustomclass : 'redandblue',
            datahighlightcustomclass2 : 'redandblue1',
            datahighlightcustomclass2 : 'redandblue2',
		}
		],

	},

	//slide 2
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext1,
			textclass : 'topText',

		},
		{
			textdata : data.string.etext12,
			textclass : 'middleText',
		},
		{
			textdata : data.string.etext13,
			textclass : 'bottomText',
		},
		{
			textdata : data.string.generalQuestion,
			textclass : 'generalQuestionClass',
		}],

		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			imgsrc : imgpath + "bee.jpg",
			containerclass : 'class1 options',
			transparentdata : data.string.bee,
			transparentclass : "transparentText"

		},
		{
			imgsrc : imgpath + "butterfly.jpg",
			containerclass : 'class2 options',
			transparentdata : data.string.butterfly,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "cat.jpg",
			containerclass : 'class3 options',
			transparentdata : data.string.cat,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "dog.jpg",
			containerclass : 'class4 options',
			transparentdata : data.string.dog,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "elephant.jpg",
			containerclass : 'class5 options',
			transparentdata : data.string.elephant,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "ostrich.jpg",
			containerclass : 'class6 options correctAns',
			transparentdata : data.string.ostrich,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "snake.jpg",
			containerclass : 'class7 options',
			transparentdata : data.string.snake,
			transparentclass : " transparentText"
		},

		{
			imgsrc : imgpath + "yak.jpg",
			containerclass : 'class8 options',
			transparentdata : data.string.yak,
			transparentclass : "transparentText"
		}],

	},
	//slide 3
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext2,
			textclass : 'topText',
		},
		{
			textdata : data.string.etext22,
			textclass : 'middleText',
		},
		{
			textdata : data.string.etext23,
			textclass : 'bottomText',
		},
		{
			textdata : data.string.generalQuestion,
			textclass : 'generalQuestionClass',
		}],

		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			imgsrc : imgpath + "bee.jpg",
			containerclass : 'class1 options',
			transparentdata : data.string.bee,
			transparentclass : "transparentText"
		},
		{
			imgsrc : imgpath + "butterfly.jpg",
			containerclass : 'class2 options correctAns',
			transparentdata : data.string.butterfly,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "cat.jpg",
			containerclass : 'class3 options',
			transparentdata : data.string.cat,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "dog.jpg",
			containerclass : 'class4 options',
			transparentdata : data.string.dog,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "elephant.jpg",
			containerclass : 'class5 options',
			transparentdata : data.string.elephant,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "ostrich.jpg",
			containerclass : 'class6 options',
			transparentdata : data.string.ostrich,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "snake.jpg",
			containerclass : 'class7 options',
			transparentdata : data.string.snake,
			transparentclass : " transparentText"
		},

		{
			imgsrc : imgpath + "yak.jpg",
			containerclass : 'class8 options',
			transparentdata : data.string.yak,
			transparentclass : "transparentText"
		}],
	},
	//slide 4
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext3,
			textclass : 'topText',
		},
		{
			textdata : data.string.etext32,
			textclass : 'middleText',
		},
		{
			textdata : data.string.etext33,
			textclass : 'bottomText',
		},
		{
			textdata : data.string.generalQuestion,
			textclass : 'generalQuestionClass',
		}],

		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			imgsrc : imgpath + "bee.jpg",
			containerclass : 'class1 options',
			transparentdata : data.string.bee,
			transparentclass : "transparentText"
		},
		{
			imgsrc : imgpath + "butterfly.jpg",
			containerclass : 'class2 options',
			transparentdata : data.string.butterfly,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "cat.jpg",
			containerclass : 'class3 options correctAns',
			transparentdata : data.string.cat,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "dog.jpg",
			containerclass : 'class4 options',
			transparentdata : data.string.dog,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "elephant.jpg",
			containerclass : 'class5 options',
			transparentdata : data.string.elephant,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "ostrich.jpg",
			containerclass : 'class6 options',
			transparentdata : data.string.ostrich,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "snake.jpg",
			containerclass : 'class7 options',
			transparentdata : data.string.snake,
			transparentclass : " transparentText"
		},

		{
			imgsrc : imgpath + "yak.jpg",
			containerclass : 'class8 options',
			transparentdata : data.string.yak,
			transparentclass : "transparentText"
		}],
	},
	//slide 5
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext4,
			textclass : 'topText',
		},
		{
			textdata : data.string.etext42,
			textclass : 'middleText',
		},
		{
			textdata : data.string.etext43,
			textclass : 'bottomText',
		},
		{
			textdata : data.string.generalQuestion,
			textclass : 'generalQuestionClass',
		}],

		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			imgsrc : imgpath + "bee.jpg",
			containerclass : 'class1 options',
			transparentdata : data.string.bee,
			transparentclass : "transparentText"
		},
		{
			imgsrc : imgpath + "butterfly.jpg",
			containerclass : 'class2 options',
			transparentdata : data.string.butterfly,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "cat.jpg",
			containerclass : 'class3 options',
			transparentdata : data.string.cat,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "dog.jpg",
			containerclass : 'class4 options',
			transparentdata : data.string.dog,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "elephant.jpg",
			containerclass : 'class5 options',
			transparentdata : data.string.elephant,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "ostrich.jpg",
			containerclass : 'class6 options',
			transparentdata : data.string.ostrich,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "snake.jpg",
			containerclass : 'class7 options correctAns',
			transparentdata : data.string.snake,
			transparentclass : " transparentText"
		},

		{
			imgsrc : imgpath + "yak.jpg",
			containerclass : 'class8 options',
			transparentdata : data.string.yak,
			transparentclass : "transparentText"
		}],
	},
	//slide 6
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext5,
			textclass : 'topText',
		},
		{
			textdata : data.string.etext52,
			textclass : 'middleText',
		},
		{
			textdata : data.string.etext53,
			textclass : 'bottomText',
		},
		{
			textdata : data.string.generalQuestion,
			textclass : 'generalQuestionClass',
		}],

		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			imgsrc : imgpath + "bee.jpg",
			containerclass : 'class1 options',
			transparentdata : data.string.bee,
			transparentclass : "transparentText"
		},
		{
			imgsrc : imgpath + "butterfly.jpg",
			containerclass : 'class2 options',
			transparentdata : data.string.butterfly,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "cat.jpg",
			containerclass : 'class3 options',
			transparentdata : data.string.cat,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "dog.jpg",
			containerclass : 'class4 options correctAns',
			transparentdata : data.string.dog,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "elephant.jpg",
			containerclass : 'class5 options',
			transparentdata : data.string.elephant,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "ostrich.jpg",
			containerclass : 'class6 options',
			transparentdata : data.string.ostrich,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "snake.jpg",
			containerclass : 'class7 options',
			transparentdata : data.string.snake,
			transparentclass : " transparentText"
		},

		{
			imgsrc : imgpath + "yak.jpg",
			containerclass : 'class8 options',
			transparentdata : data.string.yak,
			transparentclass : "transparentText"
		}],
	},
	//slide 7
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext6,
			textclass : 'topText',
		},
		{
			textdata : data.string.etext62,
			textclass : 'middleText',
		},
		{
			textdata : data.string.etext63,
			textclass : 'bottomText',
		},
		{
			textdata : data.string.generalQuestion,
			textclass : 'generalQuestionClass',
		}],

		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			imgsrc : imgpath + "bee.jpg",
			containerclass : 'class1 options',
			transparentdata : data.string.bee,
			transparentclass : "transparentText"
		},
		{
			imgsrc : imgpath + "butterfly.jpg",
			containerclass : 'class2 options',
			transparentdata : data.string.butterfly,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "cat.jpg",
			containerclass : 'class3 options',
			transparentdata : data.string.cat,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "dog.jpg",
			containerclass : 'class4 options',
			transparentdata : data.string.dog,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "elephant.jpg",
			containerclass : 'class5 options',
			transparentdata : data.string.elephant,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "ostrich.jpg",
			containerclass : 'class6 options',
			transparentdata : data.string.ostrich,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "snake.jpg",
			containerclass : 'class7 options',
			transparentdata : data.string.snake,
			transparentclass : " transparentText"
		},

		{
			imgsrc : imgpath + "yak.jpg",
			containerclass : 'class8 options correctAns',
			transparentdata : data.string.yak,
			transparentclass : "transparentText"
		}],
	},
	//slide 8
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext7,
			textclass : 'topText',
		},
		{
			textdata : data.string.etext72,
			textclass : 'middleText',
		},
		{
			textdata : data.string.etext73,
			textclass : 'bottomText',
		},
		{
			textdata : data.string.generalQuestion,
			textclass : 'generalQuestionClass',
		}],

		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			imgsrc : imgpath + "bee.jpg",
			containerclass : 'class1 options',
			transparentdata : data.string.bee,
			transparentclass : "transparentText"
		},
		{
			imgsrc : imgpath + "butterfly.jpg",
			containerclass : 'class2 options',
			transparentdata : data.string.butterfly,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "cat.jpg",
			containerclass : 'class3 options',
			transparentdata : data.string.cat,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "dog.jpg",
			containerclass : 'class4 options',
			transparentdata : data.string.dog,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "elephant.jpg",
			containerclass : 'class5 options correctAns',
			transparentdata : data.string.elephant,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "ostrich.jpg",
			containerclass : 'class6 options',
			transparentdata : data.string.ostrich,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "snake.jpg",
			containerclass : 'class7 options',
			transparentdata : data.string.snake,
			transparentclass : " transparentText"
		},

		{
			imgsrc : imgpath + "yak.jpg",
			containerclass : 'class8 options',
			transparentdata : data.string.yak,
			transparentclass : "transparentText"
		}],
	},
	//slide 9
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext8,
			textclass : 'topText',
		},
		{
			textdata : data.string.etext82,
			textclass : 'middleText',
		},
		{
			textdata : data.string.etext83,
			textclass : 'bottomText',
		},
		{
			textdata : data.string.generalQuestion,
			textclass : 'generalQuestionClass',
		}],

		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			imgsrc : imgpath + "bee.jpg",
			containerclass : 'class1 options correctAns',
			transparentdata : data.string.bee,
			transparentclass : "transparentText"
		},
		{
			imgsrc : imgpath + "butterfly.jpg",
			containerclass : 'class2 options',
			transparentdata : data.string.butterfly,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "cat.jpg",
			containerclass : 'class3 options',
			transparentdata : data.string.cat,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "dog.jpg",
			containerclass : 'class4 options',
			transparentdata : data.string.dog,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "elephant.jpg",
			containerclass : 'class5 options',
			transparentdata : data.string.elephant,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "ostrich.jpg",
			containerclass : 'class6 options',
			transparentdata : data.string.ostrich,
			transparentclass : " transparentText"
		},
		{
			imgsrc : imgpath + "snake.jpg",
			containerclass : 'class7 options',
			transparentdata : data.string.snake,
			transparentclass : " transparentText"
		},

		{
			imgsrc : imgpath + "yak.jpg",
			containerclass : 'class8 options',
			transparentdata : data.string.yak,
			transparentclass : "transparentText"
		}],
	},

];

//content.shufflearray()


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);

  	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            // sounds
            {id: "sound_1", src: soundAsset + "s1_p1.ogg"},
            {id: "sound_2", src: soundAsset + "s1_p2.ogg"},
            {id: "sound_3", src: soundAsset + "s1_p3.ogg"},
            {id: "sound_4", src: soundAsset + "s1_p4.ogg"},
            {id: "sound_5", src: soundAsset + "s1_p5.ogg"},
            {id: "sound_6", src: soundAsset + "s1_p6.ogg"},
            {id: "sound_7", src: soundAsset + "s1_p7.ogg"},
            {id: "sound_8", src: soundAsset + "s1_p8.ogg"},
            {id: "sound_9", src: soundAsset + "s1_p9.ogg"},
            {id: "sound_10", src: soundAsset + "s1_p10.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();
/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    /*===============================================
	 =            data highlight function            =
	 ===============================================*/

    /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
       CODE MODIFIED FOR THREE DIFFERENT HIGHTLIGHTS
    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightstarttag2;
        var texthighlightstarttag3;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                $(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
                    (stylerulename2 = "parsedstring2") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";
                texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);

                $(this).html(replaceinstring);
            });
        }
    }
    /*=====  End of data highlight function  ======*/



    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){

		if (countNext == 0 && $total_page != 1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }
  /*=================================================
  =            ggeneralTemeneral template function            =
  =================================================*/
 function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		texthighlight($board);
		vocabcontroller.findwords(countNext);

		$nextBtn.hide(0);
		$prevBtn.hide(0);
		countNext==0?sound_player("sound_"+(countNext+1),true):
		setTimeout(function(){
            sound_player("sound_"+(countNext+1),true);
        },1500);

		switch(countNext){
			case 1:
				$(".redandblue").eq(1).addClass("redandblue1");
				$nextBtn.hide(0);
			break;
			case 9:
				$nextBtn.css('display', 'none');
			break;


		}


		//randomize options
		var parent = $(".optionsblock");
		var divs = parent.children();
		while (divs.length) {
	    	parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
		$('.question_number').html('Question: '+ (countNext+1));
		var wrong_clicked = false;

     $('.rightic').css({
         'display': 'none'
     });
     $('.wrongic1').css({
         'display': 'none'
     });
     $('.wrongic2').css({
         'display': 'none'
     });
     $('.wrongic3').css({
         'display': 'none'
     });
     $('.wrongic4').css({
         'display': 'none'
     });
     $('.wrongic5').css({
         'display': 'none'
     });
     $('.wrongic7').css({
         'display': 'none'
     });
     $('.wrongic8').css({
         'display': 'none'
     });


     $('.options').click(function(){
         if($(this).hasClass('correctAns')){
             var $this = $(this);
             var position = $this.position();
             var width = $this.width();
             var height = $this.height();
             var centerX = ((position.left + width / 2)*100)/$('.contentblock').width()+'%';
             var centerY = (100-((position.top + height)*100)/$('.contentblock').height())+'%';
             // console.log($this.attr('?????class'));

             if($this.hasClass('class1')||$this.hasClass('class2')||$this.hasClass('class3')||$this.hasClass('class4'))
             {
                 $('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;height:5%;transform:translate(-38%,129%);z-index:2;" src="images/right.png" />').insertAfter(this);

             }
             else{
                 $('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;height:5%;transform:translate(-38%,-461%);z-index:2;" src="images/right.png" />').insertAfter(this);

             }


             $(this).css({'background':'#92AF3B','border-color':'#deef3c'});
             $('.options').css({'pointer-events':'none'})
             createjs.Sound.stop();
             createjs.Sound.stop();
             play_correct_incorrect_sound(1);
             // if(clickwrong==0){
             //     numbertemplate.update(true);
             // }

             nav_button_controls(100);
         }
         else{
             // clickwrong++;
             // numbertemplate.upd/ate(false);
             var $this = $(this);
             var position = $this.position();
             var width = $this.width();
             var height = $this.height();
             var centerX = ((position.left + width / 2)*100)/$('.contentblock').width()+'%';
             var centerY = (100-((position.top + height)*100)/$('.contentblock').height())+'%';
             if($this.hasClass('class1')||$this.hasClass('class2')||$this.hasClass('class3')||$this.hasClass('class4'))
             {
                 $('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;height:5%;transform:translate(-38%,129%);z-index:2;" src="images/wrong.png"/>').insertAfter(this);

             }
             else{
                 $('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;height:5%;transform:translate(-38%,-461%);z-index:2;" src="images/wrong.png"/>').insertAfter(this);

             }
             $(this).css({'background':'#ff0000','pointer-events':'none','border-color':'#980000','color':'black'});
             play_correct_incorrect_sound(0);

         }
     });

		// $(".options").click(function(){
        //
        //
         // //   console.log("correct clicked", wrong_clicked)
        //
        //
         //    if($(this).hasClass("correctAns")){
        //
         //        console.log("m in correct section")
        //
		// 		if(!wrong_clicked){
        //
         //             console.log("clicked value:",wrong_clicked)
         //            //
         //            // $('.rightic').show(0)
         //            // $('.wrongic1').hide(0)
         //            //
         //            // $('.wrongic2').hide(0)
         //            //
         //            // $('.wrongic3').hide(0)
         //            //
         //            // $('.wrongic4').hide(0)
         //            //
         //            // $('.wrongic5').hide(0)
         //            //
         //            // $('.wrongic7').hide(0)
         //            //
         //            // $('.wrongic8').hide(0)
        //
        //
		// 			$nextBtn.hide(0);
		// 			$prevBtn.hide(0);
        //
		// 		}
		// 		$(".options").css('pointer-events', 'none');
		// 		$(this).css({
		// 			'border': '0.2em solid #6EB260',
		// 			'background-color': '#6EB260',
		// 			'color' : '#6EB260',
        //
		// 		});
        //
         //        // $($(this).hasClass('wrongic1')){
         //         //    console.log("m in wrongic1 section")
		// 		// }
        //
		// 		play_correct_incorrect_sound(1);
		// 		if(countNext != $total_page){
		// 			$nextBtn.show(0);
		// 		}
		// 		if (countNext == 9){
		// 			$nextBtn.hide(0);
		// 			ole.footerNotificationHandler.pageEndSetNotification();
		// 		}
        //
		// 	}
		// 	else{
        //
         //    	console.log("m in wrong section")
        //
		// 		$(this).css({
		// 			'background-color': '#BA6B82',
		// 			'border': '0.2em solid #BA6B82',
		// 			'color' : '#BA6B82',
		// 		});
		// 		play_correct_incorrect_sound(0);
		// 		wrong_clicked = true;
		// 	}
		// });
	}



/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */
  function nav_button_controls(delay_ms){
      timeoutvar = setTimeout(function(){
          if(countNext==0){
              $nextBtn.show(0);
          } else if( countNext>0 && countNext == $total_page-1){
              $prevBtn.show(0);
              ole.footerNotificationHandler.pageEndSetNotification();
          } else{
              $prevBtn.show(0);
              $nextBtn.show(0);
          }
      },delay_ms);
  }
  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');


    loadTimelineProgress($total_page,countNext+1);
    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


    //call the slide indication bar handler for pink indicators

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

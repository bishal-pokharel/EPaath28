var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.question
            },
            {
                query:true,
                textdiv:"question",
                textclass: "content2 centertext",
                textdata: data.string.q1,
                ans:data.string.elephant
            },

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "option opt1",
                    imgclass: "relativecls img1",
                    imgid: 'fishImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.fish
                        }
                    ]
                },
                {
                    imgdiv: "option opt2",
                    imgclass: "relativecls img2",
                    imgid: 'ostrichImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.ostrich
                        }
                    ]
                },
                {
                    imgdiv: "option opt3",
                    imgclass: "relativecls img3",
                    imgid: 'spiderImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:" content2 images",
                            textdata:data.string.spider
                        }
                    ]
                },
                {
                    imgdiv: "option opt4",
                    imgclass: "relativecls img4",
                    imgid: 'elephantImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.elephant
                        }
                    ]
                },
            ]
        }],
    },
//    slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.question
            },
            {
                query:true,
                textdiv:"question",
                textclass: "content2 centertext",
                textdata: data.string.q2,
                ans:data.string.bee
            },

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "option opt1",
                    imgclass: "relativecls img1",
                    imgid: 'beeImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.bee
                        }
                    ]
                },
                {
                    imgdiv: "option opt2",
                    imgclass: "relativecls img2",
                    imgid: 'butterflyImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.butterfly
                        }
                    ]
                },
                {
                    imgdiv: "option opt3",
                    imgclass: "relativecls img3",
                    imgid: 'snakeImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:" content2 images",
                            textdata:data.string.snake
                        }
                    ]
                },
                {
                    imgdiv: "option opt4",
                    imgclass: "relativecls img4",
                    imgid: 'elephantImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.elephant
                        }
                    ]
                },
            ]
        }],
    },
//    slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.question
            },
            {
                query:true,
                textdiv:"question",
                textclass: "content2 centertext",
                textdata: data.string.q3,
                ans:data.string.butterfly
            },

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "option opt1",
                    imgclass: "relativecls img1",
                    imgid: 'butterflyImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.butterfly
                        }
                    ]
                },
                {
                    imgdiv: "option opt2",
                    imgclass: "relativecls img2",
                    imgid: 'catImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.cat
                        }
                    ]
                },
                {
                    imgdiv: "option opt3",
                    imgclass: "relativecls img3",
                    imgid: 'elephantImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:" content2 images",
                            textdata:data.string.elephant
                        }
                    ]
                },
                {
                    imgdiv: "option opt4",
                    imgclass: "relativecls img4",
                    imgid: 'snakeImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.snake
                        }
                    ]
                },
            ]
        }],
    },
//    slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.question
            },
            {
                query:true,
                textdiv:"question",
                textclass: "content2 centertext",
                textdata: data.string.q4,
                ans:data.string.elephant
            },

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "option opt1",
                    imgclass: "relativecls img1",
                    imgid: 'elephantImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.elephant
                        }
                    ]
                },
                {
                    imgdiv: "option opt2",
                    imgclass: "relativecls img2",
                    imgid: 'catImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.cat
                        }
                    ]
                },
                {
                    imgdiv: "option opt3",
                    imgclass: "relativecls img3",
                    imgid: 'yakImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:" content2 images",
                            textdata:data.string.yak
                        }
                    ]
                },
                {
                    imgdiv: "option opt4",
                    imgclass: "relativecls img4",
                    imgid: 'snakeImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.snake
                        }
                    ]
                },
            ]
        }],
    },
    //    slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.question
            },
            {
                query:true,
                textdiv:"question",
                textclass: "content2 centertext",
                textdata: data.string.q5,
                ans:data.string.ostrich
            },

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "option opt1",
                    imgclass: "relativecls img1",
                    imgid: 'ostrichImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.ostrich
                        }
                    ]
                },
                {
                    imgdiv: "option opt2",
                    imgclass: "relativecls img2",
                    imgid: 'beeImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.bee
                        }
                    ]
                },
                {
                    imgdiv: "option opt3",
                    imgclass: "relativecls img3",
                    imgid: 'dogImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:" content2 images",
                            textdata:data.string.dog
                        }
                    ]
                },
                {
                    imgdiv: "option opt4",
                    imgclass: "relativecls img4",
                    imgid: 'catImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.cat
                        }
                    ]
                },
            ]
        }],
    },
    //    slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.question
            },
            {
                query:true,
                textdiv:"question",
                textclass: "content2 centertext",
                textdata: data.string.q6,
                ans:data.string.cat
            },

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "option opt1",
                    imgclass: "relativecls img1",
                    imgid: 'catImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.cat
                        }
                    ]
                },
                {
                    imgdiv: "option opt2",
                    imgclass: "relativecls img2",
                    imgid: 'snakeImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.snake
                        }
                    ]
                },
                {
                    imgdiv: "option opt3",
                    imgclass: "relativecls img3",
                    imgid: 'yakImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:" content2 images",
                            textdata:data.string.yak
                        }
                    ]
                },
                {
                    imgdiv: "option opt4",
                    imgclass: "relativecls img4",
                    imgid: 'ostrichImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.ostrich
                        }
                    ]
                },
            ]
        }],
    },
//    slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.question
            },
            {
                query:true,
                textdiv:"question",
                textclass: "content2 centertext",
                textdata: data.string.q7,
                ans:data.string.yak
            },

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "option opt1",
                    imgclass: "relativecls img1",
                    imgid: 'yakImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.yak
                        }
                    ]
                },
                {
                    imgdiv: "option opt2",
                    imgclass: "relativecls img2",
                    imgid: 'dogImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.dog
                        }
                    ]
                },
                {
                    imgdiv: "option opt3",
                    imgclass: "relativecls img3",
                    imgid: 'beeImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:" content2 images",
                            textdata:data.string.bee
                        }
                    ]
                },
                {
                    imgdiv: "option opt4",
                    imgclass: "relativecls img4",
                    imgid: 'snakeImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.snake
                        }
                    ]
                },
            ]
        }],
    },
    //    slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.question
            },
            {
                query:true,
                textdiv:"question",
                textclass: "content2 centertext",
                textdata: data.string.q8,
                ans:data.string.snake
            },

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "option opt1",
                    imgclass: "relativecls img1",
                    imgid: 'snakeImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.snake
                        }
                    ]
                },
                {
                    imgdiv: "option opt2",
                    imgclass: "relativecls img2",
                    imgid: 'yakImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.yak
                        }
                    ]
                },
                {
                    imgdiv: "option opt3",
                    imgclass: "relativecls img3",
                    imgid: 'butterflyImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:" content2 images",
                            textdata:data.string.butterfly
                        }
                    ]
                },
                {
                    imgdiv: "option opt4",
                    imgclass: "relativecls img4",
                    imgid: 'ostrichImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.ostrich
                        }
                    ]
                },
            ]
        }],
    },
//    slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.question
            },
            {
                query:true,
                textdiv:"question",
                textclass: "content2 centertext",
                textdata: data.string.q9,
                ans:data.string.dog
            },

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "option opt1",
                    imgclass: "relativecls img1",
                    imgid: 'dogImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.dog
                        }
                    ]
                },
                {
                    imgdiv: "option opt2",
                    imgclass: "relativecls img2",
                    imgid: 'butterflyImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.butterfly
                        }
                    ]
                },
                {
                    imgdiv: "option opt3",
                    imgclass: "relativecls img3",
                    imgid: 'beeImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:" content2 images",
                            textdata:data.string.bee
                        }
                    ]
                },
                {
                    imgdiv: "option opt4",
                    imgclass: "relativecls img4",
                    imgid: 'ostrichImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.ostrich
                        }
                    ]
                },
            ]
        }],
    },
    //    slide 9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.question
            },
            {
                query:true,
                textdiv:"question",
                textclass: "content2 centertext",
                textdata: data.string.q10,
                ans:data.string.bee
            },

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "option opt1",
                    imgclass: "relativecls img1",
                    imgid: 'beeImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.bee
                        }
                    ]
                },
                {
                    imgdiv: "option opt2",
                    imgclass: "relativecls img2",
                    imgid: 'yakImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.yak
                        }
                    ]
                },
                {
                    imgdiv: "option opt3",
                    imgclass: "relativecls img3",
                    imgid: 'snakeImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:" content2 images",
                            textdata:data.string.snake
                        }
                    ]
                },
                {
                    imgdiv: "option opt4",
                    imgclass: "relativecls img4",
                    imgid: 'ostrichImg',
                    imgsrc: "",
                    textblock:[
                        {
                            textclass:"content2 images",
                            textdata:data.string.ostrich
                        }
                    ]
                },
            ]
        }],
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var time;
    var $total_page = 10;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var scoreupdate = 1;
    var score = new EggTemplate();
    score.init($total_page);

    var solidarray = [];
    var liquidarray = [];
    var gasarray = [];
    var mainArray = [];



    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "fishImg", src: imgpath + "fish.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "antImg", src: imgpath + "ant.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "elephantImg", src: imgpath + "elephant.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "spiderImg", src: imgpath + "spider.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "butterflyImg", src: imgpath + "butterfly.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "snakeImg", src: imgpath + "snake.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "beeImg", src: imgpath + "bee.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "catImg", src: imgpath + "cat.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "yakImg", src: imgpath + "yak.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "ostrichImg", src: imgpath + "ostrich.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "dogImg", src: imgpath + "dog.jpg", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_1", src: soundAsset + "ex.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    // Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    // Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    // Handlebars.registerPartial("fractioncontent", $("#fractioncontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        score.numberOfQuestions();
        countNext==0?sound_player("sound_1"):"";
        put_image();
        shufflehint();
        checkans();
    }


    function sound_player(sound_id, navigate) {
        console.log(sound_id);
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                score.gotoNext();
                templateCaller();
                break;
        }
    });
    //

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });



    function shufflehint() {
        var optdiv = $(".coverboardfull");

        for (var i = optdiv.find(".option").length; i >= 0; i--) {
            optdiv.append(optdiv.find(".option").eq(Math.random() * i | 0));
        }
        var optionclass = ["option opt1","option opt2","option opt3","option opt4"]
        optdiv.find(".option").removeClass().addClass("current");
        optdiv.find(".current").each(function (index) {
            $(this).addClass(optionclass[index]);
        });
    }

        function navigationcontroller(islastpageflag) {
            if (countNext == 0 && $total_page != 1) {
                $nextBtn.show(0);
            }
            else if ($total_page == 1) {
                $nextBtn.css('display', 'none');

                ole.footerNotificationHandler.lessonEndSetNotification();
            }
            else if (countNext > 0 && countNext < $total_page) {

                $nextBtn.show(0);
            }
            else if (countNext == $total_page - 2) {

                $nextBtn.css('display', 'none');
                // if lastpageflag is true
                // ole.footerNotificationHandler.pageEndSetNotification();
            }

        }

    function put_image() {
        var contentCount=content[countNext];
        var imageblockcontent=contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent,contentCount,preload);
        imageblockcontent=contentCount.hasOwnProperty('imageblock1');
        contentCount = imageblockcontent?contentCount.imageblock1[0]:false;
        imageblockcontent?dynamicimageload(imageblockcontent,contentCount):'';
    }

    function dynamicimageload(imageblockcontent,contentCount){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }

  function checkans(){
      scoreupdate = 1;
        $(".option").click(function(){
            if($(this).find("p").text()==$(".question").attr("data-answer")){
                $(this).addClass("correctans");
                $(".option").addClass("avoid-clicks");
                current_sound.stop();
                play_correct_incorrect_sound(1);
                $(this).append("<img class='correctwrongimg' src='images/right.png'/>");
                navigationcontroller(countNext, $total_page);
                scoreupdate==1?score.update(true):'';
                navigationcontroller();
            }
            else{
                scoreupdate = 0;
                $(this).addClass("wrongans avoid-clicks");
                current_sound.stop();
                play_correct_incorrect_sound(0);
                $(this).append("<img class='correctwrongimg' src='images/wrong.png'/>");
                score.update(false);

            }
      });
  }

});

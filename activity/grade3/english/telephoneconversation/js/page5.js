var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";

//title audio
var sound_t_0 = new buzz.sound((soundAsset + "p5_s1.ogg"));
var sound_t_1 = new buzz.sound((soundAsset + "p5_s2.ogg"));

//page 1 audio
var sound_l_0 = new buzz.sound((soundAsset + "ring.ogg"));
var sound_l_1 = new buzz.sound((soundAsset + "p1_s3.ogg"));
var sound_l_2 = new buzz.sound((soundAsset + "p1_s4.ogg"));
var sound_l_3 = new buzz.sound((soundAsset + "p1_s5.ogg"));
var sound_l_4 = new buzz.sound((soundAsset + "p1_s6.ogg"));
var sound_l_5 = new buzz.sound((soundAsset + "p1_s7.ogg"));
var sound_l_6 = new buzz.sound((soundAsset + "p1_s8.ogg"));
var sound_l_7 = new buzz.sound((soundAsset + "p1_s9.ogg"));
var sound_l_8 = new buzz.sound((soundAsset + "p1_s10.ogg"));
var sound_l_9 = new buzz.sound((soundAsset + "p1_s11.ogg"));

//page 2 audio
var sound_l_20 = new buzz.sound((soundAsset + "ring.ogg"));
var sound_l_21 = new buzz.sound((soundAsset + "p2_s2.ogg"));
var sound_l_22 = new buzz.sound((soundAsset + "p2_s3.ogg"));
var sound_l_23 = new buzz.sound((soundAsset + "p2_s4.ogg"));
var sound_l_24 = new buzz.sound((soundAsset + "p2_s5.ogg"));
var sound_l_25 = new buzz.sound((soundAsset + "static.ogg"));
var sound_l_26 = new buzz.sound((soundAsset + "p2_s7.ogg"));
var sound_l_27 = new buzz.sound((soundAsset + "p2_s8.ogg"));
var sound_l_28 = new buzz.sound((soundAsset + "p2_s9.ogg"));
var sound_l_29 = new buzz.sound((soundAsset + "p2_s10.ogg"));
var sound_l_30 = new buzz.sound((soundAsset + "p2_s11.ogg"));

//page 3 audio
var sound_l_31 = new buzz.sound((soundAsset + "ring.ogg"));
var sound_l_32 = new buzz.sound((soundAsset + "p3_s2.ogg"));
var sound_l_33 = new buzz.sound((soundAsset + "p3_s3.ogg"));
var sound_l_34 = new buzz.sound((soundAsset + "p3_s4.ogg"));
var sound_l_35 = new buzz.sound((soundAsset + "p3_s5.ogg"));

//page 4 audio
var sound_l_41 = new buzz.sound((soundAsset + "ring.ogg"));
var sound_l_42 = new buzz.sound((soundAsset + "p4_s2.ogg"));
var sound_l_43 = new buzz.sound((soundAsset + "p4_s3.ogg"));
var sound_l_44 = new buzz.sound((soundAsset + "p4_s4.ogg"));
var sound_l_45 = new buzz.sound((soundAsset + "p4_s5.ogg"));
var sound_l_46 = new buzz.sound((soundAsset + "p4_s6.ogg"));
var sound_l_47 = new buzz.sound((soundAsset + "p4_s7.ogg"));
var sound_l_48 = new buzz.sound((soundAsset + "p4_s8.ogg"));
var sound_l_49 = new buzz.sound((soundAsset + "p4_s9.ogg"));
var sound_l_410 = new buzz.sound((soundAsset + "p4_s10.ogg"));
var sound_l_411 = new buzz.sound((soundAsset + "p4_s11.ogg"));
var sound_l_412 = new buzz.sound((soundAsset + "p4_s12.ogg"));

var sound_group_p0 =[sound_t_0];
var sound_group_p1 = [sound_l_0, sound_l_1, sound_l_2, sound_l_3, sound_l_4, sound_l_5, sound_l_6, sound_l_7, sound_l_8, sound_l_9];
var sound_group_p2 = [sound_l_21, sound_l_22, sound_l_23, sound_l_24, sound_l_25, sound_l_26, sound_l_27, sound_l_28, sound_l_29, sound_l_30];
var sound_group_p3 = [sound_l_32, sound_l_33, sound_l_34, sound_l_35];
var sound_group_p4 = [sound_l_42, sound_l_43, sound_l_44, sound_l_45, sound_l_46, sound_l_47, sound_l_48, sound_l_49, sound_l_410, sound_l_411, sound_l_412];

// var global_audio_var = sound_l_0;
var global_audio_var = sound_t_0;

var content=[
	{
		// slide0

		contentblockadditionalclass: "custombg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "front_utb_front",
		uppertextblocknocenteradjust: true,
		uppertextblock:[{
			textclass: "description1",
			textdata: data.string.p5_s0
		}]
	},
	{
		//slide 1
		contentnocenteradjust: true,
		contentblockadditionalclass: "customcb",
		imageblock:[
			{
				imagestoshow:[{
					imgclass: "image1",
					imgsrc: imgpath+"darshan.png"
				},{
					imgclass: "image2",
					imgsrc: imgpath+"goma.png"
				}]
			}
		],
		extratextblock : [{
			textdata : data.string.p5_s3,
			textclass : 'header_conv sniglet my_font_big'
		}],
		imagetextblock: [
			{
				imagediv: 'instruction_snail',
				imgclass: '',
				imgsrc: imgpath + "worm.gif",
				textclass : "sniglet my_font_medium",
				textdata : data.string.click_on,
			}
		],
		containsdialog:[
		{
			dialogcontainer: "dialogleft dialog",
			dialogimageclass1: "dialogimage1 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage1 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentleft",
			dialogcontent: data.string.p1_s2
		},{
			dialogcontainer: "dialogright dialog",
			dialogimageclass1: "dialogimage2 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage2 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentright",
			dialogcontent: data.string.p1_s3
		},{
			dialogcontainer: "dialogleft  dialog",
			dialogimageclass1: "dialogimage1 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage1 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentleft",
			dialogcontent: data.string.p1_s4
		},{
			dialogcontainer: "dialogright  dialog",
			dialogimageclass1: "dialogimage2 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage2 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentright",
			dialogcontent: data.string.p1_s5
		},{
			dialogcontainer: "dialogleft  dialog",
			dialogimageclass1: "dialogimage1 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage1 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentleft",
			dialogcontent: data.string.p1_s6
		},{
			dialogcontainer: "dialogright  dialog",
			dialogimageclass1: "dialogimage2 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage2 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentright",
			dialogcontent: data.string.p1_s7
		},{
			dialogcontainer: "dialogleft  dialog",
			dialogimageclass1: "dialogimage1 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage1 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentleft",
			dialogcontent: data.string.p1_s8
		},{
			dialogcontainer: "dialogright  dialog",
			dialogimageclass1: "dialogimage2 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage2 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentright",
			dialogcontent: data.string.p1_s9
		},{
			dialogcontainer: "dialogleft lastdialog  dialog",
			dialogimageclass1: "dialogimage1 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage1 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentleft",
			dialogcontent: data.string.p1_s10
		}]
	},
	{
		//slide 2
		contentnocenteradjust: true,
		contentblockadditionalclass: "customcb",
		imageblock:[
			{
				imagestoshow:[{
					imgclass: "image1",
					imgsrc: imgpath+"rasheed.png"
				},{
					imgclass: "image2",
					imgsrc: imgpath+"blue.png"
				}]
			}
		],
		extratextblock : [{
			textdata : data.string.p5_s4,
			textclass : 'header_conv sniglet my_font_big'
		}],
		imagetextblock: [
			{
				imagediv: 'instruction_snail',
				imgclass: '',
				imgsrc: imgpath + "worm.gif",
				textclass : "sniglet my_font_medium",
				textdata : data.string.click_on,
			}
		],
		containsdialog:[{
			dialogcontainer: "dialogleft  dialog",
			dialogimageclass1: "dialogimage1 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage1 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentleft",
			dialogcontent: data.string.p2_s1
		},{
			dialogcontainer: "dialogright  dialog",
			dialogimageclass1: "dialogimage2 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage2 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentright",
			dialogcontent: data.string.p2_s2
		},{
			dialogcontainer: "dialogleft  dialog",
			dialogimageclass1: "dialogimage1 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage1 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentleft",
			dialogcontent: data.string.p2_s3
		},{
			dialogcontainer: "dialogright  dialog",
			dialogimageclass1: "dialogimage2 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage2 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentright",
			dialogcontent: data.string.p2_s4
		},{
			dialogcontainer: "dialogcenter",
			dialogcontentclass: "dialoginfo",
			dialogcontent: data.string.p5_s2
		},{
			dialogcontainer: "dialogright  dialog",
			dialogimageclass1: "dialogimage2 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage2 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentright",
			dialogcontent: data.string.p2_s5
		},{
			dialogcontainer: "dialogleft  dialog",
			dialogimageclass1: "dialogimage1 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage1 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentleft",
			dialogcontent: data.string.p2_s6
		},{
			dialogcontainer: "dialogright lastdialog  dialog",
			dialogimageclass1: "dialogimage2 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage2 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentright",
			dialogcontent: data.string.p2_s7
		},{
			dialogcontainer: "dialogleft  dialog",
			dialogimageclass1: "dialogimage1 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage1 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentleft",
			dialogcontent: data.string.p2_s8
		},{
			dialogcontainer: "dialogright lastdialog  dialog",
			dialogimageclass1: "dialogimage2 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage2 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentright",
			dialogcontent: data.string.p2_s9
		}]
	},
	{
		//slide 3
		contentnocenteradjust: true,
		contentblockadditionalclass: "customcb",
		imageblock:[
			{
				imagestoshow:[{
					imgclass: "image1",
					imgsrc: imgpath+"green.png"
				},{
					imgclass: "image2  ",
					imgsrc: imgpath+"red.png"
				}]
			}
		],
		extratextblock : [{
			textdata : data.string.p5_s5,
			textclass : 'header_conv sniglet my_font_big'
		}],
		imagetextblock: [
			{
				imagediv: 'instruction_snail',
				imgclass: '',
				imgsrc: imgpath + "worm.gif",
				textclass : "sniglet my_font_medium",
				textdata : data.string.click_on,
			}
		],
		containsdialog:[{
			dialogcontainer: "dialogleft  dialog",
			dialogimageclass1: "dialogimage1 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage1 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentleft",
			dialogcontent: data.string.p3_s1
		},{
			dialogcontainer: "dialogright  dialog",
			dialogimageclass1: "dialogimage2 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage2 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentright",
			dialogcontent: data.string.p3_s2
		},{
			dialogcontainer: "dialogleft  dialog",
			dialogimageclass1: "dialogimage1 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage1 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentleft",
			dialogcontent: data.string.p3_s3
		},{
			dialogcontainer: "dialogright lastdialog  dialog",
			dialogimageclass1: "dialogimage2 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage2 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentright",
			dialogcontent: data.string.p3_s4
		}]
	},
	{
		//slide 4
		contentnocenteradjust: true,
		contentblockadditionalclass: "customcb",
		imageblock:[
			{
				imagestoshow:[{
					imgclass: "image1",
					imgsrc: imgpath+"asmita.png"
				},{
					imgclass: "image2  ",
					imgsrc: imgpath+"priyata.png"
				}]
			}
		],
		extratextblock : [{
			textdata : data.string.p5_s6,
			textclass : 'header_conv sniglet my_font_big'
		}],
		imagetextblock: [
			{
				imagediv: 'instruction_snail',
				imgclass: '',
				imgsrc: imgpath + "worm.gif",
				textclass : "sniglet my_font_medium",
				textdata : data.string.click_on,
			}
		],
		containsdialog:[{
			dialogcontainer: "dialogleft  dialog",
			dialogimageclass1: "dialogimage1 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage1 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentleft",
			dialogcontent: data.string.p4_s1
		},{
			dialogcontainer: "dialogright  dialog",
			dialogimageclass1: "dialogimage2 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage2 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentright",
			dialogcontent: data.string.p4_s2
		},{
			dialogcontainer: "dialogleft  dialog",
			dialogimageclass1: "dialogimage1 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage1 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentleft",
			dialogcontent: data.string.p4_s3
		},{
			dialogcontainer: "dialogright  dialog",
			dialogimageclass1: "dialogimage2 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage2 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentright",
			dialogcontent: data.string.p4_s4
		},{
			dialogcontainer: "dialogleft  dialog",
			dialogimageclass1: "dialogimage1 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage1 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentleft",
			dialogcontent: data.string.p4_s5
		},{
			dialogcontainer: "dialogright  dialog",
			dialogimageclass1: "dialogimage2 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage2 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentright",
			dialogcontent: data.string.p4_s6
		},{
			dialogcontainer: "dialogleft  dialog",
			dialogimageclass1: "dialogimage1 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage1 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentleft",
			dialogcontent: data.string.p4_s7
		},{
			dialogcontainer: "dialogright  dialog",
			dialogimageclass1: "dialogimage2 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage2 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentright",
			dialogcontent: data.string.p4_s8
		},{
			dialogcontainer: "dialogleft  dialog",
			dialogimageclass1: "dialogimage1 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage1 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentleft",
			dialogcontent: data.string.p4_s9
		},{
			dialogcontainer: "dialogright  dialog",
			dialogimageclass1: "dialogimage2 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage2 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentright",
			dialogcontent: data.string.p4_s10
		},{
			dialogcontainer: "dialogleft lastdialog  dialog",
			dialogimageclass1: "dialogimage1 disabled",
			dialogimagesrc1: imgpath + "dialog01.png",
			dialogimageclass2: "dialogimage1 active_conversation",
			dialogimagesrc2: imgpath + "dialog02.png",
			dialogcontentclass: "dialogcontentleft",
			dialogcontent: data.string.p4_s11
		}]
	}
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

   var vocabcontroller =  new Vocabulary();
  vocabcontroller.init();

  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    vocabcontroller.findwords(countNext);

		// splitintofractions($(".fractionblock"));
		if(countNext > 0){
			var $containsDialog;
			$prevBtn.hide(0);
			if((countNext+1) == content.length){
				ole.footerNotificationHandler.hideNotification();
			}else{
				$nextBtn.hide(0);
			}
		}
		    switch(countNext){
					case 0:
					global_audio_var.stop();
					global_audio_var = sound_t_0;
					global_audio_var.play();
					break;
		    	case 1:
					global_audio_var = sound_t_1;
					global_audio_var.play();

					 		$(".containsDialog > div").each(function(index){
		    			$(this).click(function(){
		    				playaudio_new(sound_group_p1[index+1], $(".containsDialog > div").eq(index));
		    			});
		    		});
						setTimeout(function(){
							$nextBtn.show(0);
						$prevBtn.show(0);
					},4000);

		    		break;
	    		case 2:
					global_audio_var.stop();
	    			// continuousplaying = true;
	    			// $containsDialog = $(".containsDialog > div");
		    		// console.log(" div children dialog count ", $containsDialog.length);
	    			// playaudio(sound_group_p2, $containsDialog, 0);
	    			$(".containsDialog > div").each(function(index){
		    			$(this).click(function(){
		    				playaudio_new(sound_group_p2[index], $(".containsDialog > div").eq(index));
		    			});
		    		});
		    		$nextBtn.show(0);
					$prevBtn.show(0);
		    		break;
	    		case 3:
						global_audio_var.stop();
	    			$(".containsDialog > div").each(function(index){
		    			$(this).click(function(){
		    				playaudio_new(sound_group_p3[index], $(".containsDialog > div").eq(index));
		    			});
		    		});
		    		$nextBtn.show(0);
					$prevBtn.show(0);
		    		break;
		    	case 4:
						global_audio_var.stop();
	    			$(".containsDialog > div").each(function(index){
		    			$(this).click(function(){
		    				playaudio_new(sound_group_p4[index], $(".containsDialog > div").eq(index));
		    			});
		    		});
					ole.footerNotificationHandler.pageEndSetNotification();
					$prevBtn.show(0);
		    		break;
		   		default:
		   			break;
		    }


  }

/*=====  End of Templates Block  ======*/
var continuousplaying = true;
var isplaying = false;

	function playaudio_new(sound_data, $dialog_container){
		global_audio_var.stop();
		$(".containsDialog > div").children(".active_conversation").hide(0);
		$dialog_container.children(".active_conversation").show(0);

		global_audio_var = sound_data;
		global_audio_var.play();

		global_audio_var.bind('ended', function(){
			$dialog_container.children(".active_conversation").hide(0);
		});
	}

	function playaudio(sound_data, $dialog_container, index){
		var $dialog = $($dialog_container[index]);
		$($dialog[0].children[1]).show(0);
		$dialog.data("isplaying", true);
		$dialog.data("index", index);
		$dialog.children("active_conversation").show(0);
		$($dialog_container[index]).click(function(){
			if(!isplaying){
				playaudio(sound_data, $dialog_container, $(this).data("index"));
			}
			return false;
		});
		isplaying = true;
		sound_data[index].play();
		sound_data[index].bind('ended', function(){
			setTimeout(function(){
				$dialog.data("isplaying", false);
				$($dialog[0].children[1]).hide(0);
				sound_data[index].unbind('ended');
				index++;
				isplaying = false;
				if(index>=$dialog_container.length || !continuousplaying){
					continuousplaying = false;
					if((countNext+1) == content.length){
						ole.footerNotificationHandler.pageEndSetNotification();
					}else{
						$nextBtn.show(0);
					}
				}else{
					playaudio(sound_data, $dialog_container, index);
				}
			}, 1000);
		});
	}

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    loadTimelineProgress($total_page,countNext+1);
    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


    //call the slide indication bar handler for pink indicators

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

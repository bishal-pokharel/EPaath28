var imgpath = $ref+"/images/exerciseImage/";
var soundAsset = $ref+"/sounds/";

var sound_1 = new buzz.sound((soundAsset + "ex.ogg"));
// Array.prototype.shufflearray = function() {
//     var i = this.length,
//         j, temp;
//     while (--i > 0) {
//         j = Math.floor(Math.random() * (i + 1));
//         temp = this[j];
//         this[j] = this[i];
//         this[i] = temp;
//     }
//     return this;
// };

var content=[
	{
		//slide 0	
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
            datahighlightflag: true,
            datahighlightcustomclass: "ansSection",
            ans:"table",
            textdata: data.string.ques1,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "table.png",
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "boy01.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "cat.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "dog.png",
			}
			]
		}
		]
	},
	{
		//slide 0	
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
            datahighlightflag: true,
            datahighlightcustomclass: "ansSection",
            ans:"monkey",
			textdata: data.string.ques2,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "monkey.png",
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "house.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "pig.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "book.png",
			},
			]
		}
		]
	},
	{
		//slide 0	
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
            datahighlightflag: true,
            datahighlightcustomclass: "ansSection",
            ans:"tree",
			textdata: data.string.ques3,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "tree.png",
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "monkey.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "cow.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "dog.png",
			},
			]
		}
		]
	},
	{
		//slide 0	
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
            datahighlightflag: true,
            datahighlightcustomclass: "ansSection",
            ans:"bird",
			textdata: data.string.ques4,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "bird.png",
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "girl.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "elephant.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "truck.png",
			},
			]
		}
		]
	},
	{
		//slide 0	
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
            datahighlightflag: true,
            datahighlightcustomclass: "ansSection",
            ans:"cow",
			textdata: data.string.ques5,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "cow.png",
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "parrot.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "crow.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "eagle.png",
			},
			]
		}
		]
	},
	{
		//slide 0	
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
            datahighlightflag: true,
            datahighlightcustomclass: "ansSection",
            ans:"girl",
			textdata: data.string.ques6,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "girl.png",
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "cat.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "dog.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "tree.png",
			},
			]
		}
		]
	},
	{
		//slide 0	
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
            datahighlightflag: true,
            datahighlightcustomclass: "ansSection",
            ans:"house",
			textdata: data.string.ques7,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "house.png",
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "monkey.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "parrot.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "boy.png",
			},
			]
		}
		]
	},
	{
		//slide 0	
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
            datahighlightflag: true,
            datahighlightcustomclass: "ansSection",
            ans:"fish",
			textdata: data.string.ques8,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "fish.png",
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "pig.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "hen.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "goat.png",
			},
			]
		}
		]
	},
	{
		//slide 0	
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
            datahighlightflag: true,
            datahighlightcustomclass: "ansSection",
            ans:"notebook",
			textdata: data.string.ques9,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "notebook.png",
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "cow.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "sheep.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "duck.png",
			},
			]
		}
		]
	},
	{
		//slide 0	
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
            datahighlightflag: true,
            datahighlightcustomclass: "ansSection",
            ans:"boy",
			textdata: data.string.ques10,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "boy02.png",
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "hen.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "tree.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "tiger.png",
			},
			]
		}
		]
	},
];

/*remove this for non random questions*/
// content.shufflearray();

$(function (){	
	var $board    = $('.board');
	var $nextBtn  = $('#activity-page-next-btn-enabled');
	var $prevBtn  = $('#activity-page-prev-btn-enabled');
	var countNext = 0;

	var $total_page = content.length;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ? 
		islastpageflag = false : 
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}
	
	 /*values in this array is same as the name of images of eggs in image folder*/

	 //create eggs
	 var testin = new EggTemplate();
   
	 	//eggTemplate.eggMove(countNext);
 		testin.init(10);
    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";
                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);
	 	var testcount = 0;
	 	
	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);

	 	/*generate question no at the beginning of question*/
	 	testin.numberOfQuestions();
         texthighlight($board);

	 	/*for randomizing the options*/
	 	var parent = $(".optionsdiv");
	 	var divs = parent.children();
	 	while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
	 	}

        countNext==0?sound_1.play():"";
	 	/*======= SCOREBOARD SECTION ==============*/
	 	/*random scoreboard eggs*/
	 	//var i = Math.floor(Math.random() * imageArray.length);
	 	//var randImg = imageArray[i];
	 	var ansClicked = false;
	 	var wrngClicked = false;

	 	$(".correctOne").click(function(){
	 		$(".ansSection").text($(".question").attr("data-answer"));
			correct_btn(this);
	 		testin.update(true);
			play_correct_incorrect_sound(1);
	 		$nextBtn.show(0);
	 	});

	 	$(".incorrectOne").click(function(){
			incorrect_btn(this);
	 		testin.update(false);
			play_correct_incorrect_sound(0);
	 	});

	 	$(".incorrectTwo").click(function(){
			incorrect_btn(this);
	 		testin.update(false);
			play_correct_incorrect_sound(0);
	 	});

	 	function correct_btn(current_btn){
            $(current_btn).append("<img class='correctwrongImg' src='images/correct.png'>");
            $('.options_sign').addClass('disabled');
	 		$('.hidden_sign').html($(current_btn).html());
	 		$('.hidden_sign').addClass('fade_in');
	 		$('.optionscontainer').removeClass('forHover');
	 		$(current_btn).addClass('option_true');
	 	}

	 	function incorrect_btn(current_btn){
            $(current_btn).append("<img class='correctwrongImg' src='images/wrong.png'>");
            $(current_btn).addClass('disabled');
	 		$(current_btn).addClass('option_false');
	 	}

	 	/*======= SCOREBOARD SECTION ==============*/
	 }

	 function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');		

		// call navigation controller
		navigationcontroller();	

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   

		//call the slide indication bar handler for pink indicators
		

	}

	// first call to template caller
	templateCaller();	

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
		testin.gotoNext();
		
	});


	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
		previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});
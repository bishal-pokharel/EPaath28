var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg",
		extratextblock:[
		{
			textclass: "startnote",
			textdata: data.string.p2s0
		}
	]
},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg",
		extratextblock:[
		{
			textclass: "heading",
			textdata: data.string.p1s0
		},
		{
			textclass: "tabs4 a",
			textdata: data.string.p1a
		},
		{
			textclass: "tabs4 e",
			textdata: data.string.p1e
		},
		{
			textclass: "tabs4 i4",
			textdata: data.string.p1i
		},
		{
			textclass: "tabs4 o4",
			textdata: data.string.p1o
		},
		{
			textclass: "tabs4 u",
			textdata: data.string.p1u
		},
		{
			textclass: "cloudtext s11",
			textdata: data.string.p1s3txt1
		},
		{
			textclass: "ylb2 s1txt2",
			textdata: data.string.p2s1txt2
		}
		
		
	],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "clds1",
			imgid : 'cloud',
			imgsrc: ""
		},
		{
			imgclass:"speaker btmspkr",
			imgid:'speaker',
			imgsrc:" "
		}
	]
}]
},
//slide2
{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg",
		extratextblock:[
		{
			textclass: "heading",
			textdata: data.string.p1s0
		},
		{
			textclass: "tabs5 a5",
			textdata: data.string.p1a
		},
		{
			textclass: "tabs5 e5",
			textdata: data.string.p1e
		},
		{
			textclass: "tabs5 i5",
			textdata: data.string.p1i
		},
		{
			textclass: "tabs5 o5",
			textdata: data.string.p1o
		},
		{
			textclass: "tabs5 u5",
			textdata: data.string.p1u
		},
		{
			textclass: "p1s6txt",
			textdata: data.string.p2s2txt
		},
		{
			textclass: "tabbtm fstanm c",
			textdata: data.string.p2c
		},
		{
			textclass: "tabbtm fstanm o1",
			textdata: data.string.p1o
		},
		{
			textclass: "tabbtm fstanm i1",
			textdata: data.string.p1i
		},
		{
			textclass: "tabbtm fstanm n",
			textdata: data.string.p2n
		},
		{
			textclass: "tabbtm secanm b",
			textdata: data.string.p2b
		},
		{
			textclass: "tabbtm secanm o2",
			textdata: data.string.p1o
		},
		{
			textclass: "tabbtm secanm i2",
			textdata: data.string.p1i
		},
		{
			textclass: "tabbtm secanm l1",
			textdata: data.string.p2l
		},
		{
			textclass: "tabbtm thrdanm o3",
			textdata: data.string.p1O
		},
		{
			textclass: "tabbtm thrdanm i3",
			textdata: data.string.p1i
		},
		{
			textclass: "tabbtm thrdanm l2",
			textdata: data.string.p1l
		}
		
	],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "cloud5",
					imgid : 'cloud',
					imgsrc: ""
				},
				{
					imgclass: "fstanm coin",
					imgid : 'coin',
					imgsrc: ""
				},
				{
					imgclass: "secanm boil",
					imgid : 'boil',
					imgsrc: ""
				},
				{
					imgclass: "thrdanm oil",
					imgid : 'oil',
					imgsrc: ""
				},
				{
					imgclass: "speaker topspkr",
					imgid : 'speaker',
					imgsrc: ""
				},
				{
					imgclass: "speaker thrdanm spkr1",
					imgid : 'speaker',
					imgsrc: ""
				},
				{
					imgclass: "speaker fstanm spkr2",
					imgid : 'speaker',
					imgsrc: ""
				},
				{
					imgclass: "speaker secanm spkr3",
					imgid : 'speaker',
					imgsrc: ""
				},
				{
					imgclass: "box thrdanm box3",
					imgid : 'box',
					imgsrc: ""
				},
				{
					imgclass: "box fstanm box1",
					imgid : 'box',
					imgsrc: ""
				},
				{
					imgclass: "box secanm box2",
					imgid : 'box',
					imgsrc: ""
				}
			]
		}]


},
//slide4
{
			contentnocenteradjust: true,
		contentblockadditionalclass: "bg2",
		extratextblock:[
		{
			textclass: "heading",
			textdata: data.string.p1s0
		},
		{
			textclass: "tabs4 a",
			textdata: data.string.p1a
		},
		{
			textclass: "tabs4 e",
			textdata: data.string.p1e
		},
		{
			textclass: "tabs4 i",
			textdata: data.string.p1i
		},
		{
			textclass: "tabs4 o4",
			textdata: data.string.p1o
		},
		{
			textclass: "tabs4 u4",
			textdata: data.string.p1u
		},
		{
			textclass: "cloudtext s11",
			textdata: data.string.p1s3txt1
		},
		{
			textclass: "ylb2 s1txt2",
			textdata: data.string.p2s4txt2
		}
		
		
	],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "clds1",
			imgid : 'cloud',
			imgsrc: ""
		},
		{
			imgclass:"speaker btmspkr",
			imgid:'speaker',
			imgsrc:" "
		}
	]
}]
},
//slide5
{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg2",
		extratextblock:[
		{
			textclass: "heading",
			textdata: data.string.p1s0
		},
		{
			textclass: "tabs5 a5",
			textdata: data.string.p1a
		},
		{
			textclass: "tabs5 e5",
			textdata: data.string.p1e
		},
		{
			textclass: "tabs5 i55",
			textdata: data.string.p1i
		},
		{
			textclass: "tabs5 o55l",
			textdata: data.string.p1o
		},
		{
			textclass: "tabs5 u55",
			textdata: data.string.p1u
		},
		{
			textclass: "p1s6txt",
			textdata: data.string.p2s7txt
		},
		{
			textclass: "tabbtm fstanm h",
			textdata: data.string.p1h
		},
		{
			textclass: "tabbtm fstanm o1l",
			textdata: data.string.p1o
		},
		{
			textclass: "tabbtm fstanm u2l",
			textdata: data.string.p1u
		},
		{
			textclass: "tabbtm fstanm s",
			textdata: data.string.p2s
		},
		{
			textclass: "tabbtm fstanm el",
			textdata: data.string.p2e
		},
		{
			textclass: "tabbtm secanm cl",
			textdata: data.string.p2c
		},
		{
			textclass: "tabbtm secanm l1l",
			textdata: data.string.p2l
		},
		{
			textclass: "tabbtm secanm o2l",
			textdata: data.string.p1o
		},
		{
			textclass: "tabbtm secanm u1l",
			textdata: data.string.p1u
		},
		{
			textclass: "tabbtm secanm d1l",
			textdata: data.string.p2d
		},
		{
			textclass: "tabbtm thrdanm rl",
			textdata: data.string.p1r
		},
		{
			textclass: "tabbtm thrdanm o3l",
			textdata: data.string.p1o
		},
		{
			textclass: "tabbtm thrdanm u3l",
			textdata: data.string.p1u
		},
		{
			textclass: "tabbtm thrdanm n2l",
			textdata: data.string.p2n
		},
		{
			textclass: "tabbtm thrdanm dl",
			textdata: data.string.p2d
		}
		
	],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "cloud5",
					imgid : 'cloud',
					imgsrc: ""
				},
				{
					imgclass: "house",
					imgid : 'house',
					imgsrc: ""
				},
				{
					imgclass: "cloud",
					imgid : 'cloud01',
					imgsrc: ""
				},
				{
					imgclass: "round",
					imgid : 'round',
					imgsrc: ""
				},
				{
					imgclass: "speaker topspkr",
					imgid : 'speaker',
					imgsrc: ""
				},
				{
					imgclass: "speaker thrdanm spkr1",
					imgid : 'speaker',
					imgsrc: ""
				},
				{
					imgclass: "speaker fstanm spkr2",
					imgid : 'speaker',
					imgsrc: ""
				},
				{
					imgclass: "speaker secanm spkr3",
					imgid : 'speaker',
					imgsrc: ""
				},
				{
					imgclass: "box thrdanm box3",
					imgid : 'box',
					imgsrc: ""
				},
				{
					imgclass: "box fstanm box1",
					imgid : 'box',
					imgsrc: ""
				},
				{
					imgclass: "box secanm box2",
					imgid : 'box',
					imgsrc: ""
				}
			]
		}]


}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "cloud", src: imgpath+"cloud.png", type: createjs.AbstractLoader.IMAGE},
			{id: "coin", src: imgpath+"f5a82c8b68aa5a4ac196ee5fa00c1db2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boil", src: imgpath+"boilwater.png", type: createjs.AbstractLoader.IMAGE},
			{id: "oil", src: imgpath+"sunflower-oil-250x250.png", type: createjs.AbstractLoader.IMAGE},
			{id: "round", src: imgpath+"Circle-PNG-Image.png", type: createjs.AbstractLoader.IMAGE},
			{id: "house", src: imgpath+"house_PNG63.png", type: createjs.AbstractLoader.IMAGE},
			{id: "speaker", src: imgpath+"speaker.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box", src: imgpath+"box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud01", src: imgpath+"cloud01.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"p2_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p2_s1.ogg"},
			{id: "sound_3", src: soundAsset+"p2_s2.ogg"},
			{id: "sound_4", src: soundAsset+"p2_s3.ogg"},
			{id: "coins", src: soundAsset+"coin.ogg"},
			{id: "boils", src: soundAsset+"boil.ogg"},
			{id: "oils", src: soundAsset+"oil.ogg"},
			{id: "sound_5", src: soundAsset+"p2_s4.ogg"},
			{id: "houses", src: soundAsset+"house.ogg"},
			{id: "clouds", src: soundAsset+"cloud.ogg"},
			{id: "rounds", src: soundAsset+"round.ogg"},
			
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);
		var currentSound;
		switch(countNext){
			case 0:
			sound_player("sound_1");
			break;
			case 1:
			sound_player("sound_2");
			currentmainsound="sound_2";
			break;
			case 2:
            setTimeout(function(){
            sound_player1("sound_3");
            currentmainsound="sound_3";	
            },5000);
            setTimeout(function () {
            navigationcontroller();
            $('.speaker').addClass("enable");
               },16000);
			currentSound = "coins";
        	currentSound1 = "boils";
        	currentSound2 = "oils";
        	lateSound();
        	lateSound1();
        	lateSound2();
			break;
			case 3:
        	sound_player("sound_4");
			currentmainsound="sound_4";
			break;
			case 4:
            setTimeout(function(){
            	sound_player1("sound_5");
            },5000);
            currentmainsound="sound_5";
	        setTimeout(function () {
            navigationcontroller();
            $('.speaker').addClass("enable");
               },16000);
			currentSound = "houses";
        	currentSound1 = "clouds";
        	currentSound2 = "rounds";
        	lateSound();
        	lateSound1();
        	lateSound2();	
			break;
		}
//click and play start
	$('.speaker').click(function(){
		if(($(this).hasClass("enable"))&&($(this).hasClass("topspkr")||$(this).hasClass("btmspkr")))
		{
				sound_player(currentmainsound);
		}	
		if(($(this).hasClass("enable"))&&($(this).hasClass("spkr1")))
		{
				sound_player(currentSound2);
		}	
		else if(($(this).hasClass("enable"))&&($(this).hasClass("spkr2")))
		{
				sound_player(currentSound);
		}
		else if(($(this).hasClass("enable"))&&($(this).hasClass("spkr3")))
		{
				sound_player(currentSound1);
		}
		else
		$(this).hasClass("disable");
	});

//click and play end
     function lateSound(){
	    setTimeout(function()
			{
				sound_player1(currentSound)},11000)
		}
	function lateSound1(){
	    setTimeout(function()
			{
				sound_player1(currentSound1)},13000)
		}
    function lateSound2(){
	    setTimeout(function()
			{
				sound_player1(currentSound2)},15000)
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			$('.speaker').addClass("enable");
			if(next == null)
			navigationcontroller();
		});
	}
	function sound_player1(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();

	}

	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2);
		});

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

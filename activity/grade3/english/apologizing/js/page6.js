var imgpath = $ref + "/images/";

var instrn = new buzz.sound($ref+"/sounds/s6_instrn.ogg")
Array.prototype.shufflearray = function() {
    var i = this.length,
        j, temp;
    while (--i > 0) {
        j = Math.floor(Math.random() * (i + 1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
    return this;
};

var listcontent = [{
		id: "1",
		data_content: data.string.p6_s3
	},{
		id: "2",
		data_content: data.string.p6_s4
	},{
		id: "3",
		data_content: data.string.p6_s5
	},{
		id: "4",
		data_content: data.string.p6_s6
	}];

listcontent.shufflearray();

var content = [{
		whole_content: "whole_content circular-gradient",
		title: "fronttitle",
		title_text: data.string.p6_s1,
    contentnocenteradjust: true,
    imageblock:[
      {
        imagestoshow:[{
          imgclass: "mainbgimage",
          imgsrc: imgpath+"pastedImagebg2.png"
        }]
      }]
	},
	{
		whole_content: "whole_content2",
		title : "title",
		title_text : data.string.p6_s2,
		listcontent: listcontent
}];


$(function() {
	var answer123 = [];
	var countNext = 0;
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var answered = false;
	var $board = $(".board");
	var totalpages = content.length;
	answer123 = [data.string.p6_s3, data.string.p6_s4, data.string.p6_s5, data.string.p6_s6];

	var $total_page = content.length;
  	loadTimelineProgress($total_page,countNext+1);
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


	function generalTemplate() {
		answered = false;
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		var pos = 0;
		loadTimelineProgress($total_page,countNext+1);
		switch(countNext){
			case 0:
				$prevBtn.hide(0);
				$nextBtn.show(0);
        play_diy_audio();
				break;
			case 1:
        instrn.play();
				$prevBtn.show(0);
				$nextBtn.hide(0);
				$("#sortable").sortable({
					stop : function(event, ui) {
						//console.log(ui.item);
						//console.log(ui.item.attr('id'));
						pos = (ui.item.index());
						useranswer = $.trim(ui.item.text());
						if (answer123 == useranswer) {
						} else {
							//alert("Incorrect");
						}
					}
				});
				$("#sortable").disableSelection();

				$("#submit").click(function(){
					if (answered) {
						return answered;
					}
					var answer_submit = [];
					$(".submit_answer").each(function(abcd, dsa) {
						answer_submit.push($(dsa).attr('data'));
					});

					console.log(answer123);
					console.log(answer_submit);
					var is_same = (answer123.length == answer_submit.length) && answer123.every(function(element, index) {
						return element === answer_submit[index];
					});
					console.log(is_same);
					if (is_same == true) {
						play_correct_incorrect_sound(true);
						$("#incorrect_icon").hide(0);
						$("#correct_icon").show(0);
						$("#sortable").sortable("disable");
            $(this).removeClass('incorrect');
            $(this).addClass('corrects');
            $(".button").css("pointer-events","none");
						answered = true;
						if (countNext == (content.length - 1)) {
							ole.footerNotificationHandler.pageEndSetNotification();
						} else {
							$nextBtn.show(0);
						}

					} else {
						play_correct_incorrect_sound(false);
						$("#incorrect_icon").show(0);
          	$(this).addClass('incorrect');
					}
				});
				break;
			default:
				break;
		}
	}

	generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	$nextBtn.click(function() {
		countNext++;
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	});

  	$refreshBtn.click(function(){
  		templateCaller();
  	});


	$prevBtn.click(function() {
		countNext--;
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
});

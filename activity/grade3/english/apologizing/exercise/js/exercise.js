var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";

var exercise = new buzz.sound((soundAsset + "exercise.ogg"));

Array.prototype.shufflearray = function() {
    var i = this.length,
        j, temp;
    while (--i > 0) {
        j = Math.floor(Math.random() * (i + 1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
    return this;
};

var content=[
	{
	//slide 0
		uppertextblock : [

			{
				textclass : "upper",
				textdata : data.string.top

			}


		],
		exerciseblock: [
		{

			imageoptions: [

			{
				forshuffle: "options_sign incorrectOne",
				labeltext: data.string.q1opt1,
				wrongoption :[{
					wrongs : "wrongic1",
				}]

			},
			{
				forshuffle: "options_sign incorrectTwo",

				labeltext: data.string.q1opt2,
				wrongoption :[{
					wrongs : "wrongic2",
				}]
			},
			{
				forshuffle: "options_sign incorrectThree",

				labeltext: data.string.q1opt3,
				wrongoption :[{
					wrongs : "wrongic3",
				}]

			},
			{
				forshuffle: "options_sign correctOne",

				labeltext: data.string.q1opt4,
				correctoption :[{}]

			},
			],

			textwithblank:[{
				textdata : data.string.ques1,
				dashedLine: false,
				imgsrc: imgpath + "q01.jpg"
			}]


		}
		]
	},
	//slide 1
	{
		uppertextblock : [

			{
				textclass : "upper",
				textdata : data.string.top
			}


		],
		exerciseblock: [
		{
			imageoptions: [
			{
				forshuffle: "options_sign incorrectOne",

				labeltext: data.string.q2opt1,
				wrongoption :[{
					wrongs : "wrongic1",
				}]

			},
			{
				forshuffle: "options_sign incorrectTwo",

				labeltext: data.string.q2opt2,
				wrongoption :[{
					wrongs : "wrongic2",

				}]
			},
			{
				forshuffle: "options_sign incorrectThree",

				labeltext: data.string.q2opt3,
				wrongoption :[{
					wrongs : "wrongic3",
				}]

			},
			{
				forshuffle: "options_sign correctOne",

				labeltext: data.string.q2opt4,
				correctoption :[{}]

			},
			],
			textwithblank:[{
				textdata : data.string.ques2,
				dashedLine: false,
				imgsrc: imgpath + "2.jpg"
			}]


		}
		]
	},
	//slide 2
	{
		uppertextblock : [

			{
				textclass : "upper",
				textdata : data.string.top
			}


		],
		exerciseblock: [
		{
			imageoptions: [
			{
				forshuffle: "options_sign incorrectOne",

				labeltext: data.string.q3opt1,
				wrongoption :[{
					wrongs : "wrongic1",
				}]

			},
			{
				forshuffle: "options_sign incorrectTwo",

				labeltext: data.string.q3opt2,
				wrongoption :[{
					wrongs : "wrongic2",
				}]
			},
			{
				forshuffle: "options_sign incorrectThree",

				labeltext: data.string.q3opt3,
				wrongoption :[{
					wrongs : "wrongic3",
				}]

			},
			{
				forshuffle: "options_sign correctOne",

				labeltext: data.string.q3opt4,
				correctoption :[{}]

			},
			],
			textwithblank:[{
				textdata : data.string.ques3,
				remain : data.string.remain3,
				dashedLine: true,
				imgsrc: imgpath + "3.jpg"
			}]


		}
		]
	},
	//slide 3
	{
		uppertextblock : [

			{
				textclass : "upper",
				textdata : data.string.top
			}


		],
		exerciseblock: [
		{
			imageoptions: [
			{
				forshuffle: "options_sign incorrectOne",

				labeltext: data.string.q4opt1,
				wrongoption :[{
					wrongs : "wrongic1",
				}]

			},
			{
				forshuffle: "options_sign incorrectTwo",

				labeltext: data.string.q4opt2,
				wrongoption :[{
					wrongs : "wrongic2",
				}]
			},
			{
				forshuffle: "options_sign incorrectThree",

				labeltext: data.string.q4opt3,
				wrongoption :[{
					wrongs : "wrongic3",
				}]

			},
			{
				forshuffle: "options_sign correctOne",

				labeltext: data.string.q4opt4,
				correctoption :[{}]

			},
			],
			textwithblank:[{
				textdata : data.string.ques4,
				remain : data.string.remain4,
				dashedLine: true,
				imgsrc: imgpath + "4.jpg"
			}]


		}
		]
	},

	//slide 4
		{
		uppertextblock : [

			{
				textclass : "upper",
				textdata : data.string.top
			}


		],
		exerciseblock: [
		{
			imageoptions: [
			{
				forshuffle: "options_sign incorrectOne",

				labeltext: data.string.q5opt1,
				wrongoption :[{
					wrongs : "wrongic1",
				}]

			},
			{
				forshuffle: "options_sign incorrectTwo",
				labeltext: data.string.q5opt2,
				wrongoption :[{
					wrongs : "wrongic2",
				}]
			},
			{
				forshuffle: "options_sign incorrectThree",
				labeltext: data.string.q5opt3,
				wrongoption :[{
					wrongs : "wrongic3",
				}]

			},
			{
				forshuffle: "options_sign correctOne",
				labeltext: data.string.q5opt4,
				correctoption :[{}]

			},
			],
			textwithblank:[{
				textdata : data.string.ques5,
				remain : data.string.remain5,
				dashedLine: true,
				imgsrc: imgpath + "q05.jpg"
			}]


		}
		]
	},
	//slide 5
	{
		uppertextblock : [

			{
				textclass : "upper",
				textdata : data.string.top
			}


		],
		exerciseblock: [
		{
			imageoptions: [
			{
				forshuffle: "options_sign incorrectOne",
				labeltext: data.string.q6opt1,
				wrongoption :[{
					wrongs : "wrongic1",
				}]

			},
			{
				forshuffle: "options_sign incorrectTwo",
				labeltext: data.string.q6opt2,
				wrongoption :[{
					wrongs : "wrongic2",
				}]
			},
			{
				forshuffle: "options_sign incorrectThree",
				labeltext: data.string.q6opt3,
				wrongoption :[{
					wrongs : "wrongic3",
				}]

			},
			{
				forshuffle: "options_sign correctOne",
				labeltext: data.string.q6opt4,
				correctoption :[{}]

			},
			],
			textwithblank:[{
				textdata : data.string.ques6,
				remain : data.string.remain6,
				dashedLine: true,
				imgsrc: imgpath + "6.jpg"
			}]


		}
		]
	},
	//slide 6
	{
		uppertextblock : [

			{
				textclass : "upper",
				textdata : data.string.top
			}


		],
		exerciseblock: [
		{
			imageoptions: [
			{
				forshuffle: "options_sign incorrectOne",
				labeltext: data.string.q7opt1,
				wrongoption :[{
					wrongs : "wrongic1",
				}]

			},
			{
				forshuffle: "options_sign incorrectTwo",
				labeltext: data.string.q7opt2,
				wrongoption :[{
					wrongs : "wrongic2",
				}]
			},
			{
				forshuffle: "options_sign incorrectThree",
				labeltext: data.string.q7opt3,
				wrongoption :[{
					wrongs : "wrongic3",
				}]

			},
			{
				forshuffle: "options_sign correctOne",
				labeltext: data.string.q7opt4,
				correctoption :[{}]

			},
			],
			textwithblank:[{
				textdata : data.string.ques7,
				remain : data.string.remain7,
				dashedLine: true,
				imgsrc: imgpath + "7.jpg"
			}]


		}
		]
	},
	//slide 7
	{
		uppertextblock : [

			{
				textclass : "upper",
				textdata : data.string.top
			}


		],
		exerciseblock: [
		{
			imageoptions: [
			{
				forshuffle: "options_sign incorrectOne",
				labeltext: data.string.q8opt1,
				wrongoption :[{
					wrongs : "wrongic1",
				}]

			},
			{
				forshuffle: "options_sign incorrectTwo",
				labeltext: data.string.q8opt2,
				wrongoption :[{
					wrongs : "wrongic2",
				}]
			},
			{
				forshuffle: "options_sign incorrectThree",
				labeltext: data.string.q8opt3,
				wrongoption :[{
					wrongs : "wrongic3",
				}]

			},
			{
				forshuffle: "options_sign correctOne",
				labeltext: data.string.q8opt4,
				correctoption :[{}]

			},
			],
			textwithblank:[{
				textdata : data.string.ques8,
				remain : data.string.remain8,
				dashedLine: true,
				imgsrc: imgpath + "8.jpg"
			}]


		}
		]
	},
	//slide 8
	{
		uppertextblock : [

			{
				textclass : "upper",
				textdata : data.string.top
			}


		],
		exerciseblock: [
		{
			imageoptions: [
			{
				forshuffle: "options_sign incorrectOne",
				labeltext: data.string.q9opt1,
				wrongoption :[{
					wrongs : "wrongic1",
				}]

			},
			{
				forshuffle: "options_sign incorrectTwo",
				labeltext: data.string.q9opt2,
				wrongoption :[{
					wrongs : "wrongic2",
				}]
			},
			{
				forshuffle: "options_sign incorrectThree",
				labeltext: data.string.q9opt3,
				wrongoption :[{
					wrongs : "wrongic3",
				}]

			},
			{
				forshuffle: "options_sign correctOne",
				labeltext: data.string.q9opt4,
				correctoption :[{}]

			},
			],
			textwithblank:[{
				textdata : data.string.ques9,
				remain : data.string.remain9,
				dashedLine: true,
				imgsrc: imgpath + "9.jpg"
			}]


		}
		]
	},
	//slide 9

{
		uppertextblock : [

			{
				textclass : "upper",
				textdata : data.string.top
			}


		],
		exerciseblock: [
		{
			imageoptions: [
			{
				forshuffle: "options_sign incorrectOne",
				labeltext: data.string.q10opt1,
				wrongoption :[{
					wrongs : "wrongic1",
				}]

			},
			{
				forshuffle: "options_sign incorrectTwo",
				labeltext: data.string.q10opt2,
				wrongoption :[{
					wrongs : "wrongic2",
				}]
			},
			{
				forshuffle: "options_sign incorrectThree",
				labeltext: data.string.q10opt3,
				wrongoption :[{
					wrongs : "wrongic3",
				}]

			},
			{
				forshuffle: "options_sign correctOne",
				labeltext: data.string.q10opt4,
				correctoption :[{}]

			},
			],
			textwithblank:[{
				textdata : data.string.ques10,
				remain : data.string.remain10,
				dashedLine: true,
				imgsrc: imgpath + "q11.jpg"
			}]


		}
		]
	},
	];

/*remove this for non random questions*/
content.shufflearray();

$(function (){
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	var score = 0;

	/*values in this array is same as the name of images of eggs in image folder*/
	var testin = new EggTemplate();

 	testin.init(10);
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$('.rightic').hide(0);
		$('.wrongic1').hide(0);
		$('.wrongic2').hide(0);
		$('.wrongic3').hide(0);

		/*generate question no at the beginning of question*/
		testin.numberOfQuestions();
		countNext==0 ? exercise.play() :'';
		var parent = $(".optionsdiv");
		var divs = parent.children();
		while (divs.length) {
			parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}

		switch(countNext) {
			default:
				var frac_1_n = 0;
				var frac_1_d = 0;
				var frac_2_n = 0;
				var frac_2_d = 0;
				var new_question1 = 0;
				var new_question2 = 0;
				var rand_multiplier_1 = 1;
				var rand_multiplier_2 = 1;
				var incorrect = false;

				function correct_btn(current_btn){
					$('.options_sign').addClass('disabled');
					$('.hidden_sign').html($(current_btn).html());
					$('.hidden_sign').addClass('fade_in');
					$('.rightic').show(0);
					$('.optionscontainer').removeClass('forHover');
					$(current_btn).addClass('option_true');
					$nextBtn.show(0);
					if(!incorrect){
						testin.update(true);
					}
				}
				function incorrect_btn1(current_btn){
					$(current_btn).addClass('disabled');
					$(current_btn).addClass('option_false');
					testin.update(false);
					incorrect = true;
				}
				function incorrect_btn2(current_btn){
					$(current_btn).addClass('disabled');
					$(current_btn).addClass('option_false');
					$('.wrongic').show(0);
					testin.update(false);
					incorrect = true;
				}
				function incorrect_btn(current_btn){
					$(current_btn).addClass('disabled');
					$(current_btn).addClass('option_false');
					$('.wrongic').show(0);
					testin.update(false);
					incorrect = true;
				}
				break;
		}

		var correctlyanswered = false;
		$('.correctOne').click(function(){
			if(correctlyanswered){
				return correctlyanswered;
			}
			correctlyanswered = true;
			correct_btn(this);
			var texto = $(this).find(".quesLabel");
			var textoo = $(texto).text();
			$('.dashedLine').html(textoo);
			$('.dashedLine').html(textoo).addClass('dsLine');
			play_correct_incorrect_sound(1);
		});

		$('.incorrectOne').click(function(){
			if(correctlyanswered){
				return correctlyanswered;
			}
			incorrect_btn(this);
			$('.wrongic1').show(0);
			play_correct_incorrect_sound(0);
		});
		$('.incorrectTwo').click(function(){
			if(correctlyanswered){
				return correctlyanswered;
			}
			incorrect_btn(this);
			$('.wrongic2').show(0);
			play_correct_incorrect_sound(0);
		});
		$('.incorrectThree').click(function(){
			if(correctlyanswered){
				return correctlyanswered;
			}
			incorrect_btn1(this);
			$('.wrongic3').show(0);
			play_correct_incorrect_sound(0);

		});
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
			templateCaller();
			testin.gotoNext();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

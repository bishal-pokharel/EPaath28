var imgpath = $ref + "/images/";
var imgpath1 = $ref+"/images/images_for_diy/";
var soundAsset = $ref+"/sounds/";

var content = [

	//slide 0

	{
		// contentblockadditionalclass: 'default-bg',

		extratextblock:[{
			textclass:'text1',
			textdata: data.string.p2text1,
			datahighlightflag: true,
			datahighlightcustomclass: 'textcolor'
		},
		],
		imageblock:[{
			imagestoshow:[{
				imgclass: 'coverpage',
				imgsrc: imgpath+'a_37.png'
			}]
		}]
	},

  //slide 1
	{
		contentblockadditionalclass: 'purplebg',
		speechbox:[{
			txtblock:[{
				textdata : data.string.p2text2,
				textclass : 'text_inside',
				datahighlightflag: true,
				datahighlightcustomclass: 'textcolor'
			}],
			speechbox: 'sp-3',
			imgclass: 'flipped-h',
			imgsrc : imgpath1+'text_box.png',
		}],
	},

	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bgcolor',

		uppertextblockadditionalclass: 'question-text pangolin',
		uppertextblock : [{
			textdata : data.string.p2text3,
			textclass : 'my_font_very_big'
		}],

		extratextblock : [{
			textclass:'instruction',
			textdata: data.string.p2text0,
			datahighlightflag: true,
			datahighlightcustomclass: 'textcolor'
		},{
			isoption : true,
			textdata : data.string.pgood,
			textclass : 'my_font_very_big option happymonkey opt-1'
		},
		{
			isoption : true,
			textdata : data.string.pbad,
			textclass : 'my_font_very_big option happymonkey opt-2'
		}],
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bgcolor',

		uppertextblockadditionalclass: 'question-text pangolin',
		uppertextblock : [{
			textdata : data.string.p2text5,
			textclass : 'my_font_very_big'
		}],

		extratextblock : [{
			textclass:'instruction',
			textdata: data.string.p2text0,
			datahighlightflag: true,
			datahighlightcustomclass: 'textcolor'
		},{
			isoption : true,
			textdata : data.string.pyes,
			textclass : 'my_font_very_big option happymonkey opt-1'
		},
		{
			isoption : true,
			textdata : data.string.pno,
			textclass : 'my_font_very_big option happymonkey opt-2'
		}],
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bgcolor',

		uppertextblockadditionalclass: 'question-text pangolin',
		uppertextblock : [{
			textdata : data.string.p2text7,
			textclass : 'my_font_very_big'
		}],

		extratextblock : [{
			textclass:'instruction',
			textdata: data.string.p2text0,
			datahighlightflag: true,
			datahighlightcustomclass: 'textcolor'
		},{
			isoption : true,
			textdata : data.string.pyes10,
			textclass : 'my_font_very_big option happymonkey opt-1 option-l0'
		},
		{
			isoption : true,
			textdata : data.string.pno10,
			textclass : 'my_font_very_big option happymonkey opt-2 option-l0'
		}],
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bgcolor',

		uppertextblockadditionalclass: 'question-text pangolin',
		uppertextblock : [{
			textdata : data.string.p2text9,
			textclass : 'my_font_very_big'
		}],

		extratextblock : [{
			textclass:'instruction',
			textdata: data.string.p2text0,
			datahighlightflag: true,
			datahighlightcustomclass: 'textcolor'
		},{
			isoption : true,
			textdata : data.string.pyes1,
			textclass : 'my_font_very_big option happymonkey opt-1 option-l0'
		},
		{
			isoption : true,
			textdata : data.string.pno1,
			textclass : 'my_font_very_big option happymonkey opt-2 option-l0'
		}],
	},
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bgcolor',

		uppertextblockadditionalclass: 'question-text pangolin',
		uppertextblock : [{
			textdata : data.string.p2text11,
			textclass : 'my_font_very_big'
		}],

		extratextblock : [{
			textclass:'instruction',
			textdata: data.string.p2text0,
			datahighlightflag: true,
			datahighlightcustomclass: 'textcolor'
		},{
			isoption : true,
			textdata : data.string.pyes2,
			textclass : 'my_font_very_big option happymonkey opt-1'
		},
		{
			isoption : true,
			textdata : data.string.pno2,
			textclass : 'my_font_very_big option happymonkey opt-2'
		}],
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bgcolor',

		uppertextblockadditionalclass: 'question-text pangolin',
		uppertextblock : [{
			textdata : data.string.p2text13,
			textclass : 'my_font_very_big'
		}],

		extratextblock : [{
			textclass:'instruction',
			textdata: data.string.p2text0,
			datahighlightflag: true,
			datahighlightcustomclass: 'textcolor'
		},{
			isoption : true,
			textdata : data.string.pyes11,
			textclass : 'my_font_very_big option happymonkey opt-1 option-l0'
		},
		{
			isoption : true,
			textdata : data.string.pno11,
			textclass : 'my_font_very_big option happymonkey opt-2 option-l0'
		}],
	},
	// //slide8
	// {
	// 	hasheaderblock : false,
	// 	contentblocknocenteradjust : true,
	// 	contentblockadditionalclass : '',
	//
	// 	uppertextblockadditionalclass: 'question-text pangolin',
	// 	uppertextblock : [{
	// 		textdata : data.string.p2text15,
	// 		textclass : 'my_font_very_big'
	// 	}],
	//
	// 	extratextblock : [{
	// 		isoption : true,
	// 		textdata : data.string.pyes3,
	// 		textclass : 'my_font_very_big option happymonkey opt-1 option-l0'
	// 	},
	// 	{
	// 		isoption : true,
	// 		textdata : data.string.pno3,
	// 		textclass : 'my_font_very_big option happymonkey opt-2 option-l0'
	// 	},
	// 	{
	// 		textdata : data.string.p2text16,
	// 		textclass : 'feedback slide-in my_font_big'
	// 	}],
	// },
	// //slide9
	// {
	// 	hasheaderblock : false,
	// 	contentblocknocenteradjust : true,
	// 	contentblockadditionalclass : '',
	//
	// 	uppertextblockadditionalclass: 'question-text pangolin',
	// 	uppertextblock : [{
	// 		textdata : data.string.p2text17,
	// 		textclass : 'my_font_very_big'
	// 	}],
	//
	// 	extratextblock : [{
	// 		isoption : true,
	// 		textdata : data.string.pyes4,
	// 		textclass : 'my_font_very_big option happymonkey opt-1 option-l0'
	// 	},
	// 	{
	// 		isoption : true,
	// 		textdata : data.string.pno4,
	// 		textclass : 'my_font_very_big option happymonkey opt-2 option-l0'
	// 	},
	// 	{
	// 		textdata : data.string.p2text18,
	// 		textclass : 'feedback slide-in my_font_big'
	// 	}],
	// },

	//slide11
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bgcolor',
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'squirrel',
					imgsrc: imgpath + "squirrel.png",
				}
			]
		}],
		listblock : [{
			listblockadditionalclass: 'positive-list slide-down',
			textdata : data.string.p2text20,
			textclass : 'my_font_big proximanova',
			listitem: [

			],
		},
		{
			listblockadditionalclass: 'negative-list slide-down',
			textdata : data.string.p2text21,
			textclass : 'my_font_big proximanova',
			listitem: [

			],
		}],
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;

	var timeoutvar = null;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var count_arr = [-1,-1,-1,-1,-1];
	var good_arr = [
					data.string.good1,
					data.string.good2,
					data.string.good3,
					data.string.good4,
					data.string.good5,
					data.string.good6
					];
	var bad_arr = [
					data.string.bad1,
					data.string.bad2,
					data.string.bad3,
					data.string.bad4,
					data.string.bad5,
					data.string.bad6,
					];

	var bad_arr1 =  [
					data.string.bad11,
					data.string.bad12,
					data.string.bad13,
					data.string.bad14,
					data.string.bad15,
					data.string.bad16,
					];
		var preload;
		var timeoutvar = null;
		var current_sound;

	  var vocabcontroller = new Vocabulary();
	  vocabcontroller.init();

		function init() {
			//specify type otherwise it will load assests as XHR
			manifest = [
					//images
				// soundsicon-orange
				{id: "sound_1", src: soundAsset+"s4_p2.ogg"},
				{id: "sound_2", src: soundAsset+"s4_p3.ogg"},
				{id: "sound_3", src: soundAsset+"s4_p9_1.ogg"},
				{id: "sound_4", src: soundAsset+"s4_p9_2.ogg"},
			];
			preload = new createjs.LoadQueue(false);
			preload.installPlugin(createjs.Sound);//for registering sounds
			preload.on("progress", handleProgress);
			preload.on("complete", handleComplete);
			preload.on("fileload", handleFileLoad);
			preload.loadManifest(manifest, true);
		}
		function handleFileLoad(event) {
			// console.log(event.item);
		}
		function handleProgress(event) {
			$('#loading-text').html(parseInt(event.loaded*100)+'%');
		}
		function handleComplete(event) {
			$('#loading-wrapper').hide(0);
			//initialize varibales
			current_sound = createjs.Sound.play('sound_1');
			current_sound.stop();
			// call main function
			templateCaller();
		}
		//initialize
		init();

	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);

		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
			case 0:
			play_diy_audio();
			nav_button_controls(2000);
			break;
			case 1:
			sound_nav("sound_"+countNext);
			break;
			case 2:
			sound_player("sound_"+countNext);
			break;

			case 8:
				var pos_count = 0;
				var pos_count1 = 0;
				//sound_player(sound_11);
				// count_arr = [0,0,0,0,1,1,1,1];

				// nav_button_controls(15000);
				console.log('countarray: '+count_arr.length);
				for( var i = 0; i<count_arr.length+1; i++){
					$('.negative-list>ol').html($('.negative-list>ol').html()+'<li>'+bad_arr1[i]+'</li>');
					if(count_arr[i]==1){
						console.log('goodarr '+good_arr[i]+'i '+i);
						$('.positive-list>ol').html($('.positive-list>ol').html()+'<li>'+good_arr[i-1]+'</li>');
						pos_count++;
					} else if(count_arr[i]==0){
						$('.positive-list>ol').html($('.positive-list>ol').html()+'<li>'+bad_arr[i-1]+'</li>');
						pos_count++;
					}
				}

				$('.positive-list').show(0);
				var m = 0;
				while(m <= count_arr.length ){
					if(m<pos_count){
						$('.positive-list li').eq(m).delay(1500+m*1500).fadeIn(1000);
					} else if(m==pos_count){
						$('.negative-list').delay(4000+m*1500).show(0, function(){
							//sound_player(sound_12);
							$('.squirrel').css('transform', 'scaleX(-1)');
							var n= 0;
							while(n <= count_arr.length ){
								console.log('m ',+n,'pos_count '+pos_count);
								if(n<pos_count){
									$('.negative-list li').eq(n).delay(1500+n*1500).fadeIn(1000);
								}else{

								}
								n++;
							}
							setTimeout(function(){
								nav_button_controls();
							},8500)
						});
					} else {
						// $('.negative-list li').eq(m-(pos_count+1)).delay(1500+m*1500).fadeIn(1000);
					}
					m++;
				}

				// for( var i =1; i<count_arr.length; i++){
        // 	// $('.negative-list>ol').html($('.negative-list>ol').html()+'<li>'+bad_arr1[i]+'</li>');
				// 	if(count_arr[i]==1){
				// 		console.log('good',good_arr[i],'i',+i);
				// 		$('.positive-list>ol').html($('.positive-list>ol').html()+'<li>'+good_arr[i]+'</li>');
        //     pos_count++;
				// 	}else if(count_arr[i]==0){
				// 	  console.log('bad',bad_arr[i],'i',+i);
				// 		$('.positive-list>ol').html($('.positive-list>ol').html()+'<li>'+bad_arr[i]+'</li>');
				// 		pos_count++;
				// 	}
				// }
				//
				// for( var i =1; i<count_arr.length; i++){
				// 		console.log('badarr1',bad_arr1[i]);
				// 		$('.negative-list>ol').html($('.negative-list>ol').html()+'<li>'+bad_arr1[i]+'</li>');
				// }

				// $('.positive-list').show(0);
				// var m = 0;
				// while(m <= count_arr.length ){
				// 	console.log('m ',+m,'pos_count '+pos_count);
				// 	if(m<pos_count){
				// 		$('.positive-list li').eq(m-2).delay(1500+m*1000).fadeIn(1000);
				// 	} else if(m==pos_count){
				// 		  $('.negative-list').delay(4000+m*1500).show(0, function(){
				// 			//sound_player(sound_12);
				// 			$('.squirrel').css('transform', 'scaleX(-1)');
				// 			var n= 0;
				// 			while(n <= count_arr.length ){
				// 				console.log('m ',+n,'pos_count '+pos_count);
				// 				if(n<pos_count){
				// 					$('.negative-list li').eq(n).delay(1500+n*1500).fadeIn(1000);
				// 				}
				// 				n++;
				// 			}
				// 		});
				// 	}else {
				//
				// 	}
				// 	m++;
				// }

				break;
			default:

				break;
		}
		console.log('in default');
		//sound_player(sound_arr_var[countNext]);
		if(count_arr[countNext-1]==1){
			$('.opt-1').addClass('selected-option');
			//$('.feedback').show(0);
			$nextBtn.show(0);
		} else if(count_arr[countNext-1]==0){
			$('.opt-2').addClass('selected-option');
			//$('.feedback').show(0);
			$nextBtn.show(0);
		}
		$('.option').click(function(){
			$('.option').removeClass('selected-option');
			$(this).addClass('selected-option');
			if( $(this).hasClass('opt-1')){
				count_arr[countNext-1] = 1;
			} else{
				count_arr[countNext-1] = 0;
			}
				$nextBtn.show(0);
			//sound_nav(sound_arr_var2[countNext]);
			//$('.feedback').show(0);
		});
	}



	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		current_sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		current_sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(0);
		});
	}

	function templateCaller() {
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

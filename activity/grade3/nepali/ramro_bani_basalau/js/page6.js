var imgpath = $ref + "/images/images_for_diy/";
var soundAsset = $ref+"/sounds/";

var content = [
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "contentwithbg",
        imageblock: [{
            imagestoshow: [

                {
                    imgdiv: "speech speechimg1",
                    imgclass: "img1",
                    imgid: 'bgImgsp1',
                    imgsrc: ""
                },
                {
                    imgdiv: "speech speechimg2",
                    imgclass: "img2",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv: "speech speechimg3",
                    imgclass: "img3",
                    imgid: 'bgImgsp3',
                    imgsrc: ""
                },
                {
                    imgdiv: "speech speechimg4",
                    imgclass: "img4",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv: "speech speechimg5",
                    imgclass: "img5",
                    imgid: 'bgImgsp3',
                    imgsrc: ""
                },
                {
                    imgdiv: "speech speechimg6",
                    imgclass: "img6",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv: "speech speechimg7",
                    imgclass: "img7",
                    imgid: 'bgImgsp3',
                    imgsrc: ""
                },
                {
                    imgdiv: "speech speechimg8",
                    imgclass: "img8",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv: "people1",
                    imgclass: "people",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "people2",
                    imgclass: "people_sec",
                    imgid: 'bgImg0',
                    imgsrc: ""
                }
            ]
        }],
        textblock: [
            {
                textdiv: "text1",
                textclass: "centertext chapter",
                textdata: data.string.e1s0
            },
            {
                textdiv: "violet draggable",
                textclass: "centertext chapter",
                textdata: data.string.e1s2,
                ans: data.string.e1s2
            },
            {
                textdiv: "indigo draggable",
                textclass: "centertext chapter",
                textdata: data.string.e1s3,
                ans: data.string.e1s3
            },
            {
                textdiv: "text speechtxt1",
                textclass: "centertext chapter",
                textdata: data.string.e1s1,
                ans: ''
            },
            {
                textdiv: "text speechtxt2 droppable",
                textclass: "centertext chapter",
                textdata: data.string.e1s2,
                ans: data.string.e1s2
            },
            {
                textdiv: "text speechtxt3",
                textclass: "centertext chapter",
                textdata: data.string.e1s4,
                ans: ''
            },
            {
                textdiv: "text speechtxt4",
                textclass: "centertext chapter",
                textdata: data.string.e1s5,
                ans: ''
            }
        ],
    },
    // slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "contentwithbg",
        imageblock: [{
            imagestoshow: [

                {
                    imgdiv: "speech speechimg1",
                    imgclass: "img1",
                    imgid: 'bgImgsp1',
                    imgsrc: ""
                },
                {
                    imgdiv: "speech speechimg2",
                    imgclass: "img2",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv: "speech speechimg3",
                    imgclass: "img3",
                    imgid: 'bgImgsp3',
                    imgsrc: ""
                },
                {
                    imgdiv: "speech speechimg4",
                    imgclass: "img4",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv: "speech speechimg5",
                    imgclass: "img5",
                    imgid: 'bgImgsp3',
                    imgsrc: ""
                },
                {
                    imgdiv: "speech speechimg6",
                    imgclass: "img6",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv: "speech speechimg7",
                    imgclass: "img7",
                    imgid: 'bgImgsp3',
                    imgsrc: ""
                },
                {
                    imgdiv: "speech speechimg8",
                    imgclass: "img8",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv: "people1",
                    imgclass: "people",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "people2",
                    imgclass: "people_sec",
                    imgid: 'bgImg0',
                    imgsrc: ""
                }
            ]
        }],
        textblock: [
            {
                textdiv: "text1",
                textclass: "centertext chapter",
                textdata: data.string.e1s0
            },
            {
                textdiv: "violet draggable",
                textclass: "centertext chapter",
                textdata: data.string.e1s6,
                ans: data.string.e1s6
            },
            {
                textdiv: "indigo draggable",
                textclass: "centertext chapter",
                textdata: data.string.e1s7,
                ans: data.string.e1s7
            },
            {
                textdiv: "text speechtxt5 droppable",
                textclass: "centertext chapter",
                textdata: data.string.e1s6,
                ans: data.string.e1s6
            },
            {
                textdiv: "text speechtxt1",
                textclass: "centertext chapter",
                textdata: data.string.e1s1,
                ans: ''
            },
            {
                textdiv: "text speechtxt2",
                textclass: "centertext chapter",
                textdata: data.string.e1s2,
                ans: ''
            },
            {
                textdiv: "text speechtxt3",
                textclass: "centertext chapter",
                textdata: data.string.e1s4,
                ans: ''
            },
            {
                textdiv: "text speechtxt4",
                textclass: "centertext chapter",
                textdata: data.string.e1s5,
                ans: ''
            }
        ],
    },
    // //slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "contentwithbg",
        imageblock:[{
            imagestoshow: [

                {
                    imgdiv: "speech speechimg1",
                    imgclass: "img1",
                    imgid: 'bgImgsp1',
                    imgsrc: ""
                },
                {
                    imgdiv: "speech speechimg2",
                    imgclass: "img2",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv: "speech speechimg3",
                    imgclass: "img3",
                    imgid: 'bgImgsp3',
                    imgsrc: ""
                },
                {
                    imgdiv: "speech speechimg4",
                    imgclass: "img4",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv: "speech speechimg5",
                    imgclass: "img5",
                    imgid: 'bgImgsp3',
                    imgsrc: ""
                },
                {
                    imgdiv: "speech speechimg6",
                    imgclass: "img6",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv: "speech speechimg7",
                    imgclass: "img7",
                    imgid: 'bgImgsp3',
                    imgsrc: ""
                },
                {
                    imgdiv: "speech speechimg8",
                    imgclass: "img8",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv: "people1",
                    imgclass: "people",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "people2",
                    imgclass: "people_sec",
                    imgid: 'bgImg0',
                    imgsrc: ""
                }
            ]
        }],
        textblock:[
            {
                textdiv:"text1",
                textclass:"centertext chapter",
                textdata:data.string.e1s0
            },
            {
                textdiv:"text speechtxt5",
                textclass:"centertext chapter",
                textdata: data.string.e1s6,
                ans: ''
            },
            {
                textdiv:"text speechtxt6",
                textclass:"centertext chapter",
                textdata: data.string.e1s8,
                ans: ''
            },
            {
                textdiv:"violet draggable",
                textclass:"centertext chapter",
                textdata:data.string.e1s9,
                ans:data.string.e1s9
            },
            {
                textdiv:"indigo draggable",
                textclass:"centertext chapter",
                textdata:data.string.e1s10,
                ans:data.string.e1s10
            },
            {
                textdiv:"text speechtxt7 droppable",
                textclass:"centertext chapter",
                textdata: data.string.e1s9,
                ans: data.string.e1s9
            },

            {
                textdiv:"text speechtxt1",
                textclass:"centertext chapter",
                textdata: data.string.e1s1,
                ans: ''
            },
            {
                textdiv:"text speechtxt2",
                textclass:"centertext chapter",
                textdata: data.string.e1s2,
                ans: ''
            },
            {
                textdiv:"text speechtxt3",
                textclass:"centertext chapter",
                textdata: data.string.e1s4,
                ans: ''
            },
            {
                textdiv:"text speechtxt4",
                textclass:"centertext chapter",
                textdata:data.string.e1s5,
                ans:''
            }
        ],
    },
    // //slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "contentwithbg",
        imageblock:[{
            imagestoshow: [

                {
                    imgdiv: "speech speechimg1",
                    imgclass: "img1",
                    imgid: 'bgImgsp1',
                    imgsrc: ""
                },
                {
                    imgdiv: "speech speechimg2",
                    imgclass: "img2",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv: "speech speechimg3",
                    imgclass: "img3",
                    imgid: 'bgImgsp3',
                    imgsrc: ""
                },
                {
                    imgdiv: "speech speechimg4",
                    imgclass: "img4",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv: "speech speechimg5",
                    imgclass: "img5",
                    imgid: 'bgImgsp3',
                    imgsrc: ""
                },
                {
                    imgdiv: "speech speechimg6",
                    imgclass: "img6",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv: "speech speechimg7",
                    imgclass: "img7",
                    imgid: 'bgImgsp3',
                    imgsrc: ""
                },
                {
                    imgdiv: "speech speechimg8",
                    imgclass: "img8",
                    imgid: 'bgImgsp2',
                    imgsrc: ""
                },
                {
                    imgdiv: "people1",
                    imgclass: "people",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "people2",
                    imgclass: "people_sec",
                    imgid: 'bgImg0',
                    imgsrc: ""
                }
            ]
        }],
        textblock:[
            {
                textdiv:"text1",
                textclass:"centertext chapter",
                textdata:data.string.e1s0
            },
            {
                textdiv:"text speechtxt5",
                textclass:"centertext chapter",
                textdata: data.string.e1s6,
                ans: ''
            },
            {
                textdiv:"text speechtxt6",
                textclass:"centertext chapter",
                textdata: data.string.e1s8,
                ans: ''
            },
            {
                textdiv:"text speechtxt7",
                textclass:"centertext chapter",
                textdata: data.string.e1s9,
                ans: ''
            },
            {
                textdiv:"text speechtxt8",
                textclass:"centertext chapter",
                textdata: data.string.e1s11,
                ans: ''
            },
            {
                textdiv:"text speechtxt1",
                textclass:"centertext chapter",
                textdata: data.string.e1s1,
                ans: ''
            },
            {
                textdiv:"text speechtxt2",
                textclass:"centertext chapter",
                textdata: data.string.e1s2,
                ans: ''
            },
            {
                textdiv:"text speechtxt3",
                textclass:"centertext chapter",
                textdata: data.string.e1s4,
                ans: ''
            },
            {
                textdiv:"text speechtxt4",
                textclass:"centertext chapter",
                textdata:data.string.e1s5,
                ans:''
            }
        ],
    }

];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "bgmain", src:imgpath+"bg_exercise.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg", src:imgpath+"suraj.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg0", src:imgpath+"mum.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg1", src:imgpath+"q02a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg2", src:imgpath+"q2b.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg3", src:imgpath+"q03a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg4", src:imgpath+"q03b.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg5", src:imgpath+"q04a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg6", src:imgpath+"q04b.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg7", src:imgpath+"q05a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg8", src:imgpath+"q05b.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg9", src:imgpath+"q06a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg10", src:imgpath+"q06b.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg11", src:imgpath+"q07a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg12", src:imgpath+"q07b.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg13", src:imgpath+"q08a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg14", src:imgpath+"q08b.png", type: createjs.AbstractLoader.IMAGE},

            {id: "bgImg15", src:imgpath+"q09a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg16", src:imgpath+"q09b.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg17", src:imgpath+"q10a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg18", src:imgpath+"q10b.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImgsp1", src:imgpath+"textbox03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImgsp2", src:imgpath+"textbox04.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImgsp3", src:imgpath+"textbox05.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sundar", src:"images/sundar/correct-1.png", type: createjs.AbstractLoader.IMAGE},
            // soundsicon-orange
            {id: "sound_1", src: soundAsset+"s6_p1.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();
    // var rhino = new NumberTemplate();
    //
    // rhino.init(10);

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        if(countNext==0){
            sound_player("sound_1",1);
        }
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);

        $('.indigo,.violet,.speechimg1,.speechimg2,.speechimg3,.speechimg4,.speechimg5,.speechimg6,.speechimg7,.speechimg8,.speechtxt1,.speechtxt2,.speechtxt3,.speechtxt4,.speechtxt5,.speechtxt6,.speechtxt7,.speechtxt8').hide();
        switch (countNext) {

            case 0:
                $('.speechtxt1,.speechimg1').delay(1500).fadeIn(1000);
                $('.speechtxt2,.speechimg2').delay(2500).fadeIn(1000);
                $('.violet,.indigo').delay(3500).fadeIn(1000);

                dragdrop();
                break;
            case 1:
                $('.speechtxt1,.speechimg1').show();
                $('.speechtxt2,.speechimg2').show();
                $('.speechtxt3,.speechimg3').delay(1500).fadeIn(1000);
                $('.speechtxt4,.speechimg4').delay(2500).fadeIn(1000);
                $('.speechtxt5,.speechimg5').delay(3500).fadeIn(1000);
                $('.violet,.indigo').delay(3500).fadeIn(1000);

                dragdrop();
                break;
            case 2:
                $('.speechtxt1,.speechimg1').show();
                $('.speechtxt2,.speechimg2').show();
                $('.speechtxt3,.speechimg3').show();
                $('.speechtxt4,.speechimg4').show();
                $('.speechtxt5,.speechimg5').show();
                $('.speechtxt6,.speechimg6').delay(1500).fadeIn(1000);
                $('.speechtxt7,.speechimg7').delay(2500).fadeIn(1000);
                $('.violet,.indigo').delay(2500).fadeIn(1000);

                dragdrop();
                break;
            case 3:
                $('.speechtxt1,.speechimg1').show();
                $('.speechtxt2,.speechimg2').show();
                $('.speechtxt3,.speechimg3').show();
                $('.speechtxt4,.speechimg4').show();
                $('.speechtxt5,.speechimg5').show();
                $('.speechtxt6,.speechimg6').show();
                $('.speechtxt7,.speechimg7').show();
                $('.speechtxt8,.speechimg8').delay(1500).fadeIn(1000);

                nav_button_controls(1800);
                break;

            default:
                break;
        }
    }

    function nav_button_controls(delay_ms){
        timeoutvar = setTimeout(function(){
            if(countNext==0){
                $nextBtn.show(0);
            } else if( countNext>0 && countNext == $total_page-1){
                $prevBtn.show(0);
                ole.footerNotificationHandler.pageEndSetNotification();
            } else{
                $prevBtn.show(0);
                $nextBtn.show(0);
            }
        },delay_ms);
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
    }



    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
        // rhino.gotoNext();

    });

    $refreshBtn.click(function(){
        var classoption = ["draggable violet", "draggable indigo"];
        shufflehint(classoption, "draggable");
        // templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });


    function shufflehint(optionclass,option) {
        var optdiv = $(".coverboardfull");

        for (var i = optdiv.find("."+option).length; i >= 0; i--) {
            optdiv.append(optdiv.find("."+option).eq(Math.random() * i | 0));
        }
        optdiv.find("."+option).removeClass().addClass("current");
        optdiv.find(".current").each(function (index) {
            $(this).addClass(optionclass[index]);
        });
    }

    function rotatethewheel(){
        $(".wheel,.wheel1").css("top","23%");
        $(".hide3").delay(300).animate({"opacity":"0"},500);
        $(".hide2").delay(2000).animate({"opacity":"0"},500);
        $(".hide1").delay(2500).animate({"opacity":"0"},500);
    }
    function dragdrop(){
        $(".droppable").attr("disabled","disabled");
        $(".draggable").draggable({
            containment: "body",
            revert: true,
            appendTo: "body",
            zindex: 10,
            start:function(event, ui){
                $(".wrongImg").remove();
                $(".draggable,.droppable").removeClass("wrongcss");
            },
        });
        // var rhino_flag = true;
        $('.droppable').droppable({
            accept : ".draggable",
            hoverClass: "hovered",
            drop: function(event, ui) {
                var draggableans = ui.draggable.attr("data-answer");
                var droppableans = $(this).attr("data-answer");
                if(draggableans.toString().trim() == droppableans.toString().trim()) {
                  createjs.Sound.stop();
                    play_correct_incorrect_sound(1);
                    // if(rhino_flag!=false){
                    //   rhino.update(true);}
                    ui.draggable.hide(0);
                    navigationcontroller(countNext,$total_page,true);
                    $(".draggable").addClass("avoid-clicks");
                    $(this).addClass("correctans");
                    $(".sundarDiv").show(0);
                    ui.draggable.draggable('disable');
                    (countNext,$total_page);
                }else {
                    ui.draggable.addClass("wrongans avoid-clicks");
                    createjs.Sound.stop();

                    play_correct_incorrect_sound(0);
                    // rhino.update(false);
                    // rhino_flag=false;
                }
            }
        });
    }
});

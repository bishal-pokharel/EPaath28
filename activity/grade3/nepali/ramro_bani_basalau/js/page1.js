var soundAsset = $ref+"/sounds/";
var imgpath = $ref+"/images/";
var imgpath1 = $ref+"/images/";

var content=[

		// slide0
		{
		  contentblockadditionalclass: 'purplebg',
			uppertextblock:[{
				textclass:'chaptertitle',
				textdata:data.lesson.chapter
			}],
			imageblock:[{
				imagestoshow:[
				{
					imgclass:'village',
					imgid:'coverpage'
				}]
			}]
		},

		// slide1
		{
		  contentblockadditionalclass: 'purplebg',
			uppertextblockadditionalclass: 'text1',
			uppertextblock:[{
				textclass:'',
				textdata:data.string.p1text1
			}],
			imageblock:[{
				imagestoshow:[
				{
					imgclass:'village',
					imgid:'village'
				},{
					imgclass:'family',
					imgid:'family'
				}]
			}]
		},
		// slide2
		{
		  contentblockadditionalclass: 'purplebg',
			uppertextblockadditionalclass: 'text1',
			uppertextblock:[{
				textclass:'',
				textdata:data.string.p1text2
			}],
			imageblock:[{
				imagestoshow:[
				{
					imgclass:'village',
					imgid:'class'
				}]
			}]
		},
		// slide3
		{
			contentblockadditionalclass: 'purplebg',
			uppertextblockadditionalclass: 'text1',
			uppertextblock:[{
				textclass:'',
				textdata:data.string.p1text4
			}],
			imageblock:[{
				imagestoshow:[
				{
					imgclass:'village',
					imgid:'bg'
				},
				{
					imgclass:'grandparents',
					imgid:'grandparents'
				},
			]
			}]
		},
		// slide4
		{
			contentblockadditionalclass: 'purplebg',
			uppertextblockadditionalclass: 'text1',
			uppertextblock:[{
				textclass:'',
				textdata:data.string.p1text5
			}],
			imageblock:[{
				imagestoshow:[
				{
					imgclass:'village',
					imgid:'eating'
				}
			]
			}]
		},
		// slide5
		{
			contentblockadditionalclass: 'purplebg',
			uppertextblockadditionalclass: 'text1',
			uppertextblock:[{
				textclass:'',
				textdata:data.string.p1text6
			}],
			imageblock:[{
				imagestoshow:[
				{
					imgclass:'village',
					imgid:'bg1'
				}
			]
			}]
		},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var colors = ['green','pink','orange','yellow','purple','lightgreen'];
	var shuffledcolors=colors.shufflearray();
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [

			//images

			// {id: "p01", src: imgpath+"p01.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "p02", src: imgpath+"img01.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "p03", src: imgpath+"p03.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "p04", src: imgpath+"img01a.png", type: createjs.AbstractLoader.IMAGE},

			{id: "coverpage", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},
			{id: "family", src: imgpath+"with_parents.png", type: createjs.AbstractLoader.IMAGE},
			{id: "village", src: imgpath+"village.png", type: createjs.AbstractLoader.IMAGE},
			{id: "class", src: imgpath+"intheclass.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg", src: imgpath+"bg_garden.png", type: createjs.AbstractLoader.IMAGE},
			{id: "grandparents", src: imgpath+"grand_parents.png", type: createjs.AbstractLoader.IMAGE},
			{id: "brother", src: imgpath+"brother_sister.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eating", src: imgpath+"eating_khaja.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg1", src: imgpath+"harka_dai01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg2", src: imgpath+"harka_dai02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg3", src: imgpath+"harka_dai03.png", type: createjs.AbstractLoader.IMAGE},
			//sounds
			{id: "sound_1", src: soundAsset+"s1_p1.ogg"},
			{id: "sound_2", src: soundAsset+"s1_p2.ogg"},
			{id: "sound_3", src: soundAsset+"s1_p3.ogg"},
			{id: "sound_4", src: soundAsset+"s1_p4.ogg"},
			{id: "sound_5", src: soundAsset+"s1_p5.ogg"},
			{id: "sound_6", src: soundAsset+"s1_p6.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
    vocabcontroller.findwords(countNext);

     $('.family').hide();
		 $('.text1').hide();
		 $('.grandparents').hide();

		switch(countNext) {

				case 0:
				sound_player("sound_"+(countNext+1));
				break;

				case 1:
					$('.text1').delay(500).fadeIn(1000);
					$('.family').delay(2500).fadeIn(1000);
					sound_player("sound_"+(countNext+1));
				 	break;

				case 2:
				case 3:
					$('.text1').css('padding','3%');
					$('.text1').delay(500).fadeIn(1000);
					$('.grandparents').delay(2500).fadeIn(1000);
					sound_player("sound_"+(countNext+1));
					break;
				case 4:
					$('.text1').css({'padding':'3%','top':'85%'});
					$('.text1').delay(500).fadeIn(1000);
					sound_player("sound_"+(countNext+1));
					break;

				case 5:
					$('.text1').delay(500).fadeIn(1000);
					sound_player("sound_"+(countNext+1));
					break;

				default:
				nav_button_controls(100);
				break;

		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id){
		setTimeout(function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
			current_sound.on("complete", function(){
				nav_button_controls(100);
			});
		},600);
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

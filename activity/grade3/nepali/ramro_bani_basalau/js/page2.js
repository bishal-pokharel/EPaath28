var soundAsset = $ref+"/sounds/";
var imgpath = $ref+"/images/";
var imgpath1 = $ref+"/images/";

var content=[

		// slide0
    {
			contentblockadditionalclass: 'purplebg',
			imageblock:[{
				imagestoshow:[
				{
					imgclass:'village',
					imgid:'bg1'
				}
			],}],
      speechbox:[{
  			txtblock:[{
  				textdata : data.string.p1text7,
  				textclass : 'text_inside',
  				datahighlightflag: true,
  				datahighlightcustomclass: 'textcolor'
  			}],
  			speechbox: 'sp-3',
  			imgclass: 'flipped-h',
  			imgid : 'textbox',
  			imgsrc: '',
  		}],
		},
		// slide1
    {
      contentblockadditionalclass: 'purplebg',
      imageblock:[{
        imagestoshow:[
        {
          imgclass:'village',
          imgid:'bg2'
        }
      ],}],
      speechbox:[{
        txtblock:[{
          textdata : data.string.p1text8,
          textclass : 'text_inside',
          datahighlightflag: true,
          datahighlightcustomclass: 'textcolor'
        }],
        speechbox: 'sp-2',
        imgclass: 'flipped-h',
        imgid : 'textbox1',
        imgsrc: '',
      }],
    },
		// slide2
    {
      contentblockadditionalclass: 'purplebg',
      imageblock:[{
        imagestoshow:[
        {
          imgclass:'village',
          imgid:'bg3'
        }
      ],}],
      speechbox:[{
        txtblock:[{
          textdata : data.string.p1text9,
          textclass : 'text_inside',
          datahighlightflag: true,
          datahighlightcustomclass: 'textcolor'
        }],
        speechbox: 'sp-1',
        imgclass: 'flipped-h',
        imgid : 'textbox2',
        imgsrc: '',
      }],
    },
		// slide3
    {
      contentblockadditionalclass: 'purplebg',
      imageblock:[{
        imagestoshow:[
        {
          imgclass:'village',
          imgid:'bg3'
        }
      ],}],
      speechbox:[{
        txtblock:[{
          textdata : data.string.p1text10,
          textclass : 'text_inside',
          datahighlightflag: true,
          datahighlightcustomclass: 'textcolor'
        }],
        speechbox: 'sp-3',
        imgclass: 'flipped-h',
        imgid : 'textbox',
        imgsrc: '',
      }],
    },
		// slide4
    {
      contentblockadditionalclass: 'purplebg',
      imageblock:[{
        imagestoshow:[
        {
          imgclass:'village',
          imgid:'bg3'
        }
      ],}],
      speechbox:[{
        txtblock:[{
          textdata : data.string.p1text11,
          textclass : 'text_inside',
          datahighlightflag: true,
          datahighlightcustomclass: 'textcolor'
        }],
        speechbox: 'sp-0',
        imgclass: 'flipped-h',
        imgid : 'textbox1',
        imgsrc: '',
      }],
    },
		// slide5
    {
      contentblockadditionalclass: 'purplebg',
      imageblock:[{
        imagestoshow:[
        {
          imgclass:'village',
          imgid:'bg4'
        }
      ],}],
      speechbox:[{
        txtblock:[{
          textdata : data.string.p1text12,
          textclass : 'text_inside',
          datahighlightflag: true,
          datahighlightcustomclass: 'textcolor'
        }],
        speechbox: 'sp-01',
        imgclass: 'flipped-h',
        imgid : 'textbox2',
        imgsrc: '',
      }],
    },
    // slide6
    {
      contentblockadditionalclass: 'purplebg',
      imageblock:[{
        imagestoshow:[
        {
          imgclass:'village',
          imgid:'bg5'
        }
      ],}],
      speechbox:[{
        txtblock:[{
          textdata : data.string.p1text13,
          textclass : 'text_inside',
          datahighlightflag: true,
          datahighlightcustomclass: 'textcolor'
        }],
        speechbox: 'sp-02',
        imgclass: 'flipped-h',
        imgid : 'textbox1',
        imgsrc: '',
      }],
    },
    // slide7
    {
      contentblockadditionalclass: 'purplebg',
      imageblock:[{
        imagestoshow:[
        {
          imgclass:'village',
          imgid:'bg2'
        }
      ],}],
      speechbox:[{
        txtblock:[{
          textdata : data.string.p1text14,
          textclass : 'text_inside',
          datahighlightflag: true,
          datahighlightcustomclass: 'textcolor'
        }],
        speechbox: 'sp-03',
        imgclass: 'flipped-h',
        imgid : 'textbox2',
        imgsrc: '',
      }],
    },
    // slide8
    {
      contentblockadditionalclass: 'purplebg',
      imageblock:[{
        imagestoshow:[
        {
          imgclass:'village',
          imgid:'bg1'
        }
      ],}],
      speechbox:[{
        txtblock:[{
          textdata : data.string.p1text15,
          textclass : 'text_inside',
          datahighlightflag: true,
          datahighlightcustomclass: 'textcolor'
        }],
        speechbox: 'sp-3',
        imgclass: 'flipped-h',
        imgid : 'textbox',
        imgsrc: '',
      }],
    },
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var colors = ['green','pink','orange','yellow','purple','lightgreen'];
	var shuffledcolors=colors.shufflearray();
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [

			//images

			// {id: "p01", src: imgpath+"p01.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "p02", src: imgpath+"img01.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "p03", src: imgpath+"p03.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "p04", src: imgpath+"img01a.png", type: createjs.AbstractLoader.IMAGE},

			{id: "village", src: imgpath+"village.png", type: createjs.AbstractLoader.IMAGE},
			{id: "class", src: imgpath+"intheclass.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg", src: imgpath+"bg_garden.png", type: createjs.AbstractLoader.IMAGE},
			{id: "grandparents", src: imgpath+"grand_parents.png", type: createjs.AbstractLoader.IMAGE},
			{id: "brother", src: imgpath+"brother_sister.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eating", src: imgpath+"eating_khaja.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg1", src: imgpath+"harka_dai01.png", type: createjs.AbstractLoader.IMAGE},
      {id: "bg2", src: imgpath+"harka_dai02.png", type: createjs.AbstractLoader.IMAGE},
      {id: "bg3", src: imgpath+"harka_dai03.png", type: createjs.AbstractLoader.IMAGE},
      {id: "bg4", src: imgpath+"harka_dai04.png", type: createjs.AbstractLoader.IMAGE},
      {id: "bg5", src: imgpath+"harka_dai05.png", type: createjs.AbstractLoader.IMAGE},
      {id: "textbox", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
      {id: "textbox1", src: imgpath+"text_box04.png", type: createjs.AbstractLoader.IMAGE},
      {id: "textbox2", src: imgpath+"text_box03.png", type: createjs.AbstractLoader.IMAGE},
      //sounds
			{id: "sound_1", src: soundAsset+"s2_p1.ogg"},
			{id: "sound_2", src: soundAsset+"s2_p2.ogg"},
			{id: "sound_3", src: soundAsset+"s2_p3.ogg"},
			{id: "sound_4", src: soundAsset+"s2_p4.ogg"},
			{id: "sound_5", src: soundAsset+"s2_p5.ogg"},
			{id: "sound_6", src: soundAsset+"s2_p6.ogg"},
      {id: "sound_7", src: soundAsset+"s2_p7.ogg"},
      {id: "sound_8", src: soundAsset+"s2_p8.ogg"},
      {id: "sound_9", src: soundAsset+"s2_p9.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
    vocabcontroller.findwords(countNext);

		switch(countNext) {
				case 2:
				case 3:
					$('.text1').css('padding','3%');
          sound_player("sound_"+(countNext+1));
					break;
				case 4:
					$('.text1').css({'padding':'3%','top':'85%'});
			       sound_player("sound_"+(countNext+1));
					break;
        case 15:
				case 17:
			 	$('.reading').delay(2500);
			   sound_player("sound_"+(countNext+1));
				break;
				default:
			  sound_player("sound_"+(countNext+1));
				break;

		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

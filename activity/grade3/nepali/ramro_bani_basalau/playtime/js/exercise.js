var imgpath = $ref+"/playtime/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide0
	{
		contentblockadditionalclass: 'default-bg',
		ques_img: 'im-1',
		exerciseblock: [
			{
				instructiondata: data.string.exins,
				questiondata: data.string.exq11,
				option: [{
					option_class: "class1",
					optiondata: data.string.exo11a,
				},
				{
						option_class: "class2",
						optiondata: data.string.exo11b,
				},
				{
						option_class: "class3",
						optiondata: data.string.exo11c,
				},
				{
						option_class: "class4",
						optiondata: data.string.exo11d,
				}],
			}
		]
	},
	// slide1
	{
		contentblockadditionalclass: 'default-bg',
		ques_img: 'im-2',
		exerciseblock: [
			{
				instructiondata: data.string.exins,
				questiondata: data.string.exq12,
				option: [{
					option_class: "class1",
					optiondata: data.string.exo12a,
				},
				{
						option_class: "class2",
						optiondata: data.string.exo12b,
				},
				{
						option_class: "class3",
						optiondata: data.string.exo12c,
				},
				{
						option_class: "class4",
						optiondata: data.string.exo12d,
				}],
			}
		]
	},
	// slide2
	{
		contentblockadditionalclass: 'default-bg',
		ques_img: 'im-3',
		exerciseblock: [
			{
				instructiondata: data.string.exins,
				questiondata: data.string.exq13,
				option: [{
					option_class: "class1",
					optiondata: data.string.exo13a,
				},
				{
						option_class: "class2",
						optiondata: data.string.exo13b,
				},
				{
						option_class: "class3",
						optiondata: data.string.exo13c,
				},
				{
						option_class: "class4",
						optiondata: data.string.exo13d,
				}],
			}
		]
	},
	// slide3
	{
		contentblockadditionalclass: 'default-bg',
		ques_img: 'im-4',
		exerciseblock: [
			{
				instructiondata: data.string.exins,
				questiondata: data.string.exq14,
				option: [{
					option_class: "class1",
					optiondata: data.string.exo14a,
				},
				{
						option_class: "class2",
						optiondata: data.string.exo14b,
				},
				{
						option_class: "class3",
						optiondata: data.string.exo14c,
				},
				{
						option_class: "class4",
						optiondata: data.string.exo14d,
				}],
			}
		]
	},
	// slide4
	{
		contentblockadditionalclass: 'default-bg',
		ques_img: 'im-5',
		exerciseblock: [
			{
				instructiondata: data.string.exins,
				questiondata: data.string.exq15,
				option: [{
					option_class: "class4",
					optiondata: data.string.exo15a,
				},
				{
						option_class: "class2",
						optiondata: data.string.exo15b,
				},
				{
						option_class: "class3",
						optiondata: data.string.exo15c,
				},
				{
						option_class: "class1",
						optiondata: data.string.exo15d,
				}],
			}
		]
	},
	// slide5
	{
		contentblockadditionalclass: 'default-bg',
		ques_img: 'im-6',
		exerciseblock: [
			{
				instructiondata: data.string.exins,
				questiondata: data.string.exq16,
				option: [{
					option_class: "class1",
					optiondata: data.string.exo16a,
				},
				{
						option_class: "class2",
						optiondata: data.string.exo16b,
				},
				{
						option_class: "class3",
						optiondata: data.string.exo16c,
				},
				{
						option_class: "class4",
						optiondata: data.string.exo16d,
				}],
			}
		]
	},
	// slide6
	{
		contentblockadditionalclass: 'default-bg',
		ques_img: 'im-7',
		exerciseblock: [
			{
				instructiondata: data.string.exins,
				questiondata: data.string.exq17,
				option: [{
					option_class: "class1",
					optiondata: data.string.exo17a,
				},
				{
						option_class: "class2",
						optiondata: data.string.exo17b,
				},
				{
						option_class: "class3",
						optiondata: data.string.exo17c,
				},
				{
						option_class: "class4",
						optiondata: data.string.exo17d,
				}],
			}
		]
	},
	// slide7
	{
		contentblockadditionalclass: 'default-bg',
		ques_img: 'im-8',
		exerciseblock: [
			{
				instructiondata: data.string.exins,
				questiondata: data.string.exq18,
				option: [{
					option_class: "class1",
					optiondata: data.string.exo18a,
				},
				{
						option_class: "class2",
						optiondata: data.string.exo18b,
				},
				{
						option_class: "class3",
						optiondata: data.string.exo18c,
				},
				{
						option_class: "class4",
						optiondata: data.string.exo18d,
				}],
			}
		]
	},
	// slide8
	{
		contentblockadditionalclass: 'default-bg',
		ques_img: 'im-9',
		exerciseblock: [
			{
				instructiondata: data.string.exins,
				questiondata: data.string.exq19,
				option: [{
					option_class: "class1",
					optiondata: data.string.exo19a,
				},
				{
						option_class: "class2",
						optiondata: data.string.exo19b,
				},
				{
						option_class: "class3",
						optiondata: data.string.exo19c,
				},
				{
						option_class: "class4",
						optiondata: data.string.exo19d,
				}],
			}
		]
	},
	// slide10
	{
		contentblockadditionalclass: 'default-bg',
		ques_img: 'im-10',
		exerciseblock: [
			{
				instructiondata: data.string.exins,
				questiondata: data.string.exq20,
				option: [{
					option_class: "class1",
					optiondata: data.string.exo20a,
				},
				{
						option_class: "class2",
						optiondata: data.string.exo20b,
				},
				{
						option_class: "class3",
						optiondata: data.string.exo20c,
				},
				{
						option_class: "class4",
						optiondata: data.string.exo20d,
				}],
			}
		]
	}
];
// content.shufflearray();

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;


	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	var scoring = new LampTemplate();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//sundar
			{id: "im-1", src: imgpath+'q01.png', type: createjs.AbstractLoader.IMAGE},
			{id: "im-2", src: imgpath+'q02.png', type: createjs.AbstractLoader.IMAGE},
			{id: "im-3", src: imgpath+'q03.png', type: createjs.AbstractLoader.IMAGE},
			{id: "im-4", src: imgpath+'q04.png', type: createjs.AbstractLoader.IMAGE},
			{id: "im-5", src: imgpath+'q05.png', type: createjs.AbstractLoader.IMAGE},
			{id: "im-6", src: imgpath+'q06.png', type: createjs.AbstractLoader.IMAGE},
			{id: "im-7", src: imgpath+'q07.png', type: createjs.AbstractLoader.IMAGE},
			{id: "im-8", src: imgpath+'q08.png', type: createjs.AbstractLoader.IMAGE},
			{id: "im-9", src: imgpath+'q09.png', type: createjs.AbstractLoader.IMAGE},
			{id: "im-10", src: imgpath+'q10.png', type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "exer", src: soundAsset+"ex.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		scoring.init($total_page);
		templateCaller();
	}
	//initialize
	init();


	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templatecaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templatecaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
	/**
	 How to:
	 - Just call instructionblockcontroller() from the template
	 */


	/**
	 What it does:
	 - It inserts and handles closing and opening of instruction block
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	var wrongclick = false;
	var ques_count = ['१) ', '२) ', '३) ', '४) ', '५) ', '६) ', '७) ', '८) ', '९) ', '१०) '];

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);

		var option_position = [1,2,3,4];
		option_position.shufflearray();
		for(var op=0; op<4; op++){
			$('.main-container').eq(op).addClass('option-pos-'+option_position[op]);
		}
		$('#ques-no').html(ques_count[countNext]);
		$('.center-image').attr('src', preload.getResult(content[countNext].ques_img).src);
		$('.correct-icon').attr('src', preload.getResult('correct').src);
		$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);

		// for sound
		if(countNext==0){
			sound_player("exer");
		}
		var wrong_clicked = 0;
		console.log("Hello");
		$(".option-container").on('click',function(){
			console.log('clicked correctincorrect');
			if($(this).hasClass("class1")){
				var answerData = $(this).find('p').html();
				if(wrong_clicked<1){
					scoring.update(true);
				}
				// $(".option-container").css('pointer-events','none');
	 			play_correct_incorrect_sound(1);
				$(this).addClass('correct-ans');
				$(this).parent().children('.correct-icon').show(0);
				wrong_clicked = 0;
				$('.fill-blank').html(answerData);
				$('.fill-blank').css('color', '#F63477');
				if(countNext != $total_page)
					$nextBtn.show(0);
			}
			else{
				scoring.update(false);
	 			play_correct_incorrect_sound(0);
				$(this).addClass('incorrect-ans');
				$(this).parent().children('.incorrect-icon').show(0);
				wrong_clicked++;
			}
		});

		$prevBtn.hide(0);
	}
	function sound_player(sound_id, navigate) {
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
	}
	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();
		generalTemplate();
	}

	$nextBtn.on('click', function() {
		countNext++;
		scoring.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
});

var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";

var soundcontent;

var content=[
	{
		// slide0
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description_title",
			textdata: data.string.lesson_title
		},{
			textclass: "description1 white",
			textdata: data.string.writer
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "background-img",
				imgsrc: "",
				imgid : 'bg02',
			},{
				imgclass: "talking",
				imgsrc: "",
				imgid : 'talking01',
			}]
		}]
	},{
		// slide1
		contentblockadditionalclass: "additionalbackground1",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb",
		uppertextblock: [{
			textclass: "title",
			textdata: data.string.lesson_title
		}],
		storytextblockadditionalclass: "  ltb_top_left_40",
		storytextblock:[{
			textclass: "textblack",
			textdata: data.string.para1_stanza1
		} ],
		imageblock:[{
			imagestoshow:[{
				imgclass: "background-img",
				imgsrc: "",
				imgid : 'bg02',
			},{
				imgclass: "talking",
				imgsrc: "",
				imgid : 'talking02',
			}]
		},{
  			imgcontainerdiv: "dialogcontainer_left",
	  		imagestoshow:[{
	  			imgclass: "dialogimage2",
	  			imgsrc : "",
	  			imgid: "paathi"
	  			
	  		}]
	  	},{
	  		imgcontainerdiv: "dialogcontainer_right",
	  		imagestoshow:[{
	  			imgclass: "dialogimage2_b",
	  			imgsrc : "",
	  			imgid: "pathi"
	  		}]
	  	}]

	},{
		// slide2
		contentblockadditionalclass: "additionalbackground1",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb",
		uppertextblock: [{
			textclass: "title",
			textdata: data.string.lesson_title
		}],
		storytextblockadditionalclass: "  ltb_top_left_40",
		storytextblock:[{
			textclass: "textblack",
			textdata: data.string.para1_stanza2
		} ],
		imageblock:[{
			imagestoshow:[{
				imgclass: "background-img",
				imgsrc: "",
				imgid : 'bg02',
			},{
				imgclass: "talking",
				imgsrc: "",
				imgid : 'talking02',
			}]
		},{
  			imgcontainerdiv: "dialogcontainer_left",
	  		imagestoshow:[{
	  			imgclass: "dialogimage2",
	  			imgsrc : "",
	  			imgid: "leg"
	  			
	  		}]
	  	},{
	  		imgcontainerdiv: "dialogcontainer_right",
	  		imagestoshow:[{
	  			imgclass: "dialogimage2_b",
	  			imgsrc : "",
	  			imgid: "horse"
	  		}]
	  	},{
  			imgcontainerdiv: "dialogcontainer_left2",
	  		imagelabels:[{
	  			imagelabelclass: "imagelabel",
	  			imagelabeldata: data.string.p1_s2_1_a
	  		}]
	  	},{
	  		imgcontainerdiv: "dialogcontainer_right2",
	  		imagelabels:[{
	  			imagelabelclass: "imagelabel",
	  			imagelabeldata: data.string.p1_s2_1_b
	  		}]
	  	},{
  			imgcontainerdiv: "dialogcontainer_left2",
	  		imagelabels:[{
	  			imagelabelclass: "imagelabel",
	  			imagelabeldata: data.string.p1_s2_2_a
	  		}]
	  	},{
	  		imgcontainerdiv: "dialogcontainer_right2",
	  		imagelabels:[{
	  			imagelabelclass: "imagelabel",
	  			imagelabeldata: data.string.p1_s2_2_b
	  		}]
	  	}]

	},{
		// slide3
		contentblockadditionalclass: "additionalbackground1",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb",
		uppertextblock: [{
			textclass: "title",
			textdata: data.string.lesson_title
		}],
		storytextblockadditionalclass: "  ltb_top_left_40",
		storytextblock:[{
			textclass: "textblack",
			textdata: data.string.para2_stanza1
		} ],
		imageblock:[{
			imagestoshow:[{
				imgclass: "background-img",
				imgsrc: "",
				imgid : 'bg02',
			},{
				imgclass: "talking",
				imgsrc: "",
				imgid : 'talking02',
			}]
		},{
  			imgcontainerdiv: "dialogcontainer_left2",
	  		imagelabels:[{
	  			imagelabelclass: "imagelabel",
	  			imagelabeldata: data.string.p1_s3_1_a
	  		}]
	  	},{
	  		imgcontainerdiv: "dialogcontainer_right2",
	  		imagelabels:[{
	  			imagelabelclass: "imagelabel",
	  			imagelabeldata: data.string.p1_s3_1_b
	  		}]
	  	},{
  			imgcontainerdiv: "dialogcontainer_left2",
	  		imagelabels:[{
	  			imagelabelclass: "imagelabel",
	  			imagelabeldata: data.string.p1_s3_2_a
	  		}]
	  	},{
	  		imgcontainerdiv: "dialogcontainer_right2",
	  		imagelabels:[{
	  			imagelabelclass: "imagelabel",
	  			imagelabeldata: data.string.p1_s3_2_b
	  		}]
	  	},{
  			imgcontainerdiv: "dialogcontainer_left",
	  		imagestoshow:[{
	  			imgclass: "dialogimage2",
	  			imgsrc : "",
	  			imgid: "sing"
	  			
	  		}]
	  	},{
	  		imgcontainerdiv: "dialogcontainer_right",
	  		imagestoshow:[{
	  			imgclass: "dialogimage2_b",
	  			imgsrc : "",
	  			imgid: "village"
	  		}]
	  	}]

	},{
		// slide4
		contentblockadditionalclass: "additionalbackground1",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb",
		uppertextblock: [{
			textclass: "title",
			textdata: data.string.lesson_title
		}],
		storytextblockadditionalclass: "  ltb_top_left_40",
		storytextblock:[{
			textclass: "textblack",
			textdata: data.string.para2_stanza2
		} ],
		imageblock:[{
			imagestoshow:[{
				imgclass: "background-img",
				imgsrc: "",
				imgid : 'bg02',
			},{
				imgclass: "talking",
				imgsrc: "",
				imgid : 'talking02',
			}]
		},{
  			imgcontainerdiv: "dialogcontainer_left2",
	  		imagelabels: [{
	  			imagelabelclass: "dialogcontent2",
	  			imagelabeldata: data.string.p1_s4_1_a
	  		}]
	  	},{
	  		imgcontainerdiv: "dialogcontainer_right2",
	  		imagelabels: [{
	  			imagelabelclass: "dialogcontent2",
	  			imagelabeldata: data.string.p1_s4_1_b
	  		}]
	  	},{
  			imgcontainerdiv: "dialogcontainer_left2",
	  		imagelabels: [{
	  			imagelabelclass: "dialogcontent2",
	  			imagelabeldata: data.string.p1_s4_2_a
	  		}]
	  	},{
	  		imgcontainerdiv: "dialogcontainer_right2",
	  		imagelabels: [{
	  			imagelabelclass: "dialogcontent2",
	  			imagelabeldata: data.string.p1_s4_2_b
	  		}]
	  	}]

	},{
		// slide5
		contentblockadditionalclass: "additionalbackground1",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb",
		uppertextblock: [{
			textclass: "title",
			textdata: data.string.lesson_title
		}],
		storytextblockadditionalclass: "  ltb_top_left_40",
		storytextblock:[{
			textclass: "textblack",
			textdata: data.string.para3_stanza1
		} ],
		imageblock:[{
			imagestoshow:[{
				imgclass: "background-img",
				imgsrc: "",
				imgid : 'bg02',
			},{
				imgclass: "talking",
				imgsrc: "",
				imgid : 'talking02',
			},{
				imgclass: "talking2",
				imgsrc: "",
				imgid : 'talking03',
			}]
		}]

	},{
		// slide6
		contentblockadditionalclass: "additionalbackground1",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb",
		uppertextblock: [{
			textclass: "title",
			textdata: data.string.lesson_title
		}],
		storytextblockadditionalclass: "  ltb_top_left_40",
		storytextblock:[{
			textclass: "textblack",
			textdata: data.string.para3_stanza2
		} ],
		imageblock:[{
			imagestoshow:[{
				imgclass: "background-img",
				imgsrc: "",
				imgid : 'bg02',
			},{
				imgclass: "talking",
				imgsrc: "",
				imgid : 'talking03',
			},{
				imgclass: "talking2",
				imgsrc: "",
				imgid : 'talking01',
			}]
		}]

	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var isFirefox = typeof InstallTrigger !== 'undefined';
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	
	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;
	var sound_l_0;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bg02", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "horse", src: imgpath+"horse.png", type: createjs.AbstractLoader.IMAGE},
			{id: "leg", src: imgpath+"leg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "paathi", src: imgpath+"paathi.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pathi", src: imgpath+"pathi.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sing", src: imgpath+"sing.png", type: createjs.AbstractLoader.IMAGE},
			{id: "village", src: imgpath+"village.png", type: createjs.AbstractLoader.IMAGE},
			{id: "talking01", src: imgpath+"shaking-hand.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "talking02", src: imgpath+"talking02.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "talking03", src: imgpath+"talking03.gif", type: createjs.AbstractLoader.IMAGE},
			
			// sounds
			{id: "sound_start", src: soundAsset+"0.ogg"},
			{id: "sound_0", src: soundAsset+"1.ogg"},
			{id: "sound_1", src: soundAsset+"2.ogg"},
			{id: "sound_2", src: soundAsset+"3.ogg"},
			{id: "sound_3", src: soundAsset+"4.ogg"},
			{id: "sound_4", src: soundAsset+"5.ogg"},
			{id: "sound_5", src: soundAsset+"6.ogg"},
			
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		sound_l_0 = new buzz.sound((soundAsset + "0.ogg"));
		var sound_l_1 = new buzz.sound((soundAsset + "1.ogg"));
		var sound_l_2 = new buzz.sound((soundAsset + "2.ogg"));
		var sound_l_3 = new buzz.sound((soundAsset + "3.ogg"));
		var sound_l_4 = new buzz.sound((soundAsset + "4.ogg"));
		var sound_l_5 = new buzz.sound((soundAsset + "5.ogg"));
		var sound_l_6 = new buzz.sound((soundAsset + "6.ogg"));
		soundcontent = [sound_l_1, sound_l_2, sound_l_3, sound_l_4, sound_l_5, sound_l_6];
		templateCaller();
	}
	//initialize
	init();
	
/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			// $nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  var animationinprogress = false;

  var sound_data;
	var timeourcontroller;
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    vocabcontroller.findwords(countNext);
	    put_image(content, countNext);
		// splitintofractions($(".fractionblock"));
	    switch(countNext){
	    	case 0:
	    		sound_l_0.play();
	    		$nextBtn.hide(0).delay(2000).show(0);
	    		break;
	    	case 1:
	    		timeoutcontroller = setTimeout(function(){
	    			$(".dialogcontainer_left").show(0);
	    			$(".dialogcontainer_right").delay(3500).show(0);
	    		}, 5700);
				for_all_slides('textblack', soundcontent[0], false);
	    		break;
	    	case 2:
	   			 vocabcontroller.findwords(countNext);
	    		var $dialogcontainer_left2 = $(".dialogcontainer_left2");
	    		var $dialogcontainer_right2 = $(".dialogcontainer_right2");
	    		timeoutcontroller = setTimeout(function(){
	    			$(".dialogcontainer_left").show(0);
	    			$(".dialogcontainer_right").show(0);
	    			timeourcontroller = setTimeout(function(){
	    				$(".dialogcontainer_left").hide(0);
	    				$(".dialogcontainer_right").hide(0);
	    				$($dialogcontainer_left2[0]).show(0);
	    				$($dialogcontainer_right2[0]).show(0);
	    				timeourcontroller = setTimeout(function(){
		    				$($dialogcontainer_left2[0]).hide(0);
		    				$($dialogcontainer_right2[0]).hide(0);
		    				$($dialogcontainer_left2[1]).delay(100).show(0);
		    				$($dialogcontainer_right2[1]).delay(100).show(0);
		    			},2000);
	    			},2000);
	    		}, 2000);
	    		
	    		
				for_all_slides('textblack', soundcontent[1], false);
	    		break;
	    	case 3:
	    		var $dialogcontainer_left2 = $(".dialogcontainer_left2");
	    		var $dialogcontainer_right2 = $(".dialogcontainer_right2");
	    			$($dialogcontainer_left2[0]).show(0);
    				$($dialogcontainer_right2[0]).show(0);
	    			timeourcontroller = setTimeout(function(){
	    				$($dialogcontainer_left2[0]).hide(0);
	    				$($dialogcontainer_right2[0]).hide(0);
	    				$($dialogcontainer_left2[1]).delay(100).show(0);
	    				$($dialogcontainer_right2[1]).delay(100).show(0);
	    				timeourcontroller = setTimeout(function(){
		    				$($dialogcontainer_left2[1]).hide(0);
		    				$($dialogcontainer_right2[1]).hide(0);
		    				$(".dialogcontainer_left").show(0);
	    					$(".dialogcontainer_right").show(0);
		    			},3000);
	    			},3200);
				for_all_slides('textblack', soundcontent[2], false);
	    		break;
	    	case 4:
	    		var $dialogcontainer_left2 = $(".dialogcontainer_left2");
	    		var $dialogcontainer_right2 = $(".dialogcontainer_right2");
    			$($dialogcontainer_left2[0]).show(0);
				$($dialogcontainer_right2[0]).show(0);
    			timeourcontroller = setTimeout(function(){
    				$($dialogcontainer_left2[0]).hide(0);
    				$($dialogcontainer_right2[0]).hide(0);
    				$($dialogcontainer_left2[1]).delay(100).show(0);
    				$($dialogcontainer_right2[1]).delay(100).show(0);
    			},3800);
				for_all_slides('textblack', soundcontent[3], false);
	    		break;
	    	case 5:
				for_all_slides('textblack', soundcontent[4], false);
				timeoutcontroller = setTimeout(function(){
					$(".talking").hide(0);
					$(".talking2").show(0);
				}, 4500);
	    		break;
	    	case 6:
				for_all_slides('textblack', soundcontent[5], true);
				timeoutcontroller = setTimeout(function(){
					$(".talking").hide(0);
					$(".talking2").show(0);
				}, 3000);
	    		break;
	    	default:
	    		break;
	    }
	    
	    
	    
function for_all_slides(text_class, my_sound_data, last_page_flag){
			var $textblack = $("."+text_class);
			sound_data =  my_sound_data;
			var current_text = $textblack.html();
			// current_text.replace(/<.*>/, '');
			play_text($textblack, current_text);
    		sound_data.bindOnce('ended', function(){
    			change_slides(last_page_flag, text_class);
			});
			$("."+text_class).click(function(){
				sound_data.play();
				$("."+text_class).css('pointer-events','none');
				$(".speaker").css('pointer-events','none');
				$('#span_speec_text').addClass('is_playing');
    			sound_data.bindOnce('ended', function(){
	    			$prevBtn.show(0);
    				if(countNext < ($total_page-1)){
	    				$nextBtn.show(0);
		    		} else{
		    			ole.footerNotificationHandler.pageEndSetNotification();
		    		}
					$("."+text_class).css('pointer-events','all');
					$(".speaker").css('pointer-events','all');
					$('#span_speec_text').removeClass('is_playing');
				});
			});
		}
		function change_slides(last_page_flag, text_class){
			var checking_interval = setInterval(function(){
				if(textanimatecomplete){
					if(!last_page_flag){
	    				$nextBtn.show(0);
		    		} else{
		    			ole.footerNotificationHandler.pageEndSetNotification();
		    		}
	    			$prevBtn.show(0);
					$("."+text_class).css('pointer-events','all');
					$(".speaker").css('pointer-events','all');
       				vocabcontroller.findwords(countNext);
					textanimatecomplete = false;
	    			clearInterval(checking_interval);
				} else{
					$("."+text_class).css('pointer-events','none');
					$(".speaker").css('pointer-events','none');
					$prevBtn.hide(0);
					$nextBtn.hide(0);
				}
			},50);
		}
	}
	/* For typing animation appends the text to an element specified by target class or id */
	function show_text($this,  $span_speec_text, message, interval) {
		if (0 < message.length && !textanimatecomplete) {
			var nextText = message.substring(0, 1);
			var additionalinterval = 0;
			if (nextText == "<") {
				additionalinterval = 800;
				$span_speec_text.append("<br>");
				message = message.substring(4, message.length);
			} else {
				$span_speec_text.append(nextText);
				message = message.substring(1, message.length);
			}
			$this.html($span_speec_text);
			$this.append(message);
			setTimeout(function() {
				show_text($this, $span_speec_text, message, interval);
			}, (interval + additionalinterval));
		} else{
	  		textanimatecomplete = true;
	  	}
	}

	var intervalid;
	var soundplaycomplete = false;
	var textanimatecomplete = false;
	var original_text;
	// uses the show text to add typing effect with sound and glowing animations
	function play_text($this, text){
		original_text =  text;
		if(isFirefox){
			$this.html("<span id='span_speec_text'></span>"+text);
			$prevBtn.hide(0);
			var $span_speec_text = $("#span_speec_text");
			// $this.css("background-color", "#faf");
			show_text($this, $span_speec_text,text, 125);	// 65 ms is the interval found out by hit and trial
			sound_data.play();
			sound_data.bind('ended', function(){
				sound_data.unbind('ended');
				soundplaycomplete = true;
				ternimatesound_play_animate(text);
			});
		} else {
			$this.html("<span id='span_speec_text'>"+original_text+"</span>");
			sound_data.play();
			sound_data.bind('ended', function(){
				sound_data.unbind('ended');
				soundplaycomplete = true;
				textanimatecomplete =  true;
				animationinprogress = false;
			});
		}

		function ternimatesound_play_animate(text){
			 intervalid = setInterval(function () {
				if(textanimatecomplete && soundplaycomplete){
					$this.html($span_speec_text.html(original_text));
					$this.css("background-color", "transparent");
					clearInterval(intervalid);
					intervalid = null;
					animationinprogress = false;
				}
			}, 250);
		}
	}


/*=====  End of Templates Block  ======*/

function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			for(var j = 0; j < content[count].imageblock.length; j++){
				var imageblock = content[count].imageblock[j];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}					
				}
			}
		}
	}

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  // templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
		if(sound_data != null){
			sound_data.stop();
			sound_data.unbind('ended');
		}
		clearTimeout( timeourcontroller);
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		if(sound_data != null){
			sound_data.stop();
			sound_data.unbind('ended');
		}
		clearTimeout( timeourcontroller);
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

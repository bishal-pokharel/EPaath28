var imgpath = $ref+"/images/exe/";
var soundAsset = $ref+"/sounds/ex/";

var content=[
//ex 1
	{
		extratxtdiv:[
			{
				textclass: "toptext",
				textdata: data.string.exetext1
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass:"backimg",
					imgid:"cover",
					imgsrc:''
				},
				{
					imgclass:"boatcover",
					imgid:"boat",
					imgsrc:''
				}
			]
		}]

	},
//ex 2
	{
		contentblockadditionalclass: "purpback",
		extratxtdiv:[
			{
				textclass: "toptext",
				textdata: data.string.exetext2
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass:"road",
					imgid:"road",
					imgsrc:''
				},
				{
					imgclass:"roundbtn-1 selectedimg",
					imgid:"round-1",
					imgsrc:''
				},
				{
					imgclass:"roundbtn-2",
					imgid:"round-2",
					imgsrc:''
				},
				{
					imgclass:"roundbtn-3",
					imgid:"round-3",
					imgsrc:''
				},
				{
					imgclass:"roundbtn-4",
					imgid:"round-4",
					imgsrc:''
				},
				{
					imgclass:"roundbtn-5",
					imgid:"round-5",
					imgsrc:''
				}
			]
		}]
	},
//ex 3
	{
		speechbox:[{
			speechbox: 'sp-1 thisflip',
			textclass: "answer",
			textdata: data.string.exetext3,
			imgclass: 'flipped',
			imgid : 'tb-1',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"backimg",
				imgid:"newbg1",
				imgsrc:''
			},
			{
				imgclass:"boatnew1",
				imgid:"boat",
				imgsrc:''
			}]
		}]

	},
//ex 4
	{
		contentblockadditionalclass: " orgback",
		extratxtdiv:[
			{
				textclass: "toptext orgone",
				textdata: data.string.exetext4
			},
			{
				textclass: "exebtn-1 buttonsel forhover class1",
				textdata: data.string.exetext5
			},
			{
				textclass: "exebtn-2 buttonsel forhover",
				textdata: data.string.exetext6
			},
			{
				textclass: "exebtn-3 buttonsel forhover",
				textdata: data.string.exetext7
			}
		],
		imageblock:[{
			imagestoshow:[{
				imgclass:"quesimg-1",
				imgid:"cover",
				imgsrc:''
			}]
		}]

	},
//ex 5
	{
		contentblockadditionalclass: " orgback",
		extratxtdiv:[
			{
				textclass: "toptext orgone",
				textdata: data.string.exetext8
			},
			{
				textclass: "exebtn-1 buttonsel forhover class1",
				textdata: data.string.exetext9
			},
			{
				textclass: "exebtn-2 buttonsel forhover",
				textdata: data.string.exetext10
			}
		],
		imageblock:[{
			imagestoshow:[{
				imgclass:"quesimg-1",
				imgid:"cover",
				imgsrc:''
			},
			{
				imgclass:"girlbox",
				imgid:"girlbox",
				imgsrc:''
			}]
		}],
		speechbox:[{
			speechbox: 'sp-2 thisflip',
			textclass: "answer",
			textdata: data.string.exetext9,
			imgclass: 'flipped',
			imgid : 'tb-1',
			imgsrc: '',
			// audioicon: true,
		}]

	},
//ex 6
	{
		contentblockadditionalclass: "purpback",
		extratxtdiv:[
			{
				textclass: "toptext",
				textdata: data.string.exetext2
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass:"road",
					imgid:"road",
					imgsrc:''
				},
				{
					imgclass:"roundbtn-1",
					imgid:"round-1",
					imgsrc:''
				},
				{
					imgclass:"roundbtn-2 selectedimg",
					imgid:"round-2",
					imgsrc:''
				},
				{
					imgclass:"roundbtn-3",
					imgid:"round-3",
					imgsrc:''
				},
				{
					imgclass:"roundbtn-4",
					imgid:"round-4",
					imgsrc:''
				},
				{
					imgclass:"roundbtn-5",
					imgid:"round-5",
					imgsrc:''
				}
			]
		}]
	},
//ex 7
	{
		speechbox:[{
			speechbox: 'sp-3 thisflip',
			textclass: "answer",
			textdata: data.string.exetext11,
			imgclass: 'flipped',
			imgid : 'tb-1',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"backimg",
				imgid:"newbg2",
				imgsrc:''
			},
			{
				imgclass:"boatnew1",
				imgid:"boatpoint",
				imgsrc:''
			}]
		}]

	},
//ex 8
	{
		contentblockadditionalclass: " orgback",
		extratxtdiv:[
			{
				textclass: "toptext orgone",
				textdata: data.string.exetext12
			},
			{
				textclass: "exebtn-1 buttonsel forhover",
				textdata: data.string.exetext13
			},
			{
				textclass: "exebtn-2 buttonsel forhover",
				textdata: data.string.exetext14
			},
			{
				textclass: "exebtn-3 buttonsel forhover class1",
				textdata: data.string.exetext15
			}
		],
		imageblock:[{
			imagestoshow:[{
				imgclass:"quesimg-2",
				imgid:"peacock",
				imgsrc:''
			}]
		}]

	},
//ex 9
	{
		contentblockadditionalclass: " orgback",
		extratxtdiv:[
			{
				textclass: "toptext orgone",
				textdata: data.string.exetext16
			},
			{
				textclass: "twobtn-1 buttonsel forhover class1",
				textdata: data.string.exetext17
			},
			{
				textclass: "twobtn-2 buttonsel forhover",
				textdata: data.string.exetext18
			}
		],
		imageblock:[{
			imagestoshow:[{
				imgclass:"twoimg-1",
				imgid:"peacock1",
				imgsrc:''
			},{
				imgclass:"twoimg-2",
				imgid:"peacock2",
				imgsrc:''
			}]
		}]

	},
//ex 10
	{
		contentblockadditionalclass: "purpback",
		extratxtdiv:[
			{
				textclass: "toptext",
				textdata: data.string.exetext2
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass:"road",
					imgid:"road",
					imgsrc:''
				},
				{
					imgclass:"roundbtn-1",
					imgid:"round-1",
					imgsrc:''
				},
				{
					imgclass:"roundbtn-2",
					imgid:"round-2",
					imgsrc:''
				},
				{
					imgclass:"roundbtn-3 selectedimg",
					imgid:"round-3",
					imgsrc:''
				},
				{
					imgclass:"roundbtn-4",
					imgid:"round-4",
					imgsrc:''
				},
				{
					imgclass:"roundbtn-5",
					imgid:"round-5",
					imgsrc:''
				}
			]
		}]
	},
//ex 11
	{
		contentblockadditionalclass: " orgback",
		extratxtdiv:[
			{
				textclass: "toptext orgone",
				textdata: data.string.exetext19
			},
			{
				textclass: "questiontext",
				textdata: data.string.exetext23
			},
			{
				textclass: "exebtn-1 buttonsel forhover",
				textdata: data.string.exetext20
			},
			{
				textclass: "exebtn-2 buttonsel forhover class1",
				textdata: data.string.exetext21
			},
			{
				textclass: "exebtn-3 buttonsel forhover",
				textdata: data.string.exetext22
			}
		],
		imageblock:[{
			imagestoshow:[{
				imgclass:"quesimg-1",
				imgid:"elephant",
				imgsrc:''
			}]
		}]

	},
//ex 12
	{
		contentblockadditionalclass: " orgback",
		extratxtdiv:[
			{
				textclass: "toptext orgone",
				textdata: data.string.exetext19
			},
			{
				textclass: "questiontext",
				textdata: data.string.exetext26
			},
			{
				textclass: "exebtn-1 buttonsel forhover",
				textdata: data.string.exetext20
			},
			{
				textclass: "exebtn-2 buttonsel forhover class1",
				textdata: data.string.exetext24
			},
			{
				textclass: "exebtn-3 buttonsel forhover",
				textdata: data.string.exetext25
			}
		],
		imageblock:[{
			imagestoshow:[{
				imgclass:"quesimg-1",
				imgid:"elephant-gif",
				imgsrc:''
			}]
		}]

	},
//ex 13
	{
		contentblockadditionalclass: "purpback",
		extratxtdiv:[
			{
				textclass: "toptext",
				textdata: data.string.exetext2
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass:"road",
					imgid:"road",
					imgsrc:''
				},
				{
					imgclass:"roundbtn-1",
					imgid:"round-1",
					imgsrc:''
				},
				{
					imgclass:"roundbtn-2",
					imgid:"round-2",
					imgsrc:''
				},
				{
					imgclass:"roundbtn-3",
					imgid:"round-3",
					imgsrc:''
				},
				{
					imgclass:"roundbtn-4 selectedimg",
					imgid:"round-4",
					imgsrc:''
				},
				{
					imgclass:"roundbtn-5",
					imgid:"round-5",
					imgsrc:''
				}
			]
		}]
	},
//ex 14
	{
		speechbox:[{
			speechbox: 'sp-4 thisflip',
			textclass: "answer",
			textdata: data.string.exetext27,
			imgclass: '',
			imgid : 'tb-1',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"backimg",
				imgid:"newbg3",
				imgsrc:''
			},
			{
				imgclass:"boatnew2",
				imgid:"boat",
				imgsrc:''
			}]
		}]

	},
//ex 15
	{
		contentblockadditionalclass: " orgback",
		extratxtdiv:[
			{
				textclass: "toptext orgone",
				textdata: data.string.exetext280
			},
			{
				textclass: "twobtn-1 buttonsel forhover class1",
				textdata: data.string.exetext28
			},
			{
				textclass: "twobtn-2 buttonsel forhover",
				textdata: data.string.exetext29
			}
		],
		imageblock:[{
			imagestoshow:[{
				imgclass:"twoimg-1",
				imgid:"rhino1",
				imgsrc:''
			},{
				imgclass:"twoimg-2",
				imgid:"rhino2",
				imgsrc:''
			}]
		}]

	},
//ex 16
	{
		contentblockadditionalclass: " orgback",
		extratxtdiv:[
			{
				textclass: "toptext orgone",
				textdata: data.string.exetext30
			},
			{
				textclass: "exebtn-1 buttonsel forhover class1",
				textdata: data.string.exetext31
			},
			{
				textclass: "exebtn-2 buttonsel forhover",
				textdata: data.string.exetext32
			},
			{
				textclass: "exebtn-3 buttonsel forhover",
				textdata: data.string.exetext33
			}
		],
		imageblock:[{
			imagestoshow:[{
				imgclass:"quesimg-2",
				imgid:"rhino",
				imgsrc:''
			}]
		}]

	},
//ex 17
	{
		contentblockadditionalclass: "purpback",
		extratxtdiv:[
			{
				textclass: "toptext",
				textdata: data.string.exetext2
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass:"road",
					imgid:"road",
					imgsrc:''
				},
				{
					imgclass:"roundbtn-1",
					imgid:"round-1",
					imgsrc:''
				},
				{
					imgclass:"roundbtn-2",
					imgid:"round-2",
					imgsrc:''
				},
				{
					imgclass:"roundbtn-3",
					imgid:"round-3",
					imgsrc:''
				},
				{
					imgclass:"roundbtn-4",
					imgid:"round-4",
					imgsrc:''
				},
				{
					imgclass:"roundbtn-5 selectedimg",
					imgid:"round-5",
					imgsrc:''
				}
			]
		}]
	},
//ex 18
	{
		speechbox:[{
			speechbox: 'sp-4 thisflip',
			textclass: "answer",
			textdata: data.string.exetext34,
			imgclass: '',
			imgid : 'tb-1',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"backimg",
				imgid:"newbg4",
				imgsrc:''
			},
			{
				imgclass:"boatnew2",
				imgid:"boat",
				imgsrc:''
			}]
		}]

	},
//ex 19
	{
		contentblockadditionalclass: " orgback",
		extratxtdiv:[
			{
				textclass: "toptext orgone",
				textdata: data.string.exetext35
			},
			{
				textclass: "twobtn-1 buttonsel forhover class1",
				textdata: data.string.exetext40
			},
			{
				textclass: "twobtn-2 buttonsel forhover",
				textdata: data.string.exetext41
			}
		],
		imageblock:[{
			imagestoshow:[{
				imgclass:"twoimg-1",
				imgid:"animcoll1",
				imgsrc:''
			},{
				imgclass:"twoimg-2",
				imgid:"animcoll2",
				imgsrc:''
			}]
		}]

	},
//ex 20
	{
		contentblockadditionalclass: " orgback",
		extratxtdiv:[
			{
				textclass: "toptext orgone",
				textdata: data.string.exetext36
			},
			{
				textclass: "exebtn-1 buttonsel forhover class1",
				textdata: data.string.exetext37
			},
			{
				textclass: "exebtn-2 buttonsel forhover",
				textdata: data.string.exetext38
			},
			{
				textclass: "exebtn-3 buttonsel forhover",
				textdata: data.string.exetext39
			}
		],
		imageblock:[{
			imagestoshow:[{
				imgclass:"quesimg-2",
				imgid:"mamaghar",
				imgsrc:''
			}]
		}]

	}
];

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var item_txt ='';
	var countNext = 0;
	var questionFlag;

	var boxNum = 0;
	var qnArray=[2,3,4,5];
	var marksFlag = true;

	/*for limiting the questions to 10*/
	var $total_page = content.length;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "cover", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boat", src: imgpath+"boat.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "boatpoint", src: imgpath+"girl_pointing.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "newbg1", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "newbg2", src: imgpath+"bg_for_pecock.png", type: createjs.AbstractLoader.IMAGE},
			{id: "newbg3", src: imgpath+"bg_for_rhino.png", type: createjs.AbstractLoader.IMAGE},
			{id: "newbg4", src: imgpath+"bg_mama_ghar.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girlbox", src: imgpath+"boat.png", type: createjs.AbstractLoader.IMAGE},
			{id: "peacock", src: imgpath+"pecock01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "peacock1", src: imgpath+"pecock02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "peacock2", src: imgpath+"pecock03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "elephant", src: imgpath+"elephant01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "elephant-gif", src: imgpath+"elephant_walk.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "rhino1", src: imgpath+"rhino01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rhino2", src: imgpath+"rhino02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rhino", src: imgpath+"rhino03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "animcoll1", src: imgpath+"animals01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "animcoll2", src: imgpath+"animals02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mamaghar", src: imgpath+"mama_ghar.png", type: createjs.AbstractLoader.IMAGE},
			{id: "road", src: imgpath+"road01.png", type: createjs.AbstractLoader.IMAGE},

			{id: "round-1", src: imgpath+"river.png", type: createjs.AbstractLoader.IMAGE},
			{id: "round-2", src: imgpath+"pecock.png", type: createjs.AbstractLoader.IMAGE},
			{id: "round-3", src: imgpath+"elephant.png", type: createjs.AbstractLoader.IMAGE},
			{id: "round-4", src: imgpath+"rhino.png", type: createjs.AbstractLoader.IMAGE},
			{id: "round-5", src: imgpath+"hosue.png", type: createjs.AbstractLoader.IMAGE},

			//textboxes
			{id: "tb-1", src: 'images/textbox/white/tl-2.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_1", src: soundAsset+"ex_1.ogg"},
			{id: "sound_2", src: soundAsset+"ex_2.ogg"},
			{id: "sound_3", src: soundAsset+"ex_3.ogg"},
			{id: "sound_4", src: soundAsset+"ex_4.ogg"},
			{id: "sound_5", src: soundAsset+"ex_5.ogg"},
			{id: "sound_7", src: soundAsset+"ex_7.ogg"},
			{id: "sound_8", src: soundAsset+"ex_8.ogg"},
			{id: "sound_9", src: soundAsset+"ex_9.ogg"},
			{id: "sound_11", src: soundAsset+"ex_11.ogg"},
			{id: "sound_14", src: soundAsset+"ex_14.ogg"},
			{id: "sound_15", src: soundAsset+"ex_15.ogg"},
			{id: "sound_16", src: soundAsset+"ex_16.ogg"},
			{id: "sound_18", src: soundAsset+"ex_18.ogg"},
			{id: "sound_19", src: soundAsset+"ex_19.ogg"},
			{id: "sound_20", src: soundAsset+"ex_20.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		//scoring.init(10);
		templateCaller();
	}
	//initialize
	init();

	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
 	}

	 /*values in this array is same as the name of images of eggs in image folder*/

	 //create eggs
	 var testin = new NumberTemplate();

	 	//eggTemplate.eggMove(countNext);
 		testin.init(10);

	 function generalTemplate() {
		 questionFlag = false;
		 loadTimelineProgress($total_page, countNext + 1);
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		var updateScore = 0;
		// put_image_sec(content, countNext);
	 	var testcount = 0;

	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);

	 	/*generate question no at the beginning of question*/
	 	testin.numberOfQuestions();

		$(".selectedimg").click(function(){
			$nextBtn.trigger("click");
		});

	 	switch(countNext){
			case 0:
				// $nextBtn.show(0);
				sound_player("sound_"+(countNext+1), 1);
			break;
			case 2:
			case 6:
			case 13:
			case 17:
			sound_player("sound_"+(countNext+1), 1);
				// $nextBtn.show(0);
			break;
			case 3:
			case 7:
			case 8:
			case 14:
			case 15:
			case 18:
			case 19:
				sound_player("sound_"+(countNext+1), 0);
			break;
			case 1:
			case 5:
			case 9:
			case 12:
			case 16:
				sound_player("sound_2", 0);
			break;
			case 4:
				sound_player("sound_"+(countNext+1), 0);
			$(".exebtn-1").click(function(){
				createjs.Sound.stop();
				$(".answer").fadeIn(1000);
			});
			break;
			case 6:
				// sound_player("sound_4", 1);
				$nextBtn.show(0);
			break;
			case 10:
				sound_player("sound_"+(countNext+1), 0);
			$(".class1").click(function(){
				createjs.Sound.stop();
				var thistext = $(this).text();
				$(".ansfiller").text(thistext);
			});
			break;
			case 11:
				sound_player("sound_11", 0);
			$('.quesimg-1').css({"width":"43%","left":"15%"});
			$(".class1").click(function(){
				createjs.Sound.stop();
				var thistext = $(this).text();
				$(".ansfiller").text(thistext);
			});
			break;
			case 13:
				// sound_player("sound_4", 1);
				$nextBtn.show(0);
			break;
			case 17:
				// sound_player("sound_4", 1);
				$nextBtn.show(0);
			break;
	 	}

		function sound_player(sound_id, next){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
			current_sound.on('complete', function(){
				if(next)
				$nextBtn.show(0);
			});
		}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					// console.log(selector);
					$(selector).attr('src',image_src);
				}
			}
		}
	}


	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}

		var ansClicked = false;
		var wrngClicked = false;
$(".buttonsel").click(function(){
	createjs.Sound.stop();
	$(this).removeClass('forhover');
		if(ansClicked == false){

			var $this = $(this);
			var position = $this.position();
			console.log(position);
			var width = $this.outerWidth();
			var height = $this.outerHeight();
			var centerX = ((position.left + width)*100)/$('.contentblock').width()-4+'%';
			var centerY = ((position.top + height/4)*100)/$('.contentblock').height()+'%';


			if($(this).hasClass("class1")){
				var cortext = $(this).text().substring(3);
				$(".blankspace").text(cortext).css("color","green");

				if(wrngClicked == false){
					testin.update(true);
				}
				play_correct_incorrect_sound(1);
				$(this).css("background","#bed62fff");
				$(this).css("border","5px solid #deef3c");
				$(this).css("color","white");
				$(this).siblings(".corctopt").show(0);
				//$('.hint_image').show(0);
				$('.buttonsel').removeClass('forhover forhoverimg');
				ansClicked = true;
				questionFlag = true;
				if(countNext != $total_page)
				$nextBtn.show(0);


				$(this).css({'background':'#98C02E','border-radius':'2vmin','border':'.5vmin solid #DEEF3C'});
				$('.options').css('pointer-events','none');
				$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;height:5%;z-index:2;" src="images/correct.png" />').insertAfter(this);
			}
			else{
				testin.update(false);
				play_correct_incorrect_sound(0);
				$(this).css("background","#FF0000");
				$(this).css("border","5px solid #980000");
				$(this).css("color","white");
				$(this).siblings(".wrngopt").show(0);
				wrngClicked = true;
				$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;height:5%;z-index:2;" src="images/wrong.png" />').insertAfter(this);
			}
		}
	});
	 	/*======= SCOREBOARD SECTION ==============*/
	 }

	 function templateCaller(){
		/*always hide next and previousloadimage navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators


	}

	// first call to template caller
	// templateCaller(); loadimage($(".myimg3").find("img"),$(".myimg3").find("p"),preload.getResult('ex1typ3qn'+quesNo).src,eval('data.string.ex2typ1op'+quesNo));


	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		createjs.Sound.stop();
		countNext++;
		if(questionFlag == true)
		testin.gotoNext();
		templateCaller();
	});

	// $refreshBtn.click(function(){
	// 	templateCaller();
	// });
 	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	function loadimage(imgrep,imgtext,imgsrc,quesNo){
		imgrep.attr("src",imgsrc);
		imgtext.text(quesNo);
	}

/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var maincontent = [
  {
    contentnocenteradjust: true,
    extratextblock:[{
      textclass:'toptextfirst',
      textdata: data.string.p1s6
    },
    {
      textclass:'optiontext1',
      textdata: data.string.p1s7
    },
    {
      textclass:'optiontext2',
      textdata: data.string.p1s8
    },
    {
      textclass:'optiontext3',
      textdata: data.string.p1s9
    },
    {
      textclass:'optiontext4',
      textdata: data.string.p1s10
    },
    {
      textclass:'optiontext5',
      textdata: data.string.p1s11
    }
  ],
  imageblock: [{
    imagestoshow: [
      {
        imgclass: "background1",
        imgid: 'bg01',
        imgsrc: ""
      },
      {
        imgclass: "options nutrition",
        imgid: 'googlemap',
        imgsrc: ""
      },
      {
        imgclass: "options respiration",
        imgid: 'img11',
        imgsrc: "",
      },
      {
        imgclass: "options internaltransport",
        imgid: 'img03',
        imgsrc: "",
      },
      {
        imgclass: "options excretion",
        imgid: 'img13',
        imgsrc: "",
      },
      {
        imgclass: "options reproduction",
        imgid: 'img05',
        imgsrc: "",
      }
    ]
  }],
},
]

var contentnutri = [
  //slide1
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "nutr zoomin",
    extratextblock:[{
      textclass:'toptext',
      textdata: data.string.p1s12
    },
    {
      textclass:'place1',
      textdata: data.string.dunai
    },
    {
      textclass:'place2',
      textdata: data.string.dho
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage1",
          imgid: 'googlemap',
          imgsrc: ""
        },
        {
          imgclass: "line",
          imgid: 'googlemapline',
          imgsrc: ""
        }
      ]
    }]
  },
  //slide2
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "nutr",
    extratextblock:[{
      textclass:'toptext',
      textdata: data.string.p1s12
    },
    {
      textclass:'bottext',
      textdata: data.string.p1s13
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage",
          imgid: 'byair01',
          imgsrc: ""
        }
      ]
    }],
    svgblock:[{
      svgid: "lineSvg",
    }]
  },
  //slide3
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "nutr",
    extratextblock:[{
      textclass:'toptext',
      textdata: data.string.p1s12
    },
    {
      textclass:'bottext',
      textdata: data.string.p1s14
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage",
          imgid: 'img07',
          imgsrc: ""
        }
      ]
    }]
  },
  //slide4
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "nutr",
    extratextblock:[{
      textclass:'toptext',
      textdata: data.string.p1s12
    },
    {
      textclass:'bottext',
      textdata: data.string.p1s15
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage",
          imgid: 'img06',
          imgsrc: ""
        }
      ]
    }]
  },
  //slide5
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "nutr",
    extratextblock:[{
      textclass:'toptext',
      textdata: data.string.p1s12
    },
    {
      textclass:'bottext',
      textdata: data.string.p1s16
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage",
          imgid: 'img09',
          imgsrc: ""
        }
      ]
    }]
  },
  //slide6
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "nutr",
    extratextblock:[{
      textclass:'toptext',
      textdata: data.string.p1s12
    },
    {
      textclass:'bottext',
      textdata: data.string.p1s17
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage",
          imgid: 'img10',
          imgsrc: ""
        }
      ]
    }]
  },
  //slide7
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "nutr",
    extratextblock:[{
      textclass:'toptext',
      textdata: data.string.p1s12
    },
    {
      textclass:'cntrtxt shakeup',//bottext
      textdata: data.string.p1s18
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage1 brightness",
          imgid: 'bg01',
          imgsrc: ""
        }
      ]
    }]
  },
]

var contentresp=[
  //slide1
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "res",
    extratextblock:[{
      textclass:'toptext2',
      textdata: data.string.p1s12b
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage1",
          imgid: 'img11',
          imgsrc: ""
        },
        {
          imgclass: "line2",
          imgid: 'img11road',
          imgsrc: ""
        }
      ]
    }]
  },
  //slide2
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "res",
    extratextblock:[{
      textclass:'toptext2',
      textdata: data.string.p1s12b
    },
    {
      textclass:'bottext',
      textdata: data.string.p1s19
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage",
          imgid: 'bg01',
          imgsrc: ""
        }
      ]
    }]

  },
  //slide3
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "res",
    extratextblock:[{
      textclass:'toptext2',
      textdata: data.string.p1s12b
    },
    {
      textclass:'bottext',
      textdata: data.string.p1s20
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage",
          imgid: 'imgsnow',
          imgsrc: ""
        }
      ]
    }]
  },
  //slide4
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "res",
    extratextblock:[{
      textclass:'toptext2',
      textdata: data.string.p1s12b
    },
    {
      textclass:'bottext',
      textdata: data.string.p1s21
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "panimage slideleft",
          imgid: 'img12',
          imgsrc: ""
        },
        {
          imgclass: "midimage",
          imgid: 'bg01',
          imgsrc: ""
        }
      ]
    }]
  },
  //slide5
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "res",
    extratextblock:[{
      textclass:'toptext2',
      textdata: data.string.p1s12b
    },
    {
      textclass:'bottext',
      textdata: data.string.p1s22
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage",
          imgid: 'img14',
          imgsrc: ""
        }
      ]
    }]
  },
  //slide6
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "res",
    extratextblock:[{
      textclass:'toptext2',
      textdata: data.string.p1s12b
    },
    {
      textclass:'cntrtxt shakeup',
      textdata: data.string.p1s23
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage1 brightness",
          imgid: 'bg01',
          imgsrc: ""
        }
      ]
    }]
  },
];

var contentinttrans=[
  //slide1
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "inttrans",
    extratextblock:[{
      textclass:'toptext3',
      textdata: data.string.p1s24
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage1",
          imgid: 'img03',
          imgsrc: ""
        }
      ]
    }]
  },
  //slide2
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "inttrans",
    extratextblock:[{
      textclass:'toptext3',
      textdata: data.string.p1s24
    },
    {
      textclass:'bottext2',
      textdata: data.string.p1s25
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage",
          imgid: 'nature03',
          imgsrc: ""
        }
      ]
    }]
  },
  //slide3
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "inttrans",
    extratextblock:[{
      textclass:'toptext3',
      textdata: data.string.p1s24
    },
    {
      textclass:'bottext2',
      textdata: data.string.p1s26
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage",
          imgid: 'nature03',
          imgsrc: ""
        },
        {
          imgclass: "flower1",
          imgid: 'flower01',
          imgsrc: ""
        },
        {
          imgclass: "flower2",
          imgid: 'flower02',
          imgsrc: ""
        },
        {
          imgclass: "flower3",
          imgid: 'flower03',
          imgsrc: ""
        }
      ]
    }]
  },
  //slide4
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "inttrans",
    extratextblock:[{
      textclass:'toptext3',
      textdata: data.string.p1s24
    },
    {
      textclass:'bottext2',
      textdata: data.string.p1s27
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage",
          imgid: 'img03',
          imgsrc: ""
        }
      ]
    }]
  },
  //slide5
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "inttrans",
    extratextblock:[{
      textclass:'toptext3',
      textdata: data.string.p1s24
    },
    {
      textclass:'bottext2',
      textdata: data.string.p1s28
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage",
          imgid: 'nature07',
          imgsrc: ""
        }
      ]
    }]
  },
  //slide6
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "inttrans",
    extratextblock:[{
      textclass:'toptext3',
      textdata: data.string.p1s24
    },
    {
      textclass:'bottext2',
      textdata: data.string.p1s29
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage",
          imgid: 'leopard',
          imgsrc: ""
        }
      ]
    }]
  },
  //slide7
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "inttrans",
    extratextblock:[{
      textclass:'toptext3',
      textdata: data.string.p1s24
    },
    {
      textclass:'cntrtxt shakeup',
      textdata: data.string.p1s30
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage1 brightness",
          imgid: 'bg01',
          imgsrc: ""
        }
      ]
    }]
  },
];

var contentexcre=[
  //slide1
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "excre",
    extratextblock:[{
      textclass:'toptext4',
      textdata: data.string.p1s31
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage1",
          imgid: 'img13',
          imgsrc: ""
        }
      ]
    }]
  },
  //slide2
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "excre",
    extratextblock:[{
      textclass:'toptext4',
      textdata: data.string.p1s31
    },
    {
      textclass:'bottext',
      textdata: data.string.p1s32
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage",
          imgid: 'culture01',
          imgsrc: ""
        }
      ]
    }]
  },
  //slide3
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "excre",
    extratextblock:[{
      textclass:'toptext4',
      textdata: data.string.p1s31
    },
    {
      textclass:'bottext',
      textdata: data.string.p1s33
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage",
          imgid: 'culture07',
          imgsrc: ""
        }
      ]
    }]
  },
  //slide4
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "excre",
    extratextblock:[{
      textclass:'toptext4',
      textdata: data.string.p1s31
    },
    {
      textclass:'bottext',
      textdata: data.string.p1s34
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage",
          imgid: 'culture08',
          imgsrc: ""
        }
      ]
    }]
  },
  //slide5
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "excre",
    extratextblock:[{
      textclass:'toptext4',
      textdata: data.string.p1s31
    },
    {
      textclass:'bottext',
      textdata: data.string.p1s35
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage",
          imgid: 'img04',
          imgsrc: ""
        }
      ]
    }]
  },
  //slide6
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "excre",
    extratextblock:[{
      textclass:'toptext4',
      textdata: data.string.p1s31
    },
    {
      textclass:'bottext',
      textdata: data.string.p1s36
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage",
          imgid: 'culture03',
          imgsrc: ""
        }
      ]
    }]
  },
  //slide7
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "excre",
    extratextblock:[{
      textclass:'toptext4',
      textdata: data.string.p1s31
    },
    {
      textclass:'bottext',
      textdata: data.string.p1s37
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage",
          imgid: 'culture09',
          imgsrc: ""
        }
      ]
    }]
  },
  //slide8
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "excre",
    extratextblock:[{
      textclass:'toptext4',
      textdata: data.string.p1s31
    },
    {
      textclass:'cntrtxt shakeup',
      textdata: data.string.p1s38
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage1 brightness",
          imgid: 'bg01',
          imgsrc: ""
        }
      ]
    }]
  },
];

var contentrepro=[
  //slide1
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "repro",
    extratextblock:[{
      textclass:'toptext5',
      textdata: data.string.p1s39
    },
    {
      textclass:'bottext3',
      textdata: data.string.p1s39a
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage",
          imgid: 'img05a',
          imgsrc: ""
        }
      ]
    }]
  },
  //slide2
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "repro",
    extratextblock:[{
      textclass:'toptext5',
      textdata: data.string.p1s39
    },
    {
      textclass:'bottext3',
      textdata: data.string.p1s40
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage",
          imgid: 'img05',
          imgsrc: ""
        },
        {
          imgclass: "flower1",
          imgid: 'veg1',
          imgsrc: ""
        },
        {
          imgclass: "flower2",
          imgid: 'veg2',
          imgsrc: ""
        },
        {
          imgclass: "flower3",
          imgid: 'veg3',
          imgsrc: ""
        }
      ]
    }]
  },
  //slide3
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "repro",
    extratextblock:[{
      textclass:'toptext5',
      textdata: data.string.p1s39
    },
    {
      textclass:'bottext3',
      textdata: data.string.p1s41
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage",
          imgid: 'imgyak',
          imgsrc: ""
        }
      ]
    }]
  },
  //slide4
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "repro",
    extratextblock:[{
      textclass:'toptext5',
      textdata: data.string.p1s39
    },
    {
      textclass:'bottext3',
      textdata: data.string.p1s42
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage",
          imgid: 'pesa09',
          imgsrc: ""
        }
      ]
    }]
  },
  //slide5
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "repro",
    extratextblock:[{
      textclass:'toptext5',
      textdata: data.string.p1s39
    },
    {
      textclass:'bottext3',
      textdata: data.string.p1s43
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage",
          imgid: 'pesa07',
          imgsrc: ""
        }
      ]
    }]
  },
  //slide6
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "repro",
    extratextblock:[{
      textclass:'toptext5',
      textdata: data.string.p1s39
    },
    {
      textclass:'bottext3',
      textdata: data.string.p1s44
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage",
          imgid: 'pesa02',
          imgsrc: ""
        },
        {
          imgclass: "flower3",
          imgid: 'yasha',
          imgsrc: ""
        },
        {
          imgclass: "arrow",
          imgid: 'arrow',
          imgsrc: ""
        }
      ]
    }]
  },
  //slide7
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "repro",
    extratextblock:[{
      textclass:'toptext5',
      textdata: data.string.p1s39
    },
    {
      textclass:'bottext3',
      textdata: data.string.p1s45
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage",
          imgid: 'pesa04',
          imgsrc: ""
        }
      ]
    }]
  },
  // slide8
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "repro",
    extratextblock:[{
      textclass:'toptext5',
      textdata: data.string.p1s39
    },
    {
      textclass:'cntrtxt shakeup',
      textdata: data.string.p1s46
    }],
    imageblock: [{
      imagestoshow: [
        {
          imgclass: "midimage1 brightness",
          imgid: 'bg01',
          imgsrc: ""
        }
      ]
    }]
  },
];

$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var flager = 0;
  var countNext = 0;
  var count = 0;
  var setTime;
  var $total_page = maincontent.length;
  loadTimelineProgress($total_page, countNext + 1);

  var preload;
  var timeoutvar = null;
  var current_sound;
  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();
  var mergecontent = [];
  var visitedcontent =
  {"nutrition":false,
  "respiration":false,
  "inttrans":false,
  "excretion":false,
  "reproduction":false};

  var nutritionContent = $.merge(mergecontent,maincontent);
  nutritionContent = $.merge(mergecontent,contentnutri);
  mergecontent = [];
  var respirationContent = $.merge(mergecontent,maincontent)
  respirationContent = $.merge(mergecontent,contentresp);
  mergecontent = [];
  var inttransContent = $.merge(mergecontent,maincontent)
  inttransContent = $.merge(mergecontent,contentinttrans);
  mergecontent = [];
  var excretionContnent = $.merge(mergecontent,maincontent)
  excretionContnent = $.merge(mergecontent,contentexcre);
  mergecontent = [];
  var reproductionContent = $.merge(mergecontent,maincontent)
  reproductionContent = $.merge(mergecontent,contentrepro);

  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [

      {id: "cloudimg", src: imgpath+"ima01.png", type: createjs.AbstractLoader.IMAGE},
      {id: "googlemap", src: imgpath+"img01.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "googlemapline", src: imgpath+"img01_zoom_line.png", type: createjs.AbstractLoader.IMAGE},
      {id: "byair", src: imgpath+"by_air.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "byair01", src: imgpath+"by_air01.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "imgsnow", src: imgpath+"imgsnow.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "leopard", src: imgpath+"snow_leopard.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "img02", src: imgpath+"img02.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "img03", src: imgpath+"img03.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "nature03", src: imgpath+"natur03.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "nature07", src: imgpath+"natur07.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "img04", src: imgpath+"img04.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "img05", src: imgpath+"img05.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "img05a", src: imgpath+"img05a.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "img06", src: imgpath+"img06.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "img07", src: imgpath+"img07.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "img09", src: imgpath+"img09.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "img10", src: imgpath+"img10.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "img11", src: imgpath+"img11.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "img11road", src: imgpath+"img11_road.png", type: createjs.AbstractLoader.IMAGE},
      {id: "img12", src: imgpath+"img12.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "img13", src: imgpath+"img13.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "img14", src: imgpath+"img14.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "imgyak", src: imgpath+"imgyak.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "bg01", src: imgpath+"bg_cover_page.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "flower01", src: imgpath+"flower01.png", type: createjs.AbstractLoader.IMAGE},
      {id: "flower02", src: imgpath+"flower02.png", type: createjs.AbstractLoader.IMAGE},
      {id: "flower03", src: imgpath+"flower03.png", type: createjs.AbstractLoader.IMAGE},
      {id: "veg1", src: imgpath+"veg1.png", type: createjs.AbstractLoader.IMAGE},
      {id: "veg2", src: imgpath+"veg2.png", type: createjs.AbstractLoader.IMAGE},
      {id: "veg3", src: imgpath+"veg3.png", type: createjs.AbstractLoader.IMAGE},
      {id: "culture01", src: imgpath+"culture01.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "culture02", src: imgpath+"culture02.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "culture03", src: imgpath+"culture03.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "culture04", src: imgpath+"culture04.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "culture05", src: imgpath+"culture05.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "culture06", src: imgpath+"culture06.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "culture07", src: imgpath+"culture07.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "culture08", src: imgpath+"culture08.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "culture09", src: imgpath+"culture09.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "pesa09", src: imgpath+"pesa09.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "pesa07", src: imgpath+"pesa07.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "pesa02", src: imgpath+"pesa02.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "pesa04", src: imgpath+"pesa04.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "arrow", src: imgpath+"arrow.png", type: createjs.AbstractLoader.IMAGE},
      {id: "yasha", src: imgpath+"yastra_gumba.png", type: createjs.AbstractLoader.IMAGE},
      {id: "path", src: imgpath+"path.svg", type: createjs.AbstractLoader.IMAGE},
      // sounds
      {id: "sound_first", src: soundAsset + "s2_p1.ogg"},

      {id: "sound_0", src: soundAsset + "s2_yatra_p2.ogg"},
      {id: "sound_1", src: soundAsset + "s2_yatra_p3.ogg"},
      {id: "sound_2", src: soundAsset + "s2_yatra_p4.ogg"},
      {id: "sound_3", src: soundAsset + "s2_yatra_p5.ogg"},
      {id: "sound_4", src: soundAsset + "s2_yatra_p6.ogg"},
      {id: "sound_5", src: soundAsset + "s2_yatra_p7.ogg"},
      {id: "sound_6", src: soundAsset + "s2_yatra_p8.ogg"},

      {id: "sound_7", src: soundAsset + "s2_bhugol_p2.ogg"},
      {id: "sound_8", src: soundAsset + "s2_bhugol_p3.ogg"},
      {id: "sound_9", src: soundAsset + "s2_bhugol_p4.ogg"},
      {id: "sound_10", src: soundAsset + "s2_bhugol_p5.ogg"},
      {id: "sound_11", src: soundAsset + "s2_bhugol_p6.ogg"},
      {id: "sound_12", src: soundAsset + "s2_bhugol_p7.ogg"},

      {id: "sound_13", src: soundAsset + "s2_prakriti_p2.ogg"},
      {id: "sound_14", src: soundAsset + "s2_prakriti_p3.ogg"},
      {id: "sound_15", src: soundAsset + "s2_prakriti_p4.ogg"},
      {id: "sound_16", src: soundAsset + "s2_prakriti_p5.ogg"},
      {id: "sound_17", src: soundAsset + "s2_prakriti_p6.ogg"},
      {id: "sound_18", src: soundAsset + "s2_prakriti_p7.ogg"},
      {id: "sound_19", src: soundAsset + "s2_prakriti_p8.ogg"},

      {id: "sound_20", src: soundAsset + "s2_sanskriti_p2.ogg"},
      {id: "sound_21", src: soundAsset + "s2_sanskriti_p3.ogg"},
      {id: "sound_22", src: soundAsset + "s2_sanskriti_p4.ogg"},
      {id: "sound_23", src: soundAsset + "s2_sanskriti_p5.ogg"},
      {id: "sound_24", src: soundAsset + "s2_sanskriti_p6.ogg"},
      {id: "sound_25", src: soundAsset + "s2_sanskriti_p7.ogg"},
      {id: "sound_26", src: soundAsset + "s2_sanskriti_p8.ogg"},
      {id: "sound_27", src: soundAsset + "s2_sanskriti_p9.ogg"},

      {id: "sound_28", src: soundAsset + "s2_pesha_p2.ogg"},
      {id: "sound_29", src: soundAsset + "s2_pesha_p3.ogg"},
      {id: "sound_30", src: soundAsset + "s2_pesha_p4.ogg"},
      {id: "sound_31", src: soundAsset + "s2_pesha_p5.ogg"},
      {id: "sound_32", src: soundAsset + "s2_pesha_p6.ogg"},
      {id: "sound_33", src: soundAsset + "s2_pesha_p7.ogg"},
      {id: "sound_34", src: soundAsset + "s2_pesha_p8.ogg"},
      {id: "sound_35", src: soundAsset + "s2_pesha_p9.ogg"},

    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound);//for registering sounds
    preload.installPlugin(createjs.Sound);//for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }

  function handleFileLoad(event) {
    // console.log(event.item);
  }

  function handleProgress(event) {
    $('#loading-text').html(parseInt(event.loaded * 100) + '%');
  }

  function handleComplete(event) {
    $('#loading-wrapper').hide(0);
    //initialize varibales
    current_sound = createjs.Sound.play('sound_1');
    current_sound.stop();
    // call main function
    templateCaller();
  }

  //initialize
  init();

  /*==================================================
  =            Handlers and helpers Block            =
  ==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


  /*=================================================
  =            general template function            =
  =================================================*/
  function generaltemplate() {
    if (countNext==0 && flager==0) {sound_player('sound_first',0); flager=1;}
    loadpage(maincontent);
    $(".options").click(function () {
      countNext = 1;
      if($(this).hasClass("nutrition")){
        visitedcontent["nutrition"]=true;
        maincontent = nutritionContent;
        nutritiongeneraltemp();
      }
      else if($(this).hasClass("respiration")){
        visitedcontent["respiration"]=true;
        maincontent = respirationContent;
        respirationgeneraltemp();
      }
      else if($(this).hasClass("internaltransport")){
        visitedcontent["inttrans"]=true;
        maincontent = inttransContent;
        internaltransportgeneraltemp();
      }
      else if($(this).hasClass("excretion")){
        visitedcontent["excretion"]=true;
        maincontent = excretionContnent;
        excretiongeneraltemp();
      }
      else if($(this).hasClass("reproduction")){
        visitedcontent["reproduction"]=true;
        maincontent = reproductionContent;
        reproductiongeneraltemp();
      }
    });
  }
  function loadpage(contenttobeload){
    $total_page = contenttobeload.length;
    loadTimelineProgress($total_page, countNext + 1);
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(contenttobeload[countNext]);
    $board.html(html);
    texthighlight($board);
    vocabcontroller.findwords(countNext);
    put_image(contenttobeload, countNext, preload);
  }

  function nutritiongeneraltemp(){
    loadpage(maincontent);
    console.log("I am in nutrition tab"+countNext);
    switch(countNext){
      case 1:
      sound_player('sound_0',1);
      $prevBtn.css("display","none");
      break;
      case 2:
      var s= Snap('#lineSvg');
      var svg = Snap.load(preload.getResult("path").src, function ( loadedFragment ) {
        s.append(loadedFragment);
        $("#path").attr("class","lineanim");
      });
      sound_player('sound_1',1);

      break;
      case 3:
      sound_player('sound_2',1);
      $('.midimage').css({"width":"75%"});
      break;
      case 4:
      sound_player('sound_3',1);
      $('.midimage').css({"width":"75%"});
      break;
      case 5:
      sound_player('sound_4',1);
      $('.midimage').css({"width":"75%"});
      break;
      case 6:
      sound_player('sound_5',1);
      $('.midimage').css({"width":"75%"});
      break;
      case 7:
      $('.midimage').css({"width":"75%"});
      sound_player('sound_6',0);
      setTimeout(()=>navigationcontroller(countNext,$total_page,false,checkforlastpage("nutrition")),6000);
      break;
      default:
      navigationcontroller(countNext,$total_page);
    }
    if (countNext==3||countNext==4||countNext==6) {
      $('.midimage').css('left','150%').animate({'left':'50%'},1000,"linear");
    }
    if(countNext==5){
      $('.midimage').css({'transform': 'translate(-50%,-50%)scale(2)'}).addClass('commonzoom');
    }
  }

  function respirationgeneraltemp(){
    loadpage(maincontent);
    console.log("I am in res tab"+countNext);
    $('.midimage').css({"width":"75%"});

    switch(countNext){
      case 1:
      sound_player('sound_7',1);
      $prevBtn.css("display","none");
      break;
      case 2:
      sound_player('sound_8',1);
      $prevBtn.css("display","none");
      break;
      case 3:
      sound_player('sound_9',1);
      $prevBtn.css("display","none");
      break;
      case 4:
      sound_player('sound_10',1);
      $prevBtn.css("display","none");
      break;

      case 5:
      sound_player('sound_11',1);
      $prevBtn.css("display","none");
      break;

      case 6:
      sound_player('sound_12',0);

      setTimeout(()=>navigationcontroller(countNext,$total_page,false,checkforlastpage("respiration")),6000);
      break;
      default:
      navigationcontroller(countNext,$total_page);
    }
    if(countNext==3){
      $('.midimage').css({'transform': 'translate(-50%,-50%)scale(2)'}).addClass('commonzoom');
    }
    if(countNext==5){
      $('.midimage').css({'opacity': '0'}).addClass('fadein');
    }

  }

  function internaltransportgeneraltemp(){
    loadpage(maincontent);
    console.log("I am in tra tab"+countNext);
    switch(countNext){
      case 1:
      sound_player('sound_13',1);
      $prevBtn.css("display","none");
      break;
      case 2:
      sound_player('sound_14',1);
      $prevBtn.css("display","none");
      break;
      case 3:
      sound_player('sound_15',1);
      $prevBtn.css("display","none");
      break;
      case 4:
      sound_player('sound_16',1);
      $prevBtn.css("display","none");
      break;

      case 5:
      sound_player('sound_17',1);
      $prevBtn.css("display","none");
      break;

      case 6:
      $('.midimage').css({"width":"75%"});
      sound_player('sound_18',1);
      $prevBtn.css("display","none");
      break;

      case 7:
      sound_player('sound_19',0);

      setTimeout(()=>navigationcontroller(countNext,$total_page,false,checkforlastpage("inttrans")),6000);

      break;
    }
    if(countNext==2||countNext==6){
      $('.midimage').css({'transform': 'translate(-50%,-50%)scale(2)'}).addClass('commonzoom');
    }
    if(countNext==5){
      $('.midimage').css({'opacity': '0'}).addClass('fadein');
    }
    if (countNext==7||countNext==4) {
      $('.midimage').css('left','150%').animate({'left':'50%'},1000,"linear");
    }
  }

  function excretiongeneraltemp(){
    loadpage(maincontent);
    $('.midimage').css({"width":"75%"});

    console.log("I am in exec tab"+countNext);
    switch(countNext){
      case 1:
      sound_player('sound_20',1);
      $prevBtn.css("display","none");
      break;
      case 2:
      sound_player('sound_21',1);
      $prevBtn.css("display","none");
      break;
      case 3:
      sound_player('sound_22',1);
      $prevBtn.css("display","none");
      break;
      case 4:
      sound_player('sound_23',1);
      $prevBtn.css("display","none");
      break;

      case 5:
      sound_player('sound_24',1);
      $prevBtn.css("display","none");
      break;

      case 6:
      sound_player('sound_25',1);
      $prevBtn.css("display","none");
      break;
      case 7:
      sound_player('sound_26',1);
      $prevBtn.css("display","none");
      break;
      case 8:
      sound_player('sound_27',0);
      setTimeout(()=>navigationcontroller(countNext,$total_page,false,checkforlastpage("excretion")),5000);
      break;
    }
    if(countNext==3||countNext==6){
      $('.midimage').css({'transform': 'translate(-50%,-50%)scale(2)'}).addClass('commonzoom');
    }
    if(countNext==5){
      $('.midimage').css({'opacity': '0'}).addClass('fadein');
    }
    if (countNext==7) {
      $('.midimage').css('left','150%').animate({'left':'50%'},1000,"linear");
    }
  }

  function reproductiongeneraltemp(){
    loadpage(maincontent);
    $('.midimage').css({"width":"75%"});

    console.log("I am in rep tab"+countNext);
    switch(countNext){
      case 1:
      sound_player('sound_28',1);
      $prevBtn.css("display","none");
      break;
      case 2:
      sound_player('sound_29',1);
      $prevBtn.css("display","none");
      break;
      case 3:
      sound_player('sound_30',1);
      $prevBtn.css("display","none");
      break;
      case 4:
      sound_player('sound_31',1);
      $prevBtn.css("display","none");
      break;

      case 5:
      sound_player('sound_32',1);
      $prevBtn.css("display","none");
      break;

      case 6:
      sound_player('sound_33',1);
      $prevBtn.css("display","none");
      break;
      case 7:
      sound_player('sound_34',1);
      $prevBtn.css("display","none");
      break;
      case 8:
      sound_player('sound_35',0);
      setTimeout(()=>navigationcontroller(countNext,$total_page,false,checkforlastpage("reproduction")),6000);
      break;
    }
    if(countNext==3||countNext==6){
      $('.midimage').css({'transform': 'translate(-50%,-50%)scale(2)'}).addClass('commonzoom');
    }
    if(countNext==5){
      $('.midimage').css({'opacity': '0'}).addClass('fadein');
    }
    if (countNext==7||countNext==4) {
      $('.midimage').css('left','150%').animate({'left':'50%'},1000,"linear");
    }
  }

  function  checkforlastpage(updatekey){
    var test = 0;
    visitedcontent[updatekey] = true;
    if( visitedcontent["nutrition"] == true &&
    visitedcontent["respiration"] == true &&
    visitedcontent["inttrans"] == true &&
    visitedcontent["excretion"] == true &&
    visitedcontent["reproduction"] == true
  )
  return false;
  else
  return true;
}

function sound_player(sound_id, navigate) {
  createjs.Sound.stop();
  current_sound = createjs.Sound.play(sound_id);
  current_sound.play();
  current_sound.on('complete', function () {
    navigate ? navigationcontroller(countNext, $total_page) : "";
  });
}

function templateCaller() {
  $prevBtn.css('display', 'none');
  $nextBtn.css('display', 'none');
  generaltemplate();
  loadTimelineProgress($total_page, countNext + 1);
}

$nextBtn.on("click", function () {
  createjs.Sound.stop();
  clearTimeout(timeoutvar);
  if (countNext == $total_page - 1) {
    countNext = 0;
    templateCaller();
  }
  else {
    countNext++;
    switchtemplate();
  }
});

function switchtemplate(){
  $prevBtn.css('display', 'none');
  $nextBtn.css('display', 'none');
  $(".contentblock").hasClass("nutr")?nutritiongeneraltemp():
  $(".contentblock").hasClass("res")?respirationgeneraltemp():
  $(".contentblock").hasClass("inttrans")?internaltransportgeneraltemp():
  $(".contentblock").hasClass("excre")?excretiongeneraltemp():
  $(".contentblock").hasClass("repro")?reproductiongeneraltemp():generaltemplate();
}

$refreshBtn.click(function(){
  countNext==0?templateCaller():switchtemplate();
});


$prevBtn.on('click', function () {
  createjs.Sound.stop();
  clearTimeout(timeoutvar);
  countNext--;
  countNext==0?templateCaller():switchtemplate();
  /* if footerNotificationHandler pageEndSetNotification was called then on click of
  previous slide button hide the footernotification */
  countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
});


function texthighlight($highlightinside) {
  //check if $highlightinside is provided
  typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

  var $alltextpara = $highlightinside.find("*[data-highlight='true']");
  var stylerulename;
  var replaceinstring;
  var texthighlightstarttag;
  var texthighlightendtag = "</span>";
  if ($alltextpara.length > 0) {
    $.each($alltextpara, function(index, val) {
      /*if there is a data-highlightcustomclass attribute defined for the text element
      use that or else use default 'parsedstring'*/
      $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
      ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

      texthighlightstarttag = "<span class='" + stylerulename + "'>";
      replaceinstring = $(this).html();
      replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
      replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
      $(this).html(replaceinstring);
    });
  }
}
function appearoneafteranother(delaytime){
  $(".appear").hide();
  clearInterval(setTime);
  setTime = setInterval(function(index){
    $(".appear").each(function(){
      $(this).delay(delaytime).fadeIn(1000);
      delaytime = delaytime +1500;
    });
  });
}

function shufflehint(optionName,a){
  console.log("I am in shufflehint")
  var optiondiv = $(".contentblock");
  for (var i = 2; i >= 0; i--) {
    optiondiv.append(optiondiv.find("."+optionName).eq(Math.random() * i | 0));
  }
  optiondiv.find("."+optionName).removeClass().addClass("current");
  optiondiv.find(".current").each(function (index) {
    var $this = $(this)
    $this.addClass(a[index]);
  });
  optiondiv.find("."+optionName).removeClass("current");
}
});

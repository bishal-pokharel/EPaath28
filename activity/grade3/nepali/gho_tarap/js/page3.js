var imgpath = $ref + "/images/diy/";
var imgpath2 = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[

	// slide0
  {
    extratextblock:[
    {
      textclass:'lesson-title',
      textdata:data.string.diy
    }],
    imageblock:[{
      imagestoshow:[
        {
          imgclass:'bgfull',
          imgid:'bgdiy'
        }
      ]
    }]
  },
  //slide1
  {
    contentblockadditionalclass:'bgyellow',
    extratextblock:[
    {
      textclass:'toptext',
      textdata:data.string.p2s1
    },
    {
      textclass:'opttext1',
      textdata:data.string.p2s2
    },
    {
      textclass:'opttext2',
      textdata:data.string.p2s3
    }
    ],
    imageblock:[{
      imagestoshow:[
        {
          imgclass:'option1 correct',
          imgid:'yak'
        },
        {
          imgclass:'option2',
          imgid:'cow'
        }
      ]
    }]
  },
  //slide2
  // {
  //   contentblockadditionalclass:'bgyellow',
  // },
  //slide2
  {
    contentblockadditionalclass:'bgyellow',
    extratextblock:[
    {
      textclass:'toptext',
      textdata:data.string.p2s1
    },
    {
      textclass:'opttext1',
      textdata:data.string.p2s4
    },
    {
      textclass:'opttext2',
      textdata:data.string.p2s5
    }
    ],
    imageblock:[{
      imagestoshow:[
        {
          imgclass:'option1',
          imgid:'pumpkin'
        },
        {
          imgclass:'option2 correct',
          imgid:'potato'
        }
      ]
    }]
  },
  //slide4
  // {
  //   contentblockadditionalclass:'bgyellow',
  // },
  //slide3
  {
    contentblockadditionalclass:'bgyellow',
    extratextblock:[
    {
      textclass:'toptext',
      textdata:data.string.p2s1
    },
    {
      textclass:'opttext1',
      textdata:data.string.p2s6
    },
    {
      textclass:'opttext2',
      textdata:data.string.p2s7
    }
    ],
    imageblock:[{
      imagestoshow:[
        {
          imgclass:'option1 correct',
          imgid:'gumba'
        },
        {
          imgclass:'option2',
          imgid:'masjad'
        }
      ]
    }]
  },
  //slide6
  // {
  //   contentblockadditionalclass:'bgyellow',
  // },
  //slide4
  {
    contentblockadditionalclass:'bgyellow',
    extratextblock:[
    {
      textclass:'toptext',
      textdata:data.string.p2s1
    },
    {
      textclass:'opttext1',
      textdata:data.string.p2s8
    },
    {
      textclass:'opttext2',
      textdata:data.string.p2s9
    }
    ],
    imageblock:[{
      imagestoshow:[
        {
          imgclass:'option1 correct',
          imgid:'yak_farming'
        },
        {
          imgclass:'option2',
          imgid:'goat_farming'
        }
      ]
    }]
  },
  //slide8
  // {
  //   contentblockadditionalclass:'bgyellow',
  // },
  //slide5
  {
    contentblockadditionalclass:'bgyellow',
    extratextblock:[
    {
      textclass:'toptext',
      textdata:data.string.p2s1
    },
    {
      textclass:'opttext1',
      textdata:data.string.p2s10
    },
    {
      textclass:'opttext2',
      textdata:data.string.p2s11
    }
    ],
    imageblock:[{
      imagestoshow:[
        {
          imgclass:'option1 correct',
          imgid:'phoksundo_lake'
        },
        {
          imgclass:'option2',
          imgid:'rani_pokhari'
        }
      ]
    }]
  },
  //slide10
  // {
  //   contentblockadditionalclass:'bgyellow',
  // },
  //slide6
  {
    contentblockadditionalclass:'bgyellow',
    extratextblock:[
    {
      textclass:'toptext',
      textdata:data.string.p2s1
    },
    {
      textclass:'opttext1',
      textdata:data.string.p2lakhed
    },
    {
      textclass:'opttext2',
      textdata:data.string.p2tibetd
    }
    ],
    imageblock:[{
      imagestoshow:[
        {
          imgclass:'option1',
          imgid:'lakha_dance'
        },
        {
          imgclass:'option2 correct',
          imgid:'folk_dance'
        }
      ]
    }]
  },
  //slide12
  // {
  //   contentblockadditionalclass:'bgyellow',
  // },
  //slide7
  {
    contentblockadditionalclass:'bgyellow',
    extratextblock:[
    {
      textclass:'toptext',
      textdata:data.string.p2s1
    },
    {
      textclass:'opttext1',
      textdata:data.string.p2kamal
    },
    {
      textclass:'opttext2',
      textdata:data.string.p2yasa
    }
    ],
    imageblock:[{
      imagestoshow:[
        {
          imgclass:'option1',
          imgid:'lotus'
        },
        {
          imgclass:'option2 correct',
          imgid:'yasha'
        }
      ]
    }]
  },
  //slide14
  // {
  //   contentblockadditionalclass:'bgyellow',
  // },
  //slide8
  {
    contentblockadditionalclass:'bgyellow',
    extratextblock:[
    {
      textclass:'toptext',
      textdata:data.string.p2s1
    },
    {
      textclass:'opttext1',
      textdata:data.string.p2s12
    },
    {
      textclass:'opttext2',
      textdata:data.string.p2s13
    }
    ],
    imageblock:[{
      imagestoshow:[
        {
          imgclass:'option1',
          imgid:'five_star_hotel'
        },
        {
          imgclass:'option2 correct',
          imgid:'hotel_dolpa'
        }
      ]
    }]
  },
  //slide16
  // {
  //   contentblockadditionalclass:'bgyellow',
  // },
  //slide9
  {
    contentblockadditionalclass:'bgyellow',
    extratextblock:[
    {
      textclass:'toptext',
      textdata:data.string.p2s1
    },
    {
      textclass:'opttext1',
      textdata:data.string.p2s14
    },
    {
      textclass:'opttext2',
      textdata:data.string.p2s15
    }
    ],
    imageblock:[{
      imagestoshow:[
        {
          imgclass:'option1',
          imgid:'onehornedrhino'
        },
        {
          imgclass:'option2 correct',
          imgid:'snow_leapord'
        }
      ]
    }]
  },
  //slide17
  // {
  //   contentblockadditionalclass:'bgyellow',
  // },
  //slide10
  {
    imageblock:[{
      imagestoshow:[
        {
          imgclass:'boyclass',
          imgid:'sagar01'
        },
        {
          imgclass:'background1',
          imgid:'bg01'
        }
      ]
    }],
    speechbox:[{
      speechbox: 'sp-1',
      textclass: "answer",
      textdata: data.string.p2s16,
      imgclass: '',
      imgid : 'textbox',
      imgsrc: '',
    }],
  },

];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	$('.mainBox > div').prepend('<p class="replay_button"></p>');

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
      {id: "bgdiy", src: imgpath+"bg_diy.png", type: createjs.AbstractLoader.IMAGE},
      {id: "yak", src: imgpath+"yak.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "cow", src: imgpath+"cow.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "pumpkin", src: imgpath+"pumkin.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "potato", src: imgpath+"potato.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "gumba", src: imgpath+"gumba.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "masjad", src: imgpath+"masjad.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "yak_farming", src: imgpath+"yak_farming.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "goat_farming", src: imgpath+"goat_farming.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "phoksundo_lake", src: imgpath+"phoksundo_lake.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "rani_pokhari", src: imgpath+"rani_pokhari.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "folk_dance", src: imgpath+"folk_dance.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "lakha_dance", src: imgpath+"lakha_dance.png", type: createjs.AbstractLoader.IMAGE},
      {id: "lotus", src: imgpath+"lotus-flower.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "yasha", src: imgpath+"yacha_gumba.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "five_star_hotel", src: imgpath+"five_star_hotel.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "hotel_dolpa", src: imgpath+"hotel_dolpa.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "onehornedrhino", src: imgpath+"onehornedrhino.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "snow_leapord", src: imgpath+"snow_leapord.jpg", type: createjs.AbstractLoader.IMAGE},
      {id: "sagar01", src: imgpath+"sagar01.png", type: createjs.AbstractLoader.IMAGE},
      {id: "correct", src: imgpath+"correct.png", type: createjs.AbstractLoader.IMAGE},
      {id: "incorrect", src: imgpath+"incorrect.png", type: createjs.AbstractLoader.IMAGE},
      {id: "textbox", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
      {id: "bg01", src: imgpath2+"bg_cover_page.jpg", type: createjs.AbstractLoader.IMAGE},

			//soundAsset

      {id: "sound_0", src: soundAsset + "s3_p2_ins.ogg"},
      {id: "sound_1", src: soundAsset + "s3_p11.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound); //for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
    if(countNext==0){
      play_diy_audio();
    }

/*	var top_val = (($('.top_text').height()/$('.coverboardfull').height() )* 100) +6.5+"%";
	$('.top_below_text').css({
		'top':top_val
		});
*/
		switch(countNext) {
      case 0:
      nav_button_controls(1000);
      break;
      case 1:
      sound_player('sound_0',0);
      break;
      case 10:
      sound_player('sound_1',1);
      break;
    }
    if(countNext==6){
      $('.option1').css({"background-color":"black"});
    }
      if(countNext>0&&countNext<10){
      $('.option1').click(function(){
        createjs.Sound.stop();
        $('.option1').css({'pointer-events':'none'});
        if($('.option1').hasClass('correct')){
          $('.opttext1').addClass('corr');
          play_correct_incorrect_sound(1);
          $('.option1,.option2').css({'pointer-events':'none'});
          $('<img id="correct" style="left:70%;top:30%;position:absolute;width:5%;transform:translate(-900%,680%)" src="'+imgpath +'correct.png" />').insertAfter(this);
          nav_button_controls(100);
        }
        else{
          $('.opttext1').addClass('incorr');
          play_correct_incorrect_sound(0);
          $('<img id="incorrect" style="left:70%;top:30%;position:absolute;width:5%;transform:translate(-900%,680%)" src="'+imgpath +'incorrect.png" />').insertAfter(this);
        }
      });
      $('.option2').click(function(){
        createjs.Sound.stop();

        $('.option2').css({'pointer-events':'none'});
        if($('.option2').hasClass('correct')){
          $('.opttext2').addClass('corr');
          play_correct_incorrect_sound(1);
          $('.option1,.option2').css({'pointer-events':'none'});
          $('<img id="correct" style="left:70%;top:30%;position:absolute;width:5%;transform:translate(50%,680%)" src="'+imgpath +'correct.png" />').insertAfter(this);
          nav_button_controls(100);
        }
        else{
          $('.opttext2').addClass('incorr');
          play_correct_incorrect_sound(0);
          $('<img id="incorrect" style="left:70%;top:30%;position:absolute;width:5%;transform:translate(50%,680%)" src="'+imgpath +'incorrect.png" />').insertAfter(this);
        }
      });

		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

  function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			nav_button_controls(100);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

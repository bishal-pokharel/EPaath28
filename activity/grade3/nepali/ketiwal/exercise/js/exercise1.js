var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/";

var content=[

	//ex1
	{
		exerciseblock: [
			{
				instructiondata: data.string.ex_ins_1,
				textdata: data.string.e1_q,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.e1_1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.e1_2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.e1_3,
				}],
			}
		]
	},
	//ex2
	{
		exerciseblock: [
			{
				instructiondata: data.string.ex_ins_1,
				textdata: data.string.e2_q,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.e2_1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.e2_2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.e2_3,
				}],
			}
		]
	},
	//ex3
	{
		exerciseblock: [
			{
				instructiondata: data.string.ex_ins_1,
				textdata: data.string.e3_q,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.e3_1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.e3_2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.e3_3,
				}],
			}
		]
	},
	//ex4
	{
		exerciseblock: [
			{
				instructiondata: data.string.ex_ins_1,
				textdata: data.string.e4_q,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.e4_1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.e4_2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.e4_3,
				}],
			}
		]
	},
	//ex5
	{
		exerciseblock: [
			{
				instructiondata: data.string.ex_ins_1,
				textdata: data.string.e5_q,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.e5_1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.e5_2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.e5_3,
				}],
			}
		]
	},
	//ex6
	{
		exerciseblock: [
			{
				instructiondata: data.string.ex_ins_1,
				textdata: data.string.e6_q,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.e6_1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.e6_2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.e6_3,
				}],
			}
		]
	},
	//ex7
	{
		exerciseblock: [
			{
				instructiondata: data.string.ex_ins_1,
				textdata: data.string.e7_q,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.e7_1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.e7_2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.e7_3,
				}],
			}
		]
	},
	//ex8
	{
		exerciseblock: [
			{
				instructiondata: data.string.ex_ins_1,
				textdata: data.string.e8_q,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.e8_1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.e8_2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.e8_3,
				}],
			}
		]
	},
	//ex9
	{
		exerciseblock: [
			{
				instructiondata: data.string.ex_ins_1,
				textdata: data.string.e9_q,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.e9_1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.e9_2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.e9_3,
				}],
			}
		]
	},
	//ex10
	{
		exerciseblock: [
			{
				instructiondata: data.string.ex_ins_1,
				textdata: data.string.e10_q,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.e10_1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.e10_2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.e10_3,
				}],
			}
		]
	},
];

/*remove this for non random questions*/
content.shufflearray();


$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	/*for limiting the questions to 10*/
	var $total_page = 10;
	var current_sound;
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            // sounds
            {id: "sound_1", src: soundAsset + "Ex_1_ins.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();
	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	var score = 0;
	var testin = new EggTemplate();

 	testin.init(10);
	/*values in this array is same as the name of images of eggs in image folder*/

    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }

    function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);

		/*generate question no at the beginning of question*/
		testin.numberOfQuestions();
		countNext==0?sound_player("sound_1"):"";
		/*for randomizing the options*/
		var parent = $(".optionsdiv");
		var divs = parent.children();
			 while (divs.length) {
			        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			  }

		var ansClicked = false;
		var wrngClicked = false;

		$(".buttonsel").click(function(){
			$(this).removeClass('forhover');
				if(ansClicked == false){
					/*class 1 is always for the right answer. updates scoreboard and disables other click if
					right answer is clicked*/
					if($(this).hasClass("class1")){

						if(wrngClicked == false){
							testin.update(true);
						}
                        current_sound.stop();
						play_correct_incorrect_sound(1);
						$(this).css("background","#6AA84F");
						$(this).css("border","5px solid #B6D7A8");
						$('.buttonsel').removeClass('forhover forhoverimg');
						$(this).children('.wrngopt').hide(0);
						$(this).children('.corctopt').show(0);
						$(this).siblings('.wrngopt').hide(0);
						$(this).siblings('.corctopt').show(0);
						ansClicked = true;

						if(countNext != $total_page)
						$nextBtn.show(0);
					}else{
						testin.update(false);
                        current_sound.stop();
						play_correct_incorrect_sound(0);
						$(this).css("background","#EA9999");
						$(this).css("border","5px solid #efb3b3");
						$(this).css("color","#FFFFFF");
						wrngClicked = true;
						$(this).children('.wrngopt').show(0);
						$(this).siblings('.wrngopt').show(0);
					}
				}
			});

		/*======= SCOREBOARD SECTION ==============*/
	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg1",
		singletext:[
		{
			textclass: "covertext",
			textdata: data.lesson.chapter
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'cover',
				imgsrc: ""
			}
		]
	}]
},
// slide1
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg1",
	poemtext:[
	{
		// textclass: "covertext",
		textdata1: data.string.p1part1,
		textdata2: data.string.p1part2
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg1',
			imgsrc: ""
		},
		{
			imgclass: "p1img1",
			imgid : 'p1img1',
			imgsrc: ""
		},
		{
			imgclass: "p1img2",
			imgid : 'p1img2',
			imgsrc: ""
		},
		{
			imgclass: "p1img3",
			imgid : 'p1img3',
			imgsrc: ""
		}
	]
}]
},
// slide2
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg1",
	poemtext:[
	{
		// textclass: "covertext",
		textdata1: data.string.p2part1,
		textdata2: data.string.p2part2
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg2',
			imgsrc: ""
		},
		{
			imgclass: "p2img1",
			imgid : 'p2img1',
			imgsrc: ""
		},
		{
			imgclass: "p2img2",
			imgid : 'p2img2',
			imgsrc: ""
		},
		{
			imgclass: "tear1",
			imgid : 'p2tear1',
			imgsrc: ""
		},
		{
			imgclass: "tear2",
			imgid : 'p2tear2',
			imgsrc: ""
		}
	]
}]
},
// slide3
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg1",
	poemtext:[
	{
		// textclass: "covertext",
		textdata1: data.string.p3part1,
		textdata2: data.string.p3part2
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg3',
			imgsrc: ""
		},
		{
			imgclass: "p3img1",
			imgid : 'p3img1',
			imgsrc: ""
		},
		{
			imgclass: "imgkoili",
			imgid : 'p3koili1',
			imgsrc: ""
		},
		{
			imgclass: "imgbee",
			imgid : 'p3bee2',
			imgsrc: ""
		}
	]
}]
},
// slide4
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg1",
	poemtext:[
	{
		// textclass: "covertext",
		textdata1: data.string.p4part1,
		textdata2: data.string.p4part2
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg4',
			imgsrc: ""
		},
		{
			imgclass: "p4img1",
			imgid : 'p4img1',
			imgsrc: ""
		}
	]
}]
},
// slide5
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg1",
	poemtext:[
	{
		// textclass: "covertext",
		textdata1: data.string.p5part1,
		textdata2: data.string.p5part2
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg5',
			imgsrc: ""
		},
		{
			imgclass: "p5img1 ringing",
			imgid : 'p5img1',
			imgsrc: ""
		},
		{
			imgclass: "p5img2",
			imgid : 'p5img2',
			imgsrc: ""
		},
		{
			imgclass: "p5img3",
			imgid : 'p5img4',
			imgsrc: ""
		}
	]
}]
},
// slide6
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg1",
	poemtext:[
	{
		// textclass: "covertext",
		textdata1: data.string.p6part1,
		textdata2: data.string.p6part2
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg6',
			imgsrc: ""
		},
		{
			imgclass: "p6img1",
			imgid : 'p6img1',
			imgsrc: ""
		},
		{
			imgclass: "p6img2",
			imgid : 'p6img2',
			imgsrc: ""
		}
	]
}]
},
// slide7
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg1",
	poemtext:[
	{
		// textclass: "covertext",
		textdata1: data.string.p7part1,
		textdata2: data.string.p7part2
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg7',
			imgsrc: ""
		},
		{
			imgclass: "p7img1",
			imgid : 'p7img1',
			imgsrc: ""
		},
	]
}]
},
// slide8
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg1",
	poemtext:[
	{
		// textclass: "covertext",
		textdata1: data.string.p8part1,
		textdata2: data.string.p8part2
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg8',
			imgsrc: ""
		},
		{
			imgclass: "p8img1",
			imgid : 'p8img2',
			imgsrc: ""
		}
	]
}]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var isFirefox = typeof InstallTrigger !== 'undefined';

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "cover", src: imgpath+"coverpage.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg1", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg2", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg3", src: imgpath+"bg03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg4", src: imgpath+"bg04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg5", src: imgpath+"bg05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg6", src: imgpath+"bg06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg7", src: imgpath+"bg07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg8", src: imgpath+"bg08.png", type: createjs.AbstractLoader.IMAGE},

			{id: "p1img1", src: imgpath+"aasha.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p1img2", src: imgpath+"suraj02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p1img3", src: imgpath+"niti02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p1img4", src: imgpath+"suraj01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p1img5", src: imgpath+"niti01.png", type: createjs.AbstractLoader.IMAGE},

			{id: "p2img1", src: imgpath+"angry_girl.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p2img2", src: imgpath+"sad_girl.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p2img3", src: imgpath+"cry_girl.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p2tear1", src: imgpath+"tear01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p2tear2", src: imgpath+"tear02.png", type: createjs.AbstractLoader.IMAGE},

			{id: "p3img1", src: imgpath+"twokids01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p3koili1", src: imgpath+"koili.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "p3koili2", src: imgpath+"koili.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p3bee1", src: imgpath+"bee.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "p3bee2", src: imgpath+"bee.png", type: createjs.AbstractLoader.IMAGE},

			{id: "p4img1", src: imgpath+"ball.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p4img2", src: imgpath+"spingingball.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "p5img1", src: imgpath+"alarmclock.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p5img2", src: imgpath+"bed01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p5img3", src: imgpath+"bed02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p5img4", src: imgpath+"sofa01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p5img5", src: imgpath+"sofa02.png", type: createjs.AbstractLoader.IMAGE},

			{id: "p6img1", src: imgpath+"twokids.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p6img2", src: imgpath+"bench.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p6img3", src: imgpath+"student01.png", type: createjs.AbstractLoader.IMAGE},

			{id: "p7img1", src: imgpath+"kidscycling.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "p8img1", src: imgpath+"takingblessing01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p8img2", src: imgpath+"takingblessing02.png", type: createjs.AbstractLoader.IMAGE},

			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "title", src: soundAsset+"title.ogg"},

			{id: "para11", src: soundAsset+"para1-1.ogg"},
			{id: "para12", src: soundAsset+"para1-2.ogg"},

			{id: "para21", src: soundAsset+"para2-1.ogg"},
			{id: "para22", src: soundAsset+"para2-2.ogg"},

			{id: "para31", src: soundAsset+"para3-1.ogg"},
			{id: "para32", src: soundAsset+"para3-2.ogg"},

			{id: "para41", src: soundAsset+"para4-1.ogg"},
			{id: "para42", src: soundAsset+"para4-2.ogg"},

			{id: "para51", src: soundAsset+"para5-1.ogg"},
			{id: "para52", src: soundAsset+"para5-2.ogg"},

			{id: "para61", src: soundAsset+"para6-1.ogg"},
			{id: "para62", src: soundAsset+"para6-2.ogg"},

			{id: "para71", src: soundAsset+"para7-1.ogg"},
			{id: "para72", src: soundAsset+"para7-2.ogg"},

			{id: "para81", src: soundAsset+"para8-1.ogg"},
			{id: "para82", src: soundAsset+"para8-2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		var pbox1, pbox2, ptext1, ptext2;

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		pbox1 = "poembox1";
		pbox2 = "poembox2";
		switch(countNext){
			case 0:
			sound_player("title",1);
			break;
			case 1:
				ptext1 = "para11";
				ptext2 = "para12";
			break;
			case 2:
				ptext1 = "para21";
				ptext2 = "para22";
			break;
			case 3:
				ptext1 = "para31";
				ptext2 = "para32";
			break;
			case 4:
				ptext1 = "para41";
				ptext2 = "para42";
			break;
			case 5:
				ptext1 = "para51";
				ptext2 = "para52";
			break;
			case 6:
				ptext1 = "para61";
				ptext2 = "para62";
			break;
			case 7:
				ptext1 = "para71";
				ptext2 = "para72";
			break;
			case 8:
				ptext1 = "para81";
				ptext2 = "para82";
			break;
		}
		for_all_slides(pbox1, ptext1, false);

		function firstline_fin(){
			$("#span_speec_text").append("<br>");
			for_all_slides(pbox2, ptext2, true);
			switch(countNext){
				case 1:
					$(".p1img2").attr("src",preload.getResult('p1img4').src);
					$(".p1img3").attr("src",preload.getResult('p1img5').src);
				break;
				case 2:
					$(".p2img1").addClass("rotgirl");
					$(".p2img2").attr("src",preload.getResult('p2img3').src);
					$(".tear1").show(0);
					$(".tear2").show(0);
				break;
				case 3:
					$(".p3img1").addClass("flippeep");
						$(".imgkoili").attr("src",preload.getResult('p3koili2').src);
						$(".imgbee").attr("src",preload.getResult('p3bee1').src);
				break;
				case 4:
						$(".p4img1").attr("src",preload.getResult('p4img2').src);
				break;
				case 5:
						$(".p5img1").removeClass("ringing");
						$(".p5img2").attr("src",preload.getResult('p5img3').src);
						$(".p5img3").attr("src",preload.getResult('p5img5').src);
				break;
				case 6:
						$(".p6img1").hide(0);
						$(".p6img2").attr("src",preload.getResult('p6img3').src);
				break;
				case 7:
						$(".p7img1").addClass("movecyc");
				break;
				case 8:
						$(".p8img1").attr("src",preload.getResult('p8img1').src);
				break;
			}
		}

	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function for_all_slides(text_class, my_sound_data, last_page_flag){
		var $textblack = $("."+text_class);
		var current_text = $textblack.html();
		// current_text.replace(/<.*>/, '');
		play_text($textblack, current_text);
			current_sound = createjs.Sound.play(my_sound_data);
			current_sound.play();
			current_sound.on('complete', function(){
				textanimatecomplete = false;
				if(!last_page_flag)
						firstline_fin();
				else{
					vocabcontroller.findwords(countNext);
					navigationcontroller();
				}
			});
		}
		var textanimatecomplete = false;

		function play_text($this, text){
			original_text =  text;
			if(isFirefox){
				$this.html("<span id='span_speec_text'></span>"+text);
				$prevBtn.hide(0);
				var $span_speec_text = $("#span_speec_text");
				// $this.css("background-color", "#faf");
				show_text($this, $span_speec_text,text, 110);	// 65 ms is the interval found out by hit and trial
			} else {
				$this.html("<span id='span_speec_text'>"+original_text+"</span>");
			}
		}

		function show_text($this,  $span_speec_text, message, interval) {
			if (0 < message.length && !textanimatecomplete) {
				var nextText = message.substring(0, 1);
				var additionalinterval = 0;
				if (nextText == "<") {
					additionalinterval = 800;
					message = message.substring(4, message.length);
				} else {
					$span_speec_text.append(nextText);
					message = message.substring(1, message.length);
				}
				$this.html($span_speec_text);
				$this.append(message);
				setTimeout(function() {
					show_text($this, $span_speec_text, message, interval);
				}, (interval + additionalinterval));
			} else{
		  		textanimatecomplete = true;
		  	}
		}
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// if(countNext == 0)
		// navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

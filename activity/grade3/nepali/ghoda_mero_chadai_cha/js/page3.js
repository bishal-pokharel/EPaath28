var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentblockadditionalclass: "thebg1",
		singletext:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "wtext",
			textclass: "centertext",
			textdata: data.string.p3text1
		}
	]
},
// slide1
{
	contentblockadditionalclass: "thebg1",
	headerblock:[{
		textdata: data.string.p3text2
	}],
	singletext:[
		{
			textclass: "dropme drophere-1",
		},
		{
			textclass: "dropme drophere-2",
		},
		{
			textclass: "dropme drophere-3",
		},
		{
			textclass: "forhover corr2 dragthis-1",
			textdata: data.string.ddt1o1
		},
		{
			textclass: "forhover corr3 dragthis-2",
			textdata: data.string.ddt1o2
		},
		{
			textclass: "forhover corr dragthis-3",
			textdata: data.string.ddt1o3
		}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cusimg-1",
			imgid : 'dashain',
			imgsrc: ""
		},
		{
			imgclass: "cusimg-2",
			imgid : 'rhino',
			imgsrc: ""
		},
		{
			imgclass: "cusimg-3",
			imgid : 'money',
			imgsrc: ""
		}
	]
}]
},
// slide2
{
	contentblockadditionalclass: "thebg1",
	headerblock:[{
		textdata: data.string.p3text2
	}],
	singletext:[
		{
			textclass: "dropme drophere-1",
		},
		{
			textclass: "dropme drophere-2",
		},
		{
			textclass: "dropme drophere-3",
		},
		{
			textclass: "forhover corr dragthis-1",
			textdata: data.string.ddt2o1
		},
		{
			textclass: "forhover corr3 dragthis-2",
			textdata: data.string.ddt2o2
		},
		{
			textclass: "forhover corr2 dragthis-3",
			textdata: data.string.ddt2o3
		}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cusimg-1",
			imgid : 'ladder',
			imgsrc: ""
		},
		{
			imgclass: "cusimg-2",
			imgid : 'ring',
			imgsrc: ""
		},
		{
			imgclass: "cusimg-3",
			imgid : 'tear',
			imgsrc: ""
		}
	]
}]
},
// slide3
{
	contentblockadditionalclass: "thebg1",
	headerblock:[{
		textdata: data.string.p3text2
	}],
	singletext:[
		{
			textclass: "dropme drophere-1",
		},
		{
			textclass: "dropme drophere-2",
		},
		{
			textclass: "dropme drophere-3",
		},
		{
			textclass: "forhover corr2 dragthis-1",
			textdata: data.string.ddt3o1
		},
		{
			textclass: "forhover corr dragthis-2",
			textdata: data.string.ddt3o2
		},
		{
			textclass: "forhover corr3 dragthis-3",
			textdata: data.string.ddt3o3
		}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cusimg-1",
			imgid : 'five',
			imgsrc: ""
		},
		{
			imgclass: "cusimg-2",
			imgid : 'scissors',
			imgsrc: ""
		},
		{
			imgclass: "cusimg-3",
			imgid : 'smokes',
			imgsrc: ""
		}
	]
}]
},
// slide4
{
	singletext:[
		{
			textclass: "diy2text",
			textdata: data.string.clickdiy
		},
		{
			textclass: "buttonsel forhover correct diytyp1 cdiy-1",
			textdata: data.string.cdiy1opt1
		},
		{
			textclass: "buttonsel forhover diytyp1 cdiy-2",
			textdata: data.string.cdiy1opt2
		},
		{
			textclass: "buttonsel forhover diytyp1 cdiy-3",
			textdata: data.string.cdiy1opt3
		}
],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'bg1',
				imgsrc: ""
			}
		]
	}]
},
// slide5
{
	singletext:[
		{
			textclass: "diy2text",
			textdata: data.string.clickdiy
		},
		{
			textclass: "buttonsel forhover diytyp2 cdiy-1",
			textdata: data.string.cdiy2opt1
		},
		{
			textclass: "buttonsel forhover diytyp2 cdiy-2",
			textdata: data.string.cdiy2opt2
		},
		{
			textclass: "buttonsel forhover correct diytyp2 cdiy-3",
			textdata: data.string.cdiy2opt3
		}
],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'bg1',
				imgsrc: ""
			}
		]
	}]
},
// slide6
{
	singletext:[
		{
			textclass: "diy2text",
			textdata: data.string.clickdiy
		},
		{
			textclass: "buttonsel forhover diytyp3 cdiy-1",
			textdata: data.string.cdiy3opt1
		},
		{
			textclass: "buttonsel forhover diytyp3 cdiy-2",
			textdata: data.string.cdiy3opt2
		},
		{
			textclass: "buttonsel forhover correct diytyp3 cdiy-3",
			textdata: data.string.cdiy3opt3
		}
],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'bg1',
				imgsrc: ""
			}
		]
	}]
},
// slide7
{
	singletext:[
		{
			textclass: "diy2text",
			textdata: data.string.clickdiy
		},
		{
			textclass: "buttonsel forhover correct diytyp1 cdiy-1",
			textdata: data.string.cdiy4opt1
		},
		{
			textclass: "buttonsel forhover diytyp1 cdiy-2",
			textdata: data.string.cdiy4opt2
		},
		{
			textclass: "buttonsel forhover diytyp1 cdiy-3",
			textdata: data.string.cdiy4opt3
		}
],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'bg1',
				imgsrc: ""
			}
		]
	}]
},
// slide8
{
	singletext:[
		{
			textclass: "diy2text",
			textdata: data.string.clickdiy
		},
		{
			textclass: "buttonsel forhover diytyp2 cdiy-1",
			textdata: data.string.cdiy5opt1
		},
		{
			textclass: "buttonsel forhover diytyp2 cdiy-2",
			textdata: data.string.cdiy5opt2
		},
		{
			textclass: "buttonsel forhover correct diytyp2 cdiy-3",
			textdata: data.string.cdiy5opt3
		}
],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'bg1',
				imgsrc: ""
			}
		]
	}]
},
// slide9
{
	singletext:[
		{
			textclass: "diy2text",
			textdata: data.string.clickdiy
		},
		{
			textclass: "buttonsel forhover diytyp3 cdiy-1",
			textdata: data.string.cdiy6opt1
		},
		{
			textclass: "buttonsel forhover correct diytyp3 cdiy-2",
			textdata: data.string.cdiy6opt2
		},
		{
			textclass: "buttonsel forhover diytyp3 cdiy-3",
			textdata: data.string.cdiy6opt3
		}
],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'bg1',
				imgsrc: ""
			}
		]
	}]
},
// slide10
{
	singletext:[
		{
			textclass: "diy2text",
			textdata: data.string.clickdiy2
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "cloptions clohover",
			textclass: "sentdiy",
			textdata: data.string.sdiy1
		}
],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'bg1',
				imgsrc: ""
			}
		]
	}]
},
// slide11
{
	singletext:[
		{
			textclass: "diy2text",
			textdata: data.string.clickdiy2
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "cloptions clohover",
			textclass: "sentdiy",
			textdata: data.string.sdiy2
		}
],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'bg1',
				imgsrc: ""
			}
		]
	}]
},
// slide12
{
	singletext:[
		{
			textclass: "diy2text",
			textdata: data.string.clickdiy2
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "cloptions clohover",
			textclass: "sentdiy",
			textdata: data.string.sdiy3
		}
],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'bg1',
				imgsrc: ""
			}
		]
	}]
},
// slide13
{
	singletext:[
		{
			textclass: "diy2text",
			textdata: data.string.clickdiy2
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "cloptions clohover",
			textclass: "sentdiy",
			textdata: data.string.sdiy4
		}
],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'bg1',
				imgsrc: ""
			}
		]
	}]
},
// slide14
{
	singletext:[
		{
			textclass: "diy2text",
			textdata: data.string.clickdiy2
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "cloptions clohover",
			textclass: "sentdiy",
			textdata: data.string.sdiy5
		}
],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'bg1',
				imgsrc: ""
			}
		]
	}]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "dashain", src: imgpath+"diy/dashain.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ladder", src: imgpath+"diy/ladder.png", type: createjs.AbstractLoader.IMAGE},
			{id: "money", src: imgpath+"diy/money.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rhino", src: imgpath+"diy/rhino.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ring", src: imgpath+"diy/ring.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tear", src: imgpath+"diy/tear.png", type: createjs.AbstractLoader.IMAGE},
			{id: "five", src: imgpath+"diy/five.png", type: createjs.AbstractLoader.IMAGE},
			{id: "scissors", src: imgpath+"diy/scissors.png", type: createjs.AbstractLoader.IMAGE},
			{id: "smokes", src: imgpath+"diy/smokes.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg1", src: imgpath+"bg_new01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"p3_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p3_s2.ogg"},
			{id: "sound_5", src: soundAsset+"p3_s5.ogg"},
			{id: "sound_11", src: soundAsset+"p3_s11.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
      sound_player("sound_"+(countNext+1));

        switch(countNext){
			case 1:
			case 2:
			case 3:
			var moveNext = 0;
			$("[class*='dragthis']").draggable({
		 		containment : ".generalTemplateblock",
	      revert : true,
		 		cursor : "move",
		 		zIndex: 100000,
		 	});

			$(".dropme:eq(0)").droppable({
	      hoverClass: "hovered",
		 		drop: function (event, ui){
		 			$this = $(this);
		 			dropfunc(event, ui, $this, "corr");
		 		}
		 	});

			$(".dropme:eq(1)").droppable({
	      hoverClass: "hovered",
		 		drop: function (event, ui){
		 			$this = $(this);
		 			dropfunc(event, ui, $this, "corr2");
		 		}
		 	});

			$(".dropme:eq(2)").droppable({
	      hoverClass: "hovered",
		 		drop: function (event, ui){
		 			$this = $(this);
		 			dropfunc(event, ui, $this, "corr3");
		 		}
		 	});

			function dropfunc(event, ui, $droppedOn, classname){
				if(ui.draggable.hasClass(classname)){
					// $(".corr-ico").show(0);
					var newVal = ui.draggable.text();
					// newVal = newVal.substr(newVal.length - 1);
					$droppedOn.text(newVal);
					current_sound.stop();
					play_correct_incorrect_sound(true);
	        ui.draggable.draggable('option', 'revert', false);
			 		ui.draggable.draggable('disable').hide(0);
			 		$droppedOn.css({
						"background":"#98C02E",
						"border":"0.2em solid #DEEF3C",
						"color":"white"
					});
					// $("[class*='dragthis']").draggable("disable");
					// $("[class*='dragthis']").removeClass("forhover");
					moveNext++;
					if(moveNext == 3)
						navigationcontroller();
		 		}
				else{
                    current_sound.stop();
					play_correct_incorrect_sound(false);
					// $(".wrong-ico").fadeIn();
				}
		 	}
			break;
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
			$(".buttonsel").click(function(){
				if($(this).hasClass("forhover")){
					$(this).removeClass('forhover');
						if($(this).hasClass("correct")){
                            current_sound.stop();
							play_correct_incorrect_sound(1);
							$(this).css("background","#bed62f");
							$(this).css("border","5px solid #deef3c");
							$(this).css("color","white");
							// $(this).siblings(".corctopt").show(0);
							//$('.hint_image').show(0);
							appender($(this),'correct');
							$('.buttonsel').removeClass('forhover forhoverimg');
							navigationcontroller();
						}
						else{
                            current_sound.stop();
							play_correct_incorrect_sound(0);
							appender($(this),'incorrect');
							$(this).css("background","#FF0000");
							$(this).css("border","5px solid #980000");
							$(this).css("color","white");
							// $(this).siblings(".wrngopt").show(0);
						}
				}

				function appender($this, icon){
					if($this.hasClass("cdiy-1"))
						$(".coverboardfull").append("<img class='cusicon-one' src= '"+ preload.getResult(icon).src +"'>");
					else if($this.hasClass("cdiy-2"))
						$(".coverboardfull").append("<img class='cusicon-two' src= '"+ preload.getResult(icon).src +"'>");
					else
						$(".coverboardfull").append("<img class='cusicon-three' src= '"+ preload.getResult(icon).src +"'>");
					}
			});
			break;
			case 10:
			case 11:
			case 12:
			case 13:
			case 14:
			var txtReplacer;
				switch (countNext) {
					case 10:
					$(".cloptions:eq(1)").addClass("clcorrect");
					txtReplacer = "गाउँमा";
					break;
					case 11:
					$(".cloptions:eq(0)").addClass("clcorrect");
					txtReplacer = "दिउँसो";
					break;
					case 12:
					$(".cloptions:eq(2)").addClass("clcorrect");
					txtReplacer = "दाँत";
					break;
					case 13:
					$(".cloptions:eq(0)").addClass("clcorrect");
					txtReplacer = "दसैँमा";
					break;
					case 14:
					$(".cloptions:eq(3)").addClass("clcorrect");
					txtReplacer = "डाँफे";
					break;
					default:

				}

				$(".cloptions").click(function(){
					if($(this).hasClass("clohover")){
						$(this).removeClass('clohover');
							if($(this).hasClass("clcorrect")){
                                current_sound.stop();
								play_correct_incorrect_sound(1);
								appender($(this),'correct');
								$(this).html(txtReplacer);
								$(this).parent().css("background","#523D9E");
								$(this).parent().css("color","#fff");
								$('.cloptions').removeClass('clohover');
								navigationcontroller();
							}
							else{
                                current_sound.stop();
								play_correct_incorrect_sound(0);
								appender($(this),'incorrect');
								$(this).parent().css("background","#6D5A34");
								$(this).parent().css("color","#fff");
							}
					}

					function appender($this, icon){
						var left_addfactor = $this.parent().position();
						var posIt = $this.position();
						var centerX = posIt.left + left_addfactor.left + ($this.width()/2);
						var centerY = posIt.top + left_addfactor.top + ($this.height()/14);
						$(".coverboardfull").append("<img style='left:"+centerX+";top:"+centerY+";position:absolute;width:5%;transform:translate(-50%,150%);' src= '"+ preload.getResult(icon).src +"'>");
						}
				});
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/page2/";

var sound_grandpa = new buzz.sound((soundAsset + "grandpa.ogg"));
var sound_grandma= new buzz.sound((soundAsset + "grandma.ogg"));
var sound_thulobuwa = new buzz.sound((soundAsset + "thulo_ba.ogg"));
var sound_thuliaama = new buzz.sound((soundAsset + "thulima.ogg"));
var sound_dad = new buzz.sound((soundAsset + "dad.ogg"));
var sound_mom = new buzz.sound((soundAsset + "mom.ogg"));
var sound_kaka = new buzz.sound((soundAsset + "kaka.ogg"));
var sound_kaki = new buzz.sound((soundAsset + "kaki.ogg"));
var sound_dai = new buzz.sound((soundAsset + "dai.ogg"));
var sound_bhauju = new buzz.sound((soundAsset + "bhauju.ogg"));
var sound_me = new buzz.sound((soundAsset + "me.ogg"));
var sound_title = new buzz.sound((soundAsset + "title.ogg"));

var sound_family = new buzz.sound((soundAsset + "family.ogg"));
var sound_your_family = new buzz.sound((soundAsset + "your_family.ogg"));


var current_sound = sound_grandpa;

// var sound_group_b = [sound_ques, sound_3_00, sound_5_30, sound_6_00, sound_7_00, sound_8_30, sound_12_30];

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
	
		uppertextblockadditionalclass: 'lesson_title',
		uppertextblock : [{
			textdata : data.string.p2text0,
			textclass : ''
		}]
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_light',
		
		uppertextblockadditionalclass: 'introduction',
		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : ''
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "sound_icon",
				imgsrc : imgpath + "speaker.png",
			}],
			imagelabels:[
			{
				imagelabelclass : "black_cover",
				imagelabeldata : '',
			}]
		}],
		
		familyblockadditionalclass: 'familytree',
		familyblock: [
			{
				familyblockcontainer: 'order_1',
				familyblockdiv:[
					{
						pairblockcontainer:'order_1',
						pairblock:[{
								elementcontainer:'blink grandpa',
								imgclass: '',
								imgsrc : imgpath + "grandpa.png",
								imagelabelclass : "",
								imagelabeldata : data.string.person1,
							},
							{
								elementcontainer:'blink grandma',
								imgclass: '',
								imgsrc : imgpath + "grandma.png",
								imagelabelclass : "",
								imagelabeldata : data.string.person2,
							},
						]
					}
				]				
			},
			{
				familyblockcontainer: 'order_2',
				familyblockdiv:[
					{
						pairblockcontainer:'order_1',
						pairblock:[{
								elementcontainer:'blink thulobuwa',
								imgclass: '',
								imgsrc : imgpath + "thulobuwa.png",
								imagelabelclass : "",
								imagelabeldata : data.string.person3,
							},
							{
								elementcontainer:'blink thuliaama',
								imgclass: '',
								imgsrc : imgpath + "thuliaama.png",
								imagelabelclass : "",
								imagelabeldata : data.string.person4,
							},
						]
					},
					{
						pairblockcontainer:'order_2',
						pairblock:[{
								elementcontainer:'blink dad',
								imgclass: '',
								imgsrc : imgpath + "dad.png",
								imagelabelclass : "",
								imagelabeldata : data.string.person5,
							},
							{
								elementcontainer:'blink mom',
								imgclass: '',
								imgsrc : imgpath + "mum.png",
								imagelabelclass : "",
								imagelabeldata : data.string.person6,
							},
						]
					},
					{
						pairblockcontainer:'order_3',
						pairblock:[{
								elementcontainer:'blink kaka',
								imgclass: '',
								imgsrc : imgpath + "kaka.png",
								imagelabelclass : "",
								imagelabeldata : data.string.person7,
							},
							{
								elementcontainer:'blink kaki',
								imgclass: '',
								imgsrc : imgpath + "kaki.png",
								imagelabelclass : "",
								imagelabeldata : data.string.person8,
							},
						]
					},
					
				]				
			},
			{
				familyblockcontainer: 'order_3',
				familyblockdiv:[
					{
						pairblockcontainer:'order_1',
						pairblock:[
							{
								elementcontainer:'blink dai',
								imgclass: '',
								imgsrc : imgpath + "dai.png",
								imagelabelclass : "",
								imagelabeldata : data.string.person9,
							},
							{
								elementcontainer:'blink bhauju',
								imgclass: '',
								imgsrc : imgpath + "bhauju.png",
								imagelabelclass : "",
								imagelabeldata : data.string.person10,
							},
							{
								elementcontainer:'blink me',
								imgclass: '',
								imgsrc : imgpath + "me.png",
								imagelabelclass : "",
								imagelabeldata : data.string.person11,
							}
						]
					}
				]				
			}
		],
		
		descriptionblockadditionalclass: 'item_label',
		descriptionblock : [{
			imgclass : 'description_image',
			imgsrc : imgpath + 'grandpa01.png',
			titlediv: '',
			titleclass:'title_text',
			titledata: data.string.person1,
			textclass:'description_text',
			textdata:data.string.work1,
		}],
	},	
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'shore',
		
		uppertextblockadditionalclass: 'introduction_4',
		uppertextblock : [{
			textdata : data.string.p2text2,
			textclass : ''
		}],
		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "sound_icon sound_icon_2",
				imgsrc : imgpath + "speaker.png",
			},
			{
				imgclass : "meera_1",
				imgsrc : imgpath + "me01.png",
			}],
		}]
	},
];

var image_description = ['grandpa01.png', 'grandma01.png', 'thulobuwa01.png', 'thuliaama01.png', 'dad01.png', 'mum01.png', 'kaka01.png', 'kaki01.png', 'dai01.png', 'bhauju01.png', 'me01.png'];
var title_description = [data.string.person1, data.string.person2, data.string.person3, data.string.person4, data.string.person5, data.string.person6, data.string.person7, data.string.person8, data.string.person9, data.string.person10, data.string.person11];
var text_description = [data.string.work1, data.string.work2, data.string.work3, data.string.work4, data.string.work5, data.string.work6, data.string.work7, data.string.work8, data.string.work9, data.string.work10, data.string.work11];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	var last_page = false;
	var not_next_slide = 0;
	loadTimelineProgress($total_page, countNext + 1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			 // $nextBtn.show(0);
			 $prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);
		
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch (countNext) {
		case 0:
			// $nextBtn.show(0);
			current_sound = sound_title;
			play_sound(current_sound);
			break;
		case 1:
			var $current_item;
			var class_name;
			current_sound = sound_family;
			play_sound(current_sound);
			var go_next_counter = [0,0,0, 0,0,0, 0,0,0, 0,0];
			$('.elementcontainer').click(function(){
				class_name = $(this).attr('class').toString().replace(/elementcontainer/g, '');
				class_name = class_name.replace(/blink/g, '');
				class_name = class_name.replace(/ /g, '');
				switch(class_name){
					case 'grandpa':
						$('.description_image').attr('src', imgpath + image_description[0]);
						$('.description_text').html(text_description[0]);
						$('.title_text').html(title_description[0]);
						current_sound = sound_grandpa;
						go_next_counter[0] = 1;
					break;
					case 'grandma':
						$('.description_image').attr('src', imgpath + image_description[1]);
						$('.description_text').html(text_description[1]);
						$('.title_text').html(title_description[1]);
						current_sound = sound_grandma;
						go_next_counter[1] = 1;
					break;
					case 'thulobuwa':
						$('.description_image').attr('src', imgpath + image_description[2]);
						$('.description_text').html(text_description[2]);
						$('.title_text').html(title_description[2]);
						current_sound = sound_thulobuwa;
						go_next_counter[2] = 1;
					break;
					case 'thuliaama':
						$('.description_image').attr('src', imgpath + image_description[3]);
						$('.description_text').html(text_description[3]);
						$('.title_text').html(title_description[3]);
						current_sound = sound_thuliaama;
						go_next_counter[3] = 1;
					break;
					case 'dad':
						$('.description_image').attr('src', imgpath + image_description[4]);
						$('.description_text').html(text_description[4]);
						$('.title_text').html(title_description[4]);
						current_sound = sound_dad;
						go_next_counter[4] = 1;
					break;
					case 'mom':
						$('.description_image').attr('src', imgpath + image_description[5]);
						$('.description_text').html(text_description[5]);
						$('.title_text').html(title_description[5]);
						current_sound = sound_mom;
						go_next_counter[5] = 1;
					break;
					case 'kaka':
						$('.description_image').attr('src', imgpath + image_description[6]);
						$('.description_text').html(text_description[6]);
						$('.title_text').html(title_description[6]);
						current_sound = sound_kaka;
						go_next_counter[6] = 1;
					break;
					case 'kaki':
						$('.description_image').attr('src', imgpath + image_description[7]);
						$('.description_text').html(text_description[7]);
						$('.title_text').html(title_description[7]);
						current_sound = sound_kaki;
						go_next_counter[7] = 1;
					break;
					case 'dai':
						$('.description_image').attr('src', imgpath + image_description[8]);
						$('.description_text').html(text_description[8]);
						$('.title_text').html(title_description[8]);
						current_sound = sound_dai;
						go_next_counter[8] = 1;
					break;
					case 'bhauju':
						$('.description_image').attr('src', imgpath + image_description[9]);
						$('.description_text').html(text_description[9]);
						$('.title_text').html(title_description[9]);
						current_sound = sound_bhauju;
						go_next_counter[9] = 1;
					break;
					case 'me':
						$('.description_image').attr('src', imgpath + image_description[10]);
						$('.description_text').html(text_description[10]);
						$('.title_text').html(title_description[10]);
						current_sound = sound_me;
						go_next_counter[10] = 1;
					break;
				}
				$('.elementcontainer').removeClass('blink');
				$('.' + class_name).data("clicked",true);
				$current_item = $(this);
				$('.descriptionblock').show(0);
				play_sound(current_sound);
				vocabcontroller.findwords(countNext);
			});
			$('.wrong_class').click(function(){
				current_sound.stop();
				$('.descriptionblock').hide(0);
				$('.elementcontainer').css('pointer-events', 'all');
				$('sound_title').attr('src', imgpath +"speaker.png");
				for(var i=0; i<11; i++){
					var $newEntry = $(".pairblock>div").eq(i);
					if($newEntry.data("clicked")==false){
						$newEntry.addClass('blink');
					}
				}   
				for(var i =0; i< 11; i++){
					if(go_next_counter[i]==0){
						return false;
					}
				}
				$nextBtn.show(0);
			});
			$('.sound_title').click(function(){
				current_sound.stop();
				current_sound.play();
				$prevBtn.hide(0);
				// $('.wrong_class').css('pointer-events', 'none');
				$('sound_title').attr('src', imgpath +"speaker.gif");
				current_sound.bindOnce('ended', function(){
					$('sound_title').attr('src', imgpath +"speaker.png");
					// $('.wrong_class').css('pointer-events', 'all');
					$prevBtn.show(0);
				});
			});
			break;
		case 2:
			current_sound = sound_your_family;
			play_sound(current_sound);
		break;
		default:
			$prevBtn.show(0);
			$nextBtn.show(0);
			break;
		}
		$('.sound_icon').click(function(){
			current_sound.play();
			$nextBtn.hide(0);
			$prevBtn.hide(0);
			// $('.wrong_class').css('pointer-events', 'none');
			$('.elementcontainer').css('pointer-events', 'none');
			$('sound_icon').attr('src', imgpath +"speaker.gif");
			current_sound.bindOnce('ended', function(){
				$('sound_title').attr('src', imgpath +"speaker.png");
				$('.elementcontainer').css('pointer-events', 'all');
				// $('.wrong_class').css('pointer-events', 'all');
				if(countNext == $total_page-1){
					ole.footerNotificationHandler.pageEndSetNotification();
					$prevBtn.show(0);
				}
			});
		});
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   
	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				countNext++;
				templateCaller();
			break;
		}	
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();
	
	function next_btn_caller (next_display) {
		if(!next_display) {
			return false;
		} else {
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
	}
	
	function play_sound(sound_name){
		sound_name.play();
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		// $('.wrong_class').css('pointer-events', 'none');
		$('.elementcontainer').css('pointer-events', 'none');
		$('sound_icon').attr('src', imgpath +"speaker.gif");
		sound_name.bindOnce('ended', function(){
			$('sound_title').attr('src', imgpath +"speaker.png");
			$('.elementcontainer').css('pointer-events', 'all');
			// $('.wrong_class').css('pointer-events', 'all');
			if(countNext == $total_page-1){
				ole.footerNotificationHandler.pageEndSetNotification();
				$prevBtn.show(0);
			} else if(countNext == 0){
				$nextBtn.show(0);
			}
			if(countNext != 0 ){
				$prevBtn.show(0);
			}
		});
	}
	
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

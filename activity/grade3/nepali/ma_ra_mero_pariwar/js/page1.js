var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/page1/";

var sound_meera = new buzz.sound((soundAsset + "meera.ogg"));
var sound_home= new buzz.sound((soundAsset + "home.ogg"));
var sound_room = new buzz.sound((soundAsset + "room.ogg"));
var sound_instruction = new buzz.sound((soundAsset + "instruction.ogg"));
var sound_fan = new buzz.sound((soundAsset + "fan.ogg"));
var sound_window = new buzz.sound((soundAsset + "window.ogg"));
var sound_bed = new buzz.sound((soundAsset + "bed.ogg"));
var sound_teddy = new buzz.sound((soundAsset + "teddy.ogg"));
var sound_laptop = new buzz.sound((soundAsset + "laptop.ogg"));
var sound_brush = new buzz.sound((soundAsset + "brush.ogg"));
var sound_globe = new buzz.sound((soundAsset + "globe.ogg"));
var sound_table = new buzz.sound((soundAsset + "table.ogg"));
var sound_rack = new buzz.sound((soundAsset + "rack.ogg"));
var sound_cupboard = new buzz.sound((soundAsset + "cupboard.ogg"));

var sound_your_room = new buzz.sound((soundAsset + "your_room.ogg"));
var sound_1 = new buzz.sound((soundAsset + "s1_p1.ogg"));
var sound_2 = new buzz.sound((soundAsset + "s1_p2.ogg"));


var current_sound = sound_fan;

// var sound_group_b = [sound_ques, sound_3_00, sound_5_30, sound_6_00, sound_7_00, sound_8_30, sound_12_30];

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		uppertextblockadditionalclass: 'lesson_title',
		uppertextblock : [{
			textdata : data.lesson.chapter,
		}],
		imageblock:[{
			imagestoshow:[{
				imgsrc:imgpath+'cover_page.png',
				imgclass:'bg_full'
			}]
		}]
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'shore',

		uppertextblockadditionalclass: 'introduction_4',
		uppertextblock : [{
			textdata : data.string.p1text0,
			textclass : ''
		}],
		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "meera_1",
				imgsrc : imgpath + "me01.png",
			}],
		}]
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass : 'bg_house',

		uppertextblockadditionalclass: 'introduction_1',
		uppertextblock : [{
			textdata : data.string.p1text1,
			textclass : ''
		}],
		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "sound_icon",
				imgsrc : imgpath + "speaker.png",
			},
			// {
				// imgclass : "house",
				// imgsrc : imgpath + "myhosue.jpg",
			// }
			],
		}]
	},
	//slide3
	// {
		// hasheaderblock : false,
		// contentblocknocenteradjust : true,
//
		// contentblockadditionalclass : 'bg_house',
//
		// uppertextblockadditionalclass: 'introduction_1',
		// uppertextblock : [{
			// textdata : data.string.p1text2,
			// textclass : ''
		// }],
		// imageblockadditionalclass : '',
		// imageblock : [{
			// imagestoshow : [
				// {
					// imgclass : "sound_icon",
					// imgsrc : imgpath + "speaker.png",
				// },
			// // {
				// // imgclass : "house",
				// // imgsrc : imgpath + "myhosue.jpg",
			// // }
			// ],
		// }]
	// },

	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_room',

		uppertextblockadditionalclass: 'introduction_2',
		uppertextblock : [{
			textdata : data.string.p1text3,
			textclass : ''
		}],

		lowertextblockadditionalclass: 'item_label',
		lowertextblock : [{
			textdata : data.string.p1text4,
			textclass : 'labels'
		}],
		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "item_room blink table",
				imgsrc : imgpath + "table.png",
			},
			{
				imgclass : "item_room blink bed",
				imgsrc : imgpath + "bed.png",
			},
			{
				imgclass : "item_room blink window",
				imgsrc : imgpath + "window.png",
			},
			{
				imgclass : "item_room blink paintbrush",
				imgsrc : imgpath + "paintingbrush.png",
			},
			{
				imgclass : "item_room blink teddy",
				imgsrc : imgpath + "toy.png",
			},
			{
				imgclass : "item_room blink laptop",
				imgsrc : imgpath + "xo.png",
			},
			{
				imgclass : "item_room blink globe",
				imgsrc : imgpath + "globe.png",
			},
			{
				imgclass : "item_room blink rack",
				imgsrc : imgpath + "bookrack.png",
			},
			{
				imgclass : "item_room blink fan",
				imgsrc : imgpath + "fan.png",
			},
			{
				imgclass : "item_room blink cupboard",
				imgsrc : imgpath + "daraj.png",
			},
			{
				imgclass : "wrong_icon",
				imgsrc : "images/wrong.png",
			},
			{
				imgclass : "sound_icon",
				imgsrc : imgpath + "speaker.png",
			}],
		imagelabels:[
			{
				imagelabelclass : "black_cover",
				imagelabeldata : '',
			}]
		}],
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'shore',

		uppertextblockadditionalclass: 'introduction_4',
		uppertextblock : [{
			textdata : data.string.p1text14,
			textclass : ''
		}],
		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "sound_icon sound_icon_2",
				imgsrc : imgpath + "speaker.png",
			},
			{
				imgclass : "meera_1",
				imgsrc : imgpath + "me01.png",
			}],
		}]
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	var last_page = false;
	var not_next_slide = 0;
	loadTimelineProgress($total_page, countNext + 1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			 // $nextBtn.show(0);
			 $prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch (countNext) {
		case 0:
			$nextBtn.hide(0);
			sound_1.play();
			sound_1.bindOnce('ended', function(){
				$nextBtn.show(0);
			});
			break;
		case 1:
			$nextBtn.hide(0);
			sound_2.play();
			sound_2.bindOnce('ended', function(){
				$nextBtn.show(0);
				$prevBtn.show(0);
			});
			break;
		case 2:
			current_sound = sound_home;
			play_sound(current_sound);
			break;
		// case 3:
			// current_sound = sound_room;
			// play_sound(current_sound);
			// break;
		case 3:
			var go_next_counter = [0,0,0, 0,0,0, 0,0,0];
			var $current_item;
			var class_name;
			current_sound = sound_instruction;
			play_sound(current_sound);
			$('.wrong_icon').data("clicked",true);
			$('.sound_icon').data("clicked",true);
			$nextBtn.hide(0);
			$('.item_room').click(function(){
				current_sound.stop();
				$('.item_room').removeClass('blink');
				class_name = $(this).attr('class').toString().replace(/item_room/g, '');
				class_name = class_name.replace(/blink/g, '');
				class_name = class_name.replace(/ /g, '');
				switch(class_name){
					case 'fan':
						$('.item_label>p').html(data.string.p1text4);
						current_sound = sound_fan;
						go_next_counter[0] = 1;
					break;
					case 'window':
						$('.item_label>p').html(data.string.p1text5);
						current_sound = sound_window;
						go_next_counter[1] = 1;
					break;
					case 'bed':
						$('.item_label>p').html(data.string.p1text6);
						current_sound = sound_bed;
						go_next_counter[2] = 1;
					break;
					case 'teddy':
						$('.item_label>p').html(data.string.p1text7);
						current_sound = sound_teddy;
						go_next_counter[3] = 1;
					break;
					case 'laptop':
						$('.item_label>p').html(data.string.p1text8);
						current_sound = sound_laptop;
						go_next_counter[4] = 1;
					break;
					case 'globe':
						$('.item_label>p').html(data.string.p1text9);
						current_sound = sound_globe;
						go_next_counter[5] = 1;
					break;
					case 'paintbrush':
						$('.item_label>p').html(data.string.p1text10);
						current_sound = sound_brush;
						go_next_counter[6] = 1;
					break;
					case 'table':
						$('.item_label>p').html(data.string.p1text11);
						current_sound = sound_table;
						go_next_counter[7] = 1;
					break;
					case 'rack':
						$('.item_label>p').html(data.string.p1text12);
						current_sound = sound_rack;
						go_next_counter[8] = 1;
					break;
					case 'cupboard':
						$('.item_label>p').html(data.string.p1text13);
						current_sound = sound_cupboard;
						go_next_counter[9] = 1;
					break;
				}
				// alert($('.' + class_name).data("clicked"));
				$('.' + class_name).data("clicked",true);
				$current_item = $(this);
				$(this).addClass('to_center');
				$(this).css({
					'z-index': '50',
					'animation': 'nothing 0s',
					'-webkit-animation': 'nothing 0s'
				});
				$('.black_cover').show(0);
				$('.wrong_icon').show(0);
				$('.item_room').css('pointer-events', 'none');
				vocabcontroller.findwords(countNext);
				setTimeout(function(){
					$('.item_label').show(0);
					play_sound(current_sound);
				}, 900);
				setTimeout(function(){
					$('.item_label').show(0);
				}, 1100);
			});
			$('.wrong_icon').click(function(){
				current_sound = sound_instruction;
				$('.item_label').hide(0);
				$('.item_room').removeClass('to_center');
				// alert($('.' + class_name).data("clicked"));
				$('.black_cover').hide(0);
				$('.wrong_icon').hide(0);
				setTimeout(function(){
					$('.item_room').css('z-index', '5');
				}, 900);
				for(var i=0; i<11; i++){
					var $newEntry = $(".imageblock>img").eq(i);
					if($newEntry.data("clicked")==false){
						$newEntry.addClass('blink');
					}
				}
				setTimeout(function(){
					$('.item_room').css('pointer-events', 'all');
					for(var i =0; i< 10; i++){
						if(go_next_counter[i]==0){
							return false;
						}
						else{
						$nextBtn.show(0);
					}
				}
				}, 1100);
			});
			break;
		case 4:
			current_sound = sound_your_room;
			play_sound(current_sound);
			break;
		default:
			$prevBtn.show(0);
			$nextBtn.show(0);
			break;
		}

		$('.sound_icon').click(function(){
			current_sound.stop();
			current_sound.play();
			$nextBtn.hide(0);
			$prevBtn.hide(0);
			$('.item_room').css('pointer-events', 'none');
			// $('.wrong_icon').css('pointer-events', 'none');
			$('sound_icon').attr('src', imgpath +"speaker.gif");
			current_sound.bindOnce('ended', function(){
				$('sound_icon').attr('src', imgpath +"speaker.png");
				$('.item_room').css('pointer-events', 'all');
				// $('.wrong_icon').css('pointer-events', 'all');
				if(countNext != 4){
					$nextBtn.show(0);
				}
				$prevBtn.show(0);
				if(countNext == $total_page-1){
					ole.footerNotificationHandler.pageEndSetNotification();
					$prevBtn.show(0);
				}
			});
		});
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				current_sound.stop();
				countNext++;
				templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		current_sound.stop();
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();

	function next_btn_caller (next_display) {
		if(!next_display) {
			return false;
		} else {
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
	}

	function play_sound(sound_name){
		current_sound.stop();
		current_sound = sound_name;
		current_sound.play();
		$nextBtn.hide(0);
		// $prevBtn.hide(0);
		// $('.item_room').css('pointer-events', 'none');
		$('sound_icon').attr('src', imgpath +"speaker.gif");
		$('.wrong_icon').css('pointer-events', 'none');
		current_sound.bindOnce('ended', function(){
			$('sound_icon').attr('src', imgpath +"speaker.png");
			// $('.item_room').css('pointer-events', 'all');
			$('.wrong_icon').css('pointer-events', 'all');
			if(countNext < 3){
				$nextBtn.show(0);
			}
			$prevBtn.show(0);
			if(countNext == $total_page-1){
				ole.footerNotificationHandler.pageEndSetNotification();
				$prevBtn.show(0);
			}
		});
	}

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";

var sound_l_1 = new buzz.sound((soundAsset + "1.ogg"));
var sound_l_2 = new buzz.sound((soundAsset + "2.ogg"));
var sound_l_3 = new buzz.sound((soundAsset + "3.ogg"));
var sound_l_4 = new buzz.sound((soundAsset + "4.ogg"));
var sound_l_5 = new buzz.sound((soundAsset + "5.ogg"));
var sound_l_6 = new buzz.sound((soundAsset + "6.ogg"));
var sound_l_7 = new buzz.sound((soundAsset + "7.ogg"));
var sound_l_8 = new buzz.sound((soundAsset + "8.ogg"));
var sound_l_9 = new buzz.sound((soundAsset + "s1_p1.ogg"));

var soundcontent = [sound_l_1, sound_l_2, sound_l_3, sound_l_4, sound_l_5, sound_l_6, sound_l_7, sound_l_8];

var content=[
	{
		// slide0
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description_title",
			textdata: data.string.lesson_title
		},{
			textclass: "description1",
			textdata: data.string.writer
		}]
	},{
		// slide1
		contentblockadditionalclass: "additionalbackground1",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb",
		uppertextblock: [{
			textclass: "title",
			textdata: data.string.lesson_title
		}],
		storytextblockadditionalclass: "  ltb_top_left_40",
		storytextblock:[{
			textclass: "textblack",
			textdata: data.string.para1_stanza1
		} ]

	},{
		// slide2
		contentblockadditionalclass: "additionalbackground1",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb",
		uppertextblock: [{
			textclass: "title",
			textdata: data.string.lesson_title
		}],
		storytextblockadditionalclass: "  ltb_top_left_40",
		storytextblock:[{
			textclass: "textblack",
			textdata: data.string.para1_stanza2
		} ]

	},{
		// slide3
		contentblockadditionalclass: "additionalbackground1",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb",
		uppertextblock: [{
			textclass: "title",
			textdata: data.string.lesson_title
		}],
		storytextblockadditionalclass: "  ltb_top_left_40",
		storytextblock:[{
			textclass: "textblack",
			textdata: data.string.para2_stanza1
		} ]

	},{
		// slide4
		contentblockadditionalclass: "additionalbackground1",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb",
		uppertextblock: [{
			textclass: "title",
			textdata: data.string.lesson_title
		}],
		storytextblockadditionalclass: "  ltb_top_left_40",
		storytextblock:[{
			textclass: "textblack",
			textdata: data.string.para2_stanza2
		} ]

	},{
		// slide5
		contentblockadditionalclass: "additionalbackground1",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb",
		uppertextblock: [{
			textclass: "title",
			textdata: data.string.lesson_title
		}],
		storytextblockadditionalclass: "  ltb_top_left_40",
		storytextblock:[{
			textclass: "textblack",
			textdata: data.string.para3_stanza1
		} ]

	},{
		// slide6
		contentblockadditionalclass: "additionalbackground1",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb",
		uppertextblock: [{
			textclass: "title",
			textdata: data.string.lesson_title
		}],
		storytextblockadditionalclass: "  ltb_top_left_40",
		storytextblock:[{
			textclass: "textblack",
			textdata: data.string.para3_stanza2
		} ]

	},{
		// slide7
		contentblockadditionalclass: "additionalbackground1",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb",
		uppertextblock: [{
			textclass: "title",
			textdata: data.string.lesson_title
		}],
		storytextblockadditionalclass: "  ltb_top_left_40",
		storytextblock:[{
			textclass: "textblack",
			textdata: data.string.para4_stanza1
		} ]

	},{
		// slide8
		contentblockadditionalclass: "additionalbackground1",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb",
		uppertextblock: [{
			textclass: "title",
			textdata: data.string.lesson_title
		}],
		storytextblockadditionalclass: "  ltb_top_left_40",
		storytextblock:[{
			textclass: "textblack",
			textdata: data.string.para4_stanza2
		} ]

	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var isFirefox = typeof InstallTrigger !== 'undefined';
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			// $nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  var animationinprogress = false;

  var sound_data;

  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
		// splitintofractions($(".fractionblock"));
	    switch(countNext){
				case 0:
				$nextBtn.hide(0);
				sound_l_9.play();
				sound_l_9.bindOnce('ended', function(){
					$nextBtn.show(0);
				});
				break;
	    	case 1:
				for_all_slides('textblack', soundcontent[0], false);
	    		break;
	    	case 2:
				for_all_slides('textblack', soundcontent[1], false);
	    		break;
	    	case 3:
				for_all_slides('textblack', soundcontent[2], false);
	    		break;
	    	case 4:
				for_all_slides('textblack', soundcontent[3], false);
	    		break;
	    	case 5:
				for_all_slides('textblack', soundcontent[4], false);
	    		break;
	    	case 6:
				for_all_slides('textblack', soundcontent[5], false);
	    		break;
	    	case 7:
				for_all_slides('textblack', soundcontent[6], false);
	    		break;
	    	case 8:
				for_all_slides('textblack', soundcontent[7], true);
	    		break;
	    	default:
	    		break;
	    }
function for_all_slides(text_class, my_sound_data, last_page_flag){
			var $textblack = $("."+text_class);
			sound_data =  my_sound_data;
			var current_text = $textblack.html();
			// current_text.replace(/<.*>/, '');
			play_text($textblack, current_text);
    		sound_data.bindOnce('ended', function(){
    			change_slides(last_page_flag, text_class);
			});
			$("."+text_class).click(function(){
				sound_data.play();
				$("."+text_class).css('pointer-events','none');
				$(".speaker").css('pointer-events','none');
				$('#span_speec_text').addClass('is_playing');
    			sound_data.bindOnce('ended', function(){
	    			$prevBtn.show(0);
    				if(countNext!=content.length-1){
	    				$nextBtn.show(0);
		    		} else{
		    			ole.footerNotificationHandler.pageEndSetNotification();
		    		}
					$("."+text_class).css('pointer-events','all');
					$(".speaker").css('pointer-events','all');
					$('#span_speec_text').removeClass('is_playing');
				});
			});
		}
		function change_slides(last_page_flag, text_class){
			var checking_interval = setInterval(function(){
				if(textanimatecomplete){
					if(!last_page_flag){
	    				$nextBtn.show(0);
		    		} else{
		    			ole.footerNotificationHandler.pageEndSetNotification();
		    		}
	    			$prevBtn.show(0);
					$("."+text_class).css('pointer-events','all');
					$(".speaker").css('pointer-events','all');
       				vocabcontroller.findwords(countNext);
					textanimatecomplete = false;
	    			clearInterval(checking_interval);
				} else{
					$("."+text_class).css('pointer-events','none');
					$(".speaker").css('pointer-events','none');
					$prevBtn.hide(0);
					$nextBtn.hide(0);
				}
			},50);
		}
	}
	/* For typing animation appends the text to an element specified by target class or id */
	function show_text($this,  $span_speec_text, message, interval) {
		if (0 < message.length && !textanimatecomplete) {
			var nextText = message.substring(0, 1);
			var additionalinterval = 0;
			if (nextText == "<") {
				additionalinterval = 800;
				$span_speec_text.append("<br>");
				message = message.substring(4, message.length);
			} else {
				$span_speec_text.append(nextText);
				message = message.substring(1, message.length);
			}
			$this.html($span_speec_text);
			$this.append(message);
			setTimeout(function() {
				show_text($this, $span_speec_text, message, interval);
			}, (interval + additionalinterval));
		} else{
	  		textanimatecomplete = true;
	  	}
	}

	var intervalid;
	var soundplaycomplete = false;
	var textanimatecomplete = false;
	var original_text;
	// uses the show text to add typing effect with sound and glowing animations
	function play_text($this, text){
		original_text =  text;
		if(isFirefox){
			$this.html("<span id='span_speec_text'></span>"+text);
			$prevBtn.hide(0);
			var $span_speec_text = $("#span_speec_text");
			// $this.css("background-color", "#faf");
			show_text($this, $span_speec_text,text, 155);	// 65 ms is the interval found out by hit and trial
			sound_data.play();
			sound_data.bind('ended', function(){
				sound_data.unbind('ended');
				soundplaycomplete = true;
				ternimatesound_play_animate(text);
			});
		} else {
			$this.html("<span id='span_speec_text'>"+original_text+"</span>");
			sound_data.play();
			sound_data.bind('ended', function(){
				sound_data.unbind('ended');
				soundplaycomplete = true;
				textanimatecomplete =  true;
				animationinprogress = false;
			});
		}

		function ternimatesound_play_animate(text){
			 intervalid = setInterval(function () {
				if(textanimatecomplete && soundplaycomplete){
					$this.html($span_speec_text.html(original_text));
					$this.css("background-color", "transparent");
					clearInterval(intervalid);
					intervalid = null;
					animationinprogress = false;
				}
			}, 250);
		}
	}


/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
		if(sound_data != null){
			sound_data.stop();
			sound_data.unbind('ended');
		}
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		if(sound_data != null){
			sound_data.stop();
			sound_data.unbind('ended');
		}
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

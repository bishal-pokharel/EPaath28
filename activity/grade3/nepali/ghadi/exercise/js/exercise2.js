var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/";
var sound_1 = new buzz.sound((soundAsset + "ex2.ogg"));

var content=[	
	
	//ex1
	{
		exerciseblock: [
			{	
				instructiondata: data.string.ex_ins_1,
				textdata: data.string.e1_q,
				datahighlightflag : true,
				datahighlightcustomclass : 'blank_space',
				
				sideimage: [{
					imgsrc : imgpath+'clock.png'
				}],
				
				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.e1_1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.e1_2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.e1_3,
					},
					{
						forshuffle: "class4",
						optdata: data.string.e1_4,
				}],
			}
		]
	},
	//ex2
	{
		exerciseblock: [
			{	
				instructiondata: data.string.ex_ins_1,
				textdata: data.string.e2_q,
				datahighlightflag : true,
				datahighlightcustomclass : 'blank_space',
				
				sideimage: [{
					imgsrc : imgpath+'clock.png'
				}],
				
				
				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.e2_1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.e2_2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.e2_3,
					},
					{
						forshuffle: "class4",
						optdata: data.string.e2_4,
				}],
			}
		]
	},
	//ex3
	{
		exerciseblock: [
			{	
				instructiondata: data.string.ex_ins_1,
				textdata: data.string.e3_q,
				datahighlightflag : true,
				datahighlightcustomclass : 'blank_space',
				
				
				sideimage: [{
					imgsrc : imgpath+'clock.png'
				}],
				
				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.e3_1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.e3_2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.e3_3,
					},
					{
						forshuffle: "class4",
						optdata: data.string.e3_4,
				}],
			}
		]
	},
	//ex4
	{
		exerciseblock: [
			{	
				instructiondata: data.string.ex_ins_1,
				textdata: data.string.e4_q,
				datahighlightflag : true,
				datahighlightcustomclass : 'blank_space',
				
				
				sideimage: [{
					imgsrc : imgpath+'clock.png'
				}],
				
				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.e4_1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.e4_2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.e4_3,
					},
					{
						forshuffle: "class4",
						optdata: data.string.e4_4,
				}],
			}
		]
	},
	//ex5
	{
		exerciseblock: [
			{	
				instructiondata: data.string.ex_ins_1,
				textdata: data.string.e5_q,
				datahighlightflag : true,
				datahighlightcustomclass : 'blank_space',
				
				
				sideimage: [{
					imgsrc : imgpath+'clock.png'
				}],
				
				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.e5_1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.e5_2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.e5_3,
					},
					{
						forshuffle: "class4",
						optdata: data.string.e5_4,
				}],
			}
		]
	},
	//ex6
	{
		exerciseblock: [
			{	
				instructiondata: data.string.ex_ins_1,
				textdata: data.string.e6_q,
				datahighlightflag : true,
				datahighlightcustomclass : 'blank_space',
				
				
				sideimage: [{
					imgsrc : imgpath+'clock.png'
				}],
				
				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.e6_1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.e6_2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.e6_3,
					},
					{
						forshuffle: "class4",
						optdata: data.string.e6_4,
				}],
			}
		]
	},
	//ex7
	{
		exerciseblock: [
			{	
				instructiondata: data.string.ex_ins_1,
				textdata: data.string.e7_q,
				datahighlightflag : true,
				datahighlightcustomclass : 'blank_space',
				
				sideimage: [{
					imgsrc : imgpath+'clock.png'
				}],
				
				
				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.e7_1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.e7_2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.e7_3,
					},
					{
						forshuffle: "class4",
						optdata: data.string.e7_4,
				}],
			}
		]
	},
	//ex8
	{
		exerciseblock: [
			{	
				instructiondata: data.string.ex_ins_1,
				textdata: data.string.e8_q,
				datahighlightflag : true,
				datahighlightcustomclass : 'blank_space',
				
				
				sideimage: [{
					imgsrc : imgpath+'clock.png'
				}],
				
				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.e8_1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.e8_2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.e8_3,
					},
					{
						forshuffle: "class4",
						optdata: data.string.e8_4,
				}],
			}
		]
	},
];

/*remove this for non random questions*/
content.shufflearray();


$(function () {	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	/*for limiting the questions to 10*/
	var $total_page = 10;
    loadTimelineProgress($total_page, countNext + 1);

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	var score = 0;
	var testin = new EggTemplate();
   
 	testin.init(8);
	/*values in this array is same as the name of images of eggs in image folder*/
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		
		$nextBtn.hide(0);
		$prevBtn.hide(0);
        countNext==0?sound_1.play():"";

        /*generate question no at the beginning of question*/
		testin.numberOfQuestions();
		texthighlight($board);
		
		var option_position = [1,2,3,4];
		option_position.shufflearray();
		for(var op=0; op<4; op++){
			$('.optionsdiv>span').eq(op).addClass('child-'+option_position[op]);
		}

		/*for randomizing the options*/
		var parent = $(".optionsdiv");
		var divs = parent.children();
			 while (divs.length) {
			        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			  }

		var ansClicked = false;
		var wrngClicked = false;

		$(".buttonsel").click(function(){
			$(this).addClass('selected_option');
				if(ansClicked == false){

					/*class 1 is always for the right answer. updates scoreboard and disables other click if
					right answer is clicked*/
					if($(this).hasClass("class1")){
						// $('.blank_space').css('opacity', 0);
						$(".buttonsel").addClass('selected_option');
						var blank_space_data = $(this).html();
						var box_left = ( $('.question').position().left + $('.blank_space').position().left )*100/$board.width() +'%';
						var box_top = ($('.question').position().top + $('.blank_space').position().top)*100/$board.height() +'%';
						
						var init_left = ($('.optionsdiv').position().left + $(this).position().left)*100/$board.width() +'%';
						var init_top = ($('.optionsdiv').position().top + $(this).position().top)*100/$board.height() +'%';
						
						$(this).detach().css({
							'left': init_left,
							'top': init_top,
							'z-index': '100',
							'color': '#F2D966',
						}).appendTo('.contentblock');
						
						$(this).animate({
							'left': box_left,
							'top': box_top,
						},1000, function(){
							$(this).hide(0);
							$('.blank_space').addClass('blank_space_filled');
							$('.blank_space').html(blank_space_data);
							$('.blank_space').css('color', '#EF6C55');
							$('.question').append('<div class="right_sign_back"></div>');
							$nextBtn.show(0);
							// $('.blank_space').css('opacity', 1);
						});
						// $(this).removeClass('[class|="child"]');
						if(wrngClicked == false){
							testin.update(true);
						}
						
						play_correct_incorrect_sound(1);
						$('.buttonsel').removeClass('forhover forhoverimg');
						ansClicked = true;

						// if(countNext != $total_page)
						// $nextBtn.show(0);
					}
					else{
						testin.update(false);
						play_correct_incorrect_sound(0);
						$(this).css({
							'text-decoration': 'line-through'
						});
						wrngClicked = true;
					}
				}
			}); 
		
		/*======= SCOREBOARD SECTION ==============*/
	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');		

		// call navigation controller
		navigationcontroller();	

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   

	}

	// first call to template caller
	templateCaller();	

	/* navigation buttons event handlers */
	
	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();
		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
			previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}
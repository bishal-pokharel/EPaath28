var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	{
		// slide0
		contentnocenteradjust: true,
		imageblock:[{
			imagestoshow:[{
				imgclass: "image_bg",
				imgsrc: "",
				imgid: "intro_image"
			}]
		}]
	},{
		// slide1
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "flickering_textblock",
		uppertextblock: [{
			textclass: "description",
			textdata: data.string.stanza1
		}],
		imageblock:[{
			imageblockclass: "image_ans",
			imagestoshow:[{
				imgclass: "image_100",
				imgsrc: "",
				imgid: "eyes-50"
			}],
			imagelabels:[{
				imagelabelclass: "image_label_100",
				imagelabeldata: data.string.stanza1_answer
			},{
				imagelabelclass: "image_label_100b",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "image_label_100c",
				imagelabeldata: data.string.answer
			}]
		},{
			imagestoshow:[{
				imgclass: "image_ans_final",
				imgsrc: "",
				imgid: "eyes-51"
			}]
		}]
	},{
		// slide2
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "flickering_textblock",
		uppertextblock: [{
			textclass: "description",
			textdata: data.string.stanza2
		}],
		imageblock:[{
			imageblockclass: "image_ans",
			imagestoshow:[{
				imgclass: "image_100",
				imgsrc: "",
				imgid: "crying-47"
			}],
			imagelabels:[{
				imagelabelclass: "image_label_100",
				imagelabeldata: data.string.stanza2_answer
			},{
				imagelabelclass: "image_label_100b",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "image_label_100c",
				imagelabeldata: data.string.answer
			}]
		},{
			imagestoshow:[{
				imgclass: "image_ans_final",
				imgsrc: "",
				imgid: "crying-48"
			}]
		}]
	},{
		// slide3
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "flickering_textblock",
		uppertextblock: [{
			textclass: "description",
			textdata: data.string.stanza3
		}],
		imageblock:[{
			imageblockclass: "image_ans",
			imagestoshow:[{
				imgclass: "image_100",
				imgsrc: "",
				imgid: "jumra-36"
			}],
			imagelabels:[{
				imagelabelclass: "image_label_100",
				imagelabeldata: data.string.stanza3_answer
			},{
				imagelabelclass: "image_label_100b",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "image_label_100c",
				imagelabeldata: data.string.answer
			}]
		},{
			imagestoshow:[{
				imgclass: "image_ans_final",
				imgsrc: "",
				imgid: "jumra-37"
			}]
		}]
	},{
		// slide4
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "flickering_textblock",
		uppertextblock: [{
			textclass: "description",
			textdata: data.string.stanza4
		}],
		imageblock:[{
			imageblockclass: "image_ans",
			imagestoshow:[{
				imgclass: "image_100",
				imgsrc: "",
				imgid: "pigeon-44"
			}],
			imagelabels:[{
				imagelabelclass: "image_label_100",
				imagelabeldata: data.string.stanza4_answer
			},{
				imagelabelclass: "image_label_100b",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "image_label_100c",
				imagelabeldata: data.string.answer
			}]
		},{
			imagestoshow:[{
				imgclass: "image_ans_final",
				imgsrc: "",
				imgid: "pigeon-45"
			}]
		}]
	},{
		// slide5
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "flickering_textblock",
		uppertextblock: [{
			textclass: "description",
			textdata: data.string.stanza5
		}],
		imageblock:[{
			imageblockclass: "image_ans",
			imagestoshow:[{
				imgclass: "image_100",
				imgsrc: "",
				imgid: "needle_thread-32"
			}],
			imagelabels:[{
				imagelabelclass: "image_label_100",
				imagelabeldata: data.string.stanza5_answer
			},{
				imagelabelclass: "image_label_100b",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "image_label_100c",
				imagelabeldata: data.string.answer
			}]
		},{
			imagestoshow:[{
				imgclass: "image_ans_final",
				imgsrc: "",
				imgid: "needle_thread-33"
			}]
		}]
	},{
		// slide6
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "flickering_textblock",
		uppertextblock: [{
			textclass: "description",
			textdata: data.string.stanza6
		}],
		imageblock:[{
			imageblockclass: "image_ans",
			imagestoshow:[{
				imgclass: "image_100",
				imgsrc: "",
				imgid: "mouth-34"
			}],
			imagelabels:[{
				imagelabelclass: "image_label_100",
				imagelabeldata: data.string.stanza6_answer
			},{
				imagelabelclass: "image_label_100b",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "image_label_100c",
				imagelabeldata: data.string.answer
			}]
		},{
			imagestoshow:[{
				imgclass: "image_ans_final",
				imgsrc: "",
				imgid: "mouth-35"
			}]
		}]
	},{
		// slide7
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "flickering_textblock",
		uppertextblock: [{
			textclass: "description",
			textdata: data.string.stanza7
		}],
		imageblock:[{
			imageblockclass: "image_ans",
			imagestoshow:[{
				imgclass: "image_100",
				imgsrc: "",
				imgid: "sleeping-40"
			}],
			imagelabels:[{
				imagelabelclass: "image_label_100",
				imagelabeldata: data.string.stanza7_answer
			},{
				imagelabelclass: "image_label_100b",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "image_label_100c",
				imagelabeldata: data.string.answer
			}]
		},{
			imagestoshow:[{
				imgclass: "image_ans_final",
				imgsrc: "",
				imgid: "sleeping-41"
			}]
		}]
	}
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

  loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
	var aeroplanesound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg-1", src: imgpath+"textbox.png", type: createjs.AbstractLoader.IMAGE},
			{id: "intro_image", src: imgpath+"intro_image.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eyes-50", src: imgpath+"eyes-50.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eyes-51", src: imgpath+"eyes-51.png", type: createjs.AbstractLoader.IMAGE},
			{id: "crying-47", src: imgpath+"crying-47.png", type: createjs.AbstractLoader.IMAGE},
			{id: "crying-48", src: imgpath+"crying-48.png", type: createjs.AbstractLoader.IMAGE},
			{id: "jumra-36", src: imgpath+"jumra-36.png", type: createjs.AbstractLoader.IMAGE},
			{id: "jumra-37", src: imgpath+"jumra-37.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pigeon-44", src: imgpath+"pigeon-44.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pigeon-45", src: imgpath+"pigeon-45.png", type: createjs.AbstractLoader.IMAGE},
			{id: "needle_thread-32", src: imgpath+"needle_thread-32.png", type: createjs.AbstractLoader.IMAGE},
			{id: "needle_thread-33", src: imgpath+"needle_thread-33.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mouth-34", src: imgpath+"mouth-34.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mouth-35", src: imgpath+"mouth-35.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sleeping-40", src: imgpath+"sleeping-40.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sleeping-41", src: imgpath+"sleeping-41.png", type: createjs.AbstractLoader.IMAGE},
			{id: "clouds", src: imgpath+"clouds.svg", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"poemtitle.ogg"},
			{id: "sound_1", src: soundAsset+"line1.ogg"},
			{id: "sound_2", src: soundAsset+"line2.ogg"},
			{id: "sound_3", src: soundAsset+"line3.ogg"},
			{id: "sound_4", src: soundAsset+"line4.ogg"},
			{id: "sound_5", src: soundAsset+"line5.ogg"},
			{id: "sound_6", src: soundAsset+"line6.ogg"},
			{id: "sound_7", src: soundAsset+"line7.ogg"},
			// {id: "sound_2", src: soundAsset+"ding.ogg"},
			// {id: "sound_3", src: soundAsset+"ding.ogg"},
			// {id: "sound_4", src: soundAsset+"ding.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true

			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			// $prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			  // timeoutcontroller = setTimeout(function(){
					 // islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
			 // }, 2500);
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
 var timeoutcontroller;
 var intervalcontroller;
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    vocabcontroller.findwords(countNext);
		put_image(content, countNext);

		function common_procedure(colorcode1){
			$nextBtn.hide(0);
			$(".contentblock").css("background-color", colorcode1);
			$(".image_label_100b").css("background-color", colorcode1);
			$("[class^=image_ans]").delay(2500).show(0);
			//this is done because chrome gives z-index problems when using prespective
			var $image_ans2 = $(".image_ans");
			var $image_ans = $("[class^=image_ans]");
			$image_ans.click(function(){
				$(".image_ans_final").addClass("bookslideleft");
				$image_ans2.addClass("open_book_animation");
				timeoutcontroller = setTimeout(function(){
					if(countNext == $total_page - 1){
						ole.footerNotificationHandler.pageEndSetNotification();
					}else{
						$nextBtn.delay(3000).show(0);
					}
				}, 3000);

			});
		}

		sound_player("sound_"+ countNext);

		switch(countNext) {
			case 0:
				$nextBtn.delay(2000).show(0);
				break;
			case 1:
				common_procedure("#909DDD");
				break;
			case 2:
				common_procedure("#79F2E1");
				break;
			case 3:
				common_procedure("#D9E021");
				break;
			case 4:
				common_procedure("#FFC364");
				break;
			case 5:
				$(".image_label_100c").css("color", "#fff");
				common_procedure("#7869C4");
				break;
			case 6:
				$(".image_label_100c").css("color", "#fff");
				common_procedure("#FF6040");
				break;
			case 7:
				common_procedure("#2D8FE0");
				break;
			default:
				break;
		}

		// splitintofractions($(".fractionblock"));
  }

/*=====  End of Templates Block  ======*/


	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			for(var j = 0; j < content[count].imageblock.length; j++){
				var imageblock = content[count].imageblock[j];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		clearTimeout(timeoutcontroller);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	{
		//slide 1
		contentnocenteradjust: true,
		contentblockadditionalclass: "flickering_textblock",
		uppertextblocknocenteradjust: true,
		uppertextblock:[{
			textclass: "question",
			answer_option_idx: "1",
			textdata: data.string.question1
		}],
		imageblock:[{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover yes",
				imagelabeldata: data.string.q1_o1
			}],
			imagestoshow:[{
					imgclass: "image_correct",
					imgid: "correct"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.q1_o2
			}],
			imagestoshow:[{
					imgclass: "image",
					imgid: "incorrect"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.q1_o3
			}],
			imagestoshow:[{
					imgclass: "image",
					imgid: "incorrect"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.q1_o4
			}],
			imagestoshow:[{
					imgclass: "image",
					imgid: "incorrect"
			}]
		}],
		rewardblock:[{
			imagelabels:[{
				imagelabelclass: "instruction",
				imagelabeldata: data.string.village_title
			},{
				imagelabelclass: "reward_option reward_option_hover position1",
				reward_option_idx: 1,
				imagelabeldata: data.string.village1
			},{
				imagelabelclass: "reward_option reward_option_hover position2",
				reward_option_idx: 2,
				imagelabeldata: data.string.village2
			},{
				imagelabelclass: "reward_option reward_option_hover position3",
				reward_option_idx: 3,
				imagelabeldata: data.string.village3
			},{
				imagelabelclass: "reward_option reward_option_hover position4",
				reward_option_idx: 4,
				imagelabeldata: data.string.village4
			},{
				imagelabelclass: "reward_option reward_option_hover position5",
				reward_option_idx: 5,
				imagelabeldata: data.string.village5
			},{
				imagelabelclass: "reward_option reward_option_hover position6",
				reward_option_idx: 6,
				imagelabeldata: data.string.village6
			} ],
			imagestoshow:[{
					imgclass: "image_reward_bg",
					imgsrc: "",
					imgid: "bg_for_mcq"
			},{
					imgclass: "image_reward",
					imgsrc: "",
					imgid: "gau-deu-talking-gif-FOREVER"
			},{
					imgclass: "image_reward2",
					imgsrc: "",
					imgid: "gau-deu-talking-png"
			}]
		}]
	},
	{
		//slide 2
		contentnocenteradjust: true,
		contentblockadditionalclass: "flickering_textblock",
		uppertextblocknocenteradjust: true,
		uppertextblock:[{
			textclass: "question",
			answer_option_idx: "2",
			textdata: data.string.question2
		}],
		imageblock:[{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover yes",
				imagelabeldata: data.string.q2_o1
			}],
			imagestoshow:[{
					imgclass: "image_correct",
					imgid: "correct"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.q2_o2
			}],
			imagestoshow:[{
					imgclass: "image",
					imgid: "incorrect"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.q2_o3
			}],
			imagestoshow:[{
					imgclass: "image",
					imgid: "incorrect"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.q2_o4
			}],
			imagestoshow:[{
					imgclass: "image",
					imgid: "incorrect"
			}]
		}],
		rewardblock:[{
			imagelabels:[{
				imagelabelclass: "instruction",
				imagelabeldata: data.string.village_title
			},{
				imagelabelclass: "reward_option reward_option_hover position1",
				reward_option_idx: 1,
				imagelabeldata: data.string.village1
			},{
				imagelabelclass: "reward_option reward_option_hover position2",
				reward_option_idx: 2,
				imagelabeldata: data.string.village2
			},{
				imagelabelclass: "reward_option reward_option_hover position3",
				reward_option_idx: 3,
				imagelabeldata: data.string.village3
			},{
				imagelabelclass: "reward_option reward_option_hover position4",
				reward_option_idx: 4,
				imagelabeldata: data.string.village4
			},{
				imagelabelclass: "reward_option reward_option_hover position5",
				reward_option_idx: 5,
				imagelabeldata: data.string.village5
			},{
				imagelabelclass: "reward_option reward_option_hover position6",
				reward_option_idx: 6,
				imagelabeldata: data.string.village6
			} ],
			imagestoshow:[{
					imgclass: "image_reward_bg",
					imgsrc: "",
					imgid: "bg_for_mcq"
			},{
					imgclass: "image_reward",
					imgsrc: "",
					imgid: "gau-deu-talking-gif-FOREVER"
			},{
					imgclass: "image_reward2",
					imgsrc: "",
					imgid: "gau-deu-talking-png"
			}]
		}]
	},
	{
		//slide 3
		contentnocenteradjust: true,
		contentblockadditionalclass: "flickering_textblock",
		uppertextblocknocenteradjust: true,
		uppertextblock:[{
			textclass: "question",
			answer_option_idx: "3",
			textdata: data.string.question3
		}],
		imageblock:[{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover yes",
				imagelabeldata: data.string.q3_o1
			}],
			imagestoshow:[{
					imgclass: "image_correct",
					imgid: "correct"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.q3_o2
			}],
			imagestoshow:[{
					imgclass: "image",
					imgid: "incorrect"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.q3_o3
			}],
			imagestoshow:[{
					imgclass: "image",
					imgid: "incorrect"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.q3_o4
			}],
			imagestoshow:[{
					imgclass: "image",
					imgid: "incorrect"
			}]
		}],
		rewardblock:[{
			imagelabels:[{
				imagelabelclass: "instruction",
				imagelabeldata: data.string.village_title
			},{
				imagelabelclass: "reward_option reward_option_hover position1",
				reward_option_idx: 1,
				imagelabeldata: data.string.village1
			},{
				imagelabelclass: "reward_option reward_option_hover position2",
				reward_option_idx: 2,
				imagelabeldata: data.string.village2
			},{
				imagelabelclass: "reward_option reward_option_hover position3",
				reward_option_idx: 3,
				imagelabeldata: data.string.village3
			},{
				imagelabelclass: "reward_option reward_option_hover position4",
				reward_option_idx: 4,
				imagelabeldata: data.string.village4
			},{
				imagelabelclass: "reward_option reward_option_hover position5",
				reward_option_idx: 5,
				imagelabeldata: data.string.village5
			},{
				imagelabelclass: "reward_option reward_option_hover position6",
				reward_option_idx: 6,
				imagelabeldata: data.string.village6
			} ],
			imagestoshow:[{
					imgclass: "image_reward_bg",
					imgsrc: "",
					imgid: "bg_for_mcq"
			},{
					imgclass: "image_reward",
					imgsrc: "",
					imgid: "gau-deu-talking-gif-FOREVER"
			},{
					imgclass: "image_reward2",
					imgsrc: "",
					imgid: "gau-deu-talking-png"
			}]
		}]
	},
	{
		//slide 4
		contentnocenteradjust: true,
		contentblockadditionalclass: "flickering_textblock",
		uppertextblocknocenteradjust: true,
		uppertextblock:[{
			textclass: "question",
			answer_option_idx: "4",
			textdata: data.string.question4
		}],
		imageblock:[{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover yes",
				imagelabeldata: data.string.q4_o1
			}],
			imagestoshow:[{
					imgclass: "image_correct",
					imgid: "correct"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.q4_o2
			}],
			imagestoshow:[{
					imgclass: "image",
					imgid: "incorrect"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.q4_o3
			}],
			imagestoshow:[{
					imgclass: "image",
					imgid: "incorrect"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.q4_o4
			}],
			imagestoshow:[{
					imgclass: "image",
					imgid: "incorrect"
			}]
		}],
		rewardblock:[{
			imagelabels:[{
				imagelabelclass: "instruction",
				imagelabeldata: data.string.village_title
			},{
				imagelabelclass: "reward_option reward_option_hover position1",
				reward_option_idx: 1,
				imagelabeldata: data.string.village1
			},{
				imagelabelclass: "reward_option reward_option_hover position2",
				reward_option_idx: 2,
				imagelabeldata: data.string.village2
			},{
				imagelabelclass: "reward_option reward_option_hover position3",
				reward_option_idx: 3,
				imagelabeldata: data.string.village3
			},{
				imagelabelclass: "reward_option reward_option_hover position4",
				reward_option_idx: 4,
				imagelabeldata: data.string.village4
			},{
				imagelabelclass: "reward_option reward_option_hover position5",
				reward_option_idx: 5,
				imagelabeldata: data.string.village5
			},{
				imagelabelclass: "reward_option reward_option_hover position6",
				reward_option_idx: 6,
				imagelabeldata: data.string.village6
			} ],
			imagestoshow:[{
					imgclass: "image_reward_bg",
					imgsrc: "",
					imgid: "bg_for_mcq"
			},{
					imgclass: "image_reward",
					imgsrc: "",
					imgid: "gau-deu-talking-gif-FOREVER"
			},{
					imgclass: "image_reward2",
					imgsrc: "",
					imgid: "gau-deu-talking-png"
			}]
		}]
	},
	{
		//slide 5
		contentnocenteradjust: true,
		contentblockadditionalclass: "flickering_textblock",
		uppertextblocknocenteradjust: true,
		uppertextblock:[{
			textclass: "question",
			answer_option_idx: "5",
			textdata: data.string.question5
		}],
		imageblock:[{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover yes",
				imagelabeldata: data.string.q5_o1
			}],
			imagestoshow:[{
					imgclass: "image_correct",
					imgid: "correct"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.q5_o2
			}],
			imagestoshow:[{
					imgclass: "image",
					imgid: "incorrect"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.q5_o3
			}],
			imagestoshow:[{
					imgclass: "image",
					imgid: "incorrect"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.q5_o4
			}],
			imagestoshow:[{
					imgclass: "image",
					imgid: "incorrect"
			}]
		}],
		rewardblock:[{
			imagelabels:[{
				imagelabelclass: "instruction",
				imagelabeldata: data.string.village_title
			},{
				imagelabelclass: "reward_option reward_option_hover position1",
				reward_option_idx: 1,
				imagelabeldata: data.string.village1
			},{
				imagelabelclass: "reward_option reward_option_hover position2",
				reward_option_idx: 2,
				imagelabeldata: data.string.village2
			},{
				imagelabelclass: "reward_option reward_option_hover position3",
				reward_option_idx: 3,
				imagelabeldata: data.string.village3
			},{
				imagelabelclass: "reward_option reward_option_hover position4",
				reward_option_idx: 4,
				imagelabeldata: data.string.village4
			},{
				imagelabelclass: "reward_option reward_option_hover position5",
				reward_option_idx: 5,
				imagelabeldata: data.string.village5
			},{
				imagelabelclass: "reward_option reward_option_hover position6",
				reward_option_idx: 6,
				imagelabeldata: data.string.village6
			} ],
			imagestoshow:[{
					imgclass: "image_reward_bg",
					imgsrc: "",
					imgid: "bg_for_mcq"
			},{
					imgclass: "image_reward",
					imgsrc: "",
					imgid: "gau-deu-talking-gif-FOREVER"
			},{
					imgclass: "image_reward2",
					imgsrc: "",
					imgid: "gau-deu-talking-png"
			}]
		}]
	},
	{
		//slide 6
		contentnocenteradjust: true,
		contentblockadditionalclass: "flickering_textblock",
		uppertextblocknocenteradjust: true,
		uppertextblock:[{
			textclass: "question",
			answer_option_idx: "6",
			textdata: data.string.question6
		}],
		imageblock:[{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover yes",
				imagelabeldata: data.string.q6_o1
			}],
			imagestoshow:[{
					imgclass: "image_correct",
					imgid: "correct"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.q6_o2
			}],
			imagestoshow:[{
					imgclass: "image",
					imgid: "incorrect"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.q6_o3
			}],
			imagestoshow:[{
					imgclass: "image",
					imgid: "incorrect"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.q6_o4
			}],
			imagestoshow:[{
					imgclass: "image",
					imgid: "incorrect"
			}]
		}],
		rewardblock:[{
			imagelabels:[{
				imagelabelclass: "instruction",
				imagelabeldata: data.string.village_title
			},{
				imagelabelclass: "reward_option reward_option_hover position1",
				reward_option_idx: 1,
				imagelabeldata: data.string.village1
			},{
				imagelabelclass: "reward_option reward_option_hover position2",
				reward_option_idx: 2,
				imagelabeldata: data.string.village2
			},{
				imagelabelclass: "reward_option reward_option_hover position3",
				reward_option_idx: 3,
				imagelabeldata: data.string.village3
			},{
				imagelabelclass: "reward_option reward_option_hover position4",
				reward_option_idx: 4,
				imagelabeldata: data.string.village4
			},{
				imagelabelclass: "reward_option reward_option_hover position5",
				reward_option_idx: 5,
				imagelabeldata: data.string.village5
			},{
				imagelabelclass: "reward_option reward_option_hover position6",
				reward_option_idx: 6,
				imagelabeldata: data.string.village6
			} ],
			imagestoshow:[{
					imgclass: "image_reward_bg",
					imgsrc: "",
					imgid: "bg_for_mcq"
			},{
					imgclass: "image_reward",
					imgsrc: "",
					imgid: "gau-deu-talking-gif-FOREVER"
			},{
					imgclass: "image_reward2",
					imgsrc: "",
					imgid: "gau-deu-talking-png"
			}]
		}]
	}
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

  loadTimelineProgress($total_page,countNext+1);

  var preload;
	var timeoutvar = null;
	var current_sound;
	var aeroplanesound;
	var attipatti;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "incorrect", src: imgpath+"incorrect.png", type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src: imgpath+"correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_for_mcq", src: imgpath+"bg_for_mcq.png", type: createjs.AbstractLoader.IMAGE},
			{id: "gau-deu-talking-png", src: imgpath+"gau-deu-talking.png", type: createjs.AbstractLoader.IMAGE},
			{id: "gau-deu-talking-gif-FOREVER", src: imgpath+"gau-deu-talking-gif-FOREVER.gif", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "attipatti", src: soundAsset+"attipatti.ogg"},
			{id: "gaundeu", src: soundAsset+"gaundeu.ogg"},
			{id: "ex_q1", src: soundAsset+"ex_q1.ogg"},
			{id: "ex_q2", src: soundAsset+"ex_q2.ogg"},
			{id: "ex_q3", src: soundAsset+"ex_q3.ogg"},
			{id: "ex_q4", src: soundAsset+"ex_q4.ogg"},
			{id: "ex_q5", src: soundAsset+"ex_q5.ogg"},
			{id: "ex_q6", src: soundAsset+"ex_q6.ogg"},
			{id: "ex_a1", src: soundAsset+"ex_a1.ogg"},
			{id: "ex_a2", src: soundAsset+"ex_a2.ogg"},
			{id: "ex_a3", src: soundAsset+"ex_a3.ogg"},
			{id: "ex_a4", src: soundAsset+"ex_a4.ogg"},
			{id: "ex_a5", src: soundAsset+"ex_a5.ogg"},
			{id: "ex_a6", src: soundAsset+"ex_a6.ogg"},
			{id: "gaun1", src: soundAsset+"gaun1.ogg"},
			{id: "gaun2", src: soundAsset+"gaun2.ogg"},
			{id: "gaun3", src: soundAsset+"gaun3.ogg"},
			{id: "gaun4", src: soundAsset+"gaun4.ogg"},
			{id: "gaun5", src: soundAsset+"gaun5.ogg"},
			{id: "gaun6", src: soundAsset+"gaun6.ogg"}

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		attipatti = new buzz.sound(soundAsset + "attipatti.ogg");
		testin.init(content.length);
		templateCaller();
	}
	//initialize
	init();

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			// $prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			// $prevBtn.show(0);

			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  var reward_tracker = [];
   var score = 0;

	 /*values in this array is same as the name of images of eggs in image folder*/
	var testin = new EggTemplate();

	 	//eggTemplate.eggMove(countNext);
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    var $rewardblock = $(".rewardblock");
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    vocabcontroller.findwords(countNext);
	    put_image(content, countNext);

		// splitintofractions($(".fractionblock"));
		// if(countNext > 0){
		    var $imagediv = $(".imagediv");
		    var $imageblock = $(".imageblock");
		    $imageblock.html("");
		    var $rewardblock = $(".rewardblock");
		    var $rewardoption = $(".reward_option");
			if(reward_tracker.length > 0){
				for(var i = 0; i < reward_tracker.length; i++){
					$($rewardoption[(reward_tracker[i] - 1)]).hide(0);
				}
			}

		    var randomindex;
		    var array_of_index = ["क",  "ख", "ग", "घ"];
		    while($imagediv.length > 1){
		    	randomindex = Math.floor(Math.random() * ($imagediv.length + 1));
		    	$imageblock.append($($imagediv[randomindex]));
		    	$imagediv.splice(randomindex, 1);
		    }

		    $nextBtn.hide(0);
		    $imageblock.append($($imagediv[0]));

		    var $labels = $(".imagediv > label");
		    for (var j = 0; j <  $labels.length; j++){
		    	$($labels[j]).prepend("<span> "+ array_of_index[j]+") &nbsp; </span>");
		    }

		    var answered = false;
		    sound_player("ex_q"+ (countNext + 1),1);
		    var first_round = true;
		    $(".imagediv").click(function(){
		    	if(answered){
		    		return true;
		    	}
		    	var $this = $(this);
	    		$this.find("img").show(0);
	    		var $label = $this.find("label");
	    		$label.removeClass("onhover");
	    		createjs.Sound.stop();
		    	if($label.hasClass("yes")){
		    		testin.update(true);
		    		play_correct_incorrect_sound(true);
		    		$label.addClass("correct");
		    		answered = true;
		    		$(".imagediv> label").removeClass("onhover");
		    		if(countNext != $total_page)
							$nextBtn.show(0);
		    	} else {
		    		testin.update(false);
		    		timeoutcontroller = setTimeout(function(){
			    		$rewardblock.show(0);
			    		$(".image_reward2").hide(0);
			    		if(first_round){
			    			$(".reward_option").show(0);
			    			first_round != first_round;
			    		}
			    		timeoutcontroller = setTimeout(function(){
			    			// $(".gau-deu-talking-gif").remove();
			    			// $(".rewardblock").append('<img class="image_reward gau-deu-talking-gif" src="'+preload.getResult('gau-deu-talking-gif').src+'">');
			    			sound_player("gaundeu", true,0);
		    			}, 200);
		    		}, 800);
		    		play_correct_incorrect_sound(false);
		    		$label.addClass("incorrect");
		    	}
		    });

		    var rewardoptionflag = false;
		    $rewardoption.click(function(){
		    	if (rewardoptionflag){
		    		return rewardoptionflag;
		    	}
		    	rewardoptionflag = true;
		    	$rewardoption.removeClass("reward_option_hover");
		    	var $this = $(this);
		    	var index = $this.data("rewardoptions_index");
		    	var ansindex = $(".question").data("answeroptions_index");
		    	reward_tracker[reward_tracker.length] = index;
		    	$this.hide(0);
		    	console.log("index", index);
		    	//play audio base audio id based on the rewardoptions_index
		    	var current_sound2_a = new buzz.sound((soundAsset + "gaun"+ index+".ogg"));
		    	current_sound2_a.play();
					current_sound2_a.bind('ended', function(){
						// navigationcontroller();
					});
		    	$(".image_reward2").hide(0);
		    	// $(".image_reward_infinite").remove();
			    // $(".rewardblock").append('<img class="image_reward gau-deu-talking-gif" src="'+preload.getResult('gau-deu-talking-gif-FOREVER').src+'">');
		    	current_sound2_a.bind('ended', function(){
		    		current_sound2_a.unbind('ended');
			    	attipatti.play();
			    	attipatti.bind('ended', function(){
			    		attipatti.unbind('ended');
			    		createjs.Sound.play("ex_a"+ ansindex);
				    	$rewardblock.hide(0);
				    	$rewardoption.addClass("reward_option_hover");
							$rewardoption.addClass("reward_option_hover");
				    	rewardoptionflag = false;
			    	});
		    	});
		    });
		// }
  }

/*=====  End of Templates Block  ======*/

	function sound_player(sound_id, flag, next){
		console.log('in here');
		(flag == undefined) ? false : true;
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			if(flag){
				$(".image_reward2").show(0);}
		 if (next){
			 navigationcontroller();
		 }
		});
	}

	function put_image(content, count){
		if(content[count] == null){
			return true;
		}

		if(content[count].hasOwnProperty('imageblock')){
			for(var j = 0; j < content[count].imageblock.length; j++){
				var imageblock = content[count].imageblock[j];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}

		if(content[count].hasOwnProperty('rewardblock')){
			for(var j = 0; j < content[count].rewardblock.length; j++){
				var rewardblock = content[count].rewardblock[j];
				if(rewardblock.hasOwnProperty('imagestoshow')){
					var imageClass = rewardblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						console.log("iamge id", imageClass[i].imgid);
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  // templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
			testin.gotoNext();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

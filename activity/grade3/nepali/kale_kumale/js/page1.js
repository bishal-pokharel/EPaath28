var imgpath = $ref + "/images/";
var imgpath2 = $ref + "/images/960_480/";
var soundAsset = $ref+"/sounds/";
var soundAsset2 = $ref+"/audio_"+$lang+'/';

/*	Store the questions and answers in an array, the first array[0]->question_1[0] is the question
 * 	Second array[1] is the correct answer and the rest are incorrect answers
 *	Then store all the questions in another array-> questions so that they can be displayed randomly */
var question_1 = [data.string.p2ques1, data.string.p2ans1a, data.string.p2ans1b, data.string.p2ans1c];
var question_2 = [data.string.p2ques2, data.string.p2ans2a, data.string.p2ans2b, data.string.p2ans2c];
var question_3 = [data.string.p2ques3, data.string.p2ans3a, data.string.p2ans3b, data.string.p2ans3c];
var question_4 = [data.string.p2ques4, data.string.p2ans4a, data.string.p2ans4b, data.string.p2ans4c];
var question_5 = [data.string.p2ques5, data.string.p2ans5a, data.string.p2ans5b, data.string.p2ans5c];
var question_6 = [data.string.p2ques6, data.string.p2ans6a, data.string.p2ans6b, data.string.p2ans6c];
var question_7 = [data.string.p2ques7, data.string.p2ans7a, data.string.p2ans7b, data.string.p2ans7c];
var question_8 = [data.string.p2ques8, data.string.p2ans8a, data.string.p2ans8b, data.string.p2ans8c];
var question_9 = [data.string.p2ques9, data.string.p2ans9a, data.string.p2ans9b, data.string.p2ans9c];

var questions_set1 = [question_1, question_2];
var questions_set2 = [question_3, question_4];
// var questions_set3 = [question_5, question_6];
// var questions_set4 = [question_7, question_8];
// var questions_set5 = [question_9];


var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'cover_page',

		imageblock:[{
			imagestoshow:[{
				imgclass: "kale_kumale",
				imgid: "kale_kumale",
				imgsrc: imgpath+ "kale_kumale.png"
			}]
		}],
		storytextblockadditionalclass : "intor_text",

		storytextblock : [
		{
			textclass : "text_story2 text_l0 text_on",
			textdata : data.string.title,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story2',
		}],
	},

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_story_1',

		storytextblockadditionalclass : "bottom_para",

		storytextblock : [
		{
			textclass : "text_story text_l0 text_on",
			textdata : data.string.p2text1,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		}],
	},

	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_story_1',

		storytextblockadditionalclass : "bottom_para",

		storytextblock : [
		{
			textclass : "text_story text_l1 text_on",
			textdata : data.string.p2text2,
		}]
	},

	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_story_1',

		storytextblockadditionalclass : "bottom_para_2",

		storytextblock : [
		{
			textclass : "text_story text_0 text_inactive",
			textdata : data.string.p2text1,
		},
		{
			textclass : "text_story text_1 text_inactive",
			textdata : data.string.p2text2,
		}],
		lowertextblockadditionalclass: 'ques_div drop its_hidden',
		lowertextblock : [
		{
			textclass : "text_qna text_q0 text_ques",
			textdata : '',
		},
		{
			textclass : "text_qna text_q1 ans_button_inactive",
			textdata : '',
		},
		{
			textclass : "text_qna text_q2 ans_button_inactive",
			textdata : '',
		},
		{
			textclass : "text_qna text_q3 ans_button_inactive",
			textdata : '',
		},
		{
			textclass : "next_ques next_ques_inactive",
			textdata : data.string.p2next,
		},
		{
			textclass : "hide_btn",
			textdata : data.string.p2hide,
		}],

		imageblock : [{
			imagestoshow : [
			{
				imgclass : "hide_icon its_hidden",
				imgsrc : imgpath + "q_icon_green.png",
			},
			{
				imgclass : "speaker_icon",
				imgsrc : imgpath + "audio_icon.png",
			}],
			imagelabels : [{
			imagelabelclass : "hint_para fade_in its_hidden",
			imagelabeldata : data.string.p2hint
			}]
		}]
	},

	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_story_2',

		storytextblockadditionalclass : "bottom_para",

		storytextblock : [
		{
			textclass : "text_story text_l2 text_on",
			textdata : data.string.p2text3,
		}]
	},

	//slide 4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_story_2',

		storytextblockadditionalclass : "bottom_para",

		storytextblock : [
		{
			textclass : "text_story text_l3 text_on",
			textdata : data.string.p2text4,
		}]
	},
	//slide 5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_story_2',

		storytextblockadditionalclass : "bottom_para",

		storytextblock : [
		{
			textclass : "text_story text_l4 text_on",
			textdata : data.string.p2text5,
		}]
	},
	//slide 6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_story_2',

		storytextblockadditionalclass : "bottom_para",

		storytextblock : [
		{
			textclass : "text_story text_l5 text_on",
			textdata : data.string.p2text6,
		}]
	},
	//slide 7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_story_2',

		storytextblockadditionalclass : "bottom_para",

		storytextblock : [
		{
			textclass : "text_story text_l6 text_on",
			textdata : data.string.p2text7,
		}]
	},
	//slide 8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_story_3',

		storytextblockadditionalclass : "bottom_para",

		storytextblock : [
		{
			textclass : "text_story text_l7 text_on",
			textdata : data.string.p2text8,
		}]
	},

	//slide 9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_story_3',

		storytextblockadditionalclass : "bottom_para",

		storytextblock : [
		{
			textclass : "text_story text_l8 text_on",
			textdata : data.string.p2text9,
		}]
	},

	//slide 10
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_story_3',

		storytextblockadditionalclass : "bottom_para_2",

		storytextblock : [
		{
			textclass : "text_story text_0 text_inactive",
			textdata : data.string.p2text3,
		},
		{
			textclass : "text_story text_1 text_inactive",
			textdata : data.string.p2text4,
		},
		{
			textclass : "text_story text_2 text_inactive",
			textdata : data.string.p2text5,
		},
		{
			textclass : "text_story text_3 text_inactive",
			textdata : data.string.p2text6,
		},
		{
			textclass : "text_story text_4 text_inactive",
			textdata : data.string.p2text7,
		},
		{
			textclass : "text_story text_5 text_inactive",
			textdata : data.string.p2text8,
		},
		{
			textclass : "text_story text_6 text_inactive",
			textdata : data.string.p2text9,
		}],
		lowertextblockadditionalclass: 'ques_div drop its_hidden',
		lowertextblock : [
		{
			textclass : "text_qna text_q0 text_ques",
			textdata : '',
		},
		{
			textclass : "text_qna text_q1 ans_button_inactive",
			textdata : '',
		},
		{
			textclass : "text_qna text_q2 ans_button_inactive",
			textdata : '',
		},
		{
			textclass : "text_qna text_q3 ans_button_inactive",
			textdata : '',
		},
		{
			textclass : "next_ques next_ques_inactive",
			textdata : data.string.p2next,
		},
		{
			textclass : "hide_btn",
			textdata : data.string.p2hide,
		}],

		imageblock : [{
			imagestoshow : [
			{
				imgclass : "hide_icon its_hidden",
				imgsrc : imgpath + "q_icon_green.png",
			},
			{
				imgclass : "speaker_icon",
				imgsrc : imgpath + "audio_icon.png",
			}],
			imagelabels : [{
			imagelabelclass : "hint_para fade_in its_hidden",
			imagelabeldata : data.string.p2hint
			}]
		}]
	}
];
var sound_data;
var isFirefox = typeof InstallTrigger !== 'undefined';
$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	var last_page = false;
	loadTimelineProgress($total_page, countNext + 1);
	var toggle = 0;
	var last_ques = false;
	/* var to count the number of ques displayed */
	var ques_no = 0;
	var sound_group_p1;
	/* store numbers from 0 to 3 in random in an array using the function*/
	var random_array_set1 = sandy_random_display(questions_set1.length);
	var random_array_set2 = sandy_random_display(questions_set2.length);
	// var random_array_set3 = sandy_random_display(questions_set3.length);
	// var random_array_set4 = sandy_random_display(questions_set4.length);
	// var random_array_set5 = sandy_random_display(questions_set5.length);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [

			//images
			{id: "kale_kumale", src: imgpath+"kale_kumale.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_for_coverpage", src: imgpath+"bg_for_coverpage.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_story_1", src: imgpath2+"01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_story_2", src: imgpath2+"02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_story_3", src: imgpath2+"03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_story_3b", src: imgpath2+"03b.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_story_4", src: imgpath2+"04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_story_5", src: imgpath2+"05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_story_6", src: imgpath2+"06.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0", src: soundAsset2+"p1_s0.ogg"},
			{id: "sound_1", src: soundAsset2+"p1_s1.ogg"},
			{id: "sound_3", src: soundAsset2+"p1_s3.ogg"},
			{id: "sound_4", src: soundAsset2+"p1_s4.ogg"},
			{id: "sound_5", src: soundAsset2+"p1_s5.ogg"},
			{id: "sound_6", src: soundAsset2+"p1_s6.ogg"},
			{id: "sound_7", src: soundAsset2+"p1_s7.ogg"},
			{id: "sound_8", src: soundAsset2+"p1_s8.ogg"},
			{id: "sound_9", src: soundAsset2+"p1_s9.ogg"},
			{id: "sound_10", src: soundAsset+"title.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		var sound_l_0 = new buzz.sound((soundAsset2 + "p1_s0.ogg"));
		var sound_l_1 = new buzz.sound((soundAsset2 + "p1_s1.ogg"));
		var sound_l_3 = new buzz.sound((soundAsset2 + "p1_s3.ogg"));
		var sound_l_4 = new buzz.sound((soundAsset2 + "p1_s4.ogg"));
		var sound_l_5 = new buzz.sound((soundAsset2 + "p1_s5.ogg"));
		var sound_l_6 = new buzz.sound((soundAsset2 + "p1_s6.ogg"));
		var sound_l_7 = new buzz.sound((soundAsset2 + "p1_s7.ogg"));
		var sound_l_8 = new buzz.sound((soundAsset2 + "p1_s8.ogg"));
		var sound_l_9 = new buzz.sound((soundAsset2 + "p1_s9.ogg"));

		sound_group_p1 = [sound_l_0, sound_l_1, sound_l_3, sound_l_4, sound_l_5, sound_l_6, sound_l_7, sound_l_8, sound_l_9];
		templateCaller();
	}
	//initialize
	init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("speechboxcontent", $("#speechboxcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0) {
			$nextBtn.hide(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			// $nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	var last_question = false;
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch (countNext) {
		case 0:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play('sound_10');
			current_sound.play();
			current_sound.on("complete", function(){
				$nextBtn.show(0);
			});
			// $(".cover_page").css("background-image", 'url('+ preload.getResult('bg_for_coverpage').src+')');
			// $(".cover_page").css("background-image", 'url('+ imgpath+"bg_for_coverpage.png"+')');
			// var img_src = preload.getResult('bg_for_coverpage').src;
			$(".cover_page").css({'background-image': 'url('+ preload.getResult('bg_for_coverpage').src +')', 'background-color' : "#afa"});
			break;
		case 1:
			$(".bg_story_1").css("background-image", 'url('+ preload.getResult('bg_story_1').src+')');
			var $textblack = $(".text_l0");
    		sound_data =  sound_group_p1[0];
    		play_text($textblack, $textblack.html());
			break;
		case 2:
			$(".bg_story_1").css("background-image", 'url('+ preload.getResult('bg_story_1').src+')');
			var $textblack = $(".text_l1");
    		sound_data =  sound_group_p1[1];
    		play_text($textblack, $textblack.html());
			break;
		case 3:
			$(".bg_story_1").css("background-image", 'url('+ preload.getResult('bg_story_1').src+')');
			ques_no = 0;
			last_question = false;
			$('.ques_div').show(0);
			$('.hide_icon').show(0);
			var toggle = 0;
			$('.hide_icon').click(function(){
				if((countNext+1) == content.length){
					ole.footerNotificationHandler.pageEndSetNotification();
				}else{
					$nextBtn.show(0);
				}
				if(countNext>0){
					$prevBtn.show(0);
				}
				if(toggle%2==0){
					toggle++;
					$('.ques_div').css({
						'top': '-70%',
					});
					$('.hide_icon').attr('src', imgpath + "q_icon_red.png");
				} else {
					toggle++;
					$('.ques_div').css({
						'top': '5%',
					});
					$('.hide_icon').attr('src', imgpath + "q_icon_green.png");
				}
			});
			$('.hide_btn').click(function(){
				toggle++;
				if((countNext+1) == content.length){
					ole.footerNotificationHandler.pageEndSetNotification();
				}else{
					$nextBtn.show(0);
				}
				if(countNext>0){
					$prevBtn.show(0);
				}
				$('.hint_para').removeClass('fade_away');	//adds fading effect
				$('.hint_para').show(0);
				$('.hint_para').delay(2000).addClass('fade_away');
				$('.hint_para').hide(0);
				$('.ques_div').css({
					'top': '-70%',
				});
				$('.hide_icon').attr('src', imgpath + "q_icon_red.png");
			});

			question_caller('.text_q', questions_set1, random_array_set1);
			$('.text_0').click(function() {
				sound_data = sound_group_p1[0];
				click_text(this);
			});
			$('.text_1').click(function() {
				sound_data = sound_group_p1[1];
				click_text(this);
			});
			$('.next_ques').click( function(){
				ques_no++;		// increase question number
				if(!(ques_no==questions_set1.length)){
					question_caller('.text_q', questions_set1, random_array_set1);		// call the next question
					ans_button_reset();			// reset all the answer buttons to unselected state
					$('.next_ques').removeClass('next_ques_active');
					$('.next_ques').addClass('next_ques_inactive');
				}
				if(ques_no==questions_set1.length-1){
					$('.next_ques').hide(0);	// hide the next question text
					last_question = true;
				}
			});
			break;
		case 4:
			$(".bg_story_2").css("background-image", 'url('+ preload.getResult('bg_story_1').src+')');
			var $textblack = $(".text_l2");
    		sound_data =  sound_group_p1[2];
    		play_text($textblack, $textblack.html());
			break;
		case 5:
			$(".bg_story_2").css("background-image", 'url('+ preload.getResult('bg_story_2').src+')');
			var $textblack = $(".text_l3");
    		sound_data =  sound_group_p1[3];
    		play_text($textblack, $textblack.html());
			break;
		case 6:
			$(".bg_story_2").css("background-image", 'url('+ preload.getResult('bg_story_2').src+')');
			var $textblack = $(".text_l4");
    		sound_data =  sound_group_p1[4];
    		play_text($textblack, $textblack.html());
			break;
		case 7:
			$(".bg_story_2").css("background-image", 'url('+ preload.getResult('bg_story_2').src+')');
			var $textblack = $(".text_l5");
    		sound_data =  sound_group_p1[5];
    		play_text($textblack, $textblack.html());
			break;
		case 8:
			$(".bg_story_2").css("background-image", 'url('+ preload.getResult('bg_story_2').src+')');
			var $textblack = $(".text_l6");
    		sound_data =  sound_group_p1[6];
    		play_text($textblack, $textblack.html());
			break;
		case 9:
			$(".bg_story_3").css("background-image", 'url('+ preload.getResult('bg_story_3').src+')');
			var $textblack = $(".text_l7");
    		sound_data =  sound_group_p1[7];
    		play_text($textblack, $textblack.html());
			break;
		case 10:
			$(".bg_story_3").css("background-image", 'url('+ preload.getResult('bg_story_3').src+')');
			var $textblack = $(".text_l8");
    		sound_data =  sound_group_p1[8];
    		play_text($textblack, $textblack.html());
			break;
		case 11:
			$(".bg_story_3").css("background-image", 'url('+ preload.getResult('bg_story_3').src+')');
			ques_no = 0;
			last_question = false;
			$('.ques_div').show(0);
			$('.hide_icon').show(0);
			var toggle = 0;
			$('.hide_icon').click(function(){
				if((countNext+1) == content.length){
					ole.footerNotificationHandler.pageEndSetNotification();
				}else{
					$nextBtn.show(0);
				}
				if(countNext>0){
					$prevBtn.show(0);
				}
				if(toggle%2==0){
					toggle++;
					$('.ques_div').css({
						'top': '-70%',
					});
					$('.hide_icon').attr('src', imgpath + "q_icon_red.png");
				} else {
					toggle++;
					$('.ques_div').css({
						'top': '5%',
					});
					$('.hide_icon').attr('src', imgpath + "q_icon_green.png");
				}
			});
			$('.hide_btn').click(function(){
				toggle++;
				if((countNext+1) == content.length){
					ole.footerNotificationHandler.pageEndSetNotification();
				}else{
					$nextBtn.show(0);
				}
				if(countNext>0){
					$prevBtn.show(0);
				}
				$('.hint_para').removeClass('fade_away');	//adds fading effect
				$('.hint_para').show(0);
				$('.hint_para').delay(2000).addClass('fade_away');
				$('.hint_para').hide(0);
				$('.ques_div').css({
					'top': '-70%',
				});
				$('.hide_icon').attr('src', imgpath + "q_icon_red.png");
			});

			question_caller('.text_q', questions_set2, random_array_set2);
			$('.text_0').click(function() {
				sound_data = sound_group_p1[2];
				click_text(this);
			});
			$('.text_1').click(function() {
				sound_data = sound_group_p1[3];
				click_text(this);
			});
			$('.text_2').click(function() {
				sound_data = sound_group_p1[4];
				click_text(this);
			});
			$('.text_3').click(function() {
				sound_data = sound_group_p1[5];
				click_text(this);
			});
			$('.text_4').click(function() {
				sound_data = sound_group_p1[6];
				click_text(this);
			});
			$('.text_5').click(function() {
				sound_data = sound_group_p1[7];
				click_text(this);
			});
			$('.text_6').click(function() {
				sound_data = sound_group_p1[8];
				click_text(this);
			});

			$('.next_ques').click( function(){
				ques_no++;		// increase question number
				if(!(ques_no==questions_set2.length)){
					question_caller('.text_q', questions_set2, random_array_set2);		// call the next question
					ans_button_reset();			// reset all the answer buttons to unselected state
					$('.next_ques').removeClass('next_ques_active');
					$('.next_ques').addClass('next_ques_inactive');
				}
				if(ques_no==questions_set2.length-1){
					$('.next_ques').hide(0);	// hide the next question text
					last_question = true;
				}
			});
			break;
		default:
			break;
		}
	}


	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		if(sound_data != null){
			sound_data.stop();
			sound_data.unbind("ended");
		}
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		if(sound_data != null){
			sound_data.stop();
			sound_data.unbind("ended");
		}
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	// templateCaller();

	/* --- function that make objects appear ----
	function audio_fade_caller(fade_class, audio_arr, audio_count){
		if(audio_count>audio_arr.length){
			return false;
		}
		var class_name = $(fade_class + audio_count);
		class_name.show(0);
		audio_arr[audio_count].play();
		audio_arr[audio_count].bind('ended', function(){
			audio_count++;
			audio_fade_caller(fade_class, audio_arr, audio_count);
		});
	}
	*/
	function show_text($this,  $span_speec_text, message, interval) {
	  if (0 < message.length) {
	  	var nextText = message.substring(0,1);
	  	if(nextText == "<" ){
	  		$span_speec_text.append("<br>");
	  		message = message.substring(4, message.length);
	  	}else{
	  		$span_speec_text.append(nextText);
	  		message = message.substring(1, message.length);
	  	}
	  	$this.html($span_speec_text);
	  	$this.append(message);
	    setTimeout(function () {
	    	show_text($this,  $span_speec_text, message, interval);
	  	}, interval);
	  } else{
	  	textanimatecomplete = true;
	  }
	}

	var intervalid;
	var soundplaycomplete = false;
	var textanimatecomplete = !isFirefox;
	// uses the show text to add typing effect with sound and glowing animations
	function play_text($this, text/*, sound_data*/){
		var $span_speec_text;
		if(isFirefox){
			$this.html("<span id='span_speec_text'></span>"+text);
			$span_speec_text = $("#span_speec_text");
			// $this.css("background-color", "#faf");
			show_text($this, $span_speec_text,text, 90);	// 65 ms is the interval found out by hit and trial
		} else {
            $this.html("<span id='span_speec_text'>"+text+"</span>");
			$span_speec_text = $("#span_speec_text");
			textanimatecomplete = true;
        }
		$prevBtn.hide(0);

		sound_data.play();
		sound_data.bind('ended', function(){
			// $(play_class).removeClass('text_on');
			// $(play_class).addClass('text_off');
				sound_data.unbind('ended');
				soundplaycomplete = true;
				ternimatesound_play_animate();
            vocabcontroller.findwords(countNext);
        });

		function ternimatesound_play_animate(){
			 intervalid = setInterval(function () {
					if(textanimatecomplete && soundplaycomplete){
						$this.html($span_speec_text.html());
						$this.css("background-color", "transparent");
						clearInterval(intervalid);
						intervalid = null;
						animationinprogress = false;
						vocabcontroller.findwords(countNext);
						if((countNext+1) == content.length){
							ole.footerNotificationHandler.pageEndSetNotification();
						}else{
							$nextBtn.show(0);
						}
						if(countNext>0){
							$prevBtn.show(0);
						}
				}
			}, 250);
		}
	}

	/* Typing animation ends */

	/*	Function to check if the answer is correct or incorrect as decided by answer_bool
	 *  Display button green or red accordingly with correct sound as well 	 */
	function click_answer(play_class, answer_bool){
		$(play_class).removeClass('ans_button_inactive');
		if( answer_bool){
			$(play_class).addClass('ans_button_right');
			$('.next_ques').removeClass('next_ques_inactive');
			$('.next_ques').addClass('next_ques_active');
			play_correct_incorrect_sound(true);
			$('.next_ques').css('pointer-events','all');
			$('.text_qna').css({
				'pointer-events': 'none',
			});
			if(last_question){
				if((countNext+1) == content.length){
					ole.footerNotificationHandler.pageEndSetNotification();
				}else{
					$nextBtn.show(0);
				}
				toggle++;
				$('.ques_div').css({
					'top': '-70%',
				});
				$('.hide_icon').attr('src', imgpath + "q_icon_red.png");
			}
		} else {
			$(play_class).addClass('ans_button_wrong');
			play_correct_incorrect_sound(false);
		}
	}

	/*
	function click_text_caller(click_class, sound_arr) {
			for( var i=0; i<sound_arr.length; i++) {
				var class_name = $(click_class + i);
				class_name.click(function() {
					click_text(this, sound_arr[i]);
				});
			}
		}*/

	/* function to allow user to click the text and play sound of each sentence clicked
	 * then allow appropriate animation	*/
	function click_text(play_class){
		sound_data.play();
		$(play_class).removeClass('text_inactive');
		$(play_class).addClass('text_active');
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		$('.text_story').css({
			'pointer-events': 'none',
		});
		$('.text_qna').css({
				'pointer-events': 'none',
			});
		sound_data.bind('ended', function(){
			$(play_class).removeClass('text_active');
			$(play_class).addClass('text_inactive');
			$('.text_story').css({
				'pointer-events': 'all',
			});
			$('.text_qna').css({
				'pointer-events': 'all',
			});
			$prevBtn.show(0);
			if(countNext != $total_page-1){
				$nextBtn.show(0);
			}
		});
	}


	// function to add sound event on click
	function sound_caller(sound_box_class, sound_var){
		$(sound_box_class).click(function(){
			sound_var.play();
		});
	}


	function next_btn_caller (next_display) {
		if(!next_display) {
			return false;
		} else {
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
	}

	/* This Function call the question based on global variables named random_array for random questions and ques_no
	 * to track the index of question in the questions array
	 * random number array is generated to shuffle the answer
	 * the correct answer being question[x][1] we get the text in the answerbox and compare it with the var question[x][1]
	 * click events are disabled accordingly
	 */

	var numbers = ["१", "२", "३", "४", "५", "६"];
	function question_caller(text_name, questions_arr, random_arr) {
		var text_class= $(text_name + 0);
		text_class.html(numbers[ques_no]+'. '+questions_arr[random_arr[ques_no]][0]);
		random_ans = sandy_random_display(3);
		for( var m = 0; m<3; m++) {
			var ans_no = 1 + random_ans[m];
			text_class= $(text_name + ans_no);
			text_class.html(questions_arr[random_arr[ques_no]][m+1]);
		}
		biggest_box_size(text_name, 1, 3);
		$('.text_q1').click(function() {
			click_answer(this, $(this).text()==questions_arr[random_arr[ques_no]][1]);
		});
		$('.text_q2').click(function() {
			click_answer(this, $(this).text()==questions_arr[random_arr[ques_no]][1]);
		});
		$('.text_q3').click(function() {
			click_answer(this, $(this).text()==questions_arr[random_arr[ques_no]][1]);
		});
		$('.next_ques').css('pointer-events','none');
	}


	/* Function to reset the answer button */
	function ans_button_reset() {
		for(var m = 1; m<4; m++){
			$('.text_qna').css({
				'pointer-events': 'all',
			});
			var playclass = $('.text_q'+m);
			playclass.removeClass('ans_button_right');
			playclass.removeClass('ans_button_wrong');
			playclass.addClass('ans_button_inactive');
		}
	}

	/* This function takes the class name and gets the number of string characters in that class
	 * calculates the length of the string and set width of all the class accrodingly
	 * class_initial should be myclass_ and start_index and number_of_class should be integer */
	function biggest_box_size(class_initial, start_index, number_of_class){
		var l = 0;
		for( var m=start_index; m<(number_of_class + start_index); m++){
			var class_name = $(class_initial + m);
			var l_new = class_name.text().length;
			if( l_new > l){
				l = l_new;
			}
		}
		for( var m=start_index; m<(number_of_class + start_index); m++){
			var class_name = $(class_initial + m);
			class_name.css('width', l*1.6+'vw');			//1.5 comes from font-size, change parameter according with font size
		}
	}

	/*  function used to generate numbers from 0 to range-1 in random order and store it in array
	 *	This function pushes all the numbers upto range -1 in an array, generates the random no upto range-1
	 *  moves the randomly generated index from that array to new array and repeats the process till every number < range is generated	*/
	function sandy_random_display(range){
		var random_arr = [];
		var return_arr = [];
		var random_max = range;
		for(var i=0; i<range; i++){
			random_arr.push(i);
		}

		while(random_max)
		{
			var index = ole.getRandom(1, random_max-1, 0);
			return_arr.push(random_arr[index]);
			random_arr.splice((index),1);
			random_max--;
		}
		return return_arr;
	}

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

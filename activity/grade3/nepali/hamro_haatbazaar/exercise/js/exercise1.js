var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/exercise/";

var sound_1 = new buzz.sound((soundAsset + "1.ogg"));
var sound_2 = new buzz.sound((soundAsset + "2.ogg"));
var sound_3 = new buzz.sound((soundAsset + "3.ogg"));
var sound_4 = new buzz.sound((soundAsset + "4.ogg"));
var sound_5 = new buzz.sound((soundAsset + "5.ogg"));
var sound_6 = new buzz.sound((soundAsset + "6.ogg"));
var sound_7 = new buzz.sound((soundAsset + "7.ogg"));
var sound_8 = new buzz.sound((soundAsset + "8.ogg"));
var sound_9 = new buzz.sound((soundAsset + "ex.ogg"));
var sound_ex = sound_1;

var soundcontent = [sound_1, sound_2, sound_3, sound_4, sound_5, sound_6, sound_7, sound_8];

var content=[

	//ex1
	{
		exerciseblock: [
			{
				instructiondata: data.string.ex_instruction,
				draggableblock:[{
					draggabledivadditionalclass: 'class_1',
					textdata: data.string.ex_1
				},{
					draggabledivadditionalclass: 'class_2',
					textdata: data.string.ex_2
				},{
					draggabledivadditionalclass: 'class_3',
					textdata: data.string.ex_3
				},{
					draggabledivadditionalclass: 'class_4',
					textdata: data.string.ex_4
				},{
					draggabledivadditionalclass: 'class_5',
					textdata: data.string.ex_5
				},{
					draggabledivadditionalclass: 'class_6',
					textdata: data.string.ex_6
				},{
					draggabledivadditionalclass: 'class_7',
					textdata: data.string.ex_7
				},{
					draggabledivadditionalclass: 'class_8',
					textdata: data.string.ex_8
				}],
				droppableblock:[{
					droppabledivadditionalclass: 'drop_class_1',
					imgsrc: imgpath + '1.jpg',
					textdata: data.string.ex_1
				},{
					droppabledivadditionalclass: 'drop_class_2',
					imgsrc: imgpath + '2.jpg',
					textdata: data.string.ex_2
				},{
					droppabledivadditionalclass: 'drop_class_3',
					imgsrc: imgpath + '3.jpg',
					textdata: data.string.ex_3
				},{
					droppabledivadditionalclass: 'drop_class_4',
					imgsrc: imgpath + '4.jpg',
					textdata: data.string.ex_4
				},{
					droppabledivadditionalclass: 'drop_class_5',
					imgsrc: imgpath + '5.jpg',
					textdata: data.string.ex_5
				},{
					droppabledivadditionalclass: 'drop_class_6',
					imgsrc: imgpath + '6.jpg',
					textdata: data.string.ex_6
				},{
					droppabledivadditionalclass: 'drop_class_7',
					imgsrc: imgpath + '7.jpg',
					textdata: data.string.ex_7
				},{
					droppabledivadditionalclass: 'drop_class_8',
					imgsrc: imgpath + '8.jpg',
					textdata: data.string.ex_8
				}]

			}
		],
		textblock:[{
			textclass:'clicklast',
			textdata:data.string.p2next
		}]
	},
];

/*remove this for non random questions*/

content[0].exerciseblock[0].draggableblock.shufflearray();
content[0].exerciseblock[0].droppableblock.shufflearray();

// content.shufflearray();


$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	/*for limiting the questions to 10*/
	var $total_page = 10;
    loadTimelineProgress(1, countNext + 1);

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	/*values in this array is same as the name of images of eggs in image folder*/
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);

		var drop_count = 0;
		if(countNext==0){
 		playaudio(sound_9, $(".dg-1"));
		}

		$(".nep_draggablediv").draggable({
            containment: ".board",
            revert: "invalid",
            appendTo: "body",
						helper:'clone',
            zIndex: 100,
        });
        $('.nep_draggablediv>img').click(function(){
        	sound_ex.stop();
        	var class_name = $(this).parent().attr('class');
        	class_name = parseInt(class_name.replace(/\D/g, ''))-1;
        	// console.log(class_name);
        	sound_ex = soundcontent[class_name];
        	sound_ex.play();
        });
        for(var index=1; index<=$('.nep_droppablediv').length; index++){
        	$('.drop_class_'+index).droppable({
	            accept : ".class_"+index,
	            drop: function(event, ui) {
	            	$(ui.draggable).hide(0);
	            	play_correct_incorrect_sound(1);
	            	$(this).find('.item-text-nep').fadeIn(1000);
	            	drop_count++;
								if(drop_count>7){
								var timer = new Timer(function() {
														create_exercise_menu_bar_match_picture();
														}, 1000);
								timer.pause();
								$('.insturction').animate({"opacity":"0"},1000,"linear");
								$('.clicklast').addClass('slidefromleft').click(function(){
									timer.resume();
								});
							}
            }
	        });
        }
	}

	function Timer(callback, delay) {
		var timerId, start, remaining = delay;

		this.pause = function() {
				window.clearTimeout(timerId);
				remaining -= new Date() - start;
		};

		this.resume = function() {
				start = new Date();
				window.clearTimeout(timerId);
				timerId = window.setTimeout(callback, remaining);
		};

		this.resume();
}
function playaudio(sound_data, $dialog_container){
		var playing = true;
		$dialog_container.removeClass("playable");
		$dialog_container.click(function(){
			if(!playing){
				playaudio(sound_data, $dialog_container);
			}
			return false;
		});
		$prevBtn.hide(0);
		if((countNext+1) == content.length){
			ole.footerNotificationHandler.hideNotification();
		}else{
			$nextBtn.hide(0);
		}
		sound_data.play();
		sound_data.bind('ended', function(){
			setTimeout(function(){
				if(countNext != 0)
				$prevBtn.show(0);
				$dialog_container.addClass("playable");
				playing = false;
				sound_data.unbind('ended');


			});
		});
	}
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

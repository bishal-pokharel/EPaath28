var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_animals = new buzz.sound((soundAsset + "animals.ogg"));
var animal_1 = new buzz.sound((soundAsset + "animal_1.ogg"));
var animal_2 = new buzz.sound((soundAsset + "animal_2.ogg"));
var s_animals = [animal_1, animal_2];


var sound_jewels= new buzz.sound((soundAsset + "jewels.ogg"));
var jewel_1 = new buzz.sound((soundAsset + "jewel_1.ogg"));
var jewel_2 = new buzz.sound((soundAsset + "jewel_2.ogg"));
var jewel_3 = new buzz.sound((soundAsset + "jewel_3.ogg"));
var s_jewels = [jewel_1,jewel_2,jewel_3];


var sound_pulses = new buzz.sound((soundAsset + "grains.ogg"));
var grain_1 = new buzz.sound((soundAsset + "grain_1.ogg"));
var grain_2 = new buzz.sound((soundAsset + "grain_2.ogg"));
var grain_3 = new buzz.sound((soundAsset + "grain_3.ogg"));
var grain_4 = new buzz.sound((soundAsset + "grain_4.ogg"));
var s_grains = [grain_1, grain_2, grain_3, grain_4];

var sound_vegetables = new buzz.sound((soundAsset + "vegetables.ogg"));
var veg_5 = new buzz.sound((soundAsset + "veg_1.ogg"));
var veg_2 = new buzz.sound((soundAsset + "veg_2.ogg"));
var veg_4 = new buzz.sound((soundAsset + "veg_3.ogg"));
var veg_1 = new buzz.sound((soundAsset + "veg_4.ogg"));
var veg_3 = new buzz.sound((soundAsset + "veg_5.ogg"));
var s_veg = [veg_1, veg_2, veg_3, veg_4, veg_5];


var sound_fruits = new buzz.sound((soundAsset + "fruits.ogg"));
var fruit_1 = new buzz.sound((soundAsset + "fruit_1.ogg"));
var fruit_2 = new buzz.sound((soundAsset + "fruit_2.ogg"));
var fruit_3 = new buzz.sound((soundAsset + "fruit_3.ogg"));
var fruit_4 = new buzz.sound((soundAsset + "fruit_4.ogg"));
var s_fruits = [fruit_1, fruit_2, fruit_3, fruit_4];


var sound_spices = new buzz.sound((soundAsset + "spices.ogg"));
var spice_1 = new buzz.sound((soundAsset + "spice_1.ogg"));
var spice_2 = new buzz.sound((soundAsset + "spice_2.ogg"));
var spice_3 = new buzz.sound((soundAsset + "spice_3.ogg"));
var s_spices = [spice_1, spice_2, spice_3];


var sound_handicraft = new buzz.sound((soundAsset + "handicraft.ogg"));
var handi_1 = new buzz.sound((soundAsset + "handi_1.ogg"));
var handi_2 = new buzz.sound((soundAsset + "handi_2.ogg"));
var handi_3 = new buzz.sound((soundAsset + "handi_3.ogg"));
var s_handi = [handi_1, handi_2, handi_3];

var total_sounds = [s_fruits, s_grains, s_jewels, s_animals, s_handi, s_spices, s_veg];


var sound_introduce = new buzz.sound((soundAsset + "p2_intro_1.ogg"));

var global_sound_var = sound_animals;

// var sound_group_b = [sound_ques, sound_3_00, sound_5_30, sound_6_00, sound_7_00, sound_8_30, sound_12_30];

var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_main',
		
		uppertextblockadditionalclass: 'introduction_4',
		uppertextblock : [{
			textdata : data.string.p2text0,
			textclass : 'introduction_text'
		}],
		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			// {
				// imgclass : "sound_icon sound_button",
				// imgsrc : imgpath + "speaker.png",
			// },
			{
				imgclass : "bazaar_items blink animals_img",
				imgsrc : imgpath + "animals.png",
			},
			{
				imgclass : "bazaar_items blink pulses_img",
				imgsrc : imgpath + "dhan.png",
			},
			{
				imgclass : "bazaar_items blink jewels_img",
				imgsrc : imgpath + "doripate.png",
			},
			{
				imgclass : "bazaar_items blink vegetables_img",
				imgsrc : imgpath + "vegetables.png",
			},
			{
				imgclass : "bazaar_items blink fruits_img",
				imgsrc : imgpath + "fruits.png",
			},
			{
				imgclass : "bazaar_items blink handicraft_img",
				imgsrc : imgpath + "handicraft.png",
			},
			{
				imgclass : "bazaar_items blink spices_img",
				imgsrc : imgpath + "jeera.png",
			}],
		}],
		
		divdescriptionblock:[
		{
			descriptionblockadditionalclass: 'handicraft',
			descriptionblock : [{
				each_item: [
					{
						each_item_class: 'doko',
						imgclass : '',
						imgsrc : imgpath + 'dalo.png',
						imagelabelclass : "",
						imagelabeldata : data.string.item1_1
					},
					{
						each_item_class: 'dalo',
						imgclass : '',
						imgsrc : imgpath + 'doko.png',
						imagelabelclass : "",
						imagelabeldata : data.string.item1_2
					},
					{
						each_item_class: 'supo',
						imgclass : '',
						imgsrc : imgpath + 'supo.png',
						imagelabelclass : "",
						imagelabeldata : data.string.item1_3
					}
				],
				titlediv: '',
				titleclass:'title_text',
				titledata: data.string.group1,
			}],
		},
		{
			descriptionblockadditionalclass: 'fruits',
			descriptionblock : [{
				each_item: [
					{
						each_item_class: 'grapes',
						imgclass : '',
						imgsrc : imgpath + 'grapes.png',
						imagelabelclass : "",
						imagelabeldata : data.string.item2_1
					},
					{
						each_item_class: 'litchi',
						imgclass : '',
						imgsrc : imgpath + 'litchi.png',
						imagelabelclass : "",
						imagelabeldata : data.string.item2_2
					},
					{
						each_item_class: 'pineapple',
						imgclass : '',
						imgsrc : imgpath + 'pineapple.png',
						imagelabelclass : "",
						imagelabeldata : data.string.item2_3
					},
					{
						each_item_class: 'watermelon',
						imgclass : '',
						imgsrc : imgpath + 'watermelon.png',
						imagelabelclass : "",
						imagelabeldata : data.string.item2_4
					}
				],
				titlediv: '',
				titleclass:'title_text',
				titledata: data.string.group2,
			}],
		},{
			descriptionblockadditionalclass: 'pulses',
			descriptionblock : [{
				each_item: [
					{
						each_item_class: 'makai',
						imgclass : '',
						imgsrc : imgpath + 'makai.png',
						imagelabelclass : "",
						imagelabeldata : data.string.item3_1
					},
					{
						each_item_class: 'gahu',
						imgclass : '',
						imgsrc : imgpath + 'gahu.png',
						imagelabelclass : "",
						imagelabeldata : data.string.item3_2
					},
					{
						each_item_class: 'kodo',
						imgclass : '',
						imgsrc : imgpath + 'kodo.png',
						imagelabelclass : "",
						imagelabeldata : data.string.item3_3
					},
					{
						each_item_class: 'dhan',
						imgclass : '',
						imgsrc : imgpath + 'dhan01.png',
						imagelabelclass : "",
						imagelabeldata : data.string.item3_4
					}
				],
				titlediv: '',
				titleclass:'title_text',
				titledata: data.string.group3,
			}],
		},{
			descriptionblockadditionalclass: 'jewels',
			descriptionblock : [{
				each_item: [
					{
						each_item_class: 'chura',
						imgclass : '',
						imgsrc : imgpath + 'chura.png',
						imagelabelclass : "",
						imagelabeldata : data.string.item4_1
					},
					{
						each_item_class: 'pote',
						imgclass : '',
						imgsrc : imgpath + 'pate.png',
						imagelabelclass : "",
						imagelabeldata : data.string.item4_2
					},
					{
						each_item_class: 'tiki',
						imgclass : '',
						imgsrc : imgpath + 'tiki.png',
						imagelabelclass : "",
						imagelabeldata : data.string.item4_3
					}
				],
				titlediv: '',
				titleclass:'title_text',
				titledata: data.string.group4,
			}],
		},{
			descriptionblockadditionalclass: 'animals',
			descriptionblock : [{
				each_item: [
					{
						each_item_class: 'hen',
						imgclass : '',
						imgsrc : imgpath + 'hen.png',
						imagelabelclass : "",
						imagelabeldata : data.string.item5_1
					},
					{
						each_item_class: 'goat',
						imgclass : '',
						imgsrc : imgpath + 'goat.png',
						imagelabelclass : "",
						imagelabeldata : data.string.item5_2
					}
				],
				titlediv: '',
				titleclass:'title_text',
				titledata: data.string.group5,
			}],
		},{
			descriptionblockadditionalclass: 'vegetables',
			descriptionblock : [{
				each_item: [
					{
						each_item_class: 'cauliflower',
						imgclass : '',
						imgsrc : imgpath + 'bhanta.png',
						imagelabelclass : "",
						imagelabeldata : data.string.item6_1
					},
					{
						each_item_class: 'mushroom',
						imgclass : '',
						imgsrc : imgpath + 'mushroom.png',
						imagelabelclass : "",
						imagelabeldata : data.string.item6_2
					},
					{
						each_item_class: 'chilli',
						imgclass : '',
						imgsrc : imgpath + 'chilli.png',
						imagelabelclass : "",
						imagelabeldata : data.string.item6_3
					},
					{
						each_item_class: 'banda',
						imgclass : '',
						imgsrc : imgpath + 'banda.png',
						imagelabelclass : "",
						imagelabeldata : data.string.item6_4
					},
					{
						each_item_class: 'bhanta',
						imgclass : '',
						imgsrc : imgpath + 'broccoli.png',
						imagelabelclass : "",
						imagelabeldata : data.string.item6_5
					}
				],
				titlediv: '',
				titleclass:'title_text',
				titledata: data.string.group6,
			}],
		},{
			descriptionblockadditionalclass: 'spices',
			descriptionblock : [{
				each_item: [
					{
						each_item_class: 'spices_1',
						imgclass : '',
						imgsrc : imgpath + 'jeera01.png',
						imagelabelclass : "",
						imagelabeldata : data.string.item7_1
					},
					{
						each_item_class: 'spices_2',
						imgclass : '',
						imgsrc : imgpath + 'ilatchi_big.png',
						imagelabelclass : "",
						imagelabeldata : data.string.item7_2
					},
					{
						each_item_class: 'spices_3',
						imgclass : '',
						imgsrc : imgpath + 'ilatchi.png',
						imagelabelclass : "",
						imagelabeldata : data.string.item7_3
					}
				],
				titlediv: '',
				titleclass:'title_text',
				titledata: data.string.group7,
			}],
		},
		]
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	var last_page = false;
	var not_next_slide = 0;
	loadTimelineProgress($total_page, countNext + 1);
	
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	
	var sound_count = 0;
	var c_title_sound  =  null;
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if (countNext > 0 && countNext < $total_page - 1) {
			 // $nextBtn.show(0);
			 $prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);
		
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch (countNext) {
		case 0:
			var go_next_counter = [0,0,0, 0,0,0, 0];
			var $current_item;
			var class_name;
			global_sound_var = sound_introduce;
			play_sound(global_sound_var);
			$('.bazaar_items').click(function(){
				global_sound_var.stop();
				$('.bazaar_items').removeClass('blink');
				$(this).data('clicked', true);
				class_name = $(this).attr('class').toString().replace(/bazaar_items/g, '');
				class_name = class_name.replace(/blink/g, '');
				class_name = class_name.replace(/ /g, '');
				stop_playing = false;
				switch(class_name){
					case 'animals_img':
						sound_count = 3;
						$current_item = $('.animals');
						$current_item.show(0);
						c_title_sound = sound_animals;
						global_sound_var = sound_animals;
						global_sound_var.play();
						$('.sound_button').css('pointer-events', 'none');
						$('.sound_button').attr('src', imgpath +"speaker.gif");
						global_sound_var.bindOnce('ended', function(){
							$('.sound_button').attr('src', imgpath +"speaker.png");
							$('.sound_button').css('pointer-events', 'all');
							go_next_counter[0] = 1;
							image_fade_in(0, '.animals', 1);
						});
					break;
					case 'fruits_img':
						sound_count = 0;
						$current_item = $('.fruits');
						$current_item.show(0);
						c_title_sound = sound_fruits;
						global_sound_var = sound_fruits;
						global_sound_var.play();
						$('.sound_button').css('pointer-events', 'none');
						$('.sound_button').attr('src', imgpath +"speaker.gif");
						global_sound_var.bindOnce('ended', function(){
							$('.sound_button').attr('src', imgpath +"speaker.png");
							$('.sound_button').css('pointer-events', 'all');
							go_next_counter[1] = 1;
							image_fade_in(0, '.fruits', 3);
						});
					break;
					case 'jewels_img':
						sound_count = 2;
						$current_item = $('.jewels');
						$current_item.show(0);
						c_title_sound = sound_jewels;
						global_sound_var = sound_jewels;
						global_sound_var.play();
						$('.sound_button').css('pointer-events', 'none');
						$('.sound_button').attr('src', imgpath +"speaker.gif");
						global_sound_var.bindOnce('ended', function(){
							$('.sound_button').attr('src', imgpath +"speaker.png");
							$('.sound_button').css('pointer-events', 'all');
							go_next_counter[2] = 1;
							image_fade_in(0, '.jewels', 2);
						});
					break;
					case 'handicraft_img':
						sound_count = 4;
						$current_item = $('.handicraft');
						$current_item.show(0);
						c_title_sound = sound_handicraft;
						global_sound_var = sound_handicraft;
						global_sound_var.play();
						$('.sound_button').css('pointer-events', 'none');
						$('.sound_button').attr('src', imgpath +"speaker.gif");
						global_sound_var.bindOnce('ended', function(){
							$('.sound_button').attr('src', imgpath +"speaker.png");
							$('.sound_button').css('pointer-events', 'all');
							go_next_counter[3] = 1;
							image_fade_in(0, '.handicraft', 2);
						});
					break;
					case 'pulses_img':
						sound_count = 1;
						$current_item = $('.pulses');
						$current_item.show(0);
						c_title_sound = sound_pulses;
						global_sound_var = sound_pulses;
						global_sound_var.play();
						$('.sound_button').css('pointer-events', 'none');
						$('.sound_button').attr('src', imgpath +"speaker.gif");
						global_sound_var.bindOnce('ended', function(){
							$('.sound_button').attr('src', imgpath +"speaker.png");
							$('.sound_button').css('pointer-events', 'all');
							go_next_counter[4] = 1;
							image_fade_in(0, '.pulses', 3);
						});
					break;
					case 'vegetables_img':
						sound_count = 6;
						$current_item = $('.vegetables');
						$current_item.show(0);
						c_title_sound = sound_vegetables;
						global_sound_var = sound_vegetables;
						global_sound_var.play();
						$('.sound_button').css('pointer-events', 'none');
						$('.sound_button').attr('src', imgpath +"speaker.gif");
						global_sound_var.bindOnce('ended', function(){
							$('.sound_button').attr('src', imgpath +"speaker.png");
							$('.sound_button').css('pointer-events', 'all');
							go_next_counter[5] = 1;
							image_fade_in(0, '.vegetables', 4);
						});
					break;
					case 'spices_img':
						sound_count = 5;
						$current_item = $('.spices');
						$current_item.show(0);
						c_title_sound = sound_spices;
						global_sound_var = sound_spices;
						global_sound_var.play();
						$('.sound_button').css('pointer-events', 'none');
						$('.sound_button').attr('src', imgpath +"speaker.gif");
						global_sound_var.bindOnce('ended', function(){
							$('.sound_button').attr('src', imgpath +"speaker.png");
							$('.sound_button').css('pointer-events', 'all');
							go_next_counter[6] = 1;
							image_fade_in(0, '.spices', 2);
						});
					break;
				}
			});
			
			for(let ii=0; ii<2; ii++){
				$('.animals>.item_array>.each_item').eq(ii).click(function(){
						global_sound_var.stop();
						global_sound_var = s_animals[ii];
						global_sound_var.play();
				});
			}
			for(let ii=0; ii<2; ii++){
				$('.animals>.item_array>.each_item').eq(ii).click(function(){
						global_sound_var.stop();
						global_sound_var = s_animals[ii];
						global_sound_var.play();
				});
			}
			for(let ii=0; ii<5; ii++){
				$('.vegetables>.item_array>.each_item').eq(ii).click(function(){
						global_sound_var.stop();
						global_sound_var = s_veg[ii];
						global_sound_var.play();
				});
			}
			for(let ii=0; ii<3; ii++){
				$('.spices>.item_array>.each_item').eq(ii).click(function(){
						global_sound_var.stop();
						global_sound_var = s_spices[ii];
						global_sound_var.play();
				});
			}
			for(let ii=0; ii<4; ii++){
				$('.pulses>.item_array>.each_item').eq(ii).click(function(){
						global_sound_var.stop();
						global_sound_var = s_grains[ii];
						global_sound_var.play();
				});
			}
			for(let ii=0; ii<4; ii++){
				$('.fruits>.item_array>.each_item').eq(ii).click(function(){
						global_sound_var.stop();
						global_sound_var = s_fruits[ii];
						global_sound_var.play();
				});
			}
			for(let ii=0; ii<3; ii++){
				$('.handicraft>.item_array>.each_item').eq(ii).click(function(){
						global_sound_var.stop();
						global_sound_var = s_handi[ii];
						global_sound_var.play();
				});
			}
			for(let ii=0; ii<3; ii++){
				$('.jewels>.item_array>.each_item').eq(ii).click(function(){
						global_sound_var.stop();
						global_sound_var = s_jewels[ii];
						global_sound_var.play();
				});
			}
			$('.wrong_class').click(function(){
				stop_playing = true;
				global_sound_var.stop();
				$('.each_item').css('pointer-events', 'all');
				global_sound_var = sound_introduce;
				$current_item.hide(0);
				$('.each_item').css('opacity', '0');
				$('.each_item').removeClass('fade_in');
				for(var i=0; i<8; i++){
					var $newEntry = $(".imageblock>img").eq(i);
					if($newEntry.data("clicked")==false){
						$newEntry.addClass('blink');
					}
				}   
				for(var i =0; i< 7; i++){
					if(go_next_counter[i]==0){
						return false;
					}
				}
				ole.footerNotificationHandler.pageEndSetNotification(); 
			});
			break;
		default:
			$prevBtn.show(0);
			$nextBtn.show(0);
			break;
		}
		
		$('.sound_button').click(function(){
			global_sound_var.stop();
			global_sound_var = c_title_sound;
			global_sound_var.play();
			$nextBtn.hide(0);
			$prevBtn.hide(0);
			$('.sound_button').css('pointer-events', 'none');
			$('.sound_button').attr('src', imgpath +"speaker.gif");
			global_sound_var.bindOnce('ended', function(){
				// $('.bazaar_items').css('pointer-events', 'all');
				$('.sound_button').css('pointer-events', 'all');
				$('.sound_button').attr('src', imgpath +"speaker.png");
			});
		});
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   
	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				global_sound_var.stop();
				countNext++;
				templateCaller();
			break;
		}	
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		global_sound_var.stop();
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();
	
	function next_btn_caller (next_display) {
		if(!next_display) {
			return false;
		} else {
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
	}
	
	function play_sound(sound_name){
		global_sound_var.stop();
		global_sound_var = sound_name;
		global_sound_var.play();
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		// $('.bazaar_items').css('pointer-events', 'none');
		// $('.wrong_class').css('pointer-events', 'none');
		$('.sound_button').css('pointer-events', 'none');
		$('.sound_button').attr('src', imgpath +"speaker.gif");
		global_sound_var.bindOnce('ended', function(){
			$('.sound_button').attr('src', imgpath +"speaker.png");
			// $('.bazaar_items').css('pointer-events', 'all');
			// $('.wrong_class').css('pointer-events', 'all');
			$('.sound_button').css('pointer-events', 'all');
		});
	}
	
	function image_fade_in(count, class_current, max_no){
		if(!stop_playing){
			if( typeof total_sounds[sound_count][count] != 'undefined'){
				global_sound_var.stop();
				global_sound_var = total_sounds[sound_count][count];
				global_sound_var.play();
				$('.each_item').css('pointer-events', 'none');
				$('.sound_button').css('pointer-events', 'none');
				
				$(class_current+'>.item_array>.each_item').eq(count).css({'opacity':'1'});
				$(class_current+'>.item_array>.each_item').eq(count).addClass('fade_in');
				// $('.wrong_class').css('pointer-events', 'none');
				
				global_sound_var.bindOnce('ended', function() {
				  	if(count<=max_no){
				  		count++;
				  		// console.log('hen'+count+'       '+count);
					  	image_fade_in(count, class_current, max_no);
				  	}
				});
			} else{
				$('.each_item').css('pointer-events', 'all');
				$('.sound_button').css('pointer-events', 'all');
		  	}
		}
	}
	
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_p1_intro_1 = new buzz.sound((soundAsset + "s1_p1.ogg"));
var sound_p1_intro_2= new buzz.sound((soundAsset + "s1_p2.ogg"));
var sound_p1_intro_3 = new buzz.sound((soundAsset + "s1_p3.ogg"));
var sound_p1_intro_4 = new buzz.sound((soundAsset + "s1_p4.ogg"));
var sound_p1_intro_5 = new buzz.sound((soundAsset + "s1_p5.ogg"));

var content = [

	//slide0
	{
		contentblockadditionalclass : 'titlebg',
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		uppertextblockadditionalclass: 'lesson_title',
		uppertextblock : [{
			textdata : data.lesson.chapter,
			textclass : ''
		}]
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_house',

		uppertextblockadditionalclass: 'introduction',
		uppertextblock : [{
			textdata : data.string.p1text0,
			textclass : ''
		}],
		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "sound_icon",
				imgsrc : imgpath + "speaker.png",
			},
			{
				imgclass : "meera",
				imgsrc : imgpath + "meera.png",
			}],
		}]
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_house',

		uppertextblockadditionalclass: 'introduction',
		uppertextblock : [{
			textdata : data.string.p1text1,
			textclass : ''
		}],
		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "sound_icon",
				imgsrc : imgpath + "speaker.png",
			},
			{
				imgclass : "meera",
				imgsrc : imgpath + "meera.png",
			}],
		}]
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_house',

		uppertextblockadditionalclass: 'introduction',
		uppertextblock : [{
			textdata : data.string.p1text2,
			textclass : ''
		}],
		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "sound_icon",
				imgsrc : imgpath + "speaker.png",
			},
			{
				imgclass : "meera",
				imgsrc : imgpath + "meera.png",
			},
			{
				imgclass : "imagine_image",
				imgsrc : imgpath + "uncleandaunt.png",
			},
			{
				imgclass : "circle1",
				imgsrc : imgpath + "circle.png",
			},
			{
				imgclass : "circle2",
				imgsrc : imgpath + "circle.png",
			}],
		imagelabels:[
			{
				imagelabelclass : "white_cover",
				imagelabeldata : '',
			}]
		}]
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_house',

		uppertextblockadditionalclass: 'introduction',
		uppertextblock : [{
			textdata : data.string.p1text3,
			textclass : ''
		}],
		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "sound_icon",
				imgsrc : imgpath + "speaker.png",
			},
			{
				imgclass : "meera",
				imgsrc : imgpath + "meera.png",
			},
			{
				imgclass : "imagine_image",
				imgsrc : imgpath + "elderaunt.png",
			},
			{
				imgclass : "circle1",
				imgsrc : imgpath + "circle.png",
			},
			{
				imgclass : "circle2",
				imgsrc : imgpath + "circle.png",
			}],
			imagelabels:[
			{
				imagelabelclass : "white_cover",
				imagelabeldata : '',
			}]
		}]
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	var last_page = false;
	var not_next_slide = 0;
	loadTimelineProgress($total_page, countNext + 1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			 // $nextBtn.show(0);
			 $prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch (countNext) {
		case 0:
			play_sound(sound_p1_intro_1);
			$('.sound_icon').click(function(){
				play_sound(sound_p1_intro_1);
			});
			break;
		case 1:
			play_sound(sound_p1_intro_2);
			$('.sound_icon').click(function(){
				play_sound(sound_p1_intro_2);
			});
			break;
		case 2:
			play_sound(sound_p1_intro_3);
			$('.sound_icon').click(function(){
				play_sound(sound_p1_intro_3);
			});
			break;
		case 3:
			play_sound(sound_p1_intro_4);
			$('.sound_icon').click(function(){
				play_sound(sound_p1_intro_4);
			});
			break;
		case 4:
			play_sound(sound_p1_intro_5);
			$('.sound_icon').click(function(){
				play_sound(sound_p1_intro_5);
			});
			break;
		default:
			$prevBtn.show(0);
			$nextBtn.show(0);
			break;
		}
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				countNext++;
				templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();

	function next_btn_caller (next_display) {
		if(!next_display) {
			return false;
		} else {
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
	}

	function play_sound(sound_name){
		sound_name.play();
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		$('.sound_icon').attr('src', imgpath +"speaker.gif");
		sound_name.bindOnce('ended', function(){
			$('.sound_icon').attr('src', imgpath +"speaker.png");
			if(countNext != 4){
				$nextBtn.show(0);
			}
			$prevBtn.show(0);
			if(countNext == $total_page-1){
				ole.footerNotificationHandler.pageEndSetNotification();
				$prevBtn.show(0);
			}
		});
	}

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'text-story ts-1 fade-in-3',
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "",
		}],
		imagedivblock:[{
			imagediv : 'bg-extended pos-1 zoom-out-1',
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'im-1'
				},
				{
					imgclass : "turtle",
					imgsrc : '',
					imgid : 'tor-2'
				},
				{
					imgclass : "duck-a",
					imgsrc : '',
					imgid : 'duck-a1'
				},
				{
					imgclass : "duck-b",
					imgsrc : '',
					imgid : 'duck-b2'
				}
			]
		}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'text-story ts-1 fade-in-3',
		uppertextblock:[{
			textdata: data.string.p2text2,
			textclass: "",
		}],
		imagedivblock:[{
			imagediv : 'bg-extended pos-1 zoom-out-2',
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'im-1'
				},
				{
					imgclass : "turtle",
					imgsrc : '',
					imgid : 'tor-1'
				},
				{
					imgclass : "duck-a",
					imgsrc : '',
					imgid : 'duck-a1'
				},
				{
					imgclass : "duck-b",
					imgsrc : '',
					imgid : 'duck-b1'
				}
			]
		}]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'text-story ts-1 fade-in-2',
		uppertextblock:[{
			textdata: data.string.p2text3,
			textclass: "",
		}],

		imagedivblock:[{
			imagediv : 'bg-extended pos-2 s2-anim-1',
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'im-1'
				},
				{
					imgclass : "turtle",
					imgsrc : '',
					imgid : 'tor-1'
				},
				{
					imgclass : "duck-a",
					imgsrc : '',
					imgid : 'duck-a1'
				},
				{
					imgclass : "duck-b",
					imgsrc : '',
					imgid : 'duck-b1'
				}
			]
		}]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'text-story ts-1 fade-in-2',
		uppertextblock:[{
			textdata: data.string.p2text4,
			textclass: "",
		}],

		imagedivblock:[{
			imagediv : 'bg-extended pos-2 s3-anim-1',
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'im-1'
				},
				{
					imgclass : "turtle",
					imgsrc : '',
					imgid : 'tor-6'
				},
				{
					imgclass : "duck-a",
					imgsrc : '',
					imgid : 'duck-a5'
				},
				{
					imgclass : "duck-b",
					imgsrc : '',
					imgid : 'duck-b2'
				}
			]
		}]
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'end-dialouge end-anim wobble',
		uppertextblock:[{
			textdata: data.string.p2text5,
			textclass: "",
		}],
		extratextblock:[{
			textdata: '',
			textclass: "overlay-endpage",
		}],
		imagedivblock:[{
			imagediv : 'bg-extended pos-2',
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'im-1'
				},
				{
					imgclass : "turtle",
					imgsrc : '',
					imgid : 'tor-5'
				},
				{
					imgclass : "duck-a",
					imgsrc : '',
					imgid : 'duck-a3'
				},
				{
					imgclass : "duck-b",
					imgsrc : '',
					imgid : 'duck-b4'
				}
			]
		}]
	}

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var intervalvar = null;
	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "im-1", src: imgpath+"p2/bg-02.svg", type: createjs.AbstractLoader.IMAGE},

			{id: "tor-1", src: imgpath+"p2/tortoise.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tor-2", src: imgpath+"p2/tortoise01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tor-3", src: imgpath+"p2/tortoise02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tor-4", src: imgpath+"p2/tortoise03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tor-5", src: imgpath+"p2/tortoise04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tor-6", src: imgpath+"p2/tortoise05.png", type: createjs.AbstractLoader.IMAGE},

			{id: "duck-a1", src: imgpath+"p2/duck01a.png", type: createjs.AbstractLoader.IMAGE},
			{id: "duck-a2", src: imgpath+"p2/duck01b.png", type: createjs.AbstractLoader.IMAGE},
			{id: "duck-a3", src: imgpath+"p2/duck01c.png", type: createjs.AbstractLoader.IMAGE},
			{id: "duck-a4", src: imgpath+"p2/duck01d.png", type: createjs.AbstractLoader.IMAGE},
			{id: "duck-a5", src: imgpath+"p2/duck01e.png", type: createjs.AbstractLoader.IMAGE},

			{id: "duck-b1", src: imgpath+"p2/duck02a.png", type: createjs.AbstractLoader.IMAGE},
			{id: "duck-b2", src: imgpath+"p2/duck02b.png", type: createjs.AbstractLoader.IMAGE},
			{id: "duck-b3", src: imgpath+"p2/duck02c.png", type: createjs.AbstractLoader.IMAGE},
			{id: "duck-b4", src: imgpath+"p2/duck02d.png", type: createjs.AbstractLoader.IMAGE},

			{id: "thinkbox", src: imgpath+"thinkbox.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_1", src: soundAsset+"p2_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p2_s1.ogg"},
			{id: "sound_3", src: soundAsset+"p2_s2.ogg"},
			{id: "sound_3a", src: soundAsset+"p2_s2a.ogg"},
			{id: "sound_4", src: soundAsset+"p2_s3.ogg"},
			{id: "sound_4a", src: soundAsset+"p2_s3a.ogg"},
			{id: "sound_4b", src: soundAsset+"p2_s3b.ogg"},
			{id: "sound_5", src: soundAsset+"p2_s4.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);

		switch(countNext) {
			case 0:
				timeoutvar = setTimeout(function(){
					sound_nav('sound_1');
				}, 3000);
				break;
			case 1:
				$prevBtn.show(0);
				timeoutvar = setTimeout(function(){
					sound_nav('sound_2');
				}, 3000);
				break;
			case 2:
				$prevBtn.show(0);
				timeoutvar = setTimeout(function(){
					$('.turtle').attr('src',preload.getResult('tor-2').src);
					$('.duck-a').attr('src',preload.getResult('duck-a4').src);
					$('.duck-b').attr('src',preload.getResult('duck-b2').src);
					current_sound = createjs.Sound.play('sound_3');
					current_sound.play();
					current_sound.on("complete", function(){
						$('.bg-extended').removeClass('s2-anim-1');
						$('.bg-extended').addClass('s2-anim-2');
						timeoutvar =  setTimeout(function(){
							$('.bg-extended').animate({
								'left':'-16%'
								}, 1500, function(){
									timeoutvar =  setTimeout(function(){
										$('.turtle').attr('src',preload.getResult('tor-6').src);
										$('.bg-extended').animate({
											'left':'22%'
										}, 1000);
									}, 1500);
								});
						}, 3500);
						sound_nav('sound_3a');
					});
				}, 2700);
				break;
			case 3:
				$prevBtn.show(0);
				var eye_flag = false;
				timeoutvar = setTimeout(function(){
					current_sound = createjs.Sound.play('sound_4');
					current_sound.play();
					current_sound.on("complete", function(){
						intervalvar = setInterval(function(){
							eye_flag?$('.turtle').attr('src',preload.getResult('tor-4').src):$('.turtle').attr('src',preload.getResult('tor-5').src);
							eye_flag = !eye_flag;
						}, 500);
							$('.turtle').attr('src',preload.getResult('tor-5').src);
							$('.duck-b').attr('src',preload.getResult('duck-b3').src);
							clearInterval(intervalvar);
							current_sound = createjs.Sound.play('sound_4a');
							current_sound.play();
							current_sound.on("complete", function(){
								$('.duck-b').attr('src',preload.getResult('duck-b4').src);
								sound_nav('sound_4b');
							});
					});
				}, 2700);
				break;
			case 4:
				$prevBtn.show(0);
				$('.end-dialouge').css({
					'background-image': 'url("'+ preload.getResult('thinkbox').src +'")',
					'background-size': '100% 100%',
				});
				sound_nav('sound_'+(countNext+1));
				break;
			default:
				$prevBtn.show(0);
				sound_nav('sound_'+(countNext+1));
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearInterval(intervalvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearInterval(intervalvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

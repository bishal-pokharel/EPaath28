var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'text-story ts-1 fade-in-2',
		uppertextblock:[{
			textdata: data.string.p3text1,
			textclass: "",
		}],
		imagedivblock:[{
			imagediv : 'bg-full bg-div',
			svgdiv: 'bubble',
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'im-1'
				},{
					imgclass : "tortoise",
					imgsrc : '',
					imgid : 'tor-7'
				}
			]
		}],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'text-story ts-1 fade-in-2',
		uppertextblock:[{
			textdata: data.string.p3text2,
			textclass: "",
		}],
		imagedivblock:[{
			imagediv : 'bg-full bg-div',
			svgdiv: 'bubble',
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'im-1'
				},{
					imgclass : "tortoise",
					imgsrc : '',
					imgid : 'tor-8'
				}
			]
		}]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-sky',

		uppertextblockadditionalclass: 'text-story aligned-left ts-2',
		uppertextblock:[{
			textdata: data.string.p3text3,
			textclass: "",
		}],
		imagedivblock:[{
			imagediv : 'bg-full bg-div',
			moveeffect:[{
				moveblock :'mb-1'
			}],
			imagestoshow : [
				{
					imgclass : "cloud-1",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "cloud-2",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "cloud-3",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "cloud-4",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "flying",
					imgsrc : '',
					imgid : 'fly'
				},{
					imgclass : "tortoise-2",
					imgsrc : '',
					imgid : 'tor-9'
				}
			]
		}],
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'text-story aligned-left ts-2',
		uppertextblock:[{
			textdata: data.string.p3text4,
			textclass: "",
		}],
		imagedivblock:[{
			imagediv : 'bg-long',
			imagestoshow : [
				{
					imgclass : "bg-new-1",
					imgsrc : '',
					imgid : 'bg-2'
				},{
					imgclass : "sun",
					imgsrc : '',
					imgid : 'sun'
				},{
					imgclass : "star star-1",
					imgsrc : '',
					imgid : 'star'
				}
			],
			imagelabels:[{
				imagelabelclass: 'sky-top',
				imagelabeldata: '',
			},{
				imagelabelclass: 'sky-bottom',
				imagelabeldata: '',
			}]
		}],
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'end-dialouge  end-anim wobble',
		uppertextblock:[{
			textdata: data.string.p3text5,
			textclass: "",
		}],
		extratextblock:[{
			textdata: '',
			textclass: "overlay-endpage",
		}],

		imagedivblock:[{
			imagediv : 'bg-long bg-final-pos',
			imagestoshow : [
				{
					imgclass : "bg-new-1 bg-new-final",
					imgsrc : '',
					imgid : 'bg-2'
				},{
					imgclass : "star",
					imgsrc : '',
					imgid : 'star'
				},
			],
			imagelabels:[{
				imagelabelclass: 'sky-final',
				imagelabeldata: '',
			}]
		}],
	}

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "im-1", src: imgpath+"p3/bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-2", src: imgpath+"p3/bg32a.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sun", src: imgpath+"p1/sun.png", type: createjs.AbstractLoader.IMAGE},
			{id: "star", src: imgpath+"p3/stars.png", type: createjs.AbstractLoader.IMAGE},

			{id: "bubble", src: imgpath+"p3/bubble.svg", type: createjs.AbstractLoader.IMAGE},

			{id: "cloud", src: imgpath+"p3/clouds.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fly", src: imgpath+"p3/flying.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tor-7", src: imgpath+"p3/tortoise06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tor-8", src: imgpath+"p3/tortoise07.png", type: createjs.AbstractLoader.IMAGE},

			{id: "fly-2", src: imgpath+"p3/duckwithstick.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tor-9", src: imgpath+"p3/tortoise08.png", type: createjs.AbstractLoader.IMAGE},

			{id: "fall-gif", src: imgpath+"fall.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "thinkbox", src: imgpath+"thinkbox.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_1", src: soundAsset+"p3_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p3_s1.ogg"},
			{id: "sound_3", src: soundAsset+"p3_s2.ogg"},
			{id: "sound_4", src: soundAsset+"p3_s3.ogg"},
			{id: "sound_4a", src: soundAsset+"p3_s3a.ogg"},
			{id: "sound_5", src: soundAsset+"p3_s4.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);

		switch(countNext) {
			case 0:
				var bubble, bub1, bub2;
				var s = Snap('#bubble');
				var svg = Snap.load(preload.getResult('bubble').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					bubble			= Snap.select('#bg');
					bub1			= Snap.select('#bubble01');
					bub2			= Snap.select('#bubble02');
					bubble.addClass('anim-s1-3');
					bub1.addClass('anim-s1-2');
					bub2.addClass('anim-s1-1');
				} );
				timeoutvar = setTimeout(function(){
					sound_nav('sound_1');
				}, 3000);
				break;
			case 1:
				$prevBtn.show(0);
				var s = Snap('#bubble');
				var bubble, bp1, bp2;
				var bub1, bub2;
				var svg = Snap.load(preload.getResult('bubble').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					bubble			= Snap.select('#path01');
					bub1			= Snap.select('#bubble01');
					bub2			= Snap.select('#bubble02');
					var path_2		= Snap.select("#path02");
					bp1		= bubble.attr('path');
					bp2		= path_2.attr('path');
					bub1.addClass('bubb');
					bub2.addClass('bubb');
				} );
				function move_to_2(){
					bubble.animate({ d: bp2 }, 2000, mina.easein, function(){
					});
				}
				function zoomBubble(){
					$('.bubb').fadeOut(500, function(){
						$('#bubble').animate({
							'width': '106%',
							'height': '106%',
							'left': '-3%',
							'top': '-3%'
						}, 2000);
						move_to_2();
						nav_button_controls(0);
					});
				}
				timeoutvar = setTimeout(function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play('sound_2');
					current_sound.play();
					current_sound.on("complete", function(){
						zoomBubble();
					});
				}, 3000);
				break;
			case 2:
				$prevBtn.show(0);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play('sound_3');
				current_sound.play();
				current_sound.on("complete", function(){
					for(var i=1; i<5; i++){
						$('.cloud-'+i).addClass('cloud-anim-'+i);
					}
					$('.flying').attr('src', preload.getResult('fly-2').src);
					$('.flying').addClass('fly-out');
					$('.tortoise-2').show(0);
					$('.tortoise-2').attr('src', preload.getResult('fall-gif').src);
					$('.mb-1').show(0);
					$('.bg-div').addClass('bg-zoom-1');
					timeoutvar = setTimeout(function(){
						$('.cloud-3').css('left', '60%');
						$('.cloud-4').css('left', '26%');
						$nextBtn.show(0);
					}, 6000);
				});
				break;
			case 3:
				$prevBtn.show(0);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play('sound_4');
				current_sound.play();
				current_sound.on("complete", function(){
					$('.bg-long').addClass('bg-pan-2');
					sound_nav('sound_4a');
					timeoutvar = setTimeout(function(){
						$('.bg-new-1').addClass('bg-anim-1');
						$('.sky-top').addClass('sky-anim');
						$('.sky-bottom').addClass('sky-anim-2');
						$('.sun').addClass('sun-anim');
						$('.star').addClass('star-anim');
					}, 3000);
				});
				break;
			case 4:
				$prevBtn.show(0);
				$('.end-dialouge').css({
					'background-image': 'url("'+ preload.getResult('thinkbox').src +'")',
					'background-size': '100% 100%',
				});
				sound_nav('sound_5');
				break;
			default:
				$prevBtn.show(0);
				sound_nav('sound_'+(countNext+1));
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

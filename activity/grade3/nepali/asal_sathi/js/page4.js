var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'text-story ts-1',
		uppertextblock:[{
			textdata: data.string.p4text1,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "turtle head-anim t-head",
					imgsrc : '',
					imgid : 'head'
				},
				{
					imgclass : "turtle t-body",
					imgsrc : '',
					imgid : 'body'
				}
			]
		}],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'text-story ts-2',
		uppertextblock:[{
			textdata: data.string.p4text2,
			textclass: "",
		}],
		imagedivblock:[{
			imagediv : 'bg-full',
			imagestoshow : [
				{
					imgclass : "cloud-anim-1 cloud-1",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "cloud-anim-2 cloud-2",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "cloud-anim-3 cloud-3",
					imgsrc : '',
					imgid : 'cloud-2'
				},
				{
					imgclass : "cloud-anim-4 cloud-4",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "flying",
					imgsrc : '',
					imgid : 'fly-gif'
				},
				{
					imgclass : "hill-anim hill",
					imgsrc : '',
					imgid : 'hill'
				},
			],
			imagelabels:[{
				imagelabelclass: 'bg-sky',
				imagelabeldata: '',
			}]
		}],
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'text-story ts-2',
		uppertextblock:[{
			textdata: data.string.p4text3,
			textclass: "",
		}],

		imagedivblock:[{
			imagediv : 'bg-full',
			imagestoshow : [
				{
					imgclass : "cloud-anim-1 cloud-1",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "cloud-anim-2 cloud-2",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "cloud-anim-3 cloud-3",
					imgsrc : '',
					imgid : 'cloud-2'
				},
				{
					imgclass : "cloud-anim-4 cloud-4",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "flying",
					imgsrc : '',
					imgid : 'fly-gif'
				},
				{
					imgclass : "hill-anim hill",
					imgsrc : '',
					imgid : 'hill'
				},
			],
			imagelabels:[{
				imagelabelclass: 'bg-sky',
				imagelabeldata: '',
			}]
		}],
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'text-story ts-2',
		uppertextblock:[{
			textdata: data.string.p4text4,
			textclass: "",
		}],
		imagedivblock:[{
			imagediv : 'bg-full bg-zoom-sunset',
			imagestoshow : [
				{
					imgclass : "cloud-anim-1 cloud-1",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "cloud-anim-2 cloud-2",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "cloud-anim-3 cloud-3",
					imgsrc : '',
					imgid : 'cloud-2'
				},
				{
					imgclass : "cloud-anim-4 cloud-4",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "flying",
					imgsrc : '',
					imgid : 'fly-gif'
				},
				{
					imgclass : "hill-anim hill",
					imgsrc : '',
					imgid : 'hill'
				},
				{
					imgclass : "sun-anim sun ",
					imgsrc : '',
					imgid : 'sun'
				},
			],
			imagelabels:[{
				imagelabelclass: 'sky-2 fade-in-slow',
				imagelabeldata: '',
			},{
				imagelabelclass: 'sky-1 fade-out-slow-2',
				imagelabeldata: '',
			},{
				imagelabelclass: 'bg-sky fade-out-slow',
				imagelabeldata: '',
			}]
		}],
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'text-story ts-2',
		uppertextblock:[{
			textdata: data.string.p4text5,
			textclass: "",
		}],
		imagedivblock:[{
			imagediv : 'bg-full bg-darker-time',
			imagestoshow : [
				{
					imgclass : "flypool-anim bg-flypool",
					imgsrc : '',
					imgid : 'pond-bg'
				},{
					imgclass : "tor-top tor-top-anim",
					imgsrc : '',
					imgid : 'tor-top'
				}
			]
		}],
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'text-story ts-3',
		uppertextblock:[{
			textdata: data.string.p4text6,
			textclass: "",
		},{
			textdata: data.string.p4text7,
			textclass: "",
		}],

		imagedivblock:[{
			imagediv : 'bg-full bg-darker-time',
			moveeffect:[{
				moveblock :'mb-1'
			}],
			imagestoshow : [
				{
					imgclass : "clouda-1",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "clouda-2",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "clouda-3",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "clouda-4",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "flying-1",
					imgsrc : '',
					imgid : 'fly-1'
				},{
					imgclass : "tortoise-2",
					imgsrc : '',
					imgid : 'tor-9'
				}
			],
			imagelabels:[{
				imagelabelclass: 'bg-sky-2',
				imagelabeldata: '',
			}]
		}],
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'end-dialouge  end-anim wobble',
		uppertextblock:[{
			textdata: data.string.p4text8,
			textclass: "",
		}],
		extratextblock:[{
			textdata: '',
			textclass: "overlay-endpage",
		}],

		imagedivblock:[{
			imagediv : 'bg-full bg-darker-time',
			moveeffect:[{
				moveblock :'mb-2'
			}],
			imagestoshow : [
				{
					imgclass : "clouda-3 clouda-anim-3",
					imgsrc : '',
					imgid : 'cloud'
				},
				{
					imgclass : "clouda-4 clouda-anim-4",
					imgsrc : '',
					imgid : 'cloud'
				},{
					imgclass : "tortoise-3",
					imgsrc : '',
					imgid : 'fall-gif'
				}
			],
			imagelabels:[{
				imagelabelclass: 'bg-sky-2',
				imagelabeldata: '',
			}]
		}],
	}

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var intervalvar =  null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bg", src: imgpath+"p4/page04_bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "head", src: imgpath+"p4/tortle_head.png", type: createjs.AbstractLoader.IMAGE},
			{id: "body", src: imgpath+"p4/tortle_body.png", type: createjs.AbstractLoader.IMAGE},

			{id: "cloud", src: imgpath+"p3/clouds.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-2", src: imgpath+"p1/cloud02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "flying", src: imgpath+"p4/flying.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hill", src: imgpath+"p4/hill.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sun", src: imgpath+"p4/sun.png", type: createjs.AbstractLoader.IMAGE},

			{id: "fly-1", src: imgpath+"p3/flying.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fly-2", src: imgpath+"p3/duckwithstick.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tor-9", src: imgpath+"p3/tortoise08.png", type: createjs.AbstractLoader.IMAGE},

			{id: "pond-bg", src: imgpath+"p4/bg_tortlelookingfromtop.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tor-top", src: imgpath+"p4/tortlelookingfromtop.png", type: createjs.AbstractLoader.IMAGE},

			{id: "fly-gif", src: imgpath+"flying.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "fall-gif", src: imgpath+"fall.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "thinkbox", src: imgpath+"thinkbox.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_1", src: soundAsset+"p4_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p4_s1.ogg"},
			{id: "sound_3", src: soundAsset+"p4_s2.ogg"},
			{id: "sound_4", src: soundAsset+"p4_s3.ogg"},
			{id: "sound_5", src: soundAsset+"p4_s4.ogg"},
			{id: "sound_6", src: soundAsset+"p4_s5.ogg"},
			{id: "sound_7", src: soundAsset+"p4_s6.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);

		switch(countNext) {
			case 0:
				sound_nav('sound_1');
				break;
			case 3:
				$prevBtn.show(0);
				var intCounter = 0;
				sound_player('sound_4');
				timeoutvar = setTimeout(function(){
					intervalvar = setInterval(function(){
						intCounter++;
						var brightnessVal = (100 - intCounter*0.3);
						$('.bg-full').css({
							'filter': 'brightness('+ brightnessVal +'%)',
							'-webkit-filter': 'brightness('+ brightnessVal +'%)',
						});
						if(intCounter>=20){
							clearInterval(intervalvar);
							$nextBtn.show(0);
						}
					}, 200);
				}, 10000);
				break;
			case 5:
				$prevBtn.show(0);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play('sound_6');
				current_sound.play();
				current_sound.on("complete", function(){
					for(var i=1; i<5; i++){
						$('.clouda-'+i).addClass('clouda-anim-'+i);
					}
					$('.flying-1').attr('src', preload.getResult('fly-2').src);
					$('.flying-1').addClass('fly-out');
					$('.tortoise-2').show(0);
					$('.tortoise-2').attr('src', preload.getResult('fall-gif').src);
					$('.mb-1').show(0);
					$nextBtn.show(0);
				});
				break;
			case 6:
				$prevBtn.show(0);
				$('.end-dialouge').css({
					'background-image': 'url("'+ preload.getResult('thinkbox').src +'")',
					'background-size': '100% 100%',
				});
				sound_nav('sound_'+(countNext+1));
				break;
			default:
				$prevBtn.show(0);
				sound_nav('sound_'+(countNext+1));
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearInterval(intervalvar);
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearInterval(intervalvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

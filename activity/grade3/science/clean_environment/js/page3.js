var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+ $lang+"/";

var content=[
{
	contentblockadditionalclass: "bluesky contentwithbg",
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "stone1 noanim",
			imgsrc: imgpath + "stone01.png"
		},
		{
			imgclass: "stone3 noanim",
			imgsrc: imgpath + "bush.png"
		},
		{
			imgclass: "tree1 noanim",
			imgsrc: imgpath + "cutdowntreeone.png"
		},
		{
			imgclass: "tree2 noanim",
			imgsrc: imgpath + "cutdowntree_two.png"
		},
		{
			imgclass: "tree3 noanim",
			imgsrc: imgpath + "cutdowntree_four.png"
		},
		{
			imgclass: "tree4 noanim",
			imgsrc: imgpath + "cutdowntreeone.png"
		},
		{
			imgclass: "flower1 noanim",
			imgsrc: imgpath + "flowerthree.png"
		},
		{
			imgclass: "flower2 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "bush1 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "house1 noanim",
			imgsrc: imgpath + "hosue.png"
		},
		{
			imgclass: "house2 noanim",
			imgsrc: imgpath + "hosue01.png"
		},
		{
			imgclass: "house3 noanim",
			imgsrc: imgpath + "animals-shade.png"
		},
		{
			imgclass: "house4 noanim",
			imgsrc: imgpath + "animal-grass.png"
		},
		{
			imgclass: "animals noanim",
			imgsrc: imgpath + "three-animals.png"
		},
		{
			imgclass: "hen noanim",
			imgsrc: imgpath + "hen.png"
		},
		{
			imgclass: "duck noanim",
			imgsrc: imgpath + "duck.png"
		},
		{
			imgclass: "poll1",
			imgsrc: imgpath + "base01.png"
		},
		{
			imgclass: "poll2",
			imgsrc: imgpath + "cowdunk.png"
		},
		{
			imgclass: "poll3",
			imgsrc: imgpath + "base03.png"
		},
		{
			imgclass: "poll4",
			imgsrc: imgpath + "base04.png"
		},
		{
			imgclass: "poll5",
			imgsrc: imgpath + "base02.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "poll6",
	}
	],
	thirdpagediv:[
		{
			tddata: data.string.p3t1,
		}
	]
},
{
	contentblockadditionalclass: "bluesky contentwithbg",
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "stone1 noanim",
			imgsrc: imgpath + "stone01.png"
		},
		{
			imgclass: "stone3 noanim",
			imgsrc: imgpath + "bush.png"
		},
		{
			imgclass: "tree1 noanim",
			imgsrc: imgpath + "cutdowntreeone.png"
		},
		{
			imgclass: "tree2 noanim",
			imgsrc: imgpath + "cutdowntree_two.png"
		},
		{
			imgclass: "tree3 noanim",
			imgsrc: imgpath + "cutdowntree_four.png"
		},
		{
			imgclass: "tree4 noanim",
			imgsrc: imgpath + "cutdowntreeone.png"
		},
		{
			imgclass: "flower1 noanim",
			imgsrc: imgpath + "flowerthree.png"
		},
		{
			imgclass: "flower2 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "bush1 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "house1 noanim",
			imgsrc: imgpath + "hosue.png"
		},
		{
			imgclass: "house2 noanim",
			imgsrc: imgpath + "hosue01.png"
		},
		{
			imgclass: "house3 noanim",
			imgsrc: imgpath + "animals-shade.png"
		},
		{
			imgclass: "house4 noanim",
			imgsrc: imgpath + "animal-grass.png"
		},
		{
			imgclass: "animals noanim",
			imgsrc: imgpath + "three-animals.png"
		},
		{
			imgclass: "hen noanim",
			imgsrc: imgpath + "hen.png"
		},
		{
			imgclass: "duck noanim",
			imgsrc: imgpath + "duck.png"
		},
		{
			imgclass: "poll1",
			imgsrc: imgpath + "base01.png"
		},
		{
			imgclass: "poll2",
			imgsrc: imgpath + "cowdunk.png"
		},
		{
			imgclass: "poll3",
			imgsrc: imgpath + "base03.png"
		},
		{
			imgclass: "poll4",
			imgsrc: imgpath + "base04.png"
		},
		{
			imgclass: "poll5",
			imgsrc: imgpath + "base02.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "poll6",
	}
	],
	thirdpagediv:[
		{
			tddata: data.string.p3t2,
			tdimgclass: "tdimg clickable",
			tdsrc: imgpath + "page3/twodusbin.png"
		}
	]
},
{
	contentblockadditionalclass: "bluesky contentwithbg",
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "stone1 noanim",
			imgsrc: imgpath + "stone01.png"
		},
		{
			imgclass: "stone3 noanim",
			imgsrc: imgpath + "bush.png"
		},
		{
			imgclass: "tree1 noanim",
			imgsrc: imgpath + "cutdowntreeone.png"
		},
		{
			imgclass: "tree2 noanim",
			imgsrc: imgpath + "cutdowntree_two.png"
		},
		{
			imgclass: "tree3 noanim",
			imgsrc: imgpath + "cutdowntree_four.png"
		},
		{
			imgclass: "tree4 noanim",
			imgsrc: imgpath + "cutdowntreeone.png"
		},
		{
			imgclass: "flower1 noanim",
			imgsrc: imgpath + "flowerthree.png"
		},
		{
			imgclass: "flower2 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "bush1 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "house1 noanim",
			imgsrc: imgpath + "hosue.png"
		},
		{
			imgclass: "house2 noanim",
			imgsrc: imgpath + "hosue01.png"
		},
		{
			imgclass: "house3 noanim",
			imgsrc: imgpath + "animals-shade.png"
		},
		{
			imgclass: "house4 noanim",
			imgsrc: imgpath + "animal-grass.png"
		},
		{
			imgclass: "animals noanim",
			imgsrc: imgpath + "three-animals.png"
		},
		{
			imgclass: "hen noanim",
			imgsrc: imgpath + "hen.png"
		},
		{
			imgclass: "duck noanim",
			imgsrc: imgpath + "duck.png"
		},
		{
			imgclass: "poll1",
			imgsrc: imgpath + "base01.png"
		},
		{
			imgclass: "poll2",
			imgsrc: imgpath + "cowdunk.png"
		},
		{
			imgclass: "poll3",
			imgsrc: imgpath + "base03.png"
		},
		{
			imgclass: "poll4",
			imgsrc: imgpath + "base04.png"
		},
		{
			imgclass: "poll5",
			imgsrc: imgpath + "base02.png"
		},
		{
			imgclass: "dustbincomp",
			tdimgclass: "tdimg",
			imgsrc: imgpath + "page3/twodusbin.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "poll6",
	},
	{
		textclass: "sweep",
	}
	],
	thirdpagediv:[
		{
			tddata: data.string.p3t3,
			tdimgclass: "tdimg clickable",
			tdsrc: imgpath + "page3/jhaadu.png"
		}
	]
},
{
	contentblockadditionalclass: "bluesky contentwithbg",
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "stone1 noanim",
			imgsrc: imgpath + "stone01.png"
		},
		{
			imgclass: "stone3 noanim",
			imgsrc: imgpath + "bush.png"
		},
		{
			imgclass: "tree1 noanim",
			imgsrc: imgpath + "cutdowntreeone.png"
		},
		{
			imgclass: "tree2 noanim",
			imgsrc: imgpath + "cutdowntree_two.png"
		},
		{
			imgclass: "tree3 noanim",
			imgsrc: imgpath + "cutdowntree_four.png"
		},
		{
			imgclass: "tree4 noanim",
			imgsrc: imgpath + "cutdowntreeone.png"
		},
		{
			imgclass: "flower1 noanim",
			imgsrc: imgpath + "flowerthree.png"
		},
		{
			imgclass: "flower2 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "bush1 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "house1 noanim",
			imgsrc: imgpath + "hosue.png"
		},
		{
			imgclass: "house2 noanim",
			imgsrc: imgpath + "hosue01.png"
		},
		{
			imgclass: "house3 noanim",
			imgsrc: imgpath + "animals-shade.png"
		},
		{
			imgclass: "house4 noanim",
			imgsrc: imgpath + "animal-grass.png"
		},
		{
			imgclass: "animals noanim",
			imgsrc: imgpath + "three-animals.png"
		},
		{
			imgclass: "poll4",
			imgsrc: imgpath + "base04.png"
		},
		{
			imgclass: "poll5",
			imgsrc: imgpath + "base02.png"
		},
		{
			imgclass: "hen noanim",
			imgsrc: imgpath + "hen.png"
		},
		{
			imgclass: "duck noanim",
			imgsrc: imgpath + "duck.png"
		},
		{
			imgclass: "dustbincomp",
			imgsrc: imgpath + "page3/twodusbin.png"
		}
		]
	}],
	thirdpagediv:[
		{
			tddata: data.string.p3t4,
			tdimgclass: "tdimg clickable",
			tdsrc: imgpath + "page3/toilet01.png"
		}
	]
},
{
	contentblockadditionalclass: "bluesky contentwithbg",
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "stone1 noanim",
			imgsrc: imgpath + "stone01.png"
		},
		{
			imgclass: "stone3 noanim",
			imgsrc: imgpath + "bush.png"
		},
		{
			imgclass: "tree1 noanim",
			imgsrc: imgpath + "cutdowntreeone.png"
		},
		{
			imgclass: "tree2 noanim",
			imgsrc: imgpath + "cutdowntree_two.png"
		},
		{
			imgclass: "tree3 noanim",
			imgsrc: imgpath + "cutdowntree_four.png"
		},
		{
			imgclass: "tree4 noanim",
			imgsrc: imgpath + "cutdowntreeone.png"
		},
		{
			imgclass: "flower1 noanim",
			imgsrc: imgpath + "flowerthree.png"
		},
		{
			imgclass: "flower2 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "bush1 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "house1 noanim",
			imgsrc: imgpath + "hosue.png"
		},
		{
			imgclass: "house2 noanim",
			imgsrc: imgpath + "hosue01.png"
		},
		{
			imgclass: "house3 noanim",
			imgsrc: imgpath + "animals-shade.png"
		},
		{
			imgclass: "house4 noanim",
			imgsrc: imgpath + "animal-grass.png"
		},
		{
			imgclass: "animals noanim",
			imgsrc: imgpath + "three-animals.png"
		},
		{
			imgclass: "poll4",
			imgsrc: imgpath + "base04.png"
		},
		{
			imgclass: "poll5",
			imgsrc: imgpath + "base02.png"
		},
		{
			imgclass: "hen noanim",
			imgsrc: imgpath + "hen.png"
		},
		{
			imgclass: "duck noanim",
			imgsrc: imgpath + "duck.png"
		},
		{
			imgclass: "dustbincomp",
			imgsrc: imgpath + "page3/twodusbin.png"
		},
		{
			imgclass: "toiletcomp",
			imgsrc: imgpath + "page3/toilet.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "netit",
	}
	],
	thirdpagediv:[
		{
			tddata: data.string.p3t5,
			tdimgclass: "tdimg clickable",
			tdsrc: imgpath + "page3/net.png"
		}
	]
},
{
	contentblockadditionalclass: "bluesky contentwithbg",
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "stone1 noanim",
			imgsrc: imgpath + "stone01.png"
		},
		{
			imgclass: "stone3 noanim",
			imgsrc: imgpath + "bush.png"
		},
		{
			imgclass: "tree1 noanim",
			imgsrc: imgpath + "cutdowntreeone.png"
		},
		{
			imgclass: "tree2 noanim",
			imgsrc: imgpath + "cutdowntree_two.png"
		},
		{
			imgclass: "tree3 noanim",
			imgsrc: imgpath + "cutdowntree_four.png"
		},
		{
			imgclass: "tree4 noanim",
			imgsrc: imgpath + "cutdowntreeone.png"
		},
		{
			imgclass: "flower1 noanim",
			imgsrc: imgpath + "flowerthree.png"
		},
		{
			imgclass: "flower2 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "bush1 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "house1 noanim",
			imgsrc: imgpath + "hosue.png"
		},
		{
			imgclass: "house2 noanim",
			imgsrc: imgpath + "hosue01.png"
		},
		{
			imgclass: "house3 noanim",
			imgsrc: imgpath + "animals-shade.png"
		},
		{
			imgclass: "house4 noanim",
			imgsrc: imgpath + "animal-grass.png"
		},
		{
			imgclass: "animals noanim",
			imgsrc: imgpath + "three-animals.png"
		},
		{
			imgclass: "hen noanim",
			imgsrc: imgpath + "hen.png"
		},
		{
			imgclass: "duck noanim",
			imgsrc: imgpath + "duck.png"
		},
		{
			imgclass: "dustbincomp",
			imgsrc: imgpath + "page3/twodusbin.png"
		},
		{
			imgclass: "toiletcomp",
			imgsrc: imgpath + "page3/toilet.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "plantit",
	}
	],
	thirdpagediv:[
		{
			tddata: data.string.p3t6,
			tdimgclass: "tdimg clickable",
			tdsrc: imgpath + "page3/plant01.png"
		}
	]
},
{
	contentblockadditionalclass: "bluesky contentwithbg",
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "stone1 noanim",
			imgsrc: imgpath + "stone01.png"
		},
		{
			imgclass: "stone3 noanim",
			imgsrc: imgpath + "bush.png"
		},
		{
			imgclass: "flower1 noanim",
			imgsrc: imgpath + "flowerthree.png"
		},
		{
			imgclass: "flower2 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "bush1 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "tree1",
			imgsrc: imgpath + "tree01.png"
		},
		{
			imgclass: "tree2",
			imgsrc: imgpath + "twotree.png"
		},
		{
			imgclass: "tree3",
			imgsrc: imgpath + "threefour.png"
		},
		{
			imgclass: "tree4",
			imgsrc: imgpath + "tree01.png"
		},
		{
			imgclass: "house1 noanim",
			imgsrc: imgpath + "hosue.png"
		},
		{
			imgclass: "house2 noanim",
			imgsrc: imgpath + "hosue01.png"
		},
		{
			imgclass: "house3 noanim",
			imgsrc: imgpath + "animals-shade.png"
		},
		{
			imgclass: "house4 noanim",
			imgsrc: imgpath + "animal-grass.png"
		},
		{
			imgclass: "animals noanim",
			imgsrc: imgpath + "three-animals.png"
		},
		{
			imgclass: "hen noanim",
			imgsrc: imgpath + "hen.png"
		},
		{
			imgclass: "duck noanim",
			imgsrc: imgpath + "duck.png"
		},
		{
			imgclass: "dustbincomp",
			imgsrc: imgpath + "page3/twodusbin.png"
		},
		{
			imgclass: "toiletcomp",
			imgsrc: imgpath + "page3/toilet.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "text1 fadeIn",
		textdata: data.string.p3t7
	}
	]
}
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			{id: "sound_0", src: soundAsset+"p3_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p3_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p3_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p3_s3.ogg"},
			{id: "sound_4", src: soundAsset+"p3_s4.ogg"},
			{id: "sound_5", src: soundAsset+"p3_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p3_s6.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


  function navigationcontroller(islastpageflag){
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		var div_ratio =$(".poll6").width()/$(".poll6").parent().width();
		$(".poll6").width(Math.round($(".poll6").parent().width()*div_ratio));

		var div_ratio =$(".sweep").width()/$(".sweep").parent().width();
		$(".sweep").width(Math.round($(".sweep").parent().width()*div_ratio));

		var div_ratio =$(".plantit").width()/$(".plantit").parent().width();
		$(".plantit").width(Math.round($(".plantit").parent().width()*div_ratio));

		switch(countNext){
			case 0:
			sound_player('sound_0',1);
			break;
			case 1:
				$nextBtn.hide(0);
				sound_player('sound_1',0);
				$(".clickable").click(function(){
					$(this).parent().hide(0);
					$(this).appendTo(".imageblock").addClass("dustbinanim").removeClass("tdimg clickable");
					setTimeout(function(){
						$nextBtn.show(0);
					},3000);
				});
			break;
			case 2:
				$nextBtn.hide(0);
				sound_player('sound_2',0);
				$(".sweep").hide(0);
				$(".clickable").click(function(){
					$(this).parent().hide(0);
					$(this).appendTo(".imageblock").addClass("jhadoanim").removeClass("tdimg clickable");
					$(".sweep").delay(2000).fadeIn(1000);
					$(".poll1, .poll2, .poll3, .poll6").delay(3000).addClass("pollfadeout");
					setTimeout(function(){
						$nextBtn.show(0);
					},5000);
				});
			break;
			case 3:
				$nextBtn.hide(0);
				sound_player('sound_3',0);
				$(".clickable").click(function(){
					$(this).parent().hide(0);
					$(this).appendTo(".imageblock").addClass("toiletanim").removeClass("tdimg clickable");
					setTimeout(function(){
						$nextBtn.show(0);
					},3000);
				});
			break;
			case 4:
				$nextBtn.hide(0);
				sound_player('sound_4',0);
				$(".netit").hide(0);
				$(".clickable").click(function(){
					$(this).parent().hide(0);
					$(this).appendTo(".imageblock").addClass("netanim").removeClass("tdimg clickable");
					$(".netit").delay(2000).fadeIn(1000);
					$(".poll4, .poll5").delay(3000).addClass("pollfadeout");
					setTimeout(function(){
						$nextBtn.show(0);
					},5000);
				});
			break;
			case 5:
				$nextBtn.hide(0);
				sound_player('sound_5',0);
				$(".plantit").hide(0);
				$(".clickable").click(function(){
					$(this).parent().hide(0);
					$(this).appendTo(".imageblock").addClass("plantanim").removeClass("tdimg clickable");
					$(".plantit").delay(2000).fadeIn(1000);
					setTimeout(function(){
						$nextBtn.show(0);
					},3000);
				});
			break;
		case 6:
			sound_player('sound_6',1);
			break;
		}
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next==1){
			navigationcontroller();
		}
		});
	}
	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

	//	navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});


 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

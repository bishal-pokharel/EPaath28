var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+ $lang+"/";

var content=[
{
	// contentblockcenteradjust: true,
	uppertextblock:[
	{
		textdata: data.lesson.chapter,
		textclass:'title'
	}
	],
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "bg_full",
			imgsrc: imgpath + "bg_main.png"

		}
		]
	}]
},
// {
// 	contentblockadditionalclass: "bluesky",
// 	imageblock:[{
// 		imagetoshow:[
// 		{
// 			imgclass: "sun",
// 			imgsrc: imgpath + "sun.png"
// 		},
// 		{
// 			imgclass: "thebackground",
// 			imgsrc: imgpath + "bg_main.png"
//
// 		}
// 		]
// 	}]
// },
{
	contentblockadditionalclass: "bluesky",
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "thebackground",
			imgsrc: imgpath + "bg_new.png"

		}
		]
	}],
	uppertextblock:[
	{
		textclass: "text1 fadein",
		textdata: data.string.p1t1
	}
	]
},
{
	contentblockadditionalclass: "bluesky contentwithbg",
	uppertextblock:[
	{
		textclass: "text1",
		textdata: data.string.p1t1
	},
	{
		textclass: "text2 moveup",
		textdata: data.string.p1t2
	}
	]
},
{
	contentblockadditionalclass: "bluesky contentwithbg",
	uppertextblock:[
	{
		textclass: "text1",
		textdata: data.string.p1t1
	},
	{
		textclass: "text2",
		textdata: data.string.p1t2
	},
	{
		textclass: "text3 floatit",
		textdata: data.string.p1t3
	}
	]
},
{
	contentblockadditionalclass: "bluesky contentwithbg",
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "stone1",
			imgsrc: imgpath + "stone01.png"
		},
		{
			imgclass: "stone3",
			imgsrc: imgpath + "bush.png"
		},
		{
			imgclass: "tree1",
			imgsrc: imgpath + "tree01.png"
		},
		{
			imgclass: "tree2",
			imgsrc: imgpath + "twotree.png"
		},
		{
			imgclass: "tree3",
			imgsrc: imgpath + "threefour.png"
		},
		{
			imgclass: "tree4",
			imgsrc: imgpath + "tree01.png"
		},
		{
			imgclass: "flower1",
			imgsrc: imgpath + "flowerthree.png"
		},
		{
			imgclass: "flower2",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "bush1",
			imgsrc: imgpath + "bush02.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "text4 fromleft",
		textdata: data.string.p1t4
	}
	]
},
{
	contentblockadditionalclass: "bluesky contentwithbg",
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "stone1 noanim",
			imgsrc: imgpath + "stone01.png"
		},
		{
			imgclass: "stone3 noanim",
			imgsrc: imgpath + "bush.png"
		},
		{
			imgclass: "tree1 noanim",
			imgsrc: imgpath + "tree01.png"
		},
		{
			imgclass: "tree2 noanim",
			imgsrc: imgpath + "twotree.png"
		},
		{
			imgclass: "tree3 noanim",
			imgsrc: imgpath + "threefour.png"
		},
		{
			imgclass: "tree4 noanim",
			imgsrc: imgpath + "tree01.png"
		},
		{
			imgclass: "flower1 noanim",
			imgsrc: imgpath + "flowerthree.png"
		},
		{
			imgclass: "flower2 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "bush1 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "house1",
			imgsrc: imgpath + "hosue.png"
		},
		{
			imgclass: "house2",
			imgsrc: imgpath + "hosue01.png"
		},
		{
			imgclass: "house3",
			imgsrc: imgpath + "animals-shade.png"
		},
		{
			imgclass: "house4",
			imgsrc: imgpath + "animal-grass.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "text5",
		textdata: data.string.p1t5
	}
	]
},
{
	contentblockadditionalclass: "bluesky contentwithbg",
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "stone1 noanim",
			imgsrc: imgpath + "stone01.png"
		},
		{
			imgclass: "stone3 noanim",
			imgsrc: imgpath + "bush.png"
		},
		{
			imgclass: "tree1 noanim",
			imgsrc: imgpath + "tree01.png"
		},
		{
			imgclass: "tree2 noanim",
			imgsrc: imgpath + "twotree.png"
		},
		{
			imgclass: "tree3 noanim",
			imgsrc: imgpath + "threefour.png"
		},
		{
			imgclass: "tree4 noanim",
			imgsrc: imgpath + "tree01.png"
		},
		{
			imgclass: "flower1 noanim",
			imgsrc: imgpath + "flowerthree.png"
		},
		{
			imgclass: "flower2 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "bush1 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "house1 noanim",
			imgsrc: imgpath + "hosue.png"
		},
		{
			imgclass: "house2 noanim",
			imgsrc: imgpath + "hosue01.png"
		},
		{
			imgclass: "house3 noanim",
			imgsrc: imgpath + "animals-shade.png"
		},
		{
			imgclass: "house4 noanim",
			imgsrc: imgpath + "animal-grass.png"
		},
		{
			imgclass: "animals",
			imgsrc: imgpath + "three-animals.png"
		},
		{
			imgclass: "hen",
			imgsrc: imgpath + "hen.png"
		},
		{
			imgclass: "duck",
			imgsrc: imgpath + "duck.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "text6",
		textdata: data.string.p1t6
	}
	]
},
{
	contentblockadditionalclass: "bluesky contentwithbg",
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "stone1 noanim",
			imgsrc: imgpath + "stone01.png"
		},
		{
			imgclass: "stone3 noanim",
			imgsrc: imgpath + "bush.png"
		},
		{
			imgclass: "tree1 noanim",
			imgsrc: imgpath + "tree01.png"
		},
		{
			imgclass: "tree2 noanim",
			imgsrc: imgpath + "twotree.png"
		},
		{
			imgclass: "tree3 noanim",
			imgsrc: imgpath + "threefour.png"
		},
		{
			imgclass: "tree4 noanim",
			imgsrc: imgpath + "tree01.png"
		},
		{
			imgclass: "flower1 noanim",
			imgsrc: imgpath + "flowerthree.png"
		},
		{
			imgclass: "flower2 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "bush1 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "house1 noanim",
			imgsrc: imgpath + "hosue.png"
		},
		{
			imgclass: "house2 noanim",
			imgsrc: imgpath + "hosue01.png"
		},
		{
			imgclass: "house3 noanim",
			imgsrc: imgpath + "animals-shade.png"
		},
		{
			imgclass: "house4 noanim",
			imgsrc: imgpath + "animal-grass.png"
		},
		{
			imgclass: "animals noanim",
			imgsrc: imgpath + "three-animals.png"
		},
		{
			imgclass: "hen noanim",
			imgsrc: imgpath + "hen.png"
		},
		{
			imgclass: "duck noanim",
			imgsrc: imgpath + "duck.png"
		},
		{
			imgclass: "human",
			imgsrc: imgpath + "girlwithdog.png"
		},
		{
			imgclass: "human2",
			imgsrc: imgpath + "girl.png"
		},
		{
			imgclass: "human3",
			imgsrc: imgpath + "boy.png"
		},
		{
			imgclass: "human4",
			imgsrc: imgpath + "man.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "text7",
		textdata: data.string.p1t7
	}
	]
},
{
	contentblockadditionalclass: "bluesky contentwithbg",
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "stone1 noanim",
			imgsrc: imgpath + "stone01.png"
		},
		{
			imgclass: "stone3 noanim",
			imgsrc: imgpath + "bush.png"
		},
		{
			imgclass: "tree1 noanim",
			imgsrc: imgpath + "tree01.png"
		},
		{
			imgclass: "tree2 noanim",
			imgsrc: imgpath + "twotree.png"
		},
		{
			imgclass: "tree3 noanim",
			imgsrc: imgpath + "threefour.png"
		},
		{
			imgclass: "tree4 noanim",
			imgsrc: imgpath + "tree01.png"
		},
		{
			imgclass: "flower1 noanim",
			imgsrc: imgpath + "flowerthree.png"
		},
		{
			imgclass: "flower2 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "bush1 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "house1 noanim",
			imgsrc: imgpath + "hosue.png"
		},
		{
			imgclass: "house2 noanim",
			imgsrc: imgpath + "hosue01.png"
		},
		{
			imgclass: "house3 noanim",
			imgsrc: imgpath + "animals-shade.png"
		},
		{
			imgclass: "house4 noanim",
			imgsrc: imgpath + "animal-grass.png"
		},
		{
			imgclass: "animals noanim",
			imgsrc: imgpath + "three-animals.png"
		},
		{
			imgclass: "hen noanim",
			imgsrc: imgpath + "hen.png"
		},
		{
			imgclass: "duck noanim",
			imgsrc: imgpath + "duck.png"
		},
		{
			imgclass: "human noanim",
			imgsrc: imgpath + "girlwithdog.png"
		},
		{
			imgclass: "human noanim",
			imgsrc: imgpath + "girlwithdog.png"
		},
		{
			imgclass: "human2 noanim",
			imgsrc: imgpath + "girl.png"
		},
		{
			imgclass: "human3 noanim",
			imgsrc: imgpath + "boy.png"
		},
		{
			imgclass: "human4 noanim",
			imgsrc: imgpath + "man.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "text1 fadeIn",
		textdata: data.string.p1t8
	}
	]
},
{
	contentblockadditionalclass: "bluesky contentwithbg",
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "stone1 noanim",
			imgsrc: imgpath + "stone01.png"
		},
		{
			imgclass: "stone3 noanim",
			imgsrc: imgpath + "bush.png"
		},
		{
			imgclass: "tree1 noanim",
			imgsrc: imgpath + "tree01.png"
		},
		{
			imgclass: "tree2 noanim",
			imgsrc: imgpath + "twotree.png"
		},
		{
			imgclass: "tree3 noanim",
			imgsrc: imgpath + "threefour.png"
		},
		{
			imgclass: "tree4 noanim",
			imgsrc: imgpath + "tree01.png"
		},
		{
			imgclass: "flower1 noanim",
			imgsrc: imgpath + "flowerthree.png"
		},
		{
			imgclass: "flower2 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "bush1 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "house1 noanim",
			imgsrc: imgpath + "hosue.png"
		},
		{
			imgclass: "house2 noanim",
			imgsrc: imgpath + "hosue01.png"
		},
		{
			imgclass: "house3 noanim",
			imgsrc: imgpath + "animals-shade.png"
		},
		{
			imgclass: "house4 noanim",
			imgsrc: imgpath + "animal-grass.png"
		},
		{
			imgclass: "animals noanim",
			imgsrc: imgpath + "three-animals.png"
		},
		{
			imgclass: "hen noanim",
			imgsrc: imgpath + "hen.png"
		},
		{
			imgclass: "duck noanim",
			imgsrc: imgpath + "duck.png"
		},
		{
			imgclass: "human noanim",
			imgsrc: imgpath + "girlwithdog.png"
		},
		{
			imgclass: "human noanim",
			imgsrc: imgpath + "girlwithdog.png"
		},
		{
			imgclass: "human2 noanim",
			imgsrc: imgpath + "girl.png"
		},
		{
			imgclass: "human3 noanim",
			imgsrc: imgpath + "boy.png"
		},
		{
			imgclass: "human4 noanim",
			imgsrc: imgpath + "man.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "text1 fadeIn",
		textdata: data.string.p1t9
	}
	]
},
{
	contentblockadditionalclass: "bluesky contentwithbg bgmove",
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "stone1 noanim",
			imgsrc: imgpath + "stone01.png"
		},
		{
			imgclass: "stone3 noanim",
			imgsrc: imgpath + "bush.png"
		},
		{
			imgclass: "tree1 noanim",
			imgsrc: imgpath + "tree01.png"
		},
		{
			imgclass: "tree2 noanim",
			imgsrc: imgpath + "twotree.png"
		},
		{
			imgclass: "tree3 noanim",
			imgsrc: imgpath + "twotree.png"
		},
		{
			imgclass: "tree4 noanim",
			imgsrc: imgpath + "tree01.png"
		},
		{
			imgclass: "flower1 noanim",
			imgsrc: imgpath + "flowerthree.png"
		},
		{
			imgclass: "flower2 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "bush1 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "house1 noanim",
			imgsrc: imgpath + "hosue.png"
		},
		{
			imgclass: "house2 noanim",
			imgsrc: imgpath + "hosue01.png"
		},
		{
			imgclass: "house3 noanim",
			imgsrc: imgpath + "animals-shade.png"
		},
		{
			imgclass: "house4 noanim",
			imgsrc: imgpath + "animal-grass.png"
		},
		{
			imgclass: "animals noanim",
			imgsrc: imgpath + "three-animals.png"
		},
		{
			imgclass: "hen noanim",
			imgsrc: imgpath + "hen.png"
		},
		{
			imgclass: "duck noanim",
			imgsrc: imgpath + "duck.png"
		},
		{
			imgclass: "human noanim",
			imgsrc: imgpath + "girlwithdog.png"
		},
		{
			imgclass: "human noanim",
			imgsrc: imgpath + "girlwithdog.png"
		},
		{
			imgclass: "human2 noanim",
			imgsrc: imgpath + "girl.png"
		},
		{
			imgclass: "human3 noanim",
			imgsrc: imgpath + "boy.png"
		},
		{
			imgclass: "human4 noanim",
			imgsrc: imgpath + "man.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "text8 zoomfont",
		textdata: data.string.p1t10
	}
	]
},
{
	contentblockadditionalclass: "bluesky contentwithbg bgmove-already",
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "stone1 noanim",
			imgsrc: imgpath + "stone01.png"
		},
		{
			imgclass: "stone3 noanim",
			imgsrc: imgpath + "bush.png"
		},
		{
			imgclass: "tree1 noanim",
			imgsrc: imgpath + "tree01.png"
		},
		{
			imgclass: "tree2 noanim",
			imgsrc: imgpath + "twotree.png"
		},
		{
			imgclass: "tree3 noanim",
			imgsrc: imgpath + "threefour.png"
		},
		{
			imgclass: "tree4 noanim",
			imgsrc: imgpath + "tree01.png"
		},
		{
			imgclass: "flower1 noanim",
			imgsrc: imgpath + "flowerthree.png"
		},
		{
			imgclass: "flower2 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "bush1 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "house1 noanim",
			imgsrc: imgpath + "hosue.png"
		},
		{
			imgclass: "house2 noanim",
			imgsrc: imgpath + "hosue01.png"
		},
		{
			imgclass: "house3 noanim",
			imgsrc: imgpath + "animals-shade.png"
		},
		{
			imgclass: "house4 noanim",
			imgsrc: imgpath + "animal-grass.png"
		},
		{
			imgclass: "animals noanim",
			imgsrc: imgpath + "three-animals.png"
		},
		{
			imgclass: "hen noanim",
			imgsrc: imgpath + "hen.png"
		},
		{
			imgclass: "duck noanim",
			imgsrc: imgpath + "duck.png"
		},
		{
			imgclass: "human noanim",
			imgsrc: imgpath + "girlwithdog.png"
		},
		{
			imgclass: "human noanim",
			imgsrc: imgpath + "girlwithdog.png"
		},
		{
			imgclass: "human2 noanim",
			imgsrc: imgpath + "girl.png"
		},
		{
			imgclass: "human3 noanim",
			imgsrc: imgpath + "boy.png"
		},
		{
			imgclass: "human4 noanim",
			imgsrc: imgpath + "man.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "text9 zoomfont",
		textdata: data.string.p1t11
	}
	]
},
{
	contentblockadditionalclass: "bluesky contentwithbg bgmove-already",
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "stone1 noanim",
			imgsrc: imgpath + "stone01.png"
		},
		{
			imgclass: "stone3 noanim",
			imgsrc: imgpath + "bush.png"
		},
		{
			imgclass: "tree1 noanim",
			imgsrc: imgpath + "tree01.png"
		},
		{
			imgclass: "tree2 noanim",
			imgsrc: imgpath + "twotree.png"
		},
		{
			imgclass: "tree3 noanim",
			imgsrc: imgpath + "threefour.png"
		},
		{
			imgclass: "tree4 noanim",
			imgsrc: imgpath + "tree01.png"
		},
		{
			imgclass: "flower1 noanim",
			imgsrc: imgpath + "flowerthree.png"
		},
		{
			imgclass: "flower2 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "bush1 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "house1 noanim",
			imgsrc: imgpath + "hosue.png"
		},
		{
			imgclass: "house2 noanim",
			imgsrc: imgpath + "hosue01.png"
		},
		{
			imgclass: "house3 noanim",
			imgsrc: imgpath + "animals-shade.png"
		},
		{
			imgclass: "house4 noanim",
			imgsrc: imgpath + "animal-grass.png"
		},
		{
			imgclass: "animals noanim",
			imgsrc: imgpath + "three-animals.png"
		},
		{
			imgclass: "hen noanim",
			imgsrc: imgpath + "hen.png"
		},
		{
			imgclass: "duck noanim",
			imgsrc: imgpath + "duck.png"
		},
		{
			imgclass: "human noanim",
			imgsrc: imgpath + "girlwithdog.png"
		},
		{
			imgclass: "human noanim",
			imgsrc: imgpath + "girlwithdog.png"
		},
		{
			imgclass: "human2 noanim",
			imgsrc: imgpath + "girl.png"
		},
		{
			imgclass: "human3 noanim",
			imgsrc: imgpath + "boy.png"
		},
		{
			imgclass: "human4 noanim",
			imgsrc: imgpath + "man.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "text9 zoomfont",
		textdata: data.string.p1t12
	}
	]
},
{
	contentblockadditionalclass: "bluesky contentwithbg bgmove-already",
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "stone1 noanim",
			imgsrc: imgpath + "stone01.png"
		},
		{
			imgclass: "stone3 noanim",
			imgsrc: imgpath + "bush.png"
		},
		{
			imgclass: "tree1 noanim",
			imgsrc: imgpath + "tree01.png"
		},
		{
			imgclass: "tree2 noanim",
			imgsrc: imgpath + "twotree.png"
		},
		{
			imgclass: "tree3 noanim",
			imgsrc: imgpath + "threefour.png"
		},
		{
			imgclass: "tree4 noanim",
			imgsrc: imgpath + "tree01.png"
		},
		{
			imgclass: "flower1 noanim",
			imgsrc: imgpath + "flowerthree.png"
		},
		{
			imgclass: "flower2 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "bush1 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "house1 noanim",
			imgsrc: imgpath + "hosue.png"
		},
		{
			imgclass: "house2 noanim",
			imgsrc: imgpath + "hosue01.png"
		},
		{
			imgclass: "house3 noanim",
			imgsrc: imgpath + "animals-shade.png"
		},
		{
			imgclass: "house4 noanim",
			imgsrc: imgpath + "animal-grass.png"
		},
		{
			imgclass: "animals noanim",
			imgsrc: imgpath + "three-animals.png"
		},
		{
			imgclass: "hen noanim",
			imgsrc: imgpath + "hen.png"
		},
		{
			imgclass: "duck noanim",
			imgsrc: imgpath + "duck.png"
		},
		{
			imgclass: "human noanim",
			imgsrc: imgpath + "girlwithdog.png"
		},
		{
			imgclass: "human noanim",
			imgsrc: imgpath + "girlwithdog.png"
		},
		{
			imgclass: "human2 noanim",
			imgsrc: imgpath + "girl.png"
		},
		{
			imgclass: "human3 noanim",
			imgsrc: imgpath + "boy.png"
		},
		{
			imgclass: "human4 noanim",
			imgsrc: imgpath + "man.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "text9 zoomfont",
		textdata: data.string.p1t13
	}
	]
},
{
	contentblockadditionalclass: "bluesky contentwithbg bgmove-already",
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "stone1 noanim",
			imgsrc: imgpath + "stone01.png"
		},
		{
			imgclass: "stone3 noanim",
			imgsrc: imgpath + "bush.png"
		},
		{
			imgclass: "tree1 fadeIn",
			imgsrc: imgpath + "cutdowntreeone.png"
		},
		{
			imgclass: "tree2 fadeIn",
			imgsrc: imgpath + "cutdowntree_two.png"
		},
		{
			imgclass: "tree3 fadeIn",
			imgsrc: imgpath + "cutdowntree_four.png"
		},
		{
			imgclass: "tree4 fadeIn",
			imgsrc: imgpath + "cutdowntreeone.png"
		},
		{
			imgclass: "flower1 noanim",
			imgsrc: imgpath + "flowerthree.png"
		},
		{
			imgclass: "flower2 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "bush1 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "house1 noanim",
			imgsrc: imgpath + "hosue.png"
		},
		{
			imgclass: "house2 noanim",
			imgsrc: imgpath + "hosue01.png"
		},
		{
			imgclass: "house3 noanim",
			imgsrc: imgpath + "animals-shade.png"
		},
		{
			imgclass: "house4 noanim",
			imgsrc: imgpath + "animal-grass.png"
		},
		{
			imgclass: "animals noanim",
			imgsrc: imgpath + "three-animals.png"
		},
		{
			imgclass: "hen noanim",
			imgsrc: imgpath + "hen.png"
		},
		{
			imgclass: "duck noanim",
			imgsrc: imgpath + "duck.png"
		},
		{
			imgclass: "human noanim",
			imgsrc: imgpath + "girlwithdog.png"
		},
		{
			imgclass: "poll1 fadeIn",
			imgsrc: imgpath + "base01.png"
		},
		{
			imgclass: "poll2 fadeIn",
			imgsrc: imgpath + "cowdunk.png"
		},
		{
			imgclass: "poll3 fadeIn",
			imgsrc: imgpath + "base03.png"
		},
		{
			imgclass: "poll4 fadeIn",
			imgsrc: imgpath + "base04.png"
		},
		{
			imgclass: "poll5 fadeIn",
			imgsrc: imgpath + "base02.png"
		},
		{
			imgclass: "human noanim",
			imgsrc: imgpath + "girlwithdog.png"
		},
		{
			imgclass: "human2 noanim",
			imgsrc: imgpath + "girl.png"
		},
		{
			imgclass: "human3 noanim",
			imgsrc: imgpath + "boy.png"
		},
		{
			imgclass: "human4 noanim",
			imgsrc: imgpath + "man.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "text9 zoomfont",
		textdata: data.string.p1t14
	},
	{
		textclass: "poll6",
	}
	]
},
{
	contentblockadditionalclass: "bluesky contentwithbg bgmove-already",
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "stone1 noanim",
			imgsrc: imgpath + "stone01.png"
		},
		{
			imgclass: "stone3 noanim",
			imgsrc: imgpath + "bush.png"
		},
		{
			imgclass: "tree1 noanim",
			imgsrc: imgpath + "cutdowntreeone.png"
		},
		{
			imgclass: "tree2 noanim",
			imgsrc: imgpath + "cutdowntree_two.png"
		},
		{
			imgclass: "tree3 noanim",
			imgsrc: imgpath + "cutdowntree_four.png"
		},
		{
			imgclass: "tree4 noanim",
			imgsrc: imgpath + "cutdowntreeone.png"
		},
		{
			imgclass: "flower1 noanim",
			imgsrc: imgpath + "flowerthree.png"
		},
		{
			imgclass: "flower2 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "bush1 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "house1 noanim",
			imgsrc: imgpath + "hosue.png"
		},
		{
			imgclass: "house2 noanim",
			imgsrc: imgpath + "hosue01.png"
		},
		{
			imgclass: "house3 noanim",
			imgsrc: imgpath + "animals-shade.png"
		},
		{
			imgclass: "house4 noanim",
			imgsrc: imgpath + "animal-grass.png"
		},
		{
			imgclass: "animals noanim",
			imgsrc: imgpath + "three-animals.png"
		},
		{
			imgclass: "hen noanim",
			imgsrc: imgpath + "hen.png"
		},
		{
			imgclass: "duck noanim",
			imgsrc: imgpath + "duck.png"
		},
		{
			imgclass: "human noanim",
			imgsrc: imgpath + "girlwithdog.png"
		},
		{
			imgclass: "poll1",
			imgsrc: imgpath + "base01.png"
		},
		{
			imgclass: "poll2",
			imgsrc: imgpath + "cowdunk.png"
		},
		{
			imgclass: "poll3",
			imgsrc: imgpath + "base03.png"
		},
		{
			imgclass: "poll4",
			imgsrc: imgpath + "base04.png"
		},
		{
			imgclass: "poll5",
			imgsrc: imgpath + "base02.png"
		},
		{
			imgclass: "human noanim",
			imgsrc: imgpath + "girlwithdog.png"
		},
		{
			imgclass: "human2 noanim",
			imgsrc: imgpath + "girl.png"
		},
		{
			imgclass: "human3 noanim",
			imgsrc: imgpath + "boy.png"
		},
		{
			imgclass: "human4 noanim",
			imgsrc: imgpath + "man.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "text9 zoomfont",
		textdata: data.string.p1t15
	},
	{
		textclass: "poll6",
	}
	]
},
{
	contentblockadditionalclass: "bluesky contentwithbg bgmove2",
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "stone1 noanim",
			imgsrc: imgpath + "stone01.png"
		},
		{
			imgclass: "stone3 noanim",
			imgsrc: imgpath + "bush.png"
		},
		{
			imgclass: "tree1 noanim",
			imgsrc: imgpath + "cutdowntreeone.png"
		},
		{
			imgclass: "tree2 noanim",
			imgsrc: imgpath + "cutdowntree_two.png"
		},
		{
			imgclass: "tree3 noanim",
			imgsrc: imgpath + "cutdowntree_four.png"
		},
		{
			imgclass: "tree4 noanim",
			imgsrc: imgpath + "cutdowntreeone.png"
		},
		{
			imgclass: "flower1 noanim",
			imgsrc: imgpath + "flowerthree.png"
		},
		{
			imgclass: "flower2 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "bush1 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "house1 noanim",
			imgsrc: imgpath + "hosue.png"
		},
		{
			imgclass: "house2 noanim",
			imgsrc: imgpath + "hosue01.png"
		},
		{
			imgclass: "house3 noanim",
			imgsrc: imgpath + "animals-shade.png"
		},
		{
			imgclass: "house4 noanim",
			imgsrc: imgpath + "animal-grass.png"
		},
		{
			imgclass: "animals noanim",
			imgsrc: imgpath + "three-animals.png"
		},
		{
			imgclass: "hen noanim",
			imgsrc: imgpath + "hen.png"
		},
		{
			imgclass: "duck noanim",
			imgsrc: imgpath + "duck.png"
		},
		{
			imgclass: "human noanim",
			imgsrc: imgpath + "girlwithdog.png"
		},
		{
			imgclass: "poll1",
			imgsrc: imgpath + "base01.png"
		},
		{
			imgclass: "poll2",
			imgsrc: imgpath + "cowdunk.png"
		},
		{
			imgclass: "poll3",
			imgsrc: imgpath + "base03.png"
		},
		{
			imgclass: "poll4",
			imgsrc: imgpath + "base04.png"
		},
		{
			imgclass: "poll5",
			imgsrc: imgpath + "base02.png"
		},
		{
			imgclass: "human noanim",
			imgsrc: imgpath + "girlwithdog.png"
		},
		{
			imgclass: "human2 noanim",
			imgsrc: imgpath + "girl.png"
		},
		{
			imgclass: "human3 noanim",
			imgsrc: imgpath + "boy.png"
		},
		{
			imgclass: "human4 noanim",
			imgsrc: imgpath + "man.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "text10 zoomfont",
		textdata: data.string.p1t16
	},
	{
		textclass: "poll6",
	}
	]
},
{
	contentblockadditionalclass: "bluesky contentwithbg bgmove3",
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "stone1 noanim",
			imgsrc: imgpath + "stone01.png"
		},
		{
			imgclass: "stone3 noanim",
			imgsrc: imgpath + "bush.png"
		},
		{
			imgclass: "tree1 noanim",
			imgsrc: imgpath + "cutdowntreeone.png"
		},
		{
			imgclass: "tree2 noanim",
			imgsrc: imgpath + "cutdowntree_two.png"
		},
		{
			imgclass: "tree3 noanim",
			imgsrc: imgpath + "cutdowntree_four.png"
		},
		{
			imgclass: "tree4 noanim",
			imgsrc: imgpath + "cutdowntreeone.png"
		},
		{
			imgclass: "flower1 noanim",
			imgsrc: imgpath + "flowerthree.png"
		},
		{
			imgclass: "flower2 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "bush1 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "house1 noanim",
			imgsrc: imgpath + "hosue.png"
		},
		{
			imgclass: "house2 noanim",
			imgsrc: imgpath + "hosue01.png"
		},
		{
			imgclass: "house3 noanim",
			imgsrc: imgpath + "animals-shade.png"
		},
		{
			imgclass: "house4 noanim",
			imgsrc: imgpath + "animal-grass.png"
		},
		{
			imgclass: "animals noanim",
			imgsrc: imgpath + "three-animals.png"
		},
		{
			imgclass: "hen noanim",
			imgsrc: imgpath + "hen.png"
		},
		{
			imgclass: "duck noanim",
			imgsrc: imgpath + "duck.png"
		},
		{
			imgclass: "human noanim",
			imgsrc: imgpath + "girlwithdog.png"
		},
		{
			imgclass: "poll1",
			imgsrc: imgpath + "base01.png"
		},
		{
			imgclass: "poll2",
			imgsrc: imgpath + "cowdunk.png"
		},
		{
			imgclass: "poll3",
			imgsrc: imgpath + "base03.png"
		},
		{
			imgclass: "poll4",
			imgsrc: imgpath + "base04.png"
		},
		{
			imgclass: "poll5",
			imgsrc: imgpath + "base02.png"
		},
		{
			imgclass: "human noanim",
			imgsrc: imgpath + "girlwithdog.png"
		},
		{
			imgclass: "human2 noanim",
			imgsrc: imgpath + "girl.png"
		},
		{
			imgclass: "human3 noanim",
			imgsrc: imgpath + "boy.png"
		},
		{
			imgclass: "human4 noanim",
			imgsrc: imgpath + "man.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "text11 zoomfont",
		textdata: data.string.p1t17
	},
	{
		textclass: "poll6",
	}
	]
},
{
	contentblockadditionalclass: "bluesky contentwithbg bgmove4",
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "stone1 noanim",
			imgsrc: imgpath + "stone01.png"
		},
		{
			imgclass: "stone3 noanim",
			imgsrc: imgpath + "bush.png"
		},
		{
			imgclass: "tree1 noanim",
			imgsrc: imgpath + "cutdowntreeone.png"
		},
		{
			imgclass: "tree2 noanim",
			imgsrc: imgpath + "cutdowntree_two.png"
		},
		{
			imgclass: "tree3 noanim",
			imgsrc: imgpath + "cutdowntree_four.png"
		},
		{
			imgclass: "tree4 noanim",
			imgsrc: imgpath + "cutdowntreeone.png"
		},
		{
			imgclass: "flower1 noanim",
			imgsrc: imgpath + "flowerthree.png"
		},
		{
			imgclass: "flower2 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "bush1 noanim",
			imgsrc: imgpath + "bush02.png"
		},
		{
			imgclass: "house1 noanim",
			imgsrc: imgpath + "hosue.png"
		},
		{
			imgclass: "house2 noanim",
			imgsrc: imgpath + "hosue01.png"
		},
		{
			imgclass: "house3 noanim",
			imgsrc: imgpath + "animals-shade.png"
		},
		{
			imgclass: "house4 noanim",
			imgsrc: imgpath + "animal-grass.png"
		},
		{
			imgclass: "animals noanim",
			imgsrc: imgpath + "three-animals.png"
		},
		{
			imgclass: "hen noanim",
			imgsrc: imgpath + "hen.png"
		},
		{
			imgclass: "duck noanim",
			imgsrc: imgpath + "duck.png"
		},
		{
			imgclass: "human noanim",
			imgsrc: imgpath + "girlwithdog.png"
		},
		{
			imgclass: "poll1",
			imgsrc: imgpath + "base01.png"
		},
		{
			imgclass: "poll2",
			imgsrc: imgpath + "cowdunk.png"
		},
		{
			imgclass: "poll3",
			imgsrc: imgpath + "base03.png"
		},
		{
			imgclass: "poll4",
			imgsrc: imgpath + "base04.png"
		},
		{
			imgclass: "poll5",
			imgsrc: imgpath + "base02.png"
		},
		{
			imgclass: "human noanim",
			imgsrc: imgpath + "girlwithdog.png"
		},
		{
			imgclass: "human2 noanim",
			imgsrc: imgpath + "girl.png"
		},
		{
			imgclass: "human3 noanim",
			imgsrc: imgpath + "boy.png"
		},
		{
			imgclass: "human4 noanim",
			imgsrc: imgpath + "man.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "text1",
		textdata: data.string.p1t18
	},
	{
		textclass: "poll6",
	}
	]
}
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			{id: "sound_0", src: soundAsset+"p1_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p1_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p1_s3.ogg"},
			{id: "sound_4", src: soundAsset+"p1_s4.ogg"},
			{id: "sound_5", src: soundAsset+"p1_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p1_s6.ogg"},
			{id: "sound_7", src: soundAsset+"p1_s7.ogg"},
			{id: "sound_8", src: soundAsset+"p1_s8.ogg"},
			{id: "sound_9", src: soundAsset+"p1_s9.ogg"},
			{id: "sound_10", src: soundAsset+"p1_s10.ogg"},
			{id: "sound_11", src: soundAsset+"p1_s11.ogg"},
			{id: "sound_12", src: soundAsset+"p1_s12.ogg"},
			{id: "sound_13", src: soundAsset+"p1_s13.ogg"},
			{id: "sound_14", src: soundAsset+"p1_s14.ogg"},
			{id: "sound_15", src: soundAsset+"p1_s15.ogg"},
			{id: "sound_16", src: soundAsset+"p1_s16.ogg"},
			{id: "sound_17", src: soundAsset+"p1_s17.ogg"},
			{id: "sound_18", src: soundAsset+"p1_s18.ogg"},
			{id: "sound_19", src: soundAsset+"p1_s19.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


      function navigationcontroller(islastpageflag){
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		var div_ratio =$(".poll6").width()/$(".poll6").parent().width();
		$(".poll6").width(Math.round($(".poll6").parent().width()*div_ratio));

		switch(countNext){
			case 0:
				sound_player('sound_0',1);
			break;
			case 1:
			sound_player('sound_2',1);
			break;
			case 2:
			sound_player('sound_3',1);

			break;
			case 3:
			sound_player('sound_4',1);

			break;
			case 4:
			setTimeout(function(){
				sound_player('sound_5',1);
			}, 3700);
			break;
			case 5:
			setTimeout(function(){
				sound_player('sound_6',1);
			}, 1700);
			break;
			case 6:
			setTimeout(function(){
				sound_player('sound_7',1);
			}, 1700);
			break;
			case 7:
			setTimeout(function(){
				sound_player('sound_8',1);
			}, 1700);
			break;
			case 8:
			sound_player('sound_9',1);
			break;
			case 9:
			sound_player('sound_10',1);
			break;
			case 10:
			setTimeout(function(){
				sound_player('sound_11',1);
			}, 2000);
			break;
			case 11:
			sound_player('sound_12',1);

			break;
			case 12:
			sound_player('sound_13',1);

			break;
			case 13:
			sound_player('sound_14',1);

			break;
			case 14:
			sound_player('sound_15',1);

			break;
			case 15:
			sound_player('sound_16',1);

			break;
			case 16:
			setTimeout(function(){
				sound_player('sound_17',1);
			}, 2300);
			break;
			case 17:
			setTimeout(function(){
				sound_player('sound_18',1);
			}, 2300);
			break;
			case 18:
			setTimeout(function(){
				sound_player('sound_19',1);
			}, 1800);
			break;
		}
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next==1){
			navigationcontroller();
		}
		});
	}
	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		//navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});


 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/"+ $lang+"/";

var content=[
  {
  	//page 0
  	contentblockadditionalclass: "ole-background-blue-sky",
	uppertextblock: [{
  		textclass: "maintitle",
  		textdata: data.string.p2t1
  	}],
 },{
 	//page 1
  	contentblockadditionalclass: "ole-background-gradient-opal",
	uppertextblock: [{
  		textclass: "maintitle2",
  		textdata: data.string.p2t2
  	},{
  		textclass: "lefttext",
  		textdata: data.string.p2t3
  	},{
  		textclass: "righttext ",
  		textdata: data.string.p2t4
  	}],
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "image_1 ",
  			imgsrc: imgpath+"page3/decaying.png"
  		},{
  			imgclass: "image_2 ",
  			imgsrc: imgpath+"page3/non_decaying.png"
  		}]
  	}]
 },{
 	//page 2
  	contentblockadditionalclass: "ole-backrgound-gradient-pool",
	uppertextblock: [{
  		textclass: "maintitle2",
  		textdata: data.string.p2t5
  	}],
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "image_3",
  			imgsrc: imgpath+"page3/toilet.png"
  		},{
  			imgclass: "bg_full",
  			imgsrc: imgpath+"bg_for_toilet.png"
  		}]
  	}]
 },{
 	//page 3
  	contentblockadditionalclass: "ole-background-gradient-apple",
	uppertextblock: [{
  		textclass: "maintitle2",
  		textdata: data.string.p2t6
  	}],
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "image_3",
  			imgsrc: imgpath+"page3/man-and-girl.png"
  		}]
  	}]
 }
];


$(function() {
	var $board = $('.board');
	var $board_prev = $('.board_prev');
	var $board_next = $('.board_next');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	loadTimelineProgress($total_page, countNext + 1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			{id: "sound_0", src: soundAsset+"p2_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p2_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p2_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p2_s3.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
	/*==================================================
	 =            Handlers and helpers Block            =
	 ==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	function navigationcontroller(islastpageflag){
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	/*=====  End of user navigation controller function  ======*/

	/*=====  End of Handlers and helpers Block  ======*/

	/*=======================================
	 =            Templates Block            =
	 =======================================*/
	/*=================================================
	 =            general template function            =
	 =================================================*/
	var source = $("#general-template").html();
	var template = Handlebars.compile(source);
	var interval;
	function generalTemplate(slideleft) {
		var html = template(content[countNext]);
		var html_prev;
		var html_next = template(content[countNext+1]);;
		$board_next.html(html_next);

		if(countNext > 0){
			html_prev = template(content[countNext-1]);
		}

		$board_prev.html(html_prev);
		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
    vocabcontroller.findwords(countNext);

    switch(countNext){
      case 0:
      sound_player('sound_0',1);
      $('.lefttext,.image_1').css({'opacity':"0"});
      $('.righttext,.image_2').css({'opacity':"0"});
      break;
      case 1:
      sound_player('sound_1',1);
      $('.lefttext,.image_1').css({'opacity':"0"});
      $('.righttext,.image_2').css({'opacity':"0"});
      setTimeout(function(){$('.lefttext,.image_1').animate({'opacity':"1"},1000,"linear");},3500);
      setTimeout(function(){$('.righttext,.image_2').animate({'opacity':"1"},1000,"linear");},13500);
      break;
      case 2:
      sound_player('sound_2',1);
      break;
      case 3:
      sound_player('sound_3',1);
      break;
    }
	}
  function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next==1){
			navigationcontroller();
		}
		});
	}
	function templateCaller() {
			/*always hide next and previous navigation button unless
			 explicitly called from inside a template*/
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// call navigation controller
			//navigationcontroller();

			// call the template

			generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


			//call the slide indication bar handler for pink indicators
			loadTimelineProgress($total_page, countNext + 1);

			// just for development purpose to see total slide vs current slide number
			// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click', function() {
		if(countNext < 5){
			countNext++;
			$(this).hide(0);
			$prevBtn.hide(0);
			$board_next.addClass("move_left_2");
			$board.addClass("move_left_1");
			setTimeout(function(){
				$board_next.removeClass("move_left_2");
				$board.removeClass("move_left_1");
				templateCaller();
			}, 4000);
		}else {
			countNext++;
			 if(countNext == 6){
			 	 $(".additional_ltb > p").show(0).addClass("expand");
				 setTimeout(function(){
					 templateCaller();
				 }, 1550);
			 }else{
				templateCaller();
			 }
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		if(countNext < 6){
			countNext--;
			$(this).hide(0);
			$nextBtn.hide(0);
			$board.addClass("move_right_2");
			$board_prev.addClass("move_right_1");
			setTimeout(function(){
				$board.removeClass("move_right_2");
				$board_prev.removeClass("move_right_1");
				templateCaller();
			}, 4000);
		}else{
			countNext--;
			templateCaller();
		}

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/*=====  End of Templates Controller Block  ======*/
});

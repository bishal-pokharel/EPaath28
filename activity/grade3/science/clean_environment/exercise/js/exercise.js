var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/"+ $lang+"/";

var dialog0 = new buzz.sound(soundAsset+"Ex_ins.ogg");

	var content=[
		//slide 0
		{
			contentblockadditionalclass: 'main_bg',
			extratextblock : [{
				textdata : data.string.p6text1,
				textclass : 'instruction my_font_big sniglet'
			},
			{
				textdata : data.string.p6text2,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "q1a.png",
				containerclass : 'class1 options'
			},
			{
				imgsrc : imgpath + "q1b.png",
				containerclass : 'class2 options'
			},],
		},
		//slide 1
		{
			contentblockadditionalclass: 'main_bg',
			extratextblock : [{
				textdata : data.string.p6text1,
				textclass : 'instruction my_font_big sniglet'
			},
			{
				textdata : data.string.p6text3,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "q2a.png",
				containerclass : 'class1 options'
			},
			{
				imgsrc : imgpath + "q2b.png",
				containerclass : 'class2 options'
			}],
		},
		//slide 2
		{
			contentblockadditionalclass: 'main_bg',
			extratextblock : [{
				textdata : data.string.p6text1,
				textclass : 'instruction my_font_big sniglet'
			},
			{
				textdata : data.string.p6text4,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "q3a.png",
				containerclass : 'class1 options'
			},
			{
				imgsrc : imgpath + "q3b.png",
				containerclass : 'class2 options'
			}],
		},
		//slide 3
		{
			contentblockadditionalclass: 'main_bg',
			extratextblock : [{
				textdata : data.string.p6text1,
				textclass : 'instruction my_font_big sniglet'
			},
			{
				textdata : data.string.p6text5,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "q4a.png",
				containerclass : 'class1 options'
			},
			{
				imgsrc : imgpath + "q4b.png",
				containerclass : 'class2 options'
			}],
		},
		//slide 4
		{
			contentblockadditionalclass: 'main_bg',
			extratextblock : [{
				textdata : data.string.p6text1,
				textclass : 'instruction my_font_big sniglet'
			},
			{
				textdata : data.string.p6text6,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "q5a.png",
				containerclass : 'class1 options'
			},
			{
				imgsrc : imgpath + "q5b.png",
				containerclass : 'class2 options'
			}],
		},
		//slide 5
		{
			contentblockadditionalclass: 'main_bg',
			extratextblock : [{
				textdata : data.string.p6text1,
				textclass : 'instruction my_font_big sniglet'
			},
			{
				textdata : data.string.p6text7,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "q6a.png",
				containerclass : 'class1 options'
			},
			{
				imgsrc : imgpath + "q6b.png",
				containerclass : 'class2 options'
			}],
		},
		//slide 6
		{
			contentblockadditionalclass: 'main_bg',
			extratextblock : [{
				textdata : data.string.p6text1,
				textclass : 'instruction my_font_big sniglet'
			},
			{
				textdata : data.string.p6text8,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "q7a.png",
				containerclass : 'class1 options'
			},
			{
				imgsrc : imgpath + "q7b.png",
				containerclass : 'class2 options'
			}],
		},
		//slide 7
		{
			contentblockadditionalclass: 'main_bg',
			extratextblock : [{
				textdata : data.string.p6text1,
				textclass : 'instruction my_font_big sniglet'
			},
			{
				textdata : data.string.p6text9,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "q8a.png",
				containerclass : 'class1 options'
			},
			{
				imgsrc : imgpath + "q8b.png",
				containerclass : 'class2 options'
			}],
		},
		//slide 7
		{
			contentblockadditionalclass: 'main_bg',
			extratextblock : [{
				textdata : data.string.p6text1,
				textclass : 'instruction my_font_big sniglet'
			},
			{
				textdata : data.string.p6text10,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "q9a.png",
				containerclass : 'class1 options'
			},
			{
				imgsrc : imgpath + "q9b.png",
				containerclass : 'class2 options'
			}],
		},
		//slide 7
		{
			contentblockadditionalclass: 'main_bg',
			extratextblock : [{
				textdata : data.string.p6text1,
				textclass : 'instruction my_font_big sniglet'
			},
			{
				textdata : data.string.p6text11,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "q10a.png",
				containerclass : 'class1 options'
			},
			{
				imgsrc : imgpath + "q10b.png",
				containerclass : 'class2 options'
			}],
		}
	];

	//content.shufflearray();


$(function ()
{
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 10;
	var score = 0;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

	var rhino = new RhinoTemplate();
	rhino.init($total_page);

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);

		if (countNext==0){
			soundplayer(dialog0);
		}
		//randomize options
		var parent = $(".optionsblock");
		var divs = parent.children();
		while (divs.length) {
			parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
		var wrong_clicked = false;
		$(".option_container").click(function(){
			if($(this).hasClass("class1")){
				if(!wrong_clicked){
					rhino.update(true);
				}
				$('.command_question').show(0);
				$(".option_container").css('pointer-events','none');
	 			play_correct_incorrect_sound(1);
				$(this).children('.correctans').show(0);
				if(countNext != $total_page)
					$nextBtn.show(0);
			}
			else{
				if(!wrong_clicked){
					rhino.update(false);
				}
				$(this).children('.incorrectans').show(0);
				$(this).css('pointer-events', 'none');
				wrong_clicked = true;
	 			play_correct_incorrect_sound(0);
			}
		});
	}

	function soundplayer(i){
		buzz.all().stop();
		i.play().bind("ended",function(){
				navigationcontroller();
		});
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
		rhino.gotoNext();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+ "/";
/**
 * Duplicate variables
 */

var imgpath = $ref+"/images/";

var content=[
//slide3
{	/**
	 * TODO
	 * These images are not preloaded
	 */
	startdivs:[
	{
		partimg: imgpath + "rose01.png",
		partimg2: imgpath + "plant02.png",
		partimgclass: "object-one",
		partimgclass2: "object-one-sec",
		partcontainer: "top-one",
		startdata: data.string.head1
	},
	{
		partimg: imgpath + "corn_plant.png",
		partimg2: imgpath + "tree.png",
		partimgclass: "object-two",
		partimgclass2: "object-two-sec",
		partcontainer: "top-two",
		startdata: data.string.head2
	},
	{
		partimg: imgpath + "plant01.png",
		partimg2: imgpath + "lotus.png",
		partimgclass: "object-three",
		partimgclass2: "object-three-sec",
		partcontainer: "top-three",
		startdata: data.string.head3
	},
	{
		partimg: imgpath + "castus.png",
		partimg2: imgpath + "x_mass_tree.png",
		partimgclass: "object-four",
		partimgclass2: "object-four-sec",
		partcontainer: "top-four",
		startdata: data.string.head4
	}
	]
},

//slide4
{
	startdivs:[
	{
		partimg: imgpath + "rose01.png",
		partimg2: imgpath + "plant02.png",
		partimgclass: "object-one removethisimg",
		partimgclass2: "object-one-sec removethisimg",
		partcontainer: "top-one removethis",
		startdata: data.string.head1
	},
	{
		partimg: imgpath + "corn_plant.png",
		partimg2: imgpath + "tree.png",
		partimgclass: "object-two removethisimg",
		partimgclass2: "object-two-sec removethisimg",
		partcontainer: "top-two removethis",
		startdata: data.string.head2
	},
	{
		partimg: imgpath + "plant01.png",
		partimg2: imgpath + "lotus.png",
		partimgclass: "object-three selectthisimg1",
		partimgclass2: "object-three-sec selectthisimg2",
		partcontainer: "top-three selectthis",
		startdata: data.string.head3
	},
	{
		partimg: imgpath + "castus.png",
		partimg2: imgpath + "x_mass_tree.png",
		partimgclass: "object-four removethisimg",
		partimgclass2: "object-four-sec removethisimg",
		partcontainer: "top-four removethis",
		startdata: data.string.head4
	}
	]
},
// slide5
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg2",
	uppertextblock:[
	{
		textclass: "titletext",
		textdata: data.string.p3text1
	}
],
imageblock:[{
	imagestoshow:[
	{
		imgclass: "fourplants-1",
		imgsrc: "",
		imgid : 'img1'
	},
	{
		imgclass: "fourplants-2",
		imgsrc: "",
		imgid : 'img2'
	},
	{
		imgclass: "fourplants-3",
		imgsrc: "",
		imgid : 'img3'
	},
	{
		imgclass: "fourplants-4 roseextra",
		imgsrc: "",
		imgid : 'img4'
	}
	],
}]
},
// slide6
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg2",
	uppertextblock:[
	{
		textclass: "uptext",
		textdata: data.string.p3text2
	}
],
imageblock:[{
	imagestoshow:[
	{
		imgclass: "fourplants-1",
		imgsrc: "",
		imgid : 'img1'
	},
	{
		imgclass: "fourplants-2",
		imgsrc: "",
		imgid : 'img2'
	},
	{
		imgclass: "fourplants-3",
		imgsrc: "",
		imgid : 'img3'
	},
	{
		imgclass: "fourplants-4 roseextra",
		imgsrc: "",
		imgid : 'img4'
	}
	],
}]
},
// slide7
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg2",
	uppertextblock:[
	{
		textclass: "uptext",
		textdata: data.string.p3text3
	}
],
imageblock:[{
	imagestoshow:[
	{
		imgclass: "fourplants-1",
		imgsrc: "",
		imgid : 'img5'
	},
	{
		imgclass: "fourplants-2",
		imgsrc: "",
		imgid : 'img6'
	},
	{
		imgclass: "fourplants-3",
		imgsrc: "",
		imgid : 'img7'
	},
	{
		imgclass: "fourplants-4",
		imgsrc: "",
		imgid : 'img8'
	}
	],
}]
},
// slide8
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg3",
	uppertextblock:[
	{
		textclass: "titletext",
		textdata: data.string.p3text4
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "fourplants-1",
			imgsrc: "",
			imgid : 'img9'
		},
		{
			imgclass: "fourplants-2",
			imgsrc: "",
			imgid : 'img10'
		},
		{
			imgclass: "fourplants-3",
			imgsrc: "",
			imgid : 'img11'
		},
		{
			imgclass: "fourplants-4",
			imgsrc: "",
			imgid : 'img12'
		}
	],
}]
},
// slide9
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg3",
	uppertextblock:[
	{
		textclass: "uptext",
		textdata: data.string.p3text5
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "fourplants-1",
			imgsrc: "",
			imgid : 'img9'
		},
		{
			imgclass: "fourplants-2",
			imgsrc: "",
			imgid : 'img10'
		},
		{
			imgclass: "fourplants-3",
			imgsrc: "",
			imgid : 'img11'
		},
		{
			imgclass: "fourplants-4",
			imgsrc: "",
			imgid : 'img12'
		}
	],
}]
},
// slide10
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "diybg",
	uppertextblock:[
	{
		textclass: "titletext",
		textdata: data.string.diy
	}
]
},
// slide11
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "diybg",
	uppertextblock:[
	{
		textclass: "uptext",
		textdata: data.string.diyins
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "draggable noflower drag1",
			imgsrc: "",
			imgid : 'img12'
		},
		{
			imgclass: "draggable flower drag2",
			imgsrc: "",
			imgid : 'img6'
		},
		{
			imgclass: "draggable flower drag3",
			imgsrc: "",
			imgid : 'img7'
		},
		{
			imgclass: "draggable noflower drag4",
			imgsrc: "",
			imgid : 'img11'
		},
	{
		imgclass: "droppable-1",
		imgsrc: "",
		imgid : 'cart'
	},
	{
		imgclass: "droppable-2",
		imgsrc: "",
		imgid : 'cart'
	},
	{
		imgclass: "monkey",
		imgsrc: "",
		imgid : 'monkey'
	}
	],
	imagelabels:[
		{
			imagelabelclass: "labeldr-1",
			imagelabeldata: data.string.p3text1
		},
		{
			imagelabelclass: "labeldr-2",
			imagelabeldata: data.string.p3text4
		}
	]
}]
},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "cart", src: imgpath+"cart.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img1", src: imgpath+"potato-with-plant.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img2", src: imgpath+"sugercane.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img3", src: imgpath+"karkalo.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img4", src: imgpath+"rose03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img5", src: imgpath+"x-tree.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img6", src: imgpath+"bamboo.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img7", src: imgpath+"mabooleaf.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img8", src: imgpath+"castus.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img9", src: imgpath+"waterplant01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img10", src: imgpath+"waterplant02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img11", src: imgpath+"lotusleaf.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img12", src: imgpath+"waterplant03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "monkey", src: "images/sundar/correct-2.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			/**
			 * TODO
			 * These images need not be preloaded
			 */
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			// {id: "sound_1", src: soundAsset+"p3_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p3_s1.ogg"},
			{id: "sound_3", src: soundAsset+"p3_s2.ogg"},
			{id: "sound_4", src: soundAsset+"p3_s3.ogg"},
			{id: "sound_5", src: soundAsset+"p3_s4.ogg"},
			{id: "sound_6", src: soundAsset+"p3_s5.ogg"},
			{id: "sound_7", src: soundAsset+"p3_s6.ogg"},
			{id: "sound_8", src: soundAsset+"p3_s7.ogg"},
			{id: "sound_9", src: soundAsset+"p3_s8.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		/**
	 	 * TODO
		 * This part is not needed so better to remove it
		 * I used this for testing soundjs initially and forgot to remove it later
		 */
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);

		/**
		 * TODO
		 * no need for  put_speechbox_image() and put_image2()
		 */
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext){
			case 1:
			sound_player("sound_2");
			break;
			case 2:
			sound_player("sound_3");
			break;
			case 3:
			sound_player("sound_4");
			break;
			case 4:
			$('.fourplants-2').css({"height":"71%"});

			sound_player("sound_5");
			break;
			case 5:
			sound_player("sound_6");
			break;
			case 6:
			sound_player("sound_7");
			break;
			case 7:
			sound_player("sound_8");
			$('.titletext').css({"top":"27%"});

			break;
			case 8:
			sound_player("sound_9", "no");
			$(".draggable").draggable({
				containment : ".generalTemplateblock",
				revert : true,
				cursor : "move",
				zIndex: 100000,
			});

			$(".droppable-1").droppable({
				hoverClass : 'hover-active',
				drop: function (event, ui){
				$this = $(this);
				dropfunc(event, ui, $this);
			}
			});

			$(".droppable-2").droppable({
				hoverClass : 'hover-active',
				drop: function (event, ui){
				$this = $(this);
				dropfunc(event, ui, $this);
			}
			});

			var totalDropCount = 0;
			var floCount = 0;
			var nofloCount = 0;
			function dropfunc(event, ui, $droppedOn){
				if($droppedOn.hasClass("droppable-2")){
					if(ui.draggable.hasClass("noflower")){
						ui.draggable.draggable('option', 'revert', false);
						ui.draggable.draggable('disable');
						play_correct_incorrect_sound(1);
						if(floCount == 0){
							ui.draggable.css({
							"position" : "absolute",
							"left": "64%",
							"bottom": "26%",
							"top": "auto",
							"height": "auto",
							"width": "13.5%",
							"z-index": "1"
						}).appendTo(".coverboardfull");

						}
						else{
							ui.draggable.css({
							"position" : "absolute",
							"left": "74%",
							"bottom": "26%",
							"top": "auto",
							"height": "auto",
							"width": "13.5%",
							"z-index": "1"
						}).appendTo(".coverboardfull");
						}
						console.log(floCount);
						floCount++;
						totalDropCount++;
						fincheck();
					} else{
						play_correct_incorrect_sound(0);
					}
				}
				else{
					if(ui.draggable.hasClass("flower")){
						ui.draggable.draggable('option', 'revert', false);
						ui.draggable.draggable('disable');
						play_correct_incorrect_sound(1);
						if(nofloCount == 0){
							ui.draggable.css({
							"position" : "absolute",
							"left": "10%",
							"bottom": "26%",
							"top": "auto",
							"height": "auto",
							"width": "13.5%",
							"z-index": "1"
						}).appendTo(".coverboardfull");

						}
						else{
							ui.draggable.css({
							"position" : "absolute",
							"left": "20%",
							"bottom": "26%",
							"top": "auto",
							"height": "auto",
							"width": "13.5%",
							"z-index": "1"
						}).appendTo(".coverboardfull");
						}
						console.log(floCount);
						nofloCount++;
						totalDropCount++;
						fincheck();
					} else{
						play_correct_incorrect_sound(0);
					}
				}
				function fincheck(){
					if(totalDropCount == 4){
						$(".monkey").show(0);
						navigationcontroller();
					}
					else{
						$(".drag" + (totalDropCount+1)).show(0);
					}
				}
			}
			break;
			default:
				break;
		}
	}

	/**
	 * TODO
	 * This function is not used
	 */
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next == null)
			navigationcontroller();
		});
	}

	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2);
		});

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	/**
	 * TODO
	 * This function is not used
	 */
	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}

	/**
	 * TODO
	 * This function is not used
	 */
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
  {
    //page 0
    contentnocenteradjust: true,
    uppertextblock: [{
      textclass: "title_text",
      textdata: data.lesson.chapter
    }],
    imageblock:[
      {
        imagestoshow:[
          {
            imgclass:"bg_full",
            imgsrc: imgpath+ "cover_page.png"
          }
        ]
      }
    ]
  },
  {
    //page 1
    contentnocenteradjust: true,
    uppertextblock: [{
      textclass: "description",
      textdata: data.string.p1_s1
    }],
    imageblock:[
      {
        imagestoshow:[
          {
            imgclass:"image",
            imgsrc: imgpath+ "villagelife1.png"
          }
        ]
      }
    ]
  },{
    //page 2
    contentnocenteradjust: true,
    uppertextblock: [{
      textclass: "description",
      textdata: data.string.p1_s2
    }],
    imageblock:[
      {
        imagestoshow:[
          {
            imgclass:"image",
            imgsrc: imgpath+ "villagelife1.png"
          }
        ]
      }
    ]
  },{
    //page 3
    contentnocenteradjust: true,
    uppertextblock: [{
      textclass: "description",
      datahighlightflag: true,
      textdata: data.string.p1_s3
    }],
    imageblock:[
      {
        imagestoshow:[
          {
            imgclass:"image",
            imgsrc: imgpath+ "villagelife1.png"
          }
        ]
      }
    ]
  },{
    //page 4
    contentnocenteradjust: true,
    uppertextblock: [{
      textclass: "description",
      textdata: data.string.p1_s6,
      datahighlightflag:true,
      datahighlightcustomclass:'bold'
    }],
    imageblock:[
      {
        imagestoshow:[
          {
            imgclass:"image",
            imgsrc: imgpath+ "villagelife1.png"
          }
        ]
      }
    ]
  },{
    //page 5
    contentblockadditionalclass:'bg_purple',
    uppertextblock: [{
      textclass: "description",
      textdata: data.string.p1_s8
    }]
  },{
    //page 6
    contentnocenteradjust: true,
    contentblockadditionalclass: "custombg2",
    uppertextblock: [{
      textclass: "description",
      textdata: data.string.p1_s7
    },
  {
    textclass: "border_container",
  }],
    imageblock:[
      {
        imagestoshow:[
          {
            imgclass:"container",
            imgsrc: imgpath+ "containermain.png"
          },
          {
            imgclass:"arrow",
            imgsrc: imgpath+ "arrow.png"
          },
          {
            imgclass:"pipe",
            imgsrc: imgpath+ "pipe.png"
          },
          {
            imgclass:"digestor",
            imgsrc: imgpath+ "digestor.png"
          },
          {
            imgclass:"digestor_out",
            imgsrc: imgpath+ "digestor_out.png"
          },
          {
            imgclass:"digestor_top_rod",
            imgsrc: imgpath+ "digestor_top_rod.png"
          },
          {
            imgclass:"gaspipe",
            imgsrc: imgpath+ "gaspipe.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass:"containerlabel",
            imagelabeldata: data.string.p1_s10
          }
        ]
      }
    ]
  },{
    //page 7
    contentnocenteradjust: true,
    contentblockadditionalclass: "custombg2",
    uppertextblock: [{
      textclass: "description",
      textdata: data.string.p1_s7
    },
  {
    textclass: "border_turbine",
  }],
    imageblock:[
      {
        imagestoshow:[
          {
            imgclass:"container",
            imgsrc: imgpath+ "containermain.png"
          },
          {
            imgclass:"arrow1",
            imgsrc: imgpath+ "arrow.png"
          },
          {
            imgclass:"pipe",
            imgsrc: imgpath+ "pipe.png"
          },
          {
            imgclass:"digestor",
            imgsrc: imgpath+ "digestor.png"
          },
          {
            imgclass:"digestor_out",
            imgsrc: imgpath+ "digestor_out.png"
          },
          {
            imgclass:"digestor_top_rod",
            imgsrc: imgpath+ "digestor_top_rod.png"
          },
          {
            imgclass:"gaspipe",
            imgsrc: imgpath+ "gaspipe.png"
          },
          {

          }
        ],
        imagelabels: [
          {
            imagelabelclass:"turbinelabel",
            imagelabeldata: data.string.p1_s11
          }
        ]
      }
    ]
  },{
    //page 8
    contentnocenteradjust: true,
    contentblockadditionalclass: "custombg2",
    uppertextblock: [{
      textclass: "description",
      textdata: data.string.p1_s20
    },{
      textclass: "border_bucket1",
    },
    {
      textclass:"turbinelabel_bucket",
      textdata: data.string.p1_s12
    }],
    imageblock:[
      {
        imagestoshow:[
          {
            imgclass:"container",
            imgsrc: imgpath+ "containermain.png"
          },
          {
            imgclass:"pipe",
            imgsrc: imgpath+ "pipe.png"
          },
          {
            imgclass:"digestor",
            imgsrc: imgpath+ "digestor.png"
          },
          {
            imgclass:"digestor_out",
            imgsrc: imgpath+ "digestor_out.png"
          },
          {
            imgclass:"digestor_top_rod",
            imgsrc: imgpath+ "digestor_top_rod.png"
          },
          {
            imgclass:"gaspipe",
            imgsrc: imgpath+ "gaspipe.png"
          },
          {
            imgclass:"bucket  ",
            imgsrc: imgpath+ "bucket_pani.png"
          }
        ],
        // imagelabels: [
        //   {
        //     imagelabelclass:"turbinelabel_bucket",
        //     imagelabeldata: data.string.p1_s12
        //   }
        // ]
      }
    ]
  },{
    //page 9
    contentnocenteradjust: true,
    contentblockadditionalclass: "custombg2",
    uppertextblock: [{
      textclass: "description",
      textdata: data.string.p1_s20
    },{
      textclass: "border_bucket1",
    }],
    imageblock:[
      {
        imagestoshow:[
          {
            imgclass:"container",
            imgsrc: imgpath+ "containermain_with_water.png"
          },
          {
            imgclass:"pipe",
            imgsrc: imgpath+ "pipe.png"
          },
          {
            imgclass:"digestor",
            imgsrc: imgpath+ "digestor.png"
          },
          {
            imgclass:"digestor_out",
            imgsrc: imgpath+ "digestor_out.png"
          },
          {
            imgclass:"digestor_top_rod",
            imgsrc: imgpath+ "digestor_top_rod.png"
          },
          {
            imgclass:"gaspipe",
            imgsrc: imgpath+ "gaspipe.png"
          },
          {
            imgclass:"bucket  ",
            imgsrc: imgpath+ "emptybucket.png"
          },
          {
            imgclass:"dungpile",
            imgsrc: imgpath+ "gubar.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass:"turbinelabel_bucket",
            imagelabeldata: data.string.p1_s13
          }
        ]
      }
    ]
  },{
    //page 10
    contentnocenteradjust: true,
    contentblockadditionalclass: "custombg2",
    uppertextblock: [{
      textclass: "description",
      textdata: data.string.p1_s9
    },
  {
    textclass: "border_container",
  }],
    imageblock:[
      {
        imagestoshow:[
          {
            imgclass:"container  ",
            imgsrc: imgpath+ "containermain_with_water.png"
          },
          {
            imgclass:"pipe",
            imgsrc: imgpath+ "pipe.png"
          },
          {
            imgclass:"digestor",
            imgsrc: imgpath+ "digestor.png"
          },
          {
            imgclass:"digestor_out",
            imgsrc: imgpath+ "digestor_out.png"
          },
          {
            imgclass:"digestor_top_rod",
            imgsrc: imgpath+ "digestor_top_rod.png"
          },
          {
            imgclass:"gaspipe",
            imgsrc: imgpath+ "gaspipe.png"
          },
          {
            imgclass:"bucket",
            imgsrc: imgpath+ "bucket_gobar.png"
          },
          {
            imgclass:"dungpile",
            imgsrc: imgpath+ "gubar.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass:"turbinelabel2",
            imagelabeldata: data.string.p1_s14
          },
          {
            imagelabelclass:"container_animate_holder",
            imagelabeldata: "&nbsp;"
          }
        ]
      }
    ]
  },{
    //page 11
    contentnocenteradjust: true,
    contentblockadditionalclass: "custombg2",
    uppertextblock: [{
      textclass: "description",
      textdata: data.string.p1_s9
    },
  {
    textclass: "border_turbine",
  }],
    imageblock:[
      {
        imagestoshow:[
          {
            imgclass:"container  ",
            imgsrc: imgpath+ "containermain_with_gobar.png"
          },
          {
            imgclass:"pipe",
            imgsrc: imgpath+ "pipe.png"
          },
          {
            imgclass:"digestor",
            imgsrc: imgpath+ "digestor.png"
          },
          {
            imgclass:"digestor_out",
            imgsrc: imgpath+ "digestor_out.png"
          },
          {
            imgclass:"digestor_top_rod",
            imgsrc: imgpath+ "digestor_top_rod.png"
          },
          {
            imgclass:"gaspipe",
            imgsrc: imgpath+ "gaspipe.png"
          },
          {
            imgclass:"bucket",
            imgsrc: imgpath+ "emptybucket.png"
          },
          {
            imgclass:"dungpile",
            imgsrc: imgpath+ "gubar.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass:"turbinelabel2",
            imagelabeldata: data.string.p1_s15
          },
          {
            imagelabelclass:"container_animate_holder2",
            imagelabeldata: "&nbsp;"
          }
        ]
      }
    ]
  },{
    //page 12
    contentnocenteradjust: true,
    contentblockadditionalclass: "custombg2",
    uppertextblock: [{
      textclass: "description",
      textdata: data.string.p1_s9
    },
    {
      textclass: "border_pipe",
    }],
    imageblock:[
      {
        imagestoshow:[
          {
            imgclass:"container",
            imgsrc: imgpath+ "containermain_with_gobarmix.png"
          },
          {
            imgclass:"pipe  ",
            imgsrc: imgpath+ "pipe.png"
          },
          {
            imgclass:"digestor",
            imgsrc: imgpath+ "digestor.png"
          },
          {
            imgclass:"digestor_out",
            imgsrc: imgpath+ "digestor_out.png"
          },
          {
            imgclass:"digestor_top_rod",
            imgsrc: imgpath+ "digestor_top_rod.png"
          },
          {
            imgclass:"gaspipe",
            imgsrc: imgpath+ "gaspipe.png"
          },
          {
            imgclass:"bucket",
            imgsrc: imgpath+ "emptybucket.png"
          },
          {
            imgclass:"dungpile",
            imgsrc: imgpath+ "gubar.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass:"turbinelabel2",
            imagelabeldata: data.string.p1_s16
          },
          {
            imagelabelclass:"pipe_animate_holder",
            imagelabeldata: "&nbsp;"
          }
        ]
      }
    ]
  },{
    //page 13
    contentnocenteradjust: true,
    contentblockadditionalclass: "custombg2",
    uppertextblock: [{
      textclass: "description",
      textdata: data.string.p1_s9
    }],
    imageblock:[
      {
        imagestoshow:[
          {
            imgclass:"container",
            imgsrc: imgpath+ "containermain_with_gobarmix.png"
          },
          {
            imgclass:"pipe",
            imgsrc: imgpath+ "pipewithgobar.png"
          },
          {
            imgclass:"bucket",
            imgsrc: imgpath+ "emptybucket.png"
          },
          {
            imgclass:"dungpile",
            imgsrc: imgpath+ "gubar.png"
          },
          {
            imgclass: "watch",
            imgsrc: imgpath+ "clock.png"
          },
          {
            imgclass: "watch_hour",
            imgsrc: imgpath+ "hourhand.png"
          },
          {
            imgclass: "watch_mins",
            imgsrc: imgpath+ "minutehand.png"
          },
          {
            imgclass: "arrowone",
            imgsrc: imgpath+ "redarrow_up.png"
          },
          {
            imgclass: "arrowtwo",
            imgsrc: imgpath+ "redarrow_right.png"
          }
        ],
        imagelabels: [
          {
            imagelabelclass:"turbinelabel2",
            imagelabeldata: data.string.p1_s17
          },
          {
            imagelabelclass:"digestor_animate_holder digestor_animate1",
            imagelabeldata: "&nbsp;"
          }
        ]
      }
    ]
  },
  //page 14
  {
    contentnocenteradjust: true,
    uppertextblock: [{
      textclass: "description",
      textdata: data.string.p1_s7
    },{
      textclass: "page15_text",
      textdata: data.string.p1_s23
    },{
      textclass: "border_page15",
    }],
    imageblock:[
      {
        imagestoshow:[
          {
            imgclass: "bg_full",
            imgsrc: imgpath+ "bio_gas.png"
          },
          {
            imgclass: "arrow2",
            imgsrc: imgpath+ "arrow1.png"
          }
        ]
      }
    ]
  },
  //page 15
  {
    contentnocenteradjust: true,
    uppertextblock: [{
      textclass: "description",
      textdata: data.string.p1_s7
    },{
      textclass: "page16_text",
      textdata: data.string.p1_s24
    },{
      textclass: "border_page16",
    }],
    imageblock:[
      {
        imagestoshow:[
          {
            imgclass: "bg_full",
            imgsrc: imgpath+ "bio_gas.png"
          },
          {
            imgclass: "arrow3",
            imgsrc: imgpath+ "arrow1.png"
          }
        ]
      }
    ]
  }
  ,{
    //page 16
    contentnocenteradjust: true,
    contentblockadditionalclass: "custombg3",
    // uppertextblock: [{
    //   textclass: "description",
    //   textdata: data.string.p1_s9
    // }],
    imageblock:[
      {
        imagestoshow:[
          {
            imgclass: "wave1",
            imgsrc: imgpath+ "wave.png"
          },
          {
            imgclass: "wave2",
            imgsrc: imgpath+ "wave.png"
          },
          {
            imgclass: "wave3",
            imgsrc: imgpath+ "wave.png"
          }
        ]
      }
    ]
  },
  {
    //page 17
    contentnocenteradjust: true,
    uppertextblock: [{
      textclass: "description",
      textdata: data.string.p1_s21,
      datahighlightflag:true,
      datahighlightcustomclass:'bold'
    }],
    imageblock:[
      {
        imagestoshow:[
          {
            imgclass:"image",
            imgsrc: imgpath+ "villagelife1.png"
          }
        ]
      }
    ]
  },{
    //page 18
    contentblockadditionalclass:'bg_purple',
    uppertextblock: [{
      textclass: "description",
      textdata: data.string.p1_s22
    }]
  }

];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);
  var preload;
  var timeoutvar = null;
  var current_sound;
  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      //images
      // {id: "cover", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},

      // sounds

      {id: "title", src: soundAsset+"title.ogg"},
      {id: "sound_1", src: soundAsset+"p1_s0.ogg"},
      {id: "sound_2", src: soundAsset+"p1_s1.ogg"},
      {id: "sound_3", src: soundAsset+"p1_s2.ogg"},
      {id: "sound_4", src: soundAsset+"p1_s3.ogg"},
      {id: "sound_5", src: soundAsset+"p1_s4.ogg"},
      {id: "sound_6", src: soundAsset+"p1_s5.ogg"},
      {id: "sound_5_1", src: soundAsset+"p1_s5_1.ogg"},
      {id: "sound_7", src: soundAsset+"p1_s6.ogg"},
      {id: "sound_8", src: soundAsset+"p1_s7.ogg"},
      {id: "sound_9", src: soundAsset+"p1_s8.ogg"},
      {id: "sound_10", src: soundAsset+"p1_s9.ogg"},
      {id: "sound_11", src: soundAsset+"p1_s10.ogg"},
      {id: "sound_12", src: soundAsset+"p1_s11.ogg"},
      {id: "sound_13", src: soundAsset+"p1_s12.ogg"},
      {id: "sound_15", src: soundAsset+"p1_s14.ogg"},
      {id: "sound_16", src: soundAsset+"p1_s15.ogg"},
      {id: "sound_16_1", src: soundAsset+"p1_s15_1.ogg"},
      {id: "sound_15a", src: soundAsset+"p1_s15a.ogg"},
      {id: "sound_15b", src: soundAsset+"p1_s15b.ogg"},

    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound);//for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }
  function handleFileLoad(event) {
    // console.log(event.item);
  }
  function handleProgress(event) {
    $('#loading-text').html(parseInt(event.loaded*100)+'%');
  }
  function handleComplete(event) {
    $('#loading-wrapper').hide(0);
    //initialize varibales
    current_sound = createjs.Sound.play('sound_1');
    current_sound.stop();
    // call main function
    templateCaller();
  }
  //initialize
  init();


  var vocabcontroller =  new Vocabulary();
  vocabcontroller.init();
  /*==================================================
  =            Handlers and helpers Block            =
  ==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

  /*===============================================
  =            data highlight function            =
  ===============================================*/
  /**

  What it does:
  - send an element where the function has to see
  for data to highlight
  - this function searches for all nodes whose
  data-highlight element is set to true
  -searches for # character and gives a start tag
  ;span tag here, also for @ character and replaces with
  end tag of the respective
  - if provided with data-highlightcustomclass value for highlight it
  applies the custom class or else uses parsedstring class

  E.g: caller : texthighlight($board);
  */
  function texthighlight($highlightinside){
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
    alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
    null ;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag   = "</span>";
    if($alltextpara.length > 0){
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
        use that or else use default 'parsedstring'*/
        $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
        (stylerulename = $(this).attr("data-highlightcustomclass")) :
        (stylerulename = "parsedstring") ;

        texthighlightstarttag = "<span class='"+stylerulename+"'>";
        replaceinstring       = $(this).html();
        replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
        replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
        $(this).html(replaceinstring);
      });
    }
  }
  /*=====  End of data highlight function  ======*/

  /*===============================================
  =            user notification function        =
  ===============================================*/
  /**
  How to:
  - First set any html element with
  "data-usernotification='notifyuser'" attribute,
  and "data-isclicked = ''".
  - Then call this function to give notification
  */

  /**
  What it does:
  - You send an element where the function has to see
  for data to notify user
  - this function searches for all text nodes whose
  data-usernotification attribute is set to notifyuser
  - applies event handler for each of the html element which
  removes the notification style.
  */
  function notifyuser($notifyinside){
    //check if $notifyinside is provided
    typeof $notifyinside !== "object" ?
    alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
    null ;

    /*variable that will store the element(s) to remove notification from*/
    var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
    // if there are any notifications removal required add the event handler
    if($allnotifications.length > 0){
      $allnotifications.one('click', function() {
        /* Act on the event */
        $(this).attr('data-isclicked', 'clicked');
        $(this).removeAttr('data-usernotification');
      });
    }
  }
  /*=====  End of user notification function  ======*/

  /*======================================================
  =            Navigation Controller Function            =
  ======================================================*/
  /**
  How To:
  - Just call the navigation controller if it is to be called from except the
  last page of lesson
  - If called from last page set the islastpageflag to true such that
  footernotification is called for continue button to navigate to exercise
  */

  /**
  What it does:
  - If not explicitly overriden the method for navigation button
  controls, it shows the navigation buttons as required,
  according to the total count of pages and the countNext variable
  - If for a general use it can be called from the templateCaller
  function
  - Can be put anywhere in the template function as per the need, if
  so should be taken out from the templateCaller function
  - If the total page number is
  */

  function navigationcontroller(islastpageflag){
    typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

    if (countNext == 0 && $total_page != 1) {
      $nextBtn.delay(1000).show(0);
      $prevBtn.css('display', 'none');
    } else if ($total_page == 1) {
      $prevBtn.css('display', 'none');
      $nextBtn.css('display', 'none');

      // if lastpageflag is true
      islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
    } else if (countNext > 0 && countNext < $total_page - 1) {
      $nextBtn.show(0);
      $prevBtn.show(0);
    } else if (countNext == $total_page - 1) {
      $nextBtn.css('display', 'none');
      $prevBtn.delay(7500).show(0);

      // if lastpageflag is true
      setTimeout(function(){

        islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
      });

    }

  }
  /*=====  End of user navigation controller function  ======*/

  /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
  /**
  How to:
  - Just call instructionblockcontroller() from the template
  */

  /**
  What it does:
  - It inserts and handles closing and opening of instruction block
  - this function searches for all text nodes whose
  data-usernotification attribute is set to notifyuser
  - applies event handler for each of the html element which
  removes the notification style.
  */
  function instructionblockcontroller(){
    var $instructionblock = $board.find("div.instructionblock");
    if($instructionblock.length > 0){
      var $contentblock = $board.find("div.contentblock");
      var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
      var instructionblockisvisibleflag;

      $contentblock.css('pointer-events', 'none');

      $toggleinstructionblockbutton.on('click', function() {
        instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
        if(instructionblockisvisibleflag == 'true'){
          instructionblockisvisibleflag = 'false';
          $contentblock.css('pointer-events', 'auto');
        }
        else if(instructionblockisvisibleflag == 'false'){
          instructionblockisvisibleflag = 'true';
          $contentblock.css('pointer-events', 'none');
        }

        $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
      });
    }
  }
  /*=====  End of InstructionBlockController  ======*/

  /*=====  End of Handlers and helpers Block  ======*/

  /*=======================================
  =            Templates Block            =
  =======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    vocabcontroller.findwords(countNext);

    //call instruction block controller
    // instructionblockcontroller($board);

    //call notifyuser
    // notifyuser($anydiv);
    switch (countNext){
      case 0:
      sound_player('title');
      break;
      case 1:
      sound_player('sound_'+countNext);
      break;
      case 2:
      sound_player('sound_'+countNext);
      break;
      case 3:
      sound_player('sound_'+countNext);
      var $description2 = $(".description2");
      $nextBtn.hide(0);
      $description2.hide(0);
      setTimeout(function(){
        $nextBtn.show(0);
        $description2.show(0);
      }, 1500);
      break;
      case 4:
      sound_player('sound_'+countNext);
      break;
      case 5:
      sound_player('sound_'+countNext);
      break;
      case 6:
      $('.containerlabel').css({
        "left": "20%",
        "bottom":"78%"
      });
      $('.containerlabel,.arrow').hide(0);
      $('.border_container').click(function(){
        $(this).css('animation','none');
        $('.containerlabel,.arrow').fadeIn(500);
        sound_player_only('sound_5_1');
        navigationcontroller();
      });
      sound_player_only('sound_'+countNext);
      break;
    case 7:
      $('.turbinelabel').css({
        "left": "20%",
        "bottom":"68%"
      });
      $('.turbinelabel,.arrow1').hide(0);
      $('.border_turbine').click(function(){
        $(this).css('animation','none');
        $('.turbinelabel,.arrow1').fadeIn(500);
        sound_player('sound_'+countNext);
      });
    break;
    case 8:
    sound_player_only('sound_'+countNext);
    $('.border_bucket1').click(function(){
      $(this).hide(0);
      $(".container").attr("src", imgpath+"containermain_with_water.png");
      $(".bucket").attr("src", imgpath+"emptybucket.png");
      navigationcontroller();
    });
    break;
    case 9:
    sound_player_only('sound_'+countNext);
    $('.border_bucket1').click(function(){
      $(this).hide(0);
      $(".bucket").attr("src", imgpath+"bucket_gobar.png");
      navigationcontroller();
    });
    break;
    case 10:
    sound_player('sound_'+countNext);
    var $blink = $(".blink");
    var $bucket = $(".bucket");
    $nextBtn.hide(0);
    $(".container_animate_holder").hide(0);
    $('.border_container').click(function(){
      $bucket.addClass("animatebucket");
      setTimeout(function(){
        $('.border_container').hide(0);
        $(".container_animate_holder").show(0);
      }, 2000);
      setTimeout(function(){
        $nextBtn.show(0);
        $bucket.attr("src", imgpath+"emptybucket.png").delay(200).removeClass("animatebucket");
      }, 3900);

    });
    break;
    case 11:
    sound_player('sound_'+countNext);
    var $blink = $(".blink");
    $nextBtn.hide(0);
    $(".container_animate_holder2").hide(0);
    $('.border_turbine').click(function(){
      $('.container').hide(0);
      $('.border_turbine').hide(0);
      $(".container_animate_holder2").show(0);
      setTimeout(function(){
        $nextBtn.show(0);
      }, 3900);

    });
    break;
    case 12:
    sound_player('sound_'+countNext);
    var $blink = $(".blink");
    $nextBtn.hide(0);
    $(".pipe_animate_holder").hide(0);
    $('.border_pipe').click(function(){
      $('.border_pipe').hide(0);
      $('.pipe').hide(0);
      $(".pipe_animate_holder").show(0);
      setTimeout(function(){
        $nextBtn.show(0);
      }, 3900);

    });
    break;
    case 13:
    sound_player_only('sound_'+countNext);
    $(".watch, .watch_hour, .watch_mins").hide(0);
    var $arrow1 = $(".arrowone").hide(0);
    var $arrow2 = $(".arrowtwo").hide(0);
    setTimeout(function(){
      $(".watch, .watch_hour, .watch_mins").show(0);
    }, 1000);
    setTimeout(function(){
      $(".watch, .watch_hour, .watch_mins").hide(0);
      $(".digestor_animate1").addClass("digestor_animate2");
      $arrow1.delay(1500).show(0).delay(1000).hide(0);
      $arrow2.delay(2500).show(0);
    }, 13200);
    setTimeout(function(){
      navigationcontroller();
    },18000);

    break;

    case 14:
    sound_player_only('sound_6');
    $('.page15_text,.arrow2').hide(0);
    $('.border_page15').click(function(){
      $(this).css({'animation':'none','pointer-events':'none'});
      $('.page15_text,.arrow2').fadeIn(500);
      //wrong audio
      sound_player('sound_15b');
    });
    break;
    case 15:
    sound_player_only('sound_6');
    $('.page16_text,.arrow3').hide(0);
    $('.border_page16').click(function(){
      $(this).css({'animation':'none','pointer-events':'none'});
      $('.page16_text,.arrow3').fadeIn(500);
      //wrong audio
      sound_player('sound_15a');

    });
    break;
    case 17:
    sound_player('sound_15');
    break;
    case 18:
    $prevBtn.show(0);
    sound_player('sound_16');
    break;
    default:
    navigationcontroller();
    break;
  }
  // find if there is linehorizontal div in the slide
  // var $linehorizontal = $board.find("div.linehorizontal");
  // if($linehorizontal.length > 0)
  // {
  //   $linehorizontal.attr('data-isdrawn', 'draw');
  // }
}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

/*==================================================
=            function to call templates            =
==================================================*/
/**
Motivation :
- Make a single function call that handles all the
template load easier

How To:
- Update the template caller with the required templates
- Call template caller

What it does:
- According to value of the Global Variable countNext
the slide templates are updated
*/

function sound_player(sound_id){
  createjs.Sound.stop();
  current_sound = createjs.Sound.play(sound_id);
  current_sound.play();
  current_sound.on('complete', function(){
    navigationcontroller();
  })
}
function sound_player_only(sound_id){
  createjs.Sound.stop();
  current_sound = createjs.Sound.play(sound_id);
  current_sound.play();
}
function templateCaller(){
  /*always hide next and previous navigation button unless
  explicitly called from inside a template*/
  $prevBtn.css('display', 'none');
  $nextBtn.css('display', 'none');

  // call navigation controller
  //navigationcontroller();

  // call the template
  generalTemplate();
  /*
  for (var i = 0; i < content.length; i++) {
  slides(i);
  $($('.totalsequence')[i]).html(i);
  $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
}
function slides(i){
$($('.totalsequence')[i]).click(function(){
countNext = i;
templateCaller();
templateCaller();
});
}
*/


//call the slide indication bar handler for pink indicators
loadTimelineProgress($total_page, countNext + 1);

// just for development purpose to see total slide vs current slide number
// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
}

/*this countNext variable change here is solely for development phase and
should be commented out for deployment*/
// countNext+=1;

// first call to template caller
templateCaller();

/* navigation buttons event handlers */
templateCaller();

/* navigation buttons event handlers */

$nextBtn.on('click', function() {
  countNext++;
  templateCaller();
});

$refreshBtn.click(function(){
  templateCaller();
});

$prevBtn.on('click', function() {
  countNext--;
  templateCaller();

  /* if footerNotificationHandler pageEndSetNotification was called then on click of
  previous slide button hide the footernotification */
  setTimeout(function(){
    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
  }, 1000);

});
/*=====  End of Templates Controller Block  ======*/
});

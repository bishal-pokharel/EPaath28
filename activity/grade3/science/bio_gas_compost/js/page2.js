var imgpath = $ref + "/images/compost/";
var soundAsset = $ref + "/sounds/" + $lang + "/";

var content = [{
    //page 0
    contentblockadditionalclass: "imagebg3",
    contentnocenteradjust: true,
    uppertextblockadditionalclass: "firsttitle",
    uppertextblock: [{
      textclass: "title",
      textdata: data.string.p2_s1
    }, {
      textclass: "description",
      textdata: data.string.p2_s2
    }, {
      textclass: "description_extra",
      textdata: data.string.p2_s3
    }]
  },
  {
    //page 1
    contentblockadditionalclass: "simplegreenbg",
    contentnocenteradjust: true,
    uppertextblockadditionalclass: "firsttitle",
    uppertextblock: [{
      textclass: "title",
      textdata: data.string.p2_s4
    }],
    imageblockadditionalclass: "white_transparent_img_block",
    imageblock: [{
      extraimageblockclass: "imagecollection position1",
      imagestoshow: [{
        imgclass: "image",
        imgsrc: imgpath + "dry_leaves.png"
      }],
      imagelabels: [{
        imagelabelclass: "imagelabel",
        imagelabeldata: data.string.p2_s5
      }]
    }, {
      extraimageblockclass: "imagecollection position2",
      imagestoshow: [{
        imgclass: "image",
        imgsrc: imgpath + "straw.png"
      }],
      imagelabels: [{
        imagelabelclass: "imagelabel",
        imagelabeldata: data.string.p2_s6
      }]
    }, {
      extraimageblockclass: "imagecollection position3",
      imagestoshow: [{
        imgclass: "image",
        imgsrc: imgpath + "kitchenwaste.png"
      }],
      imagelabels: [{
        imagelabelclass: "imagelabel",
        imagelabeldata: data.string.p2_s7
      }]
    }, {
      extraimageblockclass: "imagecollection position4",
      imagestoshow: [{
        imgclass: "image",
        imgsrc: imgpath + "grass.png"
      }],
      imagelabels: [{
        imagelabelclass: "imagelabel",
        imagelabeldata: data.string.p2_s8
      }]
    }, {
      extraimageblockclass: "imagecollection position5",
      imagestoshow: [{
        imgclass: "image",
        imgsrc: imgpath + "partofaplant.png"
      }],
      imagelabels: [{
        imagelabelclass: "imagelabel",
        imagelabeldata: data.string.p2_s9
      }]
    }]
  },
  {
    //page 2
    contentblockadditionalclass: "imagebg1",
    contentnocenteradjust: true,
    uppertextblockadditionalclass: "firsttitle",
    uppertextblock: [{
      textclass: "title",
      textdata: data.string.p2_s10
    }, {
      textclass: "description2",
      textdata: data.string.p2_s11
    }],
    imageblock: [{
      imagestoshow: [{
        imgclass: "image2 blink",
        imgsrc: imgpath + "mandigging.png"
      }],
      imagelabels: [{
        imagelabelclass: "image2_bg_holder",
        imagelabeldata: "&nbsp"
      }]
    }]
  },
  {
    //page 3
    contentblockadditionalclass: "imagebg2",
    contentnocenteradjust: true,
    uppertextblockadditionalclass: "firsttitle",
    uppertextblock: [{
      textclass: "title",
      textdata: data.string.p2_s10
    }, {
      textclass: "description2",
      textdata: data.string.p2_s12
    }],
    imageblock: [{
      imagestoshow: [{
        imgclass: "image3 blink",
        imgsrc: imgpath + "wastage.png"
      }, {
        imgclass: "image_cutdown_left",
        imgsrc: imgpath + "wastageformising.png"
      }, {
        imgclass: "image_cutdown_right",
        imgsrc: imgpath + "wastageformising.png"
      }]
    }]
  },
  {
    //page 4
    contentblockadditionalclass: "imagebg2",
    contentnocenteradjust: true,
    uppertextblockadditionalclass: "firsttitle",
    uppertextblock: [{
      textclass: "title",
      textdata: data.string.p2_s10
    }, {
      textclass: "description2",
      textdata: data.string.p2_s13
    }],
    imageblock: [{
      imagestoshow: [{
        imgclass: "image4 blink",
        imgsrc: imgpath + "pouring-waste.png"
      }, {
        imgclass: "image_cutdown_left",
        imgsrc: imgpath + "wastageformising.png"
      }, {
        imgclass: "image_cutdown_right",
        imgsrc: imgpath + "wastageformising.png"
      }]
    }]
  },
  {
    //page 5
    contentblockadditionalclass: "imagebg5",
    contentnocenteradjust: true,
    uppertextblockadditionalclass: "firsttitle",
    uppertextblock: [{
      textclass: "title",
      textdata: data.string.p2_s10
    }, {
      textclass: "description2",
      textdata: data.string.p2_s14
    }],
    imageblock: [{
      imagestoshow: [{
        imgclass: "image5 blink",
        imgsrc: imgpath + "pouring_water.png"
      }, {
        imgclass: "image_cutdown_left",
        imgsrc: imgpath + "wastageformising.png"
      }]
    }]
  },
  {
    //page 6
    contentblockadditionalclass: "imagebg5",
    contentnocenteradjust: true,
    uppertextblockadditionalclass: "firsttitle",
    uppertextblock: [{
      textclass: "title",
      textdata: data.string.p2_s10
    }, {
      textclass: "description2",
      textdata: data.string.p2_s15
    }],
    imageblock: [{
      imagestoshow: [{
        imgclass: "image6 blink",
        imgsrc: imgpath + "pouring_mato.png"
      }]
    }]
  },
  {
    //page 7
    contentblockadditionalclass: "imagebg6_1",
    contentnocenteradjust: true,
    uppertextblockadditionalclass: "firsttitle",
    uppertextblock: [{
      textclass: "title",
      textdata: data.string.p2_s10
    }, {
      textclass: "description4 blink",
      textdata: data.string.p2_s16
    }],
    imageblock: [{
      imagestoshow: [{
        imgclass: "image4",
        imgsrc: imgpath + "pouring-waste.png"
      }, {
        imgclass: "image_cutdown_left",
        imgsrc: imgpath + "wastageformising.png"
      }, {
        imgclass: "image_cutdown_right",
        imgsrc: imgpath + "wastageformising.png"
      }, {
        imgclass: "image5",
        imgsrc: imgpath + "pouring_water.png"
      }, {
        imgclass: "image6",
        imgsrc: imgpath + "pouring_mato.png"
      }]
    }]
  },
  {
    //page 8
    contentblockadditionalclass: "imagebg8_a",
    contentnocenteradjust: true,
    uppertextblockadditionalclass: "firsttitle",
    uppertextblock: [{
      textclass: "title",
      textdata: data.string.p2_s10
    }, {
      textclass: "description2",
      textdata: data.string.p2_s17
    }],
    imageblock: [{
      imagestoshow: [{
        imgclass: "image7 blink",
        imgsrc: imgpath + "puttingplastic.png"
      }],
      imagelabels: [{
        imagelabelclass: "image7_bg_holder",
        imagelabeldata: "&nbsp"
      }]
    }]
  },
  {
    //page 9
    contentblockadditionalclass: "imagebg9",
    contentnocenteradjust: true,
    uppertextblockadditionalclass: "firsttitle",
    uppertextblock: [{
      textclass: "title",
      textdata: data.string.p2_s10
    }, {
      textclass: "description2",
      textdata: data.string.p2_s18
    }, {
      textclass: "description",
      textdata: data.string.p2_s19
    }],
    imageblock: [{
      imagestoshow: [{
        imgclass: "image6 blink",
        imgsrc: imgpath + "pouring_mato.png"
      }]
    }]
  },
  {
    //page 10
    contentblockadditionalclass: "imagebg10",
    contentnocenteradjust: true,
    uppertextblockadditionalclass: "firsttitle",
    uppertextblock: [{
      textclass: "title",
      textdata: data.string.p2_s10
    }, {
      textclass: "description",
      textdata: data.string.p2_s21
    }],
    imageblock: [{
      imagestoshow: [{
        imgclass: "image10 blink",
        imgsrc: imgpath + "manputtingmool.png"
      }],
      imagelabels: [{
        imagelabelclass: "image10_bg_holder",
        imagelabeldata: "&nbsp"
      }]
    }]
  }
];


$(function() {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;

  loadTimelineProgress($total_page, countNext + 1);

  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();
  var preload;
  var timeoutvar = null;
  var current_sound;

  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      //images
      // {id: "cover", src: imgpath+"cover_simpleinterest.png", type: createjs.AbstractLoader.IMAGE},

      // sounds
      {
        id: "sound_0",
        src: soundAsset + "p2_s0.ogg"
      },
      {
        id: "sound_1",
        src: soundAsset + "p2_s1.ogg"
      },
      {
        id: "sound_1_1",
        src: soundAsset + "p2_s1_1.ogg"
      },
      {
        id: "sound_1_2",
        src: soundAsset + "p2_s1_2.ogg"
      },
      {
        id: "sound_1_3",
        src: soundAsset + "p2_s1_3.ogg"
      },
      {
        id: "sound_1_4",
        src: soundAsset + "p2_s1_4.ogg"
      },
      {
        id: "sound_1_5",
        src: soundAsset + "p2_s1_5.ogg"
      },
      {
        id: "sound_2",
        src: soundAsset + "p2_s2.ogg"
      },
      {
        id: "sound_3",
        src: soundAsset + "p2_s3.ogg"
      },
      {
        id: "sound_4",
        src: soundAsset + "p2_s4.ogg"
      },
      {
        id: "sound_5",
        src: soundAsset + "p2_s5.ogg"
      },
      {
        id: "sound_6",
        src: soundAsset + "p2_s6.ogg"
      },
      {
        id: "sound_7",
        src: soundAsset + "p2_s7.ogg"
      },
      {
        id: "sound_8",
        src: soundAsset + "p2_s8.ogg"
      },
      {
        id: "sound_9",
        src: soundAsset + "p2_s9.ogg"
      },
      {
        id: "sound_10",
        src: soundAsset + "p2_s10.ogg"
      },
    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound); //for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }

  function handleFileLoad(event) {
    // console.log(event.item);
  }

  function handleProgress(event) {
    $('#loading-text').html(parseInt(event.loaded * 100) + '%');
  }

  function handleComplete(event) {
    $('#loading-wrapper').hide(0);
    //initialize varibales
    current_sound = createjs.Sound.play('sound_1');
    current_sound.stop();
    // call main function
    templateCaller();
  }
  //initialize
  init();

  /*==================================================
   =            Handlers and helpers Block            =
   ==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

  /*===============================================
  =            data highlight function            =
  ===============================================*/
  /**

   What it does:
   - send an element where the function has to see
   for data to highlight
   - this function searches for all nodes whose
   data-highlight element is set to true
   -searches for # character and gives a start tag
   ;span tag here, also for @ character and replaces with
   end tag of the respective
   - if provided with data-highlightcustomclass value for highlight it
   applies the custom class or else uses parsedstring class

   E.g: caller : texthighlight($board);
   */
  function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";
    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
         use that or else use default 'parsedstring'*/
        $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
          (stylerulename = $(this).attr("data-highlightcustomclass")) : (stylerulename = "parsedstring");

        texthighlightstarttag = "<span class='" + stylerulename + "'>";
        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
        $(this).html(replaceinstring);
      });
    }
  }

  /*=====  End of data highlight function  ======*/

  /*===============================================
  =            user notification function        =
  ===============================================*/
  /**
   How to:
   - First set any html element with
   "data-usernotification='notifyuser'" attribute,
   and "data-isclicked = ''".
   - Then call this function to give notification
   */

  /**
   What it does:
   - You send an element where the function has to see
   for data to notify user
   - this function searches for all text nodes whose
   data-usernotification attribute is set to notifyuser
   - applies event handler for each of the html element which
   removes the notification style.
   */
  function notifyuser($notifyinside) {
    //check if $notifyinside is provided
    typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

    /*variable that will store the element(s) to remove notification from*/
    var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
    // if there are any notifications removal required add the event handler
    if ($allnotifications.length > 0) {
      $allnotifications.one('click', function() {
        /* Act on the event */
        $(this).attr('data-isclicked', 'clicked');
        $(this).removeAttr('data-usernotification');
      });
    }
  }

  /*=====  End of user notification function  ======*/

  /*======================================================
  =            Navigation Controller Function            =
  ======================================================*/
  /**
   How To:
   - Just call the navigation controller if it is to be called from except the
   last page of lesson
   - If called from last page set the islastpageflag to true such that
   footernotification is called for continue button to navigate to exercise
   */

  /**
   What it does:
   - If not explicitly overriden the method for navigation button
   controls, it shows the navigation buttons as required,
   according to the total count of pages and the countNext variable
   - If for a general use it can be called from the templateCaller
   function
   - Can be put anywhere in the template function as per the need, if
   so should be taken out from the templateCaller function
   - If the total page number is
   */

  function navigationcontroller(islastpageflag) {
    typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

  }

  /*=====  End of user navigation controller function  ======*/

  /*=====  End of Handlers and helpers Block  ======*/

  /*=======================================
   =            Templates Block            =
   =======================================*/
  /*=================================================
   =            general template function            =
   =================================================*/
  var source = $("#general-template").html();
  var template = Handlebars.compile(source);

  function generalTemplate() {
    var html = template(content[countNext]);
    $board.html(html);


    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    vocabcontroller.findwords(countNext);


    switch (countNext) {
      case 0:
        sound_player('sound_' + countNext);
        break;
      case 1:
        createjs.Sound.stop();
        var current_sound = createjs.Sound.play('sound_1');
        current_sound.play();
        current_sound.on('complete', function() {
          var current_sound = createjs.Sound.play('sound_1_1');
          $('.position1').css({
            'transform': 'scale(1.1)',
            'transition': '.2s'
          });
          current_sound.play();
          current_sound.on('complete', function() {
            $('.position1').css({
              'transform': 'scale(1)',
              'transition': '.2s'
            });
            var current_sound = createjs.Sound.play('sound_1_2');
            $('.position2').css({
              'transform': 'scale(1.1)',
              'transition': '.2s'
            });
            current_sound.play();
            current_sound.on('complete', function() {
              $('.position2').css({
                'transform': 'scale(1)',
                'transition': '.2s'
              });
              var current_sound = createjs.Sound.play('sound_1_3');
              $('.position3').css({
                'transform': 'scale(1.1)',
                'transition': '.2s'
              });
              current_sound.play();
              current_sound.on('complete', function() {
                $('.position3').css({
                  'transform': 'scale(1)',
                  'transition': '.2s'
                });
                var current_sound = createjs.Sound.play('sound_1_4');
                $('.position4').css({
                  'transform': 'scale(1.1)',
                  'transition': '.2s'
                });
                current_sound.play();
                current_sound.on('complete', function() {
                  $('.position4').css({
                    'transform': 'scale(1)',
                    'transition': '.2s'
                  });
                  var current_sound = createjs.Sound.play('sound_1_5');
                  $('.position5').css({
                    'transform': 'scale(1.1)',
                    'transition': '.2s'
                  });
                  current_sound.play();
                  current_sound.on('complete', function() {
                    $('.position5').css({
                      'transform': 'scale(1)',
                      'transition': '.2s'
                    });
                    nav_button_controls(0)
                  });
                });
              });
            });
          });
        });
        break;
      case 2:
        sound_player('sound_' + countNext);
        $(".blink").click(function() {
          createjs.Sound.stop();
          $(this).hide(0);
          $(".image2_bg_holder").show(0);
          $(".description2").hide(0);
          $(".imagebg1").addClass("imagebg1_animate");
          nav_button_controls(2000);
        });
        break;
      case 3:
        sound_player('sound_' + countNext);

        $(".image_cutdown_left, .image_cutdown_right").hide(0);
        $(".blink").click(function() {
          createjs.Sound.stop();
          $(this).addClass("vibrate");
          setTimeout(function() {
            $(".image_cutdown_left, .image_cutdown_right").show(0);
            $(".blink").hide(0);
            nav_button_controls(1000);
          }, 1500);
          $(".description2").hide(0);
        });
        break;
      case 4:
        sound_player('sound_' + countNext);
        $(".blink").click(function() {
          createjs.Sound.stop();
          $(this).addClass("backgroundsprite_img4_1");
          $(".description2").hide(0);
          setTimeout(function() {
            $(".imagebg2").addClass("imagebg5");
            nav_button_controls(1200);
          }, 1000);
        });
        break;
      case 5:
        sound_player('sound_' + countNext);

        $(".blink").click(function() {
          createjs.Sound.stop();
          $(this).addClass("backgroundsprite_img4_2");
          $(".description2").hide(0);
          nav_button_controls(4000);
        });
        break;
      case 6:
        sound_player('sound_' + countNext);

        $(".blink").click(function() {
          createjs.Sound.stop();
          $(this).addClass("backgroundsprite_img4_3");
          $(".description2").hide(0);
          $(".imagebg5").addClass("imagebg6");
          nav_button_controls(4000);
        });
        break;
      case 7:
        sound_player('sound_' + countNext);


        var $image4 = $(".image4").addClass("backgroundsprite_img4_1_add").hide(0);
        var $image5 = $(".image5").addClass("backgroundsprite_img4_2_add").hide(0);
        var $image6 = $(".image6").addClass("backgroundsprite_img4_3_add").hide(0);

        $(".blink").click(function() {
          createjs.Sound.stop();
          $(this).hide(0);
          $(".imagebg6_1").addClass("imagebg7");
          nav_button_controls(16000);
          for (var i = 0; i < 15; i++) {
            if (i % 2 == 0) {
              setTimeout(function() {
                $image5.hide(0);
                $image4.hide(0);
                $image6.show(0);
              }, 1000 * i);
            } else if (i % 3 == 0) {
              setTimeout(function() {
                $image4.hide(0);
                $image6.hide(0);
                $image5.show(0);
              }, 1000 * i);
            } else {
              setTimeout(function() {
                $image6.hide(0);
                $image5.hide(0);
                $image4.show(0);
              }, 1000 * i);
            }
          }

        });
        break;
      case 8:
        sound_player('sound_' + countNext);

        $(".blink").click(function() {
          createjs.Sound.stop();
          $(".image7_bg_holder").addClass("backgroundsprite_img8_1");
          $(this).hide(0);
          $(".description2").hide(0);
          setTimeout(function() {
            $(".imagebg8_a").addClass("imagebg8_b");
            nav_button_controls(200);
          }, 4000);
        });
        break;
      case 9:
        sound_player('sound_' + countNext);

        $(".blink").click(function() {
          createjs.Sound.stop();
          $(this).addClass("backgroundsprite_img4_3_add2");
          $(".description2").hide(0);
          $(".imagebg9").addClass("imagebg9_bg");
          nav_button_controls(5200);
        });
        break;
      case 10:
        $('.description').css({
          'z-index': '999',
          'cursor': 'pointer'
        });
        createjs.Sound.stop();
        current_sound = createjs.Sound.play('sound_' + countNext);
        current_sound.play();
        $(".image10_bg_holder").hide(0);
        $(".blink,.description").click(function() {
          createjs.Sound.stop();
          nav_button_controls(1000);
          $(".image10_bg_holder").show(0);
          $(this).hide(0);
          $(".description2").hide(0);
        });
        break;
      default:
        break;
    }
    //call notifyuser
    // notifyuser($anydiv);


  }

  /*=====  End of Templates Block  ======*/

  /*==================================================
  =            Templates Controller Block            =
  ==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
   Motivation :
   - Make a single function call that handles all the
   template load easier

   How To:
   - Update the template caller with the required templates
   - Call template caller

   What it does:
   - According to value of the Global Variable countNext
   the slide templates are updated
   */
  function nav_button_controls(delay_ms) {
    timeoutvar = setTimeout(function() {
      if (countNext == 0) {
        $nextBtn.show(0);
      } else if (countNext > 0 && countNext == $total_page - 1) {
        $prevBtn.show(0);
        ole.footerNotificationHandler.lessonEndSetNotification();
      } else {
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    }, delay_ms);
  }

  function sound_player(sound_id) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on('complete', function() {
      nav_button_controls(0);
    })
  }

  function templateCaller() {
    /*always hide next and previous navigation button unless
     explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    //navigationcontroller(true);

    // call the template
    generalTemplate();
    /*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page, countNext + 1);

    // just for development purpose to see total slide vs current slide number
    // $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/
  // countNext+=1;

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */

  $nextBtn.on('click', function() {
    countNext++;
    templateCaller();
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on('click', function() {
    countNext--;
    templateCaller();

    /* if footerNotificationHandler pageEndSetNotification was called then on click of
     previous slide button hide the footernotification */
    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
  });

  /*=====  End of Templates Controller Block  ======*/
});

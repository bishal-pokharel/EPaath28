var imgpath = $ref+"/exercise/images/";
if($lang == "en"){
	soundAsset = $ref+"/sound/en/";
}
else if($lang == "np")
{
	soundAsset = $ref+"/sound/np/";
}
var sound_dg1 = new buzz.sound((soundAsset + "ex.ogg"));
var content=[
	//slide 0
	{
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext1,
				option: [
					{
						option_class: "class1",
						option_image_src: imgpath+'q1a.png',
					},
					{
						option_class: "class2",
						option_image_src: imgpath+'q1b.png',
					}],
			}
		]
	},
	//slide 0
	{
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext1,
				option: [
					{
						option_class: "class1",
						option_image_src: imgpath+'q2a.png',
					},
					{
						option_class: "class2",
						option_image_src: imgpath+'q2b.png',
					}],
			}
		]
	},
	//slide 0
	{
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext1,
				option: [
					{
						option_class: "class1",
						option_image_src: imgpath+'q3a.png',
					},
					{
						option_class: "class2",
						option_image_src: imgpath+'q3b.png',
					}],
			}
		]
	},
	//slide 0
	{
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext1,
				option: [
					{
						option_class: "class1",
						option_image_src: imgpath+'q4a.png',
					},
					{
						option_class: "class2",
						option_image_src: imgpath+'q4b.png',
					}],
			}
		]
	},
	//slide 0
	{
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext1,
				option: [
					{
						option_class: "class1",
						option_image_src: imgpath+'q5a.png',
					},
					{
						option_class: "class2",
						option_image_src: imgpath+'q5b.png',
					}],
			}
		]
	},
	//slide 0
	{
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext1,
				option: [
					{
						option_class: "class1",
						option_image_src: imgpath+'q6a.png',
					},
					{
						option_class: "class2",
						option_image_src: imgpath+'q6b.png',
					}],
			}
		]
	},
	//slide 0
	{
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext1,
				option: [
					{
						option_class: "class1",
						option_image_src: imgpath+'q7a.png',
					},
					{
						option_class: "class2",
						option_image_src: imgpath+'q7b.png',
					}],
			}
		]
	},
	//slide 0
	{
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext1,
				option: [
					{
						option_class: "class1",
						option_image_src: imgpath+'q8a.png',
					},
					{
						option_class: "class2",
						option_image_src: imgpath+'q8b.png',
					}],
			}
		]
	},
	//slide 0
	{
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext1,
				option: [
					{
						option_class: "class1",
						option_image_src: imgpath+'q9a.png',
					},
					{
						option_class: "class2",
						option_image_src: imgpath+'q9b.png',
					}],
			}
		]
	},
	//slide 0
	{
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext1,
				option: [
					{
						option_class: "class1",
						option_image_src: imgpath+'q10b.png',
					},
					{
						option_class: "class2",
						option_image_src: imgpath+'q10a.png',
					}],
			}
		]
	},

];
content.shufflearray();

/*remove this for non random questions*/
$(function () {
	var testin = new NumberTemplate();

	var exercise = new template_exercise_mcq_monkey(content, testin, true);
	exercise.create_exercise();
});

function template_exercise_mcq_monkey(content, scoring){
	var $board			= $('.board');
	var $nextBtn		= $("#activity-page-next-btn-enabled");
	var $prevBtn		= $("#activity-page-prev-btn-enabled");
	var countNext		= 0;
	var testin			= new EggTemplate();

	var wrong_clicked 	= false;

	var total_page = content.length;

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);

		// testin.numberOfQuestions();

		//for sound
		if(countNext==0){
			 playaudio(sound_dg1, $(".dg-1"));
		}

		/*for randomizing the options*/
		var option_position = [3,4];
		option_position.shufflearray();
		for(var op=0; op<4; op++){
			$('.main-container').eq(op).addClass('option-pos-'+option_position[op]);
		}
		// //top-left
		// $('.option-pos-1').hover(function(){
		// 	$('.center-sundar').attr('src', 'images/sundar/top-right.png');
		// 	$('.center-sundar').css('transform','scaleX(-1)');
		// }, function(){
		//
		// });
		//bottom-left
		$('.option-pos-3').hover(function(){
			$('.center-sundar').attr('src', 'images/sundar/bottom-right.png');
			$('.center-sundar').css('transform','scaleX(-1)');
		}, function(){

		});
		// //top-right
		// $('.option-pos-2').hover(function(){
		// 	$('.center-sundar').attr('src', 'images/sundar/top-right.png');
		// 	$('.center-sundar').css('transform','none');
		// }, function(){
		//
		// });
		//bottom-right
		$('.option-pos-4').hover(function(){
			$('.center-sundar').attr('src', 'images/sundar/bottom-right.png');
			$('.center-sundar').css('transform','none');
		}, function(){

		});


		var wrong_clicked = 0;
		var correct_images = ['correct-1.png', 'correct-2.png', 'correct-3.png'];
		$(".option-container").click(function(){
			if($(this).hasClass("class1")){
				if(wrong_clicked<1){
					testin.update(true);
				}
				var rand_img = Math.floor(Math.random()*correct_images.length);
				$('.option-pos-1, .option-pos-2, .option-pos-3, .option-pos-4').off('mouseenter mouseleave');
				$('.center-sundar').attr('src', 'images/sundar/'+correct_images[rand_img]);
				$(".option-container").css('pointer-events','none');
	 			play_correct_incorrect_sound(1);
				$(this).addClass('correct-ans');
				$(this).parent().children('.correct-icon').show(0);
				wrong_clicked = 0;
				if(countNext != total_page)
					$nextBtn.show(0);
			}
			else{
				var classname_monkey = $(this).parent().attr('class').replace(/main-container/, '');
				classname_monkey = classname_monkey.replace(/ /g, '');
				$('.'+classname_monkey).off('mouseenter mouseleave');
				if(wrong_clicked==0){
					$('.center-sundar').attr('src', 'images/sundar/incorrect-1.png');
				} else if(wrong_clicked == 1){
					$('.center-sundar').attr('src', 'images/sundar/incorrect-2.png');
				} else {
					$('.center-sundar').attr('src', 'images/sundar/incorrect-3.png');
				}
				testin.update(false);
	 			play_correct_incorrect_sound(0);
				$(this).addClass('incorrect-ans');
				$(this).parent().children('.incorrect-icon').show(0);
				wrong_clicked++;
			}
		});
	};

	    	function playaudio(sound_data, $dialog_container){
	    			var playing = true;
	    			$dialog_container.removeClass("playable");
	    			$dialog_container.click(function(){
	    				if(!playing){
	    					playaudio(sound_data, $dialog_container);
	    				}
	    				return false;
	    			});
	    			$prevBtn.hide(0);
	    			if((countNext+1) == content.length){
	    				ole.footerNotificationHandler.hideNotification();
	    			}else{
	    				$nextBtn.hide(0);
	    			}
	    			sound_data.play();
	    			sound_data.bind('ended', function(){
	    				setTimeout(function(){
	    					if(countNext != 0)
	    					$prevBtn.show(0);
	    					$dialog_container.addClass("playable");
	    					playing = false;
	    					sound_data.unbind('ended');

	    				
	    				});
	    			});
	    		}

	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	};

	this.create_exercise = function(){
		if(typeof scoring != 'undefined'){
			testin = scoring;
		}
	 	testin.init(total_page);

		templateCaller();
		$nextBtn.on("click", function(){
			countNext++;
			testin.gotoNext();
			templateCaller();
		});
		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			countNext--;
			templateCaller();
		});
	};
}

var imgpath = $ref+"/images/";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
if($lang == "en"){
	soundAsset = $ref+"/sound/en/";
}
else if($lang == "np")
{
	soundAsset = $ref+"/sound/np/";
}
var sound_dg1 = new buzz.sound((soundAsset + "p2_s0.ogg"));
var sound_dg2 = new buzz.sound((soundAsset + "p2_s1.ogg"));
var sound_dg3 = new buzz.sound((soundAsset + "p2_s8.ogg"));
var sound_dg4 = new buzz.sound((soundAsset + "p2_s9.ogg"));
var sound_dg5 = new buzz.sound((soundAsset + "p2_s10.ogg"));
var content=[
 {
  	//start
    additionalclasscontentblock: "ole-background-gradient-seaweed",
  	uppertextblockadditionalclass: "bigtextcenter",
  	uppertextblock: [{
  		textdata: data.string.p2_s1,
      textclass: "DIYheading_text"
  	}],
    imageblock:[
      {
        imagetoshow:[
          {
            imgclass: "diycoverpage",
            imgsrc: imgpath + "diy_cover.png"
          }
        ]
      }
    ]
  },

  {
  	//page 1
    additionalclasscontentblock: "ole-backrgound-gradient-weepysky backgrd",
  	uppertextblock: [{
  		textclass: "topheader1",
  		textdata: data.string.p2_s2
  	}],
    imageblock:[
      {
        imagetoshow:[
          {
            imgclass: "midimage",
            imgsrc: imgpath + "raju01.png"
          }
        ]
      }
    ]
  },

 {
    //page 2
    additionalclasscontentblock: "ole-backrgound-gradient-weepysky backgrd",
    uppertextblock: [{
      textclass: "topheader",
      textdata: data.string.p2_s26
    },{
      textclass: "question",
      textdata: data.string.p2_s3
    }],
      exeoptions:[{
      optclass:"correct",
      forshuffle: "",
      optdata: data.string.p2_s4
    },
    {
      optclass:"",
      forshuffle: "",
      optdata: data.string.p2_s4_a
    }],
    imageblock:[
      {
        imagetoshow:[
          {
            imgclass: "rightimage",
            imgsrc: imgpath + "raju01.png"
          }
        ]
      }
    ]
  },

  {
    //page 3
    additionalclasscontentblock: "ole-backrgound-gradient-weepysky backgrd",
    uppertextblock: [{
      textclass: "topheader",
      textdata: data.string.p2_s26
    },{
      textclass: "question",
      textdata: data.string.p2_s5
    }],
      exeoptions:[{
      optclass:"correct",
      forshuffle: "",
      optdata: data.string.p2_s6
    },
    {
      optclass:"",
      forshuffle: "",
      optdata: data.string.p2_s7
    }],
    imageblock:[
      {
        imagetoshow:[
          {
            imgclass: "rightimage",
            imgsrc: imgpath + "raju01.png"
          }
        ]
      }
    ]
  },


 {
    //page 4
    additionalclasscontentblock: "ole-backrgound-gradient-weepysky backgrd",
    uppertextblock: [{
      textclass: "topheader",
      textdata: data.string.p2_s26
    },{
      textclass: "question",
      textdata: data.string.p2_s8
    }],
      exeoptions:[{
      optclass:"",
      forshuffle: "",
      optdata: data.string.p2_s10
    },
    {
      optclass:"correct",
      forshuffle: "",
      optdata: data.string.p2_s9
    }],
    imageblock:[
      {
        imagetoshow:[
          {
            imgclass: "rightimage",
            imgsrc: imgpath + "raju01.png"
          }
        ]
      }
    ]
  },


  {
    //page 5
    additionalclasscontentblock: "ole-backrgound-gradient-weepysky backgrd",
    uppertextblock: [{
      textclass: "topheader",
      textdata: data.string.p2_s26
    },{
      textclass: "question",
      textdata: data.string.p2_s11
    }],
      exeoptions:[{
      optclass:"correct",
      forshuffle: "",
      optdata: data.string.p2_s12
    },
    {
      optclass:"",
      forshuffle: "",
      optdata: data.string.p2_s13
    }],
    imageblock:[
      {
        imagetoshow:[
          {
            imgclass: "rightimage",
            imgsrc: imgpath + "raju01.png"
          }
        ]
      }
    ]
  },

  {
    //page 6
    additionalclasscontentblock: "ole-backrgound-gradient-weepysky backgrd",
    uppertextblock: [{
      textclass: "topheader",
      textdata: data.string.p2_s26
    },{
      textclass: "question",
      textdata: data.string.p2_s14
    }],
      exeoptions:[{
      optclass:"correct",
      forshuffle: "",
      optdata: data.string.p2_s15
    },
    {
      optclass:"",
      forshuffle: "",
      optdata: data.string.p2_s16
    }],
    imageblock:[
      {
        imagetoshow:[
          {
            imgclass: "rightimage",
            imgsrc: imgpath + "raju01.png"
          }
        ]
      }
    ]
  },
{
    //page 7
    additionalclasscontentblock: "ole-backrgound-gradient-weepysky backgrd",
    uppertextblock: [{
      textclass: "topheader",
      textdata: data.string.p2_s26
    },{
      textclass: "question",
      textdata: data.string.p2_s14a
    }],
      exeoptions:[{
      optclass:"correct",
      forshuffle: "",
      optdata: data.string.p2_s15a
    },
    {
      optclass:"",
      forshuffle: "",
      optdata: data.string.p2_s16a
    }],
    imageblock:[
      {
        imagetoshow:[
          {
            imgclass: "rightimage",
            imgsrc: imgpath + "raju01.png"
          }
        ]
      }
    ]
  },

  {
    //page 8
    additionalclasscontentblock: "ole-backrgound-gradient-weepysky backgrd",
    imageblockadditionalclass:"topchanged",
    uppertextblock: [{
      textclass: "topheadertype1",
      textdata: data.string.p2_s17
    }],
    imageblock:[
      {
        imagetoshow:[
          {
            imgclass: "centerimage",
            imgsrc: imgpath + "raju01.png"
          },
          {
            imgclass: "image1 borderedimage",
            imgsrc: imgpath + "hand.png"
          },
          {
            imgclass: "image2 borderedimage",
            imgsrc: imgpath + "shower1.png"
          },
          {
            imgclass: "image3 borderedimage",
            imgsrc: imgpath + "cloth1.png"
          },
          {
            imgclass: "image4 borderedimage",
            imgsrc: imgpath + "feet1.png"
          },
          {
            imgclass: "image5 borderedimage",
            imgsrc: imgpath + "nose.png"
          },
          {
            imgclass: "image6 borderedimage",
            imgsrc: imgpath + "teeth.png"
          }],
          imagelabels:[{
            imagelabelclass: "image11 centertext",
            imagelabeldata: data.string.p2_s18
          },{
            imagelabelclass: "image21 centertext",
            imagelabeldata: data.string.p2_s19
          },{
            imagelabelclass: "image31 centertext",
            imagelabeldata: data.string.p2_s20
          },{
            imagelabelclass: "image41 centertext",
            imagelabeldata: data.string.p2_s21
          },{
            imagelabelclass: "image51 centertext",
            imagelabeldata: data.string.p2_s22
          },{
            imagelabelclass: "image61 centertext",
            imagelabeldata: data.string.p2_s23
          }]
      }]
  },

{
    //page 9
    additionalclasscontentblock: "ole-backrgound-gradient-weepysky backgrd",
    imageblockadditionalclass:"topchanged",
    uppertextblock: [
    {
      textclass: "topheadertype1 fadein",
      textdata: data.string.p2_s24
    }],
    imageblock:[
      {
        imagetoshow:[
          {
            imgclass: "centerimage fadein1",
            imgsrc: imgpath + "raju02.png"
          },
          {
            imgclass: "centerimage fadeout",
            imgsrc: imgpath + "raju01.png"
          }
      ]
    }]
  },

  {
    //page 10
    additionalclasscontentblock: "ole-backrgound-gradient-weepysky backgrd",
    imageblockadditionalclass:"topchanged",
    uppertextblock: [
    {
      textclass: "topheadertype1 fadein",
      textdata: data.string.p2_s25
    }],
    imageblock:[
      {
        imagetoshow:[
          {
            imgclass: "centerimage fadein1",
            imgsrc: imgpath + "raju03.png"
          },
          {
            imgclass: "centerimage fadeout",
            imgsrc: imgpath + "raju02.png"
          }
      ]
    }]
  },
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;
  var vocabcontroller =  new Vocabulary();
  vocabcontroller.init();

  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
    /*=====  End of data highlight function  ======*/

    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.delay(800).show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true


		}
	}
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */


    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  	var animalarray =[];
   	var plantarray = [];



  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $nextBtn.hide(0);
    $board.html(html);
    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
	  vocabcontroller.findwords(countNext);

    		/*for randomizing the options*/
    		var parent = $(".optionsdiv");
    		var divs = parent.children();
    			 while (divs.length) {
    			        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
    			  }


        $(".optionscontainer").click(function(){
    					if($(this).hasClass("correct")){
    						play_correct_incorrect_sound(1);
    						$(this).children(".corctopt").show(0);
              	$(this).addClass('corrects');
                $(" .optionscontainer").css("pointer-events","none");
                $nextBtn.show(0);
                $prevBtn.show(0);
    					}
    					  else{
    						play_correct_incorrect_sound(0);
                $(this).addClass('incorrect');
                $(this).css("pointer-events","none");
    						$(this).children(".wrngopt").show(0);

    				}
    			});

    switch(countNext){
      case 0:
      play_diy_audio();
      setTimeout(function(){
              $nextBtn.show(0);
            }, 2000);
      break;

      case 1:
      playaudio(sound_dg1, $(".dg-1"));
      break;
      case 2:
			playaudio(sound_dg2, $(".dg-1"));
      break;

      case 3:
      break;

      case 4:
      break;

      case 5:
      break;

      case 6:
      break;

      case 7:
      break;

      case 8:
			setTimeout(function(){
							playaudio(sound_dg3, $(".dg-1"));
				}, 3000);

        break;

      case 9:
      playaudio(sound_dg4, $(".dg-1"));
        break;

      case 10:
			playaudio(sound_dg5, $(".dg-1"));
        break;
    }
  }

    function playaudio(sound_data, $dialog_container){
        var playing = true;
        current_sound = sound_data;
        $dialog_container.removeClass("playable");
        $dialog_container.click(function(){
          if(!playing){
            playaudio(sound_data, $dialog_container);
          }
          return false;
        });
        $prevBtn.hide(0);
        if((countNext+1) == content.length){
          ole.footerNotificationHandler.hideNotification();
        }else{
          $nextBtn.hide(0);
        }
        sound_data.play();
        sound_data.bind('ended', function(){
          setTimeout(function(){
            if(countNext != 0)
            $prevBtn.show(0);
            $dialog_container.addClass("playable");
            playing = false;
            sound_data.unbind('ended');
          	if(countNext==2){
							$nextBtn.hide(0);
							$prevBtn.hide(0);
						}else if(countNext==10){
							 ole.footerNotificationHandler.lessonEndSetNotification();
							 $prevBtn.show(0);
						}else{
							$prevBtn.show(0);
							$nextBtn.show(0);
						}
          });
        });
      }

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

 	function templateCaller() {
			/*always hide next and previous navigation button unless
			 explicitly called from inside a template*/
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// call navigation controller
			navigationcontroller();

      loadTimelineProgress($total_page, countNext + 1);
			// call the template
			generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


			//call the slide indication bar handler for pink indicators

			// just for development purpose to see total slide vs current slide number
			// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/
  // countNext+=1;

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */
 	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref+"/images/";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
if($lang == "en"){
	soundAsset = $ref+"/sound/en/";
}
else if($lang == "np")
{
	soundAsset = $ref+"/sound/np/";
}

var sound_dg1 = new buzz.sound((soundAsset + "p1_s0.ogg"));
var sound_dg2 = new buzz.sound((soundAsset + "p1_s1.ogg"));
var sound_dg3 = new buzz.sound((soundAsset + "p1_s2.ogg"));
var sound_dg4 = new buzz.sound((soundAsset + "p1_s3.ogg"));
var sound_dg5 = new buzz.sound((soundAsset + "p1_s4.ogg"));
var sound_dg6 = new buzz.sound((soundAsset + "p1_s5.ogg"));
var sound_dg7 = new buzz.sound((soundAsset + "p1_s6.ogg"));
var sound_dg8 = new buzz.sound((soundAsset + "p1_s7.ogg"));
var sound_dg9 = new buzz.sound((soundAsset + "p1_s8.ogg"));
var sound_dg10 = new buzz.sound((soundAsset + "p1_s9.ogg"));
var sound_dg11 = new buzz.sound((soundAsset + "p1_s10.ogg"));
var sound_dgteeth = new buzz.sound((soundAsset + "p1_s11_Teeth.ogg"));
var sound_dgnail = new buzz.sound((soundAsset + "p1_s11_Nails.ogg"));
var sound_dgbody = new buzz.sound((soundAsset + "p1_s11_Body.ogg"));
var sound_dgcloth = new buzz.sound((soundAsset + "p1_s11_Clothes.ogg"));
var sound_dgfeet = new buzz.sound((soundAsset + "p1_s11_Feet.ogg"));
var sound_dghand = new buzz.sound((soundAsset + "p1_s11_Hands.ogg"));

var content=[
 {
  	//start
    additionalclasscontentblock: "ole-background-gradient-weepysky",
  	contentnocenteradjust: true,
  	uppertextblockadditionalclass: "bigtextcenter fadein1",
  	uppertextblock: [{
  		textdata: data.lesson.chapter,
      textclass:'title_text'
  	}],
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "coverpg",
						imgsrc: imgpath + "cover_page.png"
					}
				]
			}
		]
  },

  {
  	//page 1
  	additionalclasscontentblock: "firstpagebg fadein1",
  	uppertextblock: [{
  		textclass: "lefttext",
  		textdata: data.string.p1_s1
  	}]
  },

  {
  	//page 2
    additionalclasscontentblock: "ole-backrgound-gradient-weepysky backgrd",
  	uppertextblock: [{
  		textclass: "toptext fadein1",
  		textdata: data.string.p1_s2
  	}],
    imageblock:[
      {
        imagetoshow:[
          {
            imgclass: "bigimage",
            imgsrc: imgpath + "02.png"
          }
        ]
      }
    ]
  },

  {
    //page 3
    additionalclasscontentblock: "ole-backrgound-gradient-weepysky backgrd",
    uppertextblock: [{
      textclass: "toptext fadein1",
      textdata: data.string.p1_s3
    },
    {
      textclass: "bottomtext fadein1",
      textdata: data.string.p1_s4
    }],
    imageblock:[
      {
        imagetoshow:[
          {
            imgclass: "midimage",
            imgsrc: imgpath + "03.png"
          }
        ]
      }
    ]
  },

 {
    //page 4
    additionalclasscontentblock: "ole-backrgound-gradient-weepysky backgrd",
    uppertextblock: [{
      textclass: "toptexttype1 fadein1",
      textdata: data.string.p1_s5
    }],
    imageblock:[
      {
        imagetoshow:[
          {
            imgclass: "bigimage",
            imgsrc: imgpath + "04.png"
          }
        ]
      }
    ]
  },

  {
    //page 5
    additionalclasscontentblock: "ole-backrgound-gradient-weepysky backgrd",
    imageblockadditionalclass:"topchanged",
    uppertextblock: [{
      textclass: "topheader",
      textdata: data.string.p1_s6
    }],
    imageblock:[
      {
        imagetoshow:[
          {
            imgclass: "centerimage",
            imgsrc: imgpath + "boy.png"
          },
          {
            imgclass: "image1 borderedimage",
            imgsrc: imgpath + "comb.png"
          },
          {
            imgclass: "image2 borderedimage",
            imgsrc: imgpath + "nail-cutter.png"
          },
          {
            imgclass: "image3 borderedimage",
            imgsrc: imgpath + "toothpaste.png"
          },
          {
            imgclass: "image4 borderedimage",
            imgsrc: imgpath + "shoes-brush.png"
          },
          {
            imgclass: "image5 borderedimage",
            imgsrc: imgpath + "soap.png"
          },
          {
            imgclass: "image6 borderedimage",
            imgsrc: imgpath + "hankey.png"
          }
        ]
      }
    ]
  },

  {
    //page 6
    additionalclasscontentblock: "ole-backrgound-gradient-weepysky backgrd",
    imageblockadditionalclass:"topchanged",
    uppertextblock: [{
      textclass: "topheadertype1",
      textdata: data.string.p1_s7
    }],
    imageblock:[
      {
        imagetoshow:[
          {
            imgclass: "centerimage",
            imgsrc: imgpath + "boy.png"
          },
          {
            imgclass: "imagea borderedimage",
            imgsrc: imgpath + "germs01.png"
          },
          {
            imgclass: "imageb borderedimage",
            imgsrc: imgpath + "germs02.png"
          },
          {
            imgclass: "imagec borderedimage",
            imgsrc: imgpath + "germs03.png"
          },
          {
            imgclass: "imaged borderedimage",
            imgsrc: imgpath + "germs04.png"
          },
          {
            imgclass: "imagee borderedimage",
            imgsrc: imgpath + "germs05.png"
          }
        ]
      }
    ]
  },

 {
    //page 7
    additionalclasscontentblock: "ole-backrgound-gradient-weepysky backgrd",
    imageblockadditionalclass:"topchanged",
    uppertextblock: [{
      textclass: "topheadertype1",
      textdata: data.string.p1_s8
    }],
    imageblock:[
      {
        imagetoshow:[
          {
            imgclass: "centerimage1",
            imgsrc: imgpath + "boy.png"
          },
          {
            imgclass: "image1 borderedimage",
            imgsrc: imgpath + "comb.png"
          },
          {
            imgclass: "image2 borderedimage",
            imgsrc: imgpath + "nail-cutter.png"
          },
          {
            imgclass: "image3 borderedimage",
            imgsrc: imgpath + "toothpaste.png"
          },
          {
            imgclass: "image4 borderedimage",
            imgsrc: imgpath + "shoes-brush.png"
          },
          {
            imgclass: "image5 borderedimage",
            imgsrc: imgpath + "soap.png"
          },
          {
            imgclass: "image6 borderedimage",
            imgsrc: imgpath + "hankey.png"
          },
          {
            imgclass: "imagea1",
            imgsrc: imgpath + "germs01.png"
          },
          {
            imgclass: "imageb1",
            imgsrc: imgpath + "germs02.png"
          },
          {
            imgclass: "imagec1",
            imgsrc: imgpath + "germs03.png"
          },
          {
            imgclass: "imaged1",
            imgsrc: imgpath + "germs04.png"
          },
          {
            imgclass: "imagee1",
            imgsrc: imgpath + "germs05.png"
          }

        ]
      }
    ]
  },

{
    //page 7
    additionalclasscontentblock: "ole-backrgound-gradient-weepysky backgrd",
    uppertextblock: [{
      textclass: "topheadertype2",
      textdata: data.string.p1_s9
    },
    {
      textclass: "midtexttype1",
      textdata: data.string.p1_s10
    }],
    imageblock:[
      {
        imagetoshow:[
          {
            imgclass: "girl",
            imgsrc: imgpath + "sabina.png"
          }
        ]
      }
    ]
  },

  {
    //page 9
    additionalclasscontentblock: "ole-backrgound-gradient-weepysky backgrd",
    imageblockadditionalclass:"topchanged",
    uppertextblock: [{
      textclass: "topheadertype1",
      textdata: data.string.p1_s10_1
    }],
    imageblock:[
      {
        imagetoshow:[
          {
            imgclass: "centerimage",
            imgsrc: imgpath + "boy.png"
          },
          {
            imgclass: "image1 borderedimage",
            imgsrc: imgpath + "hand.png"
          },
          {
            imgclass: "image2 borderedimage",
            imgsrc: imgpath + "eye.png"
          },
          {
            imgclass: "image3 borderedimage",
            imgsrc: imgpath + "nose.png"
          },
          {
            imgclass: "image4 borderedimage",
            imgsrc: imgpath + "clean-dress-01.png"
          },
          {
            imgclass: "image5 borderedimage",
            imgsrc: imgpath + "teeth.png"
          },
          {
            imgclass: "image6 borderedimage",
            imgsrc: imgpath + "nail.png"
          }],
          imagelabels:[{
            imagelabelclass: "image11 centertext",
            imagelabeldata: data.string.p1_s13
          },{
            imagelabelclass: "image21 centertext",
            imagelabeldata: data.string.p1_s14
          },{
            imagelabelclass: "image31 centertext",
            imagelabeldata: data.string.p1_s15
          },{
            imagelabelclass: "image41 centertext",
            imagelabeldata: data.string.p1_s16
          },{
            imagelabelclass: "image51 centertext",
            imagelabeldata: data.string.p1_s17
          },{
            imagelabelclass: "image61 centertext",
            imagelabeldata: data.string.p1_s18
          }]
      }]
  },

 {
    //page 10
    additionalclasscontentblock: "ole-backrgound-gradient-weepysky backgrd",
    imageblockadditionalclass:"topchanged",
    uppertextblock: [{
      textclass: "topheadertype1",
      textdata: data.string.p1_s33
    },
    {
      textclass: "clickinstruction",
      textdata: data.string.p1_s34
    }],
    imageblock:[
      {
          imagelabels:[{
            imagelabelclass: "image12 centertext4",
            imagelabeldata: data.string.p1_s35
          },{
            imagelabelclass: "image22 centertext4",
            imagelabeldata: data.string.p1_s36
          },{
            imagelabelclass: "image32 centertext4",
            imagelabeldata: data.string.p1_s40
          },{
            imagelabelclass: "image42 centertext4",
            imagelabeldata: data.string.p1_s37
          },{
            imagelabelclass: "image52 centertext4",
            imagelabeldata: data.string.p1_s39
          },{
            imagelabelclass: "image62 centertext4",
            imagelabeldata: data.string.p1_s38
          }]
      }],

      popups:[{
        popupclass:"popup1",
        imgclass1:'imagecls1',
        imgclass2:'imagecls2',
        textclass1:'text1',
        textclass2:'text2',
        textdata1: data.string.p1_s19,
        imgsrc1: imgpath + "hand.png",
        imgsrc2: imgpath + "white_wrong.png",
        textdata2: data.string.p1_s21
      },
      {
        popupclass:"popup2",
        imgclass1:'imagecls1',
        imgclass2:'imagecls2',
        textclass1:'text1',
        textclass2:'text2',
        textdata1: data.string.p1_s22,
        imgsrc1: imgpath + "brushing-teeth.png",
        imgsrc2: imgpath + "white_wrong.png",
        textdata2: data.string.p1_s23
      },
      {
        popupclass:"popup3",
        imgclass1:'imagecls1',
        imgclass2:'imagecls2',
        textclass1:'text1',
        textclass2:'text2',
        textdata1: data.string.p1_s24,
        imgsrc1: imgpath + "washing-foot.png",
        imgsrc2: imgpath + "white_wrong.png",
        textdata2: data.string.p1_s25,
      },
      {
        popupclass:"popup4",
        imgclass1:'imagecls1',
        imgclass2:'imagecls2',
        textclass1:'text1',
        textclass2:'text2',
        textdata1: data.string.p1_s26a,
        imgsrc1: imgpath + "nail.png",
        imgsrc2: imgpath + "white_wrong.png",
        textdata2: data.string.p1_s26,
      },
      {
        popupclass:"popup5",
        imgclass1:'imagecls1',
        imgclass2:'imagecls2',
        textclass1:'text1',
        textclass2:'text2',
        textdata1: data.string.p1_s27,
        imgsrc1: imgpath + "taking-bath.png",
        imgsrc2: imgpath + "white_wrong.png",
        textdata2: data.string.p1_s28,
      },
      {
        popupclass:"popup6",
        imgclass1:'imagecls1',
        imgclass2:'imagecls2',
        textclass1:'text1',
        textclass2:'text2',
        textdata1: data.string.p1_s29,
        imgsrc1: imgpath + "drying-clothers.png",
        imgsrc2: imgpath + "white_wrong.png",
        textdata2: data.string.p1_s30,
      }]

  }
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;
  var vocabcontroller =  new Vocabulary();
  vocabcontroller.init();
	var current_sound =sound_dg1;
  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
    /*=====  End of data highlight function  ======*/

    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.delay(800).show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true


		}
	}
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */


    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  	var animalarray =[];
   	var plantarray = [];



  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $nextBtn.hide(0);
    $board.html(html);
    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
	  vocabcontroller.findwords(countNext);


    switch(countNext){
      case 0:
      playaudio(sound_dg1, $(".dg-1"));
      break;

      case 1:
			playaudio(sound_dg2, $(".dg-1"));
      break;

      case 2:
			playaudio(sound_dg3, $(".dg-1"));
      break;

      case 3:
			playaudio(sound_dg4, $(".dg-1"));
      break;


      case 4:
			playaudio(sound_dg5, $(".dg-1"));
      break;

      case 5:
			playaudio(sound_dg6, $(".dg-1"));
      break;


      case 6:

			playaudio(sound_dg7, $(".dg-1"));
      break;

      case 7:
			playaudio(sound_dg8, $(".dg-1"));
        break;

      case 8:
			playaudio(sound_dg9, $(".dg-1"));
        break;

      case 9:
			playaudio(sound_dg10, $(".dg-1"));
        break;

      case 10:

			playaudio(sound_dg11, $(".dg-1"));
      var counter = 1;


        $('.image12').click(
        function(){
          $('.popup1').removeClass('zoomout').addClass('zoomin increasezindex');
          $(this).addClass('whitebackground');
          checkcounter();
          console.log(counter);
						playaudio(sound_dghand, $(".dg-1"));
        });


       $('.image22').click(
        function(){
          $('.popup4').removeClass('zoomout').addClass('zoomin increasezindex');
          $(this).addClass('whitebackground');
          checkcounter();
					playaudio(sound_dgnail, $(".dg-1"));
          console.log(counter);
        });


        $('.image32').click(
        function(){
          $('.popup3').removeClass('zoomout').addClass('zoomin increasezindex');
          $(this).addClass('whitebackground');
          checkcounter();
					playaudio(sound_dgfeet, $(".dg-1"));
          console.log(counter);
        });


         $('.image42').click(
        function(){
          $('.popup2').removeClass('zoomout').addClass('zoomin increasezindex');
          $(this).addClass('whitebackground');
          checkcounter();
						playaudio(sound_dgteeth, $(".dg-1"));
          console.log(counter);
        });


          $('.image52').click(
        function(){
          $('.popup5').removeClass('zoomout').addClass('zoomin increasezindex');
          $(this).addClass('whitebackground');
          checkcounter();
						playaudio(sound_dgbody, $(".dg-1"));
          console.log(counter);
        });


           $('.image62').click(
        function(){
          $('.popup6').removeClass('zoomout').addClass('zoomin increasezindex');
          $(this).addClass('whitebackground');
          checkcounter();
					playaudio(sound_dgcloth, $(".dg-1"));
          console.log(counter);
        });


       $('.imagecls2').click(
        function(){
					current_sound.stop();
          $('.popup1,.popup2,.popup3,.popup4,.popup5,.popup6').removeClass('zoomin increasezindex').addClass('zoomout');

				});

       break;
    }


    	function playaudio(sound_data, $dialog_container){
    			var playing = true;
					current_sound = sound_data;
    			$dialog_container.removeClass("playable");
    			$dialog_container.click(function(){
    				if(!playing){
    					playaudio(sound_data, $dialog_container);
    				}
    				return false;
    			});
    			$prevBtn.hide(0);
    			if((countNext+1) == content.length){
    				ole.footerNotificationHandler.hideNotification();
    			}else{
    				$nextBtn.hide(0);
    			}
    			sound_data.play();
    			sound_data.bind('ended', function(){
    				setTimeout(function(){
    					if(countNext != 0)
    					$prevBtn.show(0);
    					$dialog_container.addClass("playable");
    					playing = false;
    					sound_data.unbind('ended');
    					if((countNext+1) == content.length){
    						// ole.footerNotificationHandler.pageEndSetNotification();
    					}else{
								switch(countNext){
						      case 5:
									setTimeout(function(){
						              $nextBtn.show(0);
												}, 5000);
						      break;
						      case 6:
									setTimeout(function(){
						              $nextBtn.show(0);
												}, 4000);
						      break;
						      case 7:
									setTimeout(function(){
						              $nextBtn.show(0);
												}, 5000);
						        break;
						      case 9:
						        setTimeout(function(){
						                $nextBtn.show(0);
													}, 5000);
						        break;
										case 10:
										setTimeout(function(){
										ole.footerNotificationHandler.hideNotification();
									}, 10000);
									default:
													$nextBtn.show(0);
									}
    					}
    				});
    			});
    		}
    // Last page counter check
    function checkcounter() {
        counter = counter + 1;
        if(counter>6)
        {
            setTimeout(function(){
									$prevBtn.show(0);
                ole.footerNotificationHandler.pageEndSetNotification();
							}, 10000);
             }
        }

    }


/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

 	function templateCaller() {
			/*always hide next and previous navigation button unless
			 explicitly called from inside a template*/
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// call navigation controller
			navigationcontroller();

      loadTimelineProgress($total_page, countNext + 1);
			// call the template
			generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


			//call the slide indication bar handler for pink indicators

			// just for development purpose to see total slide vs current slide number
			// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/
  // countNext+=1;

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */
 	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

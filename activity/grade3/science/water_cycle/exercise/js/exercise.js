var imgpath = $ref+"/exercise/images/";

var content=[
	//slide 0	
	{
		contentblockadditionalclass:'',
		extratextblock: [{
			textdata: data.string.einstruction,
			textclass: 'instruction-new'
		}],
		exerciseblock: [
			{					
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext1,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.e1ansa,
					},
					{
						option_class: "class2",
						optiondata: data.string.e1ansb,
					},
					{
						option_class: "class3",
						optiondata: data.string.e1ansc, 
					},
					{
						option_class: "class4",
						optiondata: data.string.e1ansd, 
					}],
			} 
		] 
	},
	//slide 1	
	{
		extratextblock: [{
			textdata: data.string.einstruction,
			textclass: 'instruction-new'
		}],
		exerciseblock: [
			{					
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext2,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.e2ansa,
					},
					{
						option_class: "class2",
						optiondata: data.string.e2ansb,
					},
					{
						option_class: "class3",
						optiondata: data.string.e2ansc, 
					},
					{
						option_class: "class4",
						optiondata: data.string.e2ansd, 
					}],
			} 
		] 
	},
	//slide 2
	{
		extratextblock: [{
			textdata: data.string.einstruction,
			textclass: 'instruction-new'
		}],
		exerciseblock: [
			{					
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext3,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.e3ansa,
					},
					{
						option_class: "class2",
						optiondata: data.string.e3ansb,
					},
					{
						option_class: "class3",
						optiondata: data.string.e3ansc, 
					},
					{
						option_class: "class4",
						optiondata: data.string.e3ansd, 
					}],
			} 
		] 
	},
	//slide 3
	{
		extratextblock: [{
			textdata: data.string.einstruction,
			textclass: 'instruction-new'
		}],
		exerciseblock: [
			{					
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext4,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.e4ansa,
					},
					{
						option_class: "class2",
						optiondata: data.string.e4ansb,
					},
					{
						option_class: "class3",
						optiondata: data.string.e4ansc, 
					},
					{
						option_class: "class4",
						optiondata: data.string.e4ansd, 
					}],
			} 
		] 
	},
	//slide 4
	{
		extratextblock: [{
			textdata: data.string.einstruction,
			textclass: 'instruction-new'
		}],
		exerciseblock: [
			{					
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext5,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.e5ansa,
					},
					{
						option_class: "class2",
						optiondata: data.string.e5ansb,
					},
					{
						option_class: "class3",
						optiondata: data.string.e5ansc, 
					},
					{
						option_class: "class4",
						optiondata: data.string.e5ansd, 
					}],
			} 
		] 
	},
	//slide 5
	{
		extratextblock: [{
			textdata: data.string.einstruction,
			textclass: 'instruction-new'
		}],
		exerciseblock: [
			{					
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext6,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.e6ansa,
					},
					{
						option_class: "class2",
						optiondata: data.string.e6ansb,
					},
					{
						option_class: "class3",
						optiondata: data.string.e6ansc, 
					},
					{
						option_class: "class4",
						optiondata: data.string.e6ansd, 
					}],
			} 
		] 
	},
	//slide 6
	{
		extratextblock: [{
			textdata: data.string.einstruction,
			textclass: 'instruction-new'
		}],
		exerciseblock: [
			{					
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext7,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.e7ansa,
					},
					{
						option_class: "class2",
						optiondata: data.string.e7ansb,
					},
					{
						option_class: "class3",
						optiondata: data.string.e7ansc, 
					},
					{
						option_class: "class4",
						optiondata: data.string.e7ansd, 
					}],
			} 
		] 
	},
	//slide 7
	{
		extratextblock: [{
			textdata: data.string.einstruction,
			textclass: 'instruction-new'
		}],
		exerciseblock: [
			{					
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext8,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.e8ansa,
					},
					{
						option_class: "class2",
						optiondata: data.string.e8ansb,
					},
					{
						option_class: "class3",
						optiondata: data.string.e8ansc, 
					},
					{
						option_class: "class4",
						optiondata: data.string.e8ansd, 
					}],
			} 
		] 
	},
	//slide 8
	{
		extratextblock: [{
			textdata: data.string.einstruction,
			textclass: 'instruction-new'
		}],
		exerciseblock: [
			{					
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext9,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.e9ansa,
					},
					{
						option_class: "class2",
						optiondata: data.string.e9ansb,
					},
					{
						option_class: "class3",
						optiondata: data.string.e9ansc, 
					},
					{
						option_class: "class4",
						optiondata: data.string.e9ansd, 
					}],
			} 
		] 
	},
	//slide 10
	{
		extratextblock: [{
			textdata: data.string.einstruction,
			textclass: 'instruction-new'
		}],
		exerciseblock: [
			{					
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext10,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.e10ansa,
					},
					{
						option_class: "class2",
						optiondata: data.string.e10ansb,
					},
					{
						option_class: "class3",
						optiondata: data.string.e10ansc, 
					},
					{
						option_class: "class4",
						optiondata: data.string.e10ansd, 
					}],
			} 
		] 
	}
];

/*remove this for non random questions*/
$(function () {	
	var testin = new NumberTemplate();
   
	var exercise = new template_exercise_mcq_monkey(content, testin, true);
	exercise.create_exercise();
});
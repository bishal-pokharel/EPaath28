var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var sound_1 = new buzz.sound((soundAsset + "p4_s0.ogg"));
var sound_2 = new buzz.sound((soundAsset + "p4_s1.ogg"));
var sound_3 = new buzz.sound((soundAsset + "p4_s2.ogg"));
var sound_4 = new buzz.sound((soundAsset + "p4_s3.ogg"));
var sound_5 = new buzz.sound((soundAsset + "p4_s4.ogg"));
var sound_6 = new buzz.sound((soundAsset + "p4_s5.ogg"));
var sound_7 = new buzz.sound((soundAsset + "p4_s6.ogg"));
var sound_8 = new buzz.sound((soundAsset + "p4_s7.ogg"));
var sound_9 = new buzz.sound((soundAsset + "p4_s8.ogg"));
var sound_10 = new buzz.sound((soundAsset + "p4_s9.ogg"));
var sound_11 = new buzz.sound((soundAsset + "p4_s10.ogg"));
var sound_12 = new buzz.sound((soundAsset + "p4_s11.ogg"));
var current_sound = sound_1;

var sound_arr_var = [sound_1, sound_2, sound_3, sound_4, sound_5, sound_6, sound_7, sound_8, sound_9, sound_10, sound_11, sound_12];


var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-cycle',

		extratextblock : [{
			textdata : data.string.p4text1,
			textclass : 'p4-text-1 my_font_big sniglet fade_in_1'
		}],
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-cycle',

		extratextblock : [{
			textdata : data.string.p4text2,
			textclass : 'p4-text-2 my_font_big sniglet fade_in-2'
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'sun sun-rising',
					imgsrc: imgpath + "sun.png",
				}
			],
		}]
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-cycle',

		extratextblock : [{
			textdata : data.string.p4text3,
			textclass : 'p4-text-2 my_font_big sniglet fade_in-1'
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'sun sun-2',
					imgsrc: imgpath + "sun.png",
				},{
					imgclass: 'sun-rays sunray-animation',
					imgsrc: imgpath + "sunlight.png",
				}
			],
		}]
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-cycle',

		extratextblock : [{
			textdata : data.string.p4text4,
			textclass : 'p4-text-2 my_font_big sniglet fade_in-1'
		}],

		vapor:[{
			vaporclass: 'steam-center-0 steam-center-00'
		},{
			vaporclass: 'steam-center-0 steam-center-01'
		},{
			vaporclass: 'steam-center-0 steam-center-02'
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'sun sun-2',
					imgsrc: imgpath + "sun.png",
				},{
					imgclass: 'sun-rays',
					imgsrc: imgpath + "sunlight.png",
				}
			],
		}]
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-cycle',

		extratextblock : [{
			textdata : data.string.p4text5,
			textclass : 'p4-text-2 my_font_big sniglet fade_in_1'
		}],

		vapor:[{
			vaporclass: 'steam-center-0 steam-anim-1 steam-center-00'
		},{
			vaporclass: 'steam-center-0 steam-anim-1 steam-center-01'
		},{
			vaporclass: 'steam-center-0 steam-anim-1 steam-center-02'
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'sun sun-2',
					imgsrc: imgpath + "sun.png",
				},{
					imgclass: 'sun-rays',
					imgsrc: imgpath + "sunlight.png",
				}
			],
		}]
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-cycle',

		extratextblock : [{
			textdata : data.string.p4text6,
			textclass : 'p4-text-3 my_font_big sniglet fade_in-1'
		}],

		vapor:[{
			vaporclass: 'steam-center-0 steam-anim-2 steam-center-00'
		},{
			vaporclass: 'steam-center-0 steam-anim-2 steam-center-01'
		},{
			vaporclass: 'steam-center-0 steam-anim-2 steam-center-02'
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'sun sun-2',
					imgsrc: imgpath + "sun.png",
				},{
					imgclass: 'sun-rays',
					imgsrc: imgpath + "sunlight.png",
				},{
					imgclass: 'cloud-cycle-1 cloud-fade-in',
					imgsrc: imgpath + "clod04.png",
				}
			],
		}]
	},
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-cycle',

		extratextblock : [{
			textdata : data.string.p4text7,
			textclass : 'p4-text-3 my_font_big sniglet fade_in_1'
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'sun sun-2',
					imgsrc: imgpath + "sun.png",
				},{
					imgclass: 'cloud-cycle-1 cloud-move-left',
					imgsrc: imgpath + "clod04.png",
				}
			],
		}]
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-cycle',

		extratextblock : [{
			textdata : data.string.p4text8,
			textclass : 'p4-text-4 my_font_big sniglet fade_in_1'
		},{
			textdata : '',
			textclass : 'wind-sprite blowing'
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'sun sun-2',
					imgsrc: imgpath + "sun.png",
				},{
					imgclass: 'cloud-cycle-1 cloud-cycle-2',
					imgsrc: imgpath + "clod04.png",
				},{
					imgclass: 'droplets',
					imgsrc: imgpath + "waterdrop.png",
				}
			],
		}],
	},
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-cycle',

		extratextblock : [{
			textdata : data.string.p4text9,
			textclass : 'p4-text-5 my_font_big sniglet fade_in_1'
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'sun sun-2',
					imgsrc: imgpath + "sun.png",
				},{
					imgclass: 'cloud-cycle-1 cloud-cycle-2',
					imgsrc: imgpath + "clod04.png",
				}
			],
		}],

		cloudrain:[{
			cloudrainclass: 'cloudraindiv',
		}],
	},
	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-cycle',

		extratextblock : [{
			textdata : data.string.p4text10,
			textclass : 'p4-text-5n my_font_big sniglet fade_in_1'
		},{
			textdata : '',
			textclass : 'diy-ocean-lake-sprite flowing'
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'sun sun-2',
					imgsrc: imgpath + "sun.png",
				},{
					imgclass: 'arrow arrow-riv-1',
					imgsrc: imgpath + "arrow.png",
				},{
					imgclass: 'arrow arrow-riv-2',
					imgsrc: imgpath + "arrow.png",
				}
			],
		}],
	},
	// slide10
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-cycle',

		extratextblock : [{
			textdata : data.string.p4text11,
			textclass : 'p4-text-5n my_font_big sniglet fade_in_1'
		}],

		vapor:[{
			vaporclass: 'steam-center-0 steam-center-00'
		},{
			vaporclass: 'steam-center-0 steam-center-01'
		},{
			vaporclass: 'steam-center-0 steam-center-02'
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'sun sun-2',
					imgsrc: imgpath + "sun.png",
				},{
					imgclass: 'sun-rays',
					imgsrc: imgpath + "sunlight.png",
				}
			],
		}],
		cloudrain:[{
			cloudrainclass: 'cloudraindiv',
		}],
	},
	// slide11
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-cycle',

		uppertextblockadditionalclass: 'p4-text-6 my_font_big newfont proximanova',
		uppertextblock : [{
			textdata : data.string.p4text12,
			textclass : 'fade_in_1'
		},{
			textdata : data.string.p4text13,
			textclass : 'fade_in_1'
		},{
			textdata : data.string.p4text14,
			textclass : 'fade_in_1',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl_cycle',
		}],
		extratextblock : [{
			textdata : '',
			textclass : 'diy-ocean-lake-sprite flowing'
		},{
			textdata : '',
			textclass : 'diy-wind-sprite blowing'
		}],
		vapor:[{
			vaporclass: 'steam-center-0 steam-center-00'
		},{
			vaporclass: 'steam-center-0 steam-center-01'
		},{
			vaporclass: 'steam-center-0 steam-center-02'
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'sun sun-2',
					imgsrc: imgpath + "sun.png",
				},{
					imgclass: 'sun-rays',
					imgsrc: imgpath + "sunlight.png",
				},{
					imgclass: 'cloud-cycle-1',
					imgsrc: imgpath + "clod04.png",
				},{
					imgclass: 'cloud-cycle-1 cloud-cycle-2',
					imgsrc: imgpath + "clod04.png",
				},{
					imgclass: 'arrow arrow-riv-1',
					imgsrc: imgpath + "arrow.png",
				},{
					imgclass: 'arrow arrow-steam-1',
					imgsrc: imgpath + "arrow.png",
				},{
					imgclass: 'arrow arrow-cloud-1',
					imgsrc: imgpath + "arrow.png",
				},{
					imgclass: 'arrow arrow-rain-1',
					imgsrc: imgpath + "arrow.png",
				},{
					imgclass: 'arrow arrow-riv-1',
					imgsrc: imgpath + "arrow.png",
				}
			],
		}],
		cloudrain:[{
			cloudrainclass: 'cloudraindiv',
		}],
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var timeoutvar = null;
	var current_char = null;
	var $total_page = content.length;
	var drop_count = 0;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
		case 0:
			sound_nav(sound_arr_var[countNext]);
			break;
		case 1:
		case 2:
		case 3:
			$prevBtn.show(0);
			timeoutvar = setTimeout(function(){
				sound_nav(sound_arr_var[countNext]);
			}, 2000);
			break;
		case 4:
		case 5:
			$prevBtn.show(0);
			timeoutvar = setTimeout(function(){
				sound_nav(sound_arr_var[countNext]);
			}, 1000);
			break;
		case 8:
			$prevBtn.show(0);
			makeItRain(-70, 'drop drop2', 45);
			sound_nav(sound_arr_var[countNext]);
			break;
		case 11:
			$prevBtn.show(0);
			makeItRain(-70, 'drop drop2', 45);
			timeoutvar = setTimeout(function(){
				sound_nav(sound_arr_var[countNext]);
			}, 1000);
			break;
		default:
			$prevBtn.show(0);
			sound_nav(sound_arr_var[countNext]);
			break;
		}
	}
	var makeItRain = function(incrementer, drop_class, bottomgiven) {
		//clear out everything
		$('.rain').empty();

		var increment = 0;
		var drops = "";
		var backDrops = "";
		var dropclass = 'drop';
		if( typeof drop_class != 'undefined'){
			dropclass = drop_class;
		}
		var bottom= false;
		if( typeof drop_class != 'undefined'){
			bottom = bottomgiven;
		}



		while (increment < 100) {
		//couple random numbers to use for various randomizations
			//random number between 98 and 1
			var rand_1 = (Math.floor(Math.random() * (98 - 1 + 1) + 1));
			//random number between 5 and 2
			var rand_2 = (Math.floor(Math.random() * (5 - 2 + 1) + 1));
			//increment
			increment += rand_2;
			//add in a new raindrop with various randomizations to certain CSS properties
			if(bottom){
				drops += '<div class="'+ dropclass +'" style="left: ' + increment + '%; bottom: ' + (bottom + 4*rand_2) + '%; animation-delay: 0.' + rand_1 + 's; animation-duration: 0.5' + rand_1 + 's;"><div class="stem" style="animation-delay: 0.' + rand_1 + 's; animation-duration: 0.5' + rand_1 + 's;"></div><div class="splat" style="animation-delay: 0.' + rand_1 + 's; animation-duration: 0.5' + rand_1 + 's;"></div></div>';
				backDrops += '<div class="'+ dropclass +'" style="right: ' + increment + '%; bottom: ' + (bottom + 4*rand_2) + '%; animation-delay: 0.' + rand_1 + 's; animation-duration: 0.5' + rand_1 + 's;"><div class="stem" style="animation-delay: 0.' + rand_1 + 's; animation-duration: 0.5' + rand_1 + 's;"></div></div>';
			} else {
				drops += '<div class="'+ dropclass +'" style="left: ' + increment + '%; bottom: ' + (rand_2 + rand_2 + incrementer - 1 + 100) + '%; animation-delay: 0.' + rand_1 + 's; animation-duration: 0.5' + rand_1 + 's;"><div class="stem" style="animation-delay: 0.' + rand_1 + 's; animation-duration: 0.5' + rand_1 + 's;"></div><div class="splat" style="animation-delay: 0.' + rand_1 + 's; animation-duration: 0.5' + rand_1 + 's;"></div></div>';
				backDrops += '<div class="'+ dropclass +'" style="right: ' + increment + '%; bottom: ' + (rand_2 + rand_2 + incrementer - 1 + 100) + '%; animation-delay: 0.' + rand_1 + 's; animation-duration: 0.5' + rand_1 + 's;"><div class="stem" style="animation-delay: 0.' + rand_1 + 's; animation-duration: 0.5' + rand_1 + 's;"></div></div>';
			}

		}
		$('.front-rain').append(drops);
		$('.back-rain').append(backDrops);
	};

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}
	function sound_nav(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			nav_button_controls(0);
		});
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		clearTimeout(timeoutvar);
		current_sound.stop();
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		clearTimeout(timeoutvar);
		current_sound.stop();
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

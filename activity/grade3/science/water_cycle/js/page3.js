var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var sound_1 = new buzz.sound((soundAsset + "p3_s0.ogg"));
var sound_2 = new buzz.sound((soundAsset + "p3_s1.ogg"));
var sound_3 = new buzz.sound((soundAsset + "p3_s2.ogg"));
var sound_4 = new buzz.sound((soundAsset + "p3_s3.ogg"));
var sound_5 = new buzz.sound((soundAsset + "p3_s4.ogg"));
var current_sound = sound_1;

var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-l-blue',

		uppertextblockadditionalclass: 'header-desc-p3  font-med-2 sniglet',
		uppertextblock : [{
			textdata : data.string.p3text1,
			textclass : 'fade_in_1'
		},{
			textdata : data.string.p3text2,
			textclass : 'fade_in-1'
		}],

		vapor:[{
			vaporclass: 'steam-center-0 steam-center-00'
		},{
			vaporclass: 'steam-center-0 steam-center-01'
		},{
			vaporclass: 'steam-center-0 steam-center-02'
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'center-image-s1',
					imgsrc: imgpath + "boil2.png",
				},
			],
		}],
		dhakkan:[{
			imgclass: '',
			imgsrc: imgpath + "dhakkan.png",
			backclass: 'dhakkanback',
			img2src:  imgpath + "waterdew.png",
		}]
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-l-blue',

		uppertextblockadditionalclass: 'header-desc-p3',
		uppertextblock : [{
			textdata : data.string.p3text3,
			textclass : 'font-med-2 sniglet fade_in_1'
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'girl',
					imgsrc: imgpath + "girl.png",
				},
				{
					imgclass: 'chasma',
					imgsrc: imgpath + "chasma.png",
				},
				{
					imgclass: 'girl-hands',
					imgsrc: imgpath + "steamed glasses-11.png",
				}
			],
		}],
		chasma:[{
			baaf: 'baaf-1',
		},
		{
			baaf: 'baaf-2',
		}],
		vapor:[{
			vaporclass: 'steam-center-girl'
		}],
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-l-blue',

		uppertextblockadditionalclass: 'header-desc-p3',
		uppertextblock : [{
			textdata : data.string.p3text4,
			textclass : 'font-med-2 sniglet fade_in_1'
		},{
			textdata : data.string.p3text5,
			textclass : 'font-med-2 sniglet fade_in_1'
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'foggy',
					imgsrc: imgpath + "foggy.png",
				},{
					imgclass: 'fog-1 fog',
					imgsrc: imgpath + "fog3.png",
				},{
					imgclass: 'fog-2 fog',
					imgsrc: imgpath + "fog2.png",
				},{
					imgclass: 'fog-3 fog',
					imgsrc: imgpath + "fog1.png",
				}
			],
		}]
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-l-blue',

		uppertextblockadditionalclass: 'center-desc-p3',
		uppertextblock : [{
			textdata : data.string.p3text6,
			textclass : 'font-med-2 sniglet fade_in_1'
		},{
			textdata : data.string.p3text7,
			textclass : 'font-med-2 sniglet fade_in_1'
		}],
		extratextblock : [{
			textdata : '',
			textclass : 'wind-sprite blowing'
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'cloud-cycle-1',
					imgsrc: imgpath + "clod04.png",
				},{
					imgclass: 'droplets',
					imgsrc: imgpath + "waterdrop.png",
				}
			],
		}],

	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
		rain: true,

		uppertextblockadditionalclass: 'my_font_super_big sniglet droplet-text',
		uppertextblock : [{
			textdata : data.string.p3text8,
			textclass : 'my_font_very_big sniglet'
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'cloud-top',
					imgsrc: imgpath + "clod04.png",
				}
			],
			imagelabels:[{
				imagelabelclass: 'earth-atmosphere',
				imagelabeldata: '',
			}]
		}]
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
		rain: true,

		uppertextblockadditionalclass: 'my_font_super_big sniglet droplet-text-2',
		uppertextblock : [{
			textdata : data.string.p3text9,
			textclass : 'my_font_very_big sniglet'
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'boy-rain',
					imgsrc: imgpath + "boy_rain.png",
				}
			],
			imagelabels:[{
				imagelabelclass: 'earth-atmosphere earth-atmosphere-2',
				imagelabeldata: '',
			}]
		}]
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var timeoutvar2 = null;
	var timeoutvar =null;
	var current_char = null;
	var $total_page = content.length;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
		case 0:
			timeoutvar2 = setTimeout(function(){
				$('.steam-center-0').show(0);
			}, 3800);
			sound_nav(sound_1);
			break;
		case 1:
			$prevBtn.show(0);
			timeoutvar2 = setTimeout(function(){
				$('.steam-center-girl').show(0);
			}, 500);
			sound_nav(sound_2);
			break;
		case 2:
			$prevBtn.show(0);
			sound_nav(sound_3);
			break;
		case 3:
			$prevBtn.show(0);
			sound_nav(sound_4);
			break;
		case 4:
			$prevBtn.show(0);
			makeItRain(0);
			sound_nav(sound_5);
			$('.rain').hide(0);
			$('.rain').delay(1000).show(1000);
			break;
		case 5:
			$prevBtn.show(0);
			makeItRain(0);
			timeoutvar2 = setTimeout(function(){
				makeItRain(22);
			}, 2000);
			nav_button_controls(1500);
			break;
		default:
			$prevBtn.show(0);
			nav_button_controls(500);
			break;
		}
	}
	var makeItRain = function(incrementer) {
		//clear out everything
		$('.rain').empty();

		var increment = 0;
		var drops = "";
		var backDrops = "";

		$('.front-rain').html('');
		$('.back-rain').html('');
		while (increment < 100) {
		//couple random numbers to use for various randomizations
			//random number between 98 and 1
			var rand_1 = (Math.floor(Math.random() * (98 - 1 + 1) + 1));
			//random number between 5 and 2
			var rand_2 = (Math.floor(Math.random() * (5 - 2 + 1) + 1));
			//increment
			increment += rand_2;
			//add in a new raindrop with various randomizations to certain CSS properties
			drops += '<div class="drop" style="left: ' + increment + '%; bottom: ' + (3.5*rand_2 + incrementer - 1 + 100) + '%; animation-delay: 0.' + rand_1 + 's; animation-duration: 0.5' + rand_1 + 's;"><div class="stem" style="animation-delay: 0.' + rand_1 + 's; animation-duration: 0.5' + rand_1 + 's;"></div><div class="splat" style="animation-delay: 0.' + rand_1 + 's; animation-duration: 0.5' + rand_1 + 's;"></div></div>';
			backDrops += '<div class="drop" style="right: ' + increment + '%; bottom: ' + (3.5*rand_2 + rand_2 + incrementer - 1 + 100) + '%; animation-delay: 0.' + rand_1 + 's; animation-duration: 0.5' + rand_1 + 's;"><div class="stem" style="animation-delay: 0.' + rand_1 + 's; animation-duration: 0.5' + rand_1 + 's;"></div></div>';
		}
		$('.front-rain').append(drops);
		$('.back-rain').append(backDrops);
	};

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}
	function sound_nav(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			nav_button_controls(0);
		});
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		current_sound.stop();
		switch(countNext){
			case 3:
				clearTimeout(timeoutvar);
				clearTimeout(timeoutvar2);
				$prevBtn.hide(0);
				$nextBtn.hide(0);
				$('.center-desc-p3, .wind-sprite').fadeOut(500,function(){
					$('.droplets, .cloud-cycle-1').css('animation', 'move-to-s4 2.5s ease-in both');
				});
				setTimeout(function(){
					countNext++;
					templateCaller();
				}, 3200);
				break;
			default:
				clearTimeout(timeoutvar);
				clearTimeout(timeoutvar2);
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		current_sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

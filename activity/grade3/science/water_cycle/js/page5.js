var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var sound_1 = new buzz.sound((soundAsset + "p5_s0.ogg"));
var sound_2 = new buzz.sound((soundAsset + "p5_s1.ogg"));
var current_sound = sound_1;

var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'diy-bg',

	//	uppertextblockadditionalclass: 'center-text',
		extratextblock : [{
			textdata : data.string.diytext,
			textclass : 'diy-title'
		}],
		imageblock : [{
			imgdv:"imgclx",
			imagestoshow : [
				{
					imgclass: 'diycover',
					imgsrc: imgpath + "bg_diy.png",
				}]
			}]
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		// contentblockadditionalclass : 'light-bg',

		uppertextblockadditionalclass: 'diyblock',
		uppertextblock : [{
			textdata : data.string.diy1,
			textclass : 'diy-header'
		},
		{
			textdata : data.string.diy2,
			textclass : 'diy-item diy-1'
		},
		{
			textdata : data.string.diy3,
			textclass : 'diy-item diy-2'
		},
		{
			textdata : data.string.diy4,
			textclass : 'diy-item diy-3'
		},
		{
			textdata : data.string.diy5,
			textclass : 'diy-item diy-4'
		},
		{
			textdata : data.string.diy6,
			textclass : 'diy-item diy-5'
		},
		{
			textdata : data.string.diy7,
			textclass : 'diy-item diy-6'
		}],

		imageblock : [{
			imgdv:"imgclas",
			imagestoshow : [
				{
					imgclass: 'diy-bgx',
					imgsrc: imgpath + "diy.png",
				},{
					imgclass: 'diy-bg',
					imgsrc: imgpath + "re_cycle_arrow.png",
				},{
					imgclass: 'cloud-diy',
					imgsrc: imgpath + "clod04.png",
				}
			],
		}],

		dropblock:[
			{
				dropclass: 'dropdiv dropdiv-1',
				images:[{
					imgclass: 'diy-waterdrop fade_in-1 adropdiv-1',
					imgsrc: imgpath + 'waterdrop.png'
				}],
				text:[,{
					textdata : '',
					textclass : 'diy-wind-sprite'
				}]
			},
			{
				dropclass: 'dropdiv dropdiv-2',
				vapor:[{
					vaporclass: 'diy-vapor-0 diy-vapor-00'
				},{
					vaporclass: 'diy-vapor-0 diy-vapor-01'
				}]
			},
			{
				dropclass: 'dropdiv dropdiv-3',
				vapor:[{
					vaporclass: 'diy-vapor-1 diy-vapor-10 adropdiv-3 fade_out-1'
				},{
					vaporclass: 'diy-vapor-1 diy-vapor-11 adropdiv-3 fade_out-1'
				}],
				images:[{
					imgclass: 'cloud-diy-2',
					imgsrc: imgpath + 'clod04.png'
				}]
			},
			{
				dropclass: 'dropdiv dropdiv-4',
				cloudrain:[{
					cloudrainclass: 'diy-rain adropdiv-4',
				}],
				images:[{
					imgclass: 'static-rain',
					imgsrc: imgpath + 'rain.png'
				}]
			},
			{
				dropclass: 'dropdiv dropdiv-5',
				text:[,{
					textdata : '',
					textclass : 'diy-river-lake-sprite'
				}]
			},
			{
				dropclass: 'dropdiv dropdiv-6',
			}
		]
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var timeoutvar = null;
	var current_char = null;
	var $total_page = content.length;
	var drop_count = 0;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
		case 0:
			play_diy_audio();
			nav_button_controls(2000);
			break;
		case 1:
			$prevBtn.show(0);
			sound_player(sound_2);
			drop_count =0;
			$( '.diy-item' ).draggable({
				containment: ".board",
				cursor: "move",
				revert: "invalid",
				appendTo: "body",
				start: function( event, ui ){
					$(this).addClass('selected');
				},
				stop: function( event, ui ){
					$(this).removeClass('selected');
				}
			});
			$('.dropdiv-1').droppable({
					accept : ".diy-1",
					drop:function(event, ui) {
						drop_text_to_box(ui.draggable, $(this));
						$('.diy-wind-sprite').addClass('blowing');
					}
			});
			$('.dropdiv-2').droppable({
					accept : ".diy-2",
					drop:function(event, ui) {
						drop_text_to_box(ui.draggable, $(this));
						$('.diy-vapor-0').css('animation', 'vapor 2s linear infinite');
					}
			});
			$('.dropdiv-3').droppable({
					accept : ".diy-3",
					drop:function(event, ui) {
						drop_text_to_box(ui.draggable, $(this));
						$('.cloud-diy-2').addClass('cloud-fade-in-1');
					}
			});
			$('.dropdiv-4').droppable({
					accept : ".diy-4",
					drop:function(event, ui) {
						drop_text_to_box(ui.draggable, $(this));
						$('.static-rain').hide(0);
					}
			});
			$('.dropdiv-5').droppable({
					accept : ".diy-5",
					drop:function(event, ui) {
						drop_text_to_box(ui.draggable, $(this));
						$('.diy-river-lake-sprite').addClass('flowing');
					}
			});
			$('.dropdiv-6').droppable({
					accept : ".diy-6",
					drop:function(event, ui) {
						drop_text_to_box(ui.draggable, $(this));
					}
			});
			makeItRain(-70, 'drop drop2', 45);
			// for(var index=1; index<=$('.dropdiv').length; index++){
				// $('.dropdiv-'+index).droppable({
					// accept : ".diy-"+index,
					// drop:function(event, ui) {
						// drop_text_to_box(ui.draggable, $(this));
					// }
				// });
			// }
			break;
		default:
			$prevBtn.show(0);
			nav_button_controls(500);
			break;
		}
	}
	function drop_text_to_box(dropped, droppedOn){
		play_correct_incorrect_sound_old(1);
		drop_count++;
		dropped.draggable('disable');
		dropped.addClass('dropped');
		droppedOn.css('background-color', '#98c02e');
		droppedOn.css('border-color', '#deef3c');
		// droppedOn.addClass('background-fade');
		var ind = String(droppedOn.attr('class'));
		ind = parseInt(ind.replace(/[^0-9]/g, ''));
		$('.adropdiv-'+ind).delay(1000).fadeIn(1000);
		dropped.detach().css({
			'pointer-events': 'none',
			'position': 'absolute',
			'left': '50%',
			'width': '100%',
			'height': 'auto',
			'top': '50%',
			'background-color': 'transparent',
			'transform': 'translate(-50%, -50%)',
			'color':'#000'
		}).appendTo(droppedOn);
		if(drop_count >=12){
			nav_button_controls(0);
		}
	}
	var makeItRain = function(incrementer, drop_class, bottomgiven) {
		//clear out everything
		$('.rain').empty();

		var increment = 0;
		var drops = "";
		var backDrops = "";
		var dropclass = 'drop';
		if( typeof drop_class != 'undefined'){
			dropclass = drop_class;
		}
		var bottom= false;
		if( typeof drop_class != 'undefined'){
			bottom = bottomgiven;
		}



		while (increment < 100) {
		//couple random numbers to use for various randomizations
			//random number between 98 and 1
			var rand_1 = (Math.floor(Math.random() * (98 - 1 + 1) + 1));
			//random number between 5 and 2
			var rand_2 = (Math.floor(Math.random() * (5 - 2 + 1) + 1));
			//increment
			increment += rand_2;
			//add in a new raindrop with various randomizations to certain CSS properties
			if(bottom){
				drops += '<div class="'+ dropclass +'" style="left: ' + increment + '%; bottom: ' + (bottom + 4*rand_2) + '%; animation-delay: 0.' + rand_1 + 's; animation-duration: 0.5' + rand_1 + 's;"><div class="stem" style="animation-delay: 0.' + rand_1 + 's; animation-duration: 0.5' + rand_1 + 's;"></div><div class="splat" style="animation-delay: 0.' + rand_1 + 's; animation-duration: 0.5' + rand_1 + 's;"></div></div>';
				backDrops += '<div class="'+ dropclass +'" style="right: ' + increment + '%; bottom: ' + (bottom + 4*rand_2) + '%; animation-delay: 0.' + rand_1 + 's; animation-duration: 0.5' + rand_1 + 's;"><div class="stem" style="animation-delay: 0.' + rand_1 + 's; animation-duration: 0.5' + rand_1 + 's;"></div></div>';
			} else {
				drops += '<div class="'+ dropclass +'" style="left: ' + increment + '%; bottom: ' + (rand_2 + rand_2 + incrementer - 1 + 100) + '%; animation-delay: 0.' + rand_1 + 's; animation-duration: 0.5' + rand_1 + 's;"><div class="stem" style="animation-delay: 0.' + rand_1 + 's; animation-duration: 0.5' + rand_1 + 's;"></div><div class="splat" style="animation-delay: 0.' + rand_1 + 's; animation-duration: 0.5' + rand_1 + 's;"></div></div>';
				backDrops += '<div class="'+ dropclass +'" style="right: ' + increment + '%; bottom: ' + (rand_2 + rand_2 + incrementer - 1 + 100) + '%; animation-delay: 0.' + rand_1 + 's; animation-duration: 0.5' + rand_1 + 's;"><div class="stem" style="animation-delay: 0.' + rand_1 + 's; animation-duration: 0.5' + rand_1 + 's;"></div></div>';
			}

		}
		$('.front-rain').append(drops);
		$('.back-rain').append(backDrops);
	};

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}
	function sound_nav(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			nav_button_controls(0);
		});
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		clearTimeout(timeoutvar);
		current_sound.stop();
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		clearTimeout(timeoutvar);
		current_sound.stop();
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

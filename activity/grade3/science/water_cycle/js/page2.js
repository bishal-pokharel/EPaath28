var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/" + $lang + "/";

var sound_1 = new buzz.sound((soundAsset + "p2_s0.ogg"));
var sound_2 = new buzz.sound((soundAsset + "p2_s1.ogg"));
var sound_3 = new buzz.sound((soundAsset + "p2_s2.ogg"));
var sound_4 = new buzz.sound((soundAsset + "p2_s3.ogg"));
var sound_5 = new buzz.sound((soundAsset + "p2_s4.ogg"));
var sound_6 = new buzz.sound((soundAsset + "p2_s5.ogg"));
var sound_7 = new buzz.sound((soundAsset + "p2_s6.ogg"));
var sound_8 = new buzz.sound((soundAsset + "p2_s7.ogg"));
var sound_9 = new buzz.sound((soundAsset + "p2_s8.ogg"));
var current_sound = sound_1;

var content = [
  //slide0
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: 'bg-l-blue',

    uppertextblockadditionalclass: 'header-desc',
    uppertextblock: [{
      textdata: data.string.p2text1,
      textclass: 'font-med-2 sniglet fade_in_1'
    }],

    vapor: [{
      vaporclass: 'steam-center-0 steam-center-00'
    }, {
      vaporclass: 'steam-center-0 steam-center-01'
    }, {
      vaporclass: 'steam-center-0 steam-center-02'
    }],

    imageblock: [{
      imagestoshow: [{
        imgclass: 'center-image center-image-s1',
        imgsrc: imgpath + "boil.png",
      }],
    }]
  },
  //slide1
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: 'bg-l-blue',

    uppertextblockadditionalclass: 'header-desc-p2 font-med-2 sniglet',
    uppertextblock: [{
      textdata: data.string.p2text2,
      textclass: 'fade_in_1'
    }],

    vapor: [{
      vaporclass: 'steam-center-1 steam-center-10'
    }, {
      vaporclass: 'steam-center-1 steam-center-11'
    }, {
      vaporclass: 'steam-center-1 steam-center-12'
    }, {
      vaporclass: 'steam-center-1 steam-center-13'
    }, {
      vaporclass: 'steam-center-1 steam-center-14'
    }, {
      vaporclass: 'steam-center-1 steam-center-15'
    }, {
      vaporclass: 'steam-center-1 steam-center-16'
    }],

    imageblock: [{
      imagestoshow: [{
          imgclass: 'clothes-bottom',
          imgsrc: imgpath + "clothes.png",
        },
        {
          imgclass: 'sun-p2-1',
          imgsrc: imgpath + "sun.png",
        }
      ],
    }]
  },
  //slide2
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: 'bg-l-blue',

    uppertextblockadditionalclass: 'header-desc-p2 font-med-2 sniglet',
    uppertextblock: [{
        textdata: data.string.p2text3,
        textclass: 'fade_in_1'
      },
      {
        textdata: data.string.p2text4,
        textclass: ' fade_in-1'
      }
    ],

    vapor: [{
      vaporclass: 'steam-center-20'
    }],

    imageblock: [{
      imagestoshow: [{
          imgclass: 'bucket-image',
          imgsrc: imgpath + "bucket.png",
        },
        {
          imgclass: 'sun-p2',
          imgsrc: imgpath + "sun.png",
        }
      ],
    }]
  },

  //slide3
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: 'bg-l-blue',

    uppertextblockadditionalclass: 'header-desc-s2  font-med-2 sniglet',
    uppertextblock: [{
      textdata: data.string.p2text5,
      textclass: 'fade_in_1'
    }],

    vapor: [{
        vaporclass: 'steam-center-3 steam-center-30'
      },
      {
        vaporclass: 'steam-center-3 steam-center-31'
      },
      {
        vaporclass: 'steam-center-3 steam-center-32'
      },
      {
        vaporclass: 'steam-center-3 steam-center-33'
      },
      {
        vaporclass: 'steam-center-3 steam-center-34'
      },
      {
        vaporclass: 'steam-center-3 steam-center-35'
      }
    ],

    imageblock: [{
      imagestoshow: [{
          imgclass: 'p2-image-gr p2-image-1',
          imgsrc: imgpath + "lake.png",
        },
        {
          imgclass: 'p2-image-gr p2-image-2',
          imgsrc: imgpath + "river.png",
        },
        {
          imgclass: 'p2-image-gr p2-image-3',
          imgsrc: imgpath + "sea.png",
        },
        {
          imgclass: 'sun-center-p2',
          imgsrc: imgpath + "sun.png",
        },
        {
          imgclass: 'cloud-p2-05',
          imgsrc: imgpath + "clouds-05.png",
        },
        {
          imgclass: 'cloud-p2-04',
          imgsrc: imgpath + "clouds-04.png",
        }
      ],
    }]
  },
  //slide4
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: 'bg-l-blue',

    uppertextblockadditionalclass: 'header-desc',
    uppertextblock: [{
      textdata: data.string.p2text6,
      textclass: 'font-med-2 sniglet fade_in_1'
    }],

    vapor: [{
      vaporclass: 'steam-center-4 steam-center-40'
    }, {
      vaporclass: 'steam-center-4 steam-center-41'
    }, {
      vaporclass: 'steam-center-4 steam-center-42'
    }, {
      vaporclass: 'steam-center-4 steam-center-43'
    }, {
      vaporclass: 'steam-center-4 steam-center-44'
    }, {
      vaporclass: 'steam-center-4 steam-center-45'
    }, {
      vaporclass: 'steam-center-4 steam-center-47'
    }, {
      vaporclass: 'steam-center-4 steam-center-48'
    }, {
      vaporclass: 'steam-center-4 steam-center-49'
    }, {
      vaporclass: 'steam-center-4 steam-center-50'
    }, {
      vaporclass: 'steam-center-4 steam-center-51'
    }],

    imageblock: [{
      imagestoshow: [{
          imgclass: 'p2-image-gr2 p2-image2-1',
          imgsrc: imgpath + "lake.png",
        },
        {
          imgclass: 'p2-image-gr2 p2-image2-2',
          imgsrc: imgpath + "bucket.png",
        },
        {
          imgclass: 'p2-image-gr2 p2-image2-3',
          imgsrc: imgpath + "river.png",
        },
        {
          imgclass: 'p2-image-gr2 p2-image2-4',
          imgsrc: imgpath + "clothes.png",
        },
        {
          imgclass: 'p2-image-gr2 p2-image2-5',
          imgsrc: imgpath + "sea.png",
        }
      ],
    }]
  },
  //slide5
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: 'bg-l-blue',

    uppertextblockadditionalclass: 'header-desc',
    uppertextblock: [{
      textdata: data.string.p2text7,
      textclass: 'font-med-2 sniglet fade_in_1'
    }],

    vapor: [{
      vaporclass: 'steam-center-4 steam-center-40 vapor-rise-up-1'
    }, {
      vaporclass: 'steam-center-4 steam-center-41 vapor-rise-up-1'
    }, {
      vaporclass: 'steam-center-4 steam-center-42 vapor-rise-up-1'
    }, {
      vaporclass: 'steam-center-4 steam-center-43 vapor-rise-up-1'
    }, {
      vaporclass: 'steam-center-4 steam-center-44 vapor-rise-up-1'
    }, {
      vaporclass: 'steam-center-4 steam-center-45 vapor-rise-up-1'
    }, {
      vaporclass: 'steam-center-4 steam-center-47 vapor-rise-up-1'
    }, {
      vaporclass: 'steam-center-4 steam-center-48 vapor-rise-up-1'
    }, {
      vaporclass: 'steam-center-4 steam-center-49 vapor-rise-up-1'
    }, {
      vaporclass: 'steam-center-4 steam-center-50 vapor-rise-up-1'
    }, {
      vaporclass: 'steam-center-4 steam-center-51 vapor-rise-up-1'
    }],

    imageblock: [{
      imagestoshow: [{
          imgclass: 'p2-image-gr2 p2-image2-1',
          imgsrc: imgpath + "lake.png",
        },
        {
          imgclass: 'p2-image-gr2 p2-image2-2',
          imgsrc: imgpath + "bucket.png",
        },
        {
          imgclass: 'p2-image-gr2 p2-image2-3',
          imgsrc: imgpath + "river.png",
        },
        {
          imgclass: 'p2-image-gr2 p2-image2-4',
          imgsrc: imgpath + "clothes.png",
        },
        {
          imgclass: 'p2-image-gr2 p2-image2-5',
          imgsrc: imgpath + "sea.png",
        }
      ],
    }]
  },
  //slide6
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: 'bg-l-blue',

    uppertextblockadditionalclass: 'header-desc mid-p2',
    uppertextblock: [{
      textdata: data.string.p2text8,
      textclass: 'font-med-2 sniglet fade_in_1'
    }],

    vapor: [{
      vaporclass: 'steam-center-4 steam-center-40 vapor-rise-up'
    }, {
      vaporclass: 'steam-center-4 steam-center-41 vapor-rise-up'
    }, {
      vaporclass: 'steam-center-4 steam-center-42 vapor-rise-up'
    }, {
      vaporclass: 'steam-center-4 steam-center-43 vapor-rise-up'
    }, {
      vaporclass: 'steam-center-4 steam-center-44 vapor-rise-up'
    }, {
      vaporclass: 'steam-center-4 steam-center-45 vapor-rise-up'
    }, {
      vaporclass: 'steam-center-4 steam-center-47 vapor-rise-up'
    }, {
      vaporclass: 'steam-center-4 steam-center-48 vapor-rise-up'
    }, {
      vaporclass: 'steam-center-4 steam-center-49 vapor-rise-up'
    }, {
      vaporclass: 'steam-center-4 steam-center-50 vapor-rise-up'
    }, {
      vaporclass: 'steam-center-4 steam-center-51 vapor-rise-up'
    }],

    imageblock: [{
      imagestoshow: [{
          imgclass: 'p2-image-gr2 p2-image2-1 ',
          imgsrc: imgpath + "lake.png",
        },
        {
          imgclass: 'p2-image-gr2 p2-image2-2',
          imgsrc: imgpath + "bucket.png",
        },
        {
          imgclass: 'p2-image-gr2 p2-image2-3',
          imgsrc: imgpath + "river.png",
        },
        {
          imgclass: 'p2-image-gr2 p2-image2-4',
          imgsrc: imgpath + "clothes.png",
        },
        {
          imgclass: 'p2-image-gr2 p2-image2-5',
          imgsrc: imgpath + "sea.png",
        },
        {
          imgclass: 'cloud-2 cloud-fade-in',
          imgsrc: imgpath + "clod02.png",
        },
        {
          imgclass: 'cloud-2 cloud-21 cloud-fade-in',
          imgsrc: imgpath + "clod02.png",
        },
        {
          imgclass: 'cloud-2 cloud-22 cloud-fade-in',
          imgsrc: imgpath + "clod01.png",
        }
      ],
    }]
  },
  //slide7
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: 'bg-l-blue',

    uppertextblockadditionalclass: 'header-desc mid-p2',
    uppertextblock: [{
      textdata: data.string.p2text9,
      textclass: 'font-med-2 sniglet fade_in_1'
    }],

    imageblock: [{
      imagestoshow: [{
          imgclass: 'cloud-2',
          imgsrc: imgpath + "clod02.png",
        },
        {
          imgclass: 'cloud-2 cloud-21',
          imgsrc: imgpath + "clod02.png",
        },
        {
          imgclass: 'cloud-2 cloud-22',
          imgsrc: imgpath + "clod01.png",
        }
      ],
    }]
  },

  //slide8
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: 'bg-l-blue',

    uppertextblockadditionalclass: 'header-desc mid-p2',
    uppertextblock: [{
        textdata: data.string.p2text10,
        textclass: 'font-med-2 sniglet fade_in_1'
      },
      {
        textdata: data.string.p2text11,
        textclass: 'font-med-2 sniglet fade_in-1'
      }
    ],

    imageblock: [{
      imagestoshow: [{
          imgclass: 'cloud-2',
          imgsrc: imgpath + "clod02.png",
        },
        {
          imgclass: 'cloud-2 cloud-21',
          imgsrc: imgpath + "clod02.png",
        },
        {
          imgclass: 'cloud-2 cloud-22',
          imgsrc: imgpath + "clod01.png",
        }
      ],
    }]
  },
];

$(function() {

  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;
  var $total_page = content.length;

  var timeoutvar = null;
  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();

  Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

  function navigationController(islastpageflag) {
    typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
    // if lastpageflag is true
    // islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
  }

  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);

    $board.html(html);
    loadTimelineProgress($total_page, countNext + 1);

    texthighlight($board);
    vocabcontroller.findwords(countNext);

    switch (countNext) {
      case 0:
        sound_nav(sound_1);
        break;
      case 1:
        $prevBtn.show(0);
        sound_nav(sound_2);
        break;
      case 2:
        $prevBtn.show(0);
        sound_nav(sound_3);
        break;
      case 3:
        $prevBtn.show(0);
        sound_nav(sound_4);
        break;
      case 4:
        $prevBtn.show(0);
        sound_nav(sound_5);
        break;
      case 5:
        $prevBtn.show(0);
        sound_nav(sound_6);
        break;
      case 6:
        $prevBtn.show(0);
        sound_nav(sound_7);
        break;
      case 7:
        $prevBtn.show(0);
        sound_nav(sound_8);
        break;
      case 8:
        $prevBtn.show(0);
        sound_nav(sound_9);
        break;
      default:
        $prevBtn.show(0);
        nav_button_controls(500);
        break;
    }
  }


  function nav_button_controls(delay_ms) {
    timeoutvar = setTimeout(function() {
      if (countNext == 0) {
        $nextBtn.show(0);
      } else if (countNext > 0 && countNext == $total_page - 1) {
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else {
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    }, delay_ms);
  }

  function sound_player(sound_data) {
    current_sound.stop();
    current_sound = sound_data;
    current_sound.play();
  }

  function sound_nav(sound_data) {
    current_sound.stop();
    current_sound = sound_data;
    current_sound.play();
    current_sound.bindOnce('ended', function() {
      nav_button_controls(0);
    });
  }

  function templateCaller() {
    $prevBtn.hide(0);
    $nextBtn.hide(0);
    navigationController();
    generalTemplate();
    /*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

  }


  $nextBtn.on("click", function() {
    clearTimeout(timeoutvar);
    current_sound.stop();
    switch (countNext) {
      default:
        countNext++;
        templateCaller();
        break;
    }

  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    clearTimeout(timeoutvar);
    current_sound.stop();
    countNext--;
    templateCaller();
    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
  });

  total_page = content.length;
  templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
  //check if $highlightinside is provided
  typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

  var $alltextpara = $highlightinside.find("*[data-highlight='true']");
  var stylerulename;
  var replaceinstring;
  var texthighlightstarttag;
  var texthighlightendtag = "</span>";

  if ($alltextpara.length > 0) {
    $.each($alltextpara, function(index, val) {
      /*if there is a data-highlightcustomclass attribute defined for the text element
       use that or else use default 'parsedstring'*/
      $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
        (stylerulename = $(this).attr("data-highlightcustomclass")) : (stylerulename = "parsedstring");

      texthighlightstarttag = "<span class = " + stylerulename + " >";

      replaceinstring = $(this).html();
      replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
      replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

      $(this).html(replaceinstring);
    });
  }
}

/*=====  End of data highlight function  ======*/

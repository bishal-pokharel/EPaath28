Array.prototype.shufflearray = function(){
  var i = this.length, j, temp;
	    while(--i > 0){
	        j = Math.floor(Math.random() * (i+1));
	        temp = this[j];
	        this[j] = this[i];
	        this[i] = temp;
	    }
	    return this;
}
var soundAsset = $ref+"/audio_"+$lang+"/";

var imgpath = $ref+"/exercise/images/";

var content=[

	//ex1
	{
		exerciseblock: [
			{
				textdata: data.string.exques1,
				textdata1: data.string.query,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q1ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q1ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q1ansc,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q1ansd,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "1.png",
					}
				]

			}
		]
	},
	//ex2
	{
		exerciseblock: [
			{
				textdata: data.string.exques2,
                textdata1: data.string.query,
				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q2ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q2ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q2ansc,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q2ansd,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "2.png",
					}
				]

			}
		]
	},
	//ex3
	{
		exerciseblock: [
			{
				textdata: data.string.exques3,
                textdata1: data.string.query,
				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q3ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q3ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q3ansc,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q3ansd,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "3.png",
					}
				]

			}
		]
	},
	//ex4
	{
		exerciseblock: [
			{
				textdata: data.string.exques4,
                textdata1: data.string.query,
				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q4ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q4ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q4ansc,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q4ansd,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "4.png",
					}
				]

			}
		]
	},
	//ex5
	{
		exerciseblock: [
			{
				textdata: data.string.exques5,
                textdata1: data.string.query,
				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q5ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q5ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q5ansc,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q5ansd,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "5.png",
					}
				]

			}
		]
	},
	//ex6
	{
		exerciseblock: [
			{
				textdata: data.string.exques6,
                textdata1: data.string.query,
				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q6ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q6ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q6ansc,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q6ansd,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "6.png",
					}
				]

			}
		]
	},
	//ex7
	{
		exerciseblock: [
			{
				textdata: data.string.exques7,
                textdata1: data.string.query,
				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q7ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q7ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q7ansc,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q7ansd,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "7.png",
					}
				]

			}
		]
	},
	//ex8
	{
		exerciseblock: [
			{
				textdata: data.string.exques8,
                textdata1: data.string.query,
				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q8ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q8ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q8ansc,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q8ansd,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "8.png",
					}
				]

			}
		]
	},
	//ex9
	{
		exerciseblock: [
			{
				textdata: data.string.exques9,
                textdata1: data.string.query,
				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q9ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q9ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q9ansc,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q9ansd,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "9.png",
					}
				]

			}
		]
	},
	//ex10
	{
		exerciseblock: [
			{
				textdata: data.string.exques10,
                textdata1: data.string.query,
				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q10ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q10ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q10ansc,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q10ansd,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "10.png",
					}
				]

			}
		]
	},
];

/*remove this for non random questions*/
content.shufflearray();


$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var current_sound;

	/*for limiting the questions to 10*/
	var $total_page = 10;

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	var score = 0;
	var testin = new EggTemplate();

 	testin.init(10);
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            // sounds
            {id: "sound_0", src: soundAsset+"p1_s0.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }
    function handleFileLoad(event) {
        console.log(event.item);
    }
    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded*100)+'%');
    }
    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        // current_sound = createjs.Sound.play('sound_0');
        // call main function
        templateCaller();
    }
    //initialize
    init();
	/*values in this array is same as the name of images of eggs in image folder*/
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);

		/*generate question no at the beginning of question*/
		testin.numberOfQuestions();

		/*for randomizing the options*/
		var parent = $(".optionsdiv");
		var divs = parent.children();
			 while (divs.length) {
			        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			  }

		var ansClicked = false;
		var wrngClicked = false;
		if(countNext==0){
            createjs.Sound.stop();
            current_sound = createjs.Sound.play("sound_0");
            current_sound.play();
		}
		$(".buttonsel").click(function(){
			$(this).removeClass('forhover');
				if(ansClicked == false){
					/*class 1 is always for the right answer. updates scoreboard and disables other click if
					right answer is clicked*/
					if($(this).hasClass("class1")){
						if(wrngClicked == false){
							testin.update(true);
						}
                        current_sound.stop();
						play_correct_incorrect_sound(1);
						$(this).css("background","#6AA84F");
						$(this).css("border","5px solid #B6D7A8");
						$(this).siblings(".corctopt").show(0);
						$('.hint_image').show(0);
						$('.buttonsel').removeClass('forhover forhoverimg');
						ansClicked = true;

						if(countNext != $total_page)
						$nextBtn.show(0);
					}
					else{
                        current_sound.stop();
						testin.update(false);
						play_correct_incorrect_sound(0);
						$(this).css("background","#EA9999");
						$(this).css("border","5px solid #efb3b3");
						$(this).css("color","white");
						$(this).siblings(".wrngopt").show(0);
						wrngClicked = true;
					}
				}
			});

		/*======= SCOREBOARD SECTION ==============*/
	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
	}


	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();
	});


	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref + "/images/img/";
var soundAsset = $ref+"/audio_"+$lang+"/";

var content = [
    //0th slide
    {
        hasheaderblock: true,
        datahighlightflag: true,
        additionalclasscontentblock: "bg-img",
        newclass: "bg-img",
        addclass: "text-box animated fadeIn",
        // imageblock: [{
        //     imagetoshow: [{
        //         imgclass: "lips",
        //         imgsrc: imgpath + "cover_page.png"
        //     }]
        // }],
        addclass:"covertext",
        uppertextblock: [{
            texttoshow: [{
                textdata: data.lesson.chapter
            }]
        }]

    },
    //1st slide
    {
        hasheaderblock: true,
        datahighlightflag: true,
        additionalclasscontentblock: "bg-img",
        newclass: "bg-img",
        addclass: "text-box animated fadeIn",
        imageblock: [{
            imagetoshow: [{
                imgclass: "lips",
                imgsrc: imgpath + "lips-motion.png"
            }]
        }],
        uppertextblock: [{
            texttoshow: [{
                textdata: data.string.p1text1
            }]
        }]

    },
    //2nd
    {
        hasheaderblock: true,

        additionalclasscontentblock: "bg-img",
        addclass: "text-box2 animated fadeIn",
        imageblock: [{
            imagetoshow: [{
                imgclass: "sound2",
                imgsrc: imgpath + "sound-icon.png"
            }, {
                imgclass: "lips",
                imgsrc: imgpath + "lips-motion_loop.gif"
            }]
        }],
        uppertextblock: [{
            texttoshow: [{
                textdata: data.string.p2text1
            }]
        }]
    },
    //3rd

    {
        hasheaderblock: true,
        additionalclasscontentblock: "bg-img2",
        addclass: " box3  animated bounceInDown",
        txtid: "hide",
        uppertextblock: [{
            texttoshow: [{
                textdata: data.string.p3text1
            }]
        }],
        imageblock: [{
            imagetoshow: [{
                    imgclass: "house",
                    imgsrc: imgpath + "house.png"
                }, {
                    imgclass: "grass03",
                    imgsrc: imgpath + "grass03.png"
                }, {
                    imgclass: "man01",
                    imgsrc: imgpath + "man01.png"
                }, {
                    imgclass: "jungle",
                    imgsrc: imgpath + "jungle.png"
                }, {
                    imgclass: "ftree",
                    imgsrc: imgpath + "fruits-tree.png"
                }, {
                    imgclass: "girl",
                    imgsrc: imgpath + "girl.png"
                }, {
                    imgclass: "girl",
                    imgsrc: imgpath + "girl.png"
                }, {
                    imgclass: "lady",
                    imgsrc: imgpath + "lady.png"
                }, {
                    imgclass: "man",
                    imgsrc: imgpath + "man.png"
                },

                {
                    imgclass: "cow",
                    imgsrc: imgpath + "cow.png"
                }, {
                    imgclass: "flowers",
                    imgsrc: imgpath + "flowers.png"
                }, {
                    imgclass: "clothes01",
                    imgsrc: imgpath + "clothes01.png"
                }, {
                    imgclass: "cotton",
                    imgsrc: imgpath + "cotton.png"
                }, {
                    imgclass: "corn-plant",
                    imgsrc: imgpath + "corn-plant.png"
                }, {
                    imgclass: "veg-plant",
                    imgsrc: imgpath + "veg-plant.png"
                }, {
                    imgclass: "basket",
                    imgsrc: imgpath + "bamboo-baskets.png"
                }

            ]
        }]

    },

    {
        hasheaderblock: true,
        additionalclasscontentblock: "bg-img2",
        addclass: "box3 animated bounceInDown",
        uppertextblock: [{
            texttoshow: [{
                textdata: data.string.p4text1
            },
                {
                    textdata:"",
                    textclass:"popup"
                }]
        }],

        imageblock: [{
            imagetoshow: [{
                    imgclass: "house",
                    imgsrc: imgpath + "house.png"
                }, {
                    imgclass: "grass03",
                    imgsrc: imgpath + "grass03.png"
                }, {
                    imgclass: "man01 firewood",
                    imgsrc: imgpath + "man01.png"
                }, {
                    imgclass: "jungle firewood",
                    imgsrc: imgpath + "jungle.png"
                }, {
                    imgclass: "ftree fruit",
                    imgsrc: imgpath + "fruits-tree.png"
                }, {
                    imgclass: "girl fruit",
                    imgsrc: imgpath + "girl.png"
                }, {
                    imgclass: "lady bamboo",
                    imgsrc: imgpath + "lady.png"
                }, {
                    imgclass: "man",
                    imgsrc: imgpath + "man.png"
                },

                {
                    imgclass: "cow",
                    imgsrc: imgpath + "cow.png"
                }, {
                    imgclass: "flowers",
                    imgsrc: imgpath + "flowers.png"
                }, {
                    imgclass: "clothes01 cottonhig",
                    imgsrc: imgpath + "clothes01.png"
                }, {
                    imgclass: "cotton cottonhig",
                    imgsrc: imgpath + "cotton.png"
                }, {
                    imgclass: "corn-plant",
                    imgsrc: imgpath + "corn-plant.png"
                }, {
                    imgclass: "veg-plant",
                    imgsrc: imgpath + "veg-plant.png"
                }, {
                    imgclass: "basket bamboo",
                    imgsrc: imgpath + "bamboo-baskets.png"
                }

            ]
        }]

    }
];


// }

$(function() {
    var $board = $(".board");
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var globaltime = null;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "mrs_sharma", src: imgpath+"mrs_sharma.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "temple_bg", src: imgpath+"temple_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "swimming_bg", src: imgpath+"swimming_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "restaurant", src: imgpath+"restaurant.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "elephant_bg", src: imgpath+"elephant_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "school_bg", src: imgpath+"school_bg.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"title.ogg"},
			{id: "sound_1", src: soundAsset+"p1_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p1_s1.ogg"},
			{id: "sound_3", src: soundAsset+"p1_s2.ogg"},
			{id: "sound_4", src: soundAsset+"p1_s3.ogg"},
			//textboxes
			// {id: "dialog1_img", src: imgpath+'bubble-10.png', type: createjs.AbstractLoader.IMAGE},
			// {id: "bubblesc", src: imgpath+'bubblesc.png', type: createjs.AbstractLoader.IMAGE},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_0');
		// call main function
		templateCaller();
	}
	//initialize
	init();

  	var vocabcontroller =  new Vocabulary();
  	vocabcontroller.init();

    /*
    	inorder to use the handlebar partials we need to register them
    	to their respective handlebar partial pointer first
    */
    Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


    //controls the navigational state of the program
    /*======================================================
    =            Navigation Controller Function            =
    ======================================================*/
    /**
     How To:
     - Just call the navigation controller if it is to be called from except the
    	 last page of lesson
     - If called from last page set the islastpageflag to true such that
    	 footernotification is called for continue button to navigate to exercise
     */

    /**
    	 What it does:
    	 - If not explicitly overriden the method for navigation button
    		 controls, it shows the navigation buttons as required,
    		 according to the total count of pages and the countNext variable
    	 - If for a general use it can be called from the templateCaller
    		 function
    	 - Can be put anywhere in the template function as per the need, if
    		 so should be taken out from the templateCaller function
    	 - If the total page number is
    	*/

    function navigationcontroller(islastpageflag) {
        typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;


    }

	var timeoutcontroller;
    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);

        $board.html(html);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);
		vocabcontroller.findwords(countNext);
        switch (countNext) {
            case 0:
            sound_player('sound_'+countNext);
            case 1:
            	sound_player('sound_'+countNext);

            	break;
            case 2:
            	sound_player('sound_'+countNext);

                break;
            case 3:
            setTimeout(function(){
              sound_player('sound_'+countNext);
            },1000);

                $(".myNextStyle").css({
                    "z-index": "3"
                });
                break;
            case 4:
            	var houseflag = false;
                var fruitflag = false;
                var cottonhigflag = false;
                var firewoodflag = false;
                var bambooflag = false;
                var cowflag = false;
                var manflag = false;
                var cornplantflag = false;
                var vegplantflag = false;
                var flowersflag = false;
                var grass03flag = false;
				timeoutcontroller = setTimeout(function(){
	            	sound_player('sound_'+countNext);
            	}, 500);
                $(".house").hover(function() {
                    houseflag = true;
                    $(".house").toggleClass("expand");
                    $(".popup").toggleClass("popup-house ").text(data.string.p4text2).css({"top":"48%"});
                });
                $(".fruit").hover(function() {
                    fruitflag = true;
                    $(".fruit").toggleClass("expand");
                    $(".popup").toggleClass("popup-fruit").text(data.string.p19text2);
                });
                $(".cottonhig").hover(function() {
                    cottonhigflag = true;
                    $(".cottonhig").toggleClass("expand");
                    $(".popup").toggleClass("popup-cotton").text(data.string.p4text3);
                });
                $(".firewood").hover(function() {
                    firewoodflag = true;
                    $(".firewood").toggleClass("expand");
                    $(".popup").toggleClass("popup-firewood").text(data.string.p4text4);
                });
                $(".bamboo").hover(function() {
                    bambooflag = true;
                    $(".bamboo").toggleClass("expand");
                    $(".popup").toggleClass("popup-bamboo").text(data.string.p4text5);
                });
                $(".cow").hover(function() {
                    cowflag = true;
                    $(".cow").toggleClass("expand");
                    $(".popup").toggleClass("popup-foilage").text(data.string.p4text6);
                });
                $(".man").hover(function() {
                    manflag = true;
                    $(".man").toggleClass("expand");
                    $(".popup").toggleClass("popup-trunk").text(data.string.p4text7);
                });
                $(".corn-plant").hover(function() {
                    cornplantflag = true;
                    $(".corn-plant").toggleClass("expand");
                    $(".popup").toggleClass("popup-corn").text(data.string.p4text8);
                });
                $(".veg-plant").hover(function() {
                    vegplantflag = true;
                    $(".veg-plant").toggleClass("expand");
                    $(".popup").toggleClass("popup-veg").text(data.string.p4text9);
                });
                $(".flowers").hover(function() {
                    flowersflag = true;
                    $(".flowers").toggleClass("expand");
                    $(".popup").toggleClass("popup-flowers").text(data.string.p4text10);
                });
                $(".grass03").hover(function() {
                    grass03flag = true;
                    $(".grass03").toggleClass("expand");
                    $(".popup").toggleClass("popup-hay").text(data.string.p4text11);
                });
                if (houseflag == true && grass03flag == true && flowersflag == true && vegplantflag == true && cornplantflag == true && manflag == true && cowflag == true && bambooflag == true && cottonhigflag == true && firewoodflag == true && fruitflag == true) {
                    ole.footerNotificationHandler.pageEndSetNotification();
                }
                break;
        }
    }
    function nav_button_controls(delay_ms){
  		timeoutvar = setTimeout(function(){
  			if(countNext==0){
  				$nextBtn.show(0);
  			} else if( countNext>0 && countNext == $total_page-1){
  				$prevBtn.show(0);
  				ole.footerNotificationHandler.pageEndSetNotification();
  			} else{
  				$prevBtn.show(0);
  				$nextBtn.show(0);
  			}
  		},delay_ms);
  	}
  	function sound_player_nav(sound_id){
  		createjs.Sound.stop();
  		current_sound = createjs.Sound.play(sound_id);
  		current_sound.play();
  	}
  	function sound_player(sound_id){
  		createjs.Sound.stop();
  		current_sound = createjs.Sound.play(sound_id);
  		current_sound.play();
  		current_sound.on("complete", function(){
  			nav_button_controls(0);
  		});
  	}
    // Handlebars.registerHelper('listItem', function (from, to, context, options){
    // var item = "";
    // for (var i = from, j = to; i <= j; i++) {
    // item = item + options.fn(context[i]);
    // }
    // return item;
    // });

    function templateCaller() {
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

        loadTimelineProgress($total_page, countNext + 1);

        navigationcontroller();

        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


    }

    $nextBtn.on("click", function() {
    	clearTimeout(globaltime);
        countNext++;
        templateCaller();
    });
    /*
    	TODO: we need to add last page notification removal code on prevBtn pressed
    */
    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
    	clearTimeout(timeoutcontroller);
    	clearTimeout(globaltime);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });
    // setTimeout(function(){
    total_page = content.length;
    // }, 250);


});



/*===============================================
	 =            data highlight function            =
	 ===============================================*/
function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";


    if ($alltextpara.length > 0) {
        $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                (stylerulename = $(this).attr("data-highlightcustomclass")) :
                (stylerulename = "parsedstring");

            texthighlightstarttag = "<span>";


            replaceinstring = $(this).html();
            replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
            replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);


            $(this).html(replaceinstring);
        });
    }
}
/*=====  End of data highlight function  ======*/

//page 1

var imgpath = $ref + "/images/fvf/";
var imgpath2 = $ref + "/images/clothes/";
var soundAsset = $ref+"/audio_"+$lang+"/";
var content = [{
        //1st
        hasheaderblock: false,
        additionalclasscontentblock: "contentwithbg",
        addclass: "textonly",
        uppertextblock: [{
            texttoshow: [{
                textclass: "hightext animated  fadeInUp",
                textdata: data.string.p5text1
            }, {
                textclass: "hightext2 animated fadeIn",
                textdata: data.string.p5text2
            }]
        }]
    },
    //2nd
    {
        hasheaderblock: false,
        additionalclasscontentblock: "",
        addclass: "head2 animated zoomIn",
        uppertextblock: [{
            texttoshow: [{

                textdata: data.string.p19text2
            }]
        }],
        imageblock: [{
            imagetoshow: [{
                imgclass: "img1 animated fadeIn ",
                imgsrc: imgpath + "apple.jpg"
            }, {
                imgclass: "img2 animated fadeIn",
                imgsrc: imgpath + "orange.jpg"
            }, {
                imgclass: "img3 animated fadeIn",
                imgsrc: imgpath + "banana.jpg"
            }, {
                imgclass: "img4 animated fadeIn",
                imgsrc: imgpath + "grapes.jpg"
            }]
        }, {
            imagelabels: [{
                imagelabelclass: "img1lbl animated fadeInup",
                imagelabeldata: data.string.p6text1
            }, {
                imagelabelclass: "img2lbl animated fadeInup",
                imagelabeldata: data.string.p6text2
            }, {
                imagelabelclass: "img3lbl animated fadeInup",
                imagelabeldata: data.string.p6text3
            }, {
                imagelabelclass: "img4lbl animated fadeInup",
                imagelabeldata: data.string.p6text4
            }]
        }]
    },

    //3rd
    {

        hasheaderblock: true,
        additionalclasscontentblock: "",
        addclass: "head2 animated zoomIn",
        uppertextblock: [{
            texttoshow: [{

                textdata: data.string.p7text0
            }]
        }],
        imageblock: [{
            imagetoshow: [{
                imgclass: "img1 animated fadeIn",
                imgsrc: imgpath + "radish.jpg"
            }, {
                imgclass: "img2 animated fadeIn",
                imgsrc: imgpath + "carrot.jpg"
            }, {
                imgclass: "img3 animated fadeIn",
                imgsrc: imgpath + "onion01.jpg"
            }, {
                imgclass: "img4 animated fadeIn",
                imgsrc: imgpath + "turnip.jpg"
            }]
        }, {
            imagelabels: [{
                imagelabelclass: "img1lbl animated fadeInUp",
                imagelabeldata: data.string.p7text1
            }, {
                imagelabelclass: "img2lbl animated fadeInUp",
                imagelabeldata: data.string.p7text2
            }, {
                imagelabelclass: "img3lbl animated fadeInUp",
                imagelabeldata: data.string.p7text3
            }, {
                imagelabelclass: "img4lbl animated fadeInUp",
                imagelabeldata: data.string.p7text4
            }]
        }]
    },
    //4th
    {
        hasheaderblock: true,
        additionalclasscontentblock: "",
        addclass: "head2 animated zoomIn",
        uppertextblock: [{
            texttoshow: [{

                textdata: data.string.p8text0
            }]
        }],
        imageblock: [{
            imagetoshow: [{
                imgclass: "img1 animated fadeIn",
                imgsrc: imgpath + "onion.jpg"
            }, {
                imgclass: "img2 animated fadeIn",
                imgsrc: imgpath + "asparagus.jpg"
            }, {
                imgclass: "img3 animated fadeIn",
                imgsrc: imgpath + "bambooshoots.jpg"
            }, {
                imgclass: "img4 animated fadeIn",
                imgsrc: imgpath + "sugarcane.jpg"
            }]
        }, {
            imagelabels: [{
                imagelabelclass: "img1lbl animated fadeInUp",
                imagelabeldata: data.string.p8text1
            }, {
                imagelabelclass: "img2lbl animated fadeInUp",
                imagelabeldata: data.string.p8text2
            }, {
                imagelabelclass: "img3lbl shoots animated fadeInUp",
                imagelabeldata: data.string.p8text3
            }, {
                imagelabelclass: "img4lbl  animated fadeInUp",
                imagelabeldata: data.string.p8text4
            }]
        }]
    },
    //5th
    {
        hasheaderblock: true,
        additionalclasscontentblock: "",
        addclass: "head2 animated zoomIn",
        uppertextblock: [{
            texttoshow: [{

                textdata: data.string.p9text0
            }]
        }],
        imageblock: [{
            imagetoshow: [{
                imgclass: "img1 animated fadeIn",
                imgsrc: imgpath + "palak.jpg"
            }, {
                imgclass: "img2 animated fadeIn",
                imgsrc: imgpath + "cabbage.jpg"
            }, {
                imgclass: "img3 animated fadeIn",
                imgsrc: imgpath + "nettle.jpg"
            }, {
                imgclass: "img4 animated fadeIn",
                imgsrc: imgpath + "mint.jpg"
            }]
        }, {
            imagelabels: [{
                imagelabelclass: "img1lbl animated fadeInUp",
                imagelabeldata: data.string.p9text1
            }, {
                imagelabelclass: "img2lbl animated fadeInUp",
                imagelabeldata: data.string.p9text2
            }, {
                imagelabelclass: "img3lbl  animated fadeInUp",
                imagelabeldata: data.string.p9text3
            }, {
                imagelabelclass: "img4lbl  animated fadeInUp",
                imagelabeldata: data.string.p9text4
            }]
        }]
    },
    //6th
    {
        hasheaderblock: true,
        additionalclasscontentblock: "",
        addclass: "head2 animated zoomIn",
        uppertextblock: [{
            texttoshow: [{

                textdata: data.string.p10text0
            }]
        }],
        imageblock: [{
            imagetoshow: [{
                imgclass: "img1 animated fadeIn",
                imgsrc: imgpath + "rice.jpg"
            }, {
                imgclass: "img2 animated fadeIn",
                imgsrc: imgpath + "maize.jpg"
            }, {
                imgclass: "img3 animated fadeIn",
                imgsrc: imgpath + "millet.jpg"
            }, {
                imgclass: "img4 animated fadeIn",
                imgsrc: imgpath + "buckwheat.jpg"
            }]
        }, {
            imagelabels: [{
                imagelabelclass: "img1lbl animated fadeInUp",
                imagelabeldata: data.string.p10text1
            }, {
                imagelabelclass: "img2lbl animated fadeInUp",
                imagelabeldata: data.string.p10text2
            }, {
                imagelabelclass: "img3lbl  animated fadeInUp",
                imagelabeldata: data.string.p10text3
            }, {
                imagelabelclass: "img4lbl  animated fadeInUp",
                imagelabeldata: data.string.p10text4
            }]
        }]

    },
    //7th
    {
        hasheaderblock: true,
        additionalclasscontentblock: "",
        addclass: "midtext",
        uppertextblock: [{
            texttoshow: [{
                textdata: data.string.p11text1
            }]
        }],
        imageblock: [{
            imagetoshow: [{
                    imgclass: "img1a",
                    imgsrc: imgpath + "apple.jpg"
                }, {
                    imgclass: "img2a",
                    imgsrc: imgpath + "grapes.jpg"
                }, {
                    imgclass: "img3a",
                    imgsrc: imgpath + "carrot.jpg"
                }, {
                    imgclass: "img4a",
                    imgsrc: imgpath + "asparagus.jpg"
                }, {
                    imgclass: "img5a",
                    imgsrc: imgpath + "maize.jpg"
                }, {
                    imgclass: "img6a",
                    imgsrc: imgpath + "cabbage.jpg"
                }, {
                    imgclass: "img7a",
                    imgsrc: imgpath + "pea.jpg"
                }, {
                    imgclass: "img8a",
                    imgsrc: imgpath + "onion.jpg"
                }, {
                    imgclass: "img9a",
                    imgsrc: imgpath + "radish.jpg"
                }, {
                    imgclass: "img10a",
                    imgsrc: imgpath + "garlic.jpg"
                }

            ]
        }]

    }
];


$(function() {
    var $board = $(".board");
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

	var preload;
	var timeoutvar = null;
	var current_sound;

    function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "mrs_sharma", src: imgpath+"mrs_sharma.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "temple_bg", src: imgpath+"temple_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "swimming_bg", src: imgpath+"swimming_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "restaurant", src: imgpath+"restaurant.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "elephant_bg", src: imgpath+"elephant_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "school_bg", src: imgpath+"school_bg.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"p2_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p2_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p2_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p2_s3.ogg"},
			{id: "sound_4", src: soundAsset+"p2_s4.ogg"},
			{id: "sound_5", src: soundAsset+"p2_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p2_s6.ogg"},
			//textboxes
			// {id: "dialog1_img", src: imgpath+'bubble-10.png', type: createjs.AbstractLoader.IMAGE},
			// {id: "bubblesc", src: imgpath+'bubblesc.png', type: createjs.AbstractLoader.IMAGE},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_0');
		// call main function
		templateCaller();
	}
	//initialize
	init();
  	var vocabcontroller =  new Vocabulary();
  	vocabcontroller.init();


    /*
    	inorder to use the handlebar partials we need to register them
    	to their respective handlebar partial pointer first
    */
    Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

    /*======================================================
    =            Navigation Controller Function            =
    ======================================================*/
    /**
     How To:
     - Just call the navigation controller if it is to be called from except the
    	 last page of lesson
     - If called from last page set the islastpageflag to true such that
    	 footernotification is called for continue button to navigate to exercise
     */

    /**
    	 What it does:
    	 - If not explicitly overriden the method for navigation button
    		 controls, it shows the navigation buttons as required,
    		 according to the total count of pages and the countNext variable
    	 - If for a general use it can be called from the templateCaller
    		 function
    	 - Can be put anywhere in the template function as per the need, if
    		 so should be taken out from the templateCaller function
    	 - If the total page number is
    	*/

    function navigationcontroller(islastpageflag) {
        typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

        if ($total_page != 1) {
            $nextBtn.delay(800).show(0);
            $prevBtn.css('display', 'none');
        } else if ($total_page == 1) {
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');

            // if lastpageflag is true
            islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
        } else if (countNext > 0 && countNext < $total_page - 1) {
            $nextBtn.show(0);
            $prevBtn.show(0);
        } else if (countNext == $total_page - 1) {
            $nextBtn.css('display', 'none');
            $prevBtn.show(0);

            // if lastpageflag is true
            islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
        }
    }
    /*=====  End of user navigation controller function  ======*/
   var timeoutcontroller;
    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);

        $board.html(html);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);
		vocabcontroller.findwords(countNext);
    if(countNext>=1||countNext<=5){
      $(".img1,.img2,.img3,.img4").removeClass("fadeIn").hide();
      syncer();
    }
        switch (countNext) {
            case 0:
            	timeoutcontroller = setTimeout(function(){
	            	sound_player('sound_'+countNext);
            	}, 500);
                // $nextBtn.hide(0);
                // setTimeout(function(){
                //   $nextBtn.show(0);
                // },9500);

                break;
            case 1:
            case 4:
            case 5:
            	timeoutcontroller = setTimeout(function(){
	            	sound_player('sound_'+countNext);
            	}, 500);
                break;
            case 2:
            	timeoutcontroller = setTimeout(function(){
	            	sound_player('sound_'+countNext);
            	}, 500);
                $(".img1").css({
                    "left": "11%"
                });
                break;

            case 3:
            	timeoutcontroller = setTimeout(function(){
	            	sound_player('sound_'+countNext);
            	}, 500);
                $(".img2lbl").css({
                    "left": "35%"
                });
                break;
            case 6:
            	timeoutcontroller = setTimeout(function(){
	            	sound_player('sound_'+countNext);
            	}, 8000);
                $(".img1a").delay(1000).fadeIn(200);
                $(".img2a").delay(2000).fadeIn(200);
                $(".img3a").delay(3000).fadeIn(200);
                $(".img4a").delay(3800).fadeIn(200);
                $(".img5a").delay(4300).fadeIn(200);
                $(".img6a").delay(4800).fadeIn(200);
                $(".img7a").delay(5600).fadeIn(200);
                $(".img8a").delay(6400).fadeIn(200);
                $(".img9a").delay(7200).fadeIn(200);
                $(".img10a").delay(8000).fadeIn(200);

                $(".midtext").delay(8000).toggleClass("animated fadeIn").css({"width":"62%","left":"22%",
                    "font-size": "1.7em"
                }).fadeIn(function() {
                    if (countNext >= 5) {
                        ole.footerNotificationHandler.pageEndSetNotification();
                    }
                });

                break;
            default:
            	break;

        }
    }

    function syncer(){
      $(".img1").delay(1300).show(200);
      $(".img2").delay(2600).show(200);
      $(".img3").delay(3900).show(200);
      $(".img4").delay(5200).show(200);
    }
	function sound_player(sound_id,next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
    	current_sound.on('complete',()=>{
        if(!next){
          navigationcontroller();
        }
      });
	}
    function templateCaller() {
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

        loadTimelineProgress($total_page, countNext + 1);

        // navigationcontroller();

        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


    }

    $nextBtn.on("click", function() {
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
    	clearTimeout(timeoutcontroller);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

        total_page = content.length;


});



/*===============================================
	 =            data highlight function            =
	 ===============================================*/
function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";


    if ($alltextpara.length > 0) {
        $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                (stylerulename = $(this).attr("data-highlightcustomclass")) :
                (stylerulename = "parsedstring");

            texthighlightstarttag = "<span>";


            replaceinstring = $(this).html();
            replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
            replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);


            $(this).html(replaceinstring);
        });
    }
}
/*=====  End of data highlight function  ======*/

//page 1

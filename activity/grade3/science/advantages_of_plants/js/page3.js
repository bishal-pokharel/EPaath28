var imgpath = $ref + "/images/fvf/";
var imgpath2 = $ref + "/images/clothes/";
var imgpath3 = $ref + "/images/img/";
var soundAsset = $ref+"/audio_"+$lang+"/";
var content = [{
        hasheaderblock: true,
        additionalclasscontentblock: "bg-img",
        addclass: "midtext2 animated fadeInUp",
        uppertextblock: [{
            texttoshow: [{
                textdata: data.string.p12text1
            }]
        }],
    },

    {
        hasheaderblock: true,
        additionalclasscontentblock: "bg-img",
        addclass: "midtext2 animated fadeInUp",
        uppertextblock: [{
            texttoshow: [{
                textdata: data.string.p13text1
            }]
        }],
    },

    {

        hasheaderblock: true,
        additionalclasscontentblock: "bg-img",
        addclass: "head3",
        uppertextblock: [{
            texttoshow: [{
                textclass: "dialog",
                textdata: data.string.p14text1
            }, {
                textclass: "dialog2",
                textdata: data.string.p14text2
            }, {
                textclass: "dialog3",
                textdata: data.string.p14text3
            }, {
                textclass: "dialog4",
                textdata: data.string.p14text4
            }]
        }],
        imageblock: [{
            imagetoshow: [{
                imgclass: "cloud",
                imgsrc: imgpath3 + "cloud.png"
            }, {
                imgclass: "cotton",
                imgsrc: imgpath2 + "cotton-plant.jpg"
            }, {
                imgid: "cotton-zoom",
                imgsrc: imgpath2 + "cotton-zoom.jpg"
            }, {
                imgclass: "cottondrop",
                imgid: "cotton-shirt",
                imgsrc: imgpath2 + "cotton-shirt.png"
            }, {
                imgclass: "cottondrop",
                imgid: "cotton-pillow",
                imgsrc: imgpath2 + "cotton-pillow.png"
            }, {
                imgclass: "cottondrop",
                imgid: "cotton-dhaka",
                imgsrc: imgpath2 + "dhaka.png"
            }]
        }, {
            imagelabels: [{
                imagelabelclass: "dialog5",
                imagelabeldata: data.string.p14text5
            }]
        }],
    }, {
        hasheaderblock: true,
        additionalclasscontentblock: "bg-img2",
        addclass: "midtext2 animated fadeIn",
        uppertextblock: [{
            texttoshow: [{
                textdata: data.string.p15text1
            }]
        }],
    }, {
        hasheaderblock: true,
        additionalclasscontentblock: "bg-img2",
        newclass: "bg-img",
        uppertextblock: [{
            texttoshow: [{
                textclass: "plant-hemp animated fadeIn",
                textdata: data.string.p16text1
            }]
        }],
        imageblock: [{
            imagetoshow: [{
                imgclass: "cotton animated fadeIn",
                imgsrc: imgpath2 + "Hemp-Plants.jpg"
            }, {
                imgclass: "hemp-shirt",
                imgsrc: imgpath2 + "hemp-t-shirt.png"
            }]
        }]
    }, {
        hasheaderblock: true,
        additionalclasscontentblock: "bg-img3",
        uppertextblock: [{
            texttoshow: [{
                textclass: "plant-hemp animated fadeIn",
                textdata: data.string.p17text1
            }]
        }],
        imageblock: [{
            imagetoshow: [{
                imgclass: "cotton animated fadeIn",
                imgsrc: imgpath2 + "juteplant.jpg"
            }, {
                imgclass: "hemp-shirt",
                imgsrc: imgpath2 + "jute-bag.png"
            }]
        }]
    }, {
        hasheaderblock: true,
        additionalclasscontentblock: "bg-img4",
        uppertextblock: [{
            texttoshow: [{
                textclass: "plant-hemp animated fadeIn",
                textdata: data.string.p18text1
            }]
        }],
        imageblock: [{
            imagetoshow: [{
                imgclass: "cotton animated fadeIn",
                imgsrc: imgpath2 + "nettle-plant-closeup.jpg"
            }, {
                imgclass: "hemp-shirt",
                imgsrc: imgpath2 + "nettle-coat.png"
            }]
        }]
    }

];


$(function() {
    // var height = $(window).height();
    // var width = $(window).width();
    // $("#board").css({"width": width, "height": (height*580/960)});
    // function recursion(){
    // if(data.string != null){
    // } else{
    // recursion();
    // }
    // }
    // recursion();
    /*
    	TODO: do we need this??
    */
    $(window).resize(function() {
        recalculateHeightWidth();
    });
    /*
    	TODO: do we need this??
    */
    function recalculateHeightWidth() {
        var heightresized = $(window).height();
        var widthresized = $(window).width();
        var factor = 960 / 580;
        var equivalentwidthtoheight = widthresized / factor;

        if (heightresized >= equivalentwidthtoheight) {
            $(".shapes_activity").css({
                "width": widthresized,
                "height": equivalentwidthtoheight
            });
        } else {
            $(".shapes_activity").css({
                "height": heightresized,
                "width": heightresized * 960 / 580
            });
        }
        // $(".shapes_activity").css({"left": "50%" ,
        // "height": "50%" ,
        // "-webkit-transform" : "translate(-50%, - 50%)",
        // "transform" : "translate(-50%, - 50%)"});
    }
    var $board = $(".board");
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

  	var vocabcontroller =  new Vocabulary();
  	vocabcontroller.init();

    recalculateHeightWidth();

	var preload;
	var timeoutvar = null;
	var current_sound;

    function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "mrs_sharma", src: imgpath+"mrs_sharma.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "temple_bg", src: imgpath+"temple_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "swimming_bg", src: imgpath+"swimming_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "restaurant", src: imgpath+"restaurant.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "elephant_bg", src: imgpath+"elephant_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "school_bg", src: imgpath+"school_bg.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"p3_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p3_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p3_s2.ogg"},
			{id: "sound_2_1", src: soundAsset+"p3_s2_1.ogg"},
			{id: "sound_2_2", src: soundAsset+"p3_s2_2.ogg"},
			{id: "sound_2_3", src: soundAsset+"p3_s2_3.ogg"},
			{id: "sound_2_4", src: soundAsset+"p3_s2_4.ogg"},
			{id: "sound_3", src: soundAsset+"p3_s3.ogg"},
			{id: "sound_4", src: soundAsset+"p3_s4.ogg"},
			{id: "sound_5", src: soundAsset+"p3_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p3_s6.ogg"},
			//textboxes
			// {id: "dialog1_img", src: imgpath+'bubble-10.png', type: createjs.AbstractLoader.IMAGE},
			// {id: "bubblesc", src: imgpath+'bubblesc.png', type: createjs.AbstractLoader.IMAGE},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_0');
		// call main function
		templateCaller();
	}
	//initialize
	init();

    /*
    	inorder to use the handlebar partials we need to register them
    	to their respective handlebar partial pointer first
    */
    Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    /*======================================================
    =            Navigation Controller Function            =
    ======================================================*/
    /**
     How To:
     - Just call the navigation controller if it is to be called from except the
       last page of lesson
     - If called from last page set the islastpageflag to true such that
       footernotification is called for continue button to navigate to exercise
     */

    /**
        What it does:
        - If not explicitly overriden the method for navigation button
          controls, it shows the navigation buttons as required,
          according to the total count of pages and the countNext variable
        - If for a general use it can be called from the templateCaller
          function
        - Can be put anywhere in the template function as per the need, if
          so should be taken out from the templateCaller function
        - If the total page number is
       */

    function navigationcontroller(islastpageflag) {
        typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

        if (countNext == 0 && $total_page != 1) {
            $nextBtn.delay(800).show(0);
            $prevBtn.css('display', 'none');
        } else if ($total_page == 1) {
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');

            // if lastpageflag is true
            islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.lessonEndSetNotification();
        } else if (countNext > 0 && countNext < $total_page - 1) {
            $nextBtn.show(0);
            $prevBtn.show(0);
        } else if (countNext == $total_page - 1) {
            $nextBtn.css('display', 'none');
            $prevBtn.show(0);

            // if lastpageflag is true
            islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.lessonEndSetNotification();
        }
    }
    /*=====  End of user navigation controller function  ======*/
	var timeoutcontroller;
    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        /*
        	TODO: syntax and formatting
        */
        $board.html(html);
        $(".midtext2").css({
                "font-size": "1.7em"
           });
            // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);
		vocabcontroller.findwords(countNext);
        switch (countNext) {
        	case 0:
        	case 1:
        	case 3:
        		timeoutcontroller = setTimeout(function(){
	            	sound_player('sound_'+countNext);
            	}, 1000);
        		break;
            case 2:
                /*
                	TODO: now that you are comfortable can we move this portion to css??
                */
                timeoutcontroller = setTimeout(function(){
	            	sound_player('sound_'+countNext,1);
            	}, 2000);
                $(".midtext2").css({
                    "font-size": "1.5em"
                });
                $nextBtn.hide(0);
                $(".cotton").fadeIn(2000);
                $(".head3").delay(1500).fadeIn(2000);
                $("#cotton-zoom").delay(3000).fadeIn(10).animate({
                    'left': '70%',
                    'opacity': '1',
                }, 2000, function() {
                    $(".dialog2").toggleClass("animated fadeInUp");
					timeoutcontroller = setTimeout(function(){
						sound_player('sound_2_1',1);
                	}, 2000);
					// setTimeout(function(){
					// });
                    $("#cotton-shirt").delay(3000).fadeIn(10).animate({
                        'opacity': '1',
                        'top': '55%'
                    }, 2000, function() {
                    	timeoutcontroller = setTimeout(function(){
	                    	sound_player('sound_2_2',1);
                    	}, 2000);
                        $(".box1").fadeIn();
                        $(".dialog2").toggleClass("animated fadeOut");
                    });
                    $(".dialog3").toggleClass("animated fadeInUp");
                    $("#cotton-pillow").delay(7000).fadeIn(10).animate({
                        'left': '39%',
                        'top': '60%',
                        'opacity': '1',
                        'height': '17%',
                    }, 2000, function() {
                    	timeoutcontroller = setTimeout(function(){
	                    	sound_player('sound_2_3',1);
                    	}, 2000);
                        $(".box2").fadeIn();
                        $(".dialog3").toggleClass("animated fadeOut");
                    });
                    $(".dialog4").toggleClass("animated fadeInUp").animate({
                        'opacity': '1',
                        'top': '270%'
                    }, 2000, function() {
                        $("#cotton-dhaka").delay(9000).fadeIn(10).animate({
                            'left': '6%',
                            'top': '55%',
                            'opacity': '1'
                        }, 2000, function() {
                            $(".box3").fadeIn();
                            $(".dialog4").toggleClass("animated fadeOut");
	                    	timeoutcontroller = setTimeout(function(){
		                    	sound_player('sound_2_4');
	                    	}, 3000);
                            $('.dialog5,.cloud').toggleClass("animated lightSpeedIn");
                            $nextBtn.show(7000);
                        });

                    });

                });

                break;
            case 4:
            case 5:
            	timeoutcontroller = setTimeout(function(){
	            	sound_player('sound_'+countNext);
            	}, 500);
                $nextBtn.hide(0);
                $(".cotton").css({
                    'display': 'block',
                    'top': '12%'

                });
                $(".hemp-shirt").delay(1000).fadeIn(10).animate({
                    'left': '70%',
                    'opacity': '1',
                    'top': '18%'
                }, 1500);
                $("#activity-page-next-btn-enabled").delay(2000).fadeIn();
                break;

            case 6:
            	timeoutcontroller = setTimeout(function(){
	            	sound_player('sound_'+countNext);
            	}, 500);
                $(".cotton").css({
                    'display': 'block',
                    'top': '12%'

                });
                $(".hemp-shirt").delay(1000).fadeIn(10).animate({
                    'top': '18%',
                    'left': '70%',
                    'opacity': '1'
                }, 1500);

                if (countNext >= 5) {
                    ole.footerNotificationHandler.delay(1800).lessonEndSetNotification();
                }
                break;
        }
        function onDragElev(left) {

            if (left > -300) {
                left = 22;
            }
            return left;
        }
    }
    function sound_player(sound_id,next){
  		createjs.Sound.stop();
  		current_sound = createjs.Sound.play(sound_id);
  		current_sound.play();
      	current_sound.on('complete',()=>{
          if(!next){
            navigationcontroller();
          }
        });
  	}
    function templateCaller() {
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

        loadTimelineProgress($total_page, countNext + 1);
        // navigationcontroller();

        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


    }

    $nextBtn.on("click", function() {
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
    	clearTimeout(timeoutcontroller);
        countNext--;
        templateCaller();

				/* if footerNotificationHandler lessonEndSetNotification was called then on click of
				 previous slide button hide the footernotification */
				countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });
	total_page = content.length;


});



/*===============================================
	 =            data highlight function            =
	 ===============================================*/
function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";


    if ($alltextpara.length > 0) {
        $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                (stylerulename = $(this).attr("data-highlightcustomclass")) :
                (stylerulename = "parsedstring");

            texthighlightstarttag = "<span>";


            replaceinstring = $(this).html();
            replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
            replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);


            $(this).html(replaceinstring);
        });
    }
}
/*=====  End of data highlight function  ======*/

//page 1

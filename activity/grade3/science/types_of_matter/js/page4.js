var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+'/';

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-p1',
		extratextblock:[{
			textclass: 'bottom-bar',
			textdata: '',
		},{
			textclass: 'drop-ins',
			textdata: data.string.p4text1,
		},{
			textclass: 'drop-t1 drop-obj-name',
			textdata: data.string.p4item1,
		},{
			textclass: 'drop-t2 drop-obj-name',
			textdata: data.string.p4item2,
		},{
			textclass: 'drop-t3 drop-obj-name',
			textdata: data.string.p4item3,
		},{
			textclass: 'drop-t4 drop-obj-name',
			textdata: data.string.p4item4,
		},{
			textclass: 'drop-t5 drop-obj-name',
			textdata: data.string.p4item5,
		},{
			textclass: 'drop-t6 drop-obj-name',
			textdata: data.string.p4item6,
		},{
			textclass: 'drop-t7 drop-obj-name',
			textdata: data.string.p4item7,
		},{
			textclass: 'drop-t8 drop-obj-name',
			textdata: data.string.p4item8,
		}],
		imageblock : [{
			imagestoshow : [{
				imgclass : "monkey-fb",
				imgsrc : '',
				imgid : 'monkey'
			},{
				imgclass : "drop-obj soluble drop-1",
				imgsrc : '',
				imgid : 'sugar'
			},{
				imgclass : "drop-obj insoluble drop-2",
				imgsrc : '',
				imgid : 'sand'
			},{
				imgclass : "drop-obj soluble drop-3",
				imgsrc : '',
				imgid : 'salt'
			},{
				imgclass : "drop-obj insoluble drop-4",
				imgsrc : '',
				imgid : 'wooddust'
			},{
				imgclass : "drop-obj soluble drop-5",
				imgsrc : '',
				imgid : 'jiwanjal'
			},{
				imgclass : "drop-obj insoluble sink drop-6",
				imgsrc : '',
				imgid : 'brick'
			},{
				imgclass : "drop-obj insoluble sink drop-7",
				imgsrc : '',
				imgid : 'nail'
			},{
				imgclass : "drop-obj soluble drop-8",
				imgsrc : '',
				imgid : 'glucose'
			}]
		}],
		svgblock: [{
			svgblock: 'svg-drop-1',
			textclass: 'beaker-label',
			textdata: data.string.p4text2
		},{
			svgblock: 'svg-drop-2',
			textclass: 'beaker-label',
			textdata: data.string.p4text3
		}],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-p1',
		uppertextblockadditionalclass: 'last-text lt-1',
		uppertextblock:[{
			textclass: 'wrong-btn',
			textdata: '&#x2716;',
		}, {
			textclass: '',
			textdata: data.string.p4text4,
			datahighlightflag : true,
			datahighlightcustomclass : 'font-yellow',
		}],
		lowertextblockadditionalclass: 'last-text lt-2',
		lowertextblock:[{
			textclass: 'wrong-btn',
			textdata: '&#x2716;',
		}, {
			textclass: '',
			textdata: data.string.p4text5,
			datahighlightflag : true,
			datahighlightcustomclass : 'font-yellow',
		}],
		extratextblock:[{
			textclass: 'bottom-bar',
			textdata: '',
		}],
		imageblock : [{
			imagestoshow : [{
				imgclass : "drop-obj dropped-1",
				imgsrc : '',
				imgid : 'brick'
			},{
				imgclass : "drop-obj dropped-2",
				imgsrc : '',
				imgid : 'nail'
			}]
		}],
		svgblock: [{
			svgblock: 'svg-drop-1',
			textclass: 'beaker-label',
			textdata: data.string.p4text2,
			handclass: 'hand-1'
		},{
			svgblock: 'svg-drop-2',
			textclass: 'beaker-label',
			textdata: data.string.p4text3,
			handclass: 'hand-2'
		}],
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var first = 0;
	var second = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "hand", src: "images/hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "beaker-svg", src: imgpath+"diy2/beaker01.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "beaker-svg-2", src: imgpath+"diy2/beaker02.svg", type: createjs.AbstractLoader.IMAGE},

			{id: "jiwanjal", src: imgpath+"diy2/jeeban-jal.png", type: createjs.AbstractLoader.IMAGE},
			{id: "brick", src: imgpath+"diy2/stone.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sand", src: imgpath+"diy2/sand.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sugar", src: imgpath+"diy2/sugar.png", type: createjs.AbstractLoader.IMAGE},
			{id: "salt", src: imgpath+"diy2/salt.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wooddust", src: imgpath+"diy2/woodpowder.png", type: createjs.AbstractLoader.IMAGE},
			{id: "nail", src: imgpath+"diy2/nail.png", type: createjs.AbstractLoader.IMAGE},
			{id: "glucose", src: imgpath+"diy2/glucose.png", type: createjs.AbstractLoader.IMAGE},
			{id: "monkey", src: "images/sundar/correct-1.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_1", src: soundAsset+"p4_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p4_s1.ogg"},
			{id: "sound_3", src: soundAsset+"p4_s1_1.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);

		switch(countNext) {
			case 0:
				sound_player('sound_1');
				var s = Snap('#svg-drop-1');
				var s2 = Snap('#svg-drop-2');
				var count = 1;
				var insoluble_count = 0;
				var soluble_count = 0;
				$('.drop-1').show(0);
				$('.drop-t1').fadeIn(500);
				$('.drop-1').addClass('current-display');

				$('#svg-drop-1').click(function(){
					if($('.current-display').hasClass('soluble')){
						play_correct_incorrect_sound(1);
						$('.current-display').addClass('soluble-drop-anim');
						$('.drop-t'+count).fadeOut(500);
						timeoutvar = setTimeout(function(){
							switch(soluble_count){
								case 0:
								case 1:
									dissolve( '#FFFFFF', false);
									break;
								case 2:
									dissolve( '#FFB380', true);
									break;
								case 3:
									dissolve( '#F9F3F2', true);
									break;
							}
							$('.drop-'+count).removeClass('current-display');
							count++;
							soluble_count++;
							if(count>8){
								$nextBtn.show(0);
								$('.monkey-fb').show(0);
							} else{
								$('.drop-'+count).show(0);
								$('.drop-t'+count).fadeIn(500);
								$('.drop-'+count).addClass('current-display');
								$('#svg-drop-1, #svg-drop-2').css('pointer-events', 'all');
							}
						}, 2000);
						$('#svg-drop-1, #svg-drop-2').css('pointer-events', 'none');
					} else{
						play_correct_incorrect_sound(0);
						$(this).css('pointer-events', 'none');
					}
				});
				$('#svg-drop-2').click(function(){
					if($('.current-display').hasClass('insoluble')){
						play_correct_incorrect_sound(1);
						$('.drop-t'+count).fadeOut(500);
						if(insoluble_count == 2){
							$('.current-display').addClass('insoluble-drop-anim-1');
						} else if(insoluble_count == 3){
							$('.current-display').addClass('insoluble-drop-anim-2');
						} else{
							$('.current-display').addClass('insoluble-drop-anim');
						}
						timeoutvar = setTimeout(function(){
							switch(insoluble_count){
								case 0:
									go_down( '#C5914D');
									break;
								case 1:
									go_up( '#FFDFAC');
									break;
								case 2:
									raise_water_1();
									break;
								case 3:
									raise_water_2();
									break;
							}
							$('.drop-'+count).removeClass('current-display');
							count++;
							insoluble_count++;
							if(count>8){
								$nextBtn.show(0);
								$('.monkey-fb').show(0);
							} else{
								$('.drop-'+count).show(0);
								$('.drop-t'+count).fadeIn(500);
								$('.drop-'+count).addClass('current-display');
								$('#svg-drop-1, #svg-drop-2').css('pointer-events', 'all');
							}
						}, 2000);
						$('#svg-drop-1, #svg-drop-2').css('pointer-events', 'none');
					} else{
						play_correct_incorrect_sound(0);
						$(this).css('pointer-events', 'none');
					}
				});
				//svg1 var
				var p1, path1, p2, path2, p3, path3, poly;
				var pp1, ppath1, ppath2, ppath3, pup, ppup, pdown, ppdown, ppoly;
				var svg = Snap.load(preload.getResult('beaker-svg').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					//to resive whole svg
					p1 = Snap.select('#st-1');
					path1 = p1.attr('path');
					p2 = Snap.select('#st-2');
					path2 = p2.attr('path');
					p3 = Snap.select('#st-3');
					path3 = p3.attr('path');
					poly = Snap.select('#st-water');
				} );
				var svg2 = Snap.load(preload.getResult('beaker-svg-2').src, function ( loadedFragment ) {
					s2.append(loadedFragment);
					//to resive whole svg
					pp1 = Snap.select('#p-1');
					ppoly = Snap.select('#p-water');
					ppath1 = pp1.attr('path');
					var pp2 = Snap.select('#p-2');
					ppath2 = pp2.attr('path');
					var pp3 = Snap.select('#p-3');
					ppath3 = pp3.attr('path');
					pup = Snap.select('#p-up');
					ppup = pup.attr('path');
					pdown = Snap.select('#p-down');
					ppdown = pdown.attr('path');
				} );
				function dissolve(final_color, update_water){
					p1.attr({ d: p1, 'fill':final_color});
					p1.animate({ opacity: 0.5}, 600, mina.linear, to_p2());
					function to_p2(){
						p1.animate({ d: path2}, 600, mina.linear, to_p3);
					}
					function to_p3(){
						p1.animate({ d: path3}, 600, mina.linear, to_p4);
					}
					function to_p4(){
						p1.animate({ opacity: 0}, 600, mina.linear, final_fun);
						if(update_water) poly.attr({'fill': final_color, 'opacity':'0.6'});
					}
					function final_fun(){
						p1.attr({d: path1});
					}
				}
				function go_down(final_color){
					pp1.attr({ d: p1, 'fill':final_color});
					pp1.animate({ opacity: 0.8}, 600, mina.linear, to_pp2());
					function to_pp2(){
						pp1.animate({ d: ppath2}, 600, mina.linear, to_pp3);
					}
					function to_pp3(){
						pp1.animate({ d: ppath3}, 600, mina.linear, to_pp4);
					}
					function to_pp4(){
						pp1.animate({ d: ppdown, 'opacity': '1'}, 1200, mina.linear, final_pfun);
					}
					function final_pfun(){
						pp1.attr({ d: p1, 'opacity': '0'});
						pdown.attr({ display: 'block'});
						pdown.attr('fill',final_color);
					}
				}
				function go_up(final_color){
					pp1.attr({ d: p1, 'fill':final_color});
					pp1.animate({ opacity: 0.8}, 600, mina.linear, to_pp2());
					function to_pp2(){
						pp1.animate({ d: ppath2}, 600, mina.linear, to_pp3);
					}
					function to_pp3(){
						pp1.animate({ d: ppath3}, 600, mina.linear, to_pp4);
					}
					function to_pp4(){
						pp1.animate({ d: ppup, 'opacity': '1'}, 1200, mina.linear, final_pfun);
					}
					function final_pfun(){
						pp1.attr({ d: p1, 'opacity': '0'});
						pup.attr({ display: 'block'});
						pup.attr('fill',final_color);
					}
				}
				function raise_water_1(){
					var newpath = "M521.437,289.78c-84.184,6.195-343.832,18.649-411.832-1.351c-35.703-10.501-14.117,22.126-13-49.844c0.333-21.489,7.333,50.845,0.333,34.511c-6.39-14.91-15.312-16.956,4.29-21.334c51.203-11.436,169.224,1.387,204.377-2.333c63-6.667,80.63,6.939,177.107-2.151c107.195-10.102,329.224,1.647,383.893-0.837c74.082-3.366,47-8.011,53.467,20.001c2.308,9.992,0.89,11.285-0.467,14.987c-12.95,35.346-195,10-275,17C596.729,302.619,605.62,283.585,521.437,289.78z";
					ppoly.animate({ height: 550, y:275}, 600, mina.linear);
					pup.animate({ d: newpath}, 600, mina.linear);
				}
				function raise_water_2(){
					var newpath = "M521.437,239.78c-84.184,6.195-343.832,18.649-411.832-1.351c-35.703-10.501-14.117,22.126-13-49.844c0.333-21.489,7.333,50.845,0.333,34.511c-6.39-14.91-15.312-16.956,4.29-21.334c51.203-11.436,169.224,1.387,204.377-2.333c63-6.667,80.63,6.939,177.107-2.151c107.195-10.102,329.224,1.647,383.893-0.837c74.082-3.366,47-8.011,53.467,20.001c2.308,9.992,0.89,11.285-0.467,14.987c-12.95,35.346-195,10-275,17C596.729,252.619,605.62,233.585,521.437,239.78z";
					ppoly.animate({ height: 600, y:225}, 400, mina.linear);
					pup.animate({ d: newpath}, 300, mina.linear);
				}
				break;
			case 1:
				$prevBtn.show(0);
				var clicks = [0,0];
				$('.handclass').attr('src', preload.getResult('hand').src);
				var s = Snap('#svg-drop-1');
				var s2 = Snap('#svg-drop-2');
				var svg = Snap.load(preload.getResult('beaker-svg').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					var poly = Snap.select('#st-water');
					poly.attr({opacity:"0.6", fill:"#f9f3f2"});
				} );
				var svg2 = Snap.load(preload.getResult('beaker-svg-2').src, function ( loadedFragment ) {
					s2.append(loadedFragment);
					//to resive whole svg
					var pup = Snap.select('#p-up');
					var pdown = Snap.select('#p-down');
					var ppoly = Snap.select('#p-water');
					var newpath = "M521.437,239.78c-84.184,6.195-343.832,18.649-411.832-1.351c-35.703-10.501-14.117,22.126-13-49.844c0.333-21.489,7.333,50.845,0.333,34.511c-6.39-14.91-15.312-16.956,4.29-21.334c51.203-11.436,169.224,1.387,204.377-2.333c63-6.667,80.63,6.939,177.107-2.151c107.195-10.102,329.224,1.647,383.893-0.837c74.082-3.366,47-8.011,53.467,20.001c2.308,9.992,0.89,11.285-0.467,14.987c-12.95,35.346-195,10-275,17C596.729,252.619,605.62,233.585,521.437,239.78z";
					ppoly.attr({ height: 600, y:225});
					pup.attr({ display: 'block', 'path':newpath, fill:'#FFDFAC'});
					pdown.attr({ display: 'block', fill: '#C5914D'});
				} );
				$('.wrong-btn').click(function(){
					createjs.Sound.stop();
					$(this).parent().fadeOut(500);
					$('.hand-2').show(0);
					$('.hand-1').show(0);
				});
				$('#svg-drop-1').click(function(){
					$('.lt-1').show(0);
					$('.hand-1').hide(0);
					clicks[0] = 1;
					first = 1;
					setTimeout(function(){
					sound_player_control('sound_2', first, second);
					},1100);

					// $('.hand-1').hide(0);
				});
				$('#svg-drop-2').click(function(){
					$('.lt-2').show(0);
					clicks[1] = 1;
					second = 1;
					$('.hand-2').hide(0);
					setTimeout(function(){
					sound_player_control('sound_3', first, second);
				},1100);

					// $('.hand-2').hide(0);
				});
				break;
			default:
				$prevBtn.show(0);
				sound_nav('sound_2');
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function sound_player_control(sound_id,first,second){
		$('.wrong-btn').css({"pointer-events":"none"});
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			$('.wrong-btn').css({"pointer-events":"auto"});
			if(first==1&&second==1){
					$('.wrong-btn').click(()=>ole.footerNotificationHandler.pageEndSetNotification());
			}
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageDiv = content[count].imagedivblock;
			for(var i=0; i<imageDiv.length; i++){
				var imageClass = imageDiv[i].image;
				for(var j=0; j<imageClass.length; j++){
					var image_src = preload.getResult(imageClass[j].imgid).src;
					//get list of classes
					var classes_list = imageClass[j].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

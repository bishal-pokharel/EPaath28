var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/" + $lang + '/';


var content = [
  // slide0
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: '',

    imagedivblock: [{
        imagediv: 'top-bar bar-1',
        image: [{
            imgclass: "bar-img-1",
            imgsrc: '',
            imgid: 'leaf',
            imagelabelclass: 'bar-text',
            imagelabeldata: data.string.p2text2,
          },
          {
            imgclass: "bar-img-1a",
            imgsrc: '',
            imgid: 'egg',
          }
        ]
      },
      {
        imagediv: 'top-bar bar-2',
        image: [{
            imgclass: "bar-img-2",
            imgsrc: '',
            imgid: 'glass-1',
            imagelabelclass: 'bar-text',
            imagelabeldata: data.string.p2text3,
          },
          {
            imgclass: "bar-img-2a",
            imgsrc: '',
            imgid: 'glass-2',
          }
        ]
      },
      {
        imagediv: 'top-bar bar-3',
        image: [{
            imgclass: "bar-img-3",
            imgsrc: '',
            imgid: 'torch-1',
            imagelabelclass: 'bar-text',
            imagelabeldata: data.string.p2text4,
          },
          {
            imgclass: "bar-img-3a",
            imgsrc: '',
            imgid: 'torch-2',
          }
        ]
      },
    ]
  },
  // slide1
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: '',

    imagedivblock: [{
        imagediv: 'top-bar bar-1 inactive-bar-anim-2',
        image: [{
            imgclass: "bar-img-1",
            imgsrc: '',
            imgid: 'leaf',
            imagelabelclass: 'bar-text',
            imagelabeldata: data.string.p2text2,
          },
          {
            imgclass: "bar-img-1a",
            imgsrc: '',
            imgid: 'egg',
          }
        ]
      },
      {
        imagediv: 'top-bar bar-2 active-bar-anim',
        image: [{
            imgclass: "bar-img-2",
            imgsrc: '',
            imgid: 'glass-1',
            imagelabelclass: 'bar-text',
            imagelabeldata: data.string.p2text3,
          },
          {
            imgclass: "bar-img-2a",
            imgsrc: '',
            imgid: 'glass-2',
          }
        ]
      },
      {
        imagediv: 'top-bar bar-3 inactive-bar-anim',
        image: [{
            imgclass: "bar-img-3",
            imgsrc: '',
            imgid: 'torch-1',
            imagelabelclass: 'bar-text',
            imagelabeldata: data.string.p2text4,
          },
          {
            imgclass: "bar-img-3a",
            imgsrc: '',
            imgid: 'torch-2',
          }
        ]
      },
    ]
  },
  // slide2
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: 'bg-p1',
    extratextblock: [{
      textclass: 'bottom-bar',
      textdata: '',
    }, {
      textclass: 'instruction',
      textdata: data.string.p3text1,
    }],
    imageblock: [{
      imagestoshow: [{
          imgclass: "spoon",
          imgsrc: '',
          imgid: 'spoon-1'
        },
        {
          imgclass: "arrow",
          imgsrc: '',
          imgid: 'arrow'
        },
        {
          imgclass: "beaker",
          imgsrc: '',
          imgid: 'beaker'
        }
      ]
    }]
  },
  // slide3
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: 'bg-p1',
    extratextblock: [{
      textclass: 'bottom-bar',
      textdata: '',
    }, {
      textclass: 'instruction',
      textdata: data.string.p3text2,
    }, {
      textclass: 'option opt-1',
      textdata: data.string.p3text3,
    }, {
      textclass: 'option opt-2',
      textdata: data.string.p3text4,
    }],
    imageblock: [{
      imagestoshow: [{
          imgclass: "spoon",
          imgsrc: '',
          imgid: 'spoon'
        },
        {
          imgclass: "beaker",
          imgsrc: '',
          imgid: 'beaker'
        },
        {
          imgclass: "cor-1",
          imgsrc: '',
          imgid: 'incorrect'
        },
        {
          imgclass: "cor-2",
          imgsrc: '',
          imgid: 'incorrect'
        }
      ]
    }]
  },
  // slide4
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: 'bg-p1',
    extratextblock: [{
      textclass: 'bottom-bar',
      textdata: '',
    }, {
      textclass: 'instruction',
      textdata: data.string.p3text6,
    }],
    imageblock: [{
      imagestoshow: [{
          imgclass: "spoon",
          imgsrc: '',
          imgid: 'spoon-2'
        },
        {
          imgclass: "arrow",
          imgsrc: '',
          imgid: 'arrow'
        },
        {
          imgclass: "beaker",
          imgsrc: '',
          imgid: 'beaker'
        }
      ]
    }]
  },
  // slide5
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: 'bg-p1',
    extratextblock: [{
      textclass: 'bottom-bar',
      textdata: '',
    }, {
      textclass: 'instruction',
      textdata: data.string.p3text7,
    }, {
      textclass: 'option opt-1',
      textdata: data.string.p3text3,
    }, {
      textclass: 'option opt-2',
      textdata: data.string.p3text4,
    }],
    imageblock: [{
      imagestoshow: [{
          imgclass: "spoon",
          imgsrc: '',
          imgid: 'spoon'
        },
        {
          imgclass: "beaker",
          imgsrc: '',
          imgid: 'beaker-2'
        },
        {
          imgclass: "cor-1",
          imgsrc: '',
          imgid: 'incorrect'
        },
        {
          imgclass: "cor-2",
          imgsrc: '',
          imgid: 'incorrect'
        }
      ]
    }]
  }
];


$(function() {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page, countNext + 1);
  // readCSV();
  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();

  //for preload
  var preload;
  var timeoutvar = null;
  var current_sound;

  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      //images
      {
        id: "hand",
        src: "images/hand-icon.gif",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "correct",
        src: "images/correct.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "incorrect",
        src: "images/wrong.png",
        type: createjs.AbstractLoader.IMAGE
      },

      {
        id: "leaf",
        src: imgpath + "leaf.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "egg",
        src: imgpath + "egg.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "torch-1",
        src: imgpath + "torch01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "torch-2",
        src: imgpath + "torch02.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "glass-1",
        src: imgpath + "glass01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "glass-2",
        src: imgpath + "glass02.png",
        type: createjs.AbstractLoader.IMAGE
      },

      {
        id: "arrow",
        src: imgpath + "page3/arrowgreen.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "spoon",
        src: imgpath + "page3/spoon-blank.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "spoon-1",
        src: imgpath + "page3/sugarspoon.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "beaker",
        src: imgpath + "page3/sugarmix.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "beaker-gif",
        src: imgpath + "page3/sugarwatermix.gif",
        type: createjs.AbstractLoader.IMAGE
      },

      {
        id: "spoon-2",
        src: imgpath + "page3/sandspoon.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "beaker-2",
        src: imgpath + "page3/sandmix.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "beaker-gif-2",
        src: imgpath + "page3/sand.gif",
        type: createjs.AbstractLoader.IMAGE
      },

      // sounds
      {
        id: "sound_0",
        src: soundAsset + "p2_s1.ogg"
      },
      {
        id: "sound_1",
        src: soundAsset + "p3_s1.ogg"
      },
      {
        id: "sound_2",
        src: soundAsset + "p3_s2.ogg"
      },
      {
        id: "sound_3",
        src: soundAsset + "p3_s3.ogg"
      },
      {
        id: "sound_4",
        src: soundAsset + "p3_s3_1.ogg"
      },
      {
        id: "sound_5",
        src: soundAsset + "p3_s4.ogg"
      },
      {
        id: "sound_6",
        src: soundAsset + "p3_s5.ogg"
      },
      {
        id: "sound_7",
        src: soundAsset + "p3_s5_1.ogg"
      },

    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound); //for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }

  function handleFileLoad(event) {
    // console.log(event.item);
  }

  function handleProgress(event) {
    $('#loading-text').html(parseInt(event.loaded * 100) + '%');
  }

  function handleComplete(event) {
    $('#loading-wrapper').hide(0);
    //initialize varibales
    // call main function
    templateCaller();
  }
  //initialize
  init();

  /*==================================================
  =            Handlers and helpers Block            =
  ==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  /*===============================================
  =            data highlight function            =
  ===============================================*/
  function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";
    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
         use that or else use default 'parsedstring'*/
        $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
          (stylerulename = $(this).attr("data-highlightcustomclass")) : (stylerulename = "parsedstring");

        texthighlightstarttag = "<span class='" + stylerulename + "'>";
        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
        $(this).html(replaceinstring);
      });
    }
  }
  /*=====  End of data highlight function  ======*/

  /*===============================================
   =            user notification function        =
   ===============================================*/
  function notifyuser($notifyinside) {
    //check if $notifyinside is provided
    typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

    /*variable that will store the element(s) to remove notification from*/
    var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
    // if there are any notifications removal required add the event handler
    if ($allnotifications.length > 0) {
      $allnotifications.one('click', function() {
        /* Act on the event */
        $(this).attr('data-isclicked', 'clicked');
        $(this).removeAttr('data-usernotification');
      });
    }
  }

  /*=====  End of user notification function  ======*/

  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/

  function navigationcontroller(islastpageflag) {
    typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

    // islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
  }

  /*=====  End of user navigation controller function  ======*/

  /*==================================================
   =            InstructionBlockController            =
   ==================================================*/

  function instructionblockcontroller() {
    var $instructionblock = $board.find("div.instructionblock");
    if ($instructionblock.length > 0) {
      var $contentblock = $board.find("div.contentblock");
      var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
      var instructionblockisvisibleflag;

      $contentblock.css('pointer-events', 'none');

      $toggleinstructionblockbutton.on('click', function() {
        instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
        if (instructionblockisvisibleflag == 'true') {
          instructionblockisvisibleflag = 'false';
          $contentblock.css('pointer-events', 'auto');
        } else if (instructionblockisvisibleflag == 'false') {
          instructionblockisvisibleflag = 'true';
          $contentblock.css('pointer-events', 'none');
        }

        $instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
      });
    }
  }

  /*=====  End of InstructionBlockController  ======*/

  /*=================================================
   =            general template function            =
   =================================================*/
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    vocabcontroller.findwords(countNext);
    put_image(content, countNext);
    put_image2(content, countNext);

    switch (countNext) {
      case 0:
        $nextBtn.show(0);
        break;
      case 2:
        $prevBtn.show(0);
        sound_player_spoon('sound_2');
        $('.arrow').click(function() {
          $(this).hide(0);
          $('.spoon').attr('src', preload.getResult('spoon').src);
          $('.beaker').attr('src', preload.getResult('beaker-gif').src + '?a=' + Math.random());
          nav_button_controls(2000);
        });
        break;
      case 3:
        $prevBtn.show(0);
        sound_player('sound_3');
        $('.option').click(function() {
          if ($(this).hasClass('opt-1')) {
            // play_correct_incorrect_sound(1);
            $(this).css('background-color', '#6AA84F');
            $('.option').css('pointer-events', 'none');
            $('.cor-1').attr('src', preload.getResult('correct').src);
            $('.cor-1').show(0);
            $('.instruction').fadeOut(500, function() {
              $('.instruction').html(data.string.p3text5);
              $('.instruction').fadeIn(500, function() {
                sound_nav('sound_4');
              });
            });
          } else {
            play_correct_incorrect_sound(0);
            $(this).css({
              'background-color': '#E06666',
              'pointer-events': 'none'
            });
            $('.cor-2').show(0);
          }
        });
        break;
      case 4:
        $prevBtn.show(0);
        sound_player_spoon('sound_5');
        $('.arrow').click(function() {
          $(this).hide(0);
          $('.spoon').attr('src', preload.getResult('spoon').src);
          $('.beaker').attr('src', preload.getResult('beaker-gif-2').src + '?a=' + Math.random());
          nav_button_controls(2000);
        });
        break;
      case 5:
        $prevBtn.show(0);
        sound_player('sound_6');
        $('.option').click(function() {
          if ($(this).hasClass('opt-2')) {
            // play_correct_incorrect_sound(1);
            $(this).css('background-color', '#6AA84F');
            $('.option').css('pointer-events', 'none');
            $('.cor-2').attr('src', preload.getResult('correct').src);
            $('.cor-2').show(0);
            $('.instruction').fadeOut(500, function() {
              $('.instruction').html(data.string.p3text8);
              $('.instruction').fadeIn(500, function() {
                sound_nav('sound_7');
              });
            });
          } else {
            play_correct_incorrect_sound(0);
            $(this).css({
              'background-color': '#E06666',
              'pointer-events': 'none'
            });
            $('.cor-1').show(0);
          }
        });
        break;
      default:
        $prevBtn.show(0);
        sound_nav('sound_' + countNext);
        break;
    }
  }

  function nav_button_controls(delay_ms) {
    timeoutvar = setTimeout(function() {
      if (countNext == 0) {
        $nextBtn.show(0);
      } else if (countNext > 0 && countNext == $total_page - 1) {
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else {
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    }, delay_ms);
  }

  function sound_player(sound_id) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
  }

  function sound_player_spoon(sound_id) {
    $('.arrow').css({
      "pointer-events": "none"
    });
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on("complete", function() {
      $('.arrow').css({
        "pointer-events": "auto"
      });
    });
  }

  function sound_nav(sound_id) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on("complete", function() {
      nav_button_controls(0);
    });
  }

  function put_image(content, count) {
    if (content[count].hasOwnProperty('imageblock')) {
      var imageblock = content[count].imageblock[0];
      if (imageblock.hasOwnProperty('imagestoshow')) {
        var imageClass = imageblock.imagestoshow;
        for (var i = 0; i < imageClass.length; i++) {
          var image_src = preload.getResult(imageClass[i].imgid).src;
          //get list of classes
          var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
          var selector = ('.' + classes_list[classes_list.length - 1]);
          $(selector).attr('src', image_src);
        }
      }
    }
  }

  function put_image2(content, count) {
    if (content[count].hasOwnProperty('imagedivblock')) {
      var imageDiv = content[count].imagedivblock;
      for (var i = 0; i < imageDiv.length; i++) {
        var imageClass = imageDiv[i].image;
        for (var j = 0; j < imageClass.length; j++) {
          var image_src = preload.getResult(imageClass[j].imgid).src;
          //get list of classes
          var classes_list = imageClass[j].imgclass.match(/\S+/g) || [];
          var selector = ('.' + classes_list[classes_list.length - 1]);
          $(selector).attr('src', image_src);
        }
      }
    }
  }

  function templateCaller() {
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    navigationcontroller();

    generaltemplate();
    loadTimelineProgress($total_page, countNext + 1);
  }

  $nextBtn.on('click', function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    switch (countNext) {
      default:
        countNext++;
        templateCaller();
        break;
    }
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on('click', function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    countNext--;
    templateCaller();
    /* if footerNotificationHandler pageEndSetNotification was called then on click of
     previous slide button hide the footernotification */
    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
  });

});

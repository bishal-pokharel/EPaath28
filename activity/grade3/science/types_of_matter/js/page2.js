var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/" + $lang + '/';


var content = [
  // slide0
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: '',

    imagedivblock: [{
      imagediv: 'bg-full',
      image: [{
        imgclass: "bg-full",
        imgsrc: '',
        imgid: 'front-bg',
        imagelabelclass: 'lesson-title',
        imagelabeldata: data.string.p2text1,
      }]
    }, ]
  },
  // slide1
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: '',

    imagedivblock: [{
        imagediv: 'top-bar bar-1',
        image: [{
            imgclass: "bar-img-1",
            imgsrc: '',
            imgid: 'leaf',
            imagelabelclass: 'bar-text',
            imagelabeldata: data.string.p2text2,
          },
          {
            imgclass: "bar-img-1a",
            imgsrc: '',
            imgid: 'egg',
          }
        ]
      },
      {
        imagediv: 'top-bar bar-2',
        image: [{
            imgclass: "bar-img-2",
            imgsrc: '',
            imgid: 'glass-1',
            imagelabelclass: 'bar-text',
            imagelabeldata: data.string.p2text3,
          },
          {
            imgclass: "bar-img-2a",
            imgsrc: '',
            imgid: 'glass-2',
          }
        ]
      },
      {
        imagediv: 'top-bar bar-3',
        image: [{
            imgclass: "bar-img-3 nwbar-img",
            imgsrc: '',
            imgid: 'torch-1',
            imagelabelclass: 'bar-text',
            imagelabeldata: data.string.p2text4,
          },
          {
            imgclass: "bar-img-3a",
            imgsrc: '',
            imgid: 'torch-2',
          }
        ]
      },
    ]
  },
  // slide2
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: '',

    imagedivblock: [{
        imagediv: 'top-bar bar-1 active-bar-anim',
        image: [{
            imgclass: "bar-img-1",
            imgsrc: '',
            imgid: 'leaf',
            imagelabelclass: 'bar-text',
            imagelabeldata: data.string.p2text2,
          },
          {
            imgclass: "bar-img-1a",
            imgsrc: '',
            imgid: 'egg',
          }
        ]
      },
      {
        imagediv: 'top-bar bar-2 inactive-bar-anim',
        image: [{
            imgclass: "bar-img-2",
            imgsrc: '',
            imgid: 'glass-1',
            imagelabelclass: 'bar-text',
            imagelabeldata: data.string.p2text3,
          },
          {
            imgclass: "bar-img-2a",
            imgsrc: '',
            imgid: 'glass-2',
          }
        ]
      },
      {
        imagediv: 'top-bar bar-3 inactive-bar-anim',
        image: [{
            imgclass: "bar-img-3",
            imgsrc: '',
            imgid: 'torch-1',
            imagelabelclass: 'bar-text',
            imagelabeldata: data.string.p2text4,
          },
          {
            imgclass: "bar-img-3a",
            imgsrc: '',
            imgid: 'torch-2',
          }
        ]
      },
    ]
  },
  // slide3
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: 'bg-p1',
    extratextblock: [{
      textclass: 'bottom-bar',
      textdata: '',
    }, {
      textclass: 'drop-ins',
      textdata: data.string.p2text5,
    }, {
      textclass: 'drop-t1 drop-obj-name',
      textdata: data.string.p2item1,
    }, {
      textclass: 'drop-t2 drop-obj-name',
      textdata: data.string.p2item2,
    }, {
      textclass: 'drop-t3 drop-obj-name',
      textdata: data.string.p2item3,
    }, {
      textclass: 'drop-t4 drop-obj-name',
      textdata: data.string.p2item4,
    }, {
      textclass: 'drop-t5 drop-obj-name',
      textdata: data.string.p2item5,
    }],
    imageblock: [{
      imagestoshow: [{
        imgclass: "monkey-fb",
        imgsrc: '',
        imgid: 'monkey'
      }, {
        imgclass: "drop-obj sink drop-1",
        imgsrc: '',
        imgid: 'stone'
      }, {
        imgclass: "drop-obj float drop-2",
        imgsrc: '',
        imgid: 'bottle'
      }, {
        imgclass: "drop-obj sink drop-3",
        imgsrc: '',
        imgid: 'pipe'
      }, {
        imgclass: "drop-obj float drop-4",
        imgsrc: '',
        imgid: 'feather'
      }, {
        imgclass: "drop-obj sink drop-5",
        imgsrc: '',
        imgid: 'brick'
      }]
    }],
    svgblock: [{
      svgblock: 'svg-drop-1',
      textclass: 'beaker-label',
      textdata: data.string.p2text6
    }, {
      svgblock: 'svg-drop-2',
      textclass: 'beaker-label',
      textdata: data.string.p2text7
    }],
  },
  // slide4
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: 'bg-p1',
    uppertextblockadditionalclass: 'last-text lt-1',
    uppertextblock: [{
      textclass: 'wrong-btn',
      textdata: '&#x2716;',
    }, {
      textclass: '',
      textdata: data.string.p2text8,
      datahighlightflag: true,
      datahighlightcustomclass: 'font-yellow',
    }],
    lowertextblockadditionalclass: 'last-text lt-2',
    lowertextblock: [{
      textclass: 'wrong-btn',
      textdata: '&#x2716;',
    }, {
      textclass: '',
      textdata: data.string.p2text9,
      datahighlightflag: true,
      datahighlightcustomclass: 'font-yellow',
    }],
    extratextblock: [{
      textclass: 'bottom-bar',
      textdata: '',
    }],
    imageblock: [{
      imagestoshow: [{
        imgclass: "drop-obj dropped-1",
        imgsrc: '',
        imgid: 'stone'
      }, {
        imgclass: "drop-obj dropped-2",
        imgsrc: '',
        imgid: 'bottle'
      }, {
        imgclass: "drop-obj dropped-3",
        imgsrc: '',
        imgid: 'pipe'
      }, {
        imgclass: "drop-obj dropped-4",
        imgsrc: '',
        imgid: 'feather'
      }, {
        imgclass: "drop-obj dropped-5",
        imgsrc: '',
        imgid: 'brick'
      }]
    }],
    svgblock: [{
      svgblock: 'svg-drop-1',
      textclass: 'beaker-label',
      textdata: data.string.p2text6,
      handclass: 'hand-1'
    }, {
      svgblock: 'svg-drop-2',
      textclass: 'beaker-label',
      textdata: data.string.p2text7,
      handclass: 'hand-2'
    }],
  }
];


$(function() {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page, countNext + 1);
  // readCSV();
  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();

  //for preload
  var preload;
  var timeoutvar = null;
  var current_sound;

  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      //images
      {
        id: "front-bg",
        src: imgpath + "lab01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "hand",
        src: "images/hand-icon.gif",
        type: createjs.AbstractLoader.IMAGE
      },

      {
        id: "beaker-svg",
        src: imgpath + "beaker.svg",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "beaker-svg-2",
        src: imgpath + "beaker2.svg",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "monkey",
        src: "images/sundar/correct-1.png",
        type: createjs.AbstractLoader.IMAGE
      },

      {
        id: "leaf",
        src: imgpath + "leaf.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "egg",
        src: imgpath + "egg.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "torch-1",
        src: imgpath + "torch01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "torch-2",
        src: imgpath + "torch02.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "glass-1",
        src: imgpath + "glass01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "glass-2",
        src: imgpath + "glass02.png",
        type: createjs.AbstractLoader.IMAGE
      },

      {
        id: "stone",
        src: imgpath + "diy1/stone.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "brick",
        src: imgpath + "diy1/brick.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "feather",
        src: imgpath + "diy1/feather.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "bottle",
        src: imgpath + "diy1/bottle.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "pipe",
        src: imgpath + "diy1/pipe.png",
        type: createjs.AbstractLoader.IMAGE
      },

      // sounds
      {
        id: "sound_0",
        src: soundAsset + "p2_s0.ogg"
      },
      {
        id: "sound_1",
        src: soundAsset + "p2_s1.ogg"
      },
      {
        id: "sound_2",
        src: soundAsset + "p2_s2.ogg"
      },
      {
        id: "sound_3",
        src: soundAsset + "p2_s3.ogg"
      },
      {
        id: "sound_4",
        src: soundAsset + "p2_s4_0.ogg"
      },
      {
        id: "sound_5",
        src: soundAsset + "p2_s4_1.ogg"
      },

    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound); //for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }

  function handleFileLoad(event) {
    // console.log(event.item);
  }

  function handleProgress(event) {
    $('#loading-text').html(parseInt(event.loaded * 100) + '%');
  }

  function handleComplete(event) {
    $('#loading-wrapper').hide(0);
    //initialize varibales
    // call main function
    templateCaller();
  }
  //initialize
  init();

  /*==================================================
  =            Handlers and helpers Block            =
  ==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  /*===============================================
  =            data highlight function            =
  ===============================================*/
  function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";
    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
         use that or else use default 'parsedstring'*/
        $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
          (stylerulename = $(this).attr("data-highlightcustomclass")) : (stylerulename = "parsedstring");

        texthighlightstarttag = "<span class='" + stylerulename + "'>";
        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
        $(this).html(replaceinstring);
      });
    }
  }
  /*=====  End of data highlight function  ======*/

  /*===============================================
   =            user notification function        =
   ===============================================*/
  function notifyuser($notifyinside) {
    //check if $notifyinside is provided
    typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

    /*variable that will store the element(s) to remove notification from*/
    var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
    // if there are any notifications removal required add the event handler
    if ($allnotifications.length > 0) {
      $allnotifications.one('click', function() {
        /* Act on the event */
        $(this).attr('data-isclicked', 'clicked');
        $(this).removeAttr('data-usernotification');
      });
    }
  }

  /*=====  End of user notification function  ======*/

  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/

  function navigationcontroller(islastpageflag) {
    typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

    // islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
  }

  /*=====  End of user navigation controller function  ======*/

  /*==================================================
   =            InstructionBlockController            =
   ==================================================*/

  function instructionblockcontroller() {
    var $instructionblock = $board.find("div.instructionblock");
    if ($instructionblock.length > 0) {
      var $contentblock = $board.find("div.contentblock");
      var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
      var instructionblockisvisibleflag;

      $contentblock.css('pointer-events', 'none');

      $toggleinstructionblockbutton.on('click', function() {
        instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
        if (instructionblockisvisibleflag == 'true') {
          instructionblockisvisibleflag = 'false';
          $contentblock.css('pointer-events', 'auto');
        } else if (instructionblockisvisibleflag == 'false') {
          instructionblockisvisibleflag = 'true';
          $contentblock.css('pointer-events', 'none');
        }

        $instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
      });
    }
  }

  /*=====  End of InstructionBlockController  ======*/

  /*=================================================
   =            general template function            =
   =================================================*/
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    vocabcontroller.findwords(countNext);
    put_image(content, countNext);
    put_image2(content, countNext);
    var first = 0;
    var second = 0;
    switch (countNext) {
      case 0:
        sound_nav('sound_0');
        break;
      case 2:
        $prevBtn.show(0);
        timeoutvar = setTimeout(function() {
          sound_nav('sound_2');
        }, 1000);
        break;
      case 3:
        $prevBtn.show(0);
        sound_player('sound_3');

        var s = Snap('#svg-drop-1');
        var s2 = Snap('#svg-drop-2');
        var count = 1;
        var sink_count = 0;
        var float_count = 0;
        $('.drop-1').show(0);
        $('.drop-t1').fadeIn(500);
        $('.drop-1').addClass('current-display');

        $('#svg-drop-1').click(function() {
          if ($('.current-display').hasClass('float')) {
            play_correct_incorrect_sound(1);
            $('.drop-t' + count).fadeOut(500);
            var left_pos = 20 + float_count * 11;
            $('.current-display').animate({
              'left': left_pos + '%',
            }, 800, "linear", function() {
              timeoutvar = setTimeout(function() {
                init_water_1();
              }, 800);
              $('.current-display').animate({
                'bottom': '22.5%',
              }, 900, "linear", function() {
                $('.drop-' + count).removeClass('current-display');
                count++;
                float_count++;
                if (count > 5) {
                  $nextBtn.show(0);
                  $('.monkey-fb').show(0);
                } else {
                  $('.drop-' + count).show(0);
                  $('.drop-t' + count).fadeIn(500);
                  $('.drop-' + count).addClass('current-display');
                  $('#svg-drop-1, #svg-drop-2').css('pointer-events', 'all');
                }
              });
            });
            $('#svg-drop-1, #svg-drop-2').css('pointer-events', 'none');
          } else {
            play_correct_incorrect_sound(0);
            $(this).css('pointer-events', 'none');
          }
        });
        $('#svg-drop-2').click(function() {
          if ($('.current-display').hasClass('sink')) {
            play_correct_incorrect_sound(1);
            $('.drop-t' + count).fadeOut(500);
            var left_pos = 82 - sink_count * 7;
            $('.current-display').animate({
              'left': left_pos + '%',
            }, 800, function() {
              timeoutvar = setTimeout(function() {
                pinit_water_1();
              }, 800 - sink_count * 200);
              $('.current-display').addClass('drop-anim');
              timeoutvar = setTimeout(function() {
                $('.drop-' + count).removeClass('current-display');
                count++;
                sink_count++;
                $('.drop-t' + count).fadeIn(500);
                if (count > 5) {
                  $nextBtn.show(0);
                  $('.monkey-fb').show(0);
                } else {
                  $('.drop-' + count).show(0);
                  $('.drop-' + count).addClass('current-display');
                  $('#svg-drop-1, #svg-drop-2').css('pointer-events', 'all');
                }
              }, 1000);
            });
            $('#svg-drop-1, #svg-drop-2').css('pointer-events', 'none');
          } else {
            play_correct_incorrect_sound(0);
            $(this).css('pointer-events', 'none');
          }
        });
        //svg1 var
        var p1, path1, p2, path2, p3, path3, poly, move_count = 0,
          max_count = 4;
        var pp1, ppath1, ppath2, ppath3;
        var ppoly;
        var svg = Snap.load(preload.getResult('beaker-svg').src, function(loadedFragment) {
          s.append(loadedFragment);
          //to resive whole svg
          p1 = Snap.select('#path-1');
          path1 = p1.attr('path');
          ppath1 = path1;
          p2 = Snap.select('#path-2');
          path2 = p2.attr('path');
          ppath2 = path2;
          p3 = Snap.select('#path-3');
          path3 = p3.attr('path');
          ppath3 = path3;
          poly = Snap.select('#poly');
        });
        var svg2 = Snap.load(preload.getResult('beaker-svg-2').src, function(loadedFragment) {
          s2.append(loadedFragment);
          //to resive whole svg
          pp1 = Snap.select('#ppath-1');
          ppoly = Snap.select('#ppoly');
          ppath1 = pp1.attr('path');
          var pp2 = Snap.select('#ppath-2');
          ppath2 = pp2.attr('path');
          var pp3 = Snap.select('#ppath-3');
          ppath3 = pp3.attr('path');
        });

        function raise_water_2(val) {
          var cur_h = ppoly.attr('height');
          var new_cur_h = parseFloat(cur_h) + val + 30;
          var cur_y = ppoly.attr('y');
          var new_cur_y = parseFloat(cur_y) - val;

          ppath1 = ppath1.replace(/,(.*?)H/, ',' + new_cur_y + 'H'); //value to rise water here
          ppath2 = ppath2.replace(/,(.*?)H/, ',' + new_cur_y + 'H'); //value to rise water here
          ppath3 = ppath3.replace(/,(.*?)H/, ',' + new_cur_y + 'H'); //value to rise water here
          ppoly.animate({
            height: new_cur_h,
            y: new_cur_y
          }, 300, mina.linear);
        }

        function pinit_water_1() {
          move_count = 0;
          pto_path_2();
        };

        function pto_path_2() {
          move_count++;
          raise_water_2(15);
          if (move_count < max_count) {
            pp1.animate({
              d: ppath2
            }, 300, mina.linear, pto_path_3);
          } else {
            pp1.animate({
              d: ppath1
            }, 300, mina.linear);
          }
        }

        function pto_path_3() {
          move_count++;
          raise_water_2(15);
          if (move_count < max_count) {
            pp1.animate({
              d: ppath3
            }, 300, mina.linear, pto_path_2);
          } else {
            pp1.animate({
              d: ppath1
            }, 300, mina.linear);
          }
        }

        function init_water_1() {
          move_count = 0;
          to_path_2();
        };

        function to_path_2() {
          move_count++;
          if (move_count < 3) {
            p1.animate({
              d: path2
            }, 300, mina.linear, to_path_3);
          } else {
            p1.animate({
              d: path1
            }, 300, mina.linear);
          }
        }

        function to_path_3() {
          move_count++;
          if (move_count < 2) {
            p1.animate({
              d: path3
            }, 300, mina.linear, to_path_2);
          } else {
            p1.animate({
              d: path1
            }, 300, mina.linear);
          }
        }
        break;
      case 4:
        // $('.wrong-btn').click(()=>counter++);
        $prevBtn.show(0);
        var clicks = [0, 0];
        $('.handclass').attr('src', preload.getResult('hand').src);
        var s = Snap('#svg-drop-1');
        var s2 = Snap('#svg-drop-2');
        var svg = Snap.load(preload.getResult('beaker-svg').src, function(loadedFragment) {
          s.append(loadedFragment);
        });
        var svg2 = Snap.load(preload.getResult('beaker-svg-2').src, function(loadedFragment) {
          s2.append(loadedFragment);
          //to resive whole svg
          var pp1 = Snap.select('#ppath-1');
          var ppoly = Snap.select('#ppoly');
          ppoly.attr({
            height: '552.5',
            y: '588.287'
          });
          pp1.attr('path', "M912.916,588.287C912.916,588.287,100.917,588.287,100.917,588.287C100.917,588.287,100.917,307.28700000000003,100.917,307.28700000000003C100.917,307.28700000000003,255.77100000000002,307.28700000000003,255.77100000000002,307.28700000000003C255.77100000000002,307.28700000000003,501.608,307.28700000000003,501.608,307.28700000000003C501.608,307.28700000000003,742.158,307.28700000000003,742.158,307.28700000000003C742.158,307.28700000000003,912.917,307.28700000000003,912.917,307.28700000000003C912.917,307.28700000000003,912.917,813.43,912.917,813.43C912.917,813.43,912.916,588.287,912.916,588.287C912.916,588.287,912.916,588.287,912.916,588.287");
        });
        $('.wrong-btn').click(function() {
          createjs.Sound.stop();
          $(this).parent().fadeOut(500);
          $('.hand-2').show(0);
          $('.hand-1').show(0);
        });
        $('#svg-drop-1').click(function() {
          $('.lt-1').show(0);
          clicks[0] = 1;
          $('.hand-1').hide(0);
          first = 1;
          // if(clicks[0]==1&clicks[1]==1)
          setTimeout(function() {
            sound_player_control('sound_4', first, second);
          }, 1100);
          // $('.hand-1').hide(0);
        });
        $('#svg-drop-2').click(function() {
          $('.lt-2').show(0);
          clicks[1] = 1;
          $('.hand-2').hide(0);
          second = 1;
          // if(clicks[0]==1&clicks[1]==1) second=1;
          setTimeout(function() {
            sound_player_control('sound_5', first, second);
          }, 1100);

          // $('.hand-2').hide(0);
        });
        break;
      default:
        $prevBtn.show(0);
        sound_nav('sound_' + (countNext));
        break;
    }
  }

  function nav_button_controls(delay_ms) {
    timeoutvar = setTimeout(function() {
      if (countNext == 0) {
        $nextBtn.show(0);
      } else if (countNext > 0 && countNext == $total_page - 1) {
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else {
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    }, delay_ms);
  }

  function sound_player(sound_id) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
  }

  function sound_nav(sound_id) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on("complete", function() {
      nav_button_controls(0);
    });
  }

  function sound_player_control(sound_id, first, second) {
    $('.wrong-btn').css({
      "pointer-events": "none"
    });
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on("complete", function() {
      $('.wrong-btn').css({
        "pointer-events": "auto"
      });
      if (first == 1 && second == 1) {
        $('.wrong-btn').click(() => ole.footerNotificationHandler.pageEndSetNotification());
      }
    });
  }

  function put_image(content, count) {
    if (content[count].hasOwnProperty('imageblock')) {
      var imageblock = content[count].imageblock[0];
      if (imageblock.hasOwnProperty('imagestoshow')) {
        var imageClass = imageblock.imagestoshow;
        for (var i = 0; i < imageClass.length; i++) {
          var image_src = preload.getResult(imageClass[i].imgid).src;
          //get list of classes
          var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
          var selector = ('.' + classes_list[classes_list.length - 1]);
          $(selector).attr('src', image_src);
        }
      }
    }
  }

  function put_image2(content, count) {
    if (content[count].hasOwnProperty('imagedivblock')) {
      var imageDiv = content[count].imagedivblock;
      for (var i = 0; i < imageDiv.length; i++) {
        var imageClass = imageDiv[i].image;
        for (var j = 0; j < imageClass.length; j++) {
          var image_src = preload.getResult(imageClass[j].imgid).src;
          //get list of classes
          var classes_list = imageClass[j].imgclass.match(/\S+/g) || [];
          var selector = ('.' + classes_list[classes_list.length - 1]);
          $(selector).attr('src', image_src);
        }
      }
    }
  }

  function templateCaller() {
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    navigationcontroller();

    generaltemplate();
    loadTimelineProgress($total_page, countNext + 1);
  }

  $nextBtn.on('click', function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    switch (countNext) {
      default:
        countNext++;
        templateCaller();
        break;
    }
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on('click', function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    countNext--;
    templateCaller();
    /* if footerNotificationHandler pageEndSetNotification was called then on click of
     previous slide button hide the footernotification */
    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
  });

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+'/';

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		imagedivblock:[
			{
				imagediv: 'bg-full font-7',
				imgclass : "bg-full",
				imgsrc : '',
				imgid : 'front-bg',
				imagelabelclass: 'lesson-title',
				imagelabeldata: data.lesson.chapter,
			},
		]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-board',

		imagedivblock:[
			{
				imagediv: 'img-container container-1 fade-in-1',
				imgclass : "book-img",
				imgsrc : '',
				imgid : 'book',
				imagelabelclass: 'img-label',
				imagelabeldata: data.string.p1text1,
			},
			{
				imagediv: 'img-container container-2 fade-in-1',
				imgclass : "water-img",
				imgsrc : '',
				imgid : 'water',
				imagelabelclass: 'img-label',
				imagelabeldata: data.string.p1text2,
			},
			{
				imagediv: 'img-container container-3 fade-in-1',
				imgclass : "fire-img",
				imgsrc : '',
				imgid : 'fire',
				imagelabelclass: 'img-label',
				imagelabeldata: data.string.p1text3,
			}
		]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-board',
		extratextblock:[{
			textclass: 'text-1 fade-in-4',
			textdata: data.string.p1text4
		}],
		imagedivblock:[
			{
				imagediv: 'img-container container-1 go-side-1',
				imgclass : "book-img",
				imgsrc : '',
				imgid : 'book',
				imagelabelclass: 'img-label fade-out',
				imagelabeldata: data.string.p1text1,
			},
			{
				imagediv: 'img-container container-2 go-side-2',
				imgclass : "water-img",
				imgsrc : '',
				imgid : 'water',
				imagelabelclass: 'img-label fade-out',
				imagelabeldata: data.string.p1text2,
			},
			{
				imagediv: 'img-container container-3 go-side-3',
				imgclass : "fire-img",
				imgsrc : '',
				imgid : 'fire',
				imagelabelclass: 'img-label fade-out',
				imagelabeldata: data.string.p1text3,
			}
		]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-board',
		extratextblock:[{
			textclass: 'text-2 fade-in-1',
			textdata: data.string.p1text5,
			datahighlightflag : true,
			datahighlightcustomclass : 'font-yellow',
		}],
		imagedivblock:[
			{
				imagediv: 'img-container container-1a',
				imgclass : "book-img",
				imgsrc : '',
				imgid : 'book',
				imagelabelclass: 'img-label its_hidden',
				imagelabeldata: data.string.p1text1,
			},
			{
				imagediv: 'img-container container-2a',
				imgclass : "water-img",
				imgsrc : '',
				imgid : 'water',
				imagelabelclass: 'img-label its_hidden',
				imagelabeldata: data.string.p1text2,
			},
			{
				imagediv: 'img-container container-3a',
				imgclass : "fire-img",
				imgsrc : '',
				imgid : 'fire',
				imagelabelclass: 'img-label its_hidden',
				imagelabeldata: data.string.p1text3,
			}
		]
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-board',
		uppertextblockadditionalclass: 'text-3 fade-in-1',
		uppertextblock:[{
			textclass: '',
			textdata: data.string.p1text6,
		},{
			textclass: '',
			textdata: data.string.p1text7,
		}],
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "front-bg", src: imgpath+"lab03.png", type: createjs.AbstractLoader.IMAGE},

			{id: "board", src: imgpath+"green_board.svg", type: createjs.AbstractLoader.IMAGE},

			{id: "book", src: imgpath+"book.png", type: createjs.AbstractLoader.IMAGE},
			{id: "water", src: imgpath+"jar.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fire", src: imgpath+"fire.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_1", src: soundAsset+"p1_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p1_s1.ogg"},
			{id: "sound_3", src: soundAsset+"p1_s2.ogg"},
			{id: "sound_4", src: soundAsset+"p1_s3.ogg"},
			{id: "sound_5", src: soundAsset+"p1_s4.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);

		switch(countNext) {
			case 0:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play('sound_1');
				current_sound.play();
				current_sound.on("complete", function(){
					$('.font-7').addClass('zoom-in');
					nav_button_controls(2000);
				});
				break;
			case 1:
				$prevBtn.show(0);
				$('.bg-board').css({
					'background-image': 'url('+ preload.getResult('board').src + ')',
					'background-size': '101% 101%',
					'background-position': '50% -0.5%'
				});
				timeoutvar = setTimeout(function(){
					sound_nav('sound_2');
				}, 2000);
				break;
			case 2:
				$prevBtn.show(0);
				$('.bg-board').css({
					'background-image': 'url('+ preload.getResult('board').src + ')',
					'background-size': '101% 101%',
					'background-position': '50% -0.5%'
				});
				timeoutvar = setTimeout(function(){
					sound_nav('sound_3');
				}, 4500);
				break;
			case 3:
				$prevBtn.show(0);
				$('.bg-board').css({
					'background-image': 'url('+ preload.getResult('board').src + ')',
					'background-size': '101% 101%',
					'background-position': '50% -0.5%'
				});
				timeoutvar = setTimeout(function(){
					sound_nav('sound_4');
				}, 2000);
				break;
			case 4:
				$prevBtn.show(0);
				$('.coverboardfull').css({"background-color":"rgb(204, 128, 0)"});
				timeoutvar = setTimeout(function(){
					sound_nav('sound_5');
				}, 2000);
				break;
			default:
				$prevBtn.show(0);
				$('.bg-board').css({
					'background-image': 'url('+ preload.getResult('board').src + ')',
					'background-size': '101% 101%',
					'background-position': '50% -0.5%'
				});
				sound_nav('sound_'+(countNext+1));
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageClass = content[count].imagedivblock;
			for(var i=0; i<imageClass.length; i++){
				var image_src = preload.getResult(imageClass[i].imgid).src;
				//get list of classes
				var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

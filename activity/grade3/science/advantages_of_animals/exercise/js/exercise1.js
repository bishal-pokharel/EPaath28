var soundAsset = $ref+"/audio_"+$lang+"/";

Array.prototype.shufflearray = function() {
    var i = this.length,
        j, temp;
    while (--i > 0) {
        j = Math.floor(Math.random() * (i + 1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
    return this;
}

var imgpath = $ref + "/exercise/images/";

var content = [

    //ex1
    {
        exerciseblock: [{
            instrucdata: data.string.instrucdata,
            textdata: data.string.exe1,
            imageoptions: [{
                    forshuffle: "class4",
                    imgsrc: imgpath + "dog.png",
                    optdata: data.string.dog
                },
                {
                    forshuffle: "class2",
                    imgsrc: imgpath + "cat.png",
                    optdata: data.string.cat
                },
                {
                    forshuffle: "class3",
                    imgsrc: imgpath + "donkey.png",
                    optdata: data.string.mule
                },
                {
                    forshuffle: "class1",
                    imgsrc: imgpath + "hen.png",
                    optdata: data.string.chick
                }
            ]

        }]
    },
    //ex2
    {
        exerciseblock: [{
            instrucdata: data.string.instrucdata,
            textdata: data.string.exe2,
            imageoptions: [{
                    forshuffle: "class1",
                    imgsrc: imgpath + "dog.png",
                    optdata: data.string.dog
                },
                {
                    forshuffle: "class2",
                    imgsrc: imgpath + "cat.png",
                    optdata: data.string.cat
                },
                {
                    forshuffle: "class3",
                    imgsrc: imgpath + "bee.png",
                    optdata: data.string.bee
                },
                {
                    forshuffle: "class4",
                    imgsrc: imgpath + "hen.png",
                    optdata: data.string.chick
                }
            ]

        }]
    },
    //ex3
    {
        exerciseblock: [{
            instrucdata: data.string.instrucdata,
            textdata: data.string.exe3,
            imageoptions: [{
                    forshuffle: "class1",
                    imgsrc: imgpath + "bee.png",
                    optdata: data.string.bee
                },
                {
                    forshuffle: "class2",
                    imgsrc: imgpath + "cat.png",
                    optdata: data.string.cat
                },
                {
                    forshuffle: "class3",
                    imgsrc: imgpath + "donkey.png",
                    optdata: data.string.mule
                },
                {
                    forshuffle: "class4",
                    imgsrc: imgpath + "fish.png",
                    optdata: data.string.fish
                }
            ]

        }]
    },
    //ex4
    {
        exerciseblock: [{
            instrucdata: data.string.instrucdata,
            textdata: data.string.exe4,
            imageoptions: [{
                    forshuffle: "class1",
                    imgsrc: imgpath + "donkey01.png",
                    optdata: data.string.mule
                },
                {
                    forshuffle: "class2",
                    imgsrc: imgpath + "cat.png",
                    optdata: data.string.cat
                },
                {
                    forshuffle: "class3",
                    imgsrc: imgpath + "cow.png",
                    optdata: data.string.cow
                },
                {
                    forshuffle: "class4",
                    imgsrc: imgpath + "bee.png",
                    optdata: data.string.bee
                }
            ]

        }]
    },
    //ex5
    {
      exerciseblock: [{
          instrucdata: data.string.instrucdata,
          textdata: data.string.exe5,
          imageoptions: [{
                  forshuffle: "class1",
                  imgsrc: imgpath + "cow.png",
                  optdata: data.string.cow
              },
              {
                  forshuffle: "class2",
                  imgsrc: imgpath + "bull.png",
                  optdata: data.string.bull
              },
              {
                  forshuffle: "class3",
                  imgsrc: imgpath + "bee.png",
                  optdata: data.string.bee
              },
              {
                  forshuffle: "class4",
                  imgsrc: imgpath + "snake.png",
                  optdata: data.string.snake
              }
          ]

      }]
    },
    //ex6
    {
      exerciseblock: [{
          instrucdata: data.string.instrucdata,
          textdata: data.string.exe6,
          imageoptions: [{
                  forshuffle: "class1",
                  imgsrc: imgpath + "fish.png",
                  optdata: data.string.fish
              },
              {
                  forshuffle: "class2",
                  imgsrc: imgpath + "cat.png",
                  optdata: data.string.cat
              },
              {
                  forshuffle: "class3",
                  imgsrc: imgpath + "cow.png",
                  optdata: data.string.cow
              },
              {
                  forshuffle: "class4",
                  imgsrc: imgpath + "bull.png",
                  optdata: data.string.bull
              }
          ]

      }]
    },
    //ex7
    {
      exerciseblock: [{
          instrucdata: data.string.instrucdata,
          textdata: data.string.exe7,
          imageoptions: [{
                  forshuffle: "class1",
                  imgsrc: imgpath + "cat.png",
                  optdata: data.string.cat
              },
              {
                  forshuffle: "class2",
                  imgsrc: imgpath + "sheep.png",
                  optdata: data.string.sheep
              },
              {
                  forshuffle: "class3",
                  imgsrc: imgpath + "cow.png",
                  optdata: data.string.cow
              },
              {
                  forshuffle: "class4",
                  imgsrc: imgpath + "bee.png",
                  optdata: data.string.bee
              }
          ]

      }]
    },
    //exe8
    {
      exerciseblock: [{
          instrucdata: data.string.instrucdata,
          textdata: data.string.exe8,
          imageoptions: [{
                  forshuffle: "class1",
                  imgsrc: imgpath + "bull.png",
                  optdata: data.string.bull
              },
              {
                  forshuffle: "class2",
                  imgsrc: imgpath + "snake.png",
                  optdata: data.string.snake
              },
              {
                  forshuffle: "class3",
                  imgsrc: imgpath + "bee.png",
                  optdata: data.string.bee
              },
              {
                  forshuffle: "class4",
                  imgsrc: imgpath + "sheep.png",
                  optdata: data.string.sheep
              }
          ]

      }]
    },
    //exe9
    {
      exerciseblock: [{
          instrucdata: data.string.instrucdata,
          textdata: data.string.exe9,
          imageoptions: [{
                  forshuffle: "class1",
                  imgsrc: imgpath + "sheep.png",
                  optdata: data.string.sheep
              },
              {
                  forshuffle: "class2",
                  imgsrc: imgpath + "snake.png",
                  optdata: data.string.snake
              },
              {
                  forshuffle: "class3",
                  imgsrc: imgpath + "bee.png",
                  optdata: data.string.bee
              },
              {
                  forshuffle: "class4",
                  imgsrc: imgpath + "camel.png",
                  optdata: data.string.camel
              }
          ]

      }]
    },
    //exe10
    {
      exerciseblock: [{
          instrucdata: data.string.instrucdata,
          textdata: data.string.exe10,
          imageoptions: [{
                  forshuffle: "class1",
                  imgsrc: imgpath + "camel.png",
                  optdata: data.string.camel
              },
              {
                  forshuffle: "class2",
                  imgsrc: imgpath + "snake.png",
                  optdata: data.string.snake
              },
              {
                  forshuffle: "class3",
                  imgsrc: imgpath + "bee.png",
                  optdata: data.string.bee
              },
              {
                  forshuffle: "class4",
                  imgsrc: imgpath + "sheep.png",
                  optdata: data.string.sheep
              }
          ]

      }]
    }
];
//
// /*remove this for non random questions*/
content.shufflearray();


$(function() {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var current_sound;
    /*for limiting the questions to 10*/
    var $total_page = content.length;
    /*var $total_page = content.length;*/
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            // {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
            // {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
            //   ,
            //images
            // {id: "mrs_sharma", src: imgpath+"mrs_sharma.png", type: createjs.AbstractLoader.IMAGE},
            // {id: "temple_bg", src: imgpath+"temple_bg.png", type: createjs.AbstractLoader.IMAGE},
            // {id: "swimming_bg", src: imgpath+"swimming_bg.png", type: createjs.AbstractLoader.IMAGE},
            // {id: "restaurant", src: imgpath+"restaurant.png", type: createjs.AbstractLoader.IMAGE},
            // {id: "elephant_bg", src: imgpath+"elephant_bg.png", type: createjs.AbstractLoader.IMAGE},
            // {id: "school_bg", src: imgpath+"school_bg.png", type: createjs.AbstractLoader.IMAGE},
            // sounds
            {id: "sound_0", src: soundAsset+"exer.ogg"},
            //textboxes
            // {id: "dialog1_img", src: imgpath+'bubble-10.png', type: createjs.AbstractLoader.IMAGE},
            // {id: "bubblesc", src: imgpath+'bubblesc.png', type: createjs.AbstractLoader.IMAGE},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }
    function handleFileLoad(event) {
        console.log(event.item);
    }
    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded*100)+'%');
    }
    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        // current_sound = createjs.Sound.play('sound_0');
        // call main function
        templateCaller();
    }
    //initialize
    init();
    function navigationcontroller(islastpageflag) {
        // check if the parameter is defined and if a boolean,
        // update islastpageflag accordingly
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean' ?
            alert("NavigationController : Hi Master, please provide a boolean parameter") :
            null;
    }

    var score = 0;
    var testin = new EggTemplate();
   
      //eggTemplate.eggMove(countNext);
    testin.init(10);
    /*values in this array is same as the name of images of eggs in image folder*/


    function sound_player(sound_id){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
    }
    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);

        $nextBtn.hide(0);
        $prevBtn.hide(0);
        countNext==0?sound_player('sound_0'):"";

        /*for randomizing the options*/
        var parent = $(".droparea");
        var divs = parent.children();
        while (divs.length) {
            parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
        }


        /*======= SCOREBOARD SECTION ==============*/

        var ansClicked = false;
        var wrngClicked = false;

        $(".checkhere").click(function() {
            $(this).removeClass('forhoverimg');
            if (ansClicked == false) {

                /*class 1 is always for the right answer. updates scoreboard and disables other click if
                right answer is clicked*/
                if ($(this).hasClass("class1")) {
                    console.log("wweee");
                    if (wrngClicked == false) {
                        testin.update(true);
                    }
                    $(this).css("background", "#6AA84F");
                    $(this).css("border", "5px solid #B6D7A8");
                    $(this).siblings(".corctopt").show(0);
                    $(".questionimg").css("display", "none")
                        // $(".display_image").css("display", "block");
                    current_sound.stop();
                    play_correct_incorrect_sound(true);

                    $('.checkhere').removeClass('forhoverimg');
                    ansClicked = true;

                    if (countNext != $total_page) {
                            $nextBtn.show(0);
                    }

                } else {
                    testin.update(false);
                    $(this).css("background", "#EA9999");
                    $(this).css("border", "5px solid #efb3b3");
                    $(this).css("color", "#F66E20");
                    $(this).siblings(".wrngopt").show(0);
                    wrngClicked = true;
                    current_sound.stop();
                    play_correct_incorrect_sound(false);
                }
            }
        });

        /*======= SCOREBOARD SECTION ==============*/
    }


    function templateCaller() {
        /*always hide next and previous navigation button unless
        explicitly called from inside a template*/
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');


        // call the template
        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   

        //call the slide indication bar handler for pink indicators
    }


    /* navigation buttons event handlers */

    $nextBtn.on("click", function() {
        countNext++;
        templateCaller();
        testin.gotoNext();
    });


	$prevBtn.on('click', function() {
        countNext--;
        templateCaller();

        /* if footerNotificationHandler pageEndSetNotification was called then on click of
        	previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    /*=====  End of Templates Controller Block  ======*/
});

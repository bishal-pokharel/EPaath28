var imgpath = $ref + "/images/";
var soundAsset = $ref+"/audio_"+$lang+"/";
var content = [
    //1st page
    {
        additionalclasscontentblock: "firstpagebackground",
        hasheaderblock: true,
        addclass: "introBlock animated bounceInDown",
        uppertextblock: [{
            texttoshow: [{
                textclass: "chaptertext",
                textdata: data.string.intro
            }]
        }],
        imageblock: [{
            imagetoshow: [{
                imgclass: 'beecss',
                imgsrc: imgpath + "bee.gif"
            }]
        }],
    },
    //2nd page
    {
        additionalclasscontentblock: "simplebg",
        hasheaderblock: true,
        addclass: 'introBlock secondPageBlock animated fadeIn',
        uppertextblock: [{
            texttoshow: [{
                textclass: "insideText cuteText",
                textdata: data.string.cute
            }]
        }],
        lowertextblockadditionalclass: "lowerblockadd animated fadeIn",
        lowertextblock: [{
            texttoshow: [{
                textclass: " lowerText",
                textdata: data.string.cuteDescription
            }]
        }],
        slideshowblock: [{
            slides: [
                imgpath + "slideshow0.jpg",
                imgpath + "slideshow1.jpg",
                imgpath + "slideshow2.jpg",
            ]
        }]
    },

    //3rd page
    {
        additionalclasscontentblock: "simplebg",
        hasheaderblock: true,
        addclass: 'introBlock secondPageBlock animated fadeIn',
        uppertextblock: [{
            texttoshow: [{
                textclass: 'insideText',
                textdata: data.string.company
            }]
        }],
        imageblock: [{
            imagetoshow: [{
                imgclass: 'firstImg animated fadeIn',
                imgsrc: imgpath + "childWithPet.jpg"
            }, {
                imgclass: ' firstImg secondImg animated fadeIn',
                imgsrc: imgpath + 'dogwithkid.jpg',
            }, {
                imgclass: 'firstImg thirdImg animated fadeIn',
                imgsrc: imgpath + 'dogwithkid01.jpg'
            }]
        }],
        lowertextblockadditionalclass: "lowerblockadd  animated fadeIn",
        lowertextblock: [{
            texttoshow: [{
                textclass: ' lowerText',
                textdata: data.string.companyLower
            }]
        }]
    },
    //4th page
    {
        additionalclasscontentblock: "simplebg",
        hasheaderblock: true,
        addclass: 'introBlock secondPageBlock',
        uppertextblock: [{
            texttoshow: [{
                textclass: 'insideText',
                textdata: data.string.company
            }]
        }],
        imageblock: [{
            imagetoshow: [{
                imgclass: 'firstImg animated fadeIn',
                imgsrc: imgpath + "catMouse.jpg"
            }, {
                imgclass: 'firstImg secondImg animated fadeIn',
                imgsrc: imgpath + "girlKitten.jpg"
            }, {
                imgclass: 'firstImg thirdImg animated fadeIn',
                imgsrc: imgpath + 'boywithcat.jpg'
            }]
        }],
        lowertextblockadditionalclass: "lowerblockadd lowertex animated fadeIn",
        lowertextblock: [{
            texttoshow: [{
                textclass: 'lowerText',
                textdata: data.string.companyNextLower
            }]
        }]
    }
];


// }

$(function() {
    var $board = $(".board");
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var $total_page = content.length;

    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();

    loadTimelineProgress($total_page, countNext + 1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// sounds
			{id: "sound_0", src: soundAsset+"p1_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p1_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p1_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p1_s3.ogg"},
			//textboxes
			// {id: "dialog1_img", src: imgpath+'bubble-10.png', type: createjs.AbstractLoader.IMAGE},
			// {id: "bubblesc", src: imgpath+'bubblesc.png', type: createjs.AbstractLoader.IMAGE},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_0');
		// call main function
		templateCaller();
	}
	//initialize
	init();
    /*
    	inorder to use the handlebar partials we need to register them
    	to their respective handlebar partial pointer first
    */
    Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("slideshowcontent", $("#slideshowcontent-partial").html());



    //controls the navigational state of the program
    /*======================================================
    =            Navigation Controller Function            =
    ======================================================*/
    /**
     How To:
     - Just call the navigation controller if it is to be called from except the
    	 last page of lesson
     - If called from last page set the islastpageflag to true such that
    	 footernotification is called for continue button to navigate to exercise
     */

    /**
    	 What it does:
    	 - If not explicitly overriden the method for navigation button
    		 controls, it shows the navigation buttons as required,
    		 according to the total count of pages and the countNext variable
    	 - If for a general use it can be called from the templateCaller
    		 function
    	 - Can be put anywhere in the template function as per the need, if
    		 so should be taken out from the templateCaller function
    	 - If the total page number is
    	*/

    function navigationcontroller(islastpageflag) {
        typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

    }
	var timeoutcontroller;
    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);

        vocabcontroller.findwords(countNext);

        $board.html(html);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);
        switch (countNext) {
            case 1:
            case 2:
            	sound_player('sound_'+countNext);
              break;
            case 0:
            	sound_player('sound_'+countNext);
                break;
            case 3:
            	timeoutcontroller = setTimeout(function(){
	            	sound_player('sound_'+countNext);
            	}, 1500);
                break;
        }
    }
    function nav_button_controls(delay_ms){
  		timeoutvar = setTimeout(function(){
  			if(countNext==0){
  				$nextBtn.show(0);
  			} else if( countNext>0 && countNext == $total_page-1){
  				$prevBtn.show(0);
  				ole.footerNotificationHandler.pageEndSetNotification();
  			} else{
  				$prevBtn.show(0);
  				$nextBtn.show(0);
  			}
  		},delay_ms);
  	}

    function sound_player(sound_id){
      createjs.Sound.stop();
      current_sound = createjs.Sound.play(sound_id);
      current_sound.play();
      current_sound.on("complete", function(){

        nav_button_controls(0);

      });
    }

    function templateCaller() {
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

        loadTimelineProgress($total_page, countNext + 1);

        navigationcontroller();

        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


    }

    $nextBtn.on("click", function() {
        countNext++;
        templateCaller();
    });
    /*
    	TODO: we need to add last page notification removal code on prevBtn pressed
    */
    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
        countNext--;
        clearTimeout(timeoutcontroller);
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });
    // setTimeout(function(){
    total_page = content.length;
    // }, 250);


});



/*===============================================
	 =            data highlight function            =
	 ===============================================*/
function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";


    if ($alltextpara.length > 0) {
        $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                (stylerulename = $(this).attr("data-highlightcustomclass")) :
                (stylerulename = "parsedstring");

            texthighlightstarttag = "<span>";


            replaceinstring = $(this).html();
            replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
            replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);


            $(this).html(replaceinstring);
        });
    }
}
/*=====  End of data highlight function  ======*/

//page 1

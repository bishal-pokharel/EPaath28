var imgpath = $ref + "/images/";
var soundAsset = $ref+"/audio_"+$lang+"/";
var content = [
    //1st page
    {
        additionalclasscontentblock: "simplebg",
        hasheaderblock: true,
        addclass: 'introBlock animated bounceInDown',
        uppertextblock: [{
            texttoshow: [{
                textclass: "insideText cuteText",
                textdata: data.string.dontwe
            }]
        }]
    },

    //3rd page
    {
        additionalclasscontentblock: "simplebg",
        hasheaderblock: true,
        addclass: 'introBlock animated bounceInDown',
        uppertextblock: [{
            texttoshow: [{
                textclass: 'insideText cuteText',
                textdata: data.string.weget
            }]
        }],
        lowertextblockadditionalclass: "lowerblockadd anotheradd animated fadeIn",
        lowertextblock: [{
            texttoshow: [{
                textclass: 'insideText inside',
                textdata: data.string.therefore
            }]
        }]
    },
    //4th page
    {
        additionalclasscontentblock: "simplebg",
        hasheaderblock: true,
        addclass: 'introBlock animated bounceInDown',
        uppertextblock: [{
            texttoshow: [{
                textclass: 'insideText cuteText',
                textdata: data.string.wild
            }]
        }],
        lowertextblockadditionalclass: "lowerblockadd anotheradd animated fadeIn",
        lowertextblock: [{
            texttoshow: [{
                textclass: 'insideText inside',
                textdata: data.string.wildLow
            }]
        }]
    },
    //5th page
    {
        additionalclasscontentblock: "simplebg",
        hasheaderblock: true,
        addclass: 'introBlock secondPageBlock animated fadeIn',
        uppertextblock: [{
            texttoshow: [{
                textclass: 'insideText cuteText',
                textdata: data.string.wild
            }]
        }],
        imageblock: [{
            imagetoshow: [{
                imgclass: 'firstImg animated fadeIn',
                imgsrc: imgpath + "vulture-feeding.jpg"
            }, {
                imgclass: ' firstImg secondImg animated fadeIn',
                imgsrc: imgpath + 'monkey.jpg',
            }, {
                imgclass: 'firstImg thirdImg animated fadeIn',
                imgsrc: imgpath + 'bird01.jpg'
            }]
        }],
        anotherBlock: true,
        anothertextdata: data.string.wildLow
    },
    //6th page
    {
        additionalclasscontentblock: "simplebg",
        hasheaderblock: true,
        addclass: 'introBlock secondPageBlock ',
        uppertextblock: [{
            texttoshow: [{
                textclass: 'insideText cuteText',
                textdata: data.string.wild
            }]
        }],
        imageblock: [{
            imagetoshow: [{
                imgclass: 'firstImg ',
                imgsrc: imgpath + "vulture-feeding.jpg"
            }, {
                imgclass: ' firstImg secondImg  ',
                imgsrc: imgpath + 'monkey.jpg',
            }, {
                imgclass: 'firstImg thirdImg  ',
                imgsrc: imgpath + 'bird01.jpg'
            }]
        }],
        lowertextblockadditionalclass: "lowerblockadd  animated fadeIn",
        lowertextblock: [{
            texttoshow: [{
                textclass: 'insideText lowerText',
                textdata: data.string.vulture
            }]
        }],
        anotherBlock: true,
        anothertextdata: data.string.wildLow
    },
    //7th page
    {
        additionalclasscontentblock: "simplebg",
        hasheaderblock: true,
        addclass: 'introBlock secondPageBlock ',
        uppertextblock: [{
            texttoshow: [{
                textclass: 'insideText cuteText',
                textdata: data.string.wild
            }]
        }],
        imageblock: [{
            imagetoshow: [{
                imgclass: 'firstImg ',
                imgsrc: imgpath + "vulture-feeding.jpg"
            }, {
                imgclass: ' firstImg secondImg  ',
                imgsrc: imgpath + 'monkey.jpg',
            }, {
                imgclass: 'firstImg thirdImg  ',
                imgsrc: imgpath + 'bird01.jpg'
            }]
        }],
        lowertextblockadditionalclass: "lowerblockadd  animated fadeIn",
        lowertextblock: [{
            texttoshow: [{
                textclass: 'insideText lowerText',
                textdata: data.string.monkey
            }]
        }],
        anotherBlock: true,
        anothertextdata: data.string.wildLow
    },
    //8th page
    {
        additionalclasscontentblock: "simplebg",
        hasheaderblock: true,
        addclass: 'introBlock secondPageBlock animated fadeIn',
        uppertextblock: [{
            texttoshow: [{
                textclass: 'insideText cuteText',
                textdata: data.string.wild
            }]
        }],
        imageblock: [{
            imagetoshow: [{
                imgclass: 'firstImg ',
                imgsrc: imgpath + "vulture-feeding.jpg"
            }, {
                imgclass: ' firstImg secondImg  ',
                imgsrc: imgpath + 'monkey.jpg',
            }, {
                imgclass: 'firstImg thirdImg  ',
                imgsrc: imgpath + 'bird01.jpg'
            }]
        }],
        lowertextblockadditionalclass: "lowerblockadd  animated fadeIn",
        lowertextblock: [{
            texttoshow: [{
                textclass: 'insideText lowerText',
                textdata: data.string.bird
            }]
        }],
        anotherBlock: true,
        anothertextdata: data.string.wildLow
    },
    //9th page
    {
        additionalclasscontentblock: "simplebg",
        hasheaderblock: true,
        addclass: 'introBlock secondPageBlock animated fadeIn',

        slideshowblock: [{
            slides: [
                imgpath + "dolphin.jpg",
                imgpath + "panda.jpg",
                imgpath + "rabbitSlide.jpg",
                imgpath + "Crocodile.jpg",
                imgpath + "Ostrich.jpg",
                imgpath + "turtle.jpg",
            ]
        }],
        lowertextblockadditionalclass: "lowerblockadd  animated fadeIn",
        lowertextblock: [{
            texttoshow: [{
                textclass: 'insideText lowerText',
                textdata: data.string.slider
            }]
        }],
    }

];


// }

$(function() {
    var $board = $(".board");
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var $total_page = content.length;

    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();

    loadTimelineProgress($total_page, countNext + 1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "mrs_sharma", src: imgpath+"mrs_sharma.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "temple_bg", src: imgpath+"temple_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "swimming_bg", src: imgpath+"swimming_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "restaurant", src: imgpath+"restaurant.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "elephant_bg", src: imgpath+"elephant_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "school_bg", src: imgpath+"school_bg.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"p5_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p5_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p5_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p5_s4.ogg"},
			{id: "sound_5", src: soundAsset+"p5_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p5_s6.ogg"},
			{id: "sound_7", src: soundAsset+"p5_s7.ogg"},
			{id: "sound_8", src: soundAsset+"p5_s8.ogg"},
			//textboxes
			// {id: "dialog1_img", src: imgpath+'bubble-10.png', type: createjs.AbstractLoader.IMAGE},
			// {id: "bubblesc", src: imgpath+'bubblesc.png', type: createjs.AbstractLoader.IMAGE},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_0');
		// call main function
		templateCaller();
	}
	//initialize
	init();
    /*
    	inorder to use the handlebar partials we need to register them
    	to their respective handlebar partial pointer first
    */
    Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("slideshowcontent", $("#slideshowcontent-partial").html());



    //controls the navigational state of the program
    /*======================================================
    =            Navigation Controller Function            =
    ======================================================*/
    /**
     How To:
     - Just call the navigation controller if it is to be called from except the
    	 last page of lesson
     - If called from last page set the islastpageflag to true such that
    	 footernotification is called for continue button to navigate to exercise
     */

    /**
    	 What it does:
    	 - If not explicitly overriden the method for navigation button
    		 controls, it shows the navigation buttons as required,
    		 according to the total count of pages and the countNext variable
    	 - If for a general use it can be called from the templateCaller
    		 function
    	 - Can be put anywhere in the template function as per the need, if
    		 so should be taken out from the templateCaller function
    	 - If the total page number is
    	*/

    function navigationcontroller(islastpageflag) {
        typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;


    }
	var timeoutcontroller;
    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);

        vocabcontroller.findwords(countNext);

        $board.html(html);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);
        $nextBtn.hide(0);
        switch (countNext) {
        	case 0:
				sound_player('sound_'+countNext);

				break;
            case 1:
            case 2:
            	timeoutcontroller = setTimeout(function(){
	            	sound_player('sound_'+(countNext+1));
            	}, 1500);
                $('.manure1').delay(2000).fadeIn(5).animate({
                    'top': '4%',
                    'opacity': '1',
                    'display': 'block'
                }, 2000);
                $('.manure2').delay(2000).fadeIn(5).animate({
                    'top': '57%',
                    'opacity': '1',
                    'display': 'block'
                }, 2000);

                break;
            case 3:
            	timeoutcontroller = setTimeout(function(){
	            	sound_player('sound_'+(countNext+1));
            	}, 1500);
                $('.lowerText2').delay(4000).animate({
                    'display': 'block'
                }, 1000);
                nav_button_controls(5500);
                break;
            case 4:
            	timeoutcontroller = setTimeout(function(){
	            	sound_player('sound_'+(countNext+1));
            	}, 6000);
                $('.firstImg:nth-child(1)').toggleClass('border');
                $('.firstImg:nth-child(1)').delay(100).animate({
                    'height': '50%',
                    'width': '42%',
                    'top': '25%',
                    'opacity': '1'
                }, 3000);

                $('.firstImg:nth-child(2)').delay(100).animate({
                    'width': '25%',
                    'left': '45.6%',
                    'opacity': '1'
                }, 3000);

                $('.firstImg:nth-child(3)').delay(100).animate({
                    'width': '25%',
                    'left': '72%',
                    'opacity': '1'
                }, 3000);

                break;

            case 5:
            	timeoutcontroller = setTimeout(function(){
	            	sound_player('sound_'+(countNext+1));
            	}, 6000);
                $('.firstImg:nth-child(2)').toggleClass('border');
                $('.firstImg:nth-child(1)').delay(100).animate({
                    'width': '25%',
                    'left': '0.6%%',
                    'opacity': '1'
                }, 3000);

                $('.firstImg:nth-child(2)').delay(100).animate({
                    'height': '50%',
                    'width': '42%',
                    'top': '25%',
                    'left': '28%',
                    'opacity': '1'
                }, 3000);

                $('.firstImg:nth-child(3)').delay(100).animate({
                    'width': '25%',
                    'left': '72%',
                    'opacity': '1'
                }, 3000);

                break;
            case 6:
            	timeoutcontroller = setTimeout(function(){
	            	sound_player('sound_'+(countNext+1));
            	}, 6000);
                $('.firstImg:nth-child(3)').toggleClass('border');
                $('.firstImg:nth-child(1)').delay(100).animate({
                    'width': '25%',
                    'left': '0.6%%',
                    'opacity': '1'
                }, 3000);

                $('.firstImg:nth-child(2)').delay(100).animate({
                    'width': '25%',
                    'left': '30%',
                    'opacity': '1'
                }, 3000);

                $('.firstImg:nth-child(3)').delay(100).animate({
                    'height': '50%',
                    'width': '42%',
                    'top': '25%',
                    'left': '57%',
                    'opacity': '1'
                }, 3000);


                break;
            case 7:
				timeoutcontroller = setTimeout(function(){
	            	sound_player('sound_'+(countNext+1));
            	}, 4000);

                break;
        }
    }

    function nav_button_controls(delay_ms){
  		timeoutvar = setTimeout(function(){
  			if(countNext==0){
  				$nextBtn.show(0);
  			} else if( countNext>0 && countNext == $total_page-1){
  				$prevBtn.show(0);
  				ole.footerNotificationHandler.lessonEndSetNotification();
  			} else{
  				$prevBtn.show(0);
  				$nextBtn.show(0);
  			}
  		},delay_ms);
  	}

    function sound_player(sound_id){
      createjs.Sound.stop();
      current_sound = createjs.Sound.play(sound_id);
      current_sound.play();
      current_sound.on("complete", function(){

        nav_button_controls(0);

      });
    }
    // Handlebars.registerHelper('listItem', function (from, to, context, options){
    // var item = "";
    // for (var i = from, j = to; i <= j; i++) {
    // item = item + options.fn(context[i]);
    // }
    // return item;
    // });

    function templateCaller() {
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

        loadTimelineProgress($total_page, countNext + 1);

        navigationcontroller();

        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


    }

    $nextBtn.on("click", function() {
        countNext++;
        templateCaller();
    });
    /*
    	TODO: we need to add last page notification removal code on prevBtn pressed
    */
    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
    	clearTimeout(timeoutcontroller);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });
    // setTimeout(function(){
    total_page = content.length;
    // }, 250);


});



/*===============================================
	 =            data highlight function            =
	 ===============================================*/
function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";


    if ($alltextpara.length > 0) {
        $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                (stylerulename = $(this).attr("data-highlightcustomclass")) :
                (stylerulename = "parsedstring");

            texthighlightstarttag = "<span>";


            replaceinstring = $(this).html();
            replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
            replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);


            $(this).html(replaceinstring);
        });
    }
}
/*=====  End of data highlight function  ======*/

//page 1

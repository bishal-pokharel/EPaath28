var imgpath = $ref+"/images/";
var soundAsset = $ref+"/audio_"+$lang+"/";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var content=[
  {
  	//start
  	contentblockadditionalclass: "simplebg",
  	contentnocenteradjust: true,
  	uppertextblockadditionalclass: "firsttitle",
  	uppertextblock: [{
  		textdata: data.string.p3_s1
  	}]
  },{
  	//page 1
  		contentblockadditionalclass: "simplebg",
  		contentnocenteradjust: true,
	  	imageblockadditionalclass: "upperimageblockholder",
	  	imageblock: [{
	  		imagestoshow: [{
	  			//1
	  			imgclass: "draggable tigercub draggableposition1",
	  			imgsrc: imgpath+ "tiger_diy.png",
				ans:"dendrop"
	  		},{
	  			//2
	  			imgclass: "draggable fish draggableposition1 hide",
	  			imgsrc: imgpath+ "fish_diy.png",
                ans:"ponddrop"
            },{
	  			//3
	  			imgclass: "draggable horse draggableposition1 hide",
	  			imgsrc: imgpath+ "horse_diy.png",
                ans:"horsedrop"
	  		},{
	  			//4
	  			imgclass: "draggable bee draggableposition1 hide",
	  			imgsrc: imgpath+ "bee_diy.png",
                ans:"beedrop"
            },{
	  			//5
	  			imgclass: "draggable  dog draggableposition1 hide",
	  			imgsrc: imgpath+ "dog_diy.png",
				ans:"dogdrop"
	  		},{
	  			//6
	  			imgclass: "draggable bird draggableposition1 hide",
	  			imgsrc: imgpath+ "bird_diy.png",
                ans:"nestdrop"
	  		},{
	  			//7
	  			imgclass: "draggable rabbit draggableposition1 hide",
	  			imgsrc: imgpath+ "rabit_diy.png",
                ans:"rabitdrop"
            },{
	  			//8
	  			imgclass: "draggable spider draggableposition1 hide",
	  			imgsrc: imgpath+ "spider_diy.png",
                ans:"spiderdrop"
	  		},{
          //monkey welldone
          imgclass: "monkeywelldone",
          imgsrc: imgpath+ "well.png"
        }],
	  		imagelabels: [{
	  			imagelabelclass: "draganddropinst",
	  			datahighlightflag: true,
	  			datahighlightcustomclass : "customparsedString",
	  			imagelabeldata: data.string.p3_s2
	  		}]
	  	}],
	  	specialdropdiv:[{
	  		//1
	  		dropimgclass: "beeimgclass",
	  		imgsrc: imgpath+ "beehibe_diy.png",
	  		dropdiv: "beedrop droppable"
	  	},{
	  		//2
	  		dropimgclass: "dogimgclass",
	  		imgsrc: imgpath+ "doghouse_diy.png",
	  		dropdiv: "dogdrop droppable"
	  	},{
	  		//3
	  		dropimgclass: "spiderimgclass",
	  		imgsrc: imgpath+ "spidernet_diy.png",
	  		dropdiv: "spiderdrop droppable"
	  	},{
	  		//4
	  		dropimgclass: "denimgclass",
	  		imgsrc: imgpath+ "den_diy.png",
	  		dropdiv: "dendrop droppable"
	  	},{
	  		//5
	  		dropimgclass: "horseimgclass",
	  		imgsrc: imgpath+ "horseshed_diy.png",
	  		dropdiv: "horsedrop droppable"
	  	},{
	  		//6
	  		dropimgclass: "pondimgclass",
	  		imgsrc: imgpath+ "pond01_diy.png",
	  		dropdiv: "ponddrop droppable"
	  	},{
	  		//7
	  		dropimgclass: "nestimgclass",
	  		imgsrc: imgpath+ "nest_diy.png",
	  		dropdiv: "nestdrop droppable"
	  	},{
	  		//8
			dropimgclass: "rabitimgclass",
	  		imgsrc: imgpath+ "rabithouse_diy.png",
	  		dropdiv: "rabitdrop droppable"
	  	}]
  }
];


$(function() {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var $total_page = content.length;

	loadTimelineProgress($total_page, countNext + 1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "mrs_sharma", src: imgpath+"mrs_sharma.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "temple_bg", src: imgpath+"temple_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "swimming_bg", src: imgpath+"swimming_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "restaurant", src: imgpath+"restaurant.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "elephant_bg", src: imgpath+"elephant_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "school_bg", src: imgpath+"school_bg.png", type: createjs.AbstractLoader.IMAGE},
			// sounds

			{id: "sound_1", src: soundAsset+"p3_s1.ogg"},
			//textboxes
			// {id: "dialog1_img", src: imgpath+'bubble-10.png', type: createjs.AbstractLoader.IMAGE},
			// {id: "bubblesc", src: imgpath+'bubblesc.png', type: createjs.AbstractLoader.IMAGE},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_0');
		// call main function
		templateCaller();
	}
	//initialize
	init();
	/*==================================================
	 =            Handlers and helpers Block            =
	 ==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	/*=====  End of data highlight function  ======*/

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templateCaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templateCaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

	}

	/*=====  End of user navigation controller function  ======*/

	/*=====  End of Handlers and helpers Block  ======*/

	/*=======================================
	 =            Templates Block            =
	 =======================================*/
	/*=================================================
	 =            general template function            =
	 =================================================*/
	var source = $("#general-template").html();
	var template = Handlebars.compile(source);
	var timeoutcontroller;
	function generalTemplate() {
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		//call notifyuser
		// notifyuser($anydiv);

		switch(countNext){
			case 0:
	    		play_diy_audio();
          nav_button_controls(2000);
	    		break;
			case 1:
	    		sound_player('sound_'+countNext);
				var $classtoshow;
				$classtoshow = $(".upperimageblockholder>.draggableposition1").eq(0);
				setTimeout(function(){
					$classtoshow.toggleClass("draggableposition1");
				}, 1000);
				$(".draggable").draggable({
					containment : "body",
					cursor : "crosshair",
					revert : "invalid",
					appendTo : "body",
					helper : "clone",
					zindex : 1000,
					start: function(event, ui){
						// $("body").css("background-color", "#aaffaa");

					},
					stop: function(event, ui){
						// $("body").css("background-color", "");
					}
				});
                $('.droppable').droppable({
                    	accept : ".draggable",
                    	hoverClass : "hovered",
                    	drop : handleCardDrop
                    });

				var divsdroppedcount = 0;
				function handleCardDrop(event, ui) {
					var dragdata = ui.draggable.attr("data-answer");
					if($(this).hasClass(dragdata) ){
                        ui.draggable.draggable('disable');
                        var dropped = ui.draggable;
                        var droppedOn = $(this);
                        play_correct_incorrect_sound(1);
                        $(dropped).detach().css({
                            "position": "absolute",
                            "left": "30%",
                            "bottom": "4%",
                            "width": "65%",
                            "height": "auto",
                            "max-height": "60%"
                        }).appendTo(droppedOn);


                        dropped.toggleClass("draggableposition1");
                        $classtoshow = $(".upperimageblockholder>.draggableposition1").eq(0);

                        if($classtoshow != null){
                            $classtoshow.toggleClass("hide");
                            setTimeout(function(){
                                $classtoshow.toggleClass("draggableposition1");
                            }, 1000);
                        }

                        divsdroppedcount++;
                        if(divsdroppedcount == 8){
                            $(".draganddropinst").toggleClass("fadelabel");
                            $(".sheddrop").toggleClass("animate1");
                            $(".dendrop").toggleClass("animate2");
                            $(".ponddrop").toggleClass("animate3");
                            $(".monkeywelldone").delay(100).show(0);
                            setTimeout(function(){
                                ole.footerNotificationHandler.lessonEndSetNotification();
                            }, 3000);
                        }
                    }
                    else{
						play_correct_incorrect_sound(0);
					}
				}

				break;
//
			default:
				break;
		}

	}
  function nav_button_controls(delay_ms){
    timeoutvar = setTimeout(function(){
      if(countNext==0){
        $nextBtn.show(0);
      } else if( countNext>0 && countNext == $total_page-1){
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else{
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    },delay_ms);
  }
	/*=====  End of Templates Block  ======*/
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	/*==================================================
	=            Templates Controller Block            =
	==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
	 Motivation :
	 - Make a single function call that handles all the
	 template load easier

	 How To:
	 - Update the template caller with the required templates
	 - Call template caller

	 What it does:
	 - According to value of the Global Variable countNext
	 the slide templates are updated
	 */

	function templateCaller() {
			/*always hide next and previous navigation button unless
			 explicitly called from inside a template*/
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// call navigation controller
			navigationcontroller();

			loadTimelineProgress($total_page, countNext + 1);
			// call the template
			generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


			//call the slide indication bar handler for pink indicators

			// just for development purpose to see total slide vs current slide number
			// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	/* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		clearTimeout(timeoutcontroller);
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/*=====  End of Templates Controller Block  ======*/
});

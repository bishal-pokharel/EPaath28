var imgpath = $ref+"/images/";
var soundAsset = $ref+"/audio_"+$lang+"/";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var content=[
 {
  	//start
  	contentblockadditionalclass: "firstpagebackground",
  	contentnocenteradjust: true,
  	uppertextblockadditionalclass: "firsttitle",
  	uppertextblock: [{
  		textdata: data.string.p6_heading
  	}],
    imageblock: [{
      imagestoshow:[{
        imgclass: "firstpagebirdcss",
        imgid: "",
        imgsrc: imgpath+"birdandeggs.png"
      }]
    }],
  },{
  	//page 1
  	contentblockadditionalclass: "simplebg",
  	contentnocenteradjust: true,
  	additionalcoverboardfullclass: "backgroundclass",
  	uppertextblockadditionalclass: "additionalutbclass",
  	uppertextblock: [{
  		textclass: "clickinstruction clickinstructionanimate",
  		textdata: data.string.p1_s1
  	},{
  		textclass: "homeinfo",
  		textdata: data.string.p1_s8
  	}],
  	imageblockadditionalclass: "additional_image_block",
  	imageblock: [{
  		imagestoshow:[{
  			imgclass: "feathers",
  			imgid: "",
  			imgsrc: imgpath+"feather.png"
  		},{
  			imgclass: "grass",
  			imgid: "",
  			imgsrc: imgpath+"grass.png"
  		},{
  			imgclass: "cotton",
  			imgid: "",
  			imgsrc: imgpath+"cotton.png"
  		},{
  			imgclass: "twigs",
  			imgid: "",
  			imgsrc: imgpath+"sticks.png"
  		},{
  			imgclass: "ribbons",
  			imgid: "",
  			imgsrc: imgpath+"ribbon.png"
  		},{
  			imgclass: "nest",
  			imgid: "",
  			imgsrc: imgpath+"01.png"
  		},{
  			imgclass: "nest02",
  			imgid: "",
  			imgsrc: imgpath+"07.png"
  		},{
  			imgclass: "birdy",
  			imgid: "",
  			imgsrc: imgpath+"bird.png"
  		},{
  			imgclass: "feathers",
  			imgid: "nestfeather",
  			imgsrc: imgpath+"05.png"
  		},{
  			imgclass: "grass",
  			imgid: "nestgrass",
  			imgsrc: imgpath+"02.png"
  		},{
  			imgclass: "cotton",
  			imgid: "nestcotton",
  			imgsrc: imgpath+"03.png"
  		},{
  			imgclass: "twigs",
  			imgid: "nesttwig",
  			imgsrc: imgpath+"04.png"
  		},{
  			imgclass: "ribbons",
  			imgid: "nestribbons",
  			imgsrc: imgpath+"06.png"
  		},{
  			imgclass: "featherclick",
  			imgid: "",
  			imgsrc: "images/hand-icon.gif"
  		},{
  			imgclass: "grassclick",
  			imgid: "",
  			imgsrc: "images/hand-icon.gif"
  		},
  		{
  			imgclass: "cottonclick",
  			imgid: "",
  			imgsrc: "images/hand-icon.gif"
  		},{
  			imgclass: "twigsclick",
  			imgid: "",
  			imgsrc: "images/hand-icon.gif"
  		},{
  			imgclass: "ribbonsclick",
  			imgid: "",
  			imgsrc: "images/hand-icon.gif"
  		}],
  		imagelabels:[{
  			imagelabelclass: "featherslabel",
  			imagelabeldata: data.string.p1_s3
  		},{
  			imagelabelclass: "grasslabel",
  			imagelabeldata: data.string.p1_s4
  		},{
  			imagelabelclass: "cottonlabel",
  			imagelabeldata: data.string.p1_s5
  		},{
  			imagelabelclass: "twigslabel",
  			imagelabeldata: data.string.p1_s6
  		},{
  			imagelabelclass: "ribbonslabel",
  			imagelabeldata: data.string.p1_s7
  		}]
  	}],
  },{
  	//page 2
  	contentblockadditionalclass: "simplebg",
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "title",
  		textdata: data.string.p1_s9
  	}],
  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		imagestoshow:[{
  			imgclass: "nest1",
  			imgsrc: imgpath+"nest01.png"
  		},{
  			imgclass: "nest2",
  			imgsrc: imgpath+"nest02.png"
  		},{
  			imgclass: "nest3",
  			imgsrc: imgpath+"nest03.png"
  		},{
  			imgclass: "nest4",
  			imgsrc: imgpath+"nest04.png"
  		},{
  			imgclass: "nest5",
  			imgsrc: imgpath+"nest05.png"
  		},{
  			imgclass: "nest6",
  			imgsrc: imgpath+"nest06.png"
  		},{
  			imgclass: "nest7",
  			imgsrc: imgpath+"nest07.png"
  		},{
  			imgclass: "nest8",
  			imgsrc: imgpath+"nest08.png"
  		}]
  	}]
  },{
  	//page 3
  	contentblockadditionalclass: "simplebg",
  	// contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "description1",
  		textdata: data.string.p1_s10
  	},{
  		textclass: "description1",
  		textdata: data.string.p1_s10_1
  	}]
  },{
  	//page 4
  	contentnocenteradjust: true,

  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		imagestoshow:[{
  			imgclass: "animalhomeexample border1 animatioeffect2",
  			imgsrc: imgpath+"rabbits.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "animalhomedescription",
  			datahighlightflag: true,
  			imagelabeldata: data.string.p1_s11
  		}]

  	}]
  },{
  	//page 5
  	contentnocenteradjust: true,

  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		imagestoshow:[{
  			imgclass: "animalhomeexample border2 animatioeffect3",
  			imgsrc: imgpath+"rat.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "animalhomedescription",
  			datahighlightflag: true,
  			imagelabeldata: data.string.p1_s12
  		}]

  	}]
  },{
  	//page 6

  	contentnocenteradjust: true,

  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		imagestoshow:[{
  			imgclass: "animalhomeexample border3 animatioeffect4",
  			imgsrc: imgpath+"ant.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "animalhomedescription",
  			datahighlightflag: true,
  			imagelabeldata: data.string.p1_s13
  		}]

  	}]
  },{
  	//page 7
  	contentnocenteradjust: true,

  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		imagestoshow:[{
  			imgclass: "animalhomeexample border4 animatioeffect5",
  			imgsrc: imgpath+"spider.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "animalhomedescription",
  			datahighlightflag: true,
  			imagelabeldata: data.string.p1_s14
  		}]

  	}]
  },{
	//page 8
  	contentnocenteradjust: true,

  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		imagestoshow:[{
  			imgclass: "animalhomeexample border5 animatioeffect1",
  			imgsrc: imgpath+"hive.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "animalhomedescription",
  			datahighlightflag: true,
  			imagelabeldata: data.string.p1_s15
  		}]

  	}]
  },{
  	//page 9
  	contentnocenteradjust: true,

  	uppertextblock: [{
  		textclass: "description2",
  		textdata: data.string.p1_s16
  	},{
  		textclass: "description2",
  		textdata: data.string.p1_s17
  	}],

  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		imagestoshow:[{
  			imgclass: "animalhomeexample border1 animatioeffect2",
  			imgsrc: imgpath+"dog.jpg"
  		}]
  	}]
  },{
  	//page 10
  	contentnocenteradjust: true,

  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		imagestoshow:[{
  			imgclass: "animalhomeexample border2 animatioeffect3",
  			imgsrc: imgpath+"dog.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "animalhomedescription",
  			datahighlightflag: true,
  			imagelabeldata: data.string.p1_s18
  		}]

  	}]
  },{
  	//page 11
  	contentnocenteradjust: true,

  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		imagestoshow:[{
  			imgclass: "animalhomeexample border3 animatioeffect4",
  			imgsrc: imgpath+"hen.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "animalhomedescription",
  			datahighlightflag: true,
  			imagelabeldata: data.string.p1_s19
  		}]

  	}]
  },{
  	// page 12
  	contentnocenteradjust: true,

  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		imagestoshow:[{
  			imgclass: "animalhomeexample border4 animatioeffect5",
  			imgsrc: imgpath+"cow.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "animalhomedescription",
  			datahighlightflag: true,
  			imagelabeldata: data.string.p1_s20
  		}]

  	}]
  },{
  	// page 13
  	contentnocenteradjust: true,

  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		imagestoshow:[{
  			imgclass: "animalhomeexample border5 animatioeffect1",
  			imgsrc: imgpath+"pig.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "animalhomedescription",
  			datahighlightflag: true,
  			imagelabeldata: data.string.p1_s21
  		}]

  	}]
  },{
	//page 14
  	contentnocenteradjust: true,

  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		imagestoshow:[{
  			imgclass: "animalhomeexample border1 animatioeffect2",
  			imgsrc: imgpath+"goat.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "animalhomedescription",
  			datahighlightflag: true,
  			imagelabeldata: data.string.p1_s22
  		}]

  	}]
  },{
	//page 15
  	contentnocenteradjust: true,

  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		imagestoshow:[{
  			imgclass: "animalhomeexample border2 animatioeffect1",
  			imgsrc: imgpath+"horse.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "animalhomedescription",
  			datahighlightflag: true,
  			imagelabeldata: data.string.p1_s23
  		}]

  	}]
  }
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;
  var vocabcontroller =  new Vocabulary();
  vocabcontroller.init();

  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "mrs_sharma", src: imgpath+"mrs_sharma.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "temple_bg", src: imgpath+"temple_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "swimming_bg", src: imgpath+"swimming_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "restaurant", src: imgpath+"restaurant.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "elephant_bg", src: imgpath+"elephant_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "school_bg", src: imgpath+"school_bg.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"p1_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p1_s1.ogg"},
			{id: "sound_1_1", src: soundAsset+"p1_s1_1.ogg"},
			{id: "sound_2", src: soundAsset+"p1_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p1_s3.ogg"},
			{id: "sound_4", src: soundAsset+"p1_s4.ogg"},
			{id: "sound_5", src: soundAsset+"p1_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p1_s6.ogg"},
			{id: "sound_7", src: soundAsset+"p1_s7.ogg"},
			{id: "sound_8", src: soundAsset+"p1_s8.ogg"},
			{id: "sound_9", src: soundAsset+"p1_s9.ogg"},
			{id: "sound_10", src: soundAsset+"p1_s10.ogg"},
			{id: "sound_11", src: soundAsset+"p1_s11.ogg"},
			{id: "sound_12", src: soundAsset+"p1_s12.ogg"},
			{id: "sound_13", src: soundAsset+"p1_s13.ogg"},
			{id: "sound_14", src: soundAsset+"p1_s14.ogg"},
			{id: "sound_15", src: soundAsset+"p1_s15.ogg"},
			//textboxes
			// {id: "dialog1_img", src: imgpath+'bubble-10.png', type: createjs.AbstractLoader.IMAGE},
			// {id: "bubblesc", src: imgpath+'bubblesc.png', type: createjs.AbstractLoader.IMAGE},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_0');
		// call main function
		templateCaller();
	}
	//initialize
	init();
/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
    /*=====  End of data highlight function  ======*/

    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

	}
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  	var animalarray =[];
   	var plantarray = [];
	var timeoutcontroller;
  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
	vocabcontroller.findwords(countNext);
    switch(countNext){
    	case 0:
    		sound_nav('sound_'+countNext);
    		break;
    	case 1:
    		sound_player('sound_'+countNext);
    		$nextBtn.hide(0);
    		var clickcount = 0;
    		var $featherclick = $(".featherclick");
    		var $feathers = $(".feathers");
    		var $grassclick = $(".grassclick");
    		var $grass = $(".grass");
    		var $cottonclick = $(".cottonclick");
    		var $cotton = $(".cotton");
    		var $twigs = $(".twigs");
    		var $twigsclick = $(".twigsclick");
    		var $ribbons = $(".ribbons");
    		var $ribbonsclick = $(".ribbonsclick");

    		var count = 0;
    		function setClick(param1, param2, $param3, $param4, param5){
    			$("."+param1+", ."+param2).click(function(){
    			if($param3.hasClass("unselectgray")){
    				return false;
    			}

    			$param4.hide(0);
    			$param3.addClass("unselectgray");
    			$("#"+param5).show(0);
    			count++;
    			if(count == 1){
    				var $instru = $(".clickinstruction");
    				$instru.toggleClass("clickinstructionanimate");
    				$instru.toggleClass("fadedivaway");
    				$instru.delay(3000).hide(0);
    			}

    			if(count == 5){

    				$(".nest02").show(0);
    				$(".birdy").show(0);
    				timeoutcontroller = setTimeout(function(){
	    				$(".homeinfo").show(0);
			    		sound_nav('sound_1_1');
    				}, 3000);
    			}
    		});
    		}
    		setClick("featherclick", "feathers", $feathers.eq(0), $featherclick, "nestfeather");
    		setClick("grassclick", "grass", $grass.eq(0), $grassclick, "nestgrass");
    		setClick("cottonclick", "cotton", $cotton.eq(0), $cottonclick, "nestcotton");
    		setClick("twigsclick", "twigs", $twigs.eq(0), $twigsclick, "nesttwig");
    		setClick("ribbonsclick", "ribbons", $ribbons.eq(0), $ribbonsclick, "nestribbons");

    		break;
    	case 3:
    		sound_nav('sound_'+countNext);
    		break;
    	case 2:
    		sound_player('sound_'+countNext);
    		$('.coverboardfull').css('background-color', '#A4DAA8');
    		nav_button_controls(9000);
    		break;
    	case 4:
    		timeoutcontroller = setTimeout(function(){
	    		sound_nav('sound_'+countNext);
			}, 2000);
    		$('.coverboardfull').css('background-color', '#FFC965');
    		break;
    	case 5:
    		timeoutcontroller = setTimeout(function(){
	    		sound_nav('sound_'+countNext);
			}, 2000);
    		$('.coverboardfull').css('background-color', '#6DB8CB');
    		break;
    	case 6:
    		timeoutcontroller = setTimeout(function(){
	    		sound_nav('sound_'+countNext);
			}, 2000);
    		$('.coverboardfull').css('background-color', '#FFCC86');
    		break;
    	case 7:
    		timeoutcontroller = setTimeout(function(){
	    		sound_nav('sound_'+countNext);
			}, 2000);
    		$('.coverboardfull').css('background-color', '#FFF3EA');
    		break;
    	case 8:
    		timeoutcontroller = setTimeout(function(){
	    		sound_nav('sound_'+countNext);
			}, 2000);
    		$('.coverboardfull').css('background-color', '#B6E4DC');
    		break;
    	case 9:
    		timeoutcontroller = setTimeout(function(){
	    		sound_nav('sound_'+countNext);
			}, 2000);
    		$('.coverboardfull').css('background-color', '#7ADFDB');
    		break;
    	case 10:
    		timeoutcontroller = setTimeout(function(){
	    		sound_nav('sound_'+countNext);
			}, 2000);
    		$('.coverboardfull').css('background-color', '#C8ECE0');
    		break;
    	case 11:
    		timeoutcontroller = setTimeout(function(){
	    		sound_nav('sound_'+countNext);
			}, 2000);
    		$('.coverboardfull').css('background-color', '#FFCC86');
    		break;
    	case 12:
    		timeoutcontroller = setTimeout(function(){
	    		sound_nav('sound_'+countNext);
			}, 2000);
    		$('.coverboardfull').css('background-color', '#FFC965');
    		break;
    	case 13:
    		timeoutcontroller = setTimeout(function(){
	    		sound_nav('sound_'+countNext);
			}, 2000);
    		$('.coverboardfull').css('background-color', '#6DB8CB');
    		break;
    	case 14:
    		timeoutcontroller = setTimeout(function(){
	    		sound_nav('sound_'+countNext);
			}, 2000);
    		$('.coverboardfull').css('background-color', '#FFF3EA');
    		break;
    	case 15:
    		timeoutcontroller = setTimeout(function(){
	    		sound_nav('sound_'+countNext);
			}, 2000);
    		$('.coverboardfull').css('background-color', '#00D8E4');
    		break;

    	default:
    		break;
    }
  }
  function nav_button_controls(delay_ms){
    timeoutvar = setTimeout(function(){
      if(countNext==0){
        $nextBtn.show(0);
      } else if( countNext>0 && countNext == $total_page-1){
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else{
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    },delay_ms);
  }
/*=====  End of Templates Block  ======*/
function sound_nav(sound_id){
  createjs.Sound.stop();
  current_sound = createjs.Sound.play(sound_id);
  current_sound.play();
  current_sound.on("complete", function(){
    nav_button_controls(0);
  });
}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

 	function templateCaller() {
			/*always hide next and previous navigation button unless
			 explicitly called from inside a template*/
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// call navigation controller
			navigationcontroller();

      loadTimelineProgress($total_page, countNext + 1);
			// call the template
			generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


			//call the slide indication bar handler for pink indicators

			// just for development purpose to see total slide vs current slide number
			// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/
  // countNext+=1;

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */
 	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		clearTimeout(timeoutcontroller);
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref + "/images/";
var soundAsset1 = $ref+"/sounds/p1/";
var soundAsset = $ref+"/sounds/"+$lang+"/";


var content=[
	// slide0
	{
		// contentnocenteradjust: true,

		uppertextblockadditionalclass:'lesson-title my_font_super_big patrickhand',
		uppertextblock:[{
			textdata: data.lesson.chapter,
			textclass: "",
		}],
        imageblock:[{
            imagestoshow: [
                {
                    imgclass: "coverpage img1",
                    imgid: 'bgmain',
                    imgsrc: ""
                },
            ]
        }]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblock:[
			{
				textclass: "livnonfadein p1para1",
				textdata: data.string.p1t1
			}
		],
		imageblock:[{
			imagestoshow : [
				{
					imgclass: "item hen",
					imgid : 'hen',
					imgname: data.string.hen,
					imgsrc: "",
					dataname:"Hen"
				},
				{
					imgclass: "item pond",
					imgid : 'pond',
					imgname:  data.string.pond,
					imgsrc: "",
					dataname:"Pond"
				},
				{
					imgclass: "item sun",
					imgid : 'sun',
					imgname:  data.string.sun,
					imgsrc: "",
					dataname:"Sun"
				},
				{
					imgclass: "item duck",
					imgid : 'duck',
					imgname:  data.string.duck,
					imgsrc: "",
					dataname:"Duck"
				},
				{
					imgclass: "item house",
					imgid : 'house',
					imgname: data.string.house,
					imgsrc: "",
					dataname:"House"
				},
				{
					imgclass: "item shed",
					imgid : 'shed',
					imgname: data.string.shed,
					imgsrc: "",
					dataname:"Shed"
				},
				{
					imgclass: "item tree1",
					imgid : 'tree1',
					imgname: data.string.tree,
					imgsrc: "",
					dataname:"Tree"
				},
				{
					imgclass: "item tree2",
					imgid : 'tree2',
					imgname: data.string.tree,
					imgsrc: "",
					dataname:"Tree"
				},{
					imgclass: "item flower",
					imgid : 'flower',
					imgname: data.string.flower,
					imgsrc: "",
					dataname:"Flower"
				},
				{
					imgclass: "item man",
					imgid : 'man',
					imgname: data.string.man,
					imgsrc: "",
					dataname:"Man"
				},
				{
					imgclass: "item stone",
					imgid : 'stone',
					imgname: data.string.stone,
					imgsrc: "",
					dataname:"Stone"
				},
			]
		}]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				textclass: "livnonfadein p1para1",
				textdata: data.string.p1t2
			}
		],
		imageblock:[{
			imagestoshow : [
				{
					imgclass: "item hen",
					imgid : 'hen',
					imgsrc: ""
				},
				{
					imgclass: "item selectthis pond",
					imgid : 'pond',
					imgsrc: ""
				},
				{
					imgclass: "item selectthis sun",
					imgid : 'sun',
					imgsrc: ""
				},
				{
					imgclass: "item duck",
					imgid : 'duck',
					imgsrc: ""
				},
				{
					imgclass: "item selectthis house",
					imgid : 'house',
					imgsrc: ""
				},
				{
					imgclass: "item selectthis shed",
					imgid : 'shed',
					imgsrc: ""
				},
				{
					imgclass: "item tree1",
					imgid : 'tree1',
					imgsrc: ""
				},
				{
					imgclass: "item tree2",
					imgid : 'tree2',
					imgsrc: ""
				},{
					imgclass: "item flower",
					imgid : 'flower',
					imgname: 'flower',
					imgsrc: ""
				},
				{
					imgclass: "item man",
					imgid : 'man',
					imgsrc: ""
				},
				{
					imgclass: "item selectthis stone",
					imgid : 'stone',
					imgsrc: ""
				},
			]
		}]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				textclass: "livnonfadein p1para1",
				textdata: data.string.p1t3
			}
		],
		imageblock:[{
			imagestoshow : [
				{
					imgclass: "item selectthis hen",
					imgid : 'hen',
					imgsrc: ""
				},
				{
					imgclass: "item pond",
					imgid : 'pond',
					imgsrc: ""
				},
				{
					imgclass: "item sun",
					imgid : 'sun',
					imgsrc: ""
				},
				{
					imgclass: "item selectthis duck",
					imgid : 'duck',
					imgsrc: ""
				},
				{
					imgclass: "item house",
					imgid : 'house',
					imgsrc: ""
				},
				{
					imgclass: "item shed",
					imgid : 'shed',
					imgsrc: ""
				},
				{
					imgclass: "item tree1",
					imgid : 'tree1',
					imgsrc: ""
				},
				{
					imgclass: "item tree2",
					imgid : 'tree2',
					imgsrc: ""
				},{
					imgclass: "item flower",
					imgid : 'flower',
					imgname: 'flower',
					imgsrc: ""
				},
				{
					imgclass: "item selectthis man",
					imgid : 'man',
					imgsrc: ""
				},
				{
					imgclass: "item stone",
					imgid : 'stone',
					imgsrc: ""
				},
			]
		}]
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				textclass: "livnonfadein p1para1",
				textdata: data.string.p1t4
			}
		],
		imageblock:[{
			imagestoshow : [
				{
					imgclass: "item hen",
					imgid : 'hen',
					imgsrc: ""
				},
				{
					imgclass: "item pond",
					imgid : 'pond',
					imgsrc: ""
				},
				{
					imgclass: "item sun",
					imgid : 'sun',
					imgsrc: ""
				},
				{
					imgclass: "item duck",
					imgid : 'duck',
					imgsrc: ""
				},
				{
					imgclass: "item house",
					imgid : 'house',
					imgsrc: ""
				},
				{
					imgclass: "item shed",
					imgid : 'shed',
					imgsrc: ""
				},
				{
					imgclass: "item selectthis tree1",
					imgid : 'tree1',
					imgsrc: ""
				},
				{
					imgclass: "item selectthis tree2",
					imgid : 'tree2',
					imgsrc: ""
				},{
					imgclass: "item selectthis flower",
					imgid : 'flower',
					imgname: 'flower',
					imgsrc: ""
				},
				{
					imgclass: "item man",
					imgid : 'man',
					imgsrc: ""
				},
				{
					imgclass: "item stone",
					imgid : 'stone',
					imgsrc: ""
				},
			]
		}]
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		uppertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				textclass: "livnonfadein p1para1",
				textdata: data.string.p1t5
			}
		],
		imageblock:[{
			imagestoshow : [
				{
					imgclass: "item selectthis hen",
					imgid : 'hen',
					imgsrc: ""
				},
				{
					imgclass: "item pond",
					imgid : 'pond',
					imgsrc: ""
				},
				{
					imgclass: "item sun",
					imgid : 'sun',
					imgsrc: ""
				},
				{
					imgclass: "item selectthis duck",
					imgid : 'duck',
					imgsrc: ""
				},
				{
					imgclass: "item house",
					imgid : 'house',
					imgsrc: ""
				},
				{
					imgclass: "item shed",
					imgid : 'shed',
					imgsrc: ""
				},
				{
					imgclass: "item selectthis tree1",
					imgid : 'tree1',
					imgsrc: ""
				},
				{
					imgclass: "item selectthis tree2",
					imgid : 'tree2',
					imgsrc: ""
				},{
					imgclass: "item selectthis flower",
					imgid : 'flower',
					imgname: 'flower',
					imgsrc: ""
				},
				{
					imgclass: "item selectthis man",
					imgid : 'man',
					imgsrc: ""
				},
				{
					imgclass: "item stone",
					imgid : 'stone',
					imgsrc: ""
				},
			]
		}]
	},
	// slide6
	{
		contentblockadditionalclass: "ole-backrgound-gradient-pool",
		uppertextblock:[
			{
				textclass: "livnonfadein2 p1para2",
				textdata: data.string.p1t6
			}
		],
		livinnonlivin:[
			{
				lbadditionalclass: "theliving",
				thetitle: data.string.living,
				imageblock:[{
					imagestoshow:[
						{
							imgclass: "ins-chicken",
							imgid : 'hen',
							imgsrc: ""
						},
						{
							imgclass: "ins-man",
							imgid : 'man',
							imgsrc: ""
						},
						{
							imgclass: "ins-flower",
							imgid : 'flower',
							imgsrc: ""
						},
						{
							imgclass: "ins-duck",
							imgid : 'duck',
							imgsrc: ""
						},
						{
							imgclass: "ins-tree",
							imgid : 'tree1',
							imgsrc: ""
						}
					]
				}]
			},
			{
				lbadditionalclass: "thenonliving",
				thetitle: data.string.nonliving,
				imageblock:[{
					imagestoshow:[
						{
							imgclass: "ins-sun",
							imgid : 'sun',
							imgsrc: ""
						},
						{
							imgclass: "ins-stone",
							imgid : 'stone',
							imgsrc: ""
						},
						{
							imgclass: "ins-goth",
							imgid : 'shed',
							imgsrc: ""
						},
						{
							imgclass: "ins-house",
							imgid : 'house',
							imgsrc: ""
						},
						{
							imgclass: "ins-pond",
							imgid : 'pond',
							imgsrc: ""
						}
					]
				}]
			}
		]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "coverpageImg", src: imgpath+"coverpage.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "bgmain", src: imgpath+"bg_main.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hen", src: imgpath+"chicken.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pond", src: imgpath+"pond02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "duck", src: imgpath+"duck.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sun", src: imgpath+"sun.png", type: createjs.AbstractLoader.IMAGE},
			{id: "house", src: imgpath+"hosue.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shed", src: imgpath+"goth.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tree1", src: imgpath+"tree01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tree2", src: imgpath+"tree03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "flower", src: imgpath+"flower.png", type: createjs.AbstractLoader.IMAGE},
			{id: "man", src: imgpath+"man.png", type: createjs.AbstractLoader.IMAGE},
			{id: "stone", src: imgpath+"stone.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
            // // sounds
            {id: "sound_1", src: soundAsset+"s1_p1.ogg"},
            {id: "sound_2", src: soundAsset+"s1_p2.ogg"},
            {id: "sound_3", src: soundAsset+"s1_p3.ogg"},
            {id: "sound_4", src: soundAsset+"s1_p4.ogg"},
            {id: "sound_5", src: soundAsset+"s1_p5.ogg"},
            {id: "sound_6", src: soundAsset+"s1_p6.ogg"},
            {id: "sound_7", src: soundAsset+"s1_p7.ogg"},
            {id: "s_Duck", src: soundAsset+"s1_p2_duck.ogg"},
            {id: "s_Flower", src: soundAsset+"s1_p2_flower.ogg"},
            {id: "s_Hen", src: soundAsset+"s1_p2_hen.ogg"},
            {id: "s_House", src: soundAsset+"s1_p2_house.ogg"},
            {id: "s_Man", src: soundAsset+"s1_p2_man.ogg"},
            {id: "s_Pond", src: soundAsset+"s1_p2_pond.ogg"},
            {id: "s_Shed", src: soundAsset+"s1_p2_shed.ogg"},
            {id: "s_Stone", src: soundAsset+"s1_p2_stone.ogg"},
            {id: "s_Sun", src: soundAsset+"s1_p2_sun.ogg"},
            {id: "s_Tree", src: soundAsset+"s1_p2_tree.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandlergen.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=============================================imgName====
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_image2(content, countNext);
        vocabcontroller.findwords(countNext);
        if (countNext!=1) sound_player("sound_"+(countNext+1),true);
        switch(countNext){
			case 1:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_2");
			current_sound.play();
	    current_sound.on('complete', function(){
				$(".item").mouseover(function(){
					var labelpos = $(this).position();
					var imgName = $(this).attr('name');
					var audioName = $(this).attr('data-name');
					// alert("s_"+audioName);
              sound_player("s_"+audioName,true);
              $(".contentblock").append("<span class='lnlabel'>"+imgName+"</span>");
					$(".lnlabel").css({
						top: labelpos.top - 50,
						left: labelpos.left + 10,
						padding: 10
					});
				})
				.mouseleave(function(){
					$(".lnlabel").remove();
				});
				setTimeout(()=>navigationcontroller(),1000);
			});
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,navigate){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
        current_sound.on('complete', function(){
            navigate?navigationcontroller():"";
        });
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}

		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');


		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

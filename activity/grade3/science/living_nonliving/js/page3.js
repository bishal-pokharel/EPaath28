var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	{
		contentblockadditionalclass: "diyback",
		uppertextblock: [{
				textdata: data.string.diy,
				textclass: 'diytext'
		}],
	},
	// slide0
	{
		uppertextblock: [{
				textdata: data.string.diyins,
				textclass: 'ques'
		},
		],

        imageblock: [
            {
                imagestoshow: [
                    {
                        imgclass: "monkey",
                        imgid: "monkeyImg",
                        imgsrc: ""
                    },
                ]
            }
        ],
		draggableblock: [{
				draggables: [{
						draggableclass: "class_1 sliding1",
						dragldata: data.string.dragme1
				},
				{
						draggableclass: "class_2 sliding2 letshide",
						dragldata: data.string.dragme2
				},
				{
						draggableclass: "class_1 sliding3 letshide",
						dragldata: data.string.dragme3
				},
				{
						draggableclass: "class_1 sliding4 letshide",
						dragldata: data.string.dragme4
				},
				{
						draggableclass: "class_2 sliding5 letshide",
						dragldata: data.string.dragme5
				},
				{
						draggableclass: "class_1 sliding6 letshide",
						dragldata: data.string.dragme6
				},
				{
						draggableclass: "class_2 sliding7 letshide",
						dragldata: data.string.dragme7
				},
				{
						draggableclass: "class_1 sliding8 letshide",
						dragldata: data.string.dragme8
				},
				{
						draggableclass: "class_1 sliding9 letshide",
						dragldata: data.string.dragme9
				},
				{
						draggableclass: "class_2 sliding10 letshide",
						dragldata: data.string.dragme10
				},
				{
						draggableclass: "class_2 sliding11 letshide",
						dragldata: data.string.dragme11
				},
				{
						draggableclass: "class_2 sliding12 letshide",
						dragldata: data.string.dragme12
				},]
		}],
		droppableblock: [{
				droppables: [{
						headerdata: data.string.e1,
						droppablecontainerclass: "",
						droppableclass: "drop_class_1",
						imgclass: "",

				}, {
						headerdata: data.string.e2,
						droppablecontainerclass: "",
						droppableclass: "drop_class_2",
						imgclass: "",

				}]
		}],
	},
	{
		uppertextblock: [
			{
				textdata: data.string.lastt,
				textclass: 'toptext'
			},
			{
				textdata: data.string.walk,
				textclass: 'theimages walkte hide1'
			},
			{
				textdata: data.string.grow,
				textclass: 'theimages growte hide2'
			},
			{
				textdata: data.string.eat,
				textclass: 'theimages eatte hide3'
			},
			{
				textdata: data.string.feel,
				textclass: 'theimages feelte hide4'
			},
			{
				textdata: data.string.baby,
				textclass: 'theimages babyte hide5'
			},
			{
				textdata: data.string.breathe,
				textclass: 'theimages breathete hide6'
			},
			{
				textdata: data.string.excreate,
				textclass: 'theimages excreatete hide7'
			}
	],
		imageblock: [
		{
			imagestoshow: [
				{
					imgclass : "theimages boy hide1",
					imgsrc : '',
					imgid : 'walk'
				},
				{
					imgclass : "theimages plant hide2",
					imgsrc : '',
					imgid : 'plant'
				},
				{
					imgclass : "theimages cow hide3",
					imgsrc : '',
					imgid : 'cow'
				},
				{
					imgclass : "theimages parrot hide5",
					imgsrc : '',
					imgid : 'parrot'
				},
				{
					imgclass : "theimages breathe hide6",
					imgsrc : '',
					imgid : 'breathe'
				},
				{
					imgclass : "theimages elep hide7",
					imgsrc : '',
					imgid : 'elep'
				},
				{
					imgclass : "theimages feel hide4",
					imgsrc : '',
					imgid : 'feel'
				}
			],
		}
		]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "walk", src: imgpath+"boywalking.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "plant", src: imgpath+"plant02.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "cow", src: imgpath+"cow_eating_grass.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "feel", src: imgpath+"touchmenotplant.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "parrot", src: imgpath+"parrot_kids.png", type: createjs.AbstractLoader.IMAGE},
			{id: "breathe", src: imgpath+"breathing-pattern.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "elep", src: imgpath+"elephant_excreates.png", type: createjs.AbstractLoader.IMAGE},
			{id: "monkeyImg", src: imgpath + "welldone02.png", type: createjs.AbstractLoader.IMAGE},
            // sounds
            {id: "sound_1", src: soundAsset+"s3_p2.ogg"},
            {id: "sound_2", src: soundAsset+"s3_p3.ogg"},
            {id: "sound_3", src: soundAsset+"s3_p4.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("droppablecontent", $("#droppablecontent-partial").html());
	Handlebars.registerPartial("draggablecontent", $("#draggablecontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		var jpt = 2;
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
        vocabcontroller.findwords(countNext);
        $(".draggable").draggable({
				containment: "body",
				revert: function(coming){
					if(!coming){
						createjs.Sound.stop();

						play_correct_incorrect_sound(0);
					}
						return true;
				},
				appendTo: "body",
				zindex: 1000,
		});

		$('.drop_class_1').droppable({
				accept : ".class_1",
				hoverClass: "hovered",
				drop: function(event, ui) {
							ui.draggable.hide(0);
							$(".drop_class_1").append("<p class='itsdropped'>"+ui.draggable.text()+"</p>");
							createjs.Sound.stop();

							play_correct_incorrect_sound(1);
							newQuestion();
				}
		});

		$('.drop_class_2').droppable({
				accept : ".class_2",
				hoverClass: "hovered",
				drop: function(event, ui) {
							ui.draggable.hide(0);
							$(".drop_class_2").append("<p class='itsdropped'>"+ui.draggable.text()+"</p>");
							createjs.Sound.stop();

							play_correct_incorrect_sound(1);
							newQuestion();
				}
		});

		function newQuestion(){
			$(".sliding"+jpt).removeClass("letshide");
			jpt++;
			if(jpt == 14){
				sound_player("sound_3",true)
				$(".welldone").show(0);
                $(".ques").text(data.string.reward);
                $(".monkey").animate({"opacity":"1"},1000);
			}
		};

		switch(countNext){
			case 0:
			play_diy_audio();
			nav_button_controls(1200);
			break;
			case 1:
                sound_player("sound_1");
                break;
            case 2:
                sound_player("sound_2",1);
                var delaytime=0;
				for(var i=1;i<8;i++){
					delaytime= delaytime+1000;
					$(".hide"+i).hide().delay(delaytime).fadeIn(1000);
				}
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,next1){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete',function(){
			next1?navigationcontroller(true):"";
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}

		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// if(countNext != 1)
		// navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

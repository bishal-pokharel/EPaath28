var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'ole-background-gradient-blunatic',
		uppertextblock:[
			{
				textclass: "p2title",
				textdata: data.string.p2t1
			},
			{
				textclass: "p2text bluranim",
				textdata: data.string.p2t2
			}
		],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bigimgctr theimganim",
					imgsrc : '',
					imgid : 'centerone'
				}
			]
		}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'ole-background-gradient-blunatic',
		uppertextblock:[
			{
				textclass: "p2title",
				textdata: data.string.p2t1
			},
			{
				textclass: "p2text bluranim",
				textdata: data.string.p2t3
			},
			{
				textclass: "cap-no1",
				textdata: data.string.p2t4
			},
			{
				textclass: "cap-no2",
				textdata: data.string.p2t5
			},
			{
				textclass: "cap-no3",
				textdata: data.string.p2t7
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "theimganim smallimg1",
					imgid : 'centerone',
					imgsrc: ''
				},
				{
					imgclass: "theimganim smallimg2",
					imgid : 'fish',
					imgsrc: ''
				},
				{
					imgclass: "theimganim smallimg3",
					imgid : 'bird',
					imgsrc: ''
				}
			]
		}]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'ole-background-gradient-mustard',
		uppertextblock:[
			{
                datahighlightflag: true,
                datahighlightcustomclass: "hightext2",
				textclass: "p2text2 bluranim",
				textdata: data.string.p2t10
			}
		],
		lbadditionalclass: "thenonliving",
		thetitle: data.string.nonliving,
		livinnonlivin:[
			{
				lbadditionalclass: "theimganimmin bikecont",
				thetitle: data.string.bike,
				imageblock:[{
					imagestoshow:[
						{
							imgclass: "bicycle a",
							imgid : 'bike',
							imgsrc: ""
						}
					]
				}]
			},
			{
				lbadditionalclass: "theimganim fancont",
				thetitle: data.string.fan,
				imageblock:[{
					imagestoshow:[
						{
							imgclass: "bicycle b",
							imgid : 'fan',
							imgsrc: ""
						}
					]
				}]
			},
			{
				lbadditionalclass: "theimganimplu carcont",
				thetitle: data.string.car,
				imageblock:[{
					imagestoshow:[
						{
							imgclass: "bicycle c",
							imgid : 'car',
							imgsrc: ""
						}
					]
				}]
			}
		]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'ole-background-gradient-blunatic',
		uppertextblock:[
			{
				textclass: "p2title",
				textdata: data.string.p2t11
			},
			{
				textclass: "p2text bluranim",
				textdata: data.string.p2t12
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "theimganim humang",
					imgid : 'humang',
					imgsrc: ''
				},
				{
					imgclass: "theimganim parrotg",
					imgid : 'parrotg',
					imgsrc: ''
				},
				{
					imgclass: "theimganim plantg",
					imgid : 'plantg',
					imgsrc: ''
				}
			]
		}]
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'ole-background-gradient-mustard',
		uppertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext2",
				textclass: "p2text2 bluranim",
				textdata: data.string.p2t10_1
			}
		],
		lbadditionalclass: "thenonliving",
		thetitle: data.string.nonliving,
		livinnonlivin:[
			{
				lbadditionalclass: "theimganimmin bikecont",
				thetitle: data.string.table,
				imageblock:[{
					imagestoshow:[
						{
							imgclass: "bicycle a",
							imgid : 'table',
							imgsrc: ""
						}
					]
				}]
			},
			{
				lbadditionalclass: "theimganim fancont",
				thetitle: data.string.pencil,
				imageblock:[{
					imagestoshow:[
						{
							imgclass: "bicycle b",
							imgid : 'pencil',
							imgsrc: ""
						}
					]
				}]
			},
			{
				lbadditionalclass: "theimganimplu carcont",
				thetitle: data.string.housec,
				imageblock:[{
					imagestoshow:[
						{
							imgclass: "bicycle c",
							imgid : 'house',
							imgsrc: ""
						}
					]
				}]
			}
		]
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'ole-background-gradient-blunatic',
		uppertextblock:[
			{
				textclass: "p2title",
				textdata: data.string.p2t13
			},
			{
				textclass: "p2text bluranim",
				textdata: data.string.p2t14
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "theimganim humang",
					imgid : 'monkeye',
					imgsrc: ''
				},
				{
					imgclass: "theimganim parrotg",
					imgid : 'parrote',
					imgsrc: ''
				},
				{
					imgclass: "theimganim plantg",
					imgid : 'cowe',
					imgsrc: ''
				}
			]
		}]
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'ole-background-gradient-mustard',
		uppertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				textclass: "p2text2 bluranim",
				textdata: data.string.p2t15
			}
		],
		lbadditionalclass: "thenonliving",
		thetitle: data.string.nonliving,
		livinnonlivin:[
			{
				lbadditionalclass: "theimganimmin bikecont",
				thetitle: data.string.car,
				imageblock:[{
					imagestoshow:[
						{
							imgclass: "bicycle2 a",
							imgid : 'petrol',
							imgsrc: ""
						}
					]
				}]
			},
			{
				lbadditionalclass: "theimganim fancont",
				thetitle: data.string.fan,
				imageblock:[{
					imagestoshow:[
						{
							imgclass: "bicycle2 b",
							imgid : 'efan',
							imgsrc: ""
						}
					]
				}]
			},
			{
				lbadditionalclass: "theimganimplu carcont",
				thetitle: data.string.torch,
				imageblock:[{
					imagestoshow:[
						{
							imgclass: "bicycle2 c",
							imgid : 'torch',
							imgsrc: ""
						}
					]
				}]
			}
		]
	},
	// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'ole-background-gradient-blunatic',
		uppertextblock:[
			{
				textclass: "p2title",
				textdata: data.string.p2t16
			},
			{
				textclass: "p2text bluranim",
				textdata: data.string.p2t17
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "theimganim humang",
					imgid : 'manbre',
					imgsrc: ''
				},
				{
					imgclass: "theimganim parrotg",
					imgid : 'treebre',
					imgsrc: ''
				},
				{
					imgclass: "theimganim plantg",
					imgid : 'birdbre',
					imgsrc: ''
				}
			]
		}]
	},
	// slide8
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'ole-background-gradient-mustard',
		uppertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				textclass: "p2text2 bluranim",
				textdata: data.string.p2t18
			}
		],
		lbadditionalclass: "thenonliving",
		thetitle: data.string.nonliving,
		livinnonlivin:[
			{
				lbadditionalclass: "theimganimmin bikecont",
				thetitle: data.string.socks,
				imageblock:[{
					imagestoshow:[
						{
							imgclass: "bicycle2 a",
							imgid : 'socks',
							imgsrc: ""
						}
					]
				}]
			},
			{
				lbadditionalclass: "theimganim fancont",
				thetitle: data.string.brush,
				imageblock:[{
					imagestoshow:[
						{
							imgclass: "bicycle2 b",
							imgid : 'brush',
							imgsrc: ""
						}
					]
				}]
			},
			{
				lbadditionalclass: "theimganimplu carcont",
				thetitle: data.string.stonec,
				imageblock:[{
					imagestoshow:[
						{
							imgclass: "bicycle2 c",
							imgid : 'rock',
							imgsrc: ""
						}
					]
				}]
			}
		]
	},
	// slide9
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'ole-background-gradient-blunatic',
		uppertextblock:[
			{
				textclass: "p2title",
				textdata: data.string.p2t19
			},
			{
				textclass: "p2text bluranim",
				textdata: data.string.p2t20
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "theimganim humang",
					imgid : 'plantb',
					imgsrc: ''
				},
				{
					imgclass: "theimganim parrotg",
					imgid : 'parrotb',
					imgsrc: ''
				},
				{
					imgclass: "theimganim plantg",
					imgid : 'goatb',
					imgsrc: ''
				}
			]
		}]
	},
	// slide10
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'ole-background-gradient-mustard',
		uppertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				textclass: "p2text2 bluranim",
				textdata: data.string.p2t21
			}
		],
		lbadditionalclass: "thenonliving",
		thetitle: data.string.nonliving,
		livinnonlivin:[
			{
				lbadditionalclass: "theimganimmin bikecont",
				thetitle: data.string.ballon,
				imageblock:[{
					imagestoshow:[
						{
							imgclass: "bicycle a",
							imgid : 'ballonb',
							imgsrc: ""
						}
					]
				}]
			},
			{
				lbadditionalclass: "theimganim fancont",
				thetitle: data.string.book,
				imageblock:[{
					imagestoshow:[
						{
							imgclass: "bicycle b",
							imgid : 'bookb',
							imgsrc: ""
						}
					]
				}]
			},
			{
				lbadditionalclass: "theimganimplu carcont",
				thetitle: data.string.ball,
				imageblock:[{
					imagestoshow:[
						{
							imgclass: "bicycle c",
							imgid : 'ballb',
							imgsrc: ""
						}
					]
				}]
			}
		]
	},
	// slide11
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'ole-background-gradient-blunatic',
		uppertextblock:[
			{
				textclass: "p2title",
				textdata: data.string.p2t22
			},
			{
				textclass: "p2text bluranim",
				textdata: data.string.p2t23
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "theimganim humang2",
					imgid : 'fox',
					imgsrc: ''
				},
				{
					imgclass: "theimganim parrotg2",
					imgid : 'touchmn',
					imgsrc: ''
				},
				{
					imgclass: "theimganim plantg2",
					imgid : 'elehug',
					imgsrc: ''
				}
			]
		}]
	},
	// slide12
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'ole-background-gradient-mustard',
		uppertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				textclass: "p2text2 bluranim",
				textdata: data.string.p2t24
			}
		],
		lbadditionalclass: "thenonliving",
		thetitle: data.string.nonliving,
		livinnonlivin:[
			{
				lbadditionalclass: "theimganimmin bikecont",
				thetitle: data.string.chair,
				imageblock:[{
					imagestoshow:[
						{
							imgclass: "bicycle a",
							imgid : 'chair',
							imgsrc: ""
						}
					]
				}]
			},
			{
				lbadditionalclass: "theimganim fancont",
				thetitle: data.string.teddy,
				imageblock:[{
					imagestoshow:[
						{
							imgclass: "bicycle b",
							imgid : 'teddy',
							imgsrc: ""
						}
					]
				}]
			},
			{
				lbadditionalclass: "theimganimplu carcont",
				thetitle: data.string.boots,
				imageblock:[{
					imagestoshow:[
						{
							imgclass: "bicycle c",
							imgid : 'boot',
							imgsrc: ""
						}
					]
				}]
			}
		]
	},
	// slide13
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'ole-background-gradient-blunatic',
		uppertextblock:[
			{
				textclass: "p2title",
				textdata: data.string.p2t25
			},
			{
				textclass: "p2text bluranim",
				textdata: data.string.p2t26
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "theimganim humang2",
					imgid : 'eles',
					imgsrc: ''
				},
				{
					imgclass: "theimganim parrotg2",
					imgid : 'plant_excrete_01',
					imgsrc: ''
				},
				{
					imgclass: "theimganim plantg2",
					imgid : 'parrots',
					imgsrc: ''
				}
			]
		}]
	},
	// slide14
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'ole-background-gradient-mustard',
		uppertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				textclass: "p2text2 bluranim",
				textdata: data.string.p2t27
			}
		],
		lbadditionalclass: "thenonliving",
		thetitle: data.string.nonliving,
		livinnonlivin:[
			{
				lbadditionalclass: "theimganimmin bikecont",
				thetitle: data.string.hat,
				imageblock:[{
					imagestoshow:[
						{
							imgclass: "bicycle2 a",
							imgid : 'hat',
							imgsrc: ""
						}
					]
				}]
			},
			{
				lbadditionalclass: "theimganim fancont",
				thetitle: data.string.ring,
				imageblock:[{
					imagestoshow:[
						{
							imgclass: "bicycle2 b",
							imgid : 'ring',
							imgsrc: ""
						}
					]
				}]
			},
			{
				lbadditionalclass: "theimganimplu carcont",
				thetitle: data.string.computer,
				imageblock:[{
					imagestoshow:[
						{
							imgclass: "bicycle2 c",
							imgid : 'compu',
							imgsrc: ""
						}
					]
				}]
			}
		]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    loadTimelineProgress($total_page, countNext + 1);
    function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "centerone", src: imgpath+"boywalking.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "fish", src: imgpath+"fish.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "dog", src: imgpath+"dog.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "bird", src: imgpath+"bird.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "snake", src: imgpath+"snake.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "hen", src: imgpath+"hen.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "bike", src: imgpath+"bike.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fan", src: imgpath+"fan.png", type: createjs.AbstractLoader.IMAGE},
			{id: "car", src: imgpath+"car.png", type: createjs.AbstractLoader.IMAGE},
			{id: "humang", src: imgpath+"human_grow.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "monkeyg", src: imgpath+"monkey.png", type: createjs.AbstractLoader.IMAGE},
			{id: "parrotg", src: imgpath+"bird-grow.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "goatg", src: imgpath+"goat.png", type: createjs.AbstractLoader.IMAGE},
			{id: "plantg", src: imgpath+"plant02.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "table", src: imgpath+"table.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pencil", src: imgpath+"pencil.png", type: createjs.AbstractLoader.IMAGE},
			{id: "house", src: imgpath+"house.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cow", src: imgpath+"cow_eating_grass.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "goate", src: imgpath+"goateating.png", type: createjs.AbstractLoader.IMAGE},
			{id: "monkeye", src: imgpath+"girl-eating.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "parrote", src: imgpath+"parrot_eating.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "cowe", src: imgpath+"eating_grass.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "petrol", src: imgpath+"car_in_station.png", type: createjs.AbstractLoader.IMAGE},
			{id: "efan", src: imgpath+"tablefan.png", type: createjs.AbstractLoader.IMAGE},
			{id: "torch", src: imgpath+"torch_light.png", type: createjs.AbstractLoader.IMAGE},
			{id: "manbre", src: imgpath+"breathing-pattern.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "treebre", src: imgpath+"photosynthesis.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "birdbre", src: imgpath+"bird-breathing.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "socks", src: imgpath+"socks.png", type: createjs.AbstractLoader.IMAGE},
			{id: "brush", src: imgpath+"brush.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rock", src: imgpath+"stones.png", type: createjs.AbstractLoader.IMAGE},
			{id: "plantb", src: imgpath+"fruits.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "parrotb", src: imgpath+"parrot_kids.png", type: createjs.AbstractLoader.IMAGE},
			{id: "goatb", src: imgpath+"goat_with_kid.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ballonb", src: imgpath+"balloon.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bookb", src: imgpath+"books.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ballb", src: imgpath+"beach_ball.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fox", src: imgpath+"fox_run.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "elehug", src: imgpath+"elephant.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "touchmn", src: imgpath+"touchmenotplant.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "chair", src: imgpath+"chair.png", type: createjs.AbstractLoader.IMAGE},
			{id: "teddy", src: imgpath+"teddy-bear.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boot", src: imgpath+"boots.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eles", src: imgpath+"elephant_excreates.png", type: createjs.AbstractLoader.IMAGE},
			{id: "monkeys", src: imgpath+"monkey_excreates.png", type: createjs.AbstractLoader.IMAGE},
			{id: "plant_excrete_01", src: imgpath+"plant_excrete_01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "parrots", src: imgpath+"parrot_excreates.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hat", src: imgpath+"hat.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ring", src: imgpath+"ring.png", type: createjs.AbstractLoader.IMAGE},
			{id: "compu", src: imgpath+"computer.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tl-1", src: 'images/textbox/white/tl-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
            {id: "sound_1", src: soundAsset+"s2_p1.ogg"},
            {id: "sound_2", src: soundAsset+"s2_p2.ogg"},
            {id: "sound_3", src: soundAsset+"s2_p3.ogg"},
            {id: "sound_4", src: soundAsset+"s2_p4.ogg"},
            {id: "sound_5", src: soundAsset+"s2_p5.ogg"},
            {id: "sound_6", src: soundAsset+"s2_p6.ogg"},
            {id: "sound_7", src: soundAsset+"s2_p7.ogg"},
            {id: "sound_8", src: soundAsset+"s2_p8.ogg"},
            {id: "sound_9", src: soundAsset+"s2_p9.ogg"},
            {id: "sound_10", src: soundAsset+"s2_p10.ogg"},
            {id: "sound_11", src: soundAsset+"s2_p11.ogg"},
            {id: "sound_12", src: soundAsset+"s2_p12.ogg"},
            {id: "sound_13", src: soundAsset+"s2_p13.ogg"},
            {id: "sound_14", src: soundAsset+"s2_p14.ogg"},
            {id: "sound_15", src: soundAsset+"s2_p15.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	 function navigationcontroller(islastpageflag){
		 typeof islastpageflag === "undefined" ?
		 islastpageflag = false :
		 typeof islastpageflag != 'boolean'?
		 alert("NavigationController : Hi Master, please provide a boolean parameter") :
		 null;

			 if(countNext == 0 && $total_page!=1){
			 $nextBtn.show(0);
			 $prevBtn.css('display', 'none');
			 }

			 else if($total_page == 1){
			 $prevBtn.css('display', 'none');
			 $nextBtn.css('display', 'none');

			 // if lastpageflag is true
			 islastpageflag ?
			 ole.footerNotificationHandler.lessonEndSetNotification() :
			 ole.footerNotificationHandler.lessonEndSetNotification() ;
			 }

			 else if(countNext > 0 && countNext < $total_page-1){
			 $nextBtn.show(0);
			 $prevBtn.show(0);
			 }

			 else if(countNext == $total_page-1){
			 $nextBtn.css('display', 'none');
			 $prevBtn.show(0);

			 // if lastpageflag is true
			 islastpageflag ?
			 ole.footerNotificationHandler.lessonEndSetNotification() :
			 ole.footerNotificationHandler.pageEndSetNotification() ;
			 }
	 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);
        vocabcontroller.findwords(countNext);
        sound_player("sound_"+(countNext+1),true);
    }
    function sound_player(sound_id,navigate){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function(){
            navigate?navigationcontroller():"";
        });
    }
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_play_click(sound_id, click_class){
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){

		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent1 = content[count].livinnonlivin[0];
			if(lncontent1.hasOwnProperty('imageblock')){
				var imageblock = lncontent1.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent2 = content[count].livinnonlivin[1];
			if(lncontent2.hasOwnProperty('imageblock')){
				var imageblock = lncontent2.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}

			var lncontent3 = content[count].livinnonlivin[2];
			if(lncontent3.hasOwnProperty('imageblock')){
				var imageblock = lncontent3.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}

		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				// console.log(selector);
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');


		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

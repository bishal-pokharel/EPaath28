var imgpath = $ref + "/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var content=[
	{
		//starting page
		additionalclasscontentblock : 'backgroundss',
		contentblocknocenteradjust : true,
		uppertextblock : [
		{
			textclass : "firsttitle",
			textdata : data.string.title
		}
		],
		imageblock: [
		{
			imagetoshow: [
			{
			imgclass : "coverimg",
			imgsrc : imgpath + "images/cover.png"
			}
			],
		}
		]
	},

	//1st slide
	{
		uppertextblock:[
		{
			textclass : "headertext",
			textdata : data.string.p1text1,
		}
		],
		flipcontainer:[
		{
			sectioncontainer : "section seone",
			imgsrc : imgpath + "images/mango.png",
			animalname : data.string.p1text2,
		},
		{
			sectioncontainer : "section setwo",
			imgsrc : imgpath + "images/papaya.png",
			animalname : data.string.p1text3,
		},
		{
			sectioncontainer : "section sethree",
			imgsrc : imgpath + "images/banana.png",
			animalname : data.string.p1text4,
		},
		{
			sectioncontainer : "section sefour",
			imgsrc : imgpath + "images/meat.png",
			animalname : data.string.p1text5,
		},
		{
			sectioncontainer : "section sefive",
			imgsrc : imgpath + "images/mustard.png",
			animalname : data.string.p1text6,

		},
		{
			sectioncontainer : "section seone1",
			imgsrc : imgpath + "images/egg.png",
			animalname : data.string.p1text7,
		},
		{
			sectioncontainer : "section setwo1",
			imgsrc : imgpath + "images/fish.png",
			animalname : data.string.p1text8,
		},
		{
			sectioncontainer : "section sethree1",
			imgsrc : imgpath + "images/milk.png",
			animalname : data.string.p1text9,
		},
		{
			sectioncontainer : "section sefour1",
			imgsrc : imgpath + "images/spinach.png",
			animalname : data.string.p1text10,
		},
		{
			sectioncontainer : "section sefive1",
			imgsrc : imgpath + "images/coriander.png",
			animalname : data.string.p1text11,

		},
		{
			sectioncontainer : "section seone2",
			imgsrc : imgpath + "images/rice.png",
			animalname : data.string.p1text12,
		},
		{
			sectioncontainer : "section setwo2",
			imgsrc : imgpath + "images/wheat.png",
			animalname : data.string.p1text13,
		},
		{
			sectioncontainer : "section sethree2",
			imgsrc : imgpath + "images/maize.png",
			animalname : data.string.p1text14,
		},
		{
			sectioncontainer : "section sefour2",
			imgsrc : imgpath + "images/beans.png",
			animalname : data.string.p1text15,
		},
		{
			sectioncontainer : "section sefive2",
			imgsrc : imgpath + "images/peas.png",
			animalname : data.string.p1text16,

		}
		],
	},

		//2nd slide
	{
		uppertextblock:[
		{
			textclass : "headertext",
			textdata : data.string.p1text17,
		},
		{
			textclass : "text1",
			textdata : data.string.p1text18,
		},
		{
			textclass : "text2",
			textdata : data.string.p1text19,
		},
		{
			textclass : "text3",
			textdata : data.string.p1text20,
		},
		{
			textclass : "text4",
			textdata : data.string.p1text21,
		},
		{
			textclass : "text5",
			textdata : data.string.p1text22,
		}
		],
		flipcontaineradditionalclass: 'flips',
		flipcontainer:[
		{
			sectioncontainer : "section1 seone",
			imgsrc : imgpath + "images/mango.png",
			animalname : data.string.p1text2,
		},
		{
			sectioncontainer : "section1 setwo",
			imgsrc : imgpath + "images/papaya.png",
			animalname : data.string.p1text3,
		},
		{
			sectioncontainer : "section1 sethree",
			imgsrc : imgpath + "images/banana.png",
			animalname : data.string.p1text4,
		},
		{
			sectioncontainer : "section1 sefour",
			imgsrc : imgpath + "images/meat.png",
			animalname : data.string.p1text5,
		},
		{
			sectioncontainer : "section1 sefive",
			imgsrc : imgpath + "images/mustard.png",
			animalname : data.string.p1text6,

		},
		{
			sectioncontainer : "section1 seone1",
			imgsrc : imgpath + "images/egg.png",
			animalname : data.string.p1text7,
		},
		{
			sectioncontainer : "section1 setwo1",
			imgsrc : imgpath + "images/fish.png",
			animalname : data.string.p1text8,
		},
		{
			sectioncontainer : "section1 sethree1",
			imgsrc : imgpath + "images/milk.png",
			animalname : data.string.p1text9,
		},
		{
			sectioncontainer : "section1 sefour1",
			imgsrc : imgpath + "images/spinach.png",
			animalname : data.string.p1text10,
		},
		{
			sectioncontainer : "section1 sefive1",
			imgsrc : imgpath + "images/coriander.png",
			animalname : data.string.p1text11,

		},
		{
			sectioncontainer : "section1 seone2",
			imgsrc : imgpath + "images/rice.png",
			animalname : data.string.p1text12,
		},
		{
			sectioncontainer : "section1 setwo2",
			imgsrc : imgpath + "images/wheat.png",
			animalname : data.string.p1text13,
		},
		{
			sectioncontainer : "section1 sethree2",
			imgsrc : imgpath + "images/maize.png",
			animalname : data.string.p1text14,
		},
		{
			sectioncontainer : "section1 sefour2",
			imgsrc : imgpath + "images/beans.png",
			animalname : data.string.p1text15,
		},
		{
			sectioncontainer : "section1 sefive2",
			imgsrc : imgpath + "images/peas.png",
			animalname : data.string.p1text16,

		}
		],

	},

			//3rd slide
	{
		uppertextblock:[
		{
			textclass : "headertext",
			textdata : data.string.p1text17,
		},
		{
			textclass : "text1 firsttext1",
			textdata : data.string.p1text18,
		},
		{
			textclass : "text2",
			textdata : data.string.p1text19,
		},
		{
			textclass : "text3",
			textdata : data.string.p1text20,
		},
		{
			textclass : "text4",
			textdata : data.string.p1text21,
		},
		{
			textclass : "text5",
			textdata : data.string.p1text22,
		}
		],
		flipcontaineradditionalclass: 'flips',
		flipcontainer:[
		{
			sectioncontainer : "section1 seone mangoanimation",
			imgsrc : imgpath + "images/mango.png",
			animalname : data.string.p1text2,
		},
		{
			sectioncontainer : "section1 setwo papayaanimation",
			imgsrc : imgpath + "images/papaya.png",
			animalname : data.string.p1text3,
		},
		{
			sectioncontainer : "section1 sethree bananaanimation",
			imgsrc : imgpath + "images/banana.png",
			animalname : data.string.p1text4,
		},
		{
			sectioncontainer : "section1 sefour",
			imgsrc : imgpath + "images/meat.png",
			animalname : data.string.p1text5,
		},
		{
			sectioncontainer : "section1 sefive",
			imgsrc : imgpath + "images/mustard.png",
			animalname : data.string.p1text6,

		},
		{
			sectioncontainer : "section1 seone1 egganimation",
			imgsrc : imgpath + "images/egg.png",
			animalname : data.string.p1text7,
		},
		{
			sectioncontainer : "section1 setwo1 ",
			imgsrc : imgpath + "images/fish.png",
			animalname : data.string.p1text8,
		},
		{
			sectioncontainer : "section1 sethree1",
			imgsrc : imgpath + "images/milk.png",
			animalname : data.string.p1text9,
		},
		{
			sectioncontainer : "section1 sefour1",
			imgsrc : imgpath + "images/spinach.png",
			animalname : data.string.p1text10,
		},
		{
			sectioncontainer : "section1 sefive1",
			imgsrc : imgpath + "images/coriander.png",
			animalname : data.string.p1text11,

		},
		{
			sectioncontainer : "section1 seone2 riceanimation",
			imgsrc : imgpath + "images/rice.png",
			animalname : data.string.p1text12,
		},
		{
			sectioncontainer : "section1 setwo2",
			imgsrc : imgpath + "images/wheat.png",
			animalname : data.string.p1text13,
		},
		{
			sectioncontainer : "section1 sethree2",
			imgsrc : imgpath + "images/maize.png",
			animalname : data.string.p1text14,
		},
		{
			sectioncontainer : "section1 sefour2",
			imgsrc : imgpath + "images/beans.png",
			animalname : data.string.p1text15,
		},
		{
			sectioncontainer : "section1 sefive2",
			imgsrc : imgpath + "images/peas.png",
			animalname : data.string.p1text16,

		}
		],

	},

			//4th slide
	{
		uppertextblock:[
		{
			textclass : "headertext",
			textdata : data.string.p1text17,
		},
		{
			textclass : "text1a",
			textdata : data.string.p1text18,
		},
		{
			textclass : "text2 extendheight ",
			textdata : data.string.p1text19,
		},
		{
			textclass : "text3 ",
			textdata : data.string.p1text20,
		},
		{
			textclass : "text4",
			textdata : data.string.p1text21,
		},
		{
			textclass : "text5",
			textdata : data.string.p1text22,
		}
		],
		flipcontaineradditionalclass: 'flips',
		flipcontainer:[
		{
			sectioncontainer : "section1 seonea",
			imgsrc : imgpath + "images/mango.png",
			animalname : data.string.p1text2,
		},
		{
			sectioncontainer : "section1 setwoa",
			imgsrc : imgpath + "images/papaya.png",
			animalname : data.string.p1text3,
		},
		{
			sectioncontainer : "section1 sethreea",
			imgsrc : imgpath + "images/banana.png",
			animalname : data.string.p1text4,
		},
		{
			sectioncontainer : "section1 sefour",
			imgsrc : imgpath + "images/meat.png",
			animalname : data.string.p1text5,
		},
		{
			sectioncontainer : "section1 sefive mustardanimation",
			imgsrc : imgpath + "images/mustard.png",
			animalname : data.string.p1text6,

		},
		{
			sectioncontainer : "section1 egg egganimation1",
			imgsrc : imgpath + "images/egg.png",
			animalname : data.string.p1text7,
		},
		{
			sectioncontainer : "section1 setwo1 fishanimation",
			imgsrc : imgpath + "images/fish.png",
			animalname : data.string.p1text8,
		},
		{
			sectioncontainer : "section1 sethree1",
			imgsrc : imgpath + "images/milk.png",
			animalname : data.string.p1text9,
		},
		{
			sectioncontainer : "section1 sefour1 spinachanimation",
			imgsrc : imgpath + "images/spinach.png",
			animalname : data.string.p1text10,
		},
		{
			sectioncontainer : "section1 sefive1 corianderanimation",
			imgsrc : imgpath + "images/coriander.png",
			animalname : data.string.p1text11,

		},
		{
			sectioncontainer : "section1 rice",
			imgsrc : imgpath + "images/rice.png",
			animalname : data.string.p1text12,
		},
		{
			sectioncontainer : "section1 setwo2 wheatanimation",
			imgsrc : imgpath + "images/wheat.png",
			animalname : data.string.p1text13,
		},
		{
			sectioncontainer : "section1 sethree2",
			imgsrc : imgpath + "images/maize.png",
			animalname : data.string.p1text14,
		},
		{
			sectioncontainer : "section1 sefour2",
			imgsrc : imgpath + "images/beans.png",
			animalname : data.string.p1text15,
		},
		{
			sectioncontainer : "section1 sefive2 ",
			imgsrc : imgpath + "images/peas.png",
			animalname : data.string.p1text16,

		}
		],

	},

			//5th slide
	{
		uppertextblock:[
		{
			textclass : "headertext",
			textdata : data.string.p1text17,
		},
		{
			textclass : "text1a",
			textdata : data.string.p1text18,
		},
		{
			textclass : "text2a  ",
			textdata : data.string.p1text19,
		},
		{
			textclass : "text3 extendheight",
			textdata : data.string.p1text20,
		},
		{
			textclass : "text4",
			textdata : data.string.p1text21,
		},
		{
			textclass : "text5",
			textdata : data.string.p1text22,
		}
		],
		flipcontaineradditionalclass: 'flips',
		flipcontainer:[
		{
			sectioncontainer : "section1 seonea",
			imgsrc : imgpath + "images/mango.png",
			animalname : data.string.p1text2,
		},
		{
			sectioncontainer : "section1 setwoa",
			imgsrc : imgpath + "images/papaya.png",
			animalname : data.string.p1text3,
		},
		{
			sectioncontainer : "section1 sethreea",
			imgsrc : imgpath + "images/banana.png",
			animalname : data.string.p1text4,
		},
		{
			sectioncontainer : "section1 sefour meatanimation",
			imgsrc : imgpath + "images/meat.png",
			animalname : data.string.p1text5,
		},
		{
			sectioncontainer : "section1 mustard",
			imgsrc : imgpath + "images/mustard.png",
			animalname : data.string.p1text6,

		},
		{
			sectioncontainer : "section1 egg1 egganimation2",
			imgsrc : imgpath + "images/egg.png",
			animalname : data.string.p1text7,
		},
		{
			sectioncontainer : "section1 fish fishanimation1",
			imgsrc : imgpath + "images/fish.png",
			animalname : data.string.p1text8,
		},
		{
			sectioncontainer : "section1 sethree1 milkanimation1",
			imgsrc : imgpath + "images/milk.png",
			animalname : data.string.p1text9,
		},
		{
			sectioncontainer : "section1 spinach",
			imgsrc : imgpath + "images/spinach.png",
			animalname : data.string.p1text10,
		},
		{
			sectioncontainer : "section1 coriander",
			imgsrc : imgpath + "images/coriander.png",
			animalname : data.string.p1text11,

		},
		{
			sectioncontainer : "section1 rice riceanimation2",
			imgsrc : imgpath + "images/rice.png",
			animalname : data.string.p1text12,
		},
		{
			sectioncontainer : "section1 wheat",
			imgsrc : imgpath + "images/wheat.png",
			animalname : data.string.p1text13,
		},
		{
			sectioncontainer : "section1 sethree2 maizeanimation",
			imgsrc : imgpath + "images/maize.png",
			animalname : data.string.p1text14,
		},
		{
			sectioncontainer : "section1 sefour2",
			imgsrc : imgpath + "images/beans.png",
			animalname : data.string.p1text15,
		},
		{
			sectioncontainer : "section1 sefive2 ",
			imgsrc : imgpath + "images/peas.png",
			animalname : data.string.p1text16,

		}
		],

	},

				//5th slide
	{
		uppertextblock:[
		{
			textclass : "headertext",
			textdata : data.string.p1text17,
		},
		{
			textclass : "text1a",
			textdata : data.string.p1text18,
		},
		{
			textclass : "text2a  ",
			textdata : data.string.p1text19,
		},
		{
			textclass : "text3a ",
			textdata : data.string.p1text20,
		},
		{
			textclass : "text4 extendheight",
			textdata : data.string.p1text21,
		},
		{
			textclass : "text5 extendheight",
			textdata : data.string.p1text22,
		}
		],
		flipcontaineradditionalclass: 'flips',
		flipcontainer:[
		{
			sectioncontainer : "section1 seonea",
			imgsrc : imgpath + "images/mango.png",
			animalname : data.string.p1text2,
		},
		{
			sectioncontainer : "section1 setwoa",
			imgsrc : imgpath + "images/papaya.png",
			animalname : data.string.p1text3,
		},
		{
			sectioncontainer : "section1 sethreea",
			imgsrc : imgpath + "images/banana.png",
			animalname : data.string.p1text4,
		},
		{
			sectioncontainer : "section1 meat ",
			imgsrc : imgpath + "images/meat.png",
			animalname : data.string.p1text5,
		},
		{
			sectioncontainer : "section1 mustard",
			imgsrc : imgpath + "images/mustard.png",
			animalname : data.string.p1text6,

		},
		{
			sectioncontainer : "section1 egg2",
			imgsrc : imgpath + "images/egg.png",
			animalname : data.string.p1text7,
		},
		{
			sectioncontainer : "section1 fish1",
			imgsrc : imgpath + "images/fish.png",
			animalname : data.string.p1text8,
		},
		{
			sectioncontainer : "section1 milk",
			imgsrc : imgpath + "images/milk.png",
			animalname : data.string.p1text9,
		},
		{
			sectioncontainer : "section1 spinach",
			imgsrc : imgpath + "images/spinach.png",
			animalname : data.string.p1text10,
		},
		{
			sectioncontainer : "section1 coriander",
			imgsrc : imgpath + "images/coriander.png",
			animalname : data.string.p1text11,

		},
		{
			sectioncontainer : "section1 rice1 riceanimation3",
			imgsrc : imgpath + "images/rice.png",
			animalname : data.string.p1text12,
		},
		{
			sectioncontainer : "section1 wheat wheatanimation1",
			imgsrc : imgpath + "images/wheat.png",
			animalname : data.string.p1text13,
		},
		{
			sectioncontainer : "section1 maize maizeanimation1",
			imgsrc : imgpath + "images/maize.png",
			animalname : data.string.p1text14,
		},
		{
			sectioncontainer : "section1 sefour2 beananimation",
			imgsrc : imgpath + "images/beans.png",
			animalname : data.string.p1text15,
		},
		{
			sectioncontainer : "section1 sefive2 peaanimation",
			imgsrc : imgpath + "images/peas.png",
			animalname : data.string.p1text16,

		}
		],

	},
	];

	$(function(){
		var $board = $(".board");
		var $nextBtn = $("#activity-page-next-btn-enabled");
		var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
		var countNext = 0;
		var total_page = 0;

		var $total_page = content.length;

		var vocabcontroller =  new Vocabulary();
		vocabcontroller.init();


		loadTimelineProgress($total_page, countNext + 1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// sounds
			{id: "sound_0", src: soundAsset+"s1_p1.ogg"},
			{id: "sound_1", src: soundAsset+"s1_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s1_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s1_p4.ogg"},
			{id: "sound_4", src: soundAsset+"s1_p5.ogg"},
			{id: "sound_5", src: soundAsset+"s1_p6.ogg"},
			{id: "sound_6", src: soundAsset+"s1_p7.ogg"},
			{id: "sound_7", src: soundAsset+"s1_p8.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


	//controls the navigational state of the program

	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
      */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
      */
      function navigationcontroller(islastpageflag) {
      	typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		var clickcount = 0;

		$board.html(html);

		vocabcontroller.findwords(countNext);

		$(".card").one("click",function(){
			$(this).toggleClass("flipped");
			clickcount++;
			if(clickcount == 15)
			nav_button_controls(1000);
		});


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		switch (countNext) {
			case 1:
			sound_player1("sound_"+countNext);
			break;
			default:
			sound_player("sound_"+countNext);
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(0);
		});
	}
	function sound_player1(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	// Handlebars.registerHelper('listItem', function (from, to, context, options){
	    // var item = "";
	    // for (var i = from, j = to; i <= j; i++) {
	        // item = item + options.fn(context[i]);
	    // }
	    // return item;
	// });

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		loadTimelineProgress($total_page, countNext + 1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
	total_page = content.length;
	templateCaller();

});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

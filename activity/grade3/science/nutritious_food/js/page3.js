var imgpath = $ref + "/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var testanimal = "";
var content=[
//firstslide
    {
        additionalclasscontentblock: 'ole-backrgound-gradient-pool',
        uppertextblock:[
            {
                textclass : "toptext2",
                textdata : data.string.p2_s11,
            },
        ]
    },
//secondslide
    {
        additionalclasscontentblock: 'ole-backrgound-gradient-pool',
        uppertextblock:[
            {
                textclass : "toptext",
                textdata : data.string.p2_s11,
            },
            {
                textclass : "secondtext3 fadein",
                textdata : data.string.p2_s12,
            },
        ]
    },


//thirdslide
    {
        additionalclasscontentblock: 'ole-backrgound-gradient-pool',
        uppertextblock:[
            {
                textclass : "toptext",
                textdata : data.string.p2_s11,
            },
            {
                textclass : "secondtext opacity1",
                textdata : data.string.p2_s12,
            },
            {
                textclass : "thirdtext fadein",
                textdata : data.string.p2_s13,
            },
        ]
    },
//fourthslide
    {
        additionalclasscontentblock: 'motherbg',
        uppertextblock:[
            {
                textclass : "toptext white",
                textdata : data.string.p2_s14,
            }
        ]
    },

//fifthslide
    {
        additionalclasscontentblock: 'motherbg',
        uppertextblock:[
            {
                textclass : "momtext fadein",
                textdata : data.string.p2_s15,
            },
            {
                textclass : "sontext delay1fadein",
                textdata : data.string.p2_s16,
            }
        ],
        imageblock:[{
            imagetoshow:[{
                imgclass:'mombox fadein',
                imgsrc:imgpath + 'images/bubble boy-17.png'
            },
                {
                    imgclass:'sonbox delay1fadein',
                    imgsrc:imgpath + 'images/bubble boy-17.png'
                }]
        }]
    },

//sixthslide
    {
        additionalclasscontentblock: 'jugbg',
        uppertextblock:[
            {
                textclass : "momtext1 fadein",
                textdata : data.string.p2_s17,
            }
        ],
        imageblock:[{
            imagetoshow:[
                {
                    imgclass:'textbox1 fadein',
                    imgsrc:imgpath + 'images/text_box.png'
                },
                {
                imgclass:'momface fadein',
                imgsrc:imgpath + 'images/mumface.png'
                }
            ]
        }]
    },

//seventhslide
    {
        additionalclasscontentblock: 'momsonconvobg',
        uppertextblock:[
            {
                datahighlightflag:true,
                datahighlightcustomclass:'bold hide2',
                textclass : "toptexta",
                textdata : data.string.p2_s18,
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:'bold',
                textclass : "second2 nxthide",
                textdata : data.string.p2_s19,
            }
        ],
        imageblock:[{
            imagetoshow:[
                {
                    imgclass:'text1 hide2',
                    imgsrc:imgpath + 'images/text_box.png'
                },
                {
                    imgclass:'text2 nxthide',
                    imgsrc:imgpath + 'images/text_box.png'
                },
                {
                    imgclass:'momface1 nxthide',
                    imgsrc:imgpath + 'images/mumface.png'
                },
                {
                    imgclass:'sonface hide2',
                    imgsrc:imgpath + 'images/sonface.png'
                }
            ]
        }]
    },

//eightslide
    {
        additionalclasscontentblock: 'momsonconvobg1',
        uppertextblock:[
            {
                datahighlightflag:true,
                datahighlightcustomclass:'bold fadein',
                textclass : "p7t1 hide2",
                textdata : data.string.p2_s20,
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:'bold',
                textclass : "p7t2 nxthide",
                textdata : data.string.p2_s21,
            }
        ],
        imageblock:[{
            imagetoshow:[
                {
                    imgclass:'text1 hide2',
                    imgsrc:imgpath + 'images/text_box.png'
                },
                {
                    imgclass:'text2 nxthide',
                    imgsrc:imgpath + 'images/text_box.png'
                },
                {
                    imgclass:'momface1 nxthide',
                    imgsrc:imgpath + 'images/mumface.png'
                },
                {
                    imgclass:'sonface hide2',
                    imgsrc:imgpath + 'images/sonface.png'
                }
            ]
        }]
    },

//ninthslide
    {
        additionalclasscontentblock: 'momsonconvobg2',
        uppertextblock:[
            {
                datahighlightflag:true,
                datahighlightcustomclass:'bold fadein',
                textclass : "p8t1 hide2",
                textdata : data.string.p2_s22,
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:'bold',
                textclass : "p8t2 nxthide",
                textdata : data.string.p2_s23,
            }
        ],
        imageblock:[{
            imagetoshow:[
                {
                    imgclass:'p8img1 hide2',
                    imgsrc:imgpath + 'images/text_box.png'
                },
                {
                    imgclass:'p8img2 nxthide',
                    imgsrc:imgpath + 'images/text_box.png'
                },
                {
                    imgclass:'momface2 nxthide',
                    imgsrc:imgpath + 'images/mumface.png'
                },
                {
                    imgclass:'sonface1 hide2',
                    imgsrc:imgpath + 'images/sonface.png'
                }
            ]
        }]
    },

//tenthslide
    {
        additionalclasscontentblock: 'momsonconvobg3',
        uppertextblock:[
            {
                datahighlightflag:true,
                datahighlightcustomclass:'bold',
                textclass : "p9t1 show1",
                textdata : data.string.p2_s24,
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:'bold',
                textclass : "p9t2 show2",
                textdata : data.string.p2_s24_1,
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:'bold',
                textclass : "p9t3 show3",
                textdata : data.string.p2_s25,
            }
        ],
        imageblock:[{
            imagetoshow:[
                {
                    imgclass:'p9img1 show1',
                    imgsrc:imgpath + 'images/text_box.png'
                },
                {
                    imgclass:'p9img2 show2',
                    imgsrc:imgpath + 'images/text_box.png'
                },
                {
                    imgclass:'p9img3 show3',
                    imgsrc:imgpath + 'images/text_box.png'
                },
                {
                    imgclass:'momface3 show2',
                    imgsrc:imgpath + 'images/mumface.png'
                },
                {
                    imgclass:'sonface2 show1',
                    imgsrc:imgpath + 'images/sonface.png'
                },
                {
                    imgclass:'sonface3 show3',
                    imgsrc:imgpath + 'images/sonface.png'
                }
            ]
        }]
    },

//eleventhslide
    {
        additionalclasscontentblock: 'momsonconvobg4',
        uppertextblock:[
            {
                datahighlightflag:true,
                datahighlightcustomclass:'bold fadein',
                textclass : "p9t1 show1",
                textdata : data.string.p2_s26,
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:'bold',
                textclass : "p9t2 show2",
                textdata : data.string.p2_s27,
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:'bold',
                textclass : "p9t3 show3",
                textdata : data.string.p2_s28,
            }
        ],
        imageblock:[{
            imagetoshow:[
                {
                    imgclass:'p10img1 show1',
                    imgsrc:imgpath + 'images/text_box.png'
                },
                {
                    imgclass:'p10img2 show2',
                    imgsrc:imgpath + 'images/text_box.png'
                },
                {
                    imgclass:'p10img3 show3',
                    imgsrc:imgpath + 'images/text_box.png'
                },
                {
                    imgclass:'momface4 show1',
                    imgsrc:imgpath + 'images/mumface.png'
                },
                {
                    imgclass:'momface5 show3',
                    imgsrc:imgpath + 'images/mumface.png'
                },
                {
                    imgclass:'sonface4 show2',
                    imgsrc:imgpath + 'images/sonface.png'
                },
            ]
        }]
    },
];

$(function(){

    var $board = $(".board");
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var total_page = 0;
    var animalimage;
    var $total_page = content.length;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    loadTimelineProgress($total_page, countNext + 1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// sounds
      {id: "sound_0", src: soundAsset+"s3_p1.ogg"},
			{id: "sound_1", src: soundAsset+"s3_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s3_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s3_p4.ogg"},
			{id: "sound_4a", src: soundAsset+"s3_p5_1.ogg"},
			{id: "sound_4b", src: soundAsset+"s3_p5_2.ogg"},
			{id: "sound_5", src: soundAsset+"s3_p6.ogg"},
			{id: "sound_6a", src: soundAsset+"s3_p7_1.ogg"},
      {id: "sound_6b", src: soundAsset+"s3_p7_2.ogg"},
      {id: "sound_7a", src: soundAsset+"s3_p8_1.ogg"},
      {id: "sound_7b", src: soundAsset+"s3_p8_2.ogg"},
      {id: "sound_8a", src: soundAsset+"s3_p9_1.ogg"},
      {id: "sound_8b", src: soundAsset+"s3_p9_2.ogg"},
      {id: "sound_9a", src: soundAsset+"s3_p10_1.ogg"},
      {id: "sound_9b", src: soundAsset+"s3_p10_2.ogg"},
      {id: "sound_9c", src: soundAsset+"s3_p10_3.ogg"},
      {id: "sound_10a", src: soundAsset+"s3_p11_1.ogg"},
      {id: "sound_10b", src: soundAsset+"s3_p11_2.ogg"},
      {id: "sound_10c", src: soundAsset+"s3_p11_3.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

    /*
        inorder to use the handlebar partials we need to register them
        to their respective handlebar partial pointer first
        */
    Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


    //controls the navigational state of the program
    /*======================================================
 =            Navigation Controller Function            =
 ======================================================*/
    /**
     How To:
     - Just call the navigation controller if it is to be called from except the
     last page of lesson
     - If called from last page set the islastpageflag to true such that
     footernotification is called for continue button to navigate to exercise
     */

    /**
     What it does:
     - If not explicitly overriden the method for navigation button
     controls, it shows the navigation buttons as required,
     according to the total count of pages and the countNext variable
     - If for a general use it can be called from the templateCaller
     function
     - Can be put anywhere in the template function as per the need, if
     so should be taken out from the templateCaller function
     - If the total page number is
     */
    function navigationcontroller(islastpageflag){
        // check if the parameter is defined and if a boolean,
        // update islastpageflag accordingly
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean'?
                alert("NavigationController : Hi Master, please provide a boolean parameter") :
                null;
    }

    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    function generalTemplate(){

        var html = template(content[countNext]);

        $board.html(html);

        // highlight any text inside board div with datahighlightflag set true
        vocabcontroller.findwords(countNext);
        texthighlight($board);

        switch (countNext) {
            case 0:
                $('.toptext2').animate(
                    {
                        top:"18%"
                    },500);
                sound_player("sound_0");
                break;
            case 1:
                $('.secondtext3').animate(
                    {
                        bottom:"63%"
                    },500);
                sound_player("sound_1");
                break;
            case 2:
                $('.thirdtext').animate(
                    {
                        bottom:"43%"
                    },500);
                sound_player("sound_2");
                break;
            case 4:
            createjs.Sound.stop();
            current_sound = createjs.Sound.play("sound_4a");
            current_sound.play();
            current_sound.on('complete', function(){
            createjs.Sound.stop();
            current_sound = createjs.Sound.play("sound_4b");
            current_sound.play();
            current_sound.on('complete', function(){
              nav_button_controls(200);
            });
          });
          break;
            case 6:
            $(".nxthide").hide(0);
            createjs.Sound.stop();
            current_sound = createjs.Sound.play("sound_6a");
            current_sound.play();
            current_sound.on('complete', function(){
            createjs.Sound.stop();
            $(".nxthide").show(0);
            current_sound = createjs.Sound.play("sound_6b");
            current_sound.play();
            current_sound.on('complete', function(){
              nav_button_controls(200);
            });
          });
            break;
            case 7:
            $(".nxthide").hide(0);
            createjs.Sound.stop();
            current_sound = createjs.Sound.play("sound_7a");
            current_sound.play();
            current_sound.on('complete', function(){
            createjs.Sound.stop();
            $(".nxthide").show(0);
            current_sound = createjs.Sound.play("sound_7b");
            current_sound.play();
            current_sound.on('complete', function(){
              nav_button_controls(200);
            });
          });
          break;
            case 8:
                $(".toptexta").css("font-size","3.3vh");
                $(".nxthide").hide(0);
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("sound_8a");
                current_sound.play();
                current_sound.on('complete', function(){
                createjs.Sound.stop();
                $(".nxthide").show(0);
                current_sound = createjs.Sound.play("sound_8b");
                current_sound.play();
                current_sound.on('complete', function(){
                  nav_button_controls(200);
                });
              });
              break;
            case 9:
            $(".show2,.show3").hide();
            createjs.Sound.stop();
            current_sound = createjs.Sound.play("sound_9a");
            current_sound.play();
            current_sound.on('complete', function(){
            createjs.Sound.stop();
            $(".show2").show(0);
            current_sound = createjs.Sound.play("sound_9b");
            current_sound.play();
            current_sound.on('complete', function(){
            createjs.Sound.stop();
            $(".show3").show(0);
            current_sound = createjs.Sound.play("sound_9c");
            current_sound.play();
            current_sound.on('complete', function(){
              nav_button_controls(200);
            });
          });
        });
            break;
            case 10:
                $(".show2,.show3").hide();
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("sound_10a");
                current_sound.play();
                current_sound.on('complete', function(){
                createjs.Sound.stop();
                $(".show2").show(0);
                current_sound = createjs.Sound.play("sound_10b");
                current_sound.play();
                current_sound.on('complete', function(){
                createjs.Sound.stop();
                $(".show3").show(0);
                current_sound = createjs.Sound.play("sound_10c");
                current_sound.play();
                current_sound.on('complete', function(){
                  nav_button_controls(200);
                });
              });
            });
                break;
            default:
            sound_player("sound_"+countNext);
            break;

        }
    }
  function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(0);
		});
	}

    function templateCaller(){
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);
        loadTimelineProgress($total_page, countNext + 1);

        navigationcontroller();

        generalTemplate();
        /*
        for (var i = 0; i < content.length; i++) {
            slides(i);
            $($('.totalsequence')[i]).html(i);
            $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
                "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
        }
        function slides(i){
            $($('.totalsequence')[i]).click(function(){
                countNext = i;
                templateCaller();
                templateCaller();
            });
        }
        */


    }
    $nextBtn.on("click", function(){
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on("click", function(){
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
        previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });
    total_page = content.length;
    templateCaller();
});



/*===============================================
    =            data highlight function            =
    ===============================================*/
function texthighlight($highlightinside){
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag   = "</span>";


    if($alltextpara.length > 0){
        $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                (stylerulename = $(this).attr("data-highlightcustomclass")) :
                (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            $(this).html(replaceinstring);
        });
    }
}
/*=====  End of data highlight function  ======*/

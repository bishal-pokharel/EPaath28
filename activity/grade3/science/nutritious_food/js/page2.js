var imgpath = $ref + "/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var content=[
	//1st slide
	{
		uppertextblock:[
		{
			textclass : "toptext",
			textdata : data.string.p2_s1,
		}
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "foods1",
				imgsrc : imgpath + "images/fruits.png"
			},
			{
				imgclass: "foods2",
				imgsrc : imgpath + "images/animalsproducts.png"
			},
			{
				imgclass: "foods3",
				imgsrc : imgpath + "images/meatproducts.png"
			},
			{
				imgclass: "foods4",
				imgsrc : imgpath + "images/foodgrans.png"
			}
			],
		}
		]
	},

	//2nd slide
	{
		uppertextblock:[
		{
			textclass : "toptext",
			textdata : data.string.p2_s1,
		},
		{
			textclass : "midtext",
			textdata : data.string.p2_s2,
		}
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "foods1",
				imgsrc : imgpath + "images/fruits.png"
			},
			{
				imgclass: "foods2",
				imgsrc : imgpath + "images/animalsproducts.png"
			},
			{
				imgclass: "foods3",
				imgsrc : imgpath + "images/meatproducts.png"
			},
			{
				imgclass: "foods4",
				imgsrc : imgpath + "images/foodgrans.png"
			}

			],
		}
		]
	},

	//3rd slide
	{
		uppertextblock:[
		{
			textclass : "toptext fadeout",
			textdata : data.string.p2_s1,
		},
		{
			textclass : "midtext fadeout",
			textdata : data.string.p2_s2,
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass:"clickhighlight",
			textclass : "fruittext delay1fadein",
			textdata : data.string.p2_s3,
		},
		{
			textclass : "caption",
			textdata : data.string.p2_s4,
		},
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "foods1 centerfood pointer",
				imgsrc : imgpath + "images/fruits.png"
			},
			{
				imgclass: "foods2 fadeout ",
				imgsrc : imgpath + "images/animalsproducts.png"
			},
			{
				imgclass: "foods3 fadeout ",
				imgsrc : imgpath + "images/meatproducts.png"
			},
			{
				imgclass: "foods4 fadeout pointer",
				imgsrc : imgpath + "images/foodgrans.png"
			}

			],
		}
		],
        slideshowblock: [{
            slides: [
                imgpath + "images/slideshow0.jpg",
                imgpath + "images/slideshow1.jpg",
                imgpath + "images/slideshow2.jpg",
            ]
        }]
	},
	//4th slide
	{
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass:"clickhighlight",
			textclass : "fruittext delay1fadein",
			textdata : data.string.p2_s5,
		},
		{
			textclass : "caption",
			textdata : data.string.p2_s6,
		},
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "foods1 fadeout pointer",
				imgsrc : imgpath + "images/fruits.png"
			},
			{
				imgclass: "foods2 fadeout pointer",
				imgsrc : imgpath + "images/animalsproducts.png"
			},
			{
				imgclass: "foods3 fadeout pointer",
				imgsrc : imgpath + "images/meatproducts.png"
			},
			{
				imgclass: "foods4 centerfood pointer",
				imgsrc : imgpath + "images/foodgrans.png"
			}
			],
		}
		],
        slideshowblock: [{
            slides: [
                imgpath + "images/grains/slideshow0.jpg",
                imgpath + "images/grains/slideshow1.jpg",
                imgpath + "images/grains/slideshow2.jpg",
            ]
        }]
	},

	//5th slide
	{
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass:"clickhighlight",
			textclass : "fruittext delay1fadein",
			textdata : data.string.p2_s7,
		},
		{
			textclass : "caption",
			textdata : data.string.p2_s8,
		},
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "foods1 fadeout pointer",
				imgsrc : imgpath + "images/fruits.png"
			},
			{
				imgclass: "foods2 centerfood pointer",
				imgsrc : imgpath + "images/animalsproducts.png"
			},
			{
				imgclass: "foods3 fadeout pointer",
				imgsrc : imgpath + "images/meatproducts.png"
			},
			{
				imgclass: "foods4 fadeout pointer",
				imgsrc : imgpath + "images/foodgrans.png"
			}
			],
		}
		],
        slideshowblock: [{
            slides: [
                imgpath + "images/dairy/slideshow0.jpg",
                imgpath + "images/dairy/slideshow1.jpg",
                imgpath + "images/dairy/slideshow2.jpg",
            ]
        }]
	},

	//6th slide
	{
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass:"clickhighlight",
			textclass : "fruittext delay1fadein",
			textdata : data.string.p2_s9,
		},
		{
			textclass : "caption",
			textdata : data.string.p2_s10,
		},
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "foods1 fadeout pointer",
				imgsrc : imgpath + "images/fruits.png"
			},
			{
				imgclass: "foods2 fadeout pointer",
				imgsrc : imgpath + "images/animalsproducts.png"
			},
			{
				imgclass: "foods3 pointer centerfood",
				imgsrc : imgpath + "images/vegetablesmix.png"
			},
			{
				imgclass: "foods4 fadeout pointer",
				imgsrc : imgpath + "images/foodgrans.png"
			}
			],
		}
		],
        slideshowblock: [{
            slides: [
                imgpath + "images/veg/slideshow0.jpg",
                imgpath + "images/veg/slideshow1.jpg",
                imgpath + "images/veg/slideshow2.jpg",
            ]
        }]
	},

	];

	$(function(){
		var $board = $(".board");
		var $nextBtn = $("#activity-page-next-btn-enabled");
		var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
		var countNext = 0;
		var total_page = 0;
		var $total_page = content.length;
		var vocabcontroller =  new Vocabulary();
		vocabcontroller.init();
		var total_page = 0;
		loadTimelineProgress($total_page, countNext + 1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// sounds
			{id: "sound_0", src: soundAsset+"s2_p1.ogg"},
			{id: "sound_1", src: soundAsset+"s2_p2.ogg"},
			{id: "sound_2a", src: soundAsset+"s2_p3_1.ogg"},
			{id: "sound_2b", src: soundAsset+"s2_p3_2.ogg"},
			{id: "sound_3a", src: soundAsset+"s2_p4_1.ogg"},
			{id: "sound_3b", src: soundAsset+"s2_p4_2.ogg"},
			{id: "sound_4a", src: soundAsset+"s2_p5_1.ogg"},
			{id: "sound_4b", src: soundAsset+"s2_p5_2.ogg"},
			{id: "sound_5a", src: soundAsset+"s2_p6_1.ogg"},
			{id: "sound_5b", src: soundAsset+"s2_p6_2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("slideshowcontent", $("#slideshowcontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
      */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
      */
      function navigationcontroller(islastpageflag) {
      	typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		var anim_count = 0;

		// highlight any text inside board div with datahighlightflag set true
		vocabcontroller.findwords(countNext);
		texthighlight($board);
		var fin;
		switch (countNext) {
			case 2:
			sound_player1("sound_2a");

			$(".foods1").click(function(){
				console.log('hello');
				$(this).removeClass("centerfood").addClass("fruit1").animate({
				width: "20%",
    			top: "75%",
    			left:"40%"},1500);

    			$(".fruittext").removeClass("delay1fadein").addClass("opacity1").animate({
					opacity:0 },1000);


				$('.slideshowblock').addClass("delay1fadein");

				$('.caption').addClass('delay1fadein');
				setTimeout(function(){
						sound_player("sound_2b");
				},1000);
				});
			break;
			case 3:
			sound_player1("sound_3a");

			$(".foods4").click(function(){
				console.log('hello');
				$(this).removeClass("centerfood").addClass("fruit1").animate({
				width: "20%",
    			top: "75%",
    			left:"40%"},1500);

    			$(".fruittext").removeClass("delay1fadein").addClass("opacity1").animate({
					opacity:0 },1000);


				$('.slideshowblock').addClass("delay1fadein");

				$('.caption').addClass('delay1fadein');
				setTimeout(function(){
						sound_player("sound_3b");
				},1000);
				});
			break;

			case 4:
			sound_player1("sound_4a");

			$(".foods2").click(function(){
				console.log('hello');
				$(this).removeClass("centerfood").addClass("fruit1").animate({
				width: "20%",
    			top: "75%",
    			left:"40%"},1500);

    			$(".fruittext").removeClass("delay1fadein").addClass("opacity1").animate({
					opacity:0 },1000);


				$('.slideshowblock').addClass("delay1fadein");

				$('.caption').addClass('delay1fadein');
				setTimeout(function(){
						sound_player("sound_4b");
				},1000);
				});
			break;

			case 5:
			sound_player1("sound_5a");

			$(".foods3").click(function(){
				console.log('hello');
				$(this).removeClass("centerfood").addClass("fruit1").animate({
				width: "20%",
    			top: "75%",
    			left:"40%"},1500);

    			$(".fruittext").removeClass("delay1fadein").addClass("opacity1").animate({
					opacity:0 },1000);


				$('.slideshowblock').addClass("delay1fadein");

				$('.caption').addClass('delay1fadein');
				setTimeout(function(){
						sound_player("sound_5b");
				},1000);
				});
			break;
			default:
			sound_player("sound_"+countNext);
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(0);
		});
	}
	function sound_player1(sound_id, next){
		setTimeout(function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
		},1200);
	}
	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	function activitycomplete(fin){
		$('.animal, .wilddomback').css("opacity","0");
		setTimeout( function(){
			$('.animal, .wilddomback').css("opacity","1");
		}  , 100 );
		if(fin == 6 && countNext == 0){
			$(".alertbox").show(0);
			$nextBtn.show(0);
		}
		else if(fin == 6 && countNext == 1){
			$(".alertbox").show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	}


	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
	total_page = content.length;
	templateCaller();
});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

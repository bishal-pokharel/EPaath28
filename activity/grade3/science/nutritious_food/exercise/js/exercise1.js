var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	//slide 0
	{
		contentblockadditionalclass:'bluebgg',
		extratextblock : [
		{
			textdata : data.string.ext1,
			textclass : 'instruction my_font_big',
		},
		{
			textdata : data.string.ext4,
			textclass : 'picture_tag',
		},
		{
			textdata : data.string.ext2,
			textclass : 'class1 options ',
		},
		{
			textdata : data.string.ext3,
			textclass : 'class2 options corans',
		}
		],
		imageblock:[{
			imgclass:'centerimage',
			imgsrc:imgpath + 'q01.png'
		}]
	},
	//slide 1
	{
		contentblockadditionalclass:'bluebgg',
		extratextblock : [
		{
			textdata : data.string.ext1,
			textclass : 'instruction my_font_big',
		},
		{
			textdata : data.string.ext5,
			textclass : 'picture_tag',
		},
		{
			textdata : data.string.ext2,
			textclass : 'class1 options corans',
		},
		{
			textdata : data.string.ext3,
			textclass : 'class2 options ',
		}
		],
		imageblock:[{
			imgclass:'centerimage',
			imgsrc:imgpath + 'q02.png'
		}]
	},
	//slide 2
	{
		contentblockadditionalclass:'bluebgg',
		extratextblock : [
		{
			textdata : data.string.ext1,
			textclass : 'instruction my_font_big',
		},
		{
			textdata : data.string.ext6,
			textclass : 'picture_tag',
		},
		{
			textdata : data.string.ext2,
			textclass : 'class1 options corans',
		},
		{
			textdata : data.string.ext3,
			textclass : 'class2 options ',
		}
		],
		imageblock:[{
			imgclass:'centerimage',
			imgsrc:imgpath + 'q03.png'
		}]
	},
	//slide 3
	{
		contentblockadditionalclass:'bluebgg',
		extratextblock : [
		{
			textdata : data.string.ext1,
			textclass : 'instruction my_font_big',
		},
		{
			textdata : data.string.ext7,
			textclass : 'picture_tag',
		},
		{
			textdata : data.string.ext2,
			textclass : 'class1 options ',
		},
		{
			textdata : data.string.ext3,
			textclass : 'class2 options corans',
		}
		],
		imageblock:[{
			imgclass:'centerimage',
			imgsrc:imgpath + 'q04.png'
		}]
	},
	//slide 4
	{
		contentblockadditionalclass:'bluebgg',
		extratextblock : [
		{
			textdata : data.string.ext1,
			textclass : 'instruction my_font_big',
		},
		{
			textdata : data.string.ext8,
			textclass : 'picture_tag',
		},
		{
			textdata : data.string.ext2,
			textclass : 'class1 options corans',
		},
		{
			textdata : data.string.ext3,
			textclass : 'class2 options ',
		}
		],
		imageblock:[{
			imgclass:'centerimage',
			imgsrc:imgpath + 'q05.png'
		}]
	},
	//slide 5
	{
		contentblockadditionalclass:'bluebgg',
		extratextblock : [
		{
			textdata : data.string.ext1,
			textclass : 'instruction my_font_big',
		},
		{
			textdata : data.string.ext9,
			textclass : 'picture_tag',
		},
		{
			textdata : data.string.ext2,
			textclass : 'class1 options corans',
		},
		{
			textdata : data.string.ext3,
			textclass : 'class2 options ',
		}
		],
		imageblock:[{
			imgclass:'centerimage',
			imgsrc:imgpath + 'q06.png'
		}]
	},
	//slide 6
	{
		contentblockadditionalclass:'bluebgg',
		extratextblock : [
		{
			textdata : data.string.ext1,
			textclass : 'instruction my_font_big',
		},
		{
			textdata : data.string.ext10,
			textclass : 'picture_tag',
		},
		{
			textdata : data.string.ext2,
			textclass : 'class1 options ',
		},
		{
			textdata : data.string.ext3,
			textclass : 'class2 options corans',
		}
		],
		imageblock:[{
			imgclass:'centerimage',
			imgsrc:imgpath + 'q07.png'
		}]
	},
	//slide 7
	{
		contentblockadditionalclass:'bluebgg',
		extratextblock : [
		{
			textdata : data.string.ext1,
			textclass : 'instruction my_font_big',
		},
		{
			textdata : data.string.ext11,
			textclass : 'picture_tag',
		},
		{
			textdata : data.string.ext2,
			textclass : 'class1 options ',
		},
		{
			textdata : data.string.ext3,
			textclass : 'class2 options corans',
		}
		],
		imageblock:[{
			imgclass:'centerimage',
			imgsrc:imgpath + 'q08.png'
		}]
	},
	//slide 8
	{
		contentblockadditionalclass:'bluebgg',
		extratextblock : [
		{
			textdata : data.string.ext1,
			textclass : 'instruction my_font_big',
		},
		{
			textdata : data.string.ext12,
			textclass : 'picture_tag',
		},
		{
			textdata : data.string.ext2,
			textclass : 'class1 options ',
		},
		{
			textdata : data.string.ext3,
			textclass : 'class2 options corans',
		}
		],
		imageblock:[{
			imgclass:'centerimage',
			imgsrc:imgpath + 'q09.png'
		}]
	},
	//slide 9
	{
		contentblockadditionalclass:'bluebgg',
		extratextblock : [
		{
			textdata : data.string.ext1,
			textclass : 'instruction my_font_big',
		},
		{
			textdata : data.string.ext13,
			textclass : 'picture_tag',
		},
		{
			textdata : data.string.ext2,
			textclass : 'class1 options ',
		},
		{
			textdata : data.string.ext3,
			textclass : 'class2 options corans',
		}
		],
		imageblock:[{
			imgclass:'centerimage',
			imgsrc:imgpath + 'q10.png'
		}]
	},


];

content.shufflearray();


$(function ()
{
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	var score = 0;
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// sounds
      {id: "exer", src: soundAsset+"ex.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

	var rhino = new NumberTemplate();

	rhino.init($total_page);



	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);

		if(countNext==0){
			sound_player("exer");
		}
		$('.question_number').html('Question: '+ (countNext+1));
		var wrong_clicked = false;
		$(".options").click(function(){
			if($(this).hasClass("corans")){
				if(!wrong_clicked){
					rhino.update(true);
				}
				$(".options").css('pointer-events', 'none');
				$('.blank-space').html($(this).html().substring(4));
				play_correct_incorrect_sound(1);
				$(this).css({
					'color': 'rgb(56,142,55)',
				});
				if(countNext != $total_page)
					$nextBtn.show(0);
			}
			else{
				if(!wrong_clicked){
					rhino.update(false);
				}
				$(this).css({
					'color': 'rgb(182,77,4)',
					'text-decoration': 'line-through',
					'pointer-events': 'none',
				});
				wrong_clicked = true;
				play_correct_incorrect_sound(0);
			}
		});
	}
	function sound_player(sound_id, next){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
	}
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		rhino.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

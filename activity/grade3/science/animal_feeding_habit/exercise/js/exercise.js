var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+'/';

var content=[
	// slide0
	{
		contentblockadditionalclass: 'default-bg',
		ques_img_src: 'bee',
		exerciseblock: [
			{
				ques_class : '',
				instructiondata: data.string.exins,
				option: [{
					option_class: "class1",
					optionsrc : 'flower'
				},
				{
					option_class: "class2",
					optionsrc : 'fish'
				}],
			}
		]
	},
	// slide1
	{
		contentblockadditionalclass: 'default-bg',
		ques_img_src: 'mouse',
		exerciseblock: [
			{
				ques_class : '',
				instructiondata: data.string.exins,
				option: [{
					option_class: "class1",
					optionsrc : 'cheese'
				},
				{
					option_class: "class2",
					optionsrc : 'bone'
				}],
			}
		]
	},
	// slide2
	{
		contentblockadditionalclass: 'default-bg',
		ques_img_src: 'elephant',
		exerciseblock: [
			{
				ques_class : '',
				instructiondata: data.string.exins,
				option: [{
					option_class: "class1",
					optionsrc : 'grass'
				},
				{
					option_class: "class2",
					optionsrc : 'meat1'
				}],
			}
		]
	},
	// slide3
	{
		contentblockadditionalclass: 'default-bg',
		ques_img_src: 'chicken',
		exerciseblock: [
			{
				ques_class : '',
				instructiondata: data.string.exins,
				option: [{
					option_class: "class1",
					optionsrc : 'earthworm'
				},
				{
					option_class: "class2",
					optionsrc : 'milk'
				}],
			}
		]
	},
	// slide4
	{
		contentblockadditionalclass: 'default-bg',
		ques_img_src: 'dog',
		exerciseblock: [
			{
				ques_class : '',
				instructiondata: data.string.exins,
				option: [{
					option_class: "class1",
					optionsrc : 'bone'
				},
				{
					option_class: "class2",
					optionsrc : 'grass'
				}],
			}
		]
	},
	// slide5
	{
		contentblockadditionalclass: 'default-bg',
		ques_img_src: 'caterpiller',
		exerciseblock: [
			{
				ques_class : '',
				instructiondata: data.string.exins,
				option: [{
					option_class: "class1",
					optionsrc : 'leaves'
				},
				{
					option_class: "class2",
					optionsrc : 'meat'
				}],
			}
		]
	},
	// slide6
	{
		contentblockadditionalclass: 'default-bg',
		ques_img_src: 'lion',
		exerciseblock: [
			{
				ques_class : '',
				instructiondata: data.string.exins,
				option: [{
					option_class: "class1",
					optionsrc : 'meat'
				},
				{
					option_class: "class2",
					optionsrc : 'rice'
				}],
			}
		]
	},
	// slide7
	{
		contentblockadditionalclass: 'default-bg',
		ques_img_src: 'eagle',
		exerciseblock: [
			{
				ques_class : '',
				instructiondata: data.string.exins,
				option: [{
					option_class: "class1",
					optionsrc : 'snake'
				},
				{
					option_class: "class2",
					optionsrc : 'grass'
				}],
			}
		]
	},
	// slide8
	{
		contentblockadditionalclass: 'default-bg',
		ques_img_src: 'crocodile',
		exerciseblock: [
			{
				ques_class : '',
				instructiondata: data.string.exins,
				option: [{
					option_class: "class1",
					optionsrc : 'fish'
				},
				{
					option_class: "class2",
					optionsrc : 'fruit'
				}],
			}
		]
	},
	// slide9
	{
		contentblockadditionalclass: 'default-bg',
		ques_img_src: 'butterfly',
		exerciseblock: [
			{
				ques_class : '',
				instructiondata: data.string.exins,
				option: [{
					option_class: "class1",
					optionsrc : 'flower1'
				},
				{
					option_class: "class2",
					optionsrc : 'meat'
				}],
			}
		]
	},
];
content.shufflearray();

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;


	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	var scoring = new RhinoTemplate();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			{id: "bg", src: imgpath+'bg_exercise.png', type: createjs.AbstractLoader.IMAGE},

			{id: "bee", src: imgpath+'bee.png', type: createjs.AbstractLoader.IMAGE},
			{id: "flower", src: imgpath+'flower.png', type: createjs.AbstractLoader.IMAGE},
			{id: "flower1", src: imgpath+'flower01.png', type: createjs.AbstractLoader.IMAGE},
			{id: "fish", src: imgpath+'fish.png', type: createjs.AbstractLoader.IMAGE},

			{id: "mouse", src: imgpath+'mouse.png', type: createjs.AbstractLoader.IMAGE},
			{id: "cheese", src: imgpath+'cheese.png', type: createjs.AbstractLoader.IMAGE},
			{id: "bone", src: imgpath+'bone.png', type: createjs.AbstractLoader.IMAGE},

			{id: "elephant", src: imgpath+'elephant.png', type: createjs.AbstractLoader.IMAGE},
			{id: "meat", src: imgpath+'meat.png', type: createjs.AbstractLoader.IMAGE},
			{id: "meat1", src: imgpath+'meat01.png', type: createjs.AbstractLoader.IMAGE},
			{id: "grass", src: imgpath+'grass.png', type: createjs.AbstractLoader.IMAGE},

			{id: "chicken", src: imgpath+'chicken.png', type: createjs.AbstractLoader.IMAGE},
			{id: "milk", src: imgpath+'milk.png', type: createjs.AbstractLoader.IMAGE},
			{id: "earthworm", src: imgpath+'earthwom.png', type: createjs.AbstractLoader.IMAGE},

			{id: "dog", src: imgpath+'dog01.png', type: createjs.AbstractLoader.IMAGE},

			{id: "caterpiller", src: imgpath+'catterpiller.png', type: createjs.AbstractLoader.IMAGE},
			{id: "leaves", src: imgpath+'leaves.png', type: createjs.AbstractLoader.IMAGE},

			{id: "lion", src: imgpath+'lion.png', type: createjs.AbstractLoader.IMAGE},
			{id: "rice", src: imgpath+'rice.png', type: createjs.AbstractLoader.IMAGE},

			{id: "eagle", src: imgpath+'eagle.png', type: createjs.AbstractLoader.IMAGE},
			{id: "snake", src: imgpath+'snake01.png', type: createjs.AbstractLoader.IMAGE},

			{id: "crocodile", src: imgpath+'crock.png', type: createjs.AbstractLoader.IMAGE},
			{id: "fruit", src: imgpath+'fruits.png', type: createjs.AbstractLoader.IMAGE},

			{id: "butterfly", src: imgpath+'butterfly.png', type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"ex.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		scoring.init($total_page);
		templateCaller();
	}
	//initialize
	init();


	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templatecaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templatecaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
	/**
	 How to:
	 - Just call instructionblockcontroller() from the template
	 */


	/**
	 What it does:
	 - It inserts and handles closing and opening of instruction block
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	var wrongclick = false;
	var ques_count = ['1) ', '2) ', '3) ', '4) ', '5) ', '6) ', '7) ', '8) ', '9) ', '10) '];
	if ($lang == 'np') ques_count = ['१) ', '२) ', '३) ', '४) ', '५) ', '६) ', '७) ', '८) ', '९) ', '१०) '];

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		put_optimage(content, countNext);
		if(countNext==0){
			sound_player("sound_1");
		}

		var option_position = [1,2];
		option_position.shufflearray();
		for(var op=0; op<2; op++){
			$('.main-container').eq(op).addClass('option-pos-'+option_position[op]);
		}

		$('#num_ques').html(ques_count[countNext]);
		$('.default-bg').css({
			'background': "url(" + preload.getResult('bg').src + ")",
			'background-size': '100% 100%',
		});
		$('.ques_img').attr('src', preload.getResult(content[countNext].ques_img_src).src);
		$('.correct-icon').attr('src', preload.getResult('correct').src);
		$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);

		var wrong_clicked = 0;
		$(".option-container").click(function(){
			if($(this).hasClass("class1")){
				if(wrong_clicked<1){
					scoring.update(true);
				}
				$(".option-container").css('pointer-events','none');
					createjs.Sound.stop();
	 			play_correct_incorrect_sound(1);
				$(this).addClass('correct-ans');
				$(this).parent().children('.correct-icon').show(0);
				wrong_clicked = 0;
				if(countNext != $total_page)
					$nextBtn.show(0);
			}
			else{
				scoring.update(false);
					createjs.Sound.stop();
	 			play_correct_incorrect_sound(0);
				$(this).addClass('incorrect-ans');
				$(this).parent().children('.incorrect-icon').show(0);
				wrong_clicked++;
			}
		});

		$prevBtn.hide(0);
	}
	function put_optimage(content, count){
		if(content[count].hasOwnProperty('exerciseblock')){
			var exerciseblock = content[count].exerciseblock[0];
			if(exerciseblock.hasOwnProperty('option')){
				for(var i= 0; i<exerciseblock.option.length; i++){
					var imageClass = exerciseblock.option[i];
					var image_src = preload.getResult(imageClass.optionsrc).src;
					//get list of classes
					var classes_list = imageClass.option_class.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]+'>img');
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();
		generalTemplate();
	}

	$nextBtn.on('click', function() {
		countNext++;
		scoring.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
});

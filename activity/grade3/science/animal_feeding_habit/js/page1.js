var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+'/';

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		extratextblock:[{
			textdata: data.lesson.chapter,
			textclass: "lesson-title",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "cover_page",
					imgsrc : '',
					imgid : 'coverpage'
				}
			]
		}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		uppertextblockadditionalclass: 'text-1',
		uppertextblock:[{
			textdata: data.string.p1text1,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "img-bt",
					imgsrc : '',
					imgid : 'animals'
				}
			]
		}]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		uppertextblockadditionalclass: 'text-1',
		uppertextblock:[{
			textdata: data.string.p1text2,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "img-bt",
					imgsrc : '',
					imgid : 'animals'
				},
				{
					imgclass : "its_hidden img-bt-1",
					imgsrc : '',
					imgid : 'bee'
				},
				{
					imgclass : "its_hidden img-bt-2",
					imgsrc : '',
					imgid : 'rabbit'
				},
				{
					imgclass : "its_hidden img-bt-3",
					imgsrc : '',
					imgid : 'mouse'
				},
				{
					imgclass : "its_hidden img-bt-4",
					imgsrc : '',
					imgid : 'chicken'
				}
			]
		}]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		uppertextblockadditionalclass: 'text-1',
		uppertextblock:[{
			textdata: data.string.p1text3,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "img-bt-1",
					imgsrc : '',
					imgid : 'bee'
				},
				{
					imgclass : "img-bt-2",
					imgsrc : '',
					imgid : 'rabbit'
				},
				{
					imgclass : "img-bt-3",
					imgsrc : '',
					imgid : 'mouse'
				},
				{
					imgclass : "img-bt-4",
					imgsrc : '',
					imgid : 'chicken'
				}
			]
		}]
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		uppertextblockadditionalclass: 'text-1',
		uppertextblock:[{
			textdata: data.string.p1text4,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "active-img img-bt-1",
					imgsrc : '',
					imgid : 'bee'
				},
				{
					imgclass : "inactive-img img-bt-2",
					imgsrc : '',
					imgid : 'rabbit'
				},
				{
					imgclass : "inactive-img img-bt-3",
					imgsrc : '',
					imgid : 'mouse'
				},
				{
					imgclass : "inactive-img img-bt-4",
					imgsrc : '',
					imgid : 'chicken'
				}
			]
		}]
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		uppertextblockadditionalclass: 'text-1',
		uppertextblock:[{
			textdata: data.string.p1text5,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "inactive-img img-bt-1",
					imgsrc : '',
					imgid : 'bee'
				},
				{
					imgclass : "active-img img-bt-2",
					imgsrc : '',
					imgid : 'rabbit'
				},
				{
					imgclass : "inactive-img  img-bt-3",
					imgsrc : '',
					imgid : 'mouse'
				},
				{
					imgclass : "inactive-img img-bt-4",
					imgsrc : '',
					imgid : 'chicken'
				}
			]
		}]
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		uppertextblockadditionalclass: 'text-1',
		uppertextblock:[{
			textdata: data.string.p1text6,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "inactive-img img-bt-1",
					imgsrc : '',
					imgid : 'bee'
				},
				{
					imgclass : "inactive-img img-bt-2",
					imgsrc : '',
					imgid : 'rabbit'
				},
				{
					imgclass : "active-img  img-bt-3",
					imgsrc : '',
					imgid : 'mouse'
				},
				{
					imgclass : "inactive-img img-bt-4",
					imgsrc : '',
					imgid : 'chicken'
				}
			]
		}]
	},
	// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		uppertextblockadditionalclass: 'text-1',
		uppertextblock:[{
			textdata: data.string.p1text7,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "inactive-img img-bt-1",
					imgsrc : '',
					imgid : 'bee'
				},
				{
					imgclass : "inactive-img img-bt-2",
					imgsrc : '',
					imgid : 'rabbit'
				},
				{
					imgclass : "inactive-img img-bt-3",
					imgsrc : '',
					imgid : 'mouse'
				},
				{
					imgclass : "active-img  img-bt-4",
					imgsrc : '',
					imgid : 'chicken'
				}
			]
		}]
	},
	// slide8
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		uppertextblockadditionalclass: 'text-1',
		uppertextblock:[{
			textdata: data.string.p1text8,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "img-bt-1",
					imgsrc : '',
					imgid : 'bee'
				},
				{
					imgclass : "img-bt-2",
					imgsrc : '',
					imgid : 'rabbit'
				},
				{
					imgclass : "img-bt-3",
					imgsrc : '',
					imgid : 'mouse'
				},
				{
					imgclass : "img-bt-4",
					imgsrc : '',
					imgid : 'chicken'
				}
			]
		}]
	},
	// slide9
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		uppertextblockadditionalclass: 'text-2',
		uppertextblock:[{
			textdata: data.string.p1text9,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "sundar",
					imgsrc : '',
					imgid : 'sundar'
				},
				{
					imgclass : "bear",
					imgsrc : '',
					imgid : 'bear'
				},
				{
					imgclass : "turtle",
					imgsrc : '',
					imgid : 'turtle'
				},
			]
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "animals", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},

			{id: "bee", src: imgpath+"bee.png", type: createjs.AbstractLoader.IMAGE},
			{id: "chicken", src: imgpath+"hen.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sundar", src: imgpath+"monkey.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mouse", src: imgpath+"mouse.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rabbit", src: imgpath+"rabbit.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bear", src: imgpath+"beer.png", type: createjs.AbstractLoader.IMAGE},
			{id: "turtle", src: imgpath+"tortle.png", type: createjs.AbstractLoader.IMAGE},
			{id: "coverpage", src: imgpath+"coverpage.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_1", src: soundAsset+"p1_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p1_s1.ogg"},
			{id: "sound_3", src: soundAsset+"p1_s2.ogg"},
			{id: "sound_4", src: soundAsset+"p1_s3.ogg"},
			{id: "sound_5", src: soundAsset+"p1_s4.ogg"},
			{id: "sound_6", src: soundAsset+"p1_s5.ogg"},
			{id: "sound_7", src: soundAsset+"p1_s6.ogg"},
			{id: "sound_8", src: soundAsset+"p1_s7.ogg"},
			{id: "sound_9", src: soundAsset+"p1_s8.ogg"},
			{id: "sound_10", src: soundAsset+"p1_s9.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);

		switch(countNext) {
			case 2:
				$prevBtn.show(0);
				sound_nav('sound_3');
				$('.img-bt').delay(1500).fadeOut(1000, function(){
					$('.img-bt-1').fadeIn(500, function(){
						$('.img-bt-2').fadeIn(500, function(){
							$('.img-bt-3').fadeIn(500, function(){
									$('.img-bt-4').fadeIn(500);
							});
						});
					});
				});
				break;
			default:
			// if(countNext==0){
			// 	$prevBtn.hide(0);
			// }else{
			// 	$prevBtn.show(0);
			// }
							// nav_button_controls(0);
				sound_nav('sound_'+(countNext+1));

				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

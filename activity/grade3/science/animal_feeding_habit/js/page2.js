var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+'/';

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		uppertextblockadditionalclass: 'text-1',
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "",
		}],
		imagedivblock:[
			{
				imagediv: 'img-container container-1 slide-top',
				imgclass : "mouse-img",
				imgsrc : '',
				imgid : 'rabbit',
				imagelabelclass: 'img-label',
				imagelabeldata: data.string.p2text2,
			},
			{
				imagediv: 'img-container container-2 slide-top',
				imgclass : "snake-img",
				imgsrc : '',
				imgid : 'snake',
				imagelabelclass: 'img-label',
				imagelabeldata: data.string.p2text5,
			},
			{
				imagediv: 'img-container container-3 slide-top',
				imgclass : "dog-img",
				imgsrc : '',
				imgid : 'dog',
				imagelabelclass: 'img-label',
				imagelabeldata: data.string.p2text9,
			}
		]
	},

	// slide1 - herbivore
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		uppertextblockadditionalclass: 'topic-title',
		uppertextblock:[{
			textdata: data.string.p2text2,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "img-bt-2",
					imgsrc : '',
					imgid : 'rabbit'
				},
				{
					imgclass : "img-bt-3",
					imgsrc : '',
					imgid : 'caterpiller'
				},
				{
					imgclass : "img-bt-4",
					imgsrc : '',
					imgid : 'elephant'
				}
			]
		}]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		uppertextblockadditionalclass: 'text-2',
		uppertextblock:[{
			textdata: data.string.p2text3,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "img-bt-2",
					imgsrc : '',
					imgid : 'rabbit'
				},
				{
					imgclass : "img-bt-3",
					imgsrc : '',
					imgid : 'caterpiller'
				},
				{
					imgclass : "img-bt-4",
					imgsrc : '',
					imgid : 'elephant'
				}
			]
		}]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		uppertextblockadditionalclass: 'text-1',
		uppertextblock:[{
			textdata: data.string.p2text4,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "img-bt-2",
					imgsrc : '',
					imgid : 'rabbit'
				},
				{
					imgclass : "img-bt-3",
					imgsrc : '',
					imgid : 'caterpiller'
				},
				{
					imgclass : "img-bt-4",
					imgsrc : '',
					imgid : 'elephant'
				}
			]
		}]
	},

	// slide4 - carnivore
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		uppertextblockadditionalclass: 'topic-title',
		uppertextblock:[{
			textdata: data.string.p2text5,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "img-bt-c1",
					imgsrc : '',
					imgid : 'hawk'
				},
				{
					imgclass : "img-bt-c2",
					imgsrc : '',
					imgid : 'tiger'
				},
				{
					imgclass : "img-bt-c3",
					imgsrc : '',
					imgid : 'snake'
				}
			]
		}]
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		uppertextblockadditionalclass: 'text-2',
		uppertextblock:[{
			textdata: data.string.p2text6,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "img-bt-cc1",
					imgsrc : '',
					imgid : 'tiger'
				},
				{
					imgclass : "img-bt-cc2",
					imgsrc : '',
					imgid : 'snake'
				},
				{
					imgclass : "img-bt-cc3",
					imgsrc : '',
					imgid : 'crocodile'
				},
				{
					imgclass : "img-bt-cc4",
					imgsrc : '',
					imgid : 'hawk'
				}
			]
		}]
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		uppertextblockadditionalclass: 'text-2',
		uppertextblock:[{
			textdata: data.string.p2text7,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "img-bt-ccc1",
					imgsrc : '',
					imgid : 'lion'
				},
				{
					imgclass : "img-bt-ccc2",
					imgsrc : '',
					imgid : 'eagle'
				},
				{
					imgclass : "img-bt-ccc3",
					imgsrc : '',
					imgid : 'spider'
				},
			]
		}]
	},
	// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		uppertextblockadditionalclass: 'text-1',
		uppertextblock:[{
			textdata: data.string.p2text8,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "img-bt-ccc1",
					imgsrc : '',
					imgid : 'lion'
				},
				{
					imgclass : "img-bt-ccc2",
					imgsrc : '',
					imgid : 'eagle'
				},
				{
					imgclass : "img-bt-ccc3",
					imgsrc : '',
					imgid : 'spider'
				},
			]
		}]
	},

	// slide8 - omnivore
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		uppertextblockadditionalclass: 'topic-title',
		uppertextblock:[{
			textdata: data.string.p2text9,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "img-bt-s1",
					imgsrc : '',
					imgid : 'dog'
				},
				{
					imgclass : "img-bt-s2",
					imgsrc : '',
					imgid : 'boy'
				},
				{
					imgclass : "img-bt-s3",
					imgsrc : '',
					imgid : 'chicken'
				}
			]
		}]
	},
	// slide9
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		uppertextblockadditionalclass: 'text-2',
		uppertextblock:[{
			textdata: data.string.p2text10,
			textclass: "",
		},{
			textdata: data.string.p2text11,
			textclass: "late_show",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "img-bt-s1",
					imgsrc : '',
					imgid : 'dog'
				},
				{
					imgclass : "late_show img-bt-s2",
					imgsrc : '',
					imgid : 'boy'
				},
				{
					imgclass : "img-bt-s3",
					imgsrc : '',
					imgid : 'chicken'
				}
			]
		}]
	},
	// slide10
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		uppertextblockadditionalclass: 'text-1',
		uppertextblock:[{
			textdata: data.string.p2text12,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "img-bt-s1",
					imgsrc : '',
					imgid : 'dog'
				},
				{
					imgclass : "img-bt-s2",
					imgsrc : '',
					imgid : 'boy'
				},
				{
					imgclass : "img-bt-s3",
					imgsrc : '',
					imgid : 'chicken'
				}
			]
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "animals", src: imgpath+"animals.png", type: createjs.AbstractLoader.IMAGE},

			{id: "dog", src: imgpath+"dog.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy", src: imgpath+"boy-eating-carrot.png", type: createjs.AbstractLoader.IMAGE},
			{id: "chicken", src: imgpath+"hen.png", type: createjs.AbstractLoader.IMAGE},

			{id: "mouse", src: imgpath+"mouse.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rabbit", src: imgpath+"rabbit.png", type: createjs.AbstractLoader.IMAGE},
			{id: "elephant", src: imgpath+"elephant.png", type: createjs.AbstractLoader.IMAGE},
			{id: "caterpiller", src: imgpath+"catterpillar.png", type: createjs.AbstractLoader.IMAGE},

			{id: "snake", src: imgpath+"snake02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hawk", src: imgpath+"eagle01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tiger", src: imgpath+"tiger01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "crocodile", src: imgpath+"crock.png", type: createjs.AbstractLoader.IMAGE},
			{id: "lion", src: imgpath+"lion.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eagle", src: imgpath+"eagle.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spider", src: imgpath+"spider.png", type: createjs.AbstractLoader.IMAGE},
			{id: "coverpage", src: imgpath+"coverpage.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_1", src: soundAsset+"p2_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p2_s1.ogg"},
			{id: "sound_3", src: soundAsset+"p2_s2.ogg"},
			{id: "sound_4", src: soundAsset+"p2_s3.ogg"},
			{id: "sound_5", src: soundAsset+"p2_s4.ogg"},
			{id: "sound_6", src: soundAsset+"p2_s5.ogg"},
			{id: "sound_7", src: soundAsset+"p2_s6.ogg"},
			{id: "sound_8", src: soundAsset+"p2_s7.ogg"},
			{id: "sound_9", src: soundAsset+"p2_s8.ogg"},
			{id: "sound_10_1", src: soundAsset+"p2_s10_1.ogg"},
			{id: "sound_10_2", src: soundAsset+"p2_s10_2.ogg"},
			{id: "sound_10", src: soundAsset+"p2_s9.ogg"},
			{id: "sound_11", src: soundAsset+"p2_s10.ogg"},
			{id: "sound_12", src: soundAsset+"carnivourous.ogg"},
			{id: "sound_13", src: soundAsset+"herbivorous.ogg"},
			{id: "sound_14", src: soundAsset+"omnivorous.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);

		switch(countNext) {
			case 0:
				current_sound = createjs.Sound.play('sound_1');
				current_sound.play();
				current_sound.on("complete", function(){
					$('.container-1').show(0);
					timeoutvar= setTimeout(function(){
						current_sound = createjs.Sound.play('sound_13');
						current_sound.play();
						current_sound.on("complete", function(){
							$('.container-2').show(0);
							timeoutvar= setTimeout(function(){
								current_sound = createjs.Sound.play('sound_12');
								current_sound.play();
								current_sound.on("complete", function(){
									$('.container-3').show(0);
									timeoutvar= setTimeout(function(){
										current_sound = createjs.Sound.play('sound_14');
										current_sound.play();
										current_sound.on("complete", function(){
											nav_button_controls(0);
										});
									}, 1500);
								});
							}, 1500);
						});
					}, 1500);
				});
				break;
				case 9:
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("sound_10_1");
					current_sound.play();
					current_sound.on("complete", function(){
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("sound_10_2");
						current_sound.play();
						$(".late_show").addClass("fade_in");
						current_sound.on("complete", function(){
						nav_button_controls(0);
					});
				});

				break;
			default:
				sound_nav('sound_'+(countNext+1));
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageClass = content[count].imagedivblock;
			for(var i=0; i<imageClass.length; i++){
				var image_src = preload.getResult(imageClass[i].imgid).src;
				//get list of classes
				var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
// var content;

// function getContent(data){
var content=[
	//slide 0
	{
		hasheaderblock : true,
		additionalclasscontentblock : "singleshapeintro",
		imageblockadditionalclass : "fiftypage",
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "object6 opa ready7",
				imgsrc : imgpath + "images/ruler-cm-only.png"
			},
			{
				imgclass: "object7 cssfadein",
				imgsrc : imgpath + "images/pencil-green.png"
			}
			],

			imagelabels:[
			{
				imagelabelclass: "objecttext objecttext12 cssfadein",
				imagelabeldata: data.string.p2text12,
			},
			{
				imagelabelclass: "objecttext objecttext13 cssfadein",
				imagelabeldata: data.string.p2text13,
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext",

				imagelabelclass: "objecttext objecttext14 cssfadein",
				imagelabeldata: data.string.p2text14,
			},
			{
				imagelabelclass: "objecttext objecttext15 cssfadein",
				imagelabeldata: data.string.p2text15,
			}
			]
		}
		]
	},

	//slide 1
	{
		hasheaderblock : true,

		additionalclasscontentblock : "singleshapeintro",
		divtosnap: true,

		snaptoobject: "snaphere",
		imageblockadditionalclass : "fiftypage",

		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "object8 opa",
				imgsrc : imgpath + "images/ruler-cm-only.png"
			}

			],

			imagelabels:[

			{
				imagelabelclass: "objecttext objecttext16 cssfadein",
				imagelabeldata: data.string.p2text16,
			},

			]

		}
		],
		tableblock : [
		{
			checkclass: "check",
			check: data.string.p2text22,
			colclass1: "column1",
			tableheading: [
			{
				h1: data.string.name,
				h2: data.string.ojb,
				h3: data.string.len
			}],
			tablebody: [
				//1st row
				{
					c1: data.string.rp,
					tableimage:[
					{
						imgclass: "tableobject1 cssfadein",
						imgsrc : imgpath + "images/pencil-red.png"
					}
					],
					tablebox:[
					{
						boxtype: "text",
						boxname: "box1",
						textclass: "box1"
					}
					]
				},

				//2nd row
				{
					c1: data.string.yp,
					tableimage:[
					{
						imgclass: "tableobject2 cssfadein",
						imgsrc : imgpath + "images/penci-yellow.png"
					}
					],

					tablebox:[
					{
						boxtype: "text",
						boxname: "box1",
						textclass: "box2"
					}
					]
				},

				//3rd row
				{
					c1: data.string.gc,
					tableimage:[
					{
						imgclass: "tableobject3 cssfadein",
						imgsrc : imgpath + "images/crayon_green.png"
					}
					],

					tablebox:[
					{
						boxtype: "text",
						boxname: "box1",
						textclass: "box3"
					}
					]
				},

				//4th row
				{
					c1: data.string.bs,
					tableimage:[
					{
						imgclass: "tableobject4 cssfadein",
						imgsrc : imgpath + "images/sharpner.png"
					}
					],
					tablebox:[
					{
						boxtype: "text",
						boxname: "box1",
						textclass: "box4"
					}
					]
				}
				]
			}
		]
	},


	//slide 2
	{
		hasheaderblock : true,

		additionalclasscontentblock : "singleshapeintro",
		imageblockadditionalclass : "firstpage",
		imageblock: [
		{
			imagelabels:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext",
				imagelabelclass: "objecttext objecttext17 cssfadein",
				imagelabeldata: data.string.p2text17
			}
			]

		}
		]
	},

	//slide 3
	{
		hasheaderblock : true,

		additionalclasscontentblock : "singleshapeintro",
		imageblockadditionalclass : "firstpage",
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "object9",
				imgsrc : imgpath + "images/hand.png"
			},
			{
				imgclass: "object10",
				imgsrc : imgpath + "images/girl.png"
			},
			{
				imgclass: "object11",
				imgsrc : imgpath + "images/rat.png"
			},
			{
				imgclass: "object12",
				imgsrc : imgpath + "images/cat.png"
			}
			],

			imagelabels:[
			{
				imagelabelclass: "objecttext objecttext17 objecttext18 opa",
				imagelabeldata: data.string.p2text17
			},
			{
				imagelabelclass: "objecttext objecttext19",
				imagelabeldata: data.string.p2text18
			},
			{
				imagelabelclass: "objecttext objecttext20",
				imagelabeldata: data.string.p2text19
			},
			{
				imagelabelclass: "objecttext objecttext21",
				imagelabeldata: data.string.p2text20
			},
			{
				imagelabelclass: "objecttext objecttext22",
				imagelabeldata: data.string.p2text21
			}
			]
		}
		]
	},
	];

$(function(){

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	// var total_page = 0;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*recalculateHeightWidth();*/

	// var total_page = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);

	var preload;
	var timeoutvar = null;
	var current_sound;


			function init() {
				//specify type otherwise it will load assests as XHR
				manifest = [
					// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
					// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
					//   ,
					//images


					//sounds
					{id: "sound_0", src: soundAsset+"S2_P1_1.ogg"},
					{id: "sound_1", src: soundAsset+"S2_P1_3.ogg"},
					{id: "sound_2", src: soundAsset+"s2_p2.ogg"},
					{id: "sound_3", src: soundAsset+"S2_P3.ogg"},
					{id: "sound_4", src: soundAsset+"S2_P4.ogg"},
					{id: "sound_5", src: soundAsset+"S2_P1_2.ogg"},

				];
				preload = new createjs.LoadQueue(false);
				preload.installPlugin(createjs.Sound);//for registering sounds
				preload.on("progress", handleProgress);
				preload.on("complete", handleComplete);
				preload.on("fileload", handleFileLoad);
				preload.loadManifest(manifest, true);
			}
			function handleFileLoad(event) {
				console.log(event.item);
			}
			function handleProgress(event) {
				$('#loading-text').html(parseInt(event.loaded*100)+'%');
			}
			function handleComplete(event) {
				$('#loading-wrapper').hide(0);
				//initialize varibales
				// current_sound = createjs.Sound.play('sound_1');
				// current_sound.stop();
				// call main function
				templateCaller();
			}
			//initialize
			init();



	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
		Handlebars.registerPartial("tablecontent", $("#tablecontent-partial").html());


	//controls the navigational state of the program

	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
      */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
      */
        function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		$(".box1, .box2, .box3, .box4, .box5, .box6, .box7, .box8").keypress(function (e) {
			if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {

				return false;
			}

		});

		$board.on('click','.check',function () {
			if($(".box1").val()==17&& $(".box2").val()==11 && $(".box3").val()==8 && $(".box4").val()==3) {
				$nextBtn.show(0);
			}
			$("td>img").css({
				'display': 'inline'
			});
			//red pencil
			if($(".box1").val() == 17){
				$(".box1").css({
					"background":"#C5F295",
					"box-shadow":"none",
					'pointer-events':'none'
				});
				$(".box1~img").attr('src', 'images/correct.png');

			}
			else{
				$(".box1").css({
					"background":"#FF7660"
				});
                $(".box1~img").attr('src', 'images/wrong.png');
            }

			//yellow pencil
			if($(".box2").val() == 11){
				$(".box2").css({
					"background":"#C5F295",
					"box-shadow":"none",
					'pointer-events':'none'
				});
				$(".box2~img").attr('src', 'images/correct.png');
			}
			else{
				$(".box2").css({
					"background":"#FF7660"
				});
                $(".box2~img").attr('src', 'images/wrong.png');
            }

			//green crayon
			if($(".box3").val() == 8){
				$(".box3").css({
					"background":"#C5F295",
					"box-shadow":"none",
					'pointer-events':'none'
				});
				$(".box3~img").attr('src', 'images/correct.png');
			}
			else{
				$(".box3").css({
					"background":"#FF7660"
				});
                $(".box3~img").attr('src', 'images/wrong.png');
            }

			//sharpener
			if($(".box4").val() == 3){
				$(".box4").css({
					"background":"#C5F295",
					"box-shadow":"none",
					'pointer-events':'none'
				});
				$(".box4~img").attr('src', 'images/correct.png');
			}
			else{
				$(".box4").css({
					"background":"#FF7660"
				});
                $(".box4~img").attr('src', 'images/wrong.png');

            }
		});

		$board.on('click','.check2',function () {
			if($(".box5").val()!="" || $(".box6").val()!="" || $(".box7").val()!="" || $(".box8").val()!="") {
				$nextBtn.show(0);
			}
			//feather
			if($(".box5").val() == 13){
				$(".box5").css({
					"box-shadow":"none",
					"background":"#C5F295",
					'pointer-events':'none'
				});
				$(".box5~img").attr('src', 'images/correct.png');
			}
			else{
				$(".box5").css({
					"border-color":"red",
					"background":"#FF7660"
				});
                $(".box5~img").attr('src', 'images/wrong.png');
            }

			//spoon
			if($(".box6").val() == 10){
				$(".box6").css({
					"box-shadow":"none",
					"background":"#C5F295",
					'pointer-events':'none'
				});
				$(".box6~img").attr('src', 'images/correct.png');
			}
			else{
				$(".box6").css({
					"border-color":"red",
					"background":"#FF7660"
				});
                $(".box6~img").attr('src', 'images/wrong.png');

            }

			//insect
			if($(".box7").val() == 4){
				$(".box7").css({
					"box-shadow":"none",
					"background":"#C5F295",
					"box-shadow":"none",
					'pointer-events':'none'
				});
				$(".box7~img").attr('src', 'images/correct.png');
			}
			else{
				$(".box7").css({
					"border-color":"red",
					"background":"#FF7660"
				});
                $(".box7~img").attr('src', 'images/wrong.png');

            }

			//line
			if($(".box8").val() == 18){
				$(".box8").css({
					"box-shadow":"none",
					"background":"#C5F295",
					'pointer-events':'none'
				});
				$(".box8~img").attr('src', 'images/correct.png');
			}
			else{
				$(".box8").css({
					"border-color":"red",
					"background":"#FF7660"
				});
                $(".box8~img").attr('src', 'images/wrong.png');

            }
		});


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		$('.objecttext13').hide(0);
		$('.objecttext14').hide(0);
		$('.objecttext15').hide(0);
		switch (countNext) {
			case 0:
			sound_player("sound_0");
			var $imageblock = $(".imageblock");
			var top = 0;
			var left = 0;
			var bottom = 0;
			var topfixed = false;

			var leftfixed = false;
			$(".object7").draggable({
				containment : ".contentblock",
				cursor : "move",
				zIndex: 1000,
				drag : function(event, ui) {
					top = $imageblock.height();
					left = $imageblock.width();

					left = left * 0.095;
					top = top /12;
					bottom = top / 10;


					if(ui.position.top <= top && !topfixed){
						ui.position.top = top;
						topfixed = true;
						 		//$('.objecttext12').text(data.string.p2text13);

						 		$('.objecttext12').hide(0);
						 		$('.objecttext13').show(0);
								sound_player("sound_5");
						 	}
						 	else if(topfixed)
						 	{
						 		ui.position.top = top;
						 		if (ui.position.left <= left && !leftfixed){
						 			ui.position.left = left;
						 			leftfixed = true;


						 		}else if (leftfixed){
									sound_player_nav("sound_1");
						 			ui.position.left = left;
						 			$('.objecttext13').hide(0);
						 			$('.objecttext14').show(0);
						 			$('.objecttext15').show(0);
						 			// $nextBtn.show(0);
						 		}
						 	}
						 },
					 });

				break;
			case 1:
			$nextBtn.hide(0);
			sound_player("sound_2");
			var $contentblock = $(".generalTemplateblock");

			$(".tableobject1, .tableobject2, .tableobject3, .tableobject4").draggable({
				containment : ".generalTemplateblock",
				cursor : "grabbing",
				zIndex: 1000,
				snap: ".snaphere",
				snapTolerance: 50,
				snapMode: "inner",
				drag : function(event, ui){
					itemDragged(event, ui);
				}
			});

			function itemDragged(event, ui){
				if(!$(this).hasClass("tableobject1"))
					$(".tableobject1").css({top: '21%', left: '27%'});

				if(!$(this).hasClass("tableobject2"))
					$(".tableobject2").css({top: '41%', left: '27%'});

				if(!$(this).hasClass("tableobject3"))
					$(".tableobject3").css({top: '61%', left: '27%'});

				if(!$(this).hasClass("tableobject4"))
					$(".tableobject4").css({top: '81%', left: '27%'});
			}

			break;
			case 2:
			sound_player_nav("sound_3");
			break;
			case 3:
			setTimeout(function(){
				sound_player_nav("sound_4");
			},1500);
			ole.footerNotificationHandler.hideNotification();
			setTimeout(function(){ ole.footerNotificationHandler.lessonEndSetNotification()  }, 8200);
			break;

		}
		function nav_button_controls(delay_ms){
			timeoutvar = setTimeout(function(){
				if(countNext==0){
					$nextBtn.show(0);
				} else if( countNext>0 && countNext == $total_page-1){
					$prevBtn.show(0);
					ole.footerNotificationHandler.pageEndSetNotification();
				} else{
					$prevBtn.show(0);
					$nextBtn.show(0);
				}
			},delay_ms);
		}
		function sound_player(sound_id){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
			current_sound.on("complete", function(){
				// nav_button_controls(0);
				});
		}
		function sound_player_nav(sound_id){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
			current_sound.on("complete", function(){
				nav_button_controls(0);
				});
		}
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}
	templateCaller();


	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	/*
	*/
	// });

});

 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span>";


					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

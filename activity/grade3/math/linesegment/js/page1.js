var imgpath = $ref + "/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

// var content;

// function getContent(data){
var content=[
	//slide 0
	{
		additionalclasscontentblock : 'backgroundss',
		uppertextblock : [{
		textdata : data.lesson.chapter,
		textclass : 'lesson-title vertical-horizontal-center'
		}]
	},
	{
		hasheaderblock : true,

		additionalclasscontentblock : "singleshapeintro",
		imageblockadditionalclass : "firstpage",
		imageblock: [
			{
				imagetoshow: [
					{
						imgclass: "object1 cssfadein",
						imgsrc : imgpath + "images/pencil-red.png"
					},
					{
						imgclass: "object2 cssfadein",
						imgsrc : imgpath + "images/penci-yellow.png"
					}

				],

				imagelabels:[
					{
						datahighlightflag: true,
						datahighlightcustomclass: "hightext1",
						datahighlightcustomclass2: "hightext12",
						datahighlightcustomclass3: "hightext13",
						imagelabelclass: "objecttext objecttext1 cssfadein",
						imagelabeldata: data.string.p2text1
					}
				]

			}
		]
	},

	//slide 1
	{
		hasheaderblock : true,

		additionalclasscontentblock : "singleshapeintro",
		imageblockadditionalclass : "firstpage",

		imageblock: [
			{
				imagetoshow: [
					{
						imgclass: "object1 opa",
						imgsrc : imgpath + "images/pencil-red.png"
					},
					{
						imgclass: "object2 opa",
						imgsrc : imgpath + "images/penci-yellow.png"
					},
					{
						imgclass: "object3 cssfadein",
						imgsrc : imgpath + "images/crayon_green.png"
					}

				],

				imagelabels:[
					{
						datahighlightflag: true,
						datahighlightcustomclass: "hightext1",
						datahighlightcustomclass2: "hightext12",
						datahighlightcustomclass3: "hightext13",
						imagelabelclass: "objecttext objecttext1 opa",
						imagelabeldata: data.string.p2text1,
					},
					{
						datahighlightflag: true,
						datahighlightcustomclass: "hightext2",
						datahighlightcustomclass2: "hightext22",
						datahighlightcustomclass3: "hightext23",
						imagelabelclass: "objecttext objecttext2 cssfadein",
						imagelabeldata: data.string.p2text2,
					}
				]

			}
		]
	},

	//slide 2
	{
		hasheaderblock : true,

		additionalclasscontentblock : "singleshapeintro",
		imageblockadditionalclass : "firstpage",

		imageblock: [
			{
				imagetoshow: [
					{
						imgclass: "object1 opa",
						imgsrc : imgpath + "images/pencil-red.png"
					},
					{
						imgclass: "object2 opa",
						imgsrc : imgpath + "images/penci-yellow.png"
					},
					{
						imgclass: "object3 opa",
						imgsrc : imgpath + "images/crayon_green.png"
					},
					{
						imgclass: "object4 cssfadein",
						imgsrc : imgpath + "images/sharpner.png"
					}

				],

				imagelabels:[
					{
						datahighlightflag: true,
						datahighlightcustomclass: "hightext1",
						datahighlightcustomclass2: "hightext12",
						datahighlightcustomclass3: "hightext13",
						imagelabelclass: "objecttext objecttext1 opa",
						imagelabeldata: data.string.p2text1,
					},
					{
						datahighlightflag: true,
						datahighlightcustomclass: "hightext2",
						datahighlightcustomclass2: "hightext22",
						datahighlightcustomclass3: "hightext23",
						imagelabelclass: "objecttext objecttext2 opa",
						imagelabeldata: data.string.p2text2,
					},
					{
						datahighlightflag: true,
						datahighlightcustomclass: "hightext3",
						datahighlightcustomclass2: "hightext32",
						datahighlightcustomclass3: "hightext33",
						imagelabelclass: "objecttext objecttext3 cssfadein",
						imagelabeldata: data.string.p2text3,
					}
				]

			}
		]
	},

	//slide 3
	{
		hasheaderblock : true,

		additionalclasscontentblock : "singleshapeintro",
		imageblockadditionalclass : "firstpage",

		imageblock: [
			{
				imagetoshow: [
					{
						imgclass: "object1 opa",
						imgsrc : imgpath + "images/pencil-red.png"
					},
					{
						imgclass: "object2 opa",
						imgsrc : imgpath + "images/penci-yellow.png"
					},
					{
						imgclass: "object3 opa",
						imgsrc : imgpath + "images/crayon_green.png"
					},
					{
						imgclass: "object4 opa",
						imgsrc : imgpath + "images/sharpner.png"
					}

				],

				imagelabels:[
					{
						imagelabelclass: "objecttext objecttext4 csspopupleft",
						imagelabeldata: data.string.p2text4,
					}
				]

			}
		]
	},

	//slide 4
	{
		hasheaderblock : true,

		additionalclasscontentblock : "singleshapeintro",
		imageblockadditionalclass : "firstpage",

		imageblock: [
			{
				imagetoshow: [
					{
						imgclass: "object1 halfopa",
						imgsrc : imgpath + "images/pencil-red.png"
					},
					{
						imgclass: "object2 halfopa",
						imgsrc : imgpath + "images/penci-yellow.png"
					},
					{
						imgclass: "object3 halfopa",
						imgsrc : imgpath + "images/crayon_green.png"
					},
					{
						imgclass: "object4 halfopa",
						imgsrc : imgpath + "images/sharpner.png"
					},
					{
						imgclass: "object5 cssfadein",
						imgsrc : imgpath + "images/ruler01.png"
					}

				],

				imagelabels:[
					{
						imagelabelclass: "objecttext objecttext4 halfopa",
						imagelabeldata: data.string.p2text4,
					},
					{
						imagelabelclass: "objecttext objecttext5 cssfadein",
						imagelabeldata: data.string.p2text5,
					}
				]

			}
		]
	},

	//slide 5
	{
		hasheaderblock : true,

		additionalclasscontentblock : "singleshapeintro",
		imageblockadditionalclass : "fiftypage",
		imageblock: [
			{
				imagetoshow: [
					{
						imgclass: "object6 opa csspopup",
						imgsrc : imgpath + "images/ruler01.png"
					}

				],

				imagelabels:[

					{
						imagelabelclass: "objecttext objecttext6 opa csspopup",
						imagelabeldata: data.string.p2text6,
					}
				]

			}
		]
	},

	//slide 6
	{
		hasheaderblock : true,

		additionalclasscontentblock : "singleshapeintro",
		imageblockadditionalclass : "fiftypage",
		imageblock: [
			{
				imagetoshow: [
					{
						imgclass: "object6 opa ready6",
						imgsrc : imgpath + "images/ruler01.png"
					}

				],

				imagelabels:[

					{
						imagelabelclass: "objecttext objecttext7 cssfadein",
						imagelabeldata: data.string.p2text7,
					}
				]

			}
		]
	},

	//slide 7
	{
		hasheaderblock : true,

		additionalclasscontentblock : "singleshapeintro",
		imageblockadditionalclass : "fiftypage",
		imageblock: [
			{
				imagetoshow: [
					{
						imgclass: "object6 opa ready6",
						imgsrc : imgpath + "images/ruler_cm.png"
					}

				],

				imagelabels:[

					{
						imagelabelclass: "objecttext objecttext7 opa",
						imagelabeldata: data.string.p2text7,
					},
					{
						imagelabelclass: "objecttext objecttext8 cssfadein",
						imagelabeldata: data.string.p2text8,
					}
				]

			}
		]
	},

	//slide 8
	{
		hasheaderblock : true,

		additionalclasscontentblock : "singleshapeintro",
		imageblockadditionalclass : "fiftypage",
		imageblock: [
			{
				imagetoshow: [
					{
						imgclass: "object6 opa ready6",
						imgsrc : imgpath + "images/ruler_inch.png"
					}

				],

				imagelabels:[

					{
						imagelabelclass: "objecttext objecttext7 opa",
						imagelabeldata: data.string.p2text7,
					},
					{
						imagelabelclass: "objecttext objecttext8 opa",
						imagelabeldata: data.string.p2text8,
					},
					{
						imagelabelclass: "objecttext objecttext9 cssfadein",
						imagelabeldata: data.string.p2text9,
					}
				]

			}
		]
	},

	//slide 9
	{
		hasheaderblock : true,

		additionalclasscontentblock : "singleshapeintro",
		imageblockadditionalclass : "fiftypage",
		imageblock: [
			{
				imagetoshow: [
					{
						imgclass: "object6 opa cssfadeout",
						imgsrc : imgpath + "images/ruler_inch.png"
					},
					{
						imgclass: "object6 opa ready7",
						imgsrc : imgpath + "images/ruler-cm-only.png"
					}

				],

				imagelabels:[

					{
						imagelabelclass: "objecttext objecttext10 cssfadein",
						imagelabeldata: data.string.p2text10,
					},
					{
						imagelabelclass: "objecttext objecttext8 cssfadein",
						imagelabeldata: data.string.p2text8,
					},
					{
						imagelabelclass: "objecttext objecttext11 cssfadein",
						imagelabeldata: data.string.p2text11,
					}
				]

			}
		]
	},
];
// 	return content;
// }


$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	// var total_page = 0;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*recalculateHeightWidth();*/


	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);


		// var total_page = 0;


		var preload;
		var timeoutvar = null;
		var current_sound;


		function init() {
			//specify type otherwise it will load assests as XHR
			manifest = [
				// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
				// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
				//   ,
				//images


				//sounds
				{id: "sound_0", src: soundAsset+"S1_P1.ogg"},
				{id: "sound_1", src: soundAsset+"S1_P2.ogg"},
				{id: "sound_2", src: soundAsset+"S1_P3.ogg"},
				{id: "sound_3", src: soundAsset+"S1_P4.ogg"},
				{id: "sound_4", src: soundAsset+"S1_P5.ogg"},
				{id: "sound_5", src: soundAsset+"S1_P6.ogg"},
				{id: "sound_6", src: soundAsset+"S1_P7.ogg"},
				{id: "sound_7", src: soundAsset+"S1_P8.ogg"},
				{id: "sound_8", src: soundAsset+"S1_P9.ogg"},
				{id: "sound_9", src: soundAsset+"S1_P10.ogg"},
				{id: "sound_10", src: soundAsset+"S1_P11.ogg"},

			];
			preload = new createjs.LoadQueue(false);
			preload.installPlugin(createjs.Sound);//for registering sounds
			preload.on("progress", handleProgress);
			preload.on("complete", handleComplete);
			preload.on("fileload", handleFileLoad);
			preload.loadManifest(manifest, true);
		}
		function handleFileLoad(event) {
			console.log(event.item);
		}
		function handleProgress(event) {
			$('#loading-text').html(parseInt(event.loaded*100)+'%');
		}
		function handleComplete(event) {
			$('#loading-wrapper').hide(0);
			//initialize varibales
			// current_sound = createjs.Sound.play('sound_1');
			// current_sound.stop();
			// call main function
			templateCaller();
		}
		//initialize
		init();


	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
	*/
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */
 	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.delay(800).show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		switch(countNext){
			case 0:
				sound_player("sound_0");
				break;
			case 1:
			sound_player("sound_1");
				break;
			case 2:
				sound_player("sound_2");
			break;
			case 3:
				sound_player("sound_3");
			break;
			case 4:
			setTimeout(function () {
				sound_player("sound_4");
			},1000);
			break;
			case 5:
				sound_player("sound_5");
			break;
			case 6:
			setTimeout(function(){
				sound_player("sound_6");
			},1000);
			break;
			case 7:
				sound_player("sound_7");
			break;
			case 8:
				sound_player("sound_8");
			break;
			case 9:
				sound_player("sound_9");
			break;
			case 10:
			sound_player("sound_10");
			break;
				default:
				nav_button_controls(0);
				break;
		}
		function nav_button_controls(delay_ms){
			timeoutvar = setTimeout(function(){
				if(countNext==0){
					$nextBtn.show(0);
				} else if( countNext>0 && countNext == $total_page-1){
					$prevBtn.show(0);
					ole.footerNotificationHandler.pageEndSetNotification();
				} else{
					$prevBtn.show(0);
					$nextBtn.show(0);
				}
			},delay_ms);
		}
		function sound_player(sound_id){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
			current_sound.on("complete", function(){
				nav_button_controls(0);
				});
		}
	}


	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

	loadTimelineProgress($total_page, countNext + 1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
		total_page = content.length;
		templateCaller();
	// });

});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/

	 /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		CODE MODIFIED FOR THREE DIFFERENT HIGHTLIGHTS
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
		function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightstarttag2;
        var texthighlightstarttag3;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
              $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

              $(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
              (stylerulename2 = "parsedstring2") ;

              $(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
              (stylerulename3 = "parsedstring3") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
            texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
		/*=====  End of data highlight function  ======*/

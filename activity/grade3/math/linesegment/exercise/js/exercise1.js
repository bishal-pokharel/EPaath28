var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

Array.prototype.shufflearray = function(){
	var i = this.length, j, temp;
	while(--i > 0){
		j = Math.floor(Math.random() * (i+1));
		temp = this[j];
		this[j] = this[i];
		this[i] = temp;
	}
	return this;
};


var content=[
	//question 1 belna
	{
		uppertextblock: [
		{
			textdata : data.string.e1textq,
			textclass: 'question_asked'
		}],

		snapper: 'snapper',
		imageblock: [
		{
			imagestoshow: [
			{
				imgclass: "  ruler",
				imgsrc : imgpath + "ruler-cm-only.png"
			},
			{
				imgclass: "draggable obj_1",
				imgsrc : imgpath + "belna.png"
			}

			],
		}
		],

		digitblock : [{
			digitdata : '',
			digitclass : 'digit_box',
			scale : data.string.e1textcm,
			scaleclass : 'cm_text',
			buttondata : data.string.e1textcheck,
			buttonclass : 'check_button',
		}],
		correctanswer: '16'
	},
	//question 2 pen
	{
		uppertextblock: [
		{
			textdata : data.string.e1textq,
			textclass: 'question_asked'
		}],

		snapper: 'snapper',
		imageblock: [
		{
			imagestoshow: [
			{
				imgclass: "ruler",
				imgsrc : imgpath + "ruler-cm-only.png"
			},
			{
				imgclass: "draggable obj_2",
				imgsrc : imgpath + "pen.png"
			}

			],
		}
		],

		digitblock : [{
			digitdata : '',
			digitclass : 'digit_box',
			scale : data.string.e1textcm,
			scaleclass : 'cm_text',
			buttondata : data.string.e1textcheck,
			buttonclass : 'check_button',
		}],
		correctanswer: '12'
	},
	//question 3 flute
	{
		uppertextblock: [
		{
			textdata : data.string.e1textq,
			textclass: 'question_asked'
		}],

		snapper: 'snapper',
		imageblock: [
		{
			imagestoshow: [
			{
				imgclass: "ruler",
				imgsrc : imgpath + "ruler-cm-only.png"
			},
			{
				imgclass: "draggable obj_3",
				imgsrc : imgpath + "flute.png"
			}

			],
		}
		],

		digitblock : [{
			digitdata : '',
			digitclass : 'digit_box',
			scale : data.string.e1textcm,
			scaleclass : 'cm_text',
			buttondata : data.string.e1textcheck,
			buttonclass : 'check_button',
		}],
		correctanswer: '21'
	},
	//question 4 watch
	{
		uppertextblock: [
		{
			textdata : data.string.e1textq,
			textclass: 'question_asked'
		}],

		snapper: 'snapper',
		imageblock: [
		{
			imagestoshow: [
			{
				imgclass: "ruler",
				imgsrc : imgpath + "ruler-cm-only.png"
			},
			{
				imgclass: "draggable obj_4",
				imgsrc : imgpath + "watch.png"
			}

			],
		}
		],

		digitblock : [{
			digitdata : '',
			digitclass : 'digit_box',
			scale : data.string.e1textcm,
			scaleclass : 'cm_text',
			buttondata : data.string.e1textcheck,
			buttonclass : 'check_button',
		}],
		correctanswer: '20'
	},
	//question 5 rope
	{
		uppertextblock: [
		{
			textdata : data.string.e1textq,
			textclass: 'question_asked'
		}],

		snapper: 'snapper',
		imageblock: [
		{
			imagestoshow: [
			{
				imgclass: "ruler",
				imgsrc : imgpath + "ruler-cm-only.png"
			},
			{
				imgclass: "draggable obj_5",
				imgsrc : imgpath + "rope.png"
			}

			],
		}
		],

		digitblock : [{
			digitdata : '',
			digitclass : 'digit_box',
			scale : data.string.e1textcm,
			scaleclass : 'cm_text',
			buttondata : data.string.e1textcheck,
			buttonclass : 'check_button',
		}],
		correctanswer: '27'
	},
	//question 6 fish
	{
		uppertextblock: [
		{
			textdata : data.string.e1textq,
			textclass: 'question_asked'
		}],

		snapper: 'snapper',
		imageblock: [
		{
			imagestoshow: [
			{
				imgclass: "ruler",
				imgsrc : imgpath + "ruler-cm-only.png"
			},
			{
				imgclass: "draggable obj_6",
				imgsrc : imgpath + "fish.png"
			}

			],
		}
		],

		digitblock : [{
			digitdata : '',
			digitclass : 'digit_box',
			scale : data.string.e1textcm,
			scaleclass : 'cm_text',
			buttondata : data.string.e1textcheck,
			buttonclass : 'check_button',
		}],
		correctanswer: '12'
	},

	//question 7 spoon
	{
		uppertextblock: [
		{
			textdata : data.string.e1textq,
			textclass: 'question_asked'
		}],

		snapper: 'snapper',
		imageblock: [
		{
			imagestoshow: [
			{
				imgclass: "ruler",
				imgsrc : imgpath + "ruler-cm-only.png"
			},
			{
				imgclass: "draggable obj_7",
				imgsrc : imgpath + "spoon.png"
			}

			],
		}
		],

		digitblock : [{
			digitdata : '',
			digitclass : 'digit_box',
			scale : data.string.e1textcm,
			scaleclass : 'cm_text',
			buttondata : data.string.e1textcheck,
			buttonclass : 'check_button',
		}],
		correctanswer: '10'
	},

	//question 8 dragonfly
	{
		uppertextblock: [
		{
			textdata : data.string.e1textq,
			textclass: 'question_asked'
		}],

		snapper: 'snapper',
		imageblock: [
		{
			imagestoshow: [
			{
				imgclass: "ruler",
				imgsrc : imgpath + "ruler-cm-only.png"
			},
			{
				imgclass: "draggable obj_8",
				imgsrc : imgpath + "dragonfly.png"
			}

			],
		}
		],

		digitblock : [{
			digitdata : '',
			digitclass : 'digit_box',
			scale : data.string.e1textcm,
			scaleclass : 'cm_text',
			buttondata : data.string.e1textcheck,
			buttonclass : 'check_button',
		}],
		correctanswer: '4'
	},

	//question 9 matchstick
	{
		uppertextblock: [
		{
			textdata : data.string.e1textq,
			textclass: 'question_asked'
		}],

		snapper: 'snapper',
		imageblock: [
		{
			imagestoshow: [
			{
				imgclass: "ruler",
				imgsrc : imgpath + "ruler-cm-only.png"
			},
			{
				imgclass: "draggable obj_9",
				imgsrc : imgpath + "matchstick.png"
			}

			],
		}
		],

		digitblock : [{
			digitdata : '',
			digitclass : 'digit_box',
			scale : data.string.e1textcm,
			scaleclass : 'cm_text',
			buttondata : data.string.e1textcheck,
			buttonclass : 'check_button',
		}],
		correctanswer: '5'
	},
	//question 10 comb
	{
		uppertextblock: [
		{
			textdata : data.string.e1textq,
			textclass: 'question_asked'
		}],

		snapper: 'snapper',
		imageblock: [
		{
			imagestoshow: [
			{
				imgclass: "ruler",
				imgsrc : imgpath + "ruler-cm-only.png"
			},
			{
				imgclass: "draggable obj_10",
				imgsrc : imgpath + "comb.png"
			}

			],
		}
		],

		digitblock : [{
			digitdata : '',
			digitclass : 'digit_box',
			scale : data.string.e1textcm,
			scaleclass : 'cm_text',
			buttondata : data.string.e1textcheck,
			buttonclass : 'check_button',
		}],
		correctanswer: '9'
	},
	//question 11 feather
	{
		uppertextblock: [
		{
			textdata : data.string.e1textq,
			textclass: 'question_asked'
		}],

		snapper: 'snapper',
		imageblock: [
		{
			imagestoshow: [
			{
				imgclass: "ruler",
				imgsrc : imgpath + "ruler-cm-only.png"
			},
			{
				imgclass: "draggable obj_11",
				imgsrc : imgpath + "feather.png"
			}

			],
		}
		],

		digitblock : [{
			digitdata : '',
			digitclass : 'digit_box',
			scale : data.string.e1textcm,
			scaleclass : 'cm_text',
			buttondata : data.string.e1textcheck,
			buttonclass : 'check_button',
		}],
		correctanswer: '13'
	},
	//question 12 nail
	{
		uppertextblock: [
		{
			textdata : data.string.e1textq,
			textclass: 'question_asked'
		}],

		snapper: 'snapper',
		imageblock: [
		{
			imagestoshow: [
			{
				imgclass: "ruler",
				imgsrc : imgpath + "ruler-cm-only.png"
			},
			{
				imgclass: "draggable obj_12",
				imgsrc : imgpath + "nail.png"
			}

			],
		}
		],

		digitblock : [{
			digitdata : '',
			digitclass : 'digit_box',
			scale : data.string.e1textcm,
			scaleclass : 'cm_text',
			buttondata : data.string.e1textcheck,
			buttonclass : 'check_button',
		}],
		correctanswer: '6'
	}
	];

	// content.shufflearray();

	$(function () {
		var $board    = $('.board');
		var $nextBtn = $("#activity-page-next-btn-enabled");
		var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
		var countNext = 0;

		var $total_page = 10;


		var preload;
		var timeoutvar = null;
		var current_sound;


		function init() {
			//specify type otherwise it will load assests as XHR
			manifest = [
				// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
				// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
				//   ,
				//images
				{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
				{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},


				//sounds
				{id: "instruction", src: soundAsset+"instruction.ogg"},
				{id: "sound_0", src: soundAsset+"S1_P1.ogg"},

			];
			preload = new createjs.LoadQueue(false);
			preload.installPlugin(createjs.Sound);//for registering sounds
			preload.on("progress", handleProgress);
			preload.on("complete", handleComplete);
			preload.on("fileload", handleFileLoad);
			preload.loadManifest(manifest, true);
		}
		function handleFileLoad(event) {
			console.log(event.item);
		}
		function handleProgress(event) {
			$('#loading-text').html(parseInt(event.loaded*100)+'%');
		}
		function handleComplete(event) {
			$('#loading-wrapper').hide(0);
			//initialize varibales
			// current_sound = createjs.Sound.play('sound_1');
			// current_sound.stop();
			// call main function
			templateCaller();
		}
		//initialize
		init();


		Handlebars.registerPartial("digitcontent", $("#digitcontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
	 }

	 var score = 0;
	var testin = new EggTemplate();

	 	//eggTemplate.eggMove(countNext);
 	testin.init(10);
	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);

	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);
		// if(countNext==0){
		// 	sound_player("sound_0");
		// }
	 	/*generate question no at the beginning of question*/
		$(".imageblock").append("<img class='corincor right' src='"+preload.getResult("correct").src+"' />");
		$(".imageblock").append("<img class='corincor wrong' src='"+preload.getResult("incorrect").src+"' />");
	 	switch(countNext) {
	 		default:
			sound_player('instruction');
	 		integer_box('.digit_box', 2, '.check_button');
	 		$(".draggable").draggable({
	 			containment : ".generalTemplateblock",
	 			cursor : "grabbing",
	 			zIndex: 1000,
	 			snap: ".snapper",
	 			snapTolerance: 50,
	 			snapMode: "inner",
	 		});

	 		if(snap=true)
	 		break;
	 	}

	 	/*random scoreboard eggs*/
	 	var ansClicked = false;
	 	var wrngClicked = false;

	 	$(".check_button").click(function(){
	 		var answer = $('.digit_box').val();
	 		var correct_answer = content[countNext].correctanswer;
	 		if(answer==''){
	 			return true;
	 		} else if(answer == correct_answer){
	 			$(this).removeClass('incorrect_answer');
	 			$(this).addClass('correct_answer');
	 			if(wrngClicked == false){
	 				testin.update(true);
	 			}
	 			play_correct_incorrect_sound(1);
				$(".wrong").hide(0);
				$(".right").show(0);
	 			ansClicked = true;
	 			$(".check_button").css('pointer-events','none');
	 			$(".digit_box").prop('disabled', true);
	 			if(countNext < 10){
	 				$nextBtn.show(0);
	 			}
	 		} else {
	 			testin.update(false);
	 			$(this).addClass('incorrect_answer');
	 			wrngClicked = true;
	 			play_correct_incorrect_sound(0);
				$(".wrong").show(0);
	 		}
	 	});

	 	/*for randomizing the options*/
		/*var parent = $(".optionsdiv_img");
		var divs = parent.children();
			 while (divs.length) {
			        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			    }*/

			}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();

}

			function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators

	}

	// first call to template caller
	// templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		if(countNext<10)
			templateCaller();
		else
			$nextBtn.hide(0);

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	 function integer_box(input_class, max_number, button_class) {
	 	$(input_class).keydown(function(event){
	 		var charCode = (event.which) ? event.which : event.keyCode;
	 		/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys,, 46=> del */
	 		if(charCode === 13 && button_class!=null) {
	 			$(button_class).trigger("click");
	 		}
	 		var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
			if (!condition) {
				return true;
			}
    		//check . and 0-9 separately after checking arrow and other keys
    		if((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105)){
    			return false;
    		}
    		//check max no of allowed digits
    		if (String(event.target.value).length >= max_number) {
    			return false;
    		}
    		return true;
    	});
	 }

	/*  function used to generate numbers from 1 to range in random order and store it in array
	 *	This function pushes all the numbers upto range -1 in an array, generates the random no upto range-1
	 *  moves the randomly generated index from that array to new array and repeats the process till every number < range is generated	*/
	 function sandy_random_display(range){
	 	var random_arr = [];
	 	var return_arr = [];
	 	var random_max = range;
	 	for(var i=1; i<=range; i++){
	 		random_arr.push(i);
	 	}

	 	while(random_max)
	 	{
	 		var index = ole.getRandom(1, random_max-1, 0);
	 		return_arr.push(random_arr[index]);
	 		random_arr.splice((index),1);
	 		random_max--;
	 	}
	 	return return_arr;
	 }
	 /*=====  End of Templates Controller Block  ======*/
	});

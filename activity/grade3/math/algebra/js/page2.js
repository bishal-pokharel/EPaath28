var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
		// slide0
		{
			contentnocenteradjust: true,
			uppertextblock:[
			{
				textclass: "covertext2",
				textdata: data.string.diytext
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "cover",
					imgid : 'bg0',
					imgsrc: ""
				}
			]
		}]
	},
	// slide1
	{
		uppertextblock:[
			{
					textclass: "uptext",
					textdata: data.string.p2text1
			}
		],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'bg2',
				imgsrc: ""
			}
		]
	}],
	flexblockcontainers: [
		{
			flexblockadditionalclass: "flexcontainerblock100",
			flexblock:[
				{
					flexboxcolumnclass: "upcolumn",
					flexblockcolumn:[{
						flexboxrowclass :"countdiv count1",
					},{
						flexboxrowclass :"countdiv count2",
					},{
						flexboxrowclass :"countdiv count3",
					},{
						flexboxrowclass :"countdiv count4",
					},{
						flexboxrowclass :"countdiv count5",
					},{
						flexboxrowclass :"countdiv count6",
					},{
						flexboxrowclass :"countdiv count7",
					},{
						flexboxrowclass :"countdiv count8",
					},{
						flexboxrowclass :"countdiv count9",
					},{
						flexboxrowclass :"countdiv count10",
					},{
						flexboxrowclass :"countdiv count11",
					},{
						flexboxrowclass :"countdiv count12",
					}
					]
				},
				{
					flexboxcolumnclass: "upcolumn",
					flexblockcolumn:[{
						flexboxrowclass :"countdiv mark1",
					},{
						flexboxrowclass :"countdiv mark2",
					},{
						flexboxrowclass :"countdiv mark3",
					},{
						flexboxrowclass :"countdiv mark4",
					},{
						flexboxrowclass :"countdiv mark5",
					},{
						flexboxrowclass :"countdiv mark6",
					},{
						flexboxrowclass :"countdiv mark7",
					},{
						flexboxrowclass :"countdiv mark8",
					},{
						flexboxrowclass :"countdiv mark9",
					},{
						flexboxrowclass :"countdiv mark10",
					},{
						flexboxrowclass :"countdiv mark11",
					},{
						flexboxrowclass :"countdiv mark12",
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box1",
					},{
						flexboxrowclass :"rownormal bext1",
						textdata: "1"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box2",
					},{
						flexboxrowclass :"rownormal bext2",
						textdata: "2"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box3",
					},{
						flexboxrowclass :"rownormal bext3",
						textdata: "3"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box4",
					},{
						flexboxrowclass :"rownormal bext4",
						textdata: "4"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box5",
					},{
						flexboxrowclass :"rownormal bext5",
						textdata: "5"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box6",
					},{
						flexboxrowclass :"rownormal bext6",
						textdata: "6"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box7",
					},{
						flexboxrowclass :"rownormal bext7",
						textdata: "7"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box8",
					},{
						flexboxrowclass :"rownormal bext8",
						textdata: "8"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box9",
					},{
						flexboxrowclass :"rownormal bext9",
						textdata: "9"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box10",
					},{
						flexboxrowclass :"rownormal bext10",
						textdata: "10"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box11",
					},{
						flexboxrowclass :"rownormal bext11",
						textdata: "11"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box11",
					},{
						flexboxrowclass :"rownormal bext11",
						textdata: "12"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box11",
					},{
						flexboxrowclass :"rownormal bext11",
						textdata: "13"
					}
					]
				}

			]
		}
	]
	},
	// slide2
	{
	uppertextblock:[
		{
				textclass: "uptext",
				textdata: data.string.p2text2
		}
	],
	submit: data.string.submit,
	formblock1:[
		{
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'bg2',
				imgsrc: ""
			}
		]
	}],
	flexblockcontainers: [
		{
			flexblockadditionalclass: "flexcontainerblock100",
			flexblock:[
				{
					flexboxcolumnclass: "upcolumn",
					flexblockcolumn:[{
						flexboxrowclass :"countdiv count1",
					},{
						flexboxrowclass :"countdiv count2",
					},{
						flexboxrowclass :"countdiv count3",
					},{
						flexboxrowclass :"countdiv count4",
					},{
						flexboxrowclass :"countdiv count5",
					},{
						flexboxrowclass :"countdiv count6",
					},{
						flexboxrowclass :"countdiv count7",
					},{
						flexboxrowclass :"countdiv count8",
					},{
						flexboxrowclass :"countdiv count9",
					},{
						flexboxrowclass :"countdiv count10",
					},{
						flexboxrowclass :"countdiv count11",
					},{
						flexboxrowclass :"countdiv count12",
					}
					]
				},
				{
					flexboxcolumnclass: "upcolumn",
					flexblockcolumn:[{
						flexboxrowclass :"countdiv mark1",
					},{
						flexboxrowclass :"countdiv mark2",
					},{
						flexboxrowclass :"countdiv mark3",
					},{
						flexboxrowclass :"countdiv mark4",
					},{
						flexboxrowclass :"countdiv mark5",
					},{
						flexboxrowclass :"countdiv mark6",
					},{
						flexboxrowclass :"countdiv mark7",
					},{
						flexboxrowclass :"countdiv mark8",
					},{
						flexboxrowclass :"countdiv mark9",
					},{
						flexboxrowclass :"countdiv mark10",
					},{
						flexboxrowclass :"countdiv mark11",
					},{
						flexboxrowclass :"countdiv mark12",
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box1",
					},{
						flexboxrowclass :"rownormal bext1",
						textdata: "1"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box2",
					},{
						flexboxrowclass :"rownormal bext2",
						textdata: "2"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box3",
					},{
						flexboxrowclass :"rownormal bext3",
						textdata: "3"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box4",
					},{
						flexboxrowclass :"rownormal bext4",
						textdata: "4"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box5",
					},{
						flexboxrowclass :"rownormal bext5",
						textdata: "5"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box6",
					},{
						flexboxrowclass :"rownormal bext6",
						textdata: "6"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box7",
					},{
						flexboxrowclass :"rownormal bext7",
						textdata: "7"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box8",
					},{
						flexboxrowclass :"rownormal bext8",
						textdata: "8"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box9",
					},{
						flexboxrowclass :"rownormal bext9",
						textdata: "9"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box10",
					},{
						flexboxrowclass :"rownormal bext10",
						textdata: "10"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box11",
					},{
						flexboxrowclass :"rownormal bext11",
						textdata: "11"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box11",
					},{
						flexboxrowclass :"rownormal bext11",
						textdata: "12"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box11",
					},{
						flexboxrowclass :"rownormal bext11",
						textdata: "13"
					}
					]
				}

			]
		}
	]
	},
	// slide1
	{
		uppertextblock:[
			{
					textclass: "uptext",
					textdata: data.string.p1text12
			}
		],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'bg2',
				imgsrc: ""
			}
		]
	}],
	flexblockcontainers: [
		{
			flexblockadditionalclass: "flexcontainerblock100",
			flexblock:[
				{
					flexboxcolumnclass: "upcolumn",
					flexblockcolumn:[{
						flexboxrowclass :"countdiv count1",
					},{
						flexboxrowclass :"countdiv count2",
					},{
						flexboxrowclass :"countdiv count3",
					},{
						flexboxrowclass :"countdiv count4",
					},{
						flexboxrowclass :"countdiv count5",
					},{
						flexboxrowclass :"countdiv count6",
					},{
						flexboxrowclass :"countdiv count7",
					},{
						flexboxrowclass :"countdiv count8",
					},{
						flexboxrowclass :"countdiv count9",
					},{
						flexboxrowclass :"countdiv count10",
					},{
						flexboxrowclass :"countdiv count11",
					},{
						flexboxrowclass :"countdiv count12",
					}
					]
				},
				{
					flexboxcolumnclass: "upcolumn",
					flexblockcolumn:[{
						flexboxrowclass :"countdiv mark1",
					},{
						flexboxrowclass :"countdiv mark2",
					},{
						flexboxrowclass :"countdiv mark3",
					},{
						flexboxrowclass :"countdiv mark4",
					},{
						flexboxrowclass :"countdiv mark5",
					},{
						flexboxrowclass :"countdiv mark6",
					},{
						flexboxrowclass :"countdiv mark7",
					},{
						flexboxrowclass :"countdiv mark8",
					},{
						flexboxrowclass :"countdiv mark9",
					},{
						flexboxrowclass :"countdiv mark10",
					},{
						flexboxrowclass :"countdiv mark11",
					},{
						flexboxrowclass :"countdiv mark12",
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box1",
					},{
						flexboxrowclass :"rownormal bext1",
						textdata: "1"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box2",
					},{
						flexboxrowclass :"rownormal bext2",
						textdata: "2"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box3",
					},{
						flexboxrowclass :"rownormal bext3",
						textdata: "3"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box4",
					},{
						flexboxrowclass :"rownormal bext4",
						textdata: "4"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box5",
					},{
						flexboxrowclass :"rownormal bext5",
						textdata: "5"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box6",
					},{
						flexboxrowclass :"rownormal bext6",
						textdata: "6"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box7",
					},{
						flexboxrowclass :"rownormal bext7",
						textdata: "7"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box8",
					},{
						flexboxrowclass :"rownormal bext8",
						textdata: "8"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box9",
					},{
						flexboxrowclass :"rownormal bext9",
						textdata: "9"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box10",
					},{
						flexboxrowclass :"rownormal bext10",
						textdata: "10"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box11",
					},{
						flexboxrowclass :"rownormal bext11",
						textdata: "11"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box11",
					},{
						flexboxrowclass :"rownormal bext11",
						textdata: "12"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box11",
					},{
						flexboxrowclass :"rownormal bext11",
						textdata: "13"
					}
					]
				}

			]
		}
	]
	},
	// slide2
	{
	uppertextblock:[
		{
				textclass: "uptext",
				textdata: data.string.p2text2
		}
	],
	submit: data.string.submit,
	formblock2:[
		{

		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'bg2',
				imgsrc: ""
			}
		]
	}],
	flexblockcontainers: [
		{
			flexblockadditionalclass: "flexcontainerblock100",
			flexblock:[
				{
					flexboxcolumnclass: "upcolumn",
					flexblockcolumn:[{
						flexboxrowclass :"countdiv count1",
					},{
						flexboxrowclass :"countdiv count2",
					},{
						flexboxrowclass :"countdiv count3",
					},{
						flexboxrowclass :"countdiv count4",
					},{
						flexboxrowclass :"countdiv count5",
					},{
						flexboxrowclass :"countdiv count6",
					},{
						flexboxrowclass :"countdiv count7",
					},{
						flexboxrowclass :"countdiv count8",
					},{
						flexboxrowclass :"countdiv count9",
					},{
						flexboxrowclass :"countdiv count10",
					},{
						flexboxrowclass :"countdiv count11",
					},{
						flexboxrowclass :"countdiv count12",
					}
					]
				},
				{
					flexboxcolumnclass: "upcolumn",
					flexblockcolumn:[{
						flexboxrowclass :"countdiv mark1",
					},{
						flexboxrowclass :"countdiv mark2",
					},{
						flexboxrowclass :"countdiv mark3",
					},{
						flexboxrowclass :"countdiv mark4",
					},{
						flexboxrowclass :"countdiv mark5",
					},{
						flexboxrowclass :"countdiv mark6",
					},{
						flexboxrowclass :"countdiv mark7",
					},{
						flexboxrowclass :"countdiv mark8",
					},{
						flexboxrowclass :"countdiv mark9",
					},{
						flexboxrowclass :"countdiv mark10",
					},{
						flexboxrowclass :"countdiv mark11",
					},{
						flexboxrowclass :"countdiv mark12",
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box1",
					},{
						flexboxrowclass :"rownormal bext1",
						textdata: "1"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box2",
					},{
						flexboxrowclass :"rownormal bext2",
						textdata: "2"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box3",
					},{
						flexboxrowclass :"rownormal bext3",
						textdata: "3"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box4",
					},{
						flexboxrowclass :"rownormal bext4",
						textdata: "4"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box5",
					},{
						flexboxrowclass :"rownormal bext5",
						textdata: "5"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box6",
					},{
						flexboxrowclass :"rownormal bext6",
						textdata: "6"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box7",
					},{
						flexboxrowclass :"rownormal bext7",
						textdata: "7"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box8",
					},{
						flexboxrowclass :"rownormal bext8",
						textdata: "8"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box9",
					},{
						flexboxrowclass :"rownormal bext9",
						textdata: "9"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box10",
					},{
						flexboxrowclass :"rownormal bext10",
						textdata: "10"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box11",
					},{
						flexboxrowclass :"rownormal bext11",
						textdata: "11"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box11",
					},{
						flexboxrowclass :"rownormal bext11",
						textdata: "12"
					}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal box11",
					},{
						flexboxrowclass :"rownormal bext11",
						textdata: "13"
					}
					]
				}

			]
		}
	]
	},
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var flexCopier;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;
	var enteredNo;
	var jumpval;
	var tryFlag = false;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg0", src: imgpath+"bg_diy02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg1", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg2", src: imgpath+"bg03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "frog", src: imgpath+"frog.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sad", src: imgpath+"frog_sad.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "jump", src: soundAsset+"jump.ogg"},
			{id: "success", src: soundAsset+"success.ogg"},
			{id: "fail", src: soundAsset+"fail.ogg"},
			{id: "s2_p2", src: soundAsset+"s2_p2.ogg"},
			{id: "s2_p3_1", src: soundAsset+"s2_p3_1.ogg"},
			{id: "s2_p3", src: soundAsset+"s2_p3.ogg"},
			{id: "s2_p4", src: soundAsset+"s2_p4.ogg"},
			{id: "s2_p5_1", src: soundAsset+"s2_p5_1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	// $nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.hide(0);
 	$prevBtn.hide(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	// islastpageflag ?
 	// ole.footerNotificationHandler.lessonEndSetNotification() :
 	// ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);
		put_speechbox_image2(content, countNext);

		var startleft = 3.84615384;
		var currentstep = 1;
		var countkeeper = 1;

		var arraypath = [];
		for(i=0;i<13;i++){
			arraypath.push([54, startleft]);
			startleft = startleft + 7.69230769;
		}
		console.log(arraypath);

		switch(countNext){
			case 0:
			// sound_player("sound_1");
			play_diy_audio();
			nav_button_controls(1500);
			break;
			case 1:
				sound_player("s2_p"+(countNext+1),1);
				//$("#gattifrog").attr("src", preload.getResult("frog").src);
				$("#gattifrog").attr("src", preload.getResult("frog").src);
				$( "#frogcontainer" ).css({
					left: arraypath[2][1] + "%",
					top: arraypath[2][0] + "%",
				});
				$(".column").eq(2).css({
					"border":"0.3em dashed yellow"
				 });
				 $(".column").eq(9).css({
	 				"border":"0.3em dashed yellow"
	 			 });
				$(".box1, .box2, .box3, .box4, .box5, .box6, .box7, .box8, .box9, .box10, .box11").css("background", "url("+ imgpath + "flower.png)").addClass("flexback");
			break;
			case 2:
			sound_player("s2_p"+(countNext+1),0);
			$(".column").eq(2).css({
				"border":"0.3em dashed yellow"
			 });
			 $(".column").eq(9).css({
				"border":"0.3em dashed yellow"
			 });
			$("#gattifrog").attr("src", preload.getResult("frog").src);
				$(".box1, .box2, .box3, .box4, .box5, .box6, .box7, .box8, .box9, .box10, .box11").css("background", "url("+ imgpath + "flower.png)").addClass("flexback");
				$( "#frogcontainer" ).css({
					left: arraypath[2][1] + "%",
					top: arraypath[2][0] + "%",
				});

				$(".submit_button").click(function(){
					enteredNo = parseInt($(".inbox").val());
					if($(".inbox").val().length != 0 && $(this).hasClass("enabled") && tryFlag == false){
						if(enteredNo==0){
							$(".uptext").html(data.string.p4text3_2_part1 + enteredNo + data.string.p4text3_3_part2 + " 3 " + data.string.p4text3_3_part3).addClass("wrongback")
							sound_player("fail", 0);
						}else{
							$(".inbox").prop('disabled', true);
							$(this).removeClass("enabled").addClass("disabled");
							jumpval = enteredNo + 2;
							console.log("here");
							movefrog(3, jumpval);
						}
					}
					else if($(this).hasClass("enabled") && tryFlag == true){
						tryFlag = false;
						templateCaller();
					}
				});

				$(".inbox").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 48 && e.keyCode <= 57)) {
                 // let it happen, don't do anything
                 return;
        }
				if (e.keyCode == 13) {
			    $('.submit_button').click();
			  }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 49 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
			break;
			case 3:
			sound_player("s2_p"+(countNext+1),1);
				//$("#gattifrog").attr("src", preload.getResult("frog").src);
				$("#gattifrog").attr("src", preload.getResult("frog").src);
				$( "#frogcontainer" ).css({
					left: arraypath[3][1] + "%",
					top: arraypath[3][0] + "%",
				});
				$(".column").eq(3).css({
					"border":"0.3em dashed yellow"
				 });
				 $(".column").eq(12).css({
	 				"border":"0.3em dashed yellow"
	 			 });
				$(".box1, .box2, .box3, .box4, .box5, .box6, .box7, .box8, .box9, .box10, .box11").css("background", "url("+ imgpath + "flower.png)").addClass("flexback");
			break;
			case 4:
			sound_player("s2_p3",0);
				$(".column").eq(3).css({
					"border":"0.3em dashed yellow"
				 });
				 $(".column").eq(12).css({
					"border":"0.3em dashed yellow"
				 });
				$("#gattifrog").attr("src", preload.getResult("frog").src);
					$(".box1, .box2, .box3, .box4, .box5, .box6, .box7, .box8, .box9, .box10, .box11").css("background", "url("+ imgpath + "flower.png)").addClass("flexback");
					$( "#frogcontainer" ).css({
						left: arraypath[3][1] + "%",
						top: arraypath[3][0] + "%",
					});

					$(".submit_button").click(function(){
						enteredNo = parseInt($(".inbox").val());
						if($(".inbox").val().length != 0 && $(this).hasClass("enabled") && tryFlag == false){
							if(enteredNo==0){
								$(".uptext").html(data.string.p4text3_2_part1 + enteredNo + data.string.p4text3_3_part2 + " 3 " + data.string.p4text3_3_part3).addClass("wrongback");
								sound_player("fail", 0);
							}else{
							$(".inbox").prop('disabled', true);
							$(this).removeClass("enabled").addClass("disabled");
							jumpval = enteredNo + 3;
							console.log("here");
							movefrog(4, jumpval);
						}
						}
						else if($(this).hasClass("enabled") && tryFlag == true){
							tryFlag = false;
							templateCaller();
						}
					});

					$(".inbox").keydown(function (e) {
	        // Allow: backspace, delete, tab, escape, enter and .
	        if ($.inArray(e.keyCode, [46, 8, 9, 27, 110, 190]) !== -1 ||
	             // Allow: Ctrl+A, Command+A
	            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
	             // Allow: home, end, left, right, down, up
	            (e.keyCode >= 48 && e.keyCode <= 57)) {
	                 // let it happen, don't do anything
	                 return;
	        }
					if (e.keyCode == 13) {
				    $('.submit_button').click();
				  }
	        // Ensure that it is a number and stop the keypress
	        if ((e.shiftKey || (e.keyCode < 49 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
	            e.preventDefault();
	        }
	    	});
			break;
		}

		function movefrog(start, end){
			sound_player("jump", 0);
			$( "#frogcontainer" ).animate({
				left: (arraypath[start][1] + arraypath[start-1][1])/2 + "%",
				top: "11%",
			},500, function(){
				$(".count"+start+" > p").text(currentstep).addClass("count-visi");
				$(".mark"+start).css("background","url("+ imgpath + "curbe_line.png)").addClass("flexback");
				// start++;
				$( "#frogcontainer" ).animate({
					left: arraypath[start][1] + "%",
					top: arraypath[start][0] + "%",
				},500, function(){
					if(start != end){
						start++;
						currentstep++;
						setTimeout(function(){
							movefrog(start, end);
						}, 500);
					}
					else{
						if(countNext == 2 && jumpval == 9){
							corCall();
							sound_player("s2_p"+(countNext+1)+"_1",1);
						}
						else if(countNext == 4 && jumpval == 12){
								corCall();
								sound_player("s2_p"+(countNext+1)+"_1",1);
							}
						else{
							if(countNext==2){
								$(".inbox").val()>7?incCall(true):incCall(false);
							}else{
								$(".inbox").val()>9?incCall(true):incCall(false);
							}
						}
							// incCall();
					}
				});
			});
		}
	}
	function incCall(gtst){
		tryFlag = true;
		sound_player("fail", 0);
		$("#gattifrog").attr("src", preload.getResult("sad").src);
		// $(".uptext").html(data.string.p2text3part1 + enteredNo + data.string.p2text3part2 + (jumpval+1) + data.string.p2text3part3 ).addClass("wrongback");
		gtst?$(".uptext").html(data.string.p4text3_2_part1 + enteredNo + data.string.p4text3_2_part2 + (jumpval+1) + data.string.p4text3_2_part3).addClass("wrongback"):
				$(".uptext").html(data.string.p4text3_2_part1 + enteredNo + data.string.p4text3_3_part2 + (jumpval+1) + data.string.p4text3_3_part3).addClass("wrongback");
		$(".submit_button").html(data.string.try).removeClass("disabled").addClass("trybtn enabled");
		$(".column").eq(jumpval).css({
			"border":"0.3em dashed red"
		 });
	}
	function corCall(){
		$("#gattifrog").addClass("frogsuccess");
		sound_player("success", 1);
		tryFlag = true;
		countNext==2?$(".uptext").html(data.string.p2text4part1 + enteredNo + data.string.p2text4part2Sec + "3" + data.string.p2text4part3Sec ).addClass("rightback"):
		$(".uptext").html(data.string.p2text4part1 + enteredNo + data.string.p2text4part2 + "4" + data.string.p2text4part3 ).addClass("rightback");
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls():"";
		});
	}

	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2);
		});

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
function put_speechbox_image2(content, count){
	if(content[count].hasOwnProperty('flexblockcontainers')){
			var speechboxParent = content[count].flexblockcontainers[0];
		if(speechboxParent.hasOwnProperty('speechbox')){
				var speechbox = speechboxParent.speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
	}
}

	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0 || countNext == 1 || countNext == 3)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

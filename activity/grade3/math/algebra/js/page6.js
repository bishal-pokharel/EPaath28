var imgpath = $ref + "/images/page03/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var imgpath1 = $ref + "/images/";

var content=[
		//slide10
		{
				imageblock:[{
					imagestoshow:[{
						imgid:'diy_bg',
						imgclass:'bg_full'
					}]
				}],
				uppertextblock:[{
					textdata:data.string.diytext,
					textclass:'diytext'
				}]
	},
		// slide1
		{
			contentblockadditionalclass:'bluebg',
			marbeldiv:[
				{
					imgid:'marble01',
					imgclass:'marbles1'
				},{
					imgid:'marble02',
					imgclass:'marbles2'
				},{
					imgid:'marble03',
					imgclass:'marbles3'
				},{
					imgid:'marble04',
					imgclass:'marbles4'
				},{
					imgid:'marble05',
					imgclass:'marbles5'
				},{
					imgid:'marble06',
					imgclass:'marbles6'
				},{
					imgid:'marble07',
					imgclass:'marbles7'
				},{
					imgid:'marble08',
					imgclass:'marbles8'
				},{
					imgid:'marble09',
					imgclass:'marbles9'
				},{
					imgid:'marble10',
					imgclass:'marbles10'
				},{
					imgid:'marble11',
					imgclass:'marbles11'
				},{
					imgid:'marble02',
					imgclass:'marbles12'
				},{
					imgid:'marble03',
					imgclass:'marbles13'
				},{
					imgid:'marble04',
					imgclass:'marbles14'
				},{
					imgid:'marble05',
					imgclass:'marbles15'
				},{
					imgid:'marble06',
					imgclass:'marbles16'
				},{
					imgid:'marble07',
					imgclass:'marbles17'
				},{
					imgid:'marble08',
					imgclass:'marbles18'
				},{
					imgid:'marble09',
					imgclass:'marbles19'
				},{
					imgid:'marble10',
					imgclass:'marbles20'
				},],
				marbeldivother:[
					{
						imgid:'marble_emptied',
						imgclass:'marbles1a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles2a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles3a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles4a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles5a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles6a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles7a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles8a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles9a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles10a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles11a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles12a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles13a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles14a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles15a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles16a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles17a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles18a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles19a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles20a'
					},],
				imageblock:[{
					imagestoshow:[{
						imgid:'boy_talking',
						imgclass:'left_boy'
					},
					{
						imgid:'ten_marble',
						imgclass:'jar ten_marble'
					},
					{
						imgid:'empty_jar',
						imgclass:'jar empty_jar'
					}
					]
				}],
				uppertextblock:[{
					textdata:data.string.p6text1,
					textclass:'left_dialogue template-dialougebox2-top-white'
				},
				{
					textdata:data.string.p6text2,
					textclass:'thequestion'
				},
				{
					textdata:data.string.p6text3,
					textclass:'submit_button'
				},]
		},

		// slide2
		{
			contentblockadditionalclass:'bluebg',
			marbeldiv:[
				{
					imgid:'marble01',
					imgclass:'marbles1'
				},{
					imgid:'marble02',
					imgclass:'marbles2'
				},{
					imgid:'marble03',
					imgclass:'marbles3'
				},{
					imgid:'marble04',
					imgclass:'marbles4'
				},{
					imgid:'marble05',
					imgclass:'marbles5'
				},{
					imgid:'marble06',
					imgclass:'marbles6'
				},{
					imgid:'marble07',
					imgclass:'marbles7'
				},{
					imgid:'marble08',
					imgclass:'marbles8'
				},{
					imgid:'marble09',
					imgclass:'marbles9'
				},{
					imgid:'marble10',
					imgclass:'marbles10'
				},{
					imgid:'marble11',
					imgclass:'marbles11'
				},{
					imgid:'marble02',
					imgclass:'marbles12'
				},{
					imgid:'marble03',
					imgclass:'marbles13'
				},{
					imgid:'marble04',
					imgclass:'marbles14'
				},{
					imgid:'marble05',
					imgclass:'marbles15'
				},{
					imgid:'marble06',
					imgclass:'marbles16'
				},{
					imgid:'marble07',
					imgclass:'marbles17'
				},{
					imgid:'marble08',
					imgclass:'marbles18'
				},{
					imgid:'marble09',
					imgclass:'marbles19'
				},{
					imgid:'marble10',
					imgclass:'marbles20'
				},],
				marbeldivother:[
					{
						imgid:'marble_emptied',
						imgclass:'marbles1a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles2a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles3a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles4a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles5a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles6a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles7a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles8a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles9a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles10a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles11a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles12a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles13a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles14a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles15a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles16a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles17a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles18a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles19a'
					},{
						imgid:'marble_emptied',
						imgclass:'marbles20a'
					},],
				imageblock:[{
					imagestoshow:[{
						imgid:'boy_talking',
						imgclass:'left_boy'
					},
					{
						imgid:'ten_marble',
						imgclass:'jar ten_marble'
					},
					{
						imgid:'empty_jar',
						imgclass:'jar empty_jar'
					}
					]
				}],
				uppertextblock:[{
					textdata:data.string.p6text1,
					textclass:'left_dialogue template-dialougebox2-top-white'
				},
				{
					textdata:data.string.p6text4,
					textclass:'thequestion'
				},
				{
					textdata:data.string.p6text3,
					textclass:'submit_button'
				},]
		},



];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "marble01", src: imgpath+"marble01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble02", src: imgpath+"marble02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble03", src: imgpath+"marble03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble04", src: imgpath+"marble04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble05", src: imgpath+"marble05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble06", src: imgpath+"marble06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble07", src: imgpath+"marble07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble08", src: imgpath+"marble08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble09", src: imgpath+"marble09.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble10", src: imgpath+"marble10.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble11", src: imgpath+"marble11.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy_talking", src: imgpath+"prem02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble_emptied", src: imgpath+"marble.png", type: createjs.AbstractLoader.IMAGE},
			{id: "diy_bg", src: imgpath+"a_11.png", type: createjs.AbstractLoader.IMAGE},
			{id: "empty_jar", src: imgpath+"empty_jar.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ten_marble", src: imgpath+"ten_marble.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes

			// sounds
			{id: "sound_1", src: soundAsset+"success.ogg"},
			{id: "sound_2", src: soundAsset+"fail.ogg"},
			{id: "s6_p2_ins", src: soundAsset+"s6_p2_ins.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_image1(content, countNext);
		put_image2(content, countNext);
		// marblediv_image(content, countNext);
		$('.submit_button').hide(0);
		$(".inputted_number").keydown(function (e) {
			$('.submit_button').show(0);
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 110, 190]) !== -1 ||
				 // Allow: Ctrl+A, Command+A
				(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
				 // Allow: home, end, left, right, down, up
				(e.keyCode >= 48 && e.keyCode <= 57)) {
						 // let it happen, don't do anything
						 return;
		}
		if (e.keyCode == 13) {
			$('.submit_button').click();
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 49 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
		}
		});

		vocabcontroller.findwords(countNext);
		switch (countNext) {
			case 0:
				play_diy_audio();
					nav_button_controls(1500);
			break;
			case 1:
				sound_play_click("s6_p2_ins");
					ole.footerNotificationHandler.hideNotification();
					$nextBtn.hide(0);
					$('.submit_button').click(function(){
						var the_number = parseInt($(".inputted_number").val());
						if(the_number==10){
							$('.thequestion').css({'pointer-events':'none'});
							for (var i = 1; i < the_number+1; i++) {
								$('.marbles'+i).animate({"top": "70%",
						    "left": "19%",
						    "opacity": "0"},2000);
							}
							setTimeout(function(){
								$('.empty_jar').animate({'opacity':'0'},500);
								nav_button_controls(500);
							},1800);
							var $this = $('.submit_button');
							var position = $this.position();
							$this.css('pointer-events','none');
							$('.incorrect_icon').hide(0);
							var width = $this.width();
							var height = $this.height();
							var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
							var centerY = ((position.top + height)*100)/$board.height()+'%';
							$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-407%,-281%)" src="'+imgpath1 +'correct.png" />').insertAfter('.inputted_number');
							$this.css({"background":"rgb(190,214,47)","color":"white","border":"1vmin solid #DEEF3C"});
							sound_play_click('sound_1');
						}
						else{
							var $this = $('.submit_button');
							var position = $this.position();
							var width = $this.width();
							var height = $this.height();
							var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
							var centerY = ((position.top + height)*100)/$board.height()+'%';
							$('<img class="incorrect_icon" style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-407%,-281%)" src="'+imgpath1 +'incorrect.png" />').insertAfter('.inputted_number');
							sound_play_click('sound_2');
							$(this).css({"background":"rgb(168, 33, 36)","color":"white","border":"1vmin solid #980000"});
						}
					});
					break;

					case 2:
							ole.footerNotificationHandler.hideNotification();
							$nextBtn.hide(0);
							$('.submit_button').click(function(){
								var the_number = parseInt($(".inputted_number").val());
								if(the_number==20){
									$('.inputted_number').css({'pointer-events':'none'});
									for (var i = 1; i < 13; i++) {
										$('.marbles'+i).animate({"top": "70%",
								    "left": "19%",
								    "opacity": "0"},2000);
									}
									setTimeout(function(){
										$('.empty_jar').animate({'opacity':'0'},500);
										nav_button_controls(500);
									},1800);
									var $this = $('.submit_button');
									var position = $this.position();
									$this.css('pointer-events','none');
									$('.incorrect_icon').hide(0);
									var width = $this.width();
									var height = $this.height();
									var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
									var centerY = ((position.top + height)*100)/$board.height()+'%';
									$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-1076%,-281%)" src="'+imgpath1 +'correct.png" />').insertAfter('.inputted_number');
									$this.css({"background":"rgb(190,214,47)","color":"white","border":"1vmin solid #DEEF3C"});
									sound_play_click('sound_1');
								}
								else{
									var $this = $('.submit_button');
									var position = $this.position();
									var width = $this.width();
									var height = $this.height();
									var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
									var centerY = ((position.top + height)*100)/$board.height()+'%';
									$('<img class="incorrect_icon" style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-1076%,-281%)" src="'+imgpath1 +'incorrect.png" />').insertAfter('.inputted_number');
									sound_play_click('sound_2');
									$(this).css({"background":"rgb(168, 33, 36)","color":"white","border":"1vmin solid #980000"});
								}
							});
							break;




		}
	}



	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_play_click(sound_id, click_class){
		createjs.Sound.stop;
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		// current_sound.on("complete", function(){
		// 	// nav_button_controls(0);
		// });
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image1(content, count){
		if(content[count].hasOwnProperty('marbeldiv')){
			var marbeldiv = content[count].marbeldiv;
				for(var i=0; i<marbeldiv.length; i++){
					var image_src = preload.getResult(marbeldiv[i].imgid).src;
					//get list of classes
					var classes_list = marbeldiv[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('marbeldivother')){
			var marbeldivother = content[count].marbeldivother;
				for(var i=0; i<marbeldivother.length; i++){
					var image_src = preload.getResult(marbeldivother[i].imgid).src;
					//get list of classes
					var classes_list = marbeldivother[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
		}
	}

	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
			for (var i = 0; i < content.length; i++) {
				slides(i);
				$($('.totalsequence')[i]).html(i);
				$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
			"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
			}
			function slides(i){
					$($('.totalsequence')[i]).click(function(){
						countNext = i;
						templateCaller();
					});
				}
		*/
	}


	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

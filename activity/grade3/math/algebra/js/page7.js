var imgpath = $ref + "/images/page03/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var imgpath1 = $ref + "/images/";
var input_number1=0;
var first_flag=false;
var sign=' ';
var roll_default_number=0;
var right_number=0;
var correct_answer=0;

var content=[

	// slide0
	{
		contentblockadditionalclass:'bluebg',
			imageblock:[{
				imagestoshow:[{
					imgid:'mala01',
					imgclass:'left_talking_image'
				}
				]
			}],
			uppertextblock:[{
				textdata:data.string.p7text1,
				textclass:'left_dialogue template-dialougebox2-top-white'
			},
			{
				textdata:data.string.p7text2,
				textclass:'eq_1'
			},
			{
				textdata:data.string.p7text3,
				textclass:'eq_2a'
			},
			{
				textdata:data.string.p7text4,
				textclass:'eq_2b'
			},
			{
				textdata:data.string.p7text2,
				textclass:'eq_3'
			},
			{
				textdata:data.string.p7text5,
				textclass:'eq_4'
			},
			{
				textdata:data.string.p7text2,
				textclass:'eq_5'
			},
			]
	},

	// slide1
	{
		contentblockadditionalclass:'bluebg',
			imageblock:[{
				imagestoshow:[{
					imgid:'mala01',
					imgclass:'left_talking_image'
				}
				]
			}],
			uppertextblock:[{
				textdata:data.string.p7text8,
				textclass:'left_dialogue template-dialougebox2-top-white'
			},
			{
				textdata:data.string.p7text6,
				textclass:'eq_1'
			},
			{
				textdata:data.string.p7text3,
				textclass:'eq_2a'
			},
			{
				textdata:data.string.p7text4,
				textclass:'eq_2b'
			},
			{
				textdata:data.string.p7text7,
				textclass:'eq_3'
			},
			{
				textdata:data.string.p7text5,
				textclass:'eq_4'
			},
			{
				textdata:data.string.p7text2,
				textclass:'eq_5'
			},
			{
				textdata:data.string.p6text3,
				textclass:'submit_button'
			},
			]
	},

	// slide2
	{
		contentblockadditionalclass:'bluebg',
			imageblock:[{
				imagestoshow:[{
					imgid:'mala01',
					imgclass:'left_talking_image'
				},
				{
					imgid:'plus',
					imgclass:'plus'
				},
				{
					imgid:'minus',
					imgclass:'minus'
				},
				{
					imgid:'plus',
					imgclass:'selected_sign'
				}
				]
			}],
			uppertextblock:[{
				textdata:data.string.p7text10,
				textclass:'left_dialogue template-dialougebox2-top-white'
			},
			{
				textdata:'',
				textclass:'eq_1'
			},

			{
				textdata:'',
				textclass:'eq_3'
			},
			{
				textdata:data.string.p7text5,
				textclass:'eq_4'
			},
			{
				textdata:data.string.p7text2,
				textclass:'eq_5'
			},
			{
				textdata:data.string.p6text3,
				textclass:'submit_button'
			},
			]
	},

	// slide3
	{
		contentblockadditionalclass:'bluebg',
			imageblock:[{
				imagestoshow:[{
					imgid:'mala02',
					imgclass:'left_talking_image'
				},
				{
					imgid:'plus',
					imgclass:'selected_sign'
				},
				{
					imgid:'triangle',
					imgclass:'increaser'
				},
				{
					imgid:'triangle',
					imgclass:'decreaser'
				}
				]
			}],
			uppertextblock:[{
				textdata:data.string.p7text12,
				textclass:'left_dialogue template-dialougebox2-top-white'
			},
			{
				textdata:'',
				textclass:'eq_1'
			},

			{
				textdata:'',
				textclass:'eq_3'
			},
			{
				textdata:data.string.p7text5,
				textclass:'eq_4'
			},
			{
				textdata:data.string.p6text3,
				textclass:'submit_button'
			},
			{
				textdata:' ',
				textclass:'no_roll_box eq_5'
			},
			{
				textdata:data.string.p6text3,
				textclass:'submit_button1'
			},
			]
	},

	// slide4
	{
		contentblockadditionalclass:'bluebg',
			imageblock:[{
				imagestoshow:[{
					imgid:'mala01',
					imgclass:'left_talking_image'
				},
				{
					imgid:'plus',
					imgclass:'selected_sign'
				},

				]
			}],
			uppertextblock:[{
				textdata:data.string.p7text13,
				textclass:'left_dialogue template-dialougebox2-top-white'
			},
			{
				textdata:'',
				textclass:'eq_1'
			},

			{
				textdata:'',
				textclass:'eq_3'
			},
			{
				textdata:data.string.p7text5,
				textclass:'eq_4'
			},
			{
				textdata:' ',
				textclass:'eq_5'
			}
			]
	},

	// slide5
	{
		contentblockadditionalclass:'bluebg',
			imageblock:[{
				imagestoshow:[{
					imgid:'mala03',
					imgclass:'left_talking_image'
				},
				{
					imgid:'plus',
					imgclass:'selected_sign'
				},

				]
			}],
			uppertextblock:[{
				textdata:data.string.p7text14,
				textclass:'left_dialogue template-dialougebox2-top-white'
			},
			{
				textdata:'',
				textclass:'eq_1'
			},

			{
				textdata:'',
				textclass:'eq_3'
			},
			{
				textdata:data.string.p7text5,
				textclass:'eq_4'
			},
			{
				textdata:' ',
				textclass:'eq_5'
			}
			]
	},

	// slide6
	{
		contentblockadditionalclass:'bluebg',
			imageblock:[{
				imagestoshow:[
				{
					imgid:'plus',
					imgclass:'selected_sign'
				},

				]
			}],
			uppertextblock:[{
				textdata:data.string.p7text15,
				textclass:'top_info'
			},
			{
				textdata:'',
				textclass:'eq_1'
			},

			{
				textdata:'',
				textclass:'eq_3'
			},
			{
				textdata:data.string.p7text5,
				textclass:'eq_4'
			},

			{
				textdata:' ',
				textclass:'eq_5'
			},
			{
				textdata:data.string.p6text3,
				textclass:'submit_button2'
			},
			]
	},


	// slide7
	{
		contentblockadditionalclass:'bluebg',
			imageblock:[{
				imagestoshow:[
				{
					imgid:'plus',
					imgclass:'selected_sign'
				},
				{
					imgid:'mala01',
					imgclass:'left_talking_image'
				},

				]
			}],
			uppertextblock:[{
				textdata:data.string.p7text17,
				textclass:'left_dialogue template-dialougebox2-top-white'
			},
			{
				textdata:'',
				textclass:'eq_1'
			},

			{
				textdata:'',
				textclass:'eq_3'
			},
			{
				textdata:data.string.p7text5,
				textclass:'eq_4'
			},

			{
				textdata:' ',
				textclass:'eq_5'
			},
			{
				textdata:data.string.p7text18,
				textclass:'replay_button submit_button2'
			},
			]
	},




];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images

			{id: "mala01", src: imgpath+"mala01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala02", src: imgpath+"mala02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala03", src: imgpath+"mala03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "plus", src: imgpath+"plus.png", type: createjs.AbstractLoader.IMAGE},
			{id: "minus", src: imgpath+"minus.png", type: createjs.AbstractLoader.IMAGE},
			{id: "triangle", src: imgpath1+"timer.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes

			// sounds
			{id: "sound_1", src: soundAsset+"success.ogg"},
			{id: "sound_2", src: soundAsset+"fail.ogg"},
			{id: "s7_p1", src: soundAsset+"s7_p1.ogg"},
			{id: "s7_p2", src: soundAsset+"s7_p2.ogg"},
			{id: "s7_p3", src: soundAsset+"s7_p3.ogg"},
			{id: "s7_p4", src: soundAsset+"s7_p4.ogg"},
			{id: "s7_p5", src: soundAsset+"s7_p5.ogg"},
			{id: "s7_p6", src: soundAsset+"s7_p6.ogg"},
			{id: "s7_p7", src: soundAsset+"s7_p7.ogg"},
			{id: "s7_p8", src: soundAsset+"s7_p8.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_image1(content, countNext);
		put_image2(content, countNext);
		// marblediv_image(content, countNext);

		if($lang == "np"){
			$(".eq_2b").css("left","40.8%");
		}


		$('.submit_button').css('display','none');
		$("input").keyup(function(e){
			var input_number = parseInt($(this).val());
			// console.log(input_number);
			if(input_number>=0)
			{
				$('input').not(this).css('pointer-events','none');
				$('.submit_button').css('display','block');
			}
			if(input_number<0 || isNaN(input_number)==true)
			{
				$('input').not(this).css('pointer-events','auto');
				$('.submit_button').css('display','none');
			}
		});
		$("input").keydown(function (e) {
			$this=$(this);
			$('.submit_button').click(function(){
				if($this.hasClass('first_number')){
					 first_flag = true;
				}
				else{
					 first_flag = false;
				}
				$('input').not($this).replaceWith(data.string.p7text2);
				input_number1 = parseInt($this.val());
				console.log(input_number1);
				$(this).css({'background':'grey','border-color':'grey','pointer-events':'none'});
				$('input').css('pointer-events','none');
				nav_button_controls(100);
				// $('.left_dialogue').html(data.string.p7text9);
			});
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 110, 190]) !== -1 ||
				 // Allow: Ctrl+A, Command+A
				(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
				 // Allow: home, end, left, right, down, up
				(e.keyCode >= 48 && e.keyCode <= 57)) {
						 // let it happen, don't do anything
						 return;
		}
		if (e.keyCode == 13) {
			$('.submit_button').click();
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 49 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
		}
		});

		vocabcontroller.findwords(countNext);
		switch (countNext) {
			case 0:
				sound_player("s7_p"+(countNext+1),1);
			break;
			case 1:
				sound_player("s7_p"+(countNext+1),0);
			break;
			case 2:
				sound_player("s7_p"+(countNext+1),0);
					if(first_flag==true)
					{
						$('.eq_1').html(input_number1);
						$('.eq_3').html(data.string.p7text2);
					}
					else{
						$('.eq_3').html(input_number1);
						$('.eq_1').html(data.string.p7text2);
					}
					$('.minus,.plus').click(function(){
						if($(this).hasClass('plus')){
							sign = 'plus';
							console.log(sign+' selected');
						}
						else{
							sign = 'minus';
							console.log(sign+' selected');
						}
						$('.minus,.plus').css('display','none');
						$('.selected_sign').css('display','block').attr('src',preload.getResult(sign).src);
						nav_button_controls(100);
					});
					break;
			case 3:
				sound_player("s7_p"+(countNext+1),0);
					if(first_flag==true)
					{
						$('.eq_1').html(input_number1);
						$('.eq_3').html(data.string.p7text2);
					}
					else{
						$('.eq_3').html(input_number1);
						$('.eq_1').html(data.string.p7text2);
					}
					$('.selected_sign').css('display','block').attr('src',preload.getResult(sign).src);

					if(sign=='plus')
					{
						roll_default_number=input_number1+1;
						$('.no_roll_box').html(roll_default_number);
						$('.decreaser').css({'opacity':'.5','pointer-events':'none'});
						$('.increaser').click(function(){
							right_number=parseInt($('.no_roll_box').text());
							right_number++;
							console.log(right_number);
							correct_answer=right_number-input_number1;
							console.log('correct answer is '+correct_answer);
							$('.no_roll_box').html(right_number);
							if(right_number>roll_default_number)
							{
								$('.decreaser').css({'opacity':'1','pointer-events':'auto'});
							}
							if(right_number==roll_default_number)
							{
								$('.decreaser').css({'opacity':'.5','pointer-events':'none'});
							}
							if(right_number==150)
							{
								$('.increaser').css({'opacity':'.5','pointer-events':'none'});
							}
						});
						$('.decreaser').click(function(){
							right_number=parseInt($('.no_roll_box').text());
							right_number--;
							console.log(right_number);
							correct_answer=right_number-input_number1;
							console.log('correct answer is '+correct_answer);
							$('.no_roll_box').html(right_number);
							if(right_number<150)
							{
								$('.increaser').css({'opacity':'1','pointer-events':'auto'});
							}
							if(right_number>roll_default_number)
							{
								$('.decreaser').css({'opacity':'1','pointer-events':'auto'});
							}
							if(right_number==roll_default_number)
							{
								$('.decreaser').css({'opacity':'.5','pointer-events':'none'});
							}
						});
					}


					if(sign=='minus' && first_flag)
					{
						roll_default_number=input_number1-1;
						$('.no_roll_box').html(roll_default_number);
						$('.increaser').css({'opacity':'.5','pointer-events':'none'});
						$('.increaser').click(function(){
							right_number=parseInt($('.no_roll_box').text());
							right_number++;
							console.log(right_number);
							correct_answer=input_number1-right_number;
							console.log('correct answer is '+correct_answer);
							$('.no_roll_box').html(right_number);
							if(right_number<roll_default_number)
							{
								$('.increaser').css({'opacity':'1','pointer-events':'auto'});
							}
							if(right_number==150)
							{
								$('.increaser').css({'opacity':'.5','pointer-events':'none'});
							}
							if(right_number==roll_default_number)
							{
								$('.increaser').css({'opacity':'.5','pointer-events':'none'});
							}
							if(right_number<2){
								$('.decreaser').css({'opacity':'.5','pointer-events':'none'});
							}
							if(right_number>1){
								$('.decreaser').css({'opacity':'1','pointer-events':'auto'});
							}
						});
						$('.decreaser').click(function(){
							right_number=parseInt($('.no_roll_box').text());
							right_number--;
							console.log(right_number);
							correct_answer=input_number1-right_number;
							console.log('correct answer is '+correct_answer);
							$('.no_roll_box').html(right_number);
							if(right_number<150)
							{
								$('.increaser').css({'opacity':'1','pointer-events':'auto'});
							}
							if(right_number<roll_default_number)
							{
								$('.increaser').css({'opacity':'1','pointer-events':'auto'});
							}
							if(right_number==roll_default_number)
							{
								$('.increaser').css({'opacity':'.5','pointer-events':'none'});
							}
							if(right_number<2){
								$('.decreaser').css({'opacity':'.5','pointer-events':'none'});
							}
							if(right_number>1){
								$('.decreaser').css({'opacity':'1','pointer-events':'auto'});
							}
						});
					}
					if(sign=='minus' && !first_flag)
					{
						roll_default_number=1;
						$('.no_roll_box').html(roll_default_number);
						$('.decreaser').css({'opacity':'.5','pointer-events':'none'});
						$('.increaser').click(function(){
							right_number=parseInt($('.no_roll_box').text());
							right_number++;
							console.log(right_number);
							correct_answer=input_number1+right_number;
							console.log('correct answer is '+correct_answer);
							$('.no_roll_box').html(right_number);
							if(right_number==150)
							{
								$('.increaser').css({'opacity':'.5','pointer-events':'none'});
							}
							if(right_number<2){
								$('.decreaser').css({'opacity':'.5','pointer-events':'none'});
							}
							if(right_number>1){
								$('.decreaser').css({'opacity':'1','pointer-events':'auto'});
							}
						});
						$('.decreaser').click(function(){
							right_number=parseInt($('.no_roll_box').text());
							right_number--;
							correct_answer=input_number1+right_number;
							console.log('correct answer is '+correct_answer);
							console.log(right_number);
							$('.no_roll_box').html(right_number);
							if(right_number<150)
							{
								$('.increaser').css({'opacity':'1','pointer-events':'auto'});
							}
							if(right_number<2){
								$('.decreaser').css({'opacity':'.5','pointer-events':'none'});
							}
							if(right_number>1){
								$('.decreaser').css({'opacity':'1','pointer-events':'auto'});
							}
						});
					}
					$('.submit_button1').click(function(){
						$(this).css({'pointer-events':'none','background':'grey','border':'grey'});
						$('.decreaser,.increaser').css({'opacity':'.5','pointer-events':'none'});
						right_number=parseInt($('.no_roll_box').text());
						if(sign == 'plus'){
							correct_answer=right_number-input_number1;
							console.log('submitted correct answer is '+correct_answer);
						}
						if(sign=='minus' && first_flag){
							correct_answer=input_number1-right_number;
							console.log('submitted correct answer is '+correct_answer);
						}
						if(sign=='minus' && !first_flag){
							correct_answer=input_number1+right_number;
							console.log('submitted correct answer is '+correct_answer);
						}
						nav_button_controls(1000);
					});
					break;

			case 4:
				sound_player("s7_p"+(countNext+1),1);
					if(first_flag==true)
					{
						$('.eq_1').html(input_number1);
						$('.eq_3').html(data.string.p7text2);
					}
					else{
						$('.eq_3').html(input_number1);
						$('.eq_1').html(data.string.p7text2);
					}
					$('.selected_sign').css('display','block').attr('src',preload.getResult(sign).src);
					$('.eq_5').html(right_number);
					$('.right_question_number').html(right_number);
					if(first_flag==true)
					{
						$('.number1').html(input_number1);
						$('.number2').html(data.string.p7text2);
					}
					else{
						$('.number2').html(input_number1);
						$('.number1').html(data.string.p7text2);
					}
					if(sign=='plus'){
						$('.question_sign').html(data.string.p7text3);
					}
					if(sign=='minus'){
						$('.question_sign').html(data.string.p7text4);
					}
					break;

					case 5:
						sound_player("s7_p"+(countNext+1),1);
							if(first_flag==true)
							{
								$('.eq_1').html(input_number1);
								$('.eq_3').html(data.string.p7text2);
							}
							else{
								$('.eq_3').html(input_number1);
								$('.eq_1').html(data.string.p7text2);
							}
							$('.selected_sign').css('display','block').attr('src',preload.getResult(sign).src);
							$('.eq_5').html(right_number);

							break;
				case 6:
					sound_player("s7_p"+(countNext+1),0);
						if(first_flag==true)
						{
							$('.eq_1').html(input_number1);
							$('.eq_3').html(data.string.p7text16);
						}
						else{
							$('.eq_3').html(input_number1);
							$('.eq_1').html(data.string.p7text16);
						}
						$('.selected_sign').css('display','block').attr('src',preload.getResult(sign).src);
						$('.eq_5').html(right_number);
						$('.submit_button2').click(function(){
								var entered_number=parseInt($('.final_answer').val());
								console.log(entered_number);
								if(entered_number==correct_answer){
									var $this = $('.final_answer');
									var position = $this.position();
									$this.css('pointer-events','none');
									$('.incorrect_icon').hide(0);
									var width = $this.width();
									var height = $this.height();
									var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
									var centerY = ((position.top + height)*100)/$board.height()+'%';
									$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:10%;transform:translate(-25%,-142%)" src="'+imgpath1 +'correct.png" />').insertAfter('.final_answer');
									$('.final_answer').css({"background":"rgb(190,214,47)","color":"white","border":"1vmin solid #DEEF3C"});
									$('.submit_button2').css({"background":"rgb(190,214,47)","color":"white","border":"1vmin solid #DEEF3C","pointer-events":"none"});
									nav_button_controls(1000);
									sound_play_click('sound_1');
								}
								else{
									var $this = $('.final_answer');
									var position = $this.position();
									var width = $this.width();
									var height = $this.height();
									var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
									var centerY = ((position.top + height)*100)/$board.height()+'%';
									$('<img class="incorrect_icon" style="left:'+centerX+';top:'+centerY+';position:absolute;width:10%;transform:translate(-25%,-142%)" src="'+imgpath1 +'incorrect.png" />').insertAfter('.final_answer');
									sound_play_click('sound_2');
									$(this).css({"background":"rgb(168, 33, 36)","color":"white","border":"1vmin solid #980000"});
								}
						});
						$("input").keydown(function (e) {
							$this=$(this);

						if (e.keyCode == 13) {
							$('.submit_button2').click();
						}
						});
						break;
				case 7:
					sound_player("s7_p"+(countNext+1),1);
						if(first_flag==true)
						{
							$('.eq_1').html(input_number1);
							$('.eq_3').html(correct_answer);
						}
						else{
							$('.eq_3').html(input_number1);
							$('.eq_1').html(correct_answer);
						}
						$('.selected_sign').css('display','block').attr('src',preload.getResult(sign).src);
						$('.eq_5').html(right_number);
						$('.right_question_number').html(right_number);
						if(first_flag==true)
						{
							$('.number1').html(input_number1);
							$('.number2').html(correct_answer);
						}
						else{
							$('.number2').html(input_number1);
							$('.number1').html(correct_answer);
						}
						if(sign=='plus'){
							$('.question_sign').html(data.string.p7text3);
						}
						if(sign=='minus'){
							$('.question_sign').html(data.string.p7text4);
						}
						$('.replay_button').click(function(){
								countNext=1;
								templateCaller();
							 	ole.footerNotificationHandler.hideNotification();
						});
						break;

		}
	}



	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls():'';
		});
	}
	function sound_play_click(sound_id, click_class){
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			// nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image1(content, count){
		if(content[count].hasOwnProperty('marbeldiv')){
			var marbeldiv = content[count].marbeldiv;
				for(var i=0; i<marbeldiv.length; i++){
					var image_src = preload.getResult(marbeldiv[i].imgid).src;
					//get list of classes
					var classes_list = marbeldiv[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('marbeldivother')){
			var marbeldivother = content[count].marbeldivother;
				for(var i=0; i<marbeldivother.length; i++){
					var image_src = preload.getResult(marbeldivother[i].imgid).src;
					//get list of classes
					var classes_list = marbeldivother[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
		}
	}

	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
			for (var i = 0; i < content.length; i++) {
				slides(i);
				$($('.totalsequence')[i]).html(i);
				$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
			"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
			}
			function slides(i){
					$($('.totalsequence')[i]).click(function(){
						countNext = i;
						templateCaller();
					});
				}
		*/
	}


	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

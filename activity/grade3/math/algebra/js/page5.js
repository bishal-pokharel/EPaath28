var imgpath = $ref + "/images/page03/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentblockadditionalclass:'bluebg',
		marbeldiv:[
			{
				imgid:'marble01',
				imgclass:'marbles1'
			},{
				imgid:'marble02',
				imgclass:'marbles2'
			},{
				imgid:'marble03',
				imgclass:'marbles3'
			},{
				imgid:'marble04',
				imgclass:'marbles4'
			},{
				imgid:'marble05',
				imgclass:'marbles5'
			},{
				imgid:'marble06',
				imgclass:'marbles6'
			},{
				imgid:'marble07',
				imgclass:'marbles7'
			},{
				imgid:'marble08',
				imgclass:'marbles8'
			},{
				imgid:'marble09',
				imgclass:'marbles9'
			},{
				imgid:'marble10',
				imgclass:'marbles10'
			},{
				imgid:'marble11',
				imgclass:'marbles11'
			},{
				imgid:'marble01',
				imgclass:'marbles12'
			},],
			imageblock:[{
				imagestoshow:[{
					imgid:'boy_talking',
					imgclass:'left_boy'
				}]
			}],
		uppertextblock:[{
			textdata:data.string.p5text1,
			textclass:'left_dialogue template-dialougebox2-top-white'
		}]
	},


	// slide1
	{
		contentblockadditionalclass:'bluebg',
		marbeldiv:[
			{
				imgid:'marble01',
				imgclass:'marbles1'
			},{
				imgid:'marble02',
				imgclass:'marbles2'
			},{
				imgid:'marble03',
				imgclass:'marbles3'
			},{
				imgid:'marble04',
				imgclass:'marbles4'
			},{
				imgid:'marble05',
				imgclass:'marbles5'
			},{
				imgid:'marble06',
				imgclass:'marbles6'
			},{
				imgid:'marble07',
				imgclass:'marbles7'
			},{
				imgid:'marble08',
				imgclass:'marbles8'
			},{
				imgid:'marble09',
				imgclass:'marbles9'
			},{
				imgid:'marble10',
				imgclass:'marbles10'
			},{
				imgid:'marble11',
				imgclass:'marbles11'
			},{
				imgid:'marble01',
				imgclass:'marbles12'
			},],
			imageblock:[{
				imagestoshow:[{
					imgid:'boy_talking',
					imgclass:'left_boy'
				},
				{
					imgid:'empty_jar',
					imgclass: 'jar'
				}]
			}],
		uppertextblock:[{
			textdata:data.string.p5text2,
			textclass:'left_dialogue template-dialougebox2-top-white'
		},
		{
			textdata:data.string.p5text3,
			textclass:'what_question'
		}]
	},

	// slide2
	{
		contentblockadditionalclass:'bluebg',
		marbeldiv:[
			{
				imgid:'marble01',
				imgclass:'marbles1'
			},{
				imgid:'marble02',
				imgclass:'marbles2'
			},{
				imgid:'marble03',
				imgclass:'marbles3'
			},{
				imgid:'marble04',
				imgclass:'marbles4'
			},{
				imgid:'marble05',
				imgclass:'marbles5'
			},{
				imgid:'marble06',
				imgclass:'marbles6'
			},{
				imgid:'marble07',
				imgclass:'marbles7'
			},{
				imgid:'marble08',
				imgclass:'marbles8'
			},{
				imgid:'marble09',
				imgclass:'marbles9'
			},{
				imgid:'marble10',
				imgclass:'marbles10'
			},{
				imgid:'marble11',
				imgclass:'marbles11'
			},{
				imgid:'marble01',
				imgclass:'marbles12'
			},],
			imageblock:[{
				imagestoshow:[{
					imgid:'boy_talking',
					imgclass:'left_boy'
				}]
			}],
		uppertextblock:[{
			textdata:data.string.p5text4,
			textclass:'left_dialogue template-dialougebox2-top-white'
		}]
	},

	// slide3
	{
		contentblockadditionalclass:'bluebg',
		marbeldiv:[
			{
				imgid:'marble01',
				imgclass:'marbles1'
			},{
				imgid:'marble02',
				imgclass:'marbles2'
			},{
				imgid:'marble03',
				imgclass:'marbles3'
			},{
				imgid:'marble04',
				imgclass:'marbles4'
			},{
				imgid:'marble05',
				imgclass:'marbles5'
			},{
				imgid:'marble06',
				imgclass:'marbles6'
			},{
				imgid:'marble07',
				imgclass:'marbles7'
			},{
				imgid:'marble08',
				imgclass:'marbles8'
			},{
				imgid:'marble09',
				imgclass:'marbles9'
			},{
				imgid:'marble10',
				imgclass:'marbles10'
			},{
				imgid:'marble11',
				imgclass:'marbles11'
			},{
				imgid:'marble01',
				imgclass:'marbles12'
			},],
			imageblock:[{
				imagestoshow:[{
					imgid:'boy_talking',
					imgclass:'left_boy'
				},
				{
					imgid:'empty_jar',
					imgclass: 'jar'
				}]
			}],
		uppertextblock:[{
			textdata:data.string.p5text5,
			textclass:'left_dialogue template-dialougebox2-top-white'
		},
		{
			textdata:data.string.p5text3,
			textclass:'what_question'
		},
		{
			textdata:data.string.p5text6,
			textclass:'what_question1'
		}]
	},

	// slide4
	{
		contentblockadditionalclass:'bluebg',
		marbeldiv:[
			{
				imgid:'marble01',
				imgclass:'marbles1'
			},{
				imgid:'marble02',
				imgclass:'marbles2'
			},{
				imgid:'marble03',
				imgclass:'marbles3'
			},{
				imgid:'marble04',
				imgclass:'marbles4'
			},{
				imgid:'marble05',
				imgclass:'marbles5'
			},{
				imgid:'marble06',
				imgclass:'marbles6'
			},{
				imgid:'marble07',
				imgclass:'marbles7'
			},{
				imgid:'marble08',
				imgclass:'marbles8'
			},{
				imgid:'marble09',
				imgclass:'marbles9'
			},{
				imgid:'marble10',
				imgclass:'marbles10'
			},{
				imgid:'marble11',
				imgclass:'marbles11'
			},{
				imgid:'marble01',
				imgclass:'marbles12'
			},],
			marbeldivother:[
				{
					imgid:'marble_emptied',
					imgclass:'marbles1a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles2a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles3a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles4a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles5a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles6a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles7a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles8a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles9a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles10a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles11a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles12a'
				},],
			imageblock:[{
				imagestoshow:[{
					imgid:'boy_talking',
					imgclass:'left_boy'
				},
				{
					imgid:'five_marble_jar',
					imgclass: 'jar '
				},
				{
					imgid:'empty_jar',
					imgclass: 'jar empty_jar'
				},
				]
			}],
		uppertextblock:[{
			textdata:data.string.p5text7,
			textclass:'left_dialogue template-dialougebox2-top-white'
		},
		{
			textdata:data.string.p5text3,
			textclass:'what_question'
		},
		{
			textdata:data.string.p5text6,
			textclass:'what_question1'
		}]
	},

	// slide5
	{
		contentblockadditionalclass:'bluebg',
		marbeldiv:[
			{
				imgid:'marble01',
				imgclass:'marbles1'
			},{
				imgid:'marble02',
				imgclass:'marbles2'
			},{
				imgid:'marble03',
				imgclass:'marbles3'
			},{
				imgid:'marble04',
				imgclass:'marbles4'
			},{
				imgid:'marble05',
				imgclass:'marbles5'
			},{
				imgid:'marble06',
				imgclass:'marbles6'
			},{
				imgid:'marble07',
				imgclass:'marbles7'
			},{
				imgid:'marble08',
				imgclass:'marbles8'
			},{
				imgid:'marble09',
				imgclass:'marbles9'
			},{
				imgid:'marble10',
				imgclass:'marbles10'
			},{
				imgid:'marble11',
				imgclass:'marbles11'
			},{
				imgid:'marble01',
				imgclass:'marbles12'
			},],
			marbeldivother:[
				{
					imgid:'marble_emptied',
					imgclass:'marbles1a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles2a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles3a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles4a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles5a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles6a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles7a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles8a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles9a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles10a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles11a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles12a'
				},],
			imageblock:[{
				imagestoshow:[{
					imgid:'boy_talking',
					imgclass:'left_boy'
				},
				{
					imgid:'five_marble_jar',
					imgclass: 'jar '
				},
				{
					imgid:'empty_jar',
					imgclass: 'jar empty_jar'
				},
				]
			}],
		uppertextblock:[{
			textdata:data.string.p5text9,
			textclass:'left_dialogue template-dialougebox2-top-white'
		},
		{
			textdata:data.string.p5text3,
			textclass:'what_question'
		},
		{
			textdata:data.string.p5text8,
			textclass:'what_question1'
		}]
	},

	// slide6
	{
		contentblockadditionalclass:'bluebg',
		marbeldiv:[
			{
				imgid:'marble01',
				imgclass:'marbles1'
			},{
				imgid:'marble02',
				imgclass:'marbles2'
			},{
				imgid:'marble03',
				imgclass:'marbles3'
			},{
				imgid:'marble04',
				imgclass:'marbles4'
			},{
				imgid:'marble05',
				imgclass:'marbles5'
			},{
				imgid:'marble06',
				imgclass:'marbles6'
			},{
				imgid:'marble07',
				imgclass:'marbles7'
			},{
				imgid:'marble08',
				imgclass:'marbles8'
			},{
				imgid:'marble09',
				imgclass:'marbles9'
			},{
				imgid:'marble10',
				imgclass:'marbles10'
			},{
				imgid:'marble11',
				imgclass:'marbles11'
			},{
				imgid:'marble01',
				imgclass:'marbles12'
			},],
			imageblock:[{
				imagestoshow:[{
					imgid:'boy_talking',
					imgclass:'left_boy'
				},
				{
					imgid:'five_marble_jar',
					imgclass: 'jar '
				},
				{
					imgid:'empty_jar',
					imgclass: 'jar empty_jar'
				},
				]
			}],
		uppertextblock:[{
			textdata:data.string.p5text10,
			textclass:'left_dialogue template-dialougebox2-top-white'
		},
		{
			textdata:data.string.p5text3,
			textclass:'what_question'
		}]
	},

	// slide7
	{
		contentblockadditionalclass:'bluebg',
		marbeldiv:[
			{
				imgid:'marble01',
				imgclass:'marbles1'
			},{
				imgid:'marble02',
				imgclass:'marbles2'
			},{
				imgid:'marble03',
				imgclass:'marbles3'
			},{
				imgid:'marble04',
				imgclass:'marbles4'
			},{
				imgid:'marble05',
				imgclass:'marbles5'
			},{
				imgid:'marble06',
				imgclass:'marbles6'
			},{
				imgid:'marble07',
				imgclass:'marbles7'
			},{
				imgid:'marble08',
				imgclass:'marbles8'
			},{
				imgid:'marble09',
				imgclass:'marbles9'
			},{
				imgid:'marble10',
				imgclass:'marbles10'
			},{
				imgid:'marble11',
				imgclass:'marbles11'
			},{
				imgid:'marble01',
				imgclass:'marbles12'
			},],
			marbeldivother:[
				{
					imgid:'marble_emptied',
					imgclass:'marbles1a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles2a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles3a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles4a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles5a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles6a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles7a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles8a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles9a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles10a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles11a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles12a'
				},],
			imageblock:[{
				imagestoshow:[{
					imgid:'boy_talking',
					imgclass:'left_boy'
				},
				{
					imgid:'seven_marble',
					imgclass: 'jar '
				},
				{
					imgid:'empty_jar',
					imgclass: 'jar empty_jar'
				},
				]
			}],
		uppertextblock:[{
			textdata:data.string.p5text11,
			textclass:'left_dialogue template-dialougebox2-top-white'
		},
		{
			textdata:data.string.p5text3,
			textclass:'what_question'
		}]
	},

	// slide8
	{
		contentblockadditionalclass:'bluebg',
		marbeldiv:[
			{
				imgid:'marble01',
				imgclass:'marbles1'
			},{
				imgid:'marble02',
				imgclass:'marbles2'
			},{
				imgid:'marble03',
				imgclass:'marbles3'
			},{
				imgid:'marble04',
				imgclass:'marbles4'
			},{
				imgid:'marble05',
				imgclass:'marbles5'
			},{
				imgid:'marble06',
				imgclass:'marbles6'
			},{
				imgid:'marble07',
				imgclass:'marbles7'
			},{
				imgid:'marble08',
				imgclass:'marbles8'
			},{
				imgid:'marble09',
				imgclass:'marbles9'
			},{
				imgid:'marble10',
				imgclass:'marbles10'
			},{
				imgid:'marble11',
				imgclass:'marbles11'
			},{
				imgid:'marble01',
				imgclass:'marbles12'
			},],
			marbeldivother:[
				{
					imgid:'marble_emptied',
					imgclass:'marbles1a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles2a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles3a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles4a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles5a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles6a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles7a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles8a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles9a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles10a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles11a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles12a'
				},],
			imageblock:[{
				imagestoshow:[{
					imgid:'boy_talking',
					imgclass:'left_boy'
				},
				{
					imgid:'seven_marble',
					imgclass: 'jar '
				},
				{
					imgid:'empty_jar',
					imgclass: 'jar empty_jar'
				},
				]
			}],
		uppertextblock:[{
			textdata:data.string.p5text12,
			textclass:'left_dialogue template-dialougebox2-top-white'
		},
		{
			textdata:data.string.p5text13,
			textclass:'what_question'
		}]
	},

	// slide9
	{
		contentblockadditionalclass:'bluebg',

			imageblock:[{
				imagestoshow:[{
					imgid:'boy_talking',
					imgclass:'left_boy'
				},
				{
					imgid:'empty_jar',
					imgclass: 'jar empty_jar'
				},
				]
			}],
		uppertextblock:[{
			textdata:data.string.p5text14,
			textclass:'left_dialogue template-dialougebox2-top-white'
		}]
	},


	// slide10
	{
		contentblockadditionalclass:'bluebg',
		marbeldiv:[
			{
				imgid:'marble01',
				imgclass:'marbles1'
			},{
				imgid:'marble02',
				imgclass:'marbles2'
			},{
				imgid:'marble03',
				imgclass:'marbles3'
			},{
				imgid:'marble04',
				imgclass:'marbles4'
			},{
				imgid:'marble05',
				imgclass:'marbles5'
			},{
				imgid:'marble06',
				imgclass:'marbles6'
			},{
				imgid:'marble07',
				imgclass:'marbles7'
			},{
				imgid:'marble08',
				imgclass:'marbles8'
			},{
				imgid:'marble09',
				imgclass:'marbles9'
			},],
			marbeldivother:[
				{
					imgid:'marble_emptied',
					imgclass:'marbles1a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles2a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles3a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles4a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles5a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles6a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles7a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles8a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles9a'
				}],
			imageblock:[{
				imagestoshow:[{
					imgid:'boy_talking',
					imgclass:'left_boy'
				},
				{
					imgid:'six_marble',
					imgclass: 'jar '
				},

				]
			}],
		uppertextblock:[{
			textdata:data.string.p5text15,
			textclass:'left_dialogue template-dialougebox2-top-white'
		},
		{
			textdata:data.string.p5text16,
			textclass:'what_question'
		}]
	},

	// slide11
	{
		contentblockadditionalclass:'bluebg',
		marbeldiv:[
			{
				imgid:'marble01',
				imgclass:'marbles1'
			},{
				imgid:'marble02',
				imgclass:'marbles2'
			},{
				imgid:'marble03',
				imgclass:'marbles3'
			},{
				imgid:'marble04',
				imgclass:'marbles4'
			},{
				imgid:'marble05',
				imgclass:'marbles5'
			},{
				imgid:'marble06',
				imgclass:'marbles6'
			},{
				imgid:'marble07',
				imgclass:'marbles7'
			},{
				imgid:'marble08',
				imgclass:'marbles8'
			},{
				imgid:'marble09',
				imgclass:'marbles9'
			},],
			marbeldivother:[
				{
					imgid:'marble_emptied',
					imgclass:'marbles1a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles2a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles3a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles4a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles5a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles6a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles7a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles8a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles9a'
				}],
			imageblock:[{
				imagestoshow:[{
					imgid:'boy_talking',
					imgclass:'left_boy'
				},
				{
					imgid:'six_marble',
					imgclass: 'jar '
				},

				]
			}],
		uppertextblock:[{
			textdata:data.string.p5text17,
			textclass:'left_dialogue template-dialougebox2-top-white'
		},
		{
			textdata:data.string.p5text16,
			textclass:'what_question'
		},
		{
			textdata:data.string.p5text18,
			textclass:'what_question1'
		}]
	},

	// slide12
	{
		contentblockadditionalclass:'bluebg',
		marbeldiv:[
			{
				imgid:'marble01',
				imgclass:'marbles1'
			},{
				imgid:'marble02',
				imgclass:'marbles2'
			},{
				imgid:'marble03',
				imgclass:'marbles3'
			},{
				imgid:'marble04',
				imgclass:'marbles4'
			},{
				imgid:'marble05',
				imgclass:'marbles5'
			},{
				imgid:'marble06',
				imgclass:'marbles6'
			},{
				imgid:'marble07',
				imgclass:'marbles7'
			},{
				imgid:'marble08',
				imgclass:'marbles8'
			},{
				imgid:'marble09',
				imgclass:'marbles9'
			},],
			marbeldivother:[
				{
					imgid:'marble_emptied',
					imgclass:'marbles1a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles2a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles3a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles4a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles5a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles6a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles7a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles8a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles9a'
				}],
			imageblock:[{
				imagestoshow:[{
					imgid:'boy_talking',
					imgclass:'left_boy'
				},
				{
					imgid:'empty_jar',
					imgclass: 'jar '
				},
				{
					imgid:'six_marble',
					imgclass: 'jar six_marble'
				},


				]
			}],
		uppertextblock:[{
			textdata:data.string.p5text19,
			textclass:'left_dialogue template-dialougebox2-top-white'
		},
		{
			textdata:data.string.p5text16,
			textclass:'what_question'
		},
		{
			textdata:data.string.p5text21,
			textclass:'what_question1',
			datahighlightflag:true,
			datahighlightcustomclass:'border'
		}]
	},

	// slide13
	{
		contentblockadditionalclass:'bluebg',
		marbeldiv:[
			{
				imgid:'marble01',
				imgclass:'marbles1'
			},{
				imgid:'marble02',
				imgclass:'marbles2'
			},{
				imgid:'marble03',
				imgclass:'marbles3'
			},{
				imgid:'marble04',
				imgclass:'marbles4'
			},{
				imgid:'marble05',
				imgclass:'marbles5'
			},{
				imgid:'marble06',
				imgclass:'marbles6'
			},{
				imgid:'marble07',
				imgclass:'marbles7'
			},{
				imgid:'marble08',
				imgclass:'marbles8'
			},{
				imgid:'marble09',
				imgclass:'marbles9'
			},],

			imageblock:[{
				imagestoshow:[{
					imgid:'boy_talking',
					imgclass:'left_boy'
				},
				{
					imgid:'empty_jar',
					imgclass: 'jar '
				}
				]
			}],
		uppertextblock:[{
			textdata:data.string.p5text22,
			textclass:'left_dialogue template-dialougebox2-top-white'
		},
		{
			textdata:data.string.p5text16Sec,
			textclass:'what_question'
		}]
	},

	// slide14
	{
		contentblockadditionalclass:'bluebg',
		marbeldiv:[
			{
				imgid:'marble01',
				imgclass:'marbles1'
			},{
				imgid:'marble02',
				imgclass:'marbles2'
			},{
				imgid:'marble03',
				imgclass:'marbles3'
			},{
				imgid:'marble04',
				imgclass:'marbles4'
			},{
				imgid:'marble05',
				imgclass:'marbles5'
			},{
				imgid:'marble06',
				imgclass:'marbles6'
			},{
				imgid:'marble07',
				imgclass:'marbles7'
			},{
				imgid:'marble08',
				imgclass:'marbles8'
			},{
				imgid:'marble09',
				imgclass:'marbles9'
			},],
			marbeldivother:[
				{
					imgid:'marble_emptied',
					imgclass:'marbles1a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles2a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles3a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles4a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles5a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles6a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles7a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles8a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles9a'
				}],
			imageblock:[{
				imagestoshow:[{
					imgid:'boy_talking',
					imgclass:'left_boy'
				},

				{
					imgid:'six_marble',
					imgclass: 'jar six_marble'
				},
				{
					imgid:'empty_jar',
					imgclass: 'jar empty_jar'
				},
				]
			}],
		uppertextblock:[{
			textdata:data.string.p5text23,
			textclass:'left_dialogue template-dialougebox2-top-white'
		},
		{
			textdata:data.string.p5text16,
			textclass:'what_question'
		}]
	},

	// slide15
	{
		contentblockadditionalclass:'bluebg',
		marbeldiv:[
			{
				imgid:'marble01',
				imgclass:'marbles1'
			},{
				imgid:'marble02',
				imgclass:'marbles2'
			},{
				imgid:'marble03',
				imgclass:'marbles3'
			},{
				imgid:'marble04',
				imgclass:'marbles4'
			},{
				imgid:'marble05',
				imgclass:'marbles5'
			},{
				imgid:'marble06',
				imgclass:'marbles6'
			},{
				imgid:'marble07',
				imgclass:'marbles7'
			},{
				imgid:'marble08',
				imgclass:'marbles8'
			},{
				imgid:'marble09',
				imgclass:'marbles9'
			},],
			marbeldivother:[
				{
					imgid:'marble_emptied',
					imgclass:'marbles1a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles2a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles3a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles4a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles5a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles6a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles7a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles8a'
				},{
					imgid:'marble_emptied',
					imgclass:'marbles9a'
				}],
			imageblock:[{
				imagestoshow:[{
					imgid:'boy_talking',
					imgclass:'left_boy'
				},

				{
					imgid:'six_marble',
					imgclass: 'jar six_marble'
				},
				{
					imgid:'empty_jar',
					imgclass: 'jar empty_jar'
				},
				]
			}],
		uppertextblock:[{
			textdata:data.string.p5text24,
			textclass:'left_dialogue template-dialougebox2-top-white'
		},
		{
			textdata:data.string.p5text25,
			textclass:'what_question',
			datahighlightflag:true,
			datahighlightcustomclass:'border'
		}]
	},

	// slide16
	{
		contentblockadditionalclass:'bluebg',

			imageblock:[{
				imagestoshow:[{
					imgid:'boy_talking',
					imgclass:'left_boy'
				}
				]
			}],
		uppertextblock:[{
			textdata:data.string.p5text26,
			textclass:'left_dialogue template-dialougebox2-top-white'
		}]
	},

	// slide17
	{
		contentblockadditionalclass:'bluebg',

			imageblock:[{
				imagestoshow:[{
					imgid:'boy_talking',
					imgclass:'left_boy'
				}
				]
			}],
		uppertextblock:[{
			textdata:data.string.p5_s18,
			textclass:'left_dialogue template-dialougebox2-top-white'
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:'greyBg',
			textdata:data.string.p5_s18_eqn,
			textclass:'eqn'
		}]
	},

	// slide18
	{
		contentblockadditionalclass:'bluebg',

			imageblock:[{
				imagestoshow:[{
					imgid:'boy_talking',
					imgclass:'left_boy'
				}
				]
			}],
		uppertextblock:[{
			textdata:data.string.p5_s19,
			textclass:'left_dialogue template-dialougebox2-top-white'
		}],
		extratextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:'greyBg',
			textdata:data.string.p5_s18_eqn,
			textclass:'eqn'
		},{
			datahighlightflag:true,
			datahighlightcustomclass:'greyBg_1',
			textdata:data.string.p5_s19_eqn,
			textclass:'eqn eqBtm'
		},{
			// textdata:data.string.p5_s19_eqn,
			textclass:'centralDivider'
		}]
	},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "marble01", src: imgpath+"marble01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble02", src: imgpath+"marble02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble03", src: imgpath+"marble03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble04", src: imgpath+"marble04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble05", src: imgpath+"marble05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble06", src: imgpath+"marble06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble07", src: imgpath+"marble07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble08", src: imgpath+"marble08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble09", src: imgpath+"marble09.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble10", src: imgpath+"marble10.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble11", src: imgpath+"marble11.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy_quite", src: imgpath+"prem01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy_talking", src: imgpath+"prem02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "empty_jar", src: imgpath+"empty_jar.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble_emptied", src: imgpath+"marble.png", type: createjs.AbstractLoader.IMAGE},
			{id: "five_marble_jar", src: imgpath+"five_marble.png", type: createjs.AbstractLoader.IMAGE},
			{id: "seven_marble", src: imgpath+"seven_marble.png", type: createjs.AbstractLoader.IMAGE},
			{id: "six_marble", src: imgpath+"six_marble.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes

			// sounds
			{id: "s5_p1", src: soundAsset+"s5_p1.ogg"},
			{id: "s5_p2", src: soundAsset+"s5_p2.ogg"},
			{id: "s5_p3", src: soundAsset+"s5_p3.ogg"},
			{id: "s5_p4", src: soundAsset+"s5_p4.ogg"},
			{id: "s5_p5", src: soundAsset+"s5_p5.ogg"},
			{id: "s5_p6", src: soundAsset+"s5_p6.ogg"},
			{id: "s5_p7", src: soundAsset+"s5_p7.ogg"},
			{id: "s5_p8", src: soundAsset+"s5_p8.ogg"},
			{id: "s5_p9", src: soundAsset+"s5_p9.ogg"},
			{id: "s5_p10", src: soundAsset+"s5_p10.ogg"},
			{id: "s5_p11", src: soundAsset+"s5_p11.ogg"},
			{id: "s5_p12", src: soundAsset+"s5_p12.ogg"},
			{id: "s5_p13", src: soundAsset+"s5_p13.ogg"},
			{id: "s5_p14", src: soundAsset+"s5_p14.ogg"},
			{id: "s5_p15", src: soundAsset+"s5_p15.ogg"},
			{id: "s5_p16", src: soundAsset+"s5_p16.ogg"},
			{id: "s5_p17", src: soundAsset+"s5_p17.ogg"},
			{id: "s5_p18", src: soundAsset+"s5_p18.ogg"},
			{id: "s5_p19", src: soundAsset+"s5_p19.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_image1(content, countNext);
		put_image2(content, countNext);
		// marblediv_image(content, countNext);
		vocabcontroller.findwords(countNext);
		switch (countNext) {
			case 4:
				sound_player("s5_p"+(countNext+1),1);
				$('.marbles1,.marbles2,.marbles3,.marbles4,.marbles5').animate({"top": "70%",
		    "left": "19%",
		    "opacity": "0"},2000);
				setTimeout(function() {
					$('.empty_jar').fadeOut(1000);
				},1500);
				// nav_button_controls(2500);
				break;
			case 5:
				sound_player("s5_p"+(countNext+1),1);
			$('.marbles1,.marbles2,.marbles3,.marbles4,.marbles5').css('display','none');
			$('.empty_jar').hide(0);
			// nav_button_controls(2500);
			break;
			case 7:
				sound_player("s5_p"+(countNext+1),1);
				$('.marbles1,.marbles2,.marbles3,.marbles4,.marbles5,.marbles6,.marbles7').animate({"top": "70%",
		    "left": "19%",
		    "opacity": "0"},2000);
				setTimeout(function() {
					$('.empty_jar').fadeOut(1000);
				},1500);
				// nav_button_controls(2500);
				break;
			case 8:
				sound_player("s5_p"+(countNext+1),1);
			$('.marbles1,.marbles2,.marbles3,.marbles4,.marbles5,.marbles6,.marbles7').css('display','none');
			$('.empty_jar').hide(0);
			// nav_button_controls(2500);
			break;
			case 10:
			case 11:
				sound_player("s5_p"+(countNext+1),1);
			$('.marbles1,.marbles2,.marbles3,.marbles4,.marbles5,.marbles6').css('display','none');
			// nav_button_controls(2500);
			break;
			case 12:
				sound_player("s5_p"+(countNext+1),1);
			$('.marbles1,.marbles2,.marbles3,.marbles4,.marbles5,.marbles6').css('display','none');
			$('.marbles1,.marbles2,.marbles3,.marbles4,.marbles5,.marbles6').fadeIn(1000);
			$('.six_marble').fadeOut(1000);
			// nav_button_controls(2500);
			break;
			case 14:
				sound_player("s5_p"+(countNext+1),1);
				$('.marbles1,.marbles2,.marbles3,.marbles4,.marbles5,.marbles6').animate({"top": "70%",
		    "left": "19%",
		    "opacity": "0"},2000);
				setTimeout(function() {
					$('.empty_jar').fadeOut(1000);
				},1500);
				// nav_button_controls(2500);
				break;
			case 15:
				sound_player("s5_p"+(countNext+1),1);
				$('.marbles1,.marbles2,.marbles3,.marbles4,.marbles5,.marbles6').css('display','none');
				$('.empty_jar').hide(0);
				// nav_button_controls(2500);
				break;
			case 16:
				sound_player("s5_p"+(countNext+1),1);
			$('.left_dialogue').css('width','40%');
			// nav_button_controls(1000);
			break;
			case 17:
				sound_player("s5_p"+(countNext+1),1);
				$(".greyBg").hide(0);
				$(".greyBg:eq(0)").fadeIn(300, function(){
					$(".greyBg:eq(1)").delay(1000).fadeIn(300);
				});
				// nav_button_controls(2000);
			break;
			case 18:
				sound_player("s5_p"+(countNext+1),1);
				$(".greyBg_1").hide(0);
				$(".greyBg_1:eq(0)").fadeIn(300, function(){
					$(".greyBg_1:eq(1)").delay(1000).fadeIn(300);
				});
				// nav_button_controls(2000);
			break;
			default:
				sound_player("s5_p"+(countNext+1),1);
			// nav_button_controls(1000);
			break;

		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls():'';
		});
	}

	function sound_play_click(sound_id, click_class){
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image1(content, count){
		if(content[count].hasOwnProperty('marbeldiv')){
			var marbeldiv = content[count].marbeldiv;
				for(var i=0; i<marbeldiv.length; i++){
					var image_src = preload.getResult(marbeldiv[i].imgid).src;
					//get list of classes
					var classes_list = marbeldiv[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('marbeldivother')){
			var marbeldivother = content[count].marbeldivother;
				for(var i=0; i<marbeldivother.length; i++){
					var image_src = preload.getResult(marbeldivother[i].imgid).src;
					//get list of classes
					var classes_list = marbeldivother[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
		}
	}

	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
			for (var i = 0; i < content.length; i++) {
				slides(i);
				$($('.totalsequence')[i]).html(i);
				$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
			"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
			}
			function slides(i){
					$($('.totalsequence')[i]).click(function(){
						countNext = i;
						templateCaller();
					});
				}
		*/
	}


	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

Array.prototype.shufflearray = function(){
	var i = this.length, j, temp;
	while(--i > 0){
		j = Math.floor(Math.random() * (i+1));
		temp = this[j];
		this[j] = this[i];
		this[i] = temp;
	}
	return this;
}

var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	//slide 1
	{
		additionalclasscontentblock: "contentwithbg",

		uppertextblock: [{
			textdata: data.string.toptextt,
			topclass : "toptext"
		}],

		exerciseblock: [
		{
			textdata: "",
			imageoptions: [
			{
				forshuffle: "evenNumber",
				imgsrc: imgpath + "basket.png",
				imglabel: "even",
				labeltext: data.string.eventext
			},
			{
				forshuffle: "oddNumber",
				imgsrc: imgpath + "basket.png",
				imglabel: "odd",
				labeltext: data.string.oddtext
			},

			]
		}
		]
	},
	];

	$(function () {
		var $board    = $('.board');
		var $nextBtn = $("#activity-page-next-btn-enabled");
		var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
		var countNext = 0;

		/*for limiting the questions to 10*/
		var $total_page = 10;
		//loadTimelineProgress($total_page,countNext+1);

		var preload;
		var timeoutvar = null;
		var current_sound;

		function init() {
			//specify type otherwise it will load assests as XHR
			manifest = [
				// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
				// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
				//   ,
				//images
				// sounds
				{id: "exer", src: soundAsset+"ex.ogg"},

			];
			preload = new createjs.LoadQueue(false);
			preload.installPlugin(createjs.Sound);//for registering sounds
			preload.on("progress", handleProgress);
			preload.on("complete", handleComplete);
			preload.on("fileload", handleFileLoad);
			preload.loadManifest(manifest, true);
		}
		function handleFileLoad(event) {
			// console.log(event.item);
		}
		function handleProgress(event) {
			$('#loading-text').html(parseInt(event.loaded*100)+'%');
		}
		function handleComplete(event) {
			$('#loading-wrapper').hide(0);
			//initialize varibales
			current_sound = createjs.Sound.play('sound_1');
			current_sound.stop();
			// call main function
			templateCaller();
		}
		//initialize
		init();


		function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
	 }

	 var score = 0;

	 var finishedAns = [];

	 /*createing an array which has 10 random numbers of minimum 4 and maximum 6 digits. It has 5 even and 5 odd numbers.*/
	 var QuesArray = [];

	  var testin = new EggTemplate();

        //eggTemplate.eggMove(countNext);
   	 	testin.init(10);

	 function getEven(){
	 	var evenRandom = Math.floor(Math.random() * (999999 - 1000) + 1000);
	 	if(evenRandom%2 == 0)
	 		return evenRandom;
	 	else
	 		return evenRandom - 1;
	 }

	 function getOdd(){
	 	var evenRandom = Math.floor(Math.random() * (999999 - 1000) + 1000);
	 	if(evenRandom%2 != 0)
	 		return evenRandom;
	 	else
	 		return evenRandom + 1;
	 }

	 for(i = 0; i < 5; i++){
	 	QuesArray[i] = {number:getEven(), type: "even"};
			//console.log(QuesArray[i]);
		}

		for(i = 0; i < 5; i++){
			QuesArray[i+5] = {number:getOdd(), type: "odd"}
		}
		QuesArray.shufflearray();
		/*create end*/

		var evenCount = 0;
		var oddCount = 0;

		function generalTemplate() {
			var source = $("#general-template").html();
			var template = Handlebars.compile(source);
			var html = template(content[0]);
			$board.html(html);

			$nextBtn.hide(0);
			$prevBtn.hide(0);
			$('.congratulation').hide(0);
			$('.exefin').hide(0);

			for (var i=0; i<finishedAns.length; i++){
			//alert(finishedAns[i].cloud);
			$('.'+finishedAns[i].cloud).show(0);
			$('.'+finishedAns[i].cloud).html(finishedAns[i].value);
		}
		//for sound
		if(countNext==0){
			sound_player("exer");
		}
		/*======= SCOREBOARD SECTION ==============*/

		/*random scoreboard eggs*/
		var ansClicked = false;
		var wrngClicked = false;


		$(".evenNumber, .even").click(function(){
			if(QuesArray[countNext].type == "even"){
				$('.question').removeClass('addNo shakeItem').addClass('evenNo');
				$('.evenChild'+evenCount).html(QuesArray[countNext].number).delay(2000).show(0);
				finishedAns[countNext] = {cloud:"evenChild" + evenCount, value: QuesArray[countNext].number};           ;
				evenCount++;
				$('.evenNumber, .even').off();
				$('.oddNumber, .odd').off();
				setTimeout(function() {
					$nextBtn.trigger("click");
				//$('.evenNumber, .even').click(true);
				}, 2000);
				if(wrngClicked == false){
					testin.update(true);
				}
				$('.buttonsel').removeClass('forhover forhoverimg');
				ansClicked = true;
				play_correct_incorrect_sound(1);
			}
			else{
				testin.update(false);
				play_correct_incorrect_sound(0);
				$('.question').addClass('shakeItem');
				$(this).siblings(".wrngopt").css("visibility","visible");
				wrngClicked = true;
			}
		});


		$(".oddNumber, .odd").click(function(){
			if(QuesArray[countNext].type == "odd"){
				$('.question').removeClass('evenNo shakeItem').addClass('oddNo');
				$('.oddChild'+oddCount).html(QuesArray[countNext].number).delay(2000).show(0);
				finishedAns[countNext] = {cloud:"oddChild" + oddCount, value: QuesArray[countNext].number};           ;
				//alert(finishedAns[countNext]);
				oddCount++;
				$('.evenNumber, .even').off();
				$('.oddNumber, .odd').off();
				setTimeout(function() {
					$nextBtn.trigger("click");
				}, 2000);
				if(wrngClicked == false){
					testin.update(true);
				}
				play_correct_incorrect_sound(1);
				$('.buttonsel').removeClass('forhover forhoverimg');
				ansClicked = true;

				//if(countNext != $total_page)
					//$nextBtn.delay(2000).show(0);
			}
			else{
				testin.update(false);
				play_correct_incorrect_sound(0);
				$('.question').addClass('shakeItem');
				$(this).siblings(".wrngopt").css("visibility","visible");
				wrngClicked = true;
			}
		});

		/*======= SCOREBOARD SECTION ==============*/
		$('.question').html(QuesArray[countNext].number);

	}

	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators
		//loadTimelineProgress($total_page,countNext+1);

	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		testin.gotoNext();
		countNext++;
		if(countNext<10){
			templateCaller();
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/*=====  End of Templates Controller Block  ======*/
});

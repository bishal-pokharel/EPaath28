var imgpath = $ref + '/images/benchgame/';
var imgpath2 = $ref + '/images/';
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content;
content = [
	{
	    // starting page
	    contentblockadditionalclass : 'firstpagebackground',
	    centertextblock: [{
	        textclass: 'chapter-title',
	        textdata: data.string.chapter_title
	    }],
	    imageblock: [
			{
				imagestoshow: [
				{
					imgclass: 'firstpageimage',
					imgsrc: imgpath2 + "even_and_odd.png",
				}
				],
			}
		],
	},

	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bgbg',

		extraspanblock : [
			{
				textdata : data.string.p2_text4,
				textclass : 'description_yellow ',
				datahighlightflag:true,
				datahighlightcustomclass:'pair'
			},
			{
				textdata : data.string.p2_text5,
				textclass : 'example_yellow_1 my_font_big fade_in_1_11'
			}
		],
		imageblock: [
			{
				imagestoshow: [
				{
					imgclass: 'socks_1 fade_in_1_1',
					imgsrc: imgpath2 + "socks.png",
				}
				],
			}
		],
	},

	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bgbg',

		extraspanblock : [
			{
				textdata : data.string.p2_text6,
				textclass : 'description_yellow '
			},
			{
				textdata : data.string.p2_text7,
				textclass : 'example_yellow_1 my_font_big fade_in_1_11'
			}
		],
		imageblock: [
			{
				imagestoshow: [
				{
					imgclass: 'socks_1_odd fade_in_1_1',
					imgsrc: imgpath2 + "sock.png",
				}
				],
			}
		],
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bgbg',

		extraspanblock : [
			{
				textdata : data.string.p2_text9,
				textclass : 'example_yellow  odd1',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_red'
			},
			{
				textdata : data.string.p2_text8,
				textclass : 'description_yellow1 my_font_big fade_in_1_11',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_red'
			}
		],
		imageblock: [
			{
				imagestoshow: [
				{
					imgclass: 'socks_1_odd red_border fade_in_1_1',
					imgsrc: imgpath2 + "sock.png",
				}
				],
			}
		],
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bgbg',

		extraspanblock : [
			{
				textdata : data.string.p1text1,
				textclass : 'example_yellow   odd1',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_green'
			},
			{
				textdata : data.string.p1text2,
				textclass : 'example_yellow_1 my_font_big fade_in_1_11',
				datahighlightflag : true,
				datahighlightcustomclass : 'color-green'
			}
		],
		imageblock: [
			{
				imagestoshow: [
				{
					imgclass: 'socks_1_odd border-green fade_in_1_1',
					imgsrc: imgpath2 + "socks.png",
				}
				],
			}
		],
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bgbg',


		extraspanblock : [
			{
				textdata : data.string.p1text3,
				textclass : 'example_yellow  odd1',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_red'
			},
		],
		imageblock: [
			{
				imagestoshow: [
				{
					imgclass: 'socks_1_even border-green fade_in_1_1 sock1',
					imgsrc: imgpath2 + "socks.png",
				},
				{
					imgclass: 'socks_2_odd red_border fade_in_1_1 sock2',
					imgsrc: imgpath2 + "sock_2.png",
				}
				],
			}
		],
	},
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bgbg',
		extraspanblock : [
			{
				textdata : data.string.p2_text14,
				textclass : 'example_yellow my_font_big  odd1',
				datahighlightflag : true,
				datahighlightcustomclass : 'color-green'
			},
			{
				textdata : data.string.p2_text15,
				textclass : 'example_yellow_1 my_font_big fade_in_1_11 ',
				datahighlightflag : true,
				datahighlightcustomclass : 'color-green',
			}
		],
		imageblock: [
			{
				imagestoshow: [
				{
					imgclass: 'socks_1_even border-green fade_in_1_1 sock1position1',
					imgsrc: imgpath2 + "socks.png",
				},
				{
					imgclass: 'socks_2_even border-green fade_in_1_1 sock1position2',		imgsrc: imgpath2 + "socks_2.png",
				}
				],
			}
		],
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bgbg',

		extraspanblock : [
			{
				textdata : data.string.p2_text16,
				textclass : 'example_yellow my_font_big  odd1',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_red'
			},
			{
				textdata : data.string.p2_text17,
				textclass : 'example_yellow_1 my_font_big fade_in_1_11',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_red'
			}
		],
		imageblock: [
			{
				imagestoshow: [
				{
					imgclass: 'socks_1_even border-green fade_in_1_1',
					imgsrc: imgpath2 + "socks.png",
				},
				{
					imgclass: 'socks_2_even border-green fade_in_1_1',
					imgsrc: imgpath2 + "socks_2.png",
				},
				{
					imgclass: 'socks_3_odd red_border fade_in_1_1',
					imgsrc: imgpath2 + "sock_3.png",
				}
				],
			}
		],
	},
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bgbg',

		extraspanblock : [
			{
				textdata : data.string.p2_text18,
				textclass : 'example_yellow my_font_big  odd1',
				datahighlightflag : true,
				datahighlightcustomclass : 'color-green'
			},
			{
				textdata : data.string.p2_text19,
				textclass : 'example_yellow_1 my_font_big fade_in_1_11',
				datahighlightflag : true,
				datahighlightcustomclass : 'color-green'
			}
		],
		imageblock: [
			{
				imagestoshow: [
				{
					imgclass: 'socks_4_even border-green fade_in_1_1',
					imgsrc: imgpath2 + "socks.png",
				},
				{
					imgclass: 'socks_5_even border-green fade_in_1_1',
					imgsrc: imgpath2 + "socks_2.png",
				},
				{
					imgclass: 'socks_6_even border-green fade_in_1_1',
					imgsrc: imgpath2 + "socks_3.png",
				},
				],
			}
		],
	},
	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bgbg',

		extraspanblock : [
			{
				textdata : data.string.p2_text20,
				textclass : 'example_yellow_1 my_font_big odd1',
				datahighlightflag : true,
				datahighlightcustomclass : 'color-green'
			}
		],
		imageblock: [
			{
				imagestoshow: [
				{
					imgclass: 'socks_4_even border-green',
					imgsrc: imgpath2 + "socks.png",
				},
				{
					imgclass: 'socks_5_even border-green',
					imgsrc: imgpath2 + "socks_2.png",
				},
				{
					imgclass: 'socks_6_even border-green',
					imgsrc: imgpath2 + "socks_3.png",
				},
				],
			}
		],
	},
	//slide10
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bgbg',
		extraspanblock : [
			{
				textdata : data.string.p2_text21,
				textclass : 'example_yellow_1 my_font_big',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_red'
			}
		],
		imageblock: [
			{
				imagestoshow: [
				{
					imgclass: 'socks_4_even border-green',
					imgsrc: imgpath2 + "socks.png",
				},
				{
					imgclass: 'socks_5_even border-green',
					imgsrc: imgpath2 + "socks_2.png",
				},
				{
					imgclass: 'socks_7_even red_border',
					imgsrc: imgpath2 + "sock_3.png",
				}
				],
			}
		],
	},
	{
	    // slide 11
	    contentblockadditionalclass: 'bg_block',
	    uppertextblock: [{
	        textdata: data.string.p2text2
	    }],
	    extratextblock : [
			{
				textdata : data.string.p2_text23,
				textclass : 'extrafont',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_red'
			}
		],
	    draggableblock: [{
	        // draggables: [{
	        //     imgsrc: imgpath + 'deepa.png'
	        // }]
	    }],
	    droppableblock: [{
	    }]
	},
	{
	    // slide 12
	    contentblockadditionalclass: 'bg_block',
	    uppertextblock: [{
	        textdata: data.string.p2text2
	    }],
	    extratextblock : [
			{
				textdata : data.string.p2_text24,
				textclass : 'extrafont',
				datahighlightflag : true,
				datahighlightcustomclass : 'color-green'
			}
		],
	    draggableblock: [{
	        // draggables: [{
	        //     imgsrc: imgpath + 'deepa.png'
	        // }]
	    }],
	    droppableblock: [{
	    }]
	}
];


// }

$(function() {
    // var height = $(window).height();.
    // if(data.string != null){
    // } else{
    // recursion();
    // }
    // }
    // recursion();
    // objectifyActivityData("data.xml");

    function recalculateHeightWidth() {
        var heightresized = $(window).height();
        var widthresized = $(window).width();
        var factor = 960 / 580;
        var equivalentwidthtoheight = widthresized / factor;

        if (heightresized >= equivalentwidthtoheight) {
            $(".shapes_activity").css({
                "width": widthresized,
                "height": equivalentwidthtoheight
            });
        } else {
            $(".shapes_activity").css({
                "height": heightresized,
                "width": heightresized * 960 / 580
            });
        }
        // $(".shapes_activity").css({"left": "50%" ,
        // "height": "50%" ,
        // "-webkit-transform" : "translate(-50%, - 50%)",
        // "transform" : "translate(-50%, - 50%)"});
    }
    var $board = $(".board");
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    recalculateHeightWidth();

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

		var preload;
		var timeoutvar = null;
		var current_sound;

		function init() {
			//specify type otherwise it will load assests as XHR
			manifest = [
				// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
				// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
				//   ,
				//images
				// sounds
				{id: "sound_0", src: soundAsset+"s1_p1.ogg"},
				{id: "sound_1a", src: soundAsset+"s1_p2.ogg"},
				{id: "sound_1b", src: soundAsset+"s1_p2_1.ogg"},
				{id: "sound_2a", src: soundAsset+"s1_p3.ogg"},
				{id: "sound_2b", src: soundAsset+"s1_p3_1.ogg"},
				{id: "sound_3a", src: soundAsset+"s1_p4.ogg"},
				{id: "sound_3b", src: soundAsset+"s1_p4_1.ogg"},
				{id: "sound_4a", src: soundAsset+"s1_p5.ogg"},
				{id: "sound_4b", src: soundAsset+"s1_p5_1.ogg"},
				{id: "sound_5", src: soundAsset+"s1_p6.ogg"},
				{id: "sound_6a", src: soundAsset+"s1_p7.ogg"},
				{id: "sound_6b", src: soundAsset+"s1_p7_1.ogg"},
				{id: "sound_7a", src: soundAsset+"s1_p8.ogg"},
				{id: "sound_7b", src: soundAsset+"s1_p8_1.ogg"},
				{id: "sound_8a", src: soundAsset+"s1_p9.ogg"},
				{id: "sound_8b", src: soundAsset+"s1_p9_1.ogg"},
				{id: "sound_9", src: soundAsset+"s1_p10.ogg"},
				{id: "sound_10", src: soundAsset+"s1_p11.ogg"},
				{id: "sound_11", src: soundAsset+"s1_p12.ogg"},
				{id: "sound_12", src: soundAsset+"p1_s12.ogg"},
				{id: "sound_13", src: soundAsset+"p1_s13.ogg"},
			];
			preload = new createjs.LoadQueue(false);
			preload.installPlugin(createjs.Sound);//for registering sounds
			preload.on("progress", handleProgress);
			preload.on("complete", handleComplete);
			preload.on("fileload", handleFileLoad);
			preload.loadManifest(manifest, true);
		}
		function handleFileLoad(event) {
			// console.log(event.item);
		}
		function handleProgress(event) {
			$('#loading-text').html(parseInt(event.loaded*100)+'%');
		}
		function handleComplete(event) {
			$('#loading-wrapper').hide(0);
			//initialize varibales
			current_sound = createjs.Sound.play('sound_1');
			current_sound.stop();
			// call main function
			templateCaller();
		}
		//initialize
		init();

    /*
    	inorder to use the handlebar partials we need to register them
    	to their respective handlebar partial pointer first
    */
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

    /**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templateCaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templateCaller function
	 - If the total page number is
	 */

	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

	}

    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);

        $board.html(html);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);
				vocabcontroller.findwords(countNext);

        var $draggableBlock = $('.draggableblock');
        var $droppableBlock = $('.droppableblock');

        // List of image names
        var draggableNames;

        var new_min_max;
        var noOfDraggables;
        var sitCount;


        switch (countNext) {
					case 1:
					sound_player1("sound_1a","sound_1b");
					break;
					case 2:
					sound_player1("sound_2a","sound_2b");
					break;
					case 3:
					sound_player1("sound_3a","sound_3b");
					break;
					case 4:
					sound_player1("sound_4a","sound_4b");
					break;
					case 6:
					sound_player1("sound_6a","sound_6b");
					break;
					case 7:
					sound_player1("sound_7a","sound_7b");
					break;
					case 8:
					sound_player1("sound_8a","sound_8b");
					break;
            case 11:
						sound_nav("sound_"+countNext);
	            draggableNames = [
		            'deepa',
		            'deepak',
		            'kabita',
		            'partik',
		            'raj',
		            'raju',
		            'reena',
		            'rita',
		            'saroj',
		            'samjana'
		        ];

		        // inserting imgpath before every image name
		        draggables = draggableNames.map(function(imgSrc) {
		            return imgpath + imgSrc + '.png';
		        });
	            var new_min_max = [7,9];
		        noOfDraggables = new_min_max[0];
		        $('.init_people').html(noOfDraggables);
		        sitCount = 0;

                makeDraggables();
                makeDroppables();
                initDraggables();
                initDroppables();
                break;

            case 12:
	            draggableNames = [
		            'deepa',
		            'deepak',
		            'kabita',
		            'partik',
		            'raj',
		            'raju',
		            'reena',
		            'rita',
		            'saroj',
		            'samjana'
		        ];

		        // inserting imgpath before every image name
		        draggables = draggableNames.map(function(imgSrc) {
		            return imgpath + imgSrc + '.png';
		        });
	            var new_min_max = [8, 10];
		        noOfDraggables = new_min_max[0];
		        $('.init_people').html(noOfDraggables);
		        sitCount = 0;

                makeDraggables();
                makeDroppables();
                initDraggables();
                initDroppables();
                break;

            default:
          	sound_player("sound_"+(countNext));
            	break;
        }

        function initDraggables() {
            var $draggables = $('.draggable');
            $draggables.draggable({
                containment: $('.contentblock'),
                cursor: 'grabbing',
                revert: true,
                zIndex: 100
            });
        }

        function initDroppables() {
            var $droppables = $('.droppable');

            $onePairLabel = $('<p>'+data.string.p2_text25 +'</p>');
            $noPairLabel = $('<p>'+data.string.p2_text26 +'</p>');
            $droppables.droppable({
                // accept: '.draggable',
                drop: function(event, ui) {
                    var $this = $(this); // caching this
                    var $draggable = ui.draggable;
                    var draggableId = $draggable.attr('id');
                    $draggable.remove();
                    sitCount++;

                    $sitImg = $('<img>', {'src': imgpath + draggableId + 'sit.png'});
                    var hasChildren = $this.children().length > 0;
                    if (hasChildren) {
                        $sitImg.css({'right': '10%'});
                        $this.append($onePairLabel.clone());
                    } else {
                        $sitImg.css({'left': '10%'});
                    }
                    if (sitCount >= noOfDraggables) {
                    	i_have_noone();
											if(countNext==11){
												sound_nav('sound_12');
											}
											if(countNext==12){
												sound_nav('sound_13');
											}
                    	$('.extrafont').fadeIn(500);
                		nav_button_controls(1000);
                    }

                    function i_have_noone(){
                    	for(var mmm=0; mmm<5; mmm++){
                    		if($('.droppable').eq(mmm).children().length<2){
                    			$('.droppable').eq(mmm).append($noPairLabel);
                    		}
                    	}
                    }

                    $this.append($sitImg);

                    if (hasChildren) {
                        $this.droppable('disable');
                    }
                }
            });
        }

        function makeDraggables() {
            var xs = [];
            for (var i = 1; i <= 10; i++) {
                xs.push(i * 8);
            }
            xs = shuffle(xs);

            var ys = [30, 35, 40];

            var $img;
            for (var i = 0; i < noOfDraggables; i++) {
                $img = $('<img>', {'src': draggables[i],
                                'class': 'draggable',
                                'id': draggableNames[i]});

                var x = xs.shift();
                var y = randomFromArray(ys);

                $img.css({
                    top: y + '%',
                    left: x + '%'
                });

                $draggableBlock.append($img);
            }
        }

        function makeDroppables() {
            var noOfDroppables = Math.ceil(noOfDraggables / 2);

            var positions = [[5,10], [23,35], [41,10], [59,35], [77,5]];

            positions = shuffle(positions);

            var pos, $droppable;
            for (var i = 0; i < noOfDroppables; i++) {
                pos = positions.shift();

                $droppable = $('<div>', {'class': 'droppable'});
                $droppable.css({
                    'left': pos[0] + '%',
                    'top': pos[1] + '%'
                });
                $droppableBlock.append($droppable);
            }
        }

        /**
         * Utility function to shuffle array
         */
        function shuffle(array) {
            return array.sort(function() {
                return 0.5 - Math.random();
            });
        }

        /**
         * Utility function to get random element from an array
         */
        function randomFromArray(array) {
            return array[Math.floor(Math.random()*array.length)];
        }

    }
    // Handlebars.registerHelper('listItem', function (from, to, context, options){
    // var item = "";
    // for (var i = from, j = to; i <= j; i++) {
    // item = item + options.fn(context[i]);
    // }
    // return item;
    // });


	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(0);
		});
	}
	function sound_nav(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player1(sound_id, sound_id1){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id1);
			$(".fade_in_1_11").show(0);
			current_sound.play();
			current_sound.on('complete', function(){
			nav_button_controls(0);
		});
	});
	}
    function templateCaller() {
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

        loadTimelineProgress($total_page, countNext + 1);

        navigationController();

        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;Math.round(Math.random())
		        templateCaller();
		        templateCaller();initDroppables
		      });
		    }
		*/


    }

    $nextBtn.on("click", function() {
			createjs.Sound.stop();
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
        countNext--;
        templateCaller();

        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });
    // setTimeout(function(){
    total_page = content.length;
    templateCaller();
    // }, 250);
});

/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

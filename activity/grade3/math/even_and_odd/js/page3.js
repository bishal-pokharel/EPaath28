var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var	content = [
  //slide 0
    {
      contentblockadditionalclass : 'bgbg',
      numberblock: [{
          number: '358758',
          numberLabel: data.string.evened
      }],
      definitionblock: [{
          textdata: data.string.p3_text9
      }]
    },{
      contentblockadditionalclass : 'bgbg',
      numberblock: [{
          number: '358753',
          numberLabel: data.string.odded
      }],
      definitionblock: [{
          textdata: data.string.p3_text10
      }]
    },
      //slide 2
        {
      uppertextblock: [{
          textdata: data.string.p4_text1
      }],
      draggableblock: [{}],
      droppableblock: [{
          textdata: data.string.evened,
          divclass: 'even-droppable',
          textclass : 'ya'
      }, {
          textdata: data.string.odded,
          divclass: 'odd-droppable',
          textclass : 'yo'
      }]
    }
];


$(function() {
	// var height = $(window).height();
	// var width = $(window).width();
	// $("#board").css({"width": width, "height": (height*580/960)});
	// function recursion(){
	// if(data.string != null){
	// } else{
	// recursion();
	// }
	// }
	// recursion();

	function recalculateHeightWidth() {
		var heightresized = $(window).height();
		var widthresized = $(window).width();
		var factor = 960 / 580;
		var equivalentwidthtoheight = widthresized / factor;

		if (heightresized >= equivalentwidthtoheight) {
			$(".shapes_activity").css({
				"width": widthresized,
				"height": equivalentwidthtoheight
			});
		} else {
			$(".shapes_activity").css({
				"height": heightresized,
				"width": heightresized * 960 / 580
			});
		}
		// $(".shapes_activity").css({"left": "50%" ,
		// "height": "50%" ,
		// "-webkit-transform" : "translate(-50%, - 50%)",
		// "transform" : "translate(-50%, - 50%)"});

        // resizing fontsize of number with respect to numberblock
        var $num = $('p.number');
        var $numberBlock = $('.numberblock');
        var fontSize = parseInt($numberBlock.height() * 0.57)+'px';
        $num.css({'font-size': fontSize});

	}
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	recalculateHeightWidth();

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// sounds
			{id: "sound_0", src: soundAsset+"s3_p1.ogg"},
			{id: "sound_1", src: soundAsset+"s3_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s3_p3.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
	*/
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


    /**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templateCaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templateCaller function
	 - If the total page number is
	 */

	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		$(".midtext2").css({
			"font-size":"1.7em"
		});
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

        var $tableBlock = $('.tableblock');
        var $tableCover = $('.table-cover');
        var $definitionblock = $('.definitionblock');
        var $oddDigitCover = $('.odd-digit-cover');
        var $evenDigitCover = $('.even-digit-cover');
        $oddDigitCover.hide(0);
        $evenDigitCover.hide(0);
        $prevBtn.hide(0);
        $nextBtn.hide(0);

		switch (countNext) {
            case 0:
                var $digitBorder = $('.digitborder');
                var $numberLabel = $('.number-label');

                $digitBorder.animate({
                    'width': '100%'
                }, 2000);

                $numberLabel.animate({
                    'right': '40%'
                }, 2000);
                sound_player("sound_"+(countNext));
                break;
              case 1:
              var $digitBorder = $('.digitborder');
              var $numberLabel = $('.number-label');

              $digitBorder.css({
                  'border-color': '#FF93C2'
              });

              $digitBorder.animate({
                  'width': '100%'
              }, 2000);


              $numberLabel.animate({
                  'right': '40%'
              }, 2000);
              sound_player("sound_"+(countNext));
              break;
            case 2:
            sound_player("sound_"+(countNext));
                var $draggableBlock = $('.draggableblock');
                var $evenDroppable = $('.even-droppable');
                var $oddDroppable = $('.odd-droppable');
                var drop_count = 0;

                for (var i = 1; i <= 20; i++ ) {
                    var randomNum = random(100,999);
                    var $num = $('<span>', {class: 'number'});
                    if (randomNum % 2 === 0) {
                        $num.addClass('even')
                    } else {
                        $num.addClass('odd')
                    }
                    $num.html(randomNum);
                    $draggableBlock.append($num);
                }

                var $number = $('.number');
                $number.draggable({
                    containment: $('.contentblock'),
                    cursor: 'grabbing',
					revert : "invalid",
                    helper : "clone",
					zindex : 1000,
					start: function(event, ui){
						$(this).css({"opacity": "0"});
					},
					stop: function(event, ui){
						$(this).css({"opacity": "1"});
					}
                });

                var evenDropCount = 0;
                var oddDropCount = 0;
                var oddBottomValue = 20;
                var evenBottomValue = 20;

                // droppable for even
                $evenDroppable.droppable({
                    accept: '.even',
                    drop: function(event, ui) {
                        var $this = $(this);
                        var $draggable = ui.draggable;
                        $draggable.remove();
                        drop_count++;

                        var $droppedNum = $('<span>' + $draggable.html() + '</span>');
                        $droppedNum.css({
                            'position': 'absolute',
                            'left': evenDropCount % 2 === 0 ? '30%': '57%',
                            'bottom': evenBottomValue + '%',
                            'font-size': '0.9em',
                            'color' : 'white'
                        });
                        play_correct_incorrect_sound(true);
                        evenDropCount++;
                        if (evenDropCount % 2 === 0) evenBottomValue += 10;
                        $this.append($droppedNum);


                        if(drop_count >= 20){
                        	nav_button_controls(400);
                        }
                    }
                });

                // Droppable for odd
                $oddDroppable.droppable({
                    accept: '.odd',
                    drop: function(event, ui) {
                    	drop_count++;
                        var $this = $(this);
                        var $draggable = ui.draggable;
                        $draggable.remove();

                        var $droppedNum = $('<span>' + $draggable.html() + '</span>');
                        $droppedNum.css({
                            'position': 'absolute',
                            'left': oddDropCount % 2 === 0 ? '30%': '57%',
                            'bottom': oddBottomValue + '%',
                            'font-size': '0.9em',
                            'color' : 'white'

                        });
                        play_correct_incorrect_sound(true);
                        oddDropCount++;
                        if (oddDropCount % 2 === 0) oddBottomValue += 10;
                        $this.append($droppedNum);

                        if(drop_count >= 20){
                        	nav_button_controls(400);
                        }
                    }
                });

                break;

		}

        function random(min, max) {
            return parseInt(Math.random() * (max - min) + min);
        }

	}
  function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
      if(countNext==2){
        $prevBtn.hide(0);
        $nextBtn.hide(0);
      }else{
        nav_button_controls(0);
      }
		});
	}
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page,countNext+1);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function() {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {var $tableBlock = $('.tableblock');
		countNext--;
		templateCaller();

        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
	 =            data highlight function            =
	 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ?
		alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";


	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				(stylerulename = $(this).attr("data-highlightcustomclass")) :
				(stylerulename = "parsedstring");

            texthighlightstarttag = "<span class='"+stylerulename+"'>";


			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);


			$(this).html(replaceinstring);
		});
	}
}
/*=====  End of data highlight function  ======*/

//page 1

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
	//slide0
	{
		contentblockadditionalclass:'ole-background-gradient-paperlinings',
		extratextblock : [{
			textdata : data.string.exdata1,
			textclass : 'instruction',
		},
		{
			textdata : data.string.exrs,
			textclass : 'question',
		},
		{
			textdata : data.string.exp,
			textclass : 'answer',
		},
		{
			textdata : data.string.excheck,
			textclass : 'checkbutton',
		}],
		input:[{
			inputclass: 'inputformclass input-1',
			inputdata: '0'
		}],
	},
	//slide1
	{
		contentblockadditionalclass:'ole-background-gradient-paperlinings',
		extratextblock : [{
			textdata : data.string.exdata2,
			textclass : 'instruction',
		},
		{
			textdata : data.string.exrs,
			textclass : 'question',
		},
		{
			textdata : data.string.exp,
			textclass : 'answer',
		},
		{
			textdata : data.string.excheck,
			textclass : 'checkbutton',
		}],
		input:[{
			inputclass: 'inputformclass input-1',
			inputdata: '0'
		}],
	},
	//slide2
	{
		contentblockadditionalclass:'ole-background-gradient-paperlinings',
		extratextblock : [{
			textdata : data.string.exdata3,
			textclass : 'instruction',
		},
		{
			textdata : data.string.exrsp,
			textclass : 'question',
		},
		{
			textdata : data.string.exp,
			textclass : 'answer',
		},
		{
			textdata : data.string.excheck,
			textclass : 'checkbutton',
		}],
		input:[{
			inputclass: 'inputformclass input-1',
			inputdata: '0'
		}],
	},


	//slide3
	{
		contentblockadditionalclass:'ole-background-gradient-paperlinings',
		extratextblock : [{
			textdata : data.string.exdata4,
			textclass : 'instruction',
		},
		{
			textdata : data.string.exp,
			textclass : 'question',
		},
		{
			textdata : data.string.exrs,
			textclass : 'ans ans1',
		},
		{
			textdata : data.string.exp,
			textclass : 'ans ans2',
		},
		{
			textdata : data.string.excheck,
			textclass : 'checkbutton',
		}],
		input:[{
			inputclass: 'inputformclass input-1',
			inputdata: '0'
		},{
			inputclass: 'inputformclass input-2',
			inputdata: '0'
		}],
	},

	//slide4
	{
		contentblockadditionalclass:'ole-background-gradient-paperlinings',
		extratextblock : [{
			textdata : data.string.exdata5,
			textclass : 'instruction',
		},
		{
			textdata : data.string.exp,
			textclass : 'question',
		},
		{
			textdata : data.string.exrs,
			textclass : 'ans ans1',
		},
		{
			textdata : data.string.exp,
			textclass : 'ans ans2',
		},
		{
			textdata : data.string.excheck,
			textclass : 'checkbutton',
		}],
		input:[{
			inputclass: 'inputformclass input-1',
			inputdata: '0'
		},{
			inputclass: 'inputformclass input-2',
			inputdata: '0'
		}],
	},
	//slide5
	{
		contentblockadditionalclass:'ole-background-gradient-paperlinings',
		extratextblock : [{
			textdata : data.string.exdata6,
			textclass : 'instruction',
		},
		{
			textdata : data.string.exp,
			textclass : 'question',
		},
		{
			textdata : data.string.exrs,
			textclass : 'ans ans1',
		},
		{
			textdata : data.string.exp,
			textclass : 'ans ans2',
		},
		{
			textdata : data.string.excheck,
			textclass : 'checkbutton',
		}],
		input:[{
			inputclass: 'inputformclass input-1',
			inputdata: '0'
		},{
			inputclass: 'inputformclass input-2',
			inputdata: '0'
		}],
	},

	//slide6
	{
		contentblockadditionalclass:'ole-background-gradient-paperlinings',
		extratextblock : [{
			textdata : data.string.exdata7,
			textclass : 'instruction',
		},

		{
			textdata : data.string.exrs,
			textclass : 'qstr r0-c0',
		},
		{
			textdata : data.string.exp,
			textclass : 'qstr r0-c1',
		},
		{
			textdata : '1',
			textclass : 'qstr r1-c0',
		},
		{
			textdata : '2',
			textclass : 'qstr r1-c1',
		},
		{
			textdata : '3',
			textclass : 'qstr r2-c0',
		},
		{
			textdata : '4',
			textclass : 'qstr r2-c1',
		},
		{
			textdata : '+',
			textclass : 'qstr sign-place',
		},
		{
			textdata : '',
			textclass : 'divide-line',
		},



		{
			textdata : data.string.exrs,
			textclass : 'ans ans1',
		},
		{
			textdata : data.string.exp,
			textclass : 'ans ans2',
		},
		{
			textdata : data.string.excheck,
			textclass : 'checkbutton',
		}],
		input:[{
			inputclass: 'inputformclass input-1',
			inputdata: '0'
		},{
			inputclass: 'inputformclass input-2',
			inputdata: '0'
		}],
	},
	//slide7
	{
		contentblockadditionalclass:'ole-background-gradient-paperlinings',
		extratextblock : [{
			textdata : data.string.exdata8,
			textclass : 'instruction',
		},
		{
			textdata : data.string.exrs,
			textclass : 'ans ans1',
		},
		{
			textdata : data.string.exp,
			textclass : 'ans ans2',
		},
		{
			textdata : data.string.excheck,
			textclass : 'checkbutton',
		}],
		input:[{
			inputclass: 'inputformclass input-1',
			inputdata: '0'
		},{
			inputclass: 'inputformclass input-2',
			inputdata: '0'
		}],
	},

	//slide8
	{
		contentblockadditionalclass:'ole-background-gradient-paperlinings',
		extratextblock : [{
			textdata : data.string.exdata9,
			textclass : 'instruction',
		},

		{
			textdata : data.string.exrs,
			textclass : 'qstr r0-c0',
		},
		{
			textdata : data.string.exp,
			textclass : 'qstr r0-c1',
		},
		{
			textdata : '1',
			textclass : 'qstr r1-c0',
		},
		{
			textdata : '2',
			textclass : 'qstr r1-c1',
		},
		{
			textdata : '3',
			textclass : 'qstr r2-c0',
		},
		{
			textdata : '4',
			textclass : 'qstr r2-c1',
		},
		{
			textdata : '-',
			textclass : 'qstr sign-place',
		},
		{
			textdata : '',
			textclass : 'divide-line',
		},



		{
			textdata : data.string.exrs,
			textclass : 'ans ans1',
		},
		{
			textdata : data.string.exp,
			textclass : 'ans ans2',
		},
		{
			textdata : data.string.excheck,
			textclass : 'checkbutton',
		}],
		input:[{
			inputclass: 'inputformclass input-1',
			inputdata: '0'
		},{
			inputclass: 'inputformclass input-2',
			inputdata: '0'
		}],
	},
	//slide9
	{
		contentblockadditionalclass:'ole-background-gradient-paperlinings',
		extratextblock : [{
			textdata : data.string.exdata10,
			textclass : 'instruction',
		},
		{
			textdata : data.string.exrs,
			textclass : 'ans ans1',
		},
		{
			textdata : data.string.exp,
			textclass : 'ans ans2',
		},
		{
			textdata : data.string.excheck,
			textclass : 'checkbutton',
		}],
		input:[{
			inputclass: 'inputformclass input-1',
			inputdata: '0'
		},{
			inputclass: 'inputformclass input-2',
			inputdata: '0'
		}],
	},
];


$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			// sounds
			{id: "qxn_0", src: soundAsset+"ex1_1.ogg"},
			{id: "qxn_3", src: soundAsset+"ex1_4.ogg"},
			{id: "qxn_6", src: soundAsset+"ex1_7.ogg"},
			{id: "qxn_8", src: soundAsset+"ex1_9.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	var scoring = new RhinoTemplate();
	scoring.init($total_page);
	var wrong_clicked = false;


	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		scoring.numberOfQuestions();

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		wrong_clicked = false;
		var correct_ans = 0;
		switch(countNext){
			case 0:
			case 1:
			case 2:
			sound_nav("qxn_"+(countNext));
				var rand_num = 2 + Math.floor(Math.random()*8);
				var rand_num2 = 10 + Math.floor(Math.random()*90);
				var rand_num3 = 100 + Math.floor(Math.random()*900);
				var quesdata = $('.question').html();
				if(countNext==0){
					$('.question').html(quesdata+rand_num);
					correct_ans = rand_num*100;
				} else if(countNext==1){
					$('.question').html(quesdata+rand_num2);
					correct_ans = rand_num2*100;
				} else{
					$('.fill-1').html(rand_num3);
					$('.fill-2').html(rand_num2);
					correct_ans = rand_num3*100+rand_num2;
				}
				input_box('.inputformclass', 5, '.checkbutton');
				$('.checkbutton').click(function(){
					checker(correct_ans);
				});
				break;
			case 3:
			case 4:
			case 5:
			sound_nav("qxn_"+(countNext));
				var rand_num = (1 + Math.floor(Math.random()*9))*100;
				var rand_num3 = 100 + Math.floor(Math.random()*900);
				var rand_num4 = 1000 + Math.floor(Math.random()*9000);
				var quesdata = $('.question').html();
				if(countNext==3){
					$('.question').html(rand_num+quesdata);
					correct_ans = [Math.floor(rand_num/100), rand_num%100];
				} else if(countNext==4){
					$('.question').html(rand_num3+quesdata);
					correct_ans = [Math.floor(rand_num3/100), rand_num3%100];
				} else{
					$('.question').html(rand_num4+quesdata);
					correct_ans = [Math.floor(rand_num4/100), rand_num4%100];
				}
				input_box('.inputformclass', 2, '.checkbutton');
				$('.checkbutton').click(function(){
					checker_2(correct_ans[0], correct_ans[1]);
				});
				break;
			case 6:
			sound_nav("qxn_"+(countNext));
				var rand_num1 = 10 + Math.floor(Math.random()*990);
				var rand_num3 = 10 + Math.floor(Math.random()*990);

				var rand_num2 = 5+Math.floor(Math.random()*14)*5;
				var rand_num4 = 5+Math.floor(Math.random()*19)*5;
				while(rand_num4<rand_num2){
					rand_num4 = 5+Math.floor(Math.random()*19)*5;
				}
				rand_num4 = rand_num4-rand_num2;

				$('.r1-c0').html(rand_num1);
				$('.r1-c1').html(rand_num2);
				$('.r2-c0').html(rand_num3);
				$('.r2-c1').html(rand_num4);

				var correct_ans_2 = (rand_num2+rand_num4)%100;
				var temp_var = Math.floor((rand_num2+rand_num4)/100);
				var correct_ans_1 = (rand_num1+rand_num3)+temp_var;

				input_box('.input-1', 4, '.checkbutton');
				input_box('.input-2', 2, '.checkbutton');
				$('.checkbutton').click(function(){
					checker_2(correct_ans_1, correct_ans_2);
				});
				break;
			case 7:
				var rand_num1 = 10 + Math.floor(Math.random()*990);
				var rand_num3 = 10 + Math.floor(Math.random()*990);

				var rand_num2 = 5+Math.floor(Math.random()*14)*5;
				var rand_num4 = 5+Math.floor(Math.random()*19)*5;
				while(rand_num4<rand_num2){
					rand_num4 = 5+Math.floor(Math.random()*19)*5;
				}
				rand_num4 = rand_num4-rand_num2;
				$('.fill-1').html(rand_num1);
				$('.fill-2').html(rand_num2);
				$('.fill-3').html(rand_num3);
				$('.fill-4').html(rand_num4);
				var correct_ans_2 = (rand_num2+rand_num4)%100;
				var temp_var = Math.floor((rand_num2+rand_num4)/100);
				var correct_ans_1 = (rand_num1+rand_num3)+temp_var;

				input_box('.input-1', 4, '.checkbutton');
				input_box('.input-2', 2, '.checkbutton');
				$('.checkbutton').click(function(){
					checker_2(correct_ans_1, correct_ans_2);
				});
				break;
			case 8:
			sound_nav("qxn_"+(countNext));
				var rand_num1 = 99 + Math.floor(Math.random()*900);
				var rand_num3 = 10 + Math.floor(Math.random()*900);
				while(rand_num3>rand_num1){
					rand_num1 = 99 + Math.floor(Math.random()*900);
					rand_num3 = 10 + Math.floor(Math.random()*900);
				}
				var rand_num2 = 10+Math.floor(Math.random()*17)*5;
				var rand_num4 = 5+Math.floor(Math.random()*15)*5;
				while(rand_num4>rand_num2){
					rand_num2 = 10+Math.floor(Math.random()*17)*5;
					rand_num4 = 5+Math.floor(Math.random()*15)*5;
				}
				$('.r1-c0').html(rand_num1);
				$('.r1-c1').html(rand_num2);
				$('.r2-c0').html(rand_num3);
				$('.r2-c1').html(rand_num4);

				var correct_ans_2 = rand_num2-rand_num4;
				var temp_var = 0;
				if(rand_num2<rand_num4) temp_var=-1;
				var correct_ans_1 = (rand_num1-rand_num3)+temp_var;
				input_box('.input-1', 4, '.checkbutton');
				input_box('.input-2', 2, '.checkbutton');
				$('.checkbutton').click(function(){
					checker_2(correct_ans_1, correct_ans_2);
				});
				break;
			case 9:

				var rand_num1 = 99 + Math.floor(Math.random()*900);
				var rand_num3 = 10 + Math.floor(Math.random()*900);
				while(rand_num3>rand_num1){
					rand_num1 = 99 + Math.floor(Math.random()*900);
					rand_num3 = 10 + Math.floor(Math.random()*900);
				}
				var rand_num2 = 10+Math.floor(Math.random()*17)*5;
				var rand_num4 = 5+Math.floor(Math.random()*15)*5;
				while(rand_num4>rand_num2){
					rand_num2 = 10+Math.floor(Math.random()*17)*5;
					rand_num4 = 5+Math.floor(Math.random()*15)*5;
				}
				$('.fill-1').html(rand_num1);
				$('.fill-2').html(rand_num2);
				$('.fill-3').html(rand_num3);
				$('.fill-4').html(rand_num4);
				var correct_ans_2 = rand_num2-rand_num4;
				var temp_var = 0;
				if(rand_num2<rand_num4) temp_var=-1;
				var correct_ans_1 = (rand_num1-rand_num3)+temp_var;

				input_box('.input-1', 4, '.checkbutton');
				input_box('.input-2', 2, '.checkbutton');
				$('.checkbutton').click(function(){
					checker_2(correct_ans_1, correct_ans_2);
				});
				break;
			default:
				break;
		}
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function checker(ans1){
		var ip_1 = parseInt($('.input-1').val());
		if(ip_1==''){
			return false;
		} else{
			if($('.inputformclass').val()==ans1){
				$('.correct-img').show(0);
				$('.incorrect-img').hide(0);
				play_correct_incorrect_sound(1);
				$('.checkbutton').css({
					'background-color': 'rgba(93, 150, 32, 0.9)',
					'color': 'white',
					'pointer-events': 'none'
				});
				$('.inputformclass, .answer').css({
					'border-color': 'rgba(93, 150, 32, 0.9)',
					'color': 'rgba(93, 150, 32, 0.9)',
					'pointer-events': 'none'
				});
				$('.checkbutton').unbind('click');
				$('.inputformclass').trigger('blur');
				$nextBtn.show(0);
				if(!wrong_clicked) scoring.update(true);
			} else{
				if(!wrong_clicked) scoring.update(false);
				wrong_clicked = true;
				$('.incorrect-img').show(0);
				play_correct_incorrect_sound(0);
				$('.checkbutton').css({
					'background-color': 'rgba(186, 35, 35, 0.9)',
					'color': 'white'
				});
				$('.inputformclass, .answer').css({
					'border-color': 'rgba(186, 35, 35, 0.9)',
					'color': 'rgba(186, 35, 35, 0.9)',
				});
			}
		}
	}
	function checker_2(ans1, ans2){
		var ip_1 = parseInt($('.input-1').val());
		var ip_2 = parseInt($('.input-2').val());
		console.log(ip_2);
		console.log(ans2);
		if(ip_1=='' && ip_2==''){
			return false;
		} else{
			if(ip_1==ans1 && ip_2==ans2){
				$('.correct-img').show(0);
				$('.incorrect-img').hide(0);
				play_correct_incorrect_sound(1);
				$('.checkbutton').css({
					'background-color': 'rgba(93, 150, 32, 0.9)',
					'color': 'white',
					'pointer-events': 'none'
				});
				$('.inputformclass, .ans').css({
					'border-color': 'rgba(93, 150, 32, 0.9)',
					'color': 'rgba(93, 150, 32, 0.9)',
					'pointer-events': 'none'
				});
				$nextBtn.show(0);
				$('.checkbutton').unbind('click');
				$('.inputformclass').trigger('blur');
				if(!wrong_clicked) scoring.update(true);
			} else{
				if(!wrong_clicked) scoring.update(false);
				wrong_clicked = true;
				$('.incorrect-img').show(0);
				play_correct_incorrect_sound(0);
				$('.checkbutton').css({
					'background-color': 'rgba(186, 35, 35, 0.9)',
					'color': 'white'
				});
				if(ip_1==''){
					$('.input-1 , .ans1').css({
						'border-color': '#3E5BAF',
						'background-color': 'transparent',
						'color': '#3E5BAF',
					});
				} else if(ip_1==ans1){
					$('.input-1 , .ans1').css({
						'border-color': 'rgba(93, 150, 32, 0.9)',
						'color': 'rgba(93, 150, 32, 0.9)',
						'pointer-events': 'none'
					});
					$('.input-1').trigger('blur');
				} else{
					$('.input-1 , .ans1').css({
						'border-color': 'rgba(186, 35, 35, 0.9)',
						'color': 'rgba(186, 35, 35, 0.9)',
					});
				}

				if(ip_2==''){
					$('.input-2 , .ans2').css({
						'border-color': '#3E5BAF',
						'background-color': 'transparent',
						'color': '#3E5BAF',
					});
				} else if(ip_2==ans2){
					$('.input-2 , .ans2').css({
						'border-color': 'rgba(93, 150, 32, 0.9)',
						'color': 'rgba(93, 150, 32, 0.9)',
						'pointer-events': 'none'
					});
					$('.input-2').trigger('blur');
				} else{
					$('.input-2 , .ans2').css({
						'border-color': 'rgba(186, 35, 35, 0.9)',
						'color': 'rgba(186, 35, 35, 0.9)',
					});
				}
			}
		}
	}

	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		generalTemplate();
		//to clear input on click
		$('input').click(
		function() {
			if($(this).val()==0) $(this).val('');
		});
	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				scoring.gotoNext();
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();

	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event) {
			var charCode = (event.which) ? event.which : event.keyCode;
			/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
			if (charCode === 13 && button_class != null) {
				$(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
			if (!condition) {
				return true;
			}
			//check if user inputs more than one '.'
			if ((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
				return false;
			}
			//check . and 0-9 separately after checking arrow and other keys
			if ((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110) {
				return false;
			}
			//check max no of allowed digits
			if (String(event.target.value).length >= max_number) {
				return false;
			}
			return true;
		});
	}


});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

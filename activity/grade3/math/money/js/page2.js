var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		uppertextblockadditionalclass: 'center-text-blue-border my_font_ultra_big sniglet',
		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : 'center-middle-title'
		}]
	},

	//slide1 - Re1
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.string.p2text2,
		}
		],
		imagetemplate:[
			{
					imgclass: "",
					imgsrc: imgpath + "coinrs1.png",
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textclass: 'its_hidden text_1 my_font_big',
				textdata: data.string.p2text3
			},
			{
				textclass: 'its_hidden text_2 my_font_big',
				textdata: data.string.p2text4
			}
		],
	},
	//slide2
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.string.p2text2,
		}
		],
		imagetemplate:[
			{
					imgclass: "",
					imgsrc: imgpath + "coinrs1.png",
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textclass: 'text_1 my_font_big',
				textdata: data.string.p2text5
			},
			{
				textclass: 'its_hidden text_2 my_font_ultra_big conversion-text',
				textdata: data.string.p2text6
			}
		],
	},
	//slide3
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.string.p2text2,
		}
		],
		imageblockadditionalclass: 'coin-container',
		imageblock: [{
			imagestoshow: [{
					imgclass: "coin-front coin-flip-animation",
					imgsrc: imgpath + "coinrs1.png",
			},{
					imgclass: "coin-back coin-flip-animation-2",
					imgsrc: imgpath + "coinrs1-back.png",
			}]
		}],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textclass: 'text_1 my_font_big',
				textdata: data.string.p2text7
			},
			{
				textclass: 'its_hidden text_2 my_font_big',
				textdata: data.string.p2text8
			}
		],
	},
	//slide4 - Rs2
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.string.p2text9,
		}
		],
		imagetemplate:[
			{
					imgclass: "",
					imgsrc: imgpath + "coinrs2.png",
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textclass: 'its_hidden text_1 my_font_big',
				textdata: data.string.p2text10
			},
			{
				textclass: 'its_hidden text_2 my_font_big',
				textdata: data.string.p2text11
			}
		],
	},
	//slide5
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.string.p2text9,
		}
		],
		imagetemplate:[
			{
					imgclass: "",
					imgsrc: imgpath + "coinrs2.png",
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textclass: 'text_1 my_font_big',
				textdata: data.string.p2text12
			},
			{
				textclass: 'its_hidden text_2 my_font_ultra_big conversion-text',
				textdata: data.string.p2text13
			}
		],
	},
	//slide6
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.string.p2text9,
		}
		],
		imageblockadditionalclass: 'coin-container',
		imageblock: [{
			imagestoshow: [{
					imgclass: "coin-front coin-flip-animation",
					imgsrc: imgpath + "coinrs2.png",
			},{
					imgclass: "coin-back coin-flip-animation-2",
					imgsrc: imgpath + "coinrs2-back.png",
			}]
		}],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textclass: 'text_1 my_font_big',
				textdata: data.string.p2text14
			},
			{
				textclass: 'its_hidden text_2 my_font_big',
				textdata: data.string.p2text15
			}
		],
	},
	//slide7 - Rs5
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.string.p2text16,
		}
		],
		imagetemplate:[
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs5.png",
			},
			{
					imageparentadditionalclass: 'note-image secondimage',
					imgclass: "",
					imgsrc: imgpath + "rs5back.png",
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textclass: 'its_hidden text_1 my_font_big',
				textdata: data.string.p2text17
			},
			{
				textclass: 'its_hidden text_2 my_font_big',
				textdata: data.string.p2text18
			}
		],
	},
	//slide8
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.string.p2text16,
		}
		],
		imagetemplate:[
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs5.png",
			},
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs5back.png",
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textclass: 'text_1 my_font_big',
				textdata: data.string.p2text19
			},
			{
				textclass: 'its_hidden text_2 my_font_ultra_big conversion-text',
				textdata: data.string.p2text20
			}
		],
	},
	//slide9
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.string.p2text16,
		}
		],
		imagetemplate:[
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs5.png",
			},
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs5back.png",
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textclass: 'text_1 my_font_big',
				textdata: data.string.p2text21
			},
			{
				textclass: 'its_hidden text_2 my_font_big',
				textdata: data.string.p2text22
			}
		],
	},

	//slide10 - Rs10
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.string.p2text23,
		}
		],
		imagetemplate:[
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs10.png",
			},
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs10back.png",
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textclass: 'its_hidden text_1 my_font_big',
				textdata: data.string.p2text24
			},
			{
				textclass: 'its_hidden text_2 my_font_big',
				textdata: data.string.p2text25
			}
		],
	},
	//slide11
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.string.p2text23,
		}
		],
		imagetemplate:[
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs10.png",
			},
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs10back.png",
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textclass: 'text_1 my_font_big',
				textdata: data.string.p2text26
			}
		],
		inputblock: [{
			inputdiv : 'conversion-answer-block my_font_ultra_big',
			textdata: data.string.p2text27,
			textclass: ' my_font_ultra_big conversion-text',
			inputclass: 'ole-template-input-box-default',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'ole-template-check-btn-default check-button',
				}
			]
		}],
	},
	//slide12
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.string.p2text23,
		}
		],
		imagetemplate:[
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs10.png",
			},
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs10back.png",
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textclass: 'text_1 my_font_big',
				textdata: data.string.p2text28
			},
			{
				textclass: 'its_hidden text_2 my_font_big',
				textdata: data.string.p2text29
			}
		],
	},
	//slide13 - Rs20
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.string.p2text30,
		}
		],
		imagetemplate:[
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs20.png",
			},
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs20back.png",
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textclass: 'its_hidden text_1 my_font_big',
				textdata: data.string.p2text31
			},
			{
				textclass: 'its_hidden text_2 my_font_big',
				textdata: data.string.p2text32
			}
		],
	},
	//slide14
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.string.p2text30,
		}
		],
		imagetemplate:[
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs20.png",
			},
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs20back.png",
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textclass: 'text_1 my_font_big',
				textdata: data.string.p2text33
			},
		],
		inputblock: [{
			inputdiv : 'conversion-answer-block my_font_ultra_big',
			textdata: data.string.p2text34,
			textclass: ' my_font_ultra_big conversion-text',
			inputclass: 'ole-template-input-box-default',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'ole-template-check-btn-default check-button',
				}
			]
		}],
	},
	//slide15
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.string.p2text30,
		}
		],
		imagetemplate:[
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs20.png",
			},
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs20back.png",
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textclass: 'text_1 my_font_big',
				textdata: data.string.p2text35
			},
			{
				textclass: 'its_hidden text_2 my_font_big',
				textdata: data.string.p2text36
			}
		]
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			// sounds
			{id: "sound_0", src: soundAsset+"s2_p1.ogg"},
			{id: "sound_1a", src: soundAsset+"s2_p2_1.ogg"},
			{id: "sound_1b", src: soundAsset+"s2_p2_2.ogg"},
			{id: "sound_1c", src: soundAsset+"s2_p2_3.ogg"},
			{id: "sound_2a", src: soundAsset+"s2_p3_1.ogg"},
			{id: "sound_2b", src: soundAsset+"s2_p3_2.ogg"},
			{id: "sound_3a", src: soundAsset+"s2_p4_1.ogg"},
			{id: "sound_3b", src: soundAsset+"s2_p4_2.ogg"},
			{id: "sound_4a", src: soundAsset+"s2_p5_1.ogg"},
			{id: "sound_4b", src: soundAsset+"s2_p5_2.ogg"},
			{id: "sound_4c", src: soundAsset+"s2_p5_3.ogg"},
			{id: "sound_5a", src: soundAsset+"s2_p6_1.ogg"},
			{id: "sound_5b", src: soundAsset+"s2_p6_2.ogg"},
			{id: "sound_6a", src: soundAsset+"s2_p7_1.ogg"},
			{id: "sound_6b", src: soundAsset+"s2_p7_2.ogg"},
			{id: "sound_7a", src: soundAsset+"s2_p8_1.ogg"},
			{id: "sound_7b", src: soundAsset+"s2_p8_2.ogg"},
			{id: "sound_7c", src: soundAsset+"s2_p8_3.ogg"},
			{id: "sound_8a", src: soundAsset+"s2_p9_1.ogg"},
			{id: "sound_8b", src: soundAsset+"s2_p9_2.ogg"},
			{id: "sound_9a", src: soundAsset+"s2_p10_1.ogg"},
			{id: "sound_9b", src: soundAsset+"s2_p10_2.ogg"},
			{id: "sound_10a", src: soundAsset+"s2_p11_1.ogg"},
			{id: "sound_10b", src: soundAsset+"s2_p11_2.ogg"},
			{id: "sound_10c", src: soundAsset+"s2_p11_3.ogg"},
			{id: "sound_11a", src: soundAsset+"s2_p12_1.ogg"},
			{id: "sound_11b", src: soundAsset+"s2_p12_2.ogg"},
			{id: "sound_12a", src: soundAsset+"s2_p13_1.ogg"},
			{id: "sound_12b", src: soundAsset+"s2_p13_2.ogg"},
			{id: "sound_13a", src: soundAsset+"s2_p14_1.ogg"},
			{id: "sound_13b", src: soundAsset+"s2_p14_2.ogg"},
			{id: "sound_13c", src: soundAsset+"s2_p14_3.ogg"},
			{id: "sound_14a", src: soundAsset+"s2_p15_1.ogg"},
			{id: "sound_14b", src: soundAsset+"s2_p15_2.ogg"},
			{id: "sound_15a", src: soundAsset+"s2_p16_1.ogg"},
			{id: "sound_15b", src: soundAsset+"s2_p16_2.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		vocabcontroller.findwords(countNext);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		switch (countNext) {
			case 0:
			sound_player("sound_"+(countNext));
			break;
			case 1:
			case 4:
			case 7:
			case 10:
			case 13:
			sound_player1("sound_"+(countNext)+"a", "sound_"+(countNext)+"b", "sound_"+(countNext)+"c");
			break;
			case 2:
			case 3:
			case 5:
			case 6:
			case 8:
			case 9:
			case 12:
			case 15:
			sound_player2("sound_"+(countNext)+"a", "sound_"+(countNext)+"b");
			break;

			case 11:

				input_box('.ole-template-input-box-default', 5, '.check-button');
			 	$('.check-button').click(function(){
		        	if(parseInt($('.ole-template-input-box-default').val())==1000){
		        		$(this).removeClass('ole-template-check-btn-default-incorrect');
		        		$(this).addClass('ole-template-check-btn-default-correct');
						$('.ole-template-input-box-default').prop('disabled', true);
		        		$('.ole-template-input-box-default').removeClass('ole-template-input-box-default-incorrect');
		        		$('.ole-template-input-box-default').addClass('ole-template-input-box-default-correct');
						nav_button_controls(100);
						createjs.Sound.stop();
						$('.right-wrong').show(0);
						$('.ole-template-input-box-default').blur();
						$('.right-wrong').attr('src', 'images/correct.png');
						play_correct_incorrect_sound(1);
						$(".answerdiv p").text(data.string.p2text27_1);
		        	} else {
									createjs.Sound.stop();
		        		$(this).addClass('ole-template-check-btn-default-incorrect');
		        		$('.ole-template-input-box-default').addClass('ole-template-input-box-default-incorrect');
						$('.right-wrong').show(0);
						$('.right-wrong').attr('src', 'images/wrong.png');
						play_correct_incorrect_sound(0);
		        	}
		        });
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("sound_11a");
						current_sound.play();
						current_sound.on('complete', function(){
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("sound_11b");
						current_sound.play();
						});
				break;
			case 14:
				input_box('.ole-template-input-box-default', 5, '.check-button');
			 	$('.check-button').click(function(){
		        	if(parseInt($('.ole-template-input-box-default').val())==2000){
		        		$(this).removeClass('ole-template-check-btn-default-incorrect');
		        		$(this).addClass('ole-template-check-btn-default-correct');
						$('.ole-template-input-box-default').prop('disabled', true);
		        		$('.ole-template-input-box-default').removeClass('ole-template-input-box-default-incorrect');
		        		$('.ole-template-input-box-default').addClass('ole-template-input-box-default-correct');
						nav_button_controls(100);
						createjs.Sound.stop();
						$('.right-wrong').show(0);
						$('.ole-template-input-box-default').blur();
						$('.right-wrong').attr('src', 'images/correct.png');
						play_correct_incorrect_sound(1);
                        $(".answerdiv p").text(data.string.p2text34_1);
                    } else {
								createjs.Sound.stop();
		        		$(this).addClass('ole-template-check-btn-default-incorrect');
		        		$('.ole-template-input-box-default').addClass('ole-template-input-box-default-incorrect');
						$('.right-wrong').show(0);
						$('.right-wrong').attr('src', 'images/wrong.png');
						play_correct_incorrect_sound(0);
		        	}
		        });
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("sound_14a");
						current_sound.play();
						current_sound.on('complete', function(){
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("sound_14b");
						current_sound.play();
						});
				break;
			default:
				break;
		}
	}

	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player1(sound_id1, sound_id2, sound_id3){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id1);
		current_sound.play();
		current_sound.on('complete', function(){
		createjs.Sound.stop();
		$(".text_1").fadeIn(0);
		current_sound = createjs.Sound.play(sound_id2);
		current_sound.play();
		current_sound.on('complete', function(){
		createjs.Sound.stop();
		$(".text_2").fadeIn(0);
		current_sound = createjs.Sound.play(sound_id3);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(0);
		});
	});
});
}
	function sound_player2(sound_id1, sound_id2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id1);
		current_sound.play();
		$(".text_1").fadeIn(0);
		current_sound.on('complete', function(){
		createjs.Sound.stop();
		$(".text_2").fadeIn(0);
		current_sound = createjs.Sound.play(sound_id2);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(0);
		});
	});
}

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(0);
		});
	}

	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event){
    		var charCode = (event.which) ? event.which : event.keyCode;
    		/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
    		if(charCode === 13 && button_class!=null) {
		        $(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
   			if (!condition) {
    			return true;
    		}
    		//check if user inputs more than one '.'
			if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
        		return false;
    		}
    		//check . and 0-9 separately after checking arrow and other keys
    		if((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110 ){
    			return false;
    		}
    		//check max no of allowed digits
    		if (String(event.target.value).length >= max_number) {
    			return false;
    		}
  			return true;
		});
	}
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

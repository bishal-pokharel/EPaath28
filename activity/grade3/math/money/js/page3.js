var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		uppertextblockadditionalclass: 'center-text-blue-border my_font_ultra_big sniglet',
		uppertextblock : [{
			textdata : data.string.p3text1,
			textclass : 'center-middle-title'
		}]
	},

	//slide1 - Rs50
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.string.p3text2,
		}
		],
		imagetemplate:[
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs50.png",
			},
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs50back.png",
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textclass: 'its_hidden text_1 my_font_big',
				textdata: data.string.p3text3
			},
			{
				textclass: 'its_hidden text_2 my_font_big',
				textdata: data.string.p3text4
			}
		],
	},
	//slide2
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.string.p3text2,
		}
		],
		imagetemplate:[
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs50.png",
			},
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs50back.png",
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textclass: 'text_1 my_font_big',
				textdata: data.string.p3text5
			},
			{
				textclass: 'its_hidden text_2 my_font_ultra_big conversion-text',
				textdata: data.string.p3text6
			}
		],
	},
	//slide3
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.string.p3text2,
		}
		],
		imagetemplate:[
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs50.png",
			},
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs50back.png",
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textclass: 'text_1 my_font_big',
				textdata: data.string.p3text7
			},
			{
				textclass: 'its_hidden text_2 my_font_big',
				textdata: data.string.p3text8
			}
		],
	},
	//slide4 - Rs100
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.string.p3text9,
		}
		],
		imagetemplate:[
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs100.png",
			},
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs100back.png",
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textclass: 'its_hidden text_1 my_font_big',
				textdata: data.string.p3text10
			},
			{
				textclass: 'its_hidden text_2 my_font_big',
				textdata: data.string.p3text11
			}
		],
	},
	//slide5
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.string.p3text9,
		}
		],
		imagetemplate:[
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs100.png",
			},
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs100back.png",
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textclass: 'text_1 my_font_big',
				textdata: data.string.p3text12
			},
			{
				textclass: 'its_hidden text_2 my_font_ultra_big conversion-text',
				textdata: data.string.p3text13
			}
		],
	},
	//slide6
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.string.p3text9,
		}
		],
		imagetemplate:[
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs100.png",
			},
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs100back.png",
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textclass: 'text_1 my_font_big',
				textdata: data.string.p3text14
			},
			{
				textclass: 'its_hidden text_2 my_font_big',
				textdata: data.string.p3text15
			}
		],
	},
	//slide7 - Rs500
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.string.p3text16,
		}
		],
		imagetemplate:[
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs500.png",
			},
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs500back.png",
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textclass: 'its_hidden text_1 my_font_big',
				textdata: data.string.p3text17
			},
			{
				textclass: 'its_hidden text_2 my_font_big',
				textdata: data.string.p3text18
			}
		],
	},
	//slide8
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.string.p3text16,
		}
		],
		imagetemplate:[
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs500.png",
			},
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs500back.png",
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textclass: 'text_1 my_font_big',
				textdata: data.string.p3text19
			},
			{
				textclass: 'its_hidden text_2 my_font_ultra_big conversion-text',
				textdata: data.string.p3text20
			}
		],
	},
	//slide9
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.string.p3text16,
		}
		],
		imagetemplate:[
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs500.png",
			},
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs500back.png",
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textclass: 'text_1 my_font_big',
				textdata: data.string.p3text21
			},
			{
				textclass: 'its_hidden text_2 my_font_big',
				textdata: data.string.p3text22
			}
		],
	},

	//slide10 - Rs1000
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.string.p3text23,
		}
		],

		imagetemplate:[
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs1000.png",
			},
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs1000back.png",
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textclass: 'its_hidden text_1 my_font_big',
				textdata: data.string.p3text24
			},
			{
				textclass: 'its_hidden text_2 my_font_big',
				textdata: data.string.p3text25
			}
		],
	},
	//slide11
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.string.p3text23,
		}
		],
		imagetemplate:[
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs1000.png",
			},
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs1000back.png",
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textclass: 'text_1 my_font_big',
				textdata: data.string.p3text26
			},
			{
				textclass: 'its_hidden text_2 my_font_ultra_big conversion-text',
				textdata: data.string.p3text27
			}
		],
	},
	//slide12
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.string.p3text23,
		}
		],
		imagetemplate:[
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs1000.png",
			},
			{
					imageparentadditionalclass: 'note-image',
					imgclass: "",
					imgsrc: imgpath + "rs1000back.png",
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textclass: 'text_1 my_font_big',
				textdata: data.string.p3text28
			},
			{
				textclass: 'its_hidden text_2 my_font_big',
				textdata: data.string.p3text29
			}
		],
	},
	//slide13
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		uppertextblockadditionalclass: 'center-text-blue-border my_font_ultra_big sniglet',
		uppertextblock : [{
			textdata : data.string.p3text30,
			textclass : 'center-middle-title'
		}]
	},
	//slide12
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header top-margin-header",
		headerblock:[
		{
			textdata : data.string.p3text31,
		}
		],
		draggableblock: [
			{
				imgclass: "coin-1 coin draggable",
				imgsrc: imgpath + "coinrs1.png",
			},{
				imgclass: "coin-2 coin draggable",
				imgsrc: imgpath + "coinrs1-back.png",
			},{
				imgclass: "rs-note note-1 draggable",
				imgsrc: imgpath + "rs10.png",
			},{
				imgclass: "rs-note note-2 draggable",
				imgsrc: imgpath + "rs5.png",
			},{
				imgclass: "rs-note note-3 draggable",
				imgsrc: imgpath + "rs5back.png",
			}
		],
		droppableblock:[
			{
				droppableblockadditionalclass: 'jar-1 drop_1',
				imgsrcfront: imgpath + 'jar.png',
				textdata: data.string.tag1,
				textclass: 'my_font_medium sniglet'
			},
			{
				droppableblockadditionalclass: 'jar-2 drop_2',
				imgsrcfront: imgpath + 'jar.png',
				textdata: data.string.tag2,
				textclass: 'my_font_medium sniglet'
			},
			{
				droppableblockadditionalclass: 'jar-3 drop_3',
				imgsrcfront: imgpath + 'jar.png',
				textdata: data.string.tag3,
				textclass: 'my_font_medium sniglet'
			}
		],
		imageblock: [{
			imagestoshow: [{
					imgclass: "jar-backs jar-1",
					imgsrc: imgpath + "jar-1.png",
				},{
					imgclass: "jar-backs jar-2",
					imgsrc: imgpath + "jar-2.png",
				},{
					imgclass: "jar-backs jar-3",
					imgsrc: imgpath + "jar-3.png",
			}]
		}],
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			// sounds
			{id: "sound_0", src: soundAsset+"s3_p1.ogg"},
			{id: "sound_1a", src: soundAsset+"s3_p2_1.ogg"},
			{id: "sound_1b", src: soundAsset+"s3_p2_2.ogg"},
			{id: "sound_1c", src: soundAsset+"s3_p2_3.ogg"},
			{id: "sound_2a", src: soundAsset+"s3_p3_1.ogg"},
			{id: "sound_2b", src: soundAsset+"s3_p3_2.ogg"},
			{id: "sound_3a", src: soundAsset+"s3_p4_1.ogg"},
			{id: "sound_3b", src: soundAsset+"s3_p4_2.ogg"},
			{id: "sound_4a", src: soundAsset+"s3_p5_1.ogg"},
			{id: "sound_4b", src: soundAsset+"s3_p5_2.ogg"},
			{id: "sound_4c", src: soundAsset+"s3_p5_3.ogg"},
			{id: "sound_5a", src: soundAsset+"s3_p6_1.ogg"},
			{id: "sound_5b", src: soundAsset+"s3_p6_2.ogg"},
			{id: "sound_6a", src: soundAsset+"s3_p7_1.ogg"},
			{id: "sound_6b", src: soundAsset+"s3_p7_2.ogg"},
			{id: "sound_7a", src: soundAsset+"s3_p8_1.ogg"},
			{id: "sound_7b", src: soundAsset+"s3_p8_2.ogg"},
			{id: "sound_7c", src: soundAsset+"s3_p8_3.ogg"},
			{id: "sound_8a", src: soundAsset+"s3_p9_1.ogg"},
			{id: "sound_8b", src: soundAsset+"s3_p9_2.ogg"},
			{id: "sound_9a", src: soundAsset+"s3_p10_1.ogg"},
			{id: "sound_9b", src: soundAsset+"s3_p10_2.ogg"},
			{id: "sound_10a", src: soundAsset+"s3_p11_1.ogg"},
			{id: "sound_10b", src: soundAsset+"s3_p11_2.ogg"},
			{id: "sound_10c", src: soundAsset+"s3_p11_3.ogg"},
			{id: "sound_11a", src: soundAsset+"s3_p12_1.ogg"},
			{id: "sound_11b", src: soundAsset+"s3_p12_2.ogg"},
			{id: "sound_12a", src: soundAsset+"s3_p13_1.ogg"},
			{id: "sound_12b", src: soundAsset+"s3_p13_2.ogg"},
			{id: "sound_13", src: soundAsset+"s3_p14.ogg"},
			{id: "sound_14", src: soundAsset+"s3_p15.ogg"},


		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		vocabcontroller.findwords(countNext);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		switch (countNext) {
			case 0:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_0");
			current_sound.play();
			current_sound.on('complete', function(){
				nav_button_controls(0);
			});
			break;
			case 1:
			case 4:
			case 7:
			case 10:
			sound_player1("sound_"+(countNext)+"a", "sound_"+(countNext)+"b", "sound_"+(countNext)+"c");
			break;
			case 2:
			case 5:
			case 8:
			case 11:
			case 3:
			case 6:
			case 9:
			case 12:
			sound_player2("sound_"+(countNext)+"a", "sound_"+(countNext)+"b");
			break;
			case 13:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_13");
			current_sound.play();
			current_sound.on('complete', function(){
				nav_button_controls(0);
			});
			break;
			case 14:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_14");
			current_sound.play();
				var drop_count = 0;
				var x_1 = 35;
				var y_1 = 65;
				var x_2 = 15;
				var y_2 = 65;
				var r_2 = 5;
				$(".draggable").draggable({
		            containment: ".board",
		            revert: "invalid",
		            appendTo: "body",
		            zIndex: 5,
		            start: function(event, ui) {
		            	if(!$(this).hasClass('coin')){
		            		$(this).css({
			                    "cursor": "grabbing",
			                    "transform": "rotate(90deg) scale(0.5)",
			                });
		            	} else{
		            		$(this).css({
			                    "cursor": "grabbing",
			                    "transform": "scale(0.7)",
			                });
		            	}
		            },
		            stop: function(event, ui) {
		            	if(!$(this).hasClass('coin')){
			                $(this).css({
			                    "cursor": "grab",
			                    "transform": "none",
			                });
		              } else {
		              		$(this).css({
			                    "cursor": "grab",
			                    "transform": "none",
			                });
		              }
		            }
		        });
		        $('.drop_1').droppable({
		            accept : ".coin",
		            // hoverClass: "hoverd_match",
		            drop: function(event, ui) {
                		ui.draggable.draggable('disable');
                		$(ui.draggable).detach().css({
                			'width': '22%',
							'top': y_1+'%',
							'transform': 'rotateX(60deg)',
							'left': x_1+'%'
                		}).appendTo('.drop_1');
                		setTimeout(function(){
                			$(ui.draggable).css({
								'transform': 'rotateX(60deg)',
                			});
                		}, 100);
                		x_1 = 45;
                		y_1 = 55;
                		check_drop();
											createjs.Sound.stop();
										play_correct_incorrect_sound(1);
		            }
		        });
		        $('.drop_2').droppable({
		            accept : ".note-2, .note-3",
		            // hoverClass: "hoverd_match",
		            drop: function(event, ui) {
                		ui.draggable.draggable('disable');
                		$(ui.draggable).detach().css({
                			'width': '60%',
							'top': '42%',
							'transform': 'rotateX(60deg) rotateZ('+r_2+'deg)',
							'left': x_2+'%'
                		}).appendTo('.drop_2');
                		setTimeout(function(){
                			$(ui.draggable).css({
								'transform': 'rotateX(60deg)',
                			});
                		}, 100);
                		x_2 = 22;
                		y_2 = 55;
                		r_2 = -5;
                		check_drop();
											createjs.Sound.stop();
										play_correct_incorrect_sound(1);
		            }
		        });
		        $('.drop_3').droppable({
		            accept : ".note-1",
		            // hoverClass: "hoverd_match",
		            drop: function(event, ui) {
                		ui.draggable.draggable('disable');
                		$(ui.draggable).detach().css({
                			'width': '60%',
							'top': '42%',
							'transform': 'rotateX(60deg) rotateZ(5deg)',
							'left': '15%'
                		}).appendTo('.drop_3');
                		setTimeout(function(){
                			$(ui.draggable).css({
								'transform': 'rotateX(60deg)',
                			});
                		}, 100);
                		check_drop();
											createjs.Sound.stop();
										play_correct_incorrect_sound(1);
		            }
		        });
		        function check_drop(){
		        	if(drop_count >=4 ){
						nav_button_controls(100);
		        	} else{
		        		drop_count++;
		        	}
		        }
				break;
			default:
					nav_button_controls(100);
				break;
		}
	}

	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player1(sound_id1, sound_id2, sound_id3){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id1);
		current_sound.play();
		current_sound.on('complete', function(){
		createjs.Sound.stop();
		$(".text_1").fadeIn(0);
		current_sound = createjs.Sound.play(sound_id2);
		current_sound.play();
		current_sound.on('complete', function(){
		createjs.Sound.stop();
		$(".text_2").fadeIn(0);
		current_sound = createjs.Sound.play(sound_id3);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(0);
		});
	});
});
}
	function sound_player2(sound_id1, sound_id2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id1);
		current_sound.play();
		$(".text_1").fadeIn(0);
		current_sound.on('complete', function(){
		createjs.Sound.stop();
		$(".text_2").fadeIn(0);
		current_sound = createjs.Sound.play(sound_id2);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(0);
		});
	});
}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

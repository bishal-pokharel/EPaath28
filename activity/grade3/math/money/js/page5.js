var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		headerblockadditionalclass: "question-blue",
		headerblock:[
		{
			textdata : data.string.p5text1,
		}
		],
		uppertextblockadditionalclass: 'answer-block',
		uppertextblock : [{
			textdata : data.string.p5text2,
			textclass : 'my_font_big'
		}],
		lowertextblockadditionalclass: 'equal-answer-block equal-answer-block-3',
		lowertextblock : [{
			textdata : data.string.p5text3,
			textclass : 'my_font_big'
		},{
			textdata : data.string.p5text4,
			textclass : 'my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'magneto_highlight'
		}],
		extratextblock : [{
			textdata : data.string.p5text5,
			textclass : 'p5-conclusion my_font_big'
		}],
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		uppertextblockadditionalclass: 'center-text-blue-border my_font_ultra_big sniglet',
		uppertextblock : [{
			textdata : data.string.p4text8,
			textclass : 'center-middle-title'
		}]
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		headerblockadditionalclass: "question-blue",
		headerblock:[
		{
			textdata : data.string.p5text6,
		}
		],
		uppertextblockadditionalclass: 'answer-block',
		uppertextblock : [{
			textdata : data.string.p5text7,
			textclass : 'my_font_big'
		}],
		inputblock: [{
			inputdiv : 'equal-answer-block',
			textdata1: data.string.p5text8,
			textclass1: 'my_font_big',
			textdata2: data.string.p5text9,
			textclass2: 'my_font_big',
			textdata3: data.string.paisa,
			textclass3: 'my_font_big',
			inputclass: 'ole-template-input-box-default input-1',
			inputclass2: 'ole-template-input-box-default input-2',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'ole-template-check-btn-default check-button',
				}
			]
		}],
		lowertextblockadditionalclass: 'hint-block',
		lowertextblock : [{
			textdata : data.string.hint,
			textclass : 'my_font_big hint-title'
		},{
			textdata : data.string.p5text10,
			textclass : 'my_font_medium hint-1',
			datahighlightflag : true,
			datahighlightcustomclass : 'magneto_highlight'
		},{
			textdata : data.string.p5text11,
			textclass : 'my_font_medium hint-2'
		}],
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		uppertextblockadditionalclass: 'center-text-blue-border my_font_ultra_big sniglet',
		uppertextblock : [{
			textdata : data.string.p5text12,
			textclass : 'center-middle-title'
		}]
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		headerblockadditionalclass: "question-blue question-blue-2",
		headerblock:[
			{
				textdata : data.string.p5text13,
			}
		],

		imageblock : [{
			imagestoshow : [{
				imgclass : "question-image",
				imgsrc : imgpath + 'ask_money.png',

			}],
		}],
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		headerblockadditionalclass: "question-blue question-blue-2",
		headerblock:[
			{
				textdata : data.string.p5text13,
			}
		],
		tableblockadditionalclass:'fade_in_1',
		tableblock:[{
			rs: data.string.rs,
			p: data.string.paisa,
			row1_1: data.string.p5text14,
			row1_2: data.string.p5text19,
			row1_3: data.string.p5text22,
			row2_1: data.string.p5text15,
			row2_2: data.string.p5text20,
			row2_3: data.string.p5text23,
			row3_1: data.string.p5text16,
			row3_2: data.string.p5text21,
			row3_3: data.string.p5text24,
		}],
		extratextblock : [{
			textdata : data.string.p5text17,
			textclass : 'side-hint my_font_medium fade_in_1'
		}],
	},
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		headerblockadditionalclass: "question-blue question-blue-2",
		headerblock:[
			{
				textdata : data.string.p5text13,
			}
		],
		tableblockadditionalclass:'',
		tableblock:[{
			rs: data.string.rs,
			p: data.string.paisa,
			row1_1: data.string.p5text14,
			row1_2: data.string.p5text19,
			row1_3: data.string.p5text22,
			row2_1: data.string.p5text15,
			row2_2: data.string.p5text20,
			row2_3: data.string.p5text23,
			row3_1: data.string.p5text16,
			row3_2: data.string.p5text21,
			row3_3: data.string.p5text24,
		}],
		extratextblock : [{
			textdata : data.string.p5text18,
			textclass : 'conclusion my_font_big fade_in_1'
		},{
			textdata : data.string.p5text17,
			textclass : 'side-hint my_font_medium'
		}],
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		headerblockadditionalclass: "question-blue question-blue-2",
		headerblock:[
			{
				textdata : data.string.p5text25,
			}
		],

		imageblock : [{
			imagestoshow : [{
				imgclass : "question-image",
				imgsrc : imgpath + 'shopping.png',

			}],
		}],
	},
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		headerblockadditionalclass: "question-blue question-blue-2",
		headerblock:[
			{
				textdata : data.string.p5text25,
			}
		],
		tableblockadditionalclass:'fade_in_1',
		tableblock:[{
			rs: data.string.rs,
			p: data.string.paisa,
			row1_1: data.string.p5text14,
			row1_2: data.string.p5text27,
			row1_3: data.string.p5text24,
			row2_1: data.string.p5text26,
			row2_2: data.string.p5text28,
			row2_3: data.string.p5text29,
			row3_1: data.string.p5text16,
			row3_2: data.string.p5text29,
			row3_3: data.string.t_55,
		}],
		extratextblock : [{
			textdata : data.string.p5text32,
			textclass : 'conclusion my_font_big fade_in_1'
		},{
			textdata : data.string.p5text31,
			textclass : 'side-hint my_font_medium fade_in_1'
		}],
	},
	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		uppertextblockadditionalclass: 'center-text-blue-border my_font_ultra_big sniglet',
		uppertextblock : [{
			textdata : data.string.p4text8,
			textclass : 'center-middle-title'
		}],

	},

	//slide10
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		headerblockadditionalclass: "question-blue question-blue-2",
		headerblock:[
			{
				textdata : data.string.p5text25,
			}
		],
		tableblockadditionalclass:'',
		tableblock:[{
			tablehasinput: true,
			rs: data.string.rs,
			p: data.string.paisa,
			row1_1: data.string.p5text43,
			row1_2: data.string.p5text27,
			row1_3: data.string.p5text30,
			row2_1: data.string.p5text46,
			row2_2: data.string.p5text28,
			row2_3: data.string.p5text29,
			row3_1: data.string.p5text40,
			inputclass: 'ole-template-input-box-default input-1',
			inputclass2: 'ole-template-input-box-default input-2',
		}],
		extratextblock : [{
			textdata : data.string.p5text32,
			textclass : 'conclusion my_font_big its_hidden'
		},
		{
			textdata : data.string.check,
			textclass : 'ole-template-check-btn-default check-btn-2'
		}],
		imageblock : [{
			imagestoshow : [{
				imgclass : "correct-incorrect",
				imgsrc : "",

			}],
		}],
	},
	//slide11
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		headerblockadditionalclass: "question-blue question-blue-2",
		headerblock:[
			{
				textdata : data.string.p5text42,
			}
		],
		tableblockadditionalclass:'',
		tableblock:[{
			tablehasinput: true,
			rs: data.string.rs,
			p: data.string.paisa,
			row1_1: data.string.p5text34,
			row1_2: data.string.p5text35,
			row1_3: data.string.p5text36,
			row2_1: data.string.p5text37,
			row2_2: data.string.p5text38,
			row2_3: data.string.p5text39,
			row3_1: data.string.p5text40,
			inputclass: 'ole-template-input-box-default input-1',
			inputclass2: 'ole-template-input-box-default input-2',
		}],
		extratextblock : [{
			textdata : data.string.p5text41,
			textclass : 'conclusion my_font_big its_hidden'
		},
		{
			textdata : data.string.check,
			textclass : 'ole-template-check-btn-default check-btn-2'
		}],
		imageblock : [{
			imagestoshow : [{
				imgclass : "correct-incorrect",
				imgsrc : "",

			}],
		}],
	},
	//slide12
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.string.p5text50,
		}
		],
		imagetemplate:[
			{
					imageparentadditionalclass: '',
					imgclass: "",
					imgsrc: imgpath + "shopping.png",
					textdata : data.string.p5text51
			},
			{
					imageparentadditionalclass: '',
					imgclass: "",
					imgsrc: imgpath + "barbershop.jpg",
					textdata : data.string.p5text52
			},
			{
					imageparentadditionalclass: '',
					imgclass: "",
					imgsrc: imgpath + "putting-tika.png",
					textdata : data.string.p5text53,
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textclass: 'text_1 my_font_big',
				textdata: data.string.p5text54
			},
		]
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();


	var selected_item = ['tortoise', 50];
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			// sounds
			{id: "sound_0a", src: soundAsset+"s5_p1.ogg"},
			{id: "sound_0b", src: soundAsset+"s5_p1_1.ogg"},
			{id: "sound_2", src: soundAsset+"s5_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s5_p4.ogg"},
			{id: "sound_4", src: soundAsset+"s5_p5.ogg"},
			{id: "sound_5", src: soundAsset+"s5_p6.ogg"},
			{id: "sound_6", src: soundAsset+"s5_p7.ogg"},
			{id: "sound_7", src: soundAsset+"s5_p8.ogg"},
			{id: "sound_8a", src: soundAsset+"s5_p9_1.ogg"},
			{id: "sound_8b", src: soundAsset+"s5_p9_2.ogg"},
			{id: "sound_10a", src: soundAsset+"s5_p11_1.ogg"},
			{id: "sound_10b", src: soundAsset+"s5_p11_2.ogg"},
			{id: "sound_11a", src: soundAsset+"s5_p12_1.ogg"},
			{id: "sound_11b", src: soundAsset+"s5_p12_2.ogg"},
			{id: "sound_12", src: soundAsset+"s5_p13.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		vocabcontroller.findwords(countNext);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		switch (countNext) {
			case 0:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_0a");
			current_sound.play();
			current_sound.on('complete', function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_0b");
			current_sound.play();
			current_sound.on('complete', function(){
				nav_button_controls(0);
			});
		});
			break;
			case 1:
			play_diy_audio();
			nav_button_controls(2000);
			break;
			case 2:
			sound_nav("sound_"+countNext);
				var hint_count = 1;
				input_box('.ole-template-input-box-default', 5, '.check-button');
			 	$('.check-button').click(function(){
		        	if( (parseInt($('.input-1').val())==5) && (parseInt($('.input-2').val())==60) ){
		        		$(this).removeClass('ole-template-check-btn-default-incorrect');
		        		$(this).addClass('ole-template-check-btn-default-correct');
						$('.ole-template-input-box-default').prop('disabled', true);
		        		$('.ole-template-input-box-default').removeClass('ole-template-input-box-default-incorrect');
		        		$('.ole-template-input-box-default').addClass('ole-template-input-box-default-correct');
						nav_button_controls(100);
						createjs.Sound.stop();
						$('.right-wrong').show(0);
						$('.ole-template-input-box-default').blur();
						$('.hint-block, .hint-1, .hint-2').show(0);
						$('.right-wrong').attr('src', 'images/correct.png');
						play_correct_incorrect_sound(1);
		        	} else {
								createjs.Sound.stop();
		        		$(this).addClass('ole-template-check-btn-default-incorrect');
		        		$('.ole-template-input-box-default').addClass('ole-template-input-box-default-incorrect');
		        		$('.hint-block').show(0);
		        		$('.hint-'+ hint_count).show(0);
						$('.right-wrong').show(0);
						$('.right-wrong').attr('src', 'images/wrong.png');
		        		hint_count++;
						play_correct_incorrect_sound(0);
		        	}
		        });
				break;
				case 8:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_8a");
				current_sound.play();
				current_sound.on('complete', function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_8b");
				current_sound.play();
				current_sound.on('complete', function(){
					nav_button_controls(0);
				});
			});
				break;
				case 9:
				play_diy_audio();
				nav_button_controls(2000);
				break;
			case 10:
			sound_nav("sound_10a");
				input_box('.ole-template-input-box-default', 5, '.check-btn-2');
			 	$('.check-btn-2').click(function(){
		        	if( (parseInt($('.input-1').val())==20) && (parseInt($('.input-2').val())==55) ){
		        		$(this).removeClass('ole-template-check-btn-default-incorrect');
		        		$(this).addClass('ole-template-check-btn-default-correct');
						$('.ole-template-input-box-default').prop('disabled', true);
		        		$('.ole-template-input-box-default').removeClass('ole-template-input-box-default-incorrect');
		        		$('.ole-template-input-box-default').addClass('ole-template-input-box-default-correct');
						$('.correct-incorrect').show(0);
						$('.correct-incorrect').attr('src', 'images/correct.png');
						$('.ole-template-input-box-default').blur();
						$('.conclusion').fadeIn(500, function(){

							createjs.Sound.stop();
							setTimeout(function(){
								sound_player("sound_10b");
							},500);
						});
						play_correct_incorrect_sound(1);
		        	} else {
								createjs.Sound.stop();
		        		$(this).addClass('ole-template-check-btn-default-incorrect');
		        		$('.ole-template-input-box-default').addClass('ole-template-input-box-default-incorrect');
						$('.correct-incorrect').show(0);
						$('.correct-incorrect').attr('src', 'images/wrong.png');
						play_correct_incorrect_sound(0);
		        	}
		        });
				break;
			case 11:
				sound_nav("sound_11a");
				input_box('.ole-template-input-box-default', 5, '.check-btn-2');
			 	$('.check-btn-2').click(function(){
		        	if( (parseInt($('.input-1').val())==78) && (parseInt($('.input-2').val())==60) ){
		        		$(this).removeClass('ole-template-check-btn-default-incorrect');
		        		$(this).addClass('ole-template-check-btn-default-correct');
						$('.ole-template-input-box-default').prop('disabled', true);
		        		$('.ole-template-input-box-default').removeClass('ole-template-input-box-default-incorrect');
		        		$('.ole-template-input-box-default').addClass('ole-template-input-box-default-correct');
						$('.correct-incorrect').show(0);
						$('.correct-incorrect').attr('src', 'images/correct.png');
						$('.input-1, .input-2').blur();
						$('.conclusion').fadeIn(500, function(){
							setTimeout(function(){
								sound_player("sound_11b");
							},500);
						});
						play_correct_incorrect_sound(1);
		        	} else {
		        		$(this).addClass('ole-template-check-btn-default-incorrect');
		        		$('.ole-template-input-box-default').addClass('ole-template-input-box-default-incorrect');
						$('.correct-incorrect').show(0);
						$('.correct-incorrect').attr('src', 'images/wrong.png');
						play_correct_incorrect_sound(0);
		        	}
		        });
				break;
			default:
			sound_player("sound_"+(countNext));
				break;
		}
	}


		function nav_button_controls(delay_ms){
			setTimeout(function(){
				if(countNext==0){
					$nextBtn.show(0);
				} else if( countNext>0 && countNext == $total_page-1){
					$prevBtn.show(0);
					ole.footerNotificationHandler.lessonEndSetNotification();
				} else{
					$prevBtn.show(0);
					$nextBtn.show(0);
				}
			},delay_ms);
		}

		function sound_player(sound_id){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
			current_sound.on('complete', function(){
				nav_button_controls(0);
			});
	}

		function sound_nav(sound_id){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
		}
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event){
    		var charCode = (event.which) ? event.which : event.keyCode;
    		/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
    		if(charCode === 13 && button_class!=null) {
		        $(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
   			if (!condition) {
    			return true;
    		}
    		//check if user inputs more than one '.'
			if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
        		return false;
    		}
    		//check . and 0-9 separately after checking arrow and other keys
    		if((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110 ){
    			return false;
    		}
    		//check max no of allowed digits
    		if (String(event.target.value).length >= max_number) {
    			return false;
    		}
  			return true;
		});
	}
	function arrage_items_in_shelf(){
		for(var i=1; i<7; i++){
			var shelf_array = document.getElementsByClassName('shop_item_'+i);
			var total_in_array = shelf_array.length;
			for(var m=0; m<shelf_array.length; m++){
				var left_position = $('.shop_item_'+i).eq(0).position().left+m*$board.width()*0.06;
				$('.shop_item_'+i).eq(m).css({
					'left': left_position,
					'z-index':1 + (total_in_array-m)*1
				});
				// shelf_array[m].style.left = 1 + (total_in_array-m)*1;
			}
		}
	}
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

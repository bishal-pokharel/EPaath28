var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		uppertextblockadditionalclass: 'center-text-blue-border my_font_ultra_big sniglet',
		uppertextblock : [{
			textdata : data.string.p4text1,
			textclass : 'center-middle-title'
		}]
	},

	//slide1
	{
		contentblockadditionalclass: 'shop_bg_1',
		contentblocknocenteradjust : true,

		uppertextblockadditionalclass : "shop_name_board my_font_big top_text",
		uppertextblock : [
			{
				textdata : data.string.shopname,
				textclass : 'shop_name_text'
			},
		],
		imageblockadditionalclass: 'block_cover',
		imageblock : [{
			imagelabels: [{
				imagelabelclass : "shelf_top shelf_2",
				imagelabeldata : '',
			},
			{
				imagelabelclass : "shelf_bottom shelf_2",
				imagelabeldata : '',
			},
			{
				imagelabelclass : "floor",
				imagelabeldata : '',
			}]
		}],
		basketblockadditionalclass: 'basket',
		basketblock:[{
			imgclass : "basket_front",
			imgsrc : imgpath + "basket_front.png",
		},
		{
			imgclass : "basket_back",
			imgsrc : imgpath + "basketback.png",
		}],
		shoppingblockadditionalclass: 'block_cover',
		shoppingblock : [
			{
				imgclass : "shop_items shop_item_1 top_shelf",
				item_name : data.string.s2_item_1,
				imgsrc : imgpath + "toy01.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 20'
			},
			{
				imgclass : "shop_items shop_item_1 top_shelf",
				item_name : data.string.s2_item_1,
				imgsrc : imgpath + "toy01.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 20'
			},
			{
				imgclass : "shop_items shop_item_1 top_shelf",
				item_name : data.string.s2_item_1,
				imgsrc : imgpath + "toy01.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 20'
			},
			{
				imgclass : "shop_items shop_item_2 top_shelf",
				item_name : data.string.s2_item_2,
				imgsrc : imgpath + "toy02.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 40'
			},
			{
				imgclass : "shop_items shop_item_2 top_shelf",
				item_name : data.string.s2_item_2,
				imgsrc : imgpath + "toy02.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 40'
			},
			{
				imgclass : "shop_items shop_item_2 top_shelf",
				item_name : data.string.s2_item_3,
				imgsrc : imgpath + "toy03.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 30'
			},
			{
				imgclass : "shop_items shop_item_2 top_shelf",
				item_name : data.string.s2_item_3,
				imgsrc : imgpath + "toy03.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 30'
			},
			{
				imgclass : "shop_items shop_item_3 top_shelf",
				item_name : data.string.s2_item_4,
				imgsrc : imgpath + "toy04.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 45'
			},
			{
				imgclass : "shop_items shop_item_3 top_shelf",
				item_name : data.string.s2_item_4,
				imgsrc : imgpath + "toy04.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 45'
			},
			{
				imgclass : "shop_items shop_item_3 top_shelf",
				item_name : data.string.s2_item_5,
				imgsrc : imgpath + "toy06.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 15'
			},
			{
				imgclass : "shop_items shop_item_3 top_shelf",
				item_name : data.string.s2_item_5,
				imgsrc : imgpath + "toy06.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 15'
			},
			{
				imgclass : "shop_items shop_item_4 bottom_shelf",
				item_name : data.string.s2_item_6,
				imgsrc : imgpath + "toy05.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 35'
			},
			{
				imgclass : "shop_items shop_item_4 bottom_shelf",
				item_name : data.string.s2_item_6,
				imgsrc : imgpath + "toy05.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 35'
			},
			{
				imgclass : "shop_items shop_item_4 bottom_shelf",
				item_name : data.string.s2_item_6,
				imgsrc : imgpath + "toy05.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 35'
			},
			{
				imgclass : "shop_items shop_item_5 bottom_shelf",
				item_name : data.string.s2_item_7,
				imgsrc : imgpath + "toy07.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 35'
			},
			{
				imgclass : "shop_items shop_item_5 bottom_shelf",
				item_name : data.string.s2_item_7,
				imgsrc : imgpath + "toy07.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 35'
			},
			{
				imgclass : "shop_items shop_item_6 bottom_shelf",
				item_name : data.string.s2_item_8,
				imgsrc : imgpath + "toy08.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 25'
			},
			{
				imgclass : "shop_items shop_item_6 bottom_shelf",
				item_name : data.string.s2_item_8,
				imgsrc : imgpath + "toy08.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 25'
			},
		]
	},
	//slide 2
	{
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "bluecre_instruction top-margin-header ",
		headerblock:[
		{
			textdata : data.string.p4text2,
			textclass: 'font-issue-1'
		}
		],
		imageblock: [{
			imagestoshow: [{
				imgclass: "shop-counter",
				imgsrc: imgpath + "cashcounter.png",
			}]
		}],
		shoppingblock : [
			{
				imgclass : "counter-item",
				item_name : data.string.s2_item_1,
				imgsrc : imgpath + "toy01.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 200'
			}],
		draggableblock: [
			{
				imgclass: "coin-1 draggable",
				imgsrc: imgpath + "coinrs1.png",
				value: '1'
			},{
				imgclass: "rs-note note-1 draggable",
				imgsrc: imgpath + "rs5.png",
				value: '5'
			},{
				imgclass: "rs-note note-2 draggable",
				imgsrc: imgpath + "rs10.png",
				value: '10'
			},{
				imgclass: "rs-note note-3 draggable",
				imgsrc: imgpath + "rs20.png",
				value: '20'
			}
		],
		droppableblock:[
			{
				droppableblockadditionalclass: 'drop_1',
			}
		],
		lowertextblockadditionalclass : "counter-text my_font_big",
		lowertextblock : [
			{
				textdata : data.string.total,
				textclass : 'total-text my_font_big'
			},
			{
				textdata : '0',
				textclass : 'total-amount my_font_very_big'
			},
			{
				textdata : data.string.check,
				textclass : 'ole-template-check-btn-default check-button'
			},
			{
				textdata : data.string.clear,
				textclass : 'ole-template-check-btn-default clear-button'
			},
		],
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		uppertextblockadditionalclass: 'center-text-blue-border my_font_ultra_big sniglet',
		uppertextblock : [{
			textdata : data.string.p4text3,
			textclass : 'center-middle-title'
		}]
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		headerblockadditionalclass: "question-blue",
		headerblock:[
		{
			textdata : data.string.p4text4,
		}
		],
		uppertextblockadditionalclass: 'answer-block newcss',
		uppertextblock : [{
			textdata : data.string.p4text5,
			textclass : 'my_font_big'
		}],
		lowertextblockadditionalclass: 'equal-answer-block',
		lowertextblock : [{
			textdata : data.string.p4text6,
			textclass : 'my_font_big'
		},{
			textdata : data.string.p4text7,
			textclass : 'my_font_big'
		}],
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		uppertextblockadditionalclass: 'center-text-blue-border my_font_ultra_big sniglet',
		uppertextblock : [{
			textdata : data.string.p4text8,
			textclass : 'center-middle-title'
		}]
	},
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		headerblockadditionalclass: "question-blue",
		headerblock:[
		{
			textdata : data.string.p4text9,
		}
		],
		uppertextblockadditionalclass: 'answer-block',
		uppertextblock : [{
			textdata : data.string.p4text10,
			textclass : 'my_font_big'
		}],
		inputblock: [{
			inputdiv : 'equal-answer-block',
			textdata: data.string.paisa,
			textclass: 'my_font_medium',
			inputclass: 'ole-template-input-box-default',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'ole-template-check-btn-default check-button',
				}
			]
		}],
		lowertextblockadditionalclass: 'hint-block',
		lowertextblock : [{
			textdata : data.string.hint,
			textclass : 'my_font_big hint-title'
		},{
			textdata : data.string.p4text11,
			textclass : 'my_font_medium hint-1'
		},{
			textdata : data.string.p4text12,
			textclass : 'my_font_medium hint-2'
		}],
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		headerblockadditionalclass: "question-blue question-blue-2",
		headerblock:[
		{
			textdata : data.string.p4text13,
		}
		],
	},
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		headerblockadditionalclass: "question-blue question-blue-2",
		headerblock:[
		{
			textdata : data.string.p4text13,
		}
		],
		uppertextblockadditionalclass: 'answer-block newcss fade_in_1',
		uppertextblock : [{
			textdata : data.string.p4text14,
			textclass : ''
		}],
		lowertextblockadditionalclass: 'equal-answer-block equal-answer-block-2 fade_in_1',
		lowertextblock : [{
			textdata : data.string.p4text15,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'magneto_highlight'
		},{
			textdata : data.string.p4text16,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'magneto_highlight'
		}],
	},
	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		headerblockadditionalclass: "question-blue question-blue-2 ",
		headerblock:[
		{
			textdata : data.string.p4text13,
		}
		],
		uppertextblockadditionalclass: 'answer-block newcss',
		uppertextblock : [{
			textdata : data.string.p4text14,
			textclass : ''
		}],
		lowertextblockadditionalclass: 'equal-answer-block equal-answer-block-2',
		lowertextblock : [{
			textdata : data.string.p4text15,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'magneto_highlight'
		},{
			textdata : data.string.p4text16,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'magneto_highlight'
		}],
		uppertextblock2additionalclass: 'answer-block fade_in_1',
		uppertextblock2 : [{
			textdata : data.string.p4text16,
			textclass : 'my_font_big'
		}],
		lowertextblock2additionalclass: 'equal-answer-block equal-answer-block fade_in_1',
		lowertextblock2 : [{
			textdata : data.string.p4text17,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'magneto_highlight'
		},{
			textdata : data.string.p4text18,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'magneto_highlight'
		}],
		extratextblock : [{
			textdata : data.string.p4text20,
			textclass : 'conclusion my_font_big fade_in_3'
		}],
	},
	//slide10
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		uppertextblockadditionalclass: 'center-text-blue-border my_font_ultra_big sniglet',
		uppertextblock : [{
			textdata : data.string.p4text8,
			textclass : 'center-middle-title'
		}]
	},
	//slide11
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		headerblockadditionalclass: "question-blue question-blue-2 ",
		headerblock:[
		{
			textdata : data.string.p4text21,
		}
		],
		uppertextblockadditionalclass: 'answer-block answer-block-1',
		uppertextblock : [{
			textdata : data.string.p4text22,
			textclass : ''
		}],
		inputblock: [{
			inputdiv : 'equal-answer-block inputblock-1',
			textdata: data.string.paisa,
			textclass: 'my_font_medium',
			inputclass: 'ole-template-input-box-default',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'ole-template-check-btn-default check-button',
				}
			]
		}],
		lowertextblockadditionalclass: 'hint-block hint-block-new hint-block-1',
		lowertextblock : [{
			textdata : data.string.hint,
			textclass : 'hint-title'
		},{
			textdata : data.string.p4text11,
			textclass : 'hint-1'
		},{
			textdata : data.string.p4text23,
			textclass : 'hint-2'
		}],
		uppertextblock2additionalclass: 'answer-block answer-block-new hidden-utb',
		uppertextblock2 : [{
			textdata : data.string.p4text24,
			textclass : ''
		}],
		inputblock2: [{
			inputdiv : 'equal-answer-block equal-answer-block-new hidden-itb inputblock-2',
			textdata: data.string.paisa,
			textclass: 'my_font_medium',
			inputclass: 'ole-template-input-box-default',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.check,
					textclass: 'ole-template-check-btn-default check-button',
				}
			]
		}],
		lowertextblock2additionalclass: 'hint-block hint-block-new  hint-block-2',
		lowertextblock2 : [{
			textdata : data.string.hint,
			textclass : 'hint-title'
		},{
			textdata : data.string.p4text25,
			textclass : 'hint-1'
		},{
			textdata : data.string.p4text26,
			textclass : 'hint-2'
		}],

		extratextblock : [{
			textdata : data.string.p4text27,
			textclass : 'conclusion-2 my_font_big'
		}],
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();


	var selected_item = ['tortoise', 50];
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			// sounds
			{id: "sound_0", src: soundAsset+"s4_p1.ogg"},
			{id: "sound_2", src: soundAsset+"s4_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s4_p4.ogg"},
			{id: "sound_4a", src: soundAsset+"s4_p5_1.ogg"},
			{id: "sound_4b", src: soundAsset+"s4_p5_2.ogg"},
			{id: "sound_6", src: soundAsset+"s4_p7.ogg"},
			{id: "sound_7", src: soundAsset+"s4_p8.ogg"},
			{id: "sound_8", src: soundAsset+"s4_p9.ogg"},
			{id: "sound_9a", src: soundAsset+"s4_p10_1.ogg"},
			{id: "sound_9b", src: soundAsset+"s4_p10_2.ogg"},
			{id: "sound_11", src: soundAsset+"s4_p12.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		vocabcontroller.findwords(countNext);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		switch (countNext) {
			case 1:
				arrage_items_in_shelf();
				$('.shop_items').click(function(){
					var item_cost = parseInt($(this).find('label').text().split(" ")[1]);
					selected_item = [$(this).data('name'), item_cost, $(this).find('img').attr('src')];
					$('.shop_items').css('pointer-events','none');
					$(this).fadeOut(500, function(){
						$(this).detach().css({
							'width': '60%',
							'z-index': '10',
							'top': 'none',
							'bottom': '5%',
							'left': '20%',
							}).appendTo($('.basket'));
						$(this).fadeIn(500);
						nav_button_controls(500);
					});
				});
				break;
			case 2:
				sound_nav("sound_2");
				var dropped_count = 0;
				var dropped_1 = 0;
				var dropped_5 = 0;
				var dropped_10 = 0;
				var dropped_20 = 0;
				var dropped_sum = 0;
				$('.counter-item>img').attr('src', selected_item[2]);
				$('.counter-item>label').html('Rs '+selected_item[1]);
				$(".draggable").draggable({
		            containment: ".board",
		            revert: "invalid",
		            appendTo: "body",
		            helper : "clone",
		            zIndex: 200,
		            start: function(event, ui) {
		            	if(!$(this).hasClass('coin-1')){
		            		$(ui.helper).css({
			                    "cursor": "grabbing",
			                    "transform": "scale(0.5)",
			                });
		            	} else{
		            		$(ui.helper).css({
			                    "cursor": "grabbing",
			                    "transform": "scale(0.7)",
			                });
		            	}
		            },
		            stop: function(event, ui) {
		            	if(!$(this).hasClass('coin-1')){
			                $(ui.helper).css({
			                    "cursor": "grab",
			                    "transform": "none",
			                });
		              } else {
		              		$(ui.helper).css({
			                    "cursor": "grab",
			                    "transform": "none",
			                });
		              }
		            }
		        });
		        $('.drop_1').droppable({
		            // accept : "",
		            hoverClass: "hover-counter",
		            drop: function(event, ui) {
		            	var current_val = parseInt($(ui.draggable).data('value'));
		            	var top_position = 5;
		            	var item_height = 25;
		            	var left_position = 0;
		            	if(current_val==1){
		            		item_height = 10;
		            		left_position = (dropped_1*6)%90;
		            		dropped_1++;
		            	} else if(current_val==5){
		            		top_position = 20;
		            		left_position = (dropped_5*6)%80 - 5;
		            		dropped_5++;
		            	} else if(current_val==10){
		            		top_position = 45;
		            		left_position = (dropped_10*6)%80 - 5;
		            		dropped_10++;
		            	} else if(current_val==20){
		            		top_position = 70;
		            		left_position = (dropped_20*6)%80 - 5;
		            		dropped_20++;
		            	}
		            	$(ui.draggable).clone().css({
		            		'height': item_height+'%',
		            		'top': top_position+'%',
		            		'left': left_position+'%'
		            		}).appendTo(".drop_1");

		            	dropped_sum += current_val;
		        		$('.total-amount').html(dropped_sum);
		            }
		        });
		        $('.clear-button').click(function(){
		        	$('.drop_1').html('');
		        	dropped_sum = 0;
		        	dropped_1 = dropped_10 = dropped_20 = dropped_5 = 0;
		        	$('.total-amount').html(dropped_sum);
		        });
		        $('.check-button').click(function(){
		        	if(dropped_sum==selected_item[1]){
		        		$(this).removeClass('ole-template-check-btn-default-incorrect');
		        		$(this).addClass('ole-template-check-btn-default-correct');
		        		$('.check-button, .clear-button').css('pointer-events', 'none');
		        		$(".draggable").css('pointer-events', 'none');
						nav_button_controls(100);
						createjs.Sound.stop();
						play_correct_incorrect_sound(1);
		        	} else {
								createjs.Sound.stop();
		        		$(this).addClass('ole-template-check-btn-default-incorrect');
						play_correct_incorrect_sound(0);
		        	}
		        });
				break;
				case 4:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_4a");
				current_sound.play();
				current_sound.on('complete', function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_4b");
				current_sound.play();
				current_sound.on('complete', function(){
					nav_button_controls(0);
				});
			});
				break;
				case 5:
				play_diy_audio();
				nav_button_controls(2000);
				break;
				case 6:
				sound_nav("sound_6");
				var hint_count = 1;
				input_box('.ole-template-input-box-default', 5, '.check-button');
			 	$('.check-button').click(function(){
		        	if(parseInt($('.ole-template-input-box-default').val())==800){
		        		$(this).removeClass('ole-template-check-btn-default-incorrect');
		        		$(this).addClass('ole-template-check-btn-default-correct');
						$('.ole-template-input-box-default').prop('disabled', true);
		        		$('.ole-template-input-box-default').removeClass('ole-template-input-box-default-incorrect');
		        		$('.ole-template-input-box-default').addClass('ole-template-input-box-default-correct');
						nav_button_controls(100);
						createjs.Sound.stop();
						$('.right-wrong').show(0);
						$('.hint-block, .hint-1, .hint-2').show(0);
						$('.right-wrong').attr('src', 'images/correct.png');
						$('.ole-template-input-box-default').blur();
						play_correct_incorrect_sound(1);
		        	} else {
								createjs.Sound.stop();
		        		$(this).addClass('ole-template-check-btn-default-incorrect');
		        		$('.ole-template-input-box-default').addClass('ole-template-input-box-default-incorrect');
		        		$('.hint-block').show(0);
		        		$('.hint-'+ hint_count).show(0);
						$('.right-wrong').show(0);
						$('.right-wrong').attr('src', 'images/wrong.png');
		        		hint_count++;
						play_correct_incorrect_sound(0);
		        	}
		        });
				break;
				case 9:
				// $(".conclusion").hide(0);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_9a");
				current_sound.play();
				current_sound.on('complete', function(){
				createjs.Sound.stop();
				// $(".conclusion").show(0);
				current_sound = createjs.Sound.play("sound_9b");
				current_sound.play();
				current_sound.on('complete', function(){
					nav_button_controls(0);
				});
			});
				break;
				case 10:
				play_diy_audio();
				nav_button_controls(2000);
				break;
			case 11:
			sound_nav("sound_11");
				var hint_count = 1;
				input_box('.inputblock-1>.ole-template-input-box-default', 5, '.inputblock-1>.check-button');
				input_box('.inputblock-2>.ole-template-input-box-default', 5, '.inputblock-2>.check-button');
			 	$('.inputblock-1>.check-button').click(function(){
		        	if(parseInt($('.inputblock-1>.ole-template-input-box-default').val())==3400){
		        		$(this).removeClass('ole-template-check-btn-default-incorrect');
		        		$(this).addClass('ole-template-check-btn-default-correct');
						$('.inputblock-1>.ole-template-input-box-default').prop('disabled', true);
		        		$('.inputblock-1>.ole-template-input-box-default').removeClass('ole-template-input-box-default-incorrect');
		        		$('.inputblock-1>.ole-template-input-box-default').addClass('ole-template-input-box-default-correct');

						$('.inputblock-1>.ole-template-input-box-default').blur();
						$('.hidden-utb').show(0);
						$('.hidden-itb').css('display', 'flex');
						hint_count = 0;
						createjs.Sound.stop();
						$('.inputblock-1>.right-wrong').show(0);
						$('.hint-block-1, .hint-block-1>.hint-1, .hint-block-1>.hint-2').show(0);
						$('.inputblock-1>.right-wrong').attr('src', 'images/correct.png');
						play_correct_incorrect_sound(1);
		        	} else {
								createjs.Sound.stop();
		        		$(this).addClass('ole-template-check-btn-default-incorrect');
		        		$('.inputblock-1>.ole-template-input-box-default').addClass('ole-template-input-box-default-incorrect');
		        		$('.hint-block-1').show(0);
		        		$('.hint-block-1>.hint-'+ hint_count).show(0);
						$('.inputblock-1>.right-wrong').show(0);
						$('.inputblock-1>.right-wrong').attr('src', 'images/wrong.png');
		        		hint_count++;
						play_correct_incorrect_sound(0);
		        	}
		        });
		        $('.inputblock-2>.check-button').click(function(){
		        	if(parseInt($('.inputblock-2>.ole-template-input-box-default').val())==3440){
		        		$(this).removeClass('ole-template-check-btn-default-incorrect');
		        		$(this).addClass('ole-template-check-btn-default-correct');
						$('.inputblock-2>.ole-template-input-box-default').prop('disabled', true);
		        		$('.inputblock-2>.ole-template-input-box-default').removeClass('ole-template-input-box-default-incorrect');
		        		$('.inputblock-2>.ole-template-input-box-default').addClass('ole-template-input-box-default-correct');

						$('.inputblock-2>.ole-template-input-box-default').blur();
						$('.conclusion-2').fadeIn(100, function(){
							nav_button_controls(100);
						});
						createjs.Sound.stop();
						$('.inputblock-2>.right-wrong').show(0);
						$('.hint-block-2, .hint-block-2>.hint-1, .hint-block-2>.hint-2').show(0);
						$('.inputblock-2>.right-wrong').attr('src', 'images/correct.png');
						play_correct_incorrect_sound(1);
		        	} else {
								createjs.Sound.stop();
		        		$(this).addClass('ole-template-check-btn-default-incorrect');
		        		$('.inputblock-2>.ole-template-input-box-default').addClass('ole-template-input-box-default-incorrect');
		        		$('.hint-block-2').show(0);
		        		$('.hint-block-2>.hint-'+ hint_count).show(0);
						$('.inputblock-2>.right-wrong').show(0);
						$('.inputblock-2>.right-wrong').attr('src', 'images/wrong.png');
		        		hint_count++;
						play_correct_incorrect_sound(0);
		        	}
		        });
				break;
				default:
				sound_player("sound_"+countNext);
				break;
		}
	}

	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(0);
		});
}

	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event){
    		var charCode = (event.which) ? event.which : event.keyCode;
    		/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
    		if(charCode === 13 && button_class!=null) {
		        $(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
   			if (!condition) {
    			return true;
    		}
    		//check if user inputs more than one '.'
			if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
        		return false;
    		}
    		//check . and 0-9 separately after checking arrow and other keys
    		if((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110 ){
    			return false;
    		}
    		//check max no of allowed digits
    		if (String(event.target.value).length >= max_number) {
    			return false;
    		}
  			return true;
		});
	}
	function arrage_items_in_shelf(){
		for(var i=1; i<7; i++){
			var shelf_array = document.getElementsByClassName('shop_item_'+i);
			var total_in_array = shelf_array.length;
			for(var m=0; m<shelf_array.length; m++){
				var left_position = $('.shop_item_'+i).eq(0).position().left+m*$board.width()*0.06;
				$('.shop_item_'+i).eq(m).css({
					'left': left_position,
					'z-index':1 + (total_in_array-m)*1
				});
				// shelf_array[m].style.left = 1 + (total_in_array-m)*1;
			}
		}
	}
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

var imgpath = $ref+"/images/";
var soundpath = $ref+"/sound/";
var soundpath2 = $ref+"/sound/"+$lang+"/";

var ding = new buzz.sound((soundpath + "ding.ogg"));
var sound_1 = new buzz.sound((soundpath2 + "s2_p1.ogg"));
var sound_2 = new buzz.sound((soundpath2 + "s2_p2_1.ogg"));
var sound_3 = new buzz.sound((soundpath2 + "s2_p2_2.ogg"));
var sound_4 = new buzz.sound((soundpath2 + "s2_p2_3.ogg"));
var sound_5 = new buzz.sound((soundpath2 + "s2_p3_1.ogg"));
var sound_6 = new buzz.sound((soundpath2 + "s2_p3_2.ogg"));
var sound_7 = new buzz.sound((soundpath2 + "s2_p4.ogg"));


var content=[
	{
		// slide0
		uppertextblock:[{
			textclass: "description_maintitle",
			textdata: data.string.p2_s0
		}]
	},{
		// slide1
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utbaddition",
		uppertextblock: [{
			textclass: "descriptionheader fade_in",
			textdata: data.string.p2_s1_00
		},{
			textclass: "descriptionheader fade_in1",
			textdata: data.string.p2_s1_01
		}],
		/*
		uppertextblockadditionalclass: "utb_info",
				uppertextblock:[{
					textclass: "description",
					textdata: data.string.p1_s6
				},{
					textclass: "description2",
					textdata: data.string.p1_s7
				},{
					textclass: "description2",
					textdata: data.string.p1_s8
				}],*/

		weightblock:[{
			weighblocksubclass: "beambalance_base2 fade_in2"
		},{
			weighblocksubclass: "drop_left fade_in2"
		},{
			weighblocksubclass: "weight_content_show2 fade_in2"
		}],
		lowertextblockadditionalclass: "orange_ltb fade_in3",
		lowertextblock:[{
			textclass: "description1",
			textdata: data.string.p2_s1_02
		}]
	},{
		// slide2
		contentnocenteradjust: true,
		headerblock: [{
			textclass: "descriptionheader",
			textdata: data.string.p2_s1
		}],
		/*
		uppertextblockadditionalclass: "utb_info",
				uppertextblock:[{
					textclass: "description",
					textdata: data.string.p1_s6
				},{
					textclass: "description2",
					textdata: data.string.p1_s7
				},{
					textclass: "description2",
					textdata: data.string.p1_s8
				}],*/

		weightblock:[{
			weighblocksubclass: "beambalance_base"
		},{
			weighblocksubclass: "drop_left"
		},{
			weighblocksubclass: "weight_content_show"
		}],
		imageblockadditionalclass: "draggablecontainer",
		imageblock:[{
			imageblockclass: "mangoescollection",
			imagestoshow:[{
				imgclass: "platemango",
				imgsrc: imgpath+"plate.png"
			}]
		},{
			imageblockclass: "orangescollection",
			imagestoshow:[{
				imgclass: "plateorange",
				imgsrc: imgpath+"plate.png"
			}]
		},{
			imageblockclass: "potatocollection",
			imagestoshow:[{
				imgclass: "platepotato",
				imgsrc: imgpath+"plate.png"
			}]
		}],
		lowertextblockadditionalclass: "orange_ltb",
		lowertextblock:[{
			textclass: "description1",
			textdata: data.string.p2_s2
		}]
	},{
		// slide3
		contentnocenteradjust: true,
		headerblock: [{
			textclass: "descriptionheader",
			textdata: data.string.p2_s9
		}],
		uppertextblockadditionalclass: "utb_additional_block",
		uppertextblock:[{
			textclass: "description_orange",
			textdata: data.string.p2_s10
		},{
			textclass: "description",
			textdata: data.string.p2_s11
		}],
		imageblock:[{

		}],
		lowertextblockadditionalclass: "ltb_additonal_info",
		lowertextblock:[{
			textclass: "description1_title",
			textdata: data.string.p2_s16
		}],

		inputvalues:[
		{
			inputcontainerclass: "kg_inputcontainer",
			inputclass: "kg_input",
			inputtype: "text",
			suffixdata: data.string.p2_s12
		},
		{
			inputcontainerclass: "gm_inputcontainer",
			inputclass: "gm_input",
			inputtype: "text",
			suffixdata: data.string.p2_s13
		},
		{
			inputclass: "kgtogm_btn",
			inputvalue: data.string.p2_s14,
			inputtype: "button"
		},
		{
			inputclass: "gmtokg_btn",
			inputvalue: data.string.p2_s15,
			inputtype: "button"
		}
		]
	}
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

  loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    vocabcontroller.findwords(countNext);
	    switch(countNext){
				case 0:
				soundplayer(sound_1);
					break;
				case 1:
					$(".weight_content_show2").html("<p class='displayweight'><span class='kgvalue_fruit'>00</span>&nbsp;Kg&nbsp;&nbsp;<span class='gmvalue_fruit'>000</span>&nbsp;gm&nbsp;</p>");
					$(".fade_in1,.orange_ltb").css({"animation":"none","opacity":"1"}).hide();
					buzz.all().stop();
					sound_2.play().bind("ended",function(){
						$(".fade_in1").show(200);
						sound_3.play().bind("ended",function(){
							$(".orange_ltb").show(200);
							sound_4.play().bind("ended",function(){
								navigationcontroller();
							});
						});
					});
					break;
	    	case 2:
				$(".orange_ltb").css({"animation":"none","opacity":"1"}).hide();
				buzz.all().stop();
				sound_5.play().bind("ended",function(){
					$(".orange_ltb").show(200);
					sound_6.play().bind("ended",function(){
						// navigationcontroller();
						});
					});
	    		var $mangocollection = $(".mangoescollection");
	    		var $orangecollection = $(".orangescollection");
	    		var $potatocollection = $(".potatocollection");
	    		$(".weight_content_show").html("<p class='displayweight'><span class='kgvalue_fruit'>00</span>&nbsp;Kg&nbsp;&nbsp;<span class='gmvalue_fruit'>000</span>&nbsp;gm&nbsp;</p>");
	    		var fronttag = "<img class = '";
	    		var midtag = "' src = ";
	    		var endtag = "></img>";
	    		var $kgvalue_fruit = $(".kgvalue_fruit");
	    		var $gmvalue_fruit = $(".gmvalue_fruit");
	    		for(var i = 1; i <= 15; i++){
	    			$mangocollection.append(fronttag +"mango fruit"+ midtag + imgpath + "mango.png"+ endtag);
	    			$orangecollection.append(fronttag +"orange fruit"+ midtag + imgpath + "orange.png"+ endtag);
	    			$potatocollection.append(fronttag +"potato fruit"+ midtag + imgpath + "potato.png"+ endtag);
	    		}



	    		var $mango = $(".mango");
	    		var $orange = $(".orange");
	    		var $potato = $(".potato");

	    		var bottom = 14;
	    		var left = 10;
	    		var z_index = 110;
	    		var bottom_addition = 0;
	    		for(var i = 0; i < $mango.length; i++){
	    			if(i== 5){
	    				bottom = 22;
	    				left = 19.6;
	    				z_index = 109;
	    				bottom_addition = 5;
	    			}else if(i == 9){
	    				bottom = 30;
	    				left = 27.2;
	    				z_index = 108;
	    				bottom_addition += 5;
	    			}else if(i == 12){
	    				bottom = 38;
	    				left = 34.8;
	    				z_index = 107;
	    				bottom_addition += 5;
	    			}else if(i == 14){
	    				bottom = 46;
	    				left = 42.4;
	    				z_index = 106;
	    				bottom_addition += 5;
	    			}
	    			$($mango[i]).css({
	    				"position": "absolute",
	    				"width": "20%",
	    				"left": left+"%",
	    				"bottom": bottom+"%",
	    				"z-index": z_index
	    			});

	    			$($orange[i]).css({
	    				"position": "absolute",
	    				"width": "20%",
	    				"left": left+"%",
	    				"bottom": (bottom+bottom_addition)+"%",
	    				"z-index": z_index
	    			});

	    			$($potato[i]).css({
	    				"position": "absolute",
	    				"width": "20%",
	    				"left": left+"%",
	    				"bottom": bottom+"%",
	    				"z-index": z_index
	    			});
	    			left += 15.2;
	    		}

	    		$(".fruit").draggable({
	    					containment : "body",
							cursor : "grab",
							revert : "invalid",
							appendTo : "body",
							helper : "clone",
							zindex : 1000,
							start: function(event, ui){
								ui.helper.css({"width":"3%"});
								$(this).css( "filter", "grayscale(100%)");
							},
							stop: function(event, ui){
								$(this).css( "filter", "none");
							}
					});

				$(".drop_left").droppable({
					accept : function(dropelem){
						var i = 0;
						if(dropelem.hasClass("fruit")){
							var $leftimages = $(".drop_left> img");

							if($leftimages.length > 14){
								return false;
							}else{
								return true;
							}
						}else{
							return false;
						}
					},
					hoverClass : "hovered",
					drop : function upondrop(event, ui){
							handleCardDrop2(event, ui);
					}
				});

				var fruitsdropped = 0;
				function handleCardDrop2(event, ui){
					var $droppedclone = $($(ui.helper).clone());
					// var $leftimages = $(".drop_left> img");

					// if($leftimages.length > 14)
	   						// return true;

	   				var fruitclassName = $droppedclone[0].classList[0];
		   				switch(fruitclassName){
		   					case "mango":
		   						fruitsdropped += 200;
		   						break;

		   					case "orange":
		   						fruitsdropped += 100;
		   						break;

		   					case "potato":
		   						fruitsdropped += 150;
		   						break;

		   					default:
		   						break;
		   				}

		   			$droppedclone.detach().appendTo($(".drop_left"));
   					rearangeimages("drop_left");
   					addclicklistener($droppedclone, "drop_right");

   					if(animationinprogress){
   						animationinprogress = false;
   						clearInterval(intervalid);
   					}
   					animatethedisplayofweight(fruitsdropped);
				}

				var animationinprogress = false;
				var intervalid;
				function animatethedisplayofweight(newkg){
					var oldkg = parseInt($kgvalue_fruit.html())*1000+ parseInt($gmvalue_fruit.html());
					var tempoldkg = oldkg;
					var currentkgval = 0;
					var currentgmval = 0;
					animationinprogress = true;
					intervalid = setInterval(function(){
						$nextBtn.hide(0);
						if(newkg == tempoldkg){
							animationinprogress = false;
							$nextBtn.delay(400).show(0);
							ding.play();
							clearInterval(intervalid);
						}

						currentkgval = Math.floor(tempoldkg/1000);
						$kgvalue_fruit.html("0"+currentkgval);
						currentgmval = tempoldkg - currentkgval*1000;
						if(currentgmval < 10){
							$gmvalue_fruit.html("00"+currentgmval);
						}if(currentgmval < 100){
							$gmvalue_fruit.html("0"+currentgmval);
						}else{
							$gmvalue_fruit.html(currentgmval);
						}

						if(newkg > oldkg){
							tempoldkg += 5;
						}else{
							tempoldkg -= 5;
						}


					}, 40);
				}

				function rearangeimages(classname){
	   				var $content = $("."+classname+"> img");
	   				bottom = 8;
		    		left = 10;
		    		z_index = 110;
		    		bottom_addition = 0;
	   				for (var i = 0; i < $content.length; i++){

	   					if(i== 5){
		    				bottom = 18;
		    				left = 19.6;
		    				z_index = 109;
		    				bottom_addition = 8;
		    			}else if(i == 9){
		    				bottom = 26;
		    				left = 27.2;
		    				z_index = 108;
		    				bottom_addition += 8;
		    			}else if(i == 12){
		    				bottom = 34;
		    				left = 34.8;
		    				z_index = 107;
		    				bottom_addition += 8;
		    			}else if(i == 14){
		    				bottom = 42;
		    				left = 42.4;
		    				z_index = 106;
		    				bottom_addition += 8;
		    			}
	   					$($content[i]).css({
		    				"position": "absolute",
		    				"width": "13%",
								"height":"22%",
		    				"left": left+"%",
		    				"bottom": (bottom+bottom_addition)+"%",
		    				"z-index": z_index,
		    				"top": "initial"
	    				});
	    				left += 12.2;
	   				}
	   			}

	   			function addclicklistener($droppedclone, classname){
	   					$droppedclone.click(function (){
		   					var $this = $(this);
	   						switch($this[0].classList[0]){
			   					case "mango":
			   						fruitsdropped -= 200;
			   						break;

			   					case "orange":
			   						fruitsdropped -= 100;
			   						break;

			   					case "potato":
			   						fruitsdropped -= 150;
			   						break;

			   					default:
			   						break;
			   				}

		   					$this.remove();

		   					rearangeimages("drop_left");
		   					if(animationinprogress){
		   						animationinprogress = false;
		   						clearInterval(intervalid);
		   					}

		   					animatethedisplayofweight(fruitsdropped);

		   				});
	   			}

	    		break;
	   		case 3:
					soundplayer(sound_7,1);
	   			var $kgtogmbtn = $(".kgtogm_btn");
	   			var $gmtokgbtn = $(".gmtokg_btn");

	   			var $kg_input = $(".kg_input");
	   			var $gm_input = $(".gm_input");

	   			var $ltb_additonal_info = $(".ltb_additonal_info");
	   			var old_kg_value = [];
	   			var old_gm_value = [];
	   			$kgtogmbtn.click(function(){
	   				var kgval = parseFloat($kg_input.val());
	   				if(isNaN(kgval)){
	   					$kg_input.val("");
	   					$gm_input.val("");
	   					return true;
	   				}
	   				var gmval = kgval * 1000;
						if(gmval > 10000000000){
							$('.gm_input').val(0);
							$('.kg_input').val(0);
							return true;
						}else{
							$('.gm_input').val(gmval);
						}

	   				var p_in_ltb = $(".ltb_additonal_info > p");
						if(old_gm_value.indexOf(gmval) != -1){
							return true;
						}
						console.log("old_gm_value", old_gm_value);
						old_gm_value.push(gmval);
	   				if(p_in_ltb.length > 4){
								$('input').prop("disabled", true);
								old_gm_value.splice(0, 1);
		   					$(p_in_ltb[1]).addClass('slideout');
								setTimeout(function(){
									$(p_in_ltb[1]).remove();
									var $p = $("<p class='description1 slidein'>"+kgval + " "+data.string.e1_s1+" = "+ gmval + " "+data.string.e1_s2+"</p>");
									$ltb_additonal_info.append($p);
									$('input').prop("disabled", false);
									ole.footerNotificationHandler.lessonEndSetNotification();
								}, 1000);
	   				}else{
							var $p = $("<p class='description1 slidein'>"+kgval + " "+data.string.e1_s1+" = "+ gmval + " "+data.string.e1_s2+"</p>");
							$ltb_additonal_info.append($p);
						}
	   			});

	   			$gmtokgbtn.click(function(){
	   				var gmval = parseFloat($gm_input.val());
	   				if(isNaN(gmval)){
	   					$kg_input.val("");
	   					$gm_input.val("");
	   					return true;
	   				}
	   				var kgval = gmval/1000;

						if(kgval > 10000000000){
							$('.gm_input').val(0);
							$('.kg_input').val(0);
							return true;
						}else{
							$('.kg_input').val(kgval);
						}
	   				var p_in_ltb = $(".ltb_additonal_info > p");

						if(old_kg_value.indexOf(kgval) != -1){
							return true;
						}
						console.log("old_kg_value", old_kg_value);
						old_kg_value.push(kgval);

	   				if(p_in_ltb.length > 4){
							// ole.footerNotificationHandler.pageEndSetNotification();
							// 		$ltb_additonal_info.html("");
							// 		$ltb_additonal_info.append(p_in_ltb[0]);
							// 		$ltb_additonal_info.append(p_in_ltb[4]);
							// 		$ltb_additonal_info.append(p_in_ltb[5]);
							// $('input').attr("disabled", true);
							$('input').prop("disabled", true);
							old_kg_value.splice(0, 1);
							$(p_in_ltb[1]).addClass('slideout');
							setTimeout(function(){
								$(p_in_ltb[1]).remove();
								var $p = $("<p class='description1 slidein'>"+kgval + " "+data.string.e1_s1+" = "+ gmval + " "+data.string.e1_s2+"</p>");
								$ltb_additonal_info.append($p);
								$('input').prop("disabled", false);
							}, 1000);
							ole.footerNotificationHandler.lessonEndSetNotification();

	   				}else{
							var $p = $("<p class='description1 slidein'>"+kgval + " "+data.string.e1_s1+" = "+ gmval + " "+data.string.e1_s2+"</p>");
							$ltb_additonal_info.append($p);
							ole.footerNotificationHandler.lessonEndSetNotification();

						}
	   			});
	   		default:
	   			break;
	    }
  }

	function soundplayer(i,next){
		buzz.all().stop();
		i.play().bind("ended",function(){
			if (!next) navigationcontroller();
		});
	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    // navigationcontroller();

    // call the template
    generalTemplate();
    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

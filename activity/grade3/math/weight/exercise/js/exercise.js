Array.prototype.shufflearray = function() {
    var i = this.length,
    j,
    temp;
    while (--i > 0) {
        j = Math.floor(Math.random() * (i + 1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
    return this;
};


var imgpath = $ref+"/exercise/images/";
var soundpath = $ref+"/sound/";
var soundpath2 = $ref+"/sound/"+$lang+"/";


var ex_ins = new buzz.sound((soundpath2 + "ex.ogg"));

var ding = new buzz.sound((soundpath + "ding.ogg"));
var coin_drag = new buzz.sound((soundpath + "coin_drag.ogg"));

var content=[
	{
		// slide0
		contentnocenteradjust: true,
		headerblock: [{
			textclass: "descriptionheader",
			textdata: data.string.e1_s0
		}],


		uppertextblockadditionalclass: "utb_info",
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p1_s6
		},{
			textclass: "description2",
			textdata: data.string.p1_s7
		},{
			textclass: "description2",
			textdata: data.string.p1_s8
		}],

		weightblock:[{
			weighblocksubclass: "beambalance_base"
		},{
			weighblocksubclass: "beambalance_left"
		},{
			weighblocksubclass: "beambalance_right"
		},{
			weighblocksubclass: "beambalance_needle"
		},{
			weighblocksubclass: "drop_left"
		},{
			weighblocksubclass: "drop_right"
		}],
		imageblockadditionalclass: "draggablecontainer",
		imageblock:[{
			imageblockclass: "mangoescollection",
			imagestoshow:[{
				imgclass: "platemango",
				imgsrc: imgpath+"plate.png"
			},{
				imgclass: "tomato fruit",
				imgsrc: imgpath+"tomato-15.png"
			}]
		}/*
		,{
					imageblockclass: "orangescollection",
					imagestoshow:[{
						imgclass: "plateorange",
						imgsrc: imgpath+"plate.png"
					}]
				},{
					imageblockclass: "potatocollection",
					imagestoshow:[{
						imgclass: "platepotato",
						imgsrc: imgpath+"plate.png"
					}]
				}*/
		,{
			imageblockclass: "calibrated_weight4",
			imagestoshow:[{
				imgclass: "dhak1 weightdrop",
				imgsrc: imgpath+"50gm.png"
			},
			{
				imgclass: "dhak2 weightdrop",
				imgsrc: imgpath+"100gm.png"
			},
			{
				imgclass: "dhak3 weightdrop",
				imgsrc: imgpath+"200gm.png"
			},
			{
				imgclass: "dhak4 weightdrop",
				imgsrc: imgpath+"500gm.png"
			},
			{
				imgclass: "dhak5 weightdrop",
				imgsrc: imgpath+"1kg.png"
			}]
		}],

		inputvalues:[
		{
			inputcontainerclass: "kg_inputcontainer",
			inputclass: "kg_input",
			inputtype: "text",
			suffixdata: data.string.e1_s1
		},
		{
			inputcontainerclass: "gm_inputcontainer",
			inputclass: "gm_input",
			inputtype: "text",
			suffixdata: data.string.e1_s2
		},
		{
			inputclass: "kgtogm_btn",
			inputvalue: data.string.e1_s3,
			inputtype: "button"
		}
		]
	},{
		// slide1
		contentnocenteradjust: true,
		headerblock: [{
			textclass: "descriptionheader",
			textdata: data.string.e1_s0
		}],


		uppertextblockadditionalclass: "utb_info",
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p1_s6
		},{
			textclass: "description2",
			textdata: data.string.p1_s7
		},{
			textclass: "description2",
			textdata: data.string.p1_s8
		}],

		weightblock:[{
			weighblocksubclass: "beambalance_base"
		},{
			weighblocksubclass: "beambalance_left"
		},{
			weighblocksubclass: "beambalance_right"
		},{
			weighblocksubclass: "beambalance_needle"
		},{
			weighblocksubclass: "drop_left"
		},{
			weighblocksubclass: "drop_right"
		}],
		imageblockadditionalclass: "draggablecontainer",
		imageblock:[{
			imageblockclass: "mangoescollection",
			imagestoshow:[{
				imgclass: "platemango",
				imgsrc: imgpath+"plate.png"
			},{
				imgclass: "peaches fruit",
				imgsrc: imgpath+"peaches-15.png"
			}]
		}/*
		,{
					imageblockclass: "orangescollection",
					imagestoshow:[{
						imgclass: "plateorange",
						imgsrc: imgpath+"plate.png"
					}]
				},{
					imageblockclass: "potatocollection",
					imagestoshow:[{
						imgclass: "platepotato",
						imgsrc: imgpath+"plate.png"
					}]
				}*/
		,{
			imageblockclass: "calibrated_weight4",
			imagestoshow:[{
				imgclass: "dhak1 weightdrop",
				imgsrc: imgpath+"50gm.png"
			},
			{
				imgclass: "dhak2 weightdrop",
				imgsrc: imgpath+"100gm.png"
			},
			{
				imgclass: "dhak3 weightdrop",
				imgsrc: imgpath+"200gm.png"
			},
			{
				imgclass: "dhak4 weightdrop",
				imgsrc: imgpath+"500gm.png"
			},
			{
				imgclass: "dhak5 weightdrop",
				imgsrc: imgpath+"1kg.png"
			}]
		}],

		inputvalues:[
		{
			inputcontainerclass: "kg_inputcontainer",
			inputclass: "kg_input",
			inputtype: "text",
			suffixdata: data.string.e1_s1
		},
		{
			inputcontainerclass: "gm_inputcontainer",
			inputclass: "gm_input",
			inputtype: "text",
			suffixdata: data.string.e1_s2
		},
		{
			inputclass: "kgtogm_btn",
			inputvalue: data.string.e1_s3,
			inputtype: "button"
		}
		]
	},{
		// slide2
		contentnocenteradjust: true,
		headerblock: [{
			textclass: "descriptionheader",
			textdata: data.string.e1_s0
		}],


		uppertextblockadditionalclass: "utb_info",
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p1_s6
		},{
			textclass: "description2",
			textdata: data.string.p1_s7
		},{
			textclass: "description2",
			textdata: data.string.p1_s8
		}],

		weightblock:[{
			weighblocksubclass: "beambalance_base"
		},{
			weighblocksubclass: "beambalance_left"
		},{
			weighblocksubclass: "beambalance_right"
		},{
			weighblocksubclass: "beambalance_needle"
		},{
			weighblocksubclass: "drop_left"
		},{
			weighblocksubclass: "drop_right"
		}],
		imageblockadditionalclass: "draggablecontainer",
		imageblock:[{
			imageblockclass: "mangoescollection",
			imagestoshow:[{
				imgclass: "platemango",
				imgsrc: imgpath+"plate.png"
			},{
				imgclass: "bitter_gourds fruit",
				imgsrc: imgpath+"bitter_gourds-12.png"
			}]
		}/*
		,{
					imageblockclass: "orangescollection",
					imagestoshow:[{
						imgclass: "plateorange",
						imgsrc: imgpath+"plate.png"
					}]
				},{
					imageblockclass: "potatocollection",
					imagestoshow:[{
						imgclass: "platepotato",
						imgsrc: imgpath+"plate.png"
					}]
				}*/
		,{
			imageblockclass: "calibrated_weight4",
			imagestoshow:[{
				imgclass: "dhak1 weightdrop",
				imgsrc: imgpath+"50gm.png"
			},
			{
				imgclass: "dhak2 weightdrop",
				imgsrc: imgpath+"100gm.png"
			},
			{
				imgclass: "dhak3 weightdrop",
				imgsrc: imgpath+"200gm.png"
			},
			{
				imgclass: "dhak4 weightdrop",
				imgsrc: imgpath+"500gm.png"
			},
			{
				imgclass: "dhak5 weightdrop",
				imgsrc: imgpath+"1kg.png"
			}]
		}],

		inputvalues:[
		{
			inputcontainerclass: "kg_inputcontainer",
			inputclass: "kg_input",
			inputtype: "text",
			suffixdata: data.string.e1_s1
		},
		{
			inputcontainerclass: "gm_inputcontainer",
			inputclass: "gm_input",
			inputtype: "text",
			suffixdata: data.string.e1_s2
		},
		{
			inputclass: "kgtogm_btn",
			inputvalue: data.string.e1_s3,
			inputtype: "button"
		}
		]
	},{
		// slide3
		contentnocenteradjust: true,
		headerblock: [{
			textclass: "descriptionheader",
			textdata: data.string.e1_s0
		}],


		uppertextblockadditionalclass: "utb_info",
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p1_s6
		},{
			textclass: "description2",
			textdata: data.string.p1_s7
		},{
			textclass: "description2",
			textdata: data.string.p1_s8
		}],

		weightblock:[{
			weighblocksubclass: "beambalance_base"
		},{
			weighblocksubclass: "beambalance_left"
		},{
			weighblocksubclass: "beambalance_right"
		},{
			weighblocksubclass: "beambalance_needle"
		},{
			weighblocksubclass: "drop_left"
		},{
			weighblocksubclass: "drop_right"
		}],
		imageblockadditionalclass: "draggablecontainer",
		imageblock:[{
			imageblockclass: "mangoescollection",
			imagestoshow:[{
				imgclass: "platemango",
				imgsrc: imgpath+"plate.png"
			},{
				imgclass: "oranges fruit",
				imgsrc: imgpath+"orange-9.png"
			}]
		}/*
		,{
					imageblockclass: "orangescollection",
					imagestoshow:[{
						imgclass: "plateorange",
						imgsrc: imgpath+"plate.png"
					}]
				},{
					imageblockclass: "potatocollection",
					imagestoshow:[{
						imgclass: "platepotato",
						imgsrc: imgpath+"plate.png"
					}]
				}*/
		,{
			imageblockclass: "calibrated_weight4",
			imagestoshow:[{
				imgclass: "dhak1 weightdrop",
				imgsrc: imgpath+"50gm.png"
			},
			{
				imgclass: "dhak2 weightdrop",
				imgsrc: imgpath+"100gm.png"
			},
			{
				imgclass: "dhak3 weightdrop",
				imgsrc: imgpath+"200gm.png"
			},
			{
				imgclass: "dhak4 weightdrop",
				imgsrc: imgpath+"500gm.png"
			},
			{
				imgclass: "dhak5 weightdrop",
				imgsrc: imgpath+"1kg.png"
			}]
		}],

		inputvalues:[
		{
			inputcontainerclass: "kg_inputcontainer",
			inputclass: "kg_input",
			inputtype: "text",
			suffixdata: data.string.e1_s1
		},
		{
			inputcontainerclass: "gm_inputcontainer",
			inputclass: "gm_input",
			inputtype: "text",
			suffixdata: data.string.e1_s2
		},
		{
			inputclass: "kgtogm_btn",
			inputvalue: data.string.e1_s3,
			inputtype: "button"
		}
		]
	},{
		// slide4
		contentnocenteradjust: true,
		headerblock: [{
			textclass: "descriptionheader",
			textdata: data.string.e1_s0
		}],


		uppertextblockadditionalclass: "utb_info",
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p1_s6
		},{
			textclass: "description2",
			textdata: data.string.p1_s7
		},{
			textclass: "description2",
			textdata: data.string.p1_s8
		}],

		weightblock:[{
			weighblocksubclass: "beambalance_base"
		},{
			weighblocksubclass: "beambalance_left"
		},{
			weighblocksubclass: "beambalance_right"
		},{
			weighblocksubclass: "beambalance_needle"
		},{
			weighblocksubclass: "drop_left"
		},{
			weighblocksubclass: "drop_right"
		}],
		imageblockadditionalclass: "draggablecontainer",
		imageblock:[{
			imageblockclass: "mangoescollection",
			imagestoshow:[{
				imgclass: "platemango",
				imgsrc: imgpath+"plate.png"
			},{
				imgclass: "apples fruit",
				imgsrc: imgpath+"apple01.png"
			}]
		}/*
		,{
					imageblockclass: "orangescollection",
					imagestoshow:[{
						imgclass: "plateorange",
						imgsrc: imgpath+"plate.png"
					}]
				},{
					imageblockclass: "potatocollection",
					imagestoshow:[{
						imgclass: "platepotato",
						imgsrc: imgpath+"plate.png"
					}]
				}*/
		,{
			imageblockclass: "calibrated_weight4",
			imagestoshow:[{
				imgclass: "dhak1 weightdrop",
				imgsrc: imgpath+"50gm.png"
			},
			{
				imgclass: "dhak2 weightdrop",
				imgsrc: imgpath+"100gm.png"
			},
			{
				imgclass: "dhak3 weightdrop",
				imgsrc: imgpath+"200gm.png"
			},
			{
				imgclass: "dhak4 weightdrop",
				imgsrc: imgpath+"500gm.png"
			},
			{
				imgclass: "dhak5 weightdrop",
				imgsrc: imgpath+"1kg.png"
			}]
		}],

		inputvalues:[
		{
			inputcontainerclass: "kg_inputcontainer",
			inputclass: "kg_input",
			inputtype: "text",
			suffixdata: data.string.e1_s1
		},
		{
			inputcontainerclass: "gm_inputcontainer",
			inputclass: "gm_input",
			inputtype: "text",
			suffixdata: data.string.e1_s2
		},
		{
			inputclass: "kgtogm_btn",
			inputvalue: data.string.e1_s3,
			inputtype: "button"
		}
		]
	},{
		// slide5
		contentnocenteradjust: true,
		headerblock: [{
			textclass: "descriptionheader",
			textdata: data.string.e1_s0
		}],


		uppertextblockadditionalclass: "utb_info",
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p1_s6
		},{
			textclass: "description2",
			textdata: data.string.p1_s7
		},{
			textclass: "description2",
			textdata: data.string.p1_s8
		}],

		weightblock:[{
			weighblocksubclass: "beambalance_base"
		},{
			weighblocksubclass: "beambalance_left"
		},{
			weighblocksubclass: "beambalance_right"
		},{
			weighblocksubclass: "beambalance_needle"
		},{
			weighblocksubclass: "drop_left"
		},{
			weighblocksubclass: "drop_right"
		}],
		imageblockadditionalclass: "draggablecontainer",
		imageblock:[{
			imageblockclass: "mangoescollection",
			imagestoshow:[{
				imgclass: "platemango",
				imgsrc: imgpath+"plate.png"
			},{
				imgclass: "papaya fruit",
				imgsrc: imgpath+"papaya-4.png"
			}]
		}/*
		,{
					imageblockclass: "orangescollection",
					imagestoshow:[{
						imgclass: "plateorange",
						imgsrc: imgpath+"plate.png"
					}]
				},{
					imageblockclass: "potatocollection",
					imagestoshow:[{
						imgclass: "platepotato",
						imgsrc: imgpath+"plate.png"
					}]
				}*/
		,{
			imageblockclass: "calibrated_weight4",
			imagestoshow:[{
				imgclass: "dhak1 weightdrop",
				imgsrc: imgpath+"50gm.png"
			},
			{
				imgclass: "dhak2 weightdrop",
				imgsrc: imgpath+"100gm.png"
			},
			{
				imgclass: "dhak3 weightdrop",
				imgsrc: imgpath+"200gm.png"
			},
			{
				imgclass: "dhak4 weightdrop",
				imgsrc: imgpath+"500gm.png"
			},
			{
				imgclass: "dhak5 weightdrop",
				imgsrc: imgpath+"1kg.png"
			}]
		}],

		inputvalues:[
		{
			inputcontainerclass: "kg_inputcontainer",
			inputclass: "kg_input",
			inputtype: "text",
			suffixdata: data.string.e1_s1
		},
		{
			inputcontainerclass: "gm_inputcontainer",
			inputclass: "gm_input",
			inputtype: "text",
			suffixdata: data.string.e1_s2
		},
		{
			inputclass: "kgtogm_btn",
			inputvalue: data.string.e1_s3,
			inputtype: "button"
		}
		]
	},{
		// slide6
		contentnocenteradjust: true,
		headerblock: [{
			textclass: "descriptionheader",
			textdata: data.string.e1_s0
		}],


		uppertextblockadditionalclass: "utb_info",
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p1_s6
		},{
			textclass: "description2",
			textdata: data.string.p1_s7
		},{
			textclass: "description2",
			textdata: data.string.p1_s8
		}],

		weightblock:[{
			weighblocksubclass: "beambalance_base"
		},{
			weighblocksubclass: "beambalance_left"
		},{
			weighblocksubclass: "beambalance_right"
		},{
			weighblocksubclass: "beambalance_needle"
		},{
			weighblocksubclass: "drop_left"
		},{
			weighblocksubclass: "drop_right"
		}],
		imageblockadditionalclass: "draggablecontainer",
		imageblock:[{
			imageblockclass: "mangoescollection",
			imagestoshow:[{
				imgclass: "platemango",
				imgsrc: imgpath+"plate.png"
			},{
				imgclass: "brinjal fruit",
				imgsrc: imgpath+"brinjal-3.png"
			}]
		}/*
		,{
					imageblockclass: "orangescollection",
					imagestoshow:[{
						imgclass: "plateorange",
						imgsrc: imgpath+"plate.png"
					}]
				},{
					imageblockclass: "potatocollection",
					imagestoshow:[{
						imgclass: "platepotato",
						imgsrc: imgpath+"plate.png"
					}]
				}*/
		,{
			imageblockclass: "calibrated_weight4",
			imagestoshow:[{
				imgclass: "dhak1 weightdrop",
				imgsrc: imgpath+"50gm.png"
			},
			{
				imgclass: "dhak2 weightdrop",
				imgsrc: imgpath+"100gm.png"
			},
			{
				imgclass: "dhak3 weightdrop",
				imgsrc: imgpath+"200gm.png"
			},
			{
				imgclass: "dhak4 weightdrop",
				imgsrc: imgpath+"500gm.png"
			},
			{
				imgclass: "dhak5 weightdrop",
				imgsrc: imgpath+"1kg.png"
			}]
		}],

		inputvalues:[
		{
			inputcontainerclass: "kg_inputcontainer",
			inputclass: "kg_input",
			inputtype: "text",
			suffixdata: data.string.e1_s1
		},
		{
			inputcontainerclass: "gm_inputcontainer",
			inputclass: "gm_input",
			inputtype: "text",
			suffixdata: data.string.e1_s2
		},
		{
			inputclass: "kgtogm_btn",
			inputvalue: data.string.e1_s3,
			inputtype: "button"
		}
		]
	},{
		// slide7
		contentnocenteradjust: true,
		headerblock: [{
			textclass: "descriptionheader",
			textdata: data.string.e1_s0
		}],


		uppertextblockadditionalclass: "utb_info",
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p1_s6
		},{
			textclass: "description2",
			textdata: data.string.p1_s7
		},{
			textclass: "description2",
			textdata: data.string.p1_s8
		}],

		weightblock:[{
			weighblocksubclass: "beambalance_base"
		},{
			weighblocksubclass: "beambalance_left"
		},{
			weighblocksubclass: "beambalance_right"
		},{
			weighblocksubclass: "beambalance_needle"
		},{
			weighblocksubclass: "drop_left"
		},{
			weighblocksubclass: "drop_right"
		}],
		imageblockadditionalclass: "draggablecontainer",
		imageblock:[{
			imageblockclass: "mangoescollection",
			imagestoshow:[{
				imgclass: "platemango",
				imgsrc: imgpath+"plate.png"
			},{
				imgclass: "cabbage fruit",
				imgsrc: imgpath+"cabbages-3.png"
			}]
		}/*
		,{
					imageblockclass: "orangescollection",
					imagestoshow:[{
						imgclass: "plateorange",
						imgsrc: imgpath+"plate.png"
					}]
				},{
					imageblockclass: "potatocollection",
					imagestoshow:[{
						imgclass: "platepotato",
						imgsrc: imgpath+"plate.png"
					}]
				}*/
		,{
			imageblockclass: "calibrated_weight4",
			imagestoshow:[{
				imgclass: "dhak1 weightdrop",
				imgsrc: imgpath+"50gm.png"
			},
			{
				imgclass: "dhak2 weightdrop",
				imgsrc: imgpath+"100gm.png"
			},
			{
				imgclass: "dhak3 weightdrop",
				imgsrc: imgpath+"200gm.png"
			},
			{
				imgclass: "dhak4 weightdrop",
				imgsrc: imgpath+"500gm.png"
			},
			{
				imgclass: "dhak5 weightdrop",
				imgsrc: imgpath+"1kg.png"
			}]
		}],

		inputvalues:[
		{
			inputcontainerclass: "kg_inputcontainer",
			inputclass: "kg_input",
			inputtype: "text",
			suffixdata: data.string.e1_s1
		},
		{
			inputcontainerclass: "gm_inputcontainer",
			inputclass: "gm_input",
			inputtype: "text",
			suffixdata: data.string.e1_s2
		},
		{
			inputclass: "kgtogm_btn",
			inputvalue: data.string.e1_s3,
			inputtype: "button"
		}
		]
	},{
		// slide8
		contentnocenteradjust: true,
		headerblock: [{
			textclass: "descriptionheader",
			textdata: data.string.e1_s0
		}],


		uppertextblockadditionalclass: "utb_info",
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p1_s6
		},{
			textclass: "description2",
			textdata: data.string.p1_s7
		},{
			textclass: "description2",
			textdata: data.string.p1_s8
		}],

		weightblock:[{
			weighblocksubclass: "beambalance_base"
		},{
			weighblocksubclass: "beambalance_left"
		},{
			weighblocksubclass: "beambalance_right"
		},{
			weighblocksubclass: "beambalance_needle"
		},{
			weighblocksubclass: "drop_left"
		},{
			weighblocksubclass: "drop_right"
		}],
		imageblockadditionalclass: "draggablecontainer",
		imageblock:[{
			imageblockclass: "mangoescollection",
			imagestoshow:[{
				imgclass: "platemango",
				imgsrc: imgpath+"plate.png"
			},{
				imgclass: "cauliflower fruit",
				imgsrc: imgpath+"cauliflower-3.png"
			}]
		}/*
		,{
					imageblockclass: "orangescollection",
					imagestoshow:[{
						imgclass: "plateorange",
						imgsrc: imgpath+"plate.png"
					}]
				},{
					imageblockclass: "potatocollection",
					imagestoshow:[{
						imgclass: "platepotato",
						imgsrc: imgpath+"plate.png"
					}]
				}*/
		,{
			imageblockclass: "calibrated_weight4",
			imagestoshow:[{
				imgclass: "dhak1 weightdrop",
				imgsrc: imgpath+"50gm.png"
			},
			{
				imgclass: "dhak2 weightdrop",
				imgsrc: imgpath+"100gm.png"
			},
			{
				imgclass: "dhak3 weightdrop",
				imgsrc: imgpath+"200gm.png"
			},
			{
				imgclass: "dhak4 weightdrop",
				imgsrc: imgpath+"500gm.png"
			},
			{
				imgclass: "dhak5 weightdrop",
				imgsrc: imgpath+"1kg.png"
			}]
		}],

		inputvalues:[
		{
			inputcontainerclass: "kg_inputcontainer",
			inputclass: "kg_input",
			inputtype: "text",
			suffixdata: data.string.e1_s1
		},
		{
			inputcontainerclass: "gm_inputcontainer",
			inputclass: "gm_input",
			inputtype: "text",
			suffixdata: data.string.e1_s2
		},
		{
			inputclass: "kgtogm_btn",
			inputvalue: data.string.e1_s3,
			inputtype: "button"
		}
		]
	},{
		// slide9
		contentnocenteradjust: true,
		headerblock: [{
			textclass: "descriptionheader",
			textdata: data.string.e1_s0
		}],


		uppertextblockadditionalclass: "utb_info",
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p1_s6
		},{
			textclass: "description2",
			textdata: data.string.p1_s7
		},{
			textclass: "description2",
			textdata: data.string.p1_s8
		}],

		weightblock:[{
			weighblocksubclass: "beambalance_base"
		},{
			weighblocksubclass: "beambalance_left"
		},{
			weighblocksubclass: "beambalance_right"
		},{
			weighblocksubclass: "beambalance_needle"
		},{
			weighblocksubclass: "drop_left"
		},{
			weighblocksubclass: "drop_right"
		}],
		imageblockadditionalclass: "draggablecontainer",
		imageblock:[{
			imageblockclass: "mangoescollection",
			imagestoshow:[{
				imgclass: "platemango",
				imgsrc: imgpath+"plate.png"
			},{
				imgclass: "pumpkin fruit",
				imgsrc: imgpath+"pumkin02.png"
			}]
		}/*
		,{
					imageblockclass: "orangescollection",
					imagestoshow:[{
						imgclass: "plateorange",
						imgsrc: imgpath+"plate.png"
					}]
				},{
					imageblockclass: "potatocollection",
					imagestoshow:[{
						imgclass: "platepotato",
						imgsrc: imgpath+"plate.png"
					}]
				}*/
		,{
			imageblockclass: "calibrated_weight4",
			imagestoshow:[{
				imgclass: "dhak1 weightdrop",
				imgsrc: imgpath+"50gm.png"
			},
			{
				imgclass: "dhak2 weightdrop",
				imgsrc: imgpath+"100gm.png"
			},
			{
				imgclass: "dhak3 weightdrop",
				imgsrc: imgpath+"200gm.png"
			},
			{
				imgclass: "dhak4 weightdrop",
				imgsrc: imgpath+"500gm.png"
			},
			{
				imgclass: "dhak5 weightdrop",
				imgsrc: imgpath+"1kg.png"
			}]
		}],

		inputvalues:[
		{
			inputcontainerclass: "kg_inputcontainer",
			inputclass: "kg_input",
			inputtype: "text",
			suffixdata: data.string.e1_s1
		},
		{
			inputcontainerclass: "gm_inputcontainer",
			inputclass: "gm_input",
			inputtype: "text",
			suffixdata: data.string.e1_s2
		},
		{
			inputclass: "kgtogm_btn",
			inputvalue: data.string.e1_s3,
			inputtype: "button"
		}
		]
	}
];

content.shufflearray();

$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  // var vocabcontroller =  new Vocabulary();
	// vocabcontroller.init();

  loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			// $nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
	var score = 0;
	var testin = new EggTemplate();
	// testin.init(10);
	  //eggTemplate.eggMove(countNext);
	testin.init($total_page);
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    // vocabcontroller.findwords(countNext);
		// splitintofractions($(".fractionblock"));
      if(countNext==0) soundplayer(ex_ins,1);
	    switch(countNext){
	    	case 0:
	    	case 1:
	    	case 2:
	    	case 3:
	    	case 4:
	    	case 5:
	    	case 6:
	    	case 7:
	    	case 8:
	    	case 9:
	    		$nextBtn.hide(0);
	    		var $mangocollection = $(".mangoescollection");
	    		// var $orangecollection = $(".orangescollection");
	    		// var $potatocollection = $(".potatocollection");
	    		// var fronttag = "<img class = '";
	    		// var midtag = "' src = ";
	    		// var endtag = "></img>";
	    		// for(var i = 1; i <= 15; i++){
	    			// $mangocollection.append(fronttag +"mango fruit"+ midtag + imgpath + "mango.png"+ endtag);
	    			// // $orangecollection.append(fronttag +"orange fruit"+ midtag + imgpath + "orange.png"+ endtag);
	    			// // $potatocollection.append(fronttag +"potato fruit"+ midtag + imgpath + "potato.png"+ endtag);
	    		// }

	    		var $mango = $(".fruit");
	    		// var $orange = $(".orange");
	    		// var $potato = $(".potato");

	    		var bottom = 12;
	    		var left = 10;
	    		var z_index = 110;
	    		var bottom_addition = 0;
	    		for(var i = 0; i < $mango.length; i++){
	    			if(i== 5){
	    				bottom = 22;
	    				left = 19.6;
	    				z_index = 109;
	    				bottom_addition = 5;
	    			}else if(i == 9){
	    				bottom = 30;
	    				left = 27.2;
	    				z_index = 108;
	    				bottom_addition += 5;
	    			}else if(i == 12){
	    				bottom = 38;
	    				left = 34.8;
	    				z_index = 107;
	    				bottom_addition += 5;
	    			}else if(i == 14){
	    				bottom = 46;
	    				left = 42.4;
	    				z_index = 106;
	    				bottom_addition += 5;
	    			}
	    			$($mango[i]).css({
	    				"position": "absolute",
	    				"width": "80%",
	    				"left": left+"%",
	    				"bottom": bottom+"%",
	    				"z-index": z_index
	    			});

	    			// $($orange[i]).css({
	    				// "position": "absolute",
	    				// "width": "20%",
	    				// "left": left+"%",
	    				// "bottom": (bottom+bottom_addition)+"%",
	    				// "z-index": z_index
	    			// });
//
	    			// $($potato[i]).css({
	    				// "position": "absolute",
	    				// "width": "20%",
	    				// "left": left+"%",
	    				// "bottom": bottom+"%",
	    				// "z-index": z_index
	    			// });
	    			left += 15.2;
	    		}

	    		$(".fruit").draggable({
	    					containment : "board",
							cursor : "grab",
							revert : "invalid",
							appendTo : "body",
							helper : "clone",
							zindex : 1000,
							start: function(event, ui){
								ui.helper.css({"width":"12%"});
								$(this).css( "filter", "grayscale(100%)");
							},
							stop: function(event, ui){
								$(this).css( "filter", "none");
							}
					});

				$(".weightdrop").draggable({
	    					containment : "body",
							cursor : "grab",
							revert : "invalid",
							appendTo : "body",
							helper : "clone",
							zindex : 1000,
							start: function(event, ui){

								$(this).css( "filter", "grayscale(100%)");
							},
							stop: function(event, ui){
								$(this).css( "filter", "none");
							}
					});

				$(".drop_left, .drop_right").droppable({
					accept :  function(dropelem){
						var i = 0;

						if(dropelem.hasClass("weightdrop")){
							return true;
						}else if(dropelem.hasClass("fruit")){
							var $leftimages = $(".drop_left> img");

							if($(".drop_left> .fruit").length > 14 || $(".drop_left> .fruit").length > 14){
	   							return false;
	   						}else{
								return true;
							}
						}else{
							return false;
						}
					},
					hoverClass : "hovered",
					drop : function upondrop(event, ui){
						console.log("dropped triggered");
						if($(this).hasClass("drop_left")){
							handleCardDrop2(event, ui, true, false);
						}else{
							handleCardDrop2(event, ui, false, true);
						}

					}
				});

	   			var lefthasweight = false;
	   			var righthasweight = false;
	   			var fruitsdropped = 0;
	   			var weightdropped = 0;

	   			function handleCardDrop2(event, ui, isleft, isright){

	   				var $droppedclone = $($(ui.helper).clone());
	   				var droppedisweight = $droppedclone.hasClass("weightdrop");

	   				if(lefthasweight || righthasweight){
	   					if(droppedisweight){
	   						if(righthasweight && isleft){
	   							return true;
	   						}else if(lefthasweight && isright){
	   							return true;
	   						}
	   					}else {
	   						if(isleft && lefthasweight){
	   							return true;
	   						}else if(isright && righthasweight){
	   							return true;
	   						}
	   					}
	   				}else{
	   					if(droppedisweight) {
	   						lefthasweight = (isleft)? true: false;
	   						righthasweight = (isright)? true: false;
	   					} else {
	   						lefthasweight = (isleft)? false: true;
	   						righthasweight = (isright)? false: true;
	   					}
	   				}


	   				var $leftimages = $(".drop_left> img");
	   				var $rightimages = $(".drop_right> img");

	   				if(droppedisweight){

	   					var weightclassName = $droppedclone[0].classList[0];
	   					var z_index = 0;
	   					var bottom = 0;
	   					var left = 0;
	   					var width = 0;
							coin_drag.play();
	   					switch(weightclassName){
		   					case "dhak1":
		   						weightdropped += 50;
		   						z_index = 20;
		   						width = 20;
		   						break;

		   					case "dhak2":
		   						weightdropped += 100;
		   						z_index = 18;
		   						width = 20;
		   						break;

		   					case "dhak3":
		   						weightdropped += 200;
		   						z_index = 16;
		   						width = 20;
		   						break;

		   					case "dhak4":
		   						weightdropped += 500;
		   						z_index = 14;
		   						width = 17;
		   						break;

		   					case "dhak5":
		   						weightdropped += 1000;
		   						z_index = 12;
		   						width = 20;
		   						break;

		   					default:
		   						break;
		   				}

		   				if(lefthasweight){
		   					bottom = 8 + ($leftimages.length/5) * 5;
		   					left = 12.6 + 15.2 * ($leftimages.length % 5);
		   					$droppedclone.detach().css({
		   							"z-index":z_index,
		   							"top":"initial",
		   							"bottom": bottom+"%",
		   							"left":left+"%",
		   							"width": width+"%",
                    "height":"auto"
		   						}).appendTo($(".drop_left"));
		   				}else{
		   					bottom = 8 + ($rightimages.length/5) * 5;
		   					left = 12.6 + 15.2 * ($rightimages.length % 5);
		   					$droppedclone.detach().css({
		   							"z-index":z_index,
		   							"top":"initial",
		   							"bottom": bottom+"%",
		   							"left":left+"%",
		   							"width": width+"%"
		   						}).appendTo($(".drop_right"));
		   				}

		   					$droppedclone.click(function (){
			   					var $this = $(this);
									coin_drag.play();
		   						switch($this[0].classList[0]){
				   					case "dhak1":
				   						weightdropped -= 50;
				   						break;

				   					case "dhak2":
				   						weightdropped -= 100;
				   						break;

				   					case "dhak3":
				   						weightdropped -= 200;
				   						break;

				   					case "dhak4":
				   						weightdropped -= 500;
				   						break;

				   					case "dhak5":
				   						weightdropped -= 1000;
				   						break;

				   					default:
				   						break;
				   				}
			   						$this.remove();
			   					if(weightdropped == 0 && fruitsdropped == 0){
			   						lefthasweight = false;
			   						righthasweight = false;
			   					}

				   				if(lefthasweight){
				   					rearangeimages("drop_left", true);
			   					}else{
			   						rearangeimages("drop_right", true);
			   					}

			   					recalculatepositions();

			   				});

	   				}else{
	   					ui.draggable.draggable('disable');
	   					ui.draggable.detach();
	   					var fruitclassName = $droppedclone[0].classList[0];
		   				switch(fruitclassName){
		   					case "tomato":
		   						fruitsdropped += 750;
		   						break;

		   					case "peaches":
		   						fruitsdropped += 1500;
		   						break;

		   					case "bitter_gourds":
		   						fruitsdropped += 2200;
		   						break;

		   					case "oranges":
		   						fruitsdropped += 1800;
		   						break;

		   					case "apples":
		   						fruitsdropped += 1750;
		   						break;

		   					case "papaya":
		   						fruitsdropped += 1600;
		   						break;

		   					case "brinjal":
		   						fruitsdropped += 1350;
		   						break;

		   					case "cabbage":
		   						fruitsdropped += 1500;
		   						break;

		   					case "cauliflower":
		   						fruitsdropped += 1800;
		   						break;

		   					case "pumpkin":
		   						fruitsdropped += 1500;
		   						break;

		   					default:
		   						break;
		   				}

		   				var kg_dropped = Math.floor(fruitsdropped/1000);
		   				$(".kg_class").html(kg_dropped);
		   				$(".gm_class").html(fruitsdropped - (kg_dropped*1000));

		   				if(lefthasweight){
		   					$droppedclone.detach().appendTo($(".drop_right"));
		   					rearangeimages("drop_right", false);
		   					addclicklistener($droppedclone, "drop_right");

		   				}else{
		   					$droppedclone.detach().appendTo($(".drop_left"));
		   					rearangeimages("drop_left", false);
		   					addclicklistener($droppedclone, "drop_right");

		   				}

	   				}

	   				//direct addition for jq animate portion
	   				recalculatepositions();
	   			}

	   			var leftplatetop = 64;
	   			var rightplatetop = 64;
	   			var anglevalue = 360;
	   			var rightdroppabletop = 2;
	   			var leftdroppabletop = 2;

				/*
					check btn start
				*/

				var ansClicked = false;
		        var wrngClicked = false;

		        var $kg_input = $(".kg_input");
		        var $gm_input = $(".gm_input");
		        var entered_weight = 0;

		        $(".kg_input, .gm_input").keydown(function(evt) {
					var charVal = parseInt(evt.key);
					var prevValue = parseInt(evt.target.value) * 10 + charVal;
					var charCode = (evt.which) ? evt.which : evt.keyCode;
					if ((charCode > 31 && (charCode < 48 || charCode > 57)) && isNaN(charVal)) {
						// console.log("inside");
						// console.log(evt.target.value);
						return false;
					}

					if (prevValue > 999) {
						return false;
					}

					return true;
				});


		        $(".kgtogm_btn").click(function() {
		            if (ansClicked == false) {
		            	var kg = parseInt($kg_input.val());
		            	if(isNaN(kg)){
		            		kg = 0;
		            	}
		            	var gm = parseInt($gm_input.val());
		            	if(isNaN(gm)){
		            		gm = 0;
		            	}
		            	entered_weight = 1000 * kg + gm;
                  if (entered_weight!=0 && weightdropped!=0 && fruitsdropped!=0){

		                if ((entered_weight == fruitsdropped)&&(weightdropped == fruitsdropped)) {
		                    if(weightdropped > 0 && entered_weight > 0 && fruitsdropped > 0){
			                    if (countNext != $total_page) {
		                            $nextBtn.show(0);
		                            $kg_input.addClass("correct").attr('disabled', "disabled");
		                            $gm_input.addClass("correct").attr('disabled', "disabled");
				                    play_correct_incorrect_sound(true);
				                    ansClicked = true;
				                    $(".utb_info").show(0);
			                    }
		   					}
		                    if (wrngClicked == false) {
		                        testin.update(true);
		                    }
		                } else {
		                    testin.update(false);
		                    wrngClicked = true;
		                    $kg_input.addClass("incorrect");
                            $gm_input.addClass("incorrect");
		                    play_correct_incorrect_sound(false);
		                    $(".utb_info").show(0);
		                }
                  }
		            }
		        });
				/*
					check btn start
				*/

	   			function recalculatepositions(){
	   				var weight_difference = weightdropped - fruitsdropped;
	   				var $beambalance_needle = $(".beambalance_needle");
	   				var $beambalance_left = $(".beambalance_left");
	   				var $beambalance_right = $(".beambalance_right");
	   				var $drop_left = $(".drop_left");
	   				var $drop_right = $(".drop_right");
	   				console.log("weight : "+ weightdropped, " items : "+ fruitsdropped);

	   				var weightfactor = 10;
	   				var weightismore = false;
	   				if(weightdropped == fruitsdropped){
	   					if(weightdropped > 0 && fruitsdropped > 0){
								ding.play();
	   						// ole.footerNotificationHandler.pageEndSetNotification();
	   						// $nextBtn.show(0);
	   					}

	   					weightfactor = 10;
	   				}else if(weightdropped > fruitsdropped){
	   					if(fruitsdropped == 0){
	   						weightfactor = 20;
	   					}else{
	   						weightfactor = Math.round((weightdropped/fruitsdropped)*10);
	   					}

	   				}else{
	   					if(weightdropped == 0){
	   						weightfactor = 20;
	   					}else{
	   						weightfactor = Math.round((fruitsdropped/weightdropped)*10);
	   					}

	   				}

		   				$beambalance_needle.removeClass($beambalance_needle[0].classList[1]);
						$beambalance_left.removeClass($beambalance_left[0].classList[1]);
						$beambalance_right.removeClass($beambalance_right[0].classList[1]);
						$drop_left.removeClass($drop_left[0].classList[1]);
						$drop_right.removeClass($drop_right[0].classList[1]);

	   				var rotateflag = false;
					if(lefthasweight){
						if(weightdropped > fruitsdropped){
							rotateflag = true;
						}else{
							rotateflag = false;
						}
					}else{
						if(weightdropped > fruitsdropped){
							rotateflag = false;
						}else{
							rotateflag = true;
						}
					}


	   				switch(weightfactor){
	   					case 10:
	   							$beambalance_needle.addClass("rotate0");
								$beambalance_left.addClass("top_64_0");
								$beambalance_right.addClass("top_64_0");
								$drop_left.addClass("top_12_0");
								$drop_right.addClass("top_12_0");
								anglevalue = 360;
								leftplatetop = 64;
					   			rightplatetop = 64;
					   			rightdroppabletop = 2;
					   			leftdroppabletop = 2;
								break;
						case 11:
							if(rotateflag){
								$beambalance_needle.addClass("rotate1_ve");
								$beambalance_left.addClass("top_64_1");
								$beambalance_right.addClass("top_64_1_ve");
								$drop_left.addClass("top_12_1");
								$drop_right.addClass("top_12_1_ve");
								anglevalue = 355;
								leftplatetop = 65;
					   			rightplatetop = 63;
					   			leftdroppabletop = 3;
					   			rightdroppabletop = 1;
							}else{
								$beambalance_needle.addClass("rotate1");
								$beambalance_left.addClass("top_64_1_ve");
								$beambalance_right.addClass("top_64_1");
								$drop_left.addClass("top_12_1_ve");
								$drop_right.addClass("top_12_1");
								anglevalue = 365;
								leftplatetop = 63;
					   			rightplatetop = 65;
					   			leftdroppabletop = 1;
					   			rightdroppabletop = 3;
							}
								break;
						case 12:
							if(rotateflag){
								$beambalance_needle.addClass("rotate2_ve");
								$beambalance_left.addClass("top_64_2");
								$beambalance_right.addClass("top_64_2_ve");
								$drop_left.addClass("top_12_2");
								$drop_right.addClass("top_12_2_ve");
								anglevalue = 350;
								leftplatetop = 66;
					   			rightplatetop = 62;
					   			leftdroppabletop = 4;
					   			rightdroppabletop = 0;
							}else{
								$beambalance_needle.addClass("rotate2");
								$beambalance_left.addClass("top_64_2_ve");
								$beambalance_right.addClass("top_64_2");
								$drop_left.addClass("top_12_2_ve");
								$drop_right.addClass("top_12_2");
								anglevalue = 370;
								leftplatetop = 62;
					   			rightplatetop = 66;
					   			leftdroppabletop = 0;
					   			rightdroppabletop = 4;
							}

								break;
						case 13:
							if(rotateflag){
								$beambalance_needle.addClass("rotate3_ve");
								$beambalance_left.addClass("top_64_3");
								$beambalance_right.addClass("top_64_3_ve");
								$drop_left.addClass("top_12_3");
								$drop_right.addClass("top_12_3_ve");
								anglevalue = 345;
								leftplatetop = 67;
					   			rightplatetop = 61;
					   			leftdroppabletop = 5;
					   			rightdroppabletop = -1;
							}else{
								$beambalance_needle.addClass("rotate3");
								$beambalance_left.addClass("top_64_3_ve");
								$beambalance_right.addClass("top_64_3");
								$drop_left.addClass("top_12_3_ve");
								$drop_right.addClass("top_12_3");
								anglevalue = 375;
								leftplatetop = 61;
					   			rightplatetop = 67;
					   			leftdroppabletop = -1;
					   			rightdroppabletop = 5;
							}
								break;
						case 14:
							if(rotateflag){
								$beambalance_needle.addClass("rotate4_ve");
								$beambalance_left.addClass("top_64_4");
								$beambalance_right.addClass("top_64_4_ve");
								$drop_left.addClass("top_12_4");
								$drop_right.addClass("top_12_4_ve");
								anglevalue = 340;
								leftplatetop = 68;
					   			rightplatetop = 60;
					   			leftdroppabletop = 6;
					   			rightdroppabletop = -2;
							}else{
								$beambalance_needle.addClass("rotate4");
								$beambalance_left.addClass("top_64_4_ve");
								$beambalance_right.addClass("top_64_4");
								$drop_left.addClass("top_12_4_ve");
								$drop_right.addClass("top_12_4");
								anglevalue = 380;
								leftplatetop = 60;
					   			rightplatetop = 68;
					   			leftdroppabletop = -2;
					   			rightdroppabletop = 6;
							}

								break;
						case 15:
							if(rotateflag){

								$beambalance_needle.addClass("rotate5_ve");
								$beambalance_left.addClass("top_64_5");
								$beambalance_right.addClass("top_64_5_ve");
								$drop_left.addClass("top_12_5");
								$drop_right.addClass("top_12_5_ve");
								anglevalue = 335;
								leftplatetop = 69;
					   			rightplatetop = 59;
					   			leftdroppabletop = 7;
					   			rightdroppabletop = -3;
							}else{
								$beambalance_needle.addClass("rotate5");
								$beambalance_left.addClass("top_64_5_ve");
								$beambalance_right.addClass("top_64_5");
								$drop_left.addClass("top_12_5_ve");
								$drop_right.addClass("top_12_5");
								anglevalue = 385;
								leftplatetop = 59;
					   			rightplatetop = 69;
					   			leftdroppabletop = -3;
					   			rightdroppabletop = 7;
							}
								break;
						case 16:
							if(rotateflag){
								$beambalance_needle.addClass("rotate6_ve");
								$beambalance_left.addClass("top_64_6");
								$beambalance_right.addClass("top_64_6_ve");
								$drop_left.addClass("top_12_6");
								$drop_right.addClass("top_12_6_ve");
								anglevalue = 330;
								leftplatetop = 70;
					   			rightplatetop = 58;
					   			leftdroppabletop = 8;
					   			rightdroppabletop = -4;
							}else{
								$beambalance_needle.addClass("rotate6");
								$beambalance_left.addClass("top_64_6_ve");
								$beambalance_right.addClass("top_64_6");
								$drop_left.addClass("top_12_6_ve");
								$drop_right.addClass("top_12_6");
								anglevalue = 390;
								leftplatetop = 58;
					   			rightplatetop = 70;
					   			leftdroppabletop = -4;
					   			rightdroppabletop = 8;
							}

								break;
						case 17:
							if(rotateflag){
								$beambalance_needle.addClass("rotate7_ve");
								$beambalance_left.addClass("top_64_7");
								$beambalance_right.addClass("top_64_7_ve");
								$drop_left.addClass("top_12_7");
								$drop_right.addClass("top_12_7_ve");
								anglevalue = 325;
								leftplatetop = 71;
					   			rightplatetop = 57;
					   			leftdroppabletop = 9;
					   			rightdroppabletop = -5;
							}else{
								$beambalance_needle.addClass("rotate7");
								$beambalance_left.addClass("top_64_7_ve");
								$beambalance_right.addClass("top_64_7");
								$drop_left.addClass("top_12_7_ve");
								$drop_right.addClass("top_12_7");
								anglevalue = 395;
								leftplatetop = 57;
					   			rightplatetop = 71;
					   			leftdroppabletop = -5;
					   			rightdroppabletop = 9;
							}
								break;
						default:
								if(rotateflag){
									$beambalance_needle.addClass("rotate8_ve");
									$beambalance_left.addClass("top_64_8");
									$beambalance_right.addClass("top_64_8_ve");
									$drop_left.addClass("top_12_8");
									$drop_right.addClass("top_12_8_ve");
									anglevalue = 318;
									leftplatetop = 72;
						   			rightplatetop = 56;
						   			leftdroppabletop = 10;
						   			rightdroppabletop = -6;
								}else{
									$beambalance_needle.addClass("rotate8");
									$beambalance_left.addClass("top_64_8_ve");
									$beambalance_right.addClass("top_64_8");
									$drop_left.addClass("top_12_8_ve");
									$drop_right.addClass("top_12_8");
									anglevalue = 402;
									leftplatetop = 56;
						   			rightplatetop = 72;
						   			leftdroppabletop = -6;
						   			rightdroppabletop = 10;
								}
							break;
	   				}
	   				setTimeout(function(){
	   					$beambalance_needle.css("transform","rotate("+anglevalue+"deg)");
	   					$beambalance_left.css("top", leftplatetop+"%");
	   					$beambalance_right.css("top", rightplatetop+"%");
	   					$drop_left.css("top", leftdroppabletop+"%");
	   					$drop_right.css("top",rightdroppabletop+"%");
	   				},450);

	   			}

	   			function addclicklistener($droppedclone, classname){
	   					$droppedclone.click(function (){
		   					// var $this = $(this);
	   						// switch($this[0].classList[0]){
			   					// case "mango":
			   						// fruitsdropped -= 50;
			   						// break;
//
			   					// case "orange":
			   						// fruitsdropped -= 100;
			   						// break;
//
			   					// case "potato":
			   						// fruitsdropped -= 150;
			   						// break;
//
			   					// default:
			   						// break;
			   				// }
//
		   					// $this.remove();
		   					// if(weightdropped == 0 && fruitsdropped == 0){
			   						// lefthasweight = false;
			   						// righthasweight = false;
			   					// }
		   					// if(lefthasweight){
		   						// rearangeimages("drop_right");
		   					// }else{
		   						// rearangeimages("drop_left");
		   					// }
//
		   					// recalculatepositions();
		   				});
	   			}


	   			function rearangeimages(classname, isweight){
	   				var $content = $("."+classname+"> img");
	   				bottom = 8;
		    		left = 10;
		    		z_index = 110;
		    		bottom_addition = 0;
	   				for (var i = 0; i < $content.length; i++){

	   					if(i== 5){
		    				bottom = 18;
		    				left = 19.6;
		    				z_index = 109;
		    				bottom_addition = 8;
		    			}else if(i == 9){
		    				bottom = 26;
		    				left = 27.2;
		    				z_index = 108;
		    				bottom_addition += 8;
		    			}else if(i == 12){
		    				bottom = 34;
		    				left = 34.8;
		    				z_index = 107;
		    				bottom_addition += 8;
		    			}else if(i == 14){
		    				bottom = 42;
		    				left = 42.4;
		    				z_index = 106;
		    				bottom_addition += 8;
		    			}
		    			var width = 80;
		    			if(isweight){
		    				width = 20;
		    			}
	   					$($content[i]).css({
		    				"position": "absolute",
		    				// "width": width+"%",
		    				"left": left+"%",
		    				"bottom": (bottom+bottom_addition)+"%",
		    				"z-index": z_index,
		    				"top": "initial"
	    				});
	    				left += 15.2;
	   				}
	   			}
	    }
  }


	function soundplayer(i,next){
		buzz.all().stop();
		i.play().bind("ended",function(){
			if (!next) navigationcontroller();
		});
	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
			testin.gotoNext();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

//direct addition for jq animate portion
	   					// function animatefunction2($component, value){
	   						// $component.animate({  borderSpacing: value }, {
							    // step: function(now,fx) {
							      // $(this).css('top', now+'%');
							    // },
							    // duration: 400
							// },'linear');
	   					// }
//
	   					// function animatefunction($component, value, changerange){
	   						// $component.animate({  borderSpacing: changerange }, {
							    // step: function(now,fx) {
							      // $(this).css('top', (value+now)+'%');
							    // },
							    // duration: 400
							// },'linear');
	   					// }
//
	   				// if(weight_difference == 0){
   						// $beambalance_needle.animate({  borderSpacing: 0 }, {
						    // step: function(now,fx) {
						      // $(this).css('-webkit-transform','rotate('+now+'deg)');
						      // $(this).css('-moz-transform','rotate('+now+'deg)');
						      // $(this).css('transform','rotate('+now+'deg)');
						    // },
						    // duration: 400
						// },'linear');
						// animatefunction($beambalance_left, 64);
						// animatefunction($beambalance_right, 64);
						// animatefunction($drop_left, 64);
						// animatefunction($drop_right, 64);
//
//
//
	   				// }else if(weight_difference <= -2 || ((weightdropped == 0 || fruitsdropped == 0) && isleft)){
	   					// $beambalance_needle.animate({  borderSpacing: -42 }, {
						    // step: function(now,fx) {
						      // $(this).css('-webkit-transform','rotate('+now+'deg)');
						      // $(this).css('-moz-transform','rotate('+now+'deg)');
						      // $(this).css('transform','rotate('+now+'deg)');
						    // },
						    // duration:400
						// },'linear');
						// animatefunction($beambalance_left, 64, 8);
						// animatefunction($beambalance_right, 64, -8);
						// animatefunction($drop_left, 12, 8);
						// animatefunction($drop_right, 12, -8);
	   				// }else if(weight_difference >= 2 || ((weightdropped == 0 || fruitsdropped == 0) && isright)){
	   					// $beambalance_needle.animate({  borderSpacing: 42 }, {
						    // step: function(now,fx) {
						      // $(this).css('-webkit-transform','rotate('+now+'deg)');
						      // $(this).css('-moz-transform','rotate('+now+'deg)');
						      // $(this).css('transform','rotate('+now+'deg)');
						    // },
						    // duration:400
						// },'linear');
						// animatefunction($beambalance_left, 64, -8);
						// animatefunction($beambalance_right, 64, 8);
						// animatefunction($drop_left, 12, -8);
						// animatefunction($drop_right, 12, 8);
	   				// }else{
	   					// var angle = Math.floor(21 * weight_difference);
//
	   					// var factor = fruitsdropped/weightdropped;
	   					// console.log("factor", factor);
	   					// $beambalance_needle.animate({  borderSpacing: angle }, {
						    // step: function(now,fx) {
						      // $(this).css('-webkit-transform','rotate('+now+'deg)');
						      // $(this).css('-moz-transform','rotate('+now+'deg)');
						      // $(this).css('transform','rotate('+now+'deg)');
						    // },
						    // duration: 1000
						// },'linear');
//
						// var valuechange = Math.floor(8 * weight_difference);
						// animatefunction2($beambalance_left, 64 - valuechange);
						// animatefunction2($beambalance_right, 64 + valuechange);
						// animatefunction2($drop_left, 64 - valuechange);
						// animatefunction2($drop_right, 64 + valuechange);
	   				// }



// function clearclasslist($class, length){
	   					// for (var i = 1 ; i < length; i++){
	   						// $class[0].classList[i] = "";
	   					// }
	   				// }
	   				// // var length = $beambalance_needle[0].classList.length;
	   				// // (length > 1)? clearclasslist($beambalance_needle, length): 1;
// //
	   				// // length = $beambalance_left[0].classList.length;
	   				// // (length > 1)? clearclasslist($beambalance_left, length): 1;
// //
	   				// // length = $beambalance_right[0].classList.length;
	   				// // (length > 1)? clearclasslist($beambalance_right, length): 1;
// //
	   				// // length = $drop_left[0].classList.length;
	   				// // (length > 1)? clearclasslist($drop_left, length): 1;
// //
	   				// // length = $drop_right[0].classList.length;
	   				// // (length > 1)? clearclasslist($drop_right, length): 1;
//
	   				// // $beambalance_needle[0].className = $beambalance_needle[0].classList[0] +" "+ $beambalance_needle[0].classList[1];
	   				// // $beambalance_left[0].className = $beambalance_left[0].classList[0] + $beambalance_left[0].classList[1];
	   				// // $beambalance_right[0].className = $beambalance_right[0].classList[0] + $beambalance_right[0].classList[1];
	   				// // $drop_left[0].className = $drop_left[0].classList[0] + $drop_left[0].classList[1];
	   				// // $drop_right[0].className = $drop_right[0].classList[0] + $drop_right[0].classList[1];
//
	   				// if(weight_difference == 0){
						// $beambalance_needle.removeClass($beambalance_needle[0].classList[1]).addClass("rotate0");
						// $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_0");
						// $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_0");
						// $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_0");
						// $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_0");
	   				// }else if(weight_difference <= -2 || ((weightdropped == 0 || fruitsdropped == 0) && isleft)){
	   					// $beambalance_needle.removeClass($beambalance_needle[0].classList[1]).addClass("rotate8_ve");
						// $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_8");
						// $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_8_ve");
						// $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_8");
						// $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_8_ve");
	   				// }else if(weight_difference >= 2 || ((weightdropped == 0 || fruitsdropped == 0) && isright)){
						// $beambalance_needle.removeClass($beambalance_needle[0].classList[1]).addClass("rotate8");
						// $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_8_ve");
						// $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_8");
						// $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_8_ve");
						// $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_8");
	   				// }else{
	   					// if(fruitsdropped> weightdropped){
	   						// factor = Math.round((fruitsdropped / weightdropped )*10);
	   					// }else{
	   						// factor = Math.round(( weightdropped / fruitsdropped )*10);
	   					// }
//
	   					// switch (factor){
	   						// case 10:
	   						// case 11:
	   						// case 12:
	   						// case 13:
	   						// case 14:
	   						// case 15:
	   						// case 16:
	   						// case 17:
	   						// case 18:
	   						// case 19:
	   						// case 20:
//
	   							// $beambalance_needle.css({"-webkit-animation": "rotate_0 0.4s linear 1 forwards",
														// "animation": "rotate_0 0.4s linear 1 forwards" });
								// $beambalance_left.addClass("top_64_0").removeClass($beambalance_left[0].classList[1]);
								// $beambalance_right.addClass("top_64_0").removeClass($beambalance_right[0].classList[1]);
								// $drop_left.addClass("top_12_0").removeClass($drop_left[0].classList[1]);
								// $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_0");
	   							// // break;
	   						// // case 11:
	   						// // case 9:
	   							// // if(isleft){
	   								// // $beambalance_needle.removeClass($beambalance_needle[0].classList[1]).addClass("rotate1_ve");
									// // $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_1");
									// // $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_1_ve");
									// // $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_1");
									// // $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_1_ve");
	   							// // }else{
	   								// $beambalance_needle.css({"-webkit-animation": "rotate_1 0.4s linear 1 forwards",
															// "animation": "rotate_1 1.4s linear 1 forwards" });
									// $beambalance_left.addClass("top_64_1_ve").removeClass($beambalance_left[0].classList[1]);
									// $beambalance_right.addClass("top_64_1").removeClass($beambalance_right[0].classList[1]);
									// $drop_left.addClass("top_12_1_ve").removeClass($drop_left[0].classList[1]);
									// $drop_right.addClass("top_12_1").removeClass($drop_right[0].classList[1]);
	   							// // }
// //
	   							// // break;
	   						// // case 12:
	   						// // case 8:
	   							// // if(isleft){
	   								// // $beambalance_needle.removeClass($beambalance_needle[0].classList[1]).addClass("rotate2_ve");
									// // $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_2");
									// // $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_2_ve");
									// // $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_2");
									// // $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_2_ve");
	   							// // }else{
	   								// $beambalance_needle.css({"-webkit-animation": "rotate_2 0.4s linear 1 forwards",
															// "animation": "rotate_2 1.4s linear 1 forwards" });
									// $beambalance_left.addClass("top_64_2_ve").removeClass($beambalance_left[0].classList[1]);
									// $beambalance_right.addClass("top_64_2").removeClass($beambalance_right[0]);
									// $drop_left.addClass("top_12_2_ve").removeClass($drop_left[0].classList[1]);
									// $drop_right.addClass("top_12_2").removeClass($drop_right[0].classList[1]);
	   							// // }
	   							// // break;
	   						// // case 13:
	   						// // case 7:
	   							// // if(isleft){
	   								// // $beambalance_needle.removeClass($beambalance_needle[0].classList[1]).addClass("rotate3_ve");
									// // $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_3");
									// // $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_3_ve");
									// // $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_3");
									// // $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_3_ve");
	   							// // }else{
	   								// $beambalance_needle.css({"-webkit-animation": "rotate_3 0.4s linear 1 forwards",
															// "animation": "rotate_3 1.4s linear 1 forwards" });
									// $beambalance_left.addClass("top_64_3_ve").removeClass($beambalance_left[0].classList[1]);
									// $beambalance_right.addClass("top_64_3").removeClass($beambalance_right[0].classList[1]);
									// $drop_left.addClass("top_12_3_ve").removeClass($drop_left[0].classList[1]);
									// $drop_right.addClass("top_12_3").removeClass($drop_right[0].classList[1]);
	   							// // }
	   							// // break;
	   						// // case 14:
	   						// // case 6:
	   							// // if(isleft){
	   								// // $beambalance_needle.removeClass($beambalance_needle[0].classList[1]).addClass("rotate4_ve");
									// // $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_4");
									// // $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_4_ve");
									// // $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_4");
									// // $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_4_ve");
	   							// // }else{
	   								// $beambalance_needle.css({"-webkit-animation": "rotate_4 0.4s linear 1 forwards",
															// "animation": "rotate_4 1.4s linear 1 forwards" });
									// $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_4_ve");
									// $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_4");
									// $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_4_ve");
									// $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_4");
	   							// // }
	   							// // break;
	   						// // case 15:
	   						// // case 5:
	   							// // if(isleft){
	   								// // $beambalance_needle.removeClass($beambalance_needle[0].classList[1]).addClass("rotate5_ve");
									// // $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_5");
									// // $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_5_ve");
									// // $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_5");
									// // $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_5_ve");
	   							// // }else{
	   								// $beambalance_needle.css({"-webkit-animation": "rotate_5 0.4s linear 1 forwards",
															// "animation": "rotate_5 1.4s linear 1 forwards" });
									// $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_5_ve");
									// $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_5");
									// $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_5_ve");
									// $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_5");
	   							// // }
	   							// // break;
	   						// // case 16:
	   						// // case 4:
	   							// // if(isleft){
	   								// // $beambalance_needle.removeClass($beambalance_needle[0].classList[1]).addClass("rotate6_ve");
									// // $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_6");
									// // $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_6_ve");
									// // $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_6");
									// // $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_6_ve");
	   							// // }else{
	   								// $beambalance_needle.removeClass($beambalance_needle[0].classList[1]).addClass("rotate6");
									// $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_6_ve");
									// $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_6");
									// $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_6_ve");
									// $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_6");
	   							// // }
	   							// // break;
	   						// // case 17:
	   						// // case 3:
	   							// // if(isleft){
	   								// // $beambalance_needle.removeClass($beambalance_needle[0].classList[1]).addClass("rotate7_ve");
									// // $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_7");
									// // $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_7_ve");
									// // $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_7");
									// // $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_7_ve");
	   							// // }else{
	   								// $beambalance_needle.removeClass($beambalance_needle[0].classList[1]).addClass("rotate7");
									// $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_7_ve");
									// $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_7");
									// $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_7_ve");
									// $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_7");
	   							// // }
	   							// break;
	   						// default:
	   							// if(isleft){
	   								// $beambalance_needle.removeClass($beambalance_needle[0].classList[1]).addClass("rotate8_ve");
									// $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_8");
									// $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_8_ve");
									// $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_8");
									// $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_8_ve");
	   							// }else{
	   								// $beambalance_needle.removeClass($beambalance_needle[0].classList[1]).addClass("rotate8");
									// $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_8_ve");
									// $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_8");
									// $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_8_ve");
									// $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_8");
	   							// }
	   							// break;
	   					// }
	   					// console.log("factor", factor);
	   				// }

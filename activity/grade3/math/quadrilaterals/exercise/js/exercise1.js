var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";

var content=[
  //ex0
	{
		exerciseblock: [
			{
				textdata: data.string.exques1,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q1ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q1ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q1ansc,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q1ansd,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q1anse,
					}]
			}
		]
	},
  //ex1
  {
    exerciseblock: [
      {
        textdata: data.string.exques2,

        exeoptions: [
          {
            forshuffle: "class1",
            optdata: data.string.q2ansa,
          },
          {
            forshuffle: "class2",
            optdata: data.string.q2ansb,
          },
          {
            forshuffle: "class3",
            optdata: data.string.q2ansc,
          },
          {
            forshuffle: "class4",
            optdata: data.string.q2ansd,
          },
          {
            forshuffle: "class4",
            optdata: data.string.q2anse,
          }]
      }
    ]
  },
  //ex2
	{
		exerciseblock: [
			{
				textdata: data.string.exques3,
        odadditionalclass: "twoonly",

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q3ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q3ansb,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q3ansc,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q3ansd,
					}]
			}
		]
	},
  //ex2
	{
		outerques: [
			{
				textdata: data.string.exques4,
			}
		],
		imageblock:[{
			imagestoshow:[
        {
					imgclass: "quaditem correct imghover cusimg-1",
					imgid : 'quad2',
					imgsrc: ""
				},
        {
					imgclass: "quaditem correct imghover cusimg-2",
					imgid : 'quad1',
					imgsrc: ""
				},
        {
					imgclass: "quaditem correct imghover cusimg-3",
					imgid : 'quad4',
					imgsrc: ""
				},
        {
					imgclass: "quaditem imghover cusimg-4",
					imgid : 'quad5',
					imgsrc: ""
				},
        {
					imgclass: "quaditem imghover cusimg-5",
					imgid : 'quad3',
					imgsrc: ""
				}
			]
		}]
	},
  //ex3
	{
		imageblock:[{
			imagestoshow:[
        {
					imgclass: "sideimg",
					imgid : 'tfquad',
					imgsrc: ""
				}
			]
		}],
    exerciseblock: [
			{
				textdata: data.string.exques5,
        odadditionalclass: "tfonly",

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q5ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q5ansb,
					}]
			}
		]
	},
  //ex4
	{
		outerques: [
			{
				textdata: data.string.exques6,
			}
		],
    singletext: [
      {
    		textclass: "inpbig-mid",
    		textdata: data.string.q6ansa
    	},
			{
				textclass: "sbmtbtn smbthov",
				textdata: data.string.submit
			},
			{
				textclass: "trybtn tryhov",
				textdata: data.string.try
			}
    ],
		imageblock:[{
			imagestoshow:[
        {
					imgclass: "sideimg",
					imgid : 'tfquad',
					imgsrc: ""
				}
			]
		}]
	},
  //ex5
	{
		outerques: [
			{
				datahighlightflag: true,
				datahighlightcustomclass: "vartext",
				textdata: data.string.exques7,
			}
		],
		svgblock: [
			{
				svgname: "quadcont2",
			}
		],
		imageblock:[{
			imagestoshow:[
        {
					imgclass: "correctmark",
					imgid : 'correct',
					imgsrc: ""
				},
				{
					imgclass: "incorrectmark",
					imgid : 'incorrect',
					imgsrc: ""
				}
			]
		}]
	},
  //ex6
	{
		outerques: [
			{
				datahighlightflag: true,
				datahighlightcustomclass: "vartext",
				textdata: data.string.exques8,
			}
		],
    singletext: [
      {
    		textclass: "inpbig-mid",
    		textdata: data.string.q6ansa
    	},
			{
				textclass: "sbmtbtn smbthov",
				textdata: data.string.submit
			},
			{
				textclass: "trybtn tryhov",
				textdata: data.string.try
			}
    ],
		imageblock:[{
			imagestoshow:[
        {
					imgclass: "sideimg",
					imgid : 'tfquad',
					imgsrc: ""
				}
			]
		}]
	},
  //ex7
	{
		outerques: [
			{
				datahighlightflag: true,
				datahighlightcustomclass: "vartext",
				textdata: data.string.exques9,
			}
		],
    singletext: [
			{
    		textclass: "inpbig-mid",
    		textdata: data.string.q9ansa
    	},
			{
				textclass: "inpbox-1",
				textdata: data.string.p5inp1
			},
			{
				textclass: "inpbox-2",
				textdata: data.string.p5inp2
			},
			{
				textclass: "inpbox-3",
				textdata: data.string.p5inp3
			},
			{
				textclass: "inpbox-4",
				textdata: data.string.p5inp4
			},
			{
				textclass: "sbmtbtn smbthov",
				textdata: data.string.submit
			},
			{
				textclass: "trybtn tryhov",
				textdata: data.string.try
			}
    ],
		imageblock:[{
			imagestoshow:[
        {
					imgclass: "sideimg",
					imgid : 'inpquad6',
					imgsrc: ""
				}
			]
		}]
	},
  //ex8
	{
		outerques: [
			{
				textdata: data.string.exques10,
			}
		],
    singletext: [
			{
				textclass: "inpbox-6",
				textdata: data.string.p5inp6
			},
			{
				textclass: "inpbox-7",
				textdata: data.string.p5inp7
			},
			{
				textclass: "inpbox-8",
				textdata: data.string.p5inp8
			},
			{
				textclass: "inpbox-9",
				textdata: data.string.p5inp9
			},
			{
				textclass: "sbmtbtn smbthov",
				textdata: data.string.submit
			},
			{
				textclass: "trybtn tryhov",
				textdata: data.string.try
			}
    ],
		imageblock:[{
			imagestoshow:[
        {
					imgclass: "sideimg",
					imgid : 'tfquad',
					imgsrc: ""
				},
				{
					imgclass: "angimg-1",
					imgid : 'ang1',
					imgsrc: ""
				},
				{
					imgclass: "angimg-2",
					imgid : 'ang2',
					imgsrc: ""
				},
				{
					imgclass: "angimg-3",
					imgid : 'ang3',
					imgsrc: ""
				},
				{
					imgclass: "angimg-4",
					imgid : 'ang4',
					imgsrc: ""
				}
			]
		}]
	}
];

/*remove this for non random questions*/
//content.shufflearray();


$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	/*for limiting the questions to 10*/
  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);

  function navigationcontroller(islastpageflag){
   typeof islastpageflag === "undefined" ?
   islastpageflag = false :
   typeof islastpageflag != 'boolean'?
   alert("NavigationController : Hi Master, please provide a boolean parameter") :
   null;

   if(countNext == 0 && $total_page!=1){
     $nextBtn.show(0);
     $prevBtn.css('display', 'none');
   }
   else if($total_page == 1){
     $prevBtn.css('display', 'none');
     $nextBtn.css('display', 'none');
   }
   else if(countNext > 0 && countNext <= $total_page-1){
     $nextBtn.show(0);
     $prevBtn.show(0);
   }
   }

  var preload;
	var score = 0;
	var myExe = new NumberTemplate();

  function myinit() {
      //specify type otherwise it will load assests as XHR
      manifest = [
        //images
        {id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
  			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
        {id: "quad1", src: imgpath+"robot/body/b01.png", type: createjs.AbstractLoader.IMAGE},
        {id: "quad2", src: imgpath+"robot/body/b02.png", type: createjs.AbstractLoader.IMAGE},
        {id: "quad3", src: imgpath+"robot/head/head05.png", type: createjs.AbstractLoader.IMAGE},
        {id: "quad4", src: imgpath+"robot/head/head06.png", type: createjs.AbstractLoader.IMAGE},

        {id: "quad5", src: imgpath+"robot/body/b05.png", type: createjs.AbstractLoader.IMAGE},
        {id: "quad6", src: imgpath+"robot/body/b06.png", type: createjs.AbstractLoader.IMAGE},
        {id: "quad7", src: imgpath+"robot/head/head01.png", type: createjs.AbstractLoader.IMAGE},
        {id: "quad8", src: imgpath+"robot/head/head03.png", type: createjs.AbstractLoader.IMAGE},

				{id: "ang1", src: imgpath+"forques/angle01.png", type: createjs.AbstractLoader.IMAGE},
				{id: "ang2", src: imgpath+"forques/angle02.png", type: createjs.AbstractLoader.IMAGE},
				{id: "ang3", src: imgpath+"forques/angle03.png", type: createjs.AbstractLoader.IMAGE},
				{id: "ang4", src: imgpath+"forques/angle04.png", type: createjs.AbstractLoader.IMAGE},

        {id: "tfquad", src: imgpath+"forexe/q02.png", type: createjs.AbstractLoader.IMAGE},

        {id: "inpquad1", src: imgpath+"forexe/q01.png", type: createjs.AbstractLoader.IMAGE},
        {id: "inpquad2", src: imgpath+"forexe/q02.png", type: createjs.AbstractLoader.IMAGE},
        {id: "inpquad3", src: imgpath+"forexe/q03.png", type: createjs.AbstractLoader.IMAGE},
        {id: "inpquad4", src: imgpath+"forexe/q04.png", type: createjs.AbstractLoader.IMAGE},
        {id: "inpquad5", src: imgpath+"forexe/q05.png", type: createjs.AbstractLoader.IMAGE},
        {id: "inpquad6", src: imgpath+"forexe/q10.png", type: createjs.AbstractLoader.IMAGE},

        // sounds
        // {id: "sound_1", src: soundAsset + "p1_s1.ogg"},

      ];
      preload = new createjs.LoadQueue(false);
      preload.installPlugin(createjs.Sound);//for registering sounds
      preload.installPlugin(createjs.Sound);//for registering sounds
      preload.on("progress", handleProgress);
      preload.on("complete", handleComplete);
      preload.on("fileload", handleFileLoad);
      preload.loadManifest(manifest, true);
  }

  function handleFileLoad(event) {
      // console.log(event.item);
  }

  function handleProgress(event) {
      $('#loading-text').html(parseInt(event.loaded * 100) + '%');
  }

  function handleComplete(event) {
      $('#loading-wrapper').hide(0);
      //initialize varibales
    //   current_sound = createjs.Sound.play('sound_1');
    //   current_sound.stop();
      // call main function
      myExe.init(10);
      // score.myinit(content.length);

      templateCaller();
  }

  //initialize
  myinit();
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());

	/*values in this array is same as the name of images of eggs in image folder*/
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
    texthighlight($board);
    put_image(content, countNext);

		$nextBtn.hide();
		$prevBtn.hide();

		/*generate question no at the beginning of question*/
		myExe.numberOfQuestions();

		/*for randomizing the options*/
		var parent = $(".optionsdiv");
		var divs = parent.children();
			 while (divs.length) {
			        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			  }
				$('input').attr('autocomplete','off');
    switch (countNext) {
      case 0:
      case 1:
      case 2:
      case 4:
      var ansClicked = false;
      var wrngClicked = false;

      $(".buttonsel").click(function(){
        $(this).removeClass('forhover');
          if(ansClicked == false){
            if($(this).hasClass("class1")){

              if(wrngClicked == false){
                myExe.update(true);
              }
              play_correct_incorrect_sound(1);
              $(this).css("background","#bed62fff");
              $(this).css("border","5px solid #deef3c");
              $(this).css("color","white");
              $(this).siblings(".corctopt").show(0);
              //$('.hint_image').show(0);
              $('.buttonsel').removeClass('forhover forhoverimg');
              ansClicked = true;

              if(countNext != $total_page)
              navigationcontroller();
            }
            else{
              myExe.update(false);
              play_correct_incorrect_sound(0);
              $(this).css("background","#FF0000");
              $(this).css("border","5px solid #980000");
              $(this).css("color","white");
              $(this).siblings(".wrngopt").show(0);
              wrngClicked = true;
            }
          }
        });
        break;
        case 3:
				var corrFlag = true;
        var corrArray = ["quad1","quad2","quad3","quad4"];
        var incoArray = ["quad5","quad6","quad7","quad8"];
        var finalArray = [];

        corrArray.shufflearray();
  			incoArray.shufflearray();

        for(var i = 0; i < 3; i++){
  				finalArray.push(corrArray[i]);
  			}
  			for(var i = 0; i < 2; i++){
  				finalArray.push(incoArray[i]);
  			}

        for(var i = 0; i < 5; i++){
          var icon = finalArray[i];
          $(".cusimg-"+(i+1)).attr("src", preload.getResult(icon).src);
        }

        var quadiycount = 0;
        $(".quaditem").click(function(){
    			if($(this).hasClass("imghover")){
    				$(this).removeClass('imghover');
    					if($(this).hasClass("correct")){
    						play_correct_incorrect_sound(1);
    						appender($(this),'correct');
    						quadiycount++;
    						if(quadiycount == 3){
									navigationcontroller();
									if(corrFlag == true)
										myExe.update(true);
								}
    					}
    					else{
								corrFlag = false;
								myExe.update(false);
    						play_correct_incorrect_sound(0);
    						appender($(this),'incorrect');
    					}
    			}

    			function appender($this, icon){
    				var posIt = $this.position();
    				var centerX = posIt.left + ($this.width()/1.5);
    				var centerY = posIt.top + ($this.height()/14);
    				$(".contentblock").append("<img style='left:"+centerX+";top:"+centerY+";position:absolute;width:5%;' src= '"+ preload.getResult(icon).src +"'>");
    				}
    		});
        break;
        case 5:
				var corrFlag = true;
        var quesNo = rand_generator(5);
        var quadArray = ["inpquad1","inpquad2","inpquad3","inpquad4","inpquad5"];
  			var thisAns;
  			var revAns;
				var typedAns;

				var tstans = [["A","B","C","D"],["F","G","H","E"],["L","M","J","K"],["T","U","V","W"],["K","L","M","N"]];

        var corrAnswers = [];
        for(var j = 0; j < 5; j++){
          for(var i = 0; i<4; i++){
    				thisAns = tstans[j][0].concat(tstans[j][1]).concat(tstans[j][2]).concat(tstans[j][3]);
    				revAns = thisAns.split("").reverse().join("");
    				corrAnswers.push(thisAns);
    				corrAnswers.push(revAns);
    				mover = tstans[j].pop();
    				tstans[j].unshift(mover);
    			}
        }

        $(".sideimg").attr("src", preload.getResult(quadArray[quesNo-1]).src);

				$(".sbmtbtn").click(function(){
					typedAns = $("#input1").val().toUpperCase();
					for (var i = 0; i < corrAnswers.length; i++) {
						if(typedAns == corrAnswers[i]){
							$("#input1").addClass("corrbox");
							navigationcontroller();
							play_correct_incorrect_sound(1);
							if(corrFlag == true)
							myExe.update(true);
						}
					}
					if(!$("#input1").hasClass("corrbox")){
						$("#input1").addClass("incorrbox");
						play_correct_incorrect_sound(0);
						corrFlag = false;
						myExe.update(false);
					}
					$(".trybtn").click(function(){
						$(".incorrbox").prop("disabled",false).removeClass("incorrbox").val("");
						$(".incorrimg").hide(0);
					});
				});
        break;
				case 6:
				var corrFlag = true;
				var q = Snap(".quadcont2");
				var quesNo = rand_generator(4);
				var quadArray = ["CDA","DAB","ABC","BCD"];

				$(".vartext").text(quadArray[quesNo-1]);

				Snap.load(imgpath+"forexe/rectangle01.svg", function (f) {
					q.append(f);
					$("#path0"+quesNo).attr("class", "svgbtn correct");
					$(".correct").click(function(){
						$(".correctmark").show(0);
						$(".incorrectmark").hide(0);
						navigationcontroller();
						play_correct_incorrect_sound(1);
						if(corrFlag == true)
						myExe.update(true);
					});
					$('.svgbtn:not(.correct)').click(function(){
						$(".incorrectmark").show(0);
						play_correct_incorrect_sound(0);
						corrFlag = false;
						myExe.update(false);
					});

				});
				break;
				case 7:
				var corrFlag = true;
				var quesNo = rand_generator(4);
				var quadArray = ["yellow","blue","green","pink"];
				if($lang=="np"){
					var quadArray = ["पहेंलो","नीलो","हरियो","गुलाबी"];
				}
				var quadAns = [["EFG","GFE"],["FGH","HGF"],["EHG","GHE"],["FEH","HEF"]];
				var typedAns;

				$(".vartext").text(quadArray[quesNo-1]);

				$(".sbmtbtn").click(function(){
					typedAns = $("#input1").val().toUpperCase();
					for(var i=0; i<quadAns[quesNo-1].length; i++){
						console.log(quadAns[quesNo-1][i]);
						if(typedAns == quadAns[quesNo-1][i]){
							$("#input1").addClass("corrbox");
							navigationcontroller();
							play_correct_incorrect_sound(1);
							if(corrFlag == true)
							myExe.update(true);
						}
					}
					if(!$("#input1").hasClass("corrbox")){
						$("#input1").addClass("incorrbox");
						play_correct_incorrect_sound(0);
						corrFlag = false;
						myExe.update(false);
					}
					$(".trybtn").click(function(){
						$(".incorrbox").prop("disabled",false).removeClass("incorrbox").val("");
						$(".incorrimg").hide(0);
					});
				});
				break;
				case 8:
				var corrFlag = true;
				var boxtext1;
				var boxtext2;
				var boxtext3;
				var boxtext4;
				var nxtFlag = true;
				$(".sbmtbtn").click(function(){
					nxtFlag = true;
					boxtext1 = $("#input1").val().toUpperCase();
					boxtext2 = $("#input2").val().toUpperCase();
					boxtext3 = $("#input3").val().toUpperCase();
					boxtext4 = $("#input4").val().toUpperCase();

					for(var i=1; i<5; i++){
						$("#input"+i).prop('disabled', true);
						for(var j=i+1; j<5; j++){
							if($("#input"+i).val().toUpperCase() == $("#input"+j).val().toUpperCase()){
								$("#input"+i).addClass("incorrbox");
								$("#input"+j).addClass("incorrbox");
								$("#input5").addClass("incorrbox");
							}
							else{
								$("#input"+i).addClass("corrbox");
								$("#input"+j).addClass("corrbox");
							}
						}
					}

					$("input[id^='input']").each(function(){
						if($(this).hasClass("incorrbox")){
							nxtFlag = false;
						}
					});

					var arrHelper = [boxtext1, boxtext2, boxtext3, boxtext4];
					var mover;
					var posAns = [];
					var thisAns;
					var revAns;
					var typedAns;

					for(var i = 0; i<4; i++){
						thisAns = arrHelper[0].concat(arrHelper[1]).concat(arrHelper[2]).concat(arrHelper[3]);
						revAns = thisAns.split("").reverse().join("");
						posAns.push(thisAns);
						posAns.push(revAns);
						console.log(posAns);
						mover = arrHelper.pop();
						arrHelper.unshift(mover);
					}

					typedAns = $("#input5").val().toUpperCase();
					for (var i = 0; i < posAns.length; i++) {
						if(typedAns == posAns[i]){
							$("#input5").addClass("corrbox");
							navigationcontroller();
							console.log("here i am");
							play_correct_incorrect_sound(1);
							if(corrFlag == true)
							myExe.update(true);
						}
					}
					if(!$("#input5").hasClass("corrbox")){
						$("#input5").addClass("incorrbox");
						play_correct_incorrect_sound(0);
						corrFlag = false;
						myExe.update(false);
					}

					$(".trybtn").click(function(){
						$(".incorrbox").prop("disabled",false).removeClass("incorrbox").removeClass("corrbox").val("");
						$(".incorrimg").hide(0);
					});

					if(nxtFlag == true){
						console.log(nxtFlag);
						navigationcontroller();
					}
				});

				break;
        case 9:
				var corrFlag = true;
        var quesNo = rand_generator(4);
        var quadArray = ["inpquad1","inpquad2","inpquad3","inpquad4","inpquad5"];

        $(".sideimg").attr("src", preload.getResult(quadArray[quesNo-1]).src);
				var boxtext1;
				var boxtext2;
				var boxtext3;
				var boxtext4;

				switch (quesNo) {
					case 1:
						boxtext1 = "B";
						boxtext2 = "C";
						boxtext3 = "D";
						boxtext4 = "A";
				break;
					case 2:
						boxtext1 = "F";
						boxtext2 = "G";
						boxtext3 = "H";
						boxtext4 = "E";
					break;
					case 3:
						boxtext1 = "L";
						boxtext2 = "M";
						boxtext3 = "J";
						boxtext4 = "K";
					break;
					case 4:
						boxtext1 = "U";
						boxtext2 = "V";
						boxtext3 = "W";
						boxtext4 = "T";
					break;
				}


					var yelArr = [];
					var bluArr = [];
					var pinArr = [];
					var greArr = [];

					var thisAns;
					var revAns;

					thisAns = boxtext4.concat(boxtext1).concat(boxtext2);
					revAns = thisAns.split("").reverse().join("");
					yelArr.push(thisAns);
					yelArr.push(revAns);

					thisAns = boxtext1.concat(boxtext2).concat(boxtext3);
					revAns = thisAns.split("").reverse().join("");
					bluArr.push(thisAns);
					bluArr.push(revAns);

					thisAns = boxtext1.concat(boxtext4).concat(boxtext3);
					revAns = thisAns.split("").reverse().join("");
					pinArr.push(thisAns);
					pinArr.push(revAns);

					thisAns = boxtext4.concat(boxtext3).concat(boxtext2);
					revAns = thisAns.split("").reverse().join("");
					greArr.push(thisAns);
					greArr.push(revAns);

					$("#input2").val(boxtext2).prop('disabled', true).addClass("boxdisabled");
					$("#input3").val(boxtext3).prop('disabled', true).addClass("boxdisabled");
					$("#input4").val(boxtext4).prop('disabled', true).addClass("boxdisabled");
					$("#input1").val(boxtext1).prop('disabled', true).addClass("boxdisabled");

					$(".sbmtbtn").click(function(){
						nxtFlag = true;
						boxtext6 = $("#input6").val().toUpperCase();
						boxtext7 = $("#input7").val().toUpperCase();
						boxtext8 = $("#input8").val().toUpperCase();
						boxtext9 = $("#input9").val().toUpperCase();

						corrChecker(boxtext6, yelArr, $("#input6"));
						corrChecker(boxtext7, bluArr, $("#input7"));
						corrChecker(boxtext8, pinArr, $("#input8"));
						corrChecker(boxtext9, greArr, $("#input9"));

						function corrChecker(myans, myarr, myfield){
							for (var i = 0; i < myarr.length; i++) {
								if(myans == myarr[i]){
									myfield.addClass("corrbox").prop('disabled', true);
								}
							}

							if(!myfield.hasClass("corrbox")){
								myfield.addClass("incorrbox").prop('disabled', true);
							}
						}

						$("input[id^='input']").each(function(){
							if($(this).hasClass("incorrbox")){
								nxtFlag = false;
								play_correct_incorrect_sound(0);
								corrFlag = false;
								myExe.update(false);
							}
						});

						if(nxtFlag == true){
							console.log(nxtFlag);
							navigationcontroller();
							play_correct_incorrect_sound(1);
							if(corrFlag == true)
							myExe.update(true);
						}

						$(".trybtn").click(function(){
							$(".incorrbox").prop("disabled",false).removeClass("incorrbox corrbox").val("");
						});
					});

        break;
    }
	}

  function rand_generator(limit){
    var randNum = Math.floor((Math.random() * limit) + 1);
    return randNum;
  }

  function put_image(content, count){
    if(content[count].hasOwnProperty('imageblock')){
      var imageblock = content[count].imageblock[0];
      if(imageblock.hasOwnProperty('imagestoshow')){
        var imageClass = imageblock.imagestoshow;
        for(var i=0; i<imageClass.length; i++){
          var image_src = preload.getResult(imageClass[i].imgid).src;
          //get list of classes
          var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
          var selector = ('.'+classes_list[classes_list.length-1]);
          $(selector).attr('src', image_src);
        }
      }
    }
  }

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		// for myExeg purpose
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		// myExeg purpose code ends


	}

	// first call to template caller
	//templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		myExe.gotoNext();
		templateCaller();

	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

/*===============================================
  =            data highlight function            =
  ===============================================*/
  function texthighlight($highlightinside){
     //check if $highlightinside is provided
     typeof $highlightinside !== "object" ?
     alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
     null ;

     var $alltextpara = $highlightinside.find("*[data-highlight='true']");
     var stylerulename;
     var replaceinstring;
     var texthighlightstarttag;
     var texthighlightendtag   = "</span>";


     if($alltextpara.length > 0){
       $.each($alltextpara, function(index, val) {
         /*if there is a data-highlightcustomclass attribute defined for the text element
         use that or else use default 'parsedstring'*/
         $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
         (stylerulename = $(this).attr("data-highlightcustomclass")) :
         (stylerulename = "parsedstring") ;

         texthighlightstarttag = "<span class='"+stylerulename+"'>";
         replaceinstring       = $(this).html();
         replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
         replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


         $(this).html(replaceinstring);
       });
     }
   }
   /*=====  End of data highlight function  ======*/

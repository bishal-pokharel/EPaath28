var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";
var soundAsset1 = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentblockadditionalclass: 'bg-yellow',
		extratextblock:[{
			textdata: data.lesson.chapter,
			textclass: "chap-title",
		}]
	},
	// slide1
	{
		contentblockadditionalclass: 'bg-purple',
		extratextblock:[{
            datahighlightflag : true,
            datahighlightcustomclass : 'colorchng',
			textclass: "custblock-1"
		},{
            datahighlightflag : true,
            datahighlightcustomclass : 'colorchng',
			textclass: "custblock-2",
		},
			{
                datahighlightflag : true,
                datahighlightcustomclass : 'colorchng',
                textdata: data.string.triangle,
                textclass: "trianglefooter content2",
			},
			{
                datahighlightflag : true,
                datahighlightcustomclass : 'colorchng',
                textdata: data.string.quad,
                textclass: "quadfooter content2",
			}
		],
		svgblock: [
			{
				svgname: "trianglecont",
			},
			{
				svgname: "quadcont",
			}
		]
	},
	//slide2
	{
		headerblock:[{
			textdata: data.string.p1text6
		}],
		uppertextblockadditionalclass: "p1lefttext",
		uppertextblock:[{
			textdata: data.string.p1text7
		}],
		extratextblock:[{
			textclass: "greybg-1"
		},{
			textclass: "greybg-2"
		},{
			textclass: "greytext",
			textdata: data.string.p1text8
		},
		{
			textclass: "buttonsel forhover correct diybutton-1",
			textdata: data.string.yes
		},
		{
			textclass: "buttonsel forhover diybutton-2",
			textdata: data.string.no
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "objnew",
					imgid : 'quad1',
					imgsrc: ""
				}
			]
		}]
	},
	//slide3
	{
		headerblock:[{
			textdata: data.string.p1text6
		}],
		uppertextblockadditionalclass: "p1lefttext",
		uppertextblock:[{
			textdata: data.string.p1text7
		}],
		extratextblock:[{
			textclass: "greybg-1"
		},{
			textclass: "greybg-2"
		},{
			textclass: "greytext",
			textdata: data.string.p1text8
		},
		{
			textclass: "buttonsel forhover correct diybutton-1",
			textdata: data.string.yes
		},
		{
			textclass: "buttonsel forhover diybutton-2",
			textdata: data.string.no
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "objnew",
					imgid : 'kite02',
					imgsrc: ""
				},
				{
					imgclass: "objold",
					imgid : 'quad1',
					imgsrc: ""
				}
			]
		}]
	},
	//slide4
	{
		headerblock:[{
			textdata: data.string.p1text6
		}],
		uppertextblockadditionalclass: "p1lefttext",
		uppertextblock:[{
			textdata: data.string.p1text7
		}],
		extratextblock:[{
			textclass: "greybg-1"
		},{
			textclass: "greybg-2"
		},{
			textclass: "greytext",
			textdata: data.string.p1text8
		},
		{
			textclass: "buttonsel forhover correct diybutton-1",
			textdata: data.string.yes
		},
		{
			textclass: "buttonsel forhover diybutton-2",
			textdata: data.string.no
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "objnew",
					imgid : 'cupboard',
					imgsrc: ""
				},
				{
					imgclass: "objold",
					imgid : 'kite02',
					imgsrc: ""
				}
			]
		}]
	},
	// slide 5
	{
		headerblock:[{
			textdata: data.string.p1text9
		}],
		extratextblock:[{
			textclass: "lightblue",
			textdata: data.string.p1text9_1
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "cover",
					imgid : 'bg1',
					imgsrc: ""
				},
				{
					imgclass: "quaditem quadhover correct cusimg-1",
					imgid : 'cupboard',
					imgsrc: ""
				},
				{
					imgclass: "quaditem quadhover cusimg-2",
					imgid : 'flowerpot',
					imgsrc: ""
				},
				{
					imgclass: "quaditem quadhover cusimg-3",
					imgid : 'kite01',
					imgsrc: ""
				},
				{
					imgclass: "quaditem quadhover correct cusimg-4",
					imgid : 'kite02',
					imgsrc: ""
				},
				{
					imgclass: "quaditem quadhover cusimg-5",
					imgid : 'mirror',
					imgsrc: ""
				},
				{
					imgclass: "quaditem quadhover correct cusimg-6",
					imgid : 'picture',
					imgsrc: ""
				},
				{
					imgclass: "quaditem quadhover cusimg-7",
					imgid : 'salt',
					imgsrc: ""
				},
				{
					imgclass: "quaditem quadhover correct cusimg-8",
					imgid : 'window',
					imgsrc: ""
				}
			]
		}]
	},
	// slide 6
	{
		headerblock:[{
			textdata: data.string.p1text9
		}],
		extratextblock:[{
			textclass: "lightblue",
			textdata: data.string.p1_s7_Txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "cover",
					imgid : 'bg1',
					imgsrc: ""
				},
				{
					imgclass: "quaditem cusimg-1",
					imgid : 'cupboard',
					imgsrc: ""
				},
				{
					imgclass: "quaditem cusimg-4",
					imgid : 'kite02',
					imgsrc: ""
				},
				{
					imgclass: "quaditem cusimg-6",
					imgid : 'picture',
					imgsrc: ""
				},
				{
					imgclass: "quaditem cusimg-8",
					imgid : 'window',
					imgsrc: ""
				}
			]
		}]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
			{id: "triangle", src: imgpath+"triangle.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "quad1", src: imgpath+"picture.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg1", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cupboard", src: imgpath+"cupboard.png", type: createjs.AbstractLoader.IMAGE},
			{id: "flowerpot", src: imgpath+"flowerpot.png", type: createjs.AbstractLoader.IMAGE},
			{id: "kite01", src: imgpath+"kite01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "kite02", src: imgpath+"kite02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mirror", src: imgpath+"mirror.png", type: createjs.AbstractLoader.IMAGE},
			{id: "picture", src: imgpath+"picture.png", type: createjs.AbstractLoader.IMAGE},
			{id: "salt", src: imgpath+"salt.png", type: createjs.AbstractLoader.IMAGE},
			{id: "window", src: imgpath+"window.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0", src: soundAsset+"default.ogg"},
            {id: "sound_01", src: soundAsset+"default.ogg"},
            {id: "sound_1", src: soundAsset1+"s1_p1.ogg"},
            {id: "sound_2_1", src: soundAsset1+"s1_p2_1.ogg"},
            {id: "sound_2_2", src: soundAsset1+"s1_p2_2.ogg"},
            {id: "sound_2_3", src: soundAsset1+"s1_p2_3.ogg"},
            {id: "sound_2_4", src: soundAsset1+"s1_p2_4.ogg"},
            {id: "sound_2_5", src: soundAsset1+"s1_p2_5.ogg"},
            {id: "sound_2_6", src: soundAsset1+"s1_p2_6.ogg"},
            {id: "sound_3_1", src: soundAsset1+"s1_p3_1.ogg"},
            {id: "sound_3_2", src: soundAsset1+"s1_p3_2.ogg"},
            {id: "sound_3_3", src: soundAsset1+"s1_p3_3.ogg"},
            {id: "sound_6_1", src: soundAsset1+"s1_p6_1.ogg"},
            {id: "sound_6_2", src: soundAsset1+"s1_p6_2.ogg"},
            {id: "sound_1_3_1", src: soundAsset1+"s1_p3_1.ogg"},
            {id: "sound_1_3_2", src: soundAsset1+"s1_p3_2.ogg"},
            {id: "sound_1_3_3", src: soundAsset1+"s1_p3_3.ogg"},
            {id: "sound_1_6_1", src: soundAsset1+"s1_p6_1.ogg"},
            {id: "sound_1_6_2", src: soundAsset1+"s1_p6_2.ogg"},
						{id: "sound_7", src: soundAsset1+"s1_p7.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	 function navigationcontroller(islastpageflag){
	  typeof islastpageflag === "undefined" ?
	  islastpageflag = false :
	  typeof islastpageflag != 'boolean'?
	  alert("NavigationController : Hi Master, please provide a boolean parameter") :
	  null;

	  if(countNext == 0 && $total_page!=1){
	  	$nextBtn.show(0);
	  	$prevBtn.css('display', 'none');
	  }
	  else if($total_page == 1){
	  	$prevBtn.css('display', 'none');
	  	$nextBtn.css('display', 'none');

	  	// if lastpageflag is true
	  	islastpageflag ?
	  	ole.footerNotificationHandler.lessonEndSetNotification() :
	  	ole.footerNotificationHandler.lessonEndSetNotification() ;
	  }
	  else if(countNext > 0 && countNext < $total_page-1){
	  	$nextBtn.show(0);
	  	$prevBtn.show(0);
	  }
	  else if(countNext == $total_page-1){
	  	$nextBtn.css('display', 'none');
	  	$prevBtn.show(0);

	  	// if lastpageflag is true
	  	islastpageflag ?
	  	ole.footerNotificationHandler.lessonEndSetNotification() :
	  	ole.footerNotificationHandler.pageEndSetNotification() ;
	  }
	  }

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}
    function sound_player_seq(soundarray,navflag){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(soundarray[0]);
        soundarray.splice( 0, 1);
        current_sound.on("complete", function(){
            if(soundarray.length > 0){
                sound_player_seq(soundarray, navflag);
            }else{
                if(navflag)
                    nav_button_controls(0);
            }

        });
    }

	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		var quadiycount = 0;
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		$(".buttonsel").click(function(){
			if($(this).hasClass("forhover")){
				$(this).removeClass('forhover');
					if($(this).hasClass("correct")){
						current_sound.stop();
						play_correct_incorrect_sound(1);
						$(this).css("background","#bed62f");
						$(this).css("border","5px solid #deef3c");
						$(this).css("color","white");
						// $(this).siblings(".corctopt").show(0);
						//$('.hint_image').show(0);
						appender($(this),'correct');
						$('.buttonsel').removeClass('forhover forhoverimg');
						navigationcontroller();
					}
					else{
                        current_sound.stop();
						play_correct_incorrect_sound(0);
						appender($(this),'incorrect');
						$(this).css("background","#FF0000");
						$(this).css("border","5px solid #980000");
						$(this).css("color","white");
						// $(this).siblings(".wrngopt").show(0);
					}
			}

			function appender($this, icon){
				if($this.hasClass("diybutton-1"))
					$(".coverboardfull").append("<img class='icon-one' src= '"+ preload.getResult(icon).src +"'>");
				else
					$(".coverboardfull").append("<img class='icon-two' src= '"+ preload.getResult(icon).src +"'>");
				}
		});

		$(".quaditem").click(function(){
			if($(this).hasClass("quadhover")){
				$(this).removeClass('quadhover');
					if($(this).hasClass("correct")){
						current_sound.stop();
						play_correct_incorrect_sound(1);
						appender($(this),'correct');
						quadiycount++;
						if(quadiycount == 4)
						 navigationcontroller();
					}
					else{
                        current_sound.stop();
						play_correct_incorrect_sound(0);
						appender($(this),'incorrect');
					}
			}

			function appender($this, icon){
				var posIt = $this.position();
				var centerX = posIt.left + ($this.width()/1.5);
				var centerY = posIt.top + ($this.height()/14);
				$(".coverboardfull").append("<img style='left:"+centerX+";top:"+centerY+";position:absolute;width:5%;' src= '"+ preload.getResult(icon).src +"'>");
				}
		});
		switch(countNext) {
			case 0:
				sound_player("sound_1", 1);
			break;
			case 1:
				// $(".quadfooter").delay(2000).fadeIn(500);
				$(".quadfooter").delay(11000).animate({"opacity":"1"},500);
				leftquadfuncs("sound_2_1", 1, 3);
				var q = Snap(".quadcont");
				Snap.load(imgpath+"quadrilateral.svg", function (f) {
				q.append(f);
				});
			break;
			case 2:
        sound_player_seq(["sound_1_3_1","sound_1_3_2","sound_1_3_3"],false);
      break;
			case 5:
        sound_player_seq(["sound_1_6_1","sound_1_6_2"],false);
      break;
			case 6:
				$(".lightblue").hide(0);
				$(".cusimg-8, .cusimg-6, .cusimg-1").animate({
					"top":"50%"
				},2000);
				$(".cusimg-4").animate({
					"top":"50%",
					"left":"63%"
				},2000, function(){
					$(".lightblue").fadeIn(300);//
						sound_player("sound_7", 1);

				});
			break;

        }
	}

	$(".buttonsel").click(function(){
		console.log("als");

		});

	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function leftquadfuncs(audiofam, startval, endval){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(audiofam);
		current_sound.play();
		$(".custblock-1").append("<span>"+eval("data.string.p1leftquad"+startval)+"</span><br/><br/>");
		texthighlight($board);
		switch (startval) {
			case 1:
				var s = Snap(".trianglecont");
				Snap.load(imgpath+"triangle.svg", function (f) {
				s.append(f);
				});
				$(".trianglecont").fadeIn();
			break;
			case 3:
			$(".trianglecont").empty();
				var s = Snap(".trianglecont");
				Snap.load(imgpath+"triangle.svg", function (f) {
				s.append(f);
				setTimeout(function(){
					$("#tirnagle").attr("class", "borderanimtri");
				}, 3000);
				setTimeout(function(){
					$("#tirnagle").attr("class", "");
					$(".triangs").attr("class", "angleanim");
				}, 6000);
				});
			break;
			default:
		}
		startval++;
		$(".custblock-1 > span").last().hide(0).fadeIn();
		current_sound.on('complete', function(){
			if(startval <= endval)
				leftquadfuncs("sound_2_"+(startval), startval, 3);
			else
				rightquadfuncs("sound_2_4", 1, 3);
		});

	}

	function rightquadfuncs(audiofam, startval, endval){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(audiofam);
		current_sound.play();
		$(".custblock-2").append("<span>"+eval("data.string.p1rightquad"+startval)+"</span><br/><br/>");
		texthighlight($board);
		switch (startval) {
			case 1:
				var s = Snap(".quadcont");
				Snap.load(imgpath+"quadrilateral.svg", function (f) {
				s.append(f);
				});
				$(".quadcont").fadeIn();
			break;
			case 3:
			$(".quadcont").empty();
				var s = Snap(".quadcont");
				Snap.load(imgpath+"quadrilateral.svg", function (f) {
				s.append(f);
				setTimeout(function(){
					$("#quadbosel").attr("class", "borderanimtri");
				}, 3000);
				setTimeout(function(){
					$("#quadbosel").attr("class", "");
					$(".triangs").attr("class", "angleanim");
				}, 6000);
				});
			break;
			default:
		}
		startval++;
		$(".custblock-2 > span").last().hide(0).fadeIn();
		current_sound.on('complete', function(){
			if(startval <= endval)
				rightquadfuncs("sound_2_"+(startval+3), startval, 3);
			else{
                navigationcontroller();
            }
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

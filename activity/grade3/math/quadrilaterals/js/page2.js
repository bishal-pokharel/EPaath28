var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";
var soundAsset1 = $ref+"/sounds/"+$lang+"/";


var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',

		extratextblock:[{
			textdata: data.string.p1text1,
			textclass: "instruction",
		}],
		uppertextblockadditionalclass: 'button-block',
		uppertextblock:[{
			textdata: data.string.remake,
			textclass: "bt-remake",
		},{
			textdata: data.string.check,
			textclass: "bt-check",
		}],
		lowertextblockadditionalclass: 'property-block',
		lowertextblock:[{
			textdata: data.string.p1text2,
			textclass: "",
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-not text-quad',
		}],
		drawblock: [{
			drawblock: 'polydraw-block'
		}]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
    var audioplayed = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0", src: soundAsset+"default.ogg"},
			{id: "sound_1", src: soundAsset+"default.ogg"},
            {id: "sound_1_1", src: soundAsset1+"s2_p1_1.ogg"},
            {id: "sound_1_2", src: soundAsset1+"s2_p1_2.ogg"},
            {id: "sound_1_3", src: soundAsset1+"s2_p1_3.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	 function navigationcontroller(islastpageflag){
	  typeof islastpageflag === "undefined" ?
	  islastpageflag = false :
	  typeof islastpageflag != 'boolean'?
	  alert("NavigationController : Hi Master, please provide a boolean parameter") :
	  null;

	  if(countNext == 0 && $total_page!=1){
	  	$nextBtn.show(0);
	  	$prevBtn.css('display', 'none');
	  }
	  else if($total_page == 1){
	  	$prevBtn.css('display', 'none');
	  	$nextBtn.css('display', 'none');

	  	// if lastpageflag is true
	  	// ole.footerNotificationHandler.pageEndSetNotification()
	  }
	  else if(countNext > 0 && countNext < $total_page-1){
	  	$nextBtn.show(0);
	  	$prevBtn.show(0);
	  }
	  else if(countNext == $total_page-1){
	  	$nextBtn.css('display', 'none');
	  	$prevBtn.show(0);

	  	// if lastpageflag is true
          ole.footerNotificationHandler.pageEndSetNotification()

      }
	  }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);

		switch(countNext) {
			case 0:
                audioplayed==0?sound_player("sound_1_1"):"";
                audioplayed++;
				var initial_lines = [[11,7],[7,23],[23,17],[17,11]];
				var slopes = [1,1,1,1];
				slopes = fillSlopes(slopes, initial_lines);
				var lineElements =[];
				// creating dots
				initializeSVG();

				function initializeSVG(){
					var index = 0;
					for(var j=0; j<5; j++){
						for(var i=0; i<5; i++){
							var dotElement = document.createElement("div");
							var leftPos = 4+i*23;
							var topPos = 4+j*23;
							dotElement.className = 'draw-dot dot-'+index;
							dotElement.style.left =  leftPos+'%';
							dotElement.style.top =  topPos+'%';
							$('#polydraw-block').append(dotElement);
							var ellipseElement = document.createElementNS("http://www.w3.org/2000/svg", "ellipse");
							ellipseElement.setAttribute("rx", "1.5");
							ellipseElement.setAttribute("ry", "1.7");
							ellipseElement.setAttribute("cx", leftPos);
							ellipseElement.setAttribute("cy", topPos);
							ellipseElement.setAttribute("fill", '#6A7097');
							$('#line-svg').append(ellipseElement);
							index++;
						}
					}
					//creating lines<line id="svgline-1" class='draw-lines' x1="0%" y1="0%" x2="50%" y2="50%" style="stroke:#000;stroke-width:2" />
					for(var i=0; i<4; i++){
						var lineElement = document.createElementNS("http://www.w3.org/2000/svg", "line");
						lineElements.push(lineElement);
						var xPos1 = 4 + ((initial_lines[i][0])%5)*23;
						var yPos1 = 4 + Math.floor((initial_lines[i][0])/5)*23;
						var xPos2 = 4 + ((initial_lines[i][1])%5)*23;
						var yPos2 = 4 + Math.floor((initial_lines[i][1]-1)/5)*23;

						lineElement.setAttribute("x1", xPos1);
						lineElement.setAttribute("y1", yPos1);
						lineElement.setAttribute("x2", xPos2);
						lineElement.setAttribute("y2", yPos2);
						lineElement.setAttribute("stroke", '#000000');
						lineElement.setAttribute("stroke-width", '1');
						lineElement.setAttribute("class", 'draw-line-'+(i));
						$('#line-svg').append(lineElement);

						var startElement = document.createElement("div");
						startElement.className = 'vertex-dot start-vertex line-'+(i);
						var endElement = document.createElement("div");
						endElement.className = 'vertex-dot end-vertex line-'+(i);
						$('.dot-'+initial_lines[i][0]).append(startElement);
						$('.dot-'+initial_lines[i][1]).append(endElement);
					}
				}

				function drawLine(lineNumber, is_start, parIdx){
					var tempIdx = 1;
					if(is_start) tempIdx = 0;
					initial_lines[lineNumber][tempIdx] = parIdx;
					slopes = fillSlopes(slopes, initial_lines);
					var x,y;
					[x,y] = convertToXY(parIdx);
					var tempX = 4+x*23;
					var tempY = 4+y*23;
					var lineElement = lineElements[lineNumber];
					if(is_start){
						lineElement.setAttribute("x1", tempX);
						lineElement.setAttribute("y1", tempY);
					} else{
						lineElement.setAttribute("x2", tempX);
						lineElement.setAttribute("y2", tempY);
					}
				}

				function drawLineDrag(lineNumber, is_start, x, y){
					var tempX = x+2;
					var tempY = y+2;
					var lineElement = lineElements[lineNumber];
					if(is_start){
						lineElement.setAttribute("x1", tempX);
						lineElement.setAttribute("y1", tempY);
					} else{
						lineElement.setAttribute("x2", tempX);
						lineElement.setAttribute("y2", tempY);
					}
				}

				function dragger(dragItem){
					var is_start = false;
					if(dragItem.hasClass('start-vertex')){
						is_start = true;
					}
					var lineNum = dragItem.attr('class');
					lineNum = parseInt(lineNum.replace(/[^0-9]/g, ''));

					var polyblockleft = $('#polydraw-block').offset().left;
					var polyblockTop = $('#polydraw-block').offset().top;
					var polyblockWidth = $('#polydraw-block').width();
					var polyblockHeight = $('#polydraw-block').height();

					var xPos = (100*((dragItem.offset().left - polyblockleft )/polyblockWidth));
					var yPos = (100*((dragItem.offset().top - polyblockTop )/polyblockHeight));
					drawLineDrag(lineNum, is_start, xPos, yPos);
				}

				function convertToXY(pointNumber){
					var xPos = pointNumber%5;
					var yPos = Math.floor(pointNumber/5);
					return [xPos, yPos];
				}

				function getSlope(point1, point2){
					var a,b,c,d;
					[a,b] = convertToXY(point1);
					[c,d] = convertToXY(point2);
					slope = 99999;
					if(a!=c){
						slope = Math.abs((d-b)/(c-a));
					}
					return slope;
				}

				function isQuadrilateral(points, slopeVals){
					for(var i=0; i<4; i++){
						for(var j=0; j<4; j++){
							if(i!=j){
								var commonPt = hasCommonPoints(points[i], points[j]);
								if(commonPt){
									var pt1 = removeFromArray(points[i], commonPt);
									var pt2 = removeFromArray(points[j], commonPt);
									var tempSlope = getSlope(pt1, pt2);
									// console.log(pt1+', ' + pt2 +' s = '+tempSlope+'  s1='+slopeVals[i]+' s2='+slopeVals[j]);
									if(slopeVals[i]==slopeVals[j] && slopeVals[i] == tempSlope && slopeVals[j] == tempSlope){
										// console.log('not quad');
										return false;
									}
								}
							}
						}
					}
					// console.log('quad');
					return true;
				}

				function removeFromArray(arr, element){
					var arrnew = arr.slice(0);
					var indexOfel = arrnew.indexOf(element);
					arrnew.splice(indexOfel, 1);
					return arrnew;
				}

				function fillSlopes(slopeArr, pointArr){
					for(var i=0; i<slopeArr.length; i++){
						slopeArr[i] = getSlope(pointArr[i][0], pointArr[i][1]);
					}
					return slopeArr;
				}

				function hasCommonPoints(line1, line2){
					var commonPoint = false;
					for(var i=0; i<line1.length; i++){
						if(line1[i]==line2[0] || line1[i]==line2[1]){
							commonPoint = line1[i]; //could return the actual value as well
						}
					}
					return commonPoint;
				}

				// returns true iff the line from (a,b)->(c,d) intersects with (p,q)->(r,s)
				function intersects(line1, line2) {
					//if they have common point they cannot intersect
					//checking common point
					if(hasCommonPoints(line1, line2)){
						// console.log('common-point');
						return false;
					} else{
						// console.log('no-common-point');
						var a,b,c,d,p,q,r,s;
						[a,b] = convertToXY(line1[0]);
						[c,d] = convertToXY(line1[1]);
						[p,q] = convertToXY(line2[0]);
						[r,s] = convertToXY(line2[1]);
						var det,
							gamma,
							lambda;
						det = (c - a) * (s - q) - (r - p) * (d - b);
						if (det === 0) {
							return false;
						} else {
							lambda = ((s - q) * (r - a) + (p - r) * (s - b)) / det;
							gamma = ((b - d) * (r - a) + (c - a) * (s - b)) / det;
							return (0 < lambda && lambda < 1) && (0 < gamma && gamma < 1);
						}
					}
				};
				function checkIntersections(points, slopeVals){
					for(var i=0; i<4; i++){
						for(var j=0; j<4; j++){
							if(i!=j){
								if(intersects(points[i], points[j])){
									// console.log('intersection');
									return true;
								}
							}
						}
					}
					// console.log('no intersection');
					return false;
				}

				function isClosed(points){
					var points1D = [];
					var returnVar = false;
					for(var i = 0; i < points.length; i++)
					{
						points1D = points1D.concat(points[i]);
					}
					var continueLoop = true;
					while(continueLoop){
						var hasCurrentItem = false;
						for (var i = 1; i < points1D.length; i++) {
							if(points1D[0] == points1D[i]){
								points1D.splice(0,1);
								points1D.splice(i-1,1);
								hasCurrentItem = true;
							}
						}
						if(points1D.length == 0){
							returnVar = true;
							continueLoop = false;
						}else if(!hasCurrentItem){
							continueLoop = false;
							returnVar = false;
							return false;//to quit loop early
						}
					}
					return returnVar;
				}

				$('.vertex-dot').draggable({
					containment: "polydraw-block",
					cursor: "move",
					revert: true,
					appendTo: "body",
					zIndex: 50,
					tolerance: 'touch',
					start: function( event, ui ){
						$(this).draggable('option', 'revert', true);
					},
					stop: function( event, ui ){
						var lineNum = $(this).attr('class');
						lineNum = parseInt(lineNum.replace(/[^0-9]/g, ''));
						var xPos1 = 4 + ((initial_lines[lineNum][0])%5)*23;
						var yPos1 = 4 + Math.floor((initial_lines[lineNum][0])/5)*23;
						lineElements[lineNum].setAttribute("x1", xPos1);
						lineElements[lineNum].setAttribute("y1", yPos1);

						$(this).css({
							'top': '0%',
							'left': '0%',
							'width': '100%',
							'height': '100%',
							'border-radius': '50%'
						});
					},
					drag: function(){
						dragger($(this));
					}
				});

				$('.draw-dot').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'vertex-dot');
					}
				});
				function dropper(dropped, droppedOn, hasclass){
					var is_start = false;
					if(dropped.hasClass('start-vertex')){
						is_start = true;
					}
					var lineNum = dropped.attr('class');
					lineNum = parseInt(lineNum.replace(/[^0-9]/g, ''));

					// to not allow both points of line to lie on same point
					var childrens = droppedOn.children('div');
					var isSameLine = false;
					// //to avoid having many vertex on same point
					if(childrens.length>1) return false;
					for(var i=0; i<childrens.length; i++){
						if(childrens.eq(i).hasClass('line-'+lineNum)){
							isSameLine = true;
						}
					}
					if(isSameLine) return false;

					var parentClass = droppedOn.attr('class');
					parentClass = parseInt(parentClass.replace(/[^0-9]/g, ''));
					drawLine(lineNum, is_start, parentClass);

					if(dropped.hasClass(hasclass)){
						// play_correct_incorrect_sound(1);
						dropped.detach().css({
							'top': '0%',
							'left': '0%',
							'width': '100%',
							'height': '100%',
							'border-radius': '50%'
						}).appendTo(droppedOn);
					} else{
						play_correct_incorrect_sound(0);
					}
				}

				$('.bt-remake').click(function(){
					templateCaller();
				});
				$('.bt-check').click(function(){
					// check if poly is closed
					if(isClosed(initial_lines)){
						// console.log('closed');
						$('.text-closed').hide(0);
						// check if it has not 3 points on straight line
						if(isQuadrilateral(initial_lines, slopes)){
							// console.log('quad');
							if($lang=="en"){
								$('.text-quad').hide(0);
							}else{
								$('.text-quad').show(0);
								$('.text-quad').html(eval("data.string.ho"));
							}
	              sound_player("sound_1_2");
						  	ole.footerNotificationHandler.pageEndSetNotification()
            } else{
                sound_player("sound_1_3");
								// console.log('not quad');
								$('.text-quad').show(0);
						}
					}else{
            sound_player("sound_1_3");
            console.log('open');
						$('.text-closed').show(0);
						$('.text-quad').show(0);
					}

					//check intersection
					if(checkIntersections(initial_lines, slopes)){
						// console.log('intersects');
						$('.text-side').show(0);
						$('.text-angle').show(0);
					} else{
						// console.log('no intersection');
						$('.text-angle').hide(0);
						$('.text-side').hide(0);
					}
				});
				break;
			default:
				$prevBtn.show(0);
				nav_button_controls(0);
				break;
		}
	}

	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function sound_player_seq(soundarray,navflag){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(soundarray[0]);
			soundarray.splice( 0, 1);
			current_sound.on("complete", function(){
					if(soundarray.length > 0){
							sound_player_seq(soundarray, navflag);
					}else{
							if(navflag)
									nav_button_controls(0);
					}

			});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

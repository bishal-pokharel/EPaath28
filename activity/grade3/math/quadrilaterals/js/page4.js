var imgpath = $ref + "/images/";
var soundAsset1 = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentblockadditionalclass: 'bg-pink',
		headerblock:[{
			textdata: data.string.p4text1
		}],
		singletext:[
			{
			textclass: "darkblue",
			textdata: data.string.p4text2
			},
			{
				textclass: "buttonsel forhover diydnd-1",
				textdata: "2"
			},
			{
				textclass: "buttonsel forhover diydnd-2",
				textdata: "7"
			},
			{
				textclass: "buttonsel correct forhover diydnd-3",
				textdata: "4"
			},
			{
				textclass: "buttonsel forhover diydnd-4",
				textdata: "9"
			}
		],
		svgblock: [
			{
				svgname: "quadcont",
			}
		]
},
// slide1
{
	contentblockadditionalclass: 'bg-pink',
	headerblock:[{
		textdata: data.string.p4text1
	}],
	singletext:[
		{
		textclass: "leftpink",
		textdata: data.string.p4text3
		},
		{
		textclass: "rightgrey",
		},
		{
		textclass: "qualabel-a",
		textdata: "A"
		},
		{
		textclass: "qualabel-b",
		textdata: "B"
		},
		{
		textclass: "qualabel-c",
		textdata: "C"
		},
		{
		textclass: "qualabel-d",
		textdata: "D"
		}
	],
	svgblock: [
		{
			svgname: "quadcont2",
		}
	]
},
// slide2
{
	contentblockadditionalclass: 'bg-pink',
	headerblock:[{
		textdata: data.string.p4text1
	}],
	singletext:[
		{
		textclass: "leftpink",
		textdata: data.string.p4text4
		},
		{
		textclass: "rightgrey",
		},
		{
		textclass: "qualabel-a",
		textdata: "A"
		},
		{
		textclass: "qualabel-b",
		textdata: "B"
		},
		{
		textclass: "qualabel-c",
		textdata: "C"
		},
		{
		textclass: "qualabel-d",
		textdata: "D"
		}
	],
	svgblock: [
		{
			svgname: "quadcont2",
		}
	]
},
// slide3
{
	contentblockadditionalclass: 'bg-pink',
	headerblock:[{
		textdata: data.string.p4text1
	}],
	singletext:[
		{
		datahighlightflag: true,
		datahighlightcustomclass: "anglehighs",
		textclass: "leftpink",
		textdata: data.string.p4text5
		},
		{
		textclass: "rightgrey",
		},
		{
		textclass: "qualabel-a",
		textdata: "A"
		},
		{
		textclass: "qualabel-b",
		textdata: "B"
		},
		{
		textclass: "qualabel-c",
		textdata: "C"
		},
		{
		textclass: "qualabel-d",
		textdata: "D"
		}
	],
	svgblock: [
		{
			svgname: "quadcont2",
		}
	]
},
{
	contentblockadditionalclass: 'bg-yell',
	headerblock:[{
		textdata: data.string.p4text1
	}],
	singletext:[
		{
		textclass: "lightblue",
		textdata: data.string.p4text6
	},
	{
		textclass: "inpbox-1",
		textdata: data.string.p4text7
	},
	{
		textclass: "inpbox-2",
		textdata: data.string.p4text8
	},
	{
		textclass: "inpbox-3",
		textdata: data.string.p4text9
	},
	{
		textclass: "sbmtbtn smbthov",
		textdata: data.string.submit
	},
	{
		textclass: "trybtn tryhov",
		textdata: data.string.try
	}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "quaimg-1",
				imgid : 'quad1',
				imgsrc: ""
			},
			{
				imgclass: "quaimg-2",
				imgid : 'quad2',
				imgsrc: ""
			},
			{
				imgclass: "quaimg-3",
				imgid : 'quad3',
				imgsrc: ""
			}
		]
	}]

}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "corrimg", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrimg", src: "images/wrongicon.png", type: createjs.AbstractLoader.IMAGE},
			{id: "quad1", src: imgpath+"forques/quadrilaterals01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "quad2", src: imgpath+"forques/quadrilaterals02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "quad3", src: imgpath+"forques/quadrilaterals03.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
            {id: "sound_1_1", src: soundAsset1+"s4_p1_1.ogg"},
            {id: "sound_1_2", src: soundAsset1+"s4_p1_2.ogg"},
            {id: "sound_2", src: soundAsset1+"s4_p2.ogg"},
            {id: "sound_3", src: soundAsset1+"s4_p3.ogg"},
            {id: "sound_4", src: soundAsset1+"s4_p4.ogg"},
            {id: "sound_5", src: soundAsset1+"s4_p5.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}
    function sound_player_seq(soundarray,navflag){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(soundarray[0]);
        soundarray.splice( 0, 1);
        current_sound.on("complete", function(){
            if(soundarray.length > 0){
                sound_player_seq(soundarray, navflag);
            }else{
                if(navflag)
                    nav_button_controls(0);
            }

        });
    }
	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		(countNext>0 &&countNext<4)?sound_player("sound_"+(countNext+1),true):"";
		switch(countNext){
			case 0:
				sound_player_seq(["sound_1_1","sound_1_2"],false);
				var q = Snap(".quadcont");
				Snap.load(imgpath+"rectangle.svg", function (f) {
				q.append(f);
				$("#blue").attr("display","none");
				$("#pink").attr("display","none");
				$("#yellow").attr("display","none");
				$("#green").attr("display","none");
				});

				$(".buttonsel").click(function(){
					if($(this).hasClass("forhover")){
						$(this).removeClass('forhover');
							if($(this).hasClass("correct")){
								current_sound.stop();
								play_correct_incorrect_sound(1);
								$(this).css("background","#bed62f");
								$(this).css("border","5px solid #deef3c");
								$(this).css("color","white");
								appender($(this),'corrimg');
								$('.buttonsel').removeClass('forhover forhoverimg');
								navigationcontroller();
								butclicked();
							}
							else{
                                current_sound.stop();
								play_correct_incorrect_sound(0);
								appender($(this),'incorrimg');
								$(this).css("background","#FF0000");
								$(this).css("border","5px solid #980000");
								$(this).css("color","white");
							}
					}

					function butclicked(){
						$("#blue").attr("display","block");
						$("#pink").attr("display","block");
						$("#yellow").attr("display","block");
						$("#green").attr("display","block");
					}

					function appender($this, icon){
						if($this.hasClass("diydnd-1"))
							$(".coverboardfull").append("<img class='myicon-one' src= '"+ preload.getResult(icon).src +"'>");
						else if($this.hasClass("diydnd-2"))
							$(".coverboardfull").append("<img class='myicon-two' src= '"+ preload.getResult(icon).src +"'>");
						else if($this.hasClass("diydnd-3"))
							$(".coverboardfull").append("<img class='myicon-three' src= '"+ preload.getResult(icon).src +"'>");
						else
							$(".coverboardfull").append("<img class='myicon-four' src= '"+ preload.getResult(icon).src +"'>");
						}
					});
			break;
			case 1:
				var q = Snap(".quadcont2");
				Snap.load(imgpath+"rectangle.svg", function (f) {
				q.append(f);
				setTimeout(function(){
					$("#yepath").attr("fill-opacity", "1");
				}, 3000);
				setTimeout(function(){
					$("#blpath").attr("fill-opacity", "1");
				}, 4000);
				setTimeout(function(){
					$("#grpath").attr("fill-opacity", "1");
				}, 5000);
				setTimeout(function(){
					$("#pipath").attr("fill-opacity", "1");
				}, 6000);
				setTimeout(function(){
					$(".qualabel-a").show(0);
				}, 7000);
				setTimeout(function(){
					$(".qualabel-b").show(0);
				}, 8000);
				setTimeout(function(){
					$(".qualabel-c").show(0);
				}, 9000);
				setTimeout(function(){
					$(".qualabel-d").show(0);
                }, 10000);
				});
			break;
			case 2:
				var q = Snap(".quadcont2");
				Snap.load(imgpath+"rectangle.svg", function (f) {
				q.append(f);
				$("#yepath, #blpath, #pipath, #grpath").attr("fill-opacity", "1");
				$(".qualabel-a, .qualabel-b, .qualabel-c, .qualabel-d").show(0);
                });
			break;
			case 3:
				var q = Snap(".quadcont2");
				Snap.load(imgpath+"rectangle.svg", function (f) {
				q.append(f);
				$("#yepath, #blpath, #pipath, #grpath").attr("fill-opacity", "1");
				$(".qualabel-a, .qualabel-b, .qualabel-c, .qualabel-d").show(0);
                });
			break;
			case 4:
				sound_player("sound_"+(countNext+1),false);
			var corrCounter = 0;
			var corArr = [["EFG","GFE"],["KLM","MLK"],["TWV","VWT"]];

			$(".sbmtbtn").click(function(){
				for(var i=1; i<=3; i++){
					answerChecker($("#input"+i), i-1);
				}
			});

			function answerChecker(curBox, index){
				var myAns = curBox.val().toUpperCase();
				curBox.prop('disabled', true);
				if(myAns == corArr[index][0] || myAns == corArr[index][1]){
					if(!curBox.hasClass("corrbox"))
						corrCounter++;
					if(corrCounter == 3) {
                        current_sound.stop();
                        play_correct_incorrect_sound(1);
                        navigationcontroller();
                    }
					curBox.addClass("corrbox");
					appender(curBox,'corrimg');

				}
				else{
                    current_sound.stop();
                    play_correct_incorrect_sound(0);
					curBox.addClass("incorrbox");
					appender(curBox,'incorrimg');
				}
			}

			function appender($this, icon){
				if($this.attr("id") == "input1")
					$(".coverboardfull").append("<img class='quico-one "+icon+"' src= '"+ preload.getResult(icon).src +"'>");
				else if($this.attr("id") == "input2")
				$(".coverboardfull").append("<img class='quico-two "+icon+"' src= '"+ preload.getResult(icon).src +"'>");
				else
					$(".coverboardfull").append("<img class='quico-three "+icon+"' src= '"+ preload.getResult(icon).src +"'>");
				}

				$(".trybtn").click(function(){
					$(".incorrbox").prop("disabled",false).removeClass("incorrbox").val("");
					$(".incorrimg").hide(0);
				});
			break;
		}


	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');


		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

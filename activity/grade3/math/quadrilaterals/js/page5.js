var imgpath = $ref + "/images/";
var soundAsset1 = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentblockadditionalclass: 'bg-pink',
		headerblock:[{
			textdata: data.string.p4text1
		}],
		singletext:[
			{
				textclass: "lightblue",
				textdata: data.string.p5text1
			},
			{
				textclass: "inpbox-1",
				textdata: data.string.p5inp1
			},
			{
				textclass: "inpbox-2",
				textdata: data.string.p5inp2
			},
			{
				textclass: "inpbox-3",
				textdata: data.string.p5inp3
			},
			{
				textclass: "inpbox-4",
				textdata: data.string.p5inp4
			},
			{
				textclass: "sbmtbtn smbthov",
				textdata: data.string.submit
			},
			{
				textclass: "trybtn tryhov",
				textdata: data.string.try
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "quaimg-1",
					imgid : 'quad1',
					imgsrc: ""
				}
			]
		}]

	},
	// slide1
	{
		contentblockadditionalclass: 'bg-pink',
		headerblock:[{
			textdata: data.string.p4text1
		}],
		singletext:[
			{
				textclass: "lightblue",
				textdata: data.string.p5text2
			},
			{
				textclass: "inpbox-1",
				textdata: data.string.p5inp1
			},
			{
				textclass: "inpbox-2",
				textdata: data.string.p5inp2
			},
			{
				textclass: "inpbox-3",
				textdata: data.string.p5inp3
			},
			{
				textclass: "inpbox-4",
				textdata: data.string.p5inp4
			},
			{
				textclass: "inpbox-5",
				textdata: data.string.p5inp5
			},
			{
				textclass: "sbmtbtn smbthov",
				textdata: data.string.submit
			},
			{
				textclass: "trybtn tryhov",
				textdata: data.string.try
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "quaimg-1",
					imgid : 'quad1',
					imgsrc: ""
				}
			]
		}]

	},
	// slide2
	{
		contentblockadditionalclass: 'bg-pink',
		headerblock:[{
			textdata: data.string.p4text1_1
		}],
		singletext:[
			{
				textclass: "lightblue",
				textdata: data.string.p5text3
			},
			{
				textclass: "inpbox-1",
				textdata: data.string.p5inp1
			},
			{
				textclass: "inpbox-2",
				textdata: data.string.p5inp2
			},
			{
				textclass: "inpbox-3",
				textdata: data.string.p5inp3
			},
			{
				textclass: "inpbox-4",
				textdata: data.string.p5inp4
			},
			{
				textclass: "inpbox-6",
				textdata: data.string.p5inp6
			},
			{
				textclass: "inpbox-7",
				textdata: data.string.p5inp7
			},
			{
				textclass: "inpbox-8",
				textdata: data.string.p5inp8
			},
			{
				textclass: "inpbox-9",
				textdata: data.string.p5inp9
			},
			{
				textclass: "sbmtbtn smbthov",
				textdata: data.string.submit
			},
			{
				textclass: "trybtn tryhov",
				textdata: data.string.try
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "quaimg-1",
					imgid : 'quad1',
					imgsrc: ""
				},
				{
					imgclass: "angimg-1",
					imgid : 'ang1',
					imgsrc: ""
				},
				{
					imgclass: "angimg-2",
					imgid : 'ang2',
					imgsrc: ""
				},
				{
					imgclass: "angimg-3",
					imgid : 'ang3',
					imgsrc: ""
				},
				{
					imgclass: "angimg-4",
					imgid : 'ang4',
					imgsrc: ""
				}
			]
		}]

	},
	{
		contentblockadditionalclass: 'bg-pink2',
		headerblock:[{
			textdata: data.string.p5text4
		}],
		uppertextblockadditionalclass: "glist",
		uppertextblock:[{
			textdata: data.string.p5text6
		}],
		singletext:[
			{
				textclass: "lightblue2",
				textdata: data.string.p5text5
			},
			{
				textclass: "tthese",
				textdata: data.string.p5text7
			},
			{
				textclass: "creamybg",
			},
			{
				textclass: "capbox-1",
				textdata: data.string.p5text8
			},
			{
				textclass: "capbox-2",
				textdata: data.string.p5text9
			},
			{
				textclass: "capbox-3",
				textdata: data.string.p5text10
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "penimg-1",
					imgid : 'pen1',
					imgsrc: ""
				},
				{
					imgclass: "penimg-2",
					imgid : 'pen2',
					imgsrc: ""
				},
				{
					imgclass: "penimg-3",
					imgid : 'pen3',
					imgsrc: ""
				}
			]
		}]
	}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var boxtext1;
	var boxtext2;
	var boxtext3;
	var boxtext4;
	var boxtext6;
	var boxtext7;
	var boxtext8;
	var boxtext9;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "quad1", src: imgpath+"forques/quadrilaterals04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ang1", src: imgpath+"forques/angle01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ang2", src: imgpath+"forques/angle02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ang3", src: imgpath+"forques/angle03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ang4", src: imgpath+"forques/angle04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pen1", src: imgpath+"shape01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pen2", src: imgpath+"shape02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pen3", src: imgpath+"shape03.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
            {id: "sound_1", src: soundAsset1+"s5_p1.ogg"},
            {id: "sound_2", src: soundAsset1+"s5_p2.ogg"},
            {id: "sound_3", src: soundAsset1+"s5_p3.ogg"},
            {id: "sound_4_1", src: soundAsset1+"s5_p4_1.ogg"},
            {id: "sound_4_2", src: soundAsset1+"s5_p4_2.ogg"},
            {id: "sound_4_3", src: soundAsset1+"s5_p4_3.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
     ole.footerNotificationHandler.lessonEndSetNotification() ;

 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}
    function sound_player_seq(soundarray,navflag){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(soundarray[0]);
        soundarray.splice( 0, 1);
        current_sound.on("complete", function(){
            if(soundarray.length > 0){
                sound_player_seq(soundarray, navflag);
            }else{
                if(navflag)
                    navigationcontroller(0);
            }

        });
    }

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		var nxtFlag = true;
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext){
			case 0:
				sound_player("sound_1",false);
			$(".sbmtbtn").click(function(){
				nxtFlag = true;
				boxtext1 = $("#input1").val().toUpperCase();
				boxtext2 = $("#input2").val().toUpperCase();
				boxtext3 = $("#input3").val().toUpperCase();
				boxtext4 = $("#input4").val().toUpperCase();

				for(var i=1; i<5; i++){
					$("#input"+i).prop('disabled', true);
					for(var j=i+1; j<5; j++){
						if($("#input"+i).val().toUpperCase() == $("#input"+j).val().toUpperCase()){
							$("#input"+i).addClass("incorrbox");
							$("#input"+j).addClass("incorrbox");
						}
						else{
							$("#input"+i).addClass("corrbox");
							$("#input"+j).addClass("corrbox");
						}
					}
				}

				$("input[id^='input']").each(function(){
					current_sound.stop();
                    play_correct_incorrect_sound(0)
                    if($(this).hasClass("incorrbox")){
						nxtFlag = false;
					}
				});

				if(nxtFlag == true){
                    current_sound.stop();
					play_correct_incorrect_sound(1)
					console.log(nxtFlag);
					navigationcontroller();
				}
			});

			$(".trybtn").click(function(){
                $("#input5").removeClass("incorrbox").val(" ");

                $(".incorrbox").prop("disabled",false).removeClass("incorrbox corrbox").val("");
			});
			break;
			case 1:
                sound_player("sound_2",false);
                var arrHelper = [boxtext1, boxtext2, boxtext3, boxtext4];
			var mover;
			var posAns = [];
			var thisAns;
			var revAns;
			var typedAns;
			$("#input2").val(boxtext2).prop('disabled', true).addClass("boxdisabled");
			$("#input3").val(boxtext3).prop('disabled', true).addClass("boxdisabled");
			$("#input4").val(boxtext4).prop('disabled', true).addClass("boxdisabled");
			$("#input1").val(boxtext1).prop('disabled', true).addClass("boxdisabled");
			for(var i = 0; i<4; i++){
				thisAns = arrHelper[0].concat(arrHelper[1]).concat(arrHelper[2]).concat(arrHelper[3]);
				revAns = thisAns.split("").reverse().join("");
				posAns.push(thisAns);
				posAns.push(revAns);
				console.log(posAns);
				mover = arrHelper.pop();
				arrHelper.unshift(mover);
			}

			$(".sbmtbtn").click(function(){
				typedAns = $("#input5").val().toUpperCase();
				for (var i = 0; i < posAns.length; i++) {
					if(typedAns == posAns[i]){
                        current_sound.stop();
                        play_correct_incorrect_sound(1);
						$("#input5").addClass("corrbox");
						navigationcontroller();
					}
				}
				if(!$("#input5").hasClass("corrbox")){
                    current_sound.stop();
                    play_correct_incorrect_sound(0)
                    $("#input5").addClass("incorrbox");
				}
			});
                $(".trybtn").click(function() {
                    $("#input5").removeClass("incorrbox").val(" ");
                });
                break;
			case 2:
                sound_player("sound_3",false);

                var yelArr = [];
				var bluArr = [];
				var pinArr = [];
				var greArr = [];

				var thisAns;
				var revAns;

				thisAns = boxtext4.concat(boxtext1).concat(boxtext2);
				revAns = thisAns.split("").reverse().join("");
				yelArr.push(thisAns);
				yelArr.push(revAns);

				thisAns = boxtext1.concat(boxtext2).concat(boxtext3);
				revAns = thisAns.split("").reverse().join("");
				bluArr.push(thisAns);
				bluArr.push(revAns);

				thisAns = boxtext1.concat(boxtext4).concat(boxtext3);
				revAns = thisAns.split("").reverse().join("");
				pinArr.push(thisAns);
				pinArr.push(revAns);

				thisAns = boxtext4.concat(boxtext3).concat(boxtext2);
				revAns = thisAns.split("").reverse().join("");
				greArr.push(thisAns);
				greArr.push(revAns);

				console.log(yelArr);
				console.log(bluArr);
				console.log(pinArr);
				console.log(greArr);

				$("#input2").val(boxtext2).prop('disabled', true).addClass("boxdisabled");
				$("#input3").val(boxtext3).prop('disabled', true).addClass("boxdisabled");
				$("#input4").val(boxtext4).prop('disabled', true).addClass("boxdisabled");
				$("#input1").val(boxtext1).prop('disabled', true).addClass("boxdisabled");

				$(".sbmtbtn").click(function(){
					nxtFlag = true;
					boxtext6 = $("#input6").val().toUpperCase();
					boxtext7 = $("#input7").val().toUpperCase();
					boxtext8 = $("#input8").val().toUpperCase();
					boxtext9 = $("#input9").val().toUpperCase();

					console.log(boxtext6);
					console.log(boxtext7);
					console.log(boxtext8);
					console.log(boxtext9);

					corrChecker(boxtext6, yelArr, $("#input6"));
					corrChecker(boxtext7, bluArr, $("#input7"));
					corrChecker(boxtext8, pinArr, $("#input8"));
					corrChecker(boxtext9, greArr, $("#input9"));

					function corrChecker(myans, myarr, myfield){
						for (var i = 0; i < myarr.length; i++) {
							if(myans == myarr[i]){
								myfield.addClass("corrbox").prop('disabled', true);
							}
						}

						if(!myfield.hasClass("corrbox")){
							myfield.addClass("incorrbox").prop('disabled', true);
						}
					}

					$("input[id^='input']").each(function(){
						if($(this).hasClass("incorrbox")){
							nxtFlag = false;
                            current_sound.stop();
                            play_correct_incorrect_sound(0);
						}
						console.log(nxtFlag);
					});

					if(nxtFlag == true){
                        current_sound.stop();
                        play_correct_incorrect_sound(1);
						console.log(nxtFlag);
						navigationcontroller();
					}

					$(".trybtn").click(function(){
						$("#input5").text(" ");
						$(".inpbox-5").text(" ");
						$(".incorrbox").prop("disabled",false).removeClass("incorrbox corrbox").val("");
					});
				});
			break;
			case 3:
				sound_player_seq(["sound_4_1","sound_4_2","sound_4_3"],true);
				break;
		}

	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";
var soundAsset1 = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "thebg1",
		imageslider:[
			{
				imgclass: "a1",
				imgid: "head01",
				imgsrc: ""
			},
			{
				imgclass: "a2",
				imgid: "head02",
				imgsrc: ""
			},
			{
				imgclass: "a3",
				imgid: "head03",
				imgsrc: ""
			},
			{
				imgclass: "corquad a4",
				imgid: "head04",
				imgsrc: ""
			},
			{
				imgclass: "corquad a5",
				imgid: "head05",
				imgsrc: ""
			},
			{
				imgclass: "corquad a6",
				imgid: "head06",
				imgsrc: ""
			}
		],
		robot:[
			{
					imgclass: "robot-head",
					imgid : 'head06',
					imgsrc: ""
				},
				{
					imgclass: "robot-eyes",
					imgid : 'eyes01',
					imgsrc: ""
				},
				{
					imgclass: "robot-mouth",
					imgid : 'mouth01',
					imgsrc: ""
				},
				{
					imgclass: "robot-neck",
					imgid : 'neck01',
					imgsrc: ""
				},
				{
					imgclass: "robot-body",
					imgid : 'body01',
					imgsrc: ""
				},
				{
					imgclass: "robot-hands-1",
					imgid : 'handleft01',
					imgsrc: ""
				},
				{
					imgclass: "robot-hands-2",
					imgid : 'handleft01',
					imgsrc: ""
				},
				{
					imgclass: "robot-legs-1",
					imgid : 'legsleft01',
					imgsrc: ""
				},
				{
					imgclass: "robot-legs-2",
					imgid : 'legsleft01',
					imgsrc: ""
				}
		],
		singletext:[
		{
			textclass: "lightblue",
			textdata: data.string.p1text12 + data.string.p1text14
		},
		{
			textclass: "sbmtbtn smbthov",
			textdata: data.string.submit
		},
		{
			textclass: "errormsg",
			textdata: data.string.p1text13
		}
	],
	headerblock:[
	{
		textdata: data.string.p1text11
	}
]
},
//slide1
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg1",
	imageslider:[
		{
			imgclass: "corquad a1",
			imgid: "eyes01",
			imgsrc: ""
		},
		{
			imgclass: "corquad a2",
			imgid: "eyes02",
			imgsrc: ""
		},
		{
			imgclass: "corquad a3",
			imgid: "eyes03",
			imgsrc: ""
		},
		{
			imgclass: "a4",
			imgid: "eyes04",
			imgsrc: ""
		},
		{
			imgclass: "a5",
			imgid: "eyes05",
			imgsrc: ""
		},
		{
			imgclass: "a6",
			imgid: "eyes06",
			imgsrc: ""
		}
	],
	robot:[
		{
				imgclass: "robot-head",
				imgid : 'head06',
				imgsrc: ""
			},
			{
				imgclass: "robot-eyes",
				imgid : 'eyes01',
				imgsrc: ""
			},
			{
				imgclass: "robot-mouth",
				imgid : 'mouth01',
				imgsrc: ""
			},
			{
				imgclass: "robot-neck",
				imgid : 'neck01',
				imgsrc: ""
			},
			{
				imgclass: "robot-body",
				imgid : 'body01',
				imgsrc: ""
			},
			{
				imgclass: "robot-hands-1",
				imgid : 'handleft01',
				imgsrc: ""
			},
			{
				imgclass: "robot-hands-2",
				imgid : 'handleft01',
				imgsrc: ""
			},
			{
				imgclass: "robot-legs-1",
				imgid : 'legsleft01',
				imgsrc: ""
			},
			{
				imgclass: "robot-legs-2",
				imgid : 'legsleft01',
				imgsrc: ""
			}
	],
	singletext:[
	{
		textclass: "lightblue",
		textdata: data.string.p1text15
	},
	{
		textclass: "sbmtbtn smbthov",
		textdata: data.string.submit
	},
	{
		textclass: "errormsg",
		textdata: data.string.p1text13
	}
],
headerblock:[
{
	textdata: data.string.p1text11
}
]
},
//slide2
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg1",
	imageslider:[
		{
			imgclass: "corquad a1",
			imgid: "mouth01",
			imgsrc: ""
		},
		{
			imgclass: "a2",
			imgid: "mouth02",
			imgsrc: ""
		},
		{
			imgclass: "a3",
			imgid: "mouth03",
			imgsrc: ""
		},
		{
			imgclass: "a4",
			imgid: "mouth04",
			imgsrc: ""
		},
		{
			imgclass: "a5",
			imgid: "mouth05",
			imgsrc: ""
		},
		{
			imgclass: "a6",
			imgid: "mouth06",
			imgsrc: ""
		}
	],
	robot:[
		{
				imgclass: "robot-head",
				imgid : 'head06',
				imgsrc: ""
			},
			{
				imgclass: "robot-eyes",
				imgid : 'eyes01',
				imgsrc: ""
			},
			{
				imgclass: "robot-mouth",
				imgid : 'mouth01',
				imgsrc: ""
			},
			{
				imgclass: "robot-neck",
				imgid : 'neck01',
				imgsrc: ""
			},
			{
				imgclass: "robot-body",
				imgid : 'body01',
				imgsrc: ""
			},
			{
				imgclass: "robot-hands-1",
				imgid : 'handleft01',
				imgsrc: ""
			},
			{
				imgclass: "robot-hands-2",
				imgid : 'handleft01',
				imgsrc: ""
			},
			{
				imgclass: "robot-legs-1",
				imgid : 'legsleft01',
				imgsrc: ""
			},
			{
				imgclass: "robot-legs-2",
				imgid : 'legsleft01',
				imgsrc: ""
			}
	],
	singletext:[
	{
		textclass: "lightblue",
		textdata: data.string.p1text16
	},
	{
		textclass: "sbmtbtn smbthov",
		textdata: data.string.submit
	},
	{
		textclass: "errormsg",
		textdata: data.string.p1text13
	}
],
headerblock:[
{
	textdata: data.string.p1text11
}
]
},
//slide3
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg1",
	imageslider:[
		{
			imgclass: "corquad a1",
			imgid: "neck01",
			imgsrc: ""
		},
		{
			imgclass: "a2",
			imgid: "neck02",
			imgsrc: ""
		},
		{
			imgclass: "a3",
			imgid: "neck03",
			imgsrc: ""
		},
		{
			imgclass: "a4",
			imgid: "neck04",
			imgsrc: ""
		},
		{
			imgclass: "a5",
			imgid: "neck05",
			imgsrc: ""
		},
		{
			imgclass: "a6",
			imgid: "neck06",
			imgsrc: ""
		}
	],
	robot:[
		{
				imgclass: "robot-head",
				imgid : 'head06',
				imgsrc: ""
			},
			{
				imgclass: "robot-eyes",
				imgid : 'eyes01',
				imgsrc: ""
			},
			{
				imgclass: "robot-mouth",
				imgid : 'mouth01',
				imgsrc: ""
			},
			{
				imgclass: "robot-neck",
				imgid : 'neck01',
				imgsrc: ""
			},
			{
				imgclass: "robot-body",
				imgid : 'body01',
				imgsrc: ""
			},
			{
				imgclass: "robot-hands-1",
				imgid : 'handleft01',
				imgsrc: ""
			},
			{
				imgclass: "robot-hands-2",
				imgid : 'handleft01',
				imgsrc: ""
			},
			{
				imgclass: "robot-legs-1",
				imgid : 'legsleft01',
				imgsrc: ""
			},
			{
				imgclass: "robot-legs-2",
				imgid : 'legsleft01',
				imgsrc: ""
			}
	],
	singletext:[
	{
		textclass: "lightblue",
		textdata: data.string.p1text17
	},
	{
		textclass: "sbmtbtn smbthov",
		textdata: data.string.submit
	},
	{
		textclass: "errormsg",
		textdata: data.string.p1text13
	}
],
headerblock:[
{
	textdata: data.string.p1text11
}
]
},
//slide4
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg1",
	imageslider:[
		{
			imgclass: "corquad a1",
			imgid: "body01",
			imgsrc: ""
		},
		{
			imgclass: "corquad a2",
			imgid: "body02",
			imgsrc: ""
		},
		{
			imgclass: "corquad a3",
			imgid: "body03",
			imgsrc: ""
		},
		{
			imgclass: "a4",
			imgid: "body04",
			imgsrc: ""
		},
		{
			imgclass: "a5",
			imgid: "body05",
			imgsrc: ""
		},
		{
			imgclass: "a6",
			imgid: "body06",
			imgsrc: ""
		}
	],
	robot:[
		{
				imgclass: "robot-head",
				imgid : 'head06',
				imgsrc: ""
			},
			{
				imgclass: "robot-eyes",
				imgid : 'eyes01',
				imgsrc: ""
			},
			{
				imgclass: "robot-mouth",
				imgid : 'mouth01',
				imgsrc: ""
			},
			{
				imgclass: "robot-neck",
				imgid : 'neck01',
				imgsrc: ""
			},
			{
				imgclass: "robot-body",
				imgid : 'body01',
				imgsrc: ""
			},
			{
				imgclass: "robot-hands-1",
				imgid : 'handleft01',
				imgsrc: ""
			},
			{
				imgclass: "robot-hands-2",
				imgid : 'handleft01',
				imgsrc: ""
			},
			{
				imgclass: "robot-legs-1",
				imgid : 'legsleft01',
				imgsrc: ""
			},
			{
				imgclass: "robot-legs-2",
				imgid : 'legsleft01',
				imgsrc: ""
			}
	],
	singletext:[
	{
		textclass: "lightblue",
		textdata: data.string.p1text18
	},
	{
		textclass: "sbmtbtn smbthov",
		textdata: data.string.submit
	},
	{
		textclass: "errormsg",
		textdata: data.string.p1text13
	}
],
headerblock:[
{
	textdata: data.string.p1text11
}
]
},
//slide5
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg1",
	imageslider:[
		{
			imgclass: "corquad a1",
			imgid: "handleft01",
			imgsrc: ""
		},
		{
			imgclass: "corquad a2",
			imgid: "handleft02",
			imgsrc: ""
		},
		{
			imgclass: "corquad a3",
			imgid: "handleft03",
			imgsrc: ""
		},
		{
			imgclass: "a4",
			imgid: "handleft04",
			imgsrc: ""
		},
		{
			imgclass: "a5",
			imgid: "handleft05",
			imgsrc: ""
		},
		{
			imgclass: "a6",
			imgid: "handleft06",
			imgsrc: ""
		}
	],
	robot:[
		{
				imgclass: "robot-head",
				imgid : 'head06',
				imgsrc: ""
			},
			{
				imgclass: "robot-eyes",
				imgid : 'eyes01',
				imgsrc: ""
			},
			{
				imgclass: "robot-mouth",
				imgid : 'mouth01',
				imgsrc: ""
			},
			{
				imgclass: "robot-neck",
				imgid : 'neck01',
				imgsrc: ""
			},
			{
				imgclass: "robot-body",
				imgid : 'body01',
				imgsrc: ""
			},
			{
				imgclass: "robot-hands-1",
				imgid : 'handleft01',
				imgsrc: ""
			},
			{
				imgclass: "robot-hands-2",
				imgid : 'handleft01',
				imgsrc: ""
			},
			{
				imgclass: "robot-legs-1",
				imgid : 'legsleft01',
				imgsrc: ""
			},
			{
				imgclass: "robot-legs-2",
				imgid : 'legsleft01',
				imgsrc: ""
			}
	],
	singletext:[
	{
		textclass: "lightblue",
		textdata: data.string.p1text19
	},
	{
		textclass: "sbmtbtn smbthov",
		textdata: data.string.submit
	},
	{
		textclass: "errormsg",
		textdata: data.string.p1text13
	}
],
headerblock:[
{
	textdata: data.string.p1text11
}
]
},
//slide6
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "thebg1",
	imageslider:[
		{
			imgclass: "corquad a1",
			imgid: "legsleft01",
			imgsrc: ""
		},
		{
			imgclass: "corquad a2",
			imgid: "legsleft02",
			imgsrc: ""
		},
		{
			imgclass: "corquad a3",
			imgid: "legsleft03",
			imgsrc: ""
		},
		{
			imgclass: "a4",
			imgid: "legsleft04",
			imgsrc: ""
		},
		{
			imgclass: "a5",
			imgid: "legsleft05",
			imgsrc: ""
		},
		{
			imgclass: "a6",
			imgid: "legsleft06",
			imgsrc: ""
		}
	],
	robot:[
		{
				imgclass: "robot-head",
				imgid : 'head06',
				imgsrc: ""
			},
			{
				imgclass: "robot-eyes",
				imgid : 'eyes01',
				imgsrc: ""
			},
			{
				imgclass: "robot-mouth",
				imgid : 'mouth01',
				imgsrc: ""
			},
			{
				imgclass: "robot-neck",
				imgid : 'neck01',
				imgsrc: ""
			},
			{
				imgclass: "robot-body",
				imgid : 'body01',
				imgsrc: ""
			},
			{
				imgclass: "robot-hands-1",
				imgid : 'handleft01',
				imgsrc: ""
			},
			{
				imgclass: "robot-hands-2",
				imgid : 'handleft01',
				imgsrc: ""
			},
			{
				imgclass: "robot-legs-1",
				imgid : 'legsleft01',
				imgsrc: ""
			},
			{
				imgclass: "robot-legs-2",
				imgid : 'legsleft01',
				imgsrc: ""
			}
	],
	singletext:[
	{
		textclass: "lightblue",
		textdata: data.string.p1text119
	},
	{
		textclass: "sbmtbtn smbthov",
		textdata: data.string.submit
	},
	{
		textclass: "errormsg",
		textdata: data.string.p1text13
	}
],
headerblock:[
{
	textdata: data.string.p1text11
}
]
},
//slide7
{
	robot:[
		{
				imgclass: "robot-head",
				imgid : 'head06',
				imgsrc: ""
			},
			{
				imgclass: "robot-eyes",
				imgid : 'eyes01',
				imgsrc: ""
			},
			{
				imgclass: "robot-mouth",
				imgid : 'mouth01',
				imgsrc: ""
			},
			{
				imgclass: "robot-neck",
				imgid : 'neck01',
				imgsrc: ""
			},
			{
				imgclass: "robot-body",
				imgid : 'body01',
				imgsrc: ""
			},
			{
				imgclass: "robot-hands-1",
				imgid : 'handleft01',
				imgsrc: ""
			},
			{
				imgclass: "robot-hands-2",
				imgid : 'handleft01',
				imgsrc: ""
			},
			{
				imgclass: "robot-legs-1",
				imgid : 'legsleft01',
				imgsrc: ""
			},
			{
				imgclass: "robot-legs-2",
				imgid : 'legsleft01',
				imgsrc: ""
			}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "disco-1",
				imgid : 'disco1',
				imgsrc: ""
			},
			{
				imgclass: "disco-2",
				imgid : 'disco2',
				imgsrc: ""
			},
			{
				imgclass: "disco-3",
				imgid : 'disco3',
				imgsrc: ""
			},
			{
				imgclass: "cover",
				imgid : 'bgdis',
				imgsrc: ""
			}
		]
	}]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;
	var headStore;
	var eyesStore;
	var mouthStore;
	var neckStore;
	var bodyStore;
	var handsStore;
	var legsStore;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			//head
			{id: "head01", src: imgpath+"robot/head/head01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "head02", src: imgpath+"robot/head/head02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "head03", src: imgpath+"robot/head/head03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "head04", src: imgpath+"robot/head/head04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "head05", src: imgpath+"robot/head/head05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "head06", src: imgpath+"robot/head/head06.png", type: createjs.AbstractLoader.IMAGE},

			//eyes
			{id: "eyes01", src: imgpath+"robot/eye/e01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eyes02", src: imgpath+"robot/eye/e02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eyes03", src: imgpath+"robot/eye/e03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eyes04", src: imgpath+"robot/eye/e04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eyes05", src: imgpath+"robot/eye/e05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eyes06", src: imgpath+"robot/eye/e06.png", type: createjs.AbstractLoader.IMAGE},

			//mouth
			{id: "mouth01", src: imgpath+"robot/mouth/m01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mouth02", src: imgpath+"robot/mouth/m02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mouth03", src: imgpath+"robot/mouth/m03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mouth04", src: imgpath+"robot/mouth/m04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mouth05", src: imgpath+"robot/mouth/m05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mouth06", src: imgpath+"robot/mouth/m06.png", type: createjs.AbstractLoader.IMAGE},

			//neck
			{id: "neck01", src: imgpath+"robot/neck/n01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "neck02", src: imgpath+"robot/neck/n02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "neck03", src: imgpath+"robot/neck/n03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "neck04", src: imgpath+"robot/neck/n04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "neck05", src: imgpath+"robot/neck/n05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "neck06", src: imgpath+"robot/neck/n06.png", type: createjs.AbstractLoader.IMAGE},

			//body
			{id: "body01", src: imgpath+"robot/body/b01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "body02", src: imgpath+"robot/body/b02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "body03", src: imgpath+"robot/body/b03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "body04", src: imgpath+"robot/body/b04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "body05", src: imgpath+"robot/body/b05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "body06", src: imgpath+"robot/body/b06.png", type: createjs.AbstractLoader.IMAGE},

			//hand
			{id: "handleft01", src: imgpath+"robot/hands/hl01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "handleft02", src: imgpath+"robot/hands/hl02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "handleft03", src: imgpath+"robot/hands/hl03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "handleft04", src: imgpath+"robot/hands/hl04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "handleft05", src: imgpath+"robot/hands/hl05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "handleft06", src: imgpath+"robot/hands/hl06.png", type: createjs.AbstractLoader.IMAGE},

			//legs
			{id: "legsleft01", src: imgpath+"robot/legs/leg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "legsleft02", src: imgpath+"robot/legs/leg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "legsleft03", src: imgpath+"robot/legs/leg03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "legsleft04", src: imgpath+"robot/legs/leg04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "legsleft05", src: imgpath+"robot/legs/leg05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "legsleft06", src: imgpath+"robot/legs/leg06.png", type: createjs.AbstractLoader.IMAGE},

			{id: "disco1", src: imgpath+"light01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "disco2", src: imgpath+"light02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "disco3", src: imgpath+"light03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bgdis", src: imgpath+"bg-for-room.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds

			{id: "Quad_dance_loop", src: soundAsset+"Quad_dance.ogg"},
            {id: "sound_1_1", src: soundAsset1+"s3_p1_1.ogg"},
            {id: "sound_1_2", src: soundAsset1+"s3_p1_2.ogg"},
            {id: "sound_2", src: soundAsset1+"s3_p2.ogg"},
            {id: "sound_3", src: soundAsset1+"s3_p3.ogg"},
            {id: "sound_4", src: soundAsset1+"s3_p4.ogg"},
            {id: "sound_5", src: soundAsset1+"s3_p5.ogg"},
            {id: "sound_6", src: soundAsset1+"s3_p6.ogg"},
            {id: "sound_7", src: soundAsset1+"s3_p7.ogg"},
            {id: "quad", src: "sounds/common/grade1/correct.ogg"},
            {id: "nonquad", src: "sounds/common/grade1/incorrect.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}
    function sound_player_seq(soundarray,navflag){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(soundarray[0]);
        soundarray.splice( 0, 1);
        current_sound.on("complete", function(){
            if(soundarray.length > 0){
                sound_player_seq(soundarray, navflag);
            }else{
                if(navflag)
                    nav_button_controls(0);
            }

        });
    }
	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		var quadFlag = false;

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_image3(content, countNext);
		put_speechbox_image(content, countNext);

		var parent = $(".quad-list-container");
		var divs = parent.children();
	 while (divs.length) {
	        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
	  }

		var compHeight = $(".sliderimg").first().find("img").height();
		var compWidth = $(".sliderimg").first().find("img").width();
		console.log(compHeight, compWidth);
		if(compHeight > (compWidth/1.5)){
			$(".sliderimg").find("img").addClass("imgwbheight");
		}
		else{
			$(".sliderimg").find("img").addClass("imgwbwidth");
		}
		switch(countNext){
			//face
			case 0:
                sound_player_seq(["sound_1_1","sound_1_2"],false);
			$(".quad-list-container").on('click', '.sliderimg', function(){
				clickAction($(".robot-head"), $(this));
			});
			$(".sbmtbtn").click(function(){
				headStore = $(".robot-head").attr('src');
				submitClick();
			});
			break;
			//eyes
			case 1:
                sound_player("sound_2",false);
                $(".robot-head").attr('src',headStore).show(0);

				$(".quad-list-container").on('click', '.sliderimg', function(){
					clickAction($(".robot-eyes"), $(this));
				});

				$(".sbmtbtn").click(function(){
					eyesStore = $(".robot-eyes").attr('src');
					submitClick();
				});
			break;
			//mouth
			case 2:
                sound_player("sound_3",false);
                $(".robot-head").attr('src',headStore).show(0);
				$(".robot-eyes").attr('src',eyesStore).show(0);

				$(".quad-list-container").on('click', '.sliderimg', function(){
					clickAction($(".robot-mouth"), $(this));
				});

				$(".sbmtbtn").click(function(){
					mouthStore = $(".robot-mouth").attr('src');
					submitClick();
				});
			break;
			//neck
			case 3:
                sound_player("sound_4",false);
                $(".robot-head").attr('src',headStore).show(0);
				$(".robot-eyes").attr('src',eyesStore).show(0);
				$(".robot-mouth").attr('src',mouthStore).show(0);

				$(".quad-list-container").on('click', '.sliderimg', function(){
					clickAction($(".robot-neck"), $(this));
				});

				$(".sbmtbtn").click(function(){
					neckStore = $(".robot-neck").attr('src');
					submitClick();
				});
			break;
			//body
			case 4:
                sound_player("sound_5",false);
                $(".robot-head").attr('src',headStore).show(0);
				$(".robot-eyes").attr('src',eyesStore).show(0);
				$(".robot-mouth").attr('src',mouthStore).show(0);
				$(".robot-neck").attr('src',neckStore).show(0);

				$(".quad-list-container").on('click', '.sliderimg', function(){
					clickAction($(".robot-body"), $(this));
				});

				$(".sbmtbtn").click(function(){
					bodyStore = $(".robot-body").attr('src');
					submitClick();
				});
			break;
			//hands
			case 5:
                sound_player("sound_6",false);
                $(".robot-head").attr('src',headStore).show(0);
				$(".robot-eyes").attr('src',eyesStore).show(0);
				$(".robot-mouth").attr('src',mouthStore).show(0);
				$(".robot-neck").attr('src',neckStore).show(0);
				$(".robot-body").attr('src',bodyStore).show(0);

				$(".quad-list-container").on('click', '.sliderimg', function(){
					clickAction($(".robot-hands-1"), $(this));
					clickAction($(".robot-hands-2"), $(this));
				});

				$(".sbmtbtn").click(function(){
					handsStore = $(".robot-hands-1").attr('src');
					submitClick();
				});
			break;
			//legs
			case 6:
                sound_player("sound_7",false);
                $(".robot-head").attr('src',headStore).show(0);
				$(".robot-eyes").attr('src',eyesStore).show(0);
				$(".robot-mouth").attr('src',mouthStore).show(0);
				$(".robot-neck").attr('src',neckStore).show(0);
				$(".robot-body").attr('src',bodyStore).show(0);
				$(".robot-hands-1").attr('src',handsStore).show(0);
				$(".robot-hands-2").attr('src',handsStore).show(0);

				$(".quad-list-container").on('click', '.sliderimg', function(){
					clickAction($(".robot-legs-1"), $(this));
					clickAction($(".robot-legs-2"), $(this));
				});

				$(".sbmtbtn").click(function(){
					legsStore = $(".robot-legs-1").attr('src');
					submitClick();
				});
			break;
			//last
			case 7:
				sound_player("Quad_dance_loop",1);
				var intervalid=setInterval(function() {
						sound_player("Quad_dance_loop",1);
				},16100);
				nav_button_controls(10000);
				$(".robot-head").attr('src',headStore).show(0);
				$(".robot-eyes").attr('src',eyesStore).show(0).addClass("danceeye");
				$(".robot-mouth").attr('src',mouthStore).show(0);
				$(".robot-neck").attr('src',neckStore).show(0);
				$(".robot-body").attr('src',bodyStore).show(0);
				$(".robot-hands-1").attr('src',handsStore).show(0).addClass("dancelefthand");
				$(".robot-hands-2").attr('src',handsStore).show(0).addClass("dancerighthand");
				$(".robot-legs-1").attr('src',legsStore).show(0).addClass("danceleftleg");
				$(".robot-legs-2").attr('src',legsStore).show(0).addClass("dancerightleg");

				break;
		}

		function clickAction(bodyPart, currentimg){
			var rbsrc = currentimg.find("img").attr('src');
			bodyPart.attr('src',rbsrc).show();
			$nextBtn.hide(0);
			$(".errormsg").hide(0);
			$(".sbmtbtn").removeClass("corsub incsub").addClass("smbthov");
			if(currentimg.find("img").hasClass("corquad")){
				quadFlag = true;
			}
			else{
				quadFlag = false;
			}
		}

		function submitClick(){
			if(quadFlag == true){
				sound_player("quad",false);
				$(".sbmtbtn").addClass("corsub").removeClass("smbthov");
				navigationcontroller();
			}
			else{
                sound_player("nonquad",false);
                $(".sbmtbtn").addClass("incsub").removeClass("smbthov");
				$(".errormsg").show(0);
			}
		}

		$(".quad-btn-down").click(function(){
			$(".quad-btn-down").prop('disabled', true);
			var selectQuadImg = $(".sliderimg:eq(0)");
			$(".quad-list-container").append(selectQuadImg.clone());
			selectQuadImg.animate({
				height: "0%",
				padding: "0%"
			},500,function(){
				$(".quad-btn-down").prop('disabled', false);
				selectQuadImg.remove();
			});
		});
		$(".quad-btn-up").click(function(){
			$(".quad-btn-up").prop('disabled', true);
			var selectQuadImg = $(".sliderimg").last();
			$(".quad-list-container").prepend(selectQuadImg.clone());
				selectQuadImg.remove();
			var selectQuadFirst = $(".sliderimg:eq(0)");
			selectQuadFirst.animate({
				height: "0%"
			},0,function(){
				selectQuadFirst.animate({
					height: "16.667%"
				},500, function(){
					$(".quad-btn-up").prop('disabled', false);
				});
			});
		});


	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls():'';
	});
}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('imageslider')){
			var imageslider = content[count].imageslider;

			for(var i=0; i<imageslider.length; i++){
				var image_src = preload.getResult(imageslider[i].imgid).src;
				//get list of classes
				var classes_list = imageslider[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}

	function put_image3(content, count){
		if(content[count].hasOwnProperty('robot')){
			var robot = content[count].robot;

			for(var i=0; i<robot.length; i++){
				var image_src = preload.getResult(robot[i].imgid).src;
				//get list of classes
				var classes_list = robot[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref+"/images/page3/";
var commonimgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/"+$lang+'/';
var dialog14= new buzz.sound(soundAsset+"s3_p1.ogg");
var dialog15= new buzz.sound(soundAsset+"s3_p1-2.ogg");
var dialog16 = new buzz.sound(soundAsset+"s3_p2.ogg");


// function objectifyActivityData($url) {
//     data = {};
//     var xmlLoad = new Event('xmlLoad');
//     $.ajax({
//         type: "GET",
//         url: $url,
//         dataType: "xml",
//         beforeSend: function() {
//             console.log("getting xml data from : " + $url);
//         },
//     }).done(function(xml) {

//         data.lesson = {};

//         $lesson = $(xml).find("lesson"); // data for lesson
//         /*
//          *the below function should be changed to recurion, for $%^#&*
//          */
//         $lesson.children().each(function(lessonData) {

//             if ($(this).children().length > 1) {
//                 data.lesson[this.nodeName] = {};
//                 $a = data.lesson[this.nodeName];
//                 $(this).children().each(function(data) {
//                     if ($(this).children().length > 1) {
//                         $a[this.nodeName] = {};
//                         var $b = $a[this.nodeName];

//                         $(this).children().each(function(count) {
//                             $b[count] = $(this).text();
//                         });
//                     } else {
//                         $a[this.nodeName] = $(this).text();
//                     }
//                 });
//             } else {
//                 data.lesson[this.nodeName] = $(this).text();
//             }
//         });

//         data.string = {};
//         $(xml).find("string").each(function(datas) {
//             $this = $(this).text();
//             $id = $(this).attr("id");
//             data.string[$id] = $this;
//         });
//         generateContent(data);
//         document.dispatchEvent(xmlLoad);
//     }).fail(function(jqxhr, textstatus, errortype) {
//         alert("get xml data from: " + $url + " failed, status: " + textstatus + ", error: " + errortype);
//         location.reload();
//     });
//     // return data; /*return the data object to be used in activity development*/
// }

// var content;

// function generateContent(data) {
    content = [
        // First Slide
        {
            hasheaderblock: false,
            additionalclasscontentblock: "page1-bg",
            uppertextblock: [
                {
                    textclass: 'headertext',
                    textdata: data.string.p2text19,
                    datahighlightflag: true,
					datahighlightcustomclass: 'highlighttext'
                }
            ],
            imageblock: [
                {
                    imagetoshow: [
                        {
                            imgsrc: imgpath + 'birdhosue02a.png',
                            imgclass: 'house-img-slide1',
                        }
                    ]
                }
            ],
        },
        {
            hasheaderblock: false,
            additionalclasscontentblock: "page1-bg",
            uppertextblock: [
                {
                    textclass: 'headertext',
                    textdata: data.string.p2text20,
                    datahighlightflag: true,
					datahighlightcustomclass: 'highlighttext'
                },
                {
                    textclass: 'headertext',
                    textdata: data.string.p2text21,
                    datahighlightflag: true,
					datahighlightcustomclass: 'highlighttext'
                }
            ],
            imageblock: [
                {
                    imagetoshow: [
                        {
                            imgsrc: imgpath + 'birdhosue02a.png',
                            imgclass: 'house-img-slide2 house-img-hover',
                        },
                        {
                            imgsrc: imgpath + 'birdhosue01a.png',
                            imgclass: 'house-img-slide2 house-img-hover',
                        },
                        {
                            imgsrc: imgpath + 'birdhosue03a.png',
                            imgclass: 'house-img-slide2 house-img-hover',
                        },
                        {
                            imgsrc: commonimgpath + 'wrong.png',
                            imgid: 'wrong-icon1'
                        },
                        {
                            imgsrc: commonimgpath + 'right.png',
                            imgid: 'right-icon'
                        },
                        {
                            imgsrc: commonimgpath + 'wrong.png',
                            imgid: 'wrong-icon2'
                        }
                    ]
                }
            ],
        }





    ];


// }



$(function() {
    // var height = $(window).height();
    // var width = $(window).width();
    // $("#board").css({"width": width, "height": (height*580/960)});
    // function recursion(){
    // if(data.string != null){
    // } else{
    // recursion();
    // }
    // }
    // recursion();

    // objectifyActivityData("data.xml");

    $(window).resize(function() {
        // recalculateHeightWidth();
        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

    });

    // function recalculateHeightWidth() {
    //     var heightresized = $(window).height();
    //     var widthresized = $(window).width();
    //     var factor = 960 / 580;
    //     var equivalentwidthtoheight = widthresized / factor;

    //     if (heightresized >= equivalentwidthtoheight) {
    //         $(".shapes_activity").css({
    //             "width": widthresized,
    //             "height": equivalentwidthtoheight
    //         });
    //     } else {
    //         $(".shapes_activity").css({
    //             "height": heightresized,
    //             "width": heightresized * 960 / 580
    //         });
    //     }
    //     // $(".shapes_activity").css({"left": "50%" ,
    //     // "height": "50%" ,
    //     // "-webkit-transform" : "translate(-50%, - 50%)",
    //     // "transform" : "translate(-50%, - 50%)"});
    // }

    var $board = $(".board");
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var total_page = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page,countNext+1);
    // recalculateHeightWidth();
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();

    /*
    	inorder to use the handlebar partials we need to register them
    	to their respective handlebar partial pointer first
    */
    Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    // Handlebars.registerPartial("tablecontent", $("#tablecontent-partial").html());


    // controls the navigational state of the program
    function navigationcontroller(islastpageflag)
    {
    // check if the parameter is defined and if a boolean,
    // update islastpageflag accordingly
    typeof islastpageflag === "undefined" ?
    islastpageflag = false :
    typeof islastpageflag != 'boolean'?
    alert("NavigationController : Hi Master, please provide a boolean parameter") :
    null;

    if(countNext == 0 && $total_page!=1){
      $nextBtn.show(0);
      $prevBtn.css('display', 'none');
    }
    else if($total_page == 1){
      $prevBtn.css('display', 'none');
      $nextBtn.css('display', 'none');

      // if lastpageflag is true
      islastpageflag ?
      ole.footerNotificationHandler.lessonEndSetNotification() :
      ole.footerNotificationHandler.lessonEndSetNotification() ;
    }
    else if(countNext > 0 && countNext < $total_page-1){
      $nextBtn.show(0);
      $prevBtn.show(0);
    }
    else if(countNext == $total_page-1){
      $nextBtn.css('display', 'none');
      $prevBtn.show(0);

      // if lastpageflag is true
      islastpageflag ?
      ole.footerNotificationHandler.lessonEndSetNotification() :
      ole.footerNotificationHandler.pageEndSetNotification() ;
    }
  }

    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);

        $board.html(html);
        loadTimelineProgress($total_page,countNext+1);
        // highlight any text inside board div with datahighlightflag set true
        vocabcontroller.findwords(countNext);
        texthighlight($board);
        $('.objecttext13').hide(0);
        $('.objecttext14').hide(0);
        $('.objecttext15').hide(0);

        switch(countNext) {
          case 0:
          soundplayer(dialog14);
          break;
           case 1:
                var correctImg = $('.house-img-slide2').eq(1);
                $('.house-img-slide2').on('click', function(e) {
                    var target = $(e.target);

                    if (target.is(correctImg)) {
                        target.addClass('house-img-slide2-correct');
                        $('#right-icon').show(0);
                        $('.house-img-slide2').unbind('click');
                        $('.house-img-slide2').removeClass('house-img-hover');
                        $prevBtn.show(0);
                        ole.footerNotificationHandler.pageEndSetNotification();
						play_correct_incorrect_sound(1);
                    } else if(target.is($('.house-img-slide2').eq(0))) {
                        target.addClass('house-img-slide2-incorrect');
                        $('#wrong-icon1').show(0);
						play_correct_incorrect_sound(0);
                    } else {
                        target.addClass('house-img-slide2-incorrect');
                        $('#wrong-icon2').show(0);
						play_correct_incorrect_sound(0);
                    }
                    target.removeClass("house-img-hover");
                });
                soundplayer(dialog16);

                break;
        }
        function soundplayer(i){
          buzz.all().stop();
          i.play().bind("ended",function(){
            if(countNext==1){
              $nextBtn.hide(0);
              $prevBtn.hide(0);
            }
            else{
              navigationcontroller();
            }
          });
        }
    }


    function templateCaller() {
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

        // navigationcontroller();

        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

    }

    $nextBtn.on("click", function() {
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
        countNext--;
        templateCaller();
    });

    // document.addEventListener('xmlLoad', function(e) {
        total_page = content.length;
        templateCaller();
    // });


});



/*===============================================
	 =            data highlight function            =
	 ===============================================*/
function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";


    if ($alltextpara.length > 0) {
        $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                (stylerulename = $(this).attr("data-highlightcustomclass")) :
                (stylerulename = "parsedstring");

            texthighlightstarttag = "<span>";


            replaceinstring = $(this).html();
            replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
            replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);


            $(this).html(replaceinstring);
        });
    }
}
/*=====  End of data highlight function  ======*/

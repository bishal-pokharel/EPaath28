var imgpath = $ref+"/images/page1/";
var soundAsset = $ref+"/sounds/"+$lang+'/';
var dialog0 = new buzz.sound(soundAsset+"angle.ogg");
var dialog1 = new buzz.sound(soundAsset+"big_angle.ogg");
var dialog2 = new buzz.sound(soundAsset+"s1_p1.ogg");
var dialog3 = new buzz.sound(soundAsset+"s1_p2.ogg");
var dialog4 = new buzz.sound(soundAsset+"s1_p3.ogg");
var dialog5 = new buzz.sound(soundAsset+"s1_p4.ogg");
var dialog6 = new buzz.sound(soundAsset+"s1_p5.ogg");
var dialog7 = new buzz.sound(soundAsset+"s1_p6.ogg");
var dialog8 = new buzz.sound(soundAsset+"s1_p7.ogg");
var dialog9 = new buzz.sound(soundAsset+"s1_p8.ogg");

var dialog25= new buzz.sound(soundAsset+"small_angle.ogg");
// function objectifyActivityData($url) {
//     data = {};
//     var xmlLoad = new Event('xmlLoad');
//     $.ajax({
//         type: "GET",
//         url: $url,
//         dataType: "xml",
//         beforeSend: function() {
//             console.log("getting xml data from : " + $url);
//         },
//     }).done(function(xml) {

//         data.lesson = {};

//         $lesson = $(xml).find("lesson"); // data for lesson
//         /*
//          *the below function should be changed to recurion, for $%^#&*
//          */
//         $lesson.children().each(function(lessonData) {

//             if ($(this).children().length > 1) {
//                 data.lesson[this.nodeName] = {};
//                 $a = data.lesson[this.nodeName];
//                 $(this).children().each(function(data) {
//                     if ($(this).children().length > 1) {
//                         $a[this.nodeName] = {};
//                         var $b = $a[this.nodeName];

//                         $(this).children().each(function(count) {
//                             $b[count] = $(this).text();
//                         });
//                     } else {
//                         $a[this.nodeName] = $(this).text();
//                     }
//                 });
//             } else {
//                 data.lesson[this.nodeName] = $(this).text();
//             }
//         });

//         data.string = {};
//         $(xml).find("string").each(function(datas) {
//             $this = $(this).text();
//             $id = $(this).attr("id");
//             data.string[$id] = $this;
//         });
//         generateContent(data);
//         document.dispatchEvent(xmlLoad);
//     }).fail(function(jqxhr, textstatus, errortype) {
//         alert("get xml data from: " + $url + " failed, status: " + textstatus + ", error: " + errortype);
//         location.reload();
//     });
//     // return data; /*return the data object to be used in activity development*/
// }

// var content;

// function generateContent(data) {
    content = [

        {
            hascanvasblock: false,
            hascanvasblock: false,

            additionalclasscontentblock: 'opening-bg',
            uppertextblock: [
                {
                    textdata: data.lesson.chapter,
                    textclass: 'chapter-title'
                }
            ]
        },

        //1st slide
        {
            hasheaderblock: false,
			hascanvasblock: true,

			canvasid: 'canvas-first',
            additionalclasscontentblock: "page1-bg",
            definitionblockadditionclass: "",
            definitionblock: [
				{
					definitiontext: data.string.p2text1,
					definitionclass: 'definition',
					datahighlightflag: true,
					datahighlightcustomclass: 'highlighttext'
				}
            ],
        },

		//2nd slide
        {
            hasheaderblock: false,
			hascanvasblock: true,

			canvasid: 'canvas-first',
            additionalclasscontentblock: "page1-bg",
            definitionblockadditionclass: "",
            definitionblock: [
				{
					definitiontext: data.string.p2text2,
					definitionclass: 'definition',
					datahighlightflag: true,
					datahighlightcustomclass: 'highlighttext'
				}
            ],
        },

		//3nd slide
        {
            hasheaderblock: false,
			hascanvasblock: true,

			canvasid: 'canvas-first',
            additionalclasscontentblock: "page1-bg",
            definitionblockadditionclass: "",

            definitionblock: [
				{
					definitiontext: data.string.p2text3,
					definitionclass: 'definition blue-text',
				},
				{
					definitiontext: data.string.p2text4,
					definitionclass: 'definition pink-text',
				},
            ],
        },
        // 4th slied
        {
            hasheaderblock: false,
			hascanvasblock: true,

			canvasid: 'canvas-first',
            additionalclasscontentblock: "page1-bg",
            definitionblock: [
				{
					definitiontext: data.string.p2text3,
					definitionclass: 'definition blue-text',
				},
				{
					definitiontext: data.string.p2text4,
					definitionclass: 'definition pink-text',
				},
                {
                    definitiontext: data.string.p2text5,
                    definitionclass: 'float-text-1'
                }
            ],
        },

       //5th slide
        {
            hasheaderblock: false,
			hascanvasblock: true,

			canvasid: 'canvas-first',
            additionalclasscontentblock: "page1-bg",
            definitionblockadditionclass: "",
			floatingblock: [
                {
                    floatingtext: data.string.p2text8,
                    floatingclass: 'float-text-2'
                }
            ],
            definitionblock: [
				{
					definitiontext: data.string.p2text6,
					definitionclass: 'definition blue-text',
				},
				{
					definitiontext: data.string.p2text7,
					definitionclass: 'definition pink-text',
				},
            ],
        },

         //6th slide
        {
            hasheaderblock: false,
			hascanvasblock: true,

			canvasid: 'canvas-first',
            additionalclasscontentblock: "page1-bg",
            definitionblockadditionclass: "",
			floatingblock: [
                {
                    floatingtext: data.string.p2text11,
                    floatingclass: 'float-text-3'
                }
            ],
            definitionblock: [
				{
					definitiontext: data.string.p2text9,
					definitionclass: 'definition blue-text',
				},
				{
					definitiontext: data.string.p2text10,
					definitionclass: 'definition pink-text',
				},
            ],
        },
        // 7th slide
        {
            hasheaderblock: false,
            additionalclasscontentblock: "page1-bg",
            imageblock: [
                {
                    imagetoshow: [
                        {
                            imgsrc: imgpath + 'angle.png',
                            imgclass: 'angle-img',
                            imagelabeldata: data.string.p2text12,
                            imagelabelclass: 'angle-label'
                        }
                    ]
                },
                {
                    imagetoshow: [
                        {
                            imgsrc: imgpath + 'big-angle.png',
                            imgclass: 'bigangle-img',
                            imagelabeldata: data.string.p2text8,
                            imagelabelclass: 'bigangle-label'
                        }
                    ]
                },
                {
                    imagetoshow: [
                        {
                            imgsrc: imgpath + 'small-angle.png',
                            imgclass: 'smallangle-img',
                            imagelabeldata: data.string.p2text11,
                            imagelabelclass: 'smallangle-label'
                        }
                    ]
                },

            ]
        }



    ];


// }



$(function() {
    // var height = $(window).height();
    // var width = $(window).width();
    // $("#board").css({"width": width, "height": (height*580/960)});
    // function recursion(){
    // if(data.string != null){
    // } else{
    // recursion();
    // }
    // }
    // recursion();

    // objectifyActivityData("data.xml");

    $(window).resize(function() {
        // recalculateHeightWidth();
        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

    });

    // function recalculateHeightWidth() {
    //     var heightresized = $(window).height();
    //     var widthresized = $(window).width();
    //     var factor = 960 / 580;
    //     var equivalentwidthtoheight = widthresized / factor;

    //     if (heightresized >= equivalentwidthtoheight) {
    //         $(".shapes_activity").css({
    //             "width": widthresized,
    //             "height": equivalentwidthtoheight
    //         });
    //     } else {
    //         $(".shapes_activity").css({
    //             "height": heightresized,
    //             "width": heightresized * 960 / 580
    //         });
    //     }
    //     // $(".shapes_activity").css({"left": "50%" ,
    //     // "height": "50%" ,
    //     // "-webkit-transform" : "translate(-50%, - 50%)",
    //     // "transform" : "translate(-50%, - 50%)"});
    // }

    var $board = $(".board");
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var total_page = 0;

    var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
    // recalculateHeightWidth();

    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    /*
    	inorder to use the handlebar partials we need to register them
    	to their respective handlebar partial pointer first
    */
    Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    // Handlebars.registerPartial("tablecontent", $("#tablecontent-partial").html());


    // controls the navigational state of the program
    function navigationcontroller(islastpageflag)
    {
    // check if the parameter is defined and if a boolean,
    // update islastpageflag accordingly
    typeof islastpageflag === "undefined" ?
    islastpageflag = false :
    typeof islastpageflag != 'boolean'?
    alert("NavigationController : Hi Master, please provide a boolean parameter") :
    null;

    if(countNext == 0 && $total_page!=1){
      $nextBtn.show(0);
      $prevBtn.css('display', 'none');
    }
    else if($total_page == 1){
      $prevBtn.css('display', 'none');
      $nextBtn.css('display', 'none');

      // if lastpageflag is true
      islastpageflag ?
      ole.footerNotificationHandler.lessonEndSetNotification() :
      ole.footerNotificationHandler.lessonEndSetNotification() ;
    }
    else if(countNext > 0 && countNext < $total_page-1){
      $nextBtn.show(0);
      $prevBtn.show(0);
    }
    else if(countNext == $total_page-1){
      $nextBtn.css('display', 'none');
      $prevBtn.show(0);

      // if lastpageflag is true
      islastpageflag ?
      ole.footerNotificationHandler.lessonEndSetNotification() :
      ole.footerNotificationHandler.pageEndSetNotification() ;
    }
  }


    // var canvasBlock;
    // var canvas;

    // function resizeCanvas() {

    // }

    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);

        loadTimelineProgress($total_page,countNext+1);
        $board.html(html);
        vocabcontroller.findwords(countNext);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);
        $('.objecttext13').hide(0);
        $('.objecttext14').hide(0);
        $('.objecttext15').hide(0);

		var canvasBlock = $('.canvasblock');
		var canvas = $('#canvas-first');

        // for last slide, canvas is not loaded so
        // validate canvas[0] object
        if (canvas[0]) {
            canvas[0].height = canvasBlock.height();
            canvas[0].width = canvasBlock.width();
            var ctx = canvas[0].getContext('2d');

            // For moving line
            var movingStart = {
                x: canvas.width()*0.3,
                y: canvas.height()*0.6
            };
            var movingEnd = {
                x: canvas.width()*0.7,
                y: canvas.height()*0.2
            };

            // For base line
            var baseStart = {
                x: canvas.width()*0.3,
                y: canvas.height()*0.8
            };
            var baseEnd = {
                x: canvas.width()*0.8,
                y: canvas.height()*0.8
            };

            var alpha = 0;
            var angle = 0;

            function drawBaseLine() {
                ctx.beginPath();
                ctx.moveTo(baseStart.x, baseStart.y);
                ctx.lineTo(baseEnd.x, baseEnd.y);
                ctx.lineWidth=3;
                ctx.strokeStyle="#000000";
                ctx.stroke();
            }

            function drawSecondLine() {
                ctx.beginPath();
                ctx.moveTo(movingStart.x, movingStart.y);
                ctx.lineTo(movingEnd.x, movingEnd.y);
                ctx.lineWidth=3;
                ctx.strokeStyle="#000000";
                ctx.stroke();
            }
        }


        switch (countNext) {
        case 0:
        soundplayer(dialog0);
        break;
        case 1:
				$('.definition').fadeIn(2000);
				drawBaseLine();
        soundplayer(dialog3);
				break;

			case 2:
				$('.definition').fadeIn(2000);
				drawBaseLine();
				drawSecondLine();
        soundplayer(dialog4);
				break;

			case 3:
				$('h2.definition').eq(0).fadeIn(2000);
				drawBaseLine();
				drawSecondLine();
                console.log('Initial y: ' + movingEnd.y);
				function joinLine() {
					// Clear canvas
					ctx.clearRect(0, 0, canvas.width(), canvas.height());

					drawBaseLine();

					ctx.beginPath();
					ctx.moveTo(movingStart.x, movingStart.y++);
					ctx.lineTo(movingEnd.x, movingEnd.y++);
                    ctx.lineWidth=3;
					ctx.stroke();

					if (movingStart.y <= baseStart.y) {
						window.requestAnimationFrame(joinLine);
					} else {
						$('h2.definition').eq(1).fadeIn(2000);
						// $('.float-text-1').fadeIn(2000);
                        // drawArc();
					}
				}

				joinLine();
                soundplayer(dialog5);
                break;

            case 4:
                $('h2.definition').eq(0).show(0);
                $('h2.definition').eq(1).show(0);

                 // draw arc
                angle = 0;
                function drawArc() {
                    // Clear canvas
					ctx.clearRect(0, 0, canvas.width(), canvas.height());

                    drawBaseLine();

                    // Draw second line
                    ctx.beginPath();
                    ctx.moveTo(movingStart.x, baseStart.y);
                    ctx.lineTo(movingEnd.x, (baseStart.y - movingStart.y)+movingEnd.y);
                    ctx.strokeStyle="#000000";
                    ctx.stroke();

                    ctx.beginPath();
                    ctx.arc(baseStart.x, baseStart.y, 70, 0, -angle * Math.PI / 180, true);
                    ctx.strokeStyle="#EC252D";
                    ctx.stroke();
                    angle++;
                    if (angle <= 31) {
                        window.requestAnimationFrame(drawArc);
                    }
                }
                drawArc();
                $('.float-text-1').fadeIn(2000);
                soundplayer(dialog6);
                break;
            case 5:
                angle = 31;
                $('h2.definition').eq(0).fadeIn(2000);
                setTimeout(function(){
                 $('h2.definition').eq(1).fadeIn(3500);
               },1000);
                function rotateLineAntiClock() {
                    ctx.clearRect(0, 0, canvas.width(), canvas.height());

                    // Draw lines
                    drawBaseLine();
                    ctx.beginPath();
                    ctx.arc(baseStart.x, baseStart.y, 70, 0, -angle * Math.PI / 180, true);
                    ctx.strokeStyle="#EC252D";
                    ctx.stroke();

                    // Draw line according to angle
                    ctx.save();
                    ctx.beginPath();
                    ctx.translate(baseStart.x, baseStart.y);
                    ctx.rotate(-(angle-30) * Math.PI / 180);
                    ctx.moveTo(0,0);
                    ctx.lineTo(movingEnd.x-baseStart.x, movingEnd.y-baseStart.y+(baseStart.y-movingStart.y));
                    ctx.strokeStyle="#000000";
                    ctx.stroke();
                    ctx.restore();

                    angle += 0.5;
                    if (angle <= 110) {
                        window.requestAnimationFrame(rotateLineAntiClock);
                    } else {
                        $('.float-text-2').eq(0).fadeIn(2000);
                    }
                }

                rotateLineAntiClock();
                soundplayer(dialog7);

                break;

            case 6:
                angle = 110;
                $('h2.definition').eq(0).fadeIn(2000);
                setTimeout(function(){
                  $('h2.definition').eq(1).fadeIn(3500);
                },1000);
                movingStart = {
                    x: canvas.width()*0.16,
                    y: canvas.height()*0.10
                }

                function rotateLineClock() {
                    ctx.clearRect(0, 0, canvas.width(), canvas.height());

                    drawBaseLine();

                    // Draw arc
                    ctx.beginPath();
                    ctx.arc(baseStart.x, baseStart.y, 70, 0, -angle * Math.PI / 180, true);
                    ctx.strokeStyle="#EC252D";
                    ctx.stroke();

                    // Rotate the context and draw line
                    ctx.save();
                    ctx.beginPath();
                    ctx.translate(baseStart.x, baseStart.y);
                    ctx.rotate(-(angle-110) * Math.PI / 180);
                    ctx.moveTo(0, 0);
                    ctx.lineTo(movingStart.x-baseStart.x, movingStart.y-baseStart.y);
                    ctx.strokeStyle="#000000";
                    ctx.stroke();
                    ctx.restore();

                    angle -= 0.5;

                    if (angle >= 25) {
                        window.requestAnimationFrame(rotateLineClock);
                    } else {
                        $('.float-text-3').eq(0).fadeIn(2000);
                    }
                }
                rotateLineClock();
                soundplayer(dialog8);
                break;

            case 7:
                     soundplayer(dialog9);
                     $(".image-item:eq(0)").fadeIn(2000);
                        $(".image-item:eq(1)").delay(1000).show(0);
                                $(".image-item:eq(2)").delay(3000).show(0);
                                break;
        }
    }

    function soundplayer(i){
      buzz.all().stop();
      i.play().bind("ended",function(){
          navigationcontroller();
      });
    }

    function templateCaller() {
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

      //  navigationController();

        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


    }

    $nextBtn.on("click", function() {
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
        countNext--;
        templateCaller();
    });

    // document.addEventListener('xmlLoad', function(e) {
        total_page = content.length;
        templateCaller();
    // });


});



/*===============================================
	 =            data highlight function            =
	 ===============================================*/
function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";


    if ($alltextpara.length > 0) {
        $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                (stylerulename = $(this).attr("data-highlightcustomclass")) :
                (stylerulename = "parsedstring");

            texthighlightstarttag = "<span>";


            replaceinstring = $(this).html();
            replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
            replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);


            $(this).html(replaceinstring);
        });
    }
}
/*=====  End of data highlight function  ======*/

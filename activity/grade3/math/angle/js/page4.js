var imgpath = $ref+"/images/page4/";
var soundAsset = $ref+"/sounds/"+$lang+'/';

var dialog17= new buzz.sound(soundAsset+"s4_p1.ogg");
var dialog18= new buzz.sound(soundAsset+"s4_p2.ogg");
var dialog19= new buzz.sound(soundAsset+"s4_p3.ogg");
var dialog20 = new buzz.sound(soundAsset+"s4_p4.ogg");
var dialog21= new buzz.sound(soundAsset+"s4_p5.ogg");
var dialog22= new buzz.sound(soundAsset+"s4_p6.ogg");
var dialog23= new buzz.sound(soundAsset+"s4_p7.ogg");
var dialog24= new buzz.sound(soundAsset+"s4_p8.ogg");

// function objectifyActivityData($url) {
//     data = {};
//     var xmlLoad = new Event('xmlLoad');
//     $.ajax({
//         type: "GET",
//         url: $url,
//         dataType: "xml",
//         beforeSend: function() {
//             console.log("getting xml data from : " + $url);
//         },
//     }).done(function(xml) {

//         data.lesson = {};

//         $lesson = $(xml).find("lesson"); // data for lesson
//         /*
//          *the below function should be changed to recurion, for $%^#&*
//          */
//         $lesson.children().each(function(lessonData) {

//             if ($(this).children().length > 1) {
//                 data.lesson[this.nodeName] = {};
//                 $a = data.lesson[this.nodeName];
//                 $(this).children().each(function(data) {
//                     if ($(this).children().length > 1) {
//                         $a[this.nodeName] = {};
//                         var $b = $a[this.nodeName];

//                         $(this).children().each(function(count) {
//                             $b[count] = $(this).text();
//                         });
//                     } else {
//                         $a[this.nodeName] = $(this).text();
//                     }
//                 });
//             } else {
//                 data.lesson[this.nodeName] = $(this).text();
//             }
//         });

//         data.string = {};
//         $(xml).find("string").each(function(datas) {
//             $this = $(this).text();
//             $id = $(this).attr("id");
//             data.string[$id] = $this;
//         });
//         generateContent(data);
//         document.dispatchEvent(xmlLoad);
//     }).fail(function(jqxhr, textstatus, errortype) {
//         alert("get xml data from: " + $url + " failed, status: " + textstatus + ", error: " + errortype);
//         location.reload();
//     });
//     // return data; /*return the data object to be used in activity development*/
// }

// var content;

// function generateContent(data) {
    content = [
		// first slide
        // Second Slide
        {
            hasheaderblock: false,
            additionalclasscontentblock: "page1-bg",
              newdef:"texthiddendiv",
              definitionblock: [
                  {
                      textclass: 'newtext1',
                      textdata: data.string.p2text22,
                  },
                  {
                      textclass: 'newtext2',
                      textdata: data.string.p2text23,
                  },{
                    textclass: 'newtext3',
                    textdata: data.string.p2text24,
                  }
              ]
        },
        // Third Slide
        {
            hasheaderblock: true,
            additionalclasscontentblock: "page1-bg",
            headerblock: [
                {
                    textclass: 'headertext',
                    textdata: data.string.p2text25
                }
            ],
            imageblock: [
                {
                    imagetoshow: [
                        {
                            divclass: 'imgdiv',
                            imgsrc: imgpath + 'angle_abc.png',
                            imgclass: 'angle-img-slide2 non-dotted-angle',
                        },
                        {
                            divclass: 'imgdiv',
                            imgsrc: imgpath + 'angle_c.png',
                            imgclass: 'angle-img-slide2 dotted-angle',
                        }
                    ]
                }
            ],
        },
        // Fourth Slide
        {
            hasheaderblock: true,
            additionalclasscontentblock: "page1-bg",

            headerblock: [
                {
                    textclass: 'headertext',
                    textdata: data.string.p2text26
                }
            ],
            imageblock: [
                {
                    imagetoshow: [
                        {
                            divclass: 'imgdiv',
                            imglabel: [
                                {
                                    labelclass: 'label-A',
                                    labeldata: 'A'
                                },
                                {
                                    labelclass: 'label-B',
                                    labeldata: 'B'
                                },
                                {
                                    labelclass: 'label-C',
                                    labeldata: 'C'
                                },
                            ],
                            imgsrc: imgpath + 'angle_abc.png',
                            imgclass: 'angle-img-slide2',
                        },
                    ]
                }
            ],
        },
        // Fifth Slide
        {
            hasheaderblock: true,
            additionalclasscontentblock: "page1-bg",
            anglesymbol: [
                {
                    anglesymbol: '&#8736;',
                    anglesymbolclass: 'anglesymbol'
                }
            ],
            definitionblock: [
                {
                    textclass: 'definition',
                    textdata: data.string.p2text27,
                }
            ],
            headerblock: [
                {
                    textclass: 'headertext',
                    textdata: data.string.p2text27_0,
                }
            ],
            imageblock: [
                {
                    imagetoshow: [
                        {
                            divclass: 'imgdiv',
                            imglabel: [
                                {
                                    labelclass: 'label-A',
                                    labeldata: 'A'
                                },
                                {
                                    labelclass: 'label-B',
                                    labeldata: 'B'
                                },
                                {
                                    labelclass: 'label-C',
                                    labeldata: 'C'
                                },
                            ],
                            imgsrc: imgpath + 'angle_abc.png',
                            imgclass: 'angle-img-slide2',
                        },
                    ]
                }
            ],
        },
        // Sixth slide
        {
            hasheaderblock: true,
            additionalclasscontentblock: "page1-bg",
            anglesymbol: [
                {
                    anglesymbol: '&#8736;',
                    anglesymbolclass: 'anglesymbol'
                },
                {
                    anglesymbol: 'A',
                    anglesymbolclass: 'anglesymbol anglesymbol2 anglesymbol-highlight'
                },
            ],
            definitionblock: [
                {
                    textclass: 'definition',
                    textdata: data.string.p2text27,
                },
                {
                    textclass: 'definition',
                    textdata: data.string.p2text28,
                }
            ],
            headerblock: [
                {
                    textclass: 'headertext',
                    textdata: data.string.p2text27_0,
                }
            ],
            imageblock: [
                {
                    imagetoshow: [
                        {
                            divclass: 'imgdiv',
                            imglabel: [
                                {
                                    labelclass: 'label-A',
                                    labeldata: 'A'
                                },
                                {
                                    labelclass: 'label-B',
                                    labeldata: 'B'
                                },
                                {
                                    labelclass: 'label-C',
                                    labeldata: 'C'
                                },
                            ],
                            imgsrc: imgpath + 'angle_a.png',
                            imgclass: 'angle-img-slide2',
                        },
                    ]
                }
            ],
        },
        // Seventh Slide
        {
            hasheaderblock: true,
            additionalclasscontentblock: "page1-bg",
            anglesymbol: [
                {
                    anglesymbol: '&#8736;',
                    anglesymbolclass: 'anglesymbol'
                },
                {
                    anglesymbol: 'A',
                    anglesymbolclass: 'anglesymbol anglesymbol2'
                },
                {
                    anglesymbol: 'B',
                    anglesymbolclass: 'anglesymbol anglesymbol3 anglesymbol-highlight'
                },
            ],
            definitionblock: [
                {
                    textclass: 'definition',
                    textdata: data.string.p2text27,
                },
                {
                    textclass: 'definition',
                    textdata: data.string.p2text28,
                },
                {
                    textclass: 'definition',
                    textdata: data.string.p2text29,
                }
            ],
            headerblock: [
                {
                    textclass: 'headertext',
                    textdata: data.string.p2text27_0,
                }
            ],
            imageblock: [
                {
                    imagetoshow: [
                        {
                            divclass: 'imgdiv',
                            imglabel: [
                                {
                                    labelclass: 'label-A',
                                    labeldata: 'A'
                                },
                                {
                                    labelclass: 'label-B',
                                    labeldata: 'B'
                                },
                                {
                                    labelclass: 'label-C',
                                    labeldata: 'C'
                                },
                            ],
                            imgsrc: imgpath + 'angle_b.png',
                            imgclass: 'angle-img-slide2',
                        },
                    ]
                }
            ],
        },
         // Eighth Slide
        {
            hasheaderblock: true,
            additionalclasscontentblock: "page1-bg",
            anglesymbol: [
                {
                    anglesymbol: '&#8736;',
                    anglesymbolclass: 'anglesymbol'
                },
                {
                    anglesymbol: 'A',
                    anglesymbolclass: 'anglesymbol anglesymbol2'
                },
                {
                    anglesymbol: 'B',
                    anglesymbolclass: 'anglesymbol anglesymbol3'
                },
                {
                    anglesymbol: 'C',
                    anglesymbolclass: 'anglesymbol anglesymbol4 anglesymbol-highlight'
                },
            ],
            definitionblock: [
                {
                    textclass: 'definition',
                    textdata: data.string.p2text27,
                },
                {
                    textclass: 'definition',
                    textdata: data.string.p2text28,
                },
                {
                    textclass: 'definition',
                    textdata: data.string.p2text29,
                },
                {
                    textclass: 'definition',
                    textdata: data.string.p2text30,
                }
            ],
            headerblock: [
                {
                    textclass: 'headertext',
                    textdata: data.string.p2text27_0,
                }
            ],
            imageblock: [
                {
                    imagetoshow: [
                        {
                            divclass: 'imgdiv',
                            imglabel: [
                                {
                                    labelclass: 'label-A',
                                    labeldata: 'A'
                                },
                                {
                                    labelclass: 'label-B',
                                    labeldata: 'B'
                                },
                                {
                                    labelclass: 'label-C',
                                    labeldata: 'C'
                                },
                            ],
                            imgsrc: imgpath + 'angle_c.png',
                            imgclass: 'angle-img-slide2',
                        },
                    ]
                }
            ],
        },
        // Ninth Slide
         {
            hasheaderblock: true,
            additionalclasscontentblock: "page1-bg",
            headerblock: [
                {
                    textclass: 'headertext',
                    textdata: data.string.p2text31
                }
            ],
            draggable: [
                {
                    draggableclass: 'draggable-angle abc',
                    draggabletext: '&#8736;ABC'
                },
                {
                    draggableclass: 'draggable-angle mno',
                    draggabletext: '&#8736;MNO'
                },
                {
                    draggableclass: 'draggable-angle efg',
                    draggabletext: '&#8736;EFG'
                }
            ],
            imageblock: [
                {
                    imagetoshow: [
                        {
                            divclass: 'angle-slide9 angle-mno',
                            imgsrc: imgpath + 'angle_c.png',
                            imglabel: [
                                {
                                    labelclass: 'label-A',
                                    labeldata: 'M'
                                },
                                {
                                    labelclass: 'label-B',
                                    labeldata: 'N'
                                },
                                {
                                    labelclass: 'label-C',
                                    labeldata: 'O'
                                },
                            ],
                        },
                        {
                            divclass: 'angle-slide9 angle-abc',
                            imgsrc: imgpath + 'angle_c.png',
                            imglabel: [
                                {
                                    labelclass: 'label-A',
                                    labeldata: 'A'
                                },
                                {
                                    labelclass: 'label-B',
                                    labeldata: 'B'
                                },
                                {
                                    labelclass: 'label-C',
                                    labeldata: 'C'
                                },
                            ],
                        },
                        {
                            divclass: 'angle-slide9 angle-efg',
                            imgsrc: imgpath + 'angle_c.png',
                            imglabel: [
                                {
                                    labelclass: 'label-E',
                                    labeldata: 'E'
                                },
                                {
                                    labelclass: 'label-B',
                                    labeldata: 'G'
                                },
                                {
                                    labelclass: 'label-C',
                                    labeldata: 'F'
                                },
                            ],
                        }
                    ]
                }
            ]
         }

    ];


// }



$(function() {
    // var height = $(window).height();
    // var width = $(window).width();
    // $("#board").css({"width": width, "height": (height*580/960)});
    // function recursion(){
    // if(data.string != null){
    // } else{
    // recursion();
    // }
    // }
    // recursion();

    // objectifyActivityData("data.xml");

    $(window).resize(function() {
        // recalculateHeightWidth();
        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

    });

    function recalculateHeightWidth() {
        var heightresized = $(window).height();
        var widthresized = $(window).width();
        var factor = 960 / 580;
        var equivalentwidthtoheight = widthresized / factor;

        if (heightresized >= equivalentwidthtoheight) {
            $(".shapes_activity").css({
                "width": widthresized,
                "height": equivalentwidthtoheight
            });
        } else {
            $(".shapes_activity").css({
                "height": heightresized,
                "width": heightresized * 960 / 580
            });
        }
        // $(".shapes_activity").css({"left": "50%" ,
        // "height": "50%" ,
        // "-webkit-transform" : "translate(-50%, - 50%)",
        // "transform" : "translate(-50%, - 50%)"});
    }

    var $board = $(".board");
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var total_page = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page,countNext+1);
    // recalculateHeightWidth();
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();

    /*
    	inorder to use the handlebar partials we need to register them
    	to their respective handlebar partial pointer first
    */
    Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    // Handlebars.registerPartial("tablecontent", $("#tablecontent-partial").html());


    // controls the navigational state of the program
    function navigationcontroller(islastpageflag)
    {
    // check if the parameter is defined and if a boolean,
    // update islastpageflag accordingly
    typeof islastpageflag === "undefined" ?
    islastpageflag = false :
    typeof islastpageflag != 'boolean'?
    alert("NavigationController : Hi Master, please provide a boolean parameter") :
    null;

    if(countNext == 0 && $total_page!=1){
      $nextBtn.show(0);
      $prevBtn.css('display', 'none');
    }
    else if($total_page == 1){
      $prevBtn.css('display', 'none');
      $nextBtn.css('display', 'none');

      // if lastpageflag is true
      islastpageflag ?
      ole.footerNotificationHandler.lessonEndSetNotification() :
      ole.footerNotificationHandler.lessonEndSetNotification() ;
    }
    else if(countNext > 0 && countNext < $total_page-1){
      $nextBtn.show(0);
      $prevBtn.show(0);
    }
    else if(countNext == $total_page-1){
      $nextBtn.css('display', 'none');
      $prevBtn.show(0);

      // if lastpageflag is true
      islastpageflag ?
      ole.footerNotificationHandler.lessonEndSetNotification() :
      ole.footerNotificationHandler.pageEndSetNotification() ;
    }
  }

    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);

        loadTimelineProgress($total_page,countNext+1);
        $board.html(html);
        vocabcontroller.findwords(countNext);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);
        $('.objecttext13').hide(0);
        $('.objecttext14').hide(0);
        $('.objecttext15').hide(0);
        switch(countNext) {
            case 0:
            $(".newtext2").delay(2500).show(0);
            $(".newtext3").delay(5500).show(0);

                soundplayer(dialog17);
                break;
            case 1:
            	function fade_in_out(){
            		$('.dotted-angle').fadeIn(1000);
            		$('.non-dotted-angle').fadeOut(1000, function(){
            			$('.dotted-angle').fadeOut(1000);
            			$('.non-dotted-angle').fadeIn(1000, fade_in_out);
            		});
            	}
            	fade_in_out();
              soundplayer(dialog18);
            	break;
              case 2:
              soundplayer(dialog19);
              break;
              case 3:
              soundplayer(dialog20);
              break;
              case 4:
              soundplayer(dialog21);
              break;
              case 5:
              soundplayer(dialog22);
              break;
              case 6:
              soundplayer(dialog23);
              break;
            case 7:
              var corPath = imgpath+"correct.png";
              var incorPath = imgpath+"wrongicon.png";
                function initDragAndDrop() {
                    $('.draggable-angle').draggable({
                        appendTo: 'body',
                        containment: 'body',
                        revert: true,
                        zIndex: 100,
                        revert: true
                    });

                    var draggables = $('.draggable-angle');
                    $('.angle-mno').droppable({
                        accept: draggables,
                        hoverClass: 'droppable-hover',
                        drop: handleDrop
                    });

                    $('.angle-abc').droppable({
                        accept: draggables,
                        hoverClass: 'droppable-hover',
                        drop: handleDrop
                    });

                    $('.angle-efg').droppable({
                        accept: draggables,
                        hoverClass: 'droppable-hover',
                        drop: handleDrop,
                    });

                    var totalCorrect = 0;

                    function handleDrop(event, ui) {
                        var dropped = ui.draggable;

                        if ($(this).hasClass('angle-mno') && dropped.hasClass('mno')
                            || ($(this).hasClass('angle-abc') && dropped.hasClass('abc')
                            || ($(this).hasClass('angle-efg') && dropped.hasClass('efg')))) {
                                dropped.draggable('disable');

                                $(this).droppable({hoverClass: null});
                                // $(".corincor").hide(0);
                                // $(this).append("<img class='corincor' src='"+corPath+"' />");
                                $(this).append(dropped.detach().css({
                                    position: 'absolute',
                                    transform: 'scale(0.6, 0.6)',
                                    marginTop: '0%',
                                    marginLeft: "8%",
                                    left: 0,
                                    top: 0
                                }));
                                totalCorrect++;
                                play_correct_incorrect_sound(1);
                        }

                         if (totalCorrect >= 3) {
                           $prevBtn.show(0);
                             ole.footerNotificationHandler.lessonEndSetNotification();
                         }

                    }
                }
                initDragAndDrop();
                soundplayer(dialog24);
        }
        function soundplayer(i){
          buzz.all().stop();
          i.play().bind("ended",function(){
            if(countNext==7){
              $nextBtn.hide(0);
              $prevBtn.hide(0);
            }else {
                navigationcontroller();
            }

          });
        }

    }

    function templateCaller() {
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

      //  navigationcontroller();

        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

    }

    $nextBtn.on("click", function() {
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
        countNext--;
        templateCaller();
    });

    // document.addEventListener('xmlLoad', function(e) {
        total_page = content.length;
        templateCaller();
    // });


});



/*===============================================
	 =            data highlight function            =
	 ===============================================*/
function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";


    if ($alltextpara.length > 0) {
        $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                (stylerulename = $(this).attr("data-highlightcustomclass")) :
                (stylerulename = "parsedstring");

            texthighlightstarttag = "<span>";


            replaceinstring = $(this).html();
            replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
            replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);


            $(this).html(replaceinstring);
        });
    }
}
/*=====  End of data highlight function  ======*/

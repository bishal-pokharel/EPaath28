Array.prototype.shufflearray = function(){
	var i = this.length, j, temp;
	while(--i > 0){
		j = Math.floor(Math.random() * (i+1));
		temp = this[j];
		this[j] = this[i];
		this[i] = temp;
	}
	return this;
}

var imgpath = $ref+"/exercise/images/";

var content=[
	//slide 1
	{
		exerciseblock: [
		{
			textdata: data.string.ques1,

			imageoptions: [
			{
				forshuffle: "correctItem",
				imgsrc: imgpath + "ans02.png"
			},
			{
				forshuffle: "wrongItem",
				imgsrc: imgpath + "ans01.png"
			},
			{
				forshuffle: "wrongItem2",
				imgsrc: imgpath + "ans03.png"
			},

			]
		}
		]

	},

	//slide 2
	{
		exerciseblock: [
		{
			textdata: data.string.ques1,

			imageoptions: [
			{
				forshuffle: "correctItem",
				imgsrc: imgpath + "ans08.png"
			},
			{
				forshuffle: "wrongItem",
				imgsrc: imgpath + "ans06.png"
			},
			{
				forshuffle: "wrongItem2",
				imgsrc: imgpath + "ans01.png"
			}

			]
		}
		]

	},

	//slide 3
	{
		exerciseblock: [
		{
			textdata: data.string.ques1,

			imageoptions: [
			{
				forshuffle: "correctItem",
				imgsrc: imgpath + "ans05.png"
			},
			{
				forshuffle: "wrongItem",
				imgsrc: imgpath + "ans03.png"
			},
			{
				forshuffle: "wrongItem2",
				imgsrc: imgpath + "ans07.png"
			},

			]
		}
		]

	},

	//slide 4
	{
		exerciseblock: [
		{
			textdata: data.string.ques1,

			imageoptions: [
			{
				forshuffle: "correctItem",
				imgsrc: imgpath + "ans06.png"
			},
			{
				forshuffle: "wrongItem",
				imgsrc: imgpath + "ans04.png"
			},
			{
				forshuffle: "wrongItem2",
				imgsrc: imgpath + "ans07.png"
			},

			]
		}
		]

	},

	//slide 5
	{
		exerciseblock: [
		{
			textdata: data.string.ques1,

			imageoptions: [
			{
				forshuffle: "correctItem",
				imgsrc: imgpath + "ans08.png"
			},
			{
				forshuffle: "wrongItem",
				imgsrc: imgpath + "ans03.png"
			},
			{
				forshuffle: "wrongItem2",
				imgsrc: imgpath + "ans04.png"
			},

			]
		}
		]

	},
	//slide 6
	{
		exerciseblock: [
		{
			textdata: data.string.ques2,

			imageoptions: [
			{
				forshuffle: "correctItem",
				imgsrc: imgpath + "ans04.png"
			},
			{
				forshuffle: "wrongItem",
				imgsrc: imgpath + "ans06.png"
			},
			{
				forshuffle: "wrongItem2",
				imgsrc: imgpath + "ans08.png"
			},

			]
		}
		]

	},
	//slide 7
	{
		exerciseblock: [
		{
			textdata: data.string.ques2,

			imageoptions: [
			{
				forshuffle: "correctItem",
				imgsrc: imgpath + "ans01.png"
			},
			{
				forshuffle: "wrongItem",
				imgsrc: imgpath + "ans02.png"
			},
			{
				forshuffle: "wrongItem2",
				imgsrc: imgpath + "ans03.png"
			},

			]
		}
		]

	},
	//slide 8
	{
		exerciseblock: [
		{
			textdata: data.string.ques2,

			imageoptions: [
			{
				forshuffle: "correctItem",
				imgsrc: imgpath + "ans07.png"
			},
			{
				forshuffle: "wrongItem",
				imgsrc: imgpath + "ans08.png"
			},
			{
				forshuffle: "wrongItem2",
				imgsrc: imgpath + "ans05.png"
			},

			]
		}
		]

	},
	//slide 9
	{
		exerciseblock: [
		{
			textdata: data.string.ques2,

			imageoptions: [
			{
				forshuffle: "correctItem",
				imgsrc: imgpath + "ans06.png"
			},
			{
				forshuffle: "wrongItem",
				imgsrc: imgpath + "ans08.png"
			},
			{
				forshuffle: "wrongItem2",
				imgsrc: imgpath + "ans02.png"
			},
			]
		}
		]

	},
	//slide 10
	{
		exerciseblock: [
		{
			textdata: data.string.ques2,

			imageoptions: [
			{
				forshuffle: "correctItem",
				imgsrc: imgpath + "ans01.png"
			},
			{
				forshuffle: "wrongItem",
				imgsrc: imgpath + "ans07.png"
			},
			{
				forshuffle: "wrongItem2",
				imgsrc: imgpath + "ans08.png"
			},

			]
		}
		]

	},


];


/*remove this for non random questions*/
/*content.shufflearray();*/


$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	/*for limiting the questions to 10*/
	var $total_page = 10;



	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
 	}

	 /*values in this array is same as the name of images of eggs in image folder*/

	 //create eggs
	 var testin = new EggTemplate();

	 	//eggTemplate.eggMove(countNext);
 		testin.init(10);

	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);
	 	var testcount = 0;

	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);
	 	$('.congratulation').hide(0);
	 	$('.exefin').hide(0);

	 	/*generate question no at the beginning of question*/
	 	testin.numberOfQuestions();

	 	/*for randomizing the options*/
	 	var parent = $(".optionsdiv");
	 	var divs = parent.children();
	 	while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
	 	}


	 	/*======= SCOREBOARD SECTION ==============*/
	 	/*random scoreboard eggs*/
	 	//var i = Math.floor(Math.random() * imageArray.length);
	 	//var randImg = imageArray[i];
	 	var ansClicked = false;
	 	var wrngClicked = false;

	 	$(".correctItem").click(function(){
	 		testin.update(true);
	 		$(this).addClass('rightans selected');
	 		$(".wrongItem").css('pointer-events', 'none');
	 		$(".wrongItem2").css('pointer-events', 'none');
	 		$nextBtn.show(0);
			play_correct_incorrect_sound(1);
			$(this).siblings(".corctopt").show(0);
	 	});

	 	$(".wrongItem").click(function(){
	 		testin.update(false);
	 		$(this).addClass('wrongans selected');
			play_correct_incorrect_sound(0);
			$(this).siblings(".wrngopt").show(0);
	 	});

	 	$(".wrongItem2").click(function(){
	 		testin.update(false);
	 		$(this).addClass('wrongans selected');
			play_correct_incorrect_sound(0);
			$(this).siblings(".wrngopt").show(0);
	 	});

	 	/*======= SCOREBOARD SECTION ==============*/
	 }

	 function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
		testin.gotoNext();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

var soundAsset = $ref+"/sounds/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[
	// slide1
	{
		contentblockadditionalclass:'redBg',
		imageblock:[{
			imagestoshow:[
			{
				imgclass: 'suraj fstsld',
				imgid : 'suraj1',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.p2s1,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'text_box',
			imgsrc: '',
		}]
	},
	// slide2
	{
		contentblockadditionalclass:"creamBg",
		uppertextblock:[{
			textclass : 'topTxt',
			textdata: data.string.p2s2
		}],
		extratextblock:[{
			textclass:"btmTxt",
			textdata: data.string.p2s2_btm
		}],
		imgcount:[
			{
				imgcountclass:"CountContainer leftCnt"
			},
			{
				imgcountclass:"CountContainer topCnt"
			},
			{
				imgcountclass:"CountContainer answer"
			}
		],
		imgcontainer:[
			{
				img_containerclass:"marblesContainerFirst",
			}]
	},
	// slide3
	{
		contentblockadditionalclass:"creamBg",
		uppertextblock:[{
			textclass : 'topTxt',
			textdata: data.string.p2s2
		}],
		extratextblock:[
			{
				textclass:"btmTxt smaller",
				textdata: data.string.p2s2_btm
			},{
				textclass:"num topNum",
				textdata: data.string.twl
			},{
				textclass:"num lftTp",
				textdata: data.string.fv
			},{
				textclass:"num lftbtm",
				textdata: data.string.tw
			}
		],
		imgcontainer:[
			{
				img_containerclass:"marblesContainer FirstS3",
				box:[
					{
					boxclass:"mbl_box bx1"
					},
					{
					boxclass:"mbl_box bx2"
				}]
		}],
		sidetxtblock:[{
			sidetxt:[{
				datahighlightflag:"true",
				datahighlightcustomclass:'oneByOne',
				textclass:"sidetxt",
				textdata: data.string.p2s3txt
			}]
		}]
	},
	// slide4
	{
		contentblockadditionalclass:"creamBg",
		uppertextblock:[{
			textclass : 'topTxt',
			textdata: data.string.p2s2
		}],
		extratextblock:[
			{
				textclass:"num topNum",
				textdata: data.string.twl
			},{
				textclass:"num lftTp",
				textdata: data.string.fv
			},{
				textclass:"num lftbtm",
				textdata: data.string.tw
			}
		],
		imgcontainer:[
			{
				img_containerclass:"marblesContainer FirstS3",
				box:[
					{
					boxclass:"mbl_box bx1"
				},
				{
					boxclass:"mbl_box bx2"
				}
				]
		}],
		sidetxtblock:[{
			sidetxt:[{
				datahighlightflag:"true",
				datahighlightcustomclass:'oneByOne',
				textclass:"s4side",
				textdata: data.string.p2s4txt
			}]
		}]
	},
	// slide5
	{
		contentblockadditionalclass:'redBg',
		imageblock:[{
			imagestoshow:[
			{
				imgclass: 'suraj fstsld',
				imgid : 'suraj1',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.p2s1,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'text_box',
			imgsrc: '',
		}]
	},
	// slide6
	{
		contentblockadditionalclass:"creamBg",
		uppertextblock:[{
			textclass : 'topTxt',
			textdata: data.string.p2s6
		}],
		imgcount:[
			{
				imgcountclass:"CountContainer leftCntS6"
			},
			{
				imgcountclass:"CountContainer topCnt"
			}
		],
		imgcontainer:[
			{
				img_containerclass:"marblesContainerS6",
			}]
	},
	// slide7
	{
		contentblockadditionalclass:"creamBg",
		uppertextblock:[{
			textclass : 'topTxt',
			textdata: data.string.p2s6
		}],
		imgcontainer:[
			{
				img_containerclass:"marblesContainerS6 mblcntrlft",
				box:[
					{
						boxclass:"mbl_box S6bx1",
						boxtxt:[{
							textclass:"boxtxt fstRght",
							textdata:data.string.ten
						},{
							textclass:"boxtxt fstTop",
							textdata:data.string.ten
						}]
					},
					{
						boxclass:"mbl_box S6bx2",
						boxtxt:[{
							textclass:"boxtxt secTop",
							textdata:data.string.four
						}]
					},
					{
						boxclass:"mbl_box S6bx3",
						boxtxt:[{
							textclass:"boxtxt thirdRight",
							textdata:data.string.two
						}]
					},
					{
						boxclass:"mbl_box S6bx4"
				}]
		}],
			imageblock:[{
				imagestoshow:[
				{
					imgclass: 'suraj srjs7 inverted',
					imgid : 'niti',
					imgsrc: '',
				}]
			}],
			speechbox:[{
				speechbox: 'sp-2',
				datahighlightflag:'true',
				datahighlightcustomclass:"underline",
				textdata : data.string.p3s8toptxt,
				textclass : 'txt2',
				imgclass: 'box inverted',
				imgid : 'text_box',
				imgsrc: '',
			}],
			imgcount:[
				{
					imgcountclass:"CountContainer leftCntS6 left2"
				},
				{
					imgcountclass:"CountContainer topCnt"
				}
			],
	},
	// slide8
	{
		contentblockadditionalclass:"creamBg",
		uppertextblock:[{
			textclass : 'topTxt',
			textdata: data.string.p2s6
		}],
		imgcontainer:[
			{
				img_containerclass:"marblesContainerS8",
				box:[
					{
						boxclass:"mbl_box purplrBg S6bx1",
						boxtxt:[{
							textclass:"boxtxt fstRght",
							textdata:data.string.ten
						},{
							textclass:"boxtxt fstTop",
							textdata:data.string.ten
						},{
							textclass:"boxtxt fstMid fsmd",
							textdata:data.string.hundred
						}]
					},
					{
						boxclass:"mbl_box purplrBg S6bx2",
						boxtxt:[{
							textclass:"boxtxt secTop",
							textdata:data.string.four
						},{
							textclass:"boxtxt fstMid smd",
							textdata:data.string.forty
						}]
					},
					{
						boxclass:"mbl_box purplrBg S6bx3",
						boxtxt:[{
							textclass:"boxtxt thirdRight",
							textdata:data.string.two
						},{
							textclass:"boxtxt fstMid btm3 tmd",
							textdata:data.string.twenty
						}]
					},
					{
						boxclass:"mbl_box purplrBg S6bx4",
						boxtxt:[{
							textclass:"boxtxt fstMid btm3 fmd",
							textdata:data.string.eight
						}]
				}]
		}],
		sidetxtblock:[{
			sidetxt:[{
				datahighlightflag:"true",
				datahighlightcustomclass:"oneByOne",
				textclass:"sidetxt",
				textdata: data.string.p3s8sidetxt
			}]
		}]
	},
	// slide9
	{
		contentblockadditionalclass:"creamBg",
		uppertextblock:[{
			textclass : 'topTxt',
			textdata: data.string.p2s6
		}],
		imgcontainer:[
			{
				img_containerclass:"marblesContainerS8",
				box:[
					{
						boxclass:"mbl_box purplrBg S6bx1",
						boxtxt:[{
							textclass:"boxtxt fstRght",
							textdata:data.string.ten
						},{
							textclass:"boxtxt fstTop",
							textdata:data.string.ten
						},{
							textclass:"boxtxt fstMid",
							textdata:data.string.hundred
						}]
					},
					{
						boxclass:"mbl_box purplrBg S6bx2",
						boxtxt:[{
							textclass:"boxtxt secTop",
							textdata:data.string.four
						},{
							textclass:"boxtxt fstMid",
							textdata:data.string.forty
						}]
					},
					{
						boxclass:"mbl_box purplrBg S6bx3",
						boxtxt:[{
							textclass:"boxtxt thirdRight",
							textdata:data.string.two
						},{
							textclass:"boxtxt fstMid btm3",
							textdata:data.string.twenty
						}]
					},
					{
						boxclass:"mbl_box purplrBg S6bx4",
						boxtxt:[{
							textclass:"boxtxt fstMid btm3",
							textdata:data.string.eight
						}]
				}]
		}],
		sidetxtblock:[{
			sidetxt:[{
				datahighlightflag:"true",
				datahighlightcustomclass:"oneByOne",
				textclass:"sidetxt cntrAlgn",
				textdata: data.string.p3s9sidetxt
			}]
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
	var colCount;
	var rowCount;

	var vocabcontroller = new Vocabulary();
	vocabcontroller.init();
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg01", src: imgpath+"new/bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "suraj1", src: imgpath+"new/suraj01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box", src: imgpath+"new/text_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg", src: imgpath+"new/bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble1", src: imgpath+"new/marble01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble2", src: imgpath+"new/marble02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble3", src: imgpath+"new/marble03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble4", src: imgpath+"new/marble04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble5", src: imgpath+"new/marble05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble6", src: imgpath+"new/marble06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble7", src: imgpath+"new/marble07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble8", src: imgpath+"new/marble08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble9", src: imgpath+"new/marble09.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble10", src: imgpath+"new/marble10.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble11", src: imgpath+"new/marble11.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble12", src: imgpath+"new/marble12.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble13", src: imgpath+"new/marble05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble14", src: imgpath+"new/marble06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "niti", src: imgpath+"new/niti.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			{id: "S3_P1", src: soundAsset+"S3_P1.ogg"},
			{id: "S3_P2", src: soundAsset+"S3_P2.ogg"},
			{id: "S3_P3_1", src: soundAsset+"S3_P3_1.ogg"},
			{id: "S3_P3_2", src: soundAsset+"S3_P3_2.ogg"},
			{id: "S3_P3_3", src: soundAsset+"S3_P3_3.ogg"},
			{id: "S3_P3_4", src: soundAsset+"S3_P3_4.ogg"},
			{id: "S3_P4", src: soundAsset+"S3_P4.ogg"},
			{id: "S3_P5", src: soundAsset+"S3_P1.ogg"},
			{id: "S3_P6", src: soundAsset+"S3_P6.ogg"},
			{id: "S3_P7", src: soundAsset+"S3_P7.ogg"},
			{id: "S3_P8", src: soundAsset+"S3_P8.ogg"},
			{id: "S3_P9", src: soundAsset+"S3_P9.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		var itrCount = 0;
		var iteration = 0;

		var appendCount = 0;
		function mblAppender(fstContainerClass, fstCol, rows){
			appendCount++;
			for(var i = 0; i<=fstCol; i++){
				imgId = "marble"+(i+1);
				className= "mbl-"+countNext;
				$(fstContainerClass).append("<img class = 'marbles "+className+"' src ='"+preload.getResult(imgId).src+"'/>");
			}
			if(appendCount<rows){
				mblAppender(fstContainerClass, fstCol, rows);
			}
		}
		switch(countNext) {
			case 0:
				sound_player_nav("S3_P"+(countNext+1));
			break;
			case 1:
				sound_player_nav("S3_P"+(countNext+1));
				mblAppender(".marblesContainerFirst", 11, 7);
				$(".leftCnt").append("<p class='mblCount tl'>"+7+"</p>");
				$(".topCnt").append("<p class='mblCount tp'>"+12+"</p>");
				$(".marblesContainerFirst, .mblCount, .btmTxt").hide(0);
				$(".marblesContainerFirst").fadeIn(1500);
				$(".tp").delay(3500).fadeIn(500);
				$(".tl").delay(12000).fadeIn(500);
			break;
			case 2:
			// sound_player_nav("S3_P"+(countNext+1));
				mblAppender(".FirstS3", 11, 7);
				$(".marblesContainer").find("img").addClass("fadeOut");
				$(".mbl_box").addClass("bgAnim");
				$(".bx1").append("<p class='mblCt mc1'>"+60+"</p>");
				$(".bx2").append("<p class='mblCt mc2 '>"+24+"</p>");
				$(".oneByOne, .mblCt").hide(0);
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("S3_P3_1");
					current_sound.play();
					current_sound.on('complete',function(){
						$(".oneByOne:eq(0)").fadeIn();
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("S3_P3_2");
						current_sound.play();
						current_sound.on('complete', function(){
							$(".oneByOne:eq(1), .mc1").fadeIn();
							createjs.Sound.stop();
							current_sound = createjs.Sound.play("S3_P3_3");
							current_sound.play();
							current_sound.on('complete', function(){
								$(".oneByOne:eq(2), .mc2").fadeIn();
								createjs.Sound.stop();
								current_sound = createjs.Sound.play("S3_P3_4");
								current_sound.play();
								current_sound.on('complete', function(){
									nav_button_controls(500);
								});
							});
						});
					});
			break;
			case 3:
			sound_player_nav("S3_P"+(countNext+1));
				$(".oneByOne, .mblCt").hide(0);
				$(".mbl_box ").css({
					"background":"#8e7cc3",
					"border": "none"
				});
				$(".bx1").append("<p class='divNum'>"+60+"</p>");
				$(".bx2").append("<p class='divNum'>"+24+"</p>");
				$(".oneByOne:eq(0), .mc1").delay(1000).fadeIn();
				$(".oneByOne:eq(1), .mc2").delay(7000).fadeIn();
				$(".oneByOne:eq(2), .mc2").delay(12400).fadeIn();
				setTimeout(function(){
					$(".divNum").delay(2500).addClass("highlightTxt");
				},1700);
			break;
			case 4:
				sound_player_nav("S3_P"+(countNext+1));
			break;
			case 5:
				sound_player_nav("S3_P"+(countNext+1));
				mblAppender(".marblesContainerS6", 13, 12);
				$(".leftCntS6").append("<p class='mblCount'>"+12+"</p>");
				$(".topCnt").append("<p class='mblCount'>"+14+"</p>");
			break;
			case 6:
			sound_player_nav("S3_P"+(countNext+1));
				mblAppender(".marblesContainerS6", 13, 12);
				$(".marblesContainerS6").css("border","none");
				$(".leftCntS6").append("<p class='mblCount ct1'>"+12+"</p>");
				$(".topCnt").append("<p class='mblCount ct2'>"+14+"</p>");
				$(".fstTop, .fstRght, .secTop, .thirdRight").hide(0);
				$(".ct2").fadeOut(1000);
				$(".fstTop, .secTop").delay(1000).fadeIn(1000);
				$(".ct1").fadeOut(2200);
				$(".fstRght, .thirdRight").delay(2500).fadeIn(1000);
			break;
			case 7:
			sound_player_nav("S3_P"+(countNext+1));
				$(".oneByOne, .fstMid").hide(0);
				$(".oneByOne:eq(0)").fadeIn(1000);
				$(".fsmd").fadeIn(1000);
				$(".oneByOne:eq(1)").delay(5000).fadeIn(1000);
				$(".oneByOne:eq(2), .smd").delay(6000).fadeIn(1000);
				$(".oneByOne:eq(3), .tmd").delay(10000).fadeIn(2500);
				$(".oneByOne:eq(4)").delay(14500).fadeIn(1000);
					$(".oneByOne:eq(5), .fmd").delay(16000).fadeIn(1000);
					$(".fmd").fadeIn(1000);
			break;
			case 8:
			sound_player_nav("S3_P"+(countNext+1));
				$(".oneByOne").hide(0);
				$(".oneByOne:eq(0)").delay(4000).fadeIn(1000);
				$(".oneByOne:eq(1)").delay(12000).fadeIn(1000);
				$(".oneByOne:eq(2)").delay(14000).fadeIn(1000);
				setTimeout(function(){
					$(".fstMid").addClass("highlightTxt");
				},2200);
			break;
			default:
			nav_button_controls(1000);
		}
	}
	function cor(corClass) {
		$(corClass).css({
			"background":"#98c02e",
			"border":"3px solid #c8e038",
			"color":"#fff",
			"pointer-events":"none"
		});
		play_correct_incorrect_sound(1);
	}
	function inCor(inCorCLass){
		$(inCorCLass).css({
			"background":"#ff0000",
			"border":"3px solid #980000",
			"color":"#fff"
		});
		play_correct_incorrect_sound(0);
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(1000);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				// alert(image_src)
				// console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

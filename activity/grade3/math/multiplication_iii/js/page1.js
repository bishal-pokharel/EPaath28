// var soundAsset = $ref+"/audio_en/";
var soundAsset = $ref+"/sounds/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[
	// slide 1--> coverpage
	{
		extratextblock:[{
			textdata : data.lesson.chapter,
			textclass : 'chaptername',
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: 'background',
				imgid : 'cover_page',
				imgsrc: '',
			}]
		}]
	},
	// slide2
	{
		imageblock:[{
			imagestoshow:[{
				imgclass: 'background',
				imgid : 'bg',
				imgsrc: '',
			},
			{
				imgclass: 'suraj fstsld',
				imgid : 'suraj1',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.p1s2txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'text_box',
			imgsrc: '',
		}]
	},
	// slide3
	{
		imageblock:[{
			imagestoshow:[{
				imgclass: 'background',
				imgid : 'bg',
				imgsrc: '',
			},
			{
				imgclass: 'suraj fstsld',
				imgid : 'suraj1',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.p1s3txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'text_box',
			imgsrc: '',
		}],
		extratextblock:[{
			datahighlightflag:"true",
			datahighlightcustomclass:"fdIn",
			textclass : 'midEqnTxt',
			textdata : data.string.p1s3_sectxt,
		}]
	},
	// slide4
	{
		imageblock:[{
			imagestoshow:[{
				imgclass: 'background',
				imgid : 'bg',
				imgsrc: '',
			},
			{
				imgclass: 'suraj fstsld',
				imgid : 'suraj1',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.p1s4txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'text_box',
			imgsrc: '',
		}],
		extratextblock:[{
			textclass : 'midEqnTxt mdtxtFst',
			textdata : data.string.p1s4_sectxt_1,
		},{
			textclass : 'midEqnTxt mdtxtSec',
			textdata : data.string.p1s4_sectxt_2,
		}]
	},
	// slide5
	{
		contentblockadditionalclass:"blueBg",
		uppertextblock:[{
			datahighlightflag:"true",
			datahighlightcustomclass:"tpHidTxt",
			textdata : data.string.p1s5_sectxt,
			textclass : 'topTxt',
		}],
		imgcontainer:[{
			img_containerclass:"marblesContainer"
		}]

	},
	// slide6
	{
		contentblockadditionalclass:"blueBg",
		uppertextblock:[{
			textdata : data.string.p1s5txt,
			textclass : 'topTxt',
		}],
		imgcontainer:[{
			img_containerclass:"marblesContainer"
		}],
		speechbox:[{
			speechbox: 'sp-2',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.p1s6box_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'text_box01',
			imgsrc: '',
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: 'suraj sxSld',
				imgid : 'suraj4',
				imgsrc: '',
			}]
		}],
	},
	// slide7
	{
		contentblockadditionalclass:"blueBg",
		uppertextblock:[{
			textdata : data.string.p1s5txt,
			textclass : 'topTxt',
		}],
		imgcontainer:[{
			img_containerclass:"marblesContainerSmall",
			box:[{
				boxclass:"boxright",
				boxtxt:[{
					textclass:"boxtxt rbTop",
					textdata:data.string.ten
				},{
					textclass:"boxtxt rbRight",
					textdata:data.string.five
				}]
			},{
				boxclass:"boxleft",
				boxtxt:[{
					textclass:"boxtxt lbTop",
					textdata:data.string.two
				}]
			}]
		}],
		speechbox:[{
			speechbox: 'sp-2',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.p1s7box_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'text_box01',
			imgsrc: '',
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: 'suraj sxSld',
				imgid : 'suraj4',
				imgsrc: '',
			}]
		}],
	},
	// slide8
	{
		contentblockadditionalclass:"blueBg",
		uppertextblock:[{
			textdata : data.string.p1s5txt,
			textclass : 'topTxt',
		}],
		imgcontainer:[{
			img_containerclass:"marblesContainerSmall",
			box:[{
				boxclass:"purpleBg boxright ",
				boxtxt:[{
					textclass:"boxtxt rbTop",
					textdata:data.string.ten
				},{
					textclass:"boxtxt rbRight",
					textdata:data.string.five
				},{
					textclass:"boxtxt mdTxtRght tx1",
					textdata:data.string.fifty
				}]
			},{
				boxclass:"purpleBg boxleft",
				boxtxt:[{
					textclass:"boxtxt lbTop",
					textdata:data.string.two
				},{
					textclass:"boxtxt mdTxtRght tx2",
					textdata:data.string.ten
				}]
			}]
		}],
		speechbox:[{
			speechbox: 'sp-2',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.p1s8box_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'text_box01',
			imgsrc: '',
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: 'suraj sxSld',
				imgid : 'suraj4',
				imgsrc: '',
			}]
		}],
		extratextblock:[{
			textdata : data.string.p1s9txt_1,
			textclass : 'btmTxt fstTxt',
		},{
			textdata : data.string.p1s9txt_2,
			textclass : 'btmTxt secTxt',
		}]
	},
	// slide10
	{
		contentblockadditionalclass:"blueBg",
		uppertextblock:[{
			textdata : data.string.p1s5txt,
			textclass : 'topTxt',
		}],
		extratextblock:[{
			textdata : data.string.p1s10txt_1,
			textclass : 'btmTxt fstTxt',
		},{
			textdata : data.string.p1s10txt_2,
			textclass : 'btmTxt secTxt',
		}],
		imgcontainer:[{
			img_containerclass:"marblesContainerSmall",
			box:[{
				boxclass:"purpleBg boxright ",
				boxtxt:[{
					textclass:"boxtxt rbTop",
					textdata:data.string.ten
				},{
					textclass:"boxtxt rbRight",
					textdata:data.string.five
				},{
					textclass:"boxtxt mdTxtRght",
					textdata:data.string.fifty
				}]
			},{
				boxclass:"purpleBg boxleft",
				boxtxt:[{
					textclass:"boxtxt lbTop",
					textdata:data.string.two
				},{
					textclass:"boxtxt mdTxtRght",
					textdata:data.string.ten
				}]
			}]
		}],
		speechbox:[{
			speechbox: 'sp-2',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.p1s10box_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'text_box01',
			imgsrc: '',
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: 'suraj sxSld',
				imgid : 'suraj4',
				imgsrc: '',
			}]
		}],
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
	var vocabcontroller = new Vocabulary();
	vocabcontroller.init();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg01", src: imgpath+"new/bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "suraj1", src: imgpath+"new/suraj01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "suraj4", src: imgpath+"new/suraj04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box", src: imgpath+"new/text_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box01", src: imgpath+"new/text_box01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg", src: imgpath+"new/bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble1", src: imgpath+"new/marble01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble2", src: imgpath+"new/marble02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble3", src: imgpath+"new/marble03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble4", src: imgpath+"new/marble04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble5", src: imgpath+"new/marble05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble6", src: imgpath+"new/marble06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble7", src: imgpath+"new/marble07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble8", src: imgpath+"new/marble08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble9", src: imgpath+"new/marble09.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble10", src: imgpath+"new/marble10.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble11", src: imgpath+"new/marble11.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble12", src: imgpath+"new/marble12.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cover_page", src: imgpath+"new/cover_page_new.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			// {id: "dog", src: soundAsset+"S1_P1.ogg"},
			{id: "S1_P1", src: soundAsset+"s1_p1.ogg"},
			{id: "S1_P2", src: soundAsset+"S1_P2.ogg"},
			{id: "S1_P3", src: soundAsset+"S1_P3.ogg"},
			{id: "S1_P4", src: soundAsset+"S1_P4.ogg"},
			{id: "S1_P5", src: soundAsset+"S1_P5.ogg"},
			{id: "S1_P6", src: soundAsset+"S1_P6.ogg"},
			{id: "S1_P7", src: soundAsset+"S1_P7.ogg"},
			{id: "S1_P8", src: soundAsset+"S1_P8.ogg"},
			{id: "S1_P9", src: soundAsset+"S1_P9.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		var itrCount = 0;
		var iteration = 0;

		function showMarbles(mblClass, showClass){
			itrCount++;
			for(var i=0; i<=11; i++){
				$(".marbles:eq("+(iteration+i)+")").css("opacity","1");
				$(".marbles:eq("+(iteration+i)+")").addClass("mbl"+(iteration+i+1));
			}
			iteration  = iteration+i;
			setTimeout(function(){
				if(itrCount==1){
					$(showClass).show(0);
					setTimeout(function(){
						itrCount<5?showMarbles(mblClass):nav_button_controls(1000);
					},1000);
				}
				else{
					itrCount<5?showMarbles(mblClass):nav_button_controls(1000);
				}
			},1000);

		}
		var appendCount = 0;
		function mblAppender(containerClass){
			appendCount++;
			for(var i = 0; i<=11; i++){
				imgId = "marble"+(i+1);
				$(containerClass).append("<img class = 'marbles' src ='"+preload.getResult(imgId).src+"'/>");
			}
			if(appendCount<=5){
				mblAppender(containerClass);
			}
		}
		switch(countNext) {
			case 0:
				sound_player_nav("S1_P"+(countNext+1));
			break;
			case 1:
				sound_player_nav("S1_P"+(countNext+1));
			break;
			case 2:
				sound_player_nav("S1_P"+(countNext+1));
				$(".fdIn").css("display","none");
				$(".fdIn").delay(8000).fadeIn();
			break;
			case 3:
				$(".midEqnTxt").hide(0);
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("S1_P"+(countNext+1));
					current_sound.play();
					current_sound.on('complete', function(){
						$(".mdtxtFst").delay(500).fadeIn(200);
						$(".mdtxtSec").delay(1000).fadeIn(200);
						nav_button_controls(2000);
					});
			break;
			case 4:
				sound_player_nav("S1_P"+(countNext+1));
				$(".tpHidTxt").hide(0);
				var container = $(".marblesContainer");
				mblAppender(".marblesContainer");
				$(".marbles").css("opacity","0");
				setTimeout(function(){
					showMarbles(".marbles",".tpHidTxt");
				},3760);

			break;
			case 5:
				sound_player_nav("S1_P"+(countNext+1));
				mblAppender(".marblesContainer");
				$(".marblesContainer").append("<p class='box10Mbl'></p>");
				$(".marblesContainer").append("<p class='Txt_10'>10</p>");
				$(".Txt_10").hide(0);
				$(".marblesContainer").animate({
					width:"54%",
					height:"46%",
					top:"50%",
					left:"8%"
				},2000, function(){
					$(".box10Mbl").css("border","2px solid #175676");
					$(".box10Mbl").animate({
						width: "80%"
					},2000);
				});
				$(".Txt_10").delay(4000).fadeIn(100);
			break;
			case 6:
				sound_player_nav("S1_P"+(countNext+1));
				mblAppender(".marblesContainerSmall");
				$(".mdTxtRght").css("opacity","0");
				$(".marblesContainerSmall").find("img").addClass("fadeout");
				$(".boxright, .boxleft").addClass("bgAnim");
				setTimeout(function(){
					$(".mdTxtRght").animate({
						opacity:"1"
					},1000);
				},2000);
			break;
			case 7:
				sound_player_nav("S1_P"+(countNext+1));
				$(".tx1, .tx2, .fstTxt, .secTxt").hide(0);
				$(".rbTop, .rbRight").addClass("highlightTxt");
				$(".tx1, .fstTxt").delay(1700).fadeIn();
				setTimeout(function(){
					$(".rbTop").removeClass("highlightTxt");
					$(".lbTop, .rbRight").addClass("highlightTxt");
					$(".tx2, .secTxt").delay(1700).fadeIn(1000);
				},2000);
			break;
			case 8:
				sound_player_nav("S1_P"+(countNext+1));
			$(".secTxt, .fstTxt").hide(0);
				$(".fstTxt").fadeIn(500);
				setTimeout(function(){
					$(".secTxt").fadeIn(1000);
						$(".mdTxtRght").addClass("highlightTxt");
				},1500);
			break;
			default:
			nav_button_controls(1000);
			break;

		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(1000);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				// alert(image_src)
				// console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var soundAsset = $ref+"/sounds/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[
	// slide1
	{
		extratextblock:[{
			textclass : 'diyTxt',
			textdata:data.string.diytxt
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: 'background',
				imgid : 'bg_diy',
				imgsrc: '',
			}]
		}]
	},
	// slide2
	{
		contentblockadditionalclass:"creamBg",
		uppertextblock:[{
			datahighlightflag:'true',
			datahighlightcustomclass:"qnWrd",
			textclass : 'topTxt',
			textdata:data.string.p3qn1
		}],
		imgcount:[
			{
				imgcountclass:"CountContainer leftCnt"
			},
			{
				imgcountclass:"CountContainer topCnt"
			},
			{
				imgcountclass:"CountContainer answer"
			},
			{
				imgcountclass:"submit"
			}
		],
		imgcontainer:[
			{
			img_containerclass:"marblesContainerFirst",
			},
			{
			img_containerclass:"marblesContainerSec"
		}],
			imageblock:[{
				imagestoshow:[
				{
					imgclass: 'suraj fstsld',
					imgid : 'suraj1',
					imgsrc: '',
				}]
			}],
			speechbox:[{
				speechbox: 'sp-1',
				datahighlightflag:'true',
				datahighlightcustomclass:"underline",
				textdata : data.string.p3qn,
				textclass : 'txt1',
				imgclass: 'box',
				imgid : 'text_box',
				imgsrc: '',
			}]

	},
	// slide3
	{
		contentblockadditionalclass:"creamBg",
		uppertextblock:[{
			datahighlightflag:'true',
			datahighlightcustomclass:"qnWrd",
			textclass : 'topTxt',
			textdata:data.string.p3qn1
		}],
		imgcount:[
			{
				imgcountclass:"CountContainer leftCnt"
			},
			{
				imgcountclass:"CountContainer topCnt"
			},
			{
				imgcountclass:"CountContainer secCnt"
			}
		],
		imgcontainer:[
			{
			img_containerclass:"marblesContainerFirst",
			},
			{
			img_containerclass:"marblesContainerSec"
		}],
		questionblock:[{
			qntxt:[
				{
					textclass:"qn top",
					textdata:data.string.p3S3qn
				},
				{
					textclass:"qn qn1"
				},
				{
					textclass:"qn mid",
					textdata:data.string.p3S3qn_sec
				},
				{
					textclass:"qn qn2"
				},
				{
					textclass:"submit sbmSec"
				}
			]
		}]
	},
	// slide4
	{
		contentblockadditionalclass:"creamBg",
		uppertextblock:[{
			datahighlightflag:'true',
			datahighlightcustomclass:"qnWrd",
			textclass : 'topTxt',
			textdata:data.string.p3qn1
		}],
		imgcount:[
			{
				imgcountclass:"CountContainer leftCnt"
			},
			{
				imgcountclass:"CountContainer topCnt"
			},
			{
				imgcountclass:"CountContainer secCnt"
			},
			{
				imgcountclass:"practice"
			}
		],
		imgcontainer:[
			{
			img_containerclass:"marblesContainerFirst",
			},
			{
			img_containerclass:"marblesContainerSec"
		}],
		questionblock:[{
			qntxt:[
				{
					textclass:"qn top",
					textdata:data.string.p3S3qn
				},
				{
					textclass:"qn qn1"
				},
				{
					textclass:"qn mid",
					textdata:data.string.p3S3qn_sec
				},
				{
					textclass:"qn qn2"
				}
			]
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
	var colCount;
	var rowCount;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg01", src: imgpath+"new/bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "suraj1", src: imgpath+"new/suraj04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box", src: imgpath+"new/bubble2-12_a.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg", src: imgpath+"new/bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble1", src: imgpath+"new/marble01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble2", src: imgpath+"new/marble02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble3", src: imgpath+"new/marble03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble4", src: imgpath+"new/marble04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble5", src: imgpath+"new/marble05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble6", src: imgpath+"new/marble06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble7", src: imgpath+"new/marble07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble8", src: imgpath+"new/marble08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble9", src: imgpath+"new/marble09.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble10", src: imgpath+"new/marble10.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble11", src: imgpath+"new/marble11.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble12", src: imgpath+"new/marble12.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_diy", src: imgpath+"new/bg_diy.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			{id: "S2_P1", src: soundAsset+"S2_P1.ogg"},
			{id: "S2_P2", src: soundAsset+"S2_P2.ogg"},
			{id: "S2_P3", src: soundAsset+"S2_P3.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		var corflag = true, corflagSec = true;
		var itrCount = 0;
		var iteration = 0;
		$(".submit").html(eval("data.string.submit"));
		$(".practice").html(eval("data.string.practice"));
		var appendCount = 0;
		function mblAppender(fstContainerClass, fstCol, rows, secContainer, secCol){
			appendCount++;
			for(var i = 0; i<=fstCol; i++){
				imgId = "marble"+(i+1);
				$(fstContainerClass).append("<img class = 'marbles' src ='"+preload.getResult(imgId).src+"'/>");
			}
			for(var i = 0; i<secCol; i++){
				imgId = "marble"+(i+1);
				$(secContainer).append("<img class = 'marblesSec' src ='"+preload.getResult(imgId).src+"'/>");
			}
			switch (secCol) {
				case 1:
				$(".marblesContainerSec").css("width","6%");
					$(".marblesSec").css("width","87%");
				break;
				case 2:
				$(".marblesContainerSec").css("width","10%");
					$(".marblesSec").css("width","44%");
				break;
				case 3:
				$(".marblesContainerSec").css("width","15%");
					$(".marblesSec").css("width","30%");
				break;
				case 4:
				$(".marblesContainerSec").css("width","18%");
					$(".marblesSec").css("width","22%");
				break;
				case 5:
				$(".marblesContainerSec").css("width","24%");
					$(".marblesSec").css("width","17%");
				break;
				default:
			}
			if(appendCount<rows){
				mblAppender(fstContainerClass, fstCol, rows, secContainer, secCol);
			}
		}
		switch(countNext) {
			case 0:
				sound_player_nav("S2_P"+(countNext+1));
			break;
			case 1:
				sound_player("S2_P"+(countNext+1));
				colCount = Math.floor(Math.random() * (5 - 1 +1)) + 1;
				rowCount = Math.floor(Math.random() * (5 - 3 +1)) + 3;
				$(".qnWrd:eq(0)").html(rowCount);
				$(".qnWrd:eq(1)").html(10+colCount);
				var container = $(".marblesContainerFirst");
				mblAppender(".marblesContainerFirst", 9, rowCount,".marblesContainerSec",colCount);
				$(".leftCnt").append("<p class='mblCount'>"+rowCount+"</p>");
				$(".topCnt").append("<p class='mblCount'>"+10+"</p>");
				$(".answer").html(eval('data.string.diy_ans'));
				$(".submit").click(function(){
						var input = $(".answer").find("input").val();
						if(input == colCount){
							$("#input1").css({
								"background":"#98c02e",
								"border":"3px solid #c8e038",
								"color":"#fff",
								"pointer-events":"none"
							});
							play_correct_incorrect_sound(1);
							$(this).css("pointer-events","none");
							$(".marblesContainerFirst, .marblesContainerSec  ").children().css("opacity","0.3");
							$(".marblesContainerFirst").append("<p class='count fst'>"+rowCount*10+"</p>");
							$(".marblesContainerSec").append("<p class='count sec'>"+rowCount*colCount+"</p>");
							nav_button_controls(1000);
						}
						else{
							$("#input1").css({
								"background":"#ff0000",
								"border":"3px solid #980000",
								"color":"#fff"
							});
							play_correct_incorrect_sound(0);
						}
				});
			break;
			case 2:
				sound_player("S2_P"+(countNext+1));
			$(".qnWrd:eq(0)").html(rowCount);
			$(".qnWrd:eq(1)").html(10+colCount);
			$(".marblesContainerFirst").css({
				"width":"40%",
				"background":"#8e7cc3",
				"border":"none",
			});
			$(".marblesContainerSec").css({
				"width":"7%",
				"left":"49%",
				"background":"#8e7cc3",
				"border":"none",
			});
			$(".leftCnt").append("<p class='mblCount'>"+rowCount+"</p>");
			$(".topCnt").append("<p class='mblCount'>"+10+"</p>");
			$(".marblesContainerFirst").append("<p class='count white'>"+rowCount*10+"</p>");
			$(".marblesContainerSec").append("<p class='count white left15'>"+rowCount*colCount+"</p>");
			$(".secCnt").append("<p class='mblCount'>"+colCount+"</p>");
			$(".qn1").html((rowCount*10)+" + "+(colCount*rowCount)+ " = "+eval('data.string.fstans'));
			$(".qn2").html((10+colCount)+" x "+(rowCount)+ " = "+eval('data.string.secans'));
			$(".submit").click(function(){
				var inpAans1 = $(".qn1").find("input").val();
				var inpAans2 = $(".qn2").find("input").val();
				if(inpAans1 ==(rowCount*(10+colCount))){
					cor("#ip1");
					corflag=true;
				}else{
					inCor("#ip1");
					corflag=false;
				}
				if(inpAans2 ==(rowCount*(10+colCount))){
					cor("#ip2");
					corflagSec=true;
				}else{
					inCor("#ip2");
					corflagSec=false;
				}
				corflag==true&&corflagSec==true?nav_button_controls(1000):"";
			});
		break;
		case 3:
		$(".qnWrd:eq(0)").html(rowCount);
		$(".qnWrd:eq(1)").html(10+colCount);
		$(".marblesContainerFirst").css({
			"width":"40%",
			"background":"#8e7cc3",
			"border":"none",
		});
		$(".marblesContainerSec").css({
			"width":"7%",
			"left":"49%",
			"background":"#8e7cc3",
			"border":"none",
		});
		$(".leftCnt").append("<p class='mblCount'>"+rowCount+"</p>");
		$(".topCnt").append("<p class='mblCount'>"+10+"</p>");
		$(".marblesContainerFirst").append("<p class='count white'>"+rowCount*10+"</p>");
		$(".marblesContainerSec").append("<p class='count white left15'>"+rowCount*colCount+"</p>");
		$(".secCnt").append("<p class='mblCount'>"+colCount+"</p>");
		$(".qn1").html((rowCount*10)+" + "+(colCount*rowCount)+ " = "+ (rowCount*(10+colCount)));
		$(".qn2").html((10+colCount)+" x "+(rowCount)+ " = "+ (rowCount*(10+colCount)));

		$(".practice").on("click",function(){
				$prevBtn.css('display', 'none');
				$nextBtn.css('display', 'none');
				location.reload();
		});

		nav_button_controls(1000);
	break;
		}
	}
	function cor(corClass) {
		$(corClass).css({
			"background":"#98c02e",
			"border":"3px solid #c8e038",
			"color":"#fff",
			"pointer-events":"none"
		});
		play_correct_incorrect_sound(1);
	}
	function inCor(inCorCLass){
		$(inCorCLass).css({
			"background":"#ff0000",
			"border":"3px solid #980000",
			"color":"#fff"
		});
		play_correct_incorrect_sound(0);
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(1000);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				// alert(image_src)
				// console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

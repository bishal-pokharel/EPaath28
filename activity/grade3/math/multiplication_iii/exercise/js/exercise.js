var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	//exercise 1
		{
			contentblockadditionalclass:"creambg",
			extratxtdiv:[{
				textclass:"question",
				textdata:data.string.excq1
			}],
			imgcontainer:[
				{
					img_containerclass:"marblesContainerFirst",
				}],
			exetype1: [{
				optionsdivclass:"optionsdiv",
		    exeoptions:[
		      {
						startposextra:"lftCrs",
		        optaddclass: "correct",
		      },
		      {
						startposextra:"lftCrs",
		    	},
		      {
						startposextra:"lftCrs",
		    	},
		      {
						startposextra:"lftCrs",
		    	}
				]
			}]
		},
	//exercise 2
		{
			contentblockadditionalclass:"creambg",
			extratxtdiv:[{
				textclass:"question",
				textdata:data.string.excq2
			}],
			imgcontainer:[
				{
					img_containerclass:"marblesContainerFirst",
				}],
			exetype1: [{
				optionsdivclass:"optionsdiv",
		    exeoptions:[
		      {
						startposextra:"lftCrs",
		        optaddclass: "correct",
		      },
		      {
						startposextra:"lftCrs",
		    	},
		      {
						startposextra:"lftCrs",
		    	},
		      {
						startposextra:"lftCrs",
		    	}
				]
			}]
		},
	//exercise 3
		{
			contentblockadditionalclass:"creambg",
			extratxtdiv:[{
				datahighlightflag:'true',
				datahighlightcustomclass:"qnwrd",
				textclass:"question",
				textdata:data.string.excq3
			}],
			imgcontainer:[
				{
					img_containerclass:"marblesContainerFirst exc3",
				}],
			exetype1: [{
				optionsdivclass:"opnDiv3",
		    exeoptions:[
		      {
						optcontainerextra:"opncntrS6",
		        optaddclass: "correct",
		      },
		      {
						optcontainerextra:"opncntrS6",
		    	},
		      {
						optcontainerextra:"opncntrS6",
		    	},
		      {
						optcontainerextra:"opncntrS6",
		    	}
				]
			}]
		},
	//exercise 4 --> boxes
		{
			contentblockadditionalclass:"creambg",
			extratxtdiv:[{
				datahighlightflag:'true',
				datahighlightcustomclass:"qnwrd",
				textclass:"question",
				textdata:data.string.excq4
			}],
			fullboxcontainer:[
				{
					fullboxcontainerclass:"fullBox bxFst correct",
					innerbox:[
					{
						innerboxclass:"rhsBox",
						boxtxt:[
							{
								textclass:"bxTxt rhsFSt rsf"
							},
							{
								textclass:"bxTxt midright mdr_f"
							},
							{
								textclass:"bxTxt rhsTop rtf"
						}]
					},
					{
						innerboxclass:"lhsBox",
						boxtxt:[
							{
								textclass:"bxTxt lhsTop ltf"
							},
							{
								textclass:"bxTxt midleft mdl_f"
							}
						]
				 	}]
				},
				{
					fullboxcontainerclass:"fullBox bxSec",
					innerbox:[
					{
						innerboxclass:"rhsBox",
						boxtxt:[
							{
								textclass:"bxTxt rhsFSt rss"
							},
							{
								textclass:"bxTxt midright mdr_s"
							},
							{
								textclass:"bxTxt rhsTop rts"
						}]
					},
					{
						innerboxclass:"lhsBox",
						boxtxt:[
							{
								textclass:"bxTxt lhsTop lts"
							},
							{
								textclass:"bxTxt midleft mdl_s"
							}
						]
				 	}]
				},
				{
					fullboxcontainerclass:"fullBox bxthird",
					innerbox:[
					{
						innerboxclass:"rhsBox",
						boxtxt:[
							{
								textclass:"bxTxt rhsFSt rst"
							},
							{
								textclass:"bxTxt midright mdr_t"
							},
							{
								textclass:"bxTxt rhsTop rtt"
						}]
					},
					{
						innerboxclass:"lhsBox",
						boxtxt:[
							{
								textclass:"bxTxt lhsTop ltt"
							},
							{
								textclass:"bxTxt midleft mdl_t"
							}
						]
				 	}]
				},
				{
					fullboxcontainerclass:"fullBox bxfrth",
					innerbox:[
					{
						innerboxclass:"rhsBox",
						boxtxt:[
							{
								textclass:"bxTxt rhsFSt rsfth"
							},
							{
								textclass:"bxTxt midright mdr_fth"
							},
							{
								textclass:"bxTxt rhsTop rtfth"
						}]
					},
					{
						innerboxclass:"lhsBox",
						boxtxt:[
							{
								textclass:"bxTxt lhsTop ltfth"
							},
							{
								textclass:"bxTxt midleft mdl_fth"
							}
						]
				 	}]
				}
			],
		},
	//exercise 5
		{
			contentblockadditionalclass:"creambg",
			extratxtdiv:[{
				datahighlightflag:'true',
				datahighlightcustomclass:"qnwrd",
				textclass:"question",
				textdata:data.string.excq5
			}],
			exetype1: [{
				optionsdivclass:"opnDiv4",
		    exeoptions:[
		      {
						optcontainerextra:"opncntrS7",
		        optaddclass: "correct",
						startposextra:"imgS7"
		      },
		      {
						optcontainerextra:"opncntrS7",
						startposextra:"imgS7"
		    	},
		      {
						optcontainerextra:"opncntrS7",
						startposextra:"imgS7"
		    	},
		      {
						optcontainerextra:"opncntrS7",
						startposextra:"imgS7"
		    	}
				]
			}]
		},
	//exercise 6 --> boxes
		{
			contentblockadditionalclass:"creambg",
			extratxtdiv:[{
				datahighlightflag:'true',
				datahighlightcustomclass:"qnwrd",
				textclass:"question",
				textdata:data.string.excq6
			}],
			fullboxcontainer:[
				{
					fullboxcontainerclass:"fullBox bxS6",
					innerbox:[
					{
						innerboxclass:"rhsBox",
						boxtxt:[
							{
								textclass:"bxTxt rhsFSt rsf"
							},
							{
								textclass:"bxTxt midright mdr_f"
							},
							{
								textclass:"bxTxt rhsTop rtf"
						}]
					},
					{
						innerboxclass:"lhsBox",
						boxtxt:[
							{
								textclass:"bxTxt lhsTop ltf"
							},
							{
								textclass:"bxTxt midleft mdl_f"
							}
						]
					}]
				}
			],
			exetype1: [{
				optionsdivclass:"opnDiv3",
				exeoptions:[
					{
						optcontainerextra:"opncntrS6",
						optaddclass: "correct",
					},
					{
						optcontainerextra:"opncntrS6",
					},
					{
						optcontainerextra:"opncntrS6",
					},
					{
						optcontainerextra:"opncntrS6",
					}
				]
			}]
		},
	//exercise 7 --> boxes
		{
			contentblockadditionalclass:"creambg",
			extratxtdiv:[{
				datahighlightflag:'true',
				datahighlightcustomclass:"qnwrd",
				textclass:"question",
				textdata:data.string.excq7
			}],
			fullboxcontainer:[
				{
					fullboxcontainerclass:"fullBox bxFst s7fst",
					innerbox:[
					{
						innerboxclass:"rhsBox singleBx",
						boxtxt:[
							{
								textclass:"bxTxt rhsFSt rsf"
							},
							{
								textclass:"bxTxt midright mdr_f"
							},
							{
								textclass:"bxTxt rhsTop rtf"
						}]
					}]
				},
				{
					fullboxcontainerclass:"fullBox bxSec s7sec",
					innerbox:[
					{
						innerboxclass:"rhsBox rhsS7",
						boxtxt:[
							{
								textclass:"bxTxt rhsFSt rss"
							},
							{
								textclass:"bxTxt midright mdr_s"
							},
							{
								textclass:"bxTxt rhsTop rts"
						}]
					},
					{
						innerboxclass:"lhsBox rhsS7",
						boxtxt:[
							{
								textclass:"bxTxt lhsTop lts"
							},
							{
								textclass:"bxTxt midleft mdl_s"
							}
						]
				 	},
					{
						innerboxclass:"rhsBox btmBox rightBtm",
						boxtxt:[
							{
								textclass:"bxTxt rhsFSt rss_s"
							},
							{
								textclass:"bxTxt midright mdr_s_s"
							}
						]
				 	},
					{
						innerboxclass:"lhsBox btmBox leftBtm",
						boxtxt:[
							{
								textclass:"bxTxt midleft mdl_t_s"
							}
						]
				 	}]
				},
				{
					fullboxcontainerclass:"fullBox bxthird s7third",
					innerbox:[
					{
						innerboxclass:"rhsBox",
						boxtxt:[
							{
								textclass:"bxTxt rhsFSt rst"
							},
							{
								textclass:"bxTxt midright mdr_t"
							},
							{
								textclass:"bxTxt rhsTop rtt"
						}]
					},
					{
						innerboxclass:"lhsBox",
						boxtxt:[
							{
								textclass:"bxTxt lhsTop ltt"
							},
							{
								textclass:"bxTxt midleft mdl_t"
							}
						]
				 	}]
				},
			],
			imageblock:[{
				imagestoshow:[{
					imgclass: 'arrow arfst',
					imgid : 'arrow',
					imgsrc: '',
				},{
					imgclass: 'arrow arsec',
					imgid : 'arrow',
					imgsrc: '',
				}]
			}],
			exetype1: [{
				optionsdivclass:"opnDiv3",
				exeoptions:[
					{
						optcontainerextra:"opncntrS6",
						optaddclass: "correct",
					},
					{
						optcontainerextra:"opncntrS6",
					},
					{
						optcontainerextra:"opncntrS6",
					},
					{
						optcontainerextra:"opncntrS6",
					}
				]
			}]
		},
	//exercise 8
		{
			contentblockadditionalclass:"creambg",
			extratxtdiv:[{
				datahighlightflag:'true',
				datahighlightcustomclass:"qnwrd",
				textclass:"question",
				textdata:data.string.excq8
			}],
			exetype1: [{
				optionsdivclass:"opnDiv4",
		    exeoptions:[
		      {
						optcontainerextra:"opncntrS7",
		        optaddclass: "correct",
						startposextra:"imgS7"
		      },
		      {
						optcontainerextra:"opncntrS7",
						startposextra:"imgS7"
		    	},
		      {
						optcontainerextra:"opncntrS7",
						startposextra:"imgS7"
		    	},
		      {
						optcontainerextra:"opncntrS7",
						startposextra:"imgS7"
		    	}
				]
			}]
		},
	//exercise 9 --> boxes
		{
			contentblockadditionalclass:"creambg",
			extratxtdiv:[{
				datahighlightflag:'true',
				datahighlightcustomclass:"qnwrd",
				textclass:"question",
				textdata:data.string.excq9
			}],
			extratxtblock:[{
				textclass:"check",
				textdata:data.string.check
			}],
			imageblock:[{
				imagestoshow:[{
					imgclass: 'arrow s9_arfst',
					imgid : 'arrow',
					imgsrc: '',
				},{
					imgclass: 'arrow s9_arsec',
					imgid : 'arrow',
					imgsrc: '',
				}]
			}],
			fullboxcontainer:[
				{
					fullboxcontainerclass:"fullBox bxFst s9fst",
					innerbox:[
					{
						innerboxclass:"rhsBox singleBx",
						boxtxt:[
							{
								textclass:"bxTxt rhsFSt rsf"
							},
							{
								textclass:"bxTxt midright mdr_f"
							},
							{
								textclass:"bxTxt rhsTop rtf"
						}]
					}]
				},
				{
					fullboxcontainerclass:"fullBox bxSec s9sec",
					innerbox:[
					{
						innerboxclass:"rhsBox rhsS7",
						boxtxt:[
							{
								textclass:"bxTxt rhsFSt rss"
							},
							{
								textclass:"bxTxt midright mdr_s"
							},
							{
								textclass:"bxTxt rhsTop rts"
						}]
					},
					{
						innerboxclass:"lhsBox rhsS7",
						boxtxt:[
							{
								textclass:"bxTxt lhsTop lts"
							},
							{
								textclass:"bxTxt midleft mdl_s9"
							}
						]
				 	},
					{
						innerboxclass:"rhsBox btmBox rightBtm",
						boxtxt:[
							{
								textclass:"bxTxt rhsFSt rss_s"
							},
							{
								textclass:"bxTxt midright mdr_s_s"
							}
						]
				 	},
					{
						innerboxclass:"lhsBox btmBox leftBtm",
						boxtxt:[
							{
								textclass:"bxTxt midleft mdl_t_s"
							}
						]
				 	}]
				},
				{
					fullboxcontainerclass:"fullBox bxthird s9third",
					innerbox:[
					{
						innerboxclass:"rhsBox",
						boxtxt:[
							{
								textclass:"bxTxt rhsFSt rst"
							},
							{
								textclass:"bxTxt midright mdr_tS9"
							},
							{
								textclass:"bxTxt rhsTop rtt"
						}]
					},
					{
						innerboxclass:"lhsBox",
						boxtxt:[
							{
								textclass:"bxTxt lhsTop ltt"
							},
							{
								textclass:"bxTxt midleft mdl_t"
							}
						]
				 	}]
				},
			]
		},
	//exercise 10 --> boxes
		{
			contentblockadditionalclass:"creambg",
			extratxtdiv:[{
				datahighlightflag:'true',
				datahighlightcustomclass:"qnwrd",
				textclass:"question",
				textdata:data.string.excq10
			}],
			imageblock:[{
				imagestoshow:[{
					imgclass: 'arrow arfst',
					imgid : 'arrow',
					imgsrc: '',
				},{
					imgclass: 'arrow arsec',
					imgid : 'arrow',
					imgsrc: '',
				}]
			}],
			fullboxcontainer:[
				{
					fullboxcontainerclass:"fullBox bxFst s7fst",
					innerbox:[
					{
						innerboxclass:"rhsBox singleBx",
						boxtxt:[
							{
								textclass:"bxTxt rhsFSt rsf"
							},
							{
								textclass:"bxTxt midright mdr_f"
							},
							{
								textclass:"bxTxt rhsTop rtf"
						}]
					}]
				},
				{
					fullboxcontainerclass:"fullBox bxSec s7sec",
					innerbox:[
					{
						innerboxclass:"rhsBox rhsS7",
						boxtxt:[
							{
								textclass:"bxTxt rhsFSt rss"
							},
							{
								textclass:"bxTxt midright mdr_s"
							},
							{
								textclass:"bxTxt rhsTop rts"
						}]
					},
					{
						innerboxclass:"lhsBox rhsS7",
						boxtxt:[
							{
								textclass:"bxTxt lhsTop lts"
							},
							{
								textclass:"bxTxt midleft mdl_s"
							}
						]
				 	},
					{
						innerboxclass:"rhsBox btmBox rightBtm",
						boxtxt:[
							{
								textclass:"bxTxt rhsFSt rss_s"
							},
							{
								textclass:"bxTxt midright mdr_s_s"
							}
						]
				 	},
					{
						innerboxclass:"lhsBox btmBox leftBtm",
						boxtxt:[
							{
								textclass:"bxTxt midleft mdl_t_s"
							}
						]
				 	}]
				},
				{
					fullboxcontainerclass:"fullBox bxthird s7third",
					innerbox:[
					{
						innerboxclass:"rhsBox",
						boxtxt:[
							{
								textclass:"bxTxt rhsFSt rst"
							},
							{
								textclass:"bxTxt midright mdr_t"
							},
							{
								textclass:"bxTxt rhsTop rtt"
						}]
					},
					{
						innerboxclass:"lhsBox",
						boxtxt:[
							{
								textclass:"bxTxt lhsTop ltt"
							},
							{
								textclass:"bxTxt midleft mdl_t"
							}
						]
				 	}]
				},
			],
			exetype1: [{
				optionsdivclass:"opnDiv3",
				exeoptions:[
					{
						optcontainerextra:"opncntrS6",
						optaddclass: "correct",
					},
					{
						optcontainerextra:"opncntrS6",
					},
					{
						optcontainerextra:"opncntrS6",
					},
					{
						optcontainerextra:"opncntrS6",
					}
				]
			}]
		},
];

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var item_txt ='';
	var countNext = 0;
	var test = false;

	/*for limiting the questions to 10*/
	var $total_page = 10;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images		],
			{id: "marble1", src: imgpath+"new/marble01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble2", src: imgpath+"new/marble02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble3", src: imgpath+"new/marble03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble4", src: imgpath+"new/marble04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble5", src: imgpath+"new/marble05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble6", src: imgpath+"new/marble06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble7", src: imgpath+"new/marble07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble8", src: imgpath+"new/marble07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble9", src: imgpath+"new/marble09.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble10", src: imgpath+"new/marble10.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble11", src: imgpath+"new/marble11.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble12", src: imgpath+"new/marble12.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble13", src: imgpath+"new/marble05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "marble14", src: imgpath+"new/marble06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src: imgpath+"new/big_arrow.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		//scoring.init(10);
		templateCaller();
	}
	//initialize
	init();

	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/
	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
 	}

	 /*values in this array is same as the name of images of eggs in image folder*/

	 //create eggs
	 var testin = new NumberTemplate();

	 	//eggTemplate.eggMove(countNext);
 		testin.init(10);

	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);
		texthighlight($board);
		put_image(content, countNext);
		var updateScore = 0;
		// put_image_sec(content, countNext);
	 	var testcount = 0;

	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);

	 	/*generate question no at the beginning of question*/
	 	testin.numberOfQuestions();

	 	/*for randomizing the options*/
		function randomize(parent){
			var parent = $(parent);
			var divs = parent.children();
			while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}
}
	 	/*======= SCOREBOARD SECTION ==============*/
	 	/*random scoreboard eggs*/
	 	//var i = Math.floor(Math.random() * imageArray.length);
	 	//var randImg = imageArray[i];
	 	var ansClicked = false;
	 	var wrngClicked = false;
		var updateScore = 0;

		var appendCount = 0;
		function mblAppender(fstContainerClass, Col, rows){
			appendCount++;
			for(var i = 0; i<Col; i++){
				imgId = "marble"+(i+1);
				className= "mbl-"+countNext;
				$(fstContainerClass).append("<img class = 'marbles "+className+"' src ='"+preload.getResult(imgId).src+"'/>");
			}
			switch (Col) {
				case 7:
					$(".marbles").css("width","12%");
					rows == 5?$(".marbles").css("height","18%"):
					rows == 3?$(".marbles").css("height","20%"):
					rows == 2?$(".marbles").css("height","20%"):"";
					rows == 4?$(".marbles").css("height","20%"):"";
				break;
				case 6:
					$(".marbles").css("width","14%");
					rows == 5?$(".marbles").css("height","20%"):
					rows == 3?$(".marbles").css("height","23%"):
					rows == 2?$(".marbles").css("height","25%"):
					rows == 4?$(".marbles").css("height","23%"):"";
				break;
				case 5:
					$(".marbles").css("width","15%");
					rows == 2?$(".marbles").css("height","25%"):
					rows == 3?$(".marbles").css("height","24%"):
					rows == 4?$(".marbles").css("height","23%"):
					rows == 5?$(".marbles").css("height","18%"):"";
				break;
				case 4:
					$(".marbles").css("width","19%");
					rows == 2?$(".marbles").css("height","30%"):
					rows == 3?$(".marbles").css("height","30%"):
					rows == 4?$(".marbles").css({"height":"24.5%","top":"-4%"}):
					rows == 5?$(".marbles").css({"height":"21%","top":"-7%","width":"19%"}):"";
				break;
				default:

			}
			if(appendCount<rows){
				mblAppender(fstContainerClass, Col, rows);
			}
		}
		$(".question").prepend(countNext+1+". ");
	 	switch(countNext){
	 	case 0:
			column =  Math.floor(Math.random() * (7 - 5 +1)) + 5;
			row =  Math.floor(Math.random() * (4 - 2 +1)) + 2;
			mblAppender(".marblesContainerFirst", column, row);
			$(".optionscontainer:eq(0)").children().html(column+" x "+row);
			$(".optionscontainer:eq(1)").children().html(row+" x "+(column+1));
			$(".optionscontainer:eq(2)").children().html((row*2)+" x "+(column+3));
			$(".optionscontainer:eq(3)").children().html((column+3)+" x "+(row+1));
			randomize(".optionsdiv");
		break;
	 	case 1:
			// column = 4;
			column = Math.floor(Math.random() * (7 - 4 +1)) + 4;
			// row = 4;
			row = Math.floor(Math.random() * (4 - 2 +1)) + 2;
			mblAppender(".marblesContainerFirst", column, row);
			$(".optionscontainer:eq(0)").children().html(column+" x "+row+"="+column*row);
			$(".optionscontainer:eq(1)").children().html(column+" + "+row+"="+(column+row));
			$(".optionscontainer:eq(2)").children().html((row*2)+" x "+(row*2)+" = "+Math.pow((row*2),2));
			$(".optionscontainer:eq(3)").children().html((column+1)+" + "+(column+1)+" + "+(column+1)+" = "+((column+1)*3));
			randomize(".optionsdiv");
		break;
	 	case 2:
			column = Math.floor(Math.random() * (7 - 5 +1)) + 4;

			row = Math.floor(Math.random() * (3 - 2 +1)) + 2;
			mblAppender(".marblesContainerFirst", column, row);
			$(".qnwrd").html(column+" x "+row)
			var rightAns=[];
			var wrongAns_1=[];
			var wrongAns_2=[];
			for(var i=0; i<row; i++){
				i==(row-1)?rightAns.push(column):rightAns.push(column+" +");
			}
			$(".optionscontainer:eq(0)").children().html(rightAns.join(" "));

			for(var i=0; i<(column-1); i++){
				i==(column-2)?wrongAns_1.push(row):wrongAns_1.push(row+" +");
			}
			$(".optionscontainer:eq(1)").children().html(wrongAns_1.join(" "));

			for(var i=0; i<row; i++){
				i==(row-1)?wrongAns_2.push(column):wrongAns_2.push(column+" x");
			}
			$(".optionscontainer:eq(2)").children().html(wrongAns_2.join(" "));
			$(".optionscontainer:eq(3)").children().html(column+" + "+row);
			randomize(".opnDiv3");
		break;
		case 3:
			fst = Math.floor(Math.random() * (15 - 10 +1)) + 10;
			sec = Math.floor(Math.random() * (15 - 10 +1)) + 10;
			$(".fullBox").addClass("hovered");
			$(".qnwrd").html(fst+" x "+sec)
			var fstBoxArray=[[".rsf",".rtf",".ltf",".mdr_f",".mdl_f"],
											 [sec, "10", fst-10,sec*10,(fst-10)*sec]
										 ];
			 var secBoxArray=[[".rss",".rts",".lts",".mdr_s",".mdl_s"],
												 [sec, "10", sec-10,sec*10,(sec-10)*10]
											 ];
			 var thirdBoxArray=[[".rst",".rtt",".ltt",".mdr_t",".mdl_t"],
												 [sec, "10", fst-10,(fst-10)*sec, sec*10]
											 ];
			 var fourthBoxArray=[[".rsfth",".rtfth",".ltfth",".mdr_fth",".mdl_fth"],
												 		[sec, "10", fst-10,sec*10,fst*(sec-1)]
											 ];
			box_html(fstBoxArray, 5);
			box_html(secBoxArray, 5);
			box_html(thirdBoxArray, 5);
		 	box_html(fourthBoxArray, 5);
			boxClick();
		break;
		case 4:
			column = Math.floor(Math.random() * (15 - 11 +1)) + 11;
			row = Math.floor(Math.random() * (15 - 11 +1)) + 11;
			if(row>column){
				column=(column*row)/column;
				row=(column*row)/row;
			}
		 if (column==row) {
				if(row==15){row=row-1;}else{column=column+1;}
			}
			$(".qnwrd").html(column +"x"+ row)
			$(".optionscontainer:eq(0)").children().html(data.string.first+column+data.string.mul_sgn+ (row-10)+data.string.then+column+data.string.mul_sgn+ "10"+ data.string.add_ans);
			$(".optionscontainer:eq(1)").children().html(data.string.first+row+data.string.mul_sgn+(column-10)+data.string.then+row+data.string.mul_sgn+ "20"+ data.string.add_ans);
			$(".optionscontainer:eq(2)").children().html(data.string.first+column+data.string.mul_sgn+(row-1)+data.string.then+column+data.string.mul_sgn+ "10"+ data.string.mul_ans);
			$(".optionscontainer:eq(3)").children().html(data.string.first+row+data.string.mul_sgn+(column-10)+data.string.then+row+data.string.mul_sgn+ "20"+ data.string.mul_ans);
			randomize(".opnDiv4");
		break;
		case 5:
			fst = Math.floor(Math.random() * (15 - 11 +1)) + 11;
			sec = Math.floor(Math.random() * (15 - 10 +1)) + 10;
			$(".qnwrd").html(fst+" x "+sec);
			var fstBoxArray=[[".rsf",".rtf",".ltf",".mdr_f",".mdl_f"],
											 [sec, "10", fst-10,sec*10,(fst-10)*sec]
										 ];
		  box_html(fstBoxArray, 5);
			$(".optionscontainer:eq(0)").children().html((sec*(fst-10))+" + "+(sec*10)+" = "+((sec*(fst-10))+(sec*10)));
			$(".optionscontainer:eq(1)").children().html((sec*(sec-10)*11)+" + 10 = "+((sec*(sec-10)*11)+10));
			$(".optionscontainer:eq(2)").children().html((sec*10)+" x "+(sec*(fst-10)) +" = "+((sec*10)*(sec*(fst-10))));
			$(".optionscontainer:eq(3)").children().html(fst*10+" + "+sec+" = "+(fst*10+sec));
			randomize(".opnDiv3");
		break;
		case 6:
			fst = Math.floor(Math.random() * (15 - 11 +1)) + 11;
			$(".qnwrd:eq(0)").html(fst);
			$(".qnwrd:eq(1)").html(fst);
			$(".rsf, .rtf").html(fst);
			$(".mdr_f").html(data.string.qm_mark);
			$(".arrow, .bxSec, .bxthird").hide(0);
			$(".arfst, .bxSec").delay(500).fadeIn(1000);
			$(".arsec, .bxthird").delay(1500).fadeIn(1000);
			var secBoxArray=[[".rss",".rts",".lts"  ,".mdr_s",".mdl_s"    ,".rss_s",".mdr_s_s"  , ".mdl_t_s"],
												 ["10", "10" ,  fst-10, 10*10 ,(fst-10)*10  ,fst-10  ,(fst-10)*10 ,(fst-10)*(fst-10)]
											 ];

			var thirdBoxArray=[[".rst",".rtt",".ltt",".mdr_t",".mdl_t"],
												 [fst, "10", fst-10,fst*10,(fst-10)*fst]
											 ];
		  box_html(secBoxArray, 8);
		  box_html(thirdBoxArray, 5);
			$(".optionscontainer:eq(0)").children().html((fst*(fst-10))+" + "+fst*10+" = "+((fst*(fst-10))+(fst*10)));
			$(".optionscontainer:eq(1)").children().html(fst+" + "+(fst*(fst-10))+" = "+((fst*(fst-10))+fst));
			$(".optionscontainer:eq(2)").children().html(fst+" + "+((fst+1)*10)+" = "+(fst+((fst+1)*10)));
			$(".optionscontainer:eq(3)").children().html((fst*fst)+" + "+fst*10+" = "+((fst*fst)+(fst*10)));
			randomize(".opnDiv3");
		break;
		case 7:
			column = Math.floor(Math.random() * (30 - 11 +1)) + 11;
			row = Math.floor(Math.random() * (20 - 11 +1)) + 11;
			if(row>column){
				column=(column*row)/column;
				row=(column*row)/row;
			}
		 if (column==row) {
				if(row==15){row=row-1;}else{column=column+1;}
			}
			$(".qnwrd:eq(0)").html(column);
			$(".qnwrd:eq(1)").html(row);
			// $(".question").html((countNext+1)+". There are "+row+" rooms. Each room has "+column+" students. How do you solve this problem?")
			$(".optionscontainer:eq(0)").children().html(data.string.first +column+data.string.mul_sgn+ (row-10)+ data.string.then +column+data.string.mul_sgn+" 10"+data.string.add_ans);
			$(".optionscontainer:eq(1)").children().html(data.string.first +column+data.string.mul_sgn+ (row-10)+ data.string.then +column+data.string.mul_sgn+" 1"+data.string.add_ans);
			$(".optionscontainer:eq(2)").children().html(data.string.first +column+data.string.mul_sgn+ 1+ data.string.then +column+data.string.mul_sgn+" 30"+data.string.mul_ans);
			$(".optionscontainer:eq(3)").children().html(data.string.first +column+data.string.mul_sgn+ 10+ data.string.then +column+" + "+(row-10)+data.string.mul_ans);
			randomize(".opnDiv4");
		break;
		case 8:
			fst = Math.floor(Math.random() * (49 - 41 +1)) + 41;
			sec = Math.floor(Math.random() * (15 - 10 +1)) + 10;
			$(".qnwrd:eq(0)").html(fst+" x "+sec);
			$(".qnwrd:eq(1)").html(fst+" x "+sec);
			$(".rsf").html(sec);
			$(".rtf").html(fst);
			$(".mdr_f").html(fst*sec);
			$(".arrow, .bxSec, .bxthird").hide(0);
			$(".s9_arfst, .bxSec").delay(500).fadeIn(1000);
			$(".s9_arsec, .bxthird").delay(1500).fadeIn(1000);
			var secBoxArray=[[".rss",".rts"                    ,".lts"  ,".mdr_s"                     ,".rss_s",".mdr_s_s"  , ".mdl_t_s"],
												 ["10", (Math.floor(fst/10)*10) ,  fst%10 , (Math.floor(fst/10)*10)*10  ,sec-10  ,(sec-10)*40 ,(sec-10)*(fst%10)]
											 ];
		  box_html(secBoxArray, 7);
			var thirdBoxArray=[[".rst",".rtt"                  ,".ltt" ,".mdl_t"],
												 [sec   , (Math.floor(fst/10)*10),fst%10 ,sec*(fst%10)]
						];
			box_html(thirdBoxArray, 5);
			$(".mdl_s9").html(eval('data.string.s9fst'));
			$(".mdr_tS9").html(eval('data.string.s9sec'));
			input_box('.inputs', 3, null);
			$(".check").click(function(){
				var ans1 =$(".mdl_s9").find("input[type='text']").val();
				var ans2 =$(".mdr_tS9").find("input").val();
				if((ans1 == 10*(fst%10))&&(ans2 == sec*(Math.floor(fst/10)*10))){
					$("#s9ip1, #s9ip2").css({"background" : "#00ff00", "border":"3px solid #DEEF3C", "pointer-events":"none"});
					if(!test){
						!wrngClicked?testin.update(true):testin.update(false);
					}
					play_correct_incorrect_sound(1);
					$nextBtn.show(0);
				}
				else if ((ans1 == 10*(fst%10))&&(ans2 != sec*(Math.floor(fst/10)*10))) {
					$("#s9ip1").css({"background" : "#00ff00", "border":"3px solid #DEEF3C", "pointer-events":"none"});
					$("#s9ip2").css({"background" : "#ff0000", "border":"3px solid #980000"});
					test =true;
					wrngClicked = true;
					play_correct_incorrect_sound(0);
				}
				else if ((ans1 != 10*(fst%10))&&(ans2 == sec*(Math.floor(fst/10)*10))) {
					$("#s9ip2").css({"background" : "#00ff00", "border":"3px solid #DEEF3C", "pointer-events":"none"});
					$("#s9ip1").css({"background" : "#ff0000", "border":"3px solid #980000",});
					test =true;
					wrngClicked = true;
					play_correct_incorrect_sound(0);
				}
				else{
					$("#s9ip1, #s9ip2").css({"background" : "#ff0000", "border":"3px solid #980000",});
					test =true;
					play_correct_incorrect_sound(0);
					wrngClicked = true;
				}
			});
			randomize(".opnDiv3");
		break;
		case 9:
			fst = Math.floor(Math.random() * (49 - 41 +1)) + 41;
			sec = Math.floor(Math.random() * (15 - 10 +1)) + 10;
			$(".qnwrd").html(fst+" x "+sec);
			$(".rsf").html(sec);
			$(".rtf").html(fst);
			$(".mdr_f").html("?");
			$(".arrow, .bxSec, .bxthird").hide(0);
			$(".arfst, .bxSec").delay(500).fadeIn(1000);
			$(".arsec, .bxthird").delay(1500).fadeIn(1000);
			var secBoxArray=[[".rss",".rts"                    ,".lts"  ,".mdr_s"                    ,".mdl_s"    ,".rss_s",".mdr_s_s"  , ".mdl_t_s"],
												 ["10", (Math.floor(fst/10)*10) ,  fst%10 , (Math.floor(fst/10)*10)*10 ,10*(fst%10) ,sec-10  ,(sec-10)*40 ,(sec-10)*(fst%10)]
											 ];
		  box_html(secBoxArray, 8);
			var thirdBoxArray=[[".rst",".rtt"                  ,".ltt" ,".mdr_t"                         ,".mdl_t"],
												 [sec   , (Math.floor(fst/10)*10),fst%10 ,sec*(Math.floor(fst/10)*10)      ,sec*(fst%10)]
						];
			box_html(thirdBoxArray, 5);
			$(".optionscontainer:eq(0)").children().html("("+sec+"x"+(Math.floor(fst/10)*10)+") +"+"("+sec+"x"+fst%10+")");
			$(".optionscontainer:eq(1)").children().html("(10 x"+(Math.floor(fst/10)*10)+") x (10 x "+Math.floor((sec/10))+")");
			$(".optionscontainer:eq(2)").children().html("("+sec+"+"+(Math.floor(fst/10)*10)+") x "+"("+sec+"+"+fst%10+")");
			$(".optionscontainer:eq(3)").children().html(sec+"+"+sec+"+"+sec+"+"+sec);
			randomize(".opnDiv3");
		break;
	 	}

		function box_html(array, ltrNum){
		 for (var i=0; i<ltrNum; i++){
			 $(array[0][i]).html(array[1][i]);
		 }
		}

	function correct(boxClass, updScr, corCount){
				$(boxClass).css({"background" : "#00ff00"});
				$(boxClass).siblings().css({"pointer-events" : "none"});
				testin.update(true);
				$(".submit").css({"pointer-events" : "none"});
				play_correct_incorrect_sound(1);
				$nextBtn.show(0);
	}
	function incorrect(boxClass){
		$(boxClass).css({"background" : "#ff0000"});
		play_correct_incorrect_sound(0);
	}
	function secCorrect(boxClass, corCount){
		$(boxClass).css({"border" : "4px solid #00ff00"});
		$(boxClass).siblings().css({"pointer-events" : "none"});
			testin.update(true);
		play_correct_incorrect_sound(1);
		$nextBtn.show(0);
	}
	function boxClick(){
		$(".fullBox").click(function(){
			if($(this).hasClass("correct")){
				!wrngClicked?testin.update(true):testin.update(false);
				play_correct_incorrect_sound(1);
				$(this).children(".corctopt").show(0);
				$(".fullBox").css("pointer-events","none");
				$nextBtn.show(0);
			}
			else{
				play_correct_incorrect_sound(0);
				$(this).css("pointer-events", "none");
				$(this).children(".wrngopt").show(0);
				wrngClicked = true;
				testin.update(false);
			}
		});
	}
	//handle mcq
	$(".buttonsel").click(function(){
		$(this).removeClass('forhover');
			if(ansClicked == false){
				/*class 1 is always for the right answer. updates scoreboard and disables other click if
				right answer is clicked*/
				if($(this).hasClass("correct")){
					if(wrngClicked == false){
						testin.update(true);
					}
					play_correct_incorrect_sound(1);
					$(this).css({
						"background": "#bed62fff",
						"border":"5px solid #deef3c",
						"color":"#000",
						'pointer-events': 'none'
					});
					$(this).siblings(".corctopt").show(0);
					$('.buttonsel').css("pointer-events","none");
					$('.buttonsel').removeClass('forhover forhoverimg');
					$('.buttonsel').removeClass('clock-hover');
					$nextBtn.show(0);
					// navigationcontroller();
					ansClicked = true;
					// if(countNext != $total_page)
					// $nextBtn.show(0);

				}
				else{
					testin.update(false);
					play_correct_incorrect_sound(0);
					$(this).css({
						"background":"#FF0000",
						"border":"5px solid #980000",
						"color":"#000",
						'pointer-events': 'none'
					});
					$(this).siblings(".wrngopt").show(0);
					wrngClicked = true;
				}
			}
		});

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					// console.log(selector);
					$(selector).attr('src',image_src);
				}
			}
		}
	}

	function put_image_sec(content, count){
		if(content[count].arrow_nav[0].hasOwnProperty('imageblock')){
			var imagblockVal = content[count].arrow_nav[0];
			for(var j=0;j<imagblockVal.imageblock.length; j++){
							var imageblock = imagblockVal.imageblock[j];
							if(imageblock.hasOwnProperty('imagestoshow')){
								var imageClass = imageblock.imagestoshow;
								for(var i=0; i<imageClass.length; i++){
									var image_src = preload.getResult(imageClass[i].imgid).src;
									//get list of classes
									var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
									var selector = ('.'+classes_list[classes_list.length-1]);
									$(selector).attr('src', image_src);
								}
							}
    }

		}
	}
	 	/*======= SCOREBOARD SECTION ==============*/
	 }

	 function templateCaller(){
		/*always hide next and previousloadimage navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators


	}

	// first call to template caller
	// templateCaller(); loadimage($(".myimg3").find("img"),$(".myimg3").find("p"),preload.getResult('ex1typ3qn'+quesNo).src,eval('data.string.ex2typ1op'+quesNo));


	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
 	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	function loadimage(imgrep,imgtext,imgsrc,quesNo){
		imgrep.attr("src",imgsrc);
		imgtext.text(quesNo);
	}

		/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
		 * event.key reurns the value of key pressed by user and it is converted to integer
		 * event.target gets the element where event is occuring (usually a div)
		 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
		 * input_class and button_classes should be something like '.class_name'
		 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
		 */
		function input_box(input_class, max_number, button_class) {
			$(input_class).keydown(function(event) {
				var charCode = (event.which) ? event.which : event.keyCode;
				/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
				if (charCode === 13 && button_class != null) {
					$(button_class).trigger("click");
				}
				var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
				//check if user inputs del, backspace or arrow keys
				if (!condition) {
					return true;
				}
				//check if user inputs more than one '.'
				if ((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
					return false;
				}
				//check . and 0-9 separately after checking arrow and other keys
				if ((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110) {
					return false;
				}
				//check max no of allowed digits
				if (String(event.target.value).length >= max_number) {
					return false;
				}
				return true;
			});
		}
/*=====  End of Templates Controller Block  ======*/
});

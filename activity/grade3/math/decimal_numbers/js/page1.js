var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sound/"+$lang+"/";

var content=[
	{
		// slide0
		contentnocenteradjust: true,
		contentblockadditionalclass: "add_contentblk",
		extratextblock:[{
			textclass: "lesson-title",
			textdata: data.string.lesson_title
		}],
		imageblockadditionalclass:'bg-full',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'cover'
				}
			]
		}],
	},{
		// slide1
		contentnocenteradjust: true,
		contentblockadditionalclass: "add_contentblk",
		imageblock:[{
			imagestoshow:[{
				imgclass: "img1_left",
				imgsrc: "",
				imgid: "boy1"
			},{
				imgclass: "img2_right",
				imgsrc: "",
				imgid: "treeapple"
			}],
			imagelabels: [{
				imagelabelclass: "description1_right",
				datahighlightflag: true,
				imagelabeldata: data.string.p1_s0
			}]
		}]
	},{
		// slide2
		contentnocenteradjust: true,
		contentblockadditionalclass: "add_contentblk",
		imageblock:[{
			imagestoshow:[{
				imgclass: "img1_right",
				imgsrc: "",
				imgid: "girl1"
			},{
				imgclass: "img2_left",
				imgsrc: "",
				imgid: "fourcookies"
			}],
			imagelabels: [{
				imagelabelclass: "description1_left",
				datahighlightflag: true,
				imagelabeldata: data.string.p1_s1
			}]
		}]
	},{
		// slide3
		contentnocenteradjust: true,
		contentblockadditionalclass: "add_contentblk",
		imageblock:[{
			imagestoshow:[{
				imgclass: "img1_left",
				imgsrc: "",
				imgid: "girl2"
			},{
				imgclass: "img2_right",
				imgsrc: "",
				imgid: "chocolateb"
			}],
			imagelabels: [{
				imagelabelclass: "description1_right",
				datahighlightflag: true,
				imagelabeldata: data.string.p1_s2
			}]
		}]
	},{
		// slide4
		contentnocenteradjust: true,
		contentblockadditionalclass: "add_contentblk",
		imageblock:[{
			imagestoshow:[{
				imgclass: "img1_left",
				imgsrc: "",
				imgid: "monkey"
			},{
				imgclass: "half_apple",
				imgsrc: "",
				imgid: "halfapple01"
			}],
			imagelabels: [{
				imagelabelclass: "description2",
				datahighlightflag: true,
				imagelabeldata: data.string.p1_s3
			},{
				imagelabelclass: "description2",
				datahighlightflag: true,
				imagelabeldata: data.string.p1_s4
			}]
		}]
	},{
		// slide5
		contentnocenteradjust: true,
		contentblockadditionalclass: "add_contentblk",
		uppertextblock:[{
			textclass: "description_title2",
			datahighlightflag: true,
			datahighlightcustomclass: "mustard_highlight2",
			textdata: data.string.p1_s5
		}],
		imageblock:[{
			imageblockclass: "three_divs bg_half_apple",

			imagelabels: [{
				imagelabelclass: "description3_a_2",
				splitintofractionsflag: true,
				imagelabeldata: data.string.p1_s6
			}]
		},{
			imageblockclass: "three_divs",
			imagestoshow:[{
				imgclass: "img2",
				imgsrc: "",
				imgid: "cookies01"
			}],
			imagelabels: [{
				imagelabelclass: "description3_a_2",
				splitintofractionsflag: true,
				imagelabeldata: data.string.p1_s7
			}]
		},{
			imageblockclass: "three_divs",
			imagestoshow:[{
				imgclass: "img3",
				imgsrc: "",
				imgid: "chocolate01"
			}],
			imagelabels: [{
				imagelabelclass: "description3_a_2",
				splitintofractionsflag: true,
				imagelabeldata: data.string.p1_s8
			}]
		}]
	},{
		// slide6
		contentnocenteradjust: true,
		contentblockadditionalclass: "add_contentblk",
		uppertextblock:[{
			textclass: "description_title2_2",
			textdata: data.string.p1_s17
		}],
		imageblock:[{
			imageblockclass: "three_divs bg_half_apple",

			imagelabels: [{
				imagelabelclass: "description3_a_2",
				splitintofractionsflag: true,
				datahighlightflag: true,
				datahighlightcustomclass: "pink_highlight2",
				imagelabeldata: data.string.p1_s9
			}]
		},{
			imageblockclass: "three_divs",
			imagestoshow:[{
				imgclass: "img2",
				imgsrc: "",
				imgid: "cookies01"
			}],
			imagelabels: [{
				imagelabelclass: "description3_a_2",
				splitintofractionsflag: true,
				datahighlightflag: true,
				datahighlightcustomclass: "pink_highlight2",
				imagelabeldata: data.string.p1_s10
			}]
		},{
			imageblockclass: "three_divs",
			imagestoshow:[{
				imgclass: "img3",
				imgsrc: "",
				imgid: "chocolate01"
			}],
			imagelabels: [{
				imagelabelclass: "description3_a_2",
				splitintofractionsflag: true,
				datahighlightflag: true,
				datahighlightcustomclass: "pink_highlight2",
				imagelabeldata: data.string.p1_s11
			}]
		}]
	},
	{
		// slide7
		contentnocenteradjust: true,
		contentblockadditionalclass: "add_contentblk",
		uppertextblock:[{
			textclass: "description_title2_2",
			textdata: data.string.p1_s17
		}],
		imageblock:[{
			imageblockclass: "three_divs bg_half_apple",

			imagelabels: [{
				imagelabelclass: "description3_a_2",
				splitintofractionsflag: true,
				datahighlightflag: true,
				datahighlightcustomclass: "pink_highlight2",
				imagelabeldata: data.string.p1_s9
			}]
		},
		{
			imageblockclass: "three_divs",
			imagestoshow:[{
				imgclass: "img2",
				imgsrc: "",
				imgid: "cookies01"
			}],
			imagelabels: [{
				imagelabelclass: "description3_a_2",
				splitintofractionsflag: true,
				datahighlightflag: true,
				datahighlightcustomclass: "pink_highlight2",
				imagelabeldata: data.string.p1_s10
			}]
		},{
			imageblockclass: "three_divs",
			imagestoshow:[{
				imgclass: "img3",
				imgsrc: "",
				imgid: "chocolate01"
			}],
			imagelabels: [{
				imagelabelclass: "description3_a_2",
				splitintofractionsflag: true,
				datahighlightflag: true,
				datahighlightcustomclass: "pink_highlight2",
				imagelabeldata: data.string.p1_s11
			}]
		}],
		lowertextblockadditionalclass: "additional_ltb",
		lowertextblock:[{
			textclass: "description_title2",
			datahighlightcustomclass: "blue_pink_highlight",
			datahighlightflag: true,
			textdata: data.string.p1_s12
		}],
	}
	,{
		// slide8
		contentnocenteradjust: true,
		contentblockadditionalclass: "add_contentblk",
		uppertextblock:[{
			textclass: "description_center",
			textdata: data.string.p1_s13
		},{
			textclass: "description_a",
			datahighlightflag: true,
			datahighlightcustomclass: "pink_highlight",
			textdata: data.string.p1_s9
		},{
			textclass: "description_c",
			datahighlightflag: true,
			datahighlightcustomclass: "pink_highlightb",
			textdata: data.string.p1_s10
		},{
			textclass: "description_e",
			datahighlightflag: true,
			datahighlightcustomclass: "pink_highlightc",
			textdata: data.string.p1_s14
		},{
			textclass: "description_d",
			datahighlightflag: true,
			datahighlightcustomclass: "pink_highlightd",
			textdata: data.string.p1_s15
		},{
			textclass: "description_b",
			datahighlightflag: true,
			datahighlightcustomclass: "pink_highlighte",
			textdata: data.string.p1_s16
		}]
	}

];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

  loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "cover", src: imgpath+"decimal.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy1", src: imgpath+"ramu.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl1", src: imgpath+"munu.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl2", src: imgpath+"rinku.png", type: createjs.AbstractLoader.IMAGE},
			{id: "monkey", src: imgpath+"monkey.png", type: createjs.AbstractLoader.IMAGE},
			{id: "treeapple", src: imgpath+"treeapple.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fourcookies", src: imgpath+"fourcookies.png", type: createjs.AbstractLoader.IMAGE},
			{id: "chocolatebar", src: imgpath+"chocolatebar.png", type: createjs.AbstractLoader.IMAGE},
			{id: "halfapple01", src: imgpath+"halfapple01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "10apples", src: imgpath+"10apples.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cookies01", src: imgpath+"cookies01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "chocolate01", src: imgpath+"chocolate01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "chocolateb", src: imgpath+"chocolate.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
			{id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
			{id: "s1_p4", src: soundAsset+"s1_p4.ogg"},
			{id: "s1_p5", src: soundAsset+"s1_p5.ogg"},
			{id: "s1_p6", src: soundAsset+"s1_p6.ogg"},
			{id: "s1_p7", src: soundAsset+"s1_p7.ogg"},
			{id: "s1_p8", src: soundAsset+"s1_p8.ogg"},
			{id: "s1_p9", src: soundAsset+"s1_p9.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_1');
		// current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// setTimeout(function(){
			// 	if(countNext == $total_page - 1)
			// 		islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
			// }, 2500);
		}
   }
   /*=====  End of user navigation controller function  ======*/

  /*===== This function splits the string in data into convential fraction used in mathematics =====*/
    function splitintofractions($splitinside){
   		typeof $splitinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
       	if($splitintofractions.length > 0){
       		$.each($splitintofractions, function(index, value){
	        	$this = $(this);
	        	var tobesplitfraction = $this.html();
	        	if($this.hasClass('fraction')){
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
	        	}else{
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
	        	}


				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
	        	$this.html(tobesplitfraction);
	        });
       	}
   	}
   	/*===== split into fractions end =====*/


   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
 var timeoutcontroller;
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    splitintofractions($board);
	    vocabcontroller.findwords(countNext);
		put_image(content, countNext);

			$nextBtn.hide(0);
			$prevBtn.hide(0);
		switch(countNext) {
			case 0:
				sound_player("s1_p"+(countNext+1),1);
				break;
			case 1:
				sound_player("s1_p"+(countNext+1),1);
				// sound_player('sound_1');
				$(".dialog2_b").show(0);
				break;
			case 2:
				sound_player("s1_p"+(countNext+1),1);
				$(".img2_left").css("width", "20%");
				// sound_player('sound_1');
				$(".dialog1").show(0);
				break;
			case 3:
				sound_player("s1_p"+(countNext+1),1);
				// sound_player('sound_1');
				createjs.Sound.play("aeroplane", {loop: -1});
				$(".dialog2").show(0);
				break;
			case 4:
				sound_player("s1_p"+(countNext+1),1);
				// sound_player('sound_1');
				$(".coverboardfull").addClass("scale_div");
				timeoutcontroller = setTimeout(function(){
					$(".dialog1_b").show(0);
				}, 1100);
				break;
			case 5:
			case 6:
				sound_player("s1_p"+(countNext+1),1);
				$(".imageblock").css("margin-top", "10%");
			case 7:
				sound_player("s1_p"+(countNext+1),1);
				$(".bg_half_apple").css("background-image", 'url('+ preload.getResult("10apples").src +')');
				break;
			case 8:
				sound_player("s1_p"+(countNext+1),1);
			break;
			default:
				break;
		}

		// splitintofractions($(".fractionblock"));
  }

/*=====  End of Templates Block  ======*/


function nav_button_controls(delay_ms){
	timeoutvar = setTimeout(function(){
		if(countNext==0){
			$nextBtn.show(0);
		} else if( countNext>0 && countNext == $total_page-1){
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		} else{
			$prevBtn.show(0);
			$nextBtn.show(0);
		}
	},delay_ms);
}
	function sound_player(sound_id, nextBtnFlg){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nextBtnFlg?nav_button_controls():"";
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			for(var j = 0; j < content[count].imageblock.length; j++){
				var imageblock = content[count].imageblock[j];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}

		if(content[count].hasOwnProperty('containsdialog')){
				var containsdialog = content[count].containsdialog;
				for(var i=0; i<containsdialog.length; i++){
					var image_src = preload.getResult(containsdialog[i].imgid).src;
					//get list of classes
					var classes_list = containsdialog[i].dialogimageclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
		}
	}

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		clearTimeout(timeoutcontroller);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

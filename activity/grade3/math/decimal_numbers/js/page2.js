var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sound/"+$lang+"/";

var content=[
	{
		// slide0
		contentnocenteradjust: true,
		contentblockadditionalclass: "add_contentblk",
		uppertextblock:[{
			textclass: "description_title2",
			datahighlightflag: true,
			datahighlightcustomclass: "pink_highlight",
			textdata: data.string.p2_s0_a
		}],
		imageblock:[{
			imageblockclass: "container_apple",
			// imagestoshow:[{
				// imgclass: "apple_class",
				// imgsrc: "",
				// imgid: "apple"
			// }]
		},{
			imageblockclass: "container_line",
			imagestoshow:[{
				imgclass: "line_class",
				imgsrc: "",
				imgid: "line01"
			}],
			imagelabels: [{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s1
			},{
				imagelabelclass: "label_des",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s11
			}]
		}]
	},{
		// slide1
		contentnocenteradjust: true,
		contentblockadditionalclass: "add_contentblk",
		uppertextblock:[{
			textclass: "description_title2",
			datahighlightflag: true,
			datahighlightcustomclass: "pink_highlight",
			textdata: data.string.p2_s0_a
		}],
		imageblock:[{
			imageblockclass: "container_apple container_apple_animate",
			// imagestoshow:[{
				// imgclass: "apple_class",
				// imgsrc: "",
				// imgid: "apple"
			// }]
		},{
			imageblockclass: "container_line",
			imagestoshow:[{
				imgclass: "line_class",
				imgsrc: "",
				imgid: "line02"
			}],
			imagelabels: [{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s1
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s2
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s3
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s4
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s5
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s6
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s7
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s8
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s9
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s10
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s11
			}]
		}]
	},{
		// slide 2
		contentnocenteradjust: true,
		contentblockadditionalclass: "add_contentblk",
		uppertextblock:[{
			textclass: "description_title2",
			datahighlightflag: true,
			datahighlightcustomclass: "pink_highlight",
			textdata: data.string.p2_s0_b
		}],
		imageblock:[{
			imageblockclass: "container_fraction",
			imagelabels: [{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			}]
		},{
			imageblockclass: "container_line2",
			imagelabels: [{
				imagelabelclass: "label_des",
				splitintofractionsflag: true,
				imagelabeldata: data.string.p2_s13
			},{
				imagelabelclass: "label_des",
				splitintofractionsflag: true,
				imagelabeldata: data.string.p2_s14
			},{
				imagelabelclass: "label_des",
				splitintofractionsflag: true,
				imagelabeldata: data.string.p2_s15
			},{
				imagelabelclass: "label_des",
				splitintofractionsflag: true,
				imagelabeldata: data.string.p2_s16
			},{
				imagelabelclass: "label_des",
				splitintofractionsflag: true,
				imagelabeldata: data.string.p2_s17
			},{
				imagelabelclass: "label_des",
				splitintofractionsflag: true,
				imagelabeldata: data.string.p2_s18
			},{
				imagelabelclass: "label_des",
				splitintofractionsflag: true,
				imagelabeldata: data.string.p2_s19
			},{
				imagelabelclass: "label_des",
				splitintofractionsflag: true,
				imagelabeldata: data.string.p2_s20
			},{
				imagelabelclass: "label_des",
				splitintofractionsflag: true,
				imagelabeldata: data.string.p2_s21
			},{
				imagelabelclass: "label_des",
				splitintofractionsflag: true,
				imagelabeldata: data.string.p2_s22
			}]
		}]
	},{
		//page 3
		contentblockadditionalclass: "add_contentblk",
		uppertextblock:[{
			textclass: "description_title2",
			textdata: data.string.p2_s23
		}]
	},{
		//page 4
		contentnocenteradjust: true,
		contentblockadditionalclass: "add_contentblk",
		uppertextblock:[{
			textclass: "description_title2",
			datahighlightflag: true,
			datahighlightcustomclass: "pink_highlight",
			textdata: data.string.p2_s24
		}],
		imageblock:[{
			imageblockclass: "container_fraction",
			imagelabels: [{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			}]
		},{
			imageblockclass: "container_line2",
			imagelabels: [{
				imagelabelclass: "label_des",
				splitintofractionsflag: true,
				imagelabeldata: data.string.p2_s13
			},{
				imagelabelclass: "label_des",
				splitintofractionsflag: true,
				imagelabeldata: data.string.p2_s14
			},{
				imagelabelclass: "label_des",
				splitintofractionsflag: true,
				imagelabeldata: data.string.p2_s15
			},{
				imagelabelclass: "label_des",
				splitintofractionsflag: true,
				imagelabeldata: data.string.p2_s16
			},{
				imagelabelclass: "label_des",
				splitintofractionsflag: true,
				imagelabeldata: data.string.p2_s17
			},{
				imagelabelclass: "label_des",
				splitintofractionsflag: true,
				imagelabeldata: data.string.p2_s18
			},{
				imagelabelclass: "label_des",
				splitintofractionsflag: true,
				imagelabeldata: data.string.p2_s19
			},{
				imagelabelclass: "label_des",
				splitintofractionsflag: true,
				imagelabeldata: data.string.p2_s20
			},{
				imagelabelclass: "label_des",
				splitintofractionsflag: true,
				imagelabeldata: data.string.p2_s21
			},{
				imagelabelclass: "label_des",
				splitintofractionsflag: true,
				imagelabeldata: data.string.p2_s22
			}]
		},{
			imageblockclass: "container_line3",
			imagestoshow:[{
				imgclass: "line_class",
				imgsrc: "",
				imgid: "line02"
			}],
			imagelabels: [{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s1
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s2
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s3
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s4
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s5
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s6
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s7
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s8
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s9
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s10
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s11
			}]
		}]

	}
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

  loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "apple", src: imgpath+"apple.png", type: createjs.AbstractLoader.IMAGE},
			{id: "10apples", src: imgpath+"11apples.png", type: createjs.AbstractLoader.IMAGE},
			{id: "line01", src: imgpath+"line01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "line02", src: imgpath+"line02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dolma1", src: imgpath+"ole-logo.png", type: createjs.AbstractLoader.IMAGE},
			{id: "backpack_img", src: imgpath+"ole-logo.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "dialog1_img", src: imgpath+'bubble-10.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s2_p1", src: soundAsset+"s2_p1.ogg"},
			{id: "s2_p3", src: soundAsset+"s2_p3.ogg"},
			{id: "s2_p4", src: soundAsset+"s2_p4.ogg"},
			{id: "s2_p5", src: soundAsset+"s2_p5.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

	/*===== This function splits the string in data into convential fraction used in mathematics =====*/
    function splitintofractions($splitinside){
   		typeof $splitinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
       	if($splitintofractions.length > 0){
       		$.each($splitintofractions, function(index, value){
	        	$this = $(this);
	        	var tobesplitfraction = $this.html();
	        	if($this.hasClass('fraction')){
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
	        	}else{
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
	        	}


				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
	        	$this.html(tobesplitfraction);
	        });
       	}
   	}
   	/*===== split into fractions end =====*/
   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
 var timeoutcontroller;
 var intervalid;
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    splitintofractions($board);
	    vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		switch(countNext) {
			case 0:
				sound_player("s2_p"+(countNext+1),1);
				$(".container_apple").css("background-image", 'url('+ preload.getResult("10apples").src +')');
				break;
			case 1:
				// sound_player("s2_p"+(countNext+1),1);
				$(".container_apple").css("background-image", 'url('+ preload.getResult("10apples").src +')');
				var count =0;
				var $label_des = $(".label_des");
				$($label_des[count]).addClass("pink_highlight");
				intervalid = setInterval(function(){
					count++;
					$($label_des[count]).addClass("pink_highlight");
					(count > 0)? $($label_des[count-1]).removeClass("pink_highlight"): $($label_des[10]).removeClass("pink_highlight");
					if(count > 10){
						nav_button_controls(500);
					}
				}, 2250);
				break;
			case 2:
				sound_player1("s2_p"+(countNext+1),1);
				var count = 0;
				var $label_des2 = $(".label_des2");
				var $label_des = $(".label_des").css('visibility', 'hidden');
				$($label_des[0]).css('visibility', 'visible');
				$($label_des2[count]).css("background-color", "#CBC4E1");
				$($label_des[count]).show(0);
				intervalid = setInterval(function(){
					count++;
					if(count < 10){
						$($label_des2[count]).css("background-color", "#CBC4E1");
						$label_des.css('visibility', 'hidden');
						$($label_des[count]).css('visibility', 'visible');
					} else if(count > 10){
						nav_button_controls(0);
					}
				}, 2000);
				break;
			case 3:
				sound_player("s2_p"+(countNext+1),1);
				break;
			case 4:
				sound_player1("s2_p5",1);
				var count =0;
				var $label_des_a = $(".container_line3>.label_des");
				var $label_des2 = $(".label_des2");
				var $label_des_b = $(".container_line2>.label_des").css('visibility', 'hidden');
				$($label_des2[count]).css("background-color", "#CBC4E1");
				$($label_des_b[count]).css('visibility', 'visible');
				$($label_des_a[count+1]).addClass("pink_highlight");
				intervalid = setInterval(function(){
					count++;
					if(count < 10){
						$($label_des2[count]).css("background-color", "#CBC4E1");
						$label_des_b.css('visibility', 'hidden');
						$($label_des_b[count]).css('visibility', 'visible');
						$($label_des_a[count+1]).addClass("pink_highlight");
						(count > 0)?$($label_des_a[count]).removeClass("pink_highlight"): true;
					} else if(count > 10){
						nav_button_controls(500);

					}
				}, 2000);
				break;
			default:
				$prevBtn.show(0);
				break;
		}

		// splitintofractions($(".fractionblock"));
  }

/*=====  End of Templates Block  ======*/

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id, nextFlag){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete',function(){
			nextFlag?nav_button_controls():"";
		});
	}
	function sound_player1(sound_id, nextFlag){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			for(var j = 0; j < content[count].imageblock.length; j++){
				var imageblock = content[count].imageblock[j];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
		clearTimeout(timeoutcontroller);
		clearInterval(intervalid);
		createjs.Sound.stop();
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		createjs.Sound.stop();
		templateCaller();
	});

	$prevBtn.on('click', function() {
		clearTimeout(timeoutcontroller);
		clearInterval(intervalid);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

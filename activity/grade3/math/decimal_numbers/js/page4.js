var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sound/"+$lang+"/";

var content=[
	{
		// slide0
		contentnocenteradjust: true,
		contentblockadditionalclass: "add_contentblk",
		uppertextblock:[{
			textclass: "description_title2",
			datahighlightflag: true,
			datahighlightcustomclass: "pink_highlight",
			textdata: data.string.p4_s0_a
		}],
		imageblock:[{
			imageblockclass: "container_apple",
			// imagestoshow:[{
				// imgclass: "apple_class",
				// imgsrc: "",
				// imgid: "apple"
			// }]
		},{
			imageblockclass: "container_line",
			imagestoshow:[{
				imgclass: "line_class",
				imgsrc: "",
				imgid: "line02"
			}],
			imagelabels: [{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s1
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s2
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s3
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s4
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s5
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s6
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s7
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s8
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s9
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s10
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s11
			}]
		}],
		lowertextblockadditionalclass: "additional_ltb",
		lowertextblock:[{
			textclass: "description_title2",
			datahighlightcustomclass: "pink_highlight",
			datahighlightflag: true,
			textdata: data.string.p4_s0
		}]
	},{
		// slide1
		contentnocenteradjust: true,
		contentblockadditionalclass: "add_contentblk",
		uppertextblock:[{
			textclass: "description_title2",
			datahighlightflag: true,
			datahighlightcustomclass: "pink_highlight",
			textdata: data.string.p4_s0_a
		}],
		imageblock:[{
			imageblockclass: "container_apple pos1",
			// imagestoshow:[{
				// imgclass: "apple_class ",
				// imgsrc: "",
				// imgid: "apple"
			// }]
		},{
			imageblockclass: "container_line",
			imagestoshow:[{
				imgclass: "line_class",
				imgsrc: "",
				imgid: "line02"
			}],
			imagelabels: [{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s1
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s2
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s3
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s4
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s5
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s6
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s7
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s8
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s9
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s10
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s11
			}]
		}],
		lowertextblockadditionalclass: "additional_ltb",
		lowertextblock:[{
			textclass: "description_title2",
			datahighlightcustomclass: "pink_highlight",
			datahighlightflag: true,
			textdata: data.string.p4_s12
		}]
	},{
		// slide 2
		contentnocenteradjust: true,
		contentblockadditionalclass: "add_contentblk",
		uppertextblock:[{
			textclass: "description_title2",
			datahighlightflag: true,
			datahighlightcustomclass: "pink_highlight",
			textdata: data.string.p4_s0_a
		}],
		imageblock:[{
			imageblockclass: "container_apple pos2",
			// imagestoshow:[{
				// imgclass: "apple_class ",
				// imgsrc: "",
				// imgid: "apple"
			// }]
		},{
			imageblockclass: "container_line",
			imagestoshow:[{
				imgclass: "line_class",
				imgsrc: "",
				imgid: "line02"
			}],
			imagelabels: [{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s1
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s2
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s3
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s4
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s5
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s6
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s7
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s8
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s9
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s10
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s11
			}]
		}],
		lowertextblockadditionalclass: "additional_ltb",
		lowertextblock:[{
			textclass: "description_title2",
			datahighlightcustomclass: "pink_highlight",
			datahighlightflag: true,
			textdata: data.string.p4_s13
		}]
	},{
		//page 3
		contentnocenteradjust: true,
		contentblockadditionalclass: "add_contentblk",
		uppertextblock:[{
			textclass: "description_title2",
			datahighlightflag: true,
			datahighlightcustomclass: "pink_highlight",
			textdata: data.string.p4_s0_a
		}],
		imageblock:[{
			imageblockclass: "container_apple pos3",
			// imagestoshow:[{
				// imgclass: "apple_class ",
				// imgsrc: "",
				// imgid: "apple"
			// }]
		},{
			imageblockclass: "container_line",
			imagestoshow:[{
				imgclass: "line_class",
				imgsrc: "",
				imgid: "line02"
			}],
			imagelabels: [{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s1
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s2
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s3
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s4
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s5
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s6
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s7
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s8
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s9
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s10
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s11
			}]
		}],
		lowertextblockadditionalclass: "additional_ltb",
		lowertextblock:[{
			textclass: "description_title2",
			datahighlightcustomclass: "pink_highlight",
			datahighlightflag: true,
			textdata: data.string.p4_s14
		}]
	},{
		//page 4
		contentnocenteradjust: true,
		contentblockadditionalclass: "add_contentblk",
		uppertextblock:[{
			textclass: "description_title2",
			datahighlightflag: true,
			datahighlightcustomclass: "pink_highlight",
			textdata: data.string.p4_s0_a
		}],
		imageblock:[{
			imageblockclass: "container_apple pos4",
			// imagestoshow:[{
				// imgclass: "apple_class ",
				// imgsrc: "",
				// imgid: "apple"
			// }]
		},{
			imageblockclass: "container_line",
			imagestoshow:[{
				imgclass: "line_class",
				imgsrc: "",
				imgid: "line02"
			}],
			imagelabels: [{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s1
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s2
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s3
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s4
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s5
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s6
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s7
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s8
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s9
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s10
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s11
			}]
		}],
		lowertextblockadditionalclass: "additional_ltb",
		lowertextblock:[{
			textclass: "description_title2",
			datahighlightcustomclass: "pink_highlight",
			datahighlightflag: true,
			textdata: data.string.p4_s15
		}]
	},{
		//page 5
		contentnocenteradjust: true,
		contentblockadditionalclass: "add_contentblk",
		uppertextblock:[{
			textclass: "description_title2",
			datahighlightflag: true,
			datahighlightcustomclass: "pink_highlight",
			textdata: data.string.p4_s0_a
		}],
		imageblock:[{
			imageblockclass: "container_apple pos5",
			// imagestoshow:[{
				// imgclass: "apple_class ",
				// imgsrc: "",
				// imgid: "apple"
			// }]
		},{
			imageblockclass: "container_line",
			imagestoshow:[{
				imgclass: "line_class",
				imgsrc: "",
				imgid: "line02"
			}],
			imagelabels: [{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s1
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s2
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s3
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s4
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s5
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s6
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s7
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s8
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s9
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s10
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s11
			}]
		}],
		lowertextblockadditionalclass: "additional_ltb",
		lowertextblock:[{
			textclass: "description_title2",
			datahighlightcustomclass: "pink_highlight",
			datahighlightflag: true,
			textdata: data.string.p4_s16
		}]
	},{
		//page 6
		contentnocenteradjust: true,
		contentblockadditionalclass: "add_contentblk",
		uppertextblock:[{
			textclass: "description_title2",
			datahighlightflag: true,
			datahighlightcustomclass: "pink_highlight",
			textdata: data.string.p4_s0_a
		}],
		imageblock:[{
			imageblockclass: "container_apple pos6",
			// imagestoshow:[{
				// imgclass: "apple_class ",
				// imgsrc: "",
				// imgid: "apple"
			// }]
		},{
			imageblockclass: "container_line",
			imagestoshow:[{
				imgclass: "line_class",
				imgsrc: "",
				imgid: "line02"
			}],
			imagelabels: [{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s1
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s2
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s3
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s4
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s5
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s6
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s7
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s8
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s9
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s10
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s11
			}]
		}],
		lowertextblockadditionalclass: "additional_ltb",
		lowertextblock:[{
			textclass: "description_title2",
			datahighlightcustomclass: "pink_highlight",
			datahighlightflag: true,
			textdata: data.string.p4_s17
		}]

	},{
		//page 7
		contentnocenteradjust: true,
		contentblockadditionalclass: "add_contentblk",
		uppertextblock:[{
			textclass: "description_title2",
			datahighlightflag: true,
			datahighlightcustomclass: "pink_highlight",
			textdata: data.string.p4_s0_a
		}],
		imageblock:[{
			imageblockclass: "container_apple pos7",
			// imagestoshow:[{
				// imgclass: "apple_class ",
				// imgsrc: "",
				// imgid: "apple"
			// }]
		},{
			imageblockclass: "container_line",
			imagestoshow:[{
				imgclass: "line_class",
				imgsrc: "",
				imgid: "line02"
			}],
			imagelabels: [{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s1
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s2
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s3
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s4
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s5
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s6
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s7
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s8
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s9
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s10
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s11
			}]
		}],
		lowertextblockadditionalclass: "additional_ltb",
		lowertextblock:[{
			textclass: "description_title2",
			datahighlightcustomclass: "pink_highlight",
			datahighlightflag: true,
			textdata: data.string.p4_s18
		}]

	},{
		//page 8
		contentnocenteradjust: true,
		contentblockadditionalclass: "add_contentblk",
		uppertextblock:[{
			textclass: "description_title2",
			datahighlightflag: true,
			datahighlightcustomclass: "pink_highlight",
			textdata: data.string.p4_s0_a
		}],
		imageblock:[{
			imageblockclass: "container_apple pos8",
			// imagestoshow:[{
				// imgclass: "apple_class ",
				// imgsrc: "",
				// imgid: "apple"
			// }]
		},{
			imageblockclass: "container_line",
			imagestoshow:[{
				imgclass: "line_class",
				imgsrc: "",
				imgid: "line02"
			}],
			imagelabels: [{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s1
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s2
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s3
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s4
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s5
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s6
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s7
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s8
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s9
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s10
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s11
			}]
		}],
		lowertextblockadditionalclass: "additional_ltb",
		lowertextblock:[{
			textclass: "description_title2",
			datahighlightcustomclass: "pink_highlight",
			datahighlightflag: true,
			textdata: data.string.p4_s19
		}]

	},{
		//page 9
		contentnocenteradjust: true,
		contentblockadditionalclass: "add_contentblk",
		uppertextblock:[{
			textclass: "description_title2",
			datahighlightflag: true,
			datahighlightcustomclass: "pink_highlight",
			textdata: data.string.p4_s0_a
		}],
		imageblock:[{
			imageblockclass: "container_apple pos9",
			// imagestoshow:[{
				// imgclass: "apple_class ",
				// imgsrc: "",
				// imgid: "apple"
			// }]
		},{
			imageblockclass: "container_line",
			imagestoshow:[{
				imgclass: "line_class",
				imgsrc: "",
				imgid: "line02"
			}],
			imagelabels: [{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s1
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s2
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s3
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s4
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s5
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s6
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s7
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s8
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s9
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s10
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s11
			}]
		}],
		lowertextblockadditionalclass: "additional_ltb",
		lowertextblock:[{
			textclass: "description_title2",
			datahighlightcustomclass: "pink_highlight",
			datahighlightflag: true,
			textdata: data.string.p4_s20
		}]

	},{
		//page 10
		contentnocenteradjust: true,
		contentblockadditionalclass: "add_contentblk",
		uppertextblock:[{
			textclass: "description_title2",
			datahighlightflag: true,
			datahighlightcustomclass: "pink_highlight",
			textdata: data.string.p4_s0_a
		}],
		imageblock:[{
			imageblockclass: "container_apple pos10",
			// imagestoshow:[{
				// imgclass: "apple_class ",
				// imgsrc: "",
				// imgid: "apple"
			// }]
		},{
			imageblockclass: "container_line",
			imagestoshow:[{
				imgclass: "line_class",
				imgsrc: "",
				imgid: "line02"
			}],
			imagelabels: [{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s1
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s2
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s3
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s4
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s5
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s6
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s7
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s8
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s9
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s10
			},{
				imagelabelclass: "label_des",
				imagelabeldata: data.string.p2_s11
			}]
		}],
		lowertextblockadditionalclass: "additional_ltb",
		lowertextblock:[{
			textclass: "description_title2",
			datahighlightcustomclass: "pink_highlight",
			datahighlightflag: true,
			textdata: data.string.p4_s21
		}]

	}
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

  loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "apple", src: imgpath+"11apples.png", type: createjs.AbstractLoader.IMAGE},
			{id: "line01", src: imgpath+"line01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "line02", src: imgpath+"line02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dolma1", src: imgpath+"ole-logo.png", type: createjs.AbstractLoader.IMAGE},
			{id: "backpack_img", src: imgpath+"ole-logo.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "dialog1_img", src: imgpath+'bubble-10.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s4_p1", src: soundAsset+"s4_p1.ogg"},
			{id: "s4_p2", src: soundAsset+"s4_2.ogg"},
			{id: "s4_p3", src: soundAsset+"s4_3.ogg"},
			{id: "s4_p4", src: soundAsset+"s4_p4.ogg"},
			{id: "s4_p5", src: soundAsset+"s4_p5.ogg"},
			{id: "s4_p6", src: soundAsset+"s4_p6.ogg"},
			{id: "s4_p7", src: soundAsset+"s4_p7.ogg"},
			{id: "s4_p8", src: soundAsset+"s4_p8.ogg"},
			{id: "s4_p9", src: soundAsset+"s4_p9.ogg"},
			{id: "s4_p10", src: soundAsset+"s4_p10.ogg"},
			{id: "s4_p11", src: soundAsset+"s4_p11.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

	/*===== This function splits the string in data into convential fraction used in mathematics =====*/
    function splitintofractions($splitinside){
   		typeof $splitinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
       	if($splitintofractions.length > 0){
       		$.each($splitintofractions, function(index, value){
	        	$this = $(this);
	        	var tobesplitfraction = $this.html();
	        	if($this.hasClass('fraction')){
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
	        	}else{
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
	        	}


				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
	        	$this.html(tobesplitfraction);
	        });
       	}
   	}
   	/*===== split into fractions end =====*/
   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			// $nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// // if lastpageflag is true
			// timeoutcontroller = setTimeout(function(){
			// 		islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
			// }, 1000);
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
 var timeoutcontroller;
 var intervalid;
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    splitintofractions($board);
	    vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		$(".container_apple").css("background-image", 'url('+ preload.getResult("apple").src +')');

			$nextBtn.hide(0);
			$prevBtn.hide(0);
		switch(countNext) {
			default:
				sound_player("s4_p"+(countNext+1),1);
			break;
		}

		// splitintofractions($(".fractionblock"));
  }

/*=====  End of Templates Block  ======*/


	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id, nextFlag){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete',function(){
			nextFlag?nav_button_controls():"";
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			for(var j = 0; j < content[count].imageblock.length; j++){
				var imageblock = content[count].imageblock[j];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		clearTimeout(timeoutcontroller);
		clearInterval(intervalid);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

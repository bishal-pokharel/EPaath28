var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sound/"+$lang+"/";

var content=[
	{
		// slide0
		contentblockadditionalclass: "add_contentblk",
		uppertextblock:[{
			textclass: "description_title2",
			datahighlightflag: true,
			datahighlightcustomclass: "pink_highlight",
			textdata: data.string.p3_s0
		}]
	},{
		// slide1
		contentnocenteradjust: true,
		contentblockadditionalclass: "add_contentblk",
		uppertextblock:[{
			textclass: "description_title2",
			datahighlightflag: true,
			datahighlightcustomclass: "pink_highlight",
			textdata: data.string.p3_s1
		}],
		imageblock:[{
			imageblockclass: "container_fraction",
			imagelabels: [{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			},{
				imagelabelclass: "label_des2",
				imagelabeldata: "&nbsp;"
			}]
		},{
			imagestoshow:[{
				imgclass: "correct_class",
				imgid: "correct"
			},{
				imgclass: "incorrect_class",
				imgid: "incorrect"
			}]
		}],
		lowertextblockadditionalclass: "ltb_additional",
		lowertextblock:[{
			textclass: "incorrect",
			splitintofractionsflag: true,
			textdata: data.string.p3_s2
		},{
			textclass: "incorrect",
			splitintofractionsflag: true,
			textdata: data.string.p3_s3
		},{
			textclass: "correct",
			splitintofractionsflag: true,
			textdata: data.string.p3_s4
		}],
		inputvalues:[{
			inputcontainerclass: "input_container",
			prefixdata: " = ",
			inputclass: "input_val",
			inputtype: "text",
			suffixdata: "0.",
		},{
			inputcontainerclass: "check_container",
			inputclass: "check",
			inputtype: "button",
			inputvalue: data.string.p3_s7
		}]
	}
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

  loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg", src: "images/diy_bg/a_25.png", type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src: imgpath+"correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: imgpath+"incorrect.png", type: createjs.AbstractLoader.IMAGE},
			{id: "line02", src: imgpath+"line02.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s3_p2_1", src: soundAsset+"s3_p2_1.ogg"},
			{id: "s3_p2_2", src: soundAsset+"s3_p2_2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

	/*===== This function splits the string in data into convential fraction used in mathematics =====*/
    function splitintofractions($splitinside){
   		typeof $splitinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
       	if($splitintofractions.length > 0){
       		$.each($splitintofractions, function(index, value){
	        	$this = $(this);
	        	var tobesplitfraction = $this.html();
	        	if($this.hasClass('fraction')){
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
	        	}else{
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
	        	}


				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
	        	$this.html(tobesplitfraction);
	        });
       	}
   	}
   	/*===== split into fractions end =====*/
   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// setTimeout(function(){
				// if(countNext == $total_page - 1)
					// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
			// }, 2500);
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
 var timeoutcontroller;
 var intervalid;
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    splitintofractions($board);
	    vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		switch(countNext) {
			case 0:
				play_diy_audio();
				$(".contentblock").css({'background-image': 'url("'+ preload.getResult("bg").src +'")',
										'background-size': '100% 100%'});
										nav_button_controls(2000);
				break;
			case 1:
				sound_player("s3_p2_1",0);
				var $label_des2 = $(".label_des2");
				var $input_container = $(".input_container").hide(0);
				var $check_container = $(".check_container").hide(0);
				var randomvalue = [];
				var no;
				while (randomvalue.length < 3){
					no = Math.floor(Math.random() * (9 - 1 +1) + 1);
					if(randomvalue.indexOf(no) == -1){
						randomvalue.push(no);
					}

				}
				console.log("random no:", randomvalue);
				for (var i=0; i < randomvalue[0]; i++){
					$($label_des2[i]).css("background-color", "#CBC4E1");
				}

				$(".correct> .fraction2> .top").html(randomvalue[0]);
				$($(".incorrect> .fraction2 > .top")[0]).html(randomvalue[1]);
				$($(".incorrect> .fraction2> .top")[1]).html(randomvalue[2]);

				var $options = $(".ltb_additional > p");
				var idx;
				var count=1;
				while($options.length > 0){
					idx = Math.floor(Math.random()*($options.length));
					$($options[idx]).addClass("option"+count);
					$options.splice(idx, 1);
					count++;
				}

				$(".correct").click(function(){
						createjs.Sound.stop();
					play_correct_incorrect_sound(1);
					setTimeout(function(){
						sound_player("s3_p2_2",0);
					},2000);

					var $this = $(this).addClass("animate_to_left");
					var $fraction2 = $($this.find("span")[0]);
					$fraction2.css({"background-color": "#AFD37A",
									"color": "#333",
									"border": "0.1em solid #AFD37A",}).addClass("deactivate");
					$($this.find("span")[2]).css("border-top", "0.1em solid #333");
					$(".incorrect").hide(0);
					$('.description_title2').fadeOut(1000, function(){
						$('.description_title2').html(data.string.p3_s6);
						$('.description_title2').fadeIn(500);
					});
					timeoutcontroller = setTimeout(function(){
						$input_container.show(0);
						$check_container.show(0);
					}, 1700);
				});
				var $input_val = $(".input_val");
				var $check = $(".check");
				var $correct_class = $(".correct_class");
				var $incorrect_class = $(".incorrect_class");
				$input_val.keydown(function(event){
		    		var charCode = (event.which) ? event.which : event.keyCode;
		    		/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
		    		if(charCode === 13 ) {
				        $check.trigger("click");
					}
					var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46 ;
					//check if user inputs del, backspace or arrow keys
		   			if (!condition) {
		    			return true;
		    		}
		    		//check if user inputs more than one '.'
					if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
		        		return false;
		    		}else if((charCode == 190 || charCode == 110)){
		    			return true;
		    		}
		    		//check . and 0-9 separately after checking arrow and other keys
		    		if((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) ){
		    			return false;
		    		}
		    		//check max no of allowed digits
		    		if (String(event.target.value).length >= 3) {
		    			return false;
		    		}
		  			return true;
				});

				$check.click(function(){
					var rightans = "0."+$input_val.val();
					var checkans = "0."+randomvalue[0];
                    var checkans1 = "0."+randomvalue[0]+"0";
                    var checkans2 = "0."+randomvalue[0]+"00";
                    console.log(rightans)
                    console.log(checkans)
                    console.log(checkans1)
                    if(rightans.toString() == checkans.toString()||rightans.toString() == checkans1.toString()||rightans.toString() == checkans2.toString()){
						play_correct_incorrect_sound(true);
						$input_val.attr("disabled", true);
						$check.attr("disabled", true);
						$incorrect_class.hide(0);
						$correct_class.show(0);
						ole.footerNotificationHandler.pageEndSetNotification();
					}else{
						play_correct_incorrect_sound(false);
						$correct_class.hide(0);
						$incorrect_class.show(0);
					}
				});

				$(".incorrect").click(function(){
					play_correct_incorrect_sound(0);
					$($(this).find("span")[0]).css({"background-color": "#FF5A5A",
								"border": "0.1em solid #FF5A5A"}).addClass("deactivate");
				});

				break;

			default:
				break;
		}

		// splitintofractions($(".fractionblock"));
  }

/*=====  End of Templates Block  ======*/


	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id, nextFlag){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete',function(){
			nextFlag?nav_button_controls():"";
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			for(var j = 0; j < content[count].imageblock.length; j++){
				var imageblock = content[count].imageblock[j];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		clearTimeout(timeoutcontroller);
		clearInterval(intervalid);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

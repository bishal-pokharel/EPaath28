var imgpath = $ref+"/images/";
var imgpath2 = $ref+"/images/";
var soundAsset = $ref+"/sound/"+$lang+"/";

var content=[
	{
		// slide 0
		contentnocenteradjust: true,
		imageblock:[{
				imagestoshow:[{
					imgclass: "bg1_class",
					imgsrc: "",
					imgid: "bg1"
				},{
					imgclass: "think_class",
					imgsrc: "",
					imgid: "think"
				}],
				imagelabels:[{
					imagelabelclass: "description_intro",
					imagelabeldata: data.string.p8_s0
				}]
			},{
				imageblockclass: "grid",
		}]
	},
	{
		//slide 1
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description1",
			textdata: data.string.p8_s1
		},{
			textclass: "description2",
			textdata: data.string.p8_s2
		}],
		imageblock:[{
			imageblockclass: "left_partition",
		},{
			imageblockclass: "right_partition",
			imagelabels:[{
				imagelabelclass: "description3_top",
				imagelabeldata: data.string.p8_s3
			},{
				imagelabelclass: "description3",
				splitintofractionsflag: true,
				datahighlightflag: true,
				datahighlightcustomclass: "pink_highlight",
				imagelabeldata: data.string.p8_s4
			},{
				imagelabelclass: "description3",
				datahighlightflag: true,
				splitintofractionsflag: true,
				imagelabeldata: data.string.p8_s5
			}]
		}]
	},
	{
		//slide 2
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description1",
			textdata: data.string.p8_s1
		},{
			textclass: "description2",
			textdata: data.string.p8_s2
		}],
		imageblock:[{
			imageblockclass: "left_partition",
		},{
			imageblockclass: "right_partition",
			imagelabels:[{
				imagelabelclass: "description3_top",
				imagelabeldata: data.string.p8_s6
			},{
				imagelabelclass: "description3",
				splitintofractionsflag: true,
				datahighlightflag: true,
				datahighlightcustomclass: "pink_highlight",
				imagelabeldata: data.string.p8_s7
			}]
		}]
	},
	{
		//slide 3
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description1",
			textdata: data.string.p8_s1
		},{
			textclass: "description2",
			textdata: data.string.p8_s2
		}],
		imageblock:[{
			imageblockclass: "left_partition",
		},{
			imageblockclass: "right_partition",
			imagelabels:[{
				imagelabelclass: "description3_top",
				imagelabeldata: data.string.p8_s8
			},{
				imagelabelclass: "description3",
				splitintofractionsflag: true,
				datahighlightflag: true,
				datahighlightcustomclass: "pink_highlight",
				imagelabeldata: data.string.p8_s9
			}]
		}]
	},
	{
		//slide 4
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description1",
			textdata: data.string.p8_s1
		},{
			textclass: "description2",
			textdata: data.string.p8_s2
		}],
		imageblock:[{
			imageblockclass: "left_partition",
		},{
			imageblockclass: "right_partition",
			imagelabels:[{
				imagelabelclass: "description3_top",
				imagelabeldata: data.string.p8_s10
			},{
				imagelabelclass: "description3",
				splitintofractionsflag: true,
				datahighlightflag: true,
				datahighlightcustomclass: "pink_highlight",
				imagelabeldata: data.string.p8_s11
			}]
		}]
	}
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

  loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
	 var aeroplanesound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "think", src: imgpath+"think.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg1", src: imgpath+"bg1.jpg", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s8_p1", src: soundAsset+"s8_p1.ogg"},
			{id: "s8_p2", src: soundAsset+"s8_p2.ogg"},
			{id: "s8_p3", src: soundAsset+"s8_p3.ogg"},
			{id: "s8_p4", src: soundAsset+"s8_p4.ogg"},
			{id: "s8_p5", src: soundAsset+"s8_p5.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// aeroplanesound = createjs.Sound.play("aeroplane_sound", {loop: -1, volume:0.32});
		// current_sound = createjs.Sound.play('sound_1');
		// current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*===== This function splits the string in data into convential fraction used in mathematics =====*/
    function splitintofractions($splitinside){
   		typeof $splitinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
       	if($splitintofractions.length > 0){
       		$.each($splitintofractions, function(index, value){
	        	$this = $(this);
	        	var tobesplitfraction = $this.html();
	        	if($this.hasClass('fraction')){
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
	        	}else{
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
	        	}


				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
	        	$this.html(tobesplitfraction);
	        });
       	}
   	}
   	/*===== split into fractions end =====*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			// $nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.hide(0);
			$prevBtn.hide(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  var timeoutcontroller;
  var intervalcontroller;
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    splitintofractions($board);
	    vocabcontroller.findwords(countNext);
	    put_image(content, countNext);
		switch(countNext){
			case 0:
				sound_player("s8_p"+(countNext+1),1);
				var $grid = $(".grid");
				for (var i = 0; i< 100; i++){
					$grid.append("<label class='cell'> &nbsp; </label>");
				}
				$grid.css("background-color", "#CBC4E1");
				break;
			case 1:
				sound_player("s8_p"+(countNext+1),1);
				var $grid = $(".left_partition");
				for (var i = 0; i< 100; i++){
					$grid.append("<label class='cell'> &nbsp; </label>");
				}
				$(".bottom").css("border-top", "0.1em solid #674EA7");
				$($(".cell")[0]).css("background-color", "#CBC4E1");
				break;
			case 2:
				sound_player("s8_p"+(countNext+1),1);
				var $grid = $(".left_partition");
				for (var i = 0; i< 100; i++){
					$grid.append("<label class='cell'> &nbsp; </label>");
				}
				var $cell = $(".cell");
				$(".description3_a2").html("0.02");
				$(".top").html("2");
				boxFiller($cell, 4);
				break;
			case 3:
				sound_player("s8_p"+(countNext+1),1);
				var $grid = $(".left_partition");
				for (var i = 0; i< 100; i++){
					$grid.append("<label class='cell'> &nbsp; </label>");
				}
				var $cell = $(".cell");
				$(".top").html("5");
				$(".description3_a2").html("0.05");
				boxFiller($cell, 55);
				break;
			case 4:
				sound_player("s8_p"+(countNext+1),1);
				var $grid = $(".left_partition");
				for (var i = 0; i< 100; i++){
					$grid.append("<label class='cell'> &nbsp; </label>");
				}
				var $cell = $(".cell");
				$(".top").html("10");
				$(".description3_a2").html("0.10");
				boxFiller($cell, 86);
				break;
			default:
				break;
		}
  }

  function boxFiller(boxItem, maxCount) {
		var boxCount = 0;
		for (var j = 0; j < 10; j++) {
			for (var k = 0; k < 10; k++) {
				if (boxCount < maxCount) {
					var boxIndex = k * 10 + j;
					$(boxItem[boxIndex]).css("background-color", "#CBC4E1");
					boxCount++;
				}
			}
		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id, nextFlag){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete',function(){
			nextFlag?nav_button_controls():"";
		});
	}

  function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			for(var j = 0; j < content[count].imageblock.length; j++){
				var imageblock = content[count].imageblock[j];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		clearTimeout(timeoutcontroller);
		if(intervalcontroller != null){
			clearInterval(intervalcontroller);
			intervalcontroller = null;
		}
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

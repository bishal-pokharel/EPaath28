var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sound/"+$lang+"/";

var content=[
	//slide 0
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{
			textdata : data.string.e1_q1,
			splitintofractionsflag: true,
			typerange: "10",
			answer: "8",
			textclass : 'instruction2 my_font_big',
		},{
			textclass: "scoreboard",
			textdata: "0/0"
		}],
		hintimageblock: true,
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : "",
			textclass : 'instruction my_font_big',
		},{
			textdata : data.string.e1_q1_o1,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1_q1_o2,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e1_q1_o3,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.e1_q1_o4,
			textclass : 'class4 options'
		}],
	},
	//slide 1
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{
			textdata : data.string.e1_q2,
			splitintofractionsflag: true,
			typerange: "100",
			answer: "87",
			textclass : 'instruction2 my_font_big',
		},{
			textclass: "scoreboard",
			textdata: "0/0"
		}],
		hintimageblockadditionalclass: "left_partition",
		hintimageblock: true,
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : "",
			textclass : 'instruction my_font_big',
		},{
			textdata : data.string.e1_q2_o1,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1_q2_o2,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e1_q2_o3,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.e1_q2_o4,
			textclass : 'class3 options'
		}],
	},
	//slide 2
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{
			textdata : data.string.e1_q3,
			splitintofractionsflag: true,
			typerange: "10",
			answer: "2",
			textclass : 'instruction2 my_font_big',
		},{
			textclass: "scoreboard",
			textdata: "0/0"
		}],
		hintimageblockadditionalclass: "left_partition",
		hintimageblock: true,
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : "",
			textclass : 'instruction my_font_big',
		},{
			textdata : data.string.e1_q3_o1,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1_q3_o2,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e1_q3_o3,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.e1_q3_o4,
			textclass : 'class3 options'
		}],
	},
	//slide 3
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{
			textdata : data.string.e1_q4,
			splitintofractionsflag: true,
			typerange: "10",
			answer: "9",
			textclass : 'instruction2 my_font_big',
		},{
			textclass: "scoreboard",
			textdata: "0/0"
		}],
		hintimageblockadditionalclass: "left_partition",
		hintimageblock: true,
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : "",
			textclass : 'instruction my_font_big',
		},{
			textdata : data.string.e1_q4_o1,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1_q4_o2,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e1_q4_o3,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.e1_q4_o4,
			textclass : 'class3 options'
		}],
	},
	//slide 4
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{
			textdata : data.string.e1_q5,
			splitintofractionsflag: true,
			typerange: "100",
			answer: "60",
			textclass : 'instruction2 my_font_big',
		},{
			textclass: "scoreboard",
			textdata: "0/0"
		}],
		hintimageblockadditionalclass: "left_partition",
		hintimageblock: true,
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : "",
			textclass : 'instruction my_font_big',
		},{
			textdata : data.string.e1_q5_o1,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1_q5_o2,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e1_q5_o3,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.e1_q5_o4,
			textclass : 'class3 options'
		}],
	},
	//slide 5
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{
			textdata : data.string.e1_q6,
			splitintofractionsflag: true,
			typerange: "100",
			answer: "4",
			textclass : 'instruction2 my_font_big',
		},{
			textclass: "scoreboard",
			textdata: "0/0"
		}],
		hintimageblockadditionalclass: "left_partition",
		hintimageblock: true,
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : "",
			textclass : 'instruction my_font_big',
		},{
			textdata : data.string.e1_q6_o1,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1_q6_o2,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e1_q6_o3,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.e1_q6_o4,
			textclass : 'class3 options'
		}],
	},
	//slide 6
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{
			textdata : data.string.e1_q7,
			splitintofractionsflag: true,
			typerange: "100",
			answer: "54",
			textclass : 'instruction2 my_font_big',
		},{
			textclass: "scoreboard",
			textdata: "0/0"
		}],
		hintimageblockadditionalclass: "left_partition",
		hintimageblock: true,
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : "",
			textclass : 'instruction my_font_big',
		},{
			textdata : data.string.e1_q7_o1,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1_q7_o2,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e1_q7_o3,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.e1_q7_o4,
			textclass : 'class3 options'
		}],
	},
	//slide 7
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{
			textdata : data.string.e1_q8,
			splitintofractionsflag: true,
			typerange: "100",
			answer: "62",
			textclass : 'instruction2 my_font_big',
		},{
			textclass: "scoreboard",
			textdata: "0/0"
		}],
		hintimageblockadditionalclass: "left_partition",
		hintimageblock: true,
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : "",
			textclass : 'instruction my_font_big',
		},{
			textdata : data.string.e1_q8_o1,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1_q8_o2,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e1_q8_o3,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.e1_q8_o4,
			textclass : 'class3 options'
		}],
	},
	//slide 8
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{
			textdata : data.string.e1_q9,
			splitintofractionsflag: true,
			typerange: "100",
			answer: "70",
			textclass : 'instruction2 my_font_big',
		},{
			textclass: "scoreboard",
			textdata: "0/0"
		}],
		hintimageblockadditionalclass: "left_partition",
		hintimageblock: true,
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : "",
			textclass : 'instruction my_font_big',
		},{
			textdata : data.string.e1_q9_o1,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1_q9_o2,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e1_q9_o3,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.e1_q9_o4,
			textclass : 'class3 options'
		}],
	},
	//slide 9
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{
			textdata : data.string.e1_q10,
			splitintofractionsflag: true,
			typerange: "100",
			answer: "3",
			textclass : 'instruction2 my_font_big',
		},{
			textclass: "scoreboard",
			textdata: "0/0"
		}],
		hintimageblockadditionalclass: "left_partition",
		hintimageblock: true,
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : "",
			textclass : 'instruction my_font_big',
		},{
			textdata : data.string.e1_q10_o1,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1_q10_o2,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e1_q10_o3,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.e1_q10_o4,
			textclass : 'class3 options'
		}],
	}
];

// content.shufflearray();


$(function ()
{
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	// var score = 0;

	var preload;
	var timeoutvar = null;
	var current_sound;
	 var aeroplanesound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "correct", src: imgpath+"correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: imgpath+"incorrect.png", type: createjs.AbstractLoader.IMAGE},
			{id: "think", src: imgpath+"think.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg1", src: imgpath+"bg1.jpg", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "ex_1", src: soundAsset+"ex_1.ogg"},
			{id: "ex_2", src: soundAsset+"ex_2.ogg"},
			{id: "ex_3", src: soundAsset+"ex_3.ogg"},
			{id: "ex_4", src: soundAsset+"ex_4.ogg"},
			{id: "ex_5", src: soundAsset+"ex_5.ogg"},
			{id: "ex_6", src: soundAsset+"ex_6.ogg"},
			{id: "ex_7", src: soundAsset+"ex_7.ogg"},
			{id: "ex_8", src: soundAsset+"ex_8.ogg"},
			{id: "ex_9", src: soundAsset+"ex_9.ogg"},
			{id: "ex_10", src: soundAsset+"ex_10.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// aeroplanesound = createjs.Sound.play("aeroplane_sound", {loop: -1, volume:0.32});
		// current_sound = createjs.Sound.play('sound_1');
		// current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*===== This function splits the string in data into convential fraction used in mathematics =====*/
    function splitintofractions($splitinside){
   		typeof $splitinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
       	if($splitintofractions.length > 0){
       		$.each($splitintofractions, function(index, value){
	        	$this = $(this);
	        	var tobesplitfraction = $this.html();
	        	if($this.hasClass('fraction')){
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
	        	}else{
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
	        	}


				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
	        	$this.html(tobesplitfraction);
	        });
       	}
   	}
   	/*===== split into fractions end =====*/

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		texthighlight($board);
		splitintofractions($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);

		//randomize options
		var parent = $(".optionsblock");
		var divs = parent.children();
		parent.append(divs.splice(0, 1)[0]);
		while (divs.length) {
	    	parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
		$('.question_number').html(data.string.e1_title);
		var wrong_clicked = false;
		$(".scoreboard").html(score+"/"+$total_page);
		sound_player("ex_"+(countNext+1),0);
		var $grid = $(".hintimageblock ");
		var max = 0;
		var appendcontent = "<label class='cell'> &nbsp; </label>";
		switch(parseInt($(".instruction2").data("type"))){
			case 10:
				max = 10;
				appendcontent = "<label class='cell cell2'> &nbsp; </label>";
				$grid.css("padding", "28% 5% 16% 5%");
				break;
			default:
				max = 100;
				break;
		}
		for (var i = 0; i< max; i++){
			$grid.append(appendcontent);
		}

		var highlightcells = parseInt($(".instruction2").data("answer"));
		var $cell = $(".cell");
		for (var i = 0; i< highlightcells; i++){
			$($cell[i]).css("background-color", "#CBC4E1");
		}

		$(".options").append("<img class='corincor right' src='"+(imgpath+"correct.png")+"'/>");
		$(".options").append("<img class='corincor wrong' src='"+(imgpath+"incorrect.png")+"'/>");

		$(".options").click(function(){
			createjs.Sound.stop();
			if($(this).hasClass("class1")){
				if(!wrong_clicked){
					// rhino.update(true);
					score++;
				}
				$(".options").css('pointer-events', 'none');
				$(this).css({
					'border': '3px solid #FCD172',
					'background-color': '#6EB260',
					'color': 'white'
				});
				play_correct_incorrect_sound(1);
				$(this).children(".right").show(0);
				if(countNext != $total_page)
					$nextBtn.show(0);
				$(".scoreboard").html(score+"/"+$total_page);
			}
			else{
				if(!wrong_clicked){
					// rhino.update(false);
				}
				$(".hint"+(countNext+1)).css("background-color", "#A8CAE1");
				$(this).css({
					'background-color': '#BA6B82',
					'border': 'none'
				});
				wrong_clicked = true;
				play_correct_incorrect_sound(0);
				$(this).children(".wrong").show(0);
			}
		});
	}

	function sound_player(sound_id, nextFlag){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete',function(){
			nextFlag?nav_button_controls():"";
		});
	}
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();
        loadTimelineProgress($total_page,countNext+1);

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	//call the slide indication bar handler for pink indicators


	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		if(countNext == $total_page){
			TotalQues = $total_page;
			$(".board").html("").css("background-color", "#9DC4DD");
			create_exercise_menu_bar();
			$nextBtn.hide(0);
		}else{
			templateCaller();
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

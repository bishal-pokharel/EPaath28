var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/" + $lang + "/";

var content = [
  //exercise 1
  {
    contentblockadditionalclass: "creambg",
    extratxtdiv: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "qnwrd",
        textclass: "question",
        textdata: data.string.exc_q1
      }
    ],
    exetype1: [
      {
        optionsdivclass: "opnDiv3",
        exeoptions: [
          {
            optaddclass: "correct",
            datahighlightflag: true,
            datahighlightcustomclass: "opn1"
            // optdata:data.string.exc_q1_op1
          },
          {
            // optdata:data.string.exc_q1_op2
          },
          {
            // optdata:data.string.exc_q1_op3
          },
          {
            // optdata:data.string.exc_q1_op4
          }
        ]
      }
    ]
  },
  //exercise 2
  {
    contentblockadditionalclass: "creambg",
    extratxtdiv: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "qnwrd",
        textclass: "question",
        textdata: data.string.exc_q2
      }
    ],
    exetype1: [
      {
        optionsdivclass: "opnDiv3",
        exeoptions: [
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn1",
            optaddclass: "correct",
            optdata: data.string.exc_q2_op1
          },
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn2",
            optdata: data.string.exc_q2_op2
          },
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn3",
            optdata: data.string.exc_q2_op3
          },
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn4",
            optdata: data.string.exc_q2_op4
          }
        ]
      }
    ]
  },
  //exercise 3
  {
    contentblockadditionalclass: "creambg",
    extratxtdiv: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "qnwrd",
        textclass: "question",
        textdata: data.string.exc_q3
      }
    ],
    exetype1: [
      {
        optionsdivclass: "opnDiv3",
        exeoptions: [
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn1",
            optaddclass: "correct",
            optdata: data.string.exc_q3_op1
          },
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn2",
            optdata: data.string.exc_q3_op2
          },
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn3",
            optdata: data.string.exc_q3_op3
          },
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn4",
            optdata: data.string.exc_q3_op4
          }
        ]
      }
    ]
  },
  //exercise 4
  {
    contentblockadditionalclass: "creambg",
    extratxtdiv: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "qnwrd",
        textclass: "question",
        textdata: data.string.exc_q4
      }
    ],
    exetype1: [
      {
        optionsdivclass: "opnDiv3 longerDiv",
        exeoptions: [
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn1",
            optaddclass: "correct",
            optdata: data.string.exc_q4_op1,
            startposextra: "s4CorIncor"
          },
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn2",
            optdata: data.string.exc_q4_op2,
            startposextra: "s4CorIncor"
          },
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn3",
            optdata: data.string.exc_q4_op3,
            startposextra: "s4CorIncor"
          },
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn4",
            optdata: data.string.exc_q4_op4,
            startposextra: "s4CorIncor"
          }
        ]
      }
    ]
  },
  //exercise 5
  {
    contentblockadditionalclass: "creambg",
    extratxtdiv: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "qnwrd",
        textclass: "question",
        textdata: data.string.exc_q5
      }
    ],
    exetype1: [
      {
        optionsdivclass: "opnDiv3",
        exeoptions: [
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn1",
            optaddclass: "correct",
            optdata: data.string.exc_q5_op1
          },
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn2",
            optdata: data.string.exc_q5_op2
          },
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn3",
            optdata: data.string.exc_q5_op3
          },
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn4",
            optdata: data.string.exc_q5_op4
          }
        ]
      }
    ]
  },
  //exercise 6
  {
    contentblockadditionalclass: "creambg",
    extratxtdiv: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "qnwrd",
        textclass: "question",
        textdata: data.string.exc_q6
      }
    ],
    exetype1: [
      {
        optionsdivclass: "opnDiv3",
        exeoptions: [
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn1",
            optaddclass: "correct",
            optdata: data.string.exc_q6_op1
          },
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn2",
            optdata: data.string.exc_q6_op2
          },
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn3",
            optdata: data.string.exc_q6_op3
          },
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn4",
            optdata: data.string.exc_q6_op4
          }
        ]
      }
    ]
  },
  //exercise 7
  {
    contentblockadditionalclass: "creambg",
    extratxtdiv: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "qnwrd",
        textclass: "question",
        textdata: data.string.exc_q7
      }
    ],
    exetype1: [
      {
        optionsdivclass: "opnDiv3",
        exeoptions: [
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn1",
            optaddclass: "correct",
            optdata: data.string.exc_q7_op1
          },
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn2",
            optdata: data.string.exc_q7_op2
          },
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn3",
            optdata: data.string.exc_q7_op3
          },
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn4",
            optdata: data.string.exc_q7_op4
          }
        ]
      }
    ]
  },
  //exercise 8
  {
    contentblockadditionalclass: "creambg",
    extratxtdiv: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "qnwrd",
        textclass: "question",
        textdata: data.string.exc_q8
      }
    ],
    exetype1: [
      {
        optionsdivclass: "opnDiv3",
        exeoptions: [
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn1",
            optaddclass: "correct",
            optdata: data.string.exc_q8_op1
          },
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn2",
            optdata: data.string.exc_q8_op2
          },
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn3",
            optdata: data.string.exc_q8_op3
          },
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn4",
            optdata: data.string.exc_q8_op4
          }
        ]
      }
    ]
  },
  //exercise 9
  {
    contentblockadditionalclass: "creambg",
    extratxtdiv: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "qnwrd",
        textclass: "question",
        textdata: data.string.exc_q9
      }
    ],
    exetype1: [
      {
        optionsdivclass: "opnDiv3",
        exeoptions: [
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn1",
            optaddclass: "correct",
            optdata: data.string.exc_q9_op1
          },
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn2",
            optdata: data.string.exc_q9_op2
          },
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn3",
            optdata: data.string.exc_q9_op3
          },
          {
            datahighlightflag: true,
            datahighlightcustomclass: "opn4",
            optdata: data.string.exc_q9_op4
          }
        ]
      }
    ]
  },
  //exercise 10
  {
    contentblockadditionalclass: "creambg",
    extratxtdiv: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "qnwrd",
        textclass: "question",
        textdata: data.string.exc_q10
      }
    ],
    exetype1: [
      {
        optionsdivclass: "opnDiv3",
        exeoptions: [
          {
            optaddclass: "correct",
            datahighlightflag: true,
            datahighlightcustomclass: "opn1"
            // optdata:data.string.exc_q1_op1
          },
          {
            // optdata:data.string.exc_q1_op2
          },
          {
            // optdata:data.string.exc_q1_op3
          },
          {
            // optdata:data.string.exc_q1_op4
          }
        ]
      }
    ]
  }
];

$(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var item_txt = "";
  var countNext = 0;
  var test = false;
  // declarations for exercise
  var dividend,
    dvsr = [],
    quotient,
    i = 2,
    divisor,
    primes,
    ans;

  /*for limiting the questions to 10*/
  var $total_page = 10;
  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      //images		],
      // {id: "marble1", src: imgpath+"new/marble01.png", type: createjs.AbstractLoader.IMAGE},

      // sounds
      { id: "excSnd", src: soundAsset + "s1_p1.ogg" }
    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound); //for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }
  function handleFileLoad(event) {
    // console.log(event.item);
  }
  function handleProgress(event) {
    $("#loading-text").html(parseInt(event.loaded * 100) + "%");
  }
  function handleComplete(event) {
    $("#loading-wrapper").hide(0);
    //initialize varibales
    // call main function
    //scoring.init(10);
    templateCaller();
  }
  //initialize
  init();

  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  /*===============================================
	=            data highlight function            =
	===============================================*/
  function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object"
      ? alert(
          "Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted"
        )
      : null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";
    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
        $(this).attr(
          "data-highlightcustomclass"
        ) /*if there is data-highlightcustomclass defined it is true else it is not*/
          ? (stylerulename = $(this).attr("data-highlightcustomclass"))
          : (stylerulename = "parsedstring");

        texthighlightstarttag = "<span class='" + stylerulename + "'>";
        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
        $(this).html(replaceinstring);
      });
    }
  }
  /*=====  End of data highlight function  ======*/
  function navigationcontroller(islastpageflag) {
    // check if the parameter is defined and if a boolean,
    // update islastpageflag accordingly
    typeof islastpageflag === "undefined"
      ? (islastpageflag = false)
      : typeof islastpageflag != "boolean"
      ? alert(
          "NavigationController : Hi Master, please provide a boolean parameter"
        )
      : null;
  }

  /*values in this array is same as the name of images of eggs in image folder*/

  //create eggs
  var testin = new NumberTemplate();

  //eggTemplate.eggMove(countNext);
  testin.init(10);
  // function to get composites
  function getComposite(max) {
    var sieve = [],
      i,
      j,
      comps = [];
    for (i = 2; i <= max; ++i) {
      if (!sieve[i]) {
        for (j = i << 1; j <= max; j += i) {
          sieve[j] = true;
        }
      } else {
        comps.push(i);
      }
    }
    return comps;
  }
  // function to get primes
  function getPrimes(max) {
    var sieve = [],
      i,
      j,
      primes = [];
    for (i = 2; i <= max; ++i) {
      if (!sieve[i]) {
        // i has not been marked -- it is prime
        primes.push(i);
        for (j = i << 1; j <= max; j += i) {
          sieve[j] = true;
        }
      }
    }
    return primes;
  }
  composites = getComposite(100);
  prime = getPrimes(50);
  // composites.shufflearray();
  console.log(composites);
  // function to get composites ends

  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    texthighlight($board);
    put_image(content, countNext);
    var updateScore = 0;
    // put_image_sec(content, countNext);
    var testcount = 0;

    $nextBtn.hide(0);
    $prevBtn.hide(0);

    /*generate question no at the beginning of question*/
    testin.numberOfQuestions();

    /*for randomizing the options*/
    function randomize(parent) {
      var parent = $(parent);
      var divs = parent.children();
      while (divs.length) {
        parent.append(
          divs.splice(Math.floor(Math.random() * divs.length), 1)[0]
        );
      }
    }
    /*======= SCOREBOARD SECTION ==============*/
    /*random scoreboard eggs*/
    //var i = Math.floor(Math.random() * imageArray.length);
    //var randImg = imageArray[i];
    var ansClicked = false;
    var wrngClicked = false;
    var updateScore = 0;
    // if needed in nepali number
    // var number = countNext + 1;
    // number = ole.nepaliNumber(countNext + 1, $lang);
    // console.log("number is " + number);
    $(".question").prepend(countNext + 1 + ") ");
    // $(".questndiv").append("<p class='qntxt'>"+data.string.exc_qn+"</p>");

    /*for randomizing the options*/
    function randomize(parent) {
      // alert(parent);
      var parent = $(parent);
      var divs = parent.children();
      while (divs.length) {
        parent.append(
          divs.splice(Math.floor(Math.random() * divs.length), 1)[0]
        );
      }
    }

    // functions to generate random numbers
    function giveRandomNum(limit) {
      var rand = Math.floor(Math.random() * (limit - 1 + 1)) + 1;
      return rand;
    }
    function giveRandomNumSec(limit) {
      var rand = Math.floor(Math.random() * (limit - 0 + 1)) + 0;
      return rand;
    }

    // function to write questions and answer optionsdiv
    // spanclass-->class to which numbers are to be written
    // eqvalue-->spanvalue to which the particular  value to be written
    // valArr-->array of avlues to be written to the span value
    function writeValue(spanClass, eqValue, valArr) {
      for (i = 0; i < eqValue; i++) {
        $("." + spanClass + ":eq(" + i + ")").html(valArr[i]);
      }
    }

    // function to find and assign divisor, dividend and quotient from the array of composite numbers
    function assignDivisionElements() {
      dividend = composites[0];
      for (var i = 1; i < dividend; i++) {
        if (dividend % i == 0) {
          dvsr.push(i);
        }
      }
      divisor = dvsr[giveRandomNumSec(dvsr.length - 1)];
      quotient = dividend / divisor;
      ans = dividend / quotient;
    }
    switch (countNext) {
      case 0:
      case 9:
        // if(countNext==0){
        // 	createjs.Sound.stop();
        // 	current_sound = createjs.Sound.play("excSnd");
        // 	current_sound.play();
        // }
        dvsr = [];
        assignDivisionElements();
        // write question
        var qnArr = [dividend, quotient, divisor];
        writeValue("qnwrd", 2, qnArr);

        // write answers
        var AnsArr = [ans, Math.floor(ans / 2), ans + 2, ans + 3];
        writeValue("buttonsel", 4, AnsArr);
        randomize(".optionsdiv");

        // to remove the first element of array so that the number is not repeated
        composites.splice(0, 1);
        randomize(".opnDiv3");
        break;
      case 1:
        dvsr = [];
        assignDivisionElements();

        var qnArr = [dividend, divisor, quotient];
        writeValue("qnwrd", 3, qnArr);
        // opn1
        var ansArr1 = [quotient, divisor, dividend];
        writeValue("opn1", 3, ansArr1);
        // opn2
        var ansArr2 = [quotient + 1, divisor, dividend];
        writeValue("opn2", 3, ansArr2);
        // opn3
        var ansArr3 = [quotient * 2, quotient, quotient];
        writeValue("opn3", 3, ansArr3);
        // opn4
        var ansArr4 = [quotient, divisor + 5, dividend];
        writeValue("opn4", 3, ansArr4);
        composites.splice(0, 1);
        randomize(".opnDiv3");
        break;
      case 2:
        // clear the divisor array holder
        dvsr = [];
        assignDivisionElements();

        var qnArr = [dividend, divisor];
        writeValue("qnwrd", 2, qnArr);
        // opn1
        var ansArr1 = [quotient];
        writeValue("opn1", 1, ansArr1);
        // opn2
        var ansArr2 = [quotient * 2];
        writeValue("opn2", 1, ansArr2);
        // opn3
        var ansArr3 = [quotient + 5];
        writeValue("opn3", 1, ansArr3);
        // opn4
        var ansArr4 = [quotient * 3];
        writeValue("opn4", 1, ansArr4);
        composites.splice(0, 1);
        randomize(".opnDiv3");
        break;
      case 3:
        dvsr = [];
        assignDivisionElements();

        var qnArr = [dividend, divisor, quotient];
        writeValue("qnwrd", 3, qnArr);
        // opn1
        var ansArr1 = [dividend, divisor];
        writeValue("opn1", 2, ansArr1);
        composites.splice(0, 1);
        // opn2
        var ansArr2 = [dividend, quotient - 1];
        writeValue("opn2", 2, ansArr2);
        composites.splice(0, 1);
        // opn3
        var ansArr3 = [dividend * 2, divisor];
        writeValue("opn3", 2, ansArr3);
        composites.splice(0, 1);
        // opn4
        var ansArr4 = [dividend, quotient + 1];
        writeValue("opn4", 2, ansArr4);
        composites.splice(0, 1);
        randomize(".opnDiv3");
        break;
      case 4:
        dvsr = [];
        assignDivisionElements();

        var qnArr = [dividend, quotient];
        writeValue("qnwrd", 2, qnArr);
        // opn1
        var ansArr1 = [divisor];
        writeValue("opn1", 1, ansArr1);
        // opn2
        var ansArr2 = [dividend + 1];
        writeValue("opn2", 1, ansArr2);
        // opn3
        var ansArr3 = [dividend + 2];
        writeValue("opn3", 1, ansArr3);
        // opn1
        var ansArr4 = [dividend - 2];
        writeValue("opn4", 1, ansArr4);
        composites.splice(0, 1);
        randomize(".opnDiv3");
        break;
      case 5:
        dvsr = [];
        assignDivisionElements();

        var qnArr = [quotient, divisor, dividend];
        writeValue("qnwrd", 3, qnArr);
        // opn1
        var ansArr1 = [dividend, divisor, quotient];
        writeValue("opn1", 3, ansArr1);
        // opn2
        var ansArr2 = [dividend + 1, divisor, quotient];
        writeValue("opn2", 3, ansArr2);
        // opn3
        var ansArr3 = [dividend - 1, divisor + 1, quotient * 2];
        writeValue("opn3", 3, ansArr3);
        // opn4
        var ansArr4 = [dividend, divisor, quotient + 3];
        writeValue("opn4", 3, ansArr4);
        composites.splice(0, 1);
        randomize(".opnDiv3");
        break;
      case 6:
        dvsr = [];
        assignDivisionElements();

        var qnArr = [dividend, divisor];
        writeValue("qnwrd", 2, qnArr);
        // opn1
        var ansArr1 = [dividend, divisor, quotient];
        writeValue("opn1", 3, ansArr1);
        // opn2
        var ansArr2 = [dividend, divisor, quotient + 2];
        writeValue("opn2", 3, ansArr2);
        // opn3
        var ansArr3 = [dividend, divisor * 2, quotient];
        writeValue("opn3", 3, ansArr3);
        // opn4
        var ansArr4 = [dividend, divisor, quotient - 1];
        writeValue("opn4", 3, ansArr4);
        composites.splice(0, 1);
        randomize(".opnDiv3");
        break;
      case 7:
        dvsr = [];
        assignDivisionElements();

        var qnArr = [dividend, divisor];
        writeValue("qnwrd", 2, qnArr);
        // opn1
        var ansArr1 = [quotient];
        writeValue("opn1", 1, ansArr1);
        // opn2
        var ansArr2 = [quotient + 2];
        writeValue("opn2", 1, ansArr2);
        // opn3
        var ansArr3 = [quotient - 1];
        writeValue("opn3", 1, ansArr3);
        // opn4
        var ansArr4 = [quotient * 3];
        writeValue("opn4", 1, ansArr4);
        composites.splice(0, 1);
        randomize(".opnDiv3");
        break;
      case 8:
        dvsr = [];
        assignDivisionElements();
        var qnArr = [divisor, dividend];
        writeValue("qnwrd", 2, qnArr);
        // opn1
        var ansArr1 = [dividend, divisor, quotient];
        writeValue("opn1", 3, ansArr1);
        // opn2
        var ansArr2 = [dividend, divisor, dividend - divisor];
        writeValue("opn2", 3, ansArr2);
        // opn3
        var ansArr3 = [quotient, dividend, quotient * dividend];
        writeValue("opn3", 3, ansArr3);
        // opn4
        var ansArr4 = [dividend, quotient, quotient + dividend];
        writeValue("opn4", 3, ansArr4);
        composites.splice(0, 1);
        randomize(".opnDiv3");
        break;
    }
    //handle mcq
    $(".buttonsel").click(function() {
      createjs.Sound.stop();
      $(this).removeClass("forhover");
      if (ansClicked == false) {
        /*class 1 is always for the right answer. updates scoreboard and disables other click if
				right answer is clicked*/
        if ($(this).hasClass("correct")) {
          if (wrngClicked == false) {
            testin.update(true);
          }
          play_correct_incorrect_sound(1);
          $(this).css({
            background: "#bed62fff",
            border: "5px solid #deef3c",
            color: "#000",
            "pointer-events": "none"
          });
          $(this)
            .siblings(".corctopt")
            .show(0);
          $(".buttonsel").css("pointer-events", "none");
          $(".buttonsel").removeClass("forhover forhoverimg");
          $(".buttonsel").removeClass("clock-hover");
          $nextBtn.show(0);
          // navigationcontroller();
          ansClicked = true;
          // if(countNext != $total_page)
          // $nextBtn.show(0);
        } else {
          testin.update(false);
          play_correct_incorrect_sound(0);
          $(this).css({
            background: "#FF0000",
            border: "5px solid #980000",
            color: "#000",
            "pointer-events": "none"
          });
          $(this)
            .siblings(".wrngopt")
            .show(0);
          wrngClicked = true;
        }
      }
    });

    function put_image(content, count) {
      if (content[count].hasOwnProperty("imageblock")) {
        var imageblock = content[count].imageblock[0];
        if (imageblock.hasOwnProperty("imagestoshow")) {
          var imageClass = imageblock.imagestoshow;
          for (var i = 0; i < imageClass.length; i++) {
            var image_src = preload.getResult(imageClass[i].imgid).src;
            //get list of classes
            var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
            var selector = "." + classes_list[classes_list.length - 1];
            // console.log(selector);
            $(selector).attr("src", image_src);
          }
        }
      }
    }

    function put_image_sec(content, count) {
      if (content[count].arrow_nav[0].hasOwnProperty("imageblock")) {
        var imagblockVal = content[count].arrow_nav[0];
        for (var j = 0; j < imagblockVal.imageblock.length; j++) {
          var imageblock = imagblockVal.imageblock[j];
          if (imageblock.hasOwnProperty("imagestoshow")) {
            var imageClass = imageblock.imagestoshow;
            for (var i = 0; i < imageClass.length; i++) {
              var image_src = preload.getResult(imageClass[i].imgid).src;
              //get list of classes
              var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
              var selector = "." + classes_list[classes_list.length - 1];
              $(selector).attr("src", image_src);
            }
          }
        }
      }
    }
    /*======= SCOREBOARD SECTION ==============*/
  }

  function templateCaller() {
    /*always hide next and previousloadimage navigation button unless
		explicitly called from inside a template*/
    $prevBtn.css("display", "none");
    $nextBtn.css("display", "none");

    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();
    /*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

    //call the slide indication bar handler for pink indicators
  }

  // first call to template caller
  // templateCaller(); loadimage($(".myimg3").find("img"),$(".myimg3").find("p"),preload.getResult('ex1typ3qn'+quesNo).src,eval('data.string.ex2typ1op'+quesNo));

  /* navigation buttons event handlers */

  $nextBtn.on("click", function() {
    countNext++;
    testin.gotoNext();
    templateCaller();
  });

  $refreshBtn.click(function() {
    // templateCaller();
  });
  $prevBtn.on("click", function() {
    countNext--;
    templateCaller();

    /* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
    countNext < $total_page
      ? ole.footerNotificationHandler.hideNotification()
      : null;
  });

  function loadimage(imgrep, imgtext, imgsrc, quesNo) {
    imgrep.attr("src", imgsrc);
    imgtext.text(quesNo);
  }

  /*=====  End of Templates Controller Block  ======*/
});

var soundAsset = $ref+"/sounds/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[
	// slide1
	{
		contentblockadditionalclass:'blbg',
		extratextblock:[{
			textclass : 'genEqn frAnim',
			textdata : data.string.generictext,
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: 'background',
				imgid : 'bg05',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			datahighlightflag:'true',
			datahighlightcustomclass:"movTxt",
			textdata : data.string.s3_p1_txt,
			textclass : 'txt1',
			imgclass: 'inverted box',
			imgid : 'txtBx1',
			imgsrc: '',
		}]
	},
	// slide2
	{
		contentblockadditionalclass:'blbg',
		extratextblock:[{
			textclass : 'genEqn',
			textdata : data.string.generictext,
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: 'background',
				imgid : 'bg05',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.s3_p2_txt,
			textclass : 'txt1',
			imgclass: 'inverted box',
			imgid : 'txtBx1',
			imgsrc: '',
		}],
		multableblock:[{
			multableclass:"four_multable"
		}]
	},
	// slide3
	{
		contentblockadditionalclass:'blbg',
		extratextblock:[{
			textclass : 'genEqn',
			textdata : data.string.generictext,
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: 'background',
				imgid : 'bg05',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.s3_p3_txt_1,
			textclass : 'txt1',
			imgclass: 'inverted box',
			imgid : 'txtBx1',
			imgsrc: '',
		}],
		multableblock:[{
			multableclass:"four_multable"
		}]
	},
	// slide3_1
	{
		contentblockadditionalclass:'blbg',
		extratextblock:[{
			textclass : 'genEqn',
			textdata : data.string.generictext,
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: 'background',
				imgid : 'bg05',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.s3_p3_txt,
			textclass : 'txt1',
			imgclass: 'inverted box',
			imgid : 'txtBx1',
			imgsrc: '',
		}],
		multableblock:[{
			multableclass:"four_multable"
		}]
	},
	// slide4----twoblcoks here
	{
		contentblockadditionalclass:'blbg',
		extratextblock:[{
			textclass : 'genEqn',
			textdata : data.string.generictext,
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: 'background',
				imgid : 'bg05',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.s3_p4_txt,
			textclass : 'txt1',
			imgclass: 'inverted box',
			imgid : 'txtBx1',
			imgsrc: '',
		}],
		divblockcontainerclass:"divBlkCont",
		divisionblock:[{
			subdivclass:"divisionBlock divBlk1",
			dividendblock:[{
				divisorblock:true,
				divisorblock:[{
					divisorblockclass:"divisorDiv",
					textclass : 'divtxt',
					textdata : data.string.four,
				}],
				dividendblockclass:"dividendblk",
				subdividend:[{
					subdividendclass:"topdividend",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvdltr1",
						textclass : 'divtxt',
						textdata : data.string.seven,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt',
						textdata : data.string.six,
					}]
				}
				// ,{
				// 	subdividendclass:"secdividend",
				// 	dividendletter:[{
				// 		dividendletterclass:"dvdLetr dvltr2_1 dvdltr1",
				// 		textclass : 'divtxt',
				// 		textdata : data.string.seven,
				// 	},{
				// 		dividendletterclass:"dvdLetr dvdltr2",
				// 		textclass : 'divtxt',
				// 		textdata : data.string.six,
				// 	}]
				// }
				]
			}]
		}]
	},
	// slide5
	{
		contentblockadditionalclass:'blbg',
		extratextblock:[{
			textclass : 'genEqn',
			textdata : data.string.generictext,
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: 'background',
				imgid : 'bg05',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.s3_p5_txt,
			textclass : 'txt1',
			imgclass: 'inverted box',
			imgid : 'txtBx1',
			imgsrc: '',
		}],
		divblockcontainerclass:"divBlkCont",
		divisionblock:[{
			subdivclass:"divisionBlock divBlk1",
			dividendblock:[{
				divisorblock:true,
				divisorblock:[{
					divisorblockclass:"divisorDiv",
					textclass : 'divtxt',
					textdata : data.string.four,
				}],
				dividendblockclass:"dividendblk",
				subdividend:[{
					subdividendclass:"topdividend",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvdltr1",
						textclass : 'divtxt pnkTxt',
						textdata : data.string.seven,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt pnkTxt',
						textdata : data.string.six,
					}]
				}]
			}]
		}]
	},
	// slide6
	{
		contentblockadditionalclass:'blbg',
		extratextblock:[{
			textclass : 'genEqn',
			textdata : data.string.generictext,
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: 'background',
				imgid : 'bg05',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.s3_p6_txt,
			textclass : 'txt1',
			imgclass: 'inverted box',
			imgid : 'txtBx1',
			imgsrc: '',
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
				exeoptions:[{
					optaddclass:"class1",
					optdata:data.string.seven
				},{
					optdata:data.string.six
				}]
		}],
		divblockcontainerclass:"divBlkCont",
		divisionblock:[{
			subdivclass:"divisionBlock divBlk1",
			dividendblock:[{
				divisorblock:true,
				divisorblock:[{
					divisorblockclass:"divisorDiv",
					textclass : 'divtxt',
					textdata : data.string.four,
				}],
				dividendblockclass:"dividendblk",
				subdividend:[{
					subdividendclass:"topdividend",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvdltr1",
						textclass : 'divtxt ltr_1_1 pnkTxt',
						textdata : data.string.seven,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt pnkTxt',
						textdata : data.string.six,
					}]
				}]
			}]
		}]
	},
	// slide7
	{
		contentblockadditionalclass:'blbg',
		extratextblock:[{
			textclass : 'genEqn',
			textdata : data.string.generictext,
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: 'background',
				imgid : 'bg05',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.s3_p7_txt,
			textclass : 'txt1',
			imgclass: 'inverted box',
			imgid : 'txtBx1',
			imgsrc: '',
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
				exeoptions:[{
					optaddclass:"class1",
					optdata:data.string.yes
				},{
					optdata:data.string.no
				}]
		}],
		divblockcontainerclass:"divBlkCont",
		divisionblock:[{
			subdivclass:"divisionBlock divBlk1",
			dividendblock:[{
				divisorblock:true,
				divisorblock:[{
					divisorblockclass:"divisorDiv",
					textclass : 'divtxt pnkTxt',
					textdata : data.string.four,
				}],
				dividendblockclass:"dividendblk",
				subdividend:[{
					subdividendclass:"topdividend",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvdltr1",
						textclass : 'divtxt ltr_1_1 ',
						textdata : data.string.seven,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt',
						textdata : data.string.six,
					}]
				}]
			}]
		}]
	},
	// slide8
	{
		contentblockadditionalclass:'blbg',
		extratextblock:[{
			textclass : 'genEqn',
			textdata : data.string.generictext,
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: 'background',
				imgid : 'bg05',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.s3_p8_txt,
			textclass : 'txt1',
			imgclass: 'inverted box',
			imgid : 'txtBx1',
			imgsrc: '',
		}],
		divblockcontainerclass:"divBlkCont",
		divisionblock:[{
			subdivclass:"divisionBlock divBlk1",
			dividendblock:[{
				divisorblock:true,
				divisorblock:[{
					divisorblockclass:"divisorDiv",
					textclass : 'divtxt pnkTxt',
					textdata : data.string.four,
				}],
				dividendblockclass:"dividendblk",
				subdividend:[{
					subdividendclass:"topdividend",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvdltr1",
						textclass : 'divtxt ltr_1_1 encircleBlue',
						textdata : data.string.seven,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt',
						textdata : data.string.six,
					}]
				}]
			}]
		}]
	},
	// slide9
	{
		contentblockadditionalclass:'blbg',
		extratextblock:[{
			textclass : 'genEqn',
			textdata : data.string.generictext,
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: 'background',
				imgid : 'bg05',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1-lft5',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.s3_p9_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'txtBx1',
			imgsrc: '',
		}],
		divblockcontainerclass:"divBlkContSec",
		mulwithdiv:true,
		multblock:[{
			multableclass:"four_multableSec"
		}],
		divisionblock:[{
			subdivclass:"divisionBlock divBlk1",
			dividendblock:[{
				divisorblock:true,
				divisorblock:[{
					divisorblockclass:"divisorDiv",
					textclass : 'divtxt',
					textdata : data.string.four,
				}],
				dividendblockclass:"dividendblk",
				subdividend:[{
					subdividendclass:"topdividend",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvdltr1",
						textclass : 'divtxt ltr_1_1 encircleBlue',
						textdata : data.string.seven,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt',
						textdata : data.string.six,
					}]
				}]
			}]
		}]
	},
	// slide10
	{
		contentblockadditionalclass:'blbg',
		extratextblock:[{
			textclass : 'genEqn',
			textdata : data.string.generictext,
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: 'background',
				imgid : 'bg05',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1-lft5',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.s3_p10_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'txtBx1',
			imgsrc: '',
		}],
		divblockcontainerclass:"divBlkContSec",
		mulwithdiv:true,
		multblock:[{
			multableclass:"four_multableSec"
		}],
		divisionblock:[{
			subdivclass:"divisionBlock divBlk1",
			dividendblock:[{
				divisorblock:true,
				divisorblock:[{
					divisorblockclass:"divisorDiv",
					textclass : 'divtxt',
					textdata : data.string.four,
				}],
				dividendblockclass:"dividendblk",
				subdividend:[{
					subdividendclass:"topdividend",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvdltr1",
						textclass : 'divtxt ltr_1_1 encircleBlue',
						textdata : data.string.seven,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt',
						textdata : data.string.six,
					}]
				}]
			}]
		}]
	},
	// slide11
	{
		contentblockadditionalclass:'blbg',
		extratextblock:[{
			textclass : 'genEqn',
			textdata : data.string.generictext,
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: 'background',
				imgid : 'bg05',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1-lft5',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.s3_p11_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'txtBx1',
			imgsrc: '',
		}],
		divblockcontainerclass:"divBlkContSec",
		mulwithdiv:true,
		multblock:[{
			multableclass:"four_multableSec"
		}],
		divisionblock:[{
			subdivclass:"divisionBlock divBlk1",
			dividendblock:[{
				divisorblock:true,
				divisorblock:[{
					divisorblockclass:"divisorDiv",
					textclass : 'divtxt',
					textdata : data.string.four,
				}],
				dividendblockclass:"dividendblk",
				subdividend:[{
					subdividendclass:"topdividend",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvdltr1",
						textclass : 'divtxt ltr_1_1 encircleBlue',
						textdata : data.string.seven,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt',
						textdata : data.string.six,
					}]
				}]
			}]
		}]
	},
	// slide12
	{
		contentblockadditionalclass:'blbg',
		extratextblock:[{
			textclass : 'genEqn',
			textdata : data.string.generictext,
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: 'background',
				imgid : 'bg05',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1-lft5',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.s3_p12_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'txtBx1',
			imgsrc: '',
		}],
		divblockcontainerclass:"divBlkContSec",
		mulwithdiv:true,
		multblock:[{
			multableclass:"four_multableSec"
		}],
		divisionblock:[{
			subdivclass:"divisionBlock divBlk1",
			dividendblock:[{
				divisorblock:true,
				divisorblock:[{
					divisorblockclass:"divisorDiv",
					textclass : 'divtxt',
					textdata : data.string.four,
				}],
				dividendblockclass:"dividendblk",
				subdividend:[{
					subdividendclass:"topdividend",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvdltr1",
						textclass : 'divtxt ltr_1_1 encircleBlue',
						textdata : data.string.seven,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt',
						textdata : data.string.six,
					}]
				}]
			}]
		}]
	},
	// slide13
	{
		contentblockadditionalclass:'blbg',
		extratextblock:[{
			textclass : 'genEqn',
			textdata : data.string.generictext,
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: 'background',
				imgid : 'bg05',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1-lft5',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.s3_p13_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'txtBx1',
			imgsrc: '',
		}],
		divblockcontainerclass:"divBlkContSec",
		mulwithdiv:true,
		multblock:[{
			multableclass:"four_multableSec"
		}],
		divisionblock:[{
			subdivclass:"divisionBlock divBlk1",
			dividendblock:[{
				divisorblock:true,
				divisorblock:[{
					divisorblockclass:"divisorDiv",
					textclass : 'divtxt',
					textdata : data.string.four,
				}],
				dividendblockclass:"dividendblk",
				subdividend:[{
					subdividendclass:"topdividend",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvdltr1",
						textclass : 'divtxt ltr_1_1 encircleBlue',
						textdata : data.string.seven,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt',
						textdata : data.string.six,
					}]
				},{
					subdividendclass:"secdividend",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvltr2_1 dvdltr1",
						textclass : 'divtxt',
						textdata : data.string.four,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt',
						// textdata : data.string.six,
					}]
				}]
			}]
		}]
	},
	// slide14-----has 2 steps
	{
		contentblockadditionalclass:'blbg',
		extratextblock:[{
			textclass : 'genEqn',
			textdata : data.string.generictext,
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: 'background',
				imgid : 'bg05',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1-lft5',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.s3_p14_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'txtBx1',
			imgsrc: '',
		}],
		divblockcontainerclass:"divBlkContSec",
		mulwithdiv:true,
		multblock:[{
			multableclass:"four_multableSec"
		}],
		divisionblock:[{
			subdivclass:"divisionBlock divBlk1",
			dividendblock:[{
				dividendblockclass:"dividendblk",
				divisorblock:true,
				divisorblock:[{
					divisorblockclass:"divisorDiv",
					textclass : 'divtxt',
					textdata : data.string.four,
				}],
				subdividend:[{
					subdividendclass:"topdividend",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvdltr1",
						textclass : 'divtxt ltr_1_1 encircleBlue',
						textdata : data.string.seven,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt',
						textdata : data.string.six,
					}]
				},{
					subdividendclass:"secdividend brdrbtm",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvltr2_1 dvdltr1",
						textclass : 'divtxt',
						textdata : data.string.four,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt',
						// textdata : data.string.six,
					}]
				}]
			}]
		},{
			subdivclass:"divisionBlock divBlk2",
			dividendblock:[{
				dividendblockclass:"dividendblkSec",
				subdividend:[{
					subdividendclass:"topdividendSec",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvdltr1",
						textclass : 'divtxt ltr_1_1',
						textdata : data.string.three,
					}
					,{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt',
						// textdata : data.string.six,
					}
				]
				}
				// ,{
				// 	subdividendclass:"secdividend",
				// 	dividendletter:[{
				// 		dividendletterclass:"dvdLetr dvltr2_1 dvdltr1",
				// 		textclass : 'divtxt',
				// 		textdata : data.string.four,
				// 	},{
				// 		dividendletterclass:"dvdLetr dvdltr2",
				// 		textclass : 'divtxt',
				// 		// textdata : data.string.six,
				// 	}]
				// }
			]
			}]
		}]
	},
	// slide15
	{
		contentblockadditionalclass:'blbg',
		extratextblock:[{
			textclass : 'genEqn',
			textdata : data.string.generictext,
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: 'background',
				imgid : 'bg05',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1-lft5',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.s3_p15_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'txtBx1',
			imgsrc: '',
		}],
		divblockcontainerclass:"divBlkContSec",
		mulwithdiv:true,
		multblock:[{
			multableclass:"four_multableSec"
		}],
		divisionblock:[{
			subdivclass:"divisionBlock divBlk1",
			dividendblock:[{
				dividendblockclass:"dividendblk",
				divisorblock:true,
				divisorblock:[{
					divisorblockclass:"divisorDiv",
					textclass : 'divtxt',
					textdata : data.string.four,
				}],
				subdividend:[{
					subdividendclass:"topdividend",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvdltr1",
						textclass : 'divtxt ltr_1_1 encircleBlue',
						textdata : data.string.seven,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt',
						textdata : data.string.six,
					}]
				},{
					subdividendclass:"secdividend brdrbtm",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvltr2_1 dvdltr1",
						textclass : 'divtxt',
						textdata : data.string.four,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt',
						// textdata : data.string.six,
					}]
				}]
			}]
		},{
			subdivclass:"divisionBlock divBlk2",
			dividendblock:[{
				dividendblockclass:"dividendblkSec",
				subdividend:[{
					subdividendclass:"topdividendSec",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvdltr1",
						textclass : 'divtxt ltr_1_1',
						textdata : data.string.three,
					}
					,{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt sixTxt',
						textdata : data.string.six,
					}
				]
				}
			]
			}]
		}]
	},
	// slide16
	{
		contentblockadditionalclass:'blbg',
		extratextblock:[{
			textclass : 'genEqn',
			textdata : data.string.generictext,
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: 'background',
				imgid : 'bg05',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1-lft5',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.s3_p16_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'txtBx1',
			imgsrc: '',
		}],
		divblockcontainerclass:"divBlkContSec",
		mulwithdiv:true,
		multblock:[{
			multableclass:"four_multableSec"
		}],
		divisionblock:[{
			subdivclass:"divisionBlock divBlk1",
			dividendblock:[{
				dividendblockclass:"dividendblk",
				divisorblock:true,
				divisorblock:[{
					divisorblockclass:"divisorDiv",
					textclass : 'divtxt',
					textdata : data.string.four,
				}],
				subdividend:[{
					subdividendclass:"topdividend",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvdltr1",
						textclass : 'divtxt ltr_1_1',
						textdata : data.string.seven,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt',
						textdata : data.string.six,
					}]
				},{
					subdividendclass:"secdividend brdrbtm",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvltr2_1 dvdltr1",
						textclass : 'divtxt',
						textdata : data.string.four,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt',
						// textdata : data.string.six,
					}]
				}]
			}]
		},{
			subdivclass:"divisionBlock divBlk2",
			dividendblock:[{
				dividendblockclass:"dividendblkSec",
				subdividend:[{
					subdividendclass:"topdividendSec",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvdltr1",
						textclass : 'divtxt ltr_1_1',
						textdata : data.string.three,
					}
					,{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt sixTxt',
						textdata : data.string.six,
					}
				]
				}
			]
			}]
		}]
	},
	// slide17
	{
		contentblockadditionalclass:'blbg',
		extratextblock:[{
			textclass : 'genEqn',
			textdata : data.string.generictext,
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: 'background',
				imgid : 'bg05',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1-lft5',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.s3_p17_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'txtBx1',
			imgsrc: '',
		}],
		divblockcontainerclass:"divBlkContSec",
		mulwithdiv:true,
		multblock:[{
			multableclass:"four_multableSec"
		}],
		divisionblock:[{
			subdivclass:"divisionBlock divBlk1",
			dividendblock:[{
				dividendblockclass:"dividendblk",
				divisorblock:true,
				divisorblock:[{
					divisorblockclass:"divisorDiv",
					textclass : 'divtxt',
					textdata : data.string.four,
				}],
				subdividend:[{
					subdividendclass:"topdividend",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvdltr1",
						textclass : 'divtxt ltr_1_1',
						textdata : data.string.seven,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt',
						textdata : data.string.six,
					}]
				},{
					subdividendclass:"secdividend brdrbtm",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvltr2_1 dvdltr1",
						textclass : 'divtxt',
						textdata : data.string.four,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt',
						// textdata : data.string.six,
					}]
				}]
			}]
		},{
			subdivclass:"divisionBlock divBlk2",
			dividendblock:[{
				dividendblockclass:"dividendblkSec",
				subdividend:[{
					subdividendclass:"topdividendSec",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvdltr1",
						textclass : 'divtxt ltr_1_1',
						textdata : data.string.three,
					}
					,{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt sixTxt',
						textdata : data.string.six,
					}
				]
				}
			]
			}]
		}]
	},
	// slide18
	{
		contentblockadditionalclass:'blbg',
		extratextblock:[{
			textclass : 'genEqn',
			textdata : data.string.generictext,
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: 'background',
				imgid : 'bg05',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1-lft5',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.s3_p18_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'txtBx1',
			imgsrc: '',
		}],
		divblockcontainerclass:"divBlkContSec",
		mulwithdiv:true,
		multblock:[{
			multableclass:"four_multableSec"
		}],
		divisionblock:[{
			subdivclass:"divisionBlock divBlk1",
			dividendblock:[{
				dividendblockclass:"dividendblk",
				divisorblock:true,
				divisorblock:[{
					divisorblockclass:"divisorDiv",
					textclass : 'divtxt',
					textdata : data.string.four,
				}],
				subdividend:[{
					subdividendclass:"topdividend",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvdltr1",
						textclass : 'divtxt ltr_1_1',
						textdata : data.string.seven,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt',
						textdata : data.string.six,
					}]
				},{
					subdividendclass:"secdividend brdrbtm",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvltr2_1 dvdltr1",
						textclass : 'divtxt',
						textdata : data.string.four,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt',
						// textdata : data.string.six,
					}]
				}]
			}]
		},{
			subdivclass:"divisionBlock divBlk2",
			dividendblock:[{
				dividendblockclass:"dividendblkSec",
				subdividend:[{
					subdividendclass:"topdividendSec",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvdltr1",
						textclass : 'divtxt ltr_1_1',
						textdata : data.string.three,
					}
					,{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt sixTxt',
						textdata : data.string.six,
					}
				]
				}
			]
			}]
		}]
	},
	// slide19
	{
		contentblockadditionalclass:'blbg',
		extratextblock:[{
			textclass : 'genEqn',
			textdata : data.string.generictext,
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: 'background',
				imgid : 'bg05',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1-lft5',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.s3_p19_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'txtBx1',
			imgsrc: '',
		}],
		divblockcontainerclass:"divBlkContSec",
		mulwithdiv:true,
		multblock:[{
			multableclass:"four_multableSec"
		}],
		divisionblock:[{
			subdivclass:"divisionBlock divBlk1",
			dividendblock:[{
				dividendblockclass:"dividendblk",
				divisorblock:true,
				divisorblock:[{
					divisorblockclass:"divisorDiv",
					textclass : 'divtxt',
					textdata : data.string.four,
				}],
				subdividend:[{
					subdividendclass:"topdividend",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvdltr1",
						textclass : 'divtxt ltr_1_1',
						textdata : data.string.seven,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt',
						textdata : data.string.six,
					}]
				},{
					subdividendclass:"secdividend brdrbtm",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvltr2_1 dvdltr1",
						textclass : 'divtxt',
						textdata : data.string.four,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt',
						// textdata : data.string.six,
					}]
				}]
			}]
		},{
			subdivclass:"divisionBlock divBlk2",
			dividendblock:[{
				dividendblockclass:"dividendblkSec",
				subdividend:[{
					subdividendclass:"topdividendSec",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvdltr1",
						textclass : 'divtxt ltr_1_1',
						textdata : data.string.three,
					}
					,{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt sixTxt',
						textdata : data.string.six,
					}
				]
				}
				,{
					subdividendclass:"secdividend brdrbtm",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvltr2_1 dvdltr1",
						textclass : 'divtxt',
						textdata : data.string.three,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt',
						textdata : data.string.six,
					}]
				}
			]
			}]
		}]
	},
	// slide20
	{
		contentblockadditionalclass:'blbg',
		extratextblock:[{
			textclass : 'genEqn',
			textdata : data.string.generictext,
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: 'background',
				imgid : 'bg05',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1-lft5',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.s3_p20_txt_1,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'txtBx1',
			imgsrc: '',
		}],
		divblockcontainerclass:"divBlkContSec",
		mulwithdiv:true,
		multblock:[{
			multableclass:"four_multableSec"
		}],
		divisionblock:[{
			subdivclass:"divisionBlock divBlk1",
			dividendblock:[{
				dividendblockclass:"dividendblk",
				divisorblock:true,
				divisorblock:[{
					divisorblockclass:"divisorDiv",
					textclass : 'divtxt',
					textdata : data.string.four,
				}],
				subdividend:[{
					subdividendclass:"topdividend",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvdltr1",
						textclass : 'divtxt ltr_1_1',
						textdata : data.string.seven,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt',
						textdata : data.string.six,
					}]
				},{
					subdividendclass:"secdividend brdrbtm",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvltr2_1 dvdltr1",
						textclass : 'divtxt',
						textdata : data.string.four,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt',
						// textdata : data.string.six,
					}]
				}]
			}]
		},{
			subdivclass:"divisionBlock divBlk2",
			dividendblock:[{
				dividendblockclass:"dividendblkSec",
				subdividend:[{
					subdividendclass:"topdividendSec",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvdltr1",
						textclass : 'divtxt ltr_1_1',
						textdata : data.string.three,
					}
					,{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt sixTxt',
						textdata : data.string.six,
					}
				]},{
					subdividendclass:"secdividend brdrbtm",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvltr3_1 dvltr2_1 dvdltr1",
						textclass : 'divtxt',
						textdata : data.string.three,
					},{
						dividendletterclass:"dvdLetr dvltr3_2 dvdltr2",
						textclass : 'divtxt',
						textdata : data.string.six,
					}]
				}]
			}]
		}]
	},
	// slide21
	{
		contentblockadditionalclass:'blbg',
		extratextblock:[{
			textclass : 'genEqn',
			textdata : data.string.generictext,
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: 'background',
				imgid : 'bg05',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1-lft5',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.s3_p20_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'txtBx1',
			imgsrc: '',
		}],
		divblockcontainerclass:"divBlkContSec",
		mulwithdiv:true,
		multblock:[{
			multableclass:"four_multableSec"
		}],
		divisionblock:[{
			subdivclass:"divisionBlock divBlk1",
			dividendblock:[{
				dividendblockclass:"dividendblk",
				divisorblock:true,
				divisorblock:[{
					divisorblockclass:"divisorDiv",
					textclass : 'divtxt',
					textdata : data.string.four,
				}],
				subdividend:[{
					subdividendclass:"topdividend",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvdltr1",
						textclass : 'divtxt ltr_1_1',
						textdata : data.string.seven,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt',
						textdata : data.string.six,
					}]
				},{
					subdividendclass:"secdividend brdrbtm",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvltr2_1 dvdltr1",
						textclass : 'divtxt',
						textdata : data.string.four,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt',
						// textdata : data.string.six,
					}]
				}]
			}]
		},{
			subdivclass:"divisionBlock divBlk2",
			dividendblock:[{
				dividendblockclass:"dividendblkSec",
				subdividend:[{
					subdividendclass:"topdividendSec",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvdltr1",
						textclass : 'divtxt ltr_1_1',
						textdata : data.string.three,
					}
					,{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt sixTxt',
						textdata : data.string.six,
					}
				]},{
					subdividendclass:"secdividend brdrbtm",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvltr3_1 dvltr2_1 dvdltr1",
						textclass : 'divtxt',
						textdata : data.string.three,
					},{
						dividendletterclass:"dvdLetr dvltr3_2 dvdltr2",
						textclass : 'divtxt',
						textdata : data.string.six,
					}]
				}]
			}]
		}]
	},
	// slide22
	{
		contentblockadditionalclass:'blbg',
		extratextblock:[{
			textclass : 'genEqn',
			textdata : data.string.generictext,
		}],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: 'background',
				imgid : 'bg05',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1-lft5',
			datahighlightflag:'true',
			datahighlightcustomclass:"underline",
			textdata : data.string.s3_p21_txt,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'txtBx1',
			imgsrc: '',
		}],
		divblockcontainerclass:"divBlkContSec",
		mulwithdiv:true,
		multblock:[{
			multableclass:"four_multableSec"
		}],
		divisionblock:[{
			subdivclass:"divisionBlock divBlk1",
			dividendblock:[{
				dividendblockclass:"dividendblk",
				divisorblock:true,
				divisorblock:[{
					divisorblockclass:"divisorDiv",
					textclass : 'divtxt',
					textdata : data.string.four,
				}],
				subdividend:[{
					subdividendclass:"topdividend",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvdltr1",
						textclass : 'divtxt ltr_1_1',
						textdata : data.string.seven,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt',
						textdata : data.string.six,
					}]
				},{
					subdividendclass:"secdividend brdrbtm",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvltr2_1  dvdltr1",
						textclass : 'divtxt',
						textdata : data.string.four,
					},{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt',
						// textdata : data.string.six,
					}]
				}]
			}]
		},{
			subdivclass:"divisionBlock divBlk2",
			dividendblock:[{
				dividendblockclass:"dividendblkSec",
				subdividend:[{
					subdividendclass:"topdividendSec",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvdltr1",
						textclass : 'divtxt ltr_1_1',
						textdata : data.string.three,
					}
					,{
						dividendletterclass:"dvdLetr dvdltr2",
						textclass : 'divtxt sixTxt',
						textdata : data.string.six,
					}
				]},{
					subdividendclass:"secdividend brdrbtm",
					dividendletter:[{
						dividendletterclass:"dvdLetr dvltr3_1 dvltr2_1 dvdltr1",
						textclass : 'divtxt',
						textdata : data.string.three,
					},{
						dividendletterclass:"dvdLetr dvltr3_2 dvdltr2",
						textclass : 'divtxt',
						textdata : data.string.six,
					}]
				}]
			}]
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
	var colCount;
	var rowCount;

	var vocabcontroller = new Vocabulary();
	vocabcontroller.init();
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "txtBx1", src: imgpath+"bubble-1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg05", src: imgpath+"bg05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			{id: "S3_P1", src: soundAsset+"s3_p1.ogg"},
			{id: "S3_P2", src: soundAsset+"s3_p2.ogg"},
			{id: "S3_P3", src: soundAsset+"s3_p3.ogg"},

			{id: "S3_P4", src: soundAsset+"s3_p4.ogg"},
			{id: "S3_P5", src: soundAsset+"s3_p5.ogg"},
			{id: "S3_P6", src: soundAsset+"s3_p6.ogg"},
			{id: "S3_P7", src: soundAsset+"s3_p7.ogg"},
			{id: "S3_P8", src: soundAsset+"s3_p8_1.ogg"},
			{id: "S3_P9", src: soundAsset+"s3_p9.ogg"},
			{id: "S3_P10", src: soundAsset+"s3_p10.ogg"},
			{id: "S3_P11", src: soundAsset+"s3_p11.ogg"},
			{id: "S3_P12", src: soundAsset+"s3_p12.ogg"},
			{id: "S3_P13", src: soundAsset+"s3_p13.ogg"},
			{id: "S3_P14", src: soundAsset+"s3_p14.ogg"},
			{id: "S3_P15", src: soundAsset+"s3_p15.ogg"},

			{id: "S3_P16", src: soundAsset+"s3_p16.ogg"},
			{id: "S3_P17", src: soundAsset+"s3_p17.ogg"},
			{id: "S3_P18", src: soundAsset+"s3_p18.ogg"},
			{id: "S3_P19", src: soundAsset+"s3_p19.ogg"},
			{id: "S3_P20", src: soundAsset+"s3_p20.ogg"},
			{id: "S3_P21", src: soundAsset+"s3_p21.ogg"},
			{id: "S3_P22", src: soundAsset+"s3_p22.ogg"},
			{id: "S3_P23", src: soundAsset+"s3_p23.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);

		//function to generate multiplication table of given nummber
		function giveMulTable(parentClass,wrapperclass,num){
			for(var i=1;i<=10;i++){
				$(parentClass).append("<div class='"+wrapperclass+" mul_"+i+"'/>");
			}
			for(var i=1;i<=10;i++){
				$(".mul_"+i).append("<p class='multiplication mp_"+i+"'>"+num+" &#215; "+ i+ " = "+ (num*i)+ "</p>");
			}

		}

		$(".divisorDiv").append("<p class='divisorline'>)</p>");
		$(".dvltr2_1").append("<p class='minus'> - </p>");
		$(".divBlkCont").append("<img class='background' src="+preload.getResult("text_box").src+">");
		$(".four_multable").append("<img class='backgroundMul' src="+preload.getResult("text_box").src+">");
		$(".divBlkContSec").append("<img class='background scrcSec' src="+preload.getResult("text_box").src+">");
		switch(countNext) {
			case 0:
				sound_player_nav("S3_P"+(countNext+1));
				$(".frAnim").delay(1000).animate({
					top:"37%",
					left:"3%"
				},1000);
			break;
			case 1:
			case 2:
			case 3:
				giveMulTable(".four_multable",'multplWrapper',4);
				sound_player_nav("S3_P"+(countNext+1));
			break;
			case 4:
			case 5:
			case 8:
			case 9:
			sound_player_nav("S3_P"+(countNext+1));
			break;
			case 6:
			case 7:
			sound_player("S3_P"+(countNext+1));
				$(".buttonsel").click(function(){
					createjs.Sound.stop();
					if($(this).hasClass("class1")){
						play_correct_incorrect_sound(1);
						$(".txt1").html(eval("data.string.s3_p"+(countNext+1)+"_txt_1"));
						$(".ltr_1_1").addClass("encircleBlue");
						$(this).addClass("correct");
						$(".buttonsel").css("pointer-events","none");
						$(this).siblings(".corctopt").show();
						nav_button_controls(300);
					}else{
						play_correct_incorrect_sound(0);
						$(this).addClass("incorrect");
						$(this).siblings(".wrngopt").show();
					}
				});
			break;
			case 10:
				giveMulTable(".four_multableSec ",'multplWrapper',4);
			sound_player_nav("S3_P"+(countNext+1));
			break;
			case 11:
			sound_player_nav("S3_P"+(countNext+1));
				giveMulTable(".four_multableSec ",'multplWrapper',4);
				$(".mp_1").addClass("borderHighlight");
				setTimeout(function(){
					$(".mp_1").removeClass("borderHighlight");
					$(".mp_2").addClass("borderHighlight");

				},1500);
			break;
			case 12:
				giveMulTable(".four_multableSec ",'multplWrapper',4);
				$(".mp_2").addClass("borderHighlight");
			sound_player_nav("S3_P"+(countNext+1));
			break;
			case 13:
				giveMulTable(".four_multableSec ",'multplWrapper',4);
				$(".topdividend").append("<p class='quotient hidn'> 1 </p>");
				$(".dvltr2_1, .minus").hide();
				$(".quotient").fadeIn(1000, function(){
					$(".dvltr2_1, .minus").delay(600).fadeIn(99, function(){
						$(".secdividend").addClass('brdrBtm');
						sound_player_nav("S3_P"+(countNext+1));
					});
				});
				$(".mp_1,.mp_2").addClass("borderHighlight");
			break;
			case 14:
				giveMulTable(".four_multableSec ",'multplWrapper',4);
				$(".topdividend").append("<p class='quotient'> 1 </p>");
				sound_player_nav("S3_P"+(countNext+1));
			break;
			case 15:
				giveMulTable(".four_multableSec ",'multplWrapper',4);
				$(".topdividend").append("<p class='quotient'> 1 </p>");
				$(".sixTxt").hide();
				$(".sixTxt").delay(1000).fadeIn(300);
			sound_player_nav("S3_P"+(countNext+1));
			break;
			case 16:
				giveMulTable(".four_multableSec ",'multplWrapper',4);
				$(".topdividend").append("<p class='quotient'> 1 </p>");
				$(".topdividendSec").append("<p class='ovalCrcl'></p>");
			sound_player_nav("S3_P"+(countNext+1));
			break;
			case 17:
				giveMulTable(".four_multableSec ",'multplWrapper',4);
				$(".topdividend").append("<p class='quotient'> 1 </p>");
			sound_player_nav("S3_P"+(countNext+1));
			break;
			case 18:
				sound_player_nav("S3_P"+(countNext+1));
				giveMulTable(".four_multableSec ",'multplWrapper',4);
				$(".topdividend").append("<p class='quotient'> 1 </p>");
				var itnCount = 1;
				// function to highlight the dmultipliation count
				var encircleFunction = function encircle(){
					$(".mp_"+itnCount).addClass("borderHighlight");
					$(".mp_"+(itnCount-1)).removeClass("borderHighlight");
					itnCount+=1;
					if(itnCount<=9){
						timeoutvar = setTimeout(function(){
							encircleFunction();
						},500);
					}else{
						$(".topdividendSec").append("<p class='ovalCrcl'></p>");
						$(".quotient").html("19");
						nav_button_controls(300);
					}
				}
				encircleFunction();
			break;
			case 19:
				giveMulTable(".four_multableSec ",'multplWrapper',4);
				$(".topdividend").append("<p class='quotient'> 1 9 </p>");
				sound_player_nav("S3_P"+(countNext+1));
			break;
			case 20:
				giveMulTable(".four_multableSec ",'multplWrapper',4);
				$(".topdividend").append("<p class='quotient'> 1 9 </p>");
				$(".dvltr3_1").append("<p class='remainder'>0</p>");
				$(".remainder").addClass("scaleAnim");
					sound_player_nav("S3_P"+(countNext+1));
			break;
			case 21:
				giveMulTable(".four_multableSec ",'multplWrapper',4);
				$(".topdividend").append("<p class='quotient'> 1 9 </p>");
				$(".dvltr3_1").append("<p class='remainder'>0</p>");
				sound_player_nav("S3_P"+(countNext+1));
			break;
			case 22:
				giveMulTable(".four_multableSec ",'multplWrapper',4);
				$(".topdividend").append("<p class='quotient brdrBlue'> 1 9 </p>");
				$(".dvltr3_1").append("<p class='remainder'>0</p>");
				sound_player_nav("S3_P"+(countNext+1));
			break;
			default:
			sound_player_nav("S1_P"+(countNext+1));
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				// alert(image_src)
				// console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

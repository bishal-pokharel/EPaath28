// var soundAsset = $ref+"/audio_en/";
var soundAsset = $ref+"/sounds/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[
	// slide 1--> coverpage
	{
		extratextblock:[{
			textdata : data.lesson.chapter,
			textclass : 'chaptername',
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: 'fullPage',
				imgid : 'bg',
				imgsrc: '',
			},{
				imgclass: 'starFish',
				imgid : 'star',
				imgsrc: '',
			}]
		}]
	},
	// slide 2
	{
		extratextblock:[{
			textclass : 'fntSize4-josefin textPg2',
			textdata : data.string.s1_p2_txt,
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: 'fullPage',
				imgid : 'bg',
				imgsrc: '',
			},{
				imgclass: 'starFish',
				imgid : 'star',
				imgsrc: '',
			}]
		}]
	},
	// slide 3
	{
		extratextblock:[{
			textclass : 'fntSize4-josefin textPg2',
			textdata : data.string.s1_p2_txt,
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: 'fullPage',
				imgid : 'bg',
				imgsrc: '',
			},{
				imgclass: 'starFish',
				imgid : 'star',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass: 'fullPage inverted',
			imgid : 'origami_bubble01',
			imgsrc: '',
			textclass:'fntSize4 sptext',
			textdata:data.string.s1_p3_txt

		}]
	},
	// slide 4
	{
		extratextblock:[{
			textclass : 'fntSize4-josefin toptxt',
			textdata : data.string.s1_p4_txt,
		},{
			textclass : 'clkHere',
			textdata : data.string.clkhere,
		},{
			textclass : 'eqnBskt',
			textdata : data.string.s1_p4_txt_2,
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: 'starFish t50',
				imgid : 'star',
				imgsrc: '',
			},{
				imgclass: 'fullPage',
				imgid : 'bg03',
				imgsrc: '',
			},{
				imgclass: 'shell shl_1',
				imgid : 'cell01',
				imgsrc: '',
			},{
				imgclass: 'shell shl_2',
				imgid : 'cell02',
				imgsrc: '',
			},{
				imgclass: 'shell shl_3',
				imgid : 'cell01',
				imgsrc: '',
			},{
				imgclass: 'shell shl_4',
				imgid : 'cell02',
				imgsrc: '',
			},{
				imgclass: 'shell shl_5',
				imgid : 'cell01',
				imgsrc: '',
			},{
				imgclass: 'shell shl_6',
				imgid : 'cell02',
				imgsrc: '',
			},{
				imgclass: 'shell shl_7',
				imgid : 'cell01',
				imgsrc: '',
			},{
				imgclass: 'shell shl_8',
				imgid : 'cell02',
				imgsrc: '',
			},{
				imgclass: 'shell shl_9',
				imgid : 'cell01',
				imgsrc: '',
			},{
				imgclass: 'shell shl_10',
				imgid : 'cell02',
				imgsrc: '',
			},{
				imgclass: 'shell shl_11',
				imgid : 'cell01',
				imgsrc: '',
			},{
				imgclass: 'shell shl_12',
				imgid : 'cell02',
				imgsrc: '',
			},{
				imgclass: 'box box_1',
				imgid : 'open_box',
				imgsrc: '',
			},{
				imgclass: 'box box_2',
				imgid : 'open_box',
				imgsrc: '',
			},{
				imgclass: 'box box_3',
				imgid : 'open_box',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgclass: 'fullPage inverted',
			imgid : 'origami_bubble01',
			imgsrc: '',
			textclass:'fntSize4 sptext',
			textdata:data.string.s1_p4_txt_1

		}]
	},
	// slide 5
	{
		extratextblock:[{
			textclass : 'fntSize4-josefin toptxt',
			textdata : data.string.s1_p5_txt,
		},{
			textclass : 'clkHere',
			textdata : data.string.clkhere,
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: 'starFish t40',
				imgid : 'star',
				imgsrc: '',
			},{
				imgclass: 'fullPage',
				imgid : 'bg09',
				imgsrc: '',
			},{
				imgclass: 'shellSec shl_1_1',
				imgid : 'cell01',
				imgsrc: '',
			},{
				imgclass: 'shellSec shl_1_2',
				imgid : 'cell02',
				imgsrc: '',
			},{
				imgclass: 'shellSec shl_1_3',
				imgid : 'cell01',
				imgsrc: '',
			},{
				imgclass: 'shellSec shl_1_4',
				imgid : 'cell02',
				imgsrc: '',
			},{
				imgclass: 'shellSec shl_2_1',
				imgid : 'cell01',
				imgsrc: '',
			},{
				imgclass: 'shellSec shl_2_2',
				imgid : 'cell02',
				imgsrc: '',
			},{
				imgclass: 'shellSec shl_2_3',
				imgid : 'cell01',
				imgsrc: '',
			},{
				imgclass: 'shellSec shl_2_4',
				imgid : 'cell02',
				imgsrc: '',
			},{
				imgclass: 'shellSec shl_3_1',
				imgid : 'cell01',
				imgsrc: '',
			},{
				imgclass: 'shellSec shl_3_2',
				imgid : 'cell02',
				imgsrc: '',
			},{
				imgclass: 'shellSec shl_3_3',
				imgid : 'cell01',
				imgsrc: '',
			},{
				imgclass: 'shellSec shl_3_4',
				imgid : 'cell02',
				imgsrc: '',
			},{
				imgclass: 'boxSec box_1',
				imgid : 'open_box',
				imgsrc: '',
			},{
				imgclass: 'boxSec box_2',
				imgid : 'open_box',
				imgsrc: '',
			},{
				imgclass: 'boxSec box_3',
				imgid : 'open_box',
				imgsrc: '',
			}]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgclass: 'fullPage inverted',
			imgid : 'origami_bubble01',
			imgsrc: '',
			textclass:'fntSize4 sptext',
			textdata:data.string.s1_p5_txt_1

		}]
	},
	// slide 6
	{
		extratextblock:[{
			textclass : 'fntSize4-josefin toptxt',
			textdata : data.string.s1_p6_txt,
		},{
			textclass : 'divMul divClk',
			datahighlightflag:true,
			datahighlightcustomclass:"colChange",
			textdata : data.string.div_eqn,
		},{
			textclass : 'divMul mulclk',
			datahighlightflag:true,
			datahighlightcustomclass:"colChange",
			textdata : data.string.mul_eqn,
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: 'fullPage',
				imgid : 'bg03',
				imgsrc: '',
			},{
				imgclass: 'starFish',
				imgid : 'star',
				imgsrc: '',
			}
			// shells and boxes for multipliation
			,{
				imgclass: 'shellSec shl_1_1',
				imgid : 'cell01',
				imgsrc: '',
			},{
				imgclass: 'shellSec shl_1_2',
				imgid : 'cell02',
				imgsrc: '',
			},{
				imgclass: 'shellSec shl_1_3',
				imgid : 'cell01',
				imgsrc: '',
			},{
				imgclass: 'shellSec shl_1_4',
				imgid : 'cell02',
				imgsrc: '',
			},{
				imgclass: 'shellSec shl_2_1',
				imgid : 'cell01',
				imgsrc: '',
			},{
				imgclass: 'shellSec shl_2_2',
				imgid : 'cell02',
				imgsrc: '',
			},{
				imgclass: 'shellSec shl_2_3',
				imgid : 'cell01',
				imgsrc: '',
			},{
				imgclass: 'shellSec shl_2_4',
				imgid : 'cell02',
				imgsrc: '',
			},{
				imgclass: 'shellSec shl_3_1',
				imgid : 'cell01',
				imgsrc: '',
			},{
				imgclass: 'shellSec shl_3_2',
				imgid : 'cell02',
				imgsrc: '',
			},{
				imgclass: 'shellSec shl_3_3',
				imgid : 'cell01',
				imgsrc: '',
			},{
				imgclass: 'shellSec shl_3_4',
				imgid : 'cell02',
				imgsrc: '',
			},{
				imgclass: 'boxSec box_1',
				imgid : 'open_box',
				imgsrc: '',
			},{
				imgclass: 'boxSec box_2',
				imgid : 'open_box',
				imgsrc: '',
			},{
				imgclass: 'boxSec box_3',
				imgid : 'open_box',
				imgsrc: '',
			},
			// shells and boxes for division
			{
				imgclass: 'shell t35 shl_1',
				imgid : 'cell01',
				imgsrc: '',
			},{
				imgclass: 'shell t35 shl_2',
				imgid : 'cell02',
				imgsrc: '',
			},{
				imgclass: 'shell t35 shl_3',
				imgid : 'cell01',
				imgsrc: '',
			},{
				imgclass: 'shell t35 shl_4',
				imgid : 'cell02',
				imgsrc: '',
			},{
				imgclass: 'shell t35 shl_5',
				imgid : 'cell01',
				imgsrc: '',
			},{
				imgclass: 'shell t35 shl_6',
				imgid : 'cell02',
				imgsrc: '',
			},{
				imgclass: 'shell t35 shl_7',
				imgid : 'cell01',
				imgsrc: '',
			},{
				imgclass: 'shell t35 shl_8',
				imgid : 'cell02',
				imgsrc: '',
			},{
				imgclass: 'shell t35 shl_9',
				imgid : 'cell01',
				imgsrc: '',
			},{
				imgclass: 'shell t35 shl_10',
				imgid : 'cell02',
				imgsrc: '',
			},{
				imgclass: 'shell t35 shl_11',
				imgid : 'cell01',
				imgsrc: '',
			},{
				imgclass: 'shell t35 shl_12',
				imgid : 'cell02',
				imgsrc: '',
			},{
				imgclass: 'box box_1',
				imgid : 'open_box',
				imgsrc: '',
			},{
				imgclass: 'box box_2',
				imgid : 'open_box',
				imgsrc: '',
			},{
				imgclass: 'box box_3',
				imgid : 'open_box',
				imgsrc: '',
			}]
		}],
	},
	// slide 7
	{
		extratextblock:[{
			textindiv:true,
			textdivclass:"midEqnDiv",
			sectextdiv:true,
			sectextdiv:[{
				textclass : 'toptxtInDiv',
				textdata : data.string.s1_p7_txt,
			},{
				sectxtindiv:true,
				sectxtcontainerclass:"mulDivContainer md_1",
				datahighlightflag:true,
				datahighlightcustomclass:"obo",
				textclass : 'muldivTxt mdt_1',
				textdata : data.string.s1_p7_txt_1,
			},{
				sectxtindiv:true,
				sectxtcontainerclass:"mulDivContainer md_2",
				datahighlightflag:true,
				datahighlightcustomclass:"oboSec",
				textclass : 'muldivTxt mdt_2',
				textdata : data.string.s1_p7_txt_2,
			}]
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: 'fullPage',
				imgid : 'bg03',
				imgsrc: '',
			},{
				imgclass: 'shellSec shl_1_1',
				imgid : 'cell01',
				imgsrc: '',
			},{
				imgclass: 'starFish',
				imgid : 'star',
				imgsrc: '',
			}],
		}],
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
	var vocabcontroller = new Vocabulary();
	var setTime,timeout1,timeout2;
	var clkCount=0;
	vocabcontroller.init();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "origami_bubble01", src: imgpath+"bubble-1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg03", src: imgpath+"bg03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg04", src: imgpath+"bg04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg09", src: imgpath+"bg09.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cell01", src: imgpath+"cell01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cell02", src: imgpath+"cell02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "open_box", src: imgpath+"open_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "star", src: imgpath+"star.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			// {id: "dog", src: soundAsset+"S1_P1.ogg"},
			{id: "S1_P1", src: soundAsset+"s1_p1.ogg"},
			{id: "S1_P2", src: soundAsset+"s1_p2.ogg"},
			{id: "S1_P3", src: soundAsset+"s1_p3.ogg"},
			{id: "S1_P4a", src: soundAsset+"s1_p4_1.ogg"},
			{id: "S1_P4b", src: soundAsset+"s1_p4_2.ogg"},
			{id: "S1_P4c", src: soundAsset+"s1_p4_3.ogg"},
			{id: "S1_P5", src: soundAsset+"s1_p5.ogg"},
			{id: "S1_P6", src: soundAsset+"s1_p6.ogg"},
			{id: "S1_P7", src: soundAsset+"s1_p7.ogg"},
			{id: "Popsoundeffect", src: soundAsset+"Popsoundeffect.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		// function to arrange left of shells
		function shellCss(left,leftToAdd){
			for(var i=1;i<=12;i++){
				$(".shl_"+i).css({
					"left":left+"%"
				});
				left = left+leftToAdd;
			}
		}
		// function to animate shells from top into the chests
		function shellAnim(shelCnt,left,leftToAdd,top,topToAdd,shlcount,pg4flag){
			sound_nav("Popsoundeffect",0);
			$(".shl_"+shelCnt).animate({
				top:top+"%",
				left:left+"%",
				width:"6%"
			},500, function(){
				shelCnt+=1;
				if(shelCnt<=3){
					left+=leftToAdd;
					shellAnim(shelCnt,left,leftToAdd,top,topToAdd,shlcount,pg4flag);
				}else if (shelCnt>3 && shelCnt<=6) {
					left+=leftToAdd;
					shlcount==0?left=36:'';
					shlcount+=1;
					shlcount==3?shlcount=0:'';
					shellAnim(shelCnt,left,leftToAdd,top,topToAdd,shlcount,pg4flag);
				}else if (shelCnt>6 && shelCnt<=9) {
					left+=leftToAdd;
					shlcount==0?left=32:'';
					shlcount+=1;
					shlcount==3?shlcount=0:'';
					shellAnim(shelCnt,left,leftToAdd,72,topToAdd,shlcount,pg4flag);
				}else if (shelCnt>9 && shelCnt<=12) {
					left+=leftToAdd;
					shlcount==0?left=36:'';
					shlcount+=1;
					shlcount==3?shlcount=0:'';
					shellAnim(shelCnt,left,leftToAdd,72,topToAdd,shlcount,pg4flag);
				}else{
					if(pg4flag){
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("S1_P4b");
						current_sound.play();
						$(".sp-2").fadeIn(300);
						current_sound.on('complete', function(){
							createjs.Sound.stop();
							current_sound = createjs.Sound.play("S1_P4c");
							current_sound.play();
							$(".eqnBskt").fadeIn(300);
							current_sound.on('complete', function(){
								nav_button_controls(0);
							});
						});

					}else if (countNext==5) {
						// $(".colChange:eq(2)").addClass("pnkTxt");
						$(".mulclk").css("pointer-events","auto");
						clkCount+=1;
						clkCount==2?nav_button_controls(1000):'';
					}
					else{
						nav_button_controls(100);
					}
				}
			});
		}

		// shell css sec
		function shellcssSec(top,left,leftToAdd){
			for(var j=1;j<=4;j++){
				j==2?left=36:j==3?left=32:j==4?left=36:left=left;
				j==3?top=36:top=top;
				for(var i=1;i<=3;i++){
					$(".shl_"+i+"_"+j).css({
						"left":left+"%",
						"top":top+"%"
					});
					left+=leftToAdd;
				}
			}
		}
		// function to animate shell fron the chest
		function bsktToBtm(shlNo,bskNo,left){
			$(".shl_"+bskNo+"_"+shlNo).animate({
					top:"74%",
					left:left+"%"
			},500,function(){
				sound_nav("Popsoundeffect",0);
				left+=7.5;
				shlNo+=1;
				if(shlNo<=4){
					bsktToBtm(shlNo,bskNo,left);
				}else if (shlNo>4) {
					shlNo = 1, bskNo+=1;
					if(bskNo>3){
						if(countNext==5){
							$(".divClk").css("pointer-events","auto");
							clkCount+=1;
							clkCount==2?nav_button_controls(1000):'';
						}else{
							nav_button_controls(0);
						}
					}
					bsktToBtm(shlNo,bskNo,left);
				}
			});
		}
		switch(countNext) {
			case 0:
				sound_player("S1_P"+(countNext+1));
				nav_button_controls(1000);
			break;
			case 1:
			case 2:
			sound_player("S1_P"+(countNext+1));
			break;
			case 3:
				shellCss(7,7.5);
				$(".shell, .box,.clkHere").hide(0);
				$(".sp-2, .eqnBskt").hide(0);
				$(".shell").delay(1000).fadeIn(300);
				$(".box").delay(1500).fadeIn(300);
				$(".clkHere").delay(2000).fadeIn(300);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("S1_P4a");
				current_sound.play();
				current_sound.on('complete', function(){
					$(".clkHere").click(function(){
						$(".clkHere").hide(0);
						shellAnim(1,32,22,67,0,0,1);
					});
				});
			break;
			case 4:
				shellcssSec(33,32,22);
				$(".sp-2").hide(0);
				$(".shellSec, .boxSec,.clkHere").hide(0);
				$(".boxSec").delay(1000).fadeIn(300);
				$(".shellSec").delay(1500).fadeIn(300);
				$(".clkHere").delay(2000).fadeIn(300);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("S1_P5");
				current_sound.play();
				current_sound.on('complete', function(){
					$(".clkHere").click(function(){
						$(".clkHere").hide(0);
						bsktToBtm(1,1,7);
					});
				});
			break;
			case 5:
			sound_nav("S1_P6");
				shellCss(7,7.5);
				shellcssSec(33,32,22);
				$(".shell, .shellSec,.box, .boxSec").hide(0);
				$(".shell, .shellSec,.box, .boxSec").hide(0);
					$(".divClk").click(function(){
						$(".mulclk").removeClass("clickedCss");
						$(this).addClass("clickedCss");
						$(".starFish").hide(0);
						clearTimeout(timeout1);
						$(".shell").css("top","35%");
						shellCss(7,7.5);
						$(".divClk").addClass("clkAnim");
						$(".mulclk, .divClk").css("pointer-events","none");
						$(".shellSec,.boxSec").hide(0);
						// $(".colChange:eq(0)").addClass("pnkTxt");
							$(".shell").fadeIn(300);
						setTimeout(function(){
							$(".box").fadeIn(300);
							// $(".colChange:eq(1)").addClass("pnkTxt");
						},500);
						timeout1 = setTimeout(function(){
								shellAnim(1,32,22,67,0,0,0);
						},1500);
					});

					$(".mulclk").click(function(){
						$(".divClk").removeClass("clickedCss");
						$(this).addClass("clickedCss");
						$(".starFish").hide(0);
						clearTimeout(timeout2);
						shellcssSec(33,32,22);
						$(".mulclk").addClass("clkAnim");
						$(".divClk, .mulclk").css("pointer-events","none");
						$(".shell,.box").hide(0);
						$(".boxSec").fadeIn(300);
						// $(".colChange:eq(1)").addClass("pnkTxt");
						timeout2  = setTimeout(function(){
							$(".shellSec").fadeIn(300);
							// $(".colChange:eq(2)").addClass("pnkTxt");
						},500);
						setTimeout(function(){
							bsktToBtm(1,1,7);
						},1500);
					});
			break;
			case 6:
				$(".obo, .oboSec").hide(0);
				$(".obo:eq(0), .oboSec:eq(0)").delay(1000).fadeIn(300);
				$(".obo:eq(1), .oboSec:eq(1)").delay(1500).fadeIn(300);
				sound_player("S1_P7");
			break;
			default:
			nav_button_controls(1000);

		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(0);
		});
	}
	function sound_nav(sound_id,next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();

	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				// alert(image_src)
				// console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeout1);
		clearTimeout(timeout2);
		clearTimeout(setTime);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
	clearTimeout(timeout1);
	clearTimeout(timeout2);
	clearTimeout(setTime);
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(setTime);
		clearTimeout(timeout1);
		clearTimeout(timeout2);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var soundAsset = $ref+"/sounds/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[
	// slide1
	{
		coverboardadditionalclass:"blBg",
		extratextblock:[{
			textclass : 'diyTxt',
			textdata:data.string.diytxt
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: 'diybg',
				imgid : 'diy',
				imgsrc: '',
			}]
		}]
	},
	// slide2
	{
		extratextblock:[{
			textclass : 'josefin s2p2Toptxt',
			textdata:data.string.s2_p2_txt
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: 'background',
				imgid : 'bg01',
				imgsrc: '',
			},{
				imgclass: 'paperRoll',
				imgid : 'rolling_paper',
				imgsrc: '',
			}]
		}],
		exetype1:[{
			qnclass:"josefin question",
			qndata:data.string.s2_q1,
			optionsdivclass:"optionsdiv",
				exeoptions:[{
					optaddclass:"class1",
					optdata:data.string.s2_q1_op1
				},{
					optdata:data.string.s2_q1_op2
				},{
					optdata:data.string.s2_q1_op3
				},{
					optdata:data.string.s2_q1_op4
				}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass: 'fullPage inverted',
			imgid : 'origami_bubble01',
			imgsrc: '',
			textclass:'fntSize4 sptext',
			textdata:data.string.s2_p2_txt_1

		}]
	},
	// slide3
	{
		extratextblock:[{
			textclass : 'josefin s2p2Toptxt',
			textdata:data.string.s2_p2_txt
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: 'background',
				imgid : 'bg01',
				imgsrc: '',
			},{
				imgclass: 'paperRoll',
				imgid : 'rolling_paper',
				imgsrc: '',
			}]
		}],
		exetype1:[{
			qnclass:"josefin question",
			qndata:data.string.s2_q2,
			optionsdivclass:"optionsdiv",
				exeoptions:[{
					optaddclass:"class1",
					optdata:data.string.s2_q2_op1
				},{
					optdata:data.string.s2_q2_op2
				},{
					optdata:data.string.s2_q2_op3
				},{
					optdata:data.string.s2_q2_op4
				}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass: 'fullPage inverted',
			imgid : 'origami_bubble01',
			imgsrc: '',
			textclass:'fntSize4 sptext',
			textdata:data.string.s2_p2_txt_1

		}]
	},
	// slide4
	{
		extratextblock:[{
			textclass : 'josefin s2p2Toptxt',
			textdata:data.string.s2_p2_txt
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: 'background',
				imgid : 'bg01',
				imgsrc: '',
			},{
				imgclass: 'paperRoll',
				imgid : 'rolling_paper',
				imgsrc: '',
			}]
		}],
		exetype1:[{
			qnclass:"josefin question",
			datahighlightflag:true,
			datahighlightcustomclass:"insquare",
			qndata:data.string.s2_q4_1,
			optionsdivclass:"optionsdiv t50",
				exeoptions:[{
					optaddclass:"class1",
					optdata:data.string.three
				},{
					optdata:data.string.six
				},{
					optdata:data.string.four
				},{
					optdata:data.string.seven
				}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass: 'fullPage inverted',
			imgid : 'origami_bubble01',
			imgsrc: '',
			textclass:'fntSize4 sptext',
			textdata:data.string.s2_q4

		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
	var colCount;
	var rowCount;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg01", src: imgpath+"bg08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "origami_bubble01", src: imgpath+"bubble-1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rolling_paper", src: imgpath+"rolling_paper.png", type: createjs.AbstractLoader.IMAGE},
			{id: "diy", src: imgpath+"diy.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			{id: "S2_P1", src: soundAsset+"s2_p2_1.ogg"},
			{id: "S2_P2", src: soundAsset+"s2_p2_2.ogg"},
			{id: "S2_P3", src: soundAsset+"s2_p4.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	var num ;
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		function giveRandomNum(limit){
			var rand =  Math.floor(Math.random() * (limit - 1 +1)) + 1;
			return rand;
		}
		function giveRandomNumSec(limit){
			var rand =  Math.floor(Math.random() * (limit - 0 +1)) + 0;
			return rand;
		}

		function getComposite(max) {
			var sieve = [], i, j, comps = [];
			for (i = 2; i <= max; ++i) {
					if (!sieve[i]) {
							for (j = i << 1; j <= max; j += i) {
									sieve[j] = true;
							}
					}else{
						comps.push(i);
					}
			}
			return comps;
		}

	 	/*for randomizing the options*/
		function randomize(parent){
			// alert(parent);
			var parent = $(parent);
			var divs = parent.children();
			while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}
		}
		switch(countNext) {
			case 0:
				play_diy_audio();
				nav_button_controls(2000);
			break;
			case 1:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("S2_P1");
			current_sound.play();
			current_sound.on("complete", function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("S2_P2");
				current_sound.play();
				current_sound.on("complete", function(){
				});
			});
			break;
			case 3:
			sound_nav("S2_P3");
				var dividend, dvsr=[], quotient, i=2, divisor;
				composites = getComposite(100);
				dividend = composites[giveRandomNum((composites.length)-1)];
				while(i<dividend){
					if(dividend%i==0){
						dvsr.push(i)
					}
					i++;
				}
				divisor = dvsr[giveRandomNumSec((dvsr.length)-1)];
				quotient = dividend/divisor;
				var ans = dividend/quotient;
				$(".dividend").html(dividend);
				$(".quotient").html(quotient);
				$(".buttonsel:eq(0)").html(ans);
				$(".buttonsel:eq(1)").html(Math.floor(ans/2));
				$(".buttonsel:eq(2)").html(ans+2);
				$(".buttonsel:eq(3)").html(0);
					randomize(".optionsdiv");
			break;
			default:
				randomize(".optionsdiv");
			break;
		}
			$(".buttonsel").click(function(){
				createjs.Sound.stop();
				if($(this).hasClass("class1")){
					play_correct_incorrect_sound(1);
					$(this).addClass("correct");
					$(".buttonsel").css("pointer-events","none");
					$(this).siblings(".corctopt").show();
					nav_button_controls(300);
				}else{
					play_correct_incorrect_sound(0);
					$(this).addClass("incorrect");
					$(this).siblings(".wrngopt").show();
				}
			});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(1000);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				// alert(image_src)
				// console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

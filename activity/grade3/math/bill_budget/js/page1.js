var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		additionalclasscontentblock : '',
		uppertextblockadditionalclass : "covertitle",
		uppertextblock : [{
			textdata : data.lesson.chapter,
			textclass : 'lesson-title1'
		}],
        imageblockadditionalclass: 'coverpage',
        imageblock : [{
            imagestoshow : [
                {
                    imgclass : "cover",
                    imgsrc : imgpath + "cover_page_new.png",
                }],
        }]
	},

	//slide1
	{
		contentblockadditionalclass: '',
		contentblocknocenteradjust : true,

		uppertextblockadditionalclass : "lets_go_shopping my_font_big",
		uppertextblock : [
		{
			textdata : data.string.p1text1,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_red'
		}],
		imageblockadditionalclass: 'go_shop_block image_center',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "go_shopping",
				imgsrc : imgpath + "go_shop.png",
			}],
		}]
	},
	//slide2
	{
		contentblockadditionalclass: 'main_bg',
		contentblocknocenteradjust : true,

		uppertextblockadditionalclass : "instruction my_font_medium",
		uppertextblock : [
		{
			textdata : data.string.p1text2,
			textclass : 'tops_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_red'
		},
		{
			textdata : data.string.p1text3,
			textclass : 'bottoms_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_red'
		},
		{
			textdata : data.string.p1text4,
			textclass : 'shoes_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_red'
		},
		{
			textdata : data.string.p1text5,
			textclass : 'bought_today',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_red'
		}],
		lowertextblockadditionalclass : "instruction_bottom my_font_medium",
		lowertextblock : [
		{
			textdata : data.string.p1text6,
			textclass : 'bought_today',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		}],
		shoppingblockadditionalclass: 'block_cover',
		shoppingblock : [
			//shirts
			{
				imgclass : "shop_items shop_item_1 tops",
				imgsrc : imgpath + "top_1.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 1200'
			},
			{
				imgclass : "shop_items shop_item_2 tops",
				imgsrc : imgpath + "top_2.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 500'
			},
			{
				imgclass : "shop_items shop_item_3 tops",
				imgsrc : imgpath + "top_3.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 900'
			},
			{
				imgclass : "shop_items shop_item_4 tops",
				imgsrc : imgpath + "top_4.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 400'
			},
			//pants
			{
				imgclass : "shop_items shop_item_1 bottoms",
				imgsrc : imgpath + "bottom_1.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 1500'
			},
			{
				imgclass : "shop_items shop_item_2 bottoms",
				imgsrc : imgpath + "bottom_2.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 500'
			},
			{
				imgclass : "shop_items shop_item_3 bottoms",
				imgsrc : imgpath + "bottom_3.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 800'
			},
			{
				imgclass : "shop_items shop_item_4 bottoms",
				imgsrc : imgpath + "bottom_4.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 2500'
			},
			//shoes
			{
				imgclass : "shop_items shop_item_1 shoes",
				imgsrc : imgpath + "shoe_1.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 4000'
			},
			{
				imgclass : "shop_items shop_item_2 shoes",
				imgsrc : imgpath + "shoe_2.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 3500'
			},
			{
				imgclass : "shop_items shop_item_3 shoes",
				imgsrc : imgpath + "shoe_3.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 2000'
			},
			{
				imgclass : "shop_items shop_item_4 shoes",
				imgsrc : imgpath + "shoe_4.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 1000'
			}
		]
	},
	//slide3
	{
		contentblockadditionalclass: 'main_bg',
		contentblocknocenteradjust : true,

		uppertextblockadditionalclass : "user_info my_font_medium",
		uppertextblock : [
		{
			textdata : data.string.p1text7,
			textclass : '',
		}],
		infolabel:[
			{
				infolabelclass: 'user_info_label my_font_medium',
				infolabeldata: '',
			}
		]
	},
	//slide4
	{
		contentblockadditionalclass: 'main_bg',
		contentblocknocenteradjust : true,

		uppertextblockadditionalclass : "user_info my_font_medium",
		uppertextblock : [
		{
			textdata : data.string.p1text8,
			textclass : '',
		}],
		infolabel:[
			{
				infolabelclass: 'user_info_label my_font_medium',
				infolabeldata: '',
			}
		]
	},
	//slide5
	{
		contentblockadditionalclass: 'main_bg',
		contentblocknocenteradjust : true,

		uppertextblockadditionalclass : "instruction my_font_medium",
		uppertextblock : [
		{
			textdata : data.string.p1text9,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		}],

		bill_item:[{
			bill_container: 'bill_center slide_from_top',
			shop_logo: imgpath + "shop_bill/logo_cloth.png",
			shop_name: data.string.sshop2_name,
			shop_address: data.string.sshop2_address,
			customer_name:'Mark',
			customer_address:'USA',
			customer_bill: '300',
			customer_date: '12/12',
			signature: imgpath + "shop_bill/sign.png",
			shopkeeper_name: data.string.sshop2_owner
		}],
		imageblockadditionalclass: 'block_cover front_image',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "sunder pop_out",
				imgsrc : imgpath + "sundar.png",
			}],
			imagelabels: [{
				imagelabelclass : "sunder_speech speech_zoom_in",
				imagelabeldata : data.string.p1text10,
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_bill'
			}]
		}]

	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var shirt_cost= 0;
	var pant_cost= 0;
	var shoe_cost= 0;

	var user_name = '';
	var user_address = '';

	loadTimelineProgress($total_page, countNext + 1);
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            // sounds
            {id: "sound_1", src: soundAsset + "s1_p1.ogg"},
            {id: "sound_2", src: soundAsset + "s1_p2.ogg"},
            {id: "sound_3", src: soundAsset + "s1_p3.ogg"},
            {id: "sound_3_1", src: soundAsset + "s1_p3_1.ogg"},
            {id: "sound_3_2", src: soundAsset + "s1_p3_2.ogg"},
            {id: "sound_3_3", src: soundAsset + "s1_p3_3.ogg"},
            {id: "sound_4", src: soundAsset + "s1_p4.ogg"},
            {id: "sound_5", src: soundAsset + "s1_p5.ogg"},
            {id: "sound_6", src: soundAsset + "s1_p6.ogg"},
            {id: "sound_7", src: soundAsset + "s1_p7.ogg"},
            {id: "sound_8", src: soundAsset + "s1_p8.ogg"},
            {id: "sound_9", src: soundAsset + "s1_p9.ogg"},
            {id: "sound_10", src: soundAsset + "s1_p10.ogg"},
            {id: "sound_11", src: soundAsset + "s1_p11.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
			(navigate && countNext<5)?$nextBtn.delay(500).show(0):(navigate && countNext==5)?ole.footerNotificationHandler.pageEndSetNotification():"";

        });
    }
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
		case 0:
		case 1:
              sound_player("sound_"+(countNext+1),true);
              break;
    case 2:
        sound_player("sound_"+(countNext+1),false);
        $('.tops').click(function(){
				$(this).addClass('float_to_1 selected_top');
				shirt_cost = parseInt($(this).find('label').text().split(" ")[1]);
				$('.tops').not(this).each(function(){
					$('.tops_text').fadeOut(1000);
			        $(this).fadeOut(1000, function() {
				    	$('.bottoms').fadeIn(1000);
				    	$('.bottoms_text').fadeIn(1000);
				    	$('.selected_top').removeClass('float_to_1');
				    	$('.selected_top').css({'top': '10%', 'left': '4%'});
				  	});
			        $('.tops').css('pointer-events', 'none');
			    });
				setTimeout(function(){
                    sound_player("sound_"+(countNext+1)+"_1",false);
                },1000);

                });
			$('.bottoms').click(function(){
				$(this).addClass('float_to_2 selected_bottom');
				pant_cost = parseInt($(this).find('label').text().split(" ")[1]);
				$('.bottoms').not(this).each(function(){
					$('.bottoms_text').fadeOut(1000);
			        $(this).fadeOut(1000, function() {
				    	$('.shoes').fadeIn(1000);
				    	$('.shoes_text').fadeIn(1000);
				    	$('.selected_bottom').removeClass('float_to_2');
				    	$('.selected_bottom').css({'top': '10%', 'left': '76%'});
				  	});
			        $('.bottoms').css('pointer-events', 'none');
			    });
                setTimeout(function(){
                    sound_player("sound_"+(countNext+1)+"_2",false);
                },1000);
			});
			$('.shoes').click(function(){
				$(this).addClass('float_to_3 selected_shoe');
				shoe_cost = parseInt($(this).find('label').text().split(" ")[1]);
				$('.shoes').not(this).each(function(){
					$('.shoes_text').fadeOut(1000);
			        $(this).fadeOut(1000, function() {
			        	$('.bought_today').fadeIn(1000);
			        	$('.bought_today').promise().done(function() {
			        		$('.selected_shoe').addClass('float_shoe');
			        		$('.selected_bottom').addClass('float_bottom');
			        		$('.selected_top').addClass('float_top');
			        		// $nextBtn.delay(1500).show(0);
						});
			        	$('.selected_shoe').removeClass('float_to_3');
				    	$('.selected_shoe').css({'top': '30%', 'left': '40%'});
				  	});
			        $('.shoes').css('pointer-events', 'none');
                    setTimeout(function(){
                        sound_player("sound_"+(countNext+1)+"_3",true);
                    },1000);
			    });
			});
			break;
		case 3:
            sound_player("sound_"+(countNext+1),false);
            input_box('.user_info_label', $nextBtn);
			break;
		case 4:
            sound_player("sound_"+(countNext+1),false);
            input_box('.user_info_label', $nextBtn);
			$prevBtn.show(0);
			break;
		case 5:
            setTimeout(function(){
                sound_player("sound_"+(countNext+1),true);
            },2000);
            var my_bill = new bill();
			items = [];
			my_bill.reset_bill_items();
			my_bill.init(imgpath + "shop_bill/logo_cloth.png", data.string.sshop2_name, data.string.sshop2_address, user_name, user_address, 'bill_container');
			var items = [new bill_element(data.string.item_1, 1, shirt_cost), new bill_element(data.string.item_2, 1, pant_cost), new bill_element(data.string.item_3, 1, shoe_cost)];
			my_bill.add_items(items);
			my_bill.update_bill();
			$('.instruction').delay(4000).hide(0);
			// $('.bill_table_header').after('<tr><th>SN</th><th>Particulars</th><th>Qty</th><th>Rate</th><th>Amt</th></tr>');
			break;
		default:
			$nextBtn.show(0);
			$prevBtn.show(0);
			break;
		}
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		switch(countNext){
			case 3:
				user_name = global_save_val;
				countNext++;
				templateCaller();
				break;
			case 4:
				user_address = global_save_val;
				countNext++;
				templateCaller();
				break;
			default:
				countNext++;
				templateCaller();
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();
	function input_box(input_class, button_class) {
		$(input_class).keydown(function(event){
    		var charCode = (event.which) ? event.which : event.keyCode;
    		/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
    		if(charCode === 13 && button_class!=null) {
		        $(button_class).trigger("click");
			}
            // var condition = charCode != 8 && charCode != 16 && charCode != 20 && (charCode < 37 || charCode > 40) && charCode != 46;
            // //check if user inputs del, shift, caps , backspace or arrow keys
   			// if (!condition) {
    		// 	return true;
    		// }
    		// //check if user inputs more than one '.'
            // if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
        		// return false;
    		// }
    		// //check . and 0-9 separately after checking arrow and other keys
    		// if((charCode > 90)){
    		// 	return false;
    		// }

  			return true;
		});
		$(input_class).keyup(function(event){
    		if (String(event.target.value).length >= 1) {
    			$(button_class).show(0);
    			global_save_val = String(event.target.value);
    		}
  			return true;
		});
	}
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
	//slide0
	{
		contentblockadditionalclass: 'main_bg',
		contentblocknocenteradjust : true,

		uppertextblockadditionalclass : "instruction my_font_medium",
		uppertextblock : [
		{
			textdata : data.string.p3text1,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		}],
		bill_item:[{
			bill_container: 'bill_center bill_1',
			shop_logo: imgpath + "shop_bill/logo_cloth.png",
			shop_name: data.string.sshop2_name,
			shop_name: data.string.sshop2_address,
			customer_name:'Mark',
			customer_address:'USA',
			customer_bill: '300',
			customer_date: '12/12',
			signature: imgpath + "shop_bill/sign.png",
			shopkeeper_name: data.string.sshop2_owner
		}],
	},
	//slide1
	{
		contentblockadditionalclass: 'main_bg',
		contentblocknocenteradjust : true,

		uppertextblockadditionalclass : "instruction my_font_medium",
		uppertextblock : [
		{
			textdata : data.string.p3text2,
			textclass : 'bill_t_1',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		},
		{
			textdata : data.string.p3text3,
			textclass : 'bill_t_2 text_highlight_2',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		}],

		bill_item:[{
			bill_container: 'bill_center move_left bill_1',
			shop_logo: imgpath + "shop_bill/logo_cloth.png",
			shop_name: data.string.sshop2_name,
			shop_name: data.string.sshop2_address,
			customer_name:'Mark',
			customer_address:'USA',
			customer_bill: '300',
			customer_date: '12/12',
			signature: imgpath + "shop_bill/sign.png",
			shopkeeper_name: data.string.sshop2_owner
		},
		{
			bill_container: 'bill_center slide_in bill_2',
			shop_logo: imgpath + "shop_bill/logo_cloth.png",
			shop_name: data.string.sshop2_name,
			shop_name: data.string.sshop2_address,
			customer_name:'Mark',
			customer_address:'USA',
			customer_bill: '300',
			customer_date: '12/12',
			signature: imgpath + "shop_bill/sign.png",
			shopkeeper_name: data.string.sshop2_owner
		}],
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		additionalclasscontentblock : '',
		uppertextblockadditionalclass : "diytitle",
		uppertextblock : [{
			textdata : data.string.diy,
			textclass : 'diy'
		}],
        imageblockadditionalclass: 'diyimg',
        imageblock : [{
            imagestoshow : [
                {
                    imgclass : "diyimage",
                    imgsrc : "images/diy_bg/a_27.png",
                }],
        }]
	},
	//slide3
	{
		contentblockadditionalclass: 'main_bg',
		contentblocknocenteradjust : true,

		uppertextblockadditionalclass : "questions_div my_font_medium",
		uppertextblock : [
		{
			textdata : data.string.p3text4,
			textclass : 'question',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		},
		{
			textdata : data.string.customer_name_3,
			textclass : 'answer correct_ans',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		},
		{
			textdata : data.string.ecustomer_name_1,
			textclass : 'answer',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		},
		{
			textdata : data.string.customer_address_3,
			textclass : 'answer',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		},
		{
			textdata : data.string.ecustomer_name_5,
			textclass : 'answer',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		}],
		bill_item:[{
			bill_container: 'bill_center bill_left',
			shop_logo: imgpath + "shop_bill/logo_cloth.png",
			shop_name: data.string.sshop1_name,
			shop_address: data.string.sshop1_address,
			customer_name:'Ramesh Paudel',
			customer_address:'Traffic Chowk',
			customer_bill: '318',
			customer_date: '12/31',
			signature: imgpath + "shop_bill/sign.png",
			shopkeeper_name: data.string.sshop1_owner
		}],
	},
	//slide4
	{
		contentblockadditionalclass: 'main_bg',
		contentblocknocenteradjust : true,

		uppertextblockadditionalclass : "questions_div my_font_medium",
		uppertextblock : [
		{
			textdata : data.string.p3text5,
			textclass : 'question',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		},
		{
			textdata : "80",
			textclass : 'answer',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		},
		{
			textdata : "360",
			textclass : 'answer',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		},
		{
			textdata : "100",
			textclass : 'answer',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		},
		{
			textdata : "550",
			textclass : 'answer correct_ans',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		}],
		bill_item:[{
			bill_container: 'bill_center bill_left',
			shop_logo: imgpath + "shop_bill/logo_cloth.png",
			shop_name: data.string.sshop1_name,
			shop_address: data.string.sshop1_address,
			customer_name:'Ramesh Paudel',
			customer_address:'Traffic Chowk',
			customer_bill: '318',
			customer_date: '12/31',
			signature: imgpath + "shop_bill/sign.png",
			shopkeeper_name: data.string.sshop1_owner
		}],
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var shirt_cost= 0;
	var pant_cost= 0;
	var shoe_cost= 0;

	var user_name = '';
	var user_address = '';
	var rand_bill_no = ole.getRandom(1, 9999, 1000)[0];

	loadTimelineProgress($total_page, countNext + 1);
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

						{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
						{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
            // sounds
            {id: "sound_1", src: soundAsset + "s3_p1.ogg"},
            {id: "sound_2", src: soundAsset + "s3_p2.ogg"},
            {id: "sound_3", src: soundAsset + "s3_p3.ogg"},
            {id: "sound_4", src: soundAsset + "s3_p4.ogg"},
            {id: "sound_5", src: soundAsset + "s3_p5.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            (navigate && countNext<11)?$nextBtn.delay(500).show(0):(navigate && countNext==11)?ole.footerNotificationHandler.pageEndSetNotification():"";

        });
    }
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
        switch (countNext) {
		case 0:
            sound_player("sound_"+(countNext+1),true);
            bill_init();
			break;
		case 1:
            sound_player("sound_"+(countNext+1),true);
            var my_bill = new bill();
			my_bill.reset_bill_items();
			my_bill.init(imgpath + "shop_bill/logo_cloth.png", data.string.sshop2_name, data.string.sshop2_address, data.string.customer_name_1, data.string.customer_address_1, 'bill_1');
			my_bill.set_bill_number(rand_bill_no);
			var items = [new bill_element(data.string.item_1, 1, 400), new bill_element(data.string.item_2, 1, 1400), new bill_element(data.string.item_3, 1, 1350)];
			my_bill.add_items(items);
			my_bill.update_bill();

			var my_bill_2 = new bill();
			my_bill_2.reset_bill_items();
			my_bill_2.init(imgpath + "shop_bill/logo_cloth.png", data.string.sshop2_name, data.string.sshop2_address, data.string.customer_name_2, data.string.customer_address_2, 'bill_2');
			var rand_bill_no2 = Math.floor(Math.random()*9999);
			my_bill_2.set_bill_number(rand_bill_no2);
			var items = [new bill_element(data.string.item_1, 3, 700), new bill_element(data.string.item_2, 2, 900), new bill_element(data.string.item_3, 4, 1650)];
			my_bill_2.add_items(items);
			my_bill_2.update_bill();

			$('.bill_t_1').delay(1500).fadeOut(1000,function(){
				$('.bill_t_2').fadeIn(1000,function(){
					$nextBtn.delay(1000).show(0);
				});
			});
			$prevBtn.show(0);
			break;
		case 3:
            sound_player("sound_"+(countNext+1),false);
            var my_bill = new bill();
			my_bill.reset_bill_items();
			my_bill.init(imgpath + "shop_bill/logo_station.png", data.string.sshop1_name, data.string.sshop1_address, data.string.customer_name_3, data.string.customer_address_3, 'bill_center');
			var items = [new bill_element(data.string.item_4, 2, 40), new bill_element(data.string.item_5, 1, 10), new bill_element(data.string.item_6, 12, 30), new bill_element(data.string.item_7, 4, 25)];
			my_bill.add_items(items);
			my_bill.update_bill();
			$prevBtn.show(0);

			$(".answer").append("<img class='corincor correct' src='"+preload.getResult("correct").src+"' />");
			$(".answer").append("<img class='corincor wrong' src='"+preload.getResult("incorrect").src+"' />");

			$('.answer').click(function(){
	        createjs.Sound.stop();
				$(this).addClass('selected');
				$(this).css('pointer-events', 'none');
				if($(this).hasClass('correct_ans')){
					$nextBtn.show(0);
					$('.answer').css('pointer-events', 'none');
					play_correct_incorrect_sound(1);
					$(this).children(".correct").show(0);
				} else{
					play_correct_incorrect_sound(0);
					$(this).children(".wrong").show(0);
				}
			});
			$prevBtn.show(0);
			break;
		case 4:
            sound_player("sound_"+(countNext+1),false);
            var my_bill = new bill();
			my_bill.reset_bill_items();
			my_bill.init(imgpath + "shop_bill/logo_station.png", data.string.sshop1_name, data.string.sshop1_address, data.string.customer_name_3, data.string.customer_address_3, 'bill_center');
			var items = [new bill_element(data.string.item_4, 2, 40), new bill_element(data.string.item_5, 1, 10), new bill_element(data.string.item_6, 12, 30), new bill_element(data.string.item_7, 4, 25)];
			my_bill.add_items(items);
			my_bill.update_bill();
			$prevBtn.show(0);
						$(".answer").append("<img class='corincor correct' src='"+preload.getResult("correct").src+"'/>");
						$(".answer").append("<img class='corincor wrong' src='"+preload.getResult("incorrect").src+"'/>");


			$('.answer').click(function(){
	        createjs.Sound.stop();
				$(this).addClass('selected');
				$(this).css('pointer-events', 'none');
				if($(this).hasClass('correct_ans')){
					ole.footerNotificationHandler.lessonEndSetNotification();
				$('.answer').css('pointer-events', 'none');
					play_correct_incorrect_sound(1);
					$(this).children(".correct").show(0);
				} else{
					play_correct_incorrect_sound(0);
					$(this).children(".wrong").show(0);
				}
			});
			$prevBtn.show(0);
			break;
		default:
			$nextBtn.show(0);
			$prevBtn.show(0);
			break;
		}
	}

	function bill_init(){
		var my_bill = new bill();
		my_bill.reset_bill_items();
		my_bill.init(imgpath + "shop_bill/logo_cloth.png", data.string.sshop2_name, data.string.sshop2_address, data.string.customer_name_1, data.string.customer_address_1, 'bill_container');
		my_bill.set_bill_number(rand_bill_no);
		var items = [new bill_element(data.string.item_1, 1, 400), new bill_element(data.string.item_2, 1, 1400), new bill_element(data.string.item_3, 1, 1350)];
		my_bill.add_items(items);
		my_bill.update_bill('.bill_center');
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

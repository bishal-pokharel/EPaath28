var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
	//slide0
	{
		contentblockadditionalclass: 'main_bg',
		contentblocknocenteradjust : true,
		
		uppertextblockadditionalclass : "instruction my_font_medium",
		uppertextblock : [
		{
			textdata : data.string.p2text1,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		}],
		bill_item:[{
			bill_container: 'bill_center',
			shop_logo: imgpath + "shop_bill/logo_cloth.png",
			shop_name: data.string.sshop2_name,
			shop_name: data.string.sshop2_name,
			customer_name:'Mark',
			customer_address:'USA',
			customer_bill: '300',
			customer_date: '12/12',
			signature: imgpath + "shop_bill/sign.png",
			shopkeeper_name: data.string.sshop2_owner
		}],
	},
	//slide1
	{
		contentblockadditionalclass: 'main_bg',
		contentblocknocenteradjust : true,
		
		bill_item:[{
			bill_container: 'bill_center',
			shop_logo: imgpath + "shop_bill/logo_cloth.png",
			shop_name: data.string.sshop2_name,
			shop_address: data.string.sshop2_address,
			customer_name:'Mark',
			customer_address:'USA',
			customer_bill: '300',
			customer_date: '12/12',
			signature: imgpath + "shop_bill/sign.png",
			shopkeeper_name: data.string.sshop2_owner
		}],
		lowertextblockadditionalclass : "bill_labels label_1 label_right my_font_medium",
		lowertextblock : [
		{
			textdata : data.string.billpart1,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		}],
	},
	//slide2
	{
		contentblockadditionalclass: 'main_bg',
		contentblocknocenteradjust : true,
		
		bill_item:[{
			bill_container: 'bill_center',
			shop_logo: imgpath + "shop_bill/logo_cloth.png",
			shop_name: data.string.sshop2_name,
			shop_address: data.string.sshop2_address,
			customer_name:'Mark',
			customer_address:'USA',
			customer_bill: '300',
			customer_date: '12/12',
			signature: imgpath + "shop_bill/sign.png",
			shopkeeper_name: data.string.sshop2_owner
		}],
		lowertextblockadditionalclass : "bill_labels label_top label_left my_font_medium",
		lowertextblock : [
		{
			textdata : data.string.billpart2,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		}],
	},
	//slide3
	{
		contentblockadditionalclass: 'main_bg',
		contentblocknocenteradjust : true,
		
		bill_item:[{
			bill_container: 'bill_center',
			shop_logo: imgpath + "shop_bill/logo_cloth.png",
			shop_name: data.string.sshop2_name,
			shop_address: data.string.sshop2_address,
			customer_name:'Mark',
			customer_address:'USA',
			customer_bill: '300',
			customer_date: '12/12',
			signature: imgpath + "shop_bill/sign.png",
			shopkeeper_name: data.string.sshop2_owner
		}],
		lowertextblockadditionalclass : "bill_labels label_top label_right my_font_medium",
		lowertextblock : [
		{
			textdata : data.string.billpart3,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		}],
	},
	//slide4
	{
		contentblockadditionalclass: 'main_bg',
		contentblocknocenteradjust : true,
		
		bill_item:[{
			bill_container: 'bill_center',
			shop_logo: imgpath + "shop_bill/logo_cloth.png",
			shop_name: data.string.sshop2_name,
			shop_address: data.string.sshop2_address,
			customer_name:'Mark',
			customer_address:'USA',
			customer_bill: '300',
			customer_date: '12/12',
			signature: imgpath + "shop_bill/sign.png",
			shopkeeper_name: data.string.sshop2_owner
		}],
		lowertextblockadditionalclass : "bill_labels label_top label_right my_font_medium",
		lowertextblock : [
		{
			textdata : data.string.billpart4,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		}],
	},
	//slide5
	{
		contentblockadditionalclass: 'main_bg',
		contentblocknocenteradjust : true,
		
		bill_item:[{
			bill_container: 'bill_center',
			shop_logo: imgpath + "shop_bill/logo_cloth.png",
			shop_name: data.string.sshop2_name,
			shop_address: data.string.sshop2_address,
			customer_name:'Mark',
			customer_address:'USA',
			customer_bill: '300',
			customer_date: '12/12',
			signature: imgpath + "shop_bill/sign.png",
			shopkeeper_name: data.string.sshop2_owner
		}],
		lowertextblockadditionalclass : "bill_labels label_middle label_left my_font_medium",
		lowertextblock : [
		{
			textdata : data.string.billpart5,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		}],
	},
	//slide6
	{
		contentblockadditionalclass: 'main_bg',
		contentblocknocenteradjust : true,
		
		bill_item:[{
			bill_container: 'bill_center',
			shop_logo: imgpath + "shop_bill/logo_cloth.png",
			shop_name: data.string.sshop2_name,
			shop_address: data.string.sshop2_address,
			customer_name:'Mark',
			customer_address:'USA',
			customer_bill: '300',
			customer_date: '12/12',
			signature: imgpath + "shop_bill/sign.png",
			shopkeeper_name: data.string.sshop2_owner
		}],
		lowertextblockadditionalclass : "bill_labels label_middle label_left my_font_medium",
		lowertextblock : [
		{
			textdata : data.string.billpart6,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		}],
	},
	//slide7
	{
		contentblockadditionalclass: 'main_bg',
		contentblocknocenteradjust : true,
		
		bill_item:[{
			bill_container: 'bill_center',
			shop_logo: imgpath + "shop_bill/logo_cloth.png",
			shop_name: data.string.sshop2_name,
			shop_address: data.string.sshop2_address,
			customer_name:'Mark',
			customer_address:'USA',
			customer_bill: '300',
			customer_date: '12/12',
			signature: imgpath + "shop_bill/sign.png",
			shopkeeper_name: data.string.sshop2_owner
		}],
		lowertextblockadditionalclass : "bill_labels label_middle label_left my_font_medium",
		lowertextblock : [
		{
			textdata : data.string.billpart7,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		}],
	},
	//slide8
	{
		contentblockadditionalclass: 'main_bg',
		contentblocknocenteradjust : true,
		
		bill_item:[{
			bill_container: 'bill_center',
			shop_logo: imgpath + "shop_bill/logo_cloth.png",
			shop_name: data.string.sshop2_name,
			shop_address: data.string.sshop2_address,
			customer_name:'Mark',
			customer_address:'USA',
			customer_bill: '300',
			customer_date: '12/12',
			signature: imgpath + "shop_bill/sign.png",
			shopkeeper_name: data.string.sshop2_owner
		}],
		lowertextblockadditionalclass : "bill_labels label_middle label_right my_font_medium",
		lowertextblock : [
		{
			textdata : data.string.billpart8,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		}],
	},
	//slide9
	{
		contentblockadditionalclass: 'main_bg',
		contentblocknocenteradjust : true,
		
		bill_item:[{
			bill_container: 'bill_center',
			shop_logo: imgpath + "shop_bill/logo_cloth.png",
			shop_name: data.string.sshop2_name,
			shop_address: data.string.sshop2_address,
			customer_name:'Mark',
			customer_address:'USA',
			customer_bill: '300',
			customer_date: '12/12',
			signature: imgpath + "shop_bill/sign.png",
			shopkeeper_name: data.string.sshop2_owner
		}],
		lowertextblockadditionalclass : "bill_labels label_middle label_right my_font_medium",
		lowertextblock : [
		{
			textdata : data.string.billpart9,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		},
		{
			textdata : data.string.billpart10,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		}],
	},
	//slide10
	{
		contentblockadditionalclass: 'main_bg',
		contentblocknocenteradjust : true,
		
		bill_item:[{
			bill_container: 'bill_center',
			shop_logo: imgpath + "shop_bill/logo_cloth.png",
			shop_name: data.string.sshop2_name,
			shop_address: data.string.sshop2_address,
			customer_name:'Mark',
			customer_address:'USA',
			customer_bill: '300',
			customer_date: '12/12',
			signature: imgpath + "shop_bill/sign.png",
			shopkeeper_name: data.string.sshop2_owner
		}],
		lowertextblockadditionalclass : "bill_labels label_bottom label_right my_font_medium",
		lowertextblock : [
		{
			textdata : data.string.billpart11,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		}],
	},
	//slide11
	{
		contentblockadditionalclass: 'main_bg',
		contentblocknocenteradjust : true,
		
		bill_item:[{
			bill_container: 'bill_center',
			shop_logo: imgpath + "shop_bill/logo_cloth.png",
			shop_name: data.string.sshop2_name,
			shop_address: data.string.sshop2_address,
			customer_name:'Mark',
			customer_address:'USA',
			customer_bill: '300',
			customer_date: '12/12',
			signature: imgpath + "shop_bill/sign.png",
			shopkeeper_name: data.string.sshop2_owner
		}],
		lowertextblockadditionalclass : "bill_labels label_bottom label_right my_font_medium",
		lowertextblock : [
		{
			textdata : data.string.billpart12,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		}],
	}
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
		
	var shirt_cost= 0;
	var pant_cost= 0;
	var shoe_cost= 0;
	
	var user_name = '';
	var user_address = '';
	var rand_bill_no = ole.getRandom(1, 9999, 1000)[0];
	
	loadTimelineProgress($total_page, countNext + 1);
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            // sounds
            {id: "sound_1", src: soundAsset + "s2_p1.ogg"},
            {id: "sound_2", src: soundAsset + "s2_p2.ogg"},
            {id: "sound_3", src: soundAsset + "s2_p3.ogg"},
            {id: "sound_4", src: soundAsset + "s2_p4.ogg"},
            {id: "sound_5", src: soundAsset + "s2_p5.ogg"},
            {id: "sound_6", src: soundAsset + "s2_p6.ogg"},
            {id: "sound_7", src: soundAsset + "s2_p7.ogg"},
            {id: "sound_8", src: soundAsset + "s2_p8.ogg"},
            {id: "sound_9", src: soundAsset + "s2_p9.ogg"},
            {id: "sound_10", src: soundAsset + "s2_p10.ogg"},
            {id: "sound_11", src: soundAsset + "s2_p11.ogg"},
            {id: "sound_12", src: soundAsset + "s2_p12.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            (navigate && countNext<11)?$nextBtn.delay(500).show(0):(navigate && countNext==11)?ole.footerNotificationHandler.pageEndSetNotification():"";

        });
    }
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
         sound_player("sound_"+(countNext+1),true);
        switch (countNext) {
		case 0:
            bill_init();
			break;
		case 1:
            bill_init();
			$('.bill_header').addClass('highlighted_div');
			break;
		case 2:
			bill_init();
			$('.customer_name_div').addClass('highlighted_div');
			$('.customer_name_div>p').css("border", "none");
			$('.customer_name_div>p>span').css("border", "none");
			$prevBtn.show(0);
			break;
		case 3:
			bill_init();
			$('.bill_number_div>p:first-child').addClass('highlighted_div');
			$('.bill_number_div>p:first-child').css("border", "2px solid #6183D1");
			$('.bill_number_div>p:first-child>span').css("border", "none");
			$prevBtn.show(0);
			break;
		case 4:
			bill_init();
			$('.bill_number_div>p:last-child').addClass('highlighted_div');
			$('.bill_number_div>p:last-child').css("border", "2px solid #6183D1");
			$('.bill_number_div>p:last-child>span').css("border", "none");
			$prevBtn.show(0);
			break;
		case 5:
			bill_init();
			$('tr:not(.total_amount_row)>th:first-child').addClass('highlighted_div');
			$('tr:not(.total_amount_row)>td:first-child').addClass('highlighted_div');
			$prevBtn.show(0);
			break;
		case 6:
			bill_init();
			$('tr:not(.total_amount_row)>th:nth-child(2)').addClass('highlighted_div');
			$('tr:not(.total_amount_row)>td:nth-child(2)').addClass('highlighted_div');
			$prevBtn.show(0);
			break;
		case 7:
			bill_init();
			$('tr:not(.total_amount_row)>th:nth-child(3)').addClass('highlighted_div');
			$('tr:not(.total_amount_row)>td:nth-child(3)').addClass('highlighted_div');
			$prevBtn.show(0);
			break;
		case 8:
			bill_init();
			$('tr:not(.total_amount_row)>th:nth-child(4)').addClass('highlighted_div');
			$('tr:not(.total_amount_row)>td:nth-child(4)').addClass('highlighted_div');
			$prevBtn.show(0);
			break;
		case 9:
			bill_init();
			$('tr:not(.total_amount_row)>th:nth-child(5)').addClass('highlighted_div');
			$('tr:not(.total_amount_row)>td:nth-child(5)').addClass('highlighted_div');
			$nextBtn.show(0);
			$prevBtn.show(0);
			break;
		case 10:
			bill_init();
			$('.total_amount_row>td:last-child').addClass('highlighted_div');
			$prevBtn.show(0);
			break;
		case 11:
            bill_init();
			$('.bill_footer').addClass('highlighted_div');
			$('.bill_footer').css("border", "2px solid #6183D1");
			$prevBtn.show(0);
			break;
		default:
			$nextBtn.show(0);
			$prevBtn.show(0);
			break;
		}
	}
	
	function bill_init(){
		var my_bill = new bill();
		items = [];
		my_bill.reset_bill_items();
		my_bill.init(imgpath + "shop_bill/logo_cloth.png", data.string.sshop2_name, data.string.sshop2_address, data.string.customer_name_1, data.string.customer_address_1, 'bill_container');
		my_bill.set_bill_number(rand_bill_no);
		var items = [new bill_element(data.string.item_1, 1, 400), new bill_element(data.string.item_2, 1, 1400), new bill_element(data.string.item_3, 1, 1350)];
		my_bill.add_items(items);
		my_bill.update_bill('.bill_center');
	}
	
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   
	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

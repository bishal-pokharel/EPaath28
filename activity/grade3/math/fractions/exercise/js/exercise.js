var imgpath = $ref+"/images/page1/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content centertext",
                textdata: data.string.q1,
            },
            {
                splitintofractionsflag: true,
                textdiv:"option opt1",
                textclass: "content1",
                textdata: data.string.p2frac2,
                ans:"correct"
            },
            {
                splitintofractionsflag: true,
                textdiv:"option opt2 ",
                textclass: "content1",
                textdata: data.string.p2frac1,
                ans:"correct"
            },
            {
                splitintofractionsflag: true,
                textdiv:"option opt3",
                textclass: "content1",
                textdata: data.string.p2frac1,
                ans:"correct"
            },
            {
                splitintofractionsflag: true,
                textdiv:"option opt4",
                textclass: "content1",
                textdata: data.string.p2frac1,
                borderhide:"borderhide"
            },
            {
                splitintofractionsflag: true,
                textdiv:"option opt5",
                textclass: "content1",
                textdata: data.string.p2frac1,
                borderhide:"borderhide"
            }
        ],
    },
    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content centertext",
                textdata: data.string.q2,
            },
            {
                splitintofractionsflag: true,
                textdiv:"option opt1",
                textclass: "content1",
                textdata: data.string.p2frac2,
                ans:"correct"
            },
            {
                splitintofractionsflag: true,
                textdiv:"option opt2 ",
                textclass: "content1",
                textdata: data.string.p2frac1,
                ans:"correct"
            },
            {
                splitintofractionsflag: true,
                textdiv:"option opt3",
                textclass: "content1",
                textdata: data.string.p2frac1,
                ans:"correct"
            },
            {
                splitintofractionsflag: true,
                textdiv:"option opt4",
                textclass: "content1",
                textdata: data.string.p2frac1,
                borderhide:"borderhide"
            },
            {
                splitintofractionsflag: true,
                textdiv:"option opt5",
                textclass: "content1",
                textdata: data.string.p2frac1,
                borderhide:"borderhide"
            },
        ],
    },
    //slide 2

    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content centertext",
                textdata: data.string.q3,
            },
            {
                textdiv:"option option1",
                textclass: "chapter centertext",
                textdata: data.string.two,
            },
            {
                textdiv:"option  option2",
                textclass: "chapter centertext",
                textdata: data.string.three,
                ans:"correct"
            },
            {
                textdiv:"emptydiv",
                textclass:"",
                textdata:""
            }
        ],
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content centertext",
                textdata: data.string.q4,
            },
            {
                textdiv:"option option1",
                textclass: "chapter centertext",
                textdata: data.string.two,
            },
            {
                textdiv:"option  option2",
                textclass: "chapter centertext",
                textdata: data.string.three,
                ans:"correct"
            },
            {
                textdiv:"emptydiv",
                textclass:"",
                textdata:""
            }
        ],
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content centertext",
                textdata: data.string.q5,
            },
            {
                textdiv:"option option1",
                textclass: "chapter centertext",
                textdata: data.string.two,
                ans:"correct"
            },
            {
                textdiv:"option  option2",
                textclass: "chapter centertext",
                textdata: data.string.three,
            },
            {
                textdiv:"emptydiv",
                textclass:"",
                textdata:""
            }
        ],
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content centertext",
                textdata: data.string.q6,
            },
            {
                textdiv:"option option1",
                textclass: "chapter centertext",
                textdata: data.string.two,
                ans:"correct"
            },
            {
                textdiv:"option  option2",
                textclass: "chapter centertext",
                textdata: data.string.three,
            },
            {
                textdiv:"emptydiv",
                textclass:"",
                textdata:""
            }
        ],
    },
    //slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content centertext",
                textdata: data.string.q7,
            },
            {
                textdiv:"submitbtn centeralign",
                textclass:"content1 centertext",
                textdata:data.string.submit
            },
            {
                splitintofractionsflag: true,
                textdiv:"frac1 ",
                textclass: "title",
                textdata: data.string.optionfarc3,
            },
            {
                splitintofractionsflag: true,
                textdiv:"frac2 ",
                textclass: "title",
                textdata: data.string.optionfarc3,
            },
            {
                textdiv:"div1 centeralign",
                textclass:"chapter centertext",
                textdata:data.string.greaterthan
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "downarrow commonarrow",
                    imgclass: "relativecls img1",
                    imgid: 'downarrowImg',
                    imgsrc: ""
                }
            ]
        }],
        svgblock:[
            {
                svgblock: 'circlesvg1',
                handclass:"relativecls centertext"
            },
            {
                svgblock: 'circlesvg2',
                handclass:"relativecls centertext"

            }
        ]
    },
    //slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content centertext",
                textdata: data.string.q8,
            },
            {
                textdiv:"submitbtn centeralign",
                textclass:"content1 centertext",
                textdata:data.string.submit
            },
            {
                splitintofractionsflag: true,
                textdiv:"frac1 ",
                textclass: "title",
                textdata: data.string.optionfarc4,
            },
            {
                splitintofractionsflag: true,
                textdiv:"frac2 ",
                textclass: "title",
                textdata: data.string.optionfarc4,
            },
            {
                textdiv:"div1 centeralign",
                textclass:"chapter centertext",
                textdata:data.string.greaterthan
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "downarrow commonarrow",
                    imgclass: "relativecls img1",
                    imgid: 'downarrowImg',
                    imgsrc: ""
                }
            ]
        }],
        svgblock:[
            {
                svgblock: 'circlesvg1',
                handclass:"relativecls centertext"
            },
            {
                svgblock: 'circlesvg2',
                handclass:"relativecls centertext"

            }
        ]
    },
    //slide 9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content centertext",
                textdata: data.string.q9,
            },
            {
                splitintofractionsflag: true,
                textdiv:"option o1",
                textclass: "content1",
                textdata: data.string.p2frac2,
                ans:"correct"
            },
            {
                splitintofractionsflag: true,
                textdiv:"option o2",
                textclass: "content1",
                textdata: data.string.p2frac2,
            },
            {
                splitintofractionsflag: true,
                textdiv:"option o3",
                textclass: "content1",
                textdata: data.string.p2frac2,
            },
            {
                splitintofractionsflag: true,
                textdiv:"text1 ",
                textclass: "title",
                textdata: data.string.p2frac2,
                ans:"correct",
            },
            {
                splitintofractionsflag: true,
                textdiv:"text2 ",
                textclass: "title",
                textdata: data.string.p2frac2,
                ans:"correct",
            },
            {
                splitintofractionsflag: true,
                textdiv:"text3 temptext",
                textclass: "title",
                textdata: data.string.p2frac2,
                ans:"correct",
            },
            {
                textdiv: "plus centeralign",
                textclass: "chapter",
                textdata: data.string.plus,
            },
            {
                textdiv: "equalto centeralign",
                textclass: "chapter",
                textdata: data.string.equalto,
            }
        ],
    },
    //slide 10
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content centertext",
                textdata: data.string.q10,
            },
            {
                splitintofractionsflag: true,
                textdiv:"option o1",
                textclass: "content1",
                textdata: data.string.p2frac2,
                ans:"correct"
            },
            {
                splitintofractionsflag: true,
                textdiv:"option o2",
                textclass: "content1",
                textdata: data.string.p2frac2,
            },
            {
                splitintofractionsflag: true,
                textdiv:"option o3",
                textclass: "content1",
                textdata: data.string.p2frac2,
            },
            {
                splitintofractionsflag: true,
                textdiv:"text1 ",
                textclass: "title",
                textdata: data.string.p2frac2,
                ans:"correct",
            },
            {
                splitintofractionsflag: true,
                textdiv:"text2 ",
                textclass: "title",
                textdata: data.string.p2frac2,
                ans:"correct",
            },
            {
                splitintofractionsflag: true,
                textdiv:"text3 temptext",
                textclass: "title",
                textdata: data.string.p2frac2,
                ans:"correct",
            },
            {
                textdiv: "plus centeralign",
                textclass: "chapter",
                textdata: data.string.plus,
            },
            {
                textdiv: "equalto centeralign",
                textclass: "chapter",
                textdata: data.string.equalto,
            }
        ],
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var time;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var scoreupdate = 0;
    var score = new NumberTemplate();
    score.init($total_page);


    var numeratorarray = [1,2,3,4,5,6,7,8,9,10]
    var denominatorarray = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]



    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "fivemainImg", src: imgpath + "Graph01.svg", type: createjs.AbstractLoader.IMAGE},
            {id: "tenmainImg", src: imgpath + "Graph02.svg", type: createjs.AbstractLoader.IMAGE},
            {id: "downarrowImg", src: imgpath + "down.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            // {id: "sound_0", src: soundAsset + "p1_s0.ogg"},
            // {id: "sound_1", src: soundAsset + "p1_s1.ogg"},
            // {id: "sound_2", src: soundAsset + "p1_s2.ogg"},
            // {id: "sound_3", src: soundAsset + "p1_s3.ogg"},
            // {id: "sound_4", src: soundAsset + "p1_s4.ogg"},
            // {id: "sound_5", src: soundAsset + "p1_s5.ogg"},
            // {id: "sound_6", src: soundAsset + "p1_s6.ogg"},
            // {id: "sound_7", src: soundAsset + "p1_s7.ogg"},
            // {id: "sound_8", src: soundAsset + "p1_s8.ogg"},
            // {id: "sound_9", src: soundAsset + "p1_s9.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    // Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    // Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    // Handlebars.registerPartial("fractioncontent", $("#fractioncontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        splitintofractions($board);
        put_image(content, countNext, preload);
        score.numberOfQuestions();
        enterbtn();
        switch (countNext) {
            case 0:
            case 1:
                 numeratorarray = [1,2,3,4,5,6,7,8,9,10]
                 denominatorarray = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]
                $(".option").each(function(index){
                 loadnumdeno($(this).find(".top"),$(this).find(".bottom"));
                });
                var optionclass = ["option opt1","option opt2","option opt3","option opt4","option opt5"]
                shufflehint(optionclass,"option");
                checkans("correctwrongimg");
                break;
            case 2:
            case 3:
            case 4:
            case 5:
                numeratorarray = [1,2,3,4,5,6,7,8,9,10]
                denominatorarray = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]
                loadnumdeno($(".option1 p"),$(".option2 p"));
                checkans1("correctwrongimg1");
                break;
            case 6:
                count = 0;
                $(".bottom").css("border-top","0.1em solid black");
                var array = [1,2,3,4,5];
                var frac1top = parseInt(randomnum(array));
                 array = [1,2,3,4,5];
                var frac2top = parseInt(randomnum(array));
                $(".frac1 .top").text(frac1top);
                $(".frac2 .top").text(frac2top);
                loadsvg("fivemainImg",frac1top,frac2top);
                checkans4(frac1top,frac2top);
                break;
            case 7:
                count = 0;
                $(".bottom").css("border-top","0.1em solid black");
                var array = [1,2,3,4,5,6,7,8,9,10];
                var frac1top = parseInt(randomnum(array));
                array = [1,2,3,4,5,6,7,8,9,10];
                var frac2top = parseInt(randomnum(array));
                $(".frac1 .top").text(frac1top);
                $(".frac2 .top").text(frac2top);
                loadsvg("tenmainImg",frac1top,frac2top);
                checkans4(frac1top,frac2top);
                break;
            case 8:
            case 9:
                $(".text1 .bottom,.text2 .bottom").css("border-top","0.1em solid black")
                numeratorarray = [1,2,3,4,5,6,7,8,9,10]
                denominatorarray = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]
                $(".option").each(function(index){
                    loadnumdeno($(this).find(".top"),$(this).find(".bottom"));
                });

                numeratorarray = [1,2,3,4,5,6,7,8,9,10]
                denominatorarray = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]
                loadnumdeno($(".text1 .top"),$(".text1 .bottom"));
                $(".text2 .bottom,.o1 .bottom,.text3 .bottom").text($(".text1 .bottom").text());
                $(".o1 .top,.text3 .top").text(parseInt($(".text1 .top").text()) + parseInt($(".text2 .top").text()));
                var optionclass = ["option o1","option o2","option o3"]
                shufflehint(optionclass,"option");
                checkans1("correctwrongimg1");
                break;
            default:
                navigationcontroller();
                break;
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                score.gotoNext();
                templateCaller();
                break;
        }
    });
    //
    // $refreshBtn.click(function(){
    //     templateCaller();
    // });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }


    function checkans(correctwrongimg1){
        count = 0;
        scoreupdate = 0;
        $(".option").on("click",function () {
            createjs.Sound.stop();
            if($(this).attr("data-answer").toString().trim()=='correct') {
                $(this).addClass("correctans");
                $(this).append("<img class='"+correctwrongimg1+"' src='images/right.png'/>");
                play_correct_incorrect_sound(1);
                count++;
                scoreupdate++;
                console.log("count val"+count);
                if(count == 3){
                    navigationcontroller();
                    $(".option").addClass("avoid-clicks");
                }
                if(scoreupdate == 3){
                    score.update(true);
                }
            }
            else{
                scoreupdate = 0;
                $(this).addClass("wrongans avoid-clicks");
                $(this).append("<img class='"+correctwrongimg1+"' src='images/wrong.png'/>");
                play_correct_incorrect_sound(0);
                score.update(false);
            }

        });
    }

    function checkans1(correctwrongimg1){
        scoreupdate = 1;
        $(".option").on("click",function () {
            createjs.Sound.stop();
            if($(this).attr("data-answer").toString().trim()=='correct') {
                $(this).addClass("correctans avoid-clicks");
                $(this).append("<img class='"+correctwrongimg1+"' src='images/right.png'/>");
                play_correct_incorrect_sound(1);
                 navigationcontroller();
                 $(".option").addClass("avoid-clicks");
                scoreupdate==1?score.update(true):'';
                $(".text3").removeClass("temptext");
                $(".text3 .bottom").css("border-top","0.1em solid black")
            }
            else{
                scoreupdate = 0;
                $(this).addClass("wrongans avoid-clicks");
                $(this).append("<img class='"+correctwrongimg1+"' src='images/wrong.png'/>");
                play_correct_incorrect_sound(0);
                score.update(false);
            }


        });
    }
    function optionselect(array,optionselectelem){
        optionselectelem.parent().removeClass("wrongans");
        array.splice(array.indexOf(optionselectelem.text()), 1);
        var random = array[Math.floor(Math.random() * array.length)];
        optionselectelem.text(random);
        count++;
        if (count > 1) {
            count = 0;
            return [data.string.greaterthan, data.string.lessthan, data.string.equalto];
        }
        else
            return array;
    }


    function splitintofractions($splitinside){
        typeof $splitinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
        if($splitintofractions.length > 0){
            $.each($splitintofractions, function(index, value){
                $this = $(this);
                var tobesplitfraction = $this.html();
                if($this.hasClass('fraction')){
                    tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
                    tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
                }else{
                    tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
                    tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
                }


                tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
                $this.html(tobesplitfraction);
            });
        }
    }
    function shufflehint(optionclass,option) {
        var optdiv = $(".coverboardfull");

        for (var i = optdiv.find("."+option).length; i >= 0; i--) {
            optdiv.append(optdiv.find("."+option).eq(Math.random() * i | 0));
        }
        optdiv.find("."+option).removeClass().addClass("current");
        optdiv.find(".current").each(function (index) {
            $(this).addClass(optionclass[index]);
            $(this).addClass($(this).attr("data-answer"));
        });
    }

    function imageload(){
        loadstackofpear(totalpieces);
        for(var i =0;i<sureygot;i++)
            $(".peardiv1").append("<img class='pearImg' src= '"+ preload.getResult("pear").src +"'>");
        for(var i =0;i<ririgot;i++)
            $(".peardiv2").append("<img class='pearImg' src= '"+ preload.getResult("pear").src +"'>");

    }

    function popualteoption(correctans,classoption){

        var items = [];
        for(var i = 1;i<totalpieces;i++){
            items.push(i);
        }
        items.splice(items.indexOf(ririgot), 1);
        items.splice(items.indexOf(sureygot), 1);
        items.splice(items.indexOf(correctans), 1);
        var random = items[Math.floor(Math.random()*items.length)]
        $(".opt1,.option1").find(".top").text(correctans!=totalpieces?totalpieces:chankheygot);
        $(".opt1,.option1").find(".bottom").text(totalpieces);
        $(".opt2,.option2").find(".top").text(correctans);
        $(".opt2,.option2").find(".bottom").text(totalpieces);
        $(".opt3,.option3").find(".top").text(ririgot!=correctans?ririgot:sureygot);
        $(".opt3,.option3").find(".bottom").text(totalpieces);
        $(".opt4,.option4").find(".top").text(random);
        $(".opt4,.option4").find(".bottom").text(totalpieces);
        shufflehint(classoption,"option");
        checkans("correctwrongimg");
    }
    function generateRandom(min, max, donotmatchoption) {
        var num = Math.floor(Math.random() * (max - min + 1)) + min;
        return (num == donotmatchoption) ? generateRandom(min, max) : num;
    }
    function generateRirishare(totalpieces,sureygot){
        count++;
        var tempriri =  generateRandom(totalpieces-2,1,sureygot);
        return ((totalpieces -sureygot -tempriri)>=1)?tempriri:(count<10?generateRirishare(totalpieces,sureygot):1);
    }




    function loadpear(elem,noOfpieces){
        for(var i =0;i<noOfpieces;i++)
            elem.append("<img class='pearImg' src= '"+ preload.getResult("pear").src +"'>");

    }
    function popopt(classoption){
        var srtotal = sureygot+ririgot;
        $(".o1").find(".share").first().find(".top").text(totalpieces);
        $(".o1").find(".share").first().find(".bottom").text(totalpieces);
        $(".o1").find(".share").last().find(".top").text(sureygot+ririgot);
        $(".o1").find(".share").last().find(".bottom").text(totalpieces);
        $(".o2").find(".share").first().find(".top").text(totalpieces);
        $(".o2").find(".share").first().find(".bottom").text(totalpieces);
        $(".o2").find(".share").last().find(".top").text(sureygot+ririgot);
        $(".o2").find(".share").last().find(".bottom").text(totalpieces);
        shufflehint(classoption,"option");
        checkans("correctwrongimg");
    }



        function navigationcontroller(islastpageflag) {
            if (countNext == 0 && $total_page != 1) {
                $nextBtn.show(0);
            }
            else if ($total_page == 1) {
                $nextBtn.css('display', 'none');

                ole.footerNotificationHandler.lessonEndSetNotification();
            }
            else if (countNext > 0 && countNext < $total_page) {

                $nextBtn.show(0);
            }
            else if (countNext == $total_page - 2) {

                $nextBtn.css('display', 'none');
                // if lastpageflag is true
                // ole.footerNotificationHandler.pageEndSetNotification();
            }

        }

    function put_image() {
        var contentCount=content[countNext];
        var imageblockcontent=contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent,contentCount,preload);
        imageblockcontent=contentCount.hasOwnProperty('imageblock1');
        contentCount = imageblockcontent?contentCount.imageblock1[0]:false;
        imageblockcontent?dynamicimageload(imageblockcontent,contentCount):'';
    }

    function dynamicimageload(imageblockcontent,contentCount){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }


    function randomnum(items){
        var random = items[Math.floor(Math.random()*items.length)]
        items.splice(items.indexOf(random), 1);
        return random;
    }
    function randomdeno(items,numerator,deno){
        count++;
        var denominator = items[Math.floor(Math.random()*items.length)]
        items.splice(items.indexOf(denominator), 1);
        if(denominator>numerator) {
            console.log("denominator if"+denominator);
            deno.text(denominator);
            return denominator
        }
        else{
            items.splice(items.indexOf(denominator), 1);
            if(count<20){randomdeno(items,numerator,deno)} else{ return 30;};
        }
    }

    function loadnumdeno(num,deno){
        var numerator = randomnum(numeratorarray);
        console.log("numerator value00"+numerator);
        numeratorarray.splice(numeratorarray.indexOf(numerator), 1);
        num.text(numerator);
        count = 0;
        var denominator = randomdeno(denominatorarray,numerator,deno);

    }

    function loadsvg(imgid,frac1top,frac2top){
        // var s = Snap('#circlesvg');
        // var svg = Snap.load(preload.getResult(imgid).src, function ( loadedFragment ) {
        //     s.append(loadedFragment);
        // });
        var s1 = Snap('#circlesvg1');
        var svg = Snap.load(preload.getResult(imgid).src, function ( loadedFragment ) {
            s1.append(loadedFragment);
            var parts = $("#circlesvg1").find('.part')
            parts.css("opacity","1");
            parts.slice(frac1top).css("opacity","0");
        });
        var s2 = Snap('#circlesvg2');
        var svg = Snap.load(preload.getResult(imgid).src, function ( loadedFragment ) {
            s2.append(loadedFragment);
            var parts = $("#circlesvg2").find('.part')
            parts.css("opacity","1");
            parts.slice(frac2top).css("opacity","0");
        });

    }

    function checkans4(frac1top,frac2top){
        $(".div1 p").text("");
        var array1 = [data.string.greaterthan, data.string.lessthan, data.string.equalto];
        $(".downarrow").click(function(){
            array1 = optionselect(array1,$(".div1 p"));
        });
        scoreupdate = 1;
        $(".submitbtn").click(function(){
            if((frac1top > frac2top && $(".div1 p").text() == data.string.greaterthan)||(frac1top < frac2top && $(".div1 p").text() == data.string.lessthan) ||(frac1top == frac2top && $(".div1 p").text() == data.string.equalto)){
                $(".div1").addClass("correctans");
                $(".downarrow").addClass("avoid-clicks");
                play_correct_incorrect_sound(1);
                scoreupdate==1?score.update(true):"";
                navigationcontroller();
                $(this).addClass("avoid-clicks");
            }
            else {
                scoreupdate = 0;
                $(".div1").addClass("wrongans");
                play_correct_incorrect_sound(0);
                score.update(false);
            }
        });
    }

    function enterbtn() {
        $(document).keypress(function(e) {
            if(e.which == 13) {
                $('.submitbtn').click();
            }
        });
    }
});

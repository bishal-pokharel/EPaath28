var imgpath = $ref+"/images/page1/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content centertext",
                textdata: data.string.ex1,
            },
            {
                splitintofractionsflag: true,
                textdiv:"f1",
                textclass: "chapter",
                textdata: data.string.p2frac2,
            },
            {
                splitintofractionsflag: true,
                textdiv:"f2",
                textclass: "chapter",
                textdata: data.string.p2frac2,
            },
            {
                textdiv: "plus centeralign",
                textclass: "chapter",
                textdata: data.string.plus,
            },
            {
                textdiv: "equalto centeralign",
                textclass: "chapter",
                textdata: data.string.equalto,
            },
            {
                textdiv: "emptydiv",
                textclass: "",
                textdata: "",
            },
            {
                textdiv:"submitbtn centeralign",
                textclass:"content1 centertext",
                textdata:data.string.submit
            },
            {
                textdiv: "emptydiv1",
                textclass: "",
                textdata: "",
            },
            {
                textdiv: "emptydiv2",
                textclass: "",
                textdata: "",
            }
        ],
        inputblock:[
            {
                inputclass: "title  texttop1 centertext",
                inputdata: data.string.p2frac1,
                maxlength:2
            },
            {
                inputclass: "title  texttop2 centertext",
                inputdata: data.string.p2frac1,
                maxlength:2

            }
        ]
    },
    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content centertext",
                textdata: data.string.ex2,
            },
            {
                splitintofractionsflag: true,
                textdiv:"f1",
                textclass: "chapter",
                textdata: data.string.p2frac2,
            },
            {
                splitintofractionsflag: true,
                textdiv:"f2",
                textclass: "chapter",
                textdata: data.string.p2frac2,
            },
            {
                textdiv: "plus centeralign",
                textclass: "chapter",
                textdata: data.string.minus,
            },
            {
                textdiv: "equalto centeralign",
                textclass: "chapter",
                textdata: data.string.equalto,
            },
            {
                textdiv: "emptydiv",
                textclass: "",
                textdata: "",
            },
            {
                textdiv:"submitbtn centeralign",
                textclass:"content1 centertext",
                textdata:data.string.submit
            },
            {
                textdiv: "emptydiv1",
                textclass: "",
                textdata: "",
            },
            {
                textdiv: "emptydiv2",
                textclass: "",
                textdata: "",
            }
        ],
        inputblock:[
            {
                inputclass: "title  texttop1 centertext",
                inputdata: data.string.p2frac1,
                maxlength:2
            },
            {
                inputclass: "title  texttop2 centertext",
                inputdata: data.string.p2frac1,
                maxlength:2

            }
        ]
    },
    //slide 2

    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content centertext",
                textdata: data.string.ex3,
            },
            {
                textdiv:"submitbtn centeralign",
                textclass:"content1 centertext",
                textdata:data.string.submit
            },
            {
                splitintofractionsflag: true,
                textdiv:"exfrac1 ",
                textclass: "title",
                textdata: data.string.optionfarc3,
            },
            {
                splitintofractionsflag: true,
                textdiv:"exfrac2 ",
                textclass: "title",
                textdata: data.string.optionfarc3,
            },
            {
                textdiv: "e1",
                textclass: "",
                textdata: "",
            },
            {
                textdiv: "e2",
                textclass: "",
                textdata: "",
            },
            {
                textdiv: "sign1 centeralign",
                textclass: "chapter",
                textdata: data.string.minus,
            },
            {
                textdiv: "sign2 centeralign",
                textclass: "chapter",
                textdata: data.string.equalto,
            },
            {
                textdiv: "emptydiv",
                textclass: "",
                textdata: "",
            },
        ],
        inputblock:[
            {
                inputclass: "title  input1 centertext",
                inputdata: data.string.p2frac1,
                maxlength:2
            },
            {
                inputclass: "title  input2 centertext",
                inputdata: data.string.p2frac1,
                maxlength:2

            }
        ],
        svgblock:[
            // {
            //     svgblock: 'circlesvg',
            //     handclass:"relativecls centertext"
            // },
            {
                svgblock: 'circlesvg1',
                handclass:"relativecls centertext"
            },
            {
                svgblock: 'circlesvg2',
                handclass:"relativecls centertext"

            }
        ]
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content centertext",
                textdata: data.string.ex4,
            },
            {
                textdiv:"submitbtn centeralign",
                textclass:"content1 centertext",
                textdata:data.string.submit
            },
            {
                splitintofractionsflag: true,
                textdiv:"exfrac1 ",
                textclass: "title",
                textdata: data.string.optionfarc3,
            },
            {
                splitintofractionsflag: true,
                textdiv:"exfrac2 ",
                textclass: "title",
                textdata: data.string.optionfarc3,
            },
            {
                textdiv: "e1",
                textclass: "",
                textdata: "",
            },
            {
                textdiv: "e2",
                textclass: "",
                textdata: "",
            },
            {
                textdiv: "sign1 centeralign",
                textclass: "chapter",
                textdata: data.string.minus,
            },
            {
                textdiv: "sign2 centeralign",
                textclass: "chapter",
                textdata: data.string.equalto,
            },
            {
                textdiv: "emptydiv",
                textclass: "",
                textdata: "",
            },
        ],
        inputblock:[
            {
                inputclass: "title  input3 centertext",
                inputdata: data.string.p2frac1,
                maxlength:2
            },
            {
                inputclass: "title  input4 centertext",
                inputdata: data.string.p2frac1,
                maxlength:2

            }
        ],
        svgblock:[
            // {
            //     svgblock: 'circlesvg',
            //     handclass:"relativecls centertext"
            // },
            {
                svgblock: 'circlesvg1',
                handclass:"relativecls centertext"
            },
            {
                svgblock: 'circlesvg2',
                handclass:"relativecls centertext"

            }
        ]
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content centertext",
                textdata: data.string.ex5,
            },
            {
                textdiv:"submitbtn centeralign",
                textclass:"content1 centertext",
                textdata:data.string.submit
            },
            {
                splitintofractionsflag: true,
                textdiv:"f1 ",
                textclass: "chapter",
                textdata: data.string.optionfarc3,
            },
            {
                splitintofractionsflag: true,
                textdiv:"f22",
                textclass: "chapter",
                textdata: data.string.optionfarc3,
            },
            {
                textdiv:"div11 centeralign",
                textclass:"chapter centertext",
                textdata:data.string.greaterthan
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "downarrow1 commonarrow",
                    imgclass: "relativecls img1",
                    imgid: 'downarrowImg',
                    imgsrc: ""
                }
            ]
        }],
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content centertext",
                textdata: data.string.ex6,
            },
            {
                textdiv:"submitbtn centeralign",
                textclass:"content1 centertext",
                textdata:data.string.submit
            },
            {
                splitintofractionsflag: true,
                textdiv:"f1 ",
                textclass: "chapter",
                textdata: data.string.optionfarc3,
            },
            {
                splitintofractionsflag: true,
                textdiv:"f22",
                textclass: "chapter",
                textdata: data.string.optionfarc3,
            },
            {
                textdiv:"div11 centeralign",
                textclass:"chapter centertext",
                textdata:data.string.greaterthan
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "downarrow1 commonarrow",
                    imgclass: "relativecls img1",
                    imgid: 'downarrowImg',
                    imgsrc: ""
                }
            ]
        }],
    },
    //slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content centertext",
                textdata: data.string.ex7,
            },
            {
                textdiv:"submitbtn centeralign",
                textclass:"content1 centertext",
                textdata:data.string.submit
            },
            {
                splitintofractionsflag: true,
                textdiv:"f1 ",
                textclass: "chapter",
                textdata: data.string.optionfarc3,
            },
            {
                splitintofractionsflag: true,
                textdiv:"f22",
                textclass: "chapter",
                textdata: data.string.optionfarc3,
            },
            {
                splitintofractionsflag: true,
                textdiv:"f23",
                textclass: "chapter",
                textdata: data.string.optionfarc3,
            },
            {
                textdiv:"div11 centeralign",
                textclass:"chapter centertext",
                textdata:data.string.greaterthan
            },
            {
                textdiv:"div12 centeralign",
                textclass:"chapter centertext",
                textdata:data.string.greaterthan
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "downarrow1 commonarrow",
                    imgclass: "relativecls img1",
                    imgid: 'downarrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "downarrow2 commonarrow",
                    imgclass: "relativecls img1",
                    imgid: 'downarrowImg',
                    imgsrc: ""
                }
            ]
        }],
    },
    //slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content centertext",
                textdata: data.string.ex8,
            },
            {
                textdiv:"submitbtn centeralign",
                textclass:"content1 centertext",
                textdata:data.string.submit
            },
            {
                splitintofractionsflag: true,
                textdiv:"f1 ",
                textclass: "chapter",
                textdata: data.string.optionfarc3,
            },
            {
                splitintofractionsflag: true,
                textdiv:"f22",
                textclass: "chapter",
                textdata: data.string.optionfarc3,
            },
            {
                splitintofractionsflag: true,
                textdiv:"f23",
                textclass: "chapter",
                textdata: data.string.optionfarc3,
            },
            {
                textdiv:"div11 centeralign",
                textclass:"chapter centertext",
                textdata:data.string.greaterthan
            },
            {
                textdiv:"div12 centeralign",
                textclass:"chapter centertext",
                textdata:data.string.greaterthan
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "downarrow1 commonarrow",
                    imgclass: "relativecls img1",
                    imgid: 'downarrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "downarrow2 commonarrow",
                    imgclass: "relativecls img1",
                    imgid: 'downarrowImg',
                    imgsrc: ""
                }
            ]
        }],
    },
    //slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content centertext",
                textdata: data.string.ex9,
            },
            {
                splitintofractionsflag: true,
                textdiv:"draggable opt1 ",
                textclass: "title",
                textdata: data.string.optionfarc3,
                ans:"o1"
            },
            {
                splitintofractionsflag: true,
                textdiv:"draggable opt2",
                textclass: "title",
                textdata: data.string.optionfarc3,
                ans:"o2"
            },
            {
                splitintofractionsflag: true,
                textdiv:"draggable opt3",
                textclass: "title",
                textdata: data.string.optionfarc3,
                ans:"o3"
            },
            {
                splitintofractionsflag: true,
                textdiv:"droppable o1 ",
                textclass: "title",
                textdata: data.string.optionfarc3,
                ans:"o1"
            },
            {
                splitintofractionsflag: true,
                textdiv:"droppable o2",
                textclass: "title",
                textdata: data.string.optionfarc3,
                ans:"o2"
            },
            {
                splitintofractionsflag: true,
                textdiv:"droppable o3",
                textclass: "title",
                textdata: data.string.optionfarc3,
                ans:"o3"
            },
            {
                textdiv:"lessthan1 centeralign",
                textclass:"chapter centertext",
                textdata:data.string.lessthan
            },
            {
                textdiv:"lessthan2 centeralign",
                textclass:"chapter centertext",
                textdata:data.string.lessthan
            },
        ],
    },
    //slide 9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content centertext",
                textdata: data.string.ex10,
            },
            {
                splitintofractionsflag: true,
                textdiv:"draggable opt1 ",
                textclass: "title",
                textdata: data.string.optionfarc3,
                ans:"o1"
            },
            {
                splitintofractionsflag: true,
                textdiv:"draggable opt2",
                textclass: "title",
                textdata: data.string.optionfarc3,
                ans:"o2"
            },
            {
                splitintofractionsflag: true,
                textdiv:"draggable opt3",
                textclass: "title",
                textdata: data.string.optionfarc3,
                ans:"o3"
            },
            {
                splitintofractionsflag: true,
                textdiv:"droppable o1 ",
                textclass: "title",
                textdata: data.string.optionfarc3,
                ans:"o1"
            },
            {
                splitintofractionsflag: true,
                textdiv:"droppable o2",
                textclass: "title",
                textdata: data.string.optionfarc3,
                ans:"o2"
            },
            {
                splitintofractionsflag: true,
                textdiv:"droppable o3",
                textclass: "title",
                textdata: data.string.optionfarc3,
                ans:"o3"
            },
            {
                textdiv:"lessthan1 centeralign",
                textclass:"chapter centertext",
                textdata:data.string.greaterthan
            },
            {
                textdiv:"lessthan2 centeralign",
                textclass:"chapter centertext",
                textdata:data.string.greaterthan
            },
        ],
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var time;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var scoreupdate = 0;
    var score = new NumberTemplate();
    score.init($total_page);


    var numeratorarray = [2,3,4,5,6,7,8,9,10]
    var denominatorarray = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]



    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "fivemainImg", src: imgpath + "Graph01.svg", type: createjs.AbstractLoader.IMAGE},
            {id: "tenmainImg", src: imgpath + "Graph02.svg", type: createjs.AbstractLoader.IMAGE},
            {id: "downarrowImg", src: imgpath + "down.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            // {id: "sound_0", src: soundAsset + "p1_s0.ogg"},
            // {id: "sound_1", src: soundAsset + "p1_s1.ogg"},
            // {id: "sound_2", src: soundAsset + "p1_s2.ogg"},
            // {id: "sound_3", src: soundAsset + "p1_s3.ogg"},
            // {id: "sound_4", src: soundAsset + "p1_s4.ogg"},
            // {id: "sound_5", src: soundAsset + "p1_s5.ogg"},
            // {id: "sound_6", src: soundAsset + "p1_s6.ogg"},
            // {id: "sound_7", src: soundAsset + "p1_s7.ogg"},
            // {id: "sound_8", src: soundAsset + "p1_s8.ogg"},
            // {id: "sound_9", src: soundAsset + "p1_s9.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    // Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    // Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    // Handlebars.registerPartial("fractioncontent", $("#fractioncontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        splitintofractions($board);
        put_image(content, countNext, preload);
        score.numberOfQuestions();
        enterbtn();
        switch (countNext) {
            case 0:
                scoreupdate = 0;
                count = 0;
                scoreupdate = 1;
                $(".bottom").css("border-top","0.1em solid black");
                numeratorarray = [2,3,4,5,6,7,8,9,10]
                denominatorarray = [2,3,4,5,6,7,8,9,10]
                loadnumdeno($(".f1").find(".top"),$(".f1").find(".bottom"));
                var denominatorfinal = parseInt($(".f1 .bottom").text());
                var f1num = parseInt($(".f1 .top").text());
                f1num =f1num<denominatorfinal?f1num:f1num-1;
                $(".f1 .top").text(f1num);
                $(".f2 .bottom").text(denominatorfinal);
                count = 0;
                randomdeno1([2,3,4,5,6,7,8,9,10],f1num,denominatorfinal,$(".f2 .top"));
                var f2num = parseInt($(".f2 .top").text());
                var numeratorfinal = f1num + f2num;
                count = 0;
                checkinput(numeratorfinal,denominatorfinal,$(".texttop1"),$(".texttop2"));
                break;
            case 1:
                scoreupdate = 0;
                count = 0;
                scoreupdate = 1;
                $(".bottom").css("border-top","0.1em solid black");
                numeratorarray = [2,3,4,5,6,7,8,9,10]
                denominatorarray = [2,3,4,5,6,7,8,9,10]
                loadnumdeno($(".f1").find(".top"),$(".f1").find(".bottom"));
                var denominatorfinal = parseInt($(".f1 .bottom").text());
                var f1num = parseInt($(".f1 .top").text());
                f1num =f1num<denominatorfinal?f1num:f1num-1;
                $(".f1 .top").text(f1num);
                $(".f2 .bottom").text(denominatorfinal);
                count = 0;
                randomdeno2([2,3,4,5,6,7,8,9,10],f1num,denominatorfinal,$(".f2 .top"));
                var f2num = parseInt($(".f2 .top").text());
                var numeratorfinal = f1num - f2num;
                // count = 0;
                checkinput(numeratorfinal,denominatorfinal,$(".texttop1"),$(".texttop2"));

                break;
            case 2:
                count = 0;
                $(".bottom").css("border-top","0.1em solid black");
                $(".exfrac2").css("left","43%");
                $(".emptydiv").css("top","72%");
                numeratorarray = [2,3,4,5]
                denominatorarray = [1,2,3,4]
                var frac1top = parseInt(randomnum(numeratorarray));
                $(".exfrac1 .top").text(frac1top);
                randomdeno3(denominatorarray,frac1top,5,$(".exfrac2 .top"));
                var frac2top = parseInt($(".exfrac2 .top").text());
                loadsvg("fivemainImg",frac1top,frac2top);
                var numeratorfinal = frac1top -frac2top;
                var denominatorfinal = 5;
                checkinput(numeratorfinal,denominatorfinal,$(".input1"),$(".input2"));
                break
            case 3:
                count = 0;
                $(".bottom").css("border-top","0.1em solid black");
                $(".emptydiv").css({"top":"72%","left":"42%"});
                $(".exfrac2").css({"left":"66%"});
                $(".input4").val(5).attr("disabled","disabled");
                numeratorarray = [2,3,4,5]
                denominatorarray = [1,2,3,4]
                var frac1top = parseInt(randomnum(numeratorarray));
                $(".exfrac1 .top").text(frac1top);
                randomdeno3(denominatorarray,frac1top,5,$(".exfrac2 .top"));
                var frac2top = parseInt($(".exfrac2 .top").text());
                loadsvg("fivemainImg",frac1top,frac2top);
                var numeratorfinal = frac1top -frac2top;
                var denominatorfinal = 5;
                checkinput(numeratorfinal,denominatorfinal,$(".input3"),$(".input4"));
                break;

            case 4:
            case 5:
                count = 0;
                $(".bottom").css("border-top","0.1em solid black");
                var array = [1,2,3,4,5,6,7,8,9,10];
                loadnumdeno($(".f1 .top"),$(".f1 .bottom"));
                $(".f22 .bottom").text($(".f1 .bottom").text());
                var frac1top = parseInt($(".f1 .top").text());
                count = 0;
                denominatorarray = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]
                randomnumerator(denominatorarray,$(".f1 .bottom").text(),$(".f22 .top"));
                var frac2top = parseInt($(".f22 .top").text());
                checkans4(frac1top,frac2top);
                break;
            case 6:
            case 7:
                count = 0;
                $(".bottom").css("border-top","0.1em solid black");
                numeratorarray = [1,2,3,4,5,6,7,8,9,10]
                denominatorarray = [4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]
                loadnumdeno($(".f1 .top"),$(".f1 .bottom"));
                var firstnum  = parseInt($(".f1 .top").text());
                var denominator = parseInt($(".f1 .bottom").text());
                $(".f22 .bottom,.f23 .bottom").text(denominator);
                numeratorarray = [1,2,3,4,5,6,7,8,9,10];
                count = 0;
                randomnumerator(numeratorarray,denominator,$(".f22 .top"));
                var secondnum  = parseInt($(".f22 .top").text());
                numeratorarray = [1,2,3,4,5,6,7,8,9,10];
                count = 0;
                randomnumerator(numeratorarray,denominator,$(".f23 .top"));
                var thirdnum = parseInt($(".f23 .top").text());
                checkallans(firstnum,secondnum,thirdnum);
                break;
            case 8:
                count = 0;
                numeratorarray = [3,4,5,6,7,8,9,10]
                denominatorarray = [4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]
                loadnumdeno($(".opt3 .top,.o3 .top"),$(".opt3 .bottom"));
                var firstnum  = parseInt($(".opt3 .top").text());
                var denominator = parseInt($(".opt3 .bottom").text());
                $(".opt1 .bottom,.opt2 .bottom,.opt3 .bottom,.o1 .bottom,.o2 .bottom,.o3 .bottom").text(denominator);
                numeratorarray = [2,3,4,5,6,7,8,9,10];
                numeratorarray.splice(numeratorarray.indexOf(firstnum),8);
                count = 0;
                randomnumerator(numeratorarray,denominator,$(".opt2 .top ,.o2 .top"));
                var secondnum  = parseInt($(".opt2 .top").text());
                numeratorarray = [1,2,3,4,5,6,7,8,9,10];
                numeratorarray.splice(numeratorarray.indexOf(firstnum),9);
                numeratorarray.splice(numeratorarray.indexOf(secondnum),8);
                count = 0;
                randomnumerator(numeratorarray,denominator,$(".opt1 .top,.o1 .top"));
                var thirdnum = parseInt($(".opt1 .top").text());
                var optionclass = ["draggable opt1","draggable opt2","draggable opt3"];
                shufflehint(optionclass,"draggable");
                dragdrop();
                break;
            case 9:
                count = 0;
                numeratorarray = [3,4,5,6,7,8,9,10]
                denominatorarray = [4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]
                loadnumdeno($(".opt1 .top,.o1 .top"),$(".opt1 .bottom"));
                var firstnum  = parseInt($(".opt1 .top").text());
                var denominator = parseInt($(".opt1 .bottom").text());
                $(".opt2 .bottom,.opt3 .bottom,.o1 .bottom,.o2 .bottom,.o3 .bottom").text(denominator);
                numeratorarray = [2,3,4,5,6,7,8,9,10];
                numeratorarray.splice(numeratorarray.indexOf(firstnum),8);
                count = 0;
                randomnumerator(numeratorarray,denominator,$(".opt2 .top ,.o2 .top"));
                var secondnum  = parseInt($(".opt2 .top").text());
                numeratorarray = [1,2,3,4,5,6,7,8,9,10];
                numeratorarray.splice(numeratorarray.indexOf(firstnum),9);
                numeratorarray.splice(numeratorarray.indexOf(secondnum),8);
                count = 0;
                randomnumerator(numeratorarray,denominator,$(".opt3 .top,.o3 .top"));
                var thridnum = parseInt($(".opt3 .top").text());
                var optionclass = ["draggable opt1","draggable opt2","draggable opt3"];
                shufflehint(optionclass,"draggable");
                dragdrop();
                break;
            default:
                navigationcontroller();
                break;
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                score.gotoNext();
                templateCaller();
                break;
        }
    });
    //
    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }


    function checkans(correctwrongimg1){
        count = 0;
        scoreupdate = 0;
        $(".option").on("click",function () {
            createjs.Sound.stop();
            if($(this).attr("data-answer").toString().trim()=='correct') {
                $(this).addClass("correctans");
                $(this).append("<img class='"+correctwrongimg1+"' src='images/right.png'/>");
                play_correct_incorrect_sound(1);
                count++;
                scoreupdate++;
                if(count == 3){
                    navigationcontroller();
                    $(".option").addClass("avoid-clicks");
                }
                if(scoreupdate == 3){
                    score.update(true);
                }
            }
            else{
                scoreupdate = 0;
                $(this).addClass("wrongans avoid-clicks");
                $(this).append("<img class='"+correctwrongimg1+"' src='images/wrong.png'/>");
                play_correct_incorrect_sound(0);
                score.update(false);
            }

        });
    }

    function checkans1(correctwrongimg1){
        scoreupdate = 1;
        $(".option").on("click",function () {
            createjs.Sound.stop();
            if($(this).attr("data-answer").toString().trim()=='correct') {
                $(this).addClass("correctans avoid-clicks");
                $(this).append("<img class='"+correctwrongimg1+"' src='images/right.png'/>");
                play_correct_incorrect_sound(1);
                navigationcontroller();
                $(".option").addClass("avoid-clicks");
                scoreupdate==1?score.update(true):'';
                $(".text3").removeClass("temptext");
                $(".text3 .bottom").css("border-top","0.1em solid black")
            }
            else{
                scoreupdate = 0;
                $(this).addClass("wrongans avoid-clicks");
                $(this).append("<img class='"+correctwrongimg1+"' src='images/wrong.png'/>");
                play_correct_incorrect_sound(0);
                score.update(false);
            }


        });
    }
    function optionselect(array,optionselectelem){
        optionselectelem.parent().removeClass("wrongans");
        array.splice(array.indexOf(optionselectelem.text()), 1);
        var random = array[Math.floor(Math.random() * array.length)];
        optionselectelem.text(random);
        count++;
        if (count > 1) {
            count = 0;
            return [data.string.greaterthan, data.string.lessthan, data.string.equalto];
        }
        else
            return array;
    }


    function splitintofractions($splitinside){
        typeof $splitinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
        if($splitintofractions.length > 0){
            $.each($splitintofractions, function(index, value){
                $this = $(this);
                var tobesplitfraction = $this.html();
                if($this.hasClass('fraction')){
                    tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
                    tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
                }else{
                    tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
                    tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
                }


                tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
                $this.html(tobesplitfraction);
            });
        }
    }
    function shufflehint(optionclass,option) {
        var optdiv = $(".coverboardfull");

        for (var i = optdiv.find("."+option).length; i >= 0; i--) {
            optdiv.append(optdiv.find("."+option).eq(Math.random() * i | 0));
        }
        optdiv.find("."+option).removeClass().addClass("current");
        optdiv.find(".current").each(function (index) {
            $(this).addClass(optionclass[index]);
        });
    }

    function imageload(){
        loadstackofpear(totalpieces);
        for(var i =0;i<sureygot;i++)
            $(".peardiv1").append("<img class='pearImg' src= '"+ preload.getResult("pear").src +"'>");
        for(var i =0;i<ririgot;i++)
            $(".peardiv2").append("<img class='pearImg' src= '"+ preload.getResult("pear").src +"'>");

    }

    function popualteoption(correctans,classoption){

        var items = [];
        for(var i = 1;i<totalpieces;i++){
            items.push(i);
        }
        items.splice(items.indexOf(ririgot), 1);
        items.splice(items.indexOf(sureygot), 1);
        items.splice(items.indexOf(correctans), 1);
        var random = items[Math.floor(Math.random()*items.length)]
        $(".opt1,.option1").find(".top").text(correctans!=totalpieces?totalpieces:chankheygot);
        $(".opt1,.option1").find(".bottom").text(totalpieces);
        $(".opt2,.option2").find(".top").text(correctans);
        $(".opt2,.option2").find(".bottom").text(totalpieces);
        $(".opt3,.option3").find(".top").text(ririgot!=correctans?ririgot:sureygot);
        $(".opt3,.option3").find(".bottom").text(totalpieces);
        $(".opt4,.option4").find(".top").text(random);
        $(".opt4,.option4").find(".bottom").text(totalpieces);
        shufflehint(classoption,"option");
        checkans("correctwrongimg");
    }
    function generateRandom(min, max, donotmatchoption) {
        var num = Math.floor(Math.random() * (max - min + 1)) + min;
        return (num == donotmatchoption) ? generateRandom(min, max) : num;
    }
    function generateRirishare(totalpieces,sureygot){
        count++;
        var tempriri =  generateRandom(totalpieces-2,1,sureygot);
        return ((totalpieces -sureygot -tempriri)>=1)?tempriri:(count<10?generateRirishare(totalpieces,sureygot):1);
    }




    function loadpear(elem,noOfpieces){
        for(var i =0;i<noOfpieces;i++)
            elem.append("<img class='pearImg' src= '"+ preload.getResult("pear").src +"'>");

    }
    function popopt(classoption){
        var srtotal = sureygot+ririgot;
        $(".o1").find(".share").first().find(".top").text(totalpieces);
        $(".o1").find(".share").first().find(".bottom").text(totalpieces);
        $(".o1").find(".share").last().find(".top").text(sureygot+ririgot);
        $(".o1").find(".share").last().find(".bottom").text(totalpieces);
        $(".o2").find(".share").first().find(".top").text(totalpieces);
        $(".o2").find(".share").first().find(".bottom").text(totalpieces);
        $(".o2").find(".share").last().find(".top").text(sureygot+ririgot);
        $(".o2").find(".share").last().find(".bottom").text(totalpieces);
        shufflehint(classoption,"option");
        checkans("correctwrongimg");
    }



    function navigationcontroller(islastpageflag) {
        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
        }
        else if ($total_page == 1) {
            $nextBtn.css('display', 'none');

            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if (countNext > 0 && countNext < $total_page) {

            $nextBtn.show(0);
        }
        else if (countNext == $total_page - 2) {

            $nextBtn.css('display', 'none');
            // if lastpageflag is true
            // ole.footerNotificationHandler.pageEndSetNotification();
        }

    }

    function put_image() {
        var contentCount=content[countNext];
        var imageblockcontent=contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent,contentCount,preload);
        imageblockcontent=contentCount.hasOwnProperty('imageblock1');
        contentCount = imageblockcontent?contentCount.imageblock1[0]:false;
        imageblockcontent?dynamicimageload(imageblockcontent,contentCount):'';
    }

    function dynamicimageload(imageblockcontent,contentCount){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }


    function randomnum(items){
        var random = items[Math.floor(Math.random()*items.length)]
        items.splice(items.indexOf(random), 1);
        return random;
    }
    function randomdeno(items,numerator,deno){
        count++;
        var denominator = items[Math.floor(Math.random()*items.length)]
        items.splice(items.indexOf(denominator), 1);
        if(denominator>numerator) {
            deno.text(denominator);
        }
        else{
            if(count<10){
                randomdeno([2,3,4,5,6,7,8,9,10],numerator,deno);
            }
            else{
                deno.text(10);
            };
        }
    }

    function loadnumdeno(num,deno){
        var numerator = randomnum(numeratorarray);
        numeratorarray.splice(numeratorarray.indexOf(numerator), 1);
        num.text(numerator);
        count = 0;
        randomdeno(denominatorarray,numerator,deno);
    }

    function loadsvg(imgid,frac1top,frac2top){
        // var s = Snap('#circlesvg');
        // var svg = Snap.load(preload.getResult(imgid).src, function ( loadedFragment ) {
        //     s.append(loadedFragment);
        // });
        var s1 = Snap('#circlesvg1');
        var svg = Snap.load(preload.getResult(imgid).src, function ( loadedFragment ) {
            s1.append(loadedFragment);
            var parts = $("#circlesvg1").find('.part')
            parts.css("opacity","1");
            parts.slice(frac1top).css("opacity","0");
            $("#circlesvg1").css({"left":"13%"});
        });
        var s2 = Snap('#circlesvg2');
        var svg = Snap.load(preload.getResult(imgid).src, function ( loadedFragment ) {
            s2.append(loadedFragment);
            var parts = $("#circlesvg2").find('.part')
            parts.css("opacity","1");
            parts.slice(frac2top).css("opacity","0");
            countNext==3?$("#circlesvg2").css({"left":"59%"}):$("#circlesvg2").css({"left":"36%"});

        });

    }

    function checkans4(frac1top,frac2top){
        $(".div11 p").text("");
        var array1 = [data.string.greaterthan, data.string.lessthan, data.string.equalto];
        $(".downarrow1").click(function(){
            array1 = optionselect(array1,$(".div11 p"));
        });
        scoreupdate = 1;
        $(".submitbtn").click(function(){
            if((frac1top > frac2top && $(".div11 p").text() == data.string.greaterthan)||(frac1top < frac2top && $(".div11 p").text() == data.string.lessthan) ||(frac1top == frac2top && $(".div11 p").text() == data.string.equalto)){
                $(".div11").addClass("correctans");
                $(".downarrow1").addClass("avoid-clicks");
                play_correct_incorrect_sound(1);
                scoreupdate==1?score.update(true):"";
                navigationcontroller();
                $(this).addClass("avoid-clicks");
            }
            else {
                scoreupdate = 0;
                $(".div11").addClass("wrongans");
                play_correct_incorrect_sound(0);
                score.update(false);
            }
        });
    }

    function randomdeno1(items,prevnumerator,denominator,assignto){
        count++;
        var newnumerator = items[Math.floor(Math.random()*items.length)]
        items.splice(items.indexOf(newnumerator), 1);
        if(newnumerator<denominator && (newnumerator + prevnumerator)<denominator) {
            assignto.text(newnumerator);
        }
        else{
            if(count<10){randomdeno1([2,3,4,5,6,7,8,9,10],prevnumerator,denominator,assignto)}
            else{
                assignto.text(1);};
        }
    }

    function randomdeno2(items,prevnumerator,denominator,assignto){
        count++;
        var newnumerator = items[Math.floor(Math.random()*items.length)]
        items.splice(items.indexOf(newnumerator), 1);
        if(newnumerator<denominator  && (prevnumerator - newnumerator)>0 && (newnumerator + prevnumerator)<denominator) {
            assignto.text(newnumerator);
        }
        else{
            if(count<10){randomdeno2([2,3,4,5,6,7,8,9,10],prevnumerator,denominator,assignto)}
            else{
                assignto.text(1);};
        }
    }

    function randomdeno3(items,prevnumerator,denominator,assignto){
        count++;
        var newnumerator = items[Math.floor(Math.random()*items.length)]
        items.splice(items.indexOf(newnumerator), 1);
        if(newnumerator<denominator  && (prevnumerator - newnumerator)>0 ) {
            assignto.text(newnumerator);
        }
        else{
            if(count<10){randomdeno3([1,2,3,4],prevnumerator,denominator,assignto)}
            else{
                assignto.text(1);};
        }
    }

    function checkinput(numeratorfinal,denominatorfinal,firstentry,secondentry){
        scoreupdate = 1;
        $(".submitbtn").click(function(){
            if(parseInt(firstentry.val()) == numeratorfinal){
                count++;
                firstentry.removeClass("wrongans").addClass("correctans").attr("disabled","disabled");
                $("div.emptydiv1 > img").remove();
                $(".emptydiv1").append("<img class='relativecls correctwrongimg2' src='images/right.png'/>");
            }
            else{
                $(".emptydiv1").append("<img class='relativecls correctwrongimg2' src='images/wrong.png'/>");
                scoreupdate = 0;
                firstentry.addClass("wrongans");
            }
            if(parseInt(secondentry.val()) == denominatorfinal){
                count++;
                countNext!=3 ?secondentry.removeClass("wrongans").addClass("correctans").attr("disabled","disabled"):'';
                $("div.emptydiv2 > img").remove();
                $(".emptydiv2").remove("img").append("<img class='relativecls correctwrongimg2' src='images/right.png'/>");
            }
            else{
                $(".emptydiv2").append("<img class='relativecls correctwrongimg2' src='images/wrong.png'/>");
                scoreupdate = 0;
                secondentry.addClass("wrongans");
            }
            if(parseInt(secondentry.val()) == denominatorfinal && parseInt(firstentry.val()) == numeratorfinal){
                play_correct_incorrect_sound(1);
                navigationcontroller();
                $(this).addClass("avoid-clicks");
            }
            else{
                play_correct_incorrect_sound(0);
            }
            if(scoreupdate == 1){
                score.update(true);
            }
        });
    }

    function randomnumerator(items,denominator,num){
        count++;
        var numerator = items[Math.floor(Math.random()*items.length)]
        items.splice(items.indexOf(numerator), 1);
        if(denominator>numerator) {
            num.text(numerator);
        }
        else{
            if(count<10){
                randomnumerator([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30],denominator,num);
            }
            else{
                num.text(1);
            };
        }
    }


    function checkallans(option1,option2,option3){
        $(".div11 p,.div12 p").text("");
        count = 0;
        var test = 0;
        var array1 = [data.string.greaterthan, data.string.lessthan, data.string.equalto];
        $(".downarrow1").click(function(){
            array1 = optionselect(array1,$(".div11 p"));
        });
        array1 = [data.string.greaterthan, data.string.lessthan, data.string.equalto];
        $(".downarrow2").click(function(){
            array1 = optionselect(array1,$(".div12 p"));
        });
        var correctclick = 1;
        $(".submitbtn").click(function(){
            var currentdivval = $(".div11 p").text().toString().trim();
            var currentdivval1 = $(".div12 p").text().toString().trim();
            if ((option1 > option2 && currentdivval== data.string.greaterthan) || (option1 < option2 && currentdivval == data.string.lessthan) || (option1 === option2 && currentdivval == data.string.equalto)) {
                    $(".div11").removeClass("wrongans").addClass("correctans avoid-clicks");
                    test++;
                }
                else {
                correctclick = 0;
                    $(".div11").addClass("wrongans");
                    test = 0;
                }
                if ((option2 > option3 && currentdivval1 == data.string.greaterthan) || (option2 < option3 && currentdivval1 == data.string.lessthan) || (option3 == option2 && currentdivval1 == data.string.equalto)) {
                    $(".div12").removeClass("wrongans").addClass("correctans avoid_clicks");
                    test++;
                }
                else {
                correctclick = 0;
                    $(".div12").addClass("wrongans");
                    test = 0;
                }
                if(test==2){
                    $(this).addClass("avoid-clicks");
                    play_correct_incorrect_sound(1);
                    navigationcontroller(countNext,$total_page,countNext==16?true:false);
                }
                else
                    play_correct_incorrect_sound(0);
                if(correctclick == 1){
                    score.update(true);

                }
                else{
                    score.update(false);
                }

        });
    }
    function dragdrop(){
        scoreupdate = 1;
        var next = 0;
        $(".draggable").draggable({
            containment: "body",
            revert: true,
            appendTo: "body",
            zindex: 10,
            start:function(event, ui){
                $(".wrongImg").remove();
                $(".draggable,.droppable").removeClass("wrongcss");
                $(this).find(".bottom").css("border-top","0.1em solid black");
            },
            stop:function(){
                $(this).find(".bottom").css("border-top","0.1em solid white");
            }
        });
        $('.droppable').droppable({
            accept : ".draggable",
            hoverClass: "hovered",
            drop: function(event, ui) {
                var draggableans = ui.draggable.attr("data-answer");
                if(draggableans.toString().trim() == ($(this).attr("data-answer").toString().trim())) {
                    play_correct_incorrect_sound(1);
                    ui.draggable.hide(0);
                    $(this).css({"color":"black"});
                    $(this).find(".bottom").css("border-top","0.1em solid black");
                    $(this).addClass("correctans");
                    next++;
                    next>2?navigationcontroller(countNext,$total_page,true):'';
                    if(scoreupdate ==1 && next >2){
                        score.update(true)
                    }
                    else
                        score.update(false);
                }
                else {
                    scoreupdate = 0;
                    play_correct_incorrect_sound(0);
                }

            }
        });
    }

    function enterbtn() {
        $(document).keypress(function(e) {
            if(e.which == 13) {
                $('.submitbtn').click();
            }
        });
    }
});

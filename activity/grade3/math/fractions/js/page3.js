var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle centeralign",
                textclass: "content1 centertext",
                textdata: data.string.p3text1
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "bg",
                    imgclass: "relativecls img1",
                    imgid: 'coverpageImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "box",
                    imgclass: "relativecls img2",
                    imgid: 'boxImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "squirrel",
                    imgclass: "relativecls img3",
                    imgid: 'squirrelImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "monkey",
                    imgclass: "relativecls img4",
                    imgid: 'monkeyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "riri",
                    imgclass: "relativecls img5",
                    imgid: 'ririImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"maintitle1 centeralign",
                textclass: "content centertext",
                textdata: data.string.p3text2
            },
            {
                textdiv:"maintitle2 centeralign",
                textclass: "content1 centertext",
                textdata: data.string.dragdropmsg
            },
            {
                splitintofractionsflag: true,
                textdiv:"frac3_1",
                spandata: data.string.p2frac2,
                additionalspanclass:"chapter"
            },
            {
                splitintofractionsflag: true,
                textdiv:"frac3_2",
                spandata: data.string.p2frac2,
                additionalspanclass:"chapter"
            },
            {
                textdiv:"draggable opt1",
                textclass: "chapter centertext",
                textdata: data.string.greaterthan
            },
            {
                textdiv:"draggable opt2",
                textclass: "chapter centertext",
                textdata: data.string.lessthan
            },
            {
                textdiv:"draggable opt3",
                textclass: "chapter centertext",
                textdata: data.string.equalto,
                ans:"correct"
            },
            {
                textdiv:"droppable",
                textclass: "chapter centertext",
                textdata: data.string.equalto,
                ans:"correct"
            },
         ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "chankheybox",
                    imgclass: "relativecls img1",
                    imgid: 'chankheyboxImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "lookupr",
                    imgclass: "relativecls img2",
                    imgid: 'lookuprImg',
                    imgsrc: ""
                },
            ]
        }]
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"maintitle1 centeralign",
                textclass: "content centertext",
                textdata: data.string.p3text2_0
            },
            {
                textdiv:"maintitle2 centeralign",
                textclass: "content1 centertext",
                textdata: data.string.dragdropmsg
            },
            {
                splitintofractionsflag: true,
                textdiv:"frac3_1",
                spandata: data.string.p2frac2,
                additionalspanclass:"chapter"
            },
            {
                splitintofractionsflag: true,
                textdiv:"frac3_2",
                spandata: data.string.p2frac1,
                additionalspanclass:"chapter"
            },
            {
                textdiv:"draggable opt1",
                textclass: "chapter centertext",
                textdata: data.string.greaterthan,
                ans:"correct"
            },
            {
                textdiv:"draggable opt2",
                textclass: "chapter centertext",
                textdata: data.string.lessthan
            },
            {
                textdiv:"draggable opt3",
                textclass: "chapter centertext",
                textdata: data.string.equalto
            },
            {
                textdiv:"droppable",
                textclass: "chapter centertext",
                textdata: data.string.greaterthan,
                ans:"correct"
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "chankheybox",
                    imgclass: "relativecls img1",
                    imgid: 'chankheyboxImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "lookupr",
                    imgclass: "relativecls img2",
                    imgid: 'lookupsureyImg',
                    imgsrc: ""
                },
            ]
        }]
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"maintitle1 centeralign",
                textclass: "content centertext",
                textdata: data.string.p3text2_1
            },
            {
                textdiv:"maintitle2 centeralign",
                textclass: "content1 centertext",
                textdata: data.string.dragdropmsg
            },
            {
                splitintofractionsflag: true,
                textdiv:"frac3_1",
                spandata: data.string.p2frac1,
                additionalspanclass:"chapter"
            },
            {
                splitintofractionsflag: true,
                textdiv:"frac3_2",
                spandata: data.string.p2frac2,
                additionalspanclass:"chapter"
            },
            {
                textdiv:"draggable opt1",
                textclass: "chapter centertext",
                textdata: data.string.greaterthan
            },
            {
                textdiv:"draggable opt2",
                textclass: "chapter centertext",
                textdata: data.string.lessthan,
                ans:"correct"
            },
            {
                textdiv:"draggable opt3",
                textclass: "chapter centertext",
                textdata: data.string.equalto
            },
            {
                textdiv:"droppable",
                textclass: "chapter centertext",
                textdata: data.string.lessthan,
                ans:"correct"
            },
            {
                textdiv:"tipiconmsg iconmsg maintitle",
                textclass:"content2 centertext",
                textdata:data.string.tipiconmsg
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "chankheybox1",
                    imgclass: "relativecls img1",
                    imgid: 'lookupsureyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "lookupr",
                    imgclass: "relativecls img2",
                    imgid: 'lookuprImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "tipicon icon",
                    imgclass: "relativecls tipiconimg",
                    imgid: 'tipiconImg',
                    imgsrc: ""
                }
            ]
        }]
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"maintitle1 centeralign",
                textclass: "content centertext",
                textdata: data.string.p3text3
            },
            {
                textdiv:"maintitle3 centeralign",
                textclass: "content1 centertext",
                textdata: data.string.p3text4
            },
            {
                textdiv:"option option1",
                textclass: "chapter centertext",
                textdata: data.string.five
            },
            {
                textdiv:"option option2",
                textclass: "chapter centertext",
                textdata: data.string.six
            },
            {
                textdiv:"option option3",
                textclass: "chapter centertext",
                textdata: data.string.seven
            },
            {
                textdiv:"option option4",
                textclass: "chapter centertext",
                textdata: data.string.eight,
                ans:"correct"
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "box1",
                    imgclass: "relativecls img1",
                    imgid: 'chankheyboxImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "box2",
                    imgclass: "relativecls img2",
                    imgid: 'lookupsureyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "box3",
                    imgclass: "relativecls img3",
                    imgid: 'lookuprImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "pear1",
                    imgclass: "relativecls img4",
                    imgid: 'threepearImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "pear2",
                    imgclass: "relativecls img5",
                    imgid: 'twopearImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "pear3",
                    imgclass: "relativecls img6",
                    imgid: 'threepearImg',
                    imgsrc: ""
                },
            ]
        }]
    },
    // slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"maintitle1 centeralign",
                textclass: "content1 centertext",
                textdata: data.string.p3text5,

            },
            {
                textdiv:"texttopic hide1 centeralign",
                textclass: "content1 centertext",
                textdata: data.string.p3text6,

            },
            {
                textdiv:"plus  centeralign hide1",
                textclass: "datafont centertext",
                textdata: data.string.plus
            },
            {
                textdiv:"plus1  hide1 centeralign",
                textclass: "datafont centertext",
                textdata: data.string.plus
            },
            {
                textdiv:"equalto hide1 centeralign",
                textclass: "datafont centertext",
                textdata: data.string.equalto
            },
            {
                textdiv:"equalto1  hide1 centeralign",
                textclass: "datafont centertext",
                textdata: data.string.equalto
            },
            {
                textdiv:"equalto2 hide1 centeralign",
                textclass: "datafont centertext",
                textdata: data.string.equalto
            },
            {
                splitintofractionsflag: true,
                textdiv:"f1 hide1 centeralign",
                spandata: data.string.p2frac2,
                additionalspanclass:"title",
            },
            {
                splitintofractionsflag: true,
                textdiv:"f2 hide1 centeralign",
                spandata: data.string.p2frac1,
                additionalspanclass:"title",
            },
            {
                splitintofractionsflag: true,
                textdiv:"f3 hide1 centeralign",
                spandata: data.string.p2frac2,
                additionalspanclass:"title",
            },
            {
                splitintofractionsflag: true,
                textdiv:"f4 hide1 centeralign",
                spandata: data.string.p2frac6,
                additionalspanclass:"title",
            },
            {
                splitintofractionsflag: true,
                textdiv:"f5 hide1 centeralign",
                spandata: data.string.p2frac5,
                additionalspanclass:"title",
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "threeofthem",
                    imgclass: "relativecls img1",
                    imgid: 'threeofthemImg',
                    imgsrc: ""
                },
            ]
        }]
    },
    //slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"remember centeralign",
                textclass: "content1 centertext",
                textdata: data.string.remember,
            },
            {
                textdiv:"numerator1 centeralign",
                textclass: "content1 centertext",
                textdata: data.string.numerator,
            },
            {
                textdiv:"denominator1 centeralign",
                textclass: "content1 centertext",
                textdata: data.string.denominator,
            },
            {
                splitintofractionsflag: true,
                textdiv:"lastfrac centeralign",
                spandata: data.string.p2frac1,
                additionalspanclass:"title",
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "peargif",
                    imgclass: "relativecls img1",
                    imgid: 'peargifImg',
                    imgsrc: ""
                },
            ]
        }],
        lefttextblock:[
            {
                textdiv:"remembertitle",
                texts:[
                    {
                        textclass: "content2 centertext centertxt",
                        textdata: data.string.p3text7,
                    },
                    {
                        textclass: "content2 centertext centertxt",
                        textdata: data.string.p3text8,
                    },
                    {
                        splitintofractionsflag: true,
                        textclass: "content2 centertext centertxt",
                        textdata: data.string.p3text9,
                    }
                ]

            }
        ]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var time;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "coverpageImg", src: imgpath + "bg.png", type: createjs.AbstractLoader.IMAGE},
            {id: "squirrelImg", src: imgpath + "squirrel.png", type: createjs.AbstractLoader.IMAGE},
            {id: "monkeyImg", src: imgpath + "monkey.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ririImg", src: imgpath + "rhino.png", type: createjs.AbstractLoader.IMAGE},
            {id: "boxImg", src: imgpath + "box.png", type: createjs.AbstractLoader.IMAGE},
            {id: "chankheyboxImg", src: imgpath + "chankheybox.png", type: createjs.AbstractLoader.IMAGE},
            {id: "lookuprImg", src: imgpath + "rhinowhite.png", type: createjs.AbstractLoader.IMAGE},
            {id: "lookupsureyImg", src: imgpath + "sureybg.png", type: createjs.AbstractLoader.IMAGE},
            {id: "twopearImg", src: imgpath + "pearstack/twopear.png", type: createjs.AbstractLoader.IMAGE},
            {id: "threepearImg", src: imgpath + "threepear.png", type: createjs.AbstractLoader.IMAGE},
            {id: "threeofthemImg", src: imgpath + "threeofthem.png", type: createjs.AbstractLoader.IMAGE},
            {id: "peargifImg", src: imgpath + "animation/PearHQ_01.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "tipiconImg", src: "images/info_icon01.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset + "s3_p1.ogg"},
            {id: "sound_1", src: soundAsset + "s3_p2.ogg"},
            {id: "sound_2", src: soundAsset + "s3_p3.ogg"},
            {id: "sound_3", src: soundAsset + "s3_p4.ogg"},
            {id: "sound_4", src: soundAsset + "s3_p5.ogg"},
            {id: "sound_5", src: soundAsset + "s3_p6.ogg"},
            {id: "sound_6", src: soundAsset + "s3_p7.ogg"},

        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("fractioncontent", $("#fractioncontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        splitintofractions($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 1:
            case 2:
            case 3:
                sound_nav("sound_"+countNext);
                $(".maintitle1,.maintitle2,.frac3_1,.frac3_2,.draggable,.droppable").hide();
                $(".maintitle1").delay(100).fadeIn(1000);
                $(".maintitle2").delay(2000).fadeIn(1000);
                $(".draggable").delay(4000).fadeIn(1000);
                $(".frac3_1,.frac3_2,.droppable").delay(6000).fadeIn(1000);
                var classoption = ["draggable opt1","draggable opt2","draggable opt3"]
                $(".draggable").hover(function(){
                    var optiontext = $(this).text().toString().trim();
                    var appendtext = (optiontext==data.string.greaterthan)?data.string.gt:(optiontext==data.string.lessthan)?data.string.lt:data.string.et;
                    $(this).append($("<span class='signname'> "+appendtext+"</span>"));
                }, function() {
                    $( this ).find( "span" ).remove();
                });
                shufflehint(classoption,"draggable");
                dragdrop();
                $(".iconmsg").hide();

                $(".icon").hover(
                    function(){
                        $(".iconmsg").show();
                    },
                    function(){
                        $(".iconmsg").hide();
                    }
                );
                break;
            case 4:
            sound_nav("sound_"+countNext);
                var classoption = ["option option1","option option2","option option3","option option4"]
                shufflehint(classoption,"option");
                checkans("correctwrongimg");
                break;
            case 5:
                $(".hide1").hide();
                $(".f1").delay(1000).fadeIn(500);
                $(".plus,.f2").delay(2000).fadeIn(500);
                $(".plus1,.f3").delay(3000).fadeIn(500);
                $(".equalto,.f4").delay(4000).fadeIn(500);
                $(".equalto1,.f5").delay(5000).fadeIn(500);
                $(".equalto2,.texttopic").delay(6000).fadeIn(500);
                sound_player("sound_"+countNext);
                break;
            default:
                sound_player("sound_"+countNext);
                break;
        }
    }

  function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, nextBtn){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(0);
		});
	}
  function sound_nav(sound_id, nextBtn){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

    function shufflehint(optionclass,option) {
        var optdiv = $(".coverboardfull");

        for (var i = optdiv.find("."+option).length; i >= 0; i--) {
            optdiv.append(optdiv.find("."+option).eq(Math.random() * i | 0));
        }
        optdiv.find("."+option).removeClass().addClass("current");
        optdiv.find(".current").each(function (index) {
            $(this).addClass(optionclass[index]);
            $(this).addClass($(this).attr("data-answer"));
        });
    }

    function checkans(correctwrongimg1){
        $(".option,.optiontext").on("click",function () {
            createjs.Sound.stop();
            if($(this).attr("data-answer").toString().trim()=='correct') {
                $(this).addClass("correctans");
                $(".option,.optiontext").addClass("avoid-clicks");
                $(this).append("<img class='"+correctwrongimg1+"' src='images/right.png'/>");
                play_correct_incorrect_sound(1);
                navigationcontroller(countNext, $total_page);
                $(".text3").fadeOut(500);
                $(".reward").fadeIn(1000);
                $(".text3").css({    "padding-bottom": "4%"});
                $(".reward").find(".bottom").addClass("bottombrd");
            }
            else{
                $(this).addClass("wrongans avoid-clicks");
                $(this).append("<img class='"+correctwrongimg1+"' src='images/wrong.png'/>");
                play_correct_incorrect_sound(0);
            }
        });
    }

    function fractiondis(t1,line,t2,delay1,delay2,delay3){
        t2.delay(delay1).fadeIn(1000);
        line.delay(delay2).fadeIn(1000);
        t1.delay(delay3).fadeIn(1000);
    }
    function splitintofractions($splitinside){
        typeof $splitinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
        if($splitintofractions.length > 0){
            $.each($splitintofractions, function(index, value){
                $this = $(this);
                var tobesplitfraction = $this.html();
                if($this.hasClass('fraction')){
                    tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
                    tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
                }else{
                    tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
                    tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
                }


                tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
                $this.html(tobesplitfraction);
            });
        }
    }
    function dragdrop(){
        $(".draggable").draggable({
            containment: "body",
            revert: true,
            appendTo: "body",
            zindex: 10,
            start:function(event, ui){
                $(".wrongImg").remove();
                $(".draggable,.droppable").removeClass("wrongcss");
            },
            stop:function(event,ui){
                // $(".draggable").css("top","");

            }
        });
        $('.droppable').droppable({
            accept : ".draggable",
            hoverClass: "hovered",
            drop: function(event, ui) {
                var draggableans = ui.draggable.attr("data-answer");
                if(draggableans.toString().trim() == ($(this).attr("data-answer").toString().trim())) {
                    play_correct_incorrect_sound(1);
                    ui.draggable.hide(0);
                    $(this).css({"color":"black"});
                    navigationcontroller(countNext,$total_page,true);
                    $(".draggable").addClass("avoid-clicks");
                }
                else {
                    play_correct_incorrect_sound(0);
                }
            }
        });
    }

});

var imgpath = $ref + "/images/";
var imgpath1 = $ref + "/images/arrows/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle centeralign",
                textclass: "content1 centertext",
                textdata: data.string.p2text1
            },
            {
                textdiv:"text1 hide1 centeralign",
                textclass: "content2 centertext",
                textdata: data.string.p2text2
            },
            {
                textdiv:"text2 hide1 centeralign",
                textclass: "content2 centertext",
                textdata: data.string.p2text3
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "sqbgwhite  hide1",
                    imgclass: "relativecls img2",
                    imgid: 'sqbgwhiteImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ribgwhite  hide1",
                    imgclass: "relativecls img3",
                    imgid: 'ribgwhiteImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sqbgwhite1  hide1",
                    imgclass: "relativecls img4",
                    imgid: 'sqbgwhite1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "ribgwhite1  hide1",
                    imgclass: "relativecls img5",
                    imgid: 'ribgwhite1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "peartwo",
                    imgclass: "relativecls img6",
                    imgid: 'peartwoImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "peartwo1  hide1",
                    imgclass: "relativecls img7",
                    imgid: 'peartwo1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "pearthree",
                    imgclass: "relativecls img8",
                    imgid: 'pearthreeImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "pearthree1  hide1",
                    imgclass: "relativecls img9",
                    imgid: 'pearthree1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "pears ",
                    imgclass: "relativecls img10",
                    imgid: 'pearsssImg',
                    imgsrc: ""
                },
            ]
        }],
        fractionblock:[
            {
            fractionClass: "frac-1 content1 hide1",
            splitintofractionsflag: true,
            spandata: data.string.p2frac1
            },
            {
            fractionClass: "frac-2 content1 hide1",
            splitintofractionsflag: true,
            spandata: data.string.p2frac2
            }

        ]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle centeralign",
                textclass: "content1 centertext",
                textdata: data.string.p2text4
            },
            {
                splitintofractionsflag: true,
                textdiv:"text4 optiontext hide1 centeralign",
                textclass: "content2 centertext",
                textdata: data.string.p2text6,
                spandata: data.string.p2frac1,
                additionalspanclass:"content2"
            },
            {
                splitintofractionsflag: true,
                textdiv:"text5 optiontext hide1 centeralign",
                textclass: "content2 centertext",
                textdata: data.string.p2text7,
                spandata: data.string.p2frac2,
                additionalspanclass:"content2",
                ans:"correct"
            },
            // {
            //     textdiv:"text3 hide1 centeralign",
            //     textclass: "content2 centertext",
            //     textdata: data.string.p2text5
            // },
            {
                textdiv:"text3 reward centeralign",
                textclass: "content2 centertext",
                textdata: data.string.p2text8,
                additionalspanclass:"content2",
            },
            {
                splitintofractionsflag: true,
                textdiv:"fraccompare1 reward hide1",
                spandata: data.string.p2frac2,
                additionalspanclass:"content2"
            },
            {
                splitintofractionsflag: true,
                textdiv:"fraccompare2 reward hide1",
                spandata: data.string.p2frac1,
                additionalspanclass:"content2"
            },
            {
                textdiv:"greaterthan reward hide1 centeralign",
                textclass: "content2 centertext",
                textdata: data.string.greaterthan
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "sqbgwhite1  hide1",
                    imgclass: "relativecls img4",
                    imgid: 'sqbgwhite1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "ribgwhite1  hide1",
                    imgclass: "relativecls img5",
                    imgid: 'ribgwhite1Img',
                    imgsrc: ""
                },
            ]
        }]
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"chankeytext centeralign fadeInEffect",
                textclass: "content1 centertext",
                textdata: data.string.p2text9
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'backgroundImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "box fadeInEffect",
                    imgclass: "relativecls img3",
                    imgid: 'boxImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "chankey",
                    imgclass: "relativecls img2",
                    imgid: 'chankeyImg',
                    imgsrc: ""
                },

            ]
        }]
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'colortext',
                textdiv:"maintitle1 centeralign",
                textclass: "content1 centertext",
                textdata: data.string.p2text10
            },
            {
                splitintofractionsflag: true,
                textdiv:"text4 hide1 centeralign",
                textclass: "content2 centertext",
                textdata: data.string.p2text6,
                spandata: data.string.p2frac1,
                additionalspanclass:"content2",
            },
            {
                splitintofractionsflag: true,
                textdiv:"text5 hide1 centeralign",
                textclass: "content2 centertext",
                textdata: data.string.p2text7,
                spandata: data.string.p2frac2,
                additionalspanclass:"content2"
            },
            {
                textdiv:"text3 hide1 centeralign",
                textclass: "content2 centertext",
                textdata: data.string.p2text11
            },
            {
                textdiv:"rewardtext reward hide1 centeralign",
                textclass: "content2 centertext",
                textdata: data.string.p2text12
            },
            {
                textdiv:"option opt1",
                textclass: "chapter centertext",
                textdata: data.string.one
            },
            {
                textdiv:"option opt2",
                textclass: "chapter centertext",
                textdata: data.string.four
            },
            {
                textdiv:"option opt3",
                textclass: "chapter centertext",
                textdata: data.string.five,
                ans:"correct"
            },
            {
                textdiv:"option opt4",
                textclass: "chapter centertext",
                textdata: data.string.three
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "sqbgwhite1  hide1",
                    imgclass: "relativecls img4",
                    imgid: 'sqbgwhite1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "ribgwhite1  hide1",
                    imgclass: "relativecls img5",
                    imgid: 'ribgwhite1Img',
                    imgsrc: ""
                },
            ]
        }]
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                splitintofractionsflag: true,
                datahighlightflag : true,
                datahighlightcustomclass : 'colortext',
                textdiv:"maintitle2 centeralign",
                textclass: "content1 centertext",
                textdata: data.string.p2text13,
                additionalspanclass:"content2",

            },
            {
                textdiv:"num hide1 centeralign",
                textclass: "content1 centertext",
                textdata: data.string.numerator
            },
            {
                textdiv:"plus hide1 centeralign",
                textclass: "sign centertext",
                textdata: data.string.plus
            },
            {
                textdiv:"equalto hide1 centeralign",
                textclass: "sign centertext",
                textdata: data.string.equalto
            },
            {
                textdiv:"denum hide1 centeralign",
                textclass: "content1 centertext",
                textdata: data.string.denominator
            },
            {
                splitintofractionsflag: true,
                textdiv:"twofrac hide1 centeralign",
                spandata: data.string.p2frac1,
                additionalspanclass:"title",
            },
            {
                splitintofractionsflag: true,
                textdiv:"threefrac hide1 centeralign",
                spandata: data.string.p2frac2,
                additionalspanclass:"title",
            },
            {
                splitintofractionsflag: true,
                textdiv:"fivefrac hide1 centeralign",
                spandata: data.string.p2frac3,
                additionalspanclass:"title",
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "twopearimg  hide1",
                    imgclass: "relativecls img1",
                    imgid: 'twopearimgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "threepearimg  hide1",
                    imgclass: "relativecls img2",
                    imgid: 'threepearimgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "fivepearimg  hide1",
                    imgclass: "relativecls img3",
                    imgid: 'fivepearimgImg',
                    imgsrc: ""
                }
            ]
        }]
    },
    // slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                splitintofractionsflag: true,
                datahighlightflag : true,
                datahighlightcustomclass : 'colortext',
                textdiv:"maintitle2 centeralign",
                textclass: "content1 centertext",
                textdata: data.string.p2text13,
                additionalspanclass:"content2",

            },
            {
                textdiv:"maintitle3 centeralign",
                textclass: "content1 centertext",
                textdata: data.string.p2text14,

            },
            {
                textdiv:"plus  centeralign",
                textclass: "sign centertext",
                textdata: data.string.plus
            },
            {
                textdiv:"plus1  hide2 centeralign",
                textclass: "sign centertext",
                textdata: data.string.plus
            },
            {
                textdiv:"equalto  centeralign",
                textclass: "sign centertext",
                textdata: data.string.equalto
            },
            {
                splitintofractionsflag: true,
                textdiv:"twofrac  centeralign",
                spandata: data.string.p2frac1,
                additionalspanclass:"title",
            },
            {
                splitintofractionsflag: true,
                textdiv:"threefrac centeralign",
                spandata: data.string.p2frac2,
                additionalspanclass:"title",
            },
            {
                splitintofractionsflag: true,
                textdiv:"fivefrac1 hide2 centeralign",
                spandata: data.string.p2frac4,
                additionalspanclass:"title",
            },
            {
                splitintofractionsflag: true,
                textdiv:"fivefrac1 hide1 centeralign",
                spandata: data.string.p2frac3,
                additionalspanclass:"title",
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "twopearimg  ",
                    imgclass: "relativecls img1",
                    imgid: 'twopearimgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "threepearimg  ",
                    imgclass: "relativecls img2",
                    imgid: 'threepearimgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "twopearimg1 hide2 ",
                    imgclass: "relativecls  img3",
                    imgid: 'twopearimgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "threepearimg1  hide2",
                    imgclass: "relativecls  img4",
                    imgid: 'threepearimgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "fivepearimg1 hide1 ",
                    imgclass: "relativecls img5",
                    imgid: 'fivepearimg1Img',
                    imgsrc: ""
                },
            ]
        }]
    },
    //slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'colortext1',
                textdiv:"s6text1 centeralign",
                textclass: "content2 centertext",
                textdata: data.string.p2text15,

            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'colortext1',
                textdiv:"s6text2 centeralign",
                textclass: "content2 centertext",
                textdata: data.string.p2text16,

            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'colortext',
                textdiv:"s6text3 centeralign",
                textclass: "content centertext",
                textdata: data.string.p2text17,

            },
            {
                textdiv:"s6text4 centeralign",
                textclass: "content2 centertext",
                textdata: data.string.p2text18,

            },
            {
                textdiv:"option option1",
                textclass: "content centertext",
                textdata: data.string.one,
            },
            {
                textdiv:"option option2",
                textclass: "content centertext",
                textdata: data.string.four,
            },
            {
                textdiv:"option option3",
                textclass: "content centertext",
                textdata: data.string.five,
            },
            {
                textdiv:"option option4",
                textclass: "content centertext",
                textdata: data.string.three,
                ans:"correct"
            },
            {
                textdiv:"leftdiv",
                textclass: "content centertext",
                textdata: "",
            },
            {
                textdiv:"rightdiv",
                textclass: "content centertext",
                textdata: "",
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "stackpearImg",
                    imgclass: "relativecls img1",
                    imgid: 'stackpearImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ririsurey  ",
                    imgclass: "relativecls img2",
                    imgid: 'ririsureyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "chankeybox  ",
                    imgclass: "relativecls img3",
                    imgid: 'chankeyboxImg',
                    imgsrc: ""
                },
            ]
        }]
    },
    //slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                splitintofractionsflag: true,
                textdiv:"eighttitle1 centeralign",
                textclass: "title centertext",
                textdata: data.string.p2text19,
                additionalspanclass:"content2",
            },
            {
                splitintofractionsflag: true,
                textdiv:"eightsfrac  centeralign",
                spandata: data.string.p2frac5,
                additionalspanclass:"title",
            },
            {
                splitintofractionsflag: true,
                textdiv:"fivesfrac  centeralign",
                spandata: data.string.p2frac3,
                additionalspanclass:"title",
            },
            {
                splitintofractionsflag: true,
                textdiv:"threesfrac  centeralign",
                spandata: data.string.p2frac2,
                additionalspanclass:"title",
            },
            {
                textdiv:"minus  centeralign",
                textclass: "sign centertext",
                textdata: data.string.minus
            },
            {
                textdiv:"equalto1  centeralign",
                textclass: "sign centertext",
                textdata: data.string.equalto
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "eightimg1",
                    imgclass: "relativecls img1",
                    imgid: 'eightimg1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "eightimg2  ",
                    imgclass: "relativecls img2",
                    imgid: 'fivepearimgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "eightimg3  ",
                    imgclass: "relativecls img3",
                    imgid: 'threepearimgImg',
                    imgsrc: ""
                },
            ]
        }]
    },
    //slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"ninthtitle1 centeralign",
                textclass: "content centertext",
                textdata: data.string.p2text20,
            },
            {
                splitintofractionsflag: true,
                textdiv:"ninthrac  centeralign",
                spandata: data.string.p2frac2,
                additionalspanclass:"title",
            },
            {
                textdiv:"equalto2  centeralign",
                textclass: "sign centertext",
                textdata: data.string.equalto
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "ninthimg3  ",
                    imgclass: "relativecls img3",
                    imgid: 'threepearimgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "chankeybox1  ",
                    imgclass: "relativecls img4",
                    imgid: 'chankeyboxImg',
                    imgsrc: ""
                },
            ]
        }]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var time;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "coverpageImg", src: imgpath + "cover-page.png", type: createjs.AbstractLoader.IMAGE},
            {id: "lookupImg", src: imgpath + "overview.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pickupImg", src: imgpath + "pick-up-the-pear.png", type: createjs.AbstractLoader.IMAGE},
            {id: "hitImg", src: imgpath + "hit.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sadImg", src: imgpath + "sad.png", type: createjs.AbstractLoader.IMAGE},
            {id: "monkeyImg", src: imgpath + "monkey-enters.png", type: createjs.AbstractLoader.IMAGE},
            {id: "introImg", src: imgpath + "introduction.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pearreverseImg", src: imgpath + "animation/pear-reverse.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "dividedImg", src: imgpath + "divided.png", type: createjs.AbstractLoader.IMAGE},
            {id: "peartukraImg", src: imgpath + "tukra.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pearektukraImg", src: imgpath + "ek-tukraa.png", type: createjs.AbstractLoader.IMAGE},
            {id: "squirrelImg", src: imgpath + "squirrel.png", type: createjs.AbstractLoader.IMAGE},
            {id: "monkeystandImg", src: imgpath + "monkey.png", type: createjs.AbstractLoader.IMAGE},
            {id: "stackpearImg", src: imgpath + "stack-of-pear.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pearhqgifImg", src: imgpath + "animation/PearHQ.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "rhinomoreImg", src: imgpath + "rhino-more.png", type: createjs.AbstractLoader.IMAGE},
            {id: "nongreypearImg", src: imgpath +"slice8.png", type: createjs.AbstractLoader.IMAGE},
            {id: "greypearImg", src: imgpath +"slice8-grayscale.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialImg", src: imgpath +"speech-bubble-2.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dial1Img", src: imgpath +"speech-bubble.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sqbgwhiteImg", src: imgpath +"look-up-whitebg.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ribgwhiteImg", src: imgpath +"look-up-whitebgR.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sqbgwhite1Img", src: imgpath +"surrey.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ribgwhite1Img", src: imgpath +"riri.png", type: createjs.AbstractLoader.IMAGE},
            {id: "peartwoImg", src: imgpath +"pear-2.png", type: createjs.AbstractLoader.IMAGE},
            {id: "peartwo1Img", src: imgpath +"pearssstwo.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pearthreeImg", src: imgpath +"pear-3.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pearthree1Img", src: imgpath +"pearssthree.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pearsssImg", src: imgpath +"pearsss.png", type: createjs.AbstractLoader.IMAGE},
            {id: "backgroundImg", src: imgpath +"bg.png", type: createjs.AbstractLoader.IMAGE},
            {id: "chankeyImg", src: imgpath +"money-png.png", type: createjs.AbstractLoader.IMAGE},
            {id: "boxImg", src: imgpath +"box.png", type: createjs.AbstractLoader.IMAGE},
            {id: "twopearimgImg", src: imgpath +"pearstack/twopear.png", type: createjs.AbstractLoader.IMAGE},
            {id: "threepearimgImg", src: imgpath +"threepear.png", type: createjs.AbstractLoader.IMAGE},
            {id: "fivepearimgImg", src: imgpath +"fivefivepears.png", type: createjs.AbstractLoader.IMAGE},
            {id: "fivepearimg1Img", src: imgpath +"fivepear.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ririsureyImg", src: imgpath +"ririsurrey.png", type: createjs.AbstractLoader.IMAGE},
            {id: "chankeyboxImg", src: imgpath +"chankheybox.png", type: createjs.AbstractLoader.IMAGE},
            {id: "eightimg1Img", src: imgpath +"eightpears.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset + "s2_p1.ogg"},
            {id: "sound_1", src: soundAsset + "s2_p2_1.ogg"},
            {id: "sound_1a", src: soundAsset + "s2_p2_2.ogg"},
            {id: "sound_2", src: soundAsset + "s2_p3.ogg"},
            {id: "sound_3", src: soundAsset + "s2_p4_1.ogg"},
            {id: "sound_3b", src: soundAsset + "s2_p4_2.ogg"},
            {id: "sound_4", src: soundAsset + "s2_p5.ogg"},
            {id: "sound_5", src: soundAsset + "s2_p6.ogg"},
            {id: "sound_6", src: soundAsset + "s2_p7.ogg"},
            {id: "sound_7", src: soundAsset + "s2_p8.ogg"},
            {id: "sound_8", src: soundAsset + "s2_p9.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("fractioncontent", $("#fractioncontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        splitintofractions($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 0:
                $(".hide1").hide();
                $(".text1,.sqbgwhite").delay(2000).fadeIn(1000);

                $(".peartwo").delay(3000).fadeOut(1000);
                $(".peartwo1").delay(3000).fadeIn(1000);

                $(".text2,.ribgwhite").delay(5000).fadeIn(1000);
                $(".pearthree").delay(6000).fadeOut(1000);
                $(".pearthree1").delay(6000).fadeIn(1000);

                $(".sqbgwhite").delay(8000).fadeOut(1000);
                $(".peartwo1").delay(4000).fadeOut(1000);
                $(".sqbgwhite1").delay(8000).fadeIn(1000);
                $(".frac-1").delay(8000).fadeIn(1000);

                $(".ribgwhite").delay(10000).fadeOut(1000);
                $(".pearthree1").delay(3000).fadeOut(1000);
                $(".ribgwhite1").delay(10000).fadeIn(1000);
                $(".frac-2").delay(10000).fadeIn(1000);

                sound_player("sound_"+countNext);
                break;
            case 1:
                sound_nav("sound_"+countNext);
                $(".reward").hide();
                checkans("correctwrongimg");
                vocabcontroller.findwords(countNext);
                break;
            case 3:
                sound_nav("sound_"+countNext);
                $(".bottom").addClass("bottombrd");
                $(".text4,.text5").css({"color":"black","background-color":"#abe5ff"})
                $(".rewardtext").hide();
                var optionClass = ["option opt1","option opt2","option opt3","option opt4"];
                shufflehint(optionClass);
                checkans("correctwrongimg");
                break;
            case 4:
                $(".bottom").addClass("bottombrd");
                $(".hide1").hide();
                $(".twofrac,.twopearimg").delay(2000).fadeIn(500);
                $(".plus,.threefrac,.threepearimg").delay(3000).fadeIn(500);
                $(".equalto,.fivefrac,.fivepearimg").delay(4000).fadeIn(500);
                $(".num,.denum").delay(5000).fadeIn(500);
                sound_player("sound_"+countNext);
                break;
            case 5:
                $(".bottom").addClass("bottombrd");
                $(".hide1").hide();
                $(".hide1").delay(3000).fadeIn(1000);
                $(".hide2").delay(3000).fadeOut(1000);
                sound_player("sound_"+countNext);
                break;
            case 6:
                sound_nav("sound_"+countNext);
                var optionClass = ["option option1","option option2","option option3","option option4"];
                shufflehint(optionClass);
                checkans("correctwrongimg1");
                break;
            case 7:
            case 8:
                $(".bottom").addClass("bottombrd");
                sound_player("sound_"+countNext);
                break;
            default:
                sound_player("sound_"+countNext);
                break;
        }
    }
    function nav_button_controls(delay_ms){
  		timeoutvar = setTimeout(function(){
  			if(countNext==0){
  				$nextBtn.show(0);
  			} else if( countNext>0 && countNext == $total_page-1){
  				$prevBtn.show(0);
  				ole.footerNotificationHandler.pageEndSetNotification();
  			} else{
  				$prevBtn.show(0);
  				$nextBtn.show(0);
  			}
  		},delay_ms);
  	}
  	function sound_player(sound_id, nextBtn){
  		createjs.Sound.stop();
  		current_sound = createjs.Sound.play(sound_id);
  		current_sound.play();
  		current_sound.on('complete', function(){
  			nav_button_controls(0);
  		});
  	}
    function sound_nav(sound_id, nextBtn){
  		createjs.Sound.stop();
  		current_sound = createjs.Sound.play(sound_id);
  		current_sound.play();
  	}


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

    function shufflehint(optionclass) {
        var optdiv = $(".coverboardfull");

        for (var i = optdiv.find(".option").length; i >= 0; i--) {
            optdiv.append(optdiv.find(".option").eq(Math.random() * i | 0));
        }
        optdiv.find(".option").removeClass().addClass("current");
        optdiv.find(".current").each(function (index) {
            $(this).addClass(optionclass[index]);
            $(this).addClass($(this).attr("data-answer"));
        });
    }

    function checkans(correctwrongimg1){
        $(".option,.optiontext").on("click",function () {
            createjs.Sound.stop();
            if($(this).attr("data-answer").toString().trim()=='correct') {
                $(this).addClass("correctans");
                $(".option,.optiontext").addClass("avoid-clicks");
                $(this).append("<img class='"+correctwrongimg1+"' src='images/right.png'/>");
                play_correct_incorrect_sound(1);

                $(".text3").fadeOut(500);
                $(".reward").fadeIn(2500);
                $(".text3").css({    "padding-bottom": "4%"});
                $(".reward").find(".bottom").addClass("bottombrd");
                $(".text4,.sqbgwhite1").animate({"left":"65%"},1000);
                $(".text5,.ribgwhite1").animate({"right":"65%"},1000);
                setTimeout(function(){
                  if(countNext==1){
                  sound_player("sound_1a");
                }else if(countNext==3){
                  sound_player("sound_3b");
                }else if(countNext==6){
                nav_button_controls(200);
                }
              },800);
            }
            else{
                $(this).addClass("wrongans avoid-clicks");
                $(this).append("<img class='"+correctwrongimg1+"' src='images/wrong.png'/>");
                play_correct_incorrect_sound(0);
            }
        });
    }

    function fractiondis(t1,line,t2,delay1,delay2,delay3){
        t2.delay(delay1).fadeIn(1000);
        line.delay(delay2).fadeIn(1000);
        t1.delay(delay3).fadeIn(1000);
    }
    function splitintofractions($splitinside){
        typeof $splitinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
        if($splitintofractions.length > 0){
            $.each($splitintofractions, function(index, value){
                $this = $(this);
                var tobesplitfraction = $this.html();
                if($this.hasClass('fraction')){
                    tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
                    tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
                }else{
                    tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
                    tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
                }


                tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
                $this.html(tobesplitfraction);
            });
        }
    }

});

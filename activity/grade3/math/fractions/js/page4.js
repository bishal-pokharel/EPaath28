var imgpath = $ref + "/images/";
var imgpath1 = $ref + "/images/page1/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle centeralign",
                textclass: "content1 centertext",
                textdata: data.string.p4text1
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "back",
                    imgclass: "relativecls img1",
                    imgid: 'coverpageImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "box",
                    imgclass: "relativecls img2",
                    imgid: 'boxImg',
                    imgsrc: ""
                }
            ]
        }],
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"maintitle1 centeralign",
                textclass: "content2 centertext",
                textdata: data.string.p4text2
            },
            {
                textdiv:"maintitle2 centeralign",
                textclass: "content2 centertext",
                textdata: data.string.p4text3
            },
            {
                textdiv:"equalto centeralign hide1",
                textclass: "chapter centertext",
                textdata: data.string.equalto
            },
            {
                textdiv:"lessthan centeralign hide1",
                textclass: "chapter centertext",
                textdata: data.string.greaterthan
            },
            {
                splitintofractionsflag: true,
                textdiv:"f1 hide1",
                spandata: data.string.p2frac2,
                additionalspanclass:"frac"
            },
            {
                splitintofractionsflag: true,
                textdiv:"f2 hide1",
                spandata: data.string.p2frac2,
                additionalspanclass:"frac"
            },
            {
                splitintofractionsflag: true,
                textdiv:"f3 hide1",
                spandata: data.string.p2frac1,
                additionalspanclass:"frac"
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "back",
                    imgclass: "relativecls img1",
                    imgid: 'divisionImg',
                    imgsrc: ""
                }
            ]
        }]
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"s2title fadeInEffect",
                textclass: "content2 centertext",
                textdata: data.string.p4text4
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "back",
                    imgclass: "relativecls img1",
                    imgid: 'p105Img',
                    imgsrc: ""
                }
            ]
        }]
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"s3title fadeInEffect",
                textclass: "content2 centertext",
                textdata: data.string.p4text5
            },
            {
                textdiv:"s3title1 fadeInEffect",
                textclass: "content2 centertext",
                textdata: data.string.p4text6
            },
            {
                textdiv:"s3title2 fadeInEffect",
                textclass: "content2 centertext",
                textdata: data.string.p4text7
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "back",
                    imgclass: "relativecls img1",
                    imgid: 'p106Img',
                    imgsrc: ""
                }
            ]
        }]
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"s4title fadeInEffect",
                textclass: "content2 centertext",
                textdata: data.string.p4text8
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "back",
                    imgclass: "relativecls img1",
                    imgid: 'p107Img',
                    imgsrc: ""
                }
            ]
        }]
    },
    // slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"s5title fadeInEffect",
                textclass: "content2 centertext",
                textdata: data.string.p4text9
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "back1",
                    imgclass: "relativecls img1",
                    imgid: 'p108Img',
                    imgsrc: ""
                }
            ]
        }]
    },
    //slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"s6title fadeInEffect",
                textclass: "content2 centertext",
                textdata: data.string.p4text10
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "back",
                    imgclass: "relativecls img1",
                    imgid: 'p109Img',
                    imgsrc: ""
                }
            ]
        }]
    },
    //slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"s7title centeralign",
                textclass: "content2 centertext",
                textdata: data.string.p4text11
            },
            {
                splitintofractionsflag: true,
                textdiv:"s7title1 centeralign",
                textclass: "content2 centertext",
                textdata: data.string.p4text12
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "back3",
                    imgclass: "relativecls img1",
                    imgid: 'twopearImg',
                    imgsrc: ""
                }
            ]
        }]
    },
    //slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            // {
            //     textdiv:"s7title centeralign",
            //     textclass: "content2 centertext",
            //     textdata: data.string.p4text13
            // },
            {
                splitintofractionsflag: true,
                textdiv:"s7title1 centeralign",
                textclass: "content centertext",
                textdata: data.string.p4text13
            },
            {
                splitintofractionsflag: true,
                textdiv:"frac1 option centeralign",
                spandata: data.string.p2frac7,
                additionalspanclass:"title",
            },
            {
                splitintofractionsflag: true,
                textdiv:"frac2 option centeralign",
                spandata: data.string.p2frac8,
                additionalspanclass:"title",
                ans:"correct"
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "pear1",
                    imgclass: "relativecls img1",
                    imgid: 'pear1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "pear2",
                    imgclass: "relativecls img2",
                    imgid: 'pear2Img',
                    imgsrc: ""
                }
            ]
        }]
    },
    //slide 9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"s8title",
                textclass: "content2 centertext",
                textdata: data.string.p4text14
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "back",
                    imgclass: "relativecls img1",
                    imgid: 'p113Img',
                    imgsrc: ""
                }
            ]
        }]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var time;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "coverpageImg", src: imgpath1 + "p103.png", type: createjs.AbstractLoader.IMAGE},
            {id: "boxImg", src: imgpath + "box.png", type: createjs.AbstractLoader.IMAGE},
            {id: "divisionImg", src: imgpath1 + "p104.png", type: createjs.AbstractLoader.IMAGE},
            {id: "p105Img", src: imgpath1 + "p105.png", type: createjs.AbstractLoader.IMAGE},
            {id: "p106Img", src: imgpath1 + "p106.png", type: createjs.AbstractLoader.IMAGE},
            {id: "p107Img", src: imgpath1 + "p107.png", type: createjs.AbstractLoader.IMAGE},
            {id: "p108Img", src: imgpath1 + "p108.png", type: createjs.AbstractLoader.IMAGE},
            {id: "p109Img", src: imgpath1 + "p109.png", type: createjs.AbstractLoader.IMAGE},
            {id: "twopearImg", src: imgpath1 + "twopear.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pear1Img", src: imgpath1 + "pear01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pear2Img", src: imgpath1 + "pear02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "p113Img", src: imgpath1 + "p113.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset + "s4_p1.ogg"},
            {id: "sound_1", src: soundAsset + "s4_p2.ogg"},
            {id: "sound_2", src: soundAsset + "s4_p3.ogg"},
            {id: "sound_3", src: soundAsset + "s4_p4.ogg"},
            {id: "sound_4", src: soundAsset + "s4_p5.ogg"},
            {id: "sound_5", src: soundAsset + "s4_p6.ogg"},
            {id: "sound_6", src: soundAsset + "s4_p7.ogg"},
            {id: "sound_7", src: soundAsset + "s4_p8.ogg"},
            {id: "sound_8", src: soundAsset + "s4_p9.ogg"},
            {id: "sound_9", src: soundAsset + "s4_p10.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("fractioncontent", $("#fractioncontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        splitintofractions($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 1:
                $(".maintitle1").hide().delay(1000).fadeIn(1000);
                $(".maintitle2").hide().delay(2000).fadeIn(1000);
                $(".hide1").hide().delay(3000).fadeIn(1000);
                sound_player("sound_"+countNext);
                break;

            case 7:
                $(".bottom").css({
                    "border-top": "0.1em solid #8773b2"
                });
              sound_player("sound_"+countNext);
                break;
            case 8:
            sound_nav("sound_"+countNext);
                $(".s7title1").css("bottom","73%");
            $(".bottom").css({
                "border-top": "0.1em solid white"
            });
            $(".option").hover(function(){
                $(this).find(".bottom").css({
                    "border-top": "0.1em solid black"
                });
            },function(){
                $(".bottom").css({
                    "border-top": "0.1em solid white"
                });
            });
            checkans("correctwrongimg");
            break;
            default:
              sound_player("sound_"+countNext);
                break;
        }
    }

  function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, nextBtn){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(0);
		});
	}
  function sound_nav(sound_id, nextBtn){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}



    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }


    function checkans(correctwrongimg1){
        $(".option").on("click",function () {
            createjs.Sound.stop();
            if($(this).attr("data-answer").toString().trim()=='correct') {
                $(this).addClass("correctans");
                $(".option").addClass("avoid-clicks");
                $(this).append("<img class='"+correctwrongimg1+"' src='images/right.png'/>");
                play_correct_incorrect_sound(1);
                navigationcontroller(countNext, $total_page);
            }
            else{
                $(this).addClass("wrongans avoid-clicks");
                $(this).append("<img class='"+correctwrongimg1+"' src='images/wrong.png'/>");
                play_correct_incorrect_sound(0);
            }
        });
    }

    function splitintofractions($splitinside){
        typeof $splitinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
        if($splitintofractions.length > 0){
            $.each($splitintofractions, function(index, value){
                $this = $(this);
                var tobesplitfraction = $this.html();
                if($this.hasClass('fraction')){
                    tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
                    tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
                }else{
                    tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
                    tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
                }


                tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
                $this.html(tobesplitfraction);
            });
        }
    }

});

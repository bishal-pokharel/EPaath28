var imgpath = $ref + "/images/";
var imgpath1 = $ref + "/images/arrows/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "coverback",
        textblock: [
            {
                textdiv:"chtitle centeralign",
                textclass: "chapter centertext",
                textdata: data.string.chapter
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'colorblack',
                textdiv:"chtitle1",
                textclass: "newfont centertext",
                textdata: data.string.chapterex
            },
            {
                textdiv:"chtitle2",
                textclass: "newfont centertext",
                textdata: data.string.chapterex1
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls img1",
                    imgid: 'covermainImg',
                    imgsrc: ""
                },

            ]
        }]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass: "objdiv",
        uppertextblock: [
            {
                textclass: "title centertext fadeInEffect",
                textdata: data.string.p1text1
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls img1",
                    imgid: 'coverpageImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"lookupdiv1 hide2 fadeInEffect",
                textclass: "content1 centertext",
                textdata: data.string.p1text2
            },
            {
                textdiv:"lookupdiv2 hide2 fadeInEffect",
                textclass: "content1 centertext",
                textdata: data.string.p1text3
            },
            {
                textdiv:"lookupdiv3 hide1",
                textclass: "content centertext",
                textdata: data.string.p1text4
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "overview panningEffect ",
                    imgclass: "relativecls img1",
                    imgid: 'lookupImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    // slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"pickdiv1 hide1",
                textclass: "content1 centertext",
                textdata: data.string.p1text5
            },
            {
                textdiv:"pickdiv2 hide1",
                textclass: "content1 centertext",
                textdata: data.string.p1text6
            },
            {
                textdiv:"pickdiv3 hide1",
                textclass: "content1 centertext",
                textdata: data.string.p1text7
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "pickup",
                    imgclass: "relativecls img1",
                    imgid: 'pickupImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "pickup1 hide1",
                    imgclass: "relativecls img2",
                    imgid: 'hitImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"saddiv fadeInEffect",
                textclass: "content1 centertext",
                textdata: data.string.p1text8
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "sad",
                    imgclass: "relativecls img1",
                    imgid: 'sadImg',
                    imgsrc: ""
                },
            ]
        }],
    },

    // slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"monkeydiv fadeInEffect",
                textclass: "content1 centertext",
                textdata: data.string.p1text9
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "monkey",
                    imgclass: "relativecls img1",
                    imgid: 'monkeyImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"introdiv fadeInEffect",
                textclass: "content1 centertext",
                textdata: data.string.p1text10
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "monkey",
                    imgclass: "relativecls img1",
                    imgid: 'introImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"peardiv slideL",
                textclass: "content1 centertext",
                textdata: data.string.p1text11
            },
            {
                textdiv:"peardiv1 fadeInEffect",
                textclass: "content1 centertext",
                textdata: data.string.p1text12
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "pearcut",
                    imgclass: "relativecls img1",
                    imgid: 'pearreverseImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "divided",
                    imgclass: "relativecls img2",
                    imgid: 'dividedImg',
                    imgsrc: ""
                }
            ]
        }],
    },
    //slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"dividedtext1 fadeInEffect",
                textclass: "content1 centertext",
                textdata: data.string.p1text13
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "divpear",
                    imgclass: "relativecls img1",
                    imgid: 'dividedImg',
                    imgsrc: ""
                }
            ]
        }],
    },

    //slide 9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"div1",
                textclass: "content1 centertext",
                textdata: data.string.revise
            },
            {
                textdiv:"div2 hide2",
                textclass: "content1 centertext",
                textdata: data.string.qq1
            },
            {
                textdiv:"div2 hide1",
                textclass: "content1 centertext",
                textdata: data.string.q1ans
            },
            {
                textdiv:"div3 hide2",
                textclass: "content1 centertext",
                textdata: data.string.clickmsg
            },
            {
                textdiv:"div3 hide1",
                textclass: "content1 centertext",
                textdata: data.string.q1ans1
            },
            {
                textdiv:"bottomemptydiv",
                textclass: "",
                textdata: ""
            },
            {
                textdiv:"option opt1",
                textclass: "chapter centertext",
                textdata: data.string.seven
            },
            {
                textdiv:"option opt2",
                textclass: "chapter centertext",
                textdata: data.string.three
            },
            {
                textdiv:"option opt3",
                textclass: "chapter centertext",
                textdata: data.string.eight,
                ans:"correct"
            },
            {
                textdiv:"option opt4",
                textclass: "chapter centertext",
                textdata: data.string.fifteen
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "peartukra",
                    imgclass: "relativecls img1",
                    imgid: 'peartukraImg',
                    imgsrc: ""
                },
                {

                    imgdiv: "peartukra1",
                    imgclass: "relativecls img2",
                    imgid: 'peartukraImg',
                    imgsrc: ""
                }
            ]
        }],
    },

    //slide 10
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"div1",
                textclass: "content1 centertext",
                textdata: data.string.revise
            },
            {
                textdiv:"div2",
                textclass: "content1 centertext",
                textdata: data.string.qq2
            },
            {
                textdiv:"div3 hide2",
                textclass: "content1 centertext",
                textdata: data.string.clickmsg
            },
            {
                textdiv:"div3 hide1",
                textclass: "content1 centertext",
                textdata: data.string.q2ans1
            },
            {
                textdiv:"bottomemptydiv",
                textclass: "",
                textdata: ""
            },
            {
                textdiv:"option opt1",
                textclass: "chapter centertext",
                textdata: data.string.seven
            },
            {
                textdiv:"option opt2",
                textclass: "chapter centertext",
                textdata: data.string.two,
                ans:"correct"
            },
            {
                textdiv:"option opt3",
                textclass: "chapter centertext",
                textdata: data.string.nine,
            },
            {
                textdiv:"option opt4",
                textclass: "chapter centertext",
                textdata: data.string.fifteen
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "pearektukra",
                    imgclass: "relativecls img1",
                    imgid: 'pearektukraImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "pearektukra1",
                    imgclass: "relativecls img2",
                    imgid: 'pearektukraImg',
                    imgsrc: ""
                }
            ]
        }],
    },
    // slide11
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        textblock: [
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'colortext',
                textdiv:"t1 centeralign fadeInEffect",
                textclass: "content1 centertext",
                textdata: data.string.p1text14
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'colortext1',
                textdiv:"t2 centeralign hide1 ",
                textclass: "content1 centertext",
                textdata: data.string.p1text15
            },
            {
                textdiv:"t3 centeralign hide1",
                textclass: "content1 centertext",
                textdata: data.string.p1text16
            },
            {
                textdiv:"t4 centeralign hide1 slidedown1 secondtext",
                textclass: "chapter centertext",
                textdata: data.string.two
            },
            {
                textdiv:"t5 centeralign hide1 slidedown firsttext",
                textclass: "chapter centertext",
                textdata: data.string.eight
            },
            {
                textdiv:"line centeralign hide1",
                textclass: "content centertext",
                textdata: ""
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "squirrel",
                    imgclass: "relativecls img1",
                    imgid: 'squirrelImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "monkeystand",
                    imgclass: "relativecls img2",
                    imgid: 'monkeystandImg',
                    imgsrc: ""
                }
            ]
        }],
    },
    //SLIDE 12
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        textblock: [
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'colortext1',
                textdiv:"maintop centeralign fadeInEffect",
                textclass: "content1 centertext",
                textdata: data.string.p1text17
            },
            {
                textdiv:"text1 centeralign hide1",
                textclass: "chapter centertext",
                textdata: data.string.two
            },
            {
                textdiv:"text2 centeralign hide1",
                textclass: "chapter centertext",
                textdata: data.string.eight
            },
            {
                textdiv:"line1 centeralign hide1",
                textclass: "content centertext",
                textdata: ""
            },
            // {
            //     textdiv:"emptytwopear hide1",
            //     textclass: "content centertext",
            //     textdata: ""
            // }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "squirrelend",
                    imgclass: "relativecls img1",
                    imgid: 'squirrelImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "monkeystandend",
                    imgclass: "relativecls img2",
                    imgid: 'monkeystandImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "stackpear s1 hide1",
                    imgclass: "relativecls img3",
                    imgid: 'stackpearImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "stackpear hide2",
                    imgclass: "relativecls img4",
                    imgid: 'stackpeartwoImg',
                    imgsrc: ""
                }
            ]
        }],
    },
    //slide 13
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        textblock: [
            {
                textdiv:"p1 centeralign fadeInEffect",
                textclass: "content1 centertext",
                textdata: data.string.p1text18
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'colortext1',
                textdiv:"p2 centeralign fadeInEffect",
                textclass: "content1 centertext",
                textdata: data.string.p1text19
            },
            {
                textdiv:"p3 centeralign hide1",
                textclass: "content1 centertext",
                textdata: data.string.p1text20
            },
            {
                textdiv:"p4 centeralign hide1 colortext1 fadeInEffect",
                textclass: "content1 centertext",
                textdata: data.string.p1text21
            },
            {
                textdiv:"text1 centeralign hide1",
                textclass: "chapter centertext",
                textdata: data.string.two
            },
            {
                textdiv:"text2 centeralign hide1",
                textclass: "chapter centertext",
                textdata: data.string.eight
            },
            {
                textdiv:"line1 centeralign hide1",
                textclass: "content centertext",
                textdata: ""
            },
            // {
            //     textdiv:"emptytwopear1 hide1",
            //     textclass: "content centertext",
            //     textdata: ""
            // },
            // {
            //     textdiv:"emptyeightpear hide1",
            //     textclass: "content centertext",
            //     textdata: ""
            // }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "squirrelend",
                    imgclass: "relativecls img1",
                    imgid: 'squirrelImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "stackpear1 s1 hide1",
                    imgclass: "relativecls img3",
                    imgid: 'stackpearImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "stackpear1 hide2",
                    imgclass: "relativecls img4",
                    imgid: 'stackpeartwoImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "stackpear1 hide3",
                    imgclass: "relativecls img5",
                    imgid: 'stackpeareightImg',
                    imgsrc: ""
                }
            ]
        }],
    },
    //slide 14
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        textblock: [
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'colortext2',
                textdiv:"denonum centeralign hide1",
                textclass: "content1 centertext",
                textdata: data.string.p1text22
            },
            {
                textdiv:"td1 centeralign hide1 hide1",
                textclass: "chapter centertext shakingEffect",
                textdata: data.string.eight
            },
            {
                textdiv:"td2 centeralign hide1 hide1",
                textclass: "chapter centertext",
                textdata: data.string.two
            },
            {
                textdiv:"denominator colortext1 hide1",
                textclass: "content centertext",
                textdata: data.string.denominator
            },
            // {
            //     textdiv:"emptytwopear2 fadeInEffect",
            //     textclass: "content centertext",
            //     textdata: ""
            // },
            {
                textdiv:"line2 centeralign hide1",
                textclass: "content centertext",
                textdata: ""
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "stackpear2 fadeInEffect",
                    imgclass: "relativecls img1",
                    imgid: 'stackpeartwoImg',
                    imgsrc: ""
                }
            ]
        }],
    },
    //slide 15
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        textblock: [
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'colortext2',
                textdiv:"denonum centeralign hide1",
                textclass: "content1 centertext",
                textdata: data.string.p1text23
            },
            {
                textdiv:"td3 centeralign hide1",
                textclass: "chapter centertext ",
                textdata: data.string.eight
            },
            {
                textdiv:"td4 centeralign hide1",
                textclass: "chapter centertext shakingEffect",
                textdata: data.string.two
            },
            {
                textdiv:"denominator colortext1 hide1",
                textclass: "content centertext",
                textdata: data.string.denominator
            },
            {
                textdiv:"numerator colortext1 hide1",
                textclass: "content centertext",
                textdata: data.string.numerator
            },
            // {
            //     textdiv:"emptytwopear2 fadeInEffect",
            //     textclass: "content centertext",
            //     textdata: ""
            // },
            {
                textdiv:"line2 centeralign hide1",
                textclass: "content centertext",
                textdata: ""
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "stackpear2 fadeInEffect",
                    imgclass: "relativecls img1",
                    imgid: 'stackpeartwoImg',
                    imgsrc: ""
                }
            ]
        }],
    },
    //slide 16
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        textblock: [
            {
                textdiv:"textdiv centeralign fadeInEffect",
                textclass: "content1 centertext",
                textdata: data.string.p1text24
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "pearhqgif",
                    imgclass: "relativecls img1",
                    imgid: 'pearhqgifImg',
                    imgsrc: ""
                }
            ]
        }],
    },
    //slide17
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        textblock: [
            {
                textdiv:"s17_1 centeralign fadeInEffect",
                textclass: "content0 centertext",
                textdata: data.string.p1text25
            },
            {
                textdiv:"s17_2 centeralign fadeInEffect",
                textclass: "content1 centertext",
                textdata: data.string.p1text26
            },
            {
                textdiv:"s17_3 centeralign hide2 ques",
                textclass: "content1 centertext",
                textdata: data.string.qq3
            },
            {
                textdiv:"s17_3 centeralign hide1",
                textclass: "content1 centertext",
                textdata: data.string.q3ans
            },
            {
                textdiv:"option option1",
                textclass: "chapter centertext",
                textdata: data.string.three,
                ans:"correct"
            },
            {
                textdiv:"option option2",
                textclass: "chapter centertext",
                textdata: data.string.two
            },
            {
                textdiv:"option option3",
                textclass: "chapter centertext",
                textdata: data.string.nine
            },
            {
                textdiv:"option option4",
                textclass: "chapter centertext",
                textdata: data.string.fifteen
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "eightpear1",
                    imgclass: "relativecls img1",
                    imgid: 'greypearImg',
                    imgsrc: ""
                },
                // {
                //     imgdiv: "eightpear2",
                //     imgclass: "relativecls img2",
                //     imgid: 'peartukraImg',
                //     imgsrc: ""
                // },
                {
                    imgdiv: "monkeystandend",
                    imgclass: "relativecls img3",
                    imgid: 'monkeystandImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "rhinomore",
                    imgclass: "relativecls img4",
                    imgid: 'rhinomoreImg',
                    imgsrc: ""
                }
            ]
        }],
    },
    //slide 18
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        textblock: [
            {
                textdiv:"s18_1 centeralign hide1",
                textclass: "content1 centertext",
                textdata: data.string.p1text27
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'colortext3',
                textdiv:"s18_2 centeralign hide1",
                textclass: "content1 centertext",
                textdata: data.string.p1text28
            },
            {
                textdiv:"s18_3 centeralign",
                textclass: "chapter centertext",
                textdata: data.string.eight
            },
            {
                textdiv:"s18_4 centeralign",
                textclass: "chapter centertext",
                textdata: data.string.three
            },
            {
                textdiv:"line3 centeralign hide1",
                textclass: "content centertext",
                textdata: ""
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "greypear",
                    imgclass: "relativecls img1",
                    imgid: 'nongreypearImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "greypear1 hide1",
                    imgclass: "relativecls img2",
                    imgid: 'greypearImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "monkeystandend",
                    imgclass: "relativecls img3",
                    imgid: 'monkeystandImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "rhinomore",
                    imgclass: "relativecls img4",
                    imgid: 'rhinomoreImg',
                    imgsrc: ""
                }
            ]
        }],
    },
    //slide 19
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        textblock: [
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'colortext1',
                textdiv:"s19_1 centeralign fadeInEffect",
                textclass: "content1 centertext",
                textdata: data.string.p1text29
            },
            {
                textdiv:"s19_2 centeralign hide1",
                textclass: "chapter centertext",
                textdata: data.string.eight
            },
            {
                textdiv:"s19_3 centeralign hide1",
                textclass: "chapter centertext",
                textdata: data.string.three
            },
            {
                textdiv:"line4 centeralign hide1",
                textclass: "content centertext",
                textdata: ""
            },
            // {
            //     textdiv:"empty1",
            //     textclass: "content centertext",
            //     textdata: ""
            // },
            // {
            //     textdiv:"empty2",
            //     textclass: "content centertext",
            //     textdata: ""
            // }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "stack",
                    imgclass: "relativecls img1",
                    imgid: 'stackpearImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "monkeystandend",
                    imgclass: "relativecls img2",
                    imgid: 'monkeystandImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "rhinomore",
                    imgclass: "relativecls img3",
                    imgid: 'rhinomoreImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "stack hide2",
                    imgclass: "relativecls img4",
                    imgid: 'stackthreepearImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide20
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        textblock: [
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'colortext1',
                textdiv:"s20_1 centeralign fadeInEffect",
                textclass: "content1 centertext",
                textdata: data.string.p1text30
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'colortext1',
                textdiv:"s20_2 centeralign fadeInEffect",
                textclass: "content1 centertext",
                textdata: data.string.p1text31
            },
            {
                textdiv:"s19_2 centeralign hide1",
                textclass: "chapter centertext",
                textdata: data.string.eight
            },
            {
                textdiv:"s19_3 centeralign hide1",
                textclass: "chapter centertext",
                textdata: data.string.three
            },
            {
                textdiv:"line4 centeralign hide1",
                textclass: "content centertext",
                textdata: ""
            },
            // {
            //     textdiv:"empty1",
            //     textclass: "content centertext",
            //     textdata: ""
            // },
            // {
            //     textdiv:"empty2",
            //     textclass: "content centertext",
            //     textdata: ""
            // }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "stack",
                    imgclass: "relativecls img1",
                    imgid: 'stackpearImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "monkeystandend",
                    imgclass: "relativecls img2",
                    imgid: 'monkeystandImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "rhinomore",
                    imgclass: "relativecls img3",
                    imgid: 'rhinomoreImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "stack hide2",
                    imgclass: "relativecls img4",
                    imgid: 'stackthreepearImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide 21
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        textblock: [
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'underlinetext',
                textdiv:"s21_1 centeralign hide1 hide2",
                textclass: "content1 centertext",
                textdata: data.string.p1text32
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'underlinetext',
                textdiv:"s21_2 centeralign hide1",
                textclass: "content2 centertext",
                textdata: data.string.p1text33
            },
            {
                textdiv:"s19_2 centeralign hide1 clickable",
                textclass: "chapter centertext",
                textdata: data.string.eight
            },
            {
                textdiv:"s19_3 centeralign hide1 clickable",
                textclass: "chapter centertext",
                textdata: data.string.three
            },
            {
                textdiv:"line4 centeralign hide1",
                textclass: "content centertext",
                textdata: ""
            },
            // {
            //     textdiv:"empty1",
            //     textclass: "content centertext",
            //     textdata: ""
            // },
            // {
            //     textdiv:"empty2",
            //     textclass: "content centertext",
            //     textdata: ""
            // },
            {
                textdiv:"denominator1 hide1",
                textclass: "content centertext",
                textdata: data.string.denominator
            },
            {
                textdiv:"numerator1 hide1",
                textclass: "content centertext",
                textdata: data.string.numerator
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "stack",
                    imgclass: "relativecls img1",
                    imgid: 'stackthreepearImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "monkeystandend1",
                    imgclass: "relativecls img2",
                    imgid: 'monkeystandImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial hide1 hide2",
                    imgclass: "relativecls img3",
                    imgid: 'dialImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial1 hide1",
                    imgclass: "relativecls img4",
                    imgid: 'dial1Img',
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide 22
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        textblock: [
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'underlinetext',
                textdiv:"s21_1 centeralign hide1 hide2",
                textclass: "content1 centertext",
                textdata: data.string.p1text34
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'underlinetext',
                textdiv:"s21_2 centeralign hide1",
                textclass: "content2 centertext",
                textdata: data.string.p1text35
            },
            {
                textdiv:"s19_2 centeralign hide1 clickable",
                textclass: "chapter centertext",
                textdata: data.string.eight
            },
            {
                textdiv:"s19_3 centeralign hide1 clickable",
                textclass: "chapter centertext",
                textdata: data.string.three
            },
            {
                textdiv:"line4 centeralign hide1",
                textclass: "content centertext",
                textdata: ""
            },
            // {
            //     textdiv:"empty1",
            //     textclass: "content centertext",
            //     textdata: ""
            // },
            // {
            //     textdiv:"empty2",
            //     textclass: "content centertext",
            //     textdata: ""
            // },
            {
                textdiv:"denominator1 hide1",
                textclass: "content centertext",
                textdata: data.string.denominator
            },
            {
                textdiv:"numerator1 hide1",
                textclass: "content centertext",
                textdata: data.string.numerator
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "stack",
                    imgclass: "relativecls img1",
                    imgid: 'stackthreepearImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "monkeystandend1",
                    imgclass: "relativecls img2",
                    imgid: 'monkeystandImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial hide1 hide2",
                    imgclass: "relativecls img3",
                    imgid: 'dialImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial1 hide1",
                    imgclass: "relativecls img4",
                    imgid: 'dial1Img',
                    imgsrc: ""
                },
            ]
        }],
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var time;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "covermainImg", src: imgpath + "coverr-main.png", type: createjs.AbstractLoader.IMAGE},
            {id: "coverpageImg", src: imgpath + "cover-page.png", type: createjs.AbstractLoader.IMAGE},
            {id: "lookupImg", src: imgpath + "overview.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pickupImg", src: imgpath + "pick-up-the-pear.png", type: createjs.AbstractLoader.IMAGE},
            {id: "hitImg", src: imgpath + "hit.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sadImg", src: imgpath + "sad.png", type: createjs.AbstractLoader.IMAGE},
            {id: "monkeyImg", src: imgpath + "monkey-enters.png", type: createjs.AbstractLoader.IMAGE},
            {id: "introImg", src: imgpath + "introduction.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pearreverseImg", src: imgpath + "animation/pear-reverse.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "dividedImg", src: imgpath + "divided.png", type: createjs.AbstractLoader.IMAGE},
            {id: "peartukraImg", src: imgpath + "tukra.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pearektukraImg", src: imgpath + "ek-tukraa.png", type: createjs.AbstractLoader.IMAGE},
            {id: "squirrelImg", src: imgpath + "squirrel.png", type: createjs.AbstractLoader.IMAGE},
            {id: "monkeystandImg", src: imgpath + "monkey.png", type: createjs.AbstractLoader.IMAGE},
            {id: "stackpearImg", src: imgpath  + "pearstack/eightpear01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "stackpeartwoImg", src: imgpath  + "pearstack/eightpear02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "stackpeareightImg", src: imgpath  + "pearstack/eightpear04.png", type: createjs.AbstractLoader.IMAGE},
            {id: "stackpearthreeImg", src: imgpath  + "pearstack/eightpear03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pearhqgifImg", src: imgpath + "animation/PearHQ_01.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "rhinomoreImg", src: imgpath + "rhino-more.png", type: createjs.AbstractLoader.IMAGE},
            {id: "nongreypearImg", src: imgpath +"slice8.png", type: createjs.AbstractLoader.IMAGE},
            {id: "greypearImg", src: imgpath +"slice8-grayscale.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialImg", src: imgpath +"speech-bubble-2.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dial1Img", src: imgpath +"speech-bubble.png", type: createjs.AbstractLoader.IMAGE},
            {id: "stackthreepearImg", src: imgpath +"pearstack/threepear.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset + "s1_p1.ogg"},
            {id: "sound_1", src: soundAsset + "s1_p2.ogg"},
            {id: "sound_2a", src: soundAsset + "s1_p3_1.ogg"},
            {id: "sound_2b", src: soundAsset + "s1_p3_2.ogg"},
            {id: "sound_3", src: soundAsset + "s1_p4.ogg"},
            {id: "sound_4", src: soundAsset + "s1_p5.ogg"},
            {id: "sound_5", src: soundAsset + "s1_p6.ogg"},
            {id: "sound_6", src: soundAsset + "s1_p7.ogg"},
            {id: "sound_7", src: soundAsset + "s1_p8.ogg"},
            {id: "sound_8", src: soundAsset + "s1_p9.ogg"},
            {id: "sound_9", src: soundAsset + "s1_p10.ogg"},
            {id: "sound_9b", src: soundAsset + "s1_p10_2.ogg"},
            {id: "sound_10", src: soundAsset + "s1_p11_1.ogg"},
            {id: "sound_10b", src: soundAsset + "s1_p11_2.ogg"},
            {id: "sound_11", src: soundAsset + "s1_p12.ogg"},
            {id: "sound_12", src: soundAsset + "s1_p13.ogg"},
            {id: "sound_13", src: soundAsset + "s1_p14.ogg"},
            {id: "sound_14", src: soundAsset + "s1_p15.ogg"},
            {id: "sound_15", src: soundAsset + "s1_p16.ogg"},
            {id: "sound_16", src: soundAsset + "s1_p17.ogg"},
            {id: "sound_17", src: soundAsset + "s1_p18_1.ogg"},
            {id: "sound_17b", src: soundAsset + "s1_p18_2.ogg"},
            {id: "sound_18", src: soundAsset + "s1_p19.ogg"},
            {id: "sound_19", src: soundAsset + "s1_p20.ogg"},
            {id: "sound_20", src: soundAsset + "s1_p21.ogg"},
            {id: "sound_21", src: soundAsset + "s1_p22_1.ogg"},
            {id: "sound_21b", src: soundAsset + "s1_p22_2.ogg"},
            {id: "sound_22", src: soundAsset + "s1_p23_1.ogg"},
            {id: "sound_22b", src: soundAsset + "s1_p23_2.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 2:
                $(".hide1").hide();
                // $(".hide2").delay(10000).fadeOut(1000);// need to uncomment
                $(".hide1").delay(18000).fadeIn(1000);
                  sound_nav("sound_2a");
                  setTimeout(function(){
                      sound_player("sound_2b");
                  },18000);
                break;
            case 3:
                $(".hide1").hide();
                $(".pickdiv1").fadeIn(1000);
                $(".pickdiv2").delay(2000).fadeIn(1000);
                $(".pickdiv3").delay(5000).fadeIn(1000);
                $(".pickup1").delay(4000).fadeIn(1000);
                sound_player("sound_"+(countNext));
                break;
            case 7:
                $(".divided,.peardiv1").hide();
                $(".pearcut").delay(5000).fadeOut(1000);
                $(".divided").delay(5000).fadeIn(1000);
                $(".peardiv1").delay(5500).fadeIn(1000);
                sound_player("sound_"+(countNext));
                break;
            case 9:
            case 10:
            sound_nav("sound_"+(countNext));
                var optionClass = ["option opt1","option opt2","option opt3","option opt4"];
                shufflehint(optionClass);
                checkans("correctwrongimg");
                break;
            case 11:
                $(".hide1").hide();
                $(".t2").delay(2000).fadeIn(1000);
                $(".t3").delay(3000).fadeIn(1000);
                fractiondis(  $(".secondtext"), $(".line"),$(".firsttext"),6000,8000,10000)
                sound_player("sound_"+(countNext));
                break;
            case 12:
                $(".hide1,.hide2").hide();
                $(".s1").delay(1000).fadeIn(1000);
                $(".hide2").delay(3000).fadeIn(1000);
                fractiondis(  $(".text1"), $(".line1"),$(".text2"),3000,4000,5000)
                sound_player("sound_"+(countNext));
                break;
            case 13:
                $(".hide1,.hide2,.hide3").hide();
                $(".s1").delay(1500).fadeIn(1000);
                $(".hide2").delay(5000).fadeIn(1000);
                // $(".hide3").delay(7000).fadeIn(1000);
                $(".p3").delay(4000).fadeIn(1000);
                $(".p4").delay(5000).fadeIn(1000);
                fractiondis(  $(".text1"), $(".line1"),$(".text2"),3000,4000,5000)
                sound_player("sound_"+(countNext));
                break;
            case 14:
                $(".hide1").hide();
                fractiondis(  $(".td2"), $(".line2"),$(".td1"),2000,3000,4000);
                $(".denominator").delay(7000).fadeIn(1000);
                $(".denonum").delay(5000).fadeIn(1000);
                setTimeout(function(){
                  sound_player("sound_"+(countNext));
                },5000);
                break;
            case 15:
                $(".hide1").hide();
                fractiondis(  $(".td4"), $(".line2"),$(".td3"),2000,3000,4000);
                $(".denominator").delay(5000).fadeIn(1000).css("color","white");
                $(".numerator").delay(7000).fadeIn(1000);
                $(".denonum").delay(6000).fadeIn(1000);
                setTimeout(function(){
                  sound_player("sound_"+(countNext));
                },6000);
                break;
            case 17:
                $(".hide1,.ques,.option,.rhinomore").hide();
                var optionClass = ["option option1","option option2","option option3","option option4"];
                $(".ques,.rhinomore").delay(2000).fadeIn(1000);
                $(".option").delay(3000).fadeIn(1000);
                shufflehint(optionClass);
                checkans("correctwrongimg1");
                sound_nav("sound_"+(countNext));
                break;
            case 18:
                $(".hide1").hide();
                $(".greypear").delay(1000).animate({
                    'top': '6%'
                },2000);
                $(".greypear").delay(2000).fadeOut(1000);
                $(".s18_1").delay(2000).fadeIn(1000);
                $(".greypear1").delay(5000).fadeIn(1000);
                $(".s18_2").delay(5000).fadeIn(1000);
                $(".s18_3").delay(6000).animate({"right": "10%",
                "top": '19%',"opacity":"1"},2000);
                $(".line3").delay(8000).fadeIn(1000);
                $(".s18_4").delay(9000).animate({"right": "10%",
                    "top": '8%',"opacity":"1"},2000);
                setTimeout(function(){
                  sound_player("sound_"+(countNext));
                },2000);
                break;
            case 19:
            case 20:
                $(".s19_2,.s19_3").css("background-color","transparent");
                $(".hide1,.hide2").hide();
                $(".hide2").delay(1000).fadeIn(1000);
                fractiondis(  $(".s19_3"), $(".line4"),$(".s19_2"),2000,3000,4000)
                sound_player("sound_"+(countNext));
                break;
            case 21:
            setTimeout(function(){
              sound_nav("sound_"+(countNext));
            },5000);
                $(".hide1").hide();
                fractiondis(  $(".s19_3"), $(".line4"),$(".s19_2"),2000,3000,4000)
                $(".s21_1,.dial").delay(5000).fadeIn(1000);
                $(".s19_2").click(function () {
                    $(this).addClass("correctans avoid-clicks");
                    $(".s19_3").addClass("avoid-clicks");
                    play_correct_incorrect_sound(1);
                    $(".denominator1").fadeIn(1000);
                  $(".hide2").fadeOut(1000);
                  $(".dial1,.s21_2").fadeIn(1000);
                  setTimeout(function(){
                    sound_player("sound_21b");
                  },800);
                });
                $(".s19_3").click(function () {
                    $(this).addClass("wrongans avoid-clicks");
                    play_correct_incorrect_sound(0)
                });
                break;
            case 22:
            setTimeout(function(){
              sound_nav("sound_"+(countNext));
            },5000);
                $(".hide1").hide();
                fractiondis(  $(".s19_3"), $(".line4"),$(".s19_2"),2000,3000,4000)
                $(".s21_1,.dial").delay(5000).fadeIn(1000);
                $(".s19_3").click(function () {
                    $(this).addClass("correctans avoid-clicks");
                    $(".s19_2").addClass("avoid-clicks");
                    play_correct_incorrect_sound(1);
                    $(".denominator1").fadeIn(1500);
                    $(".numerator1").fadeIn(1000);
                    $(".hide2").fadeOut(1000);
                    $(".dial1,.s21_2").fadeIn(1000);
                    setTimeout(function(){
                      sound_player("sound_22b");
                    },800);
                  });
                $(".s19_2").click(function () {
                    $(this).addClass("wrongans avoid-clicks");
                    play_correct_incorrect_sound(0)
                });
                break;
                default:
                sound_player("sound_"+(countNext));
                break;
        }
    }

  function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, nextBtn){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(0);
		});
	}
  function sound_nav(sound_id, nextBtn){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

    function shufflehint(optionclass) {
        $(".hide1").hide();
        var optdiv = $(".coverboardfull");

        for (var i = optdiv.find(".option").length; i >= 0; i--) {
            optdiv.append(optdiv.find(".option").eq(Math.random() * i | 0));
        }
        optdiv.find(".option").removeClass().addClass("current");
        optdiv.find(".current").each(function (index) {
            $(this).addClass(optionclass[index]);
            $(this).addClass($(this).attr("data-answer"));
        });
    }

    function checkans(correctcss){
        $(".option").on("click",function () {
            createjs.Sound.stop();
            if($(this).attr("data-answer").toString().trim()=='correct') {
                $(this).addClass("correctans");
                $(".option").addClass("avoid-clicks");
                $(this).append("<img class='"+correctcss+"' src='images/right.png'/>");
                play_correct_incorrect_sound(1);
                $(".hide2").fadeOut(1000);
                $(".hide1").fadeIn(1000);
                $(".div3").css({"color":"#38761d"});
                setTimeout(function(){
                  if(countNext==9){
                    sound_player("sound_9b");
                  }else if(countNext==10){
                    sound_player("sound_10b");
                  }else if(countNext==17){
                      sound_player("sound_17b");
                  }
                },1000);
            }
            else{
                $(this).addClass("wrongans avoid-clicks");
                $(this).append("<img class='"+correctcss+"' src='images/wrong.png'/>");
                play_correct_incorrect_sound(0);
            }
        });
    }

    function fractiondis(t1,line,t2,delay1,delay2,delay3){
        t2.delay(delay1).fadeIn(1000);
        line.delay(delay2).fadeIn(1000);
        t1.delay(delay3).fadeIn(1000);
    }

});

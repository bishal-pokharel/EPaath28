var imgpath = $ref + "/images/";
var imgpath1 = $ref + "/images/page1/";
var soundAsset = $ref+"/sounds/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"diytext centeralign",
                textclass: "chapter centertext",
                textdata: data.string.diy
            },
         ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "back",
                    imgclass: "relativecls img1",
                    imgid: 'diyImg',
                    imgsrc: ""
                }
            ]
        }]
    },
    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle centeralign",
                textclass: "content1 centertext",
                textdata: data.string.diy1
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'share',
                textdiv:"maintitle1 centeralign",
                textclass: "content1 centertext",
                textdata: data.string.diy2
            },
            {
                textdiv:"box1",
                textclass: "",
                textdata: ""
            },
            {
                textdiv:"box2",
                textclass: "",
                textdata: ""
            },
            {
                textdiv:"box3",
                textclass: "",
                textdata: ""
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "back",
                    imgclass: "relativecls img1",
                    imgid: 'coverpageImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                splitintofractionsflag: true,
                textdiv:"option opt1 centeralign",
                textclass: "content2",
                textdata: data.string.p2frac1
            },
            {
                splitintofractionsflag: true,
                textdiv:"option opt2 centeralign",
                textclass: "content2",
                textdata: data.string.p2frac1,
                ans:"correct"
            },
            {
                splitintofractionsflag: true,
                textdiv:"option opt3 centeralign",
                textclass: "content2",
                textdata: data.string.p2frac1
            },
            {
                splitintofractionsflag: true,
                textdiv:"option opt4 centeralign",
                textclass: "content2",
                textdata: data.string.p2frac1,
            },
            {
                textdiv:"toptext centeralign",
                textclass:"content centertext ",
                textdata:data.string.diyq1
            },
            {
                textdiv:"lefttext",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"righttext",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"peardiv1",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"peardiv2",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"totalpieces centeralign",
                textclass:"content1 centertext",
                textdata:data.string.totalpieces
            },
            {
                textdiv:"sureyshare centeralign",
                textclass:"content1 centertext",
                textdata:data.string.sureyshare
            },
            {
                textdiv:"ririshare centeralign",
                textclass:"content1 centertext",
                textdata:data.string.ririshare
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "stackofpear",
                    imgclass: "relativecls img1",
                    imgid: 'sixpearsImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "surey",
                    imgclass: "relativecls img2",
                    imgid: 'sureyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "riri",
                    imgclass: "relativecls img3",
                    imgid: 'ririImg',
                    imgsrc: ""
                }
                // {
                //     imgdiv: "chankhey",
                //     imgclass: "relativecls img4",
                //     imgid: 'chankheyImg',
                //     imgsrc: ""
                // }
            ]
        }]
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                splitintofractionsflag: true,
                textdiv:"option opt1 centeralign",
                textclass: "content2",
                textdata: data.string.p2frac1
            },
            {
                splitintofractionsflag: true,
                textdiv:"option opt2 centeralign",
                textclass: "content2",
                textdata: data.string.p2frac1,
            },
            {
                splitintofractionsflag: true,
                textdiv:"option opt3 centeralign",
                textclass: "content2",
                textdata: data.string.p2frac1,
                ans:"correct"
            },
            {
                splitintofractionsflag: true,
                textdiv:"option opt4 centeralign",
                textclass: "content2",
                textdata: data.string.p2frac1,
            },
            {
                textdiv:"toptext centeralign",
                textclass:"content centertext ",
                textdata:data.string.diyq2
            },
            {
                textdiv:"lefttext",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"righttext",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"peardiv1",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"peardiv2",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"totalpieces centeralign",
                textclass:"content1 centertext",
                textdata:data.string.totalpieces
            },
            {
                textdiv:"sureyshare centeralign",
                textclass:"content1 centertext",
                textdata:data.string.sureyshare
            },
            {
                textdiv:"ririshare centeralign",
                textclass:"content1 centertext",
                textdata:data.string.ririshare
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "stackofpear",
                    imgclass: "relativecls img1",
                    imgid: 'sixpearsImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "surey",
                    imgclass: "relativecls img2",
                    imgid: 'sureyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "riri",
                    imgclass: "relativecls img3",
                    imgid: 'ririImg',
                    imgsrc: ""
                }
            ]
        }]
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                splitintofractionsflag: true,
                textdiv:"option opt1 centeralign",
                textclass: "content2",
                textdata: data.string.p2frac1
            },
            {
                splitintofractionsflag: true,
                textdiv:"option opt2 centeralign",
                textclass: "content2",
                textdata: data.string.p2frac1,
                ans:"correct"
            },
            {
                splitintofractionsflag: true,
                textdiv:"option opt3 centeralign",
                textclass: "content2",
                textdata: data.string.p2frac1,
            },
            {
                splitintofractionsflag: true,
                textdiv:"option opt4 centeralign",
                textclass: "content2",
                textdata: data.string.p2frac1,
            },
            {
                textdiv:"toptext centeralign",
                textclass:"content centertext ",
                textdata:data.string.diyq3
            },
            {
                textdiv:"lefttext",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"righttext",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"peardiv1",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"peardiv2",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"totalpieces centeralign",
                textclass:"content1 centertext",
                textdata:data.string.totalpieces
            },
            {
                textdiv:"sureyshare centeralign",
                textclass:"content1 centertext",
                textdata:data.string.sureyshare
            },
            {
                textdiv:"ririshare centeralign",
                textclass:"content1 centertext",
                textdata:data.string.ririshare
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "stackofpear",
                    imgclass: "relativecls img1",
                    imgid: 'sixpearsImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "surey",
                    imgclass: "relativecls img2",
                    imgid: 'sureyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "riri",
                    imgclass: "relativecls img3",
                    imgid: 'ririImg',
                    imgsrc: ""
                }
            ]
        }]
    },
    // slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                splitintofractionsflag: true,
                textdiv:"option opt1 centeralign",
                textclass: "content2",
                textdata: data.string.p2frac1
            },
            {
                splitintofractionsflag: true,
                textdiv:"option opt2 centeralign",
                textclass: "content2",
                textdata: data.string.p2frac1,
                ans:"correct"
            },
            {
                splitintofractionsflag: true,
                textdiv:"option opt3 centeralign",
                textclass: "content2",
                textdata: data.string.p2frac1,
            },
            {
                splitintofractionsflag: true,
                textdiv:"option opt4 centeralign",
                textclass: "content2",
                textdata: data.string.p2frac1,
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'share',
                splitintofractionsflag: true,
                textdiv:"toptext centeralign",
                textclass:"content centertext ",
                textdata:data.string.diyq4
            },
            {
                textdiv:"lefttext",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"righttext",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"peardiv1",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"peardiv2",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"totalpieces centeralign",
                textclass:"content1 centertext",
                textdata:data.string.totalpieces
            },
            {
                textdiv:"sureyshare centeralign",
                textclass:"content1 centertext",
                textdata:data.string.sureyshare
            },
            {
                textdiv:"ririshare centeralign",
                textclass:"content1 centertext",
                textdata:data.string.ririshare
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "stackofpear",
                    imgclass: "relativecls img1",
                    imgid: 'sixpearsImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "surey",
                    imgclass: "relativecls img2",
                    imgid: 'sureyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "riri",
                    imgclass: "relativecls img3",
                    imgid: 'ririImg',
                    imgsrc: ""
                }
            ]
        }]
    },
    //slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {   datahighlightflag : true,
                datahighlightcustomclass : 'share',
                splitintofractionsflag: true,
                textdiv: "option o1 centeralign",
                textclass: "content2",
                textdata: data.string.optionfarc1
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'share',
                splitintofractionsflag: true,
                textdiv: "option o2 centeralign",
                textclass: "content2",
                textdata: data.string.optionfarc2,
                ans: "correct"
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'share',
                splitintofractionsflag: true,
                textdiv: "toptext centeralign",
                textclass: "content centertext ",
                textdata: data.string.diyq5
            },
            {
                textdiv: "lefttext",
                textclass: "",
                textdata: ""
            },
            {
                textdiv: "righttext",
                textclass: "",
                textdata: ""
            },
            {
                textdiv: "peardiv1",
                textclass: "",
                textdata: ""
            },
            {
                textdiv: "peardiv2",
                textclass: "",
                textdata: ""
            },
            {
                textdiv: "totalpieces centeralign",
                textclass: "content1 centertext",
                textdata: data.string.totalpieces
            },
            {
                textdiv: "sureyshare centeralign",
                textclass: "content1 centertext",
                textdata: data.string.sureyshare
            },
            {
                textdiv: "ririshare centeralign",
                textclass: "content1 centertext",
                textdata: data.string.ririshare
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "stackofpear",
                    imgclass: "relativecls img1",
                    imgid: 'sixpearsImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "surey",
                    imgclass: "relativecls img2",
                    imgid: 'sureyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "riri",
                    imgclass: "relativecls img3",
                    imgid: 'ririImg',
                    imgsrc: ""
                },
            ]
        }]
    },
    //slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                splitintofractionsflag: true,
                textdiv: "option option1 centeralign",
                textclass: "content2",
                textdata: data.string.p2frac1
            },
            {
                splitintofractionsflag: true,
                textdiv: "option option2 centeralign",
                textclass: "content2",
                textdata: data.string.p2frac1,
                ans: "correct"
            },
            {
                splitintofractionsflag: true,
                textdiv: "option option3 centeralign",
                textclass: "content2",
                textdata: data.string.p2frac1
            },
            {
                splitintofractionsflag: true,
                textdiv: "option option4 centeralign",
                textclass: "content2",
                textdata: data.string.p2frac1,
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'share',
                splitintofractionsflag: true,
                textdiv: "toptext centeralign",
                textclass: "content centertext ",
                textdata: data.string.diyq6
            },
            {
                textdiv: "lefttext",
                textclass: "",
                textdata: ""
            },
            {
                textdiv: "righttext",
                textclass: "",
                textdata: ""
            },
            {
                textdiv: "peardiv1",
                textclass: "",
                textdata: ""
            },
            {
                textdiv: "peardiv2",
                textclass: "",
                textdata: ""
            },
            {
                textdiv: "totalpieces centeralign",
                textclass: "content1 centertext",
                textdata: data.string.totalpieces
            },
            {
                textdiv: "sureyshare centeralign",
                textclass: "content1 centertext",
                textdata: data.string.sureyshare
            },
            {
                textdiv: "ririshare centeralign",
                textclass: "content1 centertext",
                textdata: data.string.ririshare
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "stackofpear",
                    imgclass: "relativecls img1",
                    imgid: 'sixpearsImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "surey",
                    imgclass: "relativecls img2",
                    imgid: 'sureyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "riri",
                    imgclass: "relativecls img3",
                    imgid: 'ririImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "chankhey",
                    imgclass: "relativecls img4",
                    imgid: 'chankheyImg',
                    imgsrc: ""
                }
            ]
        }]
    },
//    slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                splitintofractionsflag: true,
                textdiv:"option opt1 centeralign",
                textclass: "content2",
                textdata: data.string.p2frac1
            },
            {
                splitintofractionsflag: true,
                textdiv:"option opt2 centeralign",
                textclass: "content2",
                textdata: data.string.p2frac1,
                ans:"correct"
            },
            {
                splitintofractionsflag: true,
                textdiv:"option opt3 centeralign",
                textclass: "content2",
                textdata: data.string.p2frac1
            },
            {
                splitintofractionsflag: true,
                textdiv:"option opt4 centeralign",
                textclass: "content2",
                textdata: data.string.p2frac1,
            },
            {
                textdiv:"toptext centeralign",
                textclass:"content centertext ",
                textdata:data.string.diyq7
            },
            {
                textdiv:"lefttext",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"righttext",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"peardiv1",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"peardiv2",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"totalpieces centeralign",
                textclass:"content1 centertext",
                textdata:data.string.totalpieces
            },
            {
                textdiv:"sureyshare centeralign",
                textclass:"content1 centertext",
                textdata:data.string.sureyshare
            },
            {
                textdiv:"ririshare centeralign",
                textclass:"content1 centertext",
                textdata:data.string.ririshare
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "stackofpear",
                    imgclass: "relativecls img1",
                    imgid: 'sixpearsImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "surey",
                    imgclass: "relativecls img2",
                    imgid: 'sureyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "riri",
                    imgclass: "relativecls img3",
                    imgid: 'ririImg',
                    imgsrc: ""
                }
            ]
        }]
    },
    //slide 9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"toptext centeralign",
                textclass:"content centertext ",
                textdata:data.string.diyq8
            },
            {
                splitintofractionsflag: true,
                textdiv:"div1 centeralign",
                textclass:"content1 centertext",
                textdata:data.string.p2frac1
            },
            {
                splitintofractionsflag: true,
                textdiv:"div2 centeralign",
                textclass:"content1 centertext",
                textdata:data.string.p2frac1
            },
            {
                textdiv:"div3 centeralign",
                textclass:"chapter centertext",
                textdata:data.string.greaterthan
            },
            {
                textdiv:"submitbtn centeralign",
                textclass:"content1 centertext",
                textdata:data.string.submit
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "optimg1",
                    imgclass: "relativecls img2",
                    imgid: 'sureybgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "optimg2",
                    imgclass: "relativecls img3",
                    imgid: 'riribgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "uparrow commonarrow",
                    imgclass: "relativecls img4",
                    imgid: 'uparrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "downarrow commonarrow",
                    imgclass: "relativecls img5",
                    imgid: 'downarrowImg',
                    imgsrc: ""
                }
            ]
        }]
    },
    //slide10
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"toptext centeralign",
                textclass:"content centertext ",
                textdata:data.string.diyq9
            },
            {
                splitintofractionsflag: true,
                textdiv:"div1 centeralign",
                textclass:"content1 centertext",
                textdata:data.string.p2frac1
            },
            {
                splitintofractionsflag: true,
                textdiv:"div2 centeralign",
                textclass:"content1 centertext",
                textdata:data.string.p2frac1
            },
            {
                textdiv:"div3 centeralign",
                textclass:"chapter centertext",
                textdata:data.string.greaterthan
            },
            {
                textdiv:"submitbtn centeralign",
                textclass:"content1 centertext",
                textdata:data.string.submit
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "optimg1",
                    imgclass: "relativecls img2",
                    imgid: 'chankheybgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "optimg2",
                    imgclass: "relativecls img3",
                    imgid: 'riribgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "uparrow commonarrow",
                    imgclass: "relativecls img4",
                    imgid: 'uparrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "downarrow commonarrow",
                    imgclass: "relativecls img5",
                    imgid: 'downarrowImg',
                    imgsrc: ""
                }
            ]
        }]
    },
    //slide 11
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"toptext centeralign",
                textclass:"content centertext ",
                textdata:data.string.diyq10
            },
            {
                splitintofractionsflag: true,
                textdiv:"div1 centeralign",
                textclass:"content1 centertext",
                textdata:data.string.p2frac1
            },
            {
                splitintofractionsflag: true,
                textdiv:"div2 centeralign",
                textclass:"content1 centertext",
                textdata:data.string.p2frac1
            },
            {
                textdiv:"div3 centeralign",
                textclass:"chapter centertext",
                textdata:data.string.greaterthan
            },
            {
                textdiv:"submitbtn centeralign",
                textclass:"content1 centertext",
                textdata:data.string.submit
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "optimg1",
                    imgclass: "relativecls img2",
                    imgid: 'chankheybgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "optimg2",
                    imgclass: "relativecls img3",
                    imgid: 'sureybgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "uparrow commonarrow",
                    imgclass: "relativecls img4",
                    imgid: 'uparrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "downarrow commonarrow",
                    imgclass: "relativecls img5",
                    imgid: 'downarrowImg',
                    imgsrc: ""
                }
            ]
        }]
    },
    //slide 12
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"toptext centeralign",
                textclass:"content centertext ",
                textdata:data.string.diyq11
            },
            {
                splitintofractionsflag: true,
                textdiv:"div6 centeralign",
                textclass:"content1 centertext",
                textdata:data.string.p2frac1
            },
            {
                splitintofractionsflag: true,
                textdiv:"div7 centeralign",
                textclass:"content1 centertext",
                textdata:data.string.p2frac1
            },
            {
                splitintofractionsflag: true,
                textdiv:"div8 centeralign",
                textclass:"content1 centertext",
                textdata:data.string.p2frac1
            },
            {
                textdiv:"div4 centeralign",
                textclass:"chapter centertext",
                textdata:data.string.greaterthan
            },
            {
                textdiv:"div5 centeralign",
                textclass:"chapter centertext",
                textdata:data.string.greaterthan
            },
            {
                textdiv:"submitbtn centeralign",
                textclass:"content1 centertext",
                textdata:data.string.submit
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'infobold',
                textdiv:"info centeralign zoomInEffect",
                textclass:"content1 centertext",
                textdata:data.string.info
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "optionimg1",
                    imgclass: "relativecls img2",
                    imgid: 'chankheybgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "optionimg2",
                    imgclass: "relativecls img3",
                    imgid: 'sureybgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "optionimg3",
                    imgclass: "relativecls img4",
                    imgid: 'riribgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "uparrow1 commonarrow",
                    imgclass: "relativecls img5",
                    imgid: 'uparrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "downarrow1 commonarrow",
                    imgclass: "relativecls img6",
                    imgid: 'downarrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "uparrow2 commonarrow1",
                    imgclass: "relativecls img7",
                    imgid: 'uparrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "downarrow2 commonarrow1",
                    imgclass: "relativecls img8",
                    imgid: 'downarrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "crossimg",
                    imgclass: "relativecls img9",
                    imgid: 'crossImg',
                    imgsrc: ""
                }
            ]
        }]
    },
    //slide 13
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"toptext centeralign",
                textclass:"content centertext ",
                textdata:data.string.diyq12
            },
            {
                splitintofractionsflag: true,
                textdiv:"div6 centeralign",
                textclass:"content1 centertext",
                textdata:data.string.p2frac1
            },
            {
                splitintofractionsflag: true,
                textdiv:"div7 centeralign",
                textclass:"content1 centertext",
                textdata:data.string.p2frac1
            },
            {
                splitintofractionsflag: true,
                textdiv:"div8 centeralign",
                textclass:"content1 centertext",
                textdata:data.string.p2frac1
            },
            {
                textdiv:"div4 centeralign",
                textclass:"chapter centertext",
                textdata:data.string.greaterthan
            },
            {
                textdiv:"div5 centeralign",
                textclass:"chapter centertext",
                textdata:data.string.greaterthan
            },
            {
                textdiv:"submitbtn centeralign",
                textclass:"content1 centertext",
                textdata:data.string.submit
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'infobold',
                textdiv:"info centeralign zoomInEffect",
                textclass:"content1 centertext",
                textdata:data.string.info1
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "optionimg1",
                    imgclass: "relativecls img2",
                    imgid: 'chankheybgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "optionimg2",
                    imgclass: "relativecls img3",
                    imgid: 'sureybgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "optionimg3",
                    imgclass: "relativecls img4",
                    imgid:'riribgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "uparrow1 commonarrow",
                    imgclass: "relativecls img5",
                    imgid: 'uparrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "downarrow1 commonarrow",
                    imgclass: "relativecls img6",
                    imgid: 'downarrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "uparrow2 commonarrow1",
                    imgclass: "relativecls img7",
                    imgid: 'uparrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "downarrow2 commonarrow1",
                    imgclass: "relativecls img8",
                    imgid: 'downarrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "crossimg",
                    imgclass: "relativecls img9",
                    imgid: 'crossImg',
                    imgsrc: ""
                }
            ]
        }]
    },
    //slide 14
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"toptext centeralign",
                textclass:"content centertext ",
                textdata:data.string.diyq13
            },
            {
                splitintofractionsflag: true,
                textdiv:"div6 centeralign",
                textclass:"content1 centertext",
                textdata:data.string.p2frac1
            },
            {
                splitintofractionsflag: true,
                textdiv:"div7 centeralign",
                textclass:"content1 centertext",
                textdata:data.string.p2frac1
            },
            {
                splitintofractionsflag: true,
                textdiv:"div8 centeralign",
                textclass:"content1 centertext",
                textdata:data.string.p2frac1
            },
            {
                textdiv:"div4 centeralign",
                textclass:"chapter centertext",
                textdata:data.string.greaterthan
            },
            {
                textdiv:"div5 centeralign",
                textclass:"chapter centertext",
                textdata:data.string.greaterthan
            },
            {
                textdiv:"submitbtn centeralign",
                textclass:"content1 centertext",
                textdata:data.string.submit
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "optionimg1",
                    imgclass: "relativecls img2",
                    imgid: 'sureybgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "optionimg2",
                    imgclass: "relativecls img3",
                    imgid: 'chankheybgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "optionimg3",
                    imgclass: "relativecls img4",
                    imgid:'riribgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "uparrow1 commonarrow",
                    imgclass: "relativecls img5",
                    imgid: 'uparrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "downarrow1 commonarrow",
                    imgclass: "relativecls img6",
                    imgid: 'downarrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "uparrow2 commonarrow1",
                    imgclass: "relativecls img7",
                    imgid: 'uparrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "downarrow2 commonarrow1",
                    imgclass: "relativecls img8",
                    imgid: 'downarrowImg',
                    imgsrc: ""
                }
            ]
        }]
    },
    //slide 15
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"toptext centeralign",
                textclass:"content centertext ",
                textdata:data.string.diyq14
            },
            {
                splitintofractionsflag: true,
                textdiv:"div6 centeralign",
                textclass:"content1 centertext",
                textdata:data.string.p2frac1
            },
            {
                splitintofractionsflag: true,
                textdiv:"div7 centeralign",
                textclass:"content1 centertext",
                textdata:data.string.p2frac1
            },
            {
                splitintofractionsflag: true,
                textdiv:"div8 centeralign",
                textclass:"content1 centertext",
                textdata:data.string.p2frac1
            },
            {
                textdiv:"div4 centeralign",
                textclass:"chapter centertext",
                textdata:data.string.greaterthan
            },
            {
                textdiv:"div5 centeralign",
                textclass:"chapter centertext",
                textdata:data.string.greaterthan
            },
            {
                textdiv:"submitbtn centeralign",
                textclass:"content1 centertext",
                textdata:data.string.submit
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "optionimg1",
                    imgclass: "relativecls img2",
                    imgid: 'chankheybgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "optionimg2",
                    imgclass: "relativecls img3",
                    imgid: 'riribgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "optionimg3",
                    imgclass: "relativecls img4",
                    imgid:'sureybgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "uparrow1 commonarrow",
                    imgclass: "relativecls img5",
                    imgid: 'uparrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "downarrow1 commonarrow",
                    imgclass: "relativecls img6",
                    imgid: 'downarrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "uparrow2 commonarrow1",
                    imgclass: "relativecls img7",
                    imgid: 'uparrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "downarrow2 commonarrow1",
                    imgclass: "relativecls img8",
                    imgid: 'downarrowImg',
                    imgsrc: ""
                }
            ]
        }]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var time;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    var totalpieces = 0;
    var sureygot = 0;
    var ririgot = 1;
    var chankheygot = 0;
    var masterlist;



    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "coverpageImg", src: imgpath1 + "p104a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "diyImg", src: imgpath1 + "a_10.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pear", src: imgpath + "pear-ko-tukra.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sixpearsImg", src: imgpath1 + "six_pears.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sevenpearsImg", src: imgpath1 + "seven_pears.png", type: createjs.AbstractLoader.IMAGE},
            {id: "eightpearsImg", src: imgpath1 + "eight_pears.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ninepearsImg", src: imgpath1 + "nine_pears.png", type: createjs.AbstractLoader.IMAGE},
            {id: "tenpearsImg", src: imgpath1 + "ten_pears.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sureyImg", src: imgpath1 + "squirrel.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ririImg", src: imgpath1 + "rhino.png", type: createjs.AbstractLoader.IMAGE},
            {id: "chankheyImg", src: imgpath1 + "chankhey's.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sureybgImg", src: imgpath + "look-up-whitebg.png", type: createjs.AbstractLoader.IMAGE},
            {id: "riribgImg", src: imgpath + "look-up-whitebgR.png", type: createjs.AbstractLoader.IMAGE},
            {id: "chankheybgImg", src: imgpath + "chankheybox.png", type: createjs.AbstractLoader.IMAGE},
            {id: "uparrowImg", src: imgpath1 + "up.png", type: createjs.AbstractLoader.IMAGE},
            {id: "downarrowImg", src: imgpath1 + "down.png", type: createjs.AbstractLoader.IMAGE},
            {id: "crossImg", src: "images/white_wrong.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            // {id: "sound_0", src: soundAsset + "p1_s0.ogg"},
            // {id: "sound_1", src: soundAsset + "p1_s1.ogg"},
            // {id: "sound_2", src: soundAsset + "p1_s2.ogg"},
            // {id: "sound_3", src: soundAsset + "p1_s3.ogg"},
            // {id: "sound_4", src: soundAsset + "p1_s4.ogg"},
            // {id: "sound_5", src: soundAsset + "p1_s5.ogg"},
            // {id: "sound_6", src: soundAsset + "p1_s6.ogg"},
            // {id: "sound_7", src: soundAsset + "p1_s7.ogg"},
            // {id: "sound_8", src: soundAsset + "p1_s8.ogg"},
            // {id: "sound_9", src: soundAsset + "p1_s9.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("fractioncontent", $("#fractioncontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        splitintofractions($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 0:
                play_diy_audio();
                navigationcontroller(countNext, $total_page);
                break;
            case 1:
                count = 0;
                var min = 6;
                var max = 10;
                totalpieces = Math.floor(Math.random() * (max - min + 1) + min);
                sureygot = Math.floor(Math.random()*((totalpieces -2)- 1+1)+1);
                ririgot =generateRirishare(totalpieces,sureygot);
                chankheygot = totalpieces -sureygot -ririgot;
                if(sureygot == ririgot && sureygot == chankheygot && ririgot == chankheygot){
                    generaltemplate();
                }
                $(".share").first().text(totalpieces);
                $(".share").next().text(sureygot);
                $(".share").last().text(ririgot);
                for(var i =0;i<ririgot;i++)
                    $(".box1").append("<img class='pearImg' src= '"+ preload.getResult("pear").src +"'>");
                for(var i =0;i<chankheygot;i++)
                    $(".box2").append("<img class='pearImg' src= '"+ preload.getResult("pear").src +"'>");
                for(var i =0;i<sureygot;i++)
                    $(".box3").append("<img class='pearImg' src= '"+ preload.getResult("pear").src +"'>");
                masterList = [
                    { key: "sureybgImg", val: sureygot },
                    { key: "riribgImg", val: ririgot},
                    { key: "chankheybgImg", val: chankheygot}
                ];
                masterlist = masterList.sort(function(a,b){
                    return a.val > b.val;
                });
                console.log("masterList ==== "+masterList[0].key + masterList[0].val);
                console.log("masterList ==== "+masterList[1].key + masterList[1].val);
                console.log("masterList ==== "+masterList[2].key + masterList[2].val);
                navigationcontroller(countNext, $total_page);

                break;
            case 2:
            case 3:
                imageload();
                var classoption = ["option opt1","option opt2","option opt3","option opt4"]
                popualteoption(sureygot,classoption);
                break;
            case 4:
                imageload();
                var classoption = ["option opt1","option opt2","option opt3","option opt4"]
                popualteoption(sureygot+ririgot,classoption);
                break;
            case 5:
                $(".toptext").find(".share").first().find(".top").text(sureygot);
                $(".toptext").find(".share").first().find(".bottom").text(totalpieces);
                $(".toptext").find(".share").last().find(".top").text(ririgot);
                $(".toptext").find(".share").last().find(".bottom").text(totalpieces);
                imageload();
                $(".toptext").css("padding-bottom","5%");
                $(".toptext").find("span .bottom").css("border-color","black");
                var classoption = ["option opt1","option opt2","option opt3","option opt4"]
                popualteoption(sureygot+ririgot,classoption);
                break;
            case 6:
                $(".option").css("width","16%");
                imageload();
                $(".toptext").css("padding-bottom","5%");
                $(".toptext").find(".share").first().find(".top").text(sureygot+ririgot);
                $(".toptext").find(".share").first().find(".bottom").text(totalpieces);
                $(".toptext").find("span .bottom").css("border-color","black");
                var classoption = ["option o1","option o2"];
                popopt(classoption);
                $(".content2").css("font-size","3.5vh");
                break;
            case 7:
                imageload();
                $(".toptext").find("span .bottom").css("border-color","black");
                var classoption = ["option option1","option option2","option option3","option option4"]
                popualteoption(chankheygot,classoption);
                break;
            case 8:
                imageload();
                var classoption = ["option opt1","option opt2","option opt3","option opt4"]
                popualteoption(totalpieces,classoption);
                break;
            case 9:
                enterbtn();
                $("span .bottom").css("border-color","black");
                $(".div1 .top").text(sureygot);
                $(".div1 .bottom").text(totalpieces);
                $(".div2 .top").text(ririgot);
                $(".div2 .bottom").text(totalpieces);
                checkans2(sureygot,ririgot,false);
                 break;
            case 10:
                enterbtn();
                $("span .bottom").css("border-color","black");
                $(".div1 .top").text(chankheygot);
                $(".div1 .bottom").text(totalpieces);
                $(".div2 .top").text(ririgot);
                $(".div2 .bottom").text(totalpieces);
                checkans2(chankheygot,ririgot,false);
                break;
            case 11:
                enterbtn();
                $("span .bottom").css("border-color","black");
                $(".div1 .top").text(chankheygot);
                $(".div1 .bottom").text(totalpieces);
                $(".div2 .top").text(sureygot);
                $(".div2 .bottom").text(totalpieces);
                checkans2(chankheygot,sureygot);
                break;
            case 12:
                enterbtn();
                $(".info,.crossimg").hide();
                var first = masterList[0].key;
                var second = masterList[1].key;
                var third = masterList [2].key;
                $(".optionimg1 img").attr("src",preload.getResult(first).src);
                $(".optionimg2 img").attr("src",preload.getResult(second).src);
                $(".optionimg3 img").attr("src",preload.getResult(third).src);
                $("span .bottom").css("border-color","black");
                $(".div6 .top").text(masterList[0].val);
                $(".div6 .bottom").text(totalpieces);
                $(".div7 .top").text(masterList[1].val);
                $(".div7 .bottom").text(totalpieces);
                $(".div8 .top").text(masterList[2].val);
                $(".div8 .bottom").text(totalpieces);
                checkans2(masterList[0].val,masterList[1].val,masterList[2].val);
                break;
            case 13:
                enterbtn();
                $(".info,.crossimg").hide();
                var first = masterList[2].key;
                var second = masterList[1].key;
                var third = masterList [0].key;
                $(".optionimg1 img").attr("src",preload.getResult(first).src);
                $(".optionimg2 img").attr("src",preload.getResult(second).src);
                $(".optionimg3 img").attr("src",preload.getResult(third).src);
                $("span .bottom").css("border-color","black");
                $(".div6 .top").text(masterList[2].val);
                $(".div6 .bottom").text(totalpieces);
                $(".div7 .top").text(masterList[1].val);
                $(".div7 .bottom").text(totalpieces);
                $(".div8 .top").text(masterList[0].val);
                $(".div8 .bottom").text(totalpieces);
                checkans2(masterList[2].val,masterList[1].val,masterList[0].val);
                break;
            case 14:
                enterbtn();
                $("span .bottom").css("border-color","black");
                $(".div6 .top").text(sureygot);
                $(".div6 .bottom").text(totalpieces);
                $(".div7 .top").text(chankheygot);
                $(".div7 .bottom").text(totalpieces);
                $(".div8 .top").text(ririgot);
                $(".div8 .bottom").text(totalpieces);
                checkans2(sureygot,chankheygot,ririgot);
                break;
            case 15:
                enterbtn();
                $("span .bottom").css("border-color","black");
                $(".div6 .top").text(chankheygot);
                $(".div6 .bottom").text(totalpieces);
                $(".div7 .top").text(ririgot);
                $(".div7 .bottom").text(totalpieces);
                $(".div8 .top").text(sureygot);
                $(".div8 .bottom").text(totalpieces);
                checkans2(chankheygot,ririgot,sureygot);
                break;
            default:
                navigationcontroller(countNext, $total_page);
                break;
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }


    function checkans(correctwrongimg1){
        $(".option").on("click",function () {
            createjs.Sound.stop();
            if($(this).attr("data-answer").toString().trim()=='correct') {
                $(this).addClass("correctans");
                $(".option").addClass("avoid-clicks");
                $(this).append("<img class='"+correctwrongimg1+"' src='images/right.png'/>");
                play_correct_incorrect_sound(1);
                navigationcontroller(countNext, $total_page);
            }
            else{
                $(this).addClass("wrongans avoid-clicks");
                $(this).append("<img class='"+correctwrongimg1+"' src='images/wrong.png'/>");
                play_correct_incorrect_sound(0);
            }
        });
    }

    function checkans2(option1,option2,option3){
        $(".div3,.div4,.div5").find("p").text("");
        count = 0;
        var array = [data.string.greaterthan,data.string.lessthan,data.string.equalto]
        var array1 = [data.string.greaterthan,data.string.lessthan,data.string.equalto]
        var array2 = [data.string.greaterthan,data.string.lessthan,data.string.equalto]
        $(".uparrow,.downarrow").click(function(){
            array = optionselect(array,$(".div3 p"));
            $(".div3").removeClass("wrongans");
        });
        $(".uparrow1,.downarrow1").click(function(){
            array1 = optionselect(array1,$(".div4 p"));
            $(".div4").removeClass("wrongans");
        });
        $(".uparrow2,.downarrow2").click(function(){
            array2 = optionselect(array2,$(".div5 p"));
            $(".div4").removeClass("wrongans");
        });
        $(".submitbtn").click(function(){
            var correctclick = 1;
            var currentdivval = option3?$(".div4 p").text().toString().trim():$(".div3 p").text().toString().trim();;
            if(!option3) {
                if ((option1 > option2 && currentdivval == data.string.greaterthan) || (option1 < option2 && currentdivval == data.string.lessthan) || (option1 == option2 && currentdivval == data.string.equalto)) {
                    $(".div3").removeClass("wrongans").addClass("correctans");
                    $(".commonarrow,.submitbtn").addClass("avoid-clicks");
                    play_correct_incorrect_sound(1);
                    navigationcontroller(countNext, $total_page);
                }
                else {
                    play_correct_incorrect_sound(0);
                    $(".div3").addClass("wrongans");
                }
            }
            else if(option3){
                var test = 0;
                var currentdivval1 = $(".div5 p").text().toString().trim();
                if ((option1 > option2 && currentdivval == data.string.greaterthan) || (option1 < option2 && currentdivval == data.string.lessthan) || (option1 === option2 && currentdivval == data.string.equalto)) {
                    $(".div4").removeClass("wrongans").addClass("correctans");
                    $(".commonarrow").addClass("avoid-clicks");
                    test++;
                }
                else {
                    $(".div4").addClass("wrongans");
                    test = 0;
                }
                if ((option2 > option3 && currentdivval1 == data.string.greaterthan) || (option2 < option3 && currentdivval1 == data.string.lessthan) || (option3 == option2 && currentdivval1 == data.string.equalto)) {
                    $(".div5").removeClass("wrongans").addClass("correctans avoid_clicks");
                    correctclick = 3;
                    $(".commonarrow1").addClass("avoid-clicks");
                    test++;
                }
                else {
                    correctclick = 0;
                    $(".div5").addClass("wrongans");
                    test = 0;
                }
                if(test==2){
                    play_correct_incorrect_sound(1);
                    navigationcontroller(countNext,$total_page);
                    if((option1>option2 && option2>option3)||(option1<option2 && option2<option3)){
                        $(".coverboardfull div :not(.info,.info p,.infobold,.div6,.div6 p.div7,.div7 p,.div8,.div8 p,.crossimg,.crossimg img)").css("opacity",'0.3');
                        $(".info,.crossimg").show();
                        $(".crossimg").click(function(){
                            $(".coverboardfull div").css("opacity","1");
                            $(".coverboardfull div").find('*').css("opacity","1");
                            $(".info,.crossimg").hide();

                        });
                    }

                }
                else
                    play_correct_incorrect_sound(0);


            }

        });
    }
    function optionselect(array,optionselectelem){
        optionselectelem.parent().removeClass("wrongans");
        array.splice(array.indexOf(optionselectelem.text()), 1);
        var random = array[Math.floor(Math.random() * array.length)];
        optionselectelem.text(random);
        count++;
        if (count > 1) {
            count = 0;
            return [data.string.greaterthan, data.string.lessthan, data.string.equalto]
        }
        else
            return array;
    }


    function splitintofractions($splitinside){
        typeof $splitinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
        if($splitintofractions.length > 0){
            $.each($splitintofractions, function(index, value){
                $this = $(this);
                var tobesplitfraction = $this.html();
                if($this.hasClass('fraction')){
                    tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
                    tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
                }else{
                    tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
                    tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
                }


                tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
                $this.html(tobesplitfraction);
            });
        }
    }
    function shufflehint(optionclass,option) {
        var optdiv = $(".coverboardfull");

        for (var i = optdiv.find("."+option).length; i >= 0; i--) {
            optdiv.append(optdiv.find("."+option).eq(Math.random() * i | 0));
        }
        optdiv.find("."+option).removeClass().addClass("current");
        optdiv.find(".current").each(function (index) {
            $(this).addClass(optionclass[index]);
            $(this).addClass($(this).attr("data-answer"));
        });
    }

    function imageload(){
        var stackofpearelm =  $(".stackofpear").find("img");
        switch (totalpieces){
            case 7:
                stackofpearelm.attr("src",preload.getResult("sevenpearsImg").src);
                break;
            case 8:
                stackofpearelm.attr("src",preload.getResult("eightpearsImg").src);
                break
            case 9:
                stackofpearelm.attr("src",preload.getResult("ninepearsImg").src);
                break;
            case 10:
                stackofpearelm.attr("src",preload.getResult("tenpearsImg").src);
                break;
        }
        for(var i =0;i<sureygot;i++)
            $(".peardiv1").append("<img class='pearImg' src= '"+ preload.getResult("pear").src +"'>");
        for(var i =0;i<ririgot;i++)
            $(".peardiv2").append("<img class='pearImg' src= '"+ preload.getResult("pear").src +"'>");

    }

    function popopt(classoption){
        var srtotal = sureygot+ririgot;

        $(".o1").find(".share").first().find(".top").text(totalpieces);
        $(".o1").find(".share").first().find(".bottom").text(totalpieces);
        $(".o1").find(".share").last().find(".top").text(sureygot+ririgot);
        $(".o1").find(".share").last().find(".bottom").text(totalpieces);
        $(".o2").find(".share").first().find(".top").text(totalpieces);
        $(".o2").find(".share").first().find(".bottom").text(totalpieces);
        $(".o2").find(".share").last().find(".top").text(sureygot+ririgot);
        $(".o2").find(".share").last().find(".bottom").text(totalpieces);
        shufflehint(classoption,"option");
        checkans("correctwrongimg");
    }


    function popualteoption(correctans,classoption){

        var items = [];
        for(var i = 1;i<totalpieces;i++){
            items.push(i);
        }
        items.splice(items.indexOf(ririgot), 1);
        items.splice(items.indexOf(sureygot), 1);
        items.splice(items.indexOf(correctans), 1);
        var random = items[Math.floor(Math.random()*items.length)]
        $(".opt1,.option1").find(".top").text(correctans!=totalpieces?totalpieces:chankheygot);
        $(".opt1,.option1").find(".bottom").text(totalpieces);
        $(".opt2,.option2").find(".top").text(correctans);
        $(".opt2,.option2").find(".bottom").text(totalpieces);
        $(".opt3,.option3").find(".top").text(ririgot!=correctans?ririgot:sureygot);
        $(".opt3,.option3").find(".bottom").text(totalpieces);
        $(".opt4,.option4").find(".top").text(random);
        $(".opt4,.option4").find(".bottom").text(totalpieces);
        shufflehint(classoption,"option");
        checkans("correctwrongimg");
    }
    function generateRandom(min, max, donotmatchoption) {
        var num = Math.floor(Math.random() * (max - min + 1)) + min;
        return (num == donotmatchoption) ? generateRandom(min, max) : num;
    }
    function generateRirishare(totalpieces,sureygot){
        count++;
        var tempriri =  generateRandom(totalpieces-2,1,sureygot);
        return ((totalpieces -sureygot -tempriri)>=1)?tempriri:(count<10?generateRirishare(totalpieces,sureygot):1);
    }
});
